<?php

return array(
  'rootLogger' => array(
    'appenders' => array(
      'default'
    )
  ),
  'appenders' => array(
    'default' => array(
      'class' => 'LoggerAppenderFile',
      'layout' => array(
        'class' => 'LoggerLayoutPattern',
        'params' => array(
          'conversionPattern' => '%d{m/d/y H:i:s,u}%msg %n'
        )
      ),
      'params' => array(
        'file' => trim("/var/log/htt/api_" . getenv('HTT_ENV')) . ".log",
        'append' => true
      )
    )
  )
);

?>
