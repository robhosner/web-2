<?php


include_once('db.php');
include_once('lib/provisioning/functions.php');


/*

teldata_change_db(); // connect to the DB

assign_ultra_customer_dealer(
  array(
    'iccid'         => '800000000000000001',
    'customer_id'   => '123456',
    'final_state'   => 'Neutral',
    'masteragent'   => '12',
    'distributor'   => '11',
    'store'         => '10',
    'user_id'       => '90',
    'agent'         => '80',
    'dealer'        => '70'
  )
);

$r = record_ICCID_activation(
  array(
    'iccid'         => '800000000000000001',
    'customer_id'   => '1234',
    'masteragent'   => '12',
    'distributor'   => '11',
    'store'         => '10',
    'user_id'         => '90',
    'agent'           => '80',
    'transition_uuid' => '70'
  )
);

*/

set_redis_provisioning_values(1234, 11, 12, 13, 14, 15, NULL, '{TX-F2F63CCA3A011474-679805F0B8753048}');
set_redis_provisioning_values(4320, 21, NULL, 23, 24, 25);

$r = get_redis_provisioning_values(1234);
print_r($r);

$r = get_redis_provisioning_values(4320);
print_r($r);

clear_redis_provisioning_values(1234);
print_r($r);

$r = get_redis_provisioning_values(1234);
print_r($r);

$r = get_redis_provisioning_values(4320);
print_r($r);

exit;

// wrong customer but valid transition
$r = get_redis_provisioning_values(1111, '{TX-F2F63CCA3A011474-679805F0B8753048}');
print_r($r);

// valid customer but wrong transition
$r = get_redis_provisioning_values(1234, '{TR-F2F63CCA3A011474-679805F0B8753048}');
print_r($r);

// customer only
$r = get_redis_provisioning_values(4320);
print_r($r);



?>
