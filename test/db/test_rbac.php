<?php

include_once 'db.php';
include_once 'db/rbac/activity.php';
include_once 'db/rbac/api.php';
include_once 'db/rbac/role.php';


teldata_change_db();


abstract class AbstractTestStrategy
{
  abstract function test( $argv );
}

class Test_rbac_get_activities_from_plan_state extends AbstractTestStrategy
{
  function test( $argv )
  {
    return get_activities_from_plan_state($argv[2]);
  }
}

class Test_rbac_remove_role_activity extends AbstractTestStrategy
{
  function test( $argv )
  {
    return remove_role_activity($argv[2],$argv[3]);
  }
}

class Test_rbac_get_activity_by_role extends AbstractTestStrategy
{
  function test( $argv )
  {
    return get_activity_by_role(NULL,$argv[2]);
  }
}

class Test_rbac_get_api_by_role extends AbstractTestStrategy
{
  function test( $argv )
  {
    return get_api_by_role(NULL, $argv[2], (empty($argv[3]) ? NULL : $argv[3]));
  }
}

class Test_rbac_add_api_activity extends AbstractTestStrategy
{
  function test( $argv )
  {
    return add_api_activity($argv[2],$argv[3]);
  }
}

class Test_rbac_add_api extends AbstractTestStrategy
{
  function test( $argv )
  {
    return add_api($argv[2],$argv[3]);
  }
}

class Test_rbac_add_activity extends AbstractTestStrategy
{
  function test( $argv )
  {
    return add_activity($argv[2]);
  }
}

class Test_rbac_add_role_activity extends AbstractTestStrategy
{
  function test( $argv )
  {
    return add_role_activity($argv[2],$argv[3]);
  }
}

class Test_rbac_get_plan_states_by_activity extends AbstractTestStrategy
{
  function test( $argv )
  {
    return get_plan_states_by_activity(1);
  }
}

class Test_rbac_can_map_plan_state_to_api extends AbstractTestStrategy
{
  function test( $argv )
  {
    $check = can_map_plan_state_to_api($argv[2],$argv[3]);

    return ( $check ? 'OK' : 'KO' );
  }
}

class Test_rbac_add_plan_state_activity extends AbstractTestStrategy
{
  function test($argv)
  {
    return add_plan_state_activity($argv[2], $argv[3]);
  }
}

class Test_rbac_deploy_rbac_to_prod extends AbstractTestStrategy
{
  function test($argv)
  {
    $outcome = exec_queries_in_transaction(
      deploy_rbac_queries()
    );

    if ( $outcome['success'] )
      echo "Success\n";
    else
      echo "Failure\n";
/*
DELETE FROM ULTRA_DATA.rbac.API_ACTIVITY;
DELETE FROM ULTRA_DATA.rbac.PLAN_STATE_ACTIVITY
DELETE FROM ULTRA_DATA.rbac.ROLE_ACTIVITY;
DELETE FROM ULTRA_DATA.rbac.ACTIVITY;
DELETE FROM ULTRA_DATA.rbac.API;


Set Identity_Insert ULTRA_DATA.rbac.API ON;
 INSERT INTO ULTRA_DATA.rbac.API (
  API_ID,
  API_NAME,
  API_DESC,
  API_ACTIVE_FLAG,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 )
 SELECT
  API_ID,
  API_NAME,
  API_DESC,
  API_ACTIVE_FLAG,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 FROM
  ULTRA_DEVELOP_TEL.rbac.API;
Set Identity_Insert ULTRA_DATA.rbac.API OFF;


Set Identity_Insert ULTRA_DATA.rbac.ACTIVITY ON;
 INSERT INTO ULTRA_DATA.rbac.ACTIVITY (
  ACTIVITY_ID,
  ACTIVITY_NAME,
  ACTIVITY_TYPE_ID,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 )
 SELECT
  ACTIVITY_ID,
  ACTIVITY_NAME,
  ACTIVITY_TYPE_ID,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 FROM
  ULTRA_DEVELOP_TEL.rbac.ACTIVITY;
Set Identity_Insert ULTRA_DATA.rbac.ACTIVITY OFF;


INSERT INTO ULTRA_DATA.rbac.ROLE_ACTIVITY (
 ROLE_ID,
 ACTIVITY_ID,
 LAST_MOD_TIMESTAMP,
 LAST_MOD_USERNAME,
 ACTIVE_FLAG
)
SELECT
 devra.ROLE_ID,
 devra.ACTIVITY_ID,
 devra.LAST_MOD_TIMESTAMP,
 devra.LAST_MOD_USERNAME,
 devra.ACTIVE_FLAG
FROM
 ULTRA_DEVELOP_TEL.rbac.ROLE_ACTIVITY devra;


INSERT INTO ULTRA_DATA.rbac.PLAN_STATE_ACTIVITY (
 PLAN_STATE_ID,
 ACTIVITY_ID,
 ACTIVE_FLAG,
 LAST_MOD_TIMESTAMP,
 LAST_MOD_USERNAME
)
SELECT
 devpa.PLAN_STATE_ID,
 devpa.ACTIVITY_ID,
 devpa.ACTIVE_FLAG,
 devpa.LAST_MOD_TIMESTAMP,
 devpa.LAST_MOD_USERNAME
FROM
 ULTRA_DEVELOP_TEL.rbac.PLAN_STATE_ACTIVITY devpa;


INSERT INTO ULTRA_DATA.rbac.API_ACTIVITY (
 API_ID,
 ACTIVITY_ID,
 ACTIVE_FLAG,
 LAST_MOD_TIMESTAMP,
 LAST_MOD_USERNAME
)
SELECT
 devaa.API_ID,
 devaa.ACTIVITY_ID,
 devaa.ACTIVE_FLAG,
 devaa.LAST_MOD_TIMESTAMP,
 devaa.LAST_MOD_USERNAME
FROM
 ULTRA_DEVELOP_TEL.rbac.API_ACTIVITY devaa;

*/
  }
}


# perform test #


$testClass = 'Test_rbac_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$r = $testObject->test( $argv );

print_r($r);


/*
php test/db/test_rbac.php get_activities_from_plan_state $PLAN_STATE
php test/db/test_rbac.php get_api_by_role      'Administrator All Access'
php test/db/test_rbac.php get_activity_by_role 'Administrator All Access'
php test/db/test_rbac.php remove_role_activity $ACTIVITY_NAME $ROLE_ID
php test/db/test_rbac.php add_role_activity $ACTIVITY_NAME $ROLE_ID
php test/db/test_rbac.php add_api_activity  $ACTIVITY_NAME $API_NAME
php test/db/test_rbac.php add_activity      $ACTIVITY_NAME
php test/db/test_rbac.php add_api           $API_NAME $API_DESC
php test/db/test_rbac.php get_plan_states_by_activity $ACTIVITY_ID
php test/db/test_rbac.php can_map_plan_state_to_api   $PLAN_STATE $API_NAME
php test/db/test_rbac.php add_plan_state_activity $ACTIVITY_NAME $PLAN_STATE_NAME
php test/db/test_rbac.php deploy_rbac_to_prod
*/

?>
