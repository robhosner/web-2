<?php

require_once 'db.php';
require_once 'db/ultra_cc_holder_tokens.php';

teldata_change_db();

$s = add_to_ultra_cc_holder_tokens(
  array(
    'cc_holders_id'    => 1,
    'gateway'          => 'test_gw',
    'merchant_account' => 'test_m_a',
    'token'            => 'test_token'
  )
);

$customer_id = 31;
$result = get_cc_info_and_tokens_by_customer_id($customer_id);
echo 'count: ' . count($result) . ', result: ' . print_r($result, true) . "\n";

$customer = get_customer_from_customer_id($customer_id);
$result = customer_has_credit_card($customer);
echo 'customer_has_credit_card = ' . ($result ? 'TRUE' : 'FALSE') . "\n";

?>
