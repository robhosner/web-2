<?php

require_once 'db.php';

teldata_change_db();

$customer_id = 31;

$transition_uuid = getNewTransitionUUID('test ' . time());
$action_uuid = getNewActionUUID('test ' . time());

print_r(array(
  'transition_uuid' => $transition_uuid,
  'action_uuid'     => $action_uuid
));

//

$context = array(
  'customer_id' => $customer_id
);

$current_cos_id = 1;
$current_state  = 'Active';
$target_cos_id  = 1;
$target_state   = 'Active';
$label          = 'Test_create_transition_action_sql';
$priority       = 1;

$check = log_transition($transition_uuid,
  $context,
  $current_cos_id,
  $current_state,
  $target_cos_id,
  $target_state,
  $label,
  $priority);

echo ( !$check ) ? 'log_transition failed!' : 'log_transition success!';

//

$params      = array( 'customer_id' => $customer_id );
$aparams     = array(
  'seq'  => 1,
  'type' => 'sql',
  'name' => 'UPDATE ACCOUNTS SET packaged_balance1 = 0 WHERE CUSTOMER_ID = %d'
);

$check = log_action(
  $transition_uuid,
  $action_uuid,
  $aparams,
  $params,
  'OPEN',
  NULL,
  NULL);

echo ( !$check ) ? 'log_action failed!' : 'log_action success!';

$query = "SELECT TOP 1 * FROM htt_transition_log with (nolock) 
WHERE transition_uuid = '$transition_uuid'";

$result = mssql_fetch_all_rows(logged_mssql_query($query));

print_r($result);

$query = "SELECT TOP 1 * FROM htt_action_log with (nolock) 
WHERE transition_uuid = '$transition_uuid'";

$result = mssql_fetch_all_objects(logged_mssql_query($query));

print_r($result);

$query = "SELECT TOP 10 * FROM htt_action_parameter_log with (nolock)
WHERE action_uuid = '$action_uuid'";

$result = mssql_fetch_all_objects(logged_mssql_query($query));

print_r($result);

exit;

echo open_actions_reaper('test_mode', 30) . PHP_EOL;
