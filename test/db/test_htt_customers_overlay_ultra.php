<?php


include_once('db.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('test/AbstractTestStrategy.php');
include_once('Ultra/Lib/DB/Setter/Customer.php');


# php test/db/test_htt_customers_overlay_ultra.php port_in_requested_customer_finder
# php test/db/test_htt_customers_overlay_ultra.php get_latest_customer_plan_from_transition $MSISDN
# php test/db/test_htt_customers_overlay_ultra.php getPreferredLanguageFromMSISDN $MSISDN
# php test/db/test_htt_customers_overlay_ultra.php create_ultra_customer_db_transaction
# php test/db/test_htt_customers_overlay_ultra.php get_ultra_customer_from_customer_id $ID
# php test/db/test_htt_customers_overlay_ultra.php get_customer_from_customer_id       $ID
# php test/db/test_htt_customers_overlay_ultra.php get_customer_from_actcode           $ID
# php test/db/test_htt_customers_overlay_ultra.php ultra_info
# php test/db/test_htt_customers_overlay_ultra.php cc_info
# php test/db/test_htt_customers_overlay_ultra.php trackDataSOC customer_id data_soc


teldata_change_db(); // connect to the DB


class Test_get_customer_from_actcode extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $result = get_customer_from_actcode( $argv[2] );

    print_r($result);

    #$state = get_customer_state($result);
    #print_r( $state );
  }
}


class Test_cc_info extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $params = array(
      'customer_id'       => '31',
      'state_region'      => 'WA',
      'postal_code'       => '98765',
      'country'           => 'US',
      'city'              => 'La Jolla',
      'address1'          => '123 456',
      'address2'          => '543 111'
#,'cc_number'      => '4195473398654406'
#,'cvv'            => '1212'
#,'expires_date'   => '1212'
#,'cc_postal_code' => '12121'
    );

    $result = \Ultra\Lib\DB\Setter\Customer\cc_info($params);

    print_r($result);
  }
}


class Test_ultra_info extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $params = array(
      'customer_id'           => '31',
      'notes'                 => 'testA',
      'current_mobile_number' => '1001001009',
      'current_iccid'         => '1001001001100100201',
      'easypay_activated'     => '1',
      'stored_value'          => '23.45',
      'plan_state'            => 'Active',
      'preferred_language'    => 'EN',
      #'plan_started'         => '',
      #'plan_expires'         => '',
      #'tos_accepted'         => '',
      'monthly_cc_renewal'    => '1',
      'activation_iccid'      => '1001001001100100301',
      'customer_source'       => 'testB'
    );

    $result = \Ultra\Lib\DB\Setter\Customer\ultra_info($params);

    print_r($result);
  }
}


class Test_get_customer_from_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $result = get_customer_from_customer_id( $argv[2] );

    print_r($result);

    if ( is_object($result) )
    {
      $plan = get_plan_name_from_short_name( get_plan_from_cos_id( $result->cos_id ) );

      echo "plan = $plan\n";
    }
  }
}

class Test_create_many_ultra_customers extends AbstractTestStrategy
{
  function test()
  {
exit;

    $sql = "select top 1111 customer_id from htt_customers_overlay_ultra where CURRENT_ICCID_FULL = '1234512345123451234' and current_mobile_number = '1234567890'";

    $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

    $query_result = \__::flatten( $query_result );

    foreach ( $query_result as $customer_id )
    {
      $sql = "update htt_customers_overlay_ultra set current_mobile_number = '100' + cast( cast( RAND()*10000000 as bigint ) as varchar(7)) where customer_id = ".$customer_id;

      run_sql_and_check( $sql );

      echo "customer_id = ".$customer_id."\n";
    }

exit;

    $c = 5000;

    while($c--)
    {

    $result = create_ultra_customer_db_transaction(
      array(
        "preferred_language"    => 'EN',
        "cos_id"                => get_cos_id_from_plan('STANDBY'),
        "first_name"            => 'testerABC',
        "last_name"             => 'testerABC',
        "postal_code"           => '12345',
        "country"               => 'USA',
        "e_mail"                => 'customerEMail@NULL',
        "plan_state"            => 'Neutral',
        "plan_started"          => 'NULL',
        "plan_expires"          => 'NULL',
        "customer_source"       => 'TEST',
        "current_iccid"         => '123451234512345123',
        "current_iccid_full"    => '1234512345123451234',
        "masteragent"           => 12345678,
        "dealer"                => 1234567,
        "distributor"           => 123456,
        "userid"                => 12345,
        "current_mobile_number" => '1234567890',
        "activation_type"       => 'PORT',
        "funding_source"        => 'EPAY',
        "funding_amount"        => 19,
        "last_attempted_transition" => 'last_attempted_transition'
      )
    );

    if ( empty($result['customer_id']) )
    {
      print_r($result);
      exit;
    }

    $customer_id = $result['customer_id'];

    $sql = "UPDATE ACCOUNTS SET COS_ID = ".COSID_ULTRA_NINETEEN." WHERE CUSTOMER_ID = ".$customer_id;

    run_sql_and_check( $sql );

    $sql = "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET PLAN_STATE = 'Provisioned' WHERE CUSTOMER_ID = ".$customer_id;

    run_sql_and_check( $sql );

    echo "customer_id = ".$customer_id."\n";
    }
  }
}

class Test_create_ultra_customer_db_transaction extends AbstractTestStrategy
{
  function test()
  {
    $result = create_ultra_customer_db_transaction(
      array(
        "preferred_language"    => 'ES',
        "cos_id"                => get_cos_id_from_plan('STANDBY'),
        "first_name"            => 'customerFirstName',
        "last_name"             => 'customerLastName',
        "postal_code"           => '12345',
        "country"               => 'USA',
        "e_mail"                => 'customerEMail@NULL',
        "plan_state"            => 'Neutral',
        "plan_started"          => 'NULL',
        "plan_expires"          => 'NULL',
        "customer_source"       => 'TEST',
        "current_iccid"         => '987654321098765432',
        "current_iccid_full"    => '9876543210',
        "masteragent"           => 12345678,
        "dealer"                => 1234567,
        "distributor"           => 123456,
        "userid"                => 12345,
        "current_mobile_number" => '8765432109',
        "activation_type"       => 'PORT',
        "funding_source"        => 'EPAY',
        "funding_amount"        => 49,
        "last_attempted_transition" => 'last_attempted_transition'
      )
    );

    print_r($result);
  }
}


class Test_port_in_requested_customer_finder extends AbstractTestStrategy
{
  function test()
  {
    $finder = port_in_requested_customer_finder();

    $r = mssql_fetch_all_objects(logged_mssql_query($finder));

    print_r($r[0]);
  }
}


class Test_get_ultra_customer_from_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = get_ultra_customer_from_customer_id( $argv[2] , array("customer_id","current_mobile_number","mvne") );

    print_r($r);
  }
}


class Test_getPreferredLanguageFromMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $msisdn = $argv[2];

    $preferredLanguage = getPreferredLanguageFromMSISDN( $msisdn );

    echo "preferredLanguage = $preferredLanguage\n";
  }
}


class Test_get_latest_customer_plan_from_transition extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $msisdn = $argv[2];

    $latest_plan = get_latest_customer_plan_from_transition( array( 'msisdn' => $msisdn ) );

    echo "latest_plan = $latest_plan\n";
  }
}

class Test_trackDataSOC extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $input = array();

    $customer_id = $argv[2];
    $data_soc = $argv[3];

    // fail
    $result = trackDataSOC();
    echo 'NO PARAMS' . PHP_EOL;
    print_r($result);

    // fail
    $result = trackDataSOC($customer_id);
    echo 'NO $data_soc' . PHP_EOL;
    print_r($result);

    // valid
    $result = trackDataSOC($customer_id, $data_soc);
    echo 'VALID INPUT' . PHP_EOL;
    print_r($result);
  }
}

# perform test #


$class_ext = $argv[1];

$testClass = 'Test_'.$class_ext;

echo "\n$testClass\n\n";

$testObject = new $testClass();

$testObject->test();

echo "\n";


?>
