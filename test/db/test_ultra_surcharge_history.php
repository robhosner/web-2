<?php

require_once 'db.php';
require_once 'db/ultra_surcharge_history.php';

teldata_change_db();

/*
$r = add_to_ultra_surcharge_history(
  array(
    'surcharge_type'    => 'TEST TYPE',
    'transaction_id'    => 123123,
    'amount'            => 1.1,
    'rule'              => 'TEST RULE',
    'basis'             => 'TEST BASIS',
    'location'          => '12345',
    'status'            => 'TEST STATUS'
  )
);
*/

$r = add_to_ultra_surcharge_history(
  array(
    'surcharge_type'    => 'TEST TYPE1',
    'amount'            => 1.1,
    'rule'              => 'TEST RULE1',
    'basis'             => 'TEST BASIS1',
    'location'          => '98765',
    'status'            => 'TEST STATUS1',
    'customer_id'       => 552,
    'entry_type'        => 'LOAD',
    'reference'         => '{7302F35F-B390-BB96-73F6-7749D4EC8E1F}'
  )
);

print_r($r);

?>
