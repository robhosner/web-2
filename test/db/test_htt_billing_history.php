<?php

include_once 'db.php';


/**
 * test_get_customer_time_zone
 */
function test_get_billing_transaction_history()
{ global $argv;

  $params['start_epoch'] = $argv[2];
  $params['end_epoch'] = $argv[3];
  $params['customer_id'] = $argv[4];

  $result = get_billing_transaction_history($params);
  print_r($result);
}

/**
 * test_function get_current_cycle_purchase_history
 */
function test_get_current_cycle_purchase_history()
{ global $argv;

  $result = get_current_cycle_purchase_history($argv[2], $argv[3], $argv[4]);
  print_r($result);
}


if (empty($argv[1]))
{
  echo "ERROR: no test name given\n";
  return;
}

$test = $argv[1];
if ( ! is_callable($test))
{
  echo "ERROR: function '$test' does not exist\n";
  return;
}

// run the test
teldata_change_db();
$test();
echo "\n";

# php test/db/test_htt_billing_history.php test_get_billing_transaction_history 1399925929 1402525929 3351
# php test/db/test_htt_billing_history.php test_function get_current_cycle_purchases CUSTOMER_ID ENTRY_TYPE DESCRIPTION

?>
