<?php

require_once 'db.php';
require_once 'db/ultra_acc/command_invocations.php';

/**
 * get_ultra_acc_command_invocations
 *
 * @param  array $params
 * @param  string $limit
 * @param  string $order
 * @param  boolean $nolock
 * @return object[]
function get_ultra_acc_command_invocations( $params, $limit=NULL, $order=NULL, $nolock=TRUE )
*/

\Ultra\Lib\DB\ultra_acc_connect();

$x = get_ultra_acc_command_invocations(
  array(
    'CORRELATION_ID' => time()
  ),
  1,
  NULL
);

print_r($x);

