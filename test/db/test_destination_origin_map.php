<?php

require_once 'db.php';
require_once 'db/destination_origin_map.php';

teldata_change_db();

$customer_id = 10881;

$query    = make_find_ultra_customer_query_from_customer_id($customer_id);
$customer = find_customer( $query );

if ($customer)
{
  // insert into DESTINATION_ORIGIN_MAP
  $check = destination_origin_map_insert_after_activation($customer);

  if ( $check )
  {
    echo "OK\n";
  }
  else
  {   
    echo "KO\n";
  }
}
else
{
  echo "Cannot find customer id $customer_id\n";
}

?>
