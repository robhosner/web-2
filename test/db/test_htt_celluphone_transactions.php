<?php

require_once 'db.php';
require_once 'db/htt_celluphone_transactions.php';

function line_break($n = 1)
{
  for ($i = 0; $i < $n; $i++)
    echo PHP_EOL . '*** *** *** ***' . PHP_EOL;
}

echo 'UTILITY FUNCTIONS'; line_break();

echo htt_celluphone_when_cos_id_then_plan_short();
line_break();

echo htt_celluphone_cos_id_case_values();
line_break(2);

echo celluphone_pq_query();
line_break();

echo celluphone_transaction_newperiod_log_query();
line_break();

echo celluphone_activation_log_query();
line_break();

echo celluphone_transaction_log_query();
line_break();

echo celluphone_qualified_spend_query();
line_break();

echo celluphone_transaction_log_customercare_query();
line_break();

echo celluphone_changeplan_log_query();
line_break();
