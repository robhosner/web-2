<?php

// script to move a transition back from the archive tables

// HTT_ACTION_PARAMETER_ARCHIVE --> HTT_ACTION_PARAMETER_LOG
//           HTT_ACTION_ARCHIVE --> HTT_ACTION_LOG
//       HTT_TRANSITION_ARCHIVE --> HTT_TRANSITION_LOG

// Usage: php test/db/un_archive_transition.php $TRANSITION_UUID


include_once "db.php";


teldata_change_db();


$transition_uuid = $argv[1];
dlog('',"transition_uuid = $transition_uuid");


$sql = sprintf(
  "SELECT TRANSITION_UUID FROM HTT_TRANSITION_LOG WHERE TRANSITION_UUID = %s",
  mssql_escape_with_zeroes($transition_uuid)
);

$query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

if ( $query_result && is_array($query_result) && count($query_result) )
  die("TRANSITION_UUID $transition_uuid already in HTT_TRANSITION_LOG\n");

show_before_un_archive($transition_uuid);

$result = un_archive($transition_uuid);

// we log all the content
function show_before_un_archive($transition_uuid)
{
  $sql = sprintf(
   "SELECT * FROM HTT_TRANSITION_ARCHIVE
     WHERE  TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  dlog('',"HTT_TRANSITION_ARCHIVE dump :\n%s",$query_result);

  $sql = sprintf(
   "SELECT * FROM HTT_ACTION_ARCHIVE
     WHERE  TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    dlog('',"HTT_ACTION_ARCHIVE dump :\n%s",$query_result);

    foreach($query_result as $n => $action)
    {
      $sql = sprintf(
        "SELECT * FROM HTT_ACTION_PARAMETER_ARCHIVE WHERE ACTION_UUID = %s",
        mssql_escape_with_zeroes($action->ACTION_UUID)
      );

      $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

      dlog('',"HTT_ACTION_PARAMETER_ARCHIVE dump :\n%s",$query_result);
    }
  }
  else
  {
    die("No actions found for TRANSITION_UUID $transition_uuid\n");
  }
}

// proceed with the copy
function un_archive($transition_uuid)
{
  $sql = sprintf(
    "SELECT ACTION_UUID
     FROM   HTT_ACTION_ARCHIVE
     WHERE  TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  $sql_array = array();

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    $sql_array[] = copy_transition($transition_uuid);
    $sql_array[] = copy_actions($transition_uuid);

    foreach($query_result as $n => $action)
    {
      $sql_array[] = copy_archive_action_parameter($action[0]);
      $sql_array[] = rm_archive_action_parameter($action[0]);
    }

    $sql_array[] = rm_actions($transition_uuid);
    $sql_array[] = rm_transition($transition_uuid);

    return exec_queries_in_transaction( $sql_array );
  }
  else
  {
    die("No actions found for TRANSITION_UUID $transition_uuid\n");
  }
}

function copy_transition($transition_uuid)
{
  return sprintf(
    "INSERT INTO HTT_TRANSITION_LOG
     (CUSTOMER_ID, TRANSITION_UUID, STATUS, CREATED, CLOSED, FROM_COS_ID, FROM_PLAN_STATE, TO_COS_ID, TO_PLAN_STATE, CONTEXT, HTT_ENVIRONMENT, TRANSITION_LABEL)
     SELECT
     CUSTOMER_ID, TRANSITION_UUID, STATUS, CREATED, CLOSED, FROM_COS_ID, FROM_PLAN_STATE, TO_COS_ID, TO_PLAN_STATE, CONTEXT, HTT_ENVIRONMENT, TRANSITION_LABEL
     FROM HTT_TRANSITION_ARCHIVE
     WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

function copy_actions($transition_uuid)
{
  return sprintf(
    "INSERT INTO HTT_ACTION_LOG
     (ACTION_UUID, TRANSITION_UUID, STATUS, CREATED, PENDING_SINCE, CLOSED, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, ACTION_RESULT, ACTION_SQL)
     SELECT ACTION_UUID, TRANSITION_UUID, STATUS, CREATED, PENDING_SINCE, CLOSED, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, ACTION_RESULT, ACTION_SQL
     FROM HTT_ACTION_ARCHIVE
     WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

function rm_actions($transition_uuid)
{
  return sprintf(
    "DELETE FROM HTT_ACTION_ARCHIVE WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

function rm_transition($transition_uuid)
{
  return sprintf(
    "DELETE FROM HTT_TRANSITION_ARCHIVE WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

function un_archive_action_parameter($action_uuid)
{
  return sprintf(
    "DELETE HTT_ACTION_PARAMETER_ARCHIVE
     OUTPUT
     DELETED.ACTION_UUID, DELETED.PARAM, DELETED.VAL
     INTO HTT_ACTION_PARAMETER_LOG
     WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

function un_archive_action($action_uuid)
{
  return sprintf(
    "DELETE HTT_ACTION_ARCHIVE
     OUTPUT
     DELETED.ACTION_UUID, DELETED.TRANSITION_UUID, DELETED.STATUS, DELETED.CREATED, DELETED.PENDING_SINCE, DELETED.CLOSED, DELETED.ACTION_SEQ, DELETED.ACTION_TYPE, DELETED.ACTION_NAME, DELETED.ACTION_RESULT, DELETED.ACTION_SQL
     INTO HTT_ACTION_LOG
     WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

function rm_archive_action_parameter($action_uuid)
{
  return sprintf(
    "DELETE FROM HTT_ACTION_PARAMETER_ARCHIVE WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

function copy_archive_action_parameter($action_uuid)
{
  return sprintf(
    "INSERT INTO HTT_ACTION_PARAMETER_LOG
     (ACTION_UUID, PARAM, VAL)
     SELECT ACTION_UUID, PARAM, VAL
     FROM HTT_ACTION_PARAMETER_ARCHIVE
     WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

?>
