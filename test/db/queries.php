<?php


include_once('db.php');


$params = array();


abstract class AbstractTestStrategy
{
  abstract function test($params);
}


class Test_row_number extends AbstractTestStrategy
{
  function test($params)
  {
    // ROW_NUMBER() proof of concept

    $sql = "
    WITH SUBSELECT AS
    (
      SELECT ROW_NUMBER() OVER (ORDER BY u.customer_id) as row,
             u.customer_id
      FROM   htt_customers_overlay_ultra u
      WHERE  u.customer_id > 1910
    )
    SELECT row,customer_id
    FROM   SUBSELECT
    WHERE  row BETWEEN 2 AND 4
    ";

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    print_r($data);
  }
}


teldata_change_db();

$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test($params);

echo "\n\n";

# Usage:
# php test/db/queries.php row_number

?>
