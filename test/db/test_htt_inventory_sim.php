<?php

include_once 'db.php';
require_once 'Ultra/Lib/DB/Getter.php';

teldata_change_db();

$msisdn = '1001001000';

$x = get_brand_id_from_msisdn($msisdn);

echo "brand_id = $x\n";

exit;

$iccid = '101010101010101004';

$x = get_brand_id_from_iccid( $iccid );

echo "brand_id = $x\n";

exit;

// test: no OFFER_ID
$ICCIDs = get_htt_inventory_sim_from_actcode('66666666669');
print_r($ICCIDs);

// test: existing OFFER_ID
$ICCIDs = get_htt_inventory_sim_from_actcode('10101010101');
print_r($ICCIDs);

// test get_htt_inventory_sim_from_iccid
$SIM = get_htt_inventory_sim_from_iccid('101010101010101002');
print_r($SIM);
exit;

$sql = htt_inventory_sim_update_query(
  array(
    "batch_sim_set" => array('123400789012345676'),
    "mvne" => 3
  )
);

echo "$sql\n";

$sql = htt_inventory_sim_update_query(
  array(
    "batch_sim_set" => array('1234007890123456767'),
    "mvne" => 4
  )
);

echo "$sql\n";


// PROD-178
$sql = htt_inventory_sim_update_query(
  array(
    "batch_sim_set" => array('1234007890123456767'),
    "inventory_masteragent" => '12335'
  )
);

echo "$sql\n";


/**
 * Generates n number of ICCIDs that 'always succeed'.
 *
 * @param integer $number_of_iccids_to_generate
 * @param integer $starting_iccid
 *
 * @author Ben Walters
 */
function generateIccidInsertStatements($number_of_iccids_to_generate, $starting_iccid) {
  for ($i = 0; $i < $number_of_iccids_to_generate; $i++) {
    $iccid_number = $starting_iccid + $i;
    $iccid_full   = luhnenize($iccid_number);

    $sql = htt_inventory_sim_insert_query(
      array(
        "iccid_number"      => $iccid_number,
        "imsi"              => "null",
        "inventory_status"  => "AT_FOUNDRY",
        "last_changed_by"   => "",
        "created_by"        => "jsteinmetz",
        "iccid_batch_id"    => "ALWAYS_OK",
        "iccid_full"        => $iccid_full,
        "expires_date"      => "null",
        "pin1"              => "",
        "puk1"              => "",
        "pin2"              => "",
        "puk2"              => "",
        "other"             => "ALWAYSOK"
      )
    );

		if (!is_mssql_successful(logged_mssql_query($sql))) {
			die("query failed\n");
		}
  }
}

// generateIccidInsertStatements(100, 100111010101011000);

/*
$value1 = '8901260842102249977';

$value2 = '890126084210224997';

$value3 = '8111008000008000118';

$x = \Ultra\Lib\DB\Getter\getScalar('ICCID', $value1, 'MVNE');

$y = \Ultra\Lib\DB\Getter\getScalar('ICCID', $value2, 'MVNE');

$z = \Ultra\Lib\DB\Getter\getScalar('ICCID', $value3, 'MVNE');

echo "$x\n$y\n$z\n";
*/

