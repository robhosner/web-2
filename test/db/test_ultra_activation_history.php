<?php

require_once('db.php');

/**
 * test_get_report_activation_activity_2014
 */
function test_get_report_activation_activity_2014()
{
  $params = array(
    'date_from' => time(),
    'date_to'   => time()+2
  );

  $db = 'ULTRA_CELLUPHONE_DEV';

  $result2014 = get_report_activation_activity_2014( $params , $db );

  print_r($result2014);
}

/**
 * test_get_report_activation_activity_2015
 */
function test_get_report_activation_activity_2015()
{
  $params = array(
    'date_from' => time(),
    'date_to'   => time()+2
  );

  $db = 'ULTRA_CELLUPHONE_DEV';

  $result2015 = get_report_activation_activity_2015( $params , $db );

  print_r($result2015);
}

/**
 * test_get_ultra_dealer_customers_by_plan
 */ 
function test_get_ultra_dealer_customers_by_plan()
{
  // test dealer 7036: 2 active, 2 neutral, 3 provisined
  $dealer = 7036;
  $result = get_ultra_dealer_customers_by_plan($dealer);
  print_r($result);
  $expected = array(
    'Active' => 2,
    'Cancelled' => 0,
    'Neutral' => 2,
    'Pre-Funded' => 0,
    'Port-In Requested' => 0,
    'Provisioned' => 3,
    'Suspended' => 0);
  foreach ($expected as $state => $count)
    if ($count != $result[$state])
      echo "ERROR: expected $count but got {$result[$state]}";

  // test dealer 7037: 2 active
  $dealer = 7037;
  $result = get_ultra_dealer_customers_by_plan($dealer);
  print_r($result);
  $expected = array(
    'Active' => 2,
    'Cancelled' => 0,
    'Neutral' => 0,
    'Pre-Funded' => 0,
    'Port-In Requested' => 0,
    'Provisioned' => 0,
    'Suspended' => 0);
  foreach ($expected as $state => $count)
    if ($count != $result[$state])
      echo "ERROR: expected $count but got {$result[$state]}";

  // test dealer parent 7038: 2 active, 2 neutral + counts from children dealers 7036 and 7037
  $dealer = 7038;
  $result = get_ultra_dealer_customers_by_plan($dealer);
  print_r($result);
  $expected = array(
    'Active' => 6,
    'Cancelled' => 0,
    'Neutral' => 4,
    'Pre-Funded' => 0,
    'Port-In Requested' => 0,
    'Provisioned' => 3,
    'Suspended' => 0);
  foreach ($expected as $state => $count)
    if ($count != $result[$state])
      echo "ERROR: expected $count but got {$result[$state]}";
}


/**
 * test_get_activation_history_by_customer_id
 */
function test_get_activation_history_by_customer_id()
{
  $customer_id = 5;
  $x = get_activation_history_by_customer_id( $customer_id );
  print_r($x);
}


/**
 * test_log_port_iccid_in_activation_history
 */
function test_log_port_iccid_in_activation_history()
{
  $customer_id  = 5;
  $iccid        = '1234007890123456767';

  $x = log_port_iccid_in_activation_history( $customer_id , $iccid );
  print_r($x);
}

/**
 * test_add_to_ultra_activation_history
 */
function test_add_to_ultra_activation_history()
{
  $data = array(
  'customer_id' => 31,
  'masteragent' => time(),
  'iccid_full'  => '2345678901234567891',
  'msisdn'      => '2345678901',
  'cos_id'      => 12340,
  'distributor' => ( time() - 10 ),
  'dealer'      => ( time() - 100 ),
  'userid'      => ( time() - 1000 ),
  'activation_type' => 'NEW',
  'funding_source'  => 'TEST',
  'funding_amount'  => 39,
  'last_attempted_transition' => ( time() - 10 ));

  if ( add_to_ultra_activation_history( $data ) ) { echo "OK\n"; } else { echo "KO\n"; }
}

/**
 *
 */
function test_ultra_activation_history_update_query()
{

  $data = array(
  'customer_id' => 31,
  'masteragent' => time(),
  'iccid_full'  => '2345678901234567891',
  'msisdn'      => '2345678901',
  'cos_id'      => 12340,
  'distributor' => ( time() - 10 ),
  'dealer'      => ( time() - 100 ),
  'userid'      => ( time() - 1000 ),
  'activation_type' => 'NEW',
  'funding_source'  => 'TEST',
  'funding_amount'  => 39,
  'last_attempted_transition' => ( time() - 10 ),
  'activation_transition' => 'TEST1',
  'final_state'  => 'TEST2',
  'plan_started_date_time' => 1);

  $q = ultra_activation_history_update_query( $data );

  echo "$q\n";
}

/**
 * test_get_ultra_dealer_activations_by_plan
 */
function test_get_ultra_dealer_activations_by_plan()
{
  $dealer = 7036; // child dealer of 7038: has one L19 activation
  $result = get_ultra_dealer_activations_by_plan($dealer);
  print_r($result);
  $expected = array(
    'L19' => 1,
    'L29' => 0,
    'L39' => 0,
    'L49' => 0,
    'today' => 1);
  foreach ($expected as $state => $count)
    if ($count != $result[$state])
      echo "ERROR: expected $count but got {$result[$state]}";

  $dealer = 7038; // parent dealer: has 2 L19 activations, one is today
  $result = get_ultra_dealer_activations_by_plan($dealer);
  print_r($result);
  $expected = array(
    'L19' => 3,
    'L29' => 0,
    'L39' => 0,
    'L49' => 0,
    'today' => 2);
  foreach ($expected as $state => $count)
    if ($count != $result[$state])
      echo "ERROR: expected $count but got {$result[$state]}";
}

/**
 * test_find_ultra_activation_history
 */
function test_find_ultra_activation_history()
{
  // child dealer 7036 has 7 subs
  $params = array(
    'dealer'      => 7036,
    'epoch_from'  => 1398927600, // midnight of 1 May 2014
    'epuch_to'    => 1401606000); // midnight of 1 June 2014
  $result = find_ultra_activation_history($params);
  echo 'Found ' . count($result->get_data_array()) . " records\n";
  // print_r($result->get_data_array());

  // parent dealer 7038 has 4 subs but childen have 9 more
  $params['dealer'] = 7038;
  $result = find_ultra_activation_history($params);
  echo 'Found ' . count($result->get_data_array()) . " records\n";

}

/**
 * make_where_clause_for_dealer_and_children
 */
function test_make_where_clause_for_dealer_and_children()
{
  $list = make_where_clause_for_dealer_and_children( 13225 );

  print_r($list);

  echo PHP_EOL;
  echo PHP_EOL;

  $list = make_where_clause_for_dealer_and_children( 7038 );

  print_r($list);

  echo PHP_EOL;
  echo PHP_EOL;

  exit;
}

/**
 * make_find_ultra_activation_details
 */
function test_make_find_ultra_activation_details()
{
  $data = make_find_ultra_activation_details(
    array(
      'date_selected' => 'CREATION',
      'customer_id'   => 31,
      'dealer'        => 7038
    )
  );
  print_r($data);
  echo PHP_EOL; echo PHP_EOL;


  $data = make_find_ultra_activation_details(
    array(
      'date_selected' => 'LAST_UPDATED',
      'customer_id'   => 31,
      'dealer'        => 7038
    )
  );
  print_r($data);
  echo PHP_EOL; echo PHP_EOL;

  exit;
}

/**
 * main()
 */
if (empty($argv[1]))
{
  echo "ERROR: no test name given\n";
  return;
}

$test = $argv[1];
if ( ! is_callable($test))
{
  echo "ERROR: function '$test' does not exist\n";
  return;
}

// run the test
teldata_change_db();
$test();
echo "\n";

