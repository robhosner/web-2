<?php

require_once 'db.php';

if ( ! isset($argv[1]) )
  die('First argument missing (number of ICCID to process)'."\n");

$top             = $argv[1];
$min_customer_id = 350000;
$max_customer_id = 790999;

$sql = sprintf("
  SELECT TOP %d CURRENT_ICCID_FULL, COUNT(*)
  FROM   HTT_CUSTOMERS_OVERLAY_ULTRA
  WHERE  CURRENT_ICCID_FULL IS NOT NULL
  AND    CURRENT_ICCID_FULL != ''
  AND    CURRENT_ICCID_FULL > '0'
  AND    CUSTOMER_ID >= %d
  AND    CUSTOMER_ID <= %d
  GROUP BY CURRENT_ICCID_FULL
  HAVING COUNT(*) > 1
  ORDER BY CURRENT_ICCID_FULL",
  $top,
  $min_customer_id,
  $max_customer_id
);

teldata_change_db();

$query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

if ( $query_result && is_array($query_result) && count($query_result) )
{
  echo count($query_result)." dupe(s) found in the given range ( $min_customer_id , $max_customer_id )\n";

  fix_duplicate_customers( $query_result );
}
else
{
  echo "Nothing to do\n";
}

exit;

function fix_duplicate_customers( $query_result )
{
  foreach( $query_result as $row )
    fix_duplicate_customer( $row );
}

function fix_duplicate_customer( $row )
{
  echo "\nICCID ".$row[0]." repeated ".$row[1]." times\n";

  if ( ! $row[0] || ! is_numeric($row[0]) )
    return NULL;

  // get all customers with the same CURRENT_ICCID_FULL $row[0]
  $sql = sprintf(
    "SELECT CUSTOMER_ID , PLAN_STATE , CURRENT_ICCID , CURRENT_MOBILE_NUMBER
     FROM   HTT_CUSTOMERS_OVERLAY_ULTRA WHERE CURRENT_ICCID_FULL = %s ORDER BY CUSTOMER_ID",
    mssql_escape_with_zeroes( $row[0] )
  );

  $result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
  {
    print_r($result);

    $plan_state_count = array(
      'Active'         => 0,
      'Neutral'        => 0,
      'Port-In Denied' => 0,
      'Provisioned'    => 0,
      'Suspended'      => 0,
      'Cancelled'      => 0
    );

    $customers = array();

    // loop through duplicate customers
    foreach( $result as $data )
    {
      if ( ! isset($plan_state_count[ $data[1] ]) )
        $plan_state_count[ $data[1] ] = 1;
      else
        $plan_state_count[ $data[1] ]++;

      $customers[] = $data[0];
    }

    print_r($customers);

    print_r($plan_state_count);

    if ( ( $plan_state_count['Neutral'] || $plan_state_count['Port-In Denied'] || $plan_state_count['Cancelled']  )
      && ( $plan_state_count['Active']  || $plan_state_count['Provisioned']    || $plan_state_count['Suspended'] )
    )
      delete_duplicates( $result );
    elseif( ( ( $result[0][1] == 'Port-In Requested' ) || ( $result[1][1] == 'Active' ) ) && ( $result[1][1] == 'Pre-Funded' ) )
      delete_duplicate( $result[1][0] , $result[1][1] , $result[1][2] , $result[1][3] );
    elseif( ( ( $result[0][1] == 'Provisioned'       ) || ( $result[0][1] == 'Port-In Requested' ) ) && ( $result[1][1] == 'Active'     ) )
      delete_duplicate( $result[0][0] , $result[0][1] , $result[0][2] , $result[0][3] );
    #elseif( ( $result[1][1] == 'Provisioned'       ) && ( ( $result[0][1] == 'Active'     ) ) )
    #  delete_duplicate( $result[1][0] , $result[1][1] , $result[1][2] , $result[1][3] );
    elseif( ( $result[0][1] == 'Port-In Requested' ) && ( $result[1][1] == 'Provisioned' ) )
      delete_duplicate( $result[0][0] , $result[0][1] , $result[0][2] , $result[0][3] );
    elseif( ( $result[1][1] == 'Port-In Requested' ) && ( $result[0][1] == 'Provisioned' ) )
      delete_duplicate( $result[1][0] , $result[1][1] , $result[1][2] , $result[1][3] );
    elseif( ( $result[1][1] == 'Provisioned' ) && ( $result[0][1] == 'Active' ) )
      delete_duplicate( $result[1][0] , $result[1][1] , $result[1][2] , $result[1][3] );
    elseif( ( $result[1][1] == 'Neutral' ) && ( $result[0][1] == 'Port-In Requested' ) )
      delete_duplicate( $result[1][0] , $result[1][1] , $result[1][2] , $result[1][3] );
    elseif( ( $result[1][1] == 'Neutral' ) && ( $result[0][1] == 'Port-In Denied' ) )
      delete_duplicate( $result[1][0] , $result[1][1] , $result[1][2] , $result[1][3] );
    elseif( ( $result[0][1] == 'Neutral' ) && ( $result[1][1] == 'Port-In Denied' ) )
      delete_duplicate( $result[0][0] , $result[0][1] , $result[0][2] , $result[0][3] );
    elseif( ( $plan_state_count['Active'] == count($result) )
         || ( $plan_state_count['Neutral'] == count($result) )
         || ( $plan_state_count['Provisioned'] == count($result) )
         || ( $plan_state_count['Port-In Denied'] == count($result) )
    )
    {
      $sql = htt_inventory_sim_select_query(
        array(
          "iccid_full" => luhnenize( $result[1][2] )
        )
      );

      $result_iccid = mssql_fetch_all_objects(logged_mssql_query($sql));

      if ( $result_iccid && is_array($result_iccid) && count($result_iccid) && in_array( $result_iccid[0]->CUSTOMER_ID , $customers ) )
      {
        // we have more Active/Neutral customers, but the one we want to keep is HTT_INVENTORY_SIM.CUSTOMER_ID

        foreach( $result as $data )
          if ($result_iccid[0]->CUSTOMER_ID != $data[0] )
            delete_duplicate( $data[0] , $data[1] , $data[2] , $data[3] );
          else
            echo "Keeping customer id ".$data[0]."\n";
      }
      elseif( ( $plan_state_count['Neutral'] == count($result) ) || ( $plan_state_count['Port-In Denied'] == count($result) ) )
      {
        // keep only the last one
        delete_duplicate( $result[0][0] , $result[0][1] , $result[0][2] , $result[0][3] );
      }
    }
  }
  else
  {
    echo "No customers found\n";
  }
}

function delete_duplicates( $result )
{
  if ( count( $result ) > 4 )
  {
    echo "Too many duplicates\n";
    return NULL;
  }

  foreach( $result as $data )
    if ( in_array( $data[1] , array( 'Neutral' , 'Port-In Denied' , 'Cancelled' ) ) )
      delete_duplicate( $data[0] , $data[1] , $data[2] , $data[3] );
}

function delete_duplicate( $customer_id , $plan_state , $iccid , $msisdn )
{
  echo "Deleting customer_id $customer_id ( ICCID = $iccid ; MSISDN = $msisdn )\n";

  sleep(1);

  $sql = htt_cancellation_reasons_insert_query(
    array(
      'customer_id'   => $customer_id,
      'reason'        => 'duplicate bug',
      'type'          => 'VOID',
      'status'        => $plan_state,
      'agent'         => 'fix_duplicate_customers.php',
      'ever_active'   => 0
    )
  );

  if ( is_mssql_successful(logged_mssql_query($sql) ) )
    echo "insert into htt_cancellation_reasons succeeded\n";
  else
    echo "insert into htt_cancellation_reasons failed\n";

  $sql = sprintf(
    "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA
     SET PLAN_STATE = 'Cancelled' , CURRENT_ICCID = NULL , CURRENT_ICCID_FULL = NULL , ACTIVATION_ICCID = NULL , ACTIVATION_ICCID_FULL = NULL , CURRENT_MOBILE_NUMBER = NULL
     WHERE CUSTOMER_ID = %d",
    $customer_id
  );

  if ( is_mssql_successful(logged_mssql_query($sql) ) )
    echo "customer deletion succeeded\n";
  else
    echo "customer deletion failed\n";
}

?>
