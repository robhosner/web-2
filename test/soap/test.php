<?php

echo "start\n";

$wsdl = 'runners/amdocs/TMobile_MVNE_4_5_prod_proxy.wsdl';

$command = 'QuerySubscriber';

$params = array(
  'MSISDN'   => '1001001000',
  'ICCID'    => '8901260962111192628',
  'UserData' => array(
    'senderId' => 'MVNEACC',
    'channelId' => 'ULTRA',
    'timeStamp' => '2014-10-13T12:49:00'
  )
);

echo "wsdl = $wsdl\n";

$local_cert = '/etc/httpd/certs/acc/ULTRA.pem';

try
{
  $client = new SoapClient(
    $wsdl,
    array(
        "trace"         => 1, 
        "exceptions"    => true, 
        "local_cert"    => $local_cert, 
        'passphrase'    => 'mvne1',
        "uri"           => "urn:xmethods-delayed-quotes",
        "style"         => SOAP_RPC,
        "use"           => SOAP_ENCODED,
        "soap_version"  => SOAP_1_2 ,
    )
  );

  echo "client:\n";

  print_r( $client );

  $result = $client->__soapCall(
    $command,
    array( $command => $params )
  );

  echo "result:\n";

  print_r( $result );

  echo "====== REQUEST HEADERS =====" . PHP_EOL;

  var_dump($client->__getLastRequestHeaders());

  echo "========= REQUEST ==========" . PHP_EOL;

  var_dump($client->__getLastRequest());

  echo "========= RESPONSE =========" . PHP_EOL;

  var_dump($result);
}
catch( \Exception $e )
{
  print_r($e);
}

?>
