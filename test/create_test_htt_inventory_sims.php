<?php

/*

$argv[1]        == $brand
$argv[2] if set == $date (20151131)

*/

if ( ! isset($argv[1]))
{
  echo 'FIRST ARGUMENT BRAND_ID required as first parameter' . PHP_EOL;
  die;
}

if ( ! in_array($argv[1], array(1, 2)))
{
  echo 'FIRST ARGUMENT BRAND_ID invalid' . PHP_EOL;
  die;
}

if (isset($argv[2]) && strlen($argv[2]) != 8)
{
  echo 'SECOND ARGUMENT DATE (not required) should be in format 20151131' . PHP_EOL;
  die;
}

require_once 'db.php';

$batch_amount = 1; // amount of sims to create

$prefix = '1010101'; // override identifier

$date = (isset($argv[2])) ? $argv[2] : date('Ymd');


$brand_id = $argv[1];

$iccids = array();

$ident_1 = 10;
$ident_2 = 1;

for ($i = 0; $i < $batch_amount; $i++)
{
  $iccid = $prefix . $date . $ident_1 . $ident_2;
  $iccids[] = array($iccid, luhnenize($iccid));

  if ($ident_2 > 8)
  {
    $ident_1++;
    $ident_2 = 1;
  }
  else
  {
    $ident_2++;
  }
}

teldata_change_db();

foreach ($iccids as $iccid)
{
  $query = "INSERT INTO HTT_INVENTORY_SIM
  (
    ICCID_NUMBER,
    SIM_ACTIVATED,
    INVENTORY_STATUS,
    LAST_CHANGED_DATE,
    CREATED_BY_DATE,
    CREATED_BY,
    ICCID_BATCH_ID,
    ICCID_FULL,
    OTHER,
    GLOBALLY_USED,
    PRODUCT_TYPE,
    SIM_HOT,
    MVNE,
    BRAND_ID
  ) VALUES (
    '{$iccid[0]}',
    0,
    'AT_FOUNDRY',
    GETUTCDATE(),
    GETUTCDATE(),
    'DEVELOPERS',
    'ALWAYS_OK',
    '{$iccid[1]}',
    'ALWAYSOK',
    0,
    'PURPLE',
    1,
    2,
    $brand_id
  )";

  logged_mssql_query($query);
}

foreach ($iccids as $iccid)
{
  echo $iccid[1] . PHP_EOL;
}
