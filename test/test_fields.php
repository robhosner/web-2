<?php

include_once('db.php');
include_once('fields.php');

teldata_change_db(); // connect to the DB

class test_customer_data
{
  public $LOGIN_NAME     = '1348066851';
  public $LOGIN_PASSWORD = 'swordfish';
  public $COS_ID      = 1010;
  public $CUSTOMER_ID = 42;
  public $CUSTOMER    = '1000000025';
  public $ACCOUNT_ID  = 41;
  public $ACCOUNT     = '1348066851';
  public $FIRST_NAME  = 'John';
  public $LAST_NAME   = 'Doe';
  public $COMPANY     = 'Google';
  public $ADDRESS1    = '123 Abe Ave';
  public $ADDRESS2    = '1A';
  public $CITY        = 'New York';
  public $STATE_REGION  = 'NY';
  public $COUNTRY       = 'USA';
  public $POSTAL_CODE   = '10100';
  public $LOCAL_PHONE   = '1000000025';
  public $E_MAIL        = 'abeave@example.com';
  public $CC_NUMBER     = '1234';
  public $CC_EXP_DATE   = '';
  public $ACCOUNT_GROUP_ID  = '1234';
  public $CCV               = '1234';
}

$test_customer = new test_customer_data;

$fields = array();


# 1 ) do nothing since $fields is empty


$test_fields = fields($fields, $test_customer, FALSE);
print_r($test_fields);
# Array ( [0] => Array ( [all_validated] => 1 ) )

$test_fields = fields($fields, $test_customer, TRUE);
print_r($test_fields);
# Array ( [0] => Array ( [all_validated] => 1 ) )


# 2 ) update FIRST_NAME


$fields = array('account_first_name'=>'King');

$test_fields = fields($fields, $test_customer, FALSE);
print_r($test_fields);


# 3 ) update

$fields = array(
  'account_cc_exp'      => '0314',
  'account_cc_cvv'      => '937',
  'account_cc_number'   => '5454871000080964',
  'account_postal_code' => '12345'
);

$test_fields = fields($fields, $test_customer, FALSE);
print_r($test_fields);

?>
