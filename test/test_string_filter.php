<?php

require_once 'db.php';

$values = array(
22,
03,
'04',
'qwertyuioopasdfghjklzxcvbnm',
'люди рождаются свободными и равными в своем достоинстве и правах',
'!@#$%^&*()_+=',
'人人生而自由，在尊嚴和權利上一律平等。他們賦有理性和良心，並應以兄弟關係的精神互相對待'
);

foreach( $values as $value )
{
  echo "BEFORE: $value\n";

  $value = filter_var($value , FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_ENCODE_LOW | FILTER_FLAG_ENCODE_AMP);

  echo "AFTER : $value\n\n";
}


