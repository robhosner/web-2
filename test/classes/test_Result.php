<?php


include_once('classes/Result.php');


$result = new Result();

$x = make_error_Result('e',NULL,'log_test');
print_r($x);

$x = make_ok_Result( array(), 'log_test', 'warning_test');
print_r($x);

$x = make_error_Result(array('e1','e2'));
print_r($x);

$x = make_timeout_Result('error_test', NULL,'log_test');
print_r($x);

if ( $result->is_failure() ) { echo "Fail\n"; } else { echo "Success\n"; }
if ( $result->is_success() ) { echo "Success\n"; } else { echo "Fail\n"; }

$result->succeed();

if ( $result->is_failure() ) { echo "Fail\n"; } else { echo "Success\n"; }
if ( $result->is_success() ) { echo "Success\n"; } else { echo "Fail\n"; }

$result->fail();

if ( $result->is_failure() ) { echo "Fail\n"; } else { echo "Success\n"; }
if ( $result->is_success() ) { echo "Success\n"; } else { echo "Fail\n"; }

$result->add_error("E1");
$result->add_errors(array("E2","E3"));

print_r( $result->get_errors() );

$result->add_warning("W1");
$result->add_warnings(array("W2","W3"));

print_r( $result->get_warnings() );

$result->add_log("L1");
$result->add_logs(array("L2","L3"));

print_r( $result->get_log() );

echo "\n";
print_r( $result->to_string() );
echo "\n";

$new_result = new Result();

$new_result->add_error("E4");

$result->add_result_object($new_result);

print_r( $result->get_errors() );

$result_array = array('errors' => array('E5'),'pending'=>true);

if ( $result->is_pending() ) { echo "Pending\n"; } else { echo "Complete\n"; }

$result->add_result_array($result_array);

if ( $result->is_pending() ) { echo "Pending\n"; } else { echo "Complete\n"; }

print_r( $result->get_errors() );

// test serialization

echo "\n\ntest serialization\n\n";

$return_array = array(
    'success'  => FALSE,
    'pending'  => FALSE,
    'aborted'  => FALSE,
    'errors'   => array('E'),
    'warnings' => array('W'),
    'log'      => array('L')
);

$result_1 = new Result($return_array);
print_r($result_1);

$return_json = '{ "pending":true, "aborted":true, "success":true, "errors":["EE"], "warnings":["WW"], "log":[1348595957]}'; 

$result_2 = new Result($return_json);
print_r($result_2);

