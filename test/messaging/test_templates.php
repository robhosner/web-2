<?php

require_once 'db.php';
require_once 'Ultra/Messaging/Templates.php';

$languages = array(
  'EN',
  'ES',
  'ZH'
);

$param_replacements = array(
  'active_days'         => '30',
  'amount'              => '2.50',
  'balance'             => '99.99',
  'wallet_balance'      => '99.99',
  'stored_value'        => '99.99',
  'old_plan_amount'     => '2.50',
  'value'               => '2.50',
  'bolt_on'             => 'UpData',
  'bolt_ons'            => 'UpIntl and UpData',
  'current_plan_amount' => '29',
  'data_MB'             => '2048',
  'usage'               => '2048',
  'zero_minutes'        => '9999',
  'remaining'           => '2048',
  'gizmo_amount'        => '$10.00',
  'minutes'             => '50',
  'msisdn'              => '1001001000',
  'new_plan_amount'     => '19',
  'personal'            => 'Personal',
  'plan_amount'         => '10.00',
  'plan_amount_upgrade' => '25',
  'plan_name'           => 'Ultra $29',
  'reload_amount'       => '$10.00',
  'renewal_date'        => 'Aug 30 2014',
  'date'                => 'Aug 30 2014',
  'temp_password'       => 'btxhco97',
  'url'                 => 'https://local.m.ultra.me/#/login/%7BC200EF3C-1168-65D4-AFFC-CC9120256FF8%7D 75'
);


/*
 example usage:
   php test/messaging/test_templates.php test_getPostageAppTemplateName porting-success UNIVISION ES
*/
if (function_exists($argv[1]))
  return $argv[1]($argv, $argc);


$templates      = \Ultra\Messaging\Templates\SMS_templates();

print_r( $templates );

exit;

$must_test_manually  = array();
$last_template  = '';

echo "|*Template*|*Language*|*Length*|*Params?*|*Result*|*Notes*|*Message*|\n";
foreach ($templates as $template_name => $template)
{
  $message_type = substr($template_name, 0, (strlen($template_name) - 4));
  if ($last_template != $message_type)
  {
    $parameters = array(
      'message_type' => $message_type
    );

    $last_template  = $message_type;
    foreach ($languages as $language) {
      $message  = \Ultra\Messaging\Templates\SMS_by_language($parameters, $language);
      $params   = '{color:' . ((strpos($message, '__param') !== false) ? 'red}Yes' : 'green}No') . '{color}';
      $result   = '{color:green}Pass{color}';
      $notes    = ' ';

      if (strpos($params, 'Yes') != false)
      {
        foreach ($param_replacements as $search => $replace)
        {
          // If param exists, replace it with a given value
          $message = str_replace('__params__'.$search, $replace, $message);
        }
        $length = strlen($message);
      }
      else
      {
        $length = strlen($message);
      }
      echo "|{$message_type}|{$language}|{$length}|{$params}|{$result}|{$notes}|{$message}|\n";
      ;
    }
  }
}


function test_getPostageAppTemplateName($argv, $argc)
{
  echo "calling getPostageAppTemplateName({$argv[2]}, {$argv[3]}, {$argv[4]}) => ";
  $result = getPostageAppTemplateName($argv[2], $argv[3], $argv[4]);
  echo "'$result'\n";
}
