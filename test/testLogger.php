<?php

include 'UltraLogger.php';

$test = new UltraLogger("TestLogger",TRUE);

$test->setLoggerLevel(LoggerLevel::DEBUG);
$test->logError("Test Error Message");
//$test->logFatal("Massive Error");
$test->logDebug("Test Info");

//$test->logFatalAndNotify("Email Test Notification");
$test->logErrorAndNotify("Should not email or log--FATAL level");

//$test->logFatal("Massive Error--Make Sure context is established");

?>
