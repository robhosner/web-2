<?php

  $db_pwd   = $argv[1];
  $fileName = $argv[2];

  $db_host = 'donald.ultra.me';
  $db_user = 'Raf';
  $db_name = 'INTL_DEV';

  if ( empty($db_pwd) )
    die('Password is missing');

  if ( empty($fileName) )
    die('fileName is missing');

  if (! $link = mssql_connect($db_host, $db_user, $db_pwd))
    die('Unable to connect to database!');

  if (! mssql_select_db($db_name, $link))
    die('Unable to select database!');

  $query = file_get_contents( $fileName );

  echo "$query\n";

  try
  {
    $ret = mssql_query($query);
    # ret should be FALSE on failure, but there exists deadlock results where this is true.

    $mssql_last_message = mssql_get_last_message();
    # returns '' , which == NULL. 

    if ($mssql_last_message != NULL)
    {
      echo "$mssql_last_message\n";
    }
  }
  catch (Exception $e)
  {
    print_r($e);
  }

/*
Script I am trying to use to create stored procedures

php test/test_primo_db.php $PWD /home/rgalli/db/dba/donald/Databases/Intl_Data/StoredProcedures/PRIMO.HTT_PLAN_TRACKER_INSERT.sql
*/
