<?php


include_once('fraud.php');

teldata_change_db(); // connect to the DB

$customer_id = $argv[1];

list( $fraud_error , $fraud_code ) = cc_transactions_fraud_check( $customer_id, 1000000090 );

echo "error = $error\n";

exit;

$test_array = array(
'1.1.1.1',
'208.105.5.30',
'172.16.200.25',
'172.16.200.2',
'172.16.200.14',
'172.16.200.34',
'172.16.200.15',
'172.16.200.24'
);

foreach ( $test_array as $ip )
{
  if ( whitelisted_for_fraud( $ip ) )
    echo "$ip ALLOWED\n";
  else
    echo "$ip VERBOTEN\n";
}

exit;

class test_customer_data
{
  public $LOGIN_NAME     = '1348066851';
  public $LOGIN_PASSWORD = 'swordfish';
  public $COS_ID      = 1010;
  public $CUSTOMER_ID = 0;
  public $CUSTOMER    = '1000000025';
  public $ACCOUNT_ID  = 41;
  public $ACCOUNT     = '1348066851';
  public $FIRST_NAME  = 'John';
  public $LAST_NAME   = 'Doe';
  public $COMPANY     = 'Google';
  public $ADDRESS1    = '123 Abe Ave';
  public $ADDRESS2    = '1A';
  public $CITY        = 'New York';
  public $STATE_REGION  = 'NY';
  public $COUNTRY       = 'USA';
  public $POSTAL_CODE   = '10100';
  public $LOCAL_PHONE   = '1000000025';
  public $E_MAIL        = 'abeave@example.com';
  public $CC_NUMBER     = '1234';
  public $CC_EXP_DATE   = '';
  public $ACCOUNT_GROUP_ID  = '1234';
  public $CCV               = '1234';
}

$customer = new test_customer_data;

$customer->CUSTOMER_ID = $argv[1];

$fraud_error = webcc_fraud_check( $customer );

echo "fraud_error = $fraud_error\n";

/*

$data = array(
  'transition_uuid'   => time(),
  'activation_userid' => '1234',
  'amount' => 39.9,
  'cos_id' => 99999
);

fraud_event($customer, 'provisioning', 'requestProvisionNewCustomerAsync', 'error', $data);

*/

?>
