<?php


# perform actual state transitions of test customer using internal__TakeTransition

# Usage:

# php test/state_machine/test_take_transitions.php TWENTY_NINE Provisioned          TEST OK
# php test/state_machine/test_take_transitions.php THIRTY_NINE Provisioned          TEST OK
# php test/state_machine/test_take_transitions.php FORTY_NINE  Provisioned          TEST OK
# php test/state_machine/test_take_transitions.php TWENTY_NINE Pre-Funded           TEST OK
# php test/state_machine/test_take_transitions.php THIRTY_NINE Pre-Funded           TEST OK
# php test/state_machine/test_take_transitions.php FORTY_NINE  Pre-Funded           TEST OK
# php test/state_machine/test_take_transitions.php TWENTY_NINE 'Port-In Unfunded'
# php test/state_machine/test_take_transitions.php THIRTY_NINE 'Port-In Unfunded'
# php test/state_machine/test_take_transitions.php FORTY_NINE  'Port-In Unfunded'


include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('lib/payments/functions.php');
include_once('test/state_machine/test_util.php');
include_once('web.php');


date_default_timezone_set("America/Los_Angeles");


if ( ! isset($argv[1]) ) { die("1st argument is plan\n\n"); }
$test_plan = $argv[1]; # TWENTY_NINE , THIRTY_NINE , FORTY_NINE


if ( ! isset($argv[2]) ) { die("2nd argument is path\n\n"); }
$test_path = $argv[2]; # Provisioned , Pre-Funded , Port-In Unfunded


$amount = find_credential('plans/Ultra/'.$test_plan.'/cost');


teldata_change_db(); // connect to the DB


$domain  = 'rgalli2-dev.uvnv.com';
$timeout = 10;
$apache_username = 'dougmeli';
$apache_password = 'Flora';
$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


/* *** - 01 - Create a new customer *** */
/* ************************************ */


$test_step = '01';
$customer = test_create_web_user();
$customer_id = $customer->CUSTOMER_ID;

echo "\n\n - $test_step - OK\n\n";


/* *** - 02 - Check customer state (STANDBY/Neutral) *** */
/* ***************************************************** */


$test_step = '02';

$result_decoded = test_check_state_curl($test_step, $customer_id);

test_check_state_neutral($test_step, $result_decoded);

echo "\n\n - $test_step - OK\n\n";


                                    /*  (^._.^)  ----------  (^._.^)  */
if ( $test_path == 'Pre-Funded' ) { /*  (^._.^)  Pre-Funded  (^._.^)  */
                                    /*  (^._.^)  ----------  (^._.^)  */


/* *** - 03a - Change state to Pre-Funded (dry run) expecting failure due to low funds *** */
/* *************************************************************************************** */


$test_step = '03a';

$transition_name = "Pre-Fund SIM $test_plan";

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',FALSE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 04a - Change state to Pre-Funded (dry run) expecting success *** */
/* ********************************************************************** */


$test_step = '04a';

// give some funds
$add_balance_result = func_add_balance(
  array(
    'customer'    => $customer,
    'amount'      => $amount/100, # in $
    'reason'      => 'test reason',
    'reference'   => 'test reference',
    'source'      => 'test source'
  )
);

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 05a - Change state to Pre-Funded (for real) *** */
/* ***************************************************** */


$test_step = '05a';

$result_decoded = test_take_transition_curl($test_step,$customer_id,TRUE,$transition_name,FALSE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 06a - Check customer state Pre-Funded *** */
/* *********************************************** */


$test_step = '06a';

$result_decoded = test_check_state_curl($test_step,$customer_id);

test_check_state_pre_funded($test_step,$result_decoded,$test_plan);

echo "\n\n - $test_step - OK\n\n";


$transition_name = 'Activate Shipped SIM';


                                            /*  (^._.^)  -----------  (^._.^)  */
} else if ( $test_path == 'Provisioned' ) { /*  (^._.^)  Provisioned  (^._.^)  */
                                            /*  (^._.^)  -----------  (^._.^)  */


/* *** - 03b - Change state to Provisioned (dry run) expecting success *** */
/* *********************************************************************** */


$test_step = '03b';

$transition_name = "Provision $test_plan";

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 04b - Change state to Provisioned (for real, faking actions) *** */
/* ********************************************************************** */


$test_step = '04b';

$result_decoded = test_take_transition_curl($test_step,$customer_id,TRUE,$transition_name,FALSE,'FAKE_ACTIONS',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 05b - Check customer state Provisioned *** */
/* ************************************************ */


$test_step = '05b';

$result_decoded = test_check_state_curl($test_step,$customer_id);

test_check_state_provisioned($test_step,$result_decoded,$test_plan);

echo "\n\n - $test_step - OK\n\n";


// give some funds so that activation will not fail due to lack of funds
$add_balance_result = func_add_balance(
  array(
    'customer'    => $customer,
    'amount'      => $amount/100, # in $
    'reason'      => 'test reason',
    'reference'   => 'test reference',
    'source'      => 'test source'
  )
);


$transition_name = 'Activate Provisioned '.$test_plan;


                                                 /*  (^._.^)  ------------------------------------  (^._.^)  */
} else if ( $test_path == 'Port-In Unfunded' ) { /*  (^._.^)  Port-In Unfunded , Port-In Requested  (^._.^)  */
                                                 /*  (^._.^)  ------------------------------------  (^._.^)  */


/* *** - 03c - Change state to Port-In Unfunded (dry run) expecting success *** */
/* **************************************************************************** */


$test_step = '03c';

$transition_name = "Request Unfunded Port $test_plan";

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 04c - Change state to Port-In Unfunded (for real, faking actions) *** */
/* *************************************************************************** */


$test_step = '04c';

$result_decoded = test_take_transition_curl($test_step,$customer_id,TRUE,$transition_name,FALSE,'FAKE_ACTIONS',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 05c - Check customer state Port-In Unfunded *** */
/* ***************************************************** */


$test_step = '05c';

$result_decoded = test_check_state_curl($test_step,$customer_id);

test_check_state_port_in_unfunded($test_step,$result_decoded,$test_plan);

echo "\n\n - $test_step - OK\n\n";


// give some funds so that so the next transition will not fail due to lack of funds
$add_balance_result = func_add_balance(
  array(
    'customer'    => $customer,
    'amount'      => $amount/100, # in $
    'reason'      => 'test reason',
    'reference'   => 'test reference',
    'source'      => 'test source'
  )
);


/* *** - 06c - Change state to Port-In Requested (dry run) expecting success *** */
/* ***************************************************************************** */


$test_step = '06c';

$transition_name = "Fund Port-In Request";

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 07c - Change state to Port-In Requested (for real, faking actions) *** */
/* **************************************************************************** */


$test_step = '07c';

$result_decoded = test_take_transition_curl($test_step,$customer_id,TRUE,$transition_name,FALSE,'FAKE_ACTIONS',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 08c - Check customer state Port-In Requested *** */
/* ****************************************************** */


$test_step = '08c';

$result_decoded = test_check_state_curl($test_step,$customer_id);

test_check_state_port_in_requested($test_step,$result_decoded,$test_plan);

echo "\n\n - $test_step - OK\n\n";



exit;




}


/* *** - 07 - Change state to Active (dry run) *** */
/* *********************************************** */


$test_step = '07';

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 08 - Change state to Active (for real, faking actions) *** */
/* **************************************************************** */


$test_step = '08';

$result_decoded = test_take_transition_curl($test_step,$customer_id,TRUE,$transition_name,FALSE,'FAKE_ACTIONS',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 09 - Check customer state Active *** */
/* ****************************************** */


$test_step = '09';

$result_decoded = test_check_state_curl($test_step,$customer_id);

test_check_state_active($test_step,$result_decoded,$test_plan);

echo "\n\n - $test_step - OK\n\n";


/* *** - 10 - Change state to Cancelled (dry run) *** */
/* ************************************************** */


$test_step = '10';

$transition_name = 'Cancel Active';

$result_decoded = test_take_transition_curl($test_step,$customer_id,FALSE,$transition_name,TRUE,'',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 11 - Change state to Cancelled (for real, faking actions) *** */
/* ******************************************************************* */


$test_step = '11';

$result_decoded = test_take_transition_curl($test_step,$customer_id,TRUE,$transition_name,FALSE,'FAKE_ACTIONS',TRUE);

echo "\n\n - $test_step - OK\n\n";


/* *** - 12 - Check customer state Cancelled *** */
/* ********************************************* */


$test_step = '12';

$result_decoded = test_check_state_curl($test_step,$customer_id);

test_check_state_cancelled($test_step,$result_decoded,$test_plan);

echo "\n\n - $test_step - OK\n\n";






exit;


function test_take_transition_curl($test_step,$customer_id,$resolve_now,$transition_name,$dry_run,$debug,$expect_success)
{
  global $timeout;
  global $curl_options;
  global $domain;

  $url = 'https://'.$domain.'/pr/internal/1/ultra/api/internal__TakeTransition';

  $params = array(
    'debug'           => $debug,
    'customer_id'     => $customer_id,
    'resolve_now'     => $resolve_now,
    'dry_run'         => $dry_run,
    'transition_name' => $transition_name
  );

  print_r($params);

  $result = curl_post($url,$params,$curl_options,NULL,$timeout);

  if ( ! $result ) { die("Test failed at step $test_step (a)\n"); }

  $result_decoded = json_decode($result);

  print_r($result_decoded);

  if ( $expect_success )
  {
    if ( ! $result_decoded->success ) { die("Test failed at step $test_step (b)\n"); }

    if ( count($result_decoded->errors) ) { die("Test failed at step $test_step (c)\n"); }
  }
  else
  {
    if ( $result_decoded->success ) { die("Test failed at step $test_step (b)\n"); }

    if ( ! count($result_decoded->errors) ) { die("Test failed at step $test_step (c)\n"); }
  }

  return $result_decoded;
}

?>
