<?php


# perform actual state transitions of test customer using internal__ChangeState


include_once('db.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('lib/payments/functions.php');
include_once('test/state_machine/test_util.php');
include_once('web.php');


date_default_timezone_set("America/Los_Angeles");


teldata_change_db(); // connect to the DB


$domain  = 'rgalli2-dev.uvnv.com';
$timeout = 10;
$apache_username = 'dougmeli';
$apache_password = 'Flora';
$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


/* *** - 01 - Create a new customer *** */
/* ************************************ */


$customer = test_create_web_user();
$customer_id = $customer->CUSTOMER_ID;

echo "\n\n - 01 - OK\n\n";


/* *** - 02 - Check customer state (STANDBY/Neutral) *** */
/* ***************************************************** */


$result_decoded = test_check_state_curl('02',$customer_id);

test_check_state_neutral('02',$result_decoded);

echo "\n\n - 02 - OK\n\n";


/* *** - 03 - Change state to Provisioned (dry run) *** */
/* **************************************************** */


$result_decoded = test_change_state_curl('03',$customer_id,FALSE,'L29','Provisioned',TRUE,1,'',TRUE);
echo "\n\n - 03 - OK\n\n";


/* *** - 04 - Change state to Provisioned (for real) *** */
/* ***************************************************** */


$result_decoded = test_change_state_curl('04',$customer_id,TRUE,'L29','Provisioned',FALSE,1,'FAKE_ACTIONS',TRUE);
echo "\n\n - 04 - OK\n\n";


/* *** - 05 - Check customer state (Provisioned) *** */
/* ************************************************* */


$result_decoded = test_check_state_curl('05',$customer_id);

test_check_state_provisioned('05',$result_decoded);

echo "\n\n - 05 - OK\n\n";


/* *** - 06 - Change state to Active (dry run) expecting failure due to low funds *** */
/* ********************************************************************************** */


$result_decoded = test_change_state_curl('06',$customer_id,FALSE,'L29','Active',TRUE,1,'',FALSE);
echo "\n\n - 06 - OK\n\n";


/* *** - 07 - Change state to Active (dry run) expecting success after adding funds *** */
/* ************************************************************************************ */


// give some funds
$add_balance_result = func_add_balance(
  array(
    'customer'    => $customer,
    'amount'      => 29, # in $
    'reason'      => 'test reason',
    'reference'   => 'test reference',
    'source'      => 'test source'
  )
);

if ( count($add_balance_result['errors']) ) { die("Test failed at func_add_balance - step 07\n"); }

$result_decoded = test_change_state_curl('07',$customer_id,FALSE,'L29','Active',TRUE,1,'',TRUE);

echo "\n\n - 07 - OK\n\n";


/* *** - 08 - Change state to Active (for real) expecting success after adding funds *** */
/* ************************************************************************************* */


$result_decoded = test_change_state_curl('08',$customer_id,TRUE,'L29','Active',FALSE,1,'FAKE_ACTIONS',TRUE);
echo "\n\n - 08 - OK\n\n";


/* *** - 09 - Check customer state (Active) *** */
/* ******************************************** */


$result_decoded = test_check_state_curl('09',$customer_id);

test_check_state_active('09',$result_decoded);

echo "\n\n - 09 - OK\n\n";


/* *** - 10 - Change state to Suspended (dry run) *** */
/* ************************************************** */


$result_decoded = test_change_state_curl('10',$customer_id,FALSE,'L29','Suspended',TRUE,1,'',TRUE);

echo "\n\n - 10 - OK\n\n";

# denied by pre-requisites

# ERR_API_INTERNAL: Could not find a path to L29/Suspended for customer ;
# transitions blocked by prerequisites:
# Suspend from Ultra $29/Active to Ultra $29/Suspended;
# Debugging transition from Ultra $29/Active to Ultra $29/Active;
# Debugging transition from Ultra $49/Active to Ultra $49/Active;
# Monthly Renewal from Ultra $49/Active to Ultra $49/Active;
# Debugging transition from Ultra $39/Active to Ultra $39/Active;
# Monthly Renewal from Ultra $39/Active to Ultra $39/Active


exit;





/* *** - 09 - Change state to Suspended (dry run) *** */
/* ************************************************** */


$result_decoded = test_change_state_curl('09',$customer_id,FALSE,'L29','Suspended',TRUE,1,'');

echo "\n\n - 09 - OK\n\n";


/* *** - 10 - Change state to Suspended (for real) *** */
/* *************************************************** */


$result_decoded = test_change_state_curl('07',$customer_id,TRUE,'L29','Suspended',FALSE,1,'FAKE_ACTIONS');

echo "\n\n - 10 - OK\n\n";


/* *** - 11 - Check customer state (Suspended) *** */
/* *********************************************** */


$result_decoded = test_check_state_curl('11',$customer_id);

test_check_state_suspended('11',$result_decoded);

echo "\n\n - 11 - OK\n\n";


/* *** - 12 - Change state from Suspended to Active (dry run) *** */
/* ************************************************************** */


$result_decoded = test_change_state_curl('12',$customer_id,FALSE,'L29','Active',TRUE,1,'transitions');

echo "\n\n - 12 - OK\n\n";
exit;


/* *** - 13 - Change state to Active (for real) *** */
/* ***************************************************** */


$result_decoded = test_change_state_curl('13',$customer_id,TRUE,'L29','Active',FALSE,1,'FAKE_ACTIONS');

echo "\n\n - 13 - OK\n\n";


/* *** - 14 - Check customer state (Active) *** */
/* ******************************************** */





/* ~~~ functions ~~~ */


function test_change_state_curl($test_step,$customer_id,$resolve_now,$plan,$state_name,$dry_run,$max_path_depth,$debug,$expect_success)
{
  global $timeout;
  global $curl_options;
  global $domain;

  $url = 'https://'.$domain.'/pr/internal/1/ultra/api/internal__ChangeState';

  $params = array(
    'debug'          => $debug,
    'customer_id'    => $customer_id,
    'resolve_now'    => $resolve_now,
    'plan'           => $plan,
    'state_name'     => $state_name,
    'dry_run'        => $dry_run,
    'max_path_depth' => $max_path_depth
  );

  echo "\n\n$url\n\n";

  print_r($params);

  $result = curl_post($url,$params,$curl_options,NULL,$timeout);

  if ( ! $result ) { die("Test failed at step $test_step (a)\n"); }

  $result_decoded = json_decode($result);

  print_r($result_decoded);

  if ( $expect_success )
  {
    if ( ! $result_decoded->success ) { die("Test failed at step $test_step (b)\n"); }

    if ( count($result_decoded->errors) ) { die("Test failed at step $test_step (c)\n"); }
  }
  else
  {
    if ( $result_decoded->success ) { die("Test failed at step $test_step (b)\n"); }

    if ( ! count($result_decoded->errors) ) { die("Test failed at step $test_step (c)\n"); }
  }

  return $result_decoded;
}

?>
