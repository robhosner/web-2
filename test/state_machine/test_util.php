<?php


include_once('cosid_constants.php');


function test_create_web_user()
{
  global $timeout;
  global $curl_options;
  global $domain;

  $url = 'https://'.$domain.'/pr/portal/1/ultra/api/portal__CreateWebUser';

  $params = array(
    'account_login'        => time(),
    'account_first_name'   => time() - 2,
    'account_last_name'    => time() - 1,
    'account_password'     => time(),
    'account_number_email' => time()."test@gmail.com",
    'account_country'      => 'US'
  );

  $result = curl_post($url,$params,$curl_options,NULL,$timeout);

  if ( ! $result ) { die("Test failed at step 01 (a)\n"); }

  $result_decoded = json_decode($result);

  print_r($result_decoded);

  if ( ! $result_decoded->success ) { die("Test failed at step 01 (b)\n"); }

  if ( ! $result_decoded->customer_id ) { die("Test failed at step 01 (c)\n"); }

  if ( count($result_decoded->errors) ) { die("Test failed at step 01 (d)\n"); }

  $customer_id = $result_decoded->customer_id;

  $customer = get_customer_from_customer_id($customer_id);

  if ( ! $customer ) { die("Test failed at step 01 (e)\n"); }

  return $customer;
}

function test_check_state_neutral($test_step,$result_decoded)
{
  if ( $result_decoded->plan_name != 'STANDBY' ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != "Ultra SIM Standby" ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Neutral" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != 'Ultra SIM Standby/Neutral' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_provisioned($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name != $plan_name ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name') ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Provisioned" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name').'/Provisioned' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_active($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name != $plan_name ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name') ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Active" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name').'/Active' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_suspended($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name != $plan_name ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name') ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Suspended" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name').'/Suspended' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_pre_funded($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name != $plan_name ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name') ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Pre-Funded" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name').'/Pre-Funded' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_cancelled($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name != 'STANDBY' ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != 'Ultra SIM Standby' ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != 'Cancelled' ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != 'Ultra SIM Standby/Cancelled' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_port_in_unfunded($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name !=  $plan_name ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name') ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Port-In Unfunded" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name').'/Port-In Unfunded' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_port_in_requested($test_step,$result_decoded,$plan_name)
{
  if ( $result_decoded->plan_name !=  $plan_name ) { die("Test failed at step $test_step (d)\n"); }

  if ( $result_decoded->plan_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name') ) { die("Test failed at step $test_step (e)\n"); }

  if ( $result_decoded->state_name != "Port-In Requested" ) { die("Test failed at step $test_step (f)\n"); }

  if ( $result_decoded->state_desc_name != find_credential('plans/Ultra/'.$plan_name.'/name').'/Port-In Requested' ) { die("Test failed at step $test_step (g)\n"); }
}

function test_check_state_curl($test_step,$customer_id)
{
  global $timeout;
  global $curl_options;
  global $domain;

  $url = 'https://'.$domain.'/pr/internal/1/ultra/api/internal__GetState.json';

  $params = array(
    'customer_id' => $customer_id
  );

  $result = curl_post($url,$params,$curl_options,NULL,$timeout);

  if ( ! $result ) { die("Test failed at step $test_step (a)\n"); }

  $result_decoded = json_decode($result);

  print_r($result_decoded);

  if ( ! $result_decoded->success ) { die("Test failed at step $test_step (b)\n"); }

  if ( count($result_decoded->errors) ) { die("Test failed at step $test_step (c)\n"); }

  return $result_decoded;
}

function test_resolve_pending_transitions($curl_options)
{
  # try to resolve pending transitions
  return curl_post(find_site_url() . '/pr/internal/1/ultra/api/internal__ResolvePendingTransitions.json',array(),$curl_options,NULL,240);
}

?>
