<?php

# Usage:
# %> php test/state_machine/test_aspider.php callback_ChangeMSISDN         $CUSTOMER_ID $MSISDN
# %> php test/state_machine/test_aspider.php reserve_iccid $ICCID
# %> php test/state_machine/test_aspider.php unreserve_iccid $ICCID
# %> php test/state_machine/test_aspider.php sim_activation_handler $CUSTOMER_ID $TRANSITION_UUID $COS_ID
# %> php test/state_machine/test_aspider.php msisdn_activation_handler $CUSTOMER_ID
# %> php test/state_machine/test_aspider.php make_action $TYPE						OK
# %> php test/state_machine/test_aspider.php append_action_funcall $CUSTOMER_ID $ACTION_TYPE [ $args ]  OK
# %> php test/state_machine/test_aspider.php append_action $CUSTOMER_ID $ACTION_TYPE $ACTION_NAME	
# %> php test/state_machine/test_aspider.php append_action_gw_aspiderServiceEnsure			OK
# %> php test/state_machine/test_aspider.php append_action_make_action $TYPE $CUSTOMER_ID		OK
# %> php test/state_machine/test_aspider.php example_func_action $CUSTOMER_ID
# %> php test/state_machine/test_aspider.php data_recharge_actions $CUSTOMER_ID
# %> php test/state_machine/test_aspider.php create_transition_action_sql $CUSTOMER_ID
# %> php test/state_machine/test_aspider.php callbackChangeSIM    $ICCID  $CUSTOMER_ID


include_once('db.php');
include_once('lib/state_machine/aspider.php');
require_once 'lib/inventory/functions.php';


date_default_timezone_set("America/Los_Angeles");


teldata_change_db(); // connect to the DB


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_sim_activation_handler extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id     = $argv[2];
    $transition_uuid = empty($argv[3]) ? NULL : $argv[3];
    $cos_id          = empty($argv[4]) ? NULL : $argv[4];

    $r = sim_activation_handler($customer_id,$transition_uuid, $cos_id);
    print_r($r);
  }
}


class Test_callbackChangeSIM extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $iccid          = $argv[2];
    $customer_id    = $argv[3];

    $r = callbackChangeSIM($iccid,$customer_id);
    print_r($r);
  }
}


class Test_msisdn_activation_handler extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $r = msisdn_activation_handler($customer_id);
    print_r($r);
  }
}


class Test_make_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $action_made = make_action( $argv[2] );

    print_r($action_made);

    echo "\n";
  }
}


class Test_create_transition_action_sql extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $context = array(
      'customer_id' => $customer_id
    );

    $transition_uuid = getNewTransitionUUID('test ' . time());
    $current_cos_id  = 1;
    $current_state   = 'Active';
    $target_cos_id   = 1;
    $target_state    = 'Active';
    $label           = 'Test_create_transition_action_sql';
    $priority        = 1;

    $check = log_transition($transition_uuid,
                            $context,
                            $current_cos_id,
                            $current_state,
                            $target_cos_id,
                            $target_state,
                            $label,
                            $priority
    );

    if ( !$check )
    {
      echo "log_transition failed!\n"; exit;
    }

/*
02/05/14 13:03:53,209- [11363] change_state(): invoking stupid log_transition with transition_uuid = {TX-41E5620A3B2C81DF-A9B1926919F6A864} , context = {"customer_id":2417} , cos_id = 98274 , state = Active , targetcos_id = 98274 , targetstate = Suspended , label = Suspend , priority = 5
*/

/*
02/05/14 12:45:33,606- [8513] log_actions(): invoking stupid log_action with type = sql , name = UPDATE ACCOUNTS SET packaged_balance1 = 0 WHERE CUSTOMER_ID = %d, transition_uuid = {TX-A4F7E3683325DA62-92111C6DE9360D33} , action_uuid = {AX-921FE27E447B4026-B04D7276CC61ABEB} , aparams = {"seq":103,"type":"sql","name":"UPDATE ACCOUNTS SET packaged_balance1 = 0 WHERE CUSTOMER_ID = %d"} , type = ["__customer_id__"]
*/

    $action_uuid = getNewActionUUID('test ' . time());
    $params      = array( $customer_id );
    $aparams     = array(
      'seq'  => 1,
      'type' => 'sql',
      'name' => 'UPDATE ACCOUNTS SET packaged_balance1 = 0 WHERE CUSTOMER_ID = %d'
    );

    $check = log_action($transition_uuid,
                        $action_uuid,
                        $aparams,
                        $params,
                        'OPEN',
                        $parent_action_id,
                        NULL);

    if ( !$check ) 
    {
      echo "log_action failed!\n"; exit;
    }

    $r = reset_redis_transition( $transition_uuid );

    print_r( $r );
  }
}


/*
class Test_append_action_sql extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $context = array(
      'customer_id' => $customer_id
    );

    $action_seq  = 0;
    $action_type = 'sql';
    $action_name = 'sql';
    $params      = array( $customer_id );
    $action_transaction = NULL;

    $result_status = append_action(
      $context,
      array('type'        => $action_type,
            'name'        => $action_name,
            'sql'         => 'UPDATE ACCOUNTS SET ACTIVATION_DATE_TIME = GETUTCDATE() WHERE customer_id = %d',
            'transaction' => $action_transaction,
            'seq'         => $action_seq
      ),
      $params,
      FALSE
    );

    print_r($result_status);
  }
}
*/


class Test_data_recharge_actions extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];
    $amount      = $argv[3];

    $action_seq  = 0;

    $context = array(
      'customer_id' => $customer_id
    );

    $action_type = 'funcall';
    $action_name = 'func_assert_balance';
    $params      = array( $customer_id , $amount );
    $action_transaction = NULL;

    $result_status = append_action(
      $context,
      array('type'        => $action_type,
            'name'        => $action_name,
            'transaction' => $action_transaction,
            'seq'         => $action_seq
      ),
      $params,
      FALSE
    );

    print_r($result_status);

    if ( ! $result_status['success'] ) { die("FAILURE\n"); }

    $context['transition_id'] = $result_status['transitions'][0]; # enqueue to the transition created previously

    $action_seq++;

      // Track Data Socs to htt_customers_overlay_ultra
      $result_status = append_make_funcall_action(
        'record_customer_soc',
        $context,
        $action_transaction,
        $action_seq,
        'new_data_soc'
      );

    print_r($result_status);

/*
Array
(
    [success] => 1
    [aborted] => 
    [errors] => Array () 
    [log] => Array () 
    [transitions] => Array
        ( [0] => {DDF659AF-1FB6-75CF-BD45-685E3912D045})
[actions] => Array
        ( [0] => {E69A2B8C-21B5-3258-7836-07034C5BD6F9})
)
*/
  }
}


class Test_example_func_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $context = array(
      'customer_id' => $customer_id
    );

    $action_seq  = 0;
    $action_type = 'funcall';
    $action_name = 'example_func_action';
    $params      = array( 1 );
    $action_transaction = NULL;

    $result_status = append_action(
      $context,
      array('type'        => $action_type,
            'name'        => $action_name,
            'transaction' => $action_transaction,
            'seq'         => $action_seq),
            $params,
            FALSE
    );

    print_r($result_status);
  }
}


class Test_append_action_make_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $action_made = make_action( $argv[2] );

    $customer_id = $argv[3];

    print_r($action_made);

    echo "\n";

    $action_type = 'UNDEFINED';
    $params      = array();
    $transaction = NULL;

    if ( isset( $action_made['funcall'] ) )
    {
      $action_type = "funcall";
      $action_name = $action_made['funcall'];
      $params      = $action_made['fparams'];
    }

    if ( isset( $action_made['transaction'] ) )
    {
      $transaction = $action_made['transaction'];
    }

    $aparams = array(
      'type'        => $action_type,
      'name'        => $action_name,
      'transaction' => $transaction,
      'seq'         => 1
    );

    $context = array(
      'customer_id' => $customer_id
    );

    $pending = FALSE;

    $result_status = append_action( $context , $aparams , $params , $pending );

    print_r($result_status);

    echo "\n";
  }
}

class Test_append_action_funcall extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $arguments = $argv;

    $caller      = array_shift($arguments);
    $caller      = array_shift($arguments);
    $customer_id = array_shift($arguments);
    $action_type = 'funcall';
    $action_name = array_shift($arguments);

    $context = array(
      'customer_id' => $customer_id
    );

    $aparams = array(
      'type'        => $action_type,
      'name'        => $action_name,
      'transaction' => NULL,
      'seq'         => 1
    );

    $pending = FALSE;

    $result_status = append_action( $context , $aparams , $arguments , $pending );

    print_r($result_status);

    echo "\n";
  }
}


class Test_append_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];
    $action_type = $argv[3];
    $action_name = $argv[4];

    $context = array(
      'customer_id' => $customer_id
    );

    $aparams = array(
      'type'        => $action_type,
      'name'        => $action_name,
      'transaction' => NULL,
      'seq'         => 1
    );

    $params  = array(
    );

    $pending = FALSE;

    $result_status = append_action( $context , $aparams , $params , $pending );

    print_r($result_status);

    echo "\n";
  }
}


class Test_unreserve_iccid extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $ICCID = $argv[2];

    $r = unreserve_iccid($ICCID);
    print_r($r);
  }
}


class Test_reserve_iccid extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $ICCID = $argv[2];

    $r = reserve_iccid($ICCID);
    print_r($r);
  }
}

function example_func_action( $example_func_action_param )
{
  dlog('',"example_func_action_param = $example_func_action_param");

  return TRUE;
}

class test_action_data
{
  public $TRANSITION_UUID = '{E1206C97-0066-38C6-AC1B-3245E630D1D3}';
}


# perform test(s) #


$class_ext = $argv[1];
if ( ! $class_ext ) { $class_ext = $_GET['class']; }

$testClass = 'Test_'.$class_ext;

echo "\n$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


/*
Examples:

php test/state_machine/test_aspider.php append_action_funcall 31 sleep 20
*/

?>
