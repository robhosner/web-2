<?php

// this could be improved
set_include_path(get_include_path() . PATH_SEPARATOR . '/opt/php/vendor/giggsey/');

require_once 'libphonenumber/MetadataLoaderInterface.php';
require_once 'libphonenumber/AlternateFormatsCountryCodeSet.php';
require_once 'libphonenumber/CountryCodeSource.php';
require_once 'libphonenumber/CountryCodeToRegionCodeMapForTesting.php';
require_once 'libphonenumber/CountryCodeToRegionCodeMap.php';
require_once 'libphonenumber/DefaultMetadataLoader.php';
require_once 'libphonenumber/MatcherAPIInterface.php';
require_once 'libphonenumber/Matcher.php';
require_once 'libphonenumber/MatchType.php';
require_once 'libphonenumber/NumberFormat.php';
require_once 'libphonenumber/NumberParseException.php';
require_once 'libphonenumber/PhoneMetadata.php';
require_once 'libphonenumber/PhoneNumberDesc.php';
require_once 'libphonenumber/PhoneNumberFormat.php';
require_once 'libphonenumber/PhoneNumber.php';
require_once 'libphonenumber/PhoneNumberToCarrierMapper.php';
require_once 'libphonenumber/PhoneNumberToTimeZonesMapper.php';
require_once 'libphonenumber/PhoneNumberType.php';
require_once 'libphonenumber/PhoneNumberUtil.php';
require_once 'libphonenumber/RegexBasedMatcher.php';
require_once 'libphonenumber/RegionCode.php';
require_once 'libphonenumber/ShortNumberCost.php';
require_once 'libphonenumber/ShortNumberInfo.php';
require_once 'libphonenumber/ShortNumbersRegionCodeSet.php';
require_once 'libphonenumber/ShortNumberUtil.php';
require_once 'libphonenumber/ValidationResult.php';

$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

$swissNumberProto = $phoneUtil->parse("044 668 18 00", "CH");
$usNumberProto    = $phoneUtil->parse("+1 650 253 0000", "US");
$usNumberProto2   = $phoneUtil->parse("650 253 0000", "US");
$gbNumberProto    = $phoneUtil->parse("0161 496 0000", "GB");

echo "$swissNumberProto\n";
echo "$usNumberProto\n";
echo "$usNumberProto2\n";
echo "$gbNumberProto\n\n";

echo $phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::E164) . PHP_EOL;
echo $phoneUtil->format($usNumberProto   , \libphonenumber\PhoneNumberFormat::E164) . PHP_EOL;
echo $phoneUtil->format($usNumberProto2  , \libphonenumber\PhoneNumberFormat::E164) . PHP_EOL;
echo $phoneUtil->format($gbNumberProto   , \libphonenumber\PhoneNumberFormat::E164) . PHP_EOL;

if ( $phoneUtil->isPossibleNumber( $phoneUtil->parse('044 668 18 00 11','US') ) )
{ echo "Y\n"; } else { echo "N\n"; }

