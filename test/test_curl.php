<?php


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once($e_config['git/deploy'].'/web.php');
include_once('cosid_constants.php');
include_once('test/api/test_api_include.php');


$sites = explode(' ', $e_config['www/sites']);
$base_url = 'https://'.$sites[0].'/';

$apache_username = 'dougmeli';
$apache_password = 'Flora';

$login_result = array();

if (isset($argv[2])) { $transition_source = $argv[2]; $phone_number = $argv[2]; }
if (isset($argv[3])) { $transition_dest   = $argv[3]; }

$customer = getenv('TEST_CUSTOMER') ? getenv('TEST_CUSTOMER') : 'seanb1';
$pp = getenv('TEST_PP') ? getenv('TEST_PP') : 2222;

# USAGE:
# php test/test_curl.php study_chargeup
# php test/test_curl.php chargeup
# php test/test_curl.php customer_status
# php test/test_curl.php study_plans
# php test/test_curl.php sms <NUMBER>
# php test/test_curl.php invalid_mode
# php test/test_curl.php transition 29133 30456

// get the mFunds status of a customer
// HTT_CONFIGROOT=/home/ht/config/tzz_dev TEST_CUSTOMER=t800 TEST_PP=2222 php test/test_curl.php mfunds_status

// do the signup child
// HTT_CONFIGROOT=/home/ht/config/tzz_dev TEST_CUSTOMER=t800 TEST_PP=2222 php test/test_curl.php mfunds_child_signup
// on an existing child, do prep+do
// HTT_CONFIGROOT=/home/ht/config/tzz_dev TEST_CUSTOMER=t800 TEST_PP=2222 php test/test_curl.php mfunds_child_do

// do a signup attempt with invalid fields
// HTT_CONFIGROOT=/home/ht/config/tzz_dev php test/test_curl.php bad_signup


abstract class AbstractTestStrategy
{
  abstract function test($base_url,$login_result);
}


class Test_login extends AbstractTestStrategy
{
  function test($base_url,$dummy)
  {
    global $apache_username;
    global $apache_password;
    global $login_result;
    global $customer;
    global $pp;

    $test_login_params = array(
      'json'     => '1',
      'customer' => $customer,
      'pp'       => $pp,
    );

    $url = $base_url.'api_login.php';

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $login_result = default_json_test($url,$test_login_params,$curl_options);

    $validation_errors = validate_login_result();

    if ( count($validation_errors) )
    {
      echo "** LOGIN FAILED **\n";
      print_r($validation_errors);
      exit_with_status( count($validation_errors) );
    }
  }
}


function validate_login_result()
{
  global $login_result;

  $validation_errors = array();

  if ( ! $login_result )
  {
    $validation_errors[] = "No output from login";
  }

  if (
    ( isset( $login_result["errors"] ) )
    &&
    ( count($login_result["errors"]) )
  )
  {
    $validation_errors = array_merge( $validation_errors,$login_result["errors"] );
  }

  if ( isset( $login_result["zsession"] ) )
  {
    $invalid_zsession = 0;

    $ids = array(0,1,2);

    foreach ($ids as &$id)
    {
      $invalid_zsession = $invalid_zsession || ( ! $login_result["zsession"][$id] );  
    }

    if ( $invalid_zsession )
    {
      $validation_errors[] = "Invalid zsession from login output";
    }
  }
  else
  {
    $validation_errors[] = "No zsession found in login output";
  }

  return $validation_errors;
}


class Test_bad_signup extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $random = time() % 10000;

    $params = array(
      'mode'             => 'signup',
      'fersure' => 1,
      'password' => "fa36d91f7ca4997f9e95cdc9430e157d48738a6a",
      'fields' => array('account_first_name' => '',
                        'account_last_name' => '',
                        'account_number_email' => "",
                        'account_number_phone' => '',
                        'account_country' => '',
                        'account_login' => "temp$random",
                        'account_password' => "ptemp$random",
                          )
    );

    $signup_result = default_json_test($url,$params,$curl_options);

    if (!$signup_result['signup_error']['failed_validation'])
    {
      print_r("Bad signup validation failed, test FAIL!");
      exit;
    }

    $customer_id = $signup_result['customer_id'];

    if ($customer_id)
    {
      print_r("Bad signup succeeded, test FAIL!");
      exit;
    }

    print_r("Did not sign up bad customer, test OK\n");

  }
}

class Test_mfunds_status extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $params = array(
      'mode'             => 'customer_status',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
    );

    $result = default_json_test($url,$params,$curl_options);

    $crecord = $result['customer_record'];

    $params = array(
      'mode'             => 'mfunds_status',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
    );

    $result = default_json_test($url,$params,$curl_options);

    $record = isset($result['customer_record_mfunds']) ? $result['customer_record_mfunds'] : $result['customer_record_mfunds_child'];

    print_r("Got ID $record[mfunds_person_id] and customer ID $record[customer_id]\n");
  }
}

class Test_mfunds_child_signup extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $params = array(
      'mode'             => 'mfunds_status',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
    );

    $parent_result = default_json_test($url,$params,$curl_options);

    $parent = $parent_result['customer_record_mfunds'];

    print_r("Starting with mparent ID $parent[mfunds_person_id] and parent customer $parent[customer_id]\n");

    $random = time() % 10000;

    $params = array(
      'mode'             => 'signup',
      'fersure' => 1,
      'password' => "fa36d91f7ca4997f9e95cdc9430e157d48738a6a",
      'fields' => array('account_first_name' => 'Joe',
                        'account_last_name' => 'Child',
                        'account_number_email' => "$random@ultra.me",
                        'account_number_phone' => '5555555555',
                        'account_country' => 'US',
                        'account_login' => "temp$random",
                        'account_password' => "ptemp$random",
                          )
    );

    $signup_result = default_json_test($url,$params,$curl_options);

    $child_customer_id = $signup_result['customer_id'];

    if (!$child_customer_id)
    {
      print_r("Signup failed!");
      exit;
    }

    print_r("Signed up customer $child_customer_id\n");

    $params = array(
      'mode'             => 'mfunds_order_card_prep',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
      'customer_id'      => $child_customer_id,
      'parent_id'        => $parent['mfunds_person_id'],
    );

    $prep_result = default_json_test($url,$params,$curl_options);

    print_r("Prepared mFunds child record\n");

    $params = array(
      'mode'             => 'fields',
      'mfunds_child_null'=> 1,
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
      'fersure' => 1,
      'fields' => array('mfunds_date_of_birth' => '01/01/1993',
                        'mfunds_social_security' => '343343343',
                        'mfunds_first_name' => 'Joe',
                        'mfunds_last_name' => 'Child',
                        'mfunds_address1' => '88 Wall Street',
                        'mfunds_city' => 'New York',
                        'mfunds_state_or_region' => 'NY',
                        'mfunds_email' => "$random@ultra.me",
                        'mfunds_home_phone' => '5555555555',
                        'mfunds_country' => 'US',
                        'mfunds_authentication' => 'ssn'
                          )
    );

    $signup_result = default_json_test($url,$params,$curl_options);

  }
}

class Test_mfunds_child_do extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
    CURLOPT_TIMEOUT => 60, // in seconds
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $params = array(
      'mode'             => 'mfunds_status',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
    );

    $parent_result = default_json_test($url,$params,$curl_options);

    $parent = $parent_result['customer_record_mfunds'];

    print_r("Starting with mparent ID $parent[mfunds_person_id] and parent customer $parent[customer_id]\n");

    $params = array(
      'mode'             => 'mfunds_order_card_do',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
      'parent_id'        => $parent['mfunds_person_id'],
    );

    $do_result = default_json_test($url,$params,$curl_options);

    print_r("Applied for mFunds child card\n");
  }
}

class Test_transition extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;
    global $transition_source;
    global $transition_dest;

    if (
      ( ! isset($transition_source) )
      ||
      ( ! isset($transition_dest) )
    )
    {
      echo "This test needs <source> and <destination>\n";
      exit_with_status(1);
    }

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_switch_params = array(
      'fersure'            => 1,
      'mode'               => 'transition',
      'dest'               => $transition_dest,
      'source'             => $transition_source,
      'charge_description' => 'Switch Test',
      'bifurcate'          => 'Immediate',
      'confirm_loss'       => 1,
      'suspend'            => 0,
      'customer'           => $login_result['zsession'][0],
      'zsession'           => $login_result['zsession'][2],
    );

    default_json_test($url,$test_switch_params,$curl_options);
  }
}


class Test_chargeup extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_chargeup_params = array(
      'mode'             => 'chargeup',
      'json'             => 1,
      'fersure'          => 1,
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
      'chargeup'         => '10.00',
    );

    default_json_test($url,$test_chargeup_params,$curl_options);
  }
}

class Test_study_chargeup extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_study_chargeup_params = array(
      'mode'             => 'study_chargeup',
      'json'             => 1,
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2]
    );

    default_json_test($url,$test_study_chargeup_params,$curl_options);
  }
}


class Test_sms extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;
    global $phone_number;

    if ( ! isset($phone_number) )
    {
      echo "This test needs <phone_number>\n";
      exit_with_status(1);
    }

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_sms_params = array(
      'mode'             => 'sms',
      'json'             => 1,
      'sms_text'         => 'sms text test',
      'sms_dest'         => $phone_number,
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2]
    );

    default_json_test($url,$test_sms_params,$curl_options);
  }
}


class Test_study_plans extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_study_plans_params = array(
      'mode'             => 'study_plans',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
      'json'             => 1
    );

    default_json_test($url,$test_study_plans_params,$curl_options);
  }
}


class Test_invalid_mode extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_params = array(
      'mode'             => '',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
      'json'             => 1
    );

    default_json_test($url,$test_params,$curl_options);
  }
}


class Test_customer_status extends AbstractTestStrategy
{
  function test($base_url,$login_result)
  {
    $url = $base_url.'api.php';

    global $apache_username;
    global $apache_password;

    $curl_options = array(
      CURLOPT_USERPWD  => "$apache_username:$apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $test_customer_status_params = array(
      'mode'             => 'customer_status',
      'customer'         => $login_result['zsession'][0],
      'zsession'         => $login_result['zsession'][2],
    );

    $result = default_json_test($url,$test_customer_status_params,$curl_options);

    validate_customer_status_result($result);
  }
}

function validate_customer_status_result($result)
{
  $validation_errors = array();

  if (
    ( isset( $result["errors"] ) )
    &&
    ( count($result["errors"]) )
  )
  {
    $validation_errors = array_merge( $validation_errors,$result["errors"] );
  }

  $expected_fiels = array(
    "customer_record",
    "request_id",
    "zsession",
    "customer",
    "targets",
    "tax_rate",
    "all_targets"
  );

  foreach ($expected_fiels as $id => $field)
  {
    if ( ! ( isset( $result[$field] ) ) )
    {
      $validation_errors[] = "Missing customer record in $field output";
    }
  }

  if ( count($validation_errors) )
  {
    echo "** CUSTOMER STATUS API CALL FAILED **\n";
    print_r($validation_errors);
    exit_with_status( count($validation_errors) );
  }
}


function exit_with_status($exit_status = 0)
{
  exit($exit_status);
}


# perform login #


$testClass = 'Test_login';

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test($base_url,$login_result);


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test($base_url,$login_result);


# success #


exit_with_status();


?>
