<?php

// DB connection tests

include_once('db.php');

$connection = \Ultra\Lib\DB\ultra_acc_connect();

if ( ! $connection)
  die('Cannot connect to ultra_acc db');

$params['data_xml'] = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://www.sigvalue.com/acc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><ns1:SendSMS><ns1:UserData><ns1:senderId>MVNEACC</ns1:senderId><ns1:channelId>ULTRA</ns1:channelId><ns1:timeStamp>2015-03-17T10:55:10</ns1:timeStamp></ns1:UserData><ns1:qualityOfService>801</ns1:qualityOfService><ns1:preferredLanguage>en</ns1:preferredLanguage><ns1:smsText>Test '.time().' &amp&#59; </ns1:smsText><ns1:SubscriberSMSList><ns1:SubscriberSMS><ns1:MSISDN>8323985363</ns1:MSISDN><ns1:Language>en</ns1:Language><ns1:Text xsi:nil="true"/></ns1:SubscriberSMS></ns1:SubscriberSMSList></ns1:SendSMS></SOAP-ENV:Body></SOAP-ENV:Envelope>';
$params['soap_date'] = '2000-05-11 15:41:55.223';
$params['type_id'] = 1;
$params['msisdn'] = time();
$params['iccid'] = NULL;
$params['tag'] = '{TEST-'.time().'}';
$params['session_id'] = '';
$params['command'] = 'SendSMS';
$params['env'] = 'test';

$query = soap_log_insert_query($params).
         ';SELECT SCOPE_IDENTITY() LAST_SOAP_LOG_ID;';

$result = batch_mssql_query($query);

print_r($result);

echo "\n";

