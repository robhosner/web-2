<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis.php';

$redis = new \Ultra\Lib\Util\Redis();

if ( empty( $argv[1] ) )
  exit;

$messageQueue = $argv[1];

$members = $redis->smembers( $messageQueue );

dlog('',"members = %s",$members);

$age_of_oldest = 0;

foreach ( $members as $channel )
{
  dlog('',"channel = $channel");

  $node = $redis->get( 'CM_'.$channel );

  if ( $node )
  {
    dlog('',"node = $node");

    $data = $redis->get( $node );

    if ( $data )
    {
      dlog('',"data = $data");

      $json_decoded = json_decode($data);

      if ( $json_decoded )
      {
        print_r( $json_decoded );

        $age = ( time() - strtotime( $json_decoded->_createdDateTime ) );

        echo "createdDateTime = ".$json_decoded->_createdDateTime."\n";
        echo "strtotime       = ".strtotime( $json_decoded->_createdDateTime )."\n";
        echo "age             = $age\n";

        if ( $age > $age_of_oldest )
          $age_of_oldest = $age;
      }
    }
  }
}

echo "age_of_oldest = $age_of_oldest\n";

/*
usage:
php test/redis/mq_monitor.php $MW_QUEUE

Example:
php test/redis/mq_monitor.php ULTRA_MW/OUTBOUND/SYNCH
*/

?>
