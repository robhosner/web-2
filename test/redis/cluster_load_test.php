<?php

/*

usage:

%> php test/redis/cluster_load_test.php $MAX_CYCLES

*/

require_once 'db.php';
require_once '/opt/php/vendor/predis/autoload.php';

$hosts = array(
  'tcp://162.222.64.193:6379/?timeout=60',
  'tcp://162.222.64.194:6379/?timeout=60',
  'tcp://162.222.64.195:6379/?timeout=60'
);

#Primo
#$hosts = array(
#  'tcp://162.222.64.200:6379/?timeout=60',
#  'tcp://162.222.64.201:6379/?timeout=60',
#  'tcp://162.222.64.202:6379/?timeout=60'
#);

$start = time();

dlog('',"start = ".$start);

$max = 10;

if ( ! empty( $argv[1] ) && is_numeric( $argv[1] ) )
  $max = $argv[1];

try
{
  \logDebug('hosts = '.json_encode($hosts));

  # Predis Client PHP class - object instantiation
  $redis = new \Predis\Client(
    $hosts,
    array( 'cluster' => 'redis' )
    #array( 'cluster' => 'ultra' )
  );

  load_test( $redis , $max );
}
catch(\Exception $e)
{
  \logFatal( $e->getMessage() );
}

$end = time();

dlog('',"end = ".$end);
dlog('',"time = ". ( $end - $start ) );

function load_test( $redis , $max )
{
  $numbers = range( 0 , $max - 1 );

  #dlog('',"%s",$numbers);

  foreach( $numbers as $n )
  {
    $k = 'test_'.getmypid().'_'.sprintf("%08d", $n);

    dlog('',"%s",$k);

    // strings
    $redis->del( $k );
    $redis->setnx( $k , $k );
    $redis->get( $k );
    $redis->set( $k , $k );
    $redis->get( $k );
    $redis->expire( $k , 60 );
    $redis->del( $k );

    // lists
    $l = 'L_' . $k ;
    $redis->llen( $l );
    $redis->lpush( $l , $l );
    $redis->lrange( $l , -100 , 100 );
    $redis->lpop( $l );
    $redis->del( $l );

    // sets
    $s = 'S_' . $k ;
    $redis->sadd( $s , 1 );
    $redis->sadd( $s , 2 );
    $redis->smembers( $s );
    $redis->spop( $s );
    $redis->srandmember( $s );
    $redis->del( $s );

    // hashes
    $h = 'H_' . $k ;
    $redis->hmset( $h , array('a'=>time(),'b'=>'test','q'=>'z','f'=>'t') );
    $redis->hmget( $h , array('a','b','c','f') );
    $redis->del( $h );

    // sorted sets
    $z = 'Z_' . $k ;
    $redis->zadd( $z , 1 , 1 );
    $redis->zadd( $z , 2 , 2 );
    $redis->zrange( $z , 0 , -1 );
    $redis->zrem( $z , 1 );
    $redis->zrem( $z , 2 );
    $redis->del( $z );
  }
}

