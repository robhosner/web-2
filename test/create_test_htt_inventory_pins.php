<?php

// php test/create_test_htt_inventory_pins.php 1234 5000000102

die;

require_once 'db.php';

require_once 'db/htt_inventory_pin.php';
require_once 'db/htt_inventory_pinbatches.php';

if (!isset($argv[1]) || !is_numeric($argv[1]) || strlen($argv[1]) != 4)
{
  echo 'Prefix ($argv[1]) in wrong format (len == 4 && is_numeric)' . PHP_EOL;
  exit;
}
$prefix = $argv[1];

if (!isset($argv[2]))
{
  echo 'Serial number id ($argv[2]) should be provided.';
}
$serial_number_id = $argv[2];

teldata_change_db();

$pins = array();

$author = 'jsteinmetz';
$upc = 'for_tests';

// 19
$value = 44;
$sku = '2015-04-16 $44 Cards';

$batch = array(
  'serial_number_id' => $serial_number_id,
  'sku' => $sku,
  'upc' => $upc,
  'channel' => 'INDIRECT'
);

$query = htt_inventory_pinbatches_insert_query($batch);
logged_mssql_query($query);

$complete_pins = array();

for ($i = 0; $i < 20; $i++)
{
  $suffix = $i;
  if ($suffix < 10)
    $suffix = '0' . "$i";

  $pin = $prefix . '' . $value . '' . '5432112345' . $suffix; // 1234 34 10000 12345
  $serial = $prefix . $prefix . $value . $suffix;


  $params = array(
    'serial_number' => $serial,
    'pin' => $pin,
    'pin_value' => $value,
    'method_used' => 'test',
    'status' => 'AT_FOUNDRY',
    'created_by' => $author,
    'expires_date' => 'NULL',
    'customer_used' => 0,
    'inventory_masteragent' => 32,
    'inventory_distributor' => 0,
    'inventory_dealer' => 0,
    'last_changed_by' => $author,
    'serial_number_id' => $serial_number_id,
    'expires_date' => 'NULL'
  );

  $query = htt_inventory_pin_insert_query($params);
  logged_mssql_query($query);

  $complete_pins[] = array($serial, $pin);
}

print_r($complete_pins);

?>