<?php

// PROD-262

/*
There is a system hole which allows customer numbers to be ported into our system. Our automated runner is checking for specific responses and if the response is not provided in time, we will send the account to Port-in denied and cancelled. However in a large number of cases, the port is still processed and ultra needs to be able to have records of those accounts.
*/

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/Adapter/Control.php';

if ( !isset($argv[1]) )
  die("Missing argument\n");

$msisdn = $argv[1];

$mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

$result = $mwControl->mwQuerySubscriber(
  array(
    'actionUUID' => time(),
    'iccid'      => NULL,
    'msisdn'     => $msisdn
  )
);

if ( $result->is_failure() )
  die("mwQuerySubscriber failed");

echo "\nSubscriberStatus : ".$result->data_array['body']->SubscriberStatus."\n";

if ( ( $result->data_array['body']->SubscriberStatus != 'Active' )
  && ( $result->data_array['body']->SubscriberStatus != 'Suspended' )
  )
  die("$msisdn is not Active");

// get HTT_CANCELLATION_REASONS row
$sql = sprintf(
  "SELECT * FROM HTT_CANCELLATION_REASONS WHERE MSISDN = %s ORDER BY DEACTIVATION_DATE DESC",
  mssql_escape_with_zeroes($msisdn)
);

teldata_change_db();

$result = mssql_fetch_all_objects(logged_mssql_query($sql));

if ( $result && is_array($result) && count($result) )
{
  echo "customer_id = ".$result[0]->CUSTOMER_ID."\n";
  echo "iccid       = ".$result[0]->ICCID      ."\n";

  recover_ported_cancelled( $msisdn , $result[0]->CUSTOMER_ID , $result[0]->ICCID );
}
else
  die("No HTT_CANCELLATION_REASONS rows found for this MSISDN\n");

function recover_ported_cancelled( $msisdn , $customer_id , $iccid )
{
  // get HTT_TRANSITION_ARCHIVE row
  $sql = sprintf(
    "SELECT * FROM HTT_TRANSITION_ARCHIVE WHERE STATUS = 'CLOSED' AND TRANSITION_LABEL = 'Cancel Unportable' AND CUSTOMER_ID = %d",
    $customer_id
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
  {
    print_r( $result[0] );
exit;
  }




  // get HTT_TRANSITION_LOG row
  $sql = sprintf(
    "SELECT * FROM HTT_TRANSITION_LOG WHERE STATUS = 'CLOSED' AND TRANSITION_LABEL = 'Cancel Unportable' AND CUSTOMER_ID = %d",
    $customer_id
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
  {
    print_r( $result[0] );

    $cos_id = $result[0]->FROM_COS_ID;

    print_r( $cos_id );

    if ( ! $cos_id || ( $cos_id < 2 ) )
      die('No COS is found');

    undo_cancel_overlay( $msisdn , $customer_id , $iccid , $cos_id );

    $context = array(
      'customer_id' => $customer_id
    );

    $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

    print_r($change);
  }
  else
  {
    echo "'Cancel Unportable' transition not found\n";
  }
}

function undo_cancel_overlay( $msisdn , $customer_id , $iccid , $cos_id )
{
  $iccid    = luhnenize($iccid);
  $iccid_18 = substr($iccid,0,-1);

  // fix HTT_CUSTOMERS_OVERLAY_ULTRA
  $sql = 'UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET
current_mobile_number = "'.$msisdn.'" ,
current_iccid         = "'.$iccid_18.'" ,
plan_state            = "Port-In Requested" ,
CURRENT_ICCID_FULL    = "'.$iccid.'" ,
ACTIVATION_ICCID      = "'.$iccid_18.'" ,
ACTIVATION_ICCID_FULL = "'.$iccid.'" 
WHERE CUSTOMER_ID = '.$customer_id;

  if ( ! is_mssql_successful(logged_mssql_query($sql) ) )
    die('HTT_CUSTOMERS_OVERLAY_ULTRA update error');

  // fix COS_ID
  $sql = 'UPDATE ACCOUNTS SET COS_ID = '.$cos_id.' WHERE CUSTOMER_ID = '.$customer_id;

  if ( ! is_mssql_successful(logged_mssql_query($sql) ) )
    die('ACCOUNTS update error');
}

?>
