<?php


# Usage:
# php test/api/test_portal.php OrderSIMCard
# php test/api/test_portal.php AcceptTermsOfService
# php test/api/test_portal.php CreateWebUser
# php test/api/test_portal.php Login $account_login $account_password
# php test/api/test_portal.php CustomerDetail $customer_id [OCS_BALANCE]
# php test/api/test_portal.php CustomerInfo $customer_id
# php test/api/test_portal.php ShipwireOrderDetails
# php test/api/test_portal.php GetCustomerShipwireOrders
# php test/api/test_portal.php GetMarketingSettings
# php test/api/test_portal.php SetMarketingSettings
# php test/api/test_portal.php GetTransactionHistory
# php test/api/test_portal.php ResetPasswordEmail
# php test/api/test_portal.php ResetPasswordSMS
# php test/api/test_portal.php ChangePlanImmediate $CUSTOMER_ID $PLAN
# php test/api/test_portal.php ChangePlanFuture $CUSTOMER_ID $PLAN
# php test/api/test_portal.php ActivateSuspendedCustomer
# php test/api/test_portal.php ClearCreditCard
# php test/api/test_portal.php UpdateAutoRecharge $VALUE
# php test/api/test_portal.php ForgotUsername $EMAIL
# php test/api/test_portal.php SetLoginName $LOGIN_NAME
# php test/api/test_portal.php SetPassword $PASSWORD
# php test/api/test_portal.php CheckSession
# php test/api/test_portal.php ExtendSession
# php test/api/test_portal.php KillSession
# php test/api/test_portal.php ReplaceSIMCard $CUSTOMER_ID, $OLD_ICCID $NEW_ICCID
# php test/api/test_portal.php SetCustomerFields	CUSTOMER_ID
# php test/api/test_portal.php GetAllowedDataRechargeByPlan $PLAN
# php test/api/test_portal.php ApplyDataRecharge $CUSTOMER_ID $SOC_ID (zsession)
# php test/api/test_portal.php CheckDataRechargeByMSISDN $MSISDN
# php test/api/test_portal.php CheckDataRechargeByCustomerId $CUSTOMER_ID
# php test/api/test_portal.php ChangePlanSuspended $CUSTOMER_ID $PLAN
# php test/api/test_portal.php ApplyVoiceRecharge $MSISDN
# php test/api/test_portal.php CheckVoiceRechargeByCustomerId $CUSTOMER_ID
# php test/api/test_portal.php GetIDDHistory				(zsession)

/*
zsession is obtained after login, you should set the following environment variables
 export TEST_CUSTOMER='1346851869'
 export TEST_PP='password'
*/

include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');
require_once 'Ultra/tests/API/TestConstants.php';


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/portal/1/ultra/api/';


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_portal__CheckVoiceRechargeByCustomerId extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__CheckVoiceRechargeByCustomerId';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}



class Test_portal__ApplyVoiceRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ApplyVoiceRecharge';

    $params = array(
      'msisdn'       => $argv[2],
      'voice_soc_id' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__SetLoginName extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__SetLoginName';

    $params = array(
      'zsession'      => test_login_for_zsession( $curl_options ),
      'account_login' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__CheckDataRechargeByMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__CheckDataRechargeByMSISDN';

    $params = array(
      'msisdn' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ChangePlanSuspended extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ChangePlanSuspended';

    $params = array(
      'zsession'      => make_zsession_from_customer_id( $argv[2] ),
      'targetPlan'    => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__CheckDataRechargeByCustomerId extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__CheckDataRechargeByCustomerId';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ApplyDataRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ApplyDataRecharge';

    $params = array(
      'zsession'       => make_zsession_from_customer_id($argv[2]),
      'data_soc_id'    => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,360);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__GetAllowedDataRechargeByPlan extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__GetAllowedDataRechargeByPlan';

    $params = array(
      'plan' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__GetIDDHistory extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__GetIDDHistory';

    $params = array(
      'zsession'    => test_login_for_zsession( $curl_options ),
      'start_epoch' => $argv[2],
      'end_epoch'   => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__SetCustomerFields extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__SetCustomerFields';

    teldata_change_db();
    $customer = get_customer_from_customer_id($argv[2]);

    $params = array(
      'zsession'                 => createCustomerSession($customer->LOGIN_NAME, $customer->LOGIN_PASSWORD),
/*
      'first_name'               => 'test first name',
      'last_name'                => 'test last name',
      #'cc_name'                  => 'test cc name',
      'cc_address1'              => 'test 1',
      'cc_address2'              => 'test 2',
      'cc_city'                  => 'New York',
      'cc_country'               => 'US',
      'cc_state_or_region'       => 'NY',
      'cc_postal_code'           => '12345',
      'account_number_phone'     => '1231231234',
      'account_number_email'     => 'test1@ultra.me',
*/
      'address1'                 => 'test a 1',
      'address2'                 => 'test a 2',
      'city'                     => 'New York',
      'country'                  => 'US',
      'state_or_region'          => 'NY',
      'postal_code'              => '12345',
      'preferred_language'       => 'ES'
    );

#$first_name, $last_name, $cc_name, $cc_address1, $cc_address2, $cc_city, $cc_country, $cc_state_or_region, $cc_postal_code, $account_number_phone, $account_number_email
#$account_cc_exp, $account_cc_cvv, $account_cc_number, $address1, $address2, $city, $country, $state_or_region, $postal_code

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ReplaceSIMCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ReplaceSIMCard';

    $params = array(
      'zsession'    => make_zsession_from_customer_id($argv[2]),
      'old_ICCID'   => $argv[3],
      'new_ICCID'   => $argv[4]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,1000);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__CheckSession extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__CheckSession';

    # login
    $zsession = test_login_for_zsession( $curl_options );

    print_r($zsession);

    $params = array(
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ExtendSession extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__ExtendSession';

    $params = array(
      'zsession'    => test_login_for_zsession( $curl_options ),
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__KillSession extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
  
    $url = $base_url . 'portal__KillSession';

    $params = array(
      'zsession'    => test_login_for_zsession( $curl_options ),
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__SetPassword extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__SetPassword';

    $params = array(
      'zsession'         => test_login_for_zsession( $curl_options ),
      'account_password' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__GetTransactionHistory extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__GetTransactionHistory';

    $params = array(
      'zsession'    => make_zsession_from_customer_id( 18931 ),
      'start_epoch' => time() - 60 * 60 * 24 * 365,
      'end_epoch'   => time()
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__UpdateAutoRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__UpdateAutoRecharge';

    $params = array(
      'zsession'      => test_login_for_zsession( $curl_options ),
      'auto_recharge' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ForgotUsername extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ForgotUsername';

    $params = array(
      'email' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ResetPasswordSMS extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__ResetPasswordSMS';

    $params = array(
      'login_string' => 'frankie10',#1000000031
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ResetPasswordEmail extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__ResetPasswordEmail';

    $params = array(
      'login_string' => 'alpha',#1000000031
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__GetMarketingSettings extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__GetMarketingSettings';

    $params = array(
      'zsession' => make_zsession_from_customer_id( $argv[2] )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ClearCreditCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__ClearCreditCard';

    $params = array(
      'zsession' => test_login_for_zsession( $curl_options )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ActivateSuspendedCustomer extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__ActivateSuspendedCustomer';

    $params = array(
      'zsession' => test_login_for_zsession( $curl_options )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ChangePlanImmediate extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ChangePlanImmediate';

    $params = array(
      'zsession'   => make_zsession_from_customer_id($argv[2]),
      'targetPlan' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ChangePlanFuture extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__ChangePlanFuture';

    $params = array(
      'zsession'   => make_zsession_from_customer_id($argv[2]),
      'targetPlan' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__SetMarketingSettings extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__SetMarketingSettings';

    // invalid marketing_voice_option value
    $params = array(
      'zsession' => make_zsession_from_customer_id($argv[2]),
      'marketing_settings' => array(
        'marketing_sms_option', 1,
        'marketing_email_option', 0,
        'marketing_voice_option', 3
      )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);
    echo "\n\n$json_result\n\n";
    print_r(json_decode($json_result));

    // valid
    $params = array(
      'zsession' => make_zsession_from_customer_id($argv[2]),
      'marketing_settings' => array(
        'marketing_sms_option', 1,
        'marketing_email_option', 0,
        'marketing_voice_option', 1
      )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    // check valid
    $params = array('zsession' => make_zsession_from_customer_id($argv[2]));
    $url = $base_url . 'portal__GetMarketingSettings';
    $json_result = curl_post($url,$params,$curl_options,NULL,10);
    echo "\n\n$json_result\n\n";
    print_r(json_decode($json_result));    
  }
}


class Test_portal__GetCustomerShipwireOrders extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__GetCustomerShipwireOrders';

    $params = array(
      'zsession' => test_login_for_zsession( $curl_options )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__ShipwireOrderDetails extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__ShipwireOrderDetails';

    $params = array(
      'zsession' => test_login_for_zsession( $curl_options ),
      'order_id' => '1348584820-410929-1'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__AcceptTermsOfService extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $e_config;

    $url = $base_url . 'portal__AcceptTermsOfService';

    $params = array(
      'customer_id' => 31
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__CustomerInfo extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $e_config;
    global $argv;

    $url = $base_url . 'portal__CustomerInfo';

    $params = array(
      'zsession' => make_zsession_from_customer_id($argv[2]),
      'get_voice_minutes' => (isset($argv[3])) ? $argv[3] : 0
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__CustomerDetail extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__CustomerDetail';

    if (isset($argv[2]))
      $customer_id = $argv[2];
    else
      die("ERROR: customer_id parameter is missing \n");

    $mode = isset($argv[3]) ? $argv[3] : NULL;

    $zsession = make_zsession_from_customer_id($customer_id);

    $params = array(
      'zsession' => $zsession,
      'mode'     => $mode
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__Login extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'portal__Login';

    $params = array(
      'account_login'    => $argv[2],
      'account_password' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_portal__CreateWebUser extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'portal__CreateWebUser';

    $params = array(
      'account_login'        => 'stest1sx1zzzaxz',
      'account_first_name'   => 'sas',#time() - 2,
      'account_last_name'    => 'azz',#time() - 1,
      'account_password'     => time(),
      'account_number_email' => "sxaalasxsezxx@ultra.me",
      'account_country'      => 'US'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_portal__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>
