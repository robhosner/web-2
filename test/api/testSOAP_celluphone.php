<?php
include_once 'lib/util-common.php';

// Authentication info
$username = 'dougmeli';
$password = 'Flora';

// ** WSDL Interfaces that are supported
$target = 'ePay';
$target = 'threeci';
$target = 'celluphone-develop';
$target = 'ePay';
$target = 'interactivecare';
$target = 'celluphone'; // Last one, this is used

try
{

  abstract class SoapModule
  {

    public $client = null;

    /**
     *
     * @return the $client
     */
    public function getClient()
    {
      return $this->client;
    }

    /**
     *
     * @param string $test_wsdl          
     * @param string $childInterface          
     */
    function __construct($test_wsdl, $childInterface)
    {
      try
      {
        $this->client = new SoapClient($test_wsdl, array(
          'login' => "dougmeli",'password' => "Flora"
        ));
        echo "Testing SOAP Interface for :" . $childInterface . PHP_EOL;
      }
      catch (SoapFault $e)
      {
        dlog("", $e->getTraceAsString());
      }
    }

    /**
     *
     * @param string $xml
     *          -- SOAP Response XML
     * @return string
     */
    function pretty($xml)
    {
      $domxml = new DOMDocument('1.0');
      $domxml->preserveWhiteSpace = false;
      $domxml->formatOutput = true;
      $domxml->loadXML($xml);
      return $domxml->saveXML();
    }

    abstract function params();

    abstract function execute();
  }
  
  // To create a new class to test a new SOAP innterface,
  // Copy the class below
  // Change the name to the interface you wish to test
  // Add the parameters in the returned array in the params() function
  // You are done, it will get discovered and executed as part of the test suite
  //
  class customercare__CustomerInfoByID extends SoapModule
  {

    function __construct($test_wsdl)
    {
      parent::__construct($test_wsdl, get_class($this));
    }

    function params()
    {
      return array(
        'partner_tag' => "John Testing",'customer_id' => 2546,'mode' => ''
      );
    }

    function execute()
    {
      // Execute and print out results
      echo parent::pretty(parent::getClient()->__soapCall(get_class($this), $this->params()));
    }
  }

  class SomeOtherTest extends SoapModule
  {

    function __construct($test_wsdl)
    {
      parent::__construct($test_wsdl, get_class($this));
    }

    function params()
    {
      return array(
        'p1' => 1,'p2' => 2
      );
    }

    function execute()
    {
      $bar = $this->params();
      print "SomeOtherTest::execute, p2=" . $bar['p2'] . "\n";
    }
  }

  class SOAPTestRunner
  {

    private $configData;

    private $modules = array();

    public function __construct()
    {
      $value = array(
        'execute' => array()
      );
      $classList = get_declared_classes();
      foreach ($classList as $myClass)
      {
        if (is_subclass_of($myClass, 'SoapModule'))
        {
          $this->configData[$myClass] = $value;
        }
      }
    }

    function runTest($wsdl)
    {
      $interface = new ReflectionClass('SoapModule');
      foreach ($this->configData as $modulename => $params)
      {
        $module_class = new ReflectionClass($modulename);
        if (! $module_class->isSubclassOf($interface))
        {
          throw new Exception("unknown module type: $modulename");
        }
        $module = $module_class->newInstance($wsdl);
        foreach ($module_class->getMethods() as $method)
        {
          $this->handleMethod($module, $method, $params);
        }
        array_push($this->modules, $module);
      }
    }

    function handleMethod(SoapModule $module, ReflectionMethod $method, $params)
    {
      $name = $method->getName();
      $args = $method->getParameters();
      if (count($args) != 0 || $name != "execute")
      {
        return false;
      }
      $property = $name;
      if (! isset($params[$property]))
      {
        return false;
      }
      
      if ($method->getShortName() == $name)
      {
        $method->invoke($module);
      }
    }
  }
  
  $test = new SOAPTestRunner();
  $wsdlTest = "https://jd1-dev.ultra.me/ps/" . $target . "/1/ultra/api.wsdl";
  $test->runTest($wsdlTest);
}
catch (Exception $e)
{}
