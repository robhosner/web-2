<?php


# Usage:
# php test/api/test_interactivecare.php ShowCustomerInfo           $MSISDN [GET_DATA_USAGE]           OK
# php test/api/test_interactivecare.php FindDealersByZip           $MSISDN $ZIPCODE  OK (it works, but we should increase the radius)
# php test/api/test_interactivecare.php GetCommandHelp             $MSISDN           OK
# php test/api/test_interactivecare.php ApplyPIN                   $MSISDN $PIN      OK
# php test/api/test_interactivecare.php ApplyDataRecharge          $ID     $SOC_ID
# php test/api/test_interactivecare.php CheckDataRechargeByMSISDN  $MSISDN
# php test/api/test_interactivecare.php ApplyVoiceRecharge         $CUSTOMER_ID $VOICE_SOC_ID
# php test/api/test_interactivecare.php CheckVoiceRechargeByMSISDN $MSISDN
# php test/api/test_interactivecare.php SetLanguagebyMSISDN $MSISDN $LANG [$NOTIFY]


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/interactivecare/1/ultra/api/';


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_interactivecare__CheckVoiceRechargeByMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__CheckVoiceRechargeByMSISDN';

    $params = array(
      'msisdn'       => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__SetLanguagebyMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__SetLanguagebyMSISDN';

    $params = array(
      'msisdn'              => $argv[2],
      'preferred_language'  => $argv[3],
      'notify'              => empty($argv[4]) ? NULL : $argv[4]);

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__CheckDataRechargeByMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__CheckDataRechargeByMSISDN';

    $params = array(
      'msisdn'       => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__ApplyVoiceRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyVoiceRecharge';

    $params = array(
      'customer_id'  => $argv[2],
      'voice_soc_id' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__ApplyDataRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyDataRecharge';

    $params = array(
      'customer_id'    => $argv[2],
      'data_soc_id'    => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,360);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__FindDealersByZip extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__FindDealersByZip';

    $params = array(
      'msisdn'   => $argv[2],
      'zip_code' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__GetCommandHelp extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__GetCommandHelp';

    $params = array(
      'msisdn'   => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__ShowCustomerInfo extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ShowCustomerInfo';

    $params = array(
      'msisdn' => $argv[2]
    );
    
    if ( ! empty($argv[3]))
      $params['get_data_usage'] = TRUE;

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_interactivecare__ApplyPIN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyPIN';

    $params = array(
      'msisdn'     => $argv[2],
      'pin_number' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_interactivecare__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();

/*

Examples:

php test/api/test_interactivecare.php ApplyDataRecharge 2147 PayGo_50_250
php test/api/test_interactivecare.php ApplyDataRecharge 1990 Bucketed_500_1000

*/

?>
