#!/usr/bin/perl


use warnings;
use strict;


use Data::Dumper;
use Crypt::SSLeay;
use LWP::UserAgent 6;
#require LWP::Protocol::https;

use Net::SSL (); # From Crypt-SSLeay
BEGIN {
  $Net::HTTPS::SSL_SOCKET_CLASS = "Net::SSL"; # Force use of Net::SSL
}

use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;

use Log::Report mode => 'DEBUG';

my $wsdl_file = shift;

print "wsdl_file = $wsdl_file\n";

my $ssl_cert_file = '/etc/httpd/certs/acc/ULTRA.pem';

print "ssl_cert_file = $ssl_cert_file\n";

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}  = "0";

$ENV{HTTPS_DEBUG} = 1;

#$ENV{HTTPS_CA_DIR} = '/etc/httpd/certs/acc/';

$ENV{HTTPS_CERT_FILE} = $ssl_cert_file;
$ENV{HTTPS_KEY_FILE}  = $ssl_cert_file;
#$ENV{HTTPS_CERT_PASS} = 'ultra1';

# Load and compile all the WSDL files.
my $wsdl = XML::Compile::WSDL11->new( $wsdl_file );

$wsdl->compileCalls;

my $command = 'GetNGPList';

my $params =
{
  'zipCode' => '10128'
};

my ($answer, $trace);

eval
{
  ($answer,$trace) = $wsdl->call( $command => { parameters => $params } );
};

if ( $@ )
{
  die $@;
}
elsif ( ! $trace )
{
  die("No trace available after $command SOAP call");
}
else
{
  print Dumper( $trace->request() );
print "\n\n\n\n";

  print "\nSOAP request:\n";
  print $trace->request()->content();

  print "\nSOAP response:\n";
  print $trace->response()->content();

  my $http_req = $trace->request;
  print Dumper($http_req);
}

print "\n";

__END__

perl test/api/test_SOAP5.pl runners/amdocs/TMobile_MVNE_3_00_production.wsdl


[...]
trace: loading extension XML::Compile::Transport::SOAPHTTP
Enter PEM pass phrase:
SSL_connect:before/connect initialization
SSL_connect:SSLv2/v3 write client hello A
SSL_connect:SSLv3 read server hello A
SSL_connect:SSLv3 read server certificate A
SSL_connect:SSLv3 read server certificate request A
SSL_connect:SSLv3 read server done A
SSL_connect:SSLv3 write client certificate A
SSL_connect:SSLv3 write client key exchange A
SSL_connect:SSLv3 write certificate verify A
SSL_connect:SSLv3 write change cipher spec A
SSL_connect:SSLv3 write finished A
SSL_connect:SSLv3 flush data
SSL_connect:SSLv3 read finished A
SSL3 alert read:warning:close notify

SOAP request:
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body><tns:GetNGPList xmlns:tns="http://www.sigvalue.com/acc"><tns:zipCode>10128</tns:zipCode></tns:GetNGPList></SOAP-ENV:Body></SOAP-ENV:Envelope>
[...]

http://www.openldap.org/lists/openldap-software/200307/msg00049.html

(That's what "TLS trace: SSL3 alert read:warning:close notify" means - the
server has read an alert sent from the client. The client sent the alert
saying "I'm closing this connection.")





use strictures;
use Data::Dumper;
use Crypt::SSLeay;
use LWP::UserAgent 6;
require LWP::Protocol::https;

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}  = "0";

my $ua = LWP::UserAgent->new;
$ua->ssl_opts(
    SSL_cert_file => '/home/rizwank/ULTRA.pem',
    SSL_key_file => '/home/rizwank/ULTRA.pem',
    SSL_passwd_cb => sub { return "ultra1"; },
    SSL_version         => 'SSLv3'
);
my $ret=$ua->post(
    'https://acc.mvne2.t-mobile.com:443/acc/inbound.aspx',
    Content => 'string',
);




