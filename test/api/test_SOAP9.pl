#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;
use Crypt::SSLeay;
use LWP::UserAgent 6;
use XML::Compile::SOAP 2.38;
use XML::Compile::SOAP11;
use XML::Compile::WSDL11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;
use Log::Report mode => 'DEBUG';

my $ssl_cert_file = '/etc/httpd/certs/acc/ULTRA.pem';

print "ssl_cert_file = $ssl_cert_file\n";

my $wsdl_file = shift;

print "wsdl_file = $wsdl_file\n";

my $end_point = 'https://acc.mvne2.t-mobile.com/acc/inbound.aspx';

print "end_point = $end_point\n";

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = "0";
$ENV{HTTPS_DEBUG}                  = 1;
$ENV{HTTPS_CERT_FILE}              = $ssl_cert_file;
$ENV{HTTPS_KEY_FILE}               = $ssl_cert_file;

my $ssl_opts = {
    SSL_cert_file => $ssl_cert_file,
    SSL_key_file  => $ssl_cert_file,
    SSL_passwd_cb => sub { return "ultra1"; },
    SSL_version   => 'SSLv3'
};

# Load and compile all the WSDL files.
my $wsdl = XML::Compile::WSDL11->new( $wsdl_file );

# obtained with  $wsdl->compileCalls(); $wsdl->printIndex;
my $acc_commands = [ qw/
  ActivateSubscriber
  CancelDeviceLocation
  CancelPortIn
  ChangeIMEI
  ChangeMSISDN
  ChangeSIM
  CheckBalance
  DeactivateSubscriber
  GetNGPList
  GetNetworkDetails
  PortInEligibility
  QueryStatus
  QuerySubscriber
  ReactivateSubscriber
  RenewPlan
  ResendOTA
  ResetVoiceMailPassword
  RestoreSubscriber
  SendSMS
  SuspendSubscriber
  UpdateBalance
  UpdatePlanAndFeatures
  UpdatePortIn
  UpdateWholesalePlan/ ];

# compile every command separately
for my $acc_command( @$acc_commands )
{
  my $ua = LWP::UserAgent->new;

  $ua->default_headers->header( 'SOAPAction' => 'http://www.sigvalue.com/acc/'.$acc_command );

  $ua->ssl_opts( %$ssl_opts );

  my $trans = XML::Compile::Transport::SOAPHTTP->new( user_agent => $ua , address => $end_point );

  my $op = $wsdl->operation( $acc_command );

  $wsdl->compileCall( $op , transport => $trans );
}

do_call(
  'GetNetworkDetails',
  {
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-12-04T12:00:00',
  },
  'MSISDN' => '4058878653',
  'ICCID'  => '8901260842107741804',
  }
);

do_call(
  'GetNGPList',
  {
  'zipCode' => '77583',
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-11-18T11:33:14',
  }
  }
);

exit;

sub do_call
{
  my ( $command , $params ) = @_;

  my ($answer, $trace);

  my $timestamp_start = time;

  eval
  {
    ($answer,$trace) = $wsdl->call( $command => { parameters => $params } );
  };

  my $timestamp_end = time;

  print "SOAP time = ".($timestamp_end - $timestamp_start)." seconds\n";

  if ( $@ )
  {
    die $@;
  }
  elsif ( ! $trace )
  {
    die("No trace available after $command SOAP call");
  }
  else
  {
    #print Dumper( $trace->request() );
    #print "\n\n\n\n";

    print "\nSOAP request:\n";
    print $trace->request()->content();

    print "\nSOAP response:\n";
    print $trace->response()->content();
  }

  print "\n";
  print "=======================================================================================\n";

}

__END__

perl test/api/test_SOAP9.pl runners/amdocs/TMobile_MVNE_3_00_production.wsdl

