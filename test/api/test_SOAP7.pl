#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;
use Crypt::SSLeay;
use LWP::UserAgent 6;

my $ua = LWP::UserAgent->new;

$ua->default_headers->header( 'SOAPAction' => 'http://www.sigvalue.com/acc/GetNetworkDetails' );

$ua->ssl_opts(
    SSL_cert_file => '/home/rizwank/ULTRA.pem',
    SSL_key_file => '/home/rizwank/ULTRA.pem',
    SSL_passwd_cb => sub { return "ultra1"; },
    SSL_version         => 'SSLv3'
);

use XML::Compile::SOAP 2.38;
use XML::Compile::SOAP11;
use XML::Compile::WSDL11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;

use Log::Report mode => 'DEBUG';

my $wsdl_file = shift;

print "wsdl_file = $wsdl_file\n";

my $ssl_cert_file = '/etc/httpd/certs/acc/ULTRA.pem';

print "ssl_cert_file = $ssl_cert_file\n";

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}  = "0";

$ENV{HTTPS_DEBUG} = 1;

$ENV{HTTPS_CERT_FILE} = $ssl_cert_file;
$ENV{HTTPS_KEY_FILE}  = $ssl_cert_file;

# Load and compile all the WSDL files.
my $wsdl = XML::Compile::WSDL11->new( $wsdl_file );

my $trans = XML::Compile::Transport::SOAPHTTP->new( user_agent=>$ua,
address=>"https://acc.mvne2.t-mobile.com/acc/inbound.aspx");

#$wsdl->compileCalls(transport=>$trans);



=head4
my $command = 'GetNGPList';

my $params =
{
  'zipCode' => '77583',
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-11-18T11:33:14',
  }
};

my $command = 'QuerySubscriber';

my $params =
{
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-11-18T11:33:14',
  },
  'MSISDN' => '1001001000'
};

=cut

my $command = 'GetNetworkDetails';

my $op = $wsdl->operation( $command );
$wsdl->compileCall( $op , transport=>$trans );

my $params =
{
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-12-04T12:00:00',
  },
  'MSISDN' => '4058878653',
  'ICCID'  => '8901260842107741804',
};

my ($answer, $trace);

eval
{
  ($answer,$trace) = $wsdl->call( $command => { parameters => $params } );
};

if ( $@ )
{
  die $@;
}
elsif ( ! $trace )
{
  die("No trace available after $command SOAP call");
}
else
{
  print Dumper( $trace->request() );
  print "\n\n\n\n";

  print "\nSOAP request:\n";
  print $trace->request()->content();

  print "\nSOAP response:\n";
  print Dumper($trace->response());
}

print "\n";

__END__

