<?php

/*
 * note: to obtain a valid zsession for API testing use make_zsession_from_customer_id()
 * instead of removed test_login_for_zsession()
 */

function test_curl($url,$params,$curl_options)
{
  $result = curl_post($url,$params,$curl_options,NULL,10);

  print_r(json_decode($result));
}


function record_test_ko($test_name)
{
  global $tests_outcome;

  $tests_outcome['ko']++;

  echo "Test $test_name **FAILED**\n";
}


function record_test_ok($test_name)
{
  global $tests_outcome;

  $tests_outcome['ok']++;

  echo "Test $test_name **SUCCEEDED**\n";
}


function test_success($url,$params,$expected_values_array,$timeout=10)
{
  # expects a success from the API call

  $test_name = 'test_success';

  global $curl_options;

  $result = curl_post($url,$params,$curl_options,NULL,$timeout);

  if ( ! $result )
  {
    echo "* curl_post FAILED *\n\n";
    record_test_ko($test_name);
    return;
  }

  $result_decoded = json_decode($result);

  if ( is_object($result_decoded) )
  {
    $result_decoded_array = get_object_vars($result_decoded);

    $not_found_error = FALSE;

    foreach($expected_values_array as $expected_value)
    {
      if ( ! isset($result_decoded_array[$expected_value]) )
      {
        echo "* Expected return value missing for field $expected_value *\n\n";
        $not_found_error = TRUE;
      }
    }
  }
  else
  {
    $not_found_error = TRUE;
  }

  if ( $not_found_error )
  {
    record_test_ko($test_name);
  }
  else if ( $result_decoded->success != 1 )
  {
    record_test_ko($test_name);
  }
  else if ( count( $result_decoded->errors ) > 1 )
  {
    print_r($result_decoded->errors);
    record_test_ko($test_name);
  }
  else if ( count( $result_decoded->warnings ) > 1 )
  {
    print_r($result_decoded->warnings);
    record_test_ko($test_name);
  }
  else
  {
    record_test_ok($test_name);
  }
}


function verify_succeed($result_decoded,$test_name)
{
  #TODO: check if $result_decoded is an object

  if ( $result_decoded->success != 1 )
  {
    record_test_ko($test_name);
  }
  else if ( count( $result_decoded->errors ) != 0 )
  {
    record_test_ko($test_name);
  }
  else if ( count( $result_decoded->warnings ) == 0 )
  {
    record_test_ko($test_name);
  }
  else
  {
    # [warnings] => Array ( [0] => ERR_API_INTERNAL: always_succeed )

    record_test_ok($test_name);
  }
}


function verify_fail($result_decoded,$test_name)
{
  if ( $result_decoded->success == 1 )
  {
    record_test_ko($test_name);
  }
  else if ( count( $result_decoded->errors ) == 0 )
  {
    record_test_ko($test_name);
  }
  else
  {
    # [errors] => Array ( [0] => ERR_API_INTERNAL: always fail )

    record_test_ok($test_name);
  }
}


function test_always($url,$always,$mock)
{
  $test_name = 'always_' . $always;
  global $apache_username;
  global $apache_password;

  $curl_options = array(
    CURLOPT_USERPWD  => "$apache_username:$apache_password",
    CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    CURLOPT_HEADER => 0,
    CURLOPT_FRESH_CONNECT => 1,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_FORBID_REUSE => 1,
    CURLOPT_TIMEOUT => 4,
  );

  $fields = array(
    'always' => $always,
    'mock'   => $mock
  );

  $ch = curl_init();

  $fields_string = '';

  foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
  rtrim($fields_string,'&');

  //set the url, number of POST vars, POST data
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_POST,count($fields));
  curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

  curl_setopt_array($ch, ($curl_options));

  //execute post
  $result = curl_exec($ch);

  print_r($result);
  echo "\n";

  $result_decoded = json_decode($result);

  //close connection

  curl_close($ch);

  if ( $always == 'succeed' )
  {
    verify_succeed($result_decoded,$test_name);
  }
  else
  {
    verify_fail($result_decoded,$test_name);
  }
}


function test_missing_rest($url,$params,$missing_field)
{
  # expect an error from the API call due to the missing $field

  $test_name = 'test_missing ' . $missing_field;

  global $curl_options;

  unset($params[$missing_field]);

  $result = curl_post($url,$params,$curl_options,NULL,10);

  # Verify that the error message is the correct one
  if (
    (preg_match("/Missing /i", $result))
    &&
    (preg_match("/$missing_field/", $result))
  )
  {
    record_test_ok($test_name);
  }
  else
  {
    record_test_ko($test_name);
  }
}


function test_wrong($url,$params,$field,$value)
{
  # expects an error from the API call die to a wrong value for $field

  $test_name = 'test_wrong ' . $field;

  global $curl_options;

  $params[$field] = 'wrong_value';

  if ( isset($value) && $value )
  {
    $params[$field] = $value;
  }

  $result = curl_post($url,$params,$curl_options,NULL,10);

  $result_decoded = json_decode($result);

  if ( $result_decoded->success == 1 )
  {
    record_test_ko($test_name);
  }
  else if ( count( $result_decoded->errors ) == 0 )
  {
    record_test_ko($test_name);
  }
  else
  {
    record_test_ok($test_name);
  }
}


function default_json_test($url,$params,$curl_options)
{
  # performs the curl and parse the JSON output

  $json_result = curl_post($url,$params,$curl_options);

  echo "JSON RESULT:";
  print_r($json_result);

  $parsed_json_result = json_decode($json_result, true);

  print_r($parsed_json_result);

  return $parsed_json_result;
}

/*
 * make_zsession_from_customer_id
 *
 * get customer login info from DB, call portal__Login and return result zsession
 * @param int customer_id
 * @author: VYT, 14-02-11
 */
function make_zsession_from_customer_id($customer_id)
{
    if (! $customer_id)
    {
      echo "ERROR: invalid customer_id parameter \n";
      return NULL;
    }

    // get customer info
    teldata_change_db();
    $customer = get_customer_from_customer_id($customer_id);
    if (! $customer)
    {
      echo "ERROR: failed to get customer for customer_id $customer_id \n";
      return NULL;
    }
    if (strlen($customer->LOGIN_NAME) < 4 || strlen($customer->LOGIN_PASSWORD) < 4)
    {
      echo "ERROR: username or password is missing or too short for customer_id $customer_id \n";
      return NULL;
    }

    // prepare API URL and parameters
    $url = find_site_url() . '/pr/portal/1/ultra/api/portal__Login';
    $params = array(
      'account_login'    => $customer->LOGIN_NAME,
      'account_password' => $customer->LOGIN_PASSWORD);

    echo "LOGIN    : ".$customer->LOGIN_NAME."\n";
    echo "PASSWORD : ".$customer->LOGIN_PASSWORD."\n";

    // execute login and check results
    $result = curl_post($url, $params, array(), NULL, 120);
    $decoded_result = json_decode($result);

    print_r($decoded_result);

    if (! $decoded_result->success != TRUE && count($decoded_result->errors))
    {
      echo "ERROR: failed to login customer $customer_id \n";
      return NULL;
    }
    if (empty($decoded_result->zsession))
    {
      echo "ERROR: failed to get zsession for customer $customer_id: \n";
      foreach ($decoded_result->errors as $message)
        echo "$message \n";
      return NULL;
    }

    // all's well
    return $decoded_result->zsession;
}


// returns an invalid ICCID (19 digits) unless $invalid is undefined or FALSE
function get_test_iccid($invalid=FALSE)
{
  $test_iccid = '000000000000000000';

  if ( ! $invalid )
  {
    $sql = 'select top 1 ICCID_FULL from HTT_INVENTORY_SIM where INVENTORY_STATUS in (\'ULTRA_NYC\') and GLOBALLY_USED=0 and MVNE=\'1\' order by newid()';

    echo "\n\n$sql\n\n";

    $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ( is_array($query_result) ) && count($query_result) )
    {
      $test_iccid = substr($query_result[0]->ICCID_FULL,0,-1);
    }
    else
    {
      die("Won't test due to fatal errors\n\n");
    }
  }
  else if ( $invalid == 'USED' )
  {
    # known used ICCID
    $test_iccid = '8901260842102254035';
  }

  echo "\n\nget_test_iccid returning $test_iccid\n\n";

  return $test_iccid;
}


function match_expected_values( $data , $expected_values )
{
  $errors = array();

  // verifies if $expected_values is included in $data

  foreach($expected_values as $key => $value)
  {
    if( ! isset($data[ $key ]) )
    {
      $errors[] = "No value found for field << $key >>";
    }
    else if ( $data[ $key ] != $value )
    {
      $errors[] = "Error in field << $key >> : expected << $value >> , obtained << ".$data[ $key ]." >>";
    }
  }

  return $errors;
}


function credit_card_testing_values()
{
  return array(
    'creditCard'         => '4147202104370414',#'5454871000080964',
    'creditExpiry'       => '0514',#'0815',
    'creditCVV'          => '291', #'1234',
    'creditAutoRecharge' => FALSE,
    'creditAmount'       => '5555',
    'customerAddressOne' => 'add 1 '.time(),
    'customerCity'       => 'New York',
    'customerState'      => 'NY'
  );
}

/**
 * Parse a set of HTTP headers
 *
 * @param string HTTP response headers
 * @return array with all the headers
 * @author: VYT, 13-02-18, shamlesslly riped off stackoverflow.com
 */
function parse_http_headers($headers)
{
  $output = array();
  $headers = explode("\n", $headers);

  if ('HTTP' === substr($headers[0], 0, 4))
  {
    list(, $output['status'], $output['status_text']) = explode(' ', $headers[0]);
    unset($headers[0]);
  }

  foreach ($headers as $v)
  {
    $h = preg_split('/:\s*/', $v);
    $output[strtolower($h[0])] = $h[1];
  }

  return $output;
}


?>
