use strict;

use Data::Dumper;
use SOAP::Lite +trace => 'debug';

#my $soap = SOAP::Lite->uri('ProvisioningStatus')->proxy('https://dougmeli:Flora@70.39.130.47:8799');
my $soap = SOAP::Lite->proxy('https://70.39.130.47:8799')
 ->on_action( sub { return "AddService_Callback"; } );
 #->on_action( sub { my $ac = shift; print STDOUT "-> $ac <-\n\n"; return "'http://tempuri.org/IMVNOWSAPICallbackService/AddService_Callback'"; } );
 #->on_action( sub { my $ac = shift; print STDOUT "-> $ac <-\n\n"; return "'http://tempuri.org/IMVNOWSAPICallbackService/QueryMSISDN_Callback'"; } );
#->on_action( sub { my $ac = shift; print STDOUT "-> $ac <-\n\n"; return '"http://tempuri.org/IMVNOWSAPICallbackService/CancelDeviceLocation_Callback"'; } );
#                     ->on_action( sub { my $ac = shift; print STDOUT "-> $ac <-\n\n"; return "http://tempuri.org/IMVNOWSAPIService/CancelDeviceLocation_Callback"; } );
                     #->on_action( sub { my $ac = shift; print STDOUT "-> $ac <-\n\n"; return "\"CancelDeviceLocation_Callback\""; } );
                     #->on_action( sub { my $ac = shift; print STDOUT "-> $ac <-\n\n"; return "CancelDeviceLocation_Callback"; } );

my $serializer = $soap->serializer();
$serializer->register_ns( 'http://schemas.xmlsoap.org/soap/envelope/', 's' );

my @p = (
  SOAP::Data->name('tag' => 'tag1')->type('string'),
  SOAP::Data->name('sessionId' => 'tag2')->type('string'),
  SOAP::Data->name('result' => 'tag3')->type('string'),
  SOAP::Data->name('message' => 'tag4')->type('string'),
);

my $method = 'AddService_Callback';
my $method_urn = 'http://tempuri.org/';

my $som = $soap->call(
  SOAP::Data->name($method)->attr({ 'xmlns' => $method_urn})
  =>
  SOAP::Data->name('tag' => 'sgssg')
);

print Dumper( $som->result );

__END__

&> perl test/api/test_SOAP3.pl

SOAP::Transport::HTTP::Client::send_receive: POST https://70.39.130.47:8799 HTTP/1.1
Accept: text/xml
Accept: multipart/*
Accept: application/soap
Content-Length: 458
Content-Type: text/xml; charset=utf-8
SOAPAction: AddService_Callback

<?xml version="1.0" encoding="UTF-8"?><s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><AddService_Callback xmlns="http://tempuri.org/"><tag xsi:type="xsd:string">sgssg</tag></AddService_Callback></s:Body></s:Envelope>
SOAP::Transport::HTTP::Client::send_receive: HTTP/1.1 200 Answer included
Date: Tue, 28 May 2013 20:37:32 GMT
Server: soap server
Content-Length: 235
Content-Type: text/xml; charset="utf-8"
Client-Date: Tue, 28 May 2013 20:37:32 GMT
Client-Peer: 70.39.130.47:8799
Client-Response-Num: 1
Client-SSL-Cert-Issuer: /C=US/ST=Arizona/L=Scottsdale/O=GoDaddy.com, Inc./OU=http://certificates.godaddy.com/repository/CN=Go Daddy Secure Certification Authority/serialNumber=07969287
Client-SSL-Cert-Subject: /O=*.ultra.me/OU=Domain Control Validated/CN=*.ultra.me
Client-SSL-Cipher: AES256-SHA
Client-SSL-Warning: Peer certificate not verified
X-Any-Daemon-Version: 0.14
X-LWP-Version: 5.833
X-XML-Compile-SOAP-Daemon-Version: 3.06
X-XML-Compile-SOAP-Version: 2.36
X-XML-Compile-Version: 1.33
X-XML-LibXML-Version: 2.0004

<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body><tns:AddService_CallbackResponse xmlns:tns="http://tempuri.org/"/></SOAP-ENV:Body></SOAP-ENV:Envelope>

