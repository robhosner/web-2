<?php


include_once('db.php');
include_once('db/ultra_promotional_plans.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('lib/transitions.php');
include_once('test/api/test_api_include.php');
require_once('Ultra/Lib/Util/Redis.php');


# Usage:
# php test/test_transitions.php append_action
# php test/test_transitions.php ultra_states
# php test/test_transitions.php ultra_plans
# php test/test_transitions.php run_db_transitions
# php test/test_transitions.php ultra_transitions
# php test/test_transitions.php change_state $STATE_NAME $TARGET_PLAN $CUSTOMER_ID $NOT_A_DRY_RUN
# php test/test_transitions.php suspend_account $CUSTOMER_ID
# php test/test_transitions.php cancel_account $CUSTOMER_ID
# php test/test_transitions.php cancel_prefunded_account $CUSTOMER_ID $REASON
# php test/test_transitions.php transition_customer_plan $CUSTOMER_ID $CURRENT_PLAN $TARGET_PLAN $TYPE
# php test/test_transitions.php reactivate_suspended_account $CUSTOMER_ID
# php test/test_transitions.php activate_customer $CUSTOMER_ID
# php test/test_transitions.php assign_test_orange_sim $CUSTOMER_ID
# php test/test_transitions.php take_transition_promo_prepare $CUSTOMER_ID $ICCID $ZIPCODE
# php test/test_transitions.php take_transition_promo_activate $CUSTOMER_ID $PROMO_ID
# php test/test_transitions.php internal_func_promotional_sim_init $PROMO_ID $ICCID
# php test/test_transitions.php internal_func_delayed_ensure_provisioning_or_activation $CUSTOMER_ID
# php test/test_transitions.php activate_promo_account $CUSTOMER_ID
# php test/test_transitions.php recover_zombie_action_and_transition $ACTION_UUID
# php test/test_transitions.php save_db_transition_to_redis $TRANSITION_UUID
# php test/test_transitions.php remove_db_transition_from_redis $TRANSITION_UUID
# php test/test_transitions.php reset_redis_transition $TRANSITION_UUID
# php test/test_transitions.php redis_db_transition_garbage_collector $ENV
# php test/test_transitions.php transition_garbage_collector
# php test/test_transitions.php reserve_ensure_msisdn
# php test/test_transitions.php internal_func_get_state_from_customer_id $CUSTOMER_ID
# php test/test_transitions.php reopen_action $ACTION_UUID
# php test/test_transitions.php suspend_account $CUSTOMER_ID
# php test/test_transitions.php suspend_account_by_msisdn $MSISDN
# php test/test_transitions.php is_transition_uuid_reserved_by_pid $UUID [$CUSTOMER_ID]
# php test/test_transitions.php reserve_transition_uuid_by_pid $UUID [$CUSTOMER_ID]
# php test/test_transitions.php unreserve_transition_uuid_by_pid $UUID [$CUSTOMER_ID]


teldata_change_db(); // connect to the DB


/*
$context     = array( 'customer_id' => 2566 );
$resolve_now = TRUE;
$targetPlan  = 'Change Plan Mid-Cycle L49 to L59';
#$targetPlan  = 'Monthly Renewal and Change Plan L59 to L49';
$state_name  = 'take transition';
$dry_run     = FALSE;
$max_path_depth = 1;

$result = change_state($context, $resolve_now, $targetPlan, $state_name, $dry_run, $max_path_depth);

print_r( $result );

exit;
*/


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_reserve_ensure_msisdn extends AbstractTestStrategy
{
  function test()
  {
    $msisdn = '11001001000';

    $redis = new \Ultra\Lib\Util\Redis;

    unreserve_ensure_msisdn_by_pid( $redis , $msisdn );

    $x = reserve_ensure_msisdn_by_pid( $redis , $msisdn );

    if ( $x ) { echo "OK\n"; } else { echo "KO\n"; }

    $x = reserve_ensure_msisdn_by_pid( $redis , $msisdn );

    if ( $x ) { echo "OK\n"; } else { echo "KO\n"; }

    unreserve_ensure_msisdn_by_pid( $redis , $msisdn );
  }
}


class Test_reopen_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $x = reopen_action( $argv[2] );

    print_r($x);
  }
}


class Test_reset_redis_transition extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $error = reset_redis_transition( $argv[2] );

    echo "$error\n\n";
  }
}


class Test_kill_db_transition_from_redis extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $redis = new \Ultra\Lib\Util\Redis;

    $htt_environment = $argv[2];
    $priority        = $argv[3];
    $transition_uuid = $argv[4];

    // remove transition from sorted set
    $redis->zrem( 'ultra/sortedset/transitions/'. $htt_environment . '/' . $priority , $transition_uuid );
  }
}

class Test_remove_db_transition_from_redis extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $errors = remove_db_transition_from_redis( $argv[2] );

    echo "$errors\n\n";
  }
}


class Test_internal_func_get_state_from_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state_from_customer_id( $argv[2] );

    print_r($r);
  }
}



class Test_recover_zombie_action_and_transition extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $errors = recover_zombie_action_and_transition( $argv[2] );

    echo "$errors\n\n";
  }
}


class Test_save_db_transition_to_redis extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $errors = save_db_transition_to_redis( $argv[2] );

    echo "$errors\n\n";
  }
}


class Test_transition_garbage_collector extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    transition_garbage_collector();
  }
}


class Test_redis_db_transition_garbage_collector extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $errors = redis_db_transition_garbage_collector( $argv[2] );

    echo "$errors\n\n";
  }
}


class Test_ultra_transitions extends AbstractTestStrategy
{
  function test()
  {
    $ultra_transitions = ultra_transitions();

    print_r($ultra_transitions);

    echo "\n\n";

    foreach($ultra_transitions as $id => $t)
    {
      echo "[ $id ] ".$t['source']['print']."  -->>  ".$t['dest']['print']."\n";
    }
  }
}


class Test_activate_promo_account extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $customer = get_customer_from_customer_id($customer_id);

    $context = array( 'customer_id' => $customer->CUSTOMER_ID );

    $state = internal_func_get_state_from_customer_id( $customer->CUSTOMER_ID );

    $result = activate_promo_account($state,$customer,$context);

    print_r( $result );
  }
}


class Test_internal_func_promotional_sim_init extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $promo_id = $argv[2];
    $iccid    = $argv[3];

    // load ULTRA.PROMOTIONAL_PLANS data
    $promo_data = get_ultra_promotional_plans_by_id( $promo_id );

    if ( ( ! $promo_data ) || ( ! is_array($promo_data)) || ( ! count($promo_data) ) )
      die("get_ultra_promotional_plans_by_id failed");

    $promo_data = $promo_data[0];

    dlog('',"%s",$promo_data);

    // extract and validate Zipcode List
    list( $zipcodes , $error ) = extractZipcodesFromString( $promo_data->ZIPRANGE );

    if ( $error )
      die("extractZipcodesFromString failed");

    $postal_code_id = array_rand($zipcodes, 1);

    $params = array(
      'ultra_promotional_data' => $promo_data,
      'postal_code'            => $zipcodes[$postal_code_id],
      'iccid'                  => $iccid
    );

    $result = internal_func_promotional_sim_init( $params );

    print_r( $result );
  }
}


/**
 * given a customer in 'Promo Unused' state, transition to 'Active' state
 *
 * requires $customer_id , $ultra_promotional_plans_id
 */
class Test_take_transition_promo_activate extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id                = $argv[2];
    $ultra_promotional_plans_id = $argv[3];

    $customer = get_customer_from_customer_id($customer_id);

    $return = ultra_promotional_plans_pre_activation( $customer_id , $ultra_promotional_plans_id , $customer->CURRENT_ICCID_FULL );

    print_r($return);

    if ( ! $return['success'] )
      exit;

    $transition_name = 'Activate Promo TWENTY_NINE';
    $resolve_now     = FALSE;
    $dry_run         = FALSE;
    $max_path_depth  = 1;
    $context         = array( 'customer_id' => $customer_id );

    $result_change_state = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    print_r($result_change_state);
  }
}


/**
 * given a customer in 'Neutral' state, transition to 'Promo Unused' state
 *
 * requires $customer_id , $iccid , $zipcode from command line
 */
class Test_take_transition_promo_prepare extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];
    $iccid       = $argv[3];
    $zipcode     = $argv[4];

    # fix zip code
    $sql = "UPDATE customers SET postal_code = '$zipcode' WHERE CUSTOMER_ID = $customer_id";

    if ( ! run_sql_and_check_result($sql) )
      die("DB error");

    # fix ICCID
    $iccid_19 = $iccid;
    $iccid_18 = $iccid;

    if (strlen($iccid) == 18)
    { $iccid_19 = luhnenize($iccid_18);   }
    else
    { $iccid_18 = substr($iccid_19,0,-1); }

    $sql = "UPDATE htt_customers_overlay_ultra SET current_iccid = '$iccid_18' , CURRENT_ICCID_FULL = '$iccid_19' WHERE CUSTOMER_ID = $customer_id";

    if ( ! run_sql_and_check_result($sql) )
      die("DB error");

    $transition_name = 'Promo Prepare TWENTY_NINE';
    $resolve_now     = FALSE;
    $dry_run         = FALSE;
    $max_path_depth  = 1;
    $context         = array( 'customer_id' => $customer_id );

    $result_change_state = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    print_r($result_change_state);
  }
}


class Test_assign_test_orange_sim extends AbstractTestStrategy
{
  function test()
  {
    // given a customer id in neutral state, it gets a test SIM and pretend it's ORANGE

    global $argv;

    $customer_id = $argv[2];

    if ( ! is_numeric( $customer_id ) )
      die("customer_id is not numeric : $customer_id");

    $r = internal_func_get_state(
      array(
        'states'        => NULL,
        'context'       => array( 'customer_id' => $customer_id )
      )
    );

    print_r($r);

    if ( $r['state'] != 'Neutral' )
      die('State is not Neutral : '.$r['state']."\n\n");

    $test_iccid = get_test_iccid( 'INVALID' );

    $iccid_19 = $test_iccid;
    $iccid_18 = $test_iccid;

    if (strlen($test_iccid) == 18)
    { $iccid_19 = luhnenize($iccid_18);   }
    else
    { $iccid_18 = substr($iccid_19,0,-1); }

    $sql = sprintf(
      "UPDATE htt_customers_overlay_ultra set current_iccid = '%s', CURRENT_ICCID_FULL = '%s' where CUSTOMER_ID = %d",
      $iccid_18,
      $iccid_19,
      $customer_id
    );

    dlog('',$sql);

    if ( ! run_sql_and_check_result($sql) )
      die("htt_customers_overlay_ultra update failed.\n\n");

    $sql = sprintf(
      "UPDATE htt_inventory_sim SET CUSTOMER_ID = %d , PRODUCT_TYPE = 'ORANGE' WHERE ICCID_NUMBER = '%s'",
      $customer_id,
      $iccid_18
    );

    dlog('',$sql);

    if ( ! run_sql_and_check_result($sql) )
      die("htt_inventory_sim update failed.\n\n");

    echo "\n\ncustomer id $customer_id is now associated with ICCID $iccid_18\n\n";
  }
}


class Test_activate_customer extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $customer = get_customer_from_customer_id($customer_id);

    $result_status = activate_customer($customer);

    print_r($result_status);
  }
}


class Test_reactivate_suspended_account extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];

    $customer = get_customer_from_customer_id($customer_id);

    $state = internal_func_get_state_from_customer_id($customer_id);

    $context = array(
      'customer_id' => $customer_id
    );

    $result_status = reactivate_suspended_account($state,$customer,$context);

    print_r($result_status);
  }
}


class Test_suspend_account extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_customer_from_customer_id($argv[2]);

    $context = array(
      'customer_id' => $customer->customer_id
    );

    $result = suspend_account( $customer , $context );

    print_r($result);
  }
}

class Test_suspend_account_by_msisdn extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_ultra_customer_from_msisdn($argv[2]);

    if (!$customer)
    {
      print PHP_EOL . 'Cannot find customer with MSISDN: ' . $argv[2] . PHP_EOL;
      return;
    }

    $context = array(
      'customer_id' => $customer->customer_id
    );

    $result = suspend_account( $customer , $context );

    print_r($result);
  }
}

# example: php test/test_transitions.php transition_customer_plan 2120 L29 L49 'Mid-Cycle'
class Test_transition_customer_plan extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id  = $argv[2];
    $current_plan = $argv[3];
    $targetPlan   = $argv[4];
    $type         = $argv[5];

    $customer = get_customer_from_customer_id($customer_id);

    $r = transition_customer_plan($customer,$current_plan,$targetPlan,$type);

    print_r($r);
  }
}


class Test_cancel_prefunded_account extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id  = $argv[2];
    $reason       = $argv[3];

    $customer = get_customer_from_customer_id($argv[2]);

    if ( !$customer )
      echo "customer not found for ID $customer_id\n";
    elseif ( $customer->plan_state != 'Pre-Funded' )
      echo "customer is not in Pre-Funded state\n";
    else
    {
      print_r($customer);

      echo "customer_id = $customer_id ; plan_state = ".$customer->plan_state." ; reason = $reason\n";

      $sql = htt_cancellation_reasons_insert_query(
        array(
          'customer_id' => $customer_id,
          'msisdn'      => $customer->current_mobile_number,
          'iccid'       => $customer->CURRENT_ICCID_FULL,
          'cos_id'      => $customer->COS_ID,
          'status'      => $customer->plan_state,
          'agent'       => $reason,
          'reason'      => $reason,
          'ever_active' => 0,
          'type'        => 'MANUAL'
        )
      );

      if ( !run_sql_and_check($sql) )
      {
        echo "DB error (htt_cancellation_reasons)\n"; exit;
      }

      $sql =
      "update htt_customers_overlay_ultra set plan_state = 'Cancelled' , current_iccid = NULL , CURRENT_ICCID_FULL = NULL , current_mobile_number = NULL where customer_id = ".$customer_id;

      if ( !run_sql_and_check($sql) )
      {
        echo "DB error (htt_customers_overlay_ultra)\n"; exit;
      }
    }

    exit;
  }
}

class Test_cancel_account extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_customer_from_customer_id($argv[2]);

    $context = array(
      'customer_id' => $customer->customer_id
    );

    $result = cancel_account( $customer , $context , NULL , 'agent' , 'notes' , 'source' );

    print_r($result);
  }
}


class Test_ultra_plans extends AbstractTestStrategy
{
  function test()
  {
    $ultra_plans = ultra_plans();

    print_r($ultra_plans);

    $json_ultra_plans = json_encode($ultra_plans);

    echo "\n$json_ultra_plans\n";
  }
}

class Test_customer_plans extends AbstractTestStrategy
{
  function test()
  {
    $customer_plans = customer_plans();

    print_r($customer_plans);

    $json_customer_plans = json_encode($customer_plans);

    echo "\n$json_customer_plans\n";
  }
}

class Test_internal_func_delayed_ensure_provisioning_or_activation extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $result = internal_func_delayed_ensure_provisioning_or_activation( $argv[2] );

    print_r($result);
  }
}


class Test_change_state extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $max_path_depth = 1; # obsolete/unused
    $dry_run        = ( isset($argv[5]) && ( $argv[5] == 'NOT_A_DRY_RUN' ) ) ? FALSE : TRUE ;
    $state_name     = $argv[2];
    $targetPlan     = $argv[3];
    $resolve_now    = FALSE;
    $context        = array( 'customer_id' => $argv[4] );

echo "context = ";
    print_r( $context );

echo "state_name = $state_name\n";
echo "targetPlan = $targetPlan\n";

    $result = change_state($context, $resolve_now, $targetPlan, $state_name, $dry_run, $max_path_depth);

    print_r($result);
  }
}


class Test_run_db_transitions extends AbstractTestStrategy
{
  function test()
  {
    $r = run_db_transitions();

    print_r($r);
  }
}


class Test_append_action extends AbstractTestStrategy
{
  function test()
  {

    $context = array('customer_id' => 1);
    $action_type = 'test';
    $action_name = 'TEST';
    $action_transaction = ''; # ?
    $action_seq = 0;
    $params     = array( 'a'=>1 , 'b'=>2 );
    $pending    = FALSE;

    $append_return = append_action($context,
                                   array('type' => $action_type,
                                         'name' => $action_name,
                                         'transaction' => $action_transaction,
                                         'seq' => $action_seq),
                                   $params,
                                   $pending);

    print_r($append_return);

#Array
#(
#    [success] => 1
#    [aborted] => 
#    [errors] => Array
#        (
#        )
#    [log] => Array
#        (
#        )
#    [transitions] => Array
#        (
#            [0] => {2068F748-34A1-4FE1-5F8A-C4BD9F1FB3C3}
#        )
#    [actions] => Array
#        (
#            [0] => {3C1347C6-508F-1C15-1D7C-92A18F1B999F}
#        )
#)

  }
}

class Test_ultra_states extends AbstractTestStrategy
{
  function test()
  {
    $ultra_states = ultra_states();

    print_r($ultra_states);

    $json_ultra_states = json_encode($ultra_states);

    echo "\n$json_ultra_states\n";
  }
}


class Test_is_transition_uuid_reserved_by_pid extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    $transition_uuid = empty($argv[2]) ? NULL : $argv[2];
    $customer_id = empty($argv[3]) ? NULL : $argv[3];
    echo "transition UUID: $transition_uuid, customer ID: $customer_id \n";
    $r = is_transition_uuid_reserved_by_pid($transition_uuid, NULL, $customer_id) ? 'TRUE' : 'FALSE';
    echo "result: $r \n";
  }
}


class Test_reserve_transition_uuid_by_pid extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    $transition_uuid = empty($argv[2]) ? NULL : $argv[2];
    $customer_id = empty($argv[3]) ? NULL : $argv[3];
    echo "transition UUID: $transition_uuid, customer ID: $customer_id \n";
    $r = reserve_transition_uuid_by_pid($transition_uuid, NULL, $customer_id) ? 'TRUE' : 'FALSE';
    echo "result: $r \n";
  }
}


class Test_unreserve_transition_uuid_by_pid extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    $transition_uuid = empty($argv[2]) ? NULL : $argv[2];
    $customer_id = empty($argv[3]) ? NULL : $argv[3];
    echo "transition UUID: $transition_uuid, customer ID: $customer_id \n";
    $r = unreserve_transition_uuid_by_pid($transition_uuid, NULL, $customer_id) ? 'TRUE' : 'FALSE';
    echo "result: $r \n";
  }
}

# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


/*
php test/test_transitions.php take_transition_promo_prepare 2365 890126084210224238 12345
php test/test_transitions.php take_transition_promo_activate 2365 1
php test/test_transitions.php redis_db_transition_garbage_collector ultra_api_prod_internal
*/

?>
