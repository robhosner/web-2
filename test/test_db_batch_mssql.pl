use strict;
use Data::Dumper;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::Util::Config;
use Sys::Hostname;

use POSIX qw(strftime);

use Devel::Size qw(total_size);

my $config = Ultra::Lib::Util::Config->new();

my $db_type = 'mw/';

my $dbName = $config->find_credential($db_type.'db/name');
my $dbHost = $config->find_credential($db_type.'db/host');
my $dbUser = $config->find_credential($db_type.'db/user');
my $dbPasswd = $config->find_credential_from_file($db_type.'db/pfile');

my $connect_string = "DBI:Sybase:database=".$dbName.";server=".$dbHost;

my $dbh = DBI->connect($connect_string,
                       $dbUser,
                       $dbPasswd,
                       {'RaiseError' => 1});

if ( ! defined $dbh )
{
  die "Couldn't connect to DB $dbName on host $dbHost with user $dbUser : $!";
}

my $sth;

my $query = 'INSERT INTO SOAP_LOG     ( DATA_XML,COMMAND,SESSION_ID,TYPE_ID,ENV,TAG,MSISDN,ICCID,RESULTCODE     )     VALUES     ( ?,?,?,?,?,?,?,?,?     ); SELECT SCOPE_IDENTITY() LAST_SOAP_LOG_ID';

my $bindingRef = [1,1,1,1,1,1,1,1,1];

my $executeSuccess;

eval
{
  $sth = $dbh->prepare($query);

  $sth->execute( @$bindingRef );
};

if ( $@ )
{
  die Dumper($@);
}

my $rows = [];

print Dumper($sth);

print $DBI::errstr . " - " . $dbh->err();

if ( $sth )
{
  while( my $row = $sth->fetchrow_hashref() )
  {
    push(@$rows, $row);
  }
}
else
{
  die $DBI::errstr . " - " . $dbh->err();
}

print Dumper($rows);

__END__

