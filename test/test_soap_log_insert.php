<?php

/*
Example:

%> php test/test_soap_log_insert.php 1 'SQL-PRD-01.ultra.local'
*/

require_once('db.php');

$iterations = $argv[1];
$host       = $argv[2];

while ( $iterations-- )
{
$params['data_xml'] = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://www.sigvalue.com/acc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><ns1:SendSMS><ns1:UserData><ns1:senderId>MVNEACC</ns1:senderId><ns1:channelId>ULTRA</ns1:channelId><ns1:timeStamp>2015-03-17T10:55:10</ns1:timeStamp></ns1:UserData><ns1:qualityOfService>801</ns1:qualityOfService><ns1:preferredLanguage>en</ns1:preferredLanguage><ns1:smsText>Test '.time().' &amp&#59; </ns1:smsText><ns1:SubscriberSMSList><ns1:SubscriberSMS><ns1:MSISDN>8323985363</ns1:MSISDN><ns1:Language>en</ns1:Language><ns1:Text xsi:nil="true"/></ns1:SubscriberSMS></ns1:SubscriberSMSList></ns1:SendSMS></SOAP-ENV:Body></SOAP-ENV:Envelope>';
$params['soap_date'] = '2000-05-11 15:41:55.223';
$params['type_id'] = 1;
$params['msisdn'] = time();
$params['iccid'] = NULL;
$params['tag'] = '{TEST-'.time().'}';
$params['session_id'] = '';
$params['command'] = 'SendSMS';
$params['env'] = 'test';

$query = soap_log_insert_query($params);

$connection = \Ultra\Lib\DB\ultra_acc_connect( $host );

#$connection = \Ultra\Lib\DB\mw_db_connect();

if ( ! $connection)
  die('Cannot connect to ultra_acc db');

if ( ! is_mssql_successful(logged_mssql_query($query)))
  die('soap_log insert failed');

echo "Success\n";

sleep(1);
}

