<?php

// php test/create_test_htt_inventory_pins.php 1234 12345678

include 'db.php';

if (!isset($argv[1]) || !is_numeric($argv[1]) || strlen($argv[1]) != 4)
{
  echo 'Prefix ($argv[1]) in wrong format (len == 4 && is_numeric)' . PHP_EOL;
  exit;
}

if (!isset($argv[2]) || !is_numeric($argv[2]) || strlen($argv[2]) != 8)
{
  echo 'Prefix ($argv[2]) in wrong format (len == 8 && is_numeric)' . PHP_EOL;
  exit;
}

$prefix = $argv[1];
$salt =   $argv[2];

teldata_change_db();

for ($i = 0; $i < 100; $i++)
{
  $serial = $prefix . '11' . $salt . (1000 + $i);
  $params = array(
    'iccid' => $serial,
    'type'  => '7_11',
    'created_by' => 'jsteinmetz',
    'expires_date' => 'DATEADD(mm, +1, GETUTCDATE())',
    'stored_value' => '19',
    'start_date' => 'GETUTCDATE()'
  );

  $query = ultra_incomm_inventory_sim_insert($params);
  logged_mssql_query($query);
}

?>