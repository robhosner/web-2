<?php


include_once('db.php');
include_once('lib/portin/functions.php');


date_default_timezone_set("America/Los_Angeles");


# Usage:
# php test/portin/test_functions.php log_port_request


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_log_port_request extends AbstractTestStrategy
{
  function test()
  {
    teldata_change_db(); // connect to the DB

    $customer_id  = 123;
    $msisdn       = 1001001000;
    $ocn          = 42;
    $result       = 'testing';
    $carrier_name = 'Afghan Telecom';

    $r = log_port_request($customer_id, $msisdn, $ocn, $result, $carrier_name);

    print_r($r);

  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();

exit;


?>
