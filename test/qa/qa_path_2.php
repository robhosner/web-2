<?php

/*
[ Neutral ] => [ Provisioned ] => [ Active ]
*/

# Usage:
# %> php test/qa/qa_path_2.php $MODE $PLAN $LOAD_PAYMENT [EN|ES] $ZIP $ICCID


include_once('classes/Result.php');
include_once('test/api/test_api_include.php');
include_once('test/qa/test_classes.php');
include_once('web.php');


$mode = 'default';

if ( isset($argv[1]) && $argv[1] )
  $mode = $argv[1];

# default params
$plan        = 'L29';
$loadPayment = 'NONE';

if ( isset($argv[2]) )
{
  $allowed_plans = array("L19","L29","L39","L49","L59");

  if ( in_array( $argv[2] , $allowed_plans ) )
  {
    $plan = $argv[2];
  }
  else
  {
    die($argv[2]." is not a valid plan");
  }
}

if ( isset($argv[3]) )
{
  $allowed_load_payments = array("NONE"=>1,"CCARD"=>1,"PINS"=>1);

  if ( isset( $allowed_load_payments[ $argv[3] ] ) )
  {
    $loadPayment = $argv[3];
  }
  else
  {
    die($argv[3]." is not a valid load payment");
  }
}

if ( isset($argv[4]) )
{
  $allowed_languages = array("EN","ES", "ZH");

  if ( ! in_array( $argv[4] , $allowed_languages ) )
    die($argv[4]." is not a valid language");
}

$qa_tester = new QATester($mode);

$qa_tester->data_array['targetPlan']  = $plan;
$qa_tester->data_array['loadPayment'] = 'NONE';
$qa_tester->data_array['preferred_language'] = $argv[4];

if ( array_key_exists ( 5 , $argv ) && $argv[5] )
  $qa_tester->data_array['customerZip'] = $argv[5];

if ( array_key_exists ( 6 , $argv ) && $argv[6] )
  $qa_tester->data_array['test_iccid'] = $argv[6];

$qa_tester->load_tests(
  array(

    /* provisioning (request) */
    new Test_provisioning__requestProvisionNewCustomerAsync($mode),

    /* provisioning (verify) */
    new Test_provisioning__verifyProvisionNewCustomerAsync($mode),

    /* customercare__AddCourtesyCash */
    new Test_customercare__AddCourtesyCash($mode)

  )
);

while( $qa_tester->has_next() && $qa_tester->has_no_errors() )
  $qa_tester->test_next();

/*
Example:
php test/qa/qa_path_2.php '' L39 CCARD ES 12345 1234512345123451234
*/

?>
