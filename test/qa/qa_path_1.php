<?php

/*
[ Neutral ] => [ Pre-Funded ] => [ Active ]
*/

include_once('classes/Result.php');
include_once('test/api/test_api_include.php');
include_once('test/qa/test_classes.php');
include_once('web.php');


$mode = 'default';

if ( isset($argv[1]) )
{
  $mode = $argv[1];
}

$plan = 'L29';

if ( isset($argv[2]) )
{
  $allowed_plans = array("L19","L29","L39","L49","L59");

  if ( in_array( $argv[2] , $allowed_plans ) )
  {
    $plan = $argv[2];
  }
  else
  {
    die($argv[2]." is not a valid plan");
  }
}

if ( isset($argv[3]) )
{
  $allowed_languages = array("EN","ES");

  if ( ! in_array( $argv[3] , $allowed_languages ) )
  {
    die($argv[3]." is not a valid language");
  }
}

$qa_tester = new QATester($mode);

$qa_tester->data_array['targetPlan'] = $plan;

$qa_tester->data_array['preferred_language'] = $argv[3];

$qa_tester->load_tests(
  array(

    /* create new web account with portal__CreateWebUser */
    new Test_portal__CreateWebUser($mode),

    /* check the state of the customer with internal__GetState */
    new Test_internal__GetState($mode),

    /* transition to Prefunded with portal__PrepayPlan */
    new Test_portal__PrepayPlanMimic($mode),

    /* transition to Active with provisioning__requestActivateShippedNewCustomerAsync */
    new Test_provisioning__requestActivateShippedNewCustomerAsync($mode),

    /* verify previous transition to Active with Test_provisioning__verifyActivateShippedNewCustomerAsync */
    new Test_provisioning__verifyActivateShippedNewCustomerAsync($mode)

  )
);

while( $qa_tester->has_next() && $qa_tester->has_no_errors() )
{
  $qa_tester->test_next();
}


/*

usage:

to use an invalid ICCID:
  php test/qa/qa_path_1.php INVALID L[12345]9 [EN|ES]

to use a valid ICCID:
  php test/qa/qa_path_1.php default L[12345]9 [EN|ES]

to use a given ICCID:
  php test/qa/qa_path_1.php $ICCID L[12345]9 [EN|ES]

*/


?>
