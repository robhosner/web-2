<?php

/*
[ Active ] => [ Suspend ] => [ Active ]
*/

# Usage:
# %> php test/qa/qa_path_3.php $CUSTOMER_ID


include_once('classes/Result.php');
include_once('db/customers.php');
include_once('test/api/test_api_include.php');
include_once('test/qa/test_classes.php');
include_once('web.php');


if ( ! isset($argv[1]) ) { die("Please provide customer_id"); }


$mode = 'default';

teldata_change_db();

$plan_data = get_customer_plan($argv[1]);

$qa_tester = new QATester($mode);

$qa_tester->data_array['customer_id'] = $argv[1];
$qa_tester->data_array['targetPlan']  = $plan_data['plan'];

$qa_tester->load_tests(
  array(

    /* validate state Active */
    new Test_internal__GetState_Active($mode),

    /* suspend */
    new Test_customer_suspend($mode),

    /* validate state Active */
    new Test_internal__GetState_Suspended($mode),

    /* provide cash */
    new Test_customercare__AddCourtesyCash($mode),

    /* validate state Active */
    new Test_internal__GetState_Active($mode),

    /* activate */
#    new Test_customer_activate($mode)

  )
);

while( $qa_tester->has_next() && $qa_tester->has_no_errors() )
{
  $qa_tester->test_next();
}

?>
