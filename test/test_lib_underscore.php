<?php

include_once('lib/underscore/underscore.php');

/*
http://brianhaveri.github.io/Underscore.php
*/

/*
__::map(array(1, 2, 3), function($num) { return $num * 3; }); // array(3, 6, 9)

__::map(array('one'=>1, 'two'=>2, 'three'=>3), function($num, $key) {
  return $num * 3;
}); // array(3, 6, 9);
*/

$x = __::uniq(array(1,1,2,2,3,3));

print_r($x);

$underscoreObject = new \__ ;

$x = $underscoreObject->uniq(array(1,1,2,2,3,3));

print_r($x);

exit;

$x = $underscoreObject->map(array(1, 2, 3), function($num) { return $num * 3; }); // array(3, 6, 9)

print_r($x);

$x = $underscoreObject->map(array('one'=>1, 'two'=>2, 'three'=>3), function($num, $key) {
  return $num * 3;
}); // array(3, 6, 9);

print_r($x);

