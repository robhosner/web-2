<?php

/**
 * test file for cryptography custom PHP extension
 */


if ( ! function_exists('IX_aes_256_gcm_encrypt'))
  echo "extension 'cryptography' is missing\n";
else
{
  echo "extension 'cryptography' is loaded successfully\n";

  // prepare static data to validate encryption
  $unencrypted = array('e=1458565745');
  $aad = implode("\n", $unencrypted) . "\n"; // trailing new line is required at the end of AAD

  $encrypted = array('fruits=cranberries');
  $plain = implode("\n", $encrypted);

  $secret = '21GunSalute';

  // generate initial vector (aka nonce)
  for ($i = 0, $vector = ''; $i < 12; $i++)
    $vector .= chr(rand(1, 255)); // avoid zero values since they may be interpreted as premature end of string

  // encrypt
  echo "calling function IX_aes_256_gcm_encrypt:\n";
  $result = IX_aes_256_gcm_encrypt($plain, $aad, $secret, $vector);
  foreach ($result as $name => $value)
    echo sprintf("  %s => '%s', length: %d \n", $name, $value, strlen($value));
  echo PHP_EOL;

  // how to generate IXNX REST secutiry token
  $token = $aad; // start with unencrypted additional application data string
  $token .= "t={$result['tag']}\n"; // add encryption tag
  $token .= "i={$result['vector']}\n"; // add encryption token
  $token .= "c={$result['ciphertext']}\n";  // add ciphertext
  echo "token: '$token'\n";

  // Milo's accepted token
#  $secret = '21Gun';
#  $token = "e=1467531574\nn=juser\nt=xGtLPBKrNVAHrZuLg_QsOg\ni=QHReAOvqSFtwlgs0\nc=30l3aQrGVNeHeNW84eSCVzDNkvAGR4ynj2yqg6VbShB43k_DFph_UfkY";

  // Vitaly's accepted token
#  $secret = '21GunSalute';
#  $token = "e=1458565745\nt=h7qR6xVCHnAK93q8DthOHA\ni=4IjxgUUO5qWnCtMV\nc=pZtdsXrQpa9jUg4oBbw";

  // break up token into parts
  $parts = explode("\n", $token);
  foreach ($parts as $part)
    if ( ! empty($part))
    {
      list($name, $value) = explode('=', $part);
      $fields[$name] = $value;
    }
  print_r($fields);

  $aad = NULL; $ciphertext = NULL; $vector = NULL; $tag = NULL;
  foreach ($fields as $name => $value)
    switch ($name)
    {
      case 'c': // ciphertext
        $ciphertext = $value;
        break;
      case 'i': // initial vector
        $vector = $value;
        break;
      case 't': // tag
        $tag = $value;
        break;
      default: // clear text
        $aad .= "{$name}={$value}\n";
    }
  echo "aad: '$aad', cipher: '$ciphertext', vector: '$vector', tag: '$tag'\n";

  // decrypt and validate
  echo "calling function IX_aes_256_gcm_decrypt:\n";
  $result = IX_aes_256_gcm_decrypt($ciphertext, $aad, $secret, $vector, $tag);
  foreach ($result as $name => $value)
    echo sprintf("  %s => '%s', length: %d\n", $name, $value, strlen($value));

}
