/**
 * cryptography.h
 *
 * a PHP extension for access to low level cryptography functions provided by openssh library;
 * the majority of functions in openssh library are exposed via the standard PHP extension
 * OpenSSH as documented in http://php.net/manual/en/book.openssl.php;
 * however as of Feb 2016 some are missing, specifically AES Galois/Counter Mode cipher,
 * due to different interface requirements (see https://bugs.php.net/bug.php?id=67304);
 * this custom extension provides access to this missing functionality
 * @see https://wiki.openssl.org/index.php/EVP_Authenticated_Encryption_and_Decryption
 * @project PRIMO-1347
 * @author VYT 2016
 */

#include <stdio.h>
#include <string.h> 
#include <openssl/evp.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif
#include "php.h"

#define PHP_CRYPTOGRAPHY_EXTNAME "cryptography"
#define PHP_CRYPTOGRAPHY_VERSION "1.00"

extern zend_module_entry cryptography_module_entry;
#define phpext_cryptography_ptr &cryptography_module_entry

// this extension exposes the following functions
PHP_FUNCTION(IX_aes_256_gcm_encrypt);
PHP_FUNCTION(IX_aes_256_gcm_decrypt);

static zend_function_entry cryptography_functions[] =
{
  PHP_FE(IX_aes_256_gcm_encrypt, NULL)
  PHP_FE(IX_aes_256_gcm_decrypt, NULL)
  {NULL, NULL, NULL}
};

zend_module_entry cryptography_module_entry =
{
#if ZEND_MODULE_API_NO >= 20010901
  STANDARD_MODULE_HEADER,
#endif
  PHP_CRYPTOGRAPHY_EXTNAME,   // module name
  cryptography_functions,     // module functions
  NULL,                       // name of the MINIT function or NULL if not applicable
  NULL,                       // name of the MSHUTDOWN function or NULL if not applicable
  NULL,                       // name of the RINIT function or NULL if not applicable
  NULL,                       // name of the RSHUTDOWN function or NULL if not applicable
  NULL,                       // name of the MINFO function or NULL if not applicable
#if ZEND_MODULE_API_NO >= 20010901
  PHP_CRYPTOGRAPHY_VERSION,   // module version
#endif
  STANDARD_MODULE_PROPERTIES
};

ZEND_GET_MODULE(cryptography)

// encryption constants
#define ENCRYPTION_KEY_LENGTH 32 // max length of encryption key
#define INITIAL_VECTOR_LENGTH 12 // initial vector length
#define MSG_AUTH_TAG_LENGTH   16 // message authentication tag length

