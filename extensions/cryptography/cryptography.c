/*
 * cryptography.c
 *
 * see cryptography.h for info
 * thoughts: http://stackoverflow.com/questions/13592037/php-extension-call-existing-php-function
 */

#include "cryptography.h"

static unsigned int base64todata[] = {
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,62,0,62,0,63, //convert - same as +
  52,53,54,55,56,57,58,59,
  60,61,0,0,0,0,0,0,

  0,0,1,2,3,4,5,6,
  7,8,9,10,11,12,13,14,
  15,16,17,18,19,20,21,22,
  23,24,25,0,0,0,0,63, //convert _ same as /
  0,26,27,28,29,30,31,32,
  33,34,35,36,37,38,39,40,
  41,42,43,44,45,46,47,48,
  49,50,51,0,0,0,0,0,

  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,

  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0
};


// VYT: base64 encoding - Ingate GW implements proprietary encoding table
static char encoding_table[] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
};

static char* decoding_table = NULL; // we will fill in it when needed

void fill_in_decoding_table()
{
  int i;

  decoding_table = malloc(256);
  if ( ! decoding_table)
    return;
  memset(decoding_table, 0, 256);
  for (i = 0; i < 0x40; i++)
    decoding_table[encoding_table[i]] = (char)i;
}

static size_t mod_table[] = { 0, 2, 1 };


/* wrapper for IX_Decode64Smart which adds possibly missing padding and zero-temrination */
static char str_p_at(const char *str_p, size_t len, size_t padlen, size_t idx)
{
   if (idx>=(len+padlen))
       return 0;
   else if (idx>=len)
       return '=';
   else return str_p[idx];
}

/* Can handle non-zero-terminated input and input not padded with = */
int IX_Decode64Smart(unsigned char *dst_p, unsigned int *dlen_p, const char *str_p, size_t len)
{
 unsigned int tmp;
 int i;
 unsigned char *dst_start_p = dst_p;
 unsigned char *dst_last_p = dst_p + *dlen_p -1;

 if ( ! (dlen_p && dst_p && str_p))
  return 0;

 size_t padlen = 4-(len%4);
 if (padlen==4)
   padlen=0;

 if (*str_p == '\0' )
   return 0;

 dst_start_p = dst_p;
 size_t idx = 0;
 while (str_p_at(str_p, len, padlen, idx))
   {
     tmp = 0;
     for ( i = 0; i < 4; i++ )
     {
       tmp |= base64todata[(unsigned char)str_p_at(str_p, len, padlen, idx)] << 6*(3-i);
       idx++;
     }
     if (dst_p<=dst_last_p)
       *dst_p = tmp >> 16;
     dst_p++;
     if (dst_p<=dst_last_p)
       *dst_p = tmp >> 8;
     dst_p++;
     if (dst_p<=dst_last_p)
       *dst_p = tmp;
     dst_p++;
   }
 if ( str_p_at(str_p, len, padlen, idx-1) == '=' )
   {
     if ( str_p_at(str_p, len, padlen, idx-2) == '=' )
   dst_p -= 2;
     else
   dst_p -= 1;
   }
 *dlen_p = dst_p - dst_start_p;
 if ((dst_p-1)  > dst_last_p)
   return 0;

 return 1;
}


/**
 * base64_encode
 *
 * WARNING: allocates memory and returns its pointer
 */
char * base64_encode(const unsigned char* data, size_t input_length, size_t* output_length)
{
  *output_length = 4 * ((input_length + 2) / 3);
  char * encoded_data = (char*)malloc(*output_length + 1);
  if (encoded_data == NULL)
    return NULL;

  size_t i, j;
  for (i = 0, j = 0; i < input_length;)
  {
    unsigned int octet_a = i < input_length ? data[i++] : 0;
    unsigned int octet_b = i < input_length ? data[i++] : 0;
    unsigned int octet_c = i < input_length ? data[i++] : 0;
    unsigned int triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
    encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
    encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
    encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
    encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
  }

  for (i = 0; i < mod_table[input_length % 3]; i++)
    encoded_data[ * output_length - 1 - i] = 0;
  encoded_data[ * output_length] = 0;

  return encoded_data;
}


/**
 * base64_decode
 *
 * decode base64 data stream while working around the flaw in vendor-provided base64_encode function:
 * the latter does not pad data to 4 bytes as typically required
 * WARNING: allocates memory and returns its pointer
 */
unsigned char * base64_decode(const char* input, size_t input_length, size_t* output_length)
{
  int i, j;
  unsigned char* decoded_data;

  // fill in decoding table if not done already
  if ( ! decoding_table)
    fill_in_decoding_table();

  if (input_length % 4 != 0)
    input_length += input_length % 4;

  *output_length = input_length / 4 * 3;
  if (input[input_length - 1] == '=')
    (*output_length)--;
  if (input[input_length - 2] == '=')
    (*output_length)--;

  decoded_data = (unsigned char*)malloc(*output_length);
  if ( ! decoded_data)
    return NULL;
  memset(decoded_data, 0, *output_length);

  for (i = 0, j = 0; i < input_length;)
  {
    unsigned int sextet_a = input[i] == '=' ? 0 & i++ : decoding_table[(int)input[i++]];
    unsigned int sextet_b = input[i] == '=' ? 0 & i++ : decoding_table[(int)input[i++]];
    unsigned int sextet_c = input[i] == '=' ? 0 & i++ : decoding_table[(int)input[i++]];
    unsigned int sextet_d = input[i] == '=' ? 0 & i++ : decoding_table[(int)input[i++]];

    unsigned int triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

    if (j < *output_length)
      decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
    if (j < *output_length)
      decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
    if (j < *output_length)
      decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
  }

  decoded_data[*output_length] = '\0';
  return decoded_data;
}


/**
 * add_base64_encoded
 *
 * encode string as base64 and add to output associative array
 */
void add_base64_encoded(char* data, size_t dataLen, const char* name, zval* return_value)
{
  char* encoded;
  size_t length;
  char error[128];

  if ( ! data || ! dataLen)
    return;

  if (encoded = base64_encode(data, dataLen, &length))
  {
    add_assoc_string(return_value, name, encoded, 1);
    free(encoded);
  }
  else
  {
    snprintf(error, sizeof(error), "failed %s base64 encoding", name);
    add_assoc_string(return_value, "error", error, 1);
  }
}


/**
 * IX_aes_256_gcm_encrypt
 *
 * a wrapper for OpenSSL AES 256 bit cipher using Galois/Counter Mode
 */
PHP_FUNCTION(IX_aes_256_gcm_encrypt)
{
  // input parameters
  unsigned char* plain;   // plain text to encrypt
  int plainLen;           // its length
  unsigned char* aad;     // plain text additional application data
  int aadLen;             // its legnth
  unsigned char* key;     // shared secret key
  int keyLen;             // its length
  unsigned char* vector;  // initial vector
  int vectorLen;          // its length

  // cipher variables and output
  EVP_CIPHER_CTX* ctx;    // cipher object
  int length;             // temporary
  unsigned char* secret;  // encryption key padded to ENCRYPTION_KEY_LENGTH bytes with zeroes

  // output values
  unsigned char* cipher;  // output ciphertext
  int cipherLen;          // its length
  unsigned char* tag;     // output tag
  int tagLen;             // its length

  // initialize result array
  array_init(return_value);
  add_assoc_null(return_value, "ciphertext");
  add_assoc_null(return_value, "tag");
  add_assoc_null(return_value, "vector");
  add_assoc_null(return_value, "error");

  // collect parameters: we expect four strings
  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ssss", &plain, &plainLen, &aad, &aadLen, &key, &keyLen, &vector, &vectorLen) == FAILURE)
  {
    add_assoc_string(return_value, "error", "missing one or more required function parameter", 1);
    return;
  }

  // validate key length
  if (keyLen > ENCRYPTION_KEY_LENGTH)
  {
    add_assoc_string(return_value, "error", "invalid encryption key length", 1);
    return;
  }

  // copy key into zero padded buffer
  if ( ! (secret = calloc(2, ENCRYPTION_KEY_LENGTH)))
  {
    add_assoc_string(return_value, "error", "cannot allocate secret memory", 1);
    return;
  }
  strcpy(secret, key);

  // validate vector: must be exact length
  if (vectorLen != INITIAL_VECTOR_LENGTH)
  {
    add_assoc_string(return_value, "error", "invalid initial vector length", 1);
    return;
  }

  // allocate ciphertext storage
  cipher = (char*) malloc(plainLen + 64); // ciphertext length is the same as clear text but add 64 bytes for sanity
  if ( ! cipher)
  {
    add_assoc_string(return_value, "error", "failed to allocate ciphertext memory", 1);
    return;
  }
  
  // allocate tag storage
  tag = (char*) malloc(MSG_AUTH_TAG_LENGTH * 2);
  if ( ! tag)
  {
    add_assoc_string(return_value, "error", "failed to allocate tag memory", 1);
    return;
  }

  // create cipher context
  if ( ! (ctx = EVP_CIPHER_CTX_new()))
  {
    add_assoc_string(return_value, "error", "failed to create encryption cipher", 1);
    return;
  }

  // initialize encryption operation
  if ( ! EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, secret, vector))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to initialize AES 256 GCM cipher", 1);
    return;
  }

  // authorize additional application data without encryption
  if ( ! EVP_EncryptUpdate(ctx, NULL, &length, aad, aadLen))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to authorize additional application data", 1);
    return;
  }

  // encrypt message text
  if ( ! EVP_EncryptUpdate(ctx, cipher, &length, plain, plainLen))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to encrypt message", 1);
    return;
  }
  if (length >= plainLen + 64)
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "buffer overrun during encryption", 1);
    return;
  }
  cipherLen = length;

  // finalise encryption and write out remaining data
  if ( ! EVP_EncryptFinal_ex(ctx, cipher + cipherLen, &length))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to finalize encryption", 1);
    return;
  }
  if (cipherLen + length >= plainLen + 64)
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "buffer overrun during finalization", 1);
    return;
  }
  cipherLen += length;

  // get encryption tag
  if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, MSG_AUTH_TAG_LENGTH, tag))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to retrieve tag", 1);
    return;
  }

  // encode and add output values
  add_base64_encoded(cipher, cipherLen, "ciphertext", return_value);
  add_base64_encoded(tag, MSG_AUTH_TAG_LENGTH, "tag", return_value);
  add_base64_encoded(vector, INITIAL_VECTOR_LENGTH, "vector", return_value);

  // clean up
  EVP_CIPHER_CTX_free(ctx);
  free(secret);
  free(cipher);
  free(tag);
}


/**
 * IX_aes_256_gcm_decrypt
 *
 * a wrapper for OpenSSL AES 256 bit cipher using Galois/Counter Mode
 */
PHP_FUNCTION(IX_aes_256_gcm_decrypt)
{
  // function input parameters
  unsigned char* cipher64;// ciphertext
  int cipher64Len;        // its length
  unsigned char* aad;     // additional application data
  int aadLen;             // its length
  unsigned char* key;     // encryption key
  int keyLen;             // its length
  unsigned char* vector64;// initial vector
  int vector64Len;        // its length
  unsigned char* tag64;   // input tag
  int tag64Len;           // its length

  // cipher variables
  EVP_CIPHER_CTX* ctx;    // cipher object
  int length;             // temporary
  unsigned char* cipher;  // ciphertext decoded
  unsigned int cipherLen; // its length
  unsigned char* vector;  // initial vector decoded
  unsigned int vectorLen; // its length
  unsigned char* tag;     // input tag decoded
  unsigned int tagLen;    // its length
  unsigned char* secret;  // encryption key padded to ENCRYPTION_KEY_LENGTH bytes with zeroes

  // output values
  unsigned char* plain;   // output decrypted text
  int plainLen;           // its length
  int plainSize;          // size of output text buffer
  
  // initialize result array
  array_init(return_value);
  add_assoc_null(return_value, "plain");
  add_assoc_null(return_value, "error");

  // collect parameters: we expect five strings
  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sssss", &cipher64, &cipher64Len, &aad, &aadLen, &key, &keyLen, &vector64, &vector64Len, &tag64, &tag64Len) == FAILURE)
  {
    add_assoc_string(return_value, "error", "missing one or more required function parameter", 1);
    return;
  }

  // allocate and decode base 64 ciphertext
  cipherLen = cipher64Len;
  if ( ! (cipher = malloc(cipherLen)))
  {
    add_assoc_string(return_value, "error", "failed to allocated ciphertext decoding memory", 1);
    return;
  }
  if ( ! IX_Decode64Smart(cipher, &cipherLen, cipher64, cipher64Len))
  {
    add_assoc_string(return_value, "error", "failed to decode ciphertext", 1);
    return;
  }

  // allocate and decode base 64 vector 
  vectorLen = INITIAL_VECTOR_LENGTH + 2;
  if ( ! (vector = malloc(vectorLen)))
  {
    add_assoc_string(return_value, "error", "failed to allocate vector memory", 1);
    return;
  }
  if ( ! IX_Decode64Smart(vector, &vectorLen, vector64, vector64Len))
  {
    add_assoc_string(return_value, "error", "failed to decode vector", 1);
    return;
  }

  // validate vector: must be exact length
  if (vectorLen != INITIAL_VECTOR_LENGTH)
  {
    add_assoc_string(return_value, "error", "invalid initial vector length", 1);
    return;
  }

  // allocated and decode base 64 tag
  tagLen = MSG_AUTH_TAG_LENGTH + 2;
  if ( ! (tag = malloc(tagLen)))
  {
    add_assoc_string(return_value, "error", "failed to allocate tag memory", 1);
    return;
  }
  if ( ! IX_Decode64Smart(tag, &tagLen, tag64, tag64Len))
  {
    add_assoc_string(return_value, "error", "failed to decode tag", 1);
    return;
  }

  // validate tag: must be exact length
  if (tagLen != MSG_AUTH_TAG_LENGTH)
  {
    add_assoc_string(return_value, "error", "invalid message authentication tag length", 1);
    return;
  }

  // allocate output storage
  plainSize = cipherLen + 2;
  if ( ! (plain = malloc(plainSize)))
  {
    add_assoc_string(return_value, "error", "failed to allocate plain text memory", 1);
    return;
  }
  memset(plain, 0, plainSize);

  // create cipher context
  if ( ! (ctx = EVP_CIPHER_CTX_new()))
  {
    add_assoc_string(return_value, "error", "failed to create encryption cipher", 1);
    return;
  }

  // copy key into zero padded buffer
  if ( ! (secret = calloc(2, ENCRYPTION_KEY_LENGTH)))
  {
    add_assoc_string(return_value, "error", "cannot allocate secret memory", 1);
    return;
  }
  strcpy(secret, key);

  // initialize the encryption operation
  if ( ! EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, secret, vector))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to initialize AES 256 GCM cipher", 1);
    return;
  }

  // set expected tag value
  if( ! EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, MSG_AUTH_TAG_LENGTH, tag))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to validate message authentication tag", 1);
    return;
  }

  // authenticate additional application data
  if ( ! EVP_DecryptUpdate(ctx, NULL, &length, aad, aadLen))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to decrypt additional application data", 1);
    return;
  }

  // decrypt message text
  if ( ! EVP_DecryptUpdate(ctx, plain, &length, cipher, cipherLen))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to decrypt message", 1);
    return;
  }
  if (length > plainSize)
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "buffer overrun during decryption", 1);
    return;
  }
  plainLen = length;

  // finalise decryption
  if ( ! EVP_DecryptFinal_ex(ctx, plain + plainLen, &length))
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "failed to finalize decryption", 1);
    return;
  }
  plainLen += length;
  if (plainLen > plainSize)
  {
    EVP_CIPHER_CTX_free(ctx);
    add_assoc_string(return_value, "error", "buffer overrun during decryption", 1);
    return;
  }

  // add output values and clean up
  add_assoc_string(return_value, "plain", plain, 1);

  // clean up
  EVP_CIPHER_CTX_free(ctx);
  free(cipher);
  free(vector);
  free(tag);
  free(plain);
  free(secret);
}
