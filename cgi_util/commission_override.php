<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Commission Override</title>
    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script type="text/javascript" src="../js/jquery-2.0.3.js"></script>

<script>
$(function() {
  $("#search_iccid").keyup(function(event){
    if(event.keyCode == 13){
        Main.SearchIccid();
    }
  });
});

var Main = {
  iccid: null,

  SearchIccid: function() {
    $('#search_error').hide();
    Main.hideDetails();

    var iccid = $('#search_iccid').val();

    $.ajax({
      type: 'POST',
      url: "/pr/internal/2/ultra/api/internal__SearchIccid",
      dataType: 'json',
      data: 'iccid=' + iccid,
      success: function(data) {
        if (data.errors.length > 0)
        {
          $('#search_error').show();
          return;
        }

        Main.processIccid(data);
        window.scrollTo(0,0);
      }
    });
  },

  showOverrideDetails: function() {
    if (this.iccid.override_epoch != null)
    {
      $('#has_override').show();
      $('#show_oad_button').show();
    }
    else
      $('#override_details').show();
  },

  hideOverrideDetails: function() {
    $('#show_oad_button').show();

    $('#has_override').hide();
    $('#override_details').hide();
  },
 
  showEditOverrideDetails: function() {
    $('#has_override').hide();

    $('#od_new_dealer').val(this.iccid.override_dealer_code);
    $('#od_notes').val(this.iccid.override_notes);

    $('#override_details').show();
  },

  hideEditOverrideDetails: function() {
    if (this.iccid.override_epoch != null)
      $('#has_override').show();
    else
      $('#show_oad_button').show();

    $('#override_details').hide();
  },

  updateActivationLogOverride: function() {
    var iccid = $('#search_iccid').val();
    var override_dealer = $('#od_new_dealer').val();
    var override_notes = $('#od_notes').val();

    var params = ''
      + 'iccid=' + iccid 
      + '&override_dealer=' + override_dealer 
      + '&override_notes=' + override_notes;

    $.ajax({
      type: 'POST',
      url: "/pr/internal/2/ultra/api/internal__UpdateActivationLogOverride",
      dataType: 'json',
      data: params,
      success: function(data) {
        if (data.errors.length > 0)
        {
          alert(data.errors[0]);
          return;
        }
        else
        {
          alert('Override Activation Details submitted for ICCID: ' + Main.iccid.iccid_full);
          Main.SearchIccid();
        }
      }
    });
  },

  clearInputs: function() {
    $('#od_new_dealer').val('');
    $('#od_notes').val('');
  },

  clearText: function() {
    $('#id_iccid').text('');
    $('#id_master_agent').text('');
    $('#id_distributor').text('');
    $('#id_dealer').text('');

    $('#ad_activation_date').text('');
    $('#ad_dealer').text('');
    $('#ad_master_agent').text('');
    $('#ad_distributor').text('');

    $('#cd_customer_id').text('');
    $('#cd_msisdn').text('');
    $('#cd_name').text('');
    $('#cd_plan_state').text('');
    $('#cd_base_plan').text('');
    $('#cd_boltons').html('');
    $('#cd_personal_plan').text('');
    $('#cd_funding_this_cycle').text('');

    $('#ho_new_dealer').text('');
    $('#ho_override_user').text('');
  },

  hideDetails: function() {
    $('#override_details').hide();
    $('#inventory_details').hide();
    $('#activation_details').hide();
    $('#show_oad_button').hide();
    $('#ad_unused_msg').hide();
    $('#customer_details').hide();
    $('#has_override').hide();
    $('#if_commissions_paid').hide();
    $('#if_commissions_not_paid').hide();
  },

  convertUTC: function(epcoh) {
    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(epcoh);
    return d.toLocaleDateString() + ' ' + d.toLocaleTimeString() + ' UTC';
  },

  makeBusinessCodeName: function(code, name)
  {
    return (code == null) ? 'none' : code + ' (' + name + ')';
  },

  processIccid: function(data) {
    this.clearInputs();
    this.clearText();
    this.hideDetails();

    this.iccid = data;

    $('#spiff_status').show();
    if (data.commissions_paid)
      $('#if_commissions_paid').show();
    else
      $('#if_commissions_not_paid').show();

    var gross_add_date = (data.gross_add_epoch == null) ? 'none' : this.convertUTC(data.gross_add_epoch);
    $('#gross_add_date').text(gross_add_date);

    $('#inventory_details').show();

    var id_master_agent = this.makeBusinessCodeName(data.inventory_master_code, data.inventory_master_name);
    var id_distributor = this.makeBusinessCodeName(data.inventory_distributor_code, data.inventory_distributor_name);
    var id_dealer = this.makeBusinessCodeName(data.inventory_dealer_code, data.inventory_dealer_name);

    // inventory details
    $('#id_iccid').text(data.iccid_full);
    $('#id_master_agent').text(id_master_agent);
    $('#id_distributor').text(id_distributor);
    $('#id_dealer').text(id_dealer);

    $('#activation_details').show();
    if (data.activation_epoch == null)
    {
      $('#activation_details_fields').hide();
      $('#ad_unused_msg').show();
      return;
    }

    $('#activation_details_fields').show();

    // activation details
    $('#ad_activation_date').text(this.convertUTC(data.activation_epoch));
    $('#ad_dealer').text(this.makeBusinessCodeName(data.activation_dealer_code, data.activation_dealer_name));
    $('#ad_master_agent').text(this.makeBusinessCodeName(data.activation_master_code, data.activation_master_name));
    $('#ad_distributor').text(this.makeBusinessCodeName(data.activation_distributor_code, data.activation_distributor_name));

    // customer details
    $('#customer_details').show();

    var boltons = 'none';
    if (data.bolt_ons)
    {
      boltons = '';
      for (var i = 0; i < data.bolt_ons.length; i++)
      {
        if (i) boltons += '<br><label></label>';
        boltons += data.bolt_ons[i].id; 
      }
    }

    $('#cd_customer_id').text(data.customer_id);
    $('#cd_msisdn').text(data.msisdn);
    $('#cd_name').text(data.first_name + ' ' + data.last_name);
    $('#cd_plan_state').text(data.plan_state);
    $('#cd_base_plan').text(COUtil.dollarsFromPennies(data.plan_cost));
    $('#cd_boltons').html(boltons);
    $('#cd_personal_plan').text(COUtil.dollarsFromPennies(data.full_cost));
    $('#cd_funding_this_cycle').text(COUtil.dollarsFromPennies(data.stored_value));

    // override
    if (data.override_epoch != null)
    {
      $('#has_override').show();
      $('#ho_new_dealer').text(this.makeBusinessCodeName(data.override_dealer_code, data.override_dealer_name));
      $('#ho_override_user').text(data.override_user);
      $('#ho_notes').val(data.override_notes);
    }

    $('#show_oad_button').show();
  }
}

var COUtil = {
  dollarsFromPennies: function(pennies) {
    if (parseInt(pennies) == 0)
      return '$0.00';

    pennies = '' + pennies;
    return '$' + pennies.substr(0, pennies.length - 2) + '.' + pennies.substr(-2, pennies.length);
  } 
}
</script>

<style>
body {
  font-family: sans-serif;
  font-size: 14px;
}

#container { margin-bottom: 32px;  }

.border { border: 1px solid #333; margin-bottom: 8px; padding: 8px; }
.border-top { border-top: 1px solid #333; margin-top: 32px; padding-top: 16px; }
.red { color: red; }

p label {
  display: inline-block;
  font-weight: bold;
  padding-right: 4px;
  text-align: right;
  width: 164px; }

div#spiff_status { width: 600px; }
div.leftright { margin: 16px; min-width: 1024px; }
div.left { float: left; min-width: 400px; width: 35%; }
div.right { float: left; margin-left: 8px; min-width: 600px; width: 50%; }
</style>

<body><div id="container">

<div class="leftright">
<h1>Commission Override</h1>

<div>
  <span>Enter ICCID to Lookup:</span><br />
  <input type="text" id="search_iccid" name="search_iccid" />
  <button onclick="Main.SearchIccid();">Search</button>
  <p id="search_error" class="red" style="display: none;">
    The ICCID you entered is invalid.
  </p>
</div>

<div id="spiff_status" class="border-top" style="display: none;">
  <h2>Spiff Status</h2>

  <strong>Gross Add Date:&nbsp;</strong>
  <span id="gross_add_date"></span>

  <p id="if_commissions_paid"><label>Commission Status:</label><span>Paid</span></p></p>
  <p id="if_commissions_not_paid"><label>Commission Status:</label><span>Unpaid</span></p></p>
</div>
</div>

<div class="leftright">
<div class="left">
<div id="inventory_details" class="border-top" style="display: none;">
  <h2>Inventory Details</h2>
  <div class="border">
    <p><label>ICCID:</label><span id="id_iccid"></span></p>
    <p><label>Master Agent:</label><span id="id_master_agent"></span></p>
    <p><label>Distributor:</label><span id="id_distributor"></span></p>
    <p><label>Dealer:</label><span id="id_dealer"></span></p>
  </div>
</div>

<div id="customer_details" class="border-top" style="display: none;">
  <h2>Customer Details</h2>
  <div class="border">
    <p><label>customer_id:</label><span id="cd_customer_id"></span></p>
    <p><label>MSISDN:</label><span id="cd_msisdn"></span></p>
    <p><label>Name:</label><span id="cd_name"></span></p>
    <p><label>Plan State:</label><span id="cd_plan_state"></span></p>
    <p><label>Base Plan:</label><span id="cd_base_plan"></span></p>
    <p><label>BoltOns:</label><span id="cd_boltons"></span></p>
    <p><label>Personal Plan:</label><span id="cd_personal_plan"></span></p>
    <p><label>Funding this Cycle:</label><span id="cd_funding_this_cycle"></span></p>
  </div>
</div>
</div>

<div class="right">
<div id="activation_details" class="border-top" style="display: none;">
  <h2>Activation Details</h2>
  <p class="red" id="ad_unused_msg"><strong>SIM is unused.</strong></p>
  <div id="activation_details_fields" class="border">
    <p><label>Activation Date:</label><span id="ad_activation_date"></span></p>
    <p><label>Dealer:</label><span id="ad_dealer"></span></p>
    <p><label>Master Agent:</label><span id="ad_master_agent"></span></p>
    <p><label>Distributor:</label><span id="ad_distributor"></span></p>
  </div>

  <button id="show_oad_button" onclick="Main.showOverrideDetails();" style="display: none;">Override Activation Details</button>

  <div id="has_override" style="display: none;">
    <h3>This Activation has been overridden with the below info:</h3>
    <div class="border">
      <p><label>New Dealer:</label><span id="ho_new_dealer" class="red"></span></p>
      <p><label>Override User:</label><span id="ho_override_user"></span></p>
      <p><label>Notes</label><textarea id="ho_notes" name="ho_notes" disabled cols="32" rows="5"></textarea></p>
    </div>
    <button id="show_od_button" onclick="Main.showEditOverrideDetails()">Edit Override Details</button>
    <button id="hide_oad_button" onclick="Main.hideOverrideDetails()">Hide Override Details</button>
  </div>

  <div id="override_details" style="display: none;">
    <h3>Override Activation Details</h3>
    <div class="border">
      <p><label>New Dealer:</label><input type="text" id="od_new_dealer" name="od_new_dealer" /></p>
      <p><label>Notes:</label><textarea id="od_notes" name="od_notes" cols="32" rows="5"></textarea></p>
      <p>
        <label></label>
        <div><strong>Note</strong> - Overrides to Master Agent and Sub-Distributor info will auto-populate based on the New Retailer value above.</div>
      </p>
      <p><label></label><button onclick="Main.updateActivationLogOverride();">Submit Override</button>
    </div>
    <button id="hide_od_button" onclick="Main.hideEditOverrideDetails();">Cancel Override</button>
  </div>
</div>
</div>
</div>

<div style="clear:both;"></div>

</div></body>
</html>