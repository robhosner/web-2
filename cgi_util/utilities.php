<html>
  <head>
  <title>Internal Utilities</title>
  <link rel="stylesheet" href="/css/jquery.datetimepicker.css">

  <script type="text/javascript" src="/js/show_environment_stage.js"></script>
  <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="/js/jquery.datetimepicker.js"></script>
  <script type="text/javascript" src="utilities.js"></script>
  <style>
    h1 {
      font-size: 3em;
    }
    #container {
      font-family: sans-serif;
      width : 1200px;
      margin: 0px auto;
      text-align: center;
    }
    .section {
      border-bottom: 1px solid;
      padding: 10px 10px 30px 10px;
    }
    .section div {
      margin-bottom: 10px;
    }
    .redButton {
      width: 100px;
      height: 100px;
      background-color: red;
      border: 10px solid #555;
      border-radius: 128px;
      color: white;
      cursor: pointer;
    }
    label { font-weight: bold; }
    .theButton {
      background-color: red;
      border: none;
      cursor: pointer;
    }
    #output { margin-top: 32px; }
  </style>
  </head>
  <body>
    <div id="container">
      <h1>Internal Utilities</h1>
      <div id="internal__RecoverEpay" class="section">
        <h2>Reconcile Messages</h2>
        <button data-endpoint="internal__RecoverEpay" class="redButton execute-api">EXECUTE</button>
      </div>
      <div id="internal__CleanMiddlewareRedisQueues" class="section">
        <h2>Cleanup Queues</h2>
        <button data-endpoint="internal__CleanMiddlewareRedisQueues" class="redButton execute-api">EXECUTE</button>
      </div>
      <div id="internal__ForceExpireCustomerPlan" class="section">
        <h2>End Customer Billing Cycle</h2>
        <div>
          <label>Customer ID:</label>
          <input id="input_customer_id" type="text" name="customer_id" value="" />
          <button data-endpoint="internal__ForceExpireCustomerPlan" class="theButton execute-api">EXECUTE</button>
        </div>
        <div>
          <label>Expiration Date:</label>
          <input type="text" id="newExpirationDate">
        </div>
        <div id="message"></div>
        <div id="output" style="display: none;">
          <label>Customer ID:</label> <span id="output_customer_id"></span><br />
          <label>New Expiration Date:</label> <span id="output_new_expiration_date"></span><br />
          <label>Previous Expiration Date:</label> <span id="output_previous_expiration_date"></span>
        </div>
      </div>
      <div id="internal__UndoActivationLogOverride" class="section">
        <h2>Undo Activation Log Override</h2>
        <div>
          <label>ICCID:</label>
          <input id="input_iccid" type="text" name="iccid" value="" />
          <button data-endpoint="internal__UndoActivationLogOverride" class="theButton execute-api">EXECUTE</button>
        </div>
        <div id="output" style="display: none;">
        </div>
      </div>
    </div>
  </body>
</html>
