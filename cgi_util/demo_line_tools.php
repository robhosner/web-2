<?php

/*
 */

?>
<html>
<head>

<title>DEMO LINE TOOLS</title>

<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

var readyText   = 'EXECUTE';
var workingText = 'WORKING';

var readyColor   = 'red';
var workingColor = '#888';

var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

$(function() {
  $('#action').change(function() {
    var action = $(this).val();

    if (action == 'lookup')
    {
      $('#lookup').show();
      $('#tools').hide();
    }
    else
    {
      $('#lookup').hide();
      $('#tools').show()
    }

    $('.doc').hide();
    $('#' + action + '_doc').show();

    disable_inputs();

    switch (action)
    {
      case 'decommission':
        $('#demo_line_id').prop('disabled', false);
        break;
      case 'assignCustomer':
        $('#demo_line_id').prop('disabled', false);
        $('#customer_id').prop('disabled', false);
        break;
      case 'changeSettings':
        $('#demo_line_id').prop('disabled', false);
        $('#customer_id').prop('disabled', false);
        $('#status').prop('disabled', false);
        $('#activations_needed').prop('disabled', false);
        break;
      case 'addFundingRecord':
        $('#demo_line_id').prop('disabled', false);
        break;
    }
  });

  disable_inputs();
  clear_inputs();

  $('#action').val('decommission');
  $('#demo_line_id').prop('disabled', false);
  $('#decommission_doc').show();
});

function clear_inputs()
{
  $('#demo_line_id').val('');
  $('#customer_id').val('');
  $('#status').val('');
  $('#activations_needed').val('');
}

function disable_inputs()
{
  $('#demo_line_id').prop('disabled', true);
  $('#customer_id').prop('disabled', true);
  $('#status').prop('disabled', true);
  $('#activations_needed').prop('disabled', true);
}

function contact_baseUrl(api, params, successCallback)
{
  $('#output').text('');

  $('#redButton').css('background-color', workingColor);
  $('#redButton').text(workingText);

  $.ajax({
    method: 'GET',
    url: baseURL + '&command=' + api,
    data: params,
    success: function(data) {
      var response = JSON.parse(data);

      successCallback(response);

      $('#redButton').css('background-color', readyColor);
      $('#redButton').text(readyText);
    }
  });
}

function demoLineTools()
{
  var action = $('#action').val();
  var params = '';

  if (action == 'lookup')
  {
    var dealer_code = $('#lookup_dealer_code').val();
    var customer_id = $('#lookup_customer_id').val();
    var msisdn      = $('#lookup_msisdn').val();

    params += 'dealer_code='  + dealer_code;
    params += '&customer_id=' + customer_id;
    params += '&msisdn='      + msisdn;

    contact_baseUrl('internal__LookUpDemoLine', params, function(response) {
      if ((response.errors && response.errors.length > 0) || (response.success !== true))
      {
        alert(response.errors[0]);
      }
      else
      {
        $('#output').text('');

        for (var i = 0; i < response.demo_line_info.length; i++)
        {
          var text = '';

          if (i > 0) text += '<br /><br />';

          var dl = response.demo_line_info[i];

          text += '<div>'
          text += 'dealercd: ' + dl.dealercd;
          text += '<br />demo_line_id: ' + dl.demo_line_id;
          text += '<br />demo_status: ' + dl.demo_status;
          text += '<br />activations_needed: ' + dl.activations_needed;
          text += '<br />claim_by_date: ' + dl.claim_by_date;
          text += '<br />customer_id: ' + dl.customer_id;
          text += '<br />current_mobile_number: ' + dl.current_mobile_number;
          text += '<br />customer_option_inserted: ' + dl.customer_option_inserted;
          text += '<br />entered_by: ' + dl.entered_by;
          text += '</div>';

          $('#output').append(text);
        }
      }
    });

    return;
  }

  var input = [];

  switch (action)
  {
    case 'decommission':
      input.push('demo_line_id');
      break;
    case 'assignCustomer':
      input.push('demo_line_id');
      input.push('customer_id');
      break;
    case 'changeSettings':
      input.push('demo_line_id');
      input.push('customer_id');
      input.push('status');
      input.push('activations_needed');
      break;
    case 'addFundingRecord':
      input.push('demo_line_id');
      break;
  }

  params += 'action=' + action;
  for (var i = 0; i < input.length; i++)
  {
    params += '&' + input[i] + '=' + $('#' + input[i]).val();
  }

  contact_baseUrl('internal__DemoLineTools', params, function(response) {
    if ((response.errors && response.errors.length > 0) || (response.success !== true))
    {
      alert(response.errors[0]);
    }
    else
    {
      $('#output').append('SUCCESS');
    }
  });

  return;
}

</script>

<style>
#container {
  font-family: sans-serif;
  width : 768px;
  margin: 0px auto;
  text-align: center;
}

#redButton {
  width: 128px;
  height: 32px;
  background-color: red;
  border: 3px solid #555;

  color: white;
  cursor: pointer;
  font-size: 1em;
}

p label {
  float: left;
  width: 128px;
  display: inline-block;
}
p input[type="text"] { float: left; }
p select { float: left; }

div.doc {
  border: 1px solid #111;
  margin-bottom: 16px;
  padding: 8px;
  text-align: left;
}
</style>

</head>
<body>
<div id="container">

<h1>DEMO LINE TOOLS</h1>

<div id="decommission_doc" class="doc" style="display:none">
  <p><strong>Decommission a Demo Line</strong></p>
  <p>This tool is designed to help remove an account from the Demo Line Program so that the associated dealer can become eligible again for a new demo line. This tool is useful if an ex-employee takes off with a demo line SIM, the demo line SIM stops working, etc.</p>
  <ol>
    <li>This tool sets the ULTRA.DEMO_LINE status column to be "inactive". This will allow the dealer to become eligible for a new line again when the nightly demo process runs.</li>
    <li>Additionally, this removes the associated customer from ULTRA.CUSTOMER_OPTIONS, thus preventing automatic account funding.</li>
  </ol>

  <p>The owner of the decommissioned SIM can still use Ultra's service, but will need to recharge via normal means for future recharge dates.</p>

  <p>After running this, the retailer should be eligible to claim a new demo line on the following business day, assuming they meet the minimum activation requirements.</p>
</div>

<div id="assignCustomer_doc" class="doc" style="display:none">
  <strong>Swap Existing Number into Demo Program</strong>
  <p>Use this tool if a retailer requests that a specific, already existent Ultra account be funded as a demo line. As a pre-requisite, the dealer must already be eligible for a demo line.</p>

  <p>Takes an existing ULTRA.DEMO_LINE row with "eligible" status, changes the status to "active", and updates the customer_id column</p>
  <p>Inserts a row into ULTRA.CUSTOMER_OPTIONS so that the customer_id gets funded automatically each month</p>
</div>

<div id="changeSettings_doc" class="doc" style="display:none">
  <strong>Update Demo Line's Settings</strong>
  <p>This tool can be used to manipulate a row in the ULTRA.DEMO_LINE table. This is intended as a catch-all to help cleanup any random demo line issues.</p>

  <p>Sample scenarios:</p>
  <ul>
    <li>change a line from 'warning' to 'active'</li>
    <li>reverse which of a dealer's liens require 10 and 20 activations (you would need to perform this in 2 sequential steps)</li>
    <li>if demo line has an "active" status in ULTRA.DEMO_LINE, but has no customer_id (due to an activation issue), you could reset the line to be "eligible"</li>
  </ul>
</div>

<div id="addFundingRecord_doc" class="doc" style="display:none">
  <strong>Add Demo Line Funding</strong>
  <p>This tool should be utilized when a Demo Line is not getting funded and goes into Suspend. As long as the ULTRA.DEMO_LINE status is "active" or "warning", the line should get renewed for free every month. If an "active" or "warning" demo line has gone into a Suspend plan_state, it is probably because it is missing a row in ULTRA.CUSTOMER_OPTIONS. This could be the result of an activation error, or related to a desync when the program was initially launched.</p>

  <p>Using this tool will insert the appropriate row into ULTRA.CUSTOMER_OPTIONS so that the demo line gets funded correctly in the future.</p>
</div>

<p>
  <label>ACTION</label>
  <select id="action" name="actions">
    <option value="decommission">Decommission</option>
    <option value="assignCustomer">Assign Customer</option>
    <option value="changeSettings">Change Settings</option>
    <option value="addFundingRecord">Add Funding Record</option>
    <option value="lookup">Lookup Demo Line</option>
  </select>
</p><br />

<div id="tools">
<p id="show_demo_line_id">
  <label>Demo Line ID</label>
  <input type="text" name="demo_line_id" id="demo_line_id" />
</p><br />

<p id="show_customer_id">
  <label>Customer ID</label>
  <input type="text" name="customer_id" id="customer_id" />
</p><br />

<p id="show_status">
  <label>Status</label>
  <input type="text" name="status" id="status" />
</p><br />

<p id="show_activations_needed">
  <label>Activations Needed</label>
  <input type="text" name="activations_needed" id="activations_needed" />
</p>
</div>

<div style="clear:both"></div>

<div id="lookup" style="display:none">
<p>
  <label>Dealer Code</label>
  <input type="text" name="dealer_code" id="lookup_dealer_code" />
</p><br />
<p>
  <label>Customer ID</label>
  <input type="text" name="customer_id" id="lookup_customer_id" />
</p><br />
<p>
  <label>MSISDN</label>
  <input type="text" name="msisdn" id="lookup_msisdn" />
</p>

</div>

<div style="clear:both"></div>

<p><button id="redButton" onclick="demoLineTools();">EXECUTE</button></p>

<div id="output">
</div>

</div>
</body>
</html>