<?php
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>Ultra Settings Manager</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link href="../styles/bootstrap2/css/bootstrap.min.css" rel="stylesheet">
  <link href="../styles/bootstrap2/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="../styles/modals.css" media="screen" rel="stylesheet" type="text/css" />

  <script type="text/javascript" src="/js/show_environment_stage.js"></script>
  <script language="javascript" src="../js/modernizr_custom.min.js"></script>
  <script language="javascript">

  var username = 'username'; // TODO

  Modernizr.load([
                   {
                   test: Modernizr.mq('only all'),
                   nope: '../styles/oldIE.css'
                   },
                   {
                   load: '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',
                   complete: function () { if ( !window.jQuery ) { Modernizr.load('js/jquery-1.7.2.min.js'); }}
                   },
                   {
                   load: [
                     "../js/jquery-ui-1.8.23.custom.min.js"
                     ],

                   complete: function () {
                       $(document).ready(function() {

$("#maindiv").hide();

load_all_settings();

                       });
                     }
                   },

                   {
                   test : window.JSON,
                   nope : '../js/json2.js'
                   }
                   ]);

function load_all_settings()
{
  $.ajax({
    type:     'POST',
    url:      "/ultra_api.php?bath=rest&partner=settings&version=2&command=settings__GetAllSettings&format=json",
    dataType: 'json',
    data:     { username : username },
    error:    function error_load_all_settings(data) {
                alert('error after settings__GetAllSettings API call');
              },
    success:  function callback_load_all_settings(data) {
                if ( data == null ) {
                  alert('error loading data');
                } else if ( data.success ) {
                  show_all_settings(data);
                  $("#maindiv").show();
                } else {
                  alert('settings__GetAllSettings failed');
                }
              }
  });

  $.ajax({
    type:     'POST',
    url:      "/ultra_api.php?bath=rest&partner=internal&version=2&command=internal__GetEnvironmentInfo",
    dataType: 'json',
    data:     {},
    error:    function error_GetEnvironmentInfo(data) {
                alert('error after internal__GetEnvironmentInfo API call');
              },
    success:  function callback_GetEnvironmentInfo(data) {
                if ( data == null ) {
                  alert('error loading data');
                } else if ( data.success ) {
                  $("#db_name").html(
                    (data.db_name == 'ULTRA_DEVELOP_TEL' ?
                    '<font size=4 color=blue>DEVELOPMENT</font>' :
                    '<font size=4 color=red>PRODUCTION</font>') +
                    ' (' + data.db_name + ')');
                  $("#redis_host").html(data.redis_host);
                } else {
                  alert('internal__GetEnvironmentInfo failed');
                }
              }
  });
}

function show_all_settings(data)
{
  for ( var i=0 ; i<data.setting_fields.length ; i++ )
  {
    var color   = 'green';
    var caption = 'change';
    if ( data.setting_default[i] != data.setting_values[i] )
    {
      color   = 'red';
      caption = 'reset to default';
    }

    var row = '<tr><td nowrap>' + data.setting_descriptions[i] +
             '</td><td nowrap id="field_' + i + '" value="' + data.setting_fields[i] + '">' + data.setting_fields[i] +
             '</td><td nowrap ><b>' + data.setting_default[i] + '</b>' +
             '</td><td id="value_' + i +
              '" value="' + data.setting_values[i] +
              '" type="' + data.value_type[i] +
              '" default="' + data.setting_default[i] + 
              '"><b><font color="' + color +
              '">' + data.setting_values[i] + '</font></b>' +
             '</td><td> &nbsp; <a href="#" id="' + i + '" class="toggle" value="' + i + '">' + caption + '</a> <p id="caption_' + i + '"</p>' +
             '</td></tr>';

    $('#settings_table tr:last').after( row );
  }

  // associate class 'a.toggle' to function toggle_setting_value
  $('body').on('click', 'a.toggle', function() {
    toggle_setting_value( $(this).attr("id"));
    return false;
  });

}

function toggle_setting_value(id)
{
  //  alert( $("#field_" + id).attr("value") ); // field
  //  alert( $("#value_" + id).attr("value") ); // value

  var type = $("#value_" + id).attr("type"); // element type
  var value = $("#value_" + id).attr("value"); // element value
  var new_value;

  if ( type == 'BOOLEAN' )
  {
    if (value == 'TRUE')
      new_value = 'FALSE';
    else
      new_value = 'TRUE';
  }
  else if (type == 'STRING')
  {
    new_value = $("#value_" + id).attr("default");
    var result = prompt("Enter reason:", new_value); // Sean: please change dialog box to allow 1000 chars of input

    if (result == null) // Canceled was pressed 
      return;
    else
      new_value = result;
  }

  alert( 'Setting ' + $("#field_" + id).attr("value") + ' to ' + new_value ); 

  // remove link
  $("#"+id).remove();

  // user feedback
  $("#caption_"+id).html( 'update in progress' );

  set_field_to_value( $("#field_" + id).attr("value") , new_value );
}

function set_field_to_value(field,value)
{
  $.ajax({
    type:     'POST',
    url:      "/ultra_api.php?bath=rest&partner=settings&version=2&command=settings__TriggerSetting&format=json",
    dataType: 'json',
    data:     {
                username      : username,
                setting_field : field,
                setting_value : value
              },
    error:    function error_load_all_settings(data) {
                alert('error after settings__TriggerSetting API call');
              },
    success:  function callback_load_all_settings(data) {
                if ( data == null ) {
                  alert('error loading data');
                } else if ( data.success ) {
                  setTimeout( refresh_page , 1000);
                } else {
                  alert('settings__TriggerSetting failed');
                }
              }
  });
}

function refresh_page()
{
  location.reload();
}

</script>
  <!-- html5.js for IE less than 9 -->
  <!--[if lt IE 9]>
  <style type='text/css'>
  </style>
  <![endif]-->
</head>

<body>
  <div class="container-fluid">
    <div class="row-fluid" id='main_id' name='main_id'>
      <div class='span12 wellx' style='padding-top:20px;' id="maindiv">
        <form name="mainform" id="mainform" action="/" method="POST">
          <table>
            <tr>
              <td>
                Ultra Settings Manager<br><br>
              </td>
            </tr>
            <tr>
              <td> Database : <span id="db_name"></span>
              </td>
            </tr>
            <tr>
              <td> Redis Host : <span id="redis_host"></span>
              </td>
            </tr>
          </table>
          <table id="settings_table" border="1">
            <thead>
              <tr>
                <th>Description</th>
                <th>Name</th>
                <th>Default</th>
                <th>Current Value</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </form>
      </div>
      <div>
        <table>
          <tr>
            <td>
              <div id="ajax_feedback" class='span12 wellx' style='padding-top:20px;'> </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div><!--/.fluid-container-->
</body>
</html>
