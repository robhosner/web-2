var Utilities = Utilities || {};

$(function() {
    (function(Utilities) {
        "use strict";

        Utilities.readyText   = 'EXECUTE';
        Utilities.workingText = 'WORKING';
        Utilities.readyColor   = 'red';
        Utilities.workingColor = '#888';
        Utilities.baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

        Utilities.init = function()
        {
            $(".execute-api").on("click", function() {
                var apiEndpoint = $(this).data('endpoint');
                Utilities[apiEndpoint](apiEndpoint);
            });

            $('#newExpirationDate').datetimepicker({
                format:'Y-m-d H:i:s'
            });
        };

        Utilities.internal__RecoverEpay = function(id)
        {
            if (!Utilities.checkIfOktoRun("Are you sure you wish to execute reconcile messages?"))
            {
                return;
            }

            Utilities.setWorkingButton(id);

            $.ajax({
                method: 'GET',
                url: Utilities.baseURL + '&command=internal__ReconcileMessages',
                success: function(data, status, settings) {
                    var response = JSON.parse(data);

                    if ((response.errors && response.errors.length > 0) || (response.success !== true))
                    {
                        alert(response.errors[0]);
                    }
                    else
                    {
                        alert('Success');
                    }

                    Utilities.resetButton(id);
                }
            });
        };

        Utilities.internal__CleanMiddlewareRedisQueues = function(id)
        {
            if (!Utilities.checkIfOktoRun("Are you sure you wish to execute internal__CleanMiddlewareRedisQueues?"))
            {
                return;
            }

            Utilities.setWorkingButton(id);
            $('#' + id + '.redButton').attr('disabled', true);

            $.ajax({
                method: 'GET',
                url: Utilities.baseURL + '&command=internal__CleanMiddlewareRedisQueues',
                data: {"mode": "read"},
                success: function(data, status, settings) {
                    var response = JSON.parse(data);

                    if ((response.errors && response.errors.length > 0) || (response.success !== true))
                    {
                        alert(response.errors[0]);
                    }
                    else
                    {
                        var waitTime = 90;
                        var timer = setInterval(function() {

                            var id = 'internal__CleanMiddlewareRedisQueues';
                            var button = $('#' + id + '.redButton');

                            button.attr('disabled', true);

                            if (waitTime == 0)
                            {
                                $.ajax({
                                    method: 'GET',
                                    url: Utilities.baseURL + '&command=internal__CleanMiddlewareRedisQueues',
                                    data: {"mode": "delete"},
                                    success: function(data, status, settings) {
                                        var response = JSON.parse(data);

                                        if ((response.errors && response.errors.length > 0) || (response.success !== true))
                                        {
                                            alert(response.errors[0]);
                                        }
                                        else
                                        {
                                            alert('Successfully cleaned up queues.');
                                        }

                                        clearInterval(timer);
                                        Utilities.resetButton(id);
                                        button.attr('disabled', false);
                                    }
                                });
                            }
                            else
                            {
                                button.html("preparing: " + waitTime + "s remaining");
                            }

                            waitTime--;
                        }, 1000);
                    }
                }
            });
        };

        Utilities.internal__ForceExpireCustomerPlan = function(id) {
            if (!Utilities.checkIfOktoRun("Are you sure you wish to end customer billing cycle?"))
            {
                return;
            }

            var customer_id = $('#' + id + ' #input_customer_id').val();

            Utilities.setWorkingButton(id);

            $.ajax({
                method: 'GET',
                url: Utilities.baseURL + '&command=internal__ForceExpireCustomerPlan',
                data: 'customer_id=' + customer_id + '&date=' + $('#newExpirationDate').val(),
                success: function (data, status, settings) {
                    var response = JSON.parse(data);

                    console.log(response);

                    if ((response.errors && response.errors.length > 0) || (response.success !== true)) {
                        $('#' + id + ' #output').hide();
                        $('#' + id + ' #message').html('<div style="color:red">' + response.errors[0] + '</div>').show();

                        setTimeout(function () {
                            $('#message').hide()
                        }, 3000);

                    }
                    else {
                        $('#' + id + ' #output').show();
                        $('#' + id + ' #output_customer_id').text(customer_id);
                        $('#' + id + ' #output_new_expiration_date').text(response.new_expiration_date);
                        $('#' + id + ' #output_previous_expiration_date').text(response.previous_expiration_date);
                    }

                    Utilities.resetButton(id);
                }
            });
        };

        Utilities.internal__UndoActivationLogOverride = function(id)
        {
            if (!Utilities.checkIfOktoRun("Are you sure you wish to undo activation log override?"))
            {
                return;
            }

            var iccid = $('#' + id + ' #input_iccid').val();

            Utilities.setWorkingButton(id);

            $.ajax({
                method: 'GET',
                url: Utilities.baseURL + '&command=internal__UndoActivationLogOverride',
                data: 'iccid=' + iccid,
                success: function(data, status, settings) {
                    var response = JSON.parse(data);

                    console.log(response);

                    if ((response.errors && response.errors.length > 0) || (response.success !== true))
                    {
                        $('#' + id + ' #output').hide();

                        alert(response.errors[0]);
                    }
                    else
                    {
                        $('#' + id + ' #output').show();
                        $('#' + id + ' #output').text(data);

                        alert('Success');
                    }

                    Utilities.resetButton(id);
                }
            });
        };

        Utilities.checkIfOktoRun = function(message)
        {
            return confirm(message);
        };

        Utilities.setWorkingButton = function(id)
        {
            var button = $('#' + id + '.redButton');
            button.css('background-color', Utilities.workingColor);
            button.html(Utilities.workingText);
        };

        Utilities.resetButton = function(id)
        {
            var button = $('#' + id + '.redButton');
            button.css('background-color', Utilities.readyColor);
            button.html(Utilities.readyText);
        }

    })(Utilities);

    Utilities.init();
});