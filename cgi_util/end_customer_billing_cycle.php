<?php

/*
 */

?>
<html>
  <head>
    <title>END CUSTOMER BILLING CYCLE</title>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
      $(function() {
        $('#newExpirationDate').datetimepicker({
          format:'Y-m-d H:i:s'
        });
      });

      var readyText   = 'EXECUTE';
      var workingText = 'WORKING';

      var readyColor   = 'red';
      var workingColor = '#888';

      var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

      function internal__ForceExpireCustomerPlan()
      {
        var c = confirm("Are you sure you wish to execute?");
        if (!c) return;

        var customer_id = $('#input_customer_id').val();

        $('#theButton').css('background-color', workingColor);
        $('#theButton').text(workingText);

        $.ajax({
          method: 'GET',
          url: baseURL + '&command=internal__ForceExpireCustomerPlan',
          data: 'customer_id=' + customer_id +'&date=' + $('#newExpirationDate').val(),
          success: function(data, status, settings) {
            var response = JSON.parse(data);

            console.log(response);

            if ((response.errors && response.errors.length > 0) || (response.success !== true))
            {
              $('#output').hide();
              $('#message').html('<div style="color:red">' + response.errors[0] + '</div>').show();

              setTimeout(function() {
                $('#message').hide()
              }, 3000);

            }
            else
            {
              $('#output').show();
              $('#output_customer_id').text(customer_id);
              $('#output_new_expiration_date').text(response.new_expiration_date);
              $('#output_previous_expiration_date').text(response.previous_expiration_date);
            }

            $('#theButton').css('background-color', readyColor);
            $('#theButton').text(readyText);
          }
        });
      }
    </script>
    <style>
      div { margin: 10px; 0px}
      #container {
        font-family: sans-serif;
        width : 768px;
        margin: 0px auto;
        text-align: center;
      }

      label { font-weight: bold; }
      #theButton {
        background-color: red;
        border: none;
        cursor: pointer;
      }
      #output { margin-top: 32px; }
    </style>
  </head>
  <body>
    <div id="container">
      <h1>End Customer Billing Cycle</h1>
      <div>
        <label>Customer ID:</label>
        <input id="input_customer_id" type="text" name="customer_id" value="" />
        <button id="theButton" onclick="internal__ForceExpireCustomerPlan();">EXECUTE</button>
      </div>
      <div>
        <label>Expiration Date:</label>
        <input type="text" id="newExpirationDate">
      </div>
      <div id="message"></div>
      <div id="output" style="display: none;">
        <label>Customer ID:</label> <span id="output_customer_id"></span><br />
        <label>New Expiration Date:</label> <span id="output_new_expiration_date"></span><br />
        <label>Previous Expiration Date:</label> <span id="output_previous_expiration_date"></span>
      </div>
    </div>
  </body>
</html>