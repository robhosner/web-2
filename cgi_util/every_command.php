<?php

if (!isset($_GET['partner']))
{
  echo 'ERROR: no partner provided';
  die;
}

if (!isset($_GET['version']))
{
  echo 'ERROR: no version provided';
  die;
}

$partner = $_GET['partner'];
$version = $_GET['version'];

?>

<html>
<head>
<style>
.required { color: red; }
#left { float: left; width: 496px; }
#right { float: left; width: 768px; }
label { float: left; width: 196px; }
input[type="text"] { width: 256px; }
a:visited { color: blue; }
</style>

<script type="text/javascript" src="../js/show_environment_stage.js"></script>
<script type="text/javascript" src="../js/jquery-2.0.3.js"></script>
</head>
</html>

<h1>Every Command</h1>

<div id="left">

</div>

<script type="text/javascript">
var commands = [];
var params   = [];

var version = '<?php echo $version; ?>';

function loadPartner(partner)
{
  var url = '';
  switch (version)
  {
    case '1':
      url = '../partner.php?format=qa&version=1&partner=' + '<?php echo $partner; ?>';
      break;
    case '2':
      url = '../ultra_api.php?format=QA&version=2&partner=' + '<?php echo $partner; ?>';
      break;
  }

  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    success: function(res) {
      var apis = res;
      console.log(apis);

      commands = [];
      params   = [];

      for (var key in apis)
      {
        if (typeof apis[key] != 'object')
          continue;

        commands.push([key, apis[key].desc]);
        nextParams = [];
        for (var i = 0; i < apis[key].parameters.length; i++)
        {
          var param = apis[key].parameters[i];
          nextParams.push({
            'name': param.name,
            'type': param.type,
            'desc': param.desc,
            'required': param.required,
            'validation': JSON.stringify(param.validation)
          });
        }
        params.push(nextParams);
      }



      $('#left').html('');

      for (var j = 0; j < commands.length; j++)
      {
        var link = '<a href="#" onclick="loadCommand(' + j + ');">' + commands[j][0] + '</a><br />';
        $('#left').append(link);
      }
    }
  });
}

function runCommand()
{
  if (!envStage.canRun(['QA','DEV']))
    return;

  var inputParameters = $("#api :input[value!='']").serialize();

  var url = '';
  switch (version)
  {
    case '1':
      url = '../partner.php?format=json&bath=rest&version=1';
      break;
    case '2':
      url = '../ultra_api.php?format=json&bath=rest&version=2';
      break;
  }
  
  $.ajax({
    url: url,
    type: 'POST',
    data: inputParameters,
    dataType: 'json',
    success: function(res) {
      console.log(res);
      $('#console').html(JSON.stringify(res, null, 2));
    },
    error: function(res) {
      console.log(res);
      $('#console').html(JSON.stringify(res, null, 2));
    }
  });
}

function loadCommand(command)
{
  if (!envStage.canRun(['QA','DEV']))
    return;

  $('#api_name').text(commands[command][0]);
  $('#api_desc').text(commands[command][1]);

  $('#api_command').val(commands[command][0]);

  $('#api_params').html('');

  for (var i = 0; i < params[command].length; i++)
  {
    var required = (params[command][i]['required']) ? '<span class="required">*</span>' : '';
    var param = '<p>'
    + '<label>' + params[command][i].name + required + '</label>'
    + '<input type="text" name="' + params[command][i].name + '" />'
    + '<br />'
    + '<small>' + params[command][i].desc + '</small><br />'
    + '<small><strong>Validation</strong>' + params[command][i].validation + '</small>'
    + '</p>';

    $('#api_params').append(param);

    $('#submit').show();
  }
}

$(function() {
  loadPartner('<?php echo $partner; ?>');
});
</script>

<div id="right">

<div id="name_desc">
  <h2 id="api_name"></h2>
  <p id="api_desc"></p>
</div>

<form id="api">
  <input type="hidden" name="partner" id="api_partner" value="<?php echo $partner; ?>" />
  <input type="hidden" name="command" id="api_command" value="" />

<div id="api_params">

</div>
</form>

<button id="submit" style="display: none;" onclick="runCommand();">Submit</button>

<pre id="console">
</pre>
</div>
</html
