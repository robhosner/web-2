<?php

/*
 */

?>
<html>
<head>

<title>UNDO ACTIVATION LOG OVERRIDE</title>
<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
$(function() {

});

var readyText   = 'EXECUTE';
var workingText = 'WORKING';

var readyColor   = 'red';
var workingColor = '#888';

var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

function internal__UndoActivationLogOverride()
{
  var c = confirm("Are you sure you wish to execute?");
  if (!c) return;

  var iccid = $('#input_iccid').val();

  $('#theButton').css('background-color', workingColor);
  $('#theButton').text(workingText);

  $.ajax({
    method: 'GET',
    url: baseURL + '&command=internal__UndoActivationLogOverride',
    data: 'iccid=' + iccid,
    success: function(data, status, settings) {
      var response = JSON.parse(data);

      console.log(response);

      if ((response.errors && response.errors.length > 0) || (response.success !== true))
      {
        $('#output').hide();

        alert(response.errors[0]);
      }
      else
      {
        $('#output').show();
        $('#output').text(data);

        alert('Success');
      }

      $('#theButton').css('background-color', readyColor);
      $('#theButton').text(readyText);
    }
  });
}
</script>

<style>
#container {
  font-family: sans-serif;
  width : 768px;
  margin: 0px auto;
  text-align: center;
}

label { font-weight: bold; }
#theButton { background-color: red; }
#output { margin-top: 32px; }
</style>

</head>
<body>
<div id="container">

<h1>Undo Activation Log Override</h1>
<div>
  <label>ICCID:</label>
  <input id="input_iccid" type="text" name="iccid" value="" />
  <button id="theButton" onclick="internal__UndoActivationLogOverride();">EXECUTE</button>
</div>

<div id="output" style="display: none;">
</div>

</div>
</body>
</html>
