<?php
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>Monthly Service Charge Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link href="../styles/bootstrap2/css/bootstrap.min.css" rel="stylesheet">
  <link href="../styles/bootstrap2/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="../styles/modals.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="../styles/datepicker.css" rel="stylesheet" type="text/css" />

  <script type="text/javascript" src="/js/show_environment_stage.js"></script>
  <script language="javascript" src="../js/modernizr_custom.min.js"></script>
  <script language="javascript">

  var ajax_call_count = 0;
  var get_state_invoked              = false;
  var query_sim_invoked              = false;
  var query_msisdn_invoked           = false;
  var get_all_transitions_invoked    = false;
  var query_network_settings_invoked = false;

  Modernizr.load([
                   {
                   test: Modernizr.mq('only all'),
                   nope: '../styles/oldIE.css'
                   },
                   {
                   load: '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',
                   complete: function () { if ( !window.jQuery ) { Modernizr.load('js/jquery-1.7.2.min.js'); }}
                   },
                   {
                   load: [
                     "../styles/bootstrap2/js/bootstrap.min.js",
                    "../styles/bootstrap2/js/bootstrap-datepicker.js"
                     ],

                   complete: function () {
                       $(document).ready(function() {

var l_xToday = new Date();
var l_sToday = zeroFill(zeroFill(l_xToday.getMonth()+1,2) + '-' + l_xToday.getDate(),2) + '-' + l_xToday.getFullYear();

$('#date_input').val(l_sToday).datepicker({
  format: 'mm-dd-yyyy'
});

$("#div_button_loading").hide();

$("#button_search").click(function() {
  $("#div_button").hide();
  $("#div_button_loading").show();
  $('#ajax_target_1, #ajax_feedback').html('');
  getMonthlyServiceChargeStats();
  getAllAbortedTransitions();
  return false;
});

                       });
                     }
                   },

                   {
                   test : window.JSON,
                   nope : '../js/json2.js'
                   }
                   ]);

function getAllAbortedTransitions()
{
  // internal__GetAllAbortedTransitions

  ajax_call_count++;

  $("#ajax_feedback").append( "<br>... invoking internal__GetAllAbortedTransitions" );

  $.ajax({
    type:     'POST',
    url:      "/pr/internal/1/ultra/api/internal__GetAllAbortedTransitions",
    dataType: 'json',
    data:     { transition_date : $("#date_input").val() },
    success:  function callback_all_aborted_transitions(data) {

                $("#ajax_feedback").append( "<br>internal__GetAllAbortedTransitions returned with success="+data.success );

                var html = '';

                if ( data == null ) {
                  html = 'internal__GetAllAbortedTransitions returned no data :_(<br>';
                } else if ( data.success ) {

                  if ( data.transitions_active_to_suspended.length > 0 )
                  {
                   html +=
                    '<b>Aborted transitions Active => Suspended</b>' +
                    ' &nbsp; <table border="1">' +
                    '<thead><tr><th>Transition</th><th>Label</th><th>Created</th><th>Customer Id</th><th>Errors</th></tr></thead>' +
                    '<tbody>';

                   for ( var i=0 ; i<data.transitions_active_to_suspended.length ; i += 5 )
                   {
                     html += '<tr>';
                     for ( var j=0; j<5 ; j++ )
                     { html += '<td>' + data.transitions_active_to_suspended[i+j] + '</td>'; }
                     html += '</tr>';
                   }
                   html += '</tbody></table>';
                  }
                  else
                  {
                   html += '<b>No Aborted transitions Active => Suspended</b><br><br>';
                  }

                  if ( data.transitions_active_to_active.length > 0 )
                  {
                   html +=
                    '<b>Aborted transitions Active => Active</b>' +
                    ' &nbsp; <table border="1">' +
                    '<thead><tr><th>Transition</th><th>Label</th><th>Created</th><th>Customer Id</th><th>Errors</th></tr></thead>' +
                    '<tbody>';

                   for ( var i=0 ; i<data.transitions_active_to_active.length ; i += 5 )
                   {
                     html += '<tr>';
                     for ( var j=0; j<5 ; j++ )
                     { html += '<td>' + data.transitions_active_to_active[i+j] + '</td>'; }
                     html += '</tr>';
                   }
                   html += '</tbody></table>';
                  }
                  else
                  {
                   html += '<b>No Aborted transitions Active => Active</b><br><br>';
                  }

                  if ( data.transitions_suspended_to_active.length > 0 )
                  {
                   html +=
                    '<b>Aborted transitions Suspended => Active</b>' +
                    ' &nbsp; <table border="1">' +
                    '<thead><tr><th>Transition</th><th>Label</th><th>Created</th><th>Customer Id</th><th>Errors</th></tr></thead>' +
                    '<tbody>';

                   for ( var i=0 ; i<data.transitions_suspended_to_active.length ; i += 5 )
                   {
                     html += '<tr>';
                     for ( var j=0; j<5 ; j++ )
                     { html += '<td>' + data.transitions_suspended_to_active[i+j] + '</td>'; }
                     html += '</tr>';
                   }
                   html += '</tbody></table>';
                  }
                  else
                  {
                   html += '<b>No Aborted transitions Suspended => Active</b><br><br>';
                  }

                } else {
                  html = 'internal__MonthlyServiceChargeStats error(s) : ' + data.errors;
                }

                $("#ajax_target_2").html( html );

                restore_button();
              }
  });

  return false;
}

function getMonthlyServiceChargeStats()
{
  // internal__MonthlyServiceChargeStats

  ajax_call_count++;

  $("#ajax_feedback").append( "<br>... invoking internal__MonthlyServiceChargeStats" );

  $.ajax({
    type:     'POST',
    url:      "/pr/internal/1/ultra/api/internal__MonthlyServiceChargeStats",
    dataType: 'json',
    data:     { stats_date : $("#date_input").val() },
    success:  function callback_monthly_service_charge_stats(data) {

                $("#ajax_feedback").append( "<br>internal__MonthlyServiceChargeStats returned with success="+data.success );

                var html = '';

                if ( data == null ) {
                  html = 'internal__MonthlyServiceChargeStats returned no data :_(<br>';
                } else if ( data.success ) {

                  html = ''; // data.stats;

                  if ( data.stats == null ) {
                  } else {
                    var stats = jQuery.parseJSON( data.stats );

                    if ( stats.count_by_status != 'undefined' ) {
                      html += "<b>Count By Status</b>:<br><table>";
                      for(var state in stats.count_by_status) {
                        html += "<tr><td>" + state + "</td><td align=\"right\"> &nbsp; &nbsp; " + stats.count_by_status[ state ] + "</td></tr>" ;
                      }
                      html += "</table><br>";
                    }

                    if ( stats.errors != 'undefined' ) {
                      html += "<br><b>Errors</b>:<br>";
                      var none = 'none';
                      for(var customer_id in stats.errors) {
                        none = '';
                        var color = 'red';
                        if ( stats.customer[customer_id][0] == 'Active' ) { color = '#004700'; }
                        html +=
                          "customer id : <a href=\"/cgi_util/dig2.html?customer_id=" + customer_id + "\">" + customer_id + "</a> ; " +
                          "state = <font color=\"" + color + "\">" + stats.customer[customer_id][0] + "</font> ; " +
                          "expires = " + stats.customer[customer_id][5] + " ; " +
                          "msisdn = <a href=\"/cgi_util/dig2.html?msisdn=" + stats.customer[customer_id][1] + "\">" + stats.customer[customer_id][1] + "</a> ; " +
                          "stored_value = " + stats.customer[customer_id][2] + " ; " +
                          "balance = " + stats.customer[customer_id][3] + " ; " +
                          "PACKAGED_BALANCE1 = " + stats.customer[customer_id][4] + "<br> &nbsp; ";
                        for ( var i=0 ; i<stats.errors[customer_id].length ; i += 7 )
                        { html += stats.errors[customer_id][i] + "<br>"; }
                      }
                      html += none + "<br>";
                    }
                  }
                } else {
                  html = 'internal__MonthlyServiceChargeStats error(s) : ' + data.errors;
                }

                $("#ajax_target_1").html( html );

                restore_button();
              }
  });

  return false;
}

function restore_button()
{
  ajax_call_count--;

  if ( ajax_call_count == 0 )
  {
    // this should happen when all Ajax requests complete
    $("#div_button").show();
    $("#div_button_loading").hide();
  }
}

function zeroFill( number, width ) {
                  width -= number.toString().length;
                  if ( width > 0 )
                  {
                    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
                  }
                  return number;
                }

</script>
  <!-- html5.js for IE less than 9 -->
  <!--[if lt IE 9]>
  <style type='text/css'>
  </style>
  <![endif]-->
</head>

<body>
  <div class="container-fluid">
    <div class="row-fluid" id='customer_form'>
      <div class='span12 wellx' style='padding-top:20px;'>
        <form name="customernetworkinfo" id="customernetworkinfo" action="/" method="POST">
          <table>
            <tr>
              <td>
                <input type="text" id="date_input" />
              </td>
              <td>
              <div id="div_button">
                <input type="button" value="Search" id="button_search">
              </div>
              <div id="div_button_loading"> <img src='../images/load_hero.gif' alt='' /> </div>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div>
        <table border="1">
          <tr>
            <td>
              <div id="ajax_target_1" class='span12 wellx' style='padding-top:20px;'> </div>
            </td>
          </tr>
          <tr>
            <td>
              <div id="ajax_target_2" class='span12 wellx' style='padding-top:20px;'> </div>
            </td>
          </tr>
          <tr>
            <td>
              <div id="ajax_feedback" class='span12 wellx' style='padding-top:20px;'> </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div><!--/.fluid-container-->

</body>
</html>
