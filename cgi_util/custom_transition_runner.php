<?php

/*
 */

?>
<html>
  <head>
    <title>CUSTOM TRANSITION RUNNER</title>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
      $(function() {
        $('#newExpirationDate').datetimepicker({
          format:'Y-m-d H:i:s'
        });
      });

      var readyText   = 'EXECUTE';
      var workingText = 'WORKING';

      var readyColor   = 'red';
      var workingColor = '#888';

      var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

      function internal__RunCustomTransition()
      {
        var c = confirm("Are you sure you wish to execute?");
        if (!c) return;

        var context    = $('#input_context').val();
        var label      = $('#input_label').val();
        var transition = $('#input_transition').val();
        transition = '{' + states + ', "transitions": [' + transition + '] }';

        console.log(context);
        console.log(label);
        console.log(transition);

        $('#theButton').css('background-color', workingColor);
        $('#theButton').text(workingText);

        $.ajax({
          method: 'POST',
          url: baseURL + '&command=internal__RunCustomTransition',
          data: 'context=' + context + '&label=' + label + '&transition=' + transition,
          success: function(data, status, settings) {
            var response = JSON.parse(data);

            console.log(response);
            $('#input_transition_uuids').val('');

            if ((response.errors && response.errors.length > 0) || (response.success !== true))
            {
              $('#output').hide();
              $('#message').html('<div style="color:red">' + response.errors[0] + '</div>').show();

              setTimeout(function() {
                $('#message').hide()
              }, 5000);

            }
            else
            {
              $('#output').show();
              $('#output').html('Success');
            }

            $('#theButton').css('background-color', readyColor);
            $('#theButton').text(readyText);
          }
        });
      }
    </script>
    <style>
      div { margin: 10px; 0px}
      #container {
        font-family: sans-serif;
        width : 768px;
        margin: 0px auto;
        text-align: center;
      }

      label { font-weight: bold; }
      #theButton {
        background-color: red;
        border: none;
        cursor: pointer;
      }
      #output { margin-top: 32px; }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      th, td {
        text-align: left;
        padding: 8px;
      }

      tr:nth-child(even){background-color: #f2f2f2}
    </style>
  </head>
  <body>
    <div id="container">
      <h1>CUSTOM TRANSITION RUNNER</h1>

      <div>
        <p>
          <label>Context:</label><br />
          <textarea id="input_context" type="text" name="context" cols="64" rows="16"></textarea>
        </p>

        <p>
          <label>Label:</label>
          <input id="input_label" type="text" name="label" size="32" />
        </p>

        <p>
          <label>Transition:</label><br />
          <textarea id="input_transition" type="text" name="transition" cols="64" rows="16"></textarea>
        </p>

        <br />
        <button id="theButton" onclick="internal__RunCustomTransition();">EXECUTE</button>
      </div>
      <!--
      <div>
        <label>Expiration Date:</label>
        <input type="text" id="newExpirationDate">
      </div>
    -->
      <div id="message"></div>
    </div>
    <div id="output" style="display: none;"></div>

<script type="text/javascript">
var states = '"states" : { "Neutral" : { "depart" : [], "arrival" : [] }, "Active" : { "depart" : [], "arrival" : [ "activeMaybeSetActivationDateTime", "sendActivationEmail" ] }, "Suspended" : { "depart" : [], "arrival" : [] }, "Cancelled" : { "depart" : [], "arrival" : [ "logCancellationTransitionInActivationHistory", "disableAccount", "nullifySimAndMsisdn" ] }, "Provisioned" : { "depart" : [], "arrival" : [] }, "Port-In Requested" : { "depart" : [], "arrival" : [] }, "Port-In Denied" : { "depart" : [], "arrival" : [] }, "Pre-Funded" : { "depart" : [], "arrival" : [] }, "Promo Unused" : { "depart" : [], "arrival" : [] } }';
</script>

  </body>
</html>