<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>List Invocations Tool</title>
    <link rel="stylesheet" href="../css/acc-foundation-5.css" />


    <link rel="stylesheet" href="../css/fontello.css" />

    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script src="../js/modernizr.js"></script>

    <?php
    $httpUser = "";
        if (isset($_SERVER['PHP_AUTH_USER'])) $httpUser = $_SERVER['PHP_AUTH_USER'];
    ?>

    <script type="text/javascript">
        var httpUser = "<?php echo $_SERVER['PHP_AUTH_USER']; ?>";
    </script>
</head>
<body>

<nav class="top-bar" data-topbar>
    <div class="title-full-width">
        <h1><span>ACC</span> List Invocations Tool</h1>
    </div>
</nav>


<div id="theContent" class="invocsToolContent">
<div class='top-padding'></div>

<div class="row hide">
    <div class="small-8 column">
        <div class="trayOuter">
            <div class="trayInner trayInnerPad">
                <div class="row">
                    <div class="small-9 column">
                        <h3 class="subheader">By Customer:</h3>
                    </div>
                    <div class="small-3 column align-right pad-top-6">
                        <img src='../images/ajax-30-ffffff.gif' data-bind="visible2:busy1()" class="hide" />
                    </div>
                </div>
                <form data-bind="submit:search1">
                    <div class="row">
                        <div class="small-3 column">
                            <input data-bind="value:customerId, valueUpdate:'afterkeydown'" type="text" placeholder="Customer ID">
                        </div>
                        <div class="small-3 column">
                            <input data-bind="datepicker: startDate, placeholder: true, datepickerOptions: {dateFormat: 'mm-dd-yy'}" type="text" placeholder="Start Date">
                        </div>
                        <div class="small-3 column">
                            <input data-bind="datepicker: endDate, placeholder: true, datepickerOptions: {dateFormat: 'mm-dd-yy'}" type="text" placeholder="End Date">
                        </div>
                        <div class="small-3 column">
                            <button class="button tiny expand" data-bind="disable:!customerId() || !startDate() || !endDate() || busy1() || busy2()">Search</button>
                        </div>
                    </div>
                </form>


            </div>
        </div>

    </div>
    <div class="small-4 column">
        <div class="trayOuter">
            <div class="trayInner trayInnerPad">
                <div class="row">
                    <div class="small-9 column">
                        <h3 class="subheader">By Status:</h3>
                    </div>
                    <div class="small-3 column align-right pad-top-6">
                        <img src='../images/ajax-30-ffffff.gif' data-bind="visible2:busy2()" class="hide" />
                    </div>
                </div>
                <div class="row">
                    <div class="small-8 column">
                        <div class="custom-dropdown small"  data-bind="">
                            <select placeholderSelect="Required" data-bind="options: statusOptions, optionsValue: 'status', optionsText: 'status', value: status" class="custom-dropdown__select"></select>
                        </div>
                    </div>

                    <div class="small-4 column">
                        <button class="button tiny expand" data-bind="disable:!status() || busy1() || busy2(), click:search2">Search</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="row">
    <div class="small-7 column">
        <ul class="button-group">
            <li><a role="button" data-bind="click: function(){ toggleStatus3('OPEN'); }, css: { active: status3()=='OPEN'  }" class="button tiny">OPEN</a></li>
            <li><a role="button" data-bind="click: function(){ toggleStatus3('ERROR'); }, css: { active: status3()=='ERROR'}" class="button tiny">ERROR</a></li>
            <li><a role="button" data-bind="click: function(){ toggleStatus3('REPORTED ERROR'); }, css: { active: status3()=='REPORTED ERROR'}" class="button tiny">REPORTED ERROR</a></li>
            <li><a role="button" data-bind="click: function(){ toggleStatus3('REPORTED MISSING'); }, css: { active: status3()=='REPORTED MISSING'}" class="button tiny">REPORTED MISSING</a></li>
            <li><a role="button" data-bind="click: function(){ toggleStatus3('MISSING'); }, css: { active: status3()=='MISSING'}" class="button tiny">MISSING</a></li>
        </ul>

    </div>
    <div class="small-5 column">
        <div class="row">
            <div class="small-6 column align-right invocLasUpdate no-pad-r">
                Last Updated: <span data-bind="text:lastUpdate"></span><!--br><strong>(auto updates every 30 seconds)</strong-->
            </div>
            <div class="small-4 column no-pad-r">
                <a role="button" class="button tiny expand alert" data-bind="disable:busy3(), click:search3">Refresh</a>
            </div>
            <div class="small-2 column">
                <img src='../images/ajax-30-e5e5e5.gif' data-bind="visible2:busy3()" class="hidex" />
            </div>
        </div>

    </div>


</div>

<div id="invocTableWrap" data-bind="visible2:invocationsArr().length" class="hide">
    <table>
        <thead>
        <tr><th>Status</th><th>MSISDN</th><th>customer_id</th><th>ACC API ID</th><th>ACC_API (COMMAND)</th><th>Started (PST)</th><th>Last Updated (PST)</th><th>Hours Since Update</th><th>Updated By</th><th>Ticket Info</th><th></th><th></th></tr>
        </thead>


        <tbody>
        <!-- ko foreach: invocationsArr -->
        <tr>
            <td data-bind="text:STATUS, css:$parent.dynamicClass($data)"></td>
            <td data-bind="text:MSISDN"></td>
            <td data-bind="text:CUSTOMER_ID"></td>
            <td data-bind="text:ACC_TRANSITION_ID"></td>
            <td data-bind="text:ACC_API + ' (' + COMMAND + ')'"></td>
            <td data-bind="text:START_DATE_TIME"></td>
            <td data-bind="text:LAST_STATUS_DATE_TIME"></td>
            <td data-bind="text:timeDiff"></td>
            <td data-bind="text:LAST_STATUS_UPDATED_BY"></td>
            <td data-bind="text:TICKET_DETAILS"></td>
            <td><button data-bind="showNextRowA:true">Details</button></td>
            <td><button data-bind="showNextRowB:true">Update</button></td>

        </tr>
        <tr class="hide trDark">

            <td colspan="12">


                <div class="invocDetailWrapper2">
                    <div class="row">
                        <div class="small-6 column">
                            <h6 class="subheader">Details:</h6>
                            <table>
                                <tr><td class="label-td p50">ICCID:</td><td data-bind="text:ICCID"></td></tr>
                                <tr><td class="label-td p50">Error Details:</td><td data-bind="text:ERROR_CODE_AND_MESSAGE"></td></tr>
                                <tr><td class="label-td p50">Replied:</td><td data-bind="text:REPLY_DATE_TIME"></td></tr>
                                <tr><td class="label-td p50">Reported:</td><td data-bind="text:REPORTED_DATE_TIME"></td></tr>
                                <tr><td class="label-td p50">Error Date/Time:</td><td data-bind="text:ERROR_DATE_TIME"></td></tr>
                                <tr><td class="label-td p50">Completed:</td><td data-bind="text:COMPLETED_DATE_TIME"></td></tr>
                            </table>

                        </div>
                        <div class="small-6 column">
                            <h6 class="subheader">History:</h6>
                            <table>
                                <thead>
                                <tr>
                                    <th>Event</th><th>Date</th>
                                </tr>
                                </thead>
                                <tbody data-bind="html:$parent.fInvocationsHistory(STATUS_HISTORY)">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




            </td>


        </tr>
        <tr class="hide trDark">
            <td colspan="12">


                <div class="invocDetailWrapper2">
                    <div class="row no-bottom-margin" data-bind="updateEscalation:true">


                        <!-- ko if: (STATUS!='MISSING' && STATUS!='REPORTED MISSING') -->
                        <div class="small-3 column">
                            <div class="custom-dropdown small"  data-bind="">
                                <select placeholderSelect="Required" data-bind="options: $parent.filterOptions($data), optionsValue: 'status', optionsText: 'status', value: $parent.scrap" class="to_status custom-dropdown__select"></select>
                            </div>
                        </div>
                        <div class="small-2 column">
                            <input class="user" type="text" placeholder="User ID" value="<?php echo $httpUser; ?>">
                            <input type="hidden" class="customer_id" data-bind="value:CUSTOMER_ID" />
                        </div>
                        <div class="small-5 column">
                            <input class="ticket_details" type="text" placeholder="Ticket Details">
                        </div>
                        <div class="small-2 column">
                            <button class="tiny">Update</button> <img src="/images/ajax-30-b1ced6.gif" class="hide fright" />
                        </div>
                        <!-- /ko -->


                        <!-- ko if: (STATUS=='MISSING' || STATUS=='REPORTED MISSING') -->

                        <div class="small-2 column">
                            <input class="user" type="text" placeholder="User ID" value="<?php echo $httpUser; ?>">
                            <input type="hidden" class="customer_id" data-bind="value:CUSTOMER_ID" />
                        </div>
                        <div class="small-6 column">
                            <input class="ticket_details" type="text" placeholder="Ticket Details">
                        </div>
                        <div class="small-2 column">
                            <button value="REPORTED MISSING" class="tiny">Report Missing</button> <img src="/images/ajax-30-b1ced6.gif" class="hide fright" />
                        </div>
                        <div class="small-2 column">
                            <button value="RESOLVED" class="tiny">Resolve Issue</button> <img src="/images/ajax-30-b1ced6.gif" class="hide fright" />
                        </div>
                        <!-- /ko -->

                    </div>
                </div>
            </td>
        </tr>

        <!-- /ko -->
        </tbody>
    </table>

</div>


<div class='row hide' data-bind="visible2:!invocationsArr().length && initialized() && !busy1() && !busy2() && !busy3()">
    <div class="small-12 column">
        <div class='errorDiv'>
            No Records Found
        </div>
    </div>

</div>




<script type="text/javascript">
    var vm = null;
</script>

<script src="../js/jquery-2.0.3.js"></script>
<script src="../js/dig2/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../js/vendor/knockout-3.0.0.js"></script>
<script src="../js/vendor/jquery.inputmask.js"></script>
<script src="../js/dig2/ko_bindings.js"></script>
<script src="../js/vendor/moment.js"></script>
<script src="../js/vendor/moment-timezone.js"></script>
<script type="text/javascript">

</script>
<script src="../js/dig2/ko_filters.js"></script>
<script src="../js/dig2/ko.invocations.js"></script>
<script src="../js/foundation.min.js"></script>

<script src="../js/vendor/underscore-min.js"></script>


<script>
    $(document).foundation();
    $(function() {
        vm = new AppViewModel();
        ko.applyBindings(vm);
    });

</script>


</body>
</html>
