#!/usr/bin/perl

BEGIN { push @INC, '/home/ht/www/rgalli2_dev/web/' }

use strict;
use warnings;

use CGI qw/:standard/;
use Cwd;
use Data::Dumper;
use Ultra::Lib::Util::Miscellaneous;

my $dir = getcwd;

my $txt = 'ACCESS DENIED';

if ( $dir =~ /rgalli[23]_dev/ )
{
  my $cgi = CGI->new;

  my $url = 'https://dougmeli:Flora@rgalli2-dev.uvnv.com/pr/customercare/1/ultra/api/customercare__AddCourtesyCash';

  my $data =
  {
    'customercare_event_id' => time,
    'reason'      => 'customercare__AddCourtesyCash.cgi',
    'agent_name'  => 'rgalli2-dev.uvnv.com',
    'customer_id' => $cgi->param('customer_id'),
    'amount'      => $cgi->param('amount')
  };

  my ($response,$timeout) = Ultra::Lib::Util::Miscellaneous::userAgentRequest($url,$data,undef,undef);

  $txt = ( $timeout )
  ?
  'timeout'
  :
  $response->content
  ;
}

print header,
start_html('customercare__AddCourtesyCash'),
Dumper($txt),
end_html; 

__END__

