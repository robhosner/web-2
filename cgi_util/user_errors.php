<?php

?>
<html>
<head>
  <title>User Errors</title>

  <script type="text/javascript" src="/js/show_environment_stage.js"></script>
  <script type="text/javascript" src="../../js/jquery-2.0.3.js"></script>
  <script type="text/javascript" src="../../js/utilities.js"></script>

<script type="text/javascript">
var UserErrors = {
  url: '/ultra_api.php',
  commonParameters: 'bath=rest&partner=internal&version=2&',

  data: null,
  inputKeys: null,
  lastSelected: null,

  reload: function() {
    this.getAPIErrorMessages();
  },

  getAPIErrorMessages: function() {
    $.ajax({
      url: this.url,
      type: 'POST',
      data: this.commonParameters + 'command=internal__GetAPIErrorMessages',
      success: function(res) {
        var data = JSON.parse(res);
        UserErrors.data = data.messages;
        UserErrors.prepareInputs();
      },
      error: function(res) {
        $('#message').empty().removeClass().addClass('bad');
        $('#message').text('Error retrieving error messages ' + errorCode);
      }
    });
  },

  prepareInputs: function() {
    if (this.data != null && this.data.length > 0)
    {
      $('#updateErrorFormClone .inputs').empty();
      this.inputKeys = [];
      for (var key in this.data[0])
      {
        if (key.indexOf('_MESSAGE') != -1)
        {
          this.inputKeys.push(key);
          $('#updateErrorFormClone .inputs').append(
            '<span><label>' + key + '</label>' + 
            '<input type="text" id="' + key + 'Clone" name="' + key + '" />' +
            '</span>');
        }
      }

      $('#updateErrorSelect').empty();
      for (var i = 0; i < this.data.length; i++)
      {
        $('#updateErrorSelect').append('<tr id="' + this.data[i].ERROR_CODE + '" onclick="UserErrors.updateInputs(this, ' + i + ');"><td>' + 
          this.data[i].ERROR_CODE + '</td><td>' + this.data[i].EN_MESSAGE +
          '</td></tr>');

        if (window.location.hash == '#' + this.data[i].ERROR_CODE)
          UserErrors.updateInputs($('tr#' + this.data[i].ERROR_CODE), i);
      }
    }
  },

  updateInputs: function(self, index) {
    console.log(self);

    $('#updateErrorForm').remove();
    $('<tr id="updateErrorForm"><td colspan=2>' + $("#updateErrorFormClone").html() + '</td></tr>').insertAfter(self);

    $('h2').text(this.data[index].ERROR_CODE);
    window.location.hash = this.data[index].ERROR_CODE;
    this.lastSelected = this.data[index].ERROR_CODE;

    for (var i = 0; i < this.inputKeys.length; i++)
    {
      $('table #' + this.inputKeys[i] + 'Clone').attr('id', this.inputKeys[i]);
      $('table #messageClone').attr('id', 'message');

      var val = '';
      if (this.data[index][this.inputKeys[i]] != null)
      {
        val = convertUnicodeFromJavaEscape(this.data[index][this.inputKeys[i]]);
      }
      else
        val = '';

      $('#' + this.inputKeys[i]).val(val);
    }
  },

  updateAPIErrorMessage: function() {
    var errorCode = this.lastSelected;
    var inputParameters = '&error_code=' + errorCode;

    for (var i = 0; i < this.inputKeys.length; i++)
    {
      // no english
      if (this.inputKeys[i] == 'EN_MESSAGE') continue;
      
      if (i)
        inputParameters += '&';

      var msg = $('#' + this.inputKeys[i]).val();
      if (msg.length == 0) msg = 'NULL';

      var escaped = convertUnicodeToJavaEscape(msg);

      inputParameters += this.inputKeys[i].toLowerCase() + '=' + escaped;
    }

    $.ajax({
      url: this.url,
      type: 'POST',
      data: this.commonParameters + inputParameters + '&command=internal__UpdateAPIErrorMessage',
      success: function(res) {
        $('#message').empty().removeClass().addClass('good');
        $('#message').text(errorCode + ' saved.');
        // UserErrors.reload();
      },
      error: function(res) {
        $('#message').empty().removeClass().addClass('bad');
        $('#message').text('Error updating ' + errorCode);
      }
    });
  }
};

$(function() {
  UserErrors.reload();
});
</script>

<style>
body { color: #111; font-family: sans-serif; }

.inputs { margin-top: 16px; }

#updateErrorForm {}
#updateErrorForm button {}
#updateErrorForm input {
  padding: 4px;
  width: 1024px;
}
#updateErrorForm span {
  display: block;
  margin-bottom: 8px;
}
#updateErrorForm span label {
  display: inline-block;
  width: 128px;
}

.inputs {}

#message {
  margin-bottom: 8px;
  padding: 4px;
  width: 512px;
}
#message.good { border: 1px solid green; }
#message.bad { border: 1px solid red; }

table#updateErrorSelect { margin-top: 16px; width: 100%; }
tr#updateErrorForm { background-color: #333; color: #FEFEFE; }
tr#updateErrorForm td { padding: 16px; }
tr { padding: 4px; }
tr:nth-child(even) {
  background-color: #EEE;
}
tr:hover { background-color: lightblue; cursor: pointer; }
</style>

</head>
<body>

<h1>User Errors</h1>

  <div id="updateErrorFormClone" style="display: none;">
    <h2>Select a message to edit.</h2>
    <div id="messageClone" class=""></div>
    <button onclick="UserErrors.updateAPIErrorMessage();">Save</button>

    <div class="inputs">
    </div>
  </div>

<table id="updateErrorSelect">
</table>

</body>
</html>
