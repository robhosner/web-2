<?php

?>
<html>
<head>
<title>Bulk Provision</title>

<style>
p { clear: both; padding-bottom: 8px; }
p label { float: left; width: 196px; }
p label span { display: block; font-size: small; color: gray; }
table#output {
  background-color: #333;
  border-collapse: collapse;
  color: #FFF;
  width: 100%;
}

input[type="text"], textarea { border: 1px solid gray; padding: 4px; }
table#output td { padding: 4px; }

span.error { color: red; }
span.success { color: green; }
</style>

<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script type="text/javascript" src="../js/jquery-2.0.3.js"></script>
<script type="text/javascript">
$(function() {
  Main.reset();
});

var Main = {
  queue: [],

  reset: function() {
    this.queue = [];

    $('#output').hide();
    $('#output').empty();

    this.enableSubmit();
  },

  disableSubmit: function() {
    $('#submit_button').prop('disabled', true);
    $('#submit_button').text('Executing');
  },

  enableSubmit: function() {
    $('#submit_button').prop('disabled', false);
    $('#submit_button').text('Submit');
  },

  run: function() {
    if (!confirm("Execute?"))
    {
      alert('Terminated');
      return;
    }

    this.disableSubmit();

    this.reset();
    this.validateICCIDRange();
  },

  validateICCIDRange: function(iccid_start, iccid_end) {
    var iccid_start = $('#iccid_start').val();
    var iccid_end = $('#iccid_end').val();

    try
    {
      if (!iccid_start.length) throw { message: 'Enter a valid start ICCID.' };
      if (!iccid_end.length) throw { message: 'Enter a valid end ICCID.' };
    }
    catch (e)
    {
      alert(e.message);
      this.enableSubmit();
      return;
    }

    $.ajax({
      type: 'POST',
      url: "/pr/inventory/2/ultra/api/inventory__GetSIMRangeInfo",
      dataType: 'json',
      data: 'startICCID=' + iccid_start + '&endICCID=' + iccid_end,
      success: function(data) {
        if (!data.errors.length)
        {
          var success = true;
          var sim_data = data.sim_data;
          for (var i = 0; i < sim_data.length; i++)
          {
            try
            {
              if (sim_data[i].sim_activated == 1)
                throw { message: 'is activated' };

              if (sim_data[i].sim_hot != 1)
                throw { message: 'is not hot' };

              if (sim_data[i].customer != null)
                throw { message: 'is assigned to customer' };
            }
            catch (e)
            {
              alert('ERROR: ' + sim_data[i].iccid_full + ' ' + e.message);
              this.enableSubmit();
              return;
            }
            
            Main.queue.push({ 'ICCID': sim_data[i].iccid_full });
          }
          
          Main.validateOtherInputs();
        }
      }
    });
  },

  validateOtherInputs: function() {
    var iccid_start = $('#iccid_start').val();
    var iccid_end = $('#iccid_end').val();

    var zipcodes = $('#zipcodes').val();
    var plan = $('#plan').val();
    var activation_masteragent = $('#activation_masteragent').val();
    var activation_distributor = $('#activation_distributor').val();
    var activation_agent = $('#activation_agent').val();
    var activation_userid = $('#activation_userid').val();
    var customerFirstName = $('#customerFirstName').val();
    var customerLastName = $('#customerLastName').val();
    var customerEMail = $('#customerEMail').val();

    try
    { 
      if (!zipcodes.length)
        throw { message: 'Zip Codes cannot be empty.' }

      if (zipcodes.indexOf(',') != -1) zipcodes = zipcodes.split(',');
      else if (zipcodes.indexOf(';') != -1) zipcodes = zipcodes.split(';');
      else
        zipcodes = zipcodes.split("\n");

      for (var k = 0; k < zipcodes.length; k++)
      {
        if (!zipcodes[k].length)
        {
          zipcodes.splice(k, 1);
          k--;
        }
      }

      if (/(L[12345]9)$/.test(plan) == false)
        throw { message: 'Invalid Plan' };

      if (isNaN(activation_masteragent) || !activation_masteragent.length)
        throw { message: 'Activation Master Agent must be a number.' };

      if (isNaN(activation_distributor) || !activation_distributor.length)
        throw { message: 'Activation Distributor must be a number.' };

      if (isNaN(activation_agent ) || !activation_agent.length)
        throw { message: 'Activation Agent must be a number.' };

      if (isNaN(activation_userid) || !activation_userid.length)
        throw { message: 'Activation User ID must be a number.' };

      if (!customerFirstName.length)
        throw { message: 'Customer First Name cannot be empty.' };

      if (!customerLastName.length)
        throw { message: 'Customer Last Name cannot be empty.' };

      if (!customerEMail.length)
        throw { message: 'Customer EMail cannot be empty.' };
    }
    catch (e)
    {
      alert(e.message);
      this.enableSubmit();
      return;
    }

    var j = 0;
    for (var i = 0; i < Main.queue.length; i++)
    {
      Main.queue[i]['target_plan'] = plan;
      Main.queue[i]['loadPayment'] = 'NONE';
      
      Main.queue[i]['activation_masteragent'] = activation_masteragent;
      Main.queue[i]['activation_distributor'] = activation_distributor;
      Main.queue[i]['activation_agent'] = activation_agent;
      Main.queue[i]['activation_userid'] = activation_userid;
      
      Main.queue[i]['customerFirstName'] = customerFirstName;
      Main.queue[i]['customerLastName'] = customerLastName;
      Main.queue[i]['customerEMail'] = customerEMail;

      Main.queue[i]['customerZip'] = zipcodes[j];
      j = (j + 1 == zipcodes.length) ? 0 : j + 1;
    }

    $('#output').show();
    this.requestProvisionNewCustomerAsync();
  },

  requestProvisionNewCustomerAsync: function() {
    var queueItem = this.queue.shift();
    if (queueItem == undefined)
    {
      $('#output').append('<tr><td>Completed</td></tr>');
      this.enableSubmit();
      return;
    }

    var data = '' +
    '&ICCID=' + queueItem['ICCID'] +
    '&targetPlan=' + queueItem['target_plan'] +
    '&loadPayment=' + queueItem['loadPayment'] +
    '&activation_masteragent=' + queueItem['activation_masteragent'] +
    '&activation_distributor=' + queueItem['activation_distributor'] +
    '&activation_agent=' + queueItem['activation_agent'] +
    '&activation_userid=' + queueItem['activation_userid'] +
    '&customerFirstName=' + queueItem['customerFirstName'] +
    '&customerLastName=' + queueItem['customerLastName'] +
    '&customerEMail=' + queueItem['customerEMail'] +
    '&customerZip=' + queueItem['customerZip'];

    $.ajax({
      type: 'POST',
      url: "/pr/provisioning/1/ultra/api/provisioning__requestProvisionNewCustomerAsync",
      dataType: 'json',
      data: data,
      success: function(data) {
        console.log(data);

        var msg = (data.errors.length) ? '<span class="error">' + data.errors[0] : '<span class="success">SUCCESS';
        $('#output').append('<tr><td>' + queueItem['ICCID'] + ' : ' + msg + '</span></td></tr>');

        Main.requestProvisionNewCustomerAsync();
      }
    });
  }
}
</script>
</head>
<body>
<div id="container">

<table id="output" style="display: none;">
</table>

<h1>Bulk Provision</h1>

<p>
  <label>Zip Codes<span>(New Line, comma, or semi-colon) Delimited</span></label>
  <textarea id="zipcodes" name="zipcodes" rows="10" cols="40"></textarea>
</p>

<p>
  <label>ICCID Range<span>18 to 19 digits</label>
  <input type="text" id="iccid_start" name="iccid_start" length="19" size="24" placeholder="Start" />
  <input type="text" id="iccid_end" name="iccid_end" length="19" size="24" placeholder="End" />
</p>

<p>
  <label>Plan<span>L[12345]9</span></label>
  <select id="plan" name="plan" style="width: 128px;">
    <option value="L19">L19</option>
    <option value="L29">L29</option>
    <option value="L39">L39</option>
    <option value="L49">L49</option>
    <option value="L59">L59</option>
  </select>
</p>

<p>
  <label>Activation Master Agent<span>Numeric</span></label>
  <input type="text" id="activation_masteragent" name="activation_masteragent" />
</p>

<p>
  <label>Activation Distributor<span>Numeric</span></label>
  <input type="text" id="activation_distributor" name="activation_distributor" />
</p>

<p>
  <label>Activation Agent<span>Numeric</span></label>
  <input type="text" id="activation_agent" name="activation_agent" />
</p>

<p>
  <label>Activation User ID<span>Numeric</span></label>
  <input type="text" id="activation_userid" name="activation_userid" />
</p>

<p>
  <label>Customer Name<span>First / Last</span></label>
  <input type="text" id="customerFirstName" name="customerFirstName" placeholder="First" value="FirstName" />
  <input type="text" id="customerLastName" name="customerLastName" placeholder="Last" value="LastName" />
</p>

<p>
  <label>Customer E-Mail</label>
  <input type="text" id="customerEMail" name="customerEMail" value="null@null" />
</p>

<button id="submit_button" onclick="Main.run();">Submit</button>

</div>
</body>
</html>