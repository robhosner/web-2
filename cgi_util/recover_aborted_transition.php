<?php

/*
 */

?>
<html>
  <head>
    <title>RECOVER ABORTED TRANSITION</title>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
      $(function() {
        $('#newExpirationDate').datetimepicker({
          format:'Y-m-d H:i:s'
        });
      });

      var readyText   = 'EXECUTE';
      var workingText = 'WORKING';

      var readyColor   = 'red';
      var workingColor = '#888';

      var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

      function internal__RecoverAbortedTransition()
      {
        var c = confirm("Are you sure you wish to execute?");
        if (!c) return;

        var transition_uuid = $('#input_transition_uuid').val();

        $('#theButton').css('background-color', workingColor);
        $('#theButton').text(workingText);

        $.ajax({
          method: 'GET',
          url: baseURL + '&command=internal__RecoverAbortedTransition',
          data: 'transition_uuid=' + transition_uuid,
          success: function(data, status, settings) {
            var response = JSON.parse(data);

            console.log(response);

            if ((response.errors && response.errors.length > 0) || (response.success !== true))
            {
              $('#output').hide();
              $('#message').html('<div style="color:red">' + response.errors[0] + '</div>').show();

              setTimeout(function() {
                $('#message').hide()
              }, 5000);

            }
            else
            {
              $('#output').show();
              $('#output').text(data);
            }

            $('#theButton').css('background-color', readyColor);
            $('#theButton').text(readyText);
          }
        });
      }
    </script>
    <style>
      div { margin: 10px; 0px}
      #container {
        font-family: sans-serif;
        width : 768px;
        margin: 0px auto;
        text-align: center;
      }

      label { font-weight: bold; }
      #theButton {
        background-color: red;
        border: none;
        cursor: pointer;
      }
      #output { margin-top: 32px; }
    </style>
  </head>
  <body>
    <div id="container">
      <h1>RECOVER ABORTED TRANSITION</h1>
      <div>
        <label>TRANSITION UUID:</label>
        <input id="input_transition_uuid" type="text" name="customer_id" value="" size="48" />
        <button id="theButton" onclick="internal__RecoverAbortedTransition();">EXECUTE</button>
      </div>
      <!--
      <div>
        <label>Expiration Date:</label>
        <input type="text" id="newExpirationDate">
      </div>
    -->
      <div id="message"></div>
      <div id="output" style="display: none;">
      </div>
    </div>
  </body>
</html>