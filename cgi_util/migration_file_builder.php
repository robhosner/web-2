<?php
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>Migration Tool</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link href="../styles/bootstrap2/css/bootstrap.min.css" rel="stylesheet">
  <link href="../styles/bootstrap2/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="../styles/modals.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="../styles/datepicker.css" rel="stylesheet" type="text/css" />

  <script type="text/javascript" src="/js/show_environment_stage.js"></script>
  <script language="javascript" src="../js/modernizr_custom.min.js"></script>
  <script language="javascript">

  Modernizr.load([
                   {
                   test: Modernizr.mq('only all'),
                   nope: '../styles/oldIE.css'
                   },
                   {
                   load: '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',
                   complete: function () { if ( !window.jQuery ) { Modernizr.load('js/jquery-1.7.2.min.js'); }}
                   },
                   {
                   load: [
                     "../js/jquery-ui-1.8.23.custom.min.js",
                     "../styles/bootstrap2/js/bootstrap.min.js",
                     "../styles/bootstrap2/js/bootstrap-datepicker.js",
                       "../js/sitewide.js"
                     ],

                   complete: function () {
                       $(document).ready(function() {

// instantiate Date object ( default = today )
var l_xToday = new Date();
var l_sToday = zeroFill(l_xToday.getMonth()+1,2) + '-' + zeroFill(l_xToday.getDate(),2) + '-' + l_xToday.getFullYear();

var l_xTomorrow = new Date();
l_xTomorrow.setTime(l_xTomorrow.getTime() + (24 * 60 * 60 * 1000));
var l_sTomorrow = zeroFill(l_xTomorrow.getMonth()+1,2) + '-' + zeroFill(l_xTomorrow.getDate(),2) + '-' + l_xTomorrow.getFullYear();

// Date cell hidden initially
$('#migration_date').hide();

// if Migration then date selector should default to today
$('#migration_date').val(l_sToday).datepicker({
  format: 'mm-dd-yyyy'
});

$("#migration_date").focusout(function() {

  $('#migration_check_caption').hide();
  $('#premigration_check_caption').hide();

  check_date( $(this).val() , l_sTomorrow , l_sToday );

});

$('#migration_caption').hide();
$('#premigration_check_caption').hide();
$('#migration_check_caption').hide();
$('#default_check_caption').show();

// User selected 'migration_type'
$("#migration_type").change(function() {

  $('#migration_check_caption').hide();
  $('#premigration_check_caption').hide();
  $('#migration_caption').hide();

  // if PreMigration then date selector should default to tomorrow
  if ( $(this).val() == 'PRE_MIGRATION' )
  {
    // populate date HTML field
    $('#migration_date').val(l_sTomorrow).datepicker({
      format: 'mm-dd-yyyy'
    });
  }
  else
  {
    // populate date HTML field
    $('#migration_date').val(l_sToday).datepicker({
      format: 'mm-dd-yyyy'
    });

    // if Migration, flash a big red sign saying "MIGRATION FILE. THIS WILL LOCK THE CUSTOMER - ARE YOU SURE?"
    $('#migration_caption').show();
  }

  // show date
  $('#migration_date').show();

  check_date( $('#migration_date').val() , l_sTomorrow , l_sToday );
});

// build button click event
$('#build_button').click(function() {
  submit_build();
  return false;
});

                       });
                     }
                   },

                   {
                   test : window.JSON,
                   nope : '../js/json2.js'
                   }
                   ]);

// validates migration_type and date
function check_date( date_value , l_sTomorrow , l_sToday )
{
  if ( $("#migration_type").val() == 'PRE_MIGRATION' )
  {
    // if PreMigration and date selector is not tomorrow, flash a big red sign saying "YOU ARE USING A DATE THAT IS NOT TYPICAL FOR PREMIGRATION - ARE YOU SURE?"

    if ( date_value != l_sTomorrow )
    {
      $('#premigration_check_caption').show();
    }
    else
    {
      $('#premigration_check_caption').hide();
    }
  }
  else
  {
    // if Migration and date selector is not today, flash a big red sign saying "YOU ARE SELECTING A MIGRATION FILE THAT ISNT FOR TODAY. THIS WILL LOCK THE CUSTOMER. -- ARE YOU SURE?"

    if ( date_value != l_sToday )
    {
      $('#migration_check_caption').show();
    }
    else
    {
      $('#migration_check_caption').hide();
    }
  }
}

// returns a compact MSISDN list
function get_msisdn_list()
{
  var msisdn_list = '';

  var textArea = document.getElementById("msisdn_list");

  var lines = textArea.value.split("\n");

  // loop through MSISDNs
  for( var i=0 ; i<lines.length ; i++)
  {
    if ( lines[i] != '' )
    {
      if ( /^1\d{10}$/.test( lines[i] ) )
      {
        msisdn_list = msisdn_list + lines[i].substring(1,11);
      }
      else
      {
        msisdn_list = msisdn_list + lines[i];
      }
    }
  }

  return msisdn_list;
}

// validate MSISDN list
function validate_msisdn_list()
{
  var textArea = document.getElementById("msisdn_list");

  var valid = true;

  // do we have MSISDNs?
  if ( textArea != '' )
  {
    var lines = textArea.value.split("\n");

    // loop through MSISDNs
    for( var i=0 ; i<lines.length ; i++)
    {
      if ( lines[i] != '' )
      {
        // validate a MSISDN
        if ( ! /^1?\d{10}$/.test( lines[i] ) )
        {
          // errors textarea
          var errors = document.getElementById('error_list');

          var errors_array = errors.value.split("\n");

          // append an error
          errors_array[ errors_array.length ] = 'MSISDN '+lines[i]+' IS INVALID';

          errors.value = errors_array.join("\n");

          valid = false;
        }
      }
    }
  }

  return valid;
}

// submit build
function submit_build()
{
  if ( $('#migration_type').val() == '' )
  {
    alert("Please choose a type");
  }
  else if (document.getElementById('CHECK').checked)
  {
    // first check
    if (confirm("Are you sure?"))
    {
      var errors = document.getElementById('error_list');

      errors.value = '';

      // Validate all MSISDNs in list to be formatted properly, kick back an error if invalid values
      if ( validate_msisdn_list() )
      {
        // Generate a file to be downloaded in the browser 
 
        $( "#mainform" ).submit();
      }
    }
  }
  else
  {
    alert("You are not sure. Please check the checkbox above when you are.")
  }

  // Uncheck for the next attempt
  $("#CHECK").prop("checked", false);
  $("#CHECK").attr("checked", false);
}

</script>
  <!-- html5.js for IE less than 9 -->
  <!--[if lt IE 9]>
  <style type='text/css'>
  </style>
  <![endif]-->
</head>

<body>
  <div class="container-fluid">
    <div class="row-fluid" id='main_id' name='main_id'>
      <div class='span12 wellx' style='padding-top:20px;'>
        <form name="mainform" id="mainform" action="/ultra_api.php?bath=rest&partner=internal&version=2&command=internal__GetMigrationFile&format=mime&mime=text" method="POST" target="_blank">
          <table>
            <tr>
              <td>
                <select name="migration_type" id="migration_type">
                  <option value="" SELECTED    >-- choose migration type --</option>
                  <option value="PRE_MIGRATION">PreMigration File          </option>
                  <option value="MIGRATION"    >Migration File             </option>
                </select>
              </td>
              <td>
                <input type="text" id="migration_date" name="migration_date"/>
              </td>
            </tr>
            <tr>
              <td>
                Additional MSISDNs
              </td>
              <td>
                Errors
              </td>
            </tr>
            <tr>
              <td>
                <textarea rows="20" cols="120" name="msisdn_list" id="msisdn_list"></textarea>
              </td>
              <td>
                <textarea rows="20" cols="120" name="error_list"  id="error_list" readonly></textarea>
              </td>
            </tr>
            <tr>
              <td colspan=2>
                <b id="migration_caption">MIGRATION FILE. THIS WILL LOCK THE CUSTOMER - ARE YOU SURE?</b>
              </td>
            </tr>
            <tr>
              <td colspan=2>
                <b id="premigration_check_caption">YOU ARE USING A DATE THAT IS NOT TYPICAL FOR PREMIGRATION - ARE YOU SURE?</b>
                <b id="migration_check_caption">YOU ARE SELECTING A MIGRATION FILE THAT IS NOT FOR TODAY. THIS WILL LOCK THE CUSTOMER - ARE YOU SURE?</b>
              </td>
            </tr>
            <tr>
              <td>
                <input type="checkbox" id="CHECK" name="CHECK" value="CHECK">
                <b id="default_check_caption">I know what I am doing!</b>
              </td>
            </tr>
            <tr>
              <td>
                <input type="button" value="Build" id="build_button" name="build_button">
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div>
        <table>
          <tr>
            <td>
              <div id="ajax_div"> </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div><!--/.fluid-container-->
</body>
</html>
