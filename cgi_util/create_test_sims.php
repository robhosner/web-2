<?php

class SimCreator
{
  public $iccids = [];

  private $prefix;
  private $date;
  private $time;
  protected $batchAmount = 1;
  protected $storedValue = 19;
  protected $productType = 'PURPLE';
  protected $brand   = 1;
  protected $offerId = 1;

  public function __construct()
  {
    $this->prefix = '1010101';
    $this->date   = date('md');
    $this->time   = time();
  }

  public function printIccids()
  {
    $br = '<br />';

    print 'Batch Amount: ' . $this->batchAmount . $br;
    print 'Stored Value: ' . $this->storedValue . $br;
    print 'Product Type: ' . $this->productType . $br;
    print 'Brand: '        . $this->brand . $br;

    print "$br NUMBER $br FULL $br ACT CODE $br $br";

    foreach ($this->iccids as $iccid)
    {
      print $iccid['number']   . $br;
      print $iccid['full']     . $br;
      print $iccid['act_code'] . $br;

      print $br;
    }
  }

  public function setBatchAmount($amount)
  {
    $this->batchAmount = $amount;
  }

  public function setBrand($brand)
  {
    if (!in_array($brand, [1, 2, 3]))
      $brand = 1;

    $this->brand = $brand;
  }

  public function setOfferId($offerId)
  {
    $this->offerId = $offerId;
  }

  public function setProductType($type)
  {
    if ( ! in_array($type, array('PURPLE', 'ORANGE')))
      $type = 'PURPLE';

    $this->productType = $type;
  }

  public function setStoredValue($value)
  {
    $this->storedValue = $value;
  }

  public function generateIccidData()
  {
    $j1 = 10;
    $j2 = 1;

    for ($i = 0; $i < $this->batchAmount; $i++)
    {
      $iccid['number']   = $this->prefix . rand(1111, 4444) . $this->date . $j1 . $j2;
      $iccid['full']     = SimCreator::luhnenize($iccid['number']);
      $iccid['act_code'] = ($this->productType == 'ORANGE') ? $this->generateActCode() : 'NULL';

      $this->iccids[] = $iccid;

      if ($j2 > 8) { $j1++; $j2 = 1; }
      else $j2++;
    }
  }

  public function insertSimBatch()
  {
    $simBatchId = 'DEV' . rand(11111, 99999);

    $sql = "INSERT INTO HTT_INVENTORY_SIMBATCHES (ICCID_BATCH_ID,SKU,UPC,CHANNEL,TMOPROFILESKU,EXPIRES_DATE)
      VALUES ('$simBatchId','TEST {$this->productType}','TEST','NA','TEST','Jan  7 2019 12:00:00:000AM')";

    if ( ! run_sql_and_check_result($sql))
    {
      print "SQL IF FAILING";
      die;
    }

    return $simBatchId;
  }

  public function insertIccids($simBatchId)
  {
    foreach ($this->iccids as $iccid)
    {
      $query = "INSERT INTO HTT_INVENTORY_SIM
      (
        ICCID_NUMBER,
        SIM_ACTIVATED,
        INVENTORY_STATUS,
        LAST_CHANGED_DATE,
        CREATED_BY_DATE,
        CREATED_BY,
        ICCID_BATCH_ID,
        ICCID_FULL,
        OTHER,
        GLOBALLY_USED,
        PRODUCT_TYPE,
        SIM_HOT,
        MVNE,
        BRAND_ID,
        STORED_VALUE,
        ACT_CODE,
        OFFER_ID
      ) VALUES (
        '{$iccid['number']}',
        0,
        'AT_FOUNDRY',
        GETUTCDATE(),
        GETUTCDATE(),
        'DEVELOPERS',
        '$simBatchId',
        '{$iccid['full']}',
        'ALWAYSOK',
        0,
        '{$this->productType}',
        1,
        2,
        {$this->brand},
        {$this->storedValue},
        {$iccid['act_code']},
        {$this->offerId}
      )";

      if ( ! run_sql_and_check_result($query))
      {
        print "SQL IF FAILING";
        die;
      }
    }
  }

  public function saveIccidsToDB()
  {
    $simBatchId = $this->insertSimBatch();
    $this->insertIccids($simBatchId);
  }

  public static function generateActCode()
  {
    // generate random 10 digit number
    // first digit must be > 1
    $act_code_10 = substr(time(), 0, 5) . rand(11111, 99999);
    $act_code_10[0] = rand(2, 9);

    // 11th digit
    $check_digit = SimCreator::luhn_checksum($act_code_10 * 10);
    $check_digit = $check_digit == 0 ? $check_digit : (10 - $check_digit);

    return $act_code_10 . $check_digit;
  }

  public static function validateActCode($act_code)
  {
    $act_code_10 = substr($act_code, 0, - 1);
    $check_digit = SimCreator::luhn_checksum($act_code_10 * 10);
    $check_digit = $check_digit == 0 ? $check_digit : (10 - $check_digit);
    return ("$act_code" == "$act_code_10" . "$check_digit");
  }

  public static function luhn_checksum($number)
  {
    $even = 0;
    $sum  = 0;

    foreach (str_split(strrev($number)) as $n)
      $sum += ($even = ! $even) ? $n : (($n < 5) ? $n * 2 : ($n * 2) - 9);
    return $sum % 10;
  }

  public static function luhnenize($iccid)
  {
    return (strlen($iccid) == 18) ? SimCreator::luhn_append($iccid) : $iccid;
  }

  public static function luhn_append($input)
  {
    $check_digit = SimCreator::luhn_checksum($input * 10);
    $check_digit = $check_digit == 0 ? $check_digit : (10 - $check_digit);
    return "$input$check_digit";
  }
}


if (isset($_POST['call']))
{
  set_include_path(get_include_path() . ':/home/ht/www/james_dev/web/');
  require 'db.php';

  teldata_change_db();

  $simCreator = new SimCreator();

  $simCreator->setBatchAmount($_POST['batch_amount']);
  $simCreator->setBrand($_POST['brand']);
  $simCreator->setOfferId($_POST['offer_id']);
  $simCreator->setProductType($_POST['product_type']);
  $simCreator->setStoredValue($_POST['stored_value']);

  $simCreator->generateIccidData();
  $simCreator->saveIccidsToDB();

  echo '<p><a href="' . $_SERVER['REQUEST_URI'] . '">BACK TO FORM</a></p>';

  $simCreator->printIccids();

  exit;
}

?>
<html>
<head>
  <title>CREATE TEST SIMS</title>
  <style>
    body { font-family: sans-serif; }
    p label { display: inline-block; width: 160px; }
  </style>
</head>
<body>

<h3>CREATE TEST SIMS</h3>


<form method="post" action="">
  <input type="hidden" name="call" value="1" />

  <p>
    <label>BATCH AMOUNT</label>
    <select name="batch_amount">
    <?php for ($i = 1; $i <= 20; $i++): ?>
      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
    <?php endfor; ?>
    </select>
  </p>
  <p>
    <label>PRODUCT TYPE</label>
    <select name="product_type">
      <option value="PURPLE">PURPLE</option>
      <option value="ORANGE">ORANGE</option>
    </select>
  </p>
  <p>
    <label>STORED VALUE<br><small>for orange</small></label>
    <input type="text" name="stored_value" value="19" />
  </p>
  <p>
    <label>OFFER ID<br><small>for orange</small></label>
    <select name="offer_id">
      <option value="1">NINETEEN</option>
      <option value="2">TWENTY_NINE</option>
      <option value="3">THIRTY_FOUR</option>
      <option value="4">THIRTY_NINE</option>
      <option value="5">FORTY_FOUR</option>
      <option value="6">FORTY_NINE</option>
      <option value="7">FIFTY_NINE</option>
      <option value="8">TWENTY_NINE 3 mos</option>
      <option value="9">TWENTY_NINE 6 mos</option>
      <option value="10">TWENTY_NINE 12 mos</option>
      <option value="11">STWENTY_NINE</option>
      <option value="12">SFORTY_FOUR</option>
      <option value="13">MINT_ONE_MONTH_S</option>
      <option value="14">MINT_THREE_MONTHS_S</option>
      <option value="15">MINT_SIX_MONTHS_S</option>
      <option value="16">MINT_TWELVE_MONTHS_S</option>
      <option value="17">MINT_ONE_MONTH_M</option>
      <option value="18">MINT_THREE_MONTHS_M</option>
      <option value="19">MINT_SIX_MONTHS_M</option>
      <option value="20">MINT_TWELVE_MONTHS_M</option>
      <option value="21">MINT_ONE_MONTH_L</option>
      <option value="22">MINT_THREE_MONTHS_L</option>
      <option value="23">MINT_SIX_MONTHS_L</option>
      <option value="24">MINT_TWELVE_MONTHS_L</option>
    </select>
  </p>
  <p>
    <label>BRAND</label>
    <select name="brand">
      <option value="1">ULTRA</option>
      <option value="2">UNIVISION</option>
      <option value="3">MINT</option>
    </select>
  </p>

  <input type="submit" value="CREATE" />
</form>

</body>
</html>