<?php

namespace InComm\Lib\Api\Partner\InComm;

class valins extends \InComm\Lib\Api\Partner\inComm
{
  /**
   * valins
   * inComm value insertion
   * @return object Result
   */
  public function incomm__valins()
  {
    // initialize
    list($Version, $Date, $Time, $TimeZone, $IncommRefNum, $UPC, $MIN, $Amount, $UserID, $Password, $MerchName, $StoreID, $TermID, $Zip, $SourceInfo) = $this->getInputValues();
    $ServiceProviderRefNum = \Ultra\Lib\Incomm\generateSrcRefNum();

    try
    {
      // check API login
      if ( ! $this->validateCredentials($UserID, $Password))
      {
        $RespCode = 10040;
        $RespMsg = 'Invalid UserID or Password';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid login information', 'SE0007');
      }

      // get subscriber
      teldata_change_db();
      if ( ! $subscriber = \Ultra\Lib\Incomm\decodeAccountNumber($MIN))
      {
        $RespCode = 10022;
        $RespMsg = 'Invalid MIN';
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');
      }

      // check state
      if (in_array($subscriber->plan_state, array(STATE_CANCELLED, STATE_PORT_IN_DENIED)))
      {
        $RespCode = 10059;
        $RespMsg = 'Invalid Account';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
      }

      if (checkExcessBalance($subscriber, $Amount * 100))
      {
        $RespCode = 10070;
        $RespMsg = 'Max Balance Limitation';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet', 'CH0001');
      }

      // verify UPC
      if ( ! verifyProviderSku('INCOMM', $UPC))
      {
        $RespCode = 10018;
        $RespMsg = 'Invalid UPC';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU', 'VV0006');
      }

      // verify UPC amount
      if ( ! $destination = verifyProviderSkuAmount('INCOMM', $UPC, $Amount * 100))
      {
        $RespCode = 10021;
        $RespMsg = 'Invalid Amount';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid Amount', 'VV0115');
      }

      // verify mapped destination
      if ( ! $destination = verifyAmountDestination($subscriber, $destination, $Amount * 100))
      {
        $RespCode = 10021;
        $RespMsg = 'Invalid Amount';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid amount', 'VV0115');
      }
      dlog('', 'authorized $%.2f into %s for customer %d', $Amount, $destination, $subscriber->customer_id);

      // insert a new row into HTT_BILLING_ACTIONS for processing by the billing runner
      $product = $destination == 'MONTHLY' ? 'ULTRA_PLAN_RECHARGE' : 'ULTRA_ADD_BALANCE';
      if ( ! \Ultra\Lib\Billing\addBillingActionData($subscriber, $ServiceProviderRefNum, $IncommRefNum, $product, NULL, $Amount * 100, $UPC, 'INCOMM'))
      {
        $RespCode = 10029;
        $RespMsg = 'System Error';
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001');
      }

      // store tracking parameters in Redis for retrieval by internal__SubmitChargeAmount
      if ( ! $redis = new \Ultra\Lib\Util\Redis())
      {
        $RespCode = 10029;
        $RespMsg = 'System Error';
        $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002');
      }
      $key = "externalpayments/$IncommRefNum";
      $ttl = 60 * 30;
      $redis->set("$key/store_id", $StoreID, $ttl);
      $redis->set("$key/store_zipcode", $Zip, $ttl);
      $redis->set("$key/clerk_id", $SourceInfo, $ttl);
      $redis->set("$key/terminal_id", $TermID, $ttl);
      $redis->set("$key/source", \Ultra\UltraConfig\getPaymentProviderSkuAttribue('INCOMM', $UPC, 'SOURCE'), $ttl);
      $redis->set("$key/detail", __FUNCTION__, $ttl);
      $transaction = array(
        'Date'          => $Date,
        'Time'          => $Time,
        'TimeZone'      => $TimeZone,
        'RespRefNum'    => $IncommRefNum,
        'SrcRefNum'     => 0,
        'RespAction'    => 'valins',
        'RespCode'      => 0,
        'RespMsg'       => 'Success',
        'FaceValue'     => $Amount,
        'Balance'       => 0,
        'SerialNumber'  => 0,
        'UPC'           => $UPC,
        'MerchantName'  => $MerchName,
        'StoreID'       => $StoreID);
      $redis->set("$key/transaction", gzcompress(serialize($transaction)), $ttl);

      // update ULTRA.HTT_ACTIVATION_HISTORY if necessary
      if ($product == 'ULTRA_PLAN_RECHARGE')
      {
        $activation_history = get_ultra_activation_history($subscriber->CUSTOMER_ID);
        if ($activation_history && ! in_array($activation_history->FINAL_STATE, array(STATE_CANCELLED, 'Activated', FINAL_STATE_COMPLETE)) && ! $activation_history->FUNDING_SOURCE)
          log_funding_in_activation_history($subscriber->CUSTOMER_ID, 'INCOMM', $Amount);
      }

      // add subscriber current balance
      $Amount = $subscriber->stored_value + $subscriber->BALANCE + $Amount;

      // success
      list($Date, $Time, $TimeZone) = $this->getDateTimeZone();
      $RespCode = 0;
      $RespMsg = 'Success';
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    // fill in return values
    foreach (array('Version', 'Date', 'Time', 'TimeZone', 'ServiceProviderRefNum', 'Amount', 'RespCode', 'RespMsg') as $name)
      $this->addToOutput($name, $$name);
    return $this->result;
  }
}

?>
