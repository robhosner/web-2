<?php

namespace InComm\Lib\Api\Partner\InComm;

class preauthrecharge extends \InComm\Lib\Api\Partner\inComm
{
  /**
   * preauthrecharge
   * value insert pre-authorization
   * @return object Result
   */
  public function incomm__preauthrecharge()
  {
    // initialize
    list($Version, $Date, $Time, $TimeZone, $IncommRefNum, $UPC, $MIN, $Amount, $UserID, $Password) = $this->getInputValues();
    $ServiceProviderRefNum = \Ultra\Lib\Incomm\generateSrcRefNum();

    try
    {
      // check API login
      if ( ! $this->validateCredentials($UserID, $Password))
      {
        $RespCode = 10040;
        $RespMsg = 'Invalid UserID or Password';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid login information', 'SE0007');
      }

      // get subscriber
      teldata_change_db();
      if ( ! $subscriber = \Ultra\Lib\Incomm\decodeAccountNumber($MIN))
      {
        $RespCode = 10022;
        $RespMsg = 'Invalid MIN';
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');
      }

      // check state
      if (in_array($subscriber->plan_state, array(STATE_CANCELLED, STATE_PORT_IN_DENIED)))
      {
        $RespCode = 10059;
        $RespMsg = 'Invalid Account';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
      }

      if (checkExcessBalance($subscriber, $Amount * 100))
      {
        $RespCode = 10070;
        $RespMsg = 'Max Balance Limitation';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet', 'CH0001');
      }

      // verify UPC
      if ($UPC)
      {
        if ( ! verifyProviderSku('INCOMM', $UPC))
        {
          $RespCode = 10018;
          $RespMsg = 'Invalid UPC';
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU', 'VV0006');
        }

        if ( ! $destination = verifyProviderSkuAmount('INCOMM', $UPC, $Amount * 100))
        {
          $RespCode = 10021;
          $RespMsg = 'Invalid Amount';
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid Amount', 'VV0115');
        }

        // verify mapped destination
        if ( ! $destination = verifyAmountDestination($subscriber, $destination, $Amount * 100))
        {
          $RespCode = 10021;
          $RespMsg = 'Invalid Amount';
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid amount', 'VV0115');
        }
      }
      else // we will know the destination in the actual value insertion call
        $destination = 'ACCOUNT';
      dlog('', 'authorized $%.2f into %s for customer %d', $Amount, $destination, $subscriber->customer_id);

      // add subscriber current balance
      $Amount = $subscriber->stored_value + $subscriber->BALANCE;

      // success
      list($Date, $Time, $TimeZone) = $this->getDateTimeZone();
      $RespCode = 0;
      $RespMsg = 'Success';
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    // fill in return values
    foreach (array('Version', 'Date', 'Time', 'TimeZone', 'ServiceProviderRefNum', 'Amount', 'RespCode', 'RespMsg') as $name)
      $this->addToOutput($name, $$name);
    return $this->result;
  }
}

?>
