<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');


/**
 * tests for inComm phase 2 APIs
 */
class IncommTest extends PHPUnit_Framework_TestCase
{
  protected function setUp()
  {
    $this->cgi = 'incomm_api.php';
    $this->partner = 'incomm';
    $this->responseValue = array();
    $this->responseIndex = array();
    $request = '<ServiceProviderTxn>
      <Version>3.1</Version>
      <Request>
          <MsgType></MsgType>
          <DateTimeInfo>
              <Date>20141114</Date>
              <Time>125937</Time>
              <TimeZone>EST</TimeZone>
          </DateTimeInfo>
          <IncommRefNum></IncommRefNum>
          <Origin>
              <MerchName>InCommTest</MerchName>
              <StoreInfo>
                  <StoreID>NOBILL002</StoreID>
                  <TermID>NOBINOBI</TermID>
                  <StoreLoc>
                      <Address1>250 Williams ST</Address1>
                      <Address2>5TH FL</Address2>
                      <City>ATLANTA</City>
                      <State>GA</State>
                      <Zip>30303</Zip>
                  </StoreLoc>
              </StoreInfo>
              <MerchRefNum>321140</MerchRefNum>
              <LocaleInfo>
                  <CountryCode>US</CountryCode>
                  <CurrencyCode>USD</CurrencyCode>
                  <Language>EN</Language>
              </LocaleInfo>
              <SourceInfo>POS_PLATFORM</SourceInfo>
          </Origin>
          <Product>
              <Track1>;4011111111</Track1>
              <Track2>4011111111</Track2>
              <UPC></UPC>
              <SPNumber>4011111111</SPNumber>
              <MIN></MIN>
              <Value>
                  <Money>
                      <Amount></Amount>
                      <CurrencyCode>USD</CurrencyCode>
                  </Money>
              </Value>
          </Product>
          <AuthInfo>
              <UserID>incomm_dev</UserID>
              <Password>b063fc69d59b43fD</Password>
          </AuthInfo>
      </Request>
      </ServiceProviderTxn>';
    $this->request = new \DomDocument();
    if ( ! $this->request->loadXML($request))
      echo 'Failed to load XML request';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;
  }


  // set request value
  private function setRequest($pairs)
  {
    foreach ($pairs as $name => $value)
    {
      $list = $this->request->getElementsByTagName($name);
      if ($list->length)
        $list->item(0)->nodeValue = $value;
      else
        echo "Unknown request value $name";
    }
  }


  // return XML formatted request string
  private function getRequest()
  {
    return $this->request->saveHTML();
  }


  // parse XML API response into values and indexes
  private function parseResponse($xml)
  {
    $parser = xml_parser_create();
    if ( ! xml_parse_into_struct($parser, $xml, $this->responseValue, $this->responseIndex))
      echo 'Failed to parse XML request';
  }


  // retrieve a value from previously parsed XML API response
  private function getResponse($name)
  {
    $name = strtoupper($name);
    if ( ! count($this->responseIndex[$name]))
    {
      echo "Failed to locate value $name";
      return NULL;
    }
    return $this->responseValue[$this->responseIndex[$name][0]]['value'];
  }


  /**
   * test__inComm_balinq
   */
  public function test__incomm__balinq()
  {
    $api = substr(__FUNCTION__, strlen('test__'));
    $domain = find_domain_url();

    // test invalid login
    $this->setRequest(array(
      'MsgType'         => 'balinq',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'MIN'             => '5555555555'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals(10040, $this->getResponse('RespCode'));

    // test invalid account
    $credentials =  \Ultra\UltraConfig\getPaymentProviderCredentials('incomm');
    $this->setRequest(array('UserID' => $credentials[0], 'Password' => $credentials[1]));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid MIN', $this->getResponse('RespMsg'));

    // test cancelled account
    $this->setRequest(array('MIN' => '3232757937'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Account', $this->getResponse('RespMsg'));

    // test account with $0 balance
    $this->setRequest(array('MIN' => '5862482412'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    $this->parseResponse($result);
    print_r($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertEquals(0, $this->getResponse('Amount'));
    
    // test account with some balance
    $this->setRequest(array('MIN' => '5189866600'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThan(0, $this->getResponse('Amount'));
  }


  /**
   * test__inComm_preauthrecharge
   */
  public function test__incomm__preauthrecharge()
  {
    $api = substr(__FUNCTION__, strlen('test__'));
    $domain = find_domain_url();

    // test invalid login
    $this->setRequest(array(
      'MsgType'         => 'preauthrecharge',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366321118',
      'Amount'          => 19,
      'MIN'             => '5555555555'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals(10040, $this->getResponse('RespCode'));

    // test invalid account
    $credentials =  \Ultra\UltraConfig\getPaymentProviderCredentials('incomm');
    $this->setRequest(array('UserID' => $credentials[0], 'Password' => $credentials[1]));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid MIN', $this->getResponse('RespMsg'));

    // test cancelled account
    $this->setRequest(array('MIN' => '3232757937'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Account', $this->getResponse('RespMsg'));

    // test excess balance
    $this->setRequest(array('MIN' => '6464799313'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Max Balance Limitation', $this->getResponse('RespMsg'));

    // test unknown UPC
    $this->setRequest(array('MIN' => '5866460658', 'UPC' => '999999999999'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid UPC', $this->getResponse('RespMsg'));

    // test amount that does not match UPC
    $this->setRequest(array('UPC' => '799366321118', 'Amount' => 29));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Amount', $this->getResponse('RespMsg'));

    // test insufficent MONTHLY recharge
    $this->setRequest(array('MIN' => '3127149962', 'UPC' => '799366321118', 'Amount' => 19)); // L39 subscriber, $19 MONTHLY UPC
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Amount', $this->getResponse('RespMsg'));

    // successful MONTHLY test
    $this->setRequest(array('MIN' => '6095820698'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful WALLET test
    $this->setRequest(array('UPC' => '799366329497', 'Amount' => 19));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful SMART UPC test -> map to MONTHLY
    $this->setRequest(array('UPC' => '799366270560', 'Amount' => 19));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful SMART UPC test -> map to WALLET
    $this->setRequest(array('UPC' => '799366270560', 'Amount' => 20));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful SMART no UPC
    $this->setRequest(array('UPC' => NULL, 'Amount' => 20));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
  }


  /**
   * test__inComm_prevalins
   * identical to preauthrecharge
   */
  public function test__incomm__prevalins()
  {
    $api = substr(__FUNCTION__, strlen('test__'));
    $domain = find_domain_url();

    // test invalid login
    $this->setRequest(array(
      'MsgType'         => 'prevalins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366321118',
      'Amount'          => 19,
      'MIN'             => '5555555555'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals(10040, $this->getResponse('RespCode'));

    // test invalid account
    $credentials =  \Ultra\UltraConfig\getPaymentProviderCredentials('incomm');
    $this->setRequest(array('UserID' => $credentials[0], 'Password' => $credentials[1]));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid MIN', $this->getResponse('RespMsg'));

    // test cancelled account
    $this->setRequest(array('MIN' => '3232757937'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Account', $this->getResponse('RespMsg'));

    // test excess balance
    $this->setRequest(array('MIN' => '6464799313'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Max Balance Limitation', $this->getResponse('RespMsg'));

    // test unknown UPC
    $this->setRequest(array('MIN' => '5866460658', 'UPC' => '999999999999'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid UPC', $this->getResponse('RespMsg'));

    // test amount that does not match UPC
    $this->setRequest(array('UPC' => '799366321118', 'Amount' => 29));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Amount', $this->getResponse('RespMsg'));

    // test insufficent MONTHLY recharge
    $this->setRequest(array('MIN' => '3127149962', 'UPC' => '799366321118', 'Amount' => 19)); // L39 subscriber, $19 MONTHLY UPC
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Amount', $this->getResponse('RespMsg'));

    // successful MONTHLY test
    $this->setRequest(array('MIN' => '6095820698'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful WALLET test
    $this->setRequest(array('UPC' => '799366329497', 'Amount' => 19));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful SMART UPC test -> map to MONTHLY
    $this->setRequest(array('UPC' => '799366270560', 'Amount' => 19));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful SMART UPC test -> map to WALLET
    $this->setRequest(array('UPC' => '799366270560', 'Amount' => 20));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));

    // successful without UPC
    $this->setRequest(array('UPC' => NULL, 'Amount' => 20));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
  }


  /**
   * test__inComm_valins
   */
  public function test__incomm__valins()
  {
    $api = substr(__FUNCTION__, strlen('test__'));
    $domain = find_domain_url();
/*
    // test invalid login
    $this->setRequest(array(
      'MsgType'         => 'valins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366321118',
      'Amount'          => 19,
      'MIN'             => '5555555555'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals(10040, $this->getResponse('RespCode'));

    // test invalid account
    $credentials =  \Ultra\UltraConfig\getPaymentProviderCredentials('incomm');
    $this->setRequest(array('UserID' => $credentials[0], 'Password' => $credentials[1]));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid MIN', $this->getResponse('RespMsg'));

    // test cancelled account
    $this->setRequest(array('MIN' => '3232757937'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Account', $this->getResponse('RespMsg'));

    // test excess balance
    $this->setRequest(array('MIN' => '6464799313'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Max Balance Limitation', $this->getResponse('RespMsg'));

    // test unknown UPC
    $this->setRequest(array('MIN' => '5866460658', 'UPC' => '999999999999'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid UPC', $this->getResponse('RespMsg'));

    // test amount that does not match UPC
    $this->setRequest(array('UPC' => '799366321118', 'Amount' => 29));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Amount', $this->getResponse('RespMsg'));

    // test insufficent MONTHLY recharge
    $this->setRequest(array('MIN' => '3127149962', 'UPC' => '799366321118', 'Amount' => 19)); // L39 subscriber, $19 MONTHLY UPC
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid Amount', $this->getResponse('RespMsg'));

    // successful MONTHLY test
    $this->setRequest(array('MIN' => '6095820698', 'IncommRefNum' => rand(111111111111, 999999999999)));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // successful WALLET test
    $this->setRequest(array('UPC' => '799366329497', 'Amount' => 19, 'IncommRefNum' => rand(111111111111, 999999999999)));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // successful SMART UPC test -> map to MONTHLY
    $this->setRequest(array('UPC' => '799366270560', 'Amount' => 19, 'IncommRefNum' => rand(111111111111, 999999999999)));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // successful SMART UPC test -> map to WALLET
    $this->setRequest(array('UPC' => '799366270560', 'Amount' => 20, 'IncommRefNum' => rand(111111111111, 999999999999)));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // RTRIN-18: successful test with a SKU that has source 'INCOMM'
    $this->setRequest(array(
      'MsgType'         => 'valins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366270560',
      'Amount'          => 19,
      'MIN'             => '6094018424')); // CID 18857
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // RTRIN-18: successful test with a SKU that has source 'QPAY'
    $this->setRequest(array(
      'MsgType'         => 'valins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366321118',
      'Amount'          => 19,
      'MIN'             => '6094018424')); // CID 18857
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // RTRIN-44
    $this->setRequest(array(
      'MsgType'         => 'valins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366219651',
      'Amount'          => 19,
      'MIN'             => '6094018424')); // CID 18857
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));

    // DOP-65: L24 recharge
    $this->setRequest(array(
      'MsgType'         => 'valins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366329367',
      'Amount'          => 24,
      'MIN'             => '9173655648')); // CID 18944
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));
*/

    $this->setRequest(array(
      'MsgType'         => 'valins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366321156',
      'Amount'          => 1,
      'MIN'             => '7146753662')); // CID 18944
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
    $this->assertGreaterThanOrEqual(19, $this->getResponse('Amount'));
  }


  /**
   * test__inComm_revvalins
   */
  public function test__incomm__revvalins()
  {
    $api = substr(__FUNCTION__, strlen('test__'));
    $domain = find_domain_url();

    // test invalid login
    $this->setRequest(array(
      'MsgType'         => 'revvalins',
      'IncommRefNum'    => rand(111111111111, 999999999999),
      'UPC'             => '799366321118',
      'Amount'          => 19,
      'MIN'             => '5555555555'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    print_r($result);
    $this->parseResponse($result);
    $this->assertEquals(10040, $this->getResponse('RespCode'));

    // test invalid account
    $credentials =  \Ultra\UltraConfig\getPaymentProviderCredentials('incomm');
    $this->setRequest(array('UserID' => $credentials[0], 'Password' => $credentials[1]));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Invalid MIN', $this->getResponse('RespMsg'));

    // invalid transaction
    $this->setRequest(array('MIN' => '6464799313'));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Not Reversible', $this->getResponse('RespMsg'));

    // expired transaction
    $this->setRequest(array('MIN' => '6095820698', 'IncommRefNum' => '252599898725', 'Amount' => 20));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Time Limit Expired', $this->getResponse('RespMsg'));

    // successful test
    $this->setRequest(array('MIN' => '6095820698', 'IncommRefNum' => '658232442620', 'Amount' => 19));
    $result = callUltraApi(NULL, $this->getRequest(), 4, $this->partner, NULL, NULL, $domain, $this->apache_username, $this->apache_password, NULL, NULL, NULL, NULL, $this->cgi);
    // print_r($result);
    $this->parseResponse($result);
    $this->assertEquals('Success', $this->getResponse('RespMsg'));
  }

}

