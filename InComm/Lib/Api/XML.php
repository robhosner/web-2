<?php

namespace InComm\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

class XML extends \Ultra\Lib\Api\Base
{
  /**
   * Disable Presentation mode
   */
  public function isPresentation()
  {
    return FALSE;
  }


  /**
   * output
   * generate inComm XML output based on XML template file with blank valulues
   * @param Array API results
   * @return String formatted API output
   */
  public function output($apiExecutionResult)
  {
    $output = NULL;

    // print any errors the API may have encountered
    if ( ! empty($apiExecutionResult['errors']))
      dlog('', 'ERROR: %s', $apiExecutionResult['errors'][0]);

    // check if XML template is defined
    if (empty($this->apiDefinition['commands'][$this->command]['template']))
      dlog('', 'ERROR: missing XML output template');

    // create DomDocument
    elseif ( ! $xml = new \DomDocument())
      dlog('', 'ERROR: failed to create DomDocument object');

    // load XML template file into DomDocument
    elseif ( ! $xml->load("{$this->metaDirectory}/{$this->apiDefinition['commands'][$this->command]['template']}"))
      dlog('','ERROR: failed to load XML template ' . $this->apiDefinition['commands'][$this->command]['template']);

    // fill in the template with output values
    else
    {
      foreach ($apiExecutionResult as $name => $value)
      {
        // locate the node with the given name and set its value
        $list = $xml->getElementsByTagName($name);
        if ($list->length == 1)
          $list->item(0)->nodeValue = $value;
        elseif ($list->length > 1)
          dlog('', "WARNING: multiple tags $name found in XML template");
        // else: ignore output values such as ultra_trans_epoch, user_errors, etc not found in XML template
      }

      // generate XML with new values
      $output = $xml->saveHTML();
    }

    return $output;
  }
}

?>
