<?php

namespace InComm\Lib;

require_once 'InComm/Lib/Api/Errors.php';
require_once 'InComm/Lib/Api/XML.php';
require_once 'InComm/Lib/Api/Partner/InComm.php';
require_once 'InComm/Lib/Api/Partner/InComm/balinq.php';
require_once 'InComm/Lib/Api/Partner/InComm/preauthrecharge.php';
require_once 'InComm/Lib/Api/Partner/InComm/prevalins.php';
require_once 'InComm/Lib/Api/Partner/InComm/valins.php';
require_once 'InComm/Lib/Api/Partner/InComm/revvalins.php';

/**
 * Subclass of Ultra/Lib/Api for inComm phase 2 APIs
 * how it works:
 * - inComm POST XML data to the API (typically without any name, hence we cannot reference it but must extract raw POST)
 * - our constructor reads XML and parses it to identify the API name (aka Ultra command, aka inComm Message Type)
 * - our constructor sets class-specific values for protected variables (parent must not override them)
 * - finally our constructor calls parent constructor to initialize the API as usual
 * - our method loadApiParameters overrides the parent method and uses already parsed XML to extract parameters loaded from JSON definition file
 * - our XML output method uses XML templates with mostly blank values to fill in the API return parameters
 * - all response XML templates are pre-filled with generic error code and message which will be overritten on successfull API execution
 * @see https://issues.hometowntelecom.com:8443/browse/RTRIN-1
 * @author VYT, 2015-01
 */
class Api extends \Ultra\Lib\Api
{
  const API_NAME_TAG = 'MSGTYPE'; // inComm passes API name in MsgType tag
  const API_VERSION  = 4; // version required for all API calls

  // parsed XML input
  private $xmlValue = array();
  private $xmlIndex = array();

  /**
   * __construct
   * set class-specific values then call parent contrsuctor
   */
  public function __construct ()
  {
    // override parent's values
    $this->metaDirectory = 'InComm/Lib/Api/Meta';
    $this->apiNameSpace  = '\InComm\Lib\Api';
    $this->format = 'xml';
    $this->partner = 'incomm';

    // process XML POST data
    try
    {
      // parse and save XML input
      if ( ! $xml = file_get_contents('php://input')) // $HTTP_RAW_POST_DATA is deprecated
        throw new \Exception('failed to load XML data');
      dlog('', 'INPUT: %s', $xml);
      if ( ! $parser = xml_parser_create())
        throw new \Exception('failed to create XML parser');
      if ( ! xml_parse_into_struct($parser, $xml, $this->xmlValue, $this->xmlIndex))
        throw new \Exception('Failed to parse XML request');
      dlog('', 'VALUES: %s', $this->xmlValue);
      dlog('', 'INDEXES: %s', $this->xmlIndex);

      // assure that only a single API name tag is given (not likely to happen unless we receive bad XML)
      if ( ! count($this->xmlIndex[self::API_NAME_TAG]))
        throw new \Exception('missing tag ' . self::API_NAME_TAG);
      elseif (count($this->xmlIndex[self::API_NAME_TAG]) > 1)
        throw new \Exception('multiple tags ' . self::API_NAME_TAG);

      // set API name (aka command)
      if (empty($this->xmlValue[$this->xmlIndex[self::API_NAME_TAG][0]]['value']))
        throw new \Exception("missing tag self::API_NAME_TAG value");
      $this->command = $this->partner . '__' . $this->xmlValue[$this->xmlIndex[self::API_NAME_TAG][0]]['value'];
      dlog('', 'API command: %s', $this->command);

      // finally call the usual API initailiaztion
      parent::__construct();

      // check version
      if ($this->version != self::API_VERSION)
        throw new \Exception("version {$this->version} is invalid");
    } 
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
      $this->result = new \Result;
      $this->result->add_error($e->getMessage());
    }
  }


  /**
   * loadApiParameters
   * load parameter values from XML
   * @return NULL
   */
  public function loadApiParameters()
  {
    $logInput = array();

    if ( ! isset($this->apiDefinition['commands'][$this->command]))
    {
      $this->error_code = 'FA0006';
      $this->result->add_error("ERR_API_INTERNAL: command not defined in partner {$this->partner}");
      return NULL;
    }

    $optionalParamsToSkip = $this->getOptionalParamsToSkipByApi($this->apiDefinition['commands'][$this->command]["template"]);
    $optionalParamsCount = count($optionalParamsToSkip);

    // loop through input parameters
    foreach ($this->apiDefinition['commands'][$this->command]['parameters'] as $paramDefinition)
    {
      $parameter = strtoupper($paramDefinition['name']);

      if ($optionalParamsCount > 0 && in_array($paramDefinition['name'], $optionalParamsToSkip))
      {
        // Add null values for optional params if not specified
        if (empty($this->xmlIndex[$parameter]))
        {
          dlog('', 'PARAMETER %s is missing', $parameter);
          $this->apiParameters[] = null;
          continue;
        }
      }

      if (isset($this->xmlValue[$this->xmlIndex[$parameter][0]]) && is_array($this->xmlValue[$this->xmlIndex[$parameter][0]]) && ! empty($this->xmlValue[$this->xmlIndex[$parameter][0]]['value']))
      {
        $value = $this->xmlValue[$this->xmlIndex[$parameter][0]]['value'];

        $this->apiParameters[] = $value;
        dlog('', 'PARAMETER %s = %s', $parameter, $value);

        // truncate input for logging
        if ( isset( $paramDefinition['truncate'] ) && $paramDefinition['truncate'] && strlen( (string)$value ) > 16 )
          $logInput[$parameter] = substr( $value , 0 , 4 ) . '*TRUNCATED*';
        else
          $logInput[$parameter] = $value;
      }
      elseif ($paramDefinition['required'])
      {
        $this->error_code[] = \Ultra\Lib\Api\Errors\missingParamCode($parameter);
        $this->result->add_error("API_ERR_PARAMETER: Missing {$this->command} REST parameter $$parameter!");
        dlog('', 'PARAMETER %s is missing', $parameter);
      }
      else // missing and not required
      {
        $this->apiParameters[] = '';
        dlog('', 'PARAMETER %s is missing', $parameter);
      }
    }

    dlog('',"apiParameters = %s",$this->apiParameters);

    return $logInput;
  }

  /**
   * Get optional parameters to skip by api template name.
   *
   * @param $apiTemplateName
   * @return array
   */
  private function getOptionalParamsToSkipByApi($apiTemplateName)
  {
    switch ($apiTemplateName)
    {
      case 'valins.xml':
        return array('Zip');
        break;
      default:
        return array();
    }
  }
}

