<?php

require_once('web.php');
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/Util/Crypt.php';
require_once 'Ultra/UltraConfig.php';

/**
 * createSessionCookie
 * helper function for command line API testing
 * requires both valid MSISDN and customer ID since Session class does not touch the database
 * does not validate parameters and will always create a session, even for cancelled or non-existing subscriber
 * @param String msisdn
 * @param Integer customer_id
 */
function createSessionCookie($msisdn, $customer_id)
{
  // this function is for development purposes only
  if ( ! \Ultra\UltraConfig\isDevelopmentDB())
  {
    dlog('', 'ERROR: cannot execute in production environment');
    return NULL;
  }

  if (empty($msisdn))
  {
    dlog('', 'ERROR: missing parameter MSISDN');
    return NULL;
  }

  // create fake required headers
  $_SERVER['HTTPS'] = 'on';
  $_SERVER['User-Agent'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B411 Safari/600.1.4';
  $_SERVER['HTTP_CLIENT_IP'] = gethostbyname(gethostname());

  $session = new Session($msisdn);
  $session->confirm($customer_id);
  return $session->cookie;
}


/**
 * class Session
 * cookie-based web session management, reads TMO enrichment headers, does not touch database (all data is stored in redis)
 * note: due to the nature of this class it cannot be tested on command line, only as apache module
 * @see Mobile Account Management Site
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Mobile+Account+Management+Site+%28m.ultra.me%29+-+Product+Spec
 * @author VYT, Nov 2014
 *
 * TMO GW inject the following enrichment headers ONLY if a) accessing Ultra domain and b) from an Ultra subscriber's device and c) on TMO network and d) without SSL
 *  X-Nokia-MSISDN          13476455407
 *  X-Nokia-chargingid      392201448
 *  MSISDN                  3476455407
 *  X-Nokia-sgsnipaddress   172.57.1.150
 *  X-Nokia-IMSI            310260840773799
 *  CHARGINGID              392201448
 *  TMO-IMEI                01378900-976938-07
 *  SGSNIP                  172.57.1.150
 *  GGSNIP                  216.155.171.7
 *  IMSI                    310260840773799
 */

// TODO: reduce dubugging, remove curly braces

class Session
{
  const SESSION_LIFE = 604800; // maximum life of a cookie based session is 7 days
  const COOKIE_LIFE = 604800; // maximum life of a cookie on the client is also 7 days
  const TOKEN_LIFE = 172800;  // maximum life of pre-authorization token is 2 days
  const SYSTEM_TOKEN_LIFE = 2592000;  // maximum life of system-issued authorization token is 30 days
  const COOKIE_NAME = 'session'; // used for both redis and client browser
  const TOKEN_V1 = 38; // length of v1 token
  const TOKEN_V2 = 28; // length of v2 token
  const TOKEN_V3 = 14; // length of v3 token

  // v3 token flags
  const SYSTEM_TOKEN = 1;   // system generated token (no session)
  const TEST_TOKEN = 32;    // test flag, first bit

  private $redis = NULL;    // sessions are stored entirely in redis
  private $msisdn = NULL;   // subscriber MSISDN
  private $imsi = NULL;     // device IMSI
  private $token = NULL;    // pre-authorization token
  private $cookie = NULL;   // session cookie
  private $customer_id = NULL;      // customer ID
  private $expiration = NULL; // when session expires, epoch
  // these values must match between HTTP and HTTPS requests of the same session
  private $ip = NULL;       // client IP address
  private $agent = NULL;    // browser user agent

  /**
   * constructor
   * collect all available session information
   * @param string MSISDN: when passed overrides MSISDN header
   */
  public function __construct($msisdn = NULL, $token = NULL)
  {
    try
    {
      // getallheaders is only available when running as apache module
      if (function_exists('getallheaders'))
        $headers = getallheaders();
      else
        $headers = $this->getHeaders();

      // collect common headers
      $this->ip = self::getClientIp(); // this is typically gateway, not device (due to NAT)
      $this->agent = empty($headers['User-Agent']) ? NULL : $headers['User-Agent'];
      if (empty($this->agent))
         dlog('', 'WARNING: missing User-Agent');

      // read HTTPS cookie
      if ($this->isHttps())
      {
        if ( ! empty($_COOKIE[self::COOKIE_NAME]))
        {
          $this->cookie = $_COOKIE[self::COOKIE_NAME];
          if ($session = $this->load($this->cookie))
          {
            // copy saved session info into current session
            foreach ($session as $key => $value)
              $this->$key = $value;
            dlog('', "successfully loaded session by cookie {$this->cookie}");
          }
          else
            dlog('', "failed to load session by cookie {$this->cookie}");
        }

        // expired token or MSISDN is passed when requesting a token login by SMS
        if ( ! $msisdn && $token)
          $msisdn = $this::decryptToken($token);
        if ($msisdn)
        {
          $this->msisdn = $msisdn;
          $this->createToken();
          $this->save($this->token, self::TOKEN_LIFE);
        }
      }
      // collect TMO enrichment headers
      else
      {
        foreach ($headers as $header => $value)
        {
          $header = strtoupper($header);
          switch ($header)
          {
            case 'MSISDN':
            case 'HTTP_MSISDN':
            case 'HTTP_X_NOKIA_MSISDN':
              $this->msisdn = $value;
              break;
            case 'IMSI':
            case 'HTTP_IMSI':
            case 'HTTP_X_NOKIA_IMSI':
              $this->imsi = $value;
              break;
          }
        }
        $this->cleanHeaders();
        $this->createToken();
        $this->save($this->token, self::TOKEN_LIFE);
      }
    }
    
    catch(Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

  }


  /**
   * __get
   * magic function for reading-only private session properties
   * @return string
   */
  public function __get($name)
  {
    if (property_exists($this, $name))
      return $this->$name;
    else
      dlog('', "uknown property $name");
  }


  /**
   * verifyToken
   * verify pre-authorization token previosly created from enrichment headers by getAuthorizationToken
   * this step is necessary because the token is created with HTTP call while all *other* API calls are HTTPS
   * @param string token
   * @return boolean TRUE on success or FALSE on failure
   */
  public function verifyToken($token)
  {
    $result = FALSE;

    try
    {
      // this call requires SSL
      if (!$this->isHttps())
        throw new Exception('SSL required');

      // v2 tokens handle live sessions: previously saved in redis (if not expired), must match User-Agent
      $length = strlen($token);
      if ($length == self::TOKEN_V2)
      {
        // load and verify session by pre-authorization token
        if ( ! $session = $this->load($token))
          throw new Exception("token '$token' not found");

        if ($this->ip !== $session['ip'])
          dlog('', 'IP mismatch: expected %s, got %s', $session['ip'], $this->ip);

        if ($this->agent !== $session['agent'])
        {
          dlog('', 'agent %s, expected %s', $this->agent, $session['agent']);
          throw new Exception('User-Agent mismatch');
        }

        // sync saved session info
        foreach ($session as $key => $value)
          $this->$key = $value;
      }

      // v3 tokens are for system use only
      elseif ($length == self::TOKEN_V3)
      {
        if ( ! $this->msisdn = $this::decryptToken($token))
          throw new Exception("failed to decrypt token $token");
      }

      else
        throw new Exception("invalid token $token with length $length");

      // success
      $this->token = NULL; // token has been used up
      $result = TRUE;
    }

    catch(Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $result;
  }


  /**
   * getTokenMsisdn
   * decrypt and verify token and return embedded MSISDN
   * @param String token
   * @return String MSISDN on success of NULL on failure
   */
  public function getTokenMsisdn($token)
  {
    return $this::decryptToken($token);
  }


  /**
   * confirm
   * mark session as valid and save session cookie along with customer_id
   * this function should be called after successfull token or user login
   * @param int customer ID
   * @return boolean TRUE on success or FALSE on failure
   */
  public function confirm($customer_id)
  {
    $result = FALSE;

    try
    {
      // this call requires SSL
      if (!$this->isHttps())
        throw new Exception('SSL required');

      if (empty($customer_id))
        throw new Exception('missing customer ID');
      $this->customer_id = $customer_id;

      // check requirements
      if ( ! $this->cookie)
        if ( ! $this->cookie =  create_guid())
          throw new Exception('failed to create session token');

      // set cookie
      $this->expiration = time() + self::COOKIE_LIFE;
      if (php_sapi_name() != 'cli') // we cannot set cookie in command line mode
        if ( ! setcookie(self::COOKIE_NAME, $this->cookie, $this->expiration, '/', NULL, TRUE))
          throw new Exception('failed to set session cookie');

      $this->save($this->cookie, self::SESSION_LIFE);
      $result = TRUE;
    }

    catch(Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $result;
  }


  /**
   * kill
   * expire and remove session info
   * @return boolean TRUE on success or FALSE on failure
   */
  public function kill()
  {
    $result = FALSE;

    try
    {
      // do we have a valid or confirmed session?
      if ( ! $this->msisdn && ! $this->customer_id)
      {
        $result = TRUE;
        throw new Exception('missing MSISDN or customer ID');
      }

      // ensure redis connection
      if ( ! $this->redis)
        if ( ! $this->redis = new \Ultra\Lib\Util\Redis())
          throw new Exception('failed to create redis object');

      // remove session from redis
      $this->redis->del(self::COOKIE_NAME . '/' . $this->cookie);

      // remove cookie
      unset($_COOKIE[self::COOKIE_NAME]);
      if ( ! setcookie(self::COOKIE_NAME, NULL, -1, '/'))
        throw new Exception('failed to remove cookie');

      $result = TRUE;
    }

    catch(Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $result;
  }


  /**
   * createToken
   * create a new pre-authorization token based on HTTP enrichment headers
   * @return string pre-authorization token or NULL on failure
   */
  private function createToken()
  {
    // ensure redis connection
    if ( ! $this->redis)
      if ( ! $this->redis = new \Ultra\Lib\Util\Redis())
        throw new Exception('failed to create redis object');

    // check required enrichment headers info
    if ( ! $this->msisdn)
      throw new Exception('missing session MSISDN');
    if ( ! $this->ip)
      throw new Exception('missing session IP');
    if ( ! $this->agent)
      throw new Exception('missing session user-agent');
    if ( ! $this->token = $this::encryptToken($this->msisdn))
      throw new Exception('failed to encrypt token');
  }


  /**
   * save
   * save session info to redis either by cookie or token as key
   * @param string redis key
   */
  private function save($key, $ttl)
  {
    // ensure redis connection
    if ( ! $this->redis)
      if ( ! $this->redis = new \Ultra\Lib\Util\Redis())
        throw new Exception('failed to create redis object');

    // check requirements
    if (empty($key))
      throw new Exception('key is missing');

    // save session by either token or cookie
    $key = self::COOKIE_NAME . '/' . $key;
    $data = array(
      'msisdn' => $this->msisdn,
      'imsi' => $this->imsi,
      'customer_id' => $this->customer_id,
      'expiration' => $this->expiration,
      'ip' => $this->ip,
      'agent' => $this->agent);
    $value = gzcompress(serialize($data));
    $this->redis->set($key, $value, $ttl);
  }


  /**
   * load
   * load and return session info from redis either by cookie or token
   * @param string token
   * @param string cookie
   */
  private function load($key)
  {
    $session = NULL;

    // ensure redis connection
    if ( ! $this->redis)
      if ( ! $this->redis = new \Ultra\Lib\Util\Redis())
        throw new Exception('failed to create redis object');

    if (empty($key))
      throw new Exception('key is missing');

    // load session
    if ($session = $this->redis->get(self::COOKIE_NAME . '/' . $key))
      $session = unserialize(gzuncompress($session));

    return $session;
  }


  /*
   * cleanHeaders
   * decrypt possibly (but not necessarily) encrypted TMO headers and normalize values if needed
   */
  private function cleanHeaders()
  {
    // decrypt and normalize MSISDN
    if ( ! empty($this->msisdn))
    {
      if ( ! ctype_digit($this->msisdn))
      {
        $msisdn = \Ultra\Lib\Util\Crypt\decodeRC4(\Ultra\UltraConfig\getTMOEncryptionKey(), $this->msisdn);
        if ( ! ctype_digit($msisdn))
        {
          dlog('', "WARNING: failed to decrypt MSISDN header {$this->msisdn}");
          $this->msisdn = NULL;
        }
        else
          $this->msisdn = normalize_msisdn($msisdn);
      }
        else
          $this->msisdn = normalize_msisdn($this->msisdn);
    }

    // decrypt IMSI
    if ( ! empty($this->imsi) && ! ctype_digit($this->imsi))
    {
      $imsi = \Ultra\Lib\Util\Crypt\decodeRC4(\Ultra\UltraConfig\getTMOEncryptionKey(), $this->imsi);
      if ( ! ctype_digit($imsi))
      {
        dlog('', "WARNING: failed to decrypt IMSI header {$this->imsi}");
        $this->imsi = NULL;
      }
      else
        $this->imsi = normalize_msisdn($imsi);
    }
  }


  /**
   * getClientIp
   *
   * read client IP address from headers
   * @return string IP address
   */
  public static function getClientIp()
  {
    // CLI mode: return the local host IP
    $mode = php_sapi_name();
    if ($mode == 'cli')
      $result = gethostbyname(gethostname());

    // apache mode: attempt to get client's IP
    else
    {
      // important: evaluate in this order of preference
      $result = NULL;
      $keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');

      // first pass: filter out private ranges
      foreach ($keys as $key)
        if (array_key_exists($key, $_SERVER))
          foreach (explode(',', $_SERVER[$key]) as $ip)
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE))
            {
              $result = $ip;
              break;
            }

      // second pass: include private ranges (we have no choice) 
      if ( ! $result)
        foreach ($keys as $key)
          if (array_key_exists($key, $_SERVER))
          {
            list($result) = explode(',', $_SERVER[$key]);
            break;
          }
    }

    dlog('', "IP: $result, mode: $mode");
    return $result;
  }

  /**
   * encryptToken
   * generate encrypted token which contains embedded subsriber's MSISDN
   * @param String MSISDN
   * @param Integer token version to generate
   * @param Array of v3 token flags
   * @return String plain text token or NULL on failure
   */
  public static function encryptToken($msisdn, $version = self::TOKEN_V3, $flags = [self::SYSTEM_TOKEN])
  {
    $result = NULL;

    try
    {
      // check parameters
      $length = strlen($msisdn);
      if ($length != 10 && $length != 11)
        throw new Exception('invalid parametes: ' . json_encode(func_get_args()));
      if ($length != 10)
        $msisdn = normalize_msisdn($msisdn);

      $crypter = new \Crypt_RC4();
      $crypter->setKey(\Ultra\UltraConfig\getSessionTokenEncryptionKey());

      // v2 token: 28 chars base 32, contains MSISDN and a random digit
      if ($version == self::TOKEN_V2)
      {
        // hash is 9 digit random string + checksum
        $hash = rand(100000000, 999999999);
        $hash .= luhn_checksum($hash);

        // in order to avoid statically encrypted MSISDN (same string in every token) we interleave it with hash and convert to base 36, which also conveniently shortens the string
        for ($i = 0, $token = ''; $i < 10; $i++)
          $token .= $hash[$i] . $msisdn[$i];

        // base_convert will overflow on large numbers and may produce shorter strings, hence we neeed break up and pad to fixed width of 14 chars
        $base = sprintf('%7s%7s', base_convert(substr($token, 0, 10), 10, 36), base_convert(substr($token, -10), 10, 36));

        // encryption produces binary, hence we need to conver it to HEX it in order to transport over SMS as plain text
        $result = bin2hex($crypter->encrypt($base));
        dlog('', "hash: $hash, msisdn: $msisdn -> token: $result (v2)");
      }

      // v3 token: date (YYMMDD) + flags (xx) + checksum (x) + msisdn (1234567890)+ checksum (x) + rand (xx)
      elseif ($version == self::TOKEN_V3)
      {
        date_default_timezone_set('UTC');
        $date = date('ymd'); // string of 6 digits

        $pad = 0;
        if (is_array($flags))
          foreach ($flags as $flag)
            $pad |= $flag;
        $pad = sprintf('%02d', $pad); // 2 digit string

        // first checksum
        $token = $date . $pad;
        $token .= luhn_checksum($token);

        // add MSISDN
        $token .= $msisdn;

        // second checksum
        $token .= luhn_checksum($token);

        // assert proper string length
        if (strlen($token) != 20)
          throw new Exception('failed token composition: ' . $token);

        // 2 digit ramdomization
        $token .= sprintf('%02s', rand(0, 99));

        // pack 22 digit decimal string to binary
        $base = pack('L', substr($token, 0, 9)) . pack('L', substr($token, 9, 9)) . pack('S', substr($token, -4));

        // encrypt and convert to base 64 and then URL-safe
        $encrypted = $crypter->encrypt($base);
        $result = rtrim(strtr(base64_encode($encrypted), '+/', '-_'), '=');
        dlog('', "msisdn: $msisdn -> token: $result");
      }

      else
        throw new Exception("invalid token version $version");

    }

    catch(Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $result;
  }


  /**
   * decryptToken
   * decrypts encrypted token and extracts embedded MSISDN (reverse of encryptToken)
   * @param String 28 char plain text token
   * @return String 10 char msisdn or NULL on failure
   */
  public static function decryptToken($token)
  {
    $msisdn = NULL;

    try
    {
      // v1 tokens cannot be decrypted
      $length = strlen($token);
      if ($length == self::TOKEN_V1)
        throw new Exception("cannot decrupt v1 token $token");

      $crypter = new \Crypt_RC4();
      $crypter->setKey(\Ultra\UltraConfig\getSessionTokenEncryptionKey());

      // v2 token
      if ($length == self::TOKEN_V2)
      {

        // de-HEX to binary, decrypt and convert to 20 char decimal string
        $decrypted = $crypter->decrypt(pack('H*', $token));
        $base = substr('0000000000' . base_convert(substr($decrypted, 0, 7), 36, 10), -10) . substr('0000000000' . base_convert(substr($decrypted, -7), 36, 10), -10); // must prepend short strings

        // separate hash from MSISDN
        for ($i = 0, $hash = '', $number = ''; $i < 20; $i += 2)
        {
          $hash .= $base[$i];
          $number .= $base[$i + 1];
        }

        // verify hash and MSISDN integrity
        if (strlen($hash) != 10 || ! ctype_digit($hash) || strlen($number) != 10 || ! ctype_digit($number))
          throw new Exception("msisdn '$number' or hash '$hash' type check failed");
        if (luhn_checksum(substr($hash, 0, 9)) != substr($hash, -1))
          throw new Exception("hash $hash check sum failed");

        $msisdn = $number;
        dlog('', "token $token (v2) -> hash: $hash, msisdn $msisdn");
      }

      // v3 token: shorter than v2 and contains date and flags in addition to MSISDN; used only for system SMS
      elseif ($length == self::TOKEN_V3)
      {
        // de-base to binary and decrypt
        $base = base64_decode(str_pad(strtr($token, '-_', '+/'), strlen($token) % 4, '=', STR_PAD_RIGHT));
        $decrypted = $crypter->decrypt($base);

        // unpack binary into 22 digit decimal string
        $token = substr('000000000' . implode('', unpack('L', substr($decrypted, 0, 4))), -9) . 
          substr('000000000' . implode('', unpack('L', substr($decrypted, 4, 4))), -9) .
          substr('0000' . implode('', unpack('S', substr($decrypted, -2))), -4);

        // assert proper length
        if (strlen($token) != 22)
          throw new Exception('failed token decomposition: ' . $token);

        // extract and check issue date
        $date = substr($token, 0, 6);
        if ( ! $issued = strtotime(sprintf('20%s-%s-%s', substr($date, 0, 2), substr($date, 2, 2), substr($date, -2))))
          throw new Exception("failed to parse token $token issue date $date");
        if ($issued + self::SYSTEM_TOKEN_LIFE < time())
          throw new Exception("token $token exceeded SYSTEM_TOKEN_LIFE");

        // extract and check token flags
        $flags = substr($token, 6, 2);
        if ( ! ($flags & self::SYSTEM_TOKEN))
          throw new Exception("system token $token missing SYSTEM_TOKEN flag");

        // verify first and second checksums
        if (luhn_checksum($date . $flags) != substr($token, 8, 1))
          throw new Exception("first checksum failed on token $token");
        if (luhn_checksum(substr($token, 0, 19)) != substr($token, 19, 1))
          throw new Exception("second checksum failed on token $token");

        // extract MSISDN
        $msisdn = substr($token, 9, 10);
        dlog('', "token $token (v3) -> date: $date, flags: $flags, MSISDN: $msisdn");
      }

      // invalid token length
      else
        dlog('', 'invalid token %s length %d', $token, $length);
    }

    catch(Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $msisdn;
  }

  private function getHeaders()
  {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
      }
    }
    return $headers;
  }

  private function isHttps()
  {
    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
      return true;
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
      return true;
    } else {
      return false;
    }
  }
}

