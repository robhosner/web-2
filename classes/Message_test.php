<?php

require_once 'db.php';
require_once 'classes/Message.php';
require_once 'classes/Messenger.php';
require_once 'Ultra/Lib/Util/Redis/Messaging.php';

teldata_change_db();

// $m = get_next_message_to_send();
// print_r( $m );

$the_messenger = new Messenger( array('max_iterations' => 1) );
$the_messenger->process_message_queue();

?>
