<?php

require_once 'Ultra/Lib/Util/Redis/WebPos.php';

class WebPosRunner
{
  // how many seconds we sleep between one cycle and the next
  private $sleep_seconds      = 5;

  // how many cycles we perform per thread
  private $max_cycles         = 12;

  // for how many seconds should a thread run at most
  private $max_execution_time = 60;

  /**
   * main
   *
   * Main runner body
   * takes optional $uuid parameter when performing operation on single $uuid
   *
   * @param  String $uuid
   * @return NULL
   */
  public function main( $uuid=NULL )
  {
    $timestampStart = time();
    $countCycles    = 0;
    $error          = FALSE;

    // connect to DB
    teldata_change_db();

    $redisWebPos = new \Ultra\Lib\Util\Redis\WebPos();

    // perform several cycles, until the limits are met or a fatal error occurs
    while( ! $error && $this->continueExecution( $countCycles , $timestampStart ) )
    {
      if ( $countCycles > 0 )
        sleep($this->sleep_seconds);

      #dlog('',"Cycle # ".++$countCycles);

      if ( $uuid )
        $countCycles = $this->max_cycles;

      $error = $this->webPosTask( $redisWebPos, $uuid );
      if ($error)
        dlog('', $error);
      else
      {
        // if running on a single transaction, remove UUID from priority queue
        if ($uuid)
          $redisWebPos->delUUID($uuid);
      }
    }

    return NULL;
  }

  /**
   * continueExecution
   *
   * Returns TRUE if the runner should continue
   *
   * @param  Integer $countCycles
   * @param  Integer $timestampStart time runner started
   * @return Boolean
   */
  protected function continueExecution( $countCycles , $timestampStart )
  {
    if ( ( $timestampStart + $this->max_execution_time ) <= time() )
    {
      dlog('',"Execution won't continue after ".$this->max_execution_time." seconds.");

      return FALSE;
    }

    if ( $countCycles < $this->max_cycles )
      return TRUE;

    #dlog('',"Cycles are over.");

    return FALSE;
  }


  /**
   * webPosTask
   * 
   * @param  Object  $redisWebPos WebPos class / redis interface
   * @param  String  $uuid        transaction identifier
   * @return Boolean || String    FALSE on success , error string on error
   */
  protected function webPosTask($redisWebPos, $uuid = NULL)
  {
    $error = FALSE;

    if ( ! $uuid )
      $uuid = $redisWebPos->getNextUUID();

    if ( $uuid )
    {
      dlog('', 'uuid = %s', $uuid);

      // check if transaction is old enough to be processed
      if (time() - $redisWebPos->getUUIDTimestamp($uuid) < \Ultra\UltraConfig\getPaymentProviderDelay('WEBPOS'))
      {
        dlog('', "Transaction with UUID $uuid not ready to be processed");
      }

      $webPosActionData = $this->getWebPosActionData($uuid);

      if ( ! $webPosActionData )
      {
        if ($uuid)
          dlog('', 'No data found for uuid : ' . $uuid);
        else
          dlog('', 'No data to process');
      }
      elseif ( $webPosActionData->STATUS == 'OPEN')
      {
        $error = $this->processWebPosTransaction($webPosActionData);
      }
      else
        dlog('', "Invalid status for uuid $uuid : " . $webPosActionData->STATUS);
    }
    else
    {
      $error = 'NO JOBS AVAILABLE';
    }

    return $error;
  }

  /**
   * processWebPosTransaction
   * @param  Object $webPosActionData row from ULTRA.WEBPOS_ACTIONS
   * @return Boolean || String FALSE on success , error string on error
   */
  public function processWebPosTransaction($webPosActionData)
  {
    $error = FALSE;

    // two types of supported activations
    $webPosTypes = array(
      1 => 'Activation',
      2 => 'Port-In'
    );

    // add balance to account
    $result = func_add_balance(
      array(
        'customer'      => get_customer_from_customer_id($webPosActionData->CUSTOMER_ID),
        'amount'        => $webPosActionData->AMOUNT,
        'reason'        => $webPosTypes[$webPosActionData->TYPE],
        'source'        => 'WEBPOS',
        'reference'     => $webPosActionData->TRANSACTION_ID,
        'pay_quicker'   => substr($webPosActionData->SUBPRODUCT_ID, 0, 2) == 'PQ' ? $webPosActionData->SUBPRODUCT_ID : FALSE,
        'target_cos_id' => get_cos_id_from_plan($webPosActionData->PLAN_NAME)
      )
    );

    if ( ! $result['success'])
    {
      $error = (count($result['errors'])) ? $result['errors'][0] : 'ERROR adding balance to account';
    }
    else
    {
      // perform state change
      $success = NULL;
      switch ($webPosActionData->TYPE)
      {
        case 1: // Activation
          $success = $this->changeStateActivation($webPosActionData);
          break;
        case 2: // Port-In
          $success = $this->changeStatePortIn($webPosActionData);
          break;
      }

      if ($success)
      {
        // update ULTRA.WEBPOS_ACTIONS according to API result
        if ( ! $this->updateWebPosActionDataSuccess( $webPosActionData->UUID ))
        {
          $error = 'ERROR: updating ULTRA.WEBPOS_ACTION status to DONE for UUID : ' . $webPosActionData->UUID;
        }
      }
      else
      {
        if ( ! $this->updateWebPosActionDataError( $webPosActionData->UUID ))
        {
          $error = 'ERROR: updating ULTRA.WEBPOS_ACTION status to ERROR for UUID : ' . $webPosActionData->UUID;
        }

        $error = 'ERROR: error initiating state transition';
      }
    }

    return $error;
  }

  /**
   * getWebPosActionData
   *
   * gets row from ULTRA.WEBPOS_ACTIONS
   * 
   * @param  String $uuid        transaction identifier
   * @return Object $transaction row from ULTRA.WEBPOS_ACTIONS
   */
  public function getWebPosActionData($uuid)
  {
    $transaction = ultra_webpos_actions_get_transaction($uuid);

    dlog('', 'webPosActionData = %s', $transaction);

    return $transaction;
  }

  /**
   * updateWebPosActionDataSuccess
   *
   * updates status to DONE for ULTRA.WEBPOS_ACTIONS row
   * 
   * @param  String  $uuid    transaction identifier
   * @return Boolean $success SQL query sucessful?
   */
  public function updateWebPosActionDataSuccess( $uuid )
  {
    $success = ultra_webpos_actions_update_transaction_status( $uuid, 'DONE' );
    dlog('', 'updatewebPosActionDataSuccess '.( $success ? 'succeeded' : 'failed' ));
    return $success;
  }

  /**
   * updateWebPosActionDataError
   *
   * updates status to ERROR for ULTRA.WEBPOS_ACTIONS row
   * 
   * @param  String  $uuid    transaction identifier
   * @return Boolean $success SQL query sucessful?
   */
  public function updateWebPosActionDataError( $uuid )
  {
    $success = ultra_webpos_actions_update_transaction_status( $uuid, 'ERROR' );
    dlog('', 'updatewebPosActionDataError '.( $success ? 'succeeded' : 'failed' ));
    return $success;
  }

  /**
   * changeStateActivation
   * @param  Object $webPosActionData row from ULTRA.WEBPOS_ACTIONS
   * @return Boolean TRUE on SUCCESS
   */
  public function changeStateActivation($webPosActionData)
  {
    $success = TRUE;

    $context = array('customer_id' => $webPosActionData->CUSTOMER_ID);
    $transition_name  = 'Activate ' . $webPosActionData->PLAN_NAME;

    $error = $this->performChangeState($context, $transition_name);
    if ($error)
    {
      dlog('', $error);
      $success = FALSE;
    }

    return $success;
  }

  /**
   * changeStatePortIn
   * @param  Object  $webPosActionData row from ULTRA.WEBPOS_ACTIONS
   * @return Boolean $success          TRUE on SUCCESS
   */
  public function changeStatePortIn($webPosActionData)
  {
    $success = TRUE;

    $context = array(
      'customer_id'              => $webPosActionData->CUSTOMER_ID,
      'port_in_msisdn'           => $webPosActionData->MSISDN,
      'port_in_iccid'            => $webPosActionData->ICCID,
      'port_in_account_number'   => $webPosActionData->PORT_ACCOUNT_NUMBER,
      'port_in_account_password' => $webPosActionData->PORT_ACCOUNT_PASSWORD,
      'port_in_zipcode'          => $webPosActionData->PORT_ACCOUNT_ZIPCODE
    );
    $transition_name  = 'Request Port ' . $webPosActionData->PLAN_NAME;

    $error = $this->performChangeState($context, $transition_name);
    if ($error)
    {
      dlog('', $error);
      $success = FALSE;
    }

    return $success;
  }

  /**
   * performChangeState
   * @param  Object $context
   * @param  String $transition_name
   * @return $error BOOL || error string
   */
  public function performChangeState($context, $transition_name)
  {
    $error = FALSE;

    $resolve_now      = FALSE;
    $max_path_depth   = 1;

    // try dry_run state change
    $dry_run = TRUE;

    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    if ($result_status['success'] != 1)
    {
      $error = ( ! empty($result_status['errors']) && count($result_status['errors']))
        ? $result_status['errors'][0]
        : 'ERROR: state transition_error';
    }
    else
    {
      // perform actual state change
      $dry_run = FALSE;

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

      if ($result_status['success'] != 1)
      {
        $error = ( ! empty($result_status['errors']) && count($result_status['errors']))
          ? $result_status['errors'][0]
          : 'ERROR: state transition_error';
      }
    }

    return $error;
  }
} // END CLASS
