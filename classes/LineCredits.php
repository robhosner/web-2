<?php

define('LINE_CREDIT_REGULAR',      1);
define('LINE_CREDIT_AUTO_RENEWAL', 2);
define('LINE_CREDIT_BOLTON',       3);
define('MAX_LINE_CREDITS',        16);

class LineCredits
{
  protected $pdo = null;

  public function __construct()
  {
    $this->dbConnect();
  }

  /**
   * automatically connects to line credit database
   */
  public function dbConnect()
  {
    return \Ultra\Lib\DB\line_credits_connect();
  }

  /**
   * checks if a customer_id can receive amount of line credits
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function canReceiveLineCredits($customer_id, $amount)
  {
    $balance = $this->getBalance($customer_id);
    return ($balance + $amount > MAX_LINE_CREDITS)
      ? make_error_Result("Customer $customer_id would exceed maximum allowed credits " . MAX_LINE_CREDITS)
      : make_ok_Result();
  }

  /**
   * attempts to activate suspended or provisioned customer_id
   * if customer_id was_activated, Result object will contain
   * boolean was_activated in the data array
   * @param  integer
   * @return Result
   */
  public function tryActivation($customer_id)
  {
    $result = new \Result();
    $wasActivated = false;

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, ['plan_state']);

      if ( ! $customer || ! isset($customer->plan_state))
        throw new Exception("Error retrieving customer $customer_id plan_state");

      $plan_state = $customer->plan_state;

      if ( ! in_array($plan_state, [STATE_SUSPENDED,STATE_PROVISIONED]))
        throw new Exception("Customer in invalid state to activate {$plan_state}");

      // attempt to transition customer state to active
      $context = [ 'customer_id' => $customer_id, 'use_line_credit' => 1 ];
          $ret = change_state($context, false, 'FLEX_FIFTEEN', 'Active', false);

      // check if transition was successful
      if ( ! $ret['success'] || $ret['aborted'])
      {
        if (count($ret['errors']))
          throw new Exception($ret['errors'][0]);

        throw new Exception("Error transitioning customer $customer_id state to active");
      }

      //
      $wasActivated = true;

      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $result->add_error($e->getMessage());
    }

    $result->add_to_data_array('was_activated', $wasActivated);

    $this->dbConnect();

    return $result;
  }

  /**
   * Checks if customer is inactive, before transferring credit
   * @param  integer
   * @return bool
   */
  public function shouldTransfer($customer_id)
  {
    try
    {
      teldata_change_db();

      // check for open to Active transitions
      $active = get_by_column('htt_transition_log',
                               array('customer_id' => $customer_id),
                               " AND status <> 'ABORTED' AND status <> 'CLOSED'
                                 AND to_cos_id IS NOT NULL
                                 AND to_plan_state IS NOT NULL
                                 AND to_plan_state = 'Active'");

      if ($active != NULL) {
        dlog('', 'Open to Active transition detected where customer_id = ' . $customer_id);
        return false;
      }

      // check current plan_state
      $customer = get_ultra_customer_from_customer_id($customer_id, ['plan_state']);

      if ( ! $customer || ! isset($customer->plan_state))
        throw new Exception("Error retrieving customer $customer_id plan_state");

      $plan_state = $customer->plan_state;

      $this->dbConnect();

      return in_array($plan_state, [STATE_SUSPENDED,STATE_PROVISIONED]);
    }
    catch (Exception $e)
    {
      $this->dbConnect();

      \dlog('', $e->getMessage());
      return false;
    }
  }

  /**
   * gets line credit costs from DB
   * @return object[]
   */
  public function getCosts()
  {
    $rows = [];

    try
    {
      $sql  = "SELECT CreditsPurchased,CreditsTotalCost FROM LineCreditCost with (nolock)";
      $rows = mssql_fetch_all_objects(logged_mssql_query($sql));
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $rows = [];
    }

    return $rows;
  }

  /**
   * gets aggregated balance of array of customer_ids
   * @param  Array of customer ids
   * @return integer
   */
  public function getAggregatedBalance($customer_ids)
  {
    $row = null;

    try
    {
      $in  = implode(',', $customer_ids);
      $sql = "SELECT SUM (BALANCE) as Balance FROM LineCreditWallet WHERE CUSTOMER_ID IN ($in)";
      $row = find_first($sql);
    }
    catch (Exception $e)
    {
      \logError($e->getMessage());
    }

    return ($row && $row->Balance) ? $row->Balance : 0;
  }

  /**
   * returns line credit wallet balance
   * @param  integer
   * @return integer
   */
  public function getBalance($customer_id)
  {
    $row = null;

    try
    {
      $sql = "SELECT TOP 1 * FROM LineCreditWallet WHERE CUSTOMER_ID = $customer_id";
      $row = find_first($sql);
    }
    catch (Exception $e)
    {
      \logError($e->getMessage());
    }

    return ($row && $row->Balance) ? $row->Balance : 0;
  }

  /**
   * returns if customer_id has a line credit in wallet
   * @param  integer
   * @return bool
   */
  public function hasLineCredit($customer_id)
  {
    return $this->getBalance($customer_id);
  }

  /**
   * increments line credits by amount
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function incrementLineCredit($customer_id, $amount)
  {
    $result = new \Result();

    try
    {
      $sql = "exec [dbo].[incrementLineCredit]
              @CUSTOMER_ID = $customer_id,
              @Amount = $amount";

      if ( ! run_sql_and_check($sql))
        throw new Exception("Error incrementing wallet balance for customer_id $customer_id");

      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * decrements line credits by 1
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function decrementLineCredit($customer_id)
  {
    $result = new \Result();

    try
    {
      $sql = "UPDATE LineCreditWallet SET BALANCE = BALANCE - 1 WHERE CUSTOMER_ID = $customer_id";
      if ( ! run_sql_and_check($sql))
        throw new Exception("Error decrementing wallet balance for customer_id $customer_id");

      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * transfer line credits from one customer_id to another customer_id
   * @param  integer
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function transferLineCredits($from_customer_id, $to_customer_id, $amount = 1)
  {
    $result = new \Result();

    try
    {
      $sql = "SELECT TOP 1 * FROM LineCreditWallet with (nolock) WHERE CUSTOMER_ID = $from_customer_id";
      $row = find_first($sql);

      if ( ! $row)
        throw new Exception("LineCreditWallet does not exist for parent customer_id $from_customer_id");

      \logDebug(json_encode($row));

      if ($row->Balance < $amount)
        throw new Exception("LineCreditWallet for customer_id $from_customer_id does not have sufficient balance");

      $sql = "exec [dbo].[transferLineCredits]
              @from_customer_id = $from_customer_id,
              @to_customer_id = $to_customer_id,
              @Amount = $amount";

      $check = run_sql_and_check($sql);
      if ( ! $check)
        throw new Exception('Error transferring line credit');

      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * records transfer of line credits
   * @param  integer
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function recordTransferHistory($from_customer_id, $to_customer_id, $amount)
  {
    $result = new \Result();

    try
    {
      $sql = "exec [dbo].[recordTransferHistory]
              @from_customer_id = $from_customer_id,
              @to_customer_id = $to_customer_id,
              @Amount = $amount";

      $data = find_first($sql);

      if ( ! $data || ! $data->last_insert_id)
        throw new Exception('Error transferring line credit(s)');

      \dlog('', 'last insert id %s', $data->last_insert_id);
      $result->add_to_data_array('last_insert_id', $data->last_insert_id);
      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * records line credit usage history
   * @param  integer
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function recordUsageHistory($customer_id, $payer_customer_id, $amount)
  {
    $result = new \Result();

    try
    {
      $sql = "exec [dbo].[recordUsageHistory]
              @Customer_ID = $customer_id,
              @Customer_ID_Payer = $payer_customer_id,
              @amount = $amount";

      $data = find_first($sql);

      if ( ! $data || ! $data->last_insert_id)
        throw new Exception('Error recording usage history');

      \dlog('', 'last insert id %s', $data->last_insert_id);
      $result->add_to_data_array('last_insert_id', $data->last_insert_id);
      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * records line credit purchase history
   * @param  integer
   * @param  integer
   * @param  integer
   * @return Result
   */
  public function recordPurchaseHistory($customer_id, $amount, $type)
  {
    $result = new \Result();

    try
    {
      $sql = "exec [dbo].[recordPurchaseHistory]
              @Customer_ID = $customer_id,
              @Amount = $amount,
              @LineCreditPurchaseTypeID = $type";

      $check = run_sql_and_check($sql);
      if ( ! $check)
        throw new Exception('Error recording purchase history');

      $result->succeed();
    }
    catch (Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $result;
  }

  /**
   * performs line credit transfer
   * @param  integer
   * @param  integer
   * @param  integer
   * @return [Result, string]
   */
  public function doTransfer($from_customer_id, $to_customer_id, $amount)
  {
    $warnings = [];

    $result = $this->canReceiveLineCredits($to_customer_id, $amount);
    if ( ! $result->is_success())
      return [$result, 'IN0001'];

    // transfer line credits
    $result = $this->transferLineCredits($from_customer_id, $to_customer_id, $amount);
    if ( ! $result->is_success())
      return [$result, 'IN0001'];

    // record transfer
    $result = $this->recordTransferHistory($from_customer_id, $to_customer_id, $amount);
    if ( ! $result->is_success())
      return [$result, 'IN0001'];

    return $this->useLineCredit($to_customer_id);
  }

  /**
   * performs line credit addition
   * @param  integer
   * @param  integer
   * @return [Result, string]
   */
  public function doIncrement($customer_id, $amount)
  {
    $warnings = [];

    $result = $this->canReceiveLineCredits($customer_id, $amount);
    if ( ! $result->is_success())
      return [$result, 'IN0001'];

    // increment line credits
    $result = $this->incrementLineCredit($customer_id, $amount);
    if ( ! $result->is_success())
      return [$result, 'IN0001'];

    return $this->useLineCredit($customer_id);
  }

  /**
   * uses a line credit to try activation
   * @param  integer
   * @return [Result, string]
   */
  public function useLineCredit($customer_id, $tryActivation = true)
  {
    $warnings = [];

    // try activation
    $result = $this->tryActivation($customer_id);
    if ( ! $result->is_success())
    {
      $errors     = $result->get_errors();
      $warnings[] = (count($errors)) ? $errors[0] : "Unknown error activating customer $customer_id";
    }

    // if customer was activated record necessary data
    if ($result->data_array['was_activated'] || ! $tryActivation)
    {
      // decrement line credit
      $result = $this->decrementLineCredit($customer_id);
      if ( ! $result->is_success())
        return [$result, 'IN0001'];

      // record line credit usage
      $result = $this->recordUsageHistory($customer_id, $customer_id, 1);
      if ( ! $result->is_success())
        return [$result, 'IN0001'];
    }

    $result = new \Result();
    $result->add_warnings($warnings);
    $result->succeed();

    return [$result, null];
  }
}
