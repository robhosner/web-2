<?php

namespace Psr\Log;

require_once 'classes/LoggerInterface.php';
require_once 'classes/LogLevel.php';

class Logger implements \Psr\Log\LoggerInterface
{
  /**
   * System is unusable.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function emergency($message, array $context = array())
  {
    \logFatal($message);
  }

  /**
   * Action must be taken immediately.
   *
   * Example: Entire website down, database unavailable, etc. This should
   * trigger the SMS alerts and wake you up.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function alert($message, array $context = array())
  {
    \logFatal($message);
  }

  /**
   * Critical conditions.
   *
   * Example: Application component unavailable, unexpected exception.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function critical($message, array $context = array())
  {
    \logFatal($message);
  }

  /**
   * Runtime errors that do not require immediate action but should typically
   * be logged and monitored.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function error($message, array $context = array())
  {
    \logError($message);
  }

  /**
   * Exceptional occurrences that are not errors.
   *
   * Example: Use of deprecated APIs, poor use of an API, undesirable things
   * that are not necessarily wrong.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function warning($message, array $context = array())
  {
    \logWarn($message);
  }

  /**
   * Normal but significant events.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function notice($message, array $context = array())
  {
    \logInfo($message);
  }

  /**
   * Interesting events.
   *
   * Example: User logs in, SQL logs.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function info($message, array $context = array())
  {
    \logInfo($message);
  }

  /**
   * Detailed debug information.
   *
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function debug($message, array $context = array())
  {
    \logDebug($message);
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed  $level
   * @param string $message
   * @param array  $context
   *
   * @return null
   */
  public function log($level, $message, array $context = array())
  {
    \logit($message);
  }
}