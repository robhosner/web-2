<?php

namespace ProjectW;

/*
See http://wiki.hometowntelecom.com:8090/display/SPEC/Project+W+-+Database+Considerations+During+Migration

Checklist:

CUSTOMERS
 - FIRST_NAME    From import file
 - LAST_NAME     From import file
 - POSTAL_CODE   From import file
 - E_MAIL        From import file

HTT_CUSTOMERS_OVERLAY_ULTRA
 - CURRENT_MOBILE_NUMBER   From import file
 - CURRENT_ICCID           From import file
 - CURRENT_ICCID_FULL      From import file
 - PREFERRED_LANGUAGE      From import file
 - PLAN_STARTED            From import file
 - PLAN_EXPIRES            From import file
 - ACTIVATION_ICCID        From import file
 - ACTIVATION_ICCID_FULL   From import file
 - CUSTOMER_SOURCE         "UVIMPORT"
 - GROSS_ADD_DATE          RENEWAL_DATE - 30
 - BRAND_ID                2
 - TOS_ACCEPTED            NULL
 - EASYPAY_ACTIVATED       0
 - MONTHLY_CC_RENEWAL      0

HTT_TRANSITION_LOG
 - we will add fake rows

HTT_PLAN_TRACKER
 - 1 new row as a brand new activation
 - PLAN_STARTED   as GROSS_ADD_DATE
 - PLAN_EXPIRES   as RENEWAL_DATE

HTT_INVENTORY_SIM
 - ICCID_NUMBER            From import file
 - ICCID_FULL              From import file
 - IMSI                    From import file
 - SIM_ACTIVATED           1
 - INVENTORY_MASTERAGENT   690
 - INVENTORY_DISTRIBUTOR   NULL
 - INVENTORY_DEALER        TODO: dealer
 - CUSTOMER_ID             CUSTOMER_ID
 - INVENTORY_STATUS        SHIPPED_MASTER
 - LAST_CHANGED_DATE       <migration date>
 - LAST_CHANGED_BY         UVIMPORT
 - CREATED_BY_DATE         <migration date>
 - CREATED_BY              UVIMPORT	 	 
 - ICCID_BATCH_ID          UVIMPORT
 - EXPIRES_DATE            NULL
 - LAST_TRANSITION_UUID    NULL
 - PIN1                    NULL
 - PUK1                    NULL
 - PIN2                    NULL
 - PUK2                    NULL
 - OTHER                   NULL
 - RESERVATION_TIME        <migration date>
 - GLOBALLY_USED           1
 - ACT_CODE                NULL
 - PRODUCT_TYPE            PURPLE
 - STORED_VALUE            0
 - SIM_HOT                 1
 - MVNE                    2
 - OFFER_ID                NULL
 - BRAND_ID                2

HTT_ACTIVATION_LOG
 - ICCID_NUMBER            From import file
 - ICCID_FULL              From import file
 - ACTIVATED_CUSTOMER_ID   CUSTOMER_ID
 - ACTIVATED_USERID        67
 - ACTIVATED_DATE          GROSS_ADD_DATE
 - ACTIVATED_DISTRIBUTOR   0
 - PROMISED_AMOUNT         PLAN COST
 - INITIAL_COS_ID          COS_ID
 - INITIAL_BOLTONS         NULL

//TODO
  - ACTIVATED_MASTERAGENT	masteragent
  - ACTIVATED_DEALER	 dealer
  - ACTIVATED_BY	 dealer


ULTRA.UNIVISION_CODE_TO_DEALER_CODE
 - there are some univision_code values in the migration file which will not exist in ULTRA.UNIVISION_CODE_TO_DEALER_CODE. For these cases, default to:
    dealer = 34241
    masteragent = 690



*/

/**
 * addToActivationLog
 *
 * Insert a row into HTT_ACTIVATION_LOG
 * Returns an error message if fails, NULL in case of success
 * Input:
 *  - customer_id
 *  - iccid (18 digits)
 *  - masteragent
 *  - distributor
 *  - store
 *  - user_id
 *  - agent
 *  - initial_cos_id
 *
 * @return string
 */
function addToActivationLog( $params )
{
  $sql = htt_activation_log_insert_query(
    array(
      'iccid'            => $params['iccid'], // htt_activation_log.ICCID_NUMBER lenght is 18
      'customer_id'      => $params['customer_id'],
      'masteragent'      => $params['masteragent'],
      'distributor'      => '', // must be empty
      'dealer'           => $params['dealer'],
      'user_id'          => 67, // must be 67
      'date'             => 'getutcdate()',
      'activated_by'     => $params['dealer'], // must be same as ACTIVATED_DEALER
      'promised_amount'  => $params['plan_cost'],
      'initial_cos_id'   => $params['cos_id'],
      'initial_bolt_ons' => ''
    )
  );

  if ( ! is_mssql_successful( logged_mssql_query( $sql ) ) )
    return 'Could not insert data into HTT_ACTIVATION_LOG';

  return NULL;
}

/**
 * addToPlanTracker
 *
 * Insert a row into HTT_PLAN_TRACKER
 * Returns an error message if fails, NULL in case of success
 * Input:
 *  - customer_id
 *  - cos_id
 *  - plan_started
 *  - plan_expires
 *
 * @return boolean
 */
function addToPlanTracker( $params )
{
  $sql = $htt_plan_tracker_insert_query = htt_plan_tracker_insert_query(
    array(
      'customer_id'      => $params['customer_id'],
      'cos_id'           => $params['cos_id'],
      'plan_started_sql' => $params['plan_started_sql'],
      'plan_expires'     => $params['plan_expires'],
      'period'           => 1,
      'plan_updated'     => 'NULL'
    )
  );

  return ! ! is_mssql_successful( logged_mssql_query( $sql ));
}

/**
 * addToWholesalePlanTracker
 *
 * Insert a row into ULTRA.WHOLESALE_PLAN_TRACKER
 * Returns an error message if fails, NULL in case of success
 * Input:
 *  - customer_id
 *  - wholesale
 *
 * @return string
 */
function addToWholesalePlanTracker( $customer_id , $wholesale )
{
  $success = \Ultra\Lib\DB\Customer\setWholesalePlan( $customer_id , $wholesale );

  if ( ! $success )
    return 'Could not insert data into ULTRA.WHOLESALE_PLAN_TRACKER';

  return NULL;
}

/**
 * addToBillingHistory
 *
 * Insert a row into HTT_BILLING_HISTORY
 * Returns an error message if fails, NULL in case of success
 * Input:
 *  - customer_id
 *  - balance_change
 *  - package_balance_change
 *  - cos_id
 *  - detail
 *  - description
 *  - reference
 *
 * @return boolean
 */
function addToBillingHistory( $params )
{
  $sql = htt_billing_history_insert_query(
    array(
      'customer_id'            => $params['customer_id'],
      'date'                   => 'now',
      'source'                 => $params['source'],
      'result'                 => 'COMPLETE',
      'entry_type'             => $params['entry_type'],
      'is_commissionable'      => 0,
      'stored_value_change'    => 0,
      'balance_change'         => $params['balance_change'],
      'package_balance_change' => $params['package_balance_change'],
      'charge_amount'          => $params['charge_amount'],
      'cos_id'                 => $params['cos_id'],
      'detail'                 => $params['detail'],
      'description'            => $params['description'],
      'reference'              => $params['reference'],
      'reference_source'       => get_reference_source('UVIMPORT'),
      'terminal_id'            => '0',
      'commissionable_charge_amount' => $params['commissionable_charge_amount']
    )
  );

  return ! ! is_mssql_successful( logged_mssql_query( $sql ) );
}

/**
 * createCustomer
 *
 * Creates customer in DB tables:
 *  - CUSTOMERS
 *  - ACCOUNTS
 *  - HTT_CUSTOMERS_OVERLAY_ULTRA
 *  - HTT_PREFERENCES_MARKETING
 *  - ULTRA.CUSTOMERS_DEALERS
 *  - ULTRA.HTT_ACTIVATION_HISTORY
 *
 * Input:
 *  - masteragent
 *  - distributor
 *  - dealer
 *  - userid
 *  - preferred_language
 *  - cos_id
 *  - zip_code
 *  - current_iccid
 *  - msisdn
 *  - plan_cost
 *
 * @return integer
 */
function createCustomer( $params )
{
  $params = [
    'masteragent'           => $params['masteragent'],
    #'distributor'           => $params['distributor'],
    'dealer'                => $params['dealer'],
    'userid'                => $params['userid'],
    'preferred_language'    => $params['preferred_language'],
    'cos_id'                => $params['cos_id'],
    'activation_cos_id'     => $params['cos_id'],
    'postal_code'           => $params['zip_code'],
    'country'               => DEFAULT_ULTRA_COUNTRY,
    'final_state'           => FINAL_STATE_COMPLETE,
    'plan_state'            => STATE_PORT_IN_REQUESTED,
    'plan_started'          => 'NULL',
    'plan_expires'          => 'NULL',
    'customer_source'       => 'UVIMPORT', // verified with specs
    'current_iccid'         => substr($params['current_iccid'], 0, -1),
    'current_iccid_full'    => $params['current_iccid'],
    'current_mobile_number' => $params['msisdn'],
    'promised_amount'       => $params['plan_cost'],
    'activation_type'       => 'NEW',
    'funding_source'        => 'UVIMPORT',
    'funding_amount'        => $params['plan_cost'],
    'first_name'            => $params['first_name'],
    'last_name'             => $params['last_name'],
    'address1'              => $params['address1'],
    'address2'              => $params['address2'],
    'city'                  => $params['city'],
    'state_region'          => $params['state'],
    'e_mail'                => $params['e_mail']
  ];

  $result = \create_ultra_customer_db_transaction($params);

  if ( empty( $result['customer'] ) )
    return NULL;

  dlog('',"Returning CUSTOMER_ID {$result['customer']->CUSTOMER_ID}");

  return $result['customer']->CUSTOMER_ID;
}

/**
 * addFakePortInTransition
 *
 * For consistency, we add a fake starte transition
 *
 * @return boolean
 */
function addFakePortInTransition( $customerData , $cos_id )
{
  $sql = \htt_transition_log_insert_query([
    'customer_id'     => $customerData->CUSTOMER_ID,
    'transition_uuid' => \getNewTransitionUUID('PRJW'),
    'status'          => STATUS_CLOSED,
    'from_cos_id'     => \get_cos_id_from_plan('STANDBY'),
    'to_cos_id'       => $cos_id,
    'from_plan_state' => STATE_NEUTRAL,
    'to_plan_state'   => STATE_PORT_IN_REQUESTED,
    'context'         => json_encode([
      'customer_id'              => $customerData->CUSTOMER_ID,
      'port_in_msisdn'           => $customerData->MSISDN,
      'port_in_iccid'            => $customerData->TEMP_ICCID_FULL,
      'port_in_account_number'   => $customerData->ACCOUNT_NUMBER,
      'port_in_account_password' => $customerData->ACCOUNT_PIN,
      'port_in_zipcode'          => $customerData->ZIP_CODE
    ])
  ]);

  return ! ! is_mssql_successful( logged_mssql_query( $sql ) );
}

/**
 * addFakeSuspendTransition
 *
 * @return boolean
 */
function addFakeSuspendTransition( $customer_id , $cos_id )
{
  $sql = \htt_transition_log_insert_query([
    'customer_id'     => $customer_id,
    'transition_uuid' => \getNewTransitionUUID('PRJW'),
    'status'          => STATUS_CLOSED,
    'from_cos_id'     => $cos_id,
    'to_cos_id'       => $cos_id,
    'from_plan_state' => STATE_ACTIVE,
    'to_plan_state'   => STATE_SUSPENDED,
    'context'         => '[{"customer_id":'.$customer_id.',"test":"PRJW"}]'
  ]);

  return ! ! is_mssql_successful( logged_mssql_query( $sql ) );
}

/**
 * addFakeActivationTransition
 *
 * @return boolean
 */
function addFakeActivationTransition( $customer_id , $cos_id )
{
  $sql = \htt_transition_log_insert_query([
    'customer_id'     => $customer_id,
    'transition_uuid' => \getNewTransitionUUID('PRJW'),
    'status'          => STATUS_CLOSED,
    'from_cos_id'     => $cos_id,
    'to_cos_id'       => $cos_id,
    'from_plan_state' => STATE_PORT_IN_REQUESTED,
    'to_plan_state'   => STATE_ACTIVE,
    'context'         => '[{"customer_id":'.$customer_id.',"test":"PRJW"}]'
  ]);

  return ! ! is_mssql_successful( logged_mssql_query( $sql ) );
}

/**
 * getFailedImportRows
 *
 * returns rows on UNIVISION_IMPORT where LAST_ERROR is NOT NULL
 * @param String import batch file name
 * @param Integer max number of recent rows to return
 * @return Array of Objects
 */
function getFailedImportRows($batch, $max)
{
  $sql = sprintf('SELECT TOP %d
    UNIVISION_IMPORT_ID, SUBS_NUMBER, SUBS_ID, STATUS, LAST_ERROR, LAST_ERROR_DATE, ICCID_FULL, TEMP_ICCID_FULL, MSISDN, IMEI
    FROM ULTRA.UNIVISION_IMPORT WITH (NOLOCK)
    WHERE LAST_ERROR IS NOT NULL
    AND IMPORT_FILE_NAME = %s
    ORDER BY UNIVISION_IMPORT_ID',
    $max,
    mssql_escape_with_zeroes($batch));

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * updateInventorySimCustomerId
 *
 * Updates HTT_INVENTORY_SIM.CUSTOMER_ID
 *
 * @return boolean
 */
function updateInventorySimCustomerId( $customerData )
{
  $sql = \htt_inventory_sim_update_query([
    'customer_id'   => $customerData->CUSTOMER_ID,
    'batch_sim_set' => array( $customerData->ICCID_FULL )
  ]);

  return ! ! is_mssql_successful( logged_mssql_query( $sql ) );
}

/**
 * addInventorySim
 *
 * Creates an HTT_INVENTORY_SIM record for the customer
 * Do not insert if the SIM is already in DB
 *
 * @param Object customerData
 * @param string migrationDate
 * @return boolean
 */
function addInventorySim( $customerData, $migrationDate )
{
  dlog('',"customerData  = %s",$customerData);
  dlog('',"migrationDate = %s",$migrationDate);

  $sql = htt_inventory_sim_select_query([
    'iccid' => $customerData['ICCID_FULL']
  ]);

  if ( find_first($sql) )
  {
    \logWarn("SIM {$customerData['ICCID_FULL']} already in DB");

    return TRUE;
  }
  else
    \logInfo("SIM {$customerData['ICCID_FULL']} not in DB: adding");

  $params = array(
    'iccid_number'          => mssql_escape_with_zeroes($customerData['ICCID_NUMBER']),
    'iccid_full'            => mssql_escape_with_zeroes($customerData['ICCID_FULL']),
    'imsi'                  => mssql_escape_with_zeroes($customerData['IMSI']),
    'sim_activated'         => 1,
    'inventory_masteragent' => UNIVISION_DEFAULT_MASTERAGENT,
    'inventory_distributor' => null,
    'inventory_dealer'      => $customerData['INVENTORY_DEALER'],
    'inventory_status'      => 'SHIPPED_MASTER',
    'last_changed_date'     => $migrationDate,
    'last_changed_by'       => 'UVIMPORT',
    'created_by_date'       => $migrationDate,
    'created_by'            => 'UVIMPORT',
    'iccid_batch_id'        => 'UVIMPORT',
    'expires_date'          => 'null',
    'pin1'                  => null,
    'puk1'                  => null,
    'pin2'                  => null,
    'puk2'                  => null,
    'other'                 => null,
    'reservation_time'      => $migrationDate,
    'globally_used'         => 1,
    'act_code'              => null,
    'product_type'          => 'PURPLE',
    'stored_value'          => 0,
    'sim_hot'               => 1,
    'mvne'                  => 2,
    'offer_id'              => null,
    'brand_id'              => 2
  );

  $sql = \htt_inventory_sim_insert_query( $params );

  return ! ! is_mssql_successful( logged_mssql_query( $sql ) );
}

/**
 * getDealerAndMasteragentFromUltraCode
 *
 * Given a ULTRA_CODE, return dealer and masteragent
 *
 * @return array
 */
function getDealerAndMasteragentFromUltraCode( $ultra_code )
{
  $result = getDealerInfoFromUltraCode( $ultra_code );

  // connect back to TSIP DB
  teldata_change_db();

  return [ $result->data_array['dealer'] , $result->data_array['masteragent'] ];
}

/**
 * getDealerInfoFromUltraCode
 *
 * Given a ULTRA_CODE, return dealer and masteragent by querying tblDealerSite
 *
 * @return object of class \Result
 */
function getDealerInfoFromUltraCode( $ultra_code )
{
  $result = new \Result(NULL, FALSE);

  if ( empty( $ultra_code ) )
  {
    // use default dealer/masteragent
    $result->add_data_array([
      'dealer'      => UNIVISION_DEFAULT_DEALER,
      'masteragent' => UNIVISION_DEFAULT_MASTERAGENT
    ]);

    return $result;
  }

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect() )
    $result->add_error('Dealer Portal DB connection failed');
  else
  {
    $data = mssql_fetch_all_objects( logged_mssql_query( getDealerInfoFromUltraCodeSQL( $ultra_code ) ) );

    if ( empty( $data ) || empty( $data[0] ) )
    {
      $result->add_warning( "No dealer/masteragent data found for ultra code '".$ultra_code."'" );

      \logWarn( "No dealer/masteragent data found for ultra code '".$ultra_code."'" );

      // use default dealer/masteragent
      $result->add_data_array([
        'dealer'      => UNIVISION_DEFAULT_DEALER,
        'masteragent' => UNIVISION_DEFAULT_MASTERAGENT
      ]);
    }
    else
      $result->add_data_array( (array) $data[0] );
  }

  return $result;
}

/**
 * getDealerInfoFromUltraCodeSQL
 *
 * SQL statement to retrieve dealer and masteragent by querying tblDealerSite by ultra_code
 *
 * @return string
 */
function getDealerInfoFromUltraCodeSQL( $ultra_code )
{
  return sprintf("
SELECT ds.dealerSiteId as dealer,
       ds.masterId     as masteragent
FROM   tblDealerSite ds
WHERE  ds.dealerCd = '%s'",
    $ultra_code
  );
}

