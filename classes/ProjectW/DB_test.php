<?php

require_once 'db.php';
require_once 'classes/ProjectW.php';
require_once 'classes/ProjectW/DB.php';

class Test_getDealerInfoFromUltraCode
{
  function test( $argv )
  {
    $ultra_code = 'a';

    $result = \ProjectW\getDealerInfoFromUltraCode( $ultra_code );

    print_r( $result );

    $ultra_code = 'DEAUG1';

    $result = \ProjectW\getDealerInfoFromUltraCode( $ultra_code );

    print_r( $result );
  }
}

class Test_addToActivationLog
{
  function test( $argv )
  {
    $params = [
      'iccid'           => '100111010101011000',
      'customer_id'     => 1,
      'store'           => '2',
      'masteragent'     => '3',
      'distributor'     => '4',
      'user_id'         => '5',
      'date'            => 'getutcdate()',
      'agent'           => '6',
      'initial_cos_id'  => '7'
    ];

    $error = \ProjectW\addToActivationLog( $params );
  }
}

class Test_addToPlanTracker
{
  function test( $argv )
  {
    $params = [
      'customer_id'  => 1,
      'cos_id'       => '4',
      'plan_started' => 'getutcdate()',
      'plan_expires' => 'Jan 19 2016 08:00:00:000AM'
    ];

    $error = \ProjectW\addToPlanTracker( $params );
  }
}

class Test_addToWholesalePlanTracker
{
  function test( $argv )
  {
    $customer_id = $argv[2];

    $wholesale   = 'TEST_WHOLESALE';

    $error = \ProjectW\addToWholesalePlanTracker( $customer_id , $wholesale );

    echo "wholesale = $wholesale ; error = $error\n";

    $error = \ProjectW\addToWholesalePlanTracker( $customer_id , UV_PRIMARY_WHOLESALE_PLAN );
  
    echo "wholesale = ".UV_PRIMARY_WHOLESALE_PLAN." ; error = $error\n";

    $error = \ProjectW\addToWholesalePlanTracker( $customer_id , UV_SECONDARY_WHOLESALE_PLAN );

    echo "wholesale = ".UV_SECONDARY_WHOLESALE_PLAN." ; error = $error\n";
  }
}

class Test_addToBillingHistory
{
  function test( $argv )
  {
    $params = [
      'customer_id'            => 1,
      'balance_change'         => 2,
      'package_balance_change' => 3,
      'cos_id'                 => 4,
      'detail'                 => 'test detail',
      'description'            => 'description',
      'reference'              => 'reference'
    ];

    if ( \ProjectW\addToBillingHistory( $params ) )
      echo "Ok\n";
    else
      echo "Ko\n";
  }
}

class Test_createCustomer
{
  function test( $argv )
  {
    $test_iccid = '100111010101011008';

    $sql = "update HTT_customers_overlay_ultra set current_iccid = null , CURRENT_ICCID_FULL = null where CURRENT_ICCID_FULL = '{$test_iccid}'";

    $success = is_mssql_successful(logged_mssql_query($sql));

    $params = [
      'masteragent'           => '4',
      'distributor'           => '3',
      'dealer'                => '2',
      'userid'                => '1',
      'preferred_language'    => 'ES',
      'cos_id'                => '55',
      'plan_cost'             => '55',
      'zip_code'              => '12345',
      'current_iccid'         => '100111010101011008',
      'msisdn'                => '1001001088',
      'first_name'            => 'test first_name',
      'last_name'             => 'test last_name',
      'address1'              => 'test address1',
      'address2'              => 'test address2',
      'city'                  => 'test city',
      'state'                 => 'CA',
      'e_mail'                => 'test@test.com'
    ];

    $customer_id = \ProjectW\createCustomer( $params );
  }
}

class Test_addInventorySim
{
  function test( $argv )
  {
    $customer = array(
      'ICCID_NUMBER'  => time(),
      'ICCID_FULL'    => time(),
      'IMSI'          => time()
    );

    var_dump( \ProjectW\addInventorySim( $customer, 'Jan 19 2016 08:00:00:000AM' ) );
  }
}

teldata_change_db();

$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$testObject = new $testClass();
$testObject->test( $argv );

