<?php

namespace ProjectW;

include_once 'Ultra/Lib/Util/Validator.php';
include_once 'db/univision/univision_import.php';

/**
 * Parser class for Project W
 *
 * Methods for parsing, validating and loading data into DB
 *
 * See
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Project+W+Migration
 * http://wiki.hometowntelecom.com:8090/display/ProjectW/Project+W+Plan+Configurations+-+New+Pricing
 * http://wiki.hometowntelecom.com:8090/pages/viewpage.action?title=Project+W+Balance+Reconciliation&spaceKey=ProjectW
 */

class Parser
{
  protected $filePath;
  protected $fileName;
  protected $migrationDate;
  protected $importDate;

  public function __construct( $filePath=NULL, $fileName=NULL , $migrationDate=NULL )
  {
    if ( ! empty( $filePath ) )
      $this->filePath = $filePath;

    if ( ! empty( $fileName ) )
      $this->fileName = $fileName;

    if ( ! empty( $migrationDate ) )
      $this->migrationDate = $migrationDate;
  }

  /**
   * import
   *
   * Import file in DB
   * Returns a string not NULL containing an error message
   *
   * @return \Outcome object
   */
  public function import( $filePath=NULL, $fileName=NULL , $migrationDate=NULL, $importDate=NULL )
  {
    $outcome = new \Outcome( null, true );

    if ( ! empty( $filePath ) )
      $this->filePath = $filePath;

    if ( ! empty( $fileName ) )
      $this->fileName = $fileName;

    if ( ! empty( $migrationDate ) )
      $this->migrationDate = $migrationDate;
    else
      $this->migrationDate = $this->getDefaultMigrationDate();

    if ( ! empty( $importDate ) )
      $this->importDate = $importDate;

    try
    {
      if ( empty( $this->filePath ) )
        throw new \Exception( 'filePath is empty' );

      if ( empty( $this->fileName ) )
        throw new \Exception( 'fileName is empty' );
      $fullPath = $this->filePath . $this->fileName;

      if ( ! file_exists( $fullPath ) )
        throw new \Exception( 'file does not exist : ' . $fullPath );

      // open file handle for reading
      $handle = fopen( $fullPath , 'r' );
      if ( ! $handle )
        throw new \Exception( 'Cannot open file for reading : ' . $fullPath );

      // open file handle for writing (log file)
      $logFilename = str_replace( '.csv', '.import.txt', $this->fileName );
      $logFileHandle = fopen( $filePath . $logFilename, 'w' );
      if ( ! $logFileHandle )
        throw new \Exception( 'Cannot open log file for writing: ' . $filePath . $logFilename );

      $lineCount = 1;

      // loop through each line
      while ( ( $line = fgets($handle) ) !== false )
      {
        $result = $this->processRow( $line , $lineCount , $this->fileName );
        $outcome->merge( $result );

        // write errors to log
        if ( !$result->is_success() )
        {
          list( $errors, $errorCodes, $userErrors ) = $outcome->get_errors_and_code();
          for ( $i = 0; $i < count( $errors ); $i++ )
            fwrite( $logFileHandle, 'ROW ' . $lineCount . ' - ' . $errors[$i] . PHP_EOL );
        }

        $lineCount++;
      }
      
      // close file handle
      fclose($handle);
    }
    catch( \Exception $e )
    {
      \logError( json_encode($e) );

      $outcome->add_errors_and_code( $e->getMessage(), 'IN0002', $e->getMessage() );
    }

    return $outcome;
  }

  /**
   * processRow
   *
   * Process a row
   *  - $row is a string
   *  - $lineCount is an integer
   *
   * @return \Outcome object
   */
  public function processRow( $row , $lineCount , $fileName )
  {
    $outcome = new \Outcome(NULL, TRUE);

    \logDebug( $row );

    // ignore blank newlines
    if ( $row == "\r\n" || $row == "\n" )
      return $outcome;

    $fieldSeparator = ',';

    // split
    $rowArray = explode( $fieldSeparator , $row );

    list( $rowErrors , $rowErrorCodes ) = $this->validateRow( $rowArray );

    $headerDefinition = $this->headerDefinition();

    if ( empty( $rowErrors ) )
    {
      // import in DB

      $params = array(
        'batch'           => $rowArray[0],
        'msisdn'          => $rowArray[2],
        'subs_number'     => $rowArray[2],
        'subs_id'         => $rowArray[1],
        'time_zone'       => $rowArray[3],
        'balance'         => ( $this->cleanseBalance( $rowArray[4] ) * 100 ),
        'data_allotted'   => $this->cleanseDataAmount( $rowArray[5] ),
        'data_used'       => $this->cleanseDataAmount( $rowArray[6] ),
        'data_remaining'  => $this->cleanseDataAmount( $rowArray[7] ),
        'plan_state'      => $this->getPlanStateFromStatusId( $rowArray[8] ),
        'plan_name'       => $this->getPlanNameFromPlanId( $rowArray[9] ),
        'house_account'   => ( ( $rowArray[9] == 6 ) ? 1 : 0 ), // computed from plan id
        'demo_account'    => ( ( $rowArray[9] == 7 ) ? 1 : 0 ), // computed from plan id
        'feature'         => $rowArray[10],
        'iccid_full'      => $this->cleanseICCID( $rowArray[11] ),
        'temp_iccid_full' => $this->cleanseICCID( $rowArray[12] ),
        'imsi'            => $this->cleanseIMSI( $rowArray[13] ), // we can ignore it during the import phase
        'account_number'  => (string) $rowArray[1], // TODO: confirm with TMO
        'account_pin'     => $rowArray[14], // PIN
        'renewal_date'    => $this->formatFileDate( $rowArray[15] ),
        'monthly_renewal_target' => $this->getPlanNameFromPlanId( $rowArray[16] ),
/*
ignored:
FUTURE_DATED_FEATURE,
FUTURE_DATED_FEATURE_DATE
*/
        'univision_code'  => $rowArray[20],
        'first_name'      => $rowArray[21],
        'last_name'       => $rowArray[22],
        'address'         => $this->cleanseAddress( $rowArray[23] ),
        'email'           => strtolower( trim( $rowArray[24] ) ),
        'zip_code'        => $rowArray[25],
        'language'        => $this->getLanguage( $rowArray[26] ),
        'imei'            => $this->cleanseIMEI( $rowArray[27] ), // we can ignore it during the import phase
        'file_name'       => $fileName,
        'file_date_time'  => $this->importDate,
        'activation_date' => $this->formatFileDate( $rowArray[28] )
      );

      // add to ULTRA.UNIVISION_IMPORT
      if ( \add_to_univision_import( $params ) )
      {
        $ultra_code = \get_dealer_code_from_univision_code( $params['univision_code'] );

        // get dealer and masteragent
        list( $dealer , $masteragent ) = \ProjectW\getDealerAndMasteragentFromUltraCode( $ultra_code );

        // add to HTT_INVENTORY_SIM (temporary sim)
        $success = \ProjectW\addInventorySim(
          [
          'ICCID_NUMBER'     => $params['temp_iccid_full'],
          'ICCID_FULL'       => $params['temp_iccid_full'],
          'IMSI'             => $params['imsi'],
          'INVENTORY_DEALER' => $dealer
          ],
          $this->importDate
        );

        if ( ! $success )
          \logError( "Cannot insert {$params['temp_iccid_full']} in HTT_INVENTORY_SIM" );

        // add to HTT_INVENTORY_SIM (actual sim)
        $success = \ProjectW\addInventorySim(
          [
          'ICCID_NUMBER'     => $params['iccid_full'],
          'ICCID_FULL'       => $params['iccid_full'],
          'IMSI'             => $params['imsi'],
          'INVENTORY_DEALER' => $dealer
          ],
          $this->importDate
        );

        if ( ! $success )
          \logError( "Cannot insert {$params['iccid_full']} in HTT_INVENTORY_SIM" );
      }
      else
      {
        \logError( 'DB error while inserting into ULTRA.UNIVISION_IMPORT' );

        $outcome->add_errors_and_code( 'DB error while inserting into ULTRA.UNIVISION_IMPORT' , 'DB0001' );
      }
    }
    else
    {
      \logError( 'Errors found at line ' . $lineCount );
      \logError( 'rowErrors     = ' . json_encode( $rowErrors     ) );
      \logError( 'rowErrorCodes = ' . json_encode( $rowErrorCodes ) );

      $n = count( $rowErrors );

      for ( $i = 0 ; $i < $n ; $i++ )
        $outcome->add_errors_and_code( $rowErrors[ $i ] , $rowErrorCodes[ $i ] );
    }

    return $outcome;
  }

  /**
   * getDefaultMigrationDate
   *
   * Returns the default migration date (UTC). I.e. today.
   *
   * @return string
   */
  public function getDefaultMigrationDate()
  {
    return \get_utc_date_formatted_for_mssql().'000';
  }

  /**
   * formatFileDate
   *
   * Formats date to \d\d\/\d\d\/\d\d\d\d
   * Examples:
   *  '02/20/2016' -> '02/20/2016'
   *  '2-jan-2015' -> '01/02/2015'
   *
   * @return string
   */
  public function formatFileDate( $date )
  {
    if ( empty( $date ) )
      return NULL;

    if ( ! preg_match( "/\d/" , $date , $matches ) )
    {
      \logError("< $date > is an invalid date");
      return NULL;
    }

    // strip hours, minutes, ...

    $parts = explode( ' ' , $date );

    $date = $parts[0];

    if ( preg_match( "/^\s*(\d\d?)[\-\/](\w\w\w)[\-\/](\d\d\d\d)/" , $date , $matches ) )
      return \monthNamesToDigit( $matches[2] ).'/'.str_pad($matches[1],2,'0',STR_PAD_LEFT).'/'.$matches[3];
    elseif ( preg_match( "/^\s*(\d\d?)[\-\/](\w\w\w)[\-\/](\d\d)/" , $date , $matches ) )
      return \monthNamesToDigit( $matches[2] ).'/'.str_pad($matches[1],2,'0',STR_PAD_LEFT).'/20'.$matches[3];
    elseif ( preg_match( "/^\s*(\d\d?)[\-\/](\d\d?)[\-\/](\d\d\d\d)/" , $date , $matches ) )
      return str_pad($matches[1],2,'0',STR_PAD_LEFT).'/'.str_pad($matches[2],2,'0',STR_PAD_LEFT).'/'.$matches[3];
    elseif ( preg_match( "/^\s*(\d\d?)[\-\/](\d\d?)[\-\/](\d\d)/" , $date , $matches ) )
      return str_pad($matches[1],2,'0',STR_PAD_LEFT).'/'.str_pad($matches[2],2,'0',STR_PAD_LEFT).'/20'.$matches[3];

    return $date;
  }

  public function cleanseBalance( $balance )
  {
    $balance = trim( $balance );

    if ( preg_match( "/\#/" , $balance , $matches ) )
      return 0;

    return $balance;
  }

  /**
   * cleanseDataAmount
   *
   * Remove invalid characters from DATA ALLOTTED, DATA USED, DATA REMAINING
   *
   * @return string
   */
  public function cleanseDataAmount( $string )
  {
    $string = trim( $string );

    if ( preg_match( "/\D/" , $string , $matches ) )
      return '';

    return $string;
  }

  /**
   * cleanseAddress
   *
   * Remove invalid characters from Address
   *
   * @return string
   */
  public function cleanseAddress( $address )
  {
    // cleanup address
    if ( preg_match( "/^\s*-1\s*$/" , $address , $matches ) )
      $address = '';

    return $address;
  }

  /**
   * cleanseICCID
   *
   * Remove invalid characters from ICCID
   *
   * @return string
   */
  public function cleanseICCID( $iccid )
  {
    return luhnenize( preg_replace( '/\D/' , '' , $iccid ) );
  }

  /**
   * cleanseIMSI
   *
   * Remove invalid characters from IMSI
   *
   * @return string
   */
  public function cleanseIMSI( $imsi )
  {
    return preg_replace( '/\D/' , '' , $imsi );
  }

  /**
   * cleanseIMEI
   *
   * Remove invalid characters from IMEI
   *
   * @return string
   */
  public function cleanseIMEI( $imei )
  {
    return preg_replace( '/\D/' , '' , $imei );
  }

  /**
   * getPlanNameFromPlanId
   *
   * Maps import file plan Id to Ultra Plan Name
   *   45 ($45 Unl Talk, Txt, Web (2GB 4G LTE) + Intl T/T/Roam)
   *   46 ($55 Unl Talk, Txt, Web (5GB 4G LTE) + Intl T/T/Roam)
   *   58 ($30 Unl Talk, Text + Intl Talk, Text, Roaming)
   *   79 ($35 Unl Talk, Txt, Web (500 MB 4G LTE)+Intl T/T/Roam)
   *   6 ($10 Employee Plan)
   *   7 ($0 Demo Plan)
   *
   * @return string
   */
  public function getPlanNameFromPlanId( $planId )
  {
    $map = [
      '30' => 'UV30',
      '35' => 'UV35',
      '45' => 'UV45',
      '46' => 'UV55',
      '55' => 'UV55',
      '58' => 'UV30',
      '79' => 'UV35',
      '6'  => 'UV50', // $10 Employee Plan
      '7'  => 'UV50'  //  $0 Demo Plan
    ];

    if ( empty($map[ trim( $planId ) ]) )
    {
      \logError( "Plan ID not recognized: $planId" );

      return NULL;
    }

    return $map[ trim( $planId ) ];
  }

  /**
   * getPlanStateFromStatusId
   *
   * Maps import file status Id to Ultra Plan State
   *   2 - Paid
   *   3 - NotPaid
   *   4 - Daily plan ( Logic TBD )
   *   5 - Suspended
   *
   * @return string
   */
  public function getPlanStateFromStatusId( $statusId )
  {
    return ( (string) $statusId ) == '2' ? STATE_ACTIVE : STATE_SUSPENDED ;
  }

  /**
   * getLanguage
   *
   * Maps import file language string to Ultra allowed values
   * 1 and EN are english
   * 225 and SP are spanish
   *
   * @return string
   */
  public function getLanguage( $language )
  {
    if ( ( $language == 'SP' ) || ( $language == 225 ) )
      return 'ES';
    else
      return 'EN';
  }

  /**
   * validateRow
   *
   * Validates a data row
   *
   * @return array
   */
  public function validateRow( array $rowArray )
  {
    \logDebug( 'row : ' . json_encode( $rowArray ) );

    $headerDefinition = $this->headerDefinition();

    $n = count( $headerDefinition );

    $rowErrors     = array();
    $rowErrorCodes = array();

    // loop through each value in a row
    for ( $i = 0 ; $i < $n ; $i++ )
    {
      $value = ( empty( $rowArray[ $i ] ) ) ? '' : trim($rowArray[ $i ]) ;

      // \logDebug( $i . ' value : ' . $value );
      // \logDebug( 'def   : ' . json_encode( $headerDefinition[ $i ] ) );

      // check if value is valid
      list( $errors , $errorCodes ) = $this->validateField( $value , $headerDefinition[ $i ] );

      // additional validation: the following values should be unique per file:
      // . ICCID_FULL
      // . TEMP_ICCID_FULL
      // . MSISDN
//TODO:

      if ( ! empty( $errors ) )
      {
        \logInfo( "VALIDATION ERRORS FOR VALUE = $value ; ERRORS = ".json_encode($errors) );

        $rowErrors     = array_merge( $rowErrors     , $errors     );
        $rowErrorCodes = array_merge( $rowErrorCodes , $errorCodes );
      }
    }

    return array( $rowErrors , $rowErrorCodes );
  }

  /**
   * validateField
   *
   * Validates a data field
   *
   * @return array
   */
  public function validateField( $value , $definition )
  {
    return \Ultra\Lib\Util\Validator::validate( $definition[0] , 'string' , $definition[1] , $value );
  }

  /**
   * headerDefinition
   *
   * Ordered list of fields
   * The first element is the field name
   * The second element represents the validation rules
   *
   * @return array
   */
  public function headerDefinition()
  {
#Batch,SUBS_ID,SUBS_NUMBER,Time Zone,IN Balance,Data Allotted,Data Used,Data Remaining,STATUS_ID,PLAN_ID,CURRENT_FEATURE,SIM,Temp SIM,IMSI (Perm SIM's),PIN,RENEWAL_DATE,FUTURE_DATED_PLAN,FUTURE_DATED_FEATURE,FUTURE_DATED_PLAN_DATE,FUTURE_DATED_FEATURE_DATE,DEALER_CODE,SUB_FIRST,SUB_LAST,SUB_ADDRESS,SUB_EMAIL,SUB_ZIP,LANGUAGE,IMEI,Activation_Date
    return [
      [ 'batch'                     , NULL ], // serial Batch ID : A,B,C,,,
      [ 'subs_id'                   , NULL ],
      [ 'subs_number'               , NULL ], // MSISDN
      [ 'time_zone'                 , NULL ],
      [ 'balance'                   , NULL ],
      [ 'data_allotted'             , NULL ],
      [ 'data_used'                 , NULL ],
      [ 'data_remaining'            , NULL ],
      [ 'status_id'                 , [ 'regexp' => '^[2345]$' ] ],
      [ 'plan_id'                   , [ 'matches' => array('45','46','58','79','6','7','30','35','40','45','55') ] ],
      [ 'features'                  , [ 'matches' => array('48','49','51') ] ],
      [ 'iccid'                     , [ 'regexp' => '^\d{19}F?$' ] ],
      [ 'temp_iccid'                , [ 'regexp' => '^\d{19}F?$' ] ],
      [ 'imsi'                      , NULL ],
      [ 'pin'                       , [ 'regexp' => '^\d+$' ] ],
      [ 'renewal_date'              , NULL ],
      [ 'future_dated_plan'         , NULL ],
      [ 'future_dated_feature'      , NULL ],
      [ 'future_dated_plan_date'    , NULL ],
      [ 'future_dated_feature_date' , NULL ],
      [ 'dealer_code'               , [ 'regexp' => '^\s*\d*\s*$' ] ],
      [ 'first_name'                , NULL ],
      [ 'last_name'                 , NULL ],
      [ 'address'                   , NULL ],
      [ 'email'                     , NULL ],
      [ 'zip_code'                  , [ 'regexp' => '^\d{5}$' ] ],
      [ 'language'                  , NULL ],
      [ 'imei'                      , NULL ],
      [ 'activation_date'           , NULL ]
    ];
  }
}

