<?php

require_once 'classes/Session.php';

class SessionTest extends PHPUnit_Framework_TestCase
{
  protected function setUp()
  {
    teldata_change_db();
  }


  function test_tokenEncryptionDecryption()
  {
    // test decryption of existing v2 token
    $msisdn = Session::decryptToken('d2f6c87c99ba4f724739e3c249cf');
    $this->assertEquals('3237885988', $msisdn);

    // test invalid MSISDN
    $msisdn = '385988';
    $token = Session::encryptToken($msisdn);
    $this->assertEmpty($token);

    // create and decrypt v2 token
    $msisdn = '3237885988';
    $token = Session::encryptToken($msisdn);
    $this->assertEquals(28, strlen($token));
    $result = Session::decryptToken($token);
    $this->assertEquals($msisdn, $result);

    // test bogus v2 token
    $result = Session::decryptToken(substr("z$token", 0, Session::TOKEN_V2));
    echo "RESULT: $result \n";
    $this->assertEmpty($result);

    // create and decrypt v3 tokens
    foreach (array('0000000000', '1000000000', '1010101010', '9999999999') as $msisdn)
    {
      $token = Session::encryptToken($msisdn, Session::TOKEN_V3, array(Session::SYSTEM_TOKEN, Session::TEST_TOKEN));
      $this->assertEquals(14, strlen($token));
      $result = Session::decryptToken($token);
      $this->assertEquals($result, $msisdn);
    }

    // test bogus v3 token
    $result = Session::decryptToken(substr("Z$token", 0, Session::TOKEN_V3));
    $this->assertEmpty($result);
  }
}