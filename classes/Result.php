<?php

/**
 * General purpose class to be used as a result for functions and methods.
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Result
{
  protected $success  = FALSE;
  protected $aborted  = FALSE;
  protected $pending  = FALSE;
  protected $timeout  = FALSE;
  protected $context  = 'xml';
  protected $errors   = array();
  protected $warnings = array();
  protected $log      = array();

  public  $data_array = array(); # used for miscellaneous data

  /**
   * succeed
   * sets $this->success to TRUE
   */
  public function succeed() { $this->success = TRUE;  }

  /**
   * fail
   * sets $this->success to FALSE
   */
  public function fail() { $this->success = FALSE; }

  /**
   * abort
   * sets $this->aborted to TRUE
   */
  public function abort() { $this->aborted = TRUE;  }

  /**
   * is_failure
   * returns !$this->success
   * 
   * @return boolean !$this->success
   */
  public function is_failure() { return ! $this->success; }

  /**
   * is_success
   * return $this->success
   * 
   * @return boolean $this->success
   */
  public function is_success() { return $this->success;   }

  /**
   * is_aborted
   * returns $this->aborted
   * 
   * @return boolean $this->aborted
   */
  public function is_aborted() { return $this->aborted;   }

  /**
   * is_pending
   * returns $this->pending
   * 
   * @return boolean $this->pending
   */
  public function is_pending() { return $this->pending;   }

  /**
   * is_not_pending
   * returns !$this->pending
   * 
   * @return boolean !$this->pending
   */
  public function is_not_pending() { return ! $this->pending; }

  /**
   * is_timeout
   * returns $this->timeout
   * 
   * @return boolean $this->timeout
   */
  public function is_timeout() { return $this->timeout;   }

  /**
   * is_not_timeout
   * returns !$this->timeout
   * 
   * @return boolean !$this->timeout
   */
  public function is_not_timeout() { return ! $this->timeout; }

  /**
   * timeout
   * set $this->timeout to TRUE
   */
  public function timeout() { $this->timeout = TRUE;   }

  /**
   * has_errors
   * return TRUE on has errors or FALSE
   * 
   * @return boolean has errors?
   */
  public function has_errors()
  {
    return ! ! count($this->errors);
  }

  /**
   * del_errors
   *
   * Remove all errors
   * 
   * @return NULL
   */
  public function del_errors()
  {
    $this->errors = array();
  }

  /**
   * get_first_error
   * Returns first error or null if none
   *
   * @return String first error in error array
   */
  public function get_first_error()
  {
    if (count($this->errors))
      return $this->errors[0];

    return null;
  }

  /**
   * get_errors
   * Returns array of errors for $this
   * 
   * @return array $this->errors
   */
  public function get_errors()
  {
    if (count($this->errors) < 1 && $this->is_timeout())
    {
      $this->errors[] = 'Timeout error';
    }

    if (count($this->errors) < 1 && $this->is_aborted())
    {
      $this->errors[] = 'Aborted result';
    }

    return $this->errors;
  }

  /**
   * get_warnings
   * Returns array of warnings for $this
   * 
   * @return array $this->warnings
   */
  public function get_warnings() { return $this->warnings; }

  /**
   * get_log
   * Returns array log for $this
   * 
   * @return array $this->log
   */
  public function get_log() { return $this->log; }

  /**
   * add_error
   * append $error to array $this->errors
   * set $this->success equal to ($this->success && count($this->errors))
   * 
   * @param string $error
   */
  public function add_error($error)
  {
    if (NULL != $error)
    {
      $this->errors[] = $error;
    }

    $this->success = $this->success && (count($this->errors) == 0);
  }

  /**
   * add_errors
   * append $errors to $this->errors
   * set $this->success equal to ($this->success && count($this->errors))
   * 
   * @param array $errors
   */
  public function add_errors($errors)
  {
    $this->errors = array_merge( $this->errors , $errors );
    $this->success = $this->success && (count($this->errors) == 0);
  }

  /**
   * add_warning
   * append $warning to array $this->warnings
   * 
   * @param string $warning
   */
  public function add_warning($warning) { $this->warnings[] = $warning; }

  /**
   * add_warnings
   * merges array $warnings with $this->warnings
   * 
   * @param array $warnings
   */
  public function add_warnings($warnings) { $this->warnings = array_merge( $this->warnings , $warnings ); }

  /**
   * add_log
   * append $log to array $this->log
   * 
   * @param string $log
   */
  public function add_log($log) { $this->log[] = $log; }

  /**
   * add_logs
   * merges array $logs with array $this->log
   * 
   * @param array $logs
   */
  public function add_logs($logs) { $this->log = array_merge( $this->log , is_array($logs) ? $logs : [ $logs ] ); }

  /**
   * to_string
   * outputs $this, json encoded
   * 
   * @return string json encoded Result object, $this
   */
  public function to_string() { return json_encode($this, 1); }

  /**
   * set_pending
   * sets $this->pending to TRUE
   */
  public function set_pending() { $this->pending = TRUE; }

  /**
   * unset_pending
   * sets $this->pending to FALSE
   */
  public function unset_pending() { $this->pending = FALSE; }

  /**
   * set_context
   * sets $this->context to $context
   * 
   * @param string $context
   */
  public function set_context( $context )
  {
    $this->context = $context;
  }

  /**
   * __toString
   * outputs Result object data in string format
   * outputs xml on $this->context == 'xml'
   * 
   * @return string result object data
   */
  public function __toString()
  {
    if ( $this->context == 'xml' )
    {
      return to_xml( $this->to_array() , 'data' );
    }
    else
    {
      return $this->to_string();
    }
  }
  
  /**
   * get_data_array
   * return a valid array no matter what
   * 
   * @return array $result_array
   */
  public function get_data_array()
  {
    return is_array($this->data_array) ? $this->data_array : array();
  }

  /**
   * get_data_key
   * return data value for given $key
   * 
   * @param  string $key
   * @return mixed  $this->data[$key]
   */
  public function get_data_key($key)
  {
    $data = $this->get_data_array();

    if ( isset($data[$key]) )
    { return $data[$key]; }
    else
    { return NULL; }
  }

  /**
   * to_array
   * output result array from $this members
   * 
   * @return array $result_array
   */
  public function to_array()
  {
    return array('errors'   => $this->get_errors(),
                 'warnings' => $this->get_warnings(),
                 'log'      => $this->get_log(),
                 'success'  => $this->is_success(),
                 'pending'  => $this->is_pending(),
                 'timeout'  => $this->is_timeout(),
                 'data'     => $this->data_array
    );
  }

  /**
   * add_result_object
   * given a Result object, merges its content with $this
   * 
   * @param  object $result_object
   * @return object Result $this
   */
  public function add_result_object($result_object)
  {
    /*  */

    $this->add_errors(   $result_object->get_errors()   );
    $this->add_warnings( $result_object->get_warnings() );
    $this->add_logs(     $result_object->get_log()      );

    $this->success = ! ! $result_object->is_success();
    $this->pending = ! ! $result_object->is_pending();
    $this->timeout = ! ! $result_object->is_timeout();

    $this->data_array = array_merge($this->data_array, $result_object->data_array);

    return $this;
  }

  /**
   * is_empty
   *
   * Returns true if the object contains no data , false otherwise
   * 
   * @return boolean
   */
  public function is_empty()
  {
    return ! ! empty( $this->data_array );
  }

  /**
   * append_to_data_array
   */
  public function append_to_data_array($array_name,$array_element)
  {
    return $this->append_to_array_data($array_name,$array_element);
  }

  /**
   * append_to_array_data
   * appends key($array_name), val($array_element) pair to $this->data_array
   * 
   * @param string $array_name    key
   * @param mixed  $array_element val
   * @return NULL
   */
  public function append_to_array_data($array_name,$array_element)
  {
    if ( isset( $this->data_array[ $array_name ] ) )
    {
      $this->data_array[ $array_name ][] = $array_element;
    }
    else
    {
      $this->data_array[ $array_name ] = array( $array_element );
    }

    return NULL;
  }

  /**
   * add_data_array
   * merges input $data_array with $this->data_array
   * 
   * @param array $data_array
   */
  public function add_data_array($data_array)
  {
    if ( ! empty( $data_array ) )
      $this->data_array = array_merge( $this->data_array , $data_array );
  }

  /**
   * add_to_data_array
   * @param  $key
   * @param  $val
   * @return void
   */
  public function add_to_data_array($key, $val)
  {
    $this->data_array[$key] = $val;
  }

  /**
   * add_result_array
   * given a Result array, merges its content with $this
   * 
   * @param array $result_array
   * @return object Result $this
   */
  public function add_result_array($result_array)
  {
    if ( isset($result_array) && $result_array && is_array( $result_array ) )
    {
      if ( isset($result_array['errors']) )
        $this->add_errors( $result_array['errors'] );

      if ( isset($result_array['warnings']) )
        $this->add_warnings( $result_array['warnings'] );

      if ( isset($result_array['log']) )
        $this->add_logs( $result_array['log'] );

      if ( isset($result_array['success']) )
        $this->success = $result_array['success'];

      if ( isset($result_array['pending']) )
        $this->pending = $result_array['pending'];

      if ( isset($result_array['timeout']) )
        $this->timeout = $result_array['timeout'];

      if ( isset($result_array['data']) )
        $this->data_array = array_merge( $this->data_array,
                                         is_array($result_array['data']) ? $result_array['data'] : array($result_array['data']) );

      if ( isset($result_array['aborted']) )
        $this->aborted = $result_array['aborted'];
    }

    return $this;
  }

  public function __construct($new_arg=NULL, $success=FALSE)
  {
    if ( $new_arg )
    {
      if ( is_array($new_arg) )
      { // assume is a return array
        $this->add_result_array($new_arg);
      }
      else
      { // assume is a serialized JSON string
        $this->add_result_array(json_decode($new_arg, 1));
      }
    }

    $this->success = $success;
  }

}

/**
 * make_error_Result
 * @param  string $error
 * @param  mixed $data
 * @return object Result
 */
function make_error_Result($error, $data=NULL, $log_message=NULL)
{
  $r = new Result();

  if ( !is_array($error) )
    $error = array($error);

  $r->add_result_array(array('errors'  => $error,
                             'success' => FALSE,
                             'pending' => FALSE,
                             'timeout' => FALSE,
                             'data'    => $data,
                             'log'     => $log_message,
                             'aborted' => FALSE));

  return $r;
}

/**
 * make_timeout_Result
 * @param  string $error
 * @param  mixed $data
 * @return object Result
 */
function make_timeout_Result($error, $data=NULL, $log_message=NULL)
{
  $r = new Result();

  $r->add_result_array(array('errors'  => array($error),
                             'success' => FALSE,
                             'pending' => FALSE,
                             'timeout' => TRUE,
                             'data'    => $data,
                             'log'     => $log_message,
                             'aborted' => FALSE));

  return $r;
}

/**
 * make_ok_Result
 * @param  array  $data
 * @param  string $log_message
 * @param  string $warning
 * @return object Result
 */
function make_ok_Result($data=array(), $log_message=NULL, $warning=NULL)
{
  $r = new Result();

  $r->add_result_array(array('errors'  => array(),
                             'success' => TRUE,
                             'pending' => FALSE,
                             'timeout' => FALSE,
                             'data'    => $data,
                             'log'     => $log_message,
                             'aborted' => FALSE));

  if ( $warning )
    $r->add_warning( $warning );

  return $r;
}

