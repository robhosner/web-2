<?php

require_once 'classes/ProjectW/DB.php';
require_once 'classes/ProjectW/Redis.php';
include_once('db/ultra_customer_options.php');
require_once 'db/univision/univision_import.php';
require_once 'db/htt_customers_overlay_ultra.php';
require_once 'lib/state_machine/aspider.php';
require_once 'Ultra/Lib/DemoLine.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';
require_once 'Ultra/Lib/MiddleWare/ACC/ControlCommandPortIn.php';
require_once 'Ultra/Lib/Util/ProcessControl.php';

define('UNIVISION_IMPORT_STATUS_TODO',             'TODO'            ); // the data is imported from the data file and no other operation has been performed
define('UNIVISION_IMPORT_STATUS_INVALID',          'INVALID'         ); // validation failed, this row cannot be imported
define('UNIVISION_IMPORT_STATUS_PORT_CALLED',      'PORT_CALLED'     ); // ActivatedSubscriber has been invoked and we are waiting for the inbound notification
define('UNIVISION_IMPORT_STATUS_ACTIVATED_TEMP',   'ACTIVATED_TEMP'  ); // ActivatedSubscriber has been invoked and we received a successful inbound notification
define('UNIVISION_IMPORT_STATUS_PORT_ERROR',       'PORT_ERROR'      ); // ActivatedSubscriber has been invoked and it failed (synch)
define('UNIVISION_IMPORT_STATUS_PORT_FAILED',      'PORT_FAILED'     ); // ActivatedSubscriber has been invoked but received a failure with inbound notification
define('UNIVISION_IMPORT_STATUS_CHANGESIM_CALLED', 'CHANGESIM_CALLED'); // ChangeSIM has been invoked and we are waiting for the inbound notification
define('UNIVISION_IMPORT_STATUS_ACTIVATED',        'ACTIVATED'       ); // ActivatedSubscriber and ChangeSIM have been successful
define('UNIVISION_IMPORT_STATUS_CHANGESIM_ERROR',  'CHANGESIM_ERROR' ); // ChangeSIM has been invoked and it failed (synch)
define('UNIVISION_IMPORT_STATUS_CHANGESIM_FAILED', 'CHANGESIM_FAILED'); // ChangeSIM have been successful but we received a failure with inbound notification

define('UNIVISION_FILE_STATUS_INITIALIZED',        'INITIALIZED'     ); // just uploaded and copied to UNIVISION_IMPORT DB table
define('UNIVISION_FILE_STATUS_VALIDATED',          'VALIDATED'       ); // after a validation process
define('UNIVISION_FILE_STATUS_ONGOING',            'ONGOING'         ); // runners are allowed to process customers for the given file
define('UNIVISION_FILE_STATUS_PAUSED',             'PAUSED'          ); // runners are allowed to process customers for the given file
define('UNIVISION_FILE_STATUS_COMPLETED',          'COMPLETED'       ); // no additional processing needed for this file
define('UNIVISION_FILE_STATUS_ABORTED',            'ABORTED'         );

define('UNIVISION_DEFAULT_DEALER',      '34241');
define('UNIVISION_DEFAULT_MASTERAGENT', '690'  );

/**

 - Suspended customers will be Suspended by a separate job to be executed after migration window is over
 - Data SOCs updates will be performed by a separate job to be executed after migration window is over

a) check settings
b) check resources
c) reserve msisdn in Redis

d) static validation
 - validate zip code
 - validate iccid on DB : HTT_INVENTORY_SIM + insert
 - validate msisdn on DB ( overlay table ) : verify that the phone number is not already in our system

e) skip network validation!
f) create new customer on DB

g) \Ultra\Lib\MiddleWare\ACC\ControlCommandPortIn\processCommand($parameters)

h) fake state transition [ Neutral ] => [ Port-In Requested ] (?)

i) reserve ICCID in Redis and/or DB

j) insert row into ULTRA.HTT_ACTIVATION_HISTORY

k) insert row into HTT_ACTIVATION_LOG

l) insert row into HTT_PLAN_TRACKER

m) store wholesale plan in ULTRA.WHOLESALE_PLAN_TRACKER

n) fake state transition [ Port-In Requested ] => [ Active ] (?)

o) mark subplan A

Optionally:
- Enroll MEP
- Enroll house account
- Enroll demo line

================

See
http://wiki.hometowntelecom.com:8090/display/SPEC/Project+W+Migration
http://wiki.hometowntelecom.com:8090/display/ProjectW/Project+W+Plan+Configurations+-+New+Pricing
http://wiki.hometowntelecom.com:8090/pages/viewpage.action?title=Project+W+Balance+Reconciliation&spaceKey=ProjectW

*/

class ProjectW extends Result
{
  const MAX_ALLOWED_EXECUTION_TIME = 300; // 5 minutes default
  const MAX_ALLOWED_TASKS          = 25;  // not more than 25 attempts per thread
  const MAX_ALLOWED_WW_QUEUE_SIZE  = 60;  // max allowed MW queue size
  const SLEEP_BETWEEN_TASKS        = 1;   // sleep 1 second between tasks

  protected $maxAllowedExecutionTime;
  protected $maxAllowedTasks;

  private $redis;
  private $customerData; // array of customer data to import
  private $processControlObject;
  private $controlChannel;
  private $timeStart;
  private $customersToSuspend;
  private $customersToAdjustData;
  private $customersToSendSMS;

  public function __construct( $new_arg = NULL , $success = FALSE , $redis = NULL , $maxAllowedExecutionTime = NULL , $maxAllowedTasks = NULL , $controlChannel = NULL )
  {
    parent::__construct( $new_arg , $success );

    $this->redis                     = $redis                     ? $redis                     : new \Ultra\Lib\Util\Redis        ;
    $this->maxAllowedExecutionTime   = $maxAllowedExecutionTime   ? $maxAllowedExecutionTime   : self::MAX_ALLOWED_EXECUTION_TIME ;
    $this->maxAllowedTasks           = $maxAllowedTasks           ? $maxAllowedTasks           : self::MAX_ALLOWED_TASKS          ;
    $this->controlChannel            = $controlChannel            ? $controlChannel            : new \Ultra\Lib\MQ\ControlChannel ;
    $this->timeStart                 = time(); // thread start time

    \logInfo( "timeStart : {$this->timeStart} ; maxAllowedExecutionTime = {$this->maxAllowedExecutionTime} ; maxAllowedTasks = {$this->maxAllowedTasks}" );
  }

  /**
   * run
   *
   * Start the thread for importing Univision customers
   *
   * @return NULL
   */
  public function run()
  {
    // Verify if settings allow the thread to continue
    if ( ! $this->checkSettings() )
      return NULL;

    // Verify if server resources allow the thread to continue
    if ( ! $this->checkServerResources() )
      return NULL;

    // Verify that MW queues are not overloaded
    if ( ! $this->checkMWResources() )
      return NULL;

    return $this->runThread();
  }

  /**
   * runSMSAfterMigration
   *
   * Send SMS to subscribers after migration window
   *
   * @return NULL
   */
  public function runSMSAfterMigration( $inputParams )
  {
    if ( empty( $inputParams['day'] ) )
      die('day is required');

    if ( empty( $inputParams['max_count'] ) )
      $inputParams['max_count'] = 20;

    dlog('',"inputParams = %s",$inputParams);

    return $this->runSMSAfterMigrationaDay( $inputParams['day'] , $inputParams['max_count'] , ! ! $inputParams['dry_run'] );
  }

  /**
   * runAdjustData
   *
   * Process to adjust data for Univision customers after migration window
   *
   * @return NULL
   */
  public function runAdjustData( $inputParams )
  {
    if ( empty( $inputParams['day'] ) )
      die('day is required');

    if ( empty( $inputParams['max_count'] ) )
      $inputParams['max_count'] = 20;

    dlog('',"inputParams = %s",$inputParams);

    return $this->runAdjustDataDay( $inputParams['day'] , $inputParams['max_count'] , ! ! $inputParams['dry_run'] );
  }

  /**
   * runSuspend
   *
   * Process to suspend Univision customers after migration window
   *
   * @return NULL
   */
  public function runSuspend( $inputParams )
  {
    if ( empty( $inputParams['day'] ) )
      die('day is required');

    if ( empty( $inputParams['max_count'] ) )
      $inputParams['max_count'] = 20;

    dlog('',"inputParams = %s",$inputParams);

    return $this->runSuspendDay( $inputParams['day'] , $inputParams['max_count'] , ! ! $inputParams['dry_run'] );
  }

  /**
   * runSMSAfterMigrationaDay
   *
   * Send SMS to subscribers migrated on the give day
   *
   * @return NULL
   */
  protected function runSMSAfterMigrationaDay( $day , $max_count , $dry_run=FALSE )
  {
    $this->loadCustomersToSendSMS($day, $max_count);

    \logInfo('found ' . count($this->customersToSendSMS) . ' customers');

    foreach ($this->customersToSendSMS as $customer)
    {
      $success = \funcSendExemptCustomerSMSProjectW( $customer->CUSTOMER_ID );

      if ( $success )
      {
        if ( ! \update_univision_import(array('customer_id' => $customer->CUSTOMER_ID, 'sms_sent' => TRUE)))
          \logError( "failed to update SMS_SENT for customer ID {$customer->CUSTOMER_ID}" );
      }

      \logInfo("finished processing customer ID {$customer->CUSTOMER_ID}");
    }

    return NULL;
  }

  /**
   * runAdjustDataDay
   *
   * Process to adjust data usage on the give day
   *
   * @return NULL
   */
  protected function runAdjustDataDay( $day , $max_count , $dry_run=FALSE )
  {
    $this->loadCustomersToAdjustData($day, $max_count);

    \logInfo('found ' . count($this->customersToAdjustData) . ' customers');

    // $customer properties: CUSTOMER_ID, MSISDN, ICCID_FULL, PLAN_NAME, DATA_ALLOTTED, DATA_USED, DATA_REMAINING
    foreach ($this->customersToAdjustData as $customer)
    {
      try
      {
        \logInfo("processing customer ID {$customer->CUSTOMER_ID}: " . json_encode($customer));

        if ($dry_run)
          throw new \Exception('dry run: skipped');

        // check if customer ported out by checking HTT_CANCELLATION_REASONS
        $result = get_cancellation_by_msisdn_or_iccid([
          'msisdn' => $customer->MSISDN
        ]);

        if ( ! empty( $result )
          && is_array( $result )
          && ! empty( $result[0] )
          && is_object( $result[0] )
          && ( $result[0]->TYPE == 'PORTOUT' )
        )
        {
          \logWarn( "Customer id {$customer->CUSTOMER_ID} ported out : ".json_encode($result[0]) );

          if ( ! update_univision_import([
              'customer_id'   => $customer->CUSTOMER_ID,
              'data_adjusted' => TRUE,
              'last_error'    => $result[0]->TYPE
            ])
          )
            throw new \Exception("failed to update DATA_ADJUSTED for customer ID {$customer->CUSTOMER_ID}");

          throw new \Exception( "Customer id {$customer->CUSTOMER_ID} ported out : ".json_encode($result[0]) );
        }

/*
        if ( ! $customer->DATA_ALLOTTED)
        {
          if ( ! update_univision_import(array('customer_id' => $customer->CUSTOMER_ID, 'data_adjusted' => TRUE)))
            throw new \Exception("failed to update DATA_ADJUSTED for customer ID {$customer->CUSTOMER_ID}");
          throw new \Exception("marked customer ID {$customer->CUSTOMER_ID} as ADJUSTED due to zero data allowance");
        }
*/
        if ( ! $planSocs = Ultra\MvneConfig\getAccSocsUltraPlanConfig($customer->PLAN_NAME, NULL, 'A'))
          throw new \Exception("failed to get SOC configuration for plan name {$customer->PLAN_NAME} subplan A");

        // VYT: AFAIK import data counters are given in Kb (since the numbers are quite large), therefore convert to Mb
        if ( ! $newSocs = $this->computeAdjustedPlanSocs($planSocs, round($customer->DATA_USED / 1024)))
          throw new \Exception('failed to compute adjusted plan configuration');

        $error = $this->updateAdjustedDataSocs($customer, $newSocs);
        teldata_change_db();
        if ($error)
          throw new \Exception("failed to update adjusted data SOCs: $error");

        if ( ! update_univision_import(array('customer_id' => $customer->CUSTOMER_ID, 'data_adjusted' => TRUE)))
          throw new \Exception("failed to update DATA_ADJUSTED for customer ID {$customer->CUSTOMER_ID}");

        logInfo("successfully updated customer ID {$customer->CUSTOMER_ID}");
      }
      catch(\Exception $exp)
      {
        \logError('EXCEPTION: ' . $exp->getMessage());
      }

      \logInfo("finished processing customer ID {$customer->CUSTOMER_ID}");
    }

    return NULL;
  }

  /**
   * computeAdjustedPlanSocs
   *
   * deduct used data from plan data SOCs according to their priority and return updated plan config
   * VYT: AGAIK Univision does not offer data bolt ons (only international), therefore we do not need to deal with them
   * @param Array SOC configuration
   * @param Integer amount of data used by subscriber to deduct from plan SOCs, Mb
   * @return Array re-calculated plan SOCs or NULL on error
   */
  private function computeAdjustedPlanSocs($planSocs, $amount)
  {
    logInfo('plan SOCs: ' . json_encode($planSocs) . ", amount to deduct: $amount");
    $resultSocs = array();

    // skip subs without data (los pobres UV30)
    if ($amount)
    {
      // extract data SOCs from plan config and compute their priority according to ACC designation
      $dataSocs = array();
      foreach ($planSocs as $name => $value)
        if ($dataSoc = \Ultra\MvneConfig\getAccSocDefinition($name))
          if ($dataSoc[3] == 'mbytes')
            $dataSocs[filter_var($dataSoc[1], FILTER_SANITIZE_NUMBER_INT)] = $name;
      ksort($dataSocs);

      // sanity check
      if (empty($dataSocs))
        return logError('no data SOCs found in ' . json_encode($planSocs));

      // reduce each SOCs value till either 0 or base SOC is reached (since there can be only one base SOC)
      $baseSocs = \Ultra\MvneConfig\getAccDataSocsByType('base');
      foreach ($dataSocs as $soc)
        if ($amount > 0 && ! in_array($soc, $baseSocs))
        {
          $resultSocs[$soc] = $planSocs[$soc] > $amount ? $planSocs[$soc] - $amount : 0;
          $amount = $planSocs[$soc] > $amount ? 0 : $amount - $planSocs[$soc];
        }
    }

    // PJW-155: also reset DA1 and DA7 SOCs
    foreach (array('b_voice', 'a_voicesmswallet_ir') as $soc)
      $resultSocs[$soc] = $planSocs[$soc];

    logInfo('adjusted plan SOCs: ' . json_encode($resultSocs));
    return $resultSocs;
  }

  /**
   * updateAdjustedDataSocs
   *
   * execute UpdatePlanAndFeatures with the adjusted SOCs for option RESET
   * @param Object customer
   * @param Array adjusted plan configuration
   * @return String NULL on success or error message on failure
   */
  private function updateAdjustedDataSocs($customer, $config)
  {
    logInfo('parameters: ' . json_encode(func_get_args()));

    $acc = new \Ultra\Lib\MiddleWare\ACC\Control;
    $result = $acc->processControlCommand(array(
      'command'    => 'UpdatePlanAndFeatures',
      'actionUUID' => getNewActionUUID(),
      'parameters' => array(
        'customer_id'        => $customer->CUSTOMER_ID,
        'msisdn'             => $customer->MSISDN,
        'iccid'              => $customer->ICCID_FULL,
        'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
        'ultra_plan'         => $customer->PLAN_NAME,
        'preferred_language' => $customer->LANGUAGE,
        'option'             => 'RESET',
        'reset_config'       => $config,
        'keepDataSocs'       => TRUE)
      )
    );
    
    // check response
    $error = $result->is_failure() ? 'error: ' . json_encode($result->get_errors()) : NULL;
    if ( ! empty($result->data_array['message']))
    {
      $message = json_decode($result->data_array['message']);
      if ( ! empty($message->body->errors))
        $error = "error: {$message->body->errors[0]}";
    }

    return $error;
  }

  /**
   * runSuspendDay
   *
   * Process to suspend Univision customers after migration window on the give day
   *
   * @return NULL
   */
  protected function runSuspendDay( $day , $max_count , $dry_run )
  {
    if ( ! $this->loadCustomersToSuspend( $day ) )
      return \logInfo( 'No customers to suspend' );

    $i = 0;

    while( $i < $max_count && ! empty( $this->customersToSuspend[ $i ] ) )
    {
      $customerToSuspend = $this->customersToSuspend[ $i ];

      if ( empty( $customerToSuspend->CUSTOMER_ID ) )
        \logError( "No customer id for UNIVISION_IMPORT_ID ".$customerToSuspend->UNIVISION_IMPORT_ID );
      elseif( $dry_run )
        \logInfo( "I would have suspended customer id {$customerToSuspend->CUSTOMER_ID}, but this is only a dry run ..." );
      else
      {
        $customer = \get_ultra_customer_from_customer_id( $customerToSuspend->CUSTOMER_ID , array('plan_state') );

        \logInfo( "CUSTOMER_ID = {$customerToSuspend->CUSTOMER_ID} ; PLAN_STATE = {$customer->plan_state}" );

        if ( $customer->plan_state == STATE_ACTIVE )
          $this->customerSuspend( $customerToSuspend );
      }

      $i++;
    }

    return NULL;
  }

  /**
   * customerSuspend
   *
   * Suspend a Customer
   *
   * @return NULL
   */
  protected function customerSuspend( $customerToSuspend )
  {
    // suspend on network
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwSuspendSubscriber([
      'actionUUID'  => \getNewUUID('PRJW','PRJW'),
      'msisdn'      => $customerToSuspend->MSISDN,
      'iccid'       => $customerToSuspend->ICCID_FULL,
      'customer_id' => $customerToSuspend->CUSTOMER_ID
    ]);

    // did mwSuspendSubscriber fail?
    if ( $result->is_failure() )
      return \logError( json_encode( $result->get_errors() ) );

    // did SuspendSubscriber fail?
    if ( ! $result->data_array['success'])
      return \logError( json_encode( $result->data_array['errors'] ) );

    // update to Suspended
    $success = $this->updateOverlayAfterSuspend( $customerToSuspend->CUSTOMER_ID );

    if ( ! $success )
      \logError( 'Could not update HTT_CUSTOMERS_OVERLAY_ULTRA' );

    // add fake state transition [ Active ] => [ Suspended ]
    $success = \ProjectW\addFakeSuspendTransition( $customerToSuspend->CUSTOMER_ID , \get_cos_id_from_plan( $customerToSuspend->PLAN_NAME ) );

    if ( ! $success )
      \logError( 'Could not create state transition [ Active ] => [ Suspended ]' );
  }

  /**
   * loadCustomersToSendSMS
   *
   * Load customers from DB for sending SMS
   *
   * @return boolean
   */
  protected function loadCustomersToSendSMS( $day , $max_count)
  {
    $this->customersToSendSMS = \get_candidate_rows_to_send_sms( $day , $max_count);

    return ! empty($this->customersToSendSMS);
  }

  /**
   * loadCustomersToAdjustData
   *
   * Load customers from DB for adjusting data
   *
   * @return boolean
   */
  protected function loadCustomersToAdjustData( $day , $max_count)
  {
    $this->customersToAdjustData = \get_candidate_rows_to_adjust_data($day, $max_count);

    return ! empty($this->customersToAdjustData);
  }

  /**
   * loadCustomersToSuspend
   *
   * Load customers from DB before suspending them
   *
   * @return boolean
   */
  protected function loadCustomersToSuspend( $day )
  {
    $this->customersToSuspend = \get_candidate_rows_to_suspend( $day );

    return ! empty( $this->customersToSuspend );
  }

  /**
   * runThread
   *
   * Thread work for importing Univision customers
   *
   * @return NULL
   */
  private function runThread()
  {
    $countTasks = 0;

    while (
      ( ( time() - $this->timeStart ) < $this->maxAllowedExecutionTime ) // time limit
      &&
      ( $countTasks++ < $this->maxAllowedTasks ) // max tasks per thread
    )
    {
      // process next task
      $this->processNextTask();

      \logInfo( "countTasks = $countTasks" );

      // pause before continuing with next task
      $this->pauseBetweenTasks();

      // exit right away if there are no resources or if the process has been paused
      if ( ! $this->checkSettings() || ! $this->checkServerResources() || ! $this->checkMWResources() )
        $countTasks = $this->maxAllowedTasks;
    }

    \logInfo( "timeStart : {$this->timeStart} ; timeEnd : ".time(). " ; elapsed time : ".( time() - $this->timeStart ) );

    return NULL;
  }

  /**
   * pauseBetweenTasks
   *
   * Sleep a bit before continuing with next task
   *
   * @return NULL
   */
  protected function pauseBetweenTasks()
  {
    sleep( self::SLEEP_BETWEEN_TASKS );
  }

  /**
   * loadTaskByID
   *
   * Given a UNIVISION_IMPORT_ID, load the correspondent ULTRA.UNIVISION_IMPORT data from DB
   * Returns TRUE if success
   *
   * @return boolean
   */
  public function loadTaskByID( $univision_import_id )
  {
    $sql = sprintf("
    SELECT u.ULTRA_CODE, i.*,".mep_eligible_attribute()."
    FROM   ULTRA.UNIVISION_IMPORT i
    LEFT OUTER JOIN ULTRA.UNIVISION_CODE_TO_DEALER_CODE u
    ON     i.UNIVISION_CODE = u.UNIVISION_CODE
    WHERE  UNIVISION_IMPORT_ID = %d",
    $univision_import_id);

    return $this->setCustomerData( find_first($sql) );
  }

  /**
   * processNextTask
   *
   * Work for processing the next task
   *
   * @return NULL
   */
  private function processNextTask()
  {
    // get data for next task
    if ( $this->getNextTaskData() )
    {
      // decide if we have to invoke ActivateSubscriber or ChangeSIM

      if ( $this->customerData->STATUS == UNIVISION_IMPORT_STATUS_ACTIVATED_TEMP )
        $this->processNextTaskChangeSIM();
      else
        $this->processNextTaskActivateSubscriber();

      #\ProjectW\release_msisdn( $this->customerData->MSISDN );
    }

    return NULL;
  }

  /**
   * processNextTaskChangeSIM
   *
   * ChangeSIM phase
   *
   * @return NULL
   */
  private function processNextTaskChangeSIM()
  {
    if ( ! $this->changeSIM() )
      return $this->recordError();

    // connect back to TSIP DB
    teldata_change_db();

    // update STATUS to CHANGESIM_CALLED
    if ( ! \update_univision_import_changesim_called( $this->customerData->UNIVISION_IMPORT_ID ) )
      \logError( 'Could not update STATUS' );

    return NULL;
  }

  /**
   * processNextTaskActivateSubscriber
   *
   * 1. Create customer in DB
   * 2. ActivateSubscriber ACC call
   *
   * @return NULL
   */
  private function processNextTaskActivateSubscriber()
  {
    // Create customer in DB
    if ( ! $this->createCustomer( (array) $this->customerData ) )
      return $this->recordError();

    // update ULTRA.UNIVISION_IMPORT.CUSTOMER_ID
    $success = \record_customer_id( $this->customerData->CUSTOMER_ID , $this->customerData->UNIVISION_IMPORT_ID );

    if ( ! $success ) \logError( 'Could not update CUSTOMER_ID' );

    // enroll subplan A
    $success = $this->enrollSubPlanA();

    if ( ! $success ) \logError( 'Could not enroll to subplan A' );

    // ActivateSubscriber ACC call
    if ( ! $this->portInCustomer() )
      return $this->recordError();

    // connect back to TSIP DB
    teldata_change_db();

    // update STATUS to PORT_CALLED
    if ( ! \update_univision_import_port_called( $this->customerData->UNIVISION_IMPORT_ID ) )
      \logError( 'Could not update STATUS' );

    // record UNIVISION_IMPORT_ID + MSISDN for the middleware
    \ProjectW\record_univision_msisdn( $this->customerData->UNIVISION_IMPORT_ID , $this->customerData->MSISDN );

    // add fake state transition [ Neutral ] => [ Port-In Requested ]
    $success = \ProjectW\addFakePortInTransition( $this->customerData , \get_cos_id_from_plan( $this->customerData->PLAN_NAME ) );

    if ( ! $success )
      \logError( 'Could not create state transition [ Neutral ] => [ Port-In Requested ]' );

    // add to HTT_BILLING_HISTORY after migration event ( 5.1 )
    $this->addBillingHistoryActivationRow();

    // add to HTT_BILLING_HISTORY due to positive balance ( 5.2.1 )
    $this->addBillingHistoryBalanceRow();

    return NULL;
  }

  /**
   * addBillingHistoryActivationRow
   *
   * Add to HTT_BILLING_HISTORY after migration event ( 5.1 )
   *
   * @return NULL
   */
  private function addBillingHistoryActivationRow()
  {
    $success = \ProjectW\addToBillingHistory([
      'customer_id'            => $this->customerData->CUSTOMER_ID,
      'balance_change'         => 0,
      'package_balance_change' => 0,
      'charge_amount'          => 0,
      'cos_id'                 => \get_cos_id_from_plan( $this->customerData->PLAN_NAME ),
      'detail'                 => 'addBillingHistoryActivationRow',
      'description'            => 'UVIMPORT.MIGRATION',
      'reference'              => create_guid( __FUNCTION__ ),
      'source'                 => 'UVIMPORT',
      'entry_type'             => 'UVIMPORT',
      'commissionable_charge_amount' => 0
    ]);

    if ( ! $success )
      \logError( 'Could not insert into HT_BILLING_HISTORY ( 5.1 )' );
  }

  /**
   * addBillingHistoryBalanceRow
   *
   * Add to HTT_BILLING_HISTORY due to positive balance ( 5.2.1 )
   *
   * @return NULL
   */
  private function addBillingHistoryBalanceRow()
  {
    // insert new row only if balance is positive
    if ( empty( $this->customerData->BALANCE ) )
      return NULL;

    $success = \ProjectW\addToBillingHistory([
      'customer_id'            => $this->customerData->CUSTOMER_ID,
      'balance_change'         => ( $this->customerData->BALANCE / 100 ),
      'package_balance_change' => 0,
      'charge_amount'          => ( $this->customerData->BALANCE / 100 ),
      'cos_id'                 => \get_cos_id_from_plan( $this->customerData->PLAN_NAME ),
      'detail'                 => 'addBillingHistoryBalanceRow',
      'description'            => 'UVIMPORT.WALLET',
      'reference'              => create_guid( __FUNCTION__ ),
      'source'                 => 'CSCOURTESY',
      'entry_type'             => 'LOAD',
      'commissionable_charge_amount' => ( $this->customerData->BALANCE / 100 )
    ]);

    if ( ! $success )
      \logError( 'Could not insert into HT_BILLING_HISTORY ( 5.2.1 )' );
  }

  /**
   * recordPortFailed
   *
   * ActivatedSubscriber has been invoked but received a failure with inbound notification
   * Set STATUS to PORT_FAILED
   *
   * @return NULL
   */
  public function recordPortFailed( $error )
  {
    if ( $this->customerData->STATUS != UNIVISION_IMPORT_STATUS_PORT_CALLED )
      return \logError( 'STATUS not recognized for recordPortFailed : ' . $this->customerData->STATUS );

    \update_univision_import_port_failed( $this->customerData->UNIVISION_IMPORT_ID , $error );

    return NULL;
  }

  /**
   * recordActivatedTemp
   *
   * ActivatedSubscriber has been invoked and we received a successful inbound notification
   * Set STATUS to ACTIVATED_TEMP
   *
   * @return NULL
   */
  public function recordActivatedTemp()
  {
    if ( $this->customerData->STATUS != UNIVISION_IMPORT_STATUS_PORT_CALLED )
      return \logError( 'STATUS not recognized for recordActivatedTemp : ' . $this->customerData->STATUS );

    \update_univision_import_activated_temp( $this->customerData->UNIVISION_IMPORT_ID );

    return NULL;
  }

  /**
   * recordChangeSIMFailed
   *
   * ChangeSIM have been successful but we received a failure with inbound notification
   * Set STATUS to CHANGESIM_FAILED
   *
   * @return NULL
   */
  public function recordChangeSIMFailed( $error )
  {
    if ( $this->customerData->STATUS != UNIVISION_IMPORT_STATUS_CHANGESIM_CALLED )
      return \logError( 'STATUS not recognized for recordChangeSIMFailed : ' . $this->customerData->STATUS );

    \update_univision_import_changesim_failed( $this->customerData->UNIVISION_IMPORT_ID , $error );

    return NULL;
  }

  protected function addBalanceOnActivation()
  {
    if ( ! empty( $this->customerData->BALANCE ) )
    {
      $sql = accounts_update_query([
        'add_to_balance' => ( $this->customerData->BALANCE / 100 ), // in $
        'customer_id'    => $this->customerData->CUSTOMER_ID 
      ]);

      if ( ! is_mssql_successful( logged_mssql_query( $sql ) ) )
        \logError( 'Error updating ACCOUNTS.BALANCE' );
    }
  }

  // similar to func_set_minutes
  protected function addMinutesOnActivation( $cos_id )
  {
    // get intl_minutes and unlimited_intl_minutes, given the $cos_id
    $intl_minutes           = \Ultra\UltraConfig\getUltraPlanConfigurationItem( $this->customerData->PLAN_NAME , 'intl_minutes' );
    $unlimited_intl_minutes = \Ultra\UltraConfig\getUltraPlanConfigurationItem( $this->customerData->PLAN_NAME , 'unlimited_intl_minutes' );

    if ( $subplan = \get_brand_uv_subplan( $this->customerData->CUSTOMER_ID ) )
    {
      if ( $minutes = \Ultra\UltraConfig\getMinutesBySubplan(get_plan_from_cos_id( $cos_id ), $subplan) )
      {
        $intl_minutes           = $minutes[0];
        $unlimited_intl_minutes = $minutes[1];
      }
    }

    if ( empty( $intl_minutes ) )
      $intl_minutes = 0;

    if ( empty( $unlimited_intl_minutes ) )
      $unlimited_intl_minutes = 0;

    $sql = sprintf("
          UPDATE accounts
          SET    packaged_balance1 = %d,
                 packaged_balance2 = %d
          WHERE  customer_id       = %d",
      ($intl_minutes*600),
      ($unlimited_intl_minutes*600),
      $this->customerData->CUSTOMER_ID
    );

    if ( ! run_sql_and_check($sql) )
      \logError('addMinutesOnActivation failed');
  }

  /**
   * recordActivated
   *
   * ActivatedSubscriber and ChangeSIM have been successful
   *  - set STATUS to ACTIVATED
   *  - add fake state transition [ Port-In Requested ] => [ Active ]
   *  - mark subplan A
   *  - update HTT_CUSTOMERS_OVERLAY_ULTRA
   *
   * @return NULL
   */
  public function recordActivated()
  {
    $cos_id = \get_cos_id_from_plan( $this->customerData->PLAN_NAME );

    if ( $this->customerData->STATUS != UNIVISION_IMPORT_STATUS_CHANGESIM_CALLED )
      return \logError( 'STATUS not recognized for recordActivated : ' . $this->customerData->STATUS );

    // update ULTRA.UNIVISION_IMPORT
    $success = \update_univision_import_activated( $this->customerData->UNIVISION_IMPORT_ID );

    if ( ! $success )
      \logError( 'Could not update ULTRA.UNIVISION_IMPORT' );

    // update HTT_CUSTOMERS_OVERLAY_ULTRA ( PLAN_STARTED , PLAN_EXPIRES , GROSS_ADD_DATE , ICCID )
    $success = $this->updateOverlayAfterActivation();

    if ( ! $success )
      \logError( 'Could not update HTT_CUSTOMERS_OVERLAY_ULTRA' );

    // add minutes ( packaged_balance1 and packaged_balance2 )
    $this->addMinutesOnActivation( $cos_id );

    // add balance (if any)
    $this->addBalanceOnActivation();

    // insert into ACCOUNT_ALIASES
    // insert into DESTINATION_ORIGIN_MAP
    $return = \msisdn_activation_handler( $this->customerData->CUSTOMER_ID );

    if ( ! $return['success'] )
      \logError( 'msisdn_activation_handler failed' );

    // for subs who have purchased certain bolt-ons, we will need to put additional ILD credit in their wallets for migration
    $success = $this->addBoltOnsAfterActivation();

    if ( ! $success )
      \logError( 'Could not add bolt-ons' );

    // insert a row into HTT_PLAN_TRACKER
    $success = \ProjectW\addToPlanTracker([
      'customer_id'      => $this->customerData->CUSTOMER_ID,
      'cos_id'           => $cos_id,
      'plan_started_sql' => 'DATEADD( dd , - 30 , \''.$this->customerData->RENEWAL_DATE.'\' )',
      'plan_expires'     => $this->customerData->RENEWAL_DATE
    ]);

    if ( ! $success )
      \logError( 'Could not insert into HTT_PLAN_TRACKER' );

    $ultra_code = \get_dealer_code_from_univision_code( $this->customerData->UNIVISION_CODE );

    // get dealer and masteragent
    list( $dealer , $masteragent ) = \ProjectW\getDealerAndMasteragentFromUltraCode( $ultra_code );

    \logInfo( "dealer = $dealer ; masteragent = $masteragent" );

    if ( ! $plan_cost = $this->getPlanCost( (array) $this->customerData ) )
      $plan_cost = 0;

    // insert a row into HTT_ACTIVATION_LOG
    $success = \ProjectW\addToActivationLog([
      'iccid'            => $this->customerData->ICCID_FULL,
      'customer_id'      => $this->customerData->CUSTOMER_ID,
      'masteragent'      => $masteragent,
      'dealer'           => $dealer,
      'cos_id'           => $cos_id,
      'plan_cost'        => $plan_cost
    ]);

    if ( ! $success )
      \logError( 'Could not insert into HTT_ACTIVATION_LOG' );

    // add fake state transition [ Port-In Requested ] => [ Active ]
    $success = \ProjectW\addFakeActivationTransition( $this->customerData->CUSTOMER_ID , $cos_id );

    if ( ! $success ) \logError( 'Could not create state transition [ Port-In Requested ] => [ Active ]' );

    // update HTT_INVENTORY_SIM.CUSTOMER_ID
    $success = \ProjectW\updateInventorySimCustomerId( $this->customerData );

    if ( ! $success ) \logError( 'Could not update HTT_INVENTORY_SIM' );

    // enroll demo line
    $success = $this->enrollDemoLine();

    if ( ! $success ) \logError( 'Could not enroll to demo line' );

    // enroll MEP - if eligible
    $success = $this->enrollMEP();

    if ( ! $success ) \logError( 'Could not enroll to MEP' );

    // enroll employee line
    $success = $this->enrollEmployeeLine();

    if ( ! $success ) \logError( 'Could not enroll to employee line' );

    return NULL;
  }

  /**
   * recordError
   *
   * Record error for this task
   *
   * @return NULL
   */
  private function recordError()
  {
    // connect back to TSIP DB
    teldata_change_db();

    $errors = $this->get_errors();

    if ( $this->customerData->STATUS == UNIVISION_IMPORT_STATUS_TODO )
    {
      // TODO -> PORT_ERROR
      if ( ! \update_univision_import_port_error( $this->customerData->UNIVISION_IMPORT_ID , $errors[0] ) )
        \logError( 'Could not update STATUS' );
    }
    elseif ( $this->customerData->STATUS == UNIVISION_IMPORT_STATUS_ACTIVATED_TEMP )
    {
      // ACTIVATED_TEMP -> CHANGESIM_ERROR
      if ( ! \update_univision_import_changesim_error( $this->customerData->UNIVISION_IMPORT_ID , $errors[0] ) )
        \logError( 'Could not update STATUS' );
    }
    else
    {
      \logError( 'STATUS not recognized for recordError : ' . $this->customerData->STATUS );
    }

    return NULL;
  }

  /**
   * getPlanCost
   *
   * @return integer
   */
  public function getPlanCost( array $customerData )
  {
    if ( ! $cos_id = \get_cos_id_from_plan( $customerData['PLAN_NAME'] ) )
      return 0;

    if ( ! $plan_cost = \get_plan_cost_by_cos_id( $cos_id ) )
      return 0;

    return $plan_cost;
  }

  /**
   * createCustomer
   *
   * Creates customer in DB
   *
   * @return boolean
   */
  private function createCustomer( array $customerData )
  {
    dlog('',"customerData = %s",$customerData);

    $this->succeed();

    if ( ! $cos_id = \get_cos_id_from_plan( $customerData['PLAN_NAME'] ) )
    {
      $this->add_error( 'Cannot map cos_id for plan name ' . $customerData['PLAN_NAME'] );
      return FALSE;
    }

    if ( ! $plan_cost = $this->getPlanCost( $customerData ) )
    {
      $this->add_error( "Cannot map plan cost for cos_id $cos_id" );
      return FALSE;
    }

    // get dealer and masteragent
    list( $dealer , $masteragent ) = \ProjectW\getDealerAndMasteragentFromUltraCode( $customerData['ULTRA_CODE'] );

    \logInfo( "dealer = $dealer ; masteragent = $masteragent" );

    $params = [
      'masteragent'           => UNIVISION_DEFAULT_MASTERAGENT,
      #'distributor'           => '',
      'dealer'                => $dealer,
      'userid'                => '67',
      'preferred_language'    => $customerData['LANGUAGE'],
      'cos_id'                => \get_cos_id_from_plan( $customerData['PLAN_NAME'] ),
      'plan_cost'             => $plan_cost,
      'zip_code'              => $customerData['ZIP_CODE'],
      'current_iccid'         => $customerData['TEMP_ICCID_FULL'], // we will update to ICCID_FULL during ChangeSIM phase
      'msisdn'                => $customerData['MSISDN'],
      'first_name'            => $customerData['FIRST_NAME'],
      'last_name'             => $customerData['LAST_NAME'],
      'address1'              => $customerData['ADDRESS'],
      'address2'              => '', // maybe we have to split $customerData['ADDRESS']
      'city'                  => $customerData['CITY'],
      'state'                 => $customerData['STATE'],
      'e_mail'                => $customerData['EMAIL']
    ];

    // delete any customer previously created with same UNIVISION_IMPORT_ID
    $this->deleteCustomerIfRetryPort( $customerData );

    $customer_id = \ProjectW\createCustomer( $params );

    if ( $customer_id )
      $this->customerData->CUSTOMER_ID = $customer_id;
    else
      $this->add_error( 'Cannot Create Customer in Ultra DB' );

    return $this->is_success();
  }

  /**
   * deleteCustomerIfRetryPort
   *
   * Delete any customer previously created with same UNIVISION_IMPORT_ID
   *
   * @return NULL
   */
  private function deleteCustomerIfRetryPort( $customerData )
  {
    if ( ! empty( $customerData['CUSTOMER_ID'] ) )
    {
      $sql = sprintf("
UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA
SET    PLAN_STATE            = 'Cancelled',
       CURRENT_ICCID         = NULL,
       CURRENT_ICCID_FULL    = NULL,
       CURRENT_MOBILE_NUMBER = NULL,
       NOTES                 = '%s'
WHERE  CUSTOMER_ID           = %d
AND    CURRENT_MOBILE_NUMBER = %s",
        "Cancelled failed UNIVISION_IMPORT_ID ".$customerData['UNIVISION_IMPORT_ID'],
        $customerData['CUSTOMER_ID'],
        mssql_escape_with_zeroes( $customerData['MSISDN'] )
      );

      run_sql_and_check($sql);
    }
  }

  /**
   * getNextTaskData
   *
   * Get data from DB for next task
   *
   * @return boolean
   */
  private function getNextTaskData()
  {
    $candidateTaskData = \get_candidate_rows_to_process();

    dlog('',"candidateTaskData = %s",$candidateTaskData);

    $this->customerData = NULL;

    $i = 0;

    // try to reserve one of the candidate rows obtained
    while ( empty( $this->customerData ) && ! empty( $candidateTaskData[$i] ) )
    {
      // try to reserve MSISDN using Redis
     if ( \ProjectW\reserve_msisdn( $candidateTaskData[$i]->MSISDN ) )
        $this->setCustomerData( $candidateTaskData[$i] );

      $i++;
    }

    // success if we got customer data
    $success = ! empty( $this->customerData );

    \logInfo( "getNextTaskData " . ( $success ? 'succeeded' : ' had no outcome' ) );

    return $success;
  }

  /**
   * resetCustomerData
   *
   * @return NULL
   */
  public function resetCustomerData()
  {
    $this->customerData = NULL;
  }

  /**
   * setCustomerData
   *
   * @return boolean
   */
  public function setCustomerData( $customerData=NULL )
  {
    if ( empty( $customerData ) )
      return FALSE;

    \loginfo( "customerData = ".json_encode($customerData) );

    $this->customerData = $customerData;

    return TRUE;
  }

  /**
   * getCustomerData
   *
   * @return NULL
   */
  public function getCustomerData()
  {
    return $this->customerData;
  }

  /**
   * checkSettings
   *
   * Verify if settings allow the thread to continue
   *
   * @return boolean
   */
  public function checkSettings()
  {
    $enabled = ! ! \Ultra\UltraConfig\univisionImportEnabled();

    if ( ! $enabled )
      \logInfo("Import Process Is Currently Disabled");

    return $enabled;
  }

  /**
   * getProcessControlObject
   */
  public function getProcessControlObject()
  {
    if ( empty( $this->processControlObject ) )
      $this->processControlObject = new \Ultra\Lib\Util\ProcessControl(TRUE);

    return $this->processControlObject;
  }

  /**
   * checkMWResources
   *
   * Verify that MW queues are not overloaded.
   * Returns FALSE if MW queues are overloaded.
   *
   * @return boolean
   */
  public function checkMWResources()
  {
    $max_mw_queue_size = self::MAX_ALLOWED_WW_QUEUE_SIZE;

    $mw_queue_size_redis_key = 'ultra/mw_queue/outbound/size';

    $mw_queue_size = $this->redis->get( $mw_queue_size_redis_key );

    \logInfo("mw_queue_size = $mw_queue_size");

    // the result of this method is cached because we don't want to access Redis an excessive amount of times
    if ( is_null($mw_queue_size) )
    {
      // check every $ttl seconds
      $ttl = 20;

      $messageQueue = $this->controlChannel->outboundACCMWControlChannel();

      $members_1 = $this->redis->smembers( $messageQueue );

      dlog('',"count = ".count($members_1)." members = %s",$members_1);

      $messageQueue = $this->controlChannel->outboundUltraMWControlChannel();

      $members_2 = $this->redis->smembers( $messageQueue );

      dlog('',"count = ".count($members_2)." members = %s",$members_2);

      $mw_queue_size = max( count($members_1) , count($members_2) );

      if ( $mw_queue_size )
        $this->redis->set( $mw_queue_size_redis_key , $mw_queue_size , $ttl );
      else
        $this->redis->set( $mw_queue_size_redis_key , "0"            , $ttl );
    }

    \logInfo("mw_queue_size = $mw_queue_size");

    if ( $mw_queue_size > $max_mw_queue_size  )
      \logError( "*** MW queue limit reached ***" );

    return ( $mw_queue_size <= $max_mw_queue_size );
  }

  /**
   * checkServerResources
   *
   * Verify if server resources allow the thread to continue
   *
   * @return boolean
   */
  public function checkServerResources()
  {
    $processControlObject = $this->getProcessControlObject();

    $memoryUsage = $processControlObject->getMemoryUsage();
    $cpuLoad     = $processControlObject->getCpuLoad();
    $freeMemory  = $processControlObject->getFreeMemory();

    \logInfo( "memoryUsage : $memoryUsage ; freeMemory : $freeMemory ; cpuLoad : $cpuLoad" );

    $checkServerResources = (
      ( ! ( $cpuLoad     > 8  ) ) // CPU load max is 8
      &&
      ( ! ( $memoryUsage > 99 ) ) // memory usage max is 99%
    );

    if ( ! $checkServerResources )
      \logError( "*** insufficient server resources ***" );

    return $checkServerResources;
  }

  /**
   * changeSIM
   *
   * Get customer data and invoke changeSIM ACC API
   * Returns an error message if fails, NULL in case of success
   *
   * @return string
   */
  private function changeSIM()
  {
    $this->succeed();

    \logDebug( json_encode( $this->customerData ) );

    $actionUUID = \getNewUUID('PRJW','PRJW');

    return $this->performChangeSIM( (array) $this->customerData , $actionUUID );
  }

  /**
   * getTimeStamp
   *
   * Returns timestamp for ACC APIs
   *
   * @return string
   */
  public function getTimeStamp()
  {
    $timeStamp = getTimeStamp();

    $timeStamp = preg_replace( "/Z$/" , "" , $timeStamp ); // remove the Z at the end of 'timeStamp'

    return $timeStamp;
  }

  /**
   * performChangeSIM
   *
   * Invoke ChangeSIM ACC API on the network
   * Returns an error message if fails, NULL in case of success
   *
   * @return string
   */
  private function performChangeSIM( array $customerData , $actionUUID )
  {
    $this->succeed();

    $controlCommandChangeSIMObject = new \Ultra\Lib\MiddleWare\ACC\ControlCommandChangeSIM( $actionUUID );

    $parameters = array(
      'customer_id'       => $customerData['CUSTOMER_ID'],
      'MSISDN'            => $customerData['MSISDN'],
      'OLD_ICCID'         => $customerData['TEMP_ICCID_FULL'],
      'NEW_ICCID'         => $customerData['ICCID_FULL'],
      'UserData'          => $this->getUserData()
    );

    \logDebug( json_encode( $parameters ) );

    // call the MW
    $result = $controlCommandChangeSIMObject->processCommand( $parameters );

    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();

      \logError( json_encode( $errors ) );

      $this->add_error( $errors[0] );
    }

    return $this->is_success();
  }

  /**
   * getUserData
   *
   * Return info which should be passed to ACC APIs for UserData node
   *
   * @return array
   */
  public function getUserData()
  {
    return array(
      'channelId' => 'ULTRA',
      'senderId'  => 'MVNEACC',
      'timeStamp' => $this->getTimeStamp()
    );
  }

  /**
   * canUpdateStatusTo
   *
   * return TRUE if IMPORT_FILE_STATUS can be updated to $status 
   *
   * @param  string $status
   * @return bool if can update to
   */
  public function canUpdateImportFileStatusTo($status)
  {
    if ( in_array( $status , array(
      UNIVISION_FILE_STATUS_INITIALIZED,
      UNIVISION_FILE_STATUS_VALIDATED,
      UNIVISION_FILE_STATUS_ONGOING,
      UNIVISION_FILE_STATUS_PAUSED,
      UNIVISION_FILE_STATUS_COMPLETED,
      UNIVISION_FILE_STATUS_ABORTED
    )))
      return TRUE;

    if ( ! select_univision_import_by_import_file_status($status))
      return TRUE;

    return FALSE;
  }

  /**
   * validateImportFileStatusIn
   *
   * Validates import file status given statuses array
   *
   * @param  string $filename
   * @param  array  $okStatuses
   * @return bool   is valid
   */
  public function validateImportFileStatusIn($filename, $okStatuses)
  {
    $valid = FALSE;

    if ($entry = select_univision_import_file_status_by_filename($filename))
    {
      \logInfo('Current IMPORT_FILE_STATUS: ' . $entry->IMPORT_FILE_STATUS);

      if ( ! in_array($entry->IMPORT_FILE_STATUS, $okStatuses))
        \logError("ERROR: Current IMPORT_FILE_STATUS {$entry->IMPORT_FILE_STATUS} not in " . json_encode($okStatuses));
      else
        $valid = TRUE;
    }
    else
      \logError('ERROR: No DB data associated with filename');

    return $valid;
  }

  /**
   * validateImportFile
   *
   * @param  string  $filename
   * @return array   Array with msisdn, iccid_full, temp_iccid_full, and last_error for each row
   */
  public function validateImportFile( $filename=NULL )
  {
    if ( empty( $filename ) )
      return \logError('ERROR: filename is empty');

    $result = array();

    // get records
    $records = \select_univision_import_file_by_filename( $filename );
    foreach ( $records as $r )
    {
      // check MSISDN and ICCID fields
      $error = null;

      if ( $existing = $this->validateMSISDNAndICCID( $r->MSISDN, $r->ICCID_FULL, $r->TEMP_ICCID_FULL ) )
      {
        // determine which value already exists
        if ( $r->MSISDN == $existing->current_mobile_number )
          $error = 'MSISDN ' . $r->MSISDN . ' already exists';
        elseif ( $r->ICCID_FULL == $existing->current_iccid || $r->ICCID_FULL == $existing->CURRENT_ICCID_FULL )
          $error = 'ICCID_FULL ' . $r->ICCID_FULL . ' already exists';
        elseif ( $r->TEMP_ICCID_FULL == $existing->current_iccid || $r->TEMP_ICCID_FULL == $existing->CURRENT_ICCID_FULL )
          $error = 'TEMP_ICCID_FULL ' . $r->TEMP_ICCID_FULL . ' already exists';
        else
          $error = 'Unknown validation error';  // should not happen
      }

      if ( empty($error) )
      {
        if ( ! check_if_univision_code_exists($r->UNIVISION_CODE) )
          \logWarn( "UNVISION_CODE {$r->UNIVISION_CODE} does not exit in ULTRA.UNIVISION_CODE_TO_DEALER_CODE" );
      }

      // update LAST_ERROR and LAST_ERROR_DATE values
      // clear last error on $error == null
      $this->updateLastErrorByImportId($r->UNIVISION_IMPORT_ID, $error);

      if ( ! empty($error) )
        $result[] = array(
          'MSISDN'          => $r->MSISDN,
          'ICCID_FULL'      => $r->ICCID_FULL,
          'TEMP_ICCID_FULL' => $r->TEMP_ICCID_FULL,
          'LAST_ERROR'      => $error
        );
    }

    // update IMPORT_FILE_STATUS
    // IMPORT_FILE_STATUS should be updated to VALIDATED (even in case of some failures)
    $updateResult = \update_univision_import([
      'file_name'           => $filename,
      'import_file_status'  => 'VALIDATED'
    ]);

    if ( ! $updateResult )
      \logError( 'Failed to update IMPORT_FILE_STATUS of UNIVISION_IMPORT records' );

    return $result;
  }

  /**
   * updateLastErrorByImportId
   *
   * @param  int    $univisionImportId
   * @param  string $error
   * @return string $error || null
   */
  private function updateLastErrorByImportId($univisionImportId, $error)
  {
    $updateResult = \update_univision_import([
      'univision_import_id' => $univisionImportId,
      'last_error'          => $error
    ]);

    if ( ! $updateResult )
    {
      $error = 'Failed to update LAST_ERROR of UNIVISION_IMPORT record';
      \logError( $error );
      return $error;
    }

    return NULL;
  }

  /**
   * validateMSISDNAndICCID
   *
   * @param  string  $msisdn
   * @param  string  $iccid
   * @param  string  $tempICCID
   * @return Object || NULL the first matching record, if exists
   */
  public function validateMSISDNAndICCID($msisdn, $iccid, $tempICCID)
  {
    $existing = \get_ultra_customers_by_msisdn_or_iccids($msisdn, array($iccid, $tempICCID));

    return count($existing) ? $existing[0] : null;
  }

  /**
   * updateImportFileStatusByFilename
   *
   * @param  string  $filename
   * @param  string  $newStatus status to update to
   * @return boolean was query successful?
   */
  public function updateImportFileStatusByFilename($filename, $newStatus)
  {
    return update_univision_import(
      array(
        'file_name'          => $filename,
        'import_file_status' => $newStatus
      )
    );
  }

  /**
   * portInCustomer
   *
   * Get customer data and invoke Port In ACC API
   * Returns an error message if fails, NULL in case of success
   *
   * @return boolean
   */
  private function portInCustomer()
  {
    $this->succeed();

    \logDebug( json_encode( $this->customerData ) );

    $actionUUID = \getNewUUID('PRJW','PRJW');

    return $this->performPortInCustomer( (array) $this->customerData , $actionUUID );
  }

  /**
   * performPortInCustomer
   *
   * Invoke Port In ACC API on the network
   * Returns an error message if fails, NULL in case of success
   *
   * @return boolean
   */
  private function performPortInCustomer( array $customerData , $actionUUID )
  {
    $this->succeed();

    $controlCommandPortInObject = new \Ultra\Lib\MiddleWare\ACC\ControlCommandPortIn( $actionUUID );

    $parameters = [
      'customer_id'       => $customerData['CUSTOMER_ID'],
      'MSISDN'            => $customerData['MSISDN'],
      'ICCID'             => $customerData['TEMP_ICCID_FULL'],
      'zipCode'           => $customerData['ZIP_CODE'],
      'wholesalePlan'     => \Ultra\MvneConfig\getACCWholesaleValueByPlanName( $customerData['PLAN_NAME'] ),
      'ultra_plan_name'   => $customerData['PLAN_NAME'],
      'preferredLanguage' => strtolower( $customerData['LANGUAGE'] ),
      'UserData'          => $this->getUserData(),
      'donorAccountNumber'   => $customerData['ACCOUNT_NUMBER'],
      'donorAccountPassword' => $customerData['ACCOUNT_PIN'], // PIN
      'subplan'           => 'A'
    ];

    \logDebug( json_encode( $parameters ) );

    // call the MW
    $result = $controlCommandPortInObject->processCommand( $parameters );

    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();

      \logError( json_encode( $errors ) );

      $this->add_error( $errors[0] );
    }

    return $this->is_success();
  }

  /**
   * planRenewalCovered
   *
   * Returns TRUE if plan renewal is covered
   *
   * @return boolean
   */
  public function planRenewalCovered( $customerData , $plan_cost )
  {
    \logInfo( 'Balance = ' . $this->customerData->BALANCE. ' ; plan_cost = '.$plan_cost );

    return ! ( $this->customerData->BALANCE < $plan_cost );
  }

  /**
   * enrollMEP
   *
   * Add customer to MEP.
   * During nightly migration, identify if subscriber is a MEP candidate using following eligibility criteria:
   *  - renewal date of 2/1-2/11
   *  - Suspended when migrated, OR, not "recharged" in advance when migrated (not enough balance to cover plan cost)
   *
   * @return boolean
   */
  public function enrollMEP()
  {
    // demo accounts are not MEP eligible
    if ( $this->customerData->DEMO_ACCOUNT )
    {
      \logInfo( "Customer Id {$this->customerData->CUSTOMER_ID} is a demo account, not MEP eligible" );
      return TRUE;
    }

    // house accounts are not MEP eligible
    if ( $this->customerData->HOUSE_ACCOUNT )
    {
      \logInfo( "Customer Id {$this->customerData->CUSTOMER_ID} is house account, not MEP eligible" );
      return TRUE;
    }

    if ( ! $plan_cost = $this->getPlanCost( (array) $this->customerData ) )
      return \logError( "Cannot map plan cost for plan ".$this->customerData->PLAN_NAME );

    // MEP_ELIGIBLE if renewal date is between 2/1 and 2/11
    if ( ! $this->customerData->MEP_ELIGIBLE )
    {
      \logInfo( "Customer Id {$this->customerData->CUSTOMER_ID} is not MEP eligible : {$this->customerData->RENEWAL_DATE}" );
      return TRUE;
    }

    if ( ( ! $this->planRenewalCovered( $this->customerData , $plan_cost )
      || ( $this->customerData->PLAN_STATE == STATE_SUSPENDED ) )
    )
    {
      \logInfo( "Customer Id {$this->customerData->CUSTOMER_ID} is MEP eligible : {$this->customerData->RENEWAL_DATE}" );

      if ( ! $customer = get_customer_from_customer_id( $this->customerData->CUSTOMER_ID ) )
        return \logError( "Cannot load customer data from DB - ".$this->customerData->CUSTOMER_ID );

      // add STORED_VALUE for total plan amount
      $result = \func_courtesy_add_stored_value(
        array(
          'terminal_id'      => '0',
          'amount'           => $plan_cost,
          'reference'        => create_guid( __FUNCTION__ ),
          'source'           => 'UVIMPORT',
          'reason'           => 'UVIMPORT.MEP_SV_INCREASE',
          'customer'         => $customer,
          'detail'           => 'ProjectW::enrollMEP'
        )
      );

      if ( ! empty($result['errors']) )
        return FALSE;

      // For already suspended subscribers, the MEP start date (option_value column) will be set to the migration date.
      // For active subscribers (soon to be MEP), the start date should be their renewal date.
      $mep_start_date = ( $this->customerData->PLAN_STATE == STATE_SUSPENDED )
                      ?
                      $this->customerData->IMPORT_FILE_DATE_TIME
                      :
                      $this->customerData->RENEWAL_DATE
                      ;

      // add record in ULTRA.CUSTOMER_OPTIONS
      $result = \enroll_migration_extension_plan( $this->customerData->CUSTOMER_ID , $mep_start_date );

      if ( $result->is_failure() )
        return FALSE;
    }

    return TRUE;
  }

  /**
   * enrollDemoLine
   *
   * Enroll customer as Demo Line
   * We have to insert a row into ULTRA.DEMO_LINE for new UV imported demo lines
   *
   * @return boolean
   */
  public function enrollDemoLine()
  {
    if ( $this->customerData->DEMO_ACCOUNT )
    {
//TODO
$activations  = 0; // as per Vitaly
$dealer       = '999999'; // TODO: use activating dealer ID
      return \Ultra\Lib\DemoLine\createDemoLine( $dealer , DEMO_DEALER_EPP , $activations );
    }

    return TRUE;
  }

  /**
   * enrollEmployeeLine
   *
   * Enroll customer as a house account if eligible
   *
   * @return boolean
   */
  public function enrollEmployeeLine()
  {
    if ( $this->customerData->HOUSE_ACCOUNT )
    {
      $result = enroll_mrc_house_account( $this->customerData->CUSTOMER_ID , 'UVIMPORT' );

      if ( $result->is_failure() )
        return FALSE;
    }

    return TRUE;
  }

  /**
   * enrollSubPlanA
   *
   * Add customer to SubPlan A
   *
   * @return boolean
   */
  public function enrollSubPlanA()
  {
    $result = \set_brand_uv_subplan( $this->customerData->CUSTOMER_ID , 'A' , 0 );

    if ( $result->is_failure() )
      return FALSE;

    return TRUE;
  }

  /**
   * updateOverlayAfterSuspend
   *
   * @return boolean
   */
  private function updateOverlayAfterSuspend( $customer_id )
  {
    $sql = \htt_customers_overlay_ultra_update_query(
      array(
        'customer_id'    => $customer_id,
        'plan_state'     => STATE_SUSPENDED
      )
    );

    return ! ! run_sql_and_check($sql);
  }

  /**
   * updateOverlayAfterActivation
   *
   * Update HTT_CUSTOMERS_OVERLAY_ULTRA
   * * PLAN_STARTED   = RENEWAL_DATE - 30
   * * PLAN_EXPIRES   = RENEWAL_DATE
   * * GROSS_ADD_DATE = RENEWAL_DATE - 30
   *
   * @return boolean
   */
  public function updateOverlayAfterActivation()
  {
    $sql = \htt_customers_overlay_ultra_update_query(
      array(
        'customer_id'     => $this->customerData->CUSTOMER_ID,
        'iccid_current'   => $this->customerData->ICCID_FULL,
        'iccid_activated' => $this->customerData->ICCID_FULL,
        'plan_state'      => STATE_ACTIVE,
        'plan_started'    => 'DATEADD( dd , - 30 , \''.$this->customerData->RENEWAL_DATE.'\' )',
        'plan_expires'    => "'".$this->customerData->RENEWAL_DATE."'",
        'gross_add_date'  => 'DATEADD( dd , - 30 , \''.$this->customerData->RENEWAL_DATE.'\' )',
        'monthly_renewal_target' => $this->customerData->MONTHLY_RENEWAL_TARGET
      )
    );

    return ! ! run_sql_and_check($sql);
  }

  /**
   * activationBoltOns
   *
   * Feature => Call Anywhere Credit ( in $ , to be added to packaged_balance1 )
   *
   * @return array
   */
  public function activationBoltOns()
  {
    return [
      49 => 10,
      51 => 10
    ];
  }

  /**
   * addBoltOnsAfterActivation
   *
   * For subs who have purchased certain bolt-ons, we will need to put additional ILD credit in their wallets for migration
   *
   * @return boolean
   */
  protected function addBoltOnsAfterActivation()
  {
    $success = TRUE;

    $activationBoltOns = $this->activationBoltOns();

    \logInfo( 'customer_id = '.$this->customerData->CUSTOMER_ID.' ; feature : '.$this->customerData->FEATURE );

    if ( ! empty( $this->customerData->FEATURE )
      && ! empty( $activationBoltOns[ $this->customerData->FEATURE ] ) )
    {
      \logInfo( '$'.$activationBoltOns[ $this->customerData->FEATURE ].' will be added to ACCOUNTS.PACKAGED_BALANCE1' );

      $success = $this->addBoltOnMinutes( $activationBoltOns[ $this->customerData->FEATURE ] );

      if ( ! $success )
        \logError( 'Cannot add bolt on '.$this->customerData->FEATURE );
    }

    return $success;
  }

  /**
   * addBoltOnMinutes
   *
   * Add to ACCOUNTS.PACKAGED_BALANCE1
   * Insert into HT_BILLING_HISTORY
   *
   * @return boolean
   */
  protected function addBoltOnMinutes( $dollarValue )
  {
    // Add to ACCOUNTS.PACKAGED_BALANCE1
    $sql = \accounts_update_query([
      'add_to_package_balance1' => $dollarValue * 10,
      'customer_id'             => $this->customerData->CUSTOMER_ID
    ]);

    if ( ! is_mssql_successful( logged_mssql_query( $sql ) ) )
      return FALSE;

    // Insert into HT_BILLING_HISTORY
    $success = \ProjectW\addToBillingHistory([
      'customer_id'            => $this->customerData->CUSTOMER_ID,
      'balance_change'         => 0,
      'package_balance_change' => $dollarValue,
      'cos_id'                 => \get_cos_id_from_plan( $this->customerData->PLAN_NAME ),
      'description'            => 'UVIMPORT.BOLTON',
      'detail'                 => 'ProjectW::addBoltOnMinutes',
      'reference'              => create_guid( __FUNCTION__ ),
      'source'                 => 'CSCOURTESY',
      'entry_type'             => 'UVIMPORT',
      'charge_amount'          => 0,
      'commissionable_charge_amount' => 0
    ]);

    if ( ! $success )
      \logError( 'Could not insert into HT_BILLING_HISTORY' );

    return TRUE;
  }

  /**
   * handleProvisioningStatus
   *
   * We received an inboud ProvisioningStatus notification
   *
   * @return NULL
   */
  public function handleProvisioningStatus( $parameters )
  {
    dlog('',"customerData = %s",$this->customerData);
    dlog('',"parameters   = %s",$parameters);

    if ( $parameters['ProvisioningType'] == 'UpdateSIM' )
      return $this->handleProvisioningStatusChangeSIM( $parameters );
    else
      return $this->handleProvisioningStatusActivateSubscriber( $parameters );
  }

  /**
   * handleProvisioningStatus
   *
   * We received an inboud notification after a ActivateSubscriber SOAP call
   *
   * @return NULL
   */
  protected function handleProvisioningStatusActivateSubscriber( $parameters )
  {
    if ( $parameters['ProvisioningStatus'] )
      // ActivateSubscriber success (asynch)
      $this->recordActivatedTemp();
    else
    {
      // ActivateSubscriber failure (asynch)
      // this really never happens

      $error = "PortErrorCode = {$parameters['PortErrorCode']} ; StatusDescription = {$parameters['StatusDescription']} ; PortErrorDesc = {$parameters['PortErrorDesc']}";

      $this->recordPortFailed( $error );
    }

    return NULL;
  }

  /**
   * handleProvisioningStatusChangeSIM
   *
   * We received an inboud notification after a ChangeSIM SOAP call
   *
   * @return NULL
   */
  protected function handleProvisioningStatusChangeSIM( $parameters  )
  {
    if ( $parameters['ProvisioningStatus'] )
      // ChangeSIM success (asynch)
      $this->recordActivated();
    else
    {
      // ChangeSIM failure (asynch)
      // this really never happens

      $error = "PortErrorCode = {$parameters['PortErrorCode']} ; StatusDescription = {$parameters['StatusDescription']} ; PortErrorDesc = {$parameters['PortErrorDesc']}";

      $this->recordChangeSIMFailed( $error );
    }

    return NULL;
  }

  /**
   * handlePortStatusUpdate
   *
   * We received an inboud PortStatusUpdate notification
   *
   * @return NULL
   */
  public function handlePortStatusUpdate( $parameters  )
  {
    dlog('',"customerData = %s",$this->customerData);
    dlog('',"parameters   = %s",$parameters);

    $error = "PortErrorCode = {$parameters['PortErrorCode']} ; StatusDescription = {$parameters['StatusDescription']} ; PortErrorDesc = {$parameters['PortErrorDesc']}";

    $this->recordPortFailed( $error );

    return NULL;
  }

  /**
   * getUVImportByMSISDN
   *
   * @return boolean
   */
  public function getUVImportByMSISDN( $msisdn )
  {
    $sql = \univision_import_select_query([
      'msisdn' => $msisdn
    ]);

    if ( $customerData = \find_first($sql) )
      $this->setCustomerData( $customerData );

    return $customerData;
  }

  /**
   * getUVImportByID
   *
   * @return boolean
   */
  public function getUVImportByID( $univision_import_id )
  {
    $sql = \univision_import_select_query([
      'univision_import_id' => $univision_import_id
    ]);

    if ( $customerData = \find_first($sql) )
      $this->setCustomerData( $customerData );

    return $customerData;
  }

  /**
   * checkUVImportPortRetryStatus
   *
   * Checks if univision_import record has valid status for port retry
   * Returns true if STATUS is CHANGESIM_ERROR or CHANGESIM_FAILED and IMPORT_FILE_STATUS is ONGOING
   *
   * @return boolean
   */
  public function checkUVImportPortRetryStatus( $customerData )
  {
    if ( !empty( $customerData->STATUS ) && !empty( $customerData->IMPORT_FILE_STATUS ) )
      if ( ( $customerData->STATUS == 'PORT_ERROR' || $customerData->STATUS == 'PORT_FAILED' ) && $customerData->IMPORT_FILE_STATUS == 'ONGOING' )
        return true;

    return false;
  }

  /**
   * checkUVImportChangeSIMRetryStatus
   *
   * Checks if univision_import record has valid status for change sim retry
   * Returns true if STATUS is PORT_ERROR or PORT_FAILED and IMPORT_FILE_STATUS is ONGOING
   *
   * @return boolean
   */
  public function checkUVImportChangeSIMRetryStatus( $customerData )
  {
    if ( !empty( $customerData->STATUS ) && !empty( $customerData->IMPORT_FILE_STATUS ) )
      if ( ( $customerData->STATUS == 'CHANGESIM_ERROR' || $customerData->STATUS == 'CHANGESIM_FAILED' ) && $customerData->IMPORT_FILE_STATUS == 'ONGOING' )
        return true;

    return false;
  }

  /**
   * setUVImportPortRetry
   *
   * Sets a univision_import record for port retry
   * Sets STATUS to TODO and appends "Set to retry port" to IMPORT_HISTORY
   *
   * @return boolean
   */
  public function setUVImportPortRetry( $customerData )
  {
    if ( !empty( $customerData->UNIVISION_IMPORT_ID ) )
      return \update_univision_import_port_retry( $customerData->UNIVISION_IMPORT_ID );

    return false;
  }

  /**
   * setUVImportChangeSIMRetry
   *
   * Sets a univision_import record for change SIM retry
   * Sets STATUS to ACTIVATED_TEMP and appends "Set to retry ChangeSIM" to IMPORT_HISTORY
   *
   * @return boolean
   */
  public function setUVImportChangeSIMRetry( $customerData )
  {
    if ( !empty( $customerData->UNIVISION_IMPORT_ID ) )
      return \update_univision_import_changesim_retry( $customerData->UNIVISION_IMPORT_ID );

    return false;
  }
}

