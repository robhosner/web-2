<?php

global $credentials;
require_once 'core.php';
require_once 'cosid_constants.php';
require_once 'db.php';

require_once 'classes/PHPUnitBase.php';
require_once 'classes/Provisioning.php';

class ProvisioningTest extends PHPUnitBase
{
  public function test__verifiyPortInEligibility()
  {
    // valid
    $result = Provisioning::verifyPortInEligibility('7144959606');
    print_r($result);
    $this->assertTrue($result->is_success());

    // already exists
    $result = Provisioning::verifyPortInEligibility('9796338852');
    print_r($result);
    $this->assertTrue( ! $result->is_success());

    $result = Provisioning::verifyPortInEligibility('555');
    print_r($result);
    $this->assertTrue( ! $result->is_success());
  }

  public function test__verifyNumberNotInSystem()
  {
    teldata_change_db();

    // not in system
    $result = Provisioning::verifyNumberNotInSystem('7144959606');
    print_r($result);
    $this->assertTrue($result->is_success());

    // is in system
    $result = Provisioning::verifyNumberNotInSystem('9796338852');
    print_r($result);
    $this->assertTrue( ! $result->is_success());
  }

  public function test__verifyICCIDGoodToActivate()
  {
    teldata_change_db();

    $iccid = '8901260842107734452';
    $result = Provisioning::verifyICCIDGoodToActivate($iccid);
    print_r($result);
    $this->assertTrue($result->is_success());

    $iccid = '8901260842107734320';
    $result = Provisioning::verifyICCIDGoodToActivate($iccid);
    print_r($result);
    $this->assertTrue($result->is_success());
  }

  public function test__configureBoltOns()
  {
    $boltOns = array('DATA_2048_20','IDDCA_12.5_10');
    $result = Provisioning::configureBoltOns($boltOns);
    print_r($result);
    print_r($result->get_data_key('bolt_on_configuration'));
    print_r($result->get_data_key('bolt_ons_cost'));
    $this->assertTrue($result->is_success());

    // invalid bolt ids
    $boltOns = array('a','b');
    $result = Provisioning::configureBoltOns($boltOns);
    print_r($result);
    print_r($result->get_data_key('bolt_on_configuration'));
    print_r($result->get_data_key('bolt_ons_cost'));
    $this->assertTrue( ! $result->is_success());
  }

  public function test__intraBrandPortOperations()
  {
    teldata_change_db();
    $sim = get_htt_inventory_sim_from_iccid('101010101010101004');
    $result   = Provisioning::intraBrandPortOperations(19485, $sim);
    print_r($result);
    $this->assertTrue($result->is_success());
  }
}
