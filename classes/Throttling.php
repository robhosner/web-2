<?php

class Throttling
{
  public $map = null;
  public $type = null;
  public $currentSpeed = null;

  public function __construct($plan = null)
  {
    $this->type = self::detectThrottlingType($plan);
    switch ($this->type)
    {
      case 'flex':
        $this->map = self::throttlingSpeedMapBan();
        break;
      default:
        $this->map = self::throttlingSpeedMap();
        break;
    }
  }

  /**
   * sets the current throttle speed
   *
   * @param  string full,1536,1024
   * @return null
   */
  public function setCurrentThrottleSpeed($speed)
  {
    $this->currentSpeed = $speed;
  }

  /**
   * detects throttling type for plan
   *
   * @param  string UF15
   * @return string throttling type
   */
  public static function detectThrottlingType($plan)
  {
    if (\Ultra\UltraConfig\isFlexPlan($plan))
      return 'flex';

    return null;
  }

  /**
   * check if a plan should be able to update their throttle speed
   *
   * @param  string plan short name
   * @return bool
   */
  public static function planCanUpdateThrottleSpeed($plan)
  {
    return ! (in_array($plan, ['UV30','UVTHIRTY']));
  }

  /**
   * get the marketing term for provided id
   *
   * @param  string id
   * @return string marketing term
   */
  public static function throttleSpeedMarketingTerms($id)
  {
    $map = [
      'full' => 'Full Speed',
      '1536' => 'Optimized - 1536 kbps',
      '1024' => 'Super Saver - 1024 kbps'
    ];

    return (isset($map[$id])) ? $map[$id] : null;
  }

  /**
   * a map of throttling features for BAN
   *
   * @return array
   */
  public static function throttlingSpeedMapBan()
  {
    return [
      'plan' => [
        'full' => ['feature_id' => 12924, 'name' => 'WHPADJB1',  'config' => 'a_data_pool_1' ],
        '1536' => ['feature_id' => 12931, 'name' => 'WHATH1536', 'config' => 'n_pool_thr1536'],
        '1024' => ['feature_id' => 12930, 'name' => 'WHATH1024', 'config' => 'n_pool_thr1024']
      ],
      'block' => null
    ];
  }

  /**
   * a map of throttling features
   *
   * @return array
   */
  public static function throttlingSpeedMap()
  {
    return [
      'plan' => [
        'full'  => ['feature_id' => 12909, 'name' => 'WPRBLK20',  'config' => 'a_data_blk_plan'],
        '1536'  => ['feature_id' => 12917, 'name' => 'WPRBLK33S', 'config' => 'a_data_blk1536_plan'],
        '1024'  => ['feature_id' => 12919, 'name' => 'WPRBLK35S', 'config' => 'a_data_blk1024_plan']
      ],
      'block' => [
        'full'  => ['feature_id' => 12907, 'name' => '7007',      'config' => 'a_data_blk'],
        '1536'  => ['feature_id' => 12918, 'name' => 'WPRBLK34S', 'config' => 'a_data_blk1536'],
        '1024'  => ['feature_id' => 12920, 'name' => 'WPRBLK36S', 'config' => 'a_data_blk1024']
      ]
    ];
  }

  /**
   * returns throttle speed based on BalanceValues from CheckBalance
   *
   * @param  object BalanceValues
   * @return string speed
   */
  public function detectThrottleSpeedFromBalanceValues($balanceValues)
  {
    foreach ($balanceValues as $balanceValue)
    {
      if ( ! empty($balanceValue->Type))
      {
        // just check by plan
        foreach ($this->map['plan'] as $speed => $item)
          if ($balanceValue->Type == $item['name'])
            return $speed;
      }
    }

    return null;
  }

  /**
   * adjust a plan configuration array for throttle speed
   *
   * @param array by reference
   * @param string throttle speed
   */
  public function adjustPlanConfigForThrottle(&$accSocsPlanConfig)
  {
    if ( ! $this->currentSpeed)
      return;

    foreach ($accSocsPlanConfig as $feature => $val)
    {
      if (in_array($feature, [
        'a_data_blk_plan',
        'a_data_blk1536_plan',
        'a_data_blk1024_plan',
        // 'a_data_pool_1',
        // 'n_pool_thr1536',
        // 'n_pool_thr1024'
      ]))
      {
        unset($accSocsPlanConfig[$feature]);
        $accSocsPlanConfig[$this->map['plan'][$this->currentSpeed]['config']] = $val;
      }
    }
  }

  /**
   * given a customers add on features, detects their throttle speed
   *
   * @param array
   * @param string throttle speed
   */
  public function detectThrottleSpeedFromAddOnFeatureInfo($addOnFeatureInfo)
  {
    if ($this->type == 'flex')
      return $this->detectThrottleSpeedFromAddOnFeatureInfoBan($addOnFeatureInfo);

    foreach ($addOnFeatureInfo as $addOnFeature)
    {
      if ( ! empty($addOnFeature->FeatureID))
      {
        // just check by plan
        foreach ($this->map['plan'] as $speed => $item)
          if ($addOnFeature->FeatureID == $item['feature_id'])
            return $speed;
      }
    }

    return null;
  }

  /**
   * given a customers add on features, detects their throttle speed
   *
   * @param array
   * @param string throttle speed
   */
  public function detectThrottleSpeedFromAddOnFeatureInfoBan($addOnFeatureInfo)
  {
    $featureIds = [];
    foreach( $addOnFeatureInfo as $addOnFeature )
      $featureIds[] = $addOnFeature->FeatureID;

    if (in_array(12931, $featureIds))
      return '1536';

    if (in_array(12930, $featureIds))
      return '1024';

    return 'full';
  }

  /**
   * checkThrottleUpdateRequired
   *
   * checks requested throttle speed with customers throttle speed
   * to determine if an update is required
   *
   * @param  object
   * @param  string
   * @return boolean
   */
  public function checkThrottleUpdateRequired($addOnFeatureInfo, $throttleSpeed)
  {
    if ($this->type == 'flex')
      return $this->checkThrottleUpdateRequiredBan($addOnFeatureInfo, $throttleSpeed);

    $required = ['plan' => FALSE, 'block' => FALSE];

    foreach( $addOnFeatureInfo as $addOnFeature )
    {
      // plan or block
      $feature = $this->getThrottleSpeedFeatureFromSocId($addOnFeature->FeatureID);

      // if customer already has feature
      if ( $feature && $addOnFeature->FeatureID != $this->getThrottleSpeedSocId($feature, $throttleSpeed) )
        $required[$feature] = TRUE;
    }

    return ($required['plan'] || $required['block']);
  }

  /**
   * checkThrottleUpdateRequired
   *
   * checks requested throttle speed with customers throttle speed
   * to determine if an update is required
   *
   * @param  object
   * @param  string
   * @return boolean
   */
  protected function checkThrottleUpdateRequiredBan($addOnFeatureInfo, $throttleSpeed)
  {
    $required = FALSE;

    $featureIds = [];
    foreach( $addOnFeatureInfo as $addOnFeature )
      $featureIds[] = $addOnFeature->FeatureID;

    if ($throttleSpeed == 'full')
    {
      if ( in_array($this->map['plan']['1536']['feature_id'], $featureIds)
        || in_array($this->map['plan']['1024']['feature_id'], $featureIds))
        $required = TRUE;
    }

    if ($throttleSpeed == '1536')
    {
      if (   in_array($this->map['plan']['1024']['feature_id'], $featureIds)
        || ! in_array($this->map['plan']['1536']['feature_id'], $featureIds))
        $required = TRUE;
    }

    if ($throttleSpeed == '1024')
    {
      if (   in_array($this->map['plan']['1536']['feature_id'], $featureIds)
        || ! in_array($this->map['plan']['1024']['feature_id'], $featureIds))
        $required = TRUE;
    }

    return $required;
  }

  /**
   * given a feature id, returns feature type
   *
   * @param int $socId
   *
   * @return string ['plan','block']
   */
  public function getThrottleSpeedFeatureFromSocId($socId)
  {
    foreach ($this->map as $feature => $featureItem)
    {
      foreach ($featureItem as $speedItem)
      {
        if ($speedItem['feature_id'] == $socId)
          return $feature;
      }
    }

    return null;
  }

  /**
   * given a feature type and speed, returns proper feature id
   *
   * @param string $feature ['plan','block']
   * @param string $speed
   *
   * @return int ex 12909
   */
  public function getThrottleSpeedSocId($feature, $speed)
  {
    return (isset($this->map[$feature][$speed])) ? $this->map[$feature][$speed]['feature_id'] : null;
  }

  /**
   * given a feature type and speed, returns feature name
   *
   * @param string $feature ['plan','block']
   * @param string $speed
   *
   * @return string ex WPRBLK20
   */
  function getThrottleSpeedSocType($feature, $speed)
  {
    return (isset($this->map[$feature][$speed])) ? $this->map[$feature][$speed]['name'] : null;
  }

  /**
   * given a feature name, returns feature type
   *
   * @param int $type ex. WPRBLK20
   *
   * @return string ['plan','block']
   */
  function getThrottleSpeedFeatureFromType($type)
  {
    foreach ($this->map as $feature => $featureItem)
    {
      foreach ($featureItem as $speedItem)
      {
        if ($speedItem['name'] == $type)
          return $feature;
      }
    }

    return null;
  }

  /**
   * given feature id, return feature name
   *
   * @param  int $socId
   * @return string
   */
  function getThrottleSpeedTypeFromSocId($socId)
  {
    foreach ($this->map as $feature)
    {
      foreach ($feature as $speed => $item)
      {
        if ($item['feature_id'] == $socId)
          return $item['name'];
      }
    }

    return null;
  }

  /**
   * given addOnFeatureInfo, returns roaming soc id
   *
   * @param  object $addOnFeatureInfo
   * @return string
   */
  function detectRoamingSoc($addOnFeatureInfo)
  {
    foreach ($addOnFeatureInfo as $addOnFeature)
    {
      if ( ! empty($addOnFeature->FeatureID))
      {
        if (\Ultra\UltraConfig\isRoamingSoc($addOnFeature->FeatureID))
          return $addOnFeature->FeatureID;
      }
    }

    return null;
  }

  /**
   * given addOnFeatureInfo, returns wallet soc id
   *
   * @param  object $addOnFeatureInfo
   * @return string
   */
  function detectRoamingWalletSoc($addOnFeatureInfo)
  {
    foreach ($addOnFeatureInfo as $addOnFeature)
    {
      if ( ! empty($addOnFeature->FeatureID))
      {
        if (\Ultra\UltraConfig\isRoamingWalletSoc($addOnFeature->FeatureID))
          return $addOnFeature->FeatureID;
      }
    }

    return null;
  }

  /**
   * gets balance values of throttled soc from mvneGet4gLTE[4]
   *
   * @param string
   * @param string throttle speed
   */
  function throttleValuesFromBalanceValues($balanceValues)
  {
    $values = null;

    foreach ($balanceValues as $key => $val)
    {
      foreach ($this->map as $type)
      {
        foreach ($type as $speed => $item)
          if ($key == $item['name'])
          {
            $values[$key] = [];
            $values[$key]['Used']  = $val["$key Used"];
            $values[$key]['Limit'] = $val["$key Limit"];
          }
      }
    }

    return $values;
  }

  /**
   * given a data soc, finds equivalent soc for given speed
   *
   * @param string
   * @param string throttle speed
   */
  function adjustDataSocForThrottle($dataSoc, $throttleSpeed)
  {
    if ( ! $throttleSpeed)
      return null;

    foreach ($this->map as $feature => $featureItem)
    {
      foreach ($featureItem as $speedItem)
      {
        if ($dataSoc == $speedItem['config'])
          return $this->map[$feature][$throttleSpeed]['config'];
      }
    }

    return $dataSoc;
  }
}
