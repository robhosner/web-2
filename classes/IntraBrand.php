<?php

class IntraBrand
{
  /**
   * isUVImportPortByMSISDN
   * checks for uv port by searching for
   * UVIMPORT reference in htt_billing_history
   *
   * @param  String $msisdn
   * @return Bool
   */
  public static function isUVImportPortByMSISDN($msisdn)
  {
    $customer = get_customer_from_msisdn($msisdn, 'u.CUSTOMER_ID');
    if ($customer && ! empty($customer->CUSTOMER_ID))
    {
      // if customer has reference source of UVIMPORT in htt_billing_history
      if (find_first(htt_billing_history_last_reference_source($customer->CUSTOMER_ID, get_reference_source('UVIMPORT'))))
        return true;
    }

    return false;
  }

  /**
   * isIntraBrandPortByMSISDNAndSIM
   * compares customer from msisdn BRAND_ID to $sim BRAND_ID
   * if different, is intra brand port
   *
   * @param  String $msisdn
   * @param  Object $sim
   * @return Bool
   */
  public static function isIntraBrandPortByMSISDNAndSIM($msisdn, $sim)
  {
    $customer = get_customer_from_msisdn($msisdn, 'u.CUSTOMER_ID,u.BRAND_ID');
    if ($customer && $sim->BRAND_ID != $customer->BRAND_ID)
      return true;

    return false;
  }

  /**
   * isIntraBrandPortByReferenceSource
   * checks HTT_BILLING_HISTORY for INTRAPORT reference source
   * if exists, is intra brand port
   *
   * @param  Integer $customer_id
   * @return Bool
   */
  public static function isIntraBrandPortByReferenceSource($customer_id)
  {
    if (htt_billing_history_last_reference_source($customer_id, get_reference_source('INTRAPORT')))
      return true;

    return false;
  }
}
