<?php

namespace Ultra\Lib;

require_once 'Ultra/Lib/Services/FamilyAPI.php';
require_once 'Ultra/Lib/Services/OnlineSalesAPI.php';
require_once 'Ultra/Lib/Services/SharedData.php';
require_once 'Ultra/Lib/Util/Redis.php';

class Flex
{
  const REDIS_DATA_AMOUNT_KEY = 'flex/bolt_on_data/';

  /**
   * @param  string
   * @return bool
   */
  public static function isFlexPlan($plan)
  {
    return in_array($plan, [
      'ULTRA_FLEX_FIFTEEN',
      'UF15',
      '1000000090'
    ]);
  }

  /**
   * @param  int
   * @return array
   */
  public static function getInfoByCustomerId($customerId)
  {
    $info = null;

    try
    {
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $getFamilyByCustomerIDResult = $familyApi->getFamilyByCustomerID($customerId);

      if ( ! $getFamilyByCustomerIDResult->is_success())
        throw new \Exception('ESCALATION FLEX Failed to retrieve customer family info');

      $parentCustomerId = $getFamilyByCustomerIDResult->data_array['parentCustomerId'];
      $payForFamily     = $getFamilyByCustomerIDResult->data_array['payForFamily'];
      $planCredits      = $getFamilyByCustomerIDResult->data_array['planCredits'];
      $members          = $getFamilyByCustomerIDResult->data_array['members'];

      $info['parentCustomerId'] = $parentCustomerId;
      $info['payForFamily']     = $payForFamily;
      $info['planCredits']      = $planCredits;
      $info['memberCount']      = count($members);
      $info['members']          = $members;

      $lineCredits = new \LineCredits();
      $info['customerPlanCredits'] = $lineCredits->getBalance($customerId);
      teldata_change_db();
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
    }

    return $info;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function getOrder($customerId, $status)
  {
    $result = new \Result();

    try
    {
      $onlineSales = new \Ultra\Lib\Services\OnlineSalesAPI();
      $onlineSalesResult = $onlineSales->getRetailerOrders($customerId, $status);

      \dlog('', 'OnlineSales API Result: %s', $onlineSalesResult);

      if ($onlineSalesResult->is_failure())
        throw new \Exception('Error retrieving order');

      return $onlineSalesResult;
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function fulfillOrder($orderId, $itemId)
  {
    $result = new \Result();

    try
    {
      $onlineSales = new \Ultra\Lib\Services\OnlineSalesAPI();
      $onlineSalesResult = $onlineSales->fulfillOrderItem($orderId, $itemId);

      \dlog('', 'OnlineSales API Result: %s', $onlineSalesResult);

      if ($onlineSalesResult->is_failure())
        throw new \Exception('Error marking order item as fulfilled');

      return $onlineSalesResult;
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function createFamily($customerId)
  {
    $result = new \Result();

    try
    {
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $familyApiResult = $familyApi->createCustomerFamily($customerId);

      \dlog('', 'Family API Result: %s', $familyApiResult);

      if ($familyApiResult->is_failure())
        throw new \Exception('Error creating family');

      return $familyApiResult;
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param int
   * @param Result
   **/
  public static function removeFromFamily($customerId)
  {
    $result = new \Result();

    try
    {
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $familyDetailsResult = $familyApi->getFamilyByCustomerID($customerId);

      if ($familyDetailsResult->is_success() && ! empty($familyDetailsResult->data_array['id']))
      {
        $familyId = $familyDetailsResult->data_array['id'];
        $removeFromFamilyResult = $familyApi->removeFamilyMember($familyId, $customerId);

        if ($removeFromFamilyResult->is_failure())
          throw new \Exception("Error removing customer $customerId from family $familyId");
      }

      $result->succeed();
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function getOrCreateFamily($customerId)
  {
    $result = new \Result();

    try
    {
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $familyApiResult = $familyApi->getOrCreateFamilyID($customerId);

      if ($familyApiResult->is_failure()) {
        throw new \Exception('Error creating family');
      }

      return $familyApiResult;
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  public static function addFamilyMemberToBAN($customerID, array $familyMembers)
  {
    $result = new \Result();

    try {
      $flex = (new \Ultra\Container\AppContainer())->make(\Ultra\Plans\Flex::class);
      $redis = new \Ultra\Lib\Util\Redis();

      // check balance of another family member for ADDSHARE data amount
      foreach ($familyMembers as $member) {
        // dont sync to yourself
        if ($member['customerId'] == $customerID)
          continue;

        if ( ! isset($member['plan_state']) || ! isset($member['current_mobile_number']))  {
          $customer = get_ultra_customer_from_customer_id($member['customerId'], ['plan_state', 'current_mobile_number']);
          $member['plan_state'] = $customer->plan_state;
          $member['current_mobile_number'] = $customer->current_mobile_number;
        }

        if ($member['plan_state'] == STATE_ACTIVE) {
          $cb = new \Ultra\Lib\CheckBalance();
          $cbResult = $cb->byMSISDN($member['current_mobile_number']);
          if (isset($cbResult['a_data_pool_1_limit'])) {
            $flex->queueAddFamilyMembertoBanMISO($customerID, $cbResult['a_data_pool_1_limit'], true);

            $result->succeed();
            return $result;
          }
        }
      }

      // read from redis for data amount
      $totalAmount = 0;
      foreach ($familyMembers as $member) {
        $amount = $redis->get(self::REDIS_DATA_AMOUNT_KEY . $member['customerId']);
        if ($amount) {
          $totalAmount += $amount;
        }
      }

      if ($totalAmount > 0) {
        $flex->queueAddFamilyMembertoBanMISO($customerID, $totalAmount, true);
      } else {
        \dlog('', 'ESCALATION FLEX Called addFamilyMemberToBAN with unknown data amount where customer_id = ' . $customerID);
      }

      $result->succeed();
      return $result;
    } catch (\Exception $e) {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function activateFamilyMembers($fromCustomerId, $transition = false)
  {
    $result = new \Result();

    try
    {
      // attempt to activate customer in family
      $activateFamilyMemberResult = \Ultra\Lib\Flex::activateFamilyMember($fromCustomerId);
      if ( ! $activateFamilyMemberResult->is_success())
      {
        foreach ($activateFamilyMemberResult->get_errors() as $error)
          $result->add_error($error);

        throw new \Exception('Failed to activate customer in family');
      }

      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $getFamilyByCustomerIDResult = $familyApi->getFamilyByCustomerID($fromCustomerId);

      if ( ! $getFamilyByCustomerIDResult->is_success())
        throw new \Exception('Failed to retrieve customer family info');

      $parentCustomerId = $getFamilyByCustomerIDResult->data_array['parentCustomerId'];
      $payForFamily     = $getFamilyByCustomerIDResult->data_array['payForFamily'];
      $planCredits      = $getFamilyByCustomerIDResult->data_array['planCredits'];
      $members          = $getFamilyByCustomerIDResult->data_array['members'];

      // $planCreditsRequired = ($transition) ? 1 : 0;

      // is parent logic
      if ($parentCustomerId == $fromCustomerId && $payForFamily && $planCredits > 0)
      {
        foreach ($members as $member)
        {
          \logDebug("Check activation {$member['customerId']}");

          // do not iterate over self
          if ($member['customerId'] == $parentCustomerId)
            continue;

          teldata_change_db();

          $customer = get_ultra_customer_from_customer_id($member['customerId'], ['plan_state','current_mobile_number']);

          // be sure of customer and plan_state
          if ( ! $customer || ! isset($customer->plan_state))
          {
            \dlog('', 'ESCALATION FLEX Error retrieving customer plan state %d', $member['customerId']);
            continue;
          }

          // if not suspended or provisioned, continue
          if ( ! in_array($customer->plan_state, [STATE_SUSPENDED,STATE_PROVISIONED]))
          {
            \dlog('', 'Customer %d not suspended or provisioned', $member['customerId']);
            continue;
          }

          $lineCredits = new \LineCredits();

          // transfer plan credit, fail on errors or warnings
          list ($doTransferResult, $error_code) = $lineCredits->doTransfer($parentCustomerId, $member['customerId'], 1);
          if ( ! $result->is_success())
          {
            $errors = $doTransferResult->get_errors();
            if (count($errors))
              \dlog('', 'ESCALATION FLEX error: %s', $errors[0]);

            $warnings = $doTransferResult->get_warnings();
            if (count($warnings))
              \dlog('', 'warnings: %s', $warnings[0]);
          }
        }
      }

      teldata_change_db();

      // add child to BAN, if needed
      if ($fromCustomerId != $parentCustomerId) {
        $sharedDataAPI = new \Ultra\Lib\Services\SharedData();
        $sharedDataResult = $sharedDataAPI->getBucketIDByCustomerID($fromCustomerId);
        if ($sharedDataResult->is_success() && !empty($sharedDataResult->data_array['bucket_id'])) {
          $addBANResult = self::addFamilyMemberToBAN($fromCustomerId, $members);
        }
      }

      // apply any left over order items
      // TODO: check for other errors besides order not found
      self::applyCustomerOrder($fromCustomerId, true);

      $result->succeed();
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function activateFamilyMember($customerId)
  {
    $result = new \Result();

    try
    {
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $familyApiResult = $familyApi->activateFamilyMember($customerId);

      if ($familyApiResult->is_failure()) {
        throw new \Exception('Error activating family member');
      }

      return $familyApiResult;
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @param  string ie SHAREDDATA_500_5
   * @param  bool whether bolt on will charge balance
   * @return Result
   */
  public static function addBoltOn($customerId, $boltOnId, $charge = FALSE)
  {
    $result = new \Result();

    try
    {
      $source = ($charge) ? 'SPEND' : 'ORDER';

      if ( ! $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo($boltOnId))
        throw new \Exception('bolt on id does not exist');

      list ($error, $code) = \Ultra\Lib\BoltOn\addBoltOnImmediate($customerId, $boltOnInfo, $source, __FUNCTION__, NULL);
      if ( ! empty($error))
        throw new \Exception($error);

      $result->succeed();
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  public static function applyCustomerOrder($customerID, $withBoltOns=true)
  {
    $result = new \Result();

    try
    {
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();
      $getFamilyByCustomerIDResult = $familyApi->getFamilyByCustomerID($customerID);

      if ( ! $getFamilyByCustomerIDResult->is_success())
        throw new \Exception('ESCALATION FLEX Failed to retrieve customer family info');

      $parentCustomerId = $getFamilyByCustomerIDResult->data_array['parentCustomerId'];
      $members = $getFamilyByCustomerIDResult->data_array['members'];

      $getOrderResult = self::getOrder($customerID, 'paid');
      if ( ! $getOrderResult->is_success())
        return make_ok_Result();

      // look for family boltons and plan credits
      $order = $getOrderResult->data_array;
      $familyBoltons = [];
      $otherBoltOns = [];
      $planCredits = [];
      $wallet = [];
      $totalData = 0;
      foreach ($order['items'] as $item) {
        if (isset($item['bolton']) && isset($item['type']) && $item['type'] == 'bolton') {
          // check for shared prefix
          if (strtoupper(substr($item['bolton'], 0, 6)) == 'SHARED') {
            // check amount... only support 1
            if (array_key_exists('amount', $item) && $item['amount'] > 1) {
              throw new \Exception('Invalid amount ' . $item['amount'] . ' for bolt on ' . $item['bolton']);
            } else if ($item['amount'] == 1) {
              $familyBoltons[] = $item;
            }

            // total SHAREDDATA bolt on amounts for ADDSHARE call on children activated via line credits
            if (strtoupper(substr($item['bolton'], 0, 10)) == 'SHAREDDATA') {
              $totalData += explode('_', $item['bolton'])[1];
            }
          } else {
            $otherBoltOns[] = $item;
          }
        } else if (isset($item['type']) && $item['type'] == 'planCredit') {
          $planCredits[] = $item;
        } else if (isset($item['type']) && $item['type'] == 'wallet') {
          $wallet[] = $item;
        } else if (isset($item['type'])) {
          \dlog('', 'ESCALATION FLEX Unhandled order item type ' . $item['type']);
        } else {
          \dlog('', 'ESCALATION FLEX Unexpected order item structure ' . json_encode($item));
        }
      }

      if ($withBoltOns) {
        // apply boltons
        if (count($familyBoltons)) {
          // make sure they have a family
          $createFamilyResult = self::getOrCreateFamily($customerID);
          if ( ! $createFamilyResult->is_success())
            throw new \Exception("Error creating family for customer_id {$customerID}");

          foreach ($familyBoltons as $boltOn) {
            $addBoltOnResult = \Ultra\Lib\Flex::addBoltOn($customerID, $boltOn['bolton']);
            if ( ! $addBoltOnResult->is_success()) {
              throw new \Exception('Error applying ' . $boltOn['bolton'] . ' to customer_id ' . $customerID);
            } else {
              // mark order item fulfilled
              $fulfillOrderResult = \Ultra\Lib\Flex::fulfillOrder($boltOn['orderId'], $boltOn['orderItemId']);
              if (!$fulfillOrderResult->is_success()) {
                throw new \Exception('Failed to mark bolton fulfilled where orderItemId = ' . $boltOn['orderItemId']);
              }
            }
          }
        }

        // cache totalData amount, if needed
        if ($totalData) {
          $redis = new \Ultra\Lib\Util\Redis();
          // increment any existing amount
          $existing = $redis->get(self::REDIS_DATA_AMOUNT_KEY . $customerID);
          if ($existing) {
            $totalData += $existing;
          }

          $redis->set(self::REDIS_DATA_AMOUNT_KEY . $customerID, $totalData, 3600);
        }
      }

      // apply plan credit
      if (count($planCredits)) {
        $lineCredits = new \LineCredits();

        foreach ($planCredits as $planCredit) {
          // do line credit doTransfer
          list ($doIncrementResult, $error_code) = $lineCredits->doIncrement($customerID, $planCredit['amount']);
          if ( ! $doIncrementResult->is_success()) {
            $errors = $doIncrementResult->get_errors();
            throw new \Exception("$errors[0]", $error_code);
          } else {
            // mark order item fulfilled
            $fulfillOrderResult = \Ultra\Lib\Flex::fulfillOrder($planCredit['orderId'], $planCredit['orderItemId']);
            if (!$fulfillOrderResult->is_success()) {
              throw new \Exception('Failed to mark plan credit fulfilled where orderItemId = ' . $planCredit['orderItemId']);
            }
          }
        }

        teldata_change_db();
      }

      // apply wallet items
      if (count($wallet)) {
        foreach ($wallet as $item) {
          if (empty($item['amount'])) {
            \dlog('', 'ESCALATION FLEX Missing amount in wallet order item');
            continue;
          }

          // amount in dollars
          $amount = number_format($item['amount']/100, 2, '.', '');

          // update balance
          $balanceUpdateQuery = accounts_update_query([
            'add_to_balance'          => $amount,
            'customer_id'             => $customerID,
            'last_recharge_date_time' => 'now'
          ]);

          if ( ! is_mssql_successful(logged_mssql_query($balanceUpdateQuery)))
            throw new \Exception('Failed to apply wallet order item');

          // mark order item fulfilled
          $fulfillOrderResult = \Ultra\Lib\Flex::fulfillOrder($item['orderId'], $item['orderItemId']);
          if ( ! $fulfillOrderResult->is_success())
            throw new \Exception('Failed to mark plan credit fulfilled where orderItemId = ' . $item['orderItemId']);
        }
      }

      // apply other bolt ons
      foreach ($otherBoltOns as $boltOn) {
        for ($i = 0; $i < $boltOn['amount']; $i++) {
          $addBoltOnResult = \Ultra\Lib\Flex::addBoltOn($customerID, $boltOn['bolton']);
          if ( ! $addBoltOnResult->is_success()) {
            throw new \Exception('Error applying ' . $boltOn['bolton'] . ' to customer_id ' . $customerID);
          } else {
            // mark order item fulfilled
            $fulfillOrderResult = \Ultra\Lib\Flex::fulfillOrder($boltOn['orderId'], $boltOn['orderItemId']);
            if (!$fulfillOrderResult->is_success()) {
              throw new \Exception('Failed to mark bolton fulfilled where orderItemId = ' . $boltOn['orderItemId']);
            }
          }
        }
      }

      $result->succeed();
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return Result
   */
  public static function getSharedIld($customerId, $info = null)
  {
    $result = new \Result();

    $result->add_to_data_array('sharedIntl', 0);

    try
    {
      if ( ! $info)
      {
        // get basic info
        $info = \Ultra\Lib\Flex::getInfoByCustomerId($customerId);
        if ( ! $info)
          throw Exception('error retieving family info');
      }

      foreach ($info as $key => $val)
        $result->add_to_data_array($key, $val);

      // get shared ild
      $sharedILD = new \Ultra\Lib\Services\SharedILD();
      $getBucketIDResult = $sharedILD->getBucketIDByCustomerID($info['parentCustomerId']);
      if ($getBucketIDResult->is_success())
      {
        $bucketId = $getBucketIDResult->data_array['bucket_id'];
        $account  = find_first("SELECT TOP 1 CUSTOMER_ID,PACKAGED_BALANCE1 FROM ACCOUNTS with (nolock) WHERE ACCOUNT_ID = $bucketId");
        if ($account)
          $result->add_to_data_array('sharedIntl', $account->PACKAGED_BALANCE1);
      }

      $result->succeed();
    }
    catch (\Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * @param  int
   * @return int
   */
  public static function costForCredits($amount)
  {
    return $amount * 1150;
  }
}
