<?php

global $credentials;
require_once 'core.php';
require_once 'cosid_constants.php';
require_once 'db.php';

require_once 'classes/PHPUnitBase.php';
require_once 'classes/VersionOneResult.php';

class VersionOneResultTest extends PHPUnitBase
{
  public function test__full()
  {
    $result = new \VersionOneResult();
    $result->addWarning('test warning');

    $result->addError('test error');
    $result->addErrors(['error one', 'error two']);

    print_r($return = $result->getReturnArray());

    $this->assertFalse($return['success']);

    $this->assertTrue($result->hasErrors());

    $result->clearErrors();

    $this->assertFalse($result->hasErrors());

    $result->succeed();

    print_r($result->getReturnArray());

    $result->addData('key', 'val');
    $result->addDataArray(['a' => 1, 'b' => 2]);

    print_r($return = $result->getReturnArray());

    $this->assertTrue($return['success']);
  }

  public function test__setArgs()
  {}

  public function test__getJSON()
  {}
}