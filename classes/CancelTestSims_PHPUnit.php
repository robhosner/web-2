<?php

require_once 'classes/CancelTestSims.php';

class CancelTestSimsTest extends PHPUnit_Framework_testCase
{
  public function test__isWhiteListed()
  {
    $cancelTestSims = new CancelTestSims();
    
    $bool = $cancelTestSims->isWhiteListed('890126096316366792');
    $this->assertTrue($bool);

    $bool = $cancelTestSims->isWhiteListed('890126096316366791');
    $this->assertFalse($bool);
  }

  public function test__shouldCancel()
  {
    $cancelTestSims = new CancelTestSims();

    $dataBody = [];

    $nDays = \Ultra\UltraConfig\getMintTestSimTTL();
    $planEffectiveLimit = strtotime("-$nDays days", time());

    $dataBody['PlanEffectiveDate'] = date('Y-m-d', time() - ($planEffectiveLimit * 2));
    $bool = $cancelTestSims->shouldCancel($dataBody);
    $this->assertTrue($bool);

    $dataBody['PlanEffectiveDate'] = date('Y-m-d', time() + 84600);
    $bool = $cancelTestSims->shouldCancel($dataBody);
    $this->assertFalse($bool);
  }

  public function test__cancelSimsApi()
  {
    // integration test
    return;

    $iccids = ['101010101315883885'];

    $cancelTestSims = new CancelTestSims();
    $result = $cancelTestSims->cancelSimsApi($iccids);

    $this->assertFalse( ! ! $result->success);
  }

  public function test__querySubscriber()
  {
    // integration test
    return;

    $cancelTestSims = new CancelTestSims();

    $iccid = '890126096315883885';
    $dataBody = $cancelTestSims->querySubscriber($iccid);
    $this->assertEquals($dataBody['ResultCode'], 100);

    $iccid = '101010101315883885';
    $dataBody = $cancelTestSims->querySubscriber($iccid);
    $this->assertEquals($dataBody, NULL);
  }
}