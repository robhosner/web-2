<?php


/* Raphael, the Messenger */
/*
                ______
               '-._   ```"""---.._
            ,-----.:___           `\  ,;;;,
             '-.._     ```"""--.._  |,%%%%%%              _
             ,    '.              `\;;;;  -\      _    _.'/\
           .' `-.__ \            ,;;;;" .__{=====/_)==:_  ||
      ,===/        ```";,,,,,,,;;;;;'`-./.____,'/ /     '.\/
     '---/              ';;;;;;;;'      `--.._.' /
    ,===/                          '-.        `\/
   '---/                            ,'`.        |
      ;                        __.-'    \     ,'
      \______,,.....------'''``          `---`
 
*/
/* MVNO-700 */


use Ultra\Container\AppContainer;
use Ultra\Messaging\FlexMessenger;
use function Ultra\UltraConfig\isFlexPlan;

include_once('classes/Message.php');
include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_messaging_daily_init.php');
include_once('db/ultra_customer_options.php');
include_once('lib/util-common.php');
include_once('Ultra/Lib/Util/Redis/Messaging.php');
require_once 'classes/Session.php';

date_default_timezone_set("America/Los_Angeles");


/**
 * Ultra Messenger class
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/How+it+works+-+Messaging
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra Messaging
 */
class Messenger
{
  private $sleep_delay    = 1;
  private $initialize_day = 0;
  private $max_iterations = 0;
  private $conditional_dispatch_events = array();
  private $redis_messaging    = NULL;
  private $single_customer_id = NULL;

  /**
   * @var FlexMessenger
   */
  private $flexMessenger;

  /**
   * run
   *
   * Main Messenger entry point
   *
   * @return NULL
   */
  public function run()
  {
    \logDebug("Messenger.php::run start ".time());

    if ( $this->initialize_day )
    {
      dlog('',"initializing daily dispatch events");

      $this->initialize_daily_dispatch_events();
      $this->initialize_expiring_credit_card_events();
      $this->initialize_bogo_second_month_events();

      $this->flexMessenger = (new AppContainer())->make(FlexMessenger::class);
      $this->flexMessenger->setMessageApproachingRenewDate();
      $this->flexMessenger->setMessageOnRenewDate();

      $this->reconcile_messages();
    }
    else
    {
      $this->process_message_queue();
    }

    \logDebug("Messenger.php::run end   ".time());
  }

  public function setSingleCustomerId($customer_id)
  {
    $this->single_customer_id = $customer_id;
  }

  protected function getRedisMessaging()
  {
    if ( ! $this->redis_messaging)
      $this->redis_messaging = new \Ultra\Lib\Util\Redis\Messaging();

    return $this->redis_messaging;
  }

  /**
   * process_message_queue
   *
   * Process queue ( $this->max_iterations limits the amount of message processed )
   *
   * @return NULL
   */
  public function process_message_queue()
  {
    $iteration_count = 1;

    while(
      ( ! $this->max_iterations )
      ||
      ( $iteration_count <= $this->max_iterations )
    )
    {
      \logTrace( "Messenger.php::run iteration # $iteration_count" );

      // Retrieves and reserves a message object
      $message = get_next_message_to_send();

      if ( $message )
      {
        \logTrace("Got message ".json_encode($message));

        if ( $this->check_message_condition($message) )
          $result = $message->dispatch();
        else
          $result = $message->skip_dispatch();

        if ( $result )
        {
// TODO: housekeeping ?
        }
        else
        { sleep($this->sleep_delay); }
      }
      else
      { sleep($this->sleep_delay); }

      $iteration_count++;
    }
  }

  /**
   * initialize_expiring_credit_card_events
   *
   * Initializes message events related to expiring credit cards
   *
   * @return NULL
   */
  public function initialize_expiring_credit_card_events()
  {
    if (1 == date('j')) // day of expiration ( first of month )
    {
      $customers = get_cc_info_expiring_this_month();
      $reason = 'credit_card_expired';

      foreach ($customers as $customer)
        $this->enqueueMessage($customer->CUSTOMER_ID, $reason);

      $customers = get_cc_info_expiring_next_month();
      $reason = 'credit_card_close_to_expiration';

      foreach ($customers as $customer)
        $this->enqueueMessage($customer->CUSTOMER_ID, $reason);
    }
  }

  /**
   * initialize_bogo_second_month_events
   *
   *  Enqueues message for second month BOGO customers close to expiration
   *
   * @return NULL
   */
  private function initialize_bogo_second_month_events()
  {
    $customer_ids = bogo_second_month_expiring_customer_ids();

    foreach ($customer_ids as $customer_id)
    {
      $reason = '7_11_bogo_close_to_expiration';
      $result = $this->enqueueMessage($customer_id, $reason);
      if ( ! $result->is_success())
      {
        dlog('',"Failure enqueueing $reason for customer_id : $customer_id");
      }
    }
  }


  /**
   * enqueueMessage
   *
   * Enqueues a message with $reason to HTT_MESSAGING_QUEUE
   *
   * @param  Integer $customer_id
   * @param  String  $reason
   * @return Result
   */
  private function enqueueMessage($customer_id, $reason)
  {
    list($delivery_datetime, $delivery_timestamp) = compute_delivery_daytime(
      time(),
      get_customer_time_zone($customer_id),
      extrapolate_message_priority($reason)
    );

    $result = htt_messaging_queue_add(
      array(
        'customer_id'                => $customer_id,
        'reason'                     => $reason,
        'expected_delivery_datetime' => $delivery_datetime,
        'next_attempt_datetime'      => $delivery_datetime,
        'next_attempt_timestamp'     => $delivery_timestamp,
        'type'                       => 'SMS_INTERNAL',
        'conditional'                => 0,
        'messaging_queue_params'     => array('message_type' => $reason)
      ),
      $this->getRedisMessaging()
    );

    return $result;
  }

  /**
   * initialize_daily_dispatch_events
   *
   * The Messaging routine has to be invoked once per day with this modality.
   * It will initialize 'conditional events' like
   *  - customer has X days until plan expires - the plan has not been pre-paid yet
   *  - customer has not taken advantage of international calling credit
   *
   * @return NULL
   */
  private function initialize_daily_dispatch_events()
  {
    foreach( $this->conditional_dispatch_events as $event => $event_config )
    {
      if ( $event_config['initialized'] )
      {
        $this->initialize_daily_dispatch_event($event,$event_config);
      }
    }
  }

  /**
   * initialize_daily_dispatch_event
   *
   * Initialize one kind of dispatch event
   *
   * @return NULL
   */
  private function initialize_daily_dispatch_event($event,$event_config)
  {
    \logInfo("initialize_daily_dispatch_event [ $event ] , conditions = ".json_encode($event_config['conditions'])." , message_params = ".json_encode($event_config['message_params']));

    $sql = make_find_ultra_customer_query_from_conditions($event_config['conditions']);

    $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

    if ( $query_result && is_array($query_result) )
    {
      # used to dilute initialized events messages in time
      $seconds_delay = 0;

      $redis_messaging = $this->getRedisMessaging();

      // $row = [customer_id, a.cos_id, renewal_date, plan_amount, plan_name, msisdn, monthly_renewal_target, brand_id]
      foreach( $query_result as $i => $row )
      {
        // skip the rest of the customers if we want to test a single customer
        if ($this->single_customer_id && $row[0] != $this->single_customer_id)
          continue;

        $target_cos_id = ( ! empty($row[6]) ) ? get_cos_id_from_plan($row[6]) : NULL;
        $plan_costs    = get_plan_costs_by_customer_id($row[0], $target_cos_id);

        // if monthly renewal target is set and is expiration message, check if customer has enough funds to cover
        if ($target_cos_id && in_array($event, array('plan_expires_in_5_days','plan_expires_in_2_days','plan_expires_today')))
        {
          if ($this->should_skip_renewal_message($row[0], $plan_costs['plan']))
            continue;
        }

        // if is mint customer with MONTHS REMAINING, skip customer
        if ($this->should_skip_mint_customer($row[0], $row[7]))
          continue;

        $customer_data = array(
          'customer_id'    => $row[0],
          'cos_id'         => $row[1],
          '_renewal_date_' => fix_renewal_date($row[2]),
          '_plan_amount_'  => $plan_costs['plan'] + get_bolt_ons_costs_by_customer_id($row[0]),
          '_personal_'     => ($plan_costs['personal']) ? ($plan_costs['personal']) : '',
          '_plan_name_'    => $row[4],
          '_login_token_'  => Session::encryptToken($row[5], Session::TOKEN_V3, array(Session::SYSTEM_TOKEN))

          // more fields will appear here, all fields which could be used in SMS templates

        );

        \logTrace("initialize_daily_dispatch_event customer_data = ".json_encode($customer_data));

        $message_params = fill_message_params_values($customer_data,$event_config['message_params']);

        if (isFlexPlan($customer_data['cos_id'])) {
          if ($message_params['message_type'] == 'plan_suspended_two_days_ago') {
            $message_params['message_type'] = 'flex_plan_suspended_two_days_ago';
          } elseif (
            $message_params['message_type'] == 'plan_close_to_expiration' ||
            $message_params['message_type'] == 'plan_expires_today' ||
            $message_params['message_type'] == 'auto_recharge_in_2_days'
          ) {
            continue;
          }
        }

        \logTrace("initialize_daily_dispatch_event customer message_params = ".json_encode($message_params));

        // compute proper delivery time
        list($delivery_datetime, $delivery_timestamp) = compute_delivery_daytime(time(), get_customer_time_zone($customer_data['customer_id']), extrapolate_message_priority($event));

        $result = htt_messaging_queue_add(
          array(
            'customer_id'                 => $customer_data['customer_id'],
            'reason'                      => $event,
            'expected_delivery_datetime'  => $delivery_datetime,
            'next_attempt_datetime'       => $delivery_datetime,
            'next_attempt_timestamp'      => $delivery_timestamp,
            'type'                        => 'SMS_INTERNAL',
            'conditional'                 => 1,
            'messaging_queue_params'      => $message_params
          ),
          $redis_messaging
        );

        if ( $result->is_success() )
          \logTrace("initialize_daily_dispatch_event added message in queue for customer_id ".$customer_data['customer_id']);
        else
          \logError("ERROR: initialize_daily_dispatch_event could not add message in queue for customer_id ".$customer_data['customer_id']);

        $seconds_delay += 2; # increment by 2 seconds for each conditional message
      }
    }
    else
      \logWarn("initialize_daily_dispatch_event found no customers for event $event");
  }

  /**
   * should_skip_renewal_message
   *
   * check plan_cost against stored_value and balance
   * to decide if customer can cover cost of plan
   *
   * @param  Integer $customer_id
   * @param  Integer $plan_cost to check against
   * @return Boolean
   */
  public function should_skip_renewal_message($customer_id, $plan_cost)
  {
    $result = FALSE;

    $customer = get_ultra_customer_from_customer_id($customer_id, array('STORED_VALUE'));
    if ($customer->STORED_VALUE > $plan_cost)
      $result = TRUE;

    $account  = get_account_from_customer_id($customer_id, array('BALANCE'));
    if ($account->BALANCE > $plan_cost)
      $result = TRUE;

    if ($result)
      \logDebug("SKIPPING: CUSTOMER_ID $customer_id has enough funds to cover monthly renewal target");

    return $result;
  }

  /**
   * should_skip_mint_customer
   *
   * @param  Integer $customer_id
   * @return Boolean
   */
  public function should_skip_mint_customer($customer_id, $brand_id)
  {
    $result = FALSE;

    // if MINT brand, check multi_month_overlay
    if (\Ultra\Lib\Util\validateMintBrandId($brand_id))
    {
      $overlay = ultra_multi_month_overlay_from_customer_id($customer_id);
      if ($overlay->MONTHS_REMAINING > 0)
        $result = TRUE;
    }

    if ($result)
      \logDebug("SKIPPING: CUSTOMER_ID $customer_id is MINT customer with MONTHS REMAINING");

    return $result;
  }

  /**
   * Class constructor
   */
  public function __construct($options=NULL)
  {
    $this->conditional_dispatch_events = conditional_dispatch_events();

    if ( $options )
    {
      if ( is_array($options) )
      {
        if ( isset($options['initialize_day']) && $options['initialize_day'] )
          $this->initialize_day = 1;

        if ( isset($options['max_iterations']) && $options['max_iterations'] )
          $this->max_iterations = $options['max_iterations'];

        if ( ! empty($options['single_customer_id']))
        {
          $this->setSingleCustomerId($options['single_customer_id']);
          dlog('', 'setting single customer id ' . $this->single_customer_id);
        }
      }
    }
    elseif ( $options === 'initialize_day' )
    {
      $this->initialize_day = 1;
    }
  }

  /**
   * should_skip_promo_message
   *
   * Decide if we should skip a specific messages for demo/promo accounts
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/Customer+Options+Modifier
   *
   * @return boolean - TRUE if we shound NOT send the message
   */
  public function should_skip_promo_message( $customer_id , $reason )
  {
    $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer_id );

    if ( $ultra_customer_options && is_array($ultra_customer_options) && count($ultra_customer_options) )
    {
      dlog('',"ultra_customer_options = %s",$ultra_customer_options);

      // BILLING.MRC_MULTI_MONTH
      if ( in_array( BILLING_OPTION_MULTI_MONTH , $ultra_customer_options )
        && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) )
      )
      {
        dlog('', BILLING_OPTION_MULTI_MONTH . " matched with %s for customer_id %d", $reason, $customer_id);
        return TRUE;
      }

      // BILLING.MRC_DEALER_PROMO
      if ( in_array( BILLING_OPTION_DEMO_LINE , $ultra_customer_options )
        && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) )
      )
      {
        dlog('', BILLING_OPTION_DEMO_LINE . " matched with $reason for customer_id $customer_id");
        return TRUE;
      }

      // BILLING.MRC_BOGO_MONTH
      if ( in_array( BILLING_OPTION_ATTRIBUTE_BOGO_MONTH , $ultra_customer_options )
        && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) )
      )
      {
        dlog('', BILLING_OPTION_ATTRIBUTE_BOGO_MONTH . " matched with $reason for customer_id $customer_id");
        return TRUE;
      }

      // 7 11 BOGO
      if ( in_array( BILLING_OPTION_ATTRIBUTE_7_11_BOGO , $ultra_customer_options )
        && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) )
      )
      {
        dlog('', BILLING_OPTION_ATTRIBUTE_7_11_BOGO . " matched with $reason for customer_id $customer_id");
        return TRUE;
      }

      // 7 11
      if ( in_array( BILLING_OPTION_ATTRIBUTE_7_11 , $ultra_customer_options )
        && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) )
      )
      {
        dlog('', BILLING_OPTION_ATTRIBUTE_7_11 . " matched with $reason for customer_id $customer_id");
        return TRUE;
      }

      // house accounts
      if ( in_array( MRC_HOUSE_ACCOUNT , $ultra_customer_options )
        && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) )
      )
      {
        dlog('', MRC_HOUSE_ACCOUNT . " matched with $reason for customer_id $customer_id");
        return TRUE;
      }

      // promo plans
      $index = array_search( BILLING_OPTION_PROMO_PLAN , $ultra_customer_options );

      if ( is_numeric($index) )
      {
        dlog('',$ultra_customer_options[ $index ] . ' => ' . $ultra_customer_options[ $index+1 ]);

        $values = explode(',',$ultra_customer_options[ $index+1 ]);

        // are there months left in this promo plan ?
        if ( ( $values[2] > 0 ) && in_array( $reason , array( 'plan_expires_today' , 'plan_expires_in_2_days', 'plan_expires_in_5_days' ) ) )
        {
          dlog('', BILLING_OPTION_PROMO_PLAN . " matched with $reason for customer_id $customer_id");

          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * check_message_condition
   *
   * If HTT_MESSAGING_QUEUE.CONDITIONAL is 1 we have to check the message conditions
   *
   * @return boolean - FALSE if we shound not send the message
   */
  private function check_message_condition($message)
  {
    // AMDOCS-485: check if customer has ported out after message was created

    if ( empty( $message->data_array['customer']->current_mobile_number ) )
    {
      dlog('', "CID {$message->data_array['CUSTOMER_ID']} is missing MSISDN");
      return FALSE;
    }

    if ( strlen($message->data_array['customer']->current_mobile_number) != 10 )
    {
      dlog('', "CID {$message->data_array['CUSTOMER_ID']} is associated with an invalid MSISDN");
      return FALSE;
    }

    // if HTT_MESSAGING_QUEUE.CONDITIONAL is 1 we have to check the message conditions

    $check_message_condition = TRUE;

    if ( $message->data_array['CONDITIONAL'] == 1 )
    {
      // skip some specific messages for demo/promo accounts
      if ( $this->should_skip_promo_message( $message->data_array['CUSTOMER_ID'] , $message->data_array['REASON'] ) )
        return FALSE;

      if ( isset( $this->conditional_dispatch_events[ $message->data_array['REASON'] ] ) )
      {
        $sql = make_find_ultra_customer_query_from_conditions(
          $this->conditional_dispatch_events[ $message->data_array['REASON'] ]['conditions'],
          $message->data_array['CUSTOMER_ID']
        );

        $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

        $check_message_condition = ! ! ( $query_result && is_array($query_result) );
      }
      else
      {
        dlog('',"There is no conditional event definition for <".$message->data_array['REASON'].">");

        $check_message_condition = FALSE;
      }
    }

    return $check_message_condition;
  }


  /**
   * reconcile_messages
   * reconcide pending messages in DB and redis to prevent lost messages in case of intrastructure problems
   * we typically lose messages in redis, so reconcile DB->redis only, not the other way around
   */
  public function reconcile_messages()
  {
    // initialize
    date_default_timezone_set('UTC'); // DB dates are UTC
    $overdue = time() - 60 * 60 * 24 * 2; // 2 days
    $redis = new \Ultra\Lib\Util\Redis\Messaging();

    // get all pending overdue messages from database
    $sql = htt_messaging_queue_select_query(array('status' => 'TO_SEND', 'next_attempt_datetime_lt' => 'getutcdate()'));
    $messages = mssql_fetch_all_objects(logged_mssql_query($sql));
    \logDebug('found ' . count($messages) . ' pending messages');

    if (count($messages))
      foreach ($messages as $message)
      {
        $priority = extrapolate_message_priority($message->REASON);

        // skip overdue messages
        if (strtotime($message->NEXT_ATTEMPT_DATETIME) < $overdue)
        {
          dlog('', "message {$message->HTT_MESSAGING_QUEUE_ID} is past due, skipping");

          // remove from database
          $sql = htt_messaging_queue_update_query(array(
            'htt_messaging_queue_id' => $message->HTT_MESSAGING_QUEUE_ID,
            'status'                 => 'SKIPPED',
            'attempt_log'            => $message->ATTEMPT_LOG . ' [' . date('Y-m-d H:i:s') . ' ' . getmypid() . ' skipped ]',
            'next_attempt_datetime'  => 'NULL'));
          run_sql_and_check_result($sql);

          // remove from redis
          $redis->freeMessageId($message->HTT_MESSAGING_QUEUE_ID, $priority);
        }
        else
        {
          // check if message is aready in redis queue and add it if missing
          if (! $redis->getMessageTimestamp($message->HTT_MESSAGING_QUEUE_ID, $priority))
          {
            dlog('', "message {$message->HTT_MESSAGING_QUEUE_ID} is missing, adding to redis");
            $redis->addMessageId($message->HTT_MESSAGING_QUEUE_ID, $priority, strtotime($message->NEXT_ATTEMPT_DATETIME));
          }
        }
      }
  }
}

/**
 * conditional_dispatch_events
 *
 * Static definition of conditional SMS events
 *
 * those conditions will be checked twice:
 *   - when we record the event in HTT_MESSAGING_QUEUE
 *   - just before we send the message
 * if the condition is not met just before we send the message, the STATUS will be set to SKIPPED
 *
 * currently conditional_dispatch_events are only for TYPE = SMS_INTERNAL
 *
 * if 'initialized' is TRUE, we will be inserting the event in HTT_MESSAGING_QUEUE from runners/the-messenger.php which uses this PHP module
 * if 'initialized' is FALSE, we will be inserting the event in HTT_MESSAGING_QUEUE from anywhere else (example: the state machine) using enqueue_conditional_sms
 *
 * @return array
 */
function conditional_dispatch_events()
{
  $conditional_dispatch_events = array(

    'auto_recharge_in_2_days' => array(
      'initialized'    => TRUE,
      'conditions'     => array(
        'customer_balance_lt'              => '_renewal_cost_',
        'customer_state'                   => 'Active',
        'customer_monthly_cc_renewal'      => '1',
        'customer_plan_expires_dd_mm_yyyy' => date("d-m-20y", mktime(0, 0, 0, date("m"), date("d")+2, date("Y")))
      ),
      'message_params' => array(
        'message_type'  => 'auto_monthly_cc_renewal'
      )
    ),

    'plan_expires_in_5_days' => array(
      'initialized'    => TRUE,
      'conditions'     => array(
        'customer_stored_value_lt'         => '_renewal_cost_',
        'customer_balance_lt'              => '_renewal_cost_',
        'customer_state'                   => 'Active',
        'customer_monthly_cc_renewal'      => '0',
        'customer_plan_expires_dd_mm_yyyy' => date("d-m-20y", mktime(0, 0, 0, date("m"), date("d")+5, date("Y")))
      ),
      'message_params' => array(
        'message_type'  => 'plan_close_to_expiration',
        'plan_amount'   => '_plan_amount_',
        'personal'      => '_personal_',
        'renewal_date'  => '_renewal_date_',
        'login_token'   => '_login_token_'
      )
    ),

    'plan_expires_in_2_days' => array(
      'initialized'    => TRUE,
      'conditions'     => array(
        'customer_stored_value_lt'         => '_renewal_cost_',
        'customer_balance_lt'              => '_renewal_cost_',
        'customer_state'                   => 'Active',
        'customer_monthly_cc_renewal'      => '0',
        'customer_plan_expires_dd_mm_yyyy' => date("d-m-20y", mktime(0, 0, 0, date("m"), date("d")+2, date("Y")))
      ),
      'message_params' => array(
        'message_type'  => 'plan_close_to_expiration',
        'plan_amount'   => '_plan_amount_',
        'personal'      => '_personal_',
        'renewal_date'  => '_renewal_date_',
        'login_token'   => '_login_token_'
      )
    ),

    'plan_expires_today' => array(
      'initialized'    => TRUE,
      'conditions'     => array(
        'customer_stored_value_lt'         => '_renewal_cost_',
        'customer_balance_lt'              => '_renewal_cost_',
        'customer_state'                   => 'Active',
        'customer_monthly_cc_renewal'      => '0',
        'customer_plan_expires_dd_mm_yyyy' => date("d-m-20y", mktime(0, 0, 0, date("m"), date("d")+1, date("Y")))
      ),
      'message_params' => array(
        'message_type'  => 'plan_expires_today',
        'plan_amount'   => '_plan_amount_',
        'login_token'   => '_login_token_'
      )
    ),

    'plan_suspended_two_days_ago' => array(
      'initialized'    => TRUE,
      'conditions'     => array(
        'customer_state'                   => 'Suspended',
        'customer_transition_to'           => 'Suspended',
        'customer_transition_dd_mm_yyyy'   => date("d-m-20y", mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))
      ),
      'message_params' => array(
        'message_type'  => 'plan_suspended_two_days_ago',
        'plan_amount'   => '_plan_amount_',
        'login_token'   => '_login_token_'
      )
    ),

    // this has to be added interactively when the customer transitions from Active to Suspended, (with a delay, to give the transition the time to resolve)
    // therefore it's not initialized with this script

    'plan_suspended_now' => array(
      'initialized'    => FALSE,
      'conditions'     => array(
        'customer_state'                   => 'Suspended',
        'customer_transition_to'           => 'Suspended',
        'customer_transition_dd_mm_yyyy'   => date("d-m-20y", mktime(0, 0, 0, date("m"), date("d"), date("Y")))
      ),
      'message_params' => array(
        'message_type'  => 'plan_suspended_now',
        'login_token'   => '_login_token_'
      )
    ),

    // this has to be added interactively when the customer transitions to Provisioned, (with a delay)
    'plan_provisioned_reminder' => array(
      'initialized'    => FALSE,
      'conditions'     => array(
        'customer_state'                   => 'Provisioned'
      ),
      'message_params' => array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => '_plan_amount_'
      )
    )

  );

  return $conditional_dispatch_events;
}

/**
 * fill_message_params_values
 *
 * Fills message parameters array
 *
 * @return array
 */
function fill_message_params_values( $customer_data , $event_config_message_params )
{
  $message_params = array();

  foreach( $event_config_message_params as $par => $val )
  {
    $message_params[ $par ] = ( isset($customer_data[$val]) ) ? $customer_data[$val] : $val ;
  }

  return $message_params;
}

/**
 * enabled_daily_init
 *
 * If TRUE, the messaging daemon will initialize daily events in HTT_MESSAGING_QUEUE
 * this has to happen exactly once per day
 *
 * @return boolean
 */
function enabled_daily_init()
{
  $enabled_daily_init = FALSE;

  $htt_messaging_last_init = htt_messaging_last_init();

  if ( $htt_messaging_last_init )
  {
    \logDebug("enabled_daily_init last_init = $htt_messaging_last_init");

    # at least 22 hours ago + between 9 AM and 17 PM UTC (1/2 AM - 9/10 AM PST/PDT)
    date_default_timezone_set('UTC');
    $hh = date("H");

    if ( ( $htt_messaging_last_init > 22 ) && ( $hh > 8 ) && ( $hh < 18 ) )
    {
      $check = htt_messaging_daily_init_update();

      if ( $check )
        $enabled_daily_init = TRUE;
      else
        \logError('enabled_daily_init ERROR after htt_messaging_daily_init_update');
    }
  }
  else
  {
    // no previous messaging daemon ever run

    $check = htt_messaging_daily_init_insert();

    if ( ! $check ) 
      \logError('enabled_daily_init ERROR after htt_messaging_last_init');
  }

  \logDebug("enabled_daily_init ".( $enabled_daily_init ? 'TRUE' : 'FALSE' ));

  return $enabled_daily_init;
}

