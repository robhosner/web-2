<?php

require_once 'db.php';
require_once 'classes/LineCredits.php';

class LineCredits_test
{
  public function run()
  {
    try
    {
      // these tests run against DB
      if (0)
      {
        $this->getLineCreditsCost();
        $this->hasLineCredit();
        $this->getBalance();
        $this->transferLineCredits();
        $this->recordTransferHistory();
        $this->recordUsageHistory();
        $this->recordPurchaseHistory();
      }

      // these tests run against ultra data
      if (0)
      {
        $this->checkShouldActivate();
      }
    }
    catch (Exception $e)
    {
      echo "ERRORS\n------";
      echo PHP_EOL . $e->getMessage() . PHP_EOL;
    }
  }

  public function getBalance()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();

    $customer_id = 1;
    $balance = $lineCredits->getBalance($customer_id);
    if ( ! $balance || $balance < 1)
      throw new Exception(__function__ . " customer_id $customer_id has no credits");
  }

  public function getLineCreditsCost()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();
    $costs = $lineCredits->getCosts();

    if ( ! count($costs))
      throw new Exception(__function__ . ' Error retrieving costs');

    if ( ! isset($costs[0]->LineCreditCostID))
      throw new Exception(__function__ . ' Data consistency error');
  }

  public function hasLineCredit()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();

    //
    $customer_id = 1;
    if ( ! $lineCredits->hasLineCredit($customer_id))
      throw new Exception(__function__ . " customer_id $customer_id should have credits");

    //
    $customer_id = 3;
    if ($lineCredits->hasLineCredit($customer_id))
      throw new Exception(__function__ . " customer_id $customer_id should have no credits");
  }

  public function transferLineCredits()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();

    //
    $from_customer_id = 2;
    $to_customer_id   = 1;

    $from_balance = $lineCredits->getBalance($from_customer_id);
    $to_balance   = $lineCredits->getBalance($to_customer_id);

    $result = $lineCredits->transferLineCredits($from_customer_id, $to_customer_id, 1);
    if ( ! $result->is_success())
      throw new Exception(__function__ . " Error transferring line redits from $from_customer_id to $to_customer_id");

    if ($from_balance - 1 != $lineCredits->getBalance($from_customer_id))
      throw new Exception(__function__ . " customer_id $from_customer_id should have 1 less credit");

    if ($to_balance + 1 != $lineCredits->getBalance($to_customer_id))
      throw new Exception(__function__ . " customer_id $to_customer_id should have 1 more credit");

    $result = $lineCredits->transferLineCredits($to_customer_id, $from_customer_id, 1);
    if ( ! $result->is_success())
      throw new Exception(__function__ . " Error transferring line redits from $from_customer_id to $to_customer_id");
  }

  public function recordTransferHistory()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();

    $from_customer_id = 2;
    $to_customer_id   = 1;

    $result = $lineCredits->recordTransferHistory($from_customer_id, $to_customer_id, 1);
    if ( ! isset($result->data_array['last_insert_id']))
      throw new Exception(__function__ . ' Error recording to transfer history');
  }

  public function recordUsageHistory()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();

    $from_customer_id = 2;
    $payer_customer_id = 1;

    $result = $lineCredits->recordUsageHistory($from_customer_id, $payer_customer_id, 1);
    if ( ! isset($result->data_array['last_insert_id']))
      throw new Exception(__function__ . ' Error recording to usage history');
  }

  public function recordPurchaseHistory()
  {
    $lineCredits = new \LineCredits();
    $lineCredits->dbConnect();

    $customer_id = 1;
    $amount      = 1;
    $type        = LINE_CREDIT_REGULAR;

    $result = $lineCredits->recordPurchaseHistory($customer_id, $amount, $type);
    if ( ! isset($result->data_array['last_insert_id']))
      throw new Exception(__function__ . ' Error recording to purchase history');
  }

  public function checkShouldActivate()
  {
    $lineCredits = new \LineCredits();

    // old provisioned
    $customer_id = 3322;
    $check = $lineCredits->checkShouldActivate($customer_id);
    if ( ! $check)
      throw new Exception(__function__ . " customer_id $customer_id should be good to activate");

    $customer_id = 23;
    $check = $lineCredits->checkShouldActivate($customer_id);
    if ($check)
      throw new Exception(__function__ . " customer_id $customer_id should not be good to activate");
  }
}

$tests = new LineCredits_test();
$tests->run();
