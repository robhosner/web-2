<?php

// working directory of this script
define ('PROMO_BROADCAST_WORK_DIRECTORY', '/var/tmp/Ultra/TMOWatcher/');
define ('PID_DIRECTORY',                  '/var/tmp/Ultra/TMOWatcher/pid/');

/**
 * main class to perform this task
 */
class TMOWatcher
{
  protected $redis;

  public function __construct()
  {
  }

  /**
   * processNextFile
   *
   * Process the next available file
   *
   * @return boolean
   */
  public function processNextFile( $fileName=NULL )
  {
    if ( $this->pidLimitReached() )
    {
      dlog('', 'pid limit reached');
      return FALSE;
    }

    if ( empty( $fileName ) )
      $fileName = $this->getNextFile();

    if ( empty( $fileName ) )
      dlog('',"no file to process");
    else
    {
      if ( ! $this->addPidFile() )
        return FALSE;

      $result = $this->processFile( $fileName );

      $this->removePidFile();

      return $result;
    }

    return FALSE;
  }

  /**
   * pidLimitReached
   *
   * return IF pid limit has been reached
   * 
   * returns boolean
   */
  public function pidLimitReached()
  {
    $pid_count = (count(scandir( PID_DIRECTORY )) - 2); // -2 for . && ..

    return ( $pid_count >= $this->getPidLimit() );
  }

  /**
   * getPidLimit
   *
   * returns the current pid limit
   * 
   * return integer $pid_limit
   */
  public function getPidLimit()
  {
    $pid_limit = \Ultra\UltraConfig\tmowatcher_pidlimit();

    if ( is_null($pid_limit) || ! is_integer($pid_limit) || $pid_limit == 0 )
      return 4;
    else
      return $pid_limit;
  }

  /**
   * addPidFile
   *
   * Add a PID file
   *
   * @return boolean
   */
  public function addPidFile()
  {
    $pidFile = PID_DIRECTORY . getmypid() ;

    dlog('',"PID file = $pidFile");

    $error = FALSE;

    if ( file_exists( $pidFile ) )
    {
      dlog('',"File $pidFile already exists.");

      return FALSE;
    }

    if ( ! $fileHandle = fopen( $pidFile , 'w' ) )
    {
      dlog('',"Cannot open file $pidFile for writing");

      return FALSE;
    }

    if ( ! fwrite( $fileHandle , timestamp_to_date( time() , MSSQL_DATE_FORMAT ) ) )
      $error = TRUE;

    fclose( $fileHandle );

    return ! $error;
  }

  /**
   * removePidFile
   *
   * Remove PID file
   *
   * @return NULL
   */
  public function removePidFile()
  {
    $pidFile = PID_DIRECTORY . getmypid() ;

    if ( ! unlink( $pidFile ) )
      dlog('',"Cannot remove PID file");
  }

  /**
   * processFile
   *
   * Process the given file
   *
   * @return boolean
   */
  public function processFile( $fileName )
  {
    dlog('',"processing file $fileName");

    if ( ! file_exists( $fileName ) )
    {
      dlog('',"file $fileName does not exist");

      return FALSE;
    }

    $customers_list = file( $fileName );

    if ( ! $customers_list || ! is_array( $customers_list ) )
    {
      dlog('',"could not load file $fileName");

      return FALSE;
    }

    if ( ! count( $customers_list ) )
    {
      dlog('',"File $fileName is empty");

      return FALSE;
    }

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    foreach( $customers_list as $customer_id )
    {
      if ( ! empty( $customer_id ) )
      {
        $customer_id = trim($customer_id);

        // get MSISDN and ICCID
        list( $msisdn , $iccid ) = $this->getCustomerData( $customer_id );

        // invoke GetMVNEDetails
        $this->getMVNEDetails( $mwControl , $customer_id , $msisdn , $iccid , $fileName );
      }
    }

    return TRUE;
  }

  /**
   * getMVNEDetails
   *
   * Invokes mwGetMVNEDetails
   *
   * @return NULL
   */
  public function getMVNEDetails( $mwControl , $customer_id , $msisdn , $iccid , $fileName )
  {
    $result = $mwControl->mwGetMVNEDetails(
      array(
        'actionUUID' => $this->getActionUUID( $customer_id , $fileName ),
        'msisdn'     => $msisdn,
        'iccid'      => $iccid
      )
    );

    dlog('',"result = %s",$result);
  }

  /**
   * getActionUUID
   *
   * @return string
   */
  public function getActionUUID( $customer_id , $fileName )
  {
    $path = explode('/',$fileName);

    return 'GetMVNEDetails-'.date("Y-m-d").'-'.$customer_id.'-'.$path[ count($path) - 1 ];
  }

  /**
   * getCustomerData
   *
   * @return array
   */
  public function getCustomerData( $customer_id )
  {
    $customer_data = mssql_fetch_all_rows(
      logged_mssql_query(
        \Ultra\Lib\DB\makeSelectQuery(
          'HTT_CUSTOMERS_OVERLAY_ULTRA',
          NULL,
          array( 'CURRENT_MOBILE_NUMBER' , 'CURRENT_ICCID_FULL' ),
          array( 'CUSTOMER_ID' => $customer_id )
        )
      )
    );

    dlog('',"customer_data = %s",$customer_data);

    if ( $customer_data && is_array( $customer_data ) && count( $customer_data ) )
      return array( $customer_data[0][0] , $customer_data[0][1] );

    return array( NULL , NULL );
  }

  /**
   * getNextFile
   *
   * Get the next file to process
   *
   * @return string
   */
  public function getNextFile()
  {
    $fileName = NULL;

    $files = scandir( PROMO_BROADCAST_WORK_DIRECTORY );

    dlog('',"files = %s",$files);

    foreach( $files as $file )
      // is this a csv file?
      if ( preg_match("/\.csv$/", $file, $matches) && empty( $fileName ) )
        // was the file processed in the last 12 hours?
        if ( ! $this->alreadyProcessedRecently( $file ) )
          // reserve file for this process
          if ( $this->reserveFile( $file ) )
            // this is the file we will process
            $fileName = PROMO_BROADCAST_WORK_DIRECTORY . $file;

    return $fileName;
  }
 
  /**
   * alreadyProcessedRecently
   *
   * Verify if the file has been already processed in the last 12 hours
   *
   * @return boolean
   */
  public function alreadyProcessedRecently( $fileName )
  {
    $timestampFileName = PROMO_BROADCAST_WORK_DIRECTORY . $fileName . '.ts' ;

    dlog('',"timestampFileName = $timestampFileName");

    if ( ! file_exists( $timestampFileName ) )
      return FALSE;

    $ts = file($timestampFileName);

    dlog('',"ts = %s",$ts);

    $ts = trim($ts[0]);

    dlog('',"ts = %s",$ts);

    dlog('',"$fileName has been processed %s seconds ago",( time() - $ts ) );

    if ( time() - 12*60*60 < $ts )
    {
      dlog('',"$fileName has been processed too recently");
      return TRUE;
    }

    return FALSE;
  }

  /**
   * reserveFile
   *
   * Reserve this file so that the current process will be the only one processing it
   *
   * @return boolean
   */
  public function reserveFile( $fileName )
  {
    $timestampFileName = PROMO_BROADCAST_WORK_DIRECTORY . $fileName . '.ts' ;

    dlog('',"timestampFileName = $timestampFileName");

    $error = FALSE;

    if ( ! $fileHandle = fopen( $timestampFileName , 'w' ) )
    {
      dlog('',"Cannot open file $timestampFileName for writing");

      return FALSE;
    }

    if ( ! fwrite( $fileHandle , time() ) )
      $error = TRUE;

    fclose( $fileHandle );

    return ! $error;
  }
}
