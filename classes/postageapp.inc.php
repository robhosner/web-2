<?php
include_once 'lib/util-common.php';

 if(!defined('POSTAGE_HOSTNAME')) define ('POSTAGE_HOSTNAME', 'https://api.postageapp.com');
 if(!defined('POSTAGE_API_KEY_PAYG')) define ('POSTAGE_API_KEY_PAYG', 'n64rdyUDkyUXOoWdkjH3kr2fEJutKFOm');
 if(!defined('POSTAGE_API_KEY_TTO')) define ('POSTAGE_API_KEY_TTO', 'W3jbq7yCpMYVYAwSGOGYgGYxGKcjgNrE');
 if(!defined('POSTAGE_API_KEY_INTERNAL')) define ('POSTAGE_API_KEY_INTERNAL', 'R2CCRDkeJvyG4538FzRQM9W4Oo3gIU74');
 if(!defined('POSTAGE_API_KEY_RESTAURANT')) define ('POSTAGE_API_KEY_RESTAURANT', 'gtrL4V3yXA0or59koYdkG1Od2ZzTWKrO');
 if(!defined('POSTAGE_API_KEY_CCEXPIRE')) define ('POSTAGE_API_KEY_CCEXPIRE', 'HEpYJ1ocUY6V4YK5d12w8sd1zT13Z9uv');
 if(!defined('POSTAGE_API_KEY_UV')) define ('POSTAGE_API_KEY_UV', 'RonTcr2sSOcmyvqgo1S1kOKWNvYAITDk');
 if(!defined('POSTAGE_API_KEY_ULTRA')) define ('POSTAGE_API_KEY_ULTRA', 'JmdCQj94JiTQIhlf0eHdQKNY8xb2tvaB');
 if(!defined('POSTAGE_API_KEY_MINT')) define('POSTAGE_API_KEY_MINT', 'iMDtzykMS7sAqtpVYAwvkzwd6s99Miou');

 global $global_tz_offset;
 global $global_activation_tz_offset;
 global $global_dblocal_tz_offset;

 ?><?php
  class PostageApp
  {
    // Sends a message to Postage App
    static function mail($key, $recipient, $subject, $mail_body, $header, $variables=NULL) {
      $content = array(
        'recipients'  => $recipient,
        'headers'     => $subject ? array_merge($header, array('Subject' => $subject)) : $header,
        'variables'   => $variables,
        'uid'         => create_guid()
      );
      if (is_string($mail_body)) {
        $content['template'] = $mail_body;
      } else {
        $content['content'] = $mail_body;
      }

      return PostageApp::post(
              'send_message',
              json_encode(
                array(
                  'api_key' => $key,
                  'arguments' => $content
                )
              )
             );
    }

    // Makes a call to the Postage App API
    static function post($api_method, $content) {
      $ch = curl_init(POSTAGE_HOSTNAME.'/v.1.0/'.$api_method.'.json');
      curl_setopt($ch, CURLOPT_POSTFIELDS,  $content);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));   
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $output = curl_exec($ch);
      curl_close($ch);
      return json_decode($output);
    }
  }
?><?php
/* and cast(last_recharge_date_time as date)<(getdate()-188) */
/* and cast(last_recharge_date_time as date)>(getdate()-189) */
/* and cast(creation_date_time as date)<(getdate()-188) */
/* and cast(creation_date_time as date)>(getdate()-189) */
$upcoming_expirations_query = "select
first_name,
last_name,
account,
cc.family_cos as plan_name,
e_mail as email,
balance,
CONVERT(nvarchar(30), cast(DATEADD(hh, $global_tz_offset, GETUTCDATE())+5 as date), 107) as expiration_date,
'Recharge' as last_charge_type
from accounts a, customers c, parent_cos cc
where a." . account_enabled_clause(TRUE) . " 
and balance>0
and a.cos_id in ('29955','30456','53788','53789')
and cast(DATEADD(hh, $global_tz_offset, last_recharge_date_time) as date)<(DATEADD(hh, $global_tz_offset, GETUTCDATE())-86)
and cast(DATEADD(hh, $global_tz_offset, last_recharge_date_time) as date)>(DATEADD(hh, $global_tz_offset, GETUTCDATE())-87)
and a.customer_id=c.customer_id
and a.cos_id=cc.cos_id

union all

select
first_name,
last_name,
account,
cc.family_cos as plan_name,
e_mail as email,
balance,
CONVERT(nvarchar(30), cast(DATEADD(hh, $global_tz_offset, GETUTCDATE())+5 as date), 107) as expiration_date,
'Signup' as last_charge_type
from accounts a, customers c, parent_cos cc
where a." . account_enabled_clause(TRUE) . " 
and balance>0
and a.cos_id in ('29955','30456','53788','53789')
and cast(DATEADD(hh, $global_dblocal_tz_offset, creation_date_time) as date)<(DATEADD(hh, $global_tz_offset, GETUTCDATE())-86)
and cast(DATEADD(hh, $global_dblocal_tz_offset, creation_date_time) as date)>(DATEADD(hh, $global_tz_offset, GETUTCDATE())-87)
and last_recharge_date_time is  null
and a.customer_id=c.customer_id
and a.cos_id=cc.cos_id";

/* and cast(last_recharge_date_time as date)<(getdate()-188) */
/* and cast(last_recharge_date_time as date)>(getdate()-189) */
/* and cast(creation_date_time as date)<(getdate()-188) */
/* and cast(creation_date_time as date)>(getdate()-189) */

$accounts_to_zero_query = "select account, balance
from accounts a
where " . account_enabled_clause(TRUE) . " 
and cos_id in ('29955','30456','53788','53789')
and cast(DATEADD(hh, $global_tz_offset, last_recharge_date_time) as date)<(DATEADD(hh, $global_tz_offset, GETUTCDATE())-91)
and balance>0

union all

select account, balance
from accounts a
where " . account_enabled_clause(TRUE) . " 
and cos_id in ('29955','30456','53788','53789')
and cast(DATEADD(hh, $global_dblocal_tz_offset, creation_date_time) as date)<(DATEADD(hh, $global_tz_offset, GETUTCDATE())-91)
and last_recharge_date_time is  null
and balance>0";

$CC_filtertable_query = "
 SELECT account_id,c.[first_name], c.[last_name], c.e_mail, p.*,
        a.[activation_date_time], a.[service_charge_date],
        CASE
         WHEN ISNUMERIC(REPLACE(cc_exp_date,'/',''))=1 THEN
          CASE
           WHEN ISDATE(LEFT(REPLACE(cc_exp_date,'/',''),2)+'/1/' +
                       RIGHT(REPLACE(cc_exp_date,'/',''),2)) = 1 THEN
            DATEDIFF(dd,
                     GETDATE(),
                     CONVERT(DATETIME,
                             LEFT(REPLACE(cc_exp_date,'/',''),2) + '/1/' +
                             RIGHT(REPLACE(cc_exp_date,'/',''),2)))
           ELSE NULL
          END
         ELSE NULL
        END AS days_till_expire

 FROM accounts a JOIN customers c ON a.[customer_id] = c.[customer_id]
                 JOIN [parent_cos] p ON a.[cos_id] = p.[cos_id]
  WHERE a." . account_enabled_clause(TRUE) . " AND p.[site] = 'IndiaLD'";

$customers_CC_will_expire_query = "WITH filtertable AS ( $CC_filtertable_query )
SELECT first_name, last_name, e_mail FROM filtertable
 WHERE days_till_expire < 60 AND days_till_expire >= 0
AND account_id % 28 + 1 = DATEPART(dd,GETDATE())";

$customers_CC_has_expired_query = "WITH filtertable AS ( $CC_filtertable_query )
SELECT first_name, last_name, e_mail FROM filtertable
 WHERE days_till_expire = -3";

        #'email' => $record->EMAIL,
        #'first_name' => $record->FIRST_NAME,
        #'last_name' => $record->LAST_NAME,
        #'account' => $record->ACCOUNT,

$uv_abandoned_query = "SELECT E_MAIL AS EMAIL, E_MAIL, FIRST_NAME, LAST_NAME, ACCOUNT, ACCOUNT_ID, LOGIN_NAME
 FROM customers, accounts
 WHERE accounts.customer_id = customers.customer_id AND customers.customer_id IN
   (SELECT customer_id
    FROM htt_mfunds_applications
    WHERE mfunds_person_id IS NULL AND
          (application_lifecycle < 40 OR application_lifecycle IS NULL) AND
          GETUTCDATE() - application_date_time < 1 )";

$uv_unfunded_query =  "SELECT customers.E_MAIL AS EMAIL, customers.E_MAIL, customers.FIRST_NAME, customers.LAST_NAME, accounts.ACCOUNT, accounts.ACCOUNT_ID, customers.LOGIN_NAME, htt_mfunds_applications.mfunds_person_id, htt_mfunds_applications.application_lifecycle
 FROM customers, accounts, htt_mfunds_applications
 WHERE accounts.customer_id = customers.customer_id AND htt_mfunds_applications.customer_id = customers.customer_id AND
 htt_mfunds_applications.application_lifecycle = 50 AND GETUTCDATE() - htt_mfunds_applications.application_date_time = 3";

function postage_post_template($tname, $data, $disabled, $extra1='???', $extra2='???!!!???')
{
  dlog("", "PostageApp template $tname, extra1 = $extra1, extra2 = $extra2, disabled = $disabled, data = " . json_encode($data));
  $count = count($data);
  if ($disabled) return "--DISABLED, $count results";

  $queue = array(array('template' => $tname));
  foreach ($data as $record)
  {
    switch ($tname)
    {
    case 'balance-expire':
// first_name":"Chandrashekar","last_name":"Venkatesan","account":"9773757843","plan_name":"Quarter Calls T","email":"cvenka02@yahoo.com","balance":4.75,"expiration_date":"2011-12-23","last_charge_type":"Signup"}

      $params = array(
        'email' => $record->email,
        'first_name' => $record->first_name,
        'last_name' => $record->last_name,
        'plan_name' => $record->plan_name,
        'balance' => $record->balance,
        'account' => $record->account,
        'expiration_date' => $record->expiration_date
        );

      $queue[] = $params;
      $queue[] = PostageApp::mail(POSTAGE_API_KEY_PAYG,
                                  $record->email,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

    case 'CreditCardWillExpire':
    case 'CreditCardHasExpired':

      $params = array(
        'email' => $record->e_mail,
        'first_name' => $record->first_name,
        'last_name' => $record->last_name,
        );

      $queue[] = $params;
      $queue[] = PostageApp::mail(POSTAGE_API_KEY_CCEXPIRE,
                                  array($record->e_mail),
                                  //array('tzz@lifelogs.com', 'rizwank@hometowntelecom.com'),
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

    case 'feedback':

      $params = array(
        'email' => $record->E_MAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'feedback_subject' => $extra1,
        'feedback' => $extra2
        );

      $queue[] = $params;
      $queue[] = PostageApp::mail(POSTAGE_API_KEY_INTERNAL,
                                  array('tzz@lifelogs.com', 'cfurlong@gmail.com', 'dschofield@hometowntelecom.com', 'janderson@hometowntelecom.com'),
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

    
    case 'Restaurant':

      $queue[] = $record;
      $queue[] = PostageApp::mail(POSTAGE_API_KEY_RESTAURANT,
                                  array($record['recipient'], 'cfurlong@gmail.com'),
//                                  array($record['recipient'], 'tzz@lifelogs.com', 'cfurlong@gmail.com'),
//                                  $record['recipient'],
//                                  array('tzz@lifelogs.com', 'cfurlong@gmail.com'),
//                                  array('tzz@lifelogs.com'),
                                  NULL,
                                  $tname,
                                  array(),
                                  $record);
      break;

    case 'topup-success':
    case 'topup-failure':
    case 'topup-cancelled':

      $queue[] = $record;
      $queue[] = PostageApp::mail(POSTAGE_API_KEY_TTO,
                                  $record['recipient'],
                                  NULL,
                                  $tname,
                                  array(),
                                  $record);
      break;

    case 'uv-kyc-notify':
      $queue[] = $record;
      $queue[] = PostageApp::mail(POSTAGE_API_KEY_UV,
                                  $record['recipient'],
                                  NULL,
                                  $tname,
                                  array(),
                                  $record);
      break;

    case 'uv-abandoned':
    case 'uv-unfunded':
    case 'uv-account-created':
    case 'uv-account-approved':
    case 'uv-account-rejected':
    case 'uv-child-created':
    case 'uv-child-approved':
    case 'uv-child-rejected':
      $params = array(
        'email' => $record->EMAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'new_password' => $extra1,
		'login_name' => $record->LOGIN_NAME,
		'site' => $_SERVER["SERVER_NAME"]
        );

      // we don't want the new password to leak out
      $queue[] = array(
        'email' => $record->EMAIL,
        'account' => $record->ACCOUNT
        );

      $queue[] = PostageApp::mail(POSTAGE_API_KEY_UV,
                                  $record->EMAIL,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

    case 'uv-reset-password':

      $params = array(
        'email' => $record->E_MAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'new_password' => $extra1,
		'site' => $_SERVER["SERVER_NAME"]
        );

      // we don't want the new password to leak out
      $queue[] = array(
        'email' => $record->E_MAIL,
        'account' => $record->ACCOUNT
        );

      $queue[] = PostageApp::mail(POSTAGE_API_KEY_UV,
                                  $record->E_MAIL,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

	case 'uv-send-username':

      $params = array(
        'email' => $record->E_MAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'login_name' => $record->LOGIN_NAME,
		'site' => $_SERVER["SERVER_NAME"]
        );

      // we don't want the new password to leak out
      $queue[] = array(
        'email' => $record->E_MAIL,
        'account' => $record->ACCOUNT
        );

      $queue[] = PostageApp::mail(POSTAGE_API_KEY_UV,
                                  $record->E_MAIL,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;
    
    case 'ultra-account-created':
    case 'ultra-sim-shipped':
	case 'ultra-activated':
    case 'ultra-auto-recharge-on':
    case 'ultra-auto-recharge-off':
    case 'ultra-replacement-sim-ordered':
    case 'ultra-recharge-successful':
    case 'ultra-purchase-confirmation':
    case 'ultra-paygo-topup':
    case 'ultra-plan-switched':
    case 'ultra-replacement-sim-activated':
      $params = array(
        'email' => $record->EMAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'new_password' => $extra1,
		'login_name' => $record->LOGIN_NAME,
		'site' => $_SERVER["SERVER_NAME"]
        );

      // we don't want the new password to leak out
      $queue[] = array(
        'email' => $record->EMAIL,
        'account' => $record->ACCOUNT
        );

      $queue[] = PostageApp::mail(POSTAGE_API_KEY_ULTRA,
                                  $record->EMAIL,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

    case 'ultra-forgot-password':

      $params = array(
        'email' => $record->E_MAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'new_password' => $extra1,
		'site' => $_SERVER["SERVER_NAME"]
        );

      // we don't want the new password to leak out
      $queue[] = array(
        'email' => $record->E_MAIL,
        'account' => $record->ACCOUNT
        );

      $queue[] = PostageApp::mail(POSTAGE_API_KEY_ULTRA,
                                  $record->E_MAIL,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;

	case 'ultra-send-username':

      $params = array(
        'email' => $record->E_MAIL,
        'first_name' => $record->FIRST_NAME,
        'last_name' => $record->LAST_NAME,
        'account' => $record->ACCOUNT,
        'login_name' => $record->LOGIN_NAME,
		'site' => $_SERVER["SERVER_NAME"]
        );

      // we don't want the new password to leak out
      $queue[] = array(
        'email' => $record->E_MAIL,
        'account' => $record->ACCOUNT
        );

      $queue[] = PostageApp::mail(POSTAGE_API_KEY_ULTRA,
                                  $record->E_MAIL,
                                  NULL,
                                  $tname,
                                  array(),
                                  $params);
      break;
    }
  }

  return $queue;
}

// UNUSED
function postage_topup_report($success, $disabled, $extra_recipients)
{
  global $out;

  $extra_recipients[] = $out["customer_status"]->email;

  foreach ($extra_recipients as $recipient)
  {
    $tname = $success ? 'topup-success' : 'topup-failure';

    $params = array(
      'recipient' => $recipient,
      'when' => date('Y-m-d H:i:s'),
      "transferto_cost" => $out["transferto_cost"],
      "cc4" => $out["customer_status"]->CC_LAST_4,
      "email" => $out["customer_status"]->email,
      "first_name" => $out["customer_status"]->FIRST_NAME,
      "last_name" => $out["customer_status"]->LAST_NAME,
      "customer" => $out["customer_status"]->CUSTOMER,
      );

    $todo = array("destination_currency",
                  "topup_sent_amount",
                  "product_requested",
                  "error_txt",
                  "destination_msisdn");

    if (array_key_exists('tto', $out) && is_array($out['tto']))
    {
      foreach ($out['tto'] as $record)
      {
        if (is_array($record))
        {
          foreach ($todo as $k)
          {
            if (array_key_exists($k, $record))
            {
              $params[$k] = $record[$k];
            }
          }
        }
      }
    }

    $out['postage'][] = postage_post_template($tname, array($params), $disabled);
  }
}

function postage_topup_cancel_report($customer, $request, $extra_recipients)
{
  global $out;

  $id = $customer->CUSTOMER_ID;
  $customer = find_customer(make_find_status_customer_query($id));

  $extra_recipients[] = $customer->EMAIL;

  foreach ($extra_recipients as $recipient)
  {
    $tname = 'topup-cancelled';

    $params = array(
      'recipient' => $recipient,
      'when' => date('Y-m-d H:i:s'),
      "transferto_cost" => $request->dollar_cost,
      "destination_msisdn" => $request->destination_msisdn,
      "product_requested" => $request->product,
      "destination_currency" => 'INR',
      "cc4" => $customer->CC_LAST_4,
      "email" => $customer->EMAIL,
      "first_name" => $customer->FIRST_NAME,
      "last_name" => $customer->LAST_NAME,
      "customer" => $customer->CUSTOMER,
      );

    $out['postage'][] = postage_post_template($tname, array($params), FALSE);
  }
}

function postage_clear_balance($data, $disabled)
{
  $count = count($data);
  if ($disabled) return "--DISABLED, $count results";

  $done = array();

  foreach ($data as $record)
  {
    $customer = find_customer(make_find_customer_query($record->account));
    if ($customer)
    {
      $q = sprintf('UPDATE accounts SET balance = 0 WHERE account = \'%s\'', $record->account);
      $check = is_mssql_successful(logged_mssql_query($q));
      if ($check)
      {
        $done[] = $check;

        $description = sprintf('%s expired after 90 days of no recharge.', $record->balance);
        $q = sprintf("
INSERT into BILLING
( LOGIN_NAME, ENTRY_TYPE,
  account_id, ACCOUNT, account_group,
  node, node_type,
  START_DATE_TIME, CONNECT_DATE_TIME, DISCONNECT_DATE_TIME,
  DESCRIPTION, DETAIL, AMOUNT, MODULE_NAME,
  PER_CALL_CHARGE, Per_minute_charge, PER_CALL_SURCHARGE, PER_MINUTE_SURCHARGE,
  ACTUAL_DURATION, QUANTITY, CURRENCY, CONVERSION_RATE, ANI, DNIS,
  SALES_GROUP, TAX_GROUP,
  USER_1, USER_2, USER_3, USER_4, USER_5, USER_6, USER_7, USER_8, USER_9, USER_10,
  INFO_DIGITS, RATE_INTERVAL, DISCONNECT_CHARGE, BILLING_DELAY,
  GRACE_PERIOD, ACCOUNT_TYPE, PACKAGED_BALANCE_INDEX,
  CALL_SESSION_ID, CALL_ID )
VALUES ( 'api_accounts', 10,
         %d, '%s', %d,
         'TMC', 3,
         getdate(),getdate(),getdate(),
         %s,'Balance Expiration',%d,'PHP API',
         0,0,0,0,
         0,0,0,0,0,0,
         0,0,
         0,0,0,0,0,0,0,0,0,0,
         0,0,0,0,
         0,0,0,
         newid(), newid()); ",
                         $customer->ACCOUNT_ID, $customer->ACCOUNT, $customer->ACCOUNT_GROUP_ID, mssql_escape($description), 0);

        $check = is_mssql_successful(logged_mssql_query($q));
        if ($check)
        {
          $done[] = $check;
        }
        else
        {
          $mssql_last_message = mssql_get_last_message();
          $done[] = array('error' => ($out['debug'] ? "The [$q] query failed with result [$check]; $mssql_last_message.\n" : "The query failed with result [$check]; $mssql_last_message.\n"));
        }

      }
      else
      {
        $mssql_last_message = mssql_get_last_message();
        $done[] = array('error' => ($out['debug'] ? "The [$q] query failed with result [$check]; $mssql_last_message.\n" : "The query failed with result [$check]; $mssql_last_message.\n"));
      }
    }
    else
    {
      $done[] = "Could not find customer " . $record->account;
    }

  }

  return $done;
}

function postage_allocate_certs($type, $disabled)
{
  global $global_tz_offset;
  global $global_dblocal_tz_offset;

  if ($disabled) return "--DISABLED";

  $cert_query = sprintf("select certificate, account from htt_gift_certificates where account is null and certificate_type = %s",
                        mssql_escape($type));

  $certs = mssql_fetch_all_objects(logged_mssql_query($cert_query));

  $cert_lookup_by_account = array();
  foreach (mssql_fetch_all_objects(logged_mssql_query(sprintf("select certificate, account from htt_gift_certificates where account is not null and certificate_type = %s",
                                                              mssql_escape($type)))) as $cert)
  {
    $cert_lookup_by_account[$cert->account] = $cert->certificate;
  }

  $customer_query = "SELECT c.first_name, c.e_mail, a.account,
                            DATEADD(hh, $global_dblocal_tz_offset, creation_date_time) AS local_creation_date_time,
                            pc.cos

FROM accounts a, customers c, parent_cos pc

WHERE a.customer_id=c.customer_id
AND datediff(dd, DATEADD(hh, $global_dblocal_tz_offset, creation_date_time), DATEADD(hh, $global_tz_offset, GETUTCDATE())) = 1
-- creation_date_time > '2012-02-02 17:38:09.817' --
AND a." . account_enabled_clause(TRUE) . " 
AND a.cos_id IN ('49361','49362','49363','49360')
AND a.cos_id=pc.cos_id
GROUP BY c.first_name, c.e_mail, a.account, creation_date_time, pc.cos
ORDER BY creation_date_time";

  $done = array('count' => 0);

  foreach (mssql_fetch_all_objects(logged_mssql_query($customer_query)) as $record)
  {
    //dlog("", json_encode($cert_lookup_by_account));
    //dlog("", json_encode($record->account));
    if (array_key_exists($record->account, $cert_lookup_by_account))
    {
      $done[] = array(sprintf("skipped account %s, already done",
                              $record->account) =>
                      $cert_lookup_by_account[$record->account]);
      continue;
    }

    $done['count']++;
    $cert = array_pop($certs);
    if (NULL == $cert)
    {
      $done[] = array('error' => 'No more certs!');
      break;
    }

    $cert = $cert->certificate;

    $q = sprintf("UPDATE htt_gift_certificates
SET account = '%s', assignment_date = GETUTCDATE()
WHERE certificate = %s and certificate_type = %s",
                 mssql_escape($record->account),
                 mssql_escape_with_zeroes($cert),
                 mssql_escape($type));
    $check = is_mssql_successful(logged_mssql_query($q));
    if ($check)
    {
      $done[] = $check;
      $params = array(
        'recipient' => $record->e_mail,
        'account' => $record->account,
        'first_name' => $record->first_name,
        'cert' => $cert,
        );
      $done[] = postage_post_template('Restaurant', array($params), $disabled);
    }
    else
    {
      $mssql_last_message = mssql_get_last_message();
      $done[] = array('error' => ($out['debug'] ? "The [$q] query failed with result [$check]; $mssql_last_message.\n" : "The query failed with result [$check]; $mssql_last_message.\n"));
    }

  }

  return $done;
}

function postage_uv_message($tname, $customer, $request, $extra_recipients)
{
  global $out;

  $id = $customer->CUSTOMER_ID;
  $customer = find_customer(make_find_status_customer_query($id));

  //$extra_recipients[] = $customer->EMAIL;

  foreach ($extra_recipients as $recipient)
  {
    $params_standard = array(
      'recipient' => $recipient,
      'when' => date('Y-m-d H:i:s'),
      "cc4" => $customer->CC_LAST_4,
      "email" => $customer->EMAIL,
      "first_name" => $customer->FIRST_NAME,
      "last_name" => $customer->LAST_NAME,
      "customer" => $customer->CUSTOMER,
      );

    $params = array_merge($params_standard, $request);
    //dlog("", json_encode($params));
    $out['postage'][] = postage_post_template($tname, array($params), FALSE);
  }
}

function reset_password($template, $customer_full)
{
  global $out;

  $new_password = Text_Password::create();

  $fields = array('account_password' => $new_password);

  if (validate_fields($fields))
  {
    //$out['fields'] = 
    fields($fields,
                            $customer_full,
                            FALSE);
    $out['postage'] = postage_post_template($template,
                                            array($customer_full),
                                            FALSE,
                                            $new_password);
    $out['reset_clamputed'] = 'yay';
    //$out['new_password'] = $new_password;
  }
  else
  {
    $out['errors'][] = "new password validation error";
    $out['reset_clamputed'] = 'server error resetting the password';
  }
}

function send_ultra_email($template_name, $params, $user_email) {
	
	return PostageApp::mail(POSTAGE_API_KEY_ULTRA, $user_email, NULL, $template_name, array(), $params);
}

/**
 * getPostageAppTemplateName
 *
 * given a high level template name (which typically translates into a specific action, such as 'porting-success')
 * and subscriber's brand and preferred language determinate the final template name availabe at email provider
 * TODO: rather than having a hard-coded map this function should be re-implemented after Email->getTemplateDefinition()
 * to lookup templates dynamically
 * @param String template name
 * @parma String short brand name
 * @param String language code
 * @return String template name or NULL on failure
 */
function getPostageAppTemplateName($name, $brand, $language)
{
  // map of currently available templates at https://hometowntelecom.postageapp.com/projects/2256/templates
  $map = array(
    'porting-success'   => array(
      'ultra'             => array('en', 'es'),
      'univision'         => array('en', 'es')
    ),
    'ultra-activated'   => array(
      'ultra'             => array('en', 'es'),
      'univision'         => array('en', 'es')
    )
  );

  // normalize input
  foreach (array('name', 'brand', 'language') as $key)
    $$key = strtolower($$key);

  // identify full template name: name_brand_language
  if ( ! empty($map[$name][$brand]))
  {
    if (in_array($language, $map[$name][$brand]))
      return "{$name}_{$brand}_{$language}";

    // fallback to English
    elseif (in_array('en', $map[$name][$brand]))
      return "{$name}_{$brand}_en";
  }

  // not found
  logError("missing template '$name' for brand '$brand' and language '$language'");
  return NULL;
}

