<?php

require_once 'db.php';
require_once 'classes/CommandInvocation.php';
require_once 'lib/util-common.php';


# Usage:
#    - php classes/CommandInvocation_test.php get_ultra_acc_command_invocations $CUSTOMER_ID
#    - php classes/CommandInvocation_test.php isOccupied        $CUSTOMER_ID
# A0 - php classes/CommandInvocation_test.php initiate          $CUSTOMER_ID
# M0 - php classes/CommandInvocation_test.php confirmOpen       $CUSTOMER_ID
# M2 - php classes/CommandInvocation_test.php failOnAsync       $CUSTOMER_ID
# M3 - php classes/CommandInvocation_test.php failOnTimeout     $CUSTOMER_ID
# M4   php classes/CommandInvocation_test.php failOnSync        $CUSTOMER_ID
#    - php classes/CommandInvocation_test.php loadError         $CUSTOMER_ID
#    - php classes/CommandInvocation_test.php loadOpen          $CUSTOMER_ID
#      php classes/CommandInvocation_test.php loadOpenByCustomerId $CUSTOMER_ID
#      php classes/CommandInvocation_test.php loadOpenByCommand       $COMMAND $ACC_TRANSITION_ID
#      php classes/CommandInvocation_test.php loadOpenByCorrelationID $CORRELATION_ID $COMMAND
# C3 - php classes/CommandInvocation_test.php endAsSuccess      $CUSTOMER_ID $COMMAND_INVOCATIONS_ID
# M1 - php classes/CommandInvocation_test.php escalationMissing $CUSTOMER_ID
# M1 - php classes/CommandInvocation_test.php escalationError   $CUSTOMER_ID
# C1   php classes/CommandInvocation_test.php escalationSuccess $CUSTOMER_ID
#      php classes/CommandInvocation_test.php delayedSuccess    $CUSTOMER_ID
#    - php classes/CommandInvocation_test.php loadPending       $CUSTOMER_ID
#      php classes/CommandInvocation_test.php housekeeping
#      php classes/CommandInvocation_test.php recovery
#      php classes/CommandInvocation_test.php command_invocations_initiated_cleanup


abstract class AbstractTestStrategy
{
  abstract function test( $argv  );
}


class Test_housekeeping extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->housekeeping();

    if ( $result->is_failure() ) { echo "C1 failure\n"; } else { echo "C1 success\n"; }
  }
}


class Test_command_invocations_initiated_cleanup extends AbstractTestStrategy
{
  function test( $argv )
  {
    \Ultra\Lib\DB\ultra_acc_connect();

    \command_invocations_initiated_cleanup();
  }
}


class Test_recovery extends AbstractTestStrategy
{
  function test( $argv )
  {
    // get 'MISSING' or 'INITIALIZED' or 'OPEN' invocations

    $ci = new CommandInvocation();

    $ci->dbConnect();

    #$sql = 'SELECT * FROM COMMAND_INVOCATIONS WHERE STATUS IN ("MISSING","INITIALIZED","OPEN") AND START_DATE_TIME < DATEADD( ss , - 2*60*60 , GETUTCDATE() )';
    #$sql = 'SELECT COMMAND_INVOCATIONS_ID , START_DATE_TIME
    $sql = 'SELECT *
            FROM   COMMAND_INVOCATIONS
            WHERE  STATUS IN ("MISSING","INITIALIZED","OPEN")
            AND    START_DATE_TIME < DATEADD( ss , - 2*60*60 , GETUTCDATE() )
            AND    START_DATE_TIME > DATEADD( d  , - 2       , GETUTCDATE() )
            ORDER  BY COMMAND_INVOCATIONS_ID';

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( $data && is_array($data) && count($data) )
    {
      #dlog('',"data = %s",$data);

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      foreach( $data as $row )
      {
        // garbage collect
        $ci = NULL;

        teldata_change_db();

        dlog('',"row = %s",$row);

        $customer = get_ultra_customer_from_customer_id($row->CUSTOMER_ID);

        dlog('',"customer = %s",$customer);

        if ( $customer->current_mobile_number || $customer->CURRENT_ICCID_FULL )
        {
          $data = array(
            'actionUUID' => time(),
            'iccid'      => $customer->CURRENT_ICCID_FULL,
            'msisdn'     => $customer->current_mobile_number
          );

          $result = $mwControl->mwQuerySubscriber( $data );

          if ( $result->is_success()
            && isset($result->data_array['success'])
            && $result->data_array['success']
            && isset($result->data_array['body'])
            && ( property_exists( $result->data_array['body'] , 'ResultCode' ) )
            && ( property_exists( $result->data_array['body'] , 'ResultMsg'  ) )
            && ( $result->data_array['body']->ResultCode == '100' )
            && ( $result->data_array['body']->ResultMsg  == 'Success' )
            && ( ( ! property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) ) || ( ! $result->data_array['body']->CurrentAsyncService ) )
          )
          {
            // open command invocations good to close

            print_r($result->data_array['body']);

            $commandInvocation = new CommandInvocation();

            $result = $commandInvocation->loadPending(
              array(
                'customer_id' => $row->CUSTOMER_ID
              )
            );

            if ( $result->is_success() )
            {
              $result = $commandInvocation->escalationSuccess(
                array(
                  'from_status'            => $commandInvocation->status,
                  'last_status_updated_by' => 'raf tool'
                )
              );

              if ( $result->is_success() )
                dlog('',"CUSTOMER_ID ".$row->CUSTOMER_ID.'FIXED!');
              else
                dlog('',"CUSTOMER_ID ".$row->CUSTOMER_ID.'NOT FIXED!');

sleep(1);
            }
          }
        }
      }
    }
  }
}


class Test_escalationSuccess extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadError(
      array(
        'customer_id' => $argv[2]
      )
    );

    if ( $result->is_failure() )
    {
      // there was an error
      echo "C1 - error:\n";
      print_r( $result->get_errors() );
      print_r( $result );
    }
    else
    {
      $result = $ci->escalationSuccess(
        array(
          'from_status'            => 'ERROR',
          'last_status_updated_by' => 'php'
        )
      );

      if ( $result->is_failure() ) { echo "C1 failure\n"; } else { echo "C1 success\n"; }
    }
  }
}


class Test_escalationMissing extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadError(
      array(
        'customer_id' => $argv[2]
      )
    );

    if ( $result->is_failure() )
    {
      // there was an error
      echo "M1 - error:\n";
      print_r( $result->get_errors() );
      print_r( $result );
    }
    else
    {
      $result = $ci->escalationMissing(
        array(
          'ticket_details'         => 'TICKET_DETAILS go here',
          'last_status_updated_by' => 'LAST_STATUS_UPDATED_BY goes here'
        )
      );

      if ( $result->is_failure() ) { echo "M1 failure\n"; } else { echo "M1 success\n"; }
    }
  }
}


class Test_escalationError extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadError(
      array(
        'customer_id' => $argv[2]
      )
    );

    if ( $result->is_failure() )
    {
      // there was an error
      echo "M1 - error:\n";
      print_r( $result->get_errors() );
      print_r( $result );
    }
    else
    {
      $result = $ci->escalationError(
        array(
          'ticket_details'         => 'TICKET_DETAILS go here',
          'last_status_updated_by' => 'LAST_STATUS_UPDATED_BY goes here'
        )
      );

      if ( $result->is_failure() ) { echo "M1 failure\n"; } else { echo "M1 success\n"; }
    }
  }
}


class Test_get_ultra_acc_command_invocations extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ultra_acc_command_invocations = get_ultra_acc_command_invocations(
      array(
        'customer_id' => $argv[2]
      )
    );

    print_r($ultra_acc_command_invocations);
  }
}


class Test_isOccupied extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation(
      array(
        'customer_id' => $argv[2]
      )
    );

    dlog('',"ci = %s",$ci);

    if ( $ci->isOccupied() ) { echo "isOccupied TRUE\n"; } else { echo "isOccupied FALSE\n"; }
  }
}


class Test_initiate extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation(
      array(
        'customer_id'       => $argv[2],
        'action_uuid'       => getNewActionUUID('test ' . time()),
        'acc_api'           => 'ACC_API goes here',
        'correlation_id'    => 'CORRELATION_ID goes here',
        'command'           => 'COMMAND goes here'
      )
    );

    dlog('',"ci = %s",$ci);

    $result = $ci->initiate();

    if ( $result->is_failure() ) { echo "A0 failure\n"; } else { echo "A0 success\n"; }
  }
}


class Test_confirmOpen extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation(
      array(
        'customer_id'            => $argv[2],
        'acc_transition_id'      => 'TRANSITION_ID goes here',
        'status_history'         => 'test'
      )
    );

    dlog('',"ci = %s",$ci);

    $result = $ci->confirmOpen();

    if ( $result->is_failure() ) { echo "M0 failure\n"; } else { echo "M0 success\n"; }
  }
}


class Test_delayedSuccess extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation(
      array(
        'customer_id'       => $argv[2],
        'action_uuid'       => getNewActionUUID('test ' . time()),
        'acc_api'           => 'ACC_API goes here',
        'command'           => 'COMMAND goes here'
      )
    );

    dlog('',"ci = %s",$ci);

    $result = $ci->delayedSuccess();

    if ( $result->is_failure() ) { echo "C2 failure\n"; } else { echo "C2 success\n"; }
  }
}


class Test_loadOpenByCorrelationID extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadOpenByCorrelationID( $argv[2] , $argv[3] );

    if ( $result->is_failure() )
      echo "loadOpenByCorrelationID failure\n";
    elseif ( $result->is_pending() )
    {
      echo "loadOpenByCommand success ( pending )\n";

      print_r($ci);

      print_r( (array) $ci );
    }
    else
    {
      echo "loadOpenByCommand success ( not pending )\n";

      print_r($ci);

      print_r( (array) $ci );
    }
  }
}


class Test_loadOpenByCommand extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadOpenByCommand( $argv[2] , $argv[3] );

    if ( $result->is_failure() )
      echo "loadOpenByCommand failure\n";
    elseif ( $result->is_pending() )
    {
      echo "loadOpenByCommand success ( pending )\n";

      print_r($ci);

      print_r( (array) $ci );
    }
    else
    {
      echo "loadOpenByCommand success ( not pending )\n";

      print_r($ci);

      print_r( (array) $ci );
    }
  }
}


class Test_loadPending extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new \CommandInvocation();

    $result = $ci->loadPending( array( 'customer_id' => $argv[2] ) );

    if ( $result->is_failure() )
      echo "loadPending failure\n";
    elseif ( $result->is_pending() )
      echo "loadPending success\n";
    else
    {
      echo "loadPending failure\n";

      print_r($ci);
    }

    print_r( (array) $ci );
  }
}


class Test_loadOpenByCustomerId extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadOpenByCustomerId( $argv[2] );

    if ( $result->is_failure() )
      echo "loadOpenByCustomerId failure\n";
    elseif ( $result->is_pending() )
    {
      echo "loadOpenByCustomerId success ( pending )\n";

      print_r($ci);

      print_r( (array) $ci );
    }
    else
    {
      echo "loadOpenByCustomerId success ( not pending )\n";

      print_r($ci);

      print_r( (array) $ci );
    }
  }
}


class Test_failOnSync extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->failOnSync(
      array(
        'customer_id'            => $argv[2],
        'status_history'         => 'test status_history',
        'error_code_and_message' => 'test error_code_and_message'
      ),
      'ERROR'
    );

    if ( $result->is_failure() )
      echo "fail\n";
    else
      echo "OK\n";
  }
}


class Test_failOnAsync extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadOpen(
      array(
        'customer_id'       => $argv[2]
      )
    );

    if ( $result->is_failure() )
      echo "loadOpen failure\n";
    else
    {
      echo "loadOpen success\n";

      dlog('',"ci = %s",$ci);

      $result = $ci->failOnAsync(
        array(
          'status_history'         => 'test history',
          'error_code_and_message' => 'test error'
        )
      );

      if ( $result->is_failure() ) { echo "M2 failure\n"; } else { echo "M2 success\n"; }
    }
  }
}


class Test_failOnTimeout extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadOpen(
      array(
        'customer_id'       => $argv[2]
      )
    );

    if ( $result->is_failure() )
      echo "loadOpen failure\n";
    else
    {
      echo "loadOpen success\n";

      dlog('',"ci = %s",$ci);

      $result = $ci->failOnTimeout();

      if ( $result->is_failure() ) { echo "M3 failure\n"; } else { echo "M3 success\n"; }
    }
  }
}


class Test_loadOpen extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadOpen(
      array(
        'customer_id'       => $argv[2]
      )
    );

    if ( $result->is_failure() )
      echo "loadOpen failure\n";
    else
    {
      echo "loadOpen success\n";
      print_r( (array) $ci );
    }
  }
}


class Test_loadError extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->loadError(
      array(
        'customer_id'       => $argv[2]
      )
    );

    if ( $result->is_failure() )
      echo "loadError failure\n";
    else
    {
      echo "loadError success\n";
      print_r( (array) $ci );
    }
  }
}


class Test_endAsSuccess extends AbstractTestStrategy
{
  function test( $argv )
  {
    $ci = new CommandInvocation();

    $result = $ci->endAsSuccess(
      array(
        'customer_id'            => $argv[2],
        'command_invocations_id' => $argv[3],
        'status_history'         => 'test'
      )
    );

    if ( $result->is_failure() ) { echo "C3 failure\n"; } else { echo "C3 success\n"; }
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

// connect to ULTRA_ACC DB
\Ultra\Lib\DB\ultra_acc_connect();

$testObject->test( $argv );


?>
