<?php

/**
 * UserErrorException
 *
 * Wrapper for Exception class allowing for code parameter as a string
 *
 */
class UserErrorException extends \Exception
{
  private $userErrorCode;

  public function __construct( $message, $userErrorCode=0, \Exception $previous=null )
  {
    parent::__construct( $message, 0, $previous );

    $this->userErrorCode = $userErrorCode;
  }

  public function getUserErrorCode()
  {
    return $this->userErrorCode;
  }
}