<?php

global $credentials;
require_once 'cosid_constants.php';
require_once 'db.php';

require_once 'classes/WebPosRunner.php';
require_once 'Ultra/Lib/Util/Redis/WebPos.php';

require_once 'classes/PHPUnitBase.php';
require_once 'test/api/test_api_include.php';

class WebPosRunnerTest extends PHPUnitBase
{
  public function setUp()
  {
    parent::setUp();
    $this->createDummyData();
  }

  private function createDummyData()
  {
    teldata_change_db();

    $time = time();

    $this->dummyUUID = 'webpos_test_' . $time;
    $this->dummyTransactionID = 'webpos_trans_' . $time;

    $status = 'OPEN';

    $sql = "
    INSERT INTO ULTRA.WEBPOS_ACTIONS (
      CUSTOMER_ID, UUID, STATUS, CREATED, TYPE, TRANSACTION_ID, SUBPRODUCT_ID, 
      PLAN_NAME, AMOUNT, TMOBILE_CODE, PROVIDER_NAME, ICCID, ZIPCODE, LANGUAGE
    ) VALUES (
      31,
      '" . $this->dummyUUID . "',
      '" . $status . "',
      GETUTCDATE(),
      1,
      '" . $this->dummyTransactionID . "',
      'L19',
      'NINETEEN',
      19,
      'test_tmo_code',
      'test_provider',
      '1010101010101010101',
      '92683',
      'EN'
    )";

    logged_mssql_query($sql);

    $this->redis = new \Ultra\Lib\Util\Redis\WebPos(new \Ultra\Lib\Util\Redis(TRUE));
    $this->redis->addUUID($this->dummyUUID, time());
  }

  public function tearDown()
  {
    $sql = 'DELETE FROM ULTRA.WEBPOS_ACTIONS WHERE UUID = \'' . $this->dummyUUID . '\'';
    logged_mssql_query($sql);

    $this->redis->delUUID($this->dummyUUID);
  }

  /**
   * test__internal__GetState
   * test API internal__GetState
   */
  public function test__WebPosRunner()
  {
    $webPosRunner = new WebPosRunner();

    $data = $webPosRunner->getWebPosActionData($this->dummyUUID);
    print_r($data);

    $webPosRunner->updateWebPosActionDataSuccess($this->dummyUUID);

    echo PHP_EOL . 'AFTER UPDATE to DONE' . PHP_EOL;
    $data = $webPosRunner->getWebPosActionData($this->dummyUUID);
    print_r($data);

    $this->assertEquals($data->STATUS, 'DONE');

    $success = $webPosRunner->changeStateActivation($data);
    $this->assertFalse($success);
  }


  /**
   * end-to-end WebPOS tests
   */
  public function test__EndToEnd()
  {
    $this->setOptions(array(
      'bath'      => 'soap',
      'version'   => 1,
      'partner'   => 'ePay',
      'wsdl'      => 'partner-meta/ePay.wsdl'));

    // activate
    $this->setOptions(array('api' => 'externalpayments__WebPosActivateCustomer'));
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '890126084210773968',
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00 SKU
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);
    $uuid = $result->ultra_payment_trans_id;

    // port
    // success
    $this->setOptions(array('api' => 'externalpayments__WebPosPortInCustomer'));
    $params = array(
      'partner_tag'           => 'test',
      'request_epoch'         => time(),
      'iccid'                 => '',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '',
      'port_account_password' => '1234',
      'port_account_zipcode'  => '11211',
      'port_carrier'          => 'SprintPCS',
      'port_phone_number'     => '4159609387',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'subproduct_id'         => 'PQ19',
      'tmo_id'                => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // void
    sleep(3);
    $this->setOptions(array('api' => 'externalpayments__WebPosCancelCustomerTransaction'));
    $params = array(
      'partner_tag'       => $params['partner_tag'],
      'request_epoch'     => time(),
      'iccid'             => $params['iccid'],
      'cancel_type'       => 'REFUND',
      'provider_trans_id' => '236720455', //$params['provider_trans_id'],
      'ultra_payment_trans_id' => '{4F26A501-48FC-8959-0CCF-D91735913C05}');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // process
    $uuid = '{8B250E68-0BB1-E784-0653-AEDDD7C793E0}';
    $webpos = new WebPosRunner;
    $webpos->main($uuid);

  }
}
