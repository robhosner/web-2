<?php

require_once 'classes/PHPUnitBase.php';
require_once 'db.php';
require_once 'classes/SalesTaxCalculator.php';

class SalesTaxCalculatorTest extends PHPUnitBase
{
  public function test_setParameters()
  {
    teldata_change_db();

    $customer_id = 31;

    $calc = new SalesTaxCalculator();

    // fails no amount
    $result = $calc->getSalesTax('XXX');
    $this->assertFalse($result->is_success());

    // dont fail on amount next time
    $calc->setAmount(1);

    $calc->setBillingZipCode('92683');
    $this->assertEquals('92683', $calc->billingZipCode);

    // make zip code too small
    $calc->setBillingZipCode('1234');

    // fails zip code too small
    $result = $calc->getSalesTax('XXX');
    $this->assertFalse($result->is_success());

    // setting by customer id
    $calc->setBillingInfoByCustomerId($customer_id);

    $this->assertEquals(31,           $calc->customer_id);
    $this->assertEquals('1347193206', $calc->account);
    $this->assertEquals('10044',      $calc->billingZipCode);
    $this->assertEquals(1,            $calc->amount);

    // set account
    $calc->setAccount('555555');
    $this->assertEquals('555555', $calc->account);

    // set billingZipCode
    $calc->setBillingZipCode('92683');
    $this->assertEquals('92683', $calc->billingZipCode);

    // set and add (addition) amount
    $calc->setAmount(25);
    $this->assertEquals(25, $calc->amount);
    $calc->addAmount(5);
    $this->assertEquals(30, $calc->amount);

    $calc->setAmount(0);
    $this->assertEquals(0, $calc->amount);
  }

  public function test__getRecoveryFee()
  {
    // less than zero
    $amount = 0;

    $calc = new SalesTaxCalculator();

    $fee = $calc->getRecoveryFee($amount);
    $this->assertEquals(0, $fee['recovery_fee']);

    // less than $10
    $fee = $calc->getRecoveryFee(500);
    $this->assertEquals(50, $fee['recovery_fee']);
    // greater than $10
    $fee = $calc->getRecoveryFee(1001);
    $this->assertEquals(100, $fee['recovery_fee']);
  }

  public function test__calculateTaxesFees()
  {
    teldata_change_db();

    $calc = new \SalesTaxCalculator();
    
    $calc->setBillingInfoByCustomerId(31);
    $calc->setAmount(500);

    $taxes = $calc->calculateTaxesFees('RECHARGE');
    echo PHP_EOL . 'RECHARGE' . PHP_EOL;
    print_r($taxes->data_array);

    $this->assertTrue($taxes->is_success());

    $taxes = $calc->calculateTaxesFees();
    echo PHP_EOL . 'RECHARGE' . PHP_EOL;
    print_r($taxes->data_array);

    $this->assertTrue($taxes->is_success());
  }

  public function test__isCaliforniaZip()
  {
    $this->assertTrue(SalesTaxCalculator::isCaliforniaZip("92683"));
    $this->assertTrue(SalesTaxCalculator::isCaliforniaZip(92683));
    $this->assertFalse(SalesTaxCalculator::isCaliforniaZip(34743));
  }
}
