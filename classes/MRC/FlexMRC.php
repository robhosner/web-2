<?php

// I AM MRC FOR FLEX

require_once 'db.php';
require_once 'Ultra/Lib/Services/FamilyAPI.php';
require_once 'Ultra/Lib/Util/Redis.php';
// require_once 'classes/MRC/MRC.php';
require_once 'Ultra/Lib/Billing/functions.php';

class FlexMRC // extends MRC
{
  protected $redisFundingLockKey = 'flex/funding';
  protected $redisFundingLockTTL = 60;
  protected $maxLockedTime = 65;

  protected $redis;

  public $customer;
  public $familyInfo;
  public $lockResult;

  protected $debug = false;

  public function __construct()
  {
    // parent::__construct();

    if ( ! $this->redis)
      $this->redis = new \Ultra\Lib\Util\Redis;
  }

  /**
   * turns on features for debugging and testing
   * @param  Bool on/off
   * @return null
   */

  public function setDebug($to = true)
  {
    $this->debug = $to;
  }

  public function setRedisLockTime($ttl, $maxTime)
  {
    $this->redisFundingLockTTL = $ttl;
    $this->maxLockedTime = $maxTime;
  }

  /**
   * returns redis lock key
   * @return string
   */
  public function getRedisLockKey()
  {
    return "{$this->redisFundingLockKey}/{$this->familyInfo['parentCustomerId']}";
  }

  /**
   * locks parent customer id for funding
   * @return null
   */
  public function lockFunding()
  {
    $this->redis->setnx($this->getRedisLockKey(), 1, $this->redisFundingLockTTL);
  }

  /**
   * unlocks parent customer id from funding
   * @return null
   */
  public function unlockFunding()
  {
    $this->redis->del($this->getRedisLockKey());
  }

  /**
   * checks if parent customer id is locked for funding
   * @return null
   */
  public function isFundingLocked()
  {
    return $this->redis->get($this->getRedisLockKey());
  }

  /**
   * locked for funding process
   * @return null
   */
  public function fundingLock()
  {
    $timeStarted = time();
    $heartbeat = 0;

    while ($this->isFundingLocked())
    {
      sleep(1);
      $heartbeat++;

      \logInfo("Waiting for funding to be unblocked: $heartbeat");

      $timeNow = time();
      if ($timeNow - $timeStarted > $this->maxLockedTime)
        throw new \Exception('ESCALATION FLEX timed out waiting for funding scenario to be unblocked');
    }
  }

  /**
   * process flex customer running through MRC
   * @param  Object customer
   * @return Result
   */
  public function processCustomer($customer, $lockResult)
  {
    $processResult = [
      'success' => false,
      'errors'  =>[],
      'transient_errors' => [],
      'rejection_errors' => []
    ];

    try
    {
      $this->setCustomer($customer);
      $this->setLockResult($lockResult);
      $this->getFamilyInfo($this->customer->customer_id);

      // will wait to become unlocked
      $this->fundingLock();

      $processResult = $this->processCharge();
      if ( ! $processResult['success'])
        throw new \Exception('FLEX ESCALATION error processing customer charge');

      $transitionResult = $this->hasLineCredit($this->customer->customer_id)
        ? $this->renewCustomer()
        : $this->suspendCustomer();

      // if something is wrong with result or transition failed
      if ( $transitionResult && ! $transitionResult->is_success())
        throw new \Exception('FLEX ESCALATION error transitioning customer state');

      $processResult['success'] = true;
    }
    catch (\Exception $e)
    {
      $error = $e->getMessage();
      \logError($error);
      $processResult['errors'][] = $error;
    }

    return $processResult;
  }

  /**
   * sets the customer object
   * @param Object customer
   * @return null
   */
  public function setCustomer($customer)
  {
    $this->customer = $customer;
  }

  /**
   * sets the lockResult array
   * @param Array lock result for MRC
   * @return null
   */
  public function setLockResult($lockResult)
  {
    $this->lockResult = $lockResult;
  }

  /**
   * sets the familyInfo array
   * @param  Array
   * @return null
   */
  public function setFamilyInfo($familyInfo)
  {
    $this->familyInfo = $familyInfo;
  }

  /**
   * retrives flex family info
   * sets to member familyInfo
   * @param  Int customer_id
   * @return Array family info
   */
  public function getFamilyInfo($customer_id)
  {
    $familyInfo = \Ultra\Lib\Flex::getInfoByCustomerId($customer_id);
    if ( ! $familyInfo)
      throw new \Exception('ESCALATION FLEX Failed to retrieve customer family info');

    $this->setFamilyInfo($familyInfo);

    return $familyInfo;
  }

  /**
   * get customer using getCustomerFromCustomerIdForMRCProc stored proc
   * @param  Int
   * @return Object customer
   */
  public function getCustomer($customer_id)
  {
    $customer = getCustomerFromCustomerIdForMRCProc($customer_id);
    if ( ! $customer)
      throw new \Exception("ESCALATION FLEX error pulling customer info for $customer_id");

    return $customer;
  }

  /**
   * transfers a credit from parent customer to customer_id
   * @param  Int customer_id to transfer to
   * @return null
   */
  public function transferLineCredit()
  {
    $lineCredits = new \LineCredits();

    // transfer plan credit, fail on errors or warnings
    list ($transferResult, $error_code) = $lineCredits->doTransfer(
      $this->familyInfo['parentCustomerId'],
      $this->customer->customer_id,
      1
    );

    if ( ! $transferResult->is_success())
      \logError('ESCALATION FLEX error transferring line credit');

    $this->familyInfo['customerPlanCredits'] += 1;

    teldata_change_db();

    return $transferResult->is_success();
  }

  /**
   * do charge processing
   * @param  Object customer
   * @return Array
   */
  public function processCharge()
  {
    $chargeResult = [
      'success' => true,
      'errors'  => [],
      'transient_errors' => [],
      'rejection_errors' => []
    ];

    if ($this->shouldProcessChargeFamily())
      $this->processChargeFamily();

    if ($this->shouldTransferLineCredit())
      $this->transferLineCredit();

    $costs = $this->getCostToRenewCustomerPlan();

    if ($this->shouldProcessChargeMember())
    {
      // total cost of 1 line credit + bolts
      // if ( ! $this->hasSufficientFunds($costs['plan']))
        $chargeResult = $this->processChargeCustomer($costs);
    }
    else
    {
      // if ($this->hasSufficientFunds($costs['plan']))
      // {
      //   $this->spendFromBalance($costs['plan'], $this->customer);
      //   $this->addLineCredits($this->customer->customer_id, 1);
      // }
    }

    return $chargeResult;
  }

  /**
   * checks if customer being processed is in a family > 1
   * @return null
   */
  public function isPartOfFamily()
  {
    return ($this->familyInfo['memberCount'] > 0);
  }

  /**
   * return if should process charge for family
   * @return Bool
   */
  public function shouldProcessChargeFamily()
  {
    return ( ! $this->familyInfo['customerPlanCredits']
          && ! $this->familyInfo['planCredits']
          &&   $this->familyInfo['payForFamily']
          &&   $this->isPartOfFamily()
          && ( ! $this->customer->monthly_cc_renewal && ! $this->familyInfo['parentCustomerId'] != $this->customer->customer_id )
    );
  }

  /**
   * returns if should transfer line credit
   * @return Bool
   */
  public function shouldTransferLineCredit()
  {
    return ( ! $this->familyInfo['customerPlanCredits']
          &&   $this->familyInfo['planCredits']
          &&   $this->familyInfo['payForFamily']
          && ! $this->customer->monthly_cc_renewal
    );
  }

  /**
   * returns true if should charge customer being processed
   * @return Bool
   */
  public function shouldProcessChargeMember()
  {
    return ! $this->familyInfo['customerPlanCredits'] && $this->customer->monthly_cc_renewal;
  }

  /**
   * returns cost to renew customer being processed
   * @return Array of costs
   */
  public function getCostToRenewCustomerPlan()
  {
    $costs = [];

    $costs['plan']    = ( ! $this->familyInfo['customerPlanCredits']) ? \Ultra\Lib\Flex::costForCredits(1) : 0;
    // $costs['boltOns'] = \subscriber_owed_amount_recurring_bolt_ons($this->customer->customer_id);
    $costs['total']   = $costs['plan']; // + $costs['boltOns'];

    return $costs;
  }

  /**
   * return if customer stored_value is >= total cost
   * @param  Array of costs
   * @return Bool
   */
  public function hasSufficientFunds($cost)
  {
    return ($this->customer->balance * 100 >= $cost);
  }

  public function addLineCredits($customer_id, $amount)
  {
    $lineCredits = new \LineCredits();
    $lineCredits->incrementLineCredit($customer_id, $amount);
    teldata_change_db();
  }

  /**
   * process charge for family
   * @return Array charge result
   */
  public function processChargeFamily()
  {
    $result = new \Result();

    try
    {
      $creditsRequiredByMembers = $this->calculateRequiredPlanCredits();

      if ($creditsRequiredByMembers > 0)
      {
        $parentCustomer = $this->getCustomer($this->familyInfo['parentCustomerId']);
        if ( ! $parentCustomer)
          throw new \Exception('ESCALATION FLEX error getting parent family info');

        $cost = \Ultra\Lib\Flex::costForCredits($creditsRequiredByMembers);
        $chargeResult = $this->doCharge($parentCustomer, $cost, $cost, $creditsRequiredByMembers);

        if ( ! $chargeResult['success'])
          throw new \Exception('ESCALATION FLEX error charging parent for family');

        $this->addLineCredits($this->familyInfo['parentCustomerId'], $creditsRequiredByMembers);
        $this->addBonusData($creditsRequiredByMembers);
      }

      $this->getFamilyInfo($this->customer->customer_id);

      $result->succeed();
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
    }

    return $result;
  }

  /**
   * adds bonus data per purchased line credit amount
   * @param  Int line credits purchased
   * @return null
   */
  public function addBonusData($creditsPurchased)
  {
    if ( ! $creditsPurchased)
      return;

    $map = $this->getBonusDataMap();

    $mapLength = count($map);

    // if credits purchased > max amount
    // give max amount
    if ($creditsPurchased > $mapLength)
      $creditsPurchased = $mapLength;

    $result = \Ultra\Lib\Flex::addBoltOn(
      $this->familyInfo['parentCustomerId'],
      $map[$creditsPurchased]
    );

    if ( ! $result->is_success())
      \logError('ESCALATION FLEX error adding bonus data for line credits');
  }

  /**
   * gets bonus data bolt on map
   * @return Array credits purchased / bolt on
   */
  public function getBonusDataMap()
  {
    return [
      1 => 'SHAREDDATA_250_0',
      2 => 'SHAREDDATA_500_0',
      3 => 'SHAREDDATA_750_0',
      4 => 'SHAREDDATA_1024_0'
    ];
  }

  /**
   * process charge for a single customer
   * @param  Array costs
   * @return Array charge result
   */
  public function processChargeCustomer($costs)
  {
    $chargeResult = $this->doCharge($this->customer, $costs['total'], $costs['plan'], 1);

    if ( ! $chargeResult)
      throw new \Exception('ESCALATION FLEX error processing customer charge');

    $this->addLineCredits($this->customer->customer_id, 1);
    $this->addBonusData(1);

    return $chargeResult;
  }

  /**
   * executes charge on customer at cost
   * on successful charge, executes spend from balance
   * @param  Object
   * @param  Int
   * @return Array
   */
  public function doCharge($customer, $cost, $costToSpend = 0, $amtCreditsPurchased = 1)
  {
    $transactionType = "FLEX_RECHARGE_$amtCreditsPurchased";
    $chargeResult = \Ultra\Lib\Billing\processCustomerCharge($customer, $cost, $this->lockResult, $transactionType);

    if ($chargeResult['success'] && $costToSpend > 0)
      $this->spendFromBalance($costToSpend, $customer);

    if (!$chargeResult['success'] && count($chargeResult['rejection_errors']) > 0) {
      if ($customer->customer_id == $this->familyInfo['parentCustomerId'] && $customer->monthly_cc_renewal && $this->familyInfo['payForFamily']) {
        (new \Ultra\Messaging\Messenger())->enqueueImmediateSms(
          $customer->customer_id,
          'flex_parent_pay_for_family_on_auto_recharge_fails',
          ['login_token' => \Session::encryptToken($customer->current_mobile_number)]
        );
      }

      if ($customer->customer_id != $this->familyInfo['parentCustomerId'] && $customer->monthly_cc_renewal && !$this->familyInfo['payForFamily']) {
        (new \Ultra\Messaging\Messenger())->enqueueImmediateSms(
          $customer->customer_id,
          'flex_child_pay_for_family_off_auto_recharge_fails',
          ['login_token' => \Session::encryptToken($customer->current_mobile_number)]
        );
      }
    }

    return $chargeResult;
  }

  /**
   * performs spend from balance for cost in cents
   * @param  Int cost in cents
   * @return null
   */
  public function spendFromBalance($totalCost, $customer)
  {
    // this function would not retrieve updated balance
    $result = func_spend_from_balance([
      'amount'    => $totalCost / 100,
      'reason'    => 'Reactivation',
      'reference' => __FUNCTION__,
      'source'    => 'SPEND',
      'detail'    => 'Reactivation',
      'customer'  => $this->getCustomer($customer->customer_id)
    ]);

    if ( ! $result['success'])
    {
      if (count($result['errors']))
        \logError($result['errors'][0]);

      \logError('FLEX ESCALATION error spending from balance');
    }
  }

  /**
   * returns if chargeResult should renew customer
   * @param  Array
   * @return Bool
   */
  public function shouldChargeResultRenew($chargeResult)
  {
    return $chargeResult['success'];
  }

  /**
   * returns if chargeResult should suspend customer
   * chargeResult not successful
   * chargeResult doesn't have errors
   * chargeResult has error in getInsufficientFundsErrorCodes
   * @param  Array
   * @return Bool
   */
  public function shouldChargeResultSuspend($chargeResult)
  {
    if ($chargeResult['success'])
      return false;

    if ( ! count($chargeResult['errors']) && ! count($chargeResult['transient_errors']))
      return true;

    if ( count($chargeResult['transient_errors'])
      && in_array($chargeResult['transient_errors'][0], \Ultra\UltraConfig\getInsufficientFundsErrorCodes())
    ) return true;

    return false;
  }

  public function hasLineCredit($customer_id)
  {
    $lineCredits = new \LineCredits();
    $has = $lineCredits->hasLineCredit($customer_id);
    teldata_change_db();
    return $has;
  }

  /**
   * checks if each member is renewing today
   * return array of members renewing today
   * @param  Array members
   * @return Array members renewing today
   */
  public function getMembersRenewingToday()
  {
    $membersRenewingToday = [];

    $dateToday = date('Y-m-d', time());

    foreach ($this->familyInfo['members'] as $member)
    {
      $customer = $this->getCustomer($member['customerId']);
      if ( ! $customer)
      {
        \logError("ESCALATION FLEX: error pulling family member {$member['customer_id']}");
        continue;
      }

      $renewDate = date('Y-m-d', strtotime($customer->plan_expires));
      if ($renewDate == $dateToday && ! $customer->monthly_cc_renewal)
        $membersRenewingToday[] = $member;
    }

    return $membersRenewingToday;
  }

  /**
   * checks each members line credit balance
   * return i++ if member has < 1 line credit
   * @param  Array members
   * @return Int
   */
  public function calculateRequiredPlanCredits()
  {
    $creditsRequired = 0;

    $membersRenewingToday = $this->getMembersRenewingToday();

    $lineCredits = new \LineCredits();

    foreach ($membersRenewingToday as $member)
    {
      if ($lineCredits->getBalance($member['customerId']) < 1)
        $creditsRequired++;
    }

    teldata_change_db();

    $needed = $creditsRequired - $this->familyInfo['planCredits'];

    return ($needed > 0) ? $needed : 0;
  }

  /**
   * enqueues flex renewal transition for customer
   * @return Result
   */
  public function renewCustomer()
  {
    $result = new \Result();

    try
    {
      $renewResult = transition_customer_state($this->customer, 'Flex Monthly Renewal');
      if (count($renewResult['errors']))
        throw new \Exception('ESCALATION FLEX error transitioning customer state Renew');

      // decrement line credit
      $lineCredits = new \LineCredits();
      list($result, $error) = $result = $lineCredits->useLineCredit($this->customer->customer_id, false);
      teldata_change_db();

      if ( ! $result->is_success())
        \logError('FLEX ESCALATION error decrementing line credit');

      $result->succeed();
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * enqueues suspend transition for customer
   * @return Result
   */
  public function suspendCustomer()
  {
    $result = new \Result();

    try
    {
      $suspendResult = suspend_account($this->customer, ['customer_id' => $this->customer->customer_id]);
      if (count($suspendResult['errors']))
        throw new \Exception('ESCALATION FLEX error transitioning customer state Suspend');

      $result->succeed();
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
      $result->add_error($e->getMessage());
    }

    return $result;
  }
}
