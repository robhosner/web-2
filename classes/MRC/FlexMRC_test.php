<?php

require_once 'classes/MRC/FlexMRC.php';

class FlexMRCTest
{
  protected $flexMRC;

  public function __construct()
  {
    teldata_change_db();

    $this->flexMRC = new FlexMRC();
    $this->flexMRC->setDebug(true);

    $this->setCustomer(34526);
    $this->getFamilyInfo(34526);

    // $this->lockFunding();

    $this->setLockResult(['lock_id' => 1, 'attempt_count' => 1, 'status' => 'TODO']);

    $this->shouldProcessChargeFamily();
    $this->shouldTransferLineCredit();
    $this->shouldProcessChargeMember();

    // $this->transferLineCredit();

    $this->isPartOfFamily();
    $this->getCostToRenewCustomerPlan();
    $this->hasSufficientFunds();

    // $this->flexMRC->customer->monthly_cc_renewal = 1;
    // $this->processCustomer();
    // $this->processCharge();
    // $this->processChargeFamily();
    // $this->processChargeCustomer();
    // $this->spendFromBalance();

    $chargeResult = [
      'success' => true,
      'errors' => [],
      'transient_errors' => [],
      'rejection_errors' => []
    ];
    $this->shouldChargeResultRenew($chargeResult);
    $this->shouldChargeResultSuspend($chargeResult);

    $this->getMembersRenewingToday();
    $this->calculateRequiredPlanCredits();

    // $this->renewCustomer();
    // $this->suspendCustomer();
  }

  public function logConsole($str)
  {
    echo PHP_EOL . $str . PHP_EOL;
  }

  public function getFamilyInfo($customer_id)
  {
    $familyInfo = $this->flexMRC->getFamilyInfo($customer_id);
    print_r($this->flexMRC->familyInfo);
    $this->flexMRC->setFamilyInfo($familyInfo);
    print_r($this->flexMRC->familyInfo);
  }

  public function setCustomer($customer_id)
  {
    $customer = $this->flexMRC->getCustomer($customer_id);
    $this->flexMRC->setCustomer($customer);
    print_r($this->flexMRC->customer);
  }

  public function lockFunding()
  {
    try
    {
      $this->flexMRC->setRedisLockTime(5, 10);
      $this->flexMRC->lockFunding();
      $this->flexMRC->fundingLock();
      $this->flexMRC->unlockFunding();

      $this->flexMRC->setRedisLockTime(10, 5);
      $this->flexMRC->lockFunding();
      $this->flexMRC->fundingLock();
      $this->flexMRC->unlockFunding();
    }
    catch (\Exception $e)
    {
      $this->logConsole($e->getMessage());
    }
  }

  public function processCustomer()
  {
    return;
  }

  public function processCharge()
  {
    return;
  }

  public function processChargeFamily()
  {
    return;
  }

  public function processChargeCustomer()
  {
    return;
  }

  public function addBonusData()
  {
    return;
  }

  public function getBonusDataMap()
  {
    return;
  }

  public function spendFromBalance()
  {
    return;
  }

  public function setLockResult($lockResult)
  {
    $this->flexMRC->setLockResult($lockResult);
    print_r($this->flexMRC->lockResult);
  }

  public function isPartOfFamily()
  {
    $this->logConsole('Is part of family: ' . $this->flexMRC->isPartOfFamily());
  }

  public function shouldProcessChargeFamily()
  {
    $this->logConsole('Should process charge family: ' . $this->flexMRC->shouldProcessChargeFamily());
  }

  public function shouldTransferLineCredit()
  {
    $this->logConsole('Should transfer line credit: ' . $this->flexMRC->shouldTransferLineCredit());
  }

  public function shouldProcessChargeMember()
  {
    $this->logConsole('Should process charge member: ' . $this->flexMRC->shouldProcessChargeMember());
  }

  public function getMembersRenewingToday()
  {
    print_r($this->flexMRC->getMembersRenewingToday());
  }

  public function calculateRequiredPlanCredits()
  {
    $members = $this->flexMRC->familyInfo['members'];
    $this->logConsole('Plan credits required: ' . $this->flexMRC->calculateRequiredPlanCredits($members));
  }

  public function transferLineCredit()
  {
    $this->flexMRC->transferLineCredit();
  }

  public function suspendCustomer()
  {
    $this->flexMRC->suspendCustomer();
  }

  public function renewCustomer()
  {
    $this->flexMRC->renewCustomer();
  }

  public function shouldChargeResultRenew($chargeResult)
  {
    $x = $this->flexMRC->shouldChargeResultRenew($chargeResult);
    $this->logConsole('Charge result should renew: ' . $x);
  }

  public function shouldChargeResultSuspend($chargeResult)
  {
    $x = $this->flexMRC->shouldChargeResultSuspend($chargeResult);
    $this->logConsole('Charge result should suspend: ' . $x);
  }

  public function getCostToRenewCustomerPlan()
  {
    $x = $this->flexMRC->getCostToRenewCustomerPlan();
    print_r($x);
    $this->logConsole('Cost to renew customer plan: ' . $x['total']);
  }

  public function doCharge($customer, $cost)
  {
    $resut = $this->flexMRC->doCharge($customer, $cost);
  }

  public function fundingLock()
  {
    $this->logConsole($this->flexMRC->getRedisLockKey());
    $this->flexMRC->lockFunding();

    try
    {
      $this->flexMRC->fundingLock();
    }
    catch (\Exception $e)
    {
      $this->logConsole('threw ' . $e->getMessage());
    }
  }

  public function hasSufficientFunds()
  {
    $total = 30;
    $this->setCustomer(31);

    $this->flexMRC->customer->stored_value = $total - 1;
    print_r($this->flexMRC);
    $x = $this->flexMRC->hasSufficientFunds($total * 100);
    $this->logConsole("Has sufficient funds: $x");

    $this->flexMRC->customer->stored_value = $total + 1;
    $x = $this->flexMRC->hasSufficientFunds($total * 100);
    $this->logConsole("Has sufficient funds: $x");
  }
}

$test = new FlexMRCTest();
