<?php

require_once 'db.php';
require_once 'classes/UltraException.php';

try
{
  throw new \UltraException( 'test_raf' , 0 , NULL , 'ETEST01' );
}
catch( \Exception $e )
{
  if ( is_a( $e , 'UltraException' ) )
  { echo "A\n"; echo $e->get_error_code()."\n"; }
  else
  { echo "B\n"; }
}

try
{
  throw new \Exception( 'test_raf' );
}
catch( \Exception $e )
{
  if ( is_a( $e , 'UltraException' ) )
  { echo "A\n"; echo $e->get_error_code()."\n"; }
  else
  { echo "B\n"; }
}

