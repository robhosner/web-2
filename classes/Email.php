<?php

/**
 * Electronic Mail Messenging (aka Email)
 *
 * email messenging class for HTML template based mail with delivery via third party services
 * similar to a traditional UNIX spool mail service this class allows to create a new message
 * and queue it for later delivery
 *
 * a separate background runner handles the messages queue by delivering messages them
 * via a third party service, such as mandrill; it typically takes several seconds
 * to send a message via a third party therefore this function is delegated to a separate process
 * in order to avoid application blocking
 *
 * for the sake of simplicity and consistency most public methods return a single error string:
 * NULL on success or a dynamic error message in English on failure;
 * the error message is *not* user-facing because it is technical, English-only and includes offending values
 *
 * basic application usage example:
 *   $mail = new Email();
 *   $params = array('date => '2/16/16', 'name' => 'Ulra Personal', 'cost' => '$19');
 *   if ($error = $mail->message('customer@yahoo.com', 'monthly_renewal', 'UNIVISION', 'ES', $params))
 *     handle error: 'unable to initialize email message';
 *   if ($error = $mail->queue())
 *     handle error: 'unable to queue message';
 *
 * @author VYT, Jan 2016
 */

require_once '/opt/php/vendor/mandrill/src/Mandrill.php';
require_once 'Ultra/Lib/Util/Redis.php';

class Email
{
  // redis keys
  const TEMPLATE_KEY  = 'email/template/';  // specific template definition
  const TEMPLATE_TTL  = SECONDS_IN_DAY;     // template definition life
  const QUEUE_KEY     = 'email/queue';      // queue of message IDs
  const QUEUE_TTL     = SECONDS_IN_DAY;     // queue life
  const QUEUE_SIZE    = 1000;               // max number of messages in the queue after which we reject additional messages
  const MESSAGE_KEY   = 'email/message/';   // individual message data
  const MESSAGE_TTL   = SECONDS_IN_DAY;     // individual message life

  // class data
  private $redis;       // redis instance
  private $mandrill;    // mandrill service provider instance
  private $token;       // provider authenticaiton token

  // message data
  private $id;          // message ID in queue
  private $recipient;   // recipient email address
  private $template;    // email template name: NAME_BRAND_LANGUAGE
  private $definition;  // template definition (attributes, including source code)
  private $parameters;  // template parameters (handlebars values)


  /**
   * constructor
   */
  public function __construct()
  {
    // class initialization
    $this->redis = new Ultra\Lib\Util\Redis;

    // get mandrill API key
    if ( ! $path = find_credential('ultra/mandrill/api/keyfile'))
      return logError('missing mandrill API key file');
    if ( ! $this->token = trim(file_get_contents($path)))
      return logError('missing mandrill API key');
  }


  /**
   * initialize
   *
   * initialize email message structure based on a specific template
   * @param String recipient email address (only one email per message supported)
   * @param String template name
   * @param String brand name
   * @param String two-character language code
   * @param Array associative template values (template handlebars)
   * @return String NULL on success or error message on failure
   */
  public function initialize($recipient, $template, $brand, $language, $parameters)
  {
    logInfo("parameters: " . json_encode(func_get_args()));

    try
    {
      if ( ! $this->token)
        throw new Exception('failed class initialization');

      // validate recipient
      if (filter_var($recipient, FILTER_VALIDATE_EMAIL) === FALSE)
        throw new Exception("invalid recipient address {$recipient}");
      $this->recipient = $recipient;

      // validate template: NAME_BRAND_LANGUAGE
      $this->template = implode('_', array(strtolower($template), strtolower($brand), strtolower($language)));
      if ( ! $this->definition = $this->getTemplateDefinition($this->template))
        throw new Exception("invalid template '$template', brand '$brand' or language '$language'");

      // validate parameters
      foreach ($parameters as $parameter => $value)
        if ( ! strstr($this->definition->code, '{{' . $parameter . '}}'))
          throw new Exception("unknown parameter $parameter");
      $this->parameters = $parameters;

      // add message UUID
      $this->id = getNewUUID('EM'); // Electronic Mail
    }

    catch (Exception $exp)
    {
      // add source info if thrown elsewhere
      logError(($exp->getFile() == __FILE__ ? NULL : get_class($exp) . ': ') . $exp->getMessage());
      return $exp->getMessage();
    }
  }


  /**
   * queue
   *
   * add a previously initialized message to queue
   * @return String NULL on success or error message on failure
   */
  public function queue()
  {
    logInfo("parameters: " . json_encode(func_get_args()));

    try
    {
      if ( ! $this->token)
        throw new Exception('failed class initialization');

      // check number of pending message in the queue
      $length = (integer)$this->redis->llen(self::QUEUE_KEY);
      logInfo("queue length: $length");
      if ($length > self::QUEUE_SIZE)
        throw new Exception('message rejected: queue is full');

      // save message to redis
      if ( ! $this->redis->set(self::MESSAGE_KEY . $this->id, $this->packMessage(), self::MESSAGE_TTL))
        throw new Exception("failed to save message {$this->id} to redis");

      // add message ID to queue
      if ( ! $this->redis->lpush(self::QUEUE_KEY, $this->id))
      {
        $this->redis->del(self::MESSAGE_KEY . $this->id);
        throw new Exception("failed to add message {$this->id} to redis queue");
      }

      // success
      return NULL;
    }

    catch (Exception $exp)
    {
      // add source info if thrown elsewhere
      logError(($exp->getFile() == __FILE__ ? NULL : get_class($exp) . ': ') . $exp->getMessage());
      return $exp->getMessage();
    }
  }


  /**
   * next
   *
   * retrieve next pending message ID from queue
   * @return String message ID or NULL if no messages found in queue
   */
  public function next()
  {
    try
    {
      if ( ! $this->token)
        throw new Exception('failed class initialization');

      // get message from the head of the queue
      if ( ! $id = $this->redis->rpop(self::QUEUE_KEY))
        throw new Exception('no messages in queue');

      // success
      logInfo("found message $id");
      return $id;
    }

    catch (Exception $exp)
    {
      // add source info if thrown elsewhere
      logError(($exp->getFile() == __FILE__ ? NULL : get_class($exp) . ': ') . $exp->getMessage());
      return NULL;
    }

  }


  /**
   * process
   *
   * send message to third party for delivery
   * currently we support mandrill but other provides can be added
   * @return String NULL on success or error message on failure
   */
  public function process($id)
  {
    try
    {
      if ( ! $this->token)
        throw new Exception('failed class initialization');

      // get message data and initialize ourselves with it
      if ( ! $data = $this->redis->get(self::MESSAGE_KEY . $id))
        throw new Exception("missing data for message $id");
      $this->redis->del(self::MESSAGE_KEY . $id);
      $this->unpackMessage($data);

      // validate ourselves to ensure proper initialization
      if ( ! ($this->id && $this->recipient && $this->template))
        throw new Exception('missing message initialization data');

      // send via default provider
      if ($error = $this->sendMandrillMessage())
        throw new Exception($error);

      // sucesss
      return logInfo("message {$this->id} successfully sent");
    }

    catch (Exception $exp)
    {
      // add source info if thrown elsewhere
      logError(($exp->getFile() == __FILE__ ? NULL : get_class($exp) . ': ') . $exp->getMessage());
      return $exp->getMessage();
    }

  }


  /**
   * packMessage
   *
   * pack message data into a json encoded array for storage
   * @return String
   */
  private function packMessage()
  {
    return json_encode(array($this->id, $this->recipient, $this->template, $this->parameters));
  }


  /**
   * unpackMessage
   *
   * unpack message data after storage
   * @param String encoded message data
   */
  private function unpackMessage($data)
  {
    list($this->id, $this->recipient, $this->template, $this->parameters) = json_decode($data, TRUE);
  }


  /**
   * getTemplateDefinition
   *
   * retrieve template definition from service provider and cache in redis
   * currently we support mandrill but other provides can be added
   * @param String template name
   * @return Object on success or NULL on failure
   */
  private function getTemplateDefinition($template)
  {
    // get template definition from redis
    $key = self::TEMPLATE_KEY . $template;
    if ($definition = $this->redis->get($key))
      return json_decode($definition);

    // get template description from provider
    logInfo("loading template $template from provider");
    if ( ! $this->mandrill)
      $this->mandrill = new \Mandrill($this->token);
    $definition = (object)$this->mandrill->templates->info($template);
    if (empty($definition))
      return NULL;

    // cache and return template
    $this->redis->set($key, json_encode($definition), self::TEMPLATE_TTL);
    return $definition;
  }


  /**
   * sendMandrillMessage
   *
   * send message to mandrill for delivery
   * @return String NULL on success or error message on failure
   * @throws Exception
   */
  private function sendMandrillMessage()
  {
    if ( ! $this->mandrill)
      $this->mandrill = new \Mandrill($this->token);

    // expland template parameters into handlebars syntax
    $merge = array();
    foreach ($this->parameters as $name => $value)
      $merge[] = array('name' => $name, 'content' => $value);

    // simplified mandrill message structure
    $message = array(
      'to'                => array(array('email' => $this->recipient, 'type' => 'to')),
      'merge_language'    => 'handlebars',
      'track_opens'       => FALSE,
      'track_clicks'      => FALSE,
      'global_merge_vars' => $merge
    );

    // additional API parameters
    $content = array();
    $async = TRUE;

    // send for delivery
    logInfo("outbound template: {$this->template}, message: " . json_encode($message));
    $result = $this->mandrill->messages->sendTemplate($this->template, $content, $message, $async);
    logInfo('mandrill result: ' . json_encode($result));

    // check result: we may get non-delivery
    if (empty($result[0]['status']) || $result[0]['status'] != 'sent')
      return "failed to send message {$this->id}: {$result[0]['status']}";

    // success
    return NULL;
  }
}
