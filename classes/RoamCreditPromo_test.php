<?php
require_once 'core.php';
require_once 'cosid_constants.php';
require_once 'db.php';

require_once 'classes/RoamCreditPromo.php';

class RoamCreditPromoTest
{
  public $roamCreditPromo = null;

  public function __construct()
  {
    $this->roamCreditPromo = new \Ultra\Lib\RoamCreditPromo();

    $this->addOnFeatureInfo = [];

    $this->addOnFeatureInfo[] = json_decode('{
      "AutoRenewalIndicator":"false",
      "FeatureDate":"2016-09-26T12:54:34",
      "FeatureID":"12814",
      "ExpirationDate":"2020-03-22T00:00:00",
      "UltraDescription":"N-ROAM-SMPGLB",
      "UltraServiceName":"N-ROAM-SMPGLB"
    }');

    $this->addOnFeatureInfo[] = json_decode('{
      "AutoRenewalIndicator":"false",
      "FeatureDate":"2016-09-26T12:54:34",
      "FeatureID":"12707",
      "ExpirationDate":"2020-03-22T00:00:00",
      "UltraDescription":"N-ROAM-SMPGLB",
      "UltraServiceName":"N-ROAM-SMPGLB"
    }');
  }

  public function test()
  {
    $p1 = (object)['roam_credit_promo' => 3];
    $roamCreditPromoParam = \Ultra\Lib\RoamCreditPromo::getParam();

    $p2 = [$roamCreditPromoParam => $p1->{$roamCreditPromoParam}];
    print_r($p2);

    $t = strtotime('11/19/2016');
    echo date('Y-m-d', $t);
    echo PHP_EOL;
    echo date('Y-m-d', strtotime('+1 month', $t));
    echo PHP_EOL;

    echo PHP_EOL;

    echo 'Activated Before Date: ' . $this->roamCreditPromo->activatedBeforeDate;
    echo PHP_EOL;

    $this->roamCreditPromo->setActivationDate('Aug 27 2016 05:56:38:840PM');
    $this->roamCreditPromo->setCosId(98276);

    echo 'Activated Date: ' . $this->roamCreditPromo->activationDate;
    echo PHP_EOL;

    $credit = $this->roamCreditPromo->determineRoamCreditAmount(98276);
    echo 'Credit Ultra 49: ' . $credit;
    echo PHP_EOL;

    $params = ['exit' => 1];
    $this->roamCreditPromo->addMakeitsoParam($params);

    $params['brand_id'] = 1;

    print_r($this->roamCreditPromo->proccesParamsWithAddOnFeatureInfo($params, []));

    $params['PlanData']['AddOnFeatureList']['AddOnFeature'] = [];
    print_r($this->roamCreditPromo->proccesParamsWithAddOnFeatureInfo($params, $this->addOnFeatureInfo));
  }
}

$rcpt = new RoamCreditPromoTest();
$rcpt->test();
