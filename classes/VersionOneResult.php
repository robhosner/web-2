<?php

class VersionOneResult
{
  protected $success   = false;
  protected $warnings = array();
  protected $errors   = array();

  protected $p;
  protected $api;
  protected $args = array();
  protected $data = array();

  public function __construct($p = null, $api = null)
  {
    $this->setP($p);
    $this->setApi($api);
  }

  /**
   * Makes result success if !count(errors)
   * @return null
   */
  public function succeed()
  {
    if ( ! count($this->errors))
      $this->success = true;
  }

  /**
   * Returns if successful
   * @return Boolean
   */
  public function isSuccess()
  {
    return $this->success;
  }

  /**
   * Sets $p
   * @param global $p
   * @return null
   */
  public function setP($p)
  {
    $this->p = $p;
  }

  /**
   * Name of API
   * @param String $api
   * @return null
   */
  public function setApi($api)
  {
    $this->api = $api;
  }

  /**
   * Input arguments for API
   * @param Array $args
   * @param global $mock
   * @return null
   */
  public function setArgs($args, $mock)
  {
    $this->args = $args;

    $valParamsErrs = validate_params($this->p, $this->api, $this->args, $mock);
    $this->errors = array_merge($this->errors, $valParamsErrs);
  }

  /**
   * Adds warning string to warning array
   * @param String $warning
   * @return null
   */
  public function addWarning($warning)
  {
    $this->warnings[] = $warning;
  }

  /**
   * Adds error string to error array
   * @param String $error
   * @return null
   */
  public function addError($error)
  {
    $this->errors[] = $error;
  }

  /**
   * Adds errors to error array
   * @param Array $errors
   * @return null
   */
  public function addErrors($errors)
  {
    $this->errors = array_merge($this->errors, $errors);
  }

  /**
   * returns count $errors
   * @return Integer
   */
  public function hasErrors()
  {
    return (count($this->errors)) ? true : false;
  }

  /**
   * Empties error array
   * @return null
   */
  public function clearErrors()
  {
    $this->errors = array();
  }

  /**
   * Add data to result data[key] = val
   * @param Mixed $key
   * @param Mixed $val
   * @return null
   */
  public function addData($key, $val)
  {
    $this->data[$key] = $val;
  }

  public function addDataArray($data)
  {
    $this->data = array_merge($this->data, $data);
  }

  /**
   * builds and returns JSON for API to return
   * @return String
   */
  public function getJSON()
  {
    $return = $this->getReturnArray();
    return flexi_encode(fill_return($this->p, $this->api, $this->args, $return));
  }

  /**
   * builds and returns the return array
   * @return Array
   */
  public function getReturnArray()
  {
    $return = array('success' => $this->success);

    if (count($this->warnings))
      $return['warnings'] = $this->warnings;

    if (count($this->errors))
      $return['errors'] = $this->errors;

    return array_merge($return, $this->data);
  }
}
