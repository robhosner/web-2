<?php

putenv("UNIT_TESTING=0");

include_once('lib/util-common.php');

/**
 * Send a POST requst using cURL
 * @param string $url to request
 * @param array $post values to send
 * @param array $options for cURL
 * @return string
 */
function curl_post($url,
                   array $post = NULL,
                   array $options = array(),
                   $postxml=NULL,
                   $seconds_timeout=4,
                   $echo=FALSE)
{
    global $request_id;
    $defaults = array(
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_URL => $url,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => $seconds_timeout,
#CURLOPT_SSL_VERIFYHOST => 0,
#CURLOPT_SSL_VERIFYPEER => 0,
    );

    if ($postxml)
    {
      $defaults[CURLOPT_POSTFIELDS] = $postxml;
      $defaults[CURLOPT_HTTPHEADER] = array('Content-Type: text/xml');
    }
    else
      $defaults[CURLOPT_POSTFIELDS] = http_build_query($post);

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    dlog("", $request_id . ' ' . "CURL POST = [$url] "
              . json_encode(($options + $defaults)));
    if( ! $result = curl_exec($ch))
      trigger_error(curl_error($ch));
    curl_close($ch);

    dlog("", $request_id . ' ' . "CURL POST RESULT = [$url] =>  " . $result);

    if ( $echo )
      echo "\n$result\n";

    return $result;
}

/**
 * Send a GET requst using cURL
 * @param string $url to request
 * @param array $get values to send
 * @param array $options for cURL
 * @return string
 */
function curl_get($url, array $get = NULL, array $options = array())
{
  global $request_id;

  $full = $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get);

  $defaults = array(
    CURLOPT_URL => $full,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_TIMEOUT => 4
    );

  dlog("", $request_id . ' ' . "CURL GET = [$full] "
            . json_encode(($options + $defaults)));
  $ch = curl_init();
  curl_setopt_array($ch, ($options + $defaults));
  if( ! $result = curl_exec($ch))
  {
    trigger_error(curl_error($ch));
  }
  curl_close($ch);

  dlog("", $request_id . ' ' . "CURL GET RESULT = [$full] =>  " . $result);
  return $result;
}

/* from http://stackoverflow.com/questions/1397036/how-to-convert-array-to-simplexml */
function to_xml(array $data, $root_name, $strip_xml_version=FALSE)
{
  // creating object of SimpleXMLElement
  $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><$root_name></$root_name>");

  // function call to convert array to xml
  array_to_xml($data, $xml);

  $to_xml = $xml->asXML();

  if ( $strip_xml_version )
  {
    $to_xml = preg_replace("/\<\?xml version\=\"[\d\.]+\"\?\>/",'',$to_xml);
  }

  //saving generated xml file

  $to_xml = preg_replace("/^[\n\r\s]+/",'',$to_xml);

  return $to_xml;
}

/* from http://stackoverflow.com/questions/1397036/how-to-convert-array-to-simplexml */
// function defination to convert array to xml
function array_to_xml($data, &$xml_data)
{
  foreach($data as $key => $value)
  {
    /* XML node names can't begin with a digit */
    if (is_numeric($key)) $key = "n$key";

    if (is_bool($value)) $value = $value ? 'true' : 'false';

    if(is_array($value))
    {
      if(!is_numeric($key))
      {
        $subnode = $xml_data->addChild("$key");
        array_to_xml($value, $subnode);
      }
      else
      {
        array_to_xml($value, $xml_data);
      }
    }
    else
    {
      $value = htmlentities($value, ENT_NOQUOTES, "UTF-8");

      $xml_data->addChild("$key", NULL == $value ? NULL : "$value");
    }
  }
}

/* public domain by info@noextratime.com on http://www.php.net/manual/en/function.explode.php */
function doubleExplode ($del1, $del2, $array)
{
  $array1 = explode("$del1", $array);
  foreach($array1 as $key=>$value)
  {
    $array2 = explode("$del2", $value);
    foreach($array2 as $key2=>$value2)
    {
      $array3[] = $value2;
    }
  }

  $afinal = array();
  for ( $i = 0; $i <= count($array3); $i += 2)
  {
    if($array3[$i]!="")
    {
      $afinal[trim($array3[$i])] = trim($array3[$i+1]);
    }
  }
  return $afinal;
}

function perform_asynch_command($params)
{
  $curl_options    = $params['curl_options'];
  $seconds_timeout = $params['seconds_timeout'];
  $curl_params     = $params['curl_params'];
  $url_request     = $params['url'] . '/pr/' . $params['partner'] . '/1/ultra/api/' . $params['namespace'] . '__request' . $params['command'];
  $url_verify      = $params['url'] . '/pr/' . $params['partner'] . '/1/ultra/api/' . $params['namespace'] . '__verify'  . $params['command'];

  $check_interval  = 5; # check $url_verify every $check_interval seconds

  $time_seconds_start = time();
  $time_seconds_end   = $time_seconds_start + $seconds_timeout;

  $result = array('success' => FALSE , 'errors' => array() );

# $curl_params['always'] = 'succeed'; # TEST

  $curl_result_request = curl_post($url_request,$curl_params,$curl_options,NULL,$seconds_timeout);

  if ( $curl_result_request )
  {
    $result_decoded = json_decode($curl_result_request);

    if ( isset($result_decoded->errors) && count($result_decoded->errors) )
    {
      $result['errors'] = array_merge( $result['errors'] , $result_decoded->errors );
    }
    else if ( ! $result_decoded->success )
    {
      $result['errors'][] = "Could not get a successful response from $url_request";
    }
    else if ( ! $result_decoded->request_id )
    {
      $result['errors'][] = "Could not get a request id from $url_request";
    }
    else
    {
      # we got a request_id from $url_request, now we need to ping $url_verify every $check_interval seconds

      $curl_verify_params = array(
        'request_id' => $result_decoded->request_id
      );

      $now = time();

      $failure = FALSE;
      $pending = TRUE;

      while (
        ( $now < $time_seconds_end )
        &&
        ( ! $failure )
        &&
        ( $pending )
      )
      {
        sleep($check_interval);

        $curl_result_request = curl_post($url_verify,$curl_verify_params,$curl_options,NULL,$seconds_timeout);

        if ( $curl_result_request )
        {
          $result_decoded = json_decode($curl_result_request);

          $pending = $result_decoded->pending;

          if ( isset($result_decoded->errors) && count($result_decoded->errors) )
          {
            $result['errors'] = array_merge( $result['errors'] , $result_decoded->errors );
            $failure = TRUE;
          }
          else if ( ! $result_decoded->success )
          {
            $result['errors'][] = "ERR_API_INTERNAL: Asynchronous request failure";
          }
          else if ( ! $pending )
          {
            $failure = FALSE;

            # we need to return all other attributes

            foreach( get_object_vars( $result_decoded ) as $field => $value )
            {
              if ( ( $field != 'errors' )
                && ( $field != 'partner_tag' )
                && ( $field != 'pending' )
                && ( $field != 'success' )
                && ( $field != 'ultra_trans_epoch' )
                && ( $field != 'request' )
                && ( $field != 'service_expires_epoch' ) )
              {
                $result[ $field ] = $value;
              }
            }
          }
        }

        $now = time();

      } # while loop

      $result['success'] = ! $failure;

    }
  }
  else
  {
    $result['errors'][] = "Could not get a response from $url_request";
  }

  return $result;
}

/**
 * callUltraApi
 *
 * make HTTP request to an Ultra API v2 or greater
 *
 * @param string API name
 * @param array API parameters: either array for JSON API call or XML string
 * @param int API version
 * @param string partner (if needed)
 * @param int maximum API execution time
 * @param int max connection time (should be less than 1 sec on LAN)
 * @param string domain: FQDN, e.g. 'administer.ultra.me'
 * @param string user: HTTP authentication user name
 * @param string password: HTTP authentication password
 * @param string cookie: HTTP cookies to provide with the request; e.g. 'zsession=KDJWE-8JDW0-M33ME-MN3NP'
 * @param string protocol: can be used to override default 'https' to 'http'
 * @param array headers: can be used to add custom HTTP headers to request
 * @param array options: override specific curl options
 * @return object API response or NULL on internal errors
 * @example: $result = callUltraApi('interactivecare__RequestProvisionOrangeBySMS', array('MSISDN' => '5555550000', 'zipcode' => '11211'), 2, 'interactivecare', 30, 1);
 */
function callUltraApi($api, $parameters = NULL, $version = 2, $partner = NULL, $api_runtime = 60, $connect_time = 1, $domain = NULL, $user = NULL, $password = NULL, $cookie = NULL, $protocol = NULL, $headers = array(), $options = array(), $cgi = NULL, $bath = NULL)
{
  $result = NULL;
  dlog('', '%s', func_get_args());

  try
  {
    // get API domain and credentials if not provided
    if ( ! $domain)
    {
      list($domain, $user, $password) = \Ultra\UltraConfig\ultra_api_internal_info();
      if (empty($domain))
        throw new \Exception("failed to get API DOMAIN: domain '$domain', user: '$user', password: '$password'");
    }

    // construct API URL
    $protocol = empty($protocol) ? 'https' : $protocol;
    $cgi = empty($cgi) ? ($version == 2 ? 'ultra_api.php' : 'partner.php') : $cgi;
    $bath = empty($bath) ? 'rest' : $bath;
    $version = empty($version) ? 2 : $version;
    $partner = $partner ? "&partner=$partner" : NULL;
    $api = empty($api) ? NULL : "&command=$api";
    $url = "$protocol://$domain/$cgi?bath=$bath&version=$version$partner$api";
    dlog('', "API URL $url");

    // initialize HTTP session
    if ( ! $connection = curl_init($url))
      throw new \Exception('failed to initialize API session');

    // configure session options
    $defaults = array(
      # CURLOPT_VERBOSE           => TRUE, // enable this to produce verbose debugging
      CURLOPT_HTTPAUTH          => CURLAUTH_BASIC,
      CURLOPT_USERPWD           => "$user:$password",
      CURLOPT_HEADER            => FALSE,
      CURLOPT_RETURNTRANSFER    => TRUE,
      CURLOPT_SSL_VERIFYPEER    => FALSE,
      CURLOPT_CONNECTTIMEOUT    => $connect_time,
      CURLOPT_TIMEOUT           => $api_runtime);
    foreach ($defaults as $option => $value)
      if ( ! isset($options[$option]))
        $options[$option] = $value;

    if (is_array($parameters)) // array of JSON parameters
    {
      $post = http_build_query($parameters);
      $options[CURLOPT_POST]        = TRUE;
      $options[CURLOPT_HTTPHEADER]  = array('Accept: application/json', 'Content-Length: ' . strlen($post));
      $options[CURLOPT_POSTFIELDS]  = $post;
      $format = 'JSON';
    }
    elseif (is_string($parameters) && ($parameters[0] = '<')) // XML string
    {
      dlog('', 'POST ' . strlen($parameters) . ' bytes of XML');
      $options[CURLOPT_POST]        = TRUE;
      $options[CURLOPT_HTTPHEADER] = array('Content-Type: application/soap+xml; charset=utf-8');
      $options[CURLOPT_POSTFIELDS]  = $parameters;
      $format = 'XML';
    }

    if ($cookie)
      $options[CURLOPT_COOKIE] = $cookie;

    if (count($headers))
      foreach ($headers as $header)
        $options[CURLOPT_HTTPHEADER][]  = $header;

    if (! curl_setopt_array($connection, $options))
      throw new \Exception('failed session configuration');

    // call the API
    $result = curl_exec($connection);
    if ($result === FALSE)
      throw new \Exception('failed API call: ' . curl_error($connection));
    dlog('', 'API result: %s', $result);

    // check HTTP code, we cannot parse response if not Ok
    $code = curl_getinfo($connection, CURLINFO_HTTP_CODE);
    curl_close($connection);
    if ($code != 200)
      throw new \Exception("API returned HTTP code $code");

    // parse JSON API response (which may fail), return XML as is
    if ($format == 'JSON')
      $result = json_decode($result);
  }
  catch (\Exception $e)
  {
    dlog('', 'EXCEPTION: ' . $e->getMessage());
  }

  return $result;
}


/**
 * callSoapApi
 * call a SOAP API using SoapClient library
 * @param String API name
 * @param Array API parameters
 * @param Array options:
 *   debug    - Boolean, TRUE: enable debugging (not for production!), FALSE: log errors only
 *   location - String, service URL to override default location in WSDL
 *   password - String, service login password
 *   username - String, service login username
 *   wsdl     - String, WSDL file path
 * @return Array parsed API result (not XML)
 */
function callSoapApi($api, $parameters = array(), $options = array())
{
  $result = NULL;
  $debug = empty($options['debug']) ? FALSE : TRUE;

  if ($debug)
    logInfo('arguments: ' . print_r(func_get_args(), TRUE));

  try
  {
    // required SOAP settings
    $soap = array(
      'trace'           => TRUE,
      'exceptions'      => TRUE, 
      'soap_version'    => SOAP_1_2);

    // add optional settings if provided by the caller
    if ( ! empty($options['username']))
      $soap['login'] = $options['username'];
    if ( ! empty($options['password']))
      $soap['password'] = $options['password'];
    if ( ! empty($options['location']))
      $soap['location'] = $options['location'];
    if ($debug)
      logInfo('options: ' . print_r($soap, TRUE));

    // allow self-signed SSL
    $soap['stream_context'] = stream_context_create(array(
      'ssl' => array(
        'verify_peer'       => FALSE,
        'verify_peer_name'  => FALSE,
        'allow_self_signed' => TRUE)));

    // instantiate soap client and query capabilities
    $client = new SoapClient($options['wsdl'], $soap);
    $capabilities = $client->__getFunctions();
    if ($debug)
      logInfo('capabilities: ' . print_r($capabilities, TRUE));

    // prepare parameters
    if ( ! $parameters = prepareSoapParameters($api, $parameters, $capabilities))
      return logError('unable to prepare SOAP parameters');

    // we cannot use SOAP native function calls because our parameters are inside a single array
    $response = $client->__soapCall($api, $parameters);
    if ($debug)
      logInfo('response: ' . print_r($response, TRUE));

    // parse response into object
    if ( ! empty($response))
      $result = parseXmlResponse($response);
    if ($debug)
      logInfo('parsed: ' . print_r($result, TRUE));
  }

  catch (Exception $e)
  {
    logError('EXCEPTION: ' . $e->getMessage());
  }

  return $result;
}


/**
 * prepareSoapParameters
 * (facepalm): PHP SoapClient does not validate parameter names and their order, it will pass parameter array as is
 * if parameters created by PHPUnit are out of order, the API call will fail because they mismatch
 * therefore we must to fix the order to adhere to WSDL specifications
 * @param String API name
 * @param Array API parameters (out of order)
 * @param Array SOAP client function declarations
 * @return Array properly arranged parameters or NULL on failure
 */
function prepareSoapParameters($api, $parameters, $declarations)
{
  /* sample WSDL declaration
    '[3] => string externalpayments__WebPosPortInCustomer(string $partner_tag, int $request_epoch, string $iccid)'
  */

  $result = array();
  foreach ($declarations as $declaration)
  {
    // split into function name and parameters
    if ( ! $start = strpos($declaration, '('))
      return logError("invalid declaration $declaration");
    $prefix = substr($declaration, 0, $start);
    $suffix = substr($declaration, $start + 1, strlen($declaration) - strlen($prefix) - 2);

    // identify API name
    $words = explode(' ', $prefix);
    if (count($words) != 2)
      return logError("invalid function definition $prefix");
    $name = $words[1];

    // check if this the API we need
    if ($words[1] == $api)
    {
      // extract ordered API parmeter list
      $list = array();
      $definitions = explode(',', $suffix);
      foreach ($definitions as $definition)
      {
        $parts = explode(' ', trim($definition));
        if (count($parts) != 2)
          return logError("invalid parameter definition $definition");
        $list[] = substr($parts[1], 0, 1) == '$' ? substr($parts[1], 1) : $parts[1]; // remove leading '$'
      }
      break;
    }
  }

  // re-assemble parmeters in correct order
  if ( ! empty($list))
    foreach ($list as $name)
      $result[$name] = isset($parameters[$name]) ? $parameters[$name] : NULL;

  return $result;
}


/** callJsonApi
 * call a JSON API using curl library
 * @param String API name
 * @param Array API parameters
 * @param Array options:
 *   debug    - Boolean, TRUE: enable debugging (not for production!), FALSE: log errors only
 *   cookie   - String, HTTP cookie
 *   headers  - Array of HTTP headers
 *   location - String, service URL to override default location in WSDL
 *   password - String, service login password
 *   username - String, service login username
 * @return Array parsed API result
 */
function callJsonApi($api, $parameters = array(), $options = array())
{
  $result = NULL;
  $debug = empty($options['debug']) ? FALSE : TRUE;

  if ($debug)
    logInfo('arguments: ' . print_r(func_get_args(), TRUE));

  try
  {
    // default curl settings
    $session = array(
      CURLOPT_RETURNTRANSFER    => TRUE,
      CURLOPT_SSL_VERIFYPEER    => FALSE,
      CURLOPT_CONNECTTIMEOUT    => 1,
      CURLOPT_TIMEOUT           => 60,
      CURLOPT_HEADER            => FALSE);

    // optional settings if provided by the caller
    if ( ! empty($options['username']) && ! empty($options['password']))
    {
      $session[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
      $session[CURLOPT_USERPWD] = $options['username'] . ':' . $options['password'];
    }
    if ( ! empty($parameters))
    {
      $post = http_build_query($parameters);
      $session[CURLOPT_POST]        = TRUE;
      $session[CURLOPT_HTTPHEADER]  = array('Accept: application/json', 'Content-Length: ' . strlen($post));
      $session[CURLOPT_POSTFIELDS]  = $post;
    }
    if ( ! empty($options['cookie']))
      $session[CURLOPT_COOKIE] = $options['cookie'];
    if ( ! empty($options['headers']))
      foreach ($options['headers'] as $header)
        $session[CURLOPT_HTTPHEADER][]  = $header;
    if ($debug)
      logInfo('session: ' . print_r($session, TRUE));

    // initialize HTTP session
    if ( ! $connection = curl_init($options['location']))
      throw new \Exception('failed to initialize API session');

    if ( ! curl_setopt_array($connection, $session))
      throw new \Exception('failed session configuration');

    // call the API
    $result = curl_exec($connection);
    if ($result === FALSE)
      throw new \Exception('failed API call: ' . curl_error($connection));
    if ($debug)
      logInfo('API result: ', print_r($result, TRUE));

    // check HTTP code, we cannot parse response if not Ok
    $code = curl_getinfo($connection, CURLINFO_HTTP_CODE);
    curl_close($connection);
    if ($code != 200)
      throw new \Exception("API returned HTTP code $code");
    if ($debug)
      logInfo("HTTP code: $code");

    // parse JSON API response (which may fail)
    $result = json_decode($result);
    if ($debug)
      logInfo('parsed: ' . print_r($result, TRUE));
  }

  catch (Exception $e)
  {
    logError('EXCEPTION: ' . $e->getMessage());
  }

  return $result;
}


/**
 * parseXmlResponse
 * parse XML API response into array
 * @param String XML
 * @return Object
 */
function parseXmlResponse($xml)
{
  // extract properties and values
  $result = new StdClass;
  $parser = new SimpleXMLElement($xml);
  foreach ($parser->children() as $name => $node)
  {
    $children = $node->children();

    // array of values
    if (count($children))
    {
      $data = array();
      foreach ($children as $property => $value)
        $data[] = $value->__toString();
      $result->$name = $data;
    }

    // scalar value
    else
    {
      // change Boolean types
      $value = $node->__toString();
      if ($value === 'true' || $value === 'TRUE')
        $result->$name = TRUE;
      elseif ($value === 'false' || $value === 'FALSE')
        $result->$name = FALSE;
      else
        $result->$name = $value;
    }
  }

  return $result;
}
