function printJson(obj) {
  var str = JSON.stringify(obj, undefined, 2); // indentation level = 2
  alert(str);
}

function convertUnicodeToJavaEscape(str) {
  var escaped = '';
  for (var j = 0; j < str.length; j++)
  {
    escaped += str[j].replace(/[^\u0000-\u007F]+/g, function(character) {
      return "\\u" + ("000" + character.charCodeAt(0).toString(16)).substr(-4);
    });
  }
  return escaped;
}

function convertUnicodeFromJavaEscape(str) {
  str = str.replace(/\\u([\d\w]{4})/gi, function (match, grp) {
    return String.fromCharCode(parseInt(grp, 16)); } );
  return unescape(str);
}
