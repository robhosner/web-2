ko.bindingHandlers.slideVisible = {
    init: function(element, valueAccessor) {
        var shouldDisplay = valueAccessor();
        $(element).toggle(shouldDisplay);
    },
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay = valueAccessor();
        shouldDisplay ? $(element).slideDown() : $(element).slideUp();
    }
};
ko.bindingHandlers.slideVisible2 = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay = valueAccessor();
        shouldDisplay ? $(element).slideDown() : $(element).slideUp();
    }
};
ko.bindingHandlers.visible2 = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay = valueAccessor();
        shouldDisplay ? $(element).show() : $(element).hide();
    }
};

ko.bindingHandlers.status = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay =  ko.utils.unwrapObservable(valueAccessor());


        if (shouldDisplay==0) { //nothing
            $(element).addClass('statusWaiting');
            $(element).html('...');
        } else if (shouldDisplay==1) { //thinking
            $(element).removeClass('statusWaiting');
            $(element).addClass('statusThinking');
            $(element).html('...');
        } else if (shouldDisplay==2) { //success
            $(element).removeClass('statusWaiting');
            $(element).removeClass('statusThinking');
            $(element).html('');

        } else { //error
            $(element).removeClass('statusThinking');
            $(element).addClass('statusError');
                $(element).html('Error');
        }

    }
};


ko.bindingHandlers.masked = {

    init: function(element, valueAccessor, allBindingsAccessor) {

        //this will fill the values back in when the module is returned to
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value) $(element).val(value);

        var mask = allBindingsAccessor().mask || {};
        $(element).mask(mask);

        ko.utils.registerEventHandler(element, 'keyup', function() {
            //alert('!');
            var observable = valueAccessor();
            observable($(element).val());
        });

        ko.utils.registerEventHandler(element, 'keydown', function() {
            //alert('!!');
            var observable = valueAccessor();
            observable($(element).val());
        });

        //for some reason, blur was firing at random times. only do the blur action if it was just in focus
        var inFocus = 0;
        ko.utils.registerEventHandler(element, 'focus', function() {
            inFocus = 1;

        });
        ko.utils.registerEventHandler(element, 'blur', function() {

            if (inFocus) {
                var observable = valueAccessor();
                observable($(element).val());
                inFocus = 0;
                //alert('!!!');
            }

        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value=='') {
            $(element).val(value);
        }
        var disableMe = allBindingsAccessor().disableMe;
        var updateMe = allBindingsAccessor().updateMe;
        if (updateMe || disableMe) $(element).val(value);



    }
};


ko.observableArray.fn.subscribeArrayChanged = function(addCallback, deleteCallback) {
    var previousValue = undefined;
    this.subscribe(function(_previousValue) {
        previousValue = _previousValue.slice(0);
    }, undefined, 'beforeChange');
    this.subscribe(function(latestValue) {
        var editScript = ko.utils.compareArrays(previousValue, latestValue);
        for (var i = 0, j = editScript.length; i < j; i++) {
            switch (editScript[i].status) {
                case "retained":
                    break;
                case "deleted":
                    if (deleteCallback)
                        deleteCallback(editScript[i].value);
                    break;
                case "added":
                    if (addCallback)
                        addCallback(editScript[i].value);
                    break;
            }
        }
        previousValue = undefined;
    });
};






$(document).ready(function() {

} );
