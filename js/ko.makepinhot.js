var makepinhotViewModel;
function AppViewModel() {

    var self = this;

    //self.baseUrl = 'https://' + l_sApiBase;

    /////////////////////////////////
    //KNOCKOUT.JS OBSERVABLE BOOLEANS
    self.thinking0 = ko.observable(false);
    self.thinking1 = ko.observable(false);
    self.formError = ko.observable(false);


    //KNOCKOUT.JS OBSERVABLE VALUES
    self.master = ko.observable('');
    self.pinStart = ko.observable('');
    self.pinEnd = ko.observable('');
    self.error0 = ko.observable('');
    self.error1 = ko.observable('');
    self.success1 = ko.observable('');
    self.form_errors = ko.observable({});
    self.pins = ko.observableArray([]);

    //EO KNOCKOUT.JS OBSERVABLES
    ////////////////////////////


    ////////////////////////////////
    //NON-OBSERVABLE CLASS VARIABLES

    self.bypassAPI = false;

    self.alertData = 0;

    self.requestId = '';
    self.zsession = '';

    self.fieldMap = {
        fromPinSerialNumber: 'pinStart',
        toPinSerialNumber: 'pinEnd',
        masteragent: 'master'
    };

    self.errorMap = {
        "value is shorter than min_strlen": "The value entered is too short.",
        "value is longer than max_strlen": "The value entered is too long.",
        "is not a integer": "Please select a Master Agent",
        "value fails LUHN checksum": "You entered an invalid activation code.",
        "value fails Actcode format validation": "You entered an invalid activation code.",
        "value fails validation (letters, spaces, apostrophe)": "Contains invalid character - letters, spaces, apostrophe only",
        "value fails validation (letters, numbers, spaces, punctuation, #)": "Contains invalid character.",
        "value fails validation (alphanumeric or punctuation)": "Alphanumeric and punctuation only.",
        "value fails validation (numeric)": "Value must be numeric."
    };

    //EO NON-OBSERVABLE CLASS VARS
    //////////////////////////////


    ///////////////////////////
    //KNOCKOUT.JS SUBSCRIPTIONS
    var joinedFields = _.values(self.fieldMap);
    for (var i = 0; i < joinedFields.length; i++) {

        self[joinedFields[i]].subscribe(function() {

            self.form_errors()[this] = '';
            self.form_errors.valueHasMutated();
            if (_.values(self.form_errors()).join('')=='') self.formError(false);
        },joinedFields[i]);
    }
    //EO KNOCKOUT.JS SUBSCRIPTIONS
    /////////////////////////////


    /////////////////////
    //INITIALIZATION CODE

    //Configure datatables to work with observable self.pins() object
    //Create the DataTable
    //***NOTE*** -> dataTable is lowercase. Original example was uppercase, broke in ie7

    var dt = $('#example2').dataTable( {
        "searching": 0,
        "aoColumns": [
            { "mData": "fromPinSerialNumber"},
            { "mData": "toPinSerialNumber"},
            { "mData": "status"},
            { "mData": "count" }
    ]

    } );

    self.pins.subscribe(
        function () {
            dt.fnClearTable();

            for (var i = 0, j = self.pins().length; i < j; i++) {
                dt.fnAddData(self.pins()[i]);
            }
        }
    );

    //EO INITIALIZATION CODE
    ////////////////////////

    ///////////////////////////
    //CLASS FUNCTIONS

    self.getPINRangeStatus = function() {
        self.pins([]);
        self.thinking0(true);
        self.success1('');
        self.error0('');

        var params = {
            bath: 'rest',
            version: '2',
            startPinSerialNumber: self.pinStart,
            endPinSerialNumber: self.pinEnd,
            masteragent: self.master,
        }
        if (!self.pinEnd()) {
            params.endPinSerialNumber = self.pinStart;
        }
        $.ajax({
            url: '/ultra_api.php?partner=inventory&command=inventory__GetPINRangeStatus',
            type: 'POST',
            data: params,
            success: function(data) {
               makepinhotViewModel.callback_getPINRangeStatus(JSON.parse(data));
            }
        });

    };

    self.callback_getPINRangeStatus = function(data) {
        // self.debug(data);

        self.thinking0(false);
        if (data.errors.length == 0 && data.pin_cards_data.length > 0)
        {
            self.pins(data.pin_cards_data);
        }
        else
        {
            self.formError(true);
            self.error0('No PINs for selected range.');
            self.pins([]);
        }
    };

    self.makeHot = function() {
        self.thinking1(true);
        self.error1('');

        var params = {
            bath: 'rest',
            version: '2',
            callback: 'makepinhotViewModel.callback_makeHot',
            startPinSerialNumber: self.pinStart,
            endPinSerialNumber: self.pinEnd,
            masteragent: self.master,
            user: 'make_pin_hot'
        }
        if (!self.pinEnd()) {
            params.endPinSerialNumber = self.pinStart;
        }
        $.ajax({
            url: '/ultra_api.php?partner=inventory&command=inventory__AssignPINRangeToHot',
            type: 'POST',
            data: params,
            success: function(data) {
               makepinhotViewModel.callback_makeHot(JSON.parse(data));
            }
        });
    };

    self.callback_makeHot = function(data) {
        self.debug(data);

        self.thinking1(false);
        if (data.errors.length == 0) {
            self.getPINRangeStatus();
            self.success1('SIMs successfully set to Hot.');
        } else {
            self.error1(data.errors[0]);
        }

    }

    self.iterateFieldErrors = function(apiError) {
        var apiField = _.find(
            _.keys(self.fieldMap),
            function(field){
                var containsField = apiError.indexOf(field) >= 0;
                return containsField;
            }
        );
        if (apiField) {
            var jsField = self.fieldMap[apiField];
            self.form_errors()[jsField] = self.mapError(apiError,apiField);
            self.formError(true);
            self.form_errors.valueHasMutated();
        }
    }

    self.debug = function(data) {
        if (self.alertData) printJson(data);
    }

    self.mapError = function(error,field,specialString) {

        var errorTemp = '';

        //If passed a field value, it will reduce the error to what falls after the field name, and then check the errorMap.
        if (field) {
            errorTemp = error.substr(error.indexOf(field) + field.length + 1);
            if (self.errorMap[errorTemp]) return self.errorMap[errorTemp];
        }
        //Special string does the same thing, but has its own param to reduce confusion. Used for instances like have a
        //  variable string within the error string (a specific zip, for example).
        else if (specialString) {
            errorTemp = error.substr(error.indexOf(specialString) + specialString.length + 1);
            if (self.errorMap[errorTemp]) return self.errorMap[errorTemp];
        }
        //Try matching the entire error.
        if (self.errorMap[error]) return self.errorMap[error];


        //Default to the full error, if not mapped.
        return error;
    }

    self.killSession = function() {
        var params = {
            bath: 'rest', version: '1',
            callback: 'makepinhotViewModel.doNothing',
            zsession: self.zsession
        }

        $.ajax(self.baseUrl + '/partner.php?format=jsonp&partner=portal&command=portal__KillSession',{
            dataType:'jsonp',
            data: params
        });
    }

    self.doNothing = function() {}
}
