
var filters = {};

filters.formatMsisdn = function(msisdn,noBreak) {
    if (!msisdn) msisdn = '';
    if (msisdn && $.isNumeric(msisdn)) {
        msisdn = msisdn.toString();
        if (noBreak) {
            return '(' + msisdn.substr(0,3) + ')<span style="color:#fff;">_</span>' + msisdn.substr(3,3) + '-' + msisdn.substr(6);
        } else {
            return '(' + msisdn.substr(0,3) + ') ' + msisdn.substr(3,3) + '-' + msisdn.substr(6);
        }
    } else {
        return 'N/A';
    }
}

filters.formatNotSet = function(val) {
    if (!val) {
        return '<span class="notSet">[not set]</span>'
    } else {
        return val;
    }
}


filters.formatMoney = function(num) {
    if (!num) return '$0';

    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
    {
        num = "0";
    }

    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();

    if (cents < 10)
    {
        cents = "0" + cents;
    }
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
    {
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    }

    return (((sign) ? '' : '-') + '$' + num + '.' + cents);
}

filters.monetizeCents = function(cents) {
    if (cents) {
        var dollars = cents/100;
        return filters.formatMoney(dollars);
    } else {
        return '$0';
    }
}
filters.monetizeDollars = function(dollars) {
    if (dollars) {
        return filters.formatMoney(dollars);
    } else {
        return '$0';
    }
}

filters.timeAgo = function(timestampSecs) {
    return moment.unix(timestampSecs).format('h:mma');
}

//for GMT timestamps
filters.formatDateToPST = function(timestamp) {
    return moment.unix(timestamp).tz("America/Los_Angeles").format('MM/DD/YY');
}

filters.invocationHistory = function(what) {



    var html = '';

    var topArr = what.split(';');
    for (var i=0; i<topArr.length; i++) {
        var innerArr = topArr[i].split('|');
        html+= '<tr>';
        html+= '<td>' + innerArr[0] + '</td>';
        html+= '<td>' + innerArr[1] + '</td>';
        html+= '</tr>';
    }

    return html;


    //INITIATED|2014-02-19 04:29:25|;OPEN|2014-02-19 04:29:36|;COMPLETED|2014-02-19 04:30:08|
    //return what;
}



filters.invocationDate = function(when) {

    //Feb 20 2014 12:30:09:090AM

    return moment(when,"MMM DD YYYY HH:MM:SS:SSSA").format('MM-DD-YY HH:MMA');
    //return moment.unix(timestamp).zone('+0000').format('MM/DD/YY');
}


//for timestamps that have already been adjusted to PST
filters.formatDate = function(timestamp) {
    return moment.unix(timestamp).zone('+0000').format('MM/DD/YY');
}
filters.formatYesterday = function(timestamp) {
    return moment.unix(timestamp).subtract('days', 1).zone('+0000').format('MM/DD/YY');
}
filters.formatTime = function(timestamp) {
    return moment.unix(timestamp).zone('+0000').format('h:mma');
}
filters.formatDateTime = function(timestamp) {
    //return moment.unix(timestamp).tz("America/Los_Angeles").format('MM/DD/YY h:mma');
    return moment.unix(timestamp).zone('+0000').format('MM/DD/YY h:mma');
}

filters.fullName = function(obj) {
    return obj.first_name + ' ' + obj.last_name;
}

filters.properName = function(str) {
    return str.replace(/\b./g, function(m){ return m.toUpperCase(); });
}



filters.formatPlan = function(plan,dollar) {
    if (!plan) plan='';
    var planFormatted = plan.replace('L','').replace('$','').replace('Ultra ','').replace(' ','');
    if (!$.isNumeric(planFormatted)) return '';
    if (dollar && planFormatted) {
        planFormatted = '$' + planFormatted;
    }
    return planFormatted;

}

filters.upperCase = function(phrase) {
    if (!phrase) phrase = '';
    return phrase.toUpperCase();
}

filters.pinDeformat = function(pin) {
    return pin.replace('-','').replace('-','').replace('-','').replace('-','');
}



    
