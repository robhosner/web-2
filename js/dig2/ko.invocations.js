function AppViewModel() {
    var vm = this;

    vm.initialized = ko.observable(0);

    vm.busy1 = ko.observable(0);
    vm.busy2 = ko.observable(0);
    vm.busy3 = ko.observable(0);
    vm.invocationsArr = ko.observableArray([]);

    vm.timeAdjust = 0;
    vm.apiTime = 0;

    vm.now = 0;

    vm.invocationsBase = [];
    vm.invocationsArr = ko.observableArray([]);


    vm.customerId = ko.observable('');
    vm.startDate = ko.observable('');
    vm.endDate = ko.observable('');
    vm.fInvocationsHistory = filters.invocationHistory;

    vm.lastUpdate = ko.observable('...');

    vm.status = ko.observable('');
    vm.status3 = ko.observable('');




    vm.statusOptions = ko.observableArray([
        { status: "OPEN" },
        { status: "ERROR" },
        { status: "REPORTED ERROR" },
        { status: "REPORTED MISSING" },
        { status: "MISSING" }

    ]);
    vm.escalationOptions = ko.observableArray([
        { status: "REPORTED ERROR" },
        { status: "REPORTED MISSING" },
        { status: "RESOLVED" }

    ]);

    vm.escalationOptionsLimited = ko.observableArray([
        { status: "REPORTED ERROR" },
        { status: "RESOLVED" }

    ]);
    vm.escalationOptionsLimited2 = ko.observableArray([
        { status: "REPORTED MISSING" },
        { status: "RESOLVED" }

    ]);

    vm.invocTimeDiff = function(obj) {

        var last = moment.utc(obj.LAST_STATUS_DATE_TIME, "MM/DD/YY hh:mm:ss A").zone('+0000'); //time if it were GMT
        var offset = moment.utc(obj.LAST_STATUS_DATE_TIME, "MM/DD/YY hh:mm:ss A").tz("America/Los_Angeles").zone() * 60; //pst offset

        var diffHours = ((vm.now - (last.unix() + offset))  / 60 / 60).toFixed(1);
        return diffHours;
    }

    vm.timeToPST = function(dateString) {
        return dateString ? moment.utc(dateString, "MMM DD YYYY hh:mm:ss:SSSA").tz("America/Los_Angeles").format("MM/DD/YY hh:mm:ssA") : '';

    }


    vm.search1 = function() {

        vm.initialized(1);
        vm.busy1(1);
        vm.invocationsArr([]);

        var date_from = vm.startDate().substr(3,2) + '-' + vm.startDate().substr(0,2) + '-' + vm.startDate().substr(6,4);
        var date_to = vm.endDate().substr(3,2) + '-' + vm.endDate().substr(0,2) + '-' + vm.endDate().substr(6,4);

            $.ajax({
            type: 'POST',
            url:      "/pr/mvnecare/2/ultra/api/mvnecare__ListCustomerInvocations",
            dataType: 'json',
            data: { customer_id: vm.customerId(), date_from: date_from, date_to: date_to },

            success:  function(data) {

                vm.invocationsArr(data.records.reverse());
                vm.busy1(0);

            }
        });
    }


    vm.search2 = function() {
        vm.initialized(1);
        vm.busy2(1);
        vm.invocationsArr([]);

        vm.now = moment().unix() + vm.timeAdjust;
        var date_from = moment.unix(vm.now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
        var date_to = moment.unix(vm.now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');


        $.ajax({
            type: 'POST',
            url:      "/pr/mvnecare/2/ultra/api/mvnecare__ListOpenInvocations",
            dataType: 'json',
            data: { status: vm.status() },

            success:  function(data) {

                vm.invocationsArr(data.records.reverse());
                vm.busy2(0);

            }
        });
    }

    vm.search3 = function() {
        vm.initialized(1);
        vm.busy3(1);
        vm.invocationsArr([]);

        vm.now = moment().unix() + vm.timeAdjust;
        var date_from = moment.unix(vm.now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
        var date_to = moment.unix(vm.now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');


        $.ajax({
            type: 'POST',
            url:      "/pr/mvnecare/2/ultra/api/mvnecare__ListOpenInvocations",
            dataType: 'json',
            data: { status: '' },

            success:  function(data) {

                vm.invocationsBase = data.records;

                vm.filterResults();

                vm.busy3(0);

                vm.lastUpdate(moment().format('h:mm a'));

            }
        });
    }


    vm.toggleStatus3 = function(command) {

        if (vm.status3()==command) {
            vm.status3('');
        } else {
            vm.status3(command);
        }

    }

    vm.status3.subscribe(function() {

        vm.filterResults();
    });

    vm.filterResults = function() {

        _.each(vm.invocationsBase, function(obj) {
            obj.timeDiff = vm.invocTimeDiff(obj);
        });

        if (vm.status3()) {
            vm.invocationsArr(_.filter(vm.invocationsBase, function(obj){ return obj.STATUS == vm.status3(); }));
        } else {
            vm.invocationsArr(_.filter(vm.invocationsBase, function(obj){ return obj.STATUS != 'FAILED'; })); //filter out FAILED for the time being
            //vm.invocationsArr(vm.invocationsBase); //don't filter
        }
    }



    vm.getApiTime = function(callback) {


        $.ajax({
            type: 'POST',
            url: "/pr/customercare/1/ultra/api/customercare__ValidateSIMOrigin",
            dataType: 'json',
            data: { iccid: '' },

            success:  function(data) {

                vm.apiTime = data.ultra_trans_epoch;
                var browserTime = moment().unix();
                vm.timeAdjust = vm.apiTime - browserTime;

                if (callback) callback();

            }
        });
    }

    vm.filterOptions = function(obj) {
        if (obj.STATUS=='ERROR' || obj.STATUS=='REPORTED ERROR') {
            return vm.escalationOptionsLimited;
        } else if (obj.STATUS=='MISSING' || obj.STATUS=='REPORTED MISSING') {
            return vm.escalationOptionsLimited2;
        } else {
            return vm.escalationOptions;
        }

    }

    vm.getApiTime(vm.search3);


    vm.dynamicClass = function(obj) {

        if (obj.STATUS=='REPORTED ERROR' || obj.STATUS=='REPORTED MISSING') {
            if (parseInt(vm.invocTimeDiff(obj)) >= 12) {
                return 'statusRed status';
            } else {
                return 'statusYellow status';
            }
        }
        else if (obj.STATUS=='INITIATED' || obj.STATUS=='OPEN') {
            return 'statusPurple status';
        }
        else if (obj.STATUS=='RESOLVED' || obj.STATUS=='COMPLETED') {
            return 'statusGreen status';
        }
        else if (obj.STATUS=='ERROR' || obj.STATUS=='MISSING' || obj.STATUS=='FAILED') {
            return 'statusRed status';
        }
        else if (obj.STATUS=='COMPLETED DELAYED') {
            return 'statusBlue status';
        }

    }


}


moment.tz.add({
    "zones": {
        "America/Los_Angeles": [
            "-7:52:58 - LMT 1883_10_18_12_7_2 -7:52:58",
            "-8 US P%sT 1946 -8",
            "-8 CA P%sT 1967 -8",
            "-8 US P%sT"
        ]

    },
    "rules": {
        "US": [
            "1918 1919 2 0 8 2 0 1 D",
            "1918 1919 9 0 8 2 0 0 S",
            "1942 1942 1 9 7 2 0 1 W",
            "1945 1945 7 14 7 23 1 1 P",
            "1945 1945 8 30 7 2 0 0 S",
            "1967 2006 9 0 8 2 0 0 S",
            "1967 1973 3 0 8 2 0 1 D",
            "1974 1974 0 6 7 2 0 1 D",
            "1975 1975 1 23 7 2 0 1 D",
            "1976 1986 3 0 8 2 0 1 D",
            "1987 2006 3 1 0 2 0 1 D",
            "2007 9999 2 8 0 2 0 1 D",
            "2007 9999 10 1 0 2 0 0 S"
        ],
        "CA": [
            "1948 1948 2 14 7 2 0 1 D",
            "1949 1949 0 1 7 2 0 0 S",
            "1950 1966 3 0 8 2 0 1 D",
            "1950 1961 8 0 8 2 0 0 S",
            "1962 1966 9 0 8 2 0 0 S"
        ]
    },
    "links": {}
});


function printJson(obj) {
    var str = JSON.stringify(obj, undefined, 2); // indentation level = 2
    alert(str);
}





