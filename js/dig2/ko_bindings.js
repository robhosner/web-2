ko.bindingHandlers.slideVisible = {
    init: function(element, valueAccessor) {
        var shouldDisplay = valueAccessor();
        $(element).toggle(shouldDisplay);
    },
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay = valueAccessor();
        shouldDisplay ? $(element).slideDown() : $(element).slideUp();
    }
};
ko.bindingHandlers.slideVisible2 = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay = valueAccessor();
        shouldDisplay ? $(element).slideDown() : $(element).slideUp();
    }
};
ko.bindingHandlers.visible2 = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        var shouldDisplay = valueAccessor();
        shouldDisplay ? $(element).show() : $(element).hide();
    }
};

ko.bindingHandlers.showNextRow = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        $(element).click(function(){

            if ($(this).next().is(':visible')) {
                $(this).next().hide();
            } else {
                $(this).next().show();
            }

        });
    }
};

ko.bindingHandlers.showNextRowA = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        $(element).click(function(){

            if ($(this).parent().parent().next().is(':visible')) {
                $(this).parent().parent().next().hide();
            } else {
                $(this).parent().parent().next().show();
            }

        });
    }
};
ko.bindingHandlers.showNextRowB = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        $(element).click(function(){

            if ($(this).parent().parent().next().next().is(':visible')) {
                $(this).parent().parent().next().next().hide();
            } else {
                $(this).parent().parent().next().next().show();
            }

        });
    }
};



ko.bindingHandlers.updateEscalation = {
    update: function(element, valueAccessor) {
        $(element).find("button").click(function(){

            var customerId = $(element).find('.customer_id').eq(0).val();
            var toStatus = $(element).find('.to_status').eq(0).val();
            var user = $(element).find('.user').eq(0).val();
            var ticketDetails = $(element).find('.ticket_details').eq(0).val();

            if ($(this).val()) {
                toStatus = $(this).val();
            }

            var loader = $(element).find('img').eq(0);
            loader.show();

            $.ajax({
                type: 'POST',
                url:      "/pr/mvnecare/2/ultra/api/mvnecare__UpdateEscalation",
                dataType: 'json',
                data: { customer_id: customerId, to_status: toStatus, user: user, ticket_details: ticketDetails  },

                success:  function(data) {
                    loader.hide();

                }
            });

        });
    }
};

ko.bindingHandlers.showNextRow2 = {
    update: function(element, valueAccessor) {
        // On update, fade in/out
        $(element).click(function(){



            if ($(this).next().is(':visible')) {
                $(this).next().hide();
                //$(this).next().html('');//"<img src='/images/load_20.gif' />");
                $(this).next().html("<td colspan='4'><div class='loaderWrapper'><img src='/images/load_20.gif' /></div></td>");
            } else {
                $(this).next().show();

                var that = this;

                if (!vm.foundMsisdn() && !vm.foundICCID()) return false; //TODO - couldn't call this, no customer_id

                vm.think(1);
                //vm.warning(''); //reset the error/warning at the top of the screen

                vm.sourceObj9(1);

                var now = moment().unix();
                var date_from = moment.unix(now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
                var date_to = moment.unix(now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');

                var tag = valueAccessor();

                $.ajax({
                    type: 'POST',
                    url:      "/pr/internal/2/ultra/api/internal__GetCustomerSoapLog",
                    dataType: 'json',
                    data: { ICCID : vm.foundICCID(), MSISDN: vm.foundMsisdn(), date_from: date_from, date_to: date_to, tag:tag  },

                    success:  function(data) {
                        if (vm.responseOk(data,'internal__GetCustomerAggregatedSoapLog')) {

                            var rowsHtml = '';

                            for (var i=0; i<data.records.length; i++) {
                                var record = data.records[i];
                                rowsHtml += '<tr>';
                                rowsHtml += '<td>' + record.SOAP_LOG_ID + '</td>';
                                rowsHtml += '<td>' + record.SOAP_DATE + '</td>';
                                rowsHtml += '<td>' + record.TYPE_ID + '</td>';
                                rowsHtml += '<td>' + record.SESSION_ID + '</td>';
                                rowsHtml += '<td>' + record.COMMAND + '</td>';
                                //rowsHtml += '<td>' + record.ENV + '</td>';
                                rowsHtml += '<td>' + record.RESULTCODE + '</td>';
                                //rowsHtml += '<td><xmp>' + record.xml + '</xmp></td>';
                                rowsHtml += '<td class="p40"><textarea class="xmlTextArea">' + record.xml + '</textarea></td>';

                                rowsHtml += '</tr>';
                            }

                            var html = '<td colspan="4"><div class="invocDetailWrapper"><h6 class="subheader">Customer Soap Log:</h6><table><thead><tr><th>ID</th><th>Date</th><th>type ID</th><th>session ID</th><th>Command</th><th>Result Code</th><th class="p40">XML</th></tr></thead><tbody>';
                            html += rowsHtml;
                            html += '</tbody></table></div></td>';

                            $(that).next().html(html);


                        }
                    }
                });

            }

        });
    }
};

ko.bindingHandlers.status = {
    update: function(element, valueAccessor,allBindingsAccessor) {
        // On update, fade in/out
        var shouldDisplay =  ko.utils.unwrapObservable(valueAccessor());

        var noStatusIf = allBindingsAccessor().noStatusIf;

        //alert(noStatusIf);

        if (!noStatusIf) {

            if (shouldDisplay==0) { //nothing
            $(element).removeClass('statusError');
            $(element).removeClass('statusThinking');
            $(element).addClass('statusWaiting');
            $(element).html('...');
        } else if (shouldDisplay==1) { //thinking
            $(element).removeClass('statusError');
            $(element).removeClass('statusWaiting');
            $(element).addClass('statusThinking');
            $(element).html('...');
        } else if (shouldDisplay==2) { //success
            $(element).removeClass('statusError');
            $(element).removeClass('statusWaiting');
            $(element).removeClass('statusThinking');
            $(element).html('');

        } else { //error
            $(element).removeClass('statusThinking');
            $(element).removeClass('statusWaiting');
            $(element).addClass('statusError');
                $(element).html('Error');
        }
        }

    }
};


ko.bindingHandlers.noStatusIf = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        // On update, fade in/out
        var shouldDisplay =  ko.utils.unwrapObservable(valueAccessor());


        if (shouldDisplay) { //nothing
            $(element).removeClass('statusError');
            $(element).removeClass('statusThinking');
            $(element).removeClass('statusWaiting');
            $(element).html('');
        }
    }
};

ko.bindingHandlers.statusCell = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        // On update, fade in/out
        var shouldDisplay =  ko.utils.unwrapObservable(valueAccessor());

        var noStatusIf = allBindingsAccessor().noStatusIf;



        if (!noStatusIf) {
            if (shouldDisplay==0) { //nothing
                $(element).removeClass('statusErrorCell');
                $(element).removeClass('statusThinkingCell');
                $(element).addClass('statusWaitingCell');
                $(element).html('...');
            } else if (shouldDisplay==1) { //thinking
                $(element).removeClass('statusErrorCell');
                $(element).removeClass('statusWaitingCell');
                $(element).addClass('statusThinkingCell');
                $(element).html('...');
            } else if (shouldDisplay==2) { //success
                $(element).removeClass('statusErrorCell');
                $(element).removeClass('statusWaitingCell');
                $(element).removeClass('statusThinkingCell');
                $(element).html('');

            } else { //error
                $(element).removeClass('statusThinkingCell');
                $(element).removeClass('statusWaitingCell');
                $(element).addClass('statusErrorCell');
                $(element).html('Error');
            }
        }



    }
};


ko.bindingHandlers.masked = {

    init: function(element, valueAccessor, allBindingsAccessor) {

        //this will fill the values back in when the module is returned to
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value) $(element).val(value);

        var mask = allBindingsAccessor().mask || {};
        $(element).mask(mask);

        ko.utils.registerEventHandler(element, 'keyup', function() {
            //alert('!');
            var observable = valueAccessor();
            observable($(element).val());
        });

        ko.utils.registerEventHandler(element, 'keydown', function() {
            //alert('!!');
            var observable = valueAccessor();
            observable($(element).val());
        });

        //for some reason, blur was firing at random times. only do the blur action if it was just in focus
        var inFocus = 0;
        ko.utils.registerEventHandler(element, 'focus', function() {
            inFocus = 1;

        });
        ko.utils.registerEventHandler(element, 'blur', function() {

            if (inFocus) {
                var observable = valueAccessor();
                observable($(element).val());
                inFocus = 0;
                //alert('!!!');
            }

        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value=='') {
            $(element).val(value);
        }
        var disableMe = allBindingsAccessor().disableMe;
        var updateMe = allBindingsAccessor().updateMe;
        if (updateMe || disableMe) $(element).val(value);



    }
};


ko.observableArray.fn.subscribeArrayChanged = function(addCallback, deleteCallback) {
    var previousValue = undefined;
    this.subscribe(function(_previousValue) {
        previousValue = _previousValue.slice(0);
    }, undefined, 'beforeChange');
    this.subscribe(function(latestValue) {
        var editScript = ko.utils.compareArrays(previousValue, latestValue);
        for (var i = 0, j = editScript.length; i < j; i++) {
            switch (editScript[i].status) {
                case "retained":
                    break;
                case "deleted":
                    if (deleteCallback)
                        deleteCallback(editScript[i].value);
                    break;
                case "added":
                    if (addCallback)
                        addCallback(editScript[i].value);
                    break;
            }
        }
        previousValue = undefined;
    });
};


ko.bindingHandlers.datepicker = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {},
            $el = $(element);

        $el.datepicker(options);

        //handle the field changing

        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            $el.datepicker("hide");
            $el.blur();
            observable($el.val());


            //setTimeout(function() {
            //  $el.datepicker("hide");
            //alert('hidden');
            //},500);

            //observable($el.datepicker("getDate"));
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $el.datepicker("destroy");
        });

    },
    update: function(element, valueAccessor) {

        var value = ko.utils.unwrapObservable(valueAccessor()),
            $el = $(element);

        //handle date data coming via json from Microsoft
        if (String(value).indexOf('/Date(') == 0) {
            value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
        }

        var current = $el.datepicker("getDate");

        if (value - current !== 0) {
            $el.datepicker("setDate", value);
        }
        //var observable = valueAccessor();
        //observable($el.val());
    }
};




$(document).ready(function() {

} );
