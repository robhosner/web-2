function AppViewModel() {
    var vm = this;

    vm.hasError = ko.observable(0);

    vm.sourceObj1 = ko.observable(0);
    vm.sourceObj2 = ko.observable(0);
    vm.sourceObj3 = ko.observable(0);
    vm.sourceObj4 = ko.observable(0);
    vm.sourceObj5 = ko.observable(0);
    vm.sourceObj6 = ko.observable(0);
    vm.sourceObj7 = ko.observable(0);
    vm.sourceObj8 = ko.observable(0);
    vm.sourceObj9 = ko.observable(0);
    vm.sourceObj10 = ko.observable(0);
    vm.sourceObj11 = ko.observable(0);
    vm.sourceObj12 = ko.observable(0);
    vm.sourceObj13 = ko.observable(0);
    vm.sourceObj14 = ko.observable(0);
    vm.sourceObj15 = ko.observable(0);



    vm.errObj1 = ko.observableArray([]);
    vm.errObj2 = ko.observableArray([]);
    vm.errObj3 = ko.observableArray([]);
    vm.errObj4 = ko.observableArray([]);
    vm.errObj5 = ko.observableArray([]);
    vm.errObj6 = ko.observableArray([]);
    vm.errObj7 = ko.observableArray([]);
    vm.errObj8 = ko.observableArray([]);
    vm.errObj9 = ko.observableArray([]);
    vm.errObj10 = ko.observableArray([]);
    vm.errObj11 = ko.observableArray([]);
    vm.errObj12 = ko.observableArray([]);
    vm.errObj13 = ko.observableArray([]);
    vm.errObj14 = ko.observableArray([]);
    vm.errObj15 = ko.observableArray([]);


    vm.smsSent = ko.observable(0);
    vm.smsFormVisible = ko.observable(0);
    vm.agent = ko.observable('');
    vm.smsMessage = ko.observable('');

    vm.E911FormIsWorking  = ko.observable(0);
    vm.E911FormSuccessful = ko.observable(0);
    vm.E911FormVisible    = ko.observable(0);
    vm.E911FormAddress1   = ko.observable('');
    vm.E911FormAddress2   = ko.observable('');
    vm.E911FormCity       = ko.observable('');
    vm.E911FormState      = ko.observable('');
    vm.E911FormZip        = ko.observable('');


    vm.showPortInfo = ko.observable(0);
    vm.queryPortInObj = ko.observable({});
    //vm.isEligiblePortInResult = ko.observable('');


    vm.soapLogArrAll = [];
    vm.soapLogPageSize = 20;
    vm.soapLogHasNext = ko.observable(0);
    vm.soapLogPage = ko.observable(0);

    vm.apiTime = 0;
    vm.timeAdjust = 0;

    vm.path = 0;

    vm.fDollars = filters.monetizeDollars;
    vm.fNotSet = filters.formatNotSet;
    vm.fInvocDate = filters.invocationDate;
    vm.fInvocationsHistory = filters.invocationHistory;

    vm.inputMsisdn = ko.observable(''); //msisdn input value
    vm.inputIccid = ko.observable(''); //iccid input value
    vm.inputCustomerId = ko.observable(''); //customerId input value
    vm.inputActCode = ko.observable('');

    vm.foundMsisdn = ko.observable(''); //the authoritative MSISDN (either searched by, or queried via the ICCID)
    vm.foundICCID = ko.observable(''); //the authoritative ICCID (either searched by, or queried via the MSISDN)
    vm.foundCustomerId = ko.observable('');


    vm.errors = ko.observableArray([]); //list of errors at the top of the page

    vm.showMoreTransitions = ko.observable(0);
    vm.showCustomerAux = ko.observable(0);
    vm.showSimOriginAux = ko.observable(0);

    vm.invocationsArr = ko.observableArray([]);
    vm.makeitsoArr = ko.observableArray([]);
    vm.soapLogArr = ko.observableArray([]);

    vm.simOriginArr = ko.observableArray([]);
    vm.simOriginAuxArr = ko.observableArray([]);

    vm.invokedAppArr =  ko.observableArray([]);
    vm.dependencyErrArr =  ko.observableArray([]);

    vm.mvneDetailsObj = ko.observable({ GetNetworkDetails_APNList: { APN: { APNName: null, APNValue: null} }, GetNetworkDetails_NAPFeatureList: {NAPFeature: null}, GetNetworkDetails_HLRFeatureList: {HLRFeature: null} });
    vm.networkDetailsArr = ko.observableArray([]);
    vm.querySubArr = ko.observableArray([]);
    vm.checkBalanceArr = ko.observableArray([]);
    vm.addOnList = ko.observable('');
    vm.balanceValueList = ko.observableArray('');
    vm.custPlanOptions = ko.observable('');
    vm.custStateObj = ko.observable({customers:[]});
    vm.custTransObj = ko.observable({});
    vm.custInfoObj = ko.observable({});
    vm.flexInfoObj = ko.observable({});

    vm.transitionsHtml = ko.observable('');

    vm.custPlanStarted = ko.observable('');
    vm.custPlanEnded = ko.observable('');
    vm.custMultiMonthEnds = ko.observable('');
    vm.custMultiMonthDuration = ko.observable('');

    vm.transitionCount = ko.observable(0);

    vm.showMvneDetails = ko.observable(0);
    vm.thinking = ko.observable(0);

    vm.initialized = ko.observable(0);

    vm.cancelPortResult = ko.observable('');

    // getIMEICustomerData
    vm.imeiList       = ko.observable('');
    vm.makeList       = ko.observable('');
    vm.modelList      = ko.observable('');
    vm.modelNameList  = ko.observable('');
    vm.osList         = ko.observable('');
    vm.deviceTypeList = ko.observable('');

    vm.loader1 = ko.observable(0);
    vm.loader2 = ko.observable(0);
    vm.loader3 = ko.observable(0);
    vm.loader4 = ko.observable(0);
    vm.loader5 = ko.observable(0);
    vm.loader6 = ko.observable(0);
    vm.loader7 = ko.observable(0);
    vm.loader8 = ko.observable(0);
    vm.loader9 = ko.observable(0);
    vm.loader10 = ko.observable(0);
    vm.loader11 = ko.observable(0);
    vm.loader12 = ko.observable(0);
    vm.loader13 = ko.observable(0);
    vm.loader14 = ko.observable(0);
    vm.loader15 = ko.observable(0);


    /*vm.error1 = ko.observableArray([]);
     vm.error2 = ko.observableArray([]);
     vm.error3 = ko.observableArray([]);
     vm.error4 = ko.observableArray([]);
     vm.error5 = ko.observableArray([]);
     vm.error6 = ko.observableArray([]);
     vm.error7 = ko.observableArray([]);
     vm.error8 = ko.observable('');
     vm.error9 = ko.observable('');
     vm.error10 = ko.observable('');
     vm.error11 = ko.observable('');
     vm.error12 = ko.observable('');
     vm.error13 = ko.observable('');
     vm.error14 = ko.observable('');*/


    vm.initialized.subscribe(function() {
        $('#container').removeClass('hide');
    });

    //FUNCTIONS
    vm.toggleCustomerAux = function() {
        if (vm.showCustomerAux()) {
            vm.showCustomerAux(0);
        } else {
            vm.showCustomerAux(1);
        }
    }

    vm.inputIccid.subscribe(function() {
        vm.inputMsisdn('');
        vm.inputCustomerId('');
    });

    vm.inputMsisdn.subscribe(function() {
        vm.inputIccid('');
        vm.inputCustomerId('');
    });

    vm.inputCustomerId.subscribe(function() {
        vm.inputIccid('');
        vm.inputMsisdn('');
    });
    vm.think = function(thinkMore) {
        var count = vm.thinking();
        if (thinkMore) {
            count++;
        } else {
            count--;
        }
        if (count<0) count=0;
        vm.thinking(count);

    }

    vm.toggleSimOriginAux = function() {
        if (vm.showSimOriginAux()) {
            vm.showSimOriginAux(0);
        } else {
            vm.showSimOriginAux(1);
        }


    }

    vm.toggleMoreTransitions = function() {
        if (vm.showMoreTransitions()) {
            vm.showMoreTransitions(0);
        } else {
            vm.showMoreTransitions(1);
        }
    }

    vm.map = {
        'internal__GetMVNEDetails': vm.sourceObj1,
        'internal__GetAllCustomerStateTransitions':vm.sourceObj2,
        'internal__GetState': vm.sourceObj3,
        'customercare__SearchCustomers':vm.sourceObj4,
        'customercare__CustomerInfo':vm.sourceObj5,
        'customercare__ValidateSIMOrigin':vm.sourceObj6,
        'mvnecare__ListCustomerInvocations':vm.sourceObj7,
        'mvnecare__ListCustomerMakeitso':vm.sourceObj8,
        'internal__GetCustomerAggregatedSoapLog':vm.sourceObj9,
        'internal__QueryPortIn':vm.sourceObj10,
        'customercare__CancelPortIn':vm.sourceObj11,
        'internal__GetActCodeFromICCID':vm.sourceObj12,
        'internal__GetCancellationReasons': vm.sourceObj13,
        'internal__GetIMEICustomerData': vm.sourceObj14,
        'customercare__FlexInfo': vm.sourceObj15
    };
    vm.errorMap = {
        'internal__GetMVNEDetails': vm.errObj1,
        'internal__GetAllCustomerStateTransitions':vm.errObj2,
        'internal__GetState': vm.errObj3,
        'customercare__SearchCustomers':vm.errObj4,
        'customercare__CustomerInfo':vm.errObj5,
        'customercare__ValidateSIMOrigin':vm.errObj6,
        'mvnecare__ListCustomerInvocations':vm.errObj7,
        'mvnecare__ListCustomerMakeitso':vm.errObj8,
        'internal__GetCustomerAggregatedSoapLog':vm.errObj9,
        'internal__QueryPortIn':vm.errObj10,
        'customercare__CancelPortIn':vm.errObj11,
        'internal__GetActCodeFromICCID':vm.errObj12,
        'internal__GetCancellationReasons': vm.errObj13,
        'internal__GetIMEICustomerData': vm.errObj14,
        'customercare__FlexInfo': vm.errObj15
    };


    vm.makeRow = function(key,value) {
        return {key:key, value:value };
    }



    vm.startApi = function(apiName) {
        var sourceObj = vm.map[apiName];
        var errObj = vm.errorMap[apiName];
        sourceObj(1);

        var apiObj = {
            name: apiName,
            status: sourceObj,
            errors: errObj

        };

        var alreadyInArray = 0;
        _.each(vm.invokedAppArr(), function(obj) {
            if (obj.name==apiName) alreadyInArray=1;
        });

        if (!alreadyInArray) vm.invokedAppArr.push(apiObj)


    }

    vm.objToArr = function(obj) {

        //alert(obj);

        var obsArr = ko.observableArray([]);


        _.each(JSON.parse(obj), function(val, key) {
            var tempObj = {
                key: key.replace('_',' '), val:val
            };
            obsArr.push(tempObj);
        });


        return obsArr;

    }

    vm.pushNewPair = function(key,val,obsArr) {

        var tempObj = {
            key: key.replace('_',' '), val:val
        };

        obsArr.push(tempObj);

        return obsArr;
    }


    vm.responseOk = function(data,apiName) {
        vm.think(0);
        var sourceObj = vm.map[apiName];
        var errObj = vm.errorMap[apiName];

        if (!data) {
            errObj.push( apiName + ' returned no data' );
            vm.hasError(1);
            sourceObj(3);
            //vm.showDependencyErrors(apiName);
            return false;
        } else if (!data.success) {
            errObj(data.errors);
            vm.hasError(1);
            sourceObj(3);
            //vm.showDependencyErrors(apiName);
            return false;
        } else {
            sourceObj(2);
            return true;
        }
    }



    vm.doSearch = function() {

        vm.resetUI();

        if (vm.inputMsisdn()) {

            vm.foundMsisdn(vm.inputMsisdn().replace(/\s|\(|\)|\-/g, ''));
            vm.path = 3;
            vm.getMVNEDetails();
            vm.customerInfoByMsisdn();

        }
        else if (vm.inputIccid()) {

            vm.foundICCID(vm.inputIccid());
            vm.path = 4;
            vm.getMVNEDetails();
            vm.SIMOrigin();
            vm.getActCode();
            vm.searchCustomers();
        }

        else if (vm.inputCustomerId()) {
            vm.foundCustomerId(vm.inputCustomerId());
            vm.path = 5;
            vm.getAllCustomerStateTransitions();
            vm.getState();
            vm.listCustomerInvocations();
            vm.listCustomerMakeitso();
            vm.flexInfo();
        }


    }

    vm.getIMEICustomerData = function(iccid) // internal__GetIMEICustomerData
    {
      vm.think(1);

      var apiName = 'internal__GetIMEICustomerData';
      vm.startApi(apiName);

      $.ajax({
        type: 'POST',
        url: '/pr/internal/2/ultra/api/internal__GetIMEICustomerData',
        dataType: 'json',
        data: { ICCID : iccid },
        success: function(data) {
          if (vm.responseOk(data,apiName))
          {
            vm.imeiList(data.imei_list);
            vm.makeList(data.make_list);
            vm.modelList(data.model_list);
            vm.modelNameList(data.model_name_list);
            vm.osList(data.os_list);
            vm.deviceTypeList(data.device_type_list);
          }
        }
      });
    }

    vm.getCancellationReasons = function(customer_id) { // internal__GetCancellationReasons
        vm.think(1);

        var input = {};

        input.customer_id = customer_id;

        if (!Object.keys(input).length)
            return;

        var apiName = 'internal__GetCancellationReasons';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url: "/pr/internal/2/ultra/api/internal__GetCancellationReasons",
            //url: "/js/acc-sample.json",
            dataType: 'json',
            data: input,
            success:  function(data) {
                if (vm.responseOk(data,apiName)) {
                    var table = document.getElementById('cancellation_reasons');
                    var head = ['REASON', 'TYPE', 'DEACTIVATION_DATE', 'MSISDN', 'ICCID', 'STATUS', 'AGENT'];

                    try
                    { 
                        while (table.rows.length)
                            table.deleteRow(-1);
                        
                        if (data.errors.length)
                            throw data.errors[0];
        
                        if (data.cancellation_reasons.length)
                        {
                            data.cancellation_reasons.forEach(function(reason)
                            {
                                var row = table.insertRow(-1);
                                row.className = 'row_transition';
                                head.forEach(function(name)
                                {
                                    var cell = row.insertCell(-1);
                                    cell.innerHTML = reason[name];
                                });
                            });
                        }
                        else
                        {
                            var row = table.insertRow(-1);
                            var cell = row.insertCell(-1);
                            cell.colSpan = head.length;
                            cell.className = 'noRecords'
                            cell.innerHTML = 'No Records Found';
                        }
                    }
                    catch (error)
                    {
                        var row = table.insertRow(-1);
                        var cell = row.insertCell(-1);
                        cell.colSpan = head.length;
                        cell.className = 'statusError';
                        cell.style.display = 'table-cell';
                        cell.innerHTML = 'Error';
                    }
                }
            
            }
        });
    }

    vm.getMVNEDetails = function() { // internal__GetMVNEDetails


        vm.initialized(1); //hide the welcome message

        vm.think(1);

        var apiName = 'internal__GetMVNEDetails';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url: "/pr/internal/2/ultra/api/internal__GetMVNEDetails",
            //url: "/js/acc-sample.json",
            dataType: 'json',
            data: { ICCID : vm.foundICCID(), MSISDN : vm.foundMsisdn() },

            success:  function(data) {

                vm.getCancellationReasons(vm.foundCustomerId());
                vm.getIMEICustomerData(vm.foundICCID());

                if (vm.responseOk(data,apiName)) {

                    var msisdnTemp = vm.foundICCID();
                    var iccidTemp = vm.foundICCID();

                    if (vm.path==3) {
                        if (!vm.foundICCID()) {
                            vm.foundICCID(data.GetNetworkDetails_SIM);
                            vm.SIMOrigin();
                            vm.getActCode();
                        }
                    }
                    if (vm.path==4) {
                        if (!vm.foundMsisdn()) {
                            vm.foundMsisdn(data.GetNetworkDetails_MSISDN);
                            vm.customerInfoByMsisdn();

                        }
                    }
                    if (vm.path==3 || vm.path==4) {
                        //vm.getCustomerAggregatedSoapLog();
                        if (!vm.foundCustomerId()) {
                            vm.foundCustomerId(data.customer_id);
                            vm.getAllCustomerStateTransitions();
                            vm.getState();
                            vm.listCustomerInvocations();
                            vm.flexInfo();
                            vm.listCustomerMakeitso();
                        }
                    }

                    //if we didn't have both msisdn and iccid before the call, but we do now, get the soap log
                    if (!(msisdnTemp && iccidTemp) && (vm.foundMsisdn() && vm.foundICCID())) {
                        vm.getCustomerAggregatedSoapLog();
                    }



                    //vm.getCustomerSoapLog();

                    if (data.GetNetworkDetails_MSISDN != data.QuerySubscriber_MSISDN) {
                        //TODO mismatch error
                    }
                    if (data.GetNetworkDetails_SIM != data.QuerySubscriber_SIM) {
                        //TODO mismatch error
                    }




                    if (!vm.foundCustomerId()) {
                        var err = '';
                        if (vm.inputIccid()) {
                            err = 'No customer_id found for this ICCID';
                        } else {
                            err = 'No customer_id found for this MSISDN';
                        }
                    }

                    vm.mvneDetailsObj(data);

                   vm.querySubArr([
                        {key:'ICCID', value:data.QuerySubscriber_SIM },
                        {key:'Billing IMEI', value:data.QuerySubscriber_IMEI },
                        {key:'IMSI', value:data.QuerySubscriber_IMSI },
                        {key:'MSISDN', value:data.QuerySubscriber_MSISDN },
                        {key:'Plan Effective Date', value:data.QuerySubscriber_PlanEffectiveDate },
                        {key:'Plan Expiration Date', value:data.QuerySubscriber_PlanExpirationDate },
                        {key:'PlanId', value:data.QuerySubscriber_PlanId },
                        {key:'Subscriber Status', value:data.QuerySubscriber_SubscriberStatus },
                        {key:'Wholesale Plan', value:data.QuerySubscriber_wholesalePlan },
                        {key:'Auto Renewal Indicator', value:data.QuerySubscriber_AutoRenewalIndicator }

                    ]);
                    vm.checkBalanceArr([
                        {key:'MSISDN', value:data.CheckBalance_MSISDN }
                    ]);

                    vm.addOnList(vm.flattenAddOnFeatureInfo( data.QuerySubscriber_AddOnFeatureInfoList.AddOnFeatureInfo ));

                    vm.balanceValueList(vm.flattenBalanceValue( data.CheckBalance_BalanceValueList.BalanceValue ));

                    vm.showMvneDetails(1); //show the MVNEDetails panel

                }

            }
        });
    }



    vm.scrollToBottom = function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    }


    vm.getAllCustomerStateTransitions = function() { // internal__GetMVNEDetails


        vm.initialized(1);

        if (!vm.foundCustomerId()) return false; //TODO - couldn't call this, no customer_id

        vm.think(1);

        var apiName = 'internal__GetAllCustomerStateTransitions';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url: "/pr/internal/2/ultra/api/internal__GetAllCustomerStateTransitions",
            dataType: 'json',
            data: { customer_id : vm.foundCustomerId() },

            success:  function(data) {
                if (vm.responseOk(data,apiName)) {

                    if (vm.path==5) {
                        vm.foundMsisdn(data.msisdn);
                        vm.foundICCID(data.iccid);
                        vm.getMVNEDetails();
                        vm.getCustomerAggregatedSoapLog();
                        vm.customerInfoByMsisdn();
                        vm.SIMOrigin();
                        vm.getActCode();
                    }

                    data.balance = vm.fDollars(data.balance);
                    data.packaged_balance1 = vm.fDollars(data.packaged_balance1/60/100/10);
                    data.packaged_balance2 = data.packaged_balance2/600;
                    data.customer_stored_balance = vm.fDollars(data.customer_stored_balance);

                    vm.custTransObj(data);

                    var html = '';

                    var l_bAutoPortRequest = 0;

                    var l_aMonths = [];
                    l_aMonths['Jan'] = 0;
                    l_aMonths['Feb'] = 1;
                    l_aMonths['Mar'] = 2;
                    l_aMonths['Apr'] = 3;
                    l_aMonths['May'] = 4;
                    l_aMonths['Jun'] = 5;
                    l_aMonths['Jul'] = 6;
                    l_aMonths['Aug'] = 7;
                    l_aMonths['Sep'] = 8;
                    l_aMonths['Oct'] = 9;
                    l_aMonths['Nov'] = 10;
                    l_aMonths['Dec'] = 11;

                    var l_xNow = new Date();
                    var l_nNow = l_xNow.getTime();

                    var counter = 0;

                    for ( var i=0 ; i<data.state_transitions.length ; i += 8 ) {
                        counter++;

                        if (counter>4) {
                            html += '<tr data-bind="visible:showMoreTransitions()" class="row_transition" id="row_transition_' + i + '">';
                        } else {
                            html += '<tr class="row_transition" id="row_transition_' + i + '">';
                        }

                        //format the "Created" timestamp to work with javascript, then figure out if the transition happened within the past 30 days
                        var l_bWithin30 = 0;
                        var l_sDate = data.state_transitions[ i+5 ];
                        var l_aDate = l_sDate.split(' ');
                        var l_xDate = new Date();
                        l_xDate.setFullYear(l_aDate[2],l_aMonths[l_aDate[0]],l_aDate[1]);
                        var l_nThen = l_xDate.getTime();
                        if (((l_nNow - l_nThen) / 1000 / 60 / 60 / 24) < 31) l_bWithin30=1;

                        //if Port Request within 30 days, automatically check porting status
                        if (data.state_transitions[ i+3 ]=='Port-In Requested' && data.msisdn && !l_bAutoPortRequest && l_bWithin30) {

                            l_bAutoPortRequest=1;
                            $('#msisdn').val(data.msisdn);
                            $('#section_porting_status').addClass('section_porting_status_auto').show();

                            $("#div_button").hide();
                            $("#div_button_loading img").show();

                            $("#ajax_target_11").html(' ');
                            $("#ajax_target_8").html(' ');

                            button_porting_status_action();
                            button_is_eligible_portin_action();
                        }

                        for ( var j=0 ; j< 8 ; j++ ) {
                            if ( j == 0 ) {
                                html += '<td style="white-space: nowrap"><a href="#" class="state_transition" value=' + i + '>' + data.state_transitions[ i+j ] + '</a></td>';
                            }
                            else {
                                if ( ( j == 1 ) && ( data.state_transitions[ i+j ] == 'ABORTED' ) ) {
                                    html += '<td class="fontRed">' + data.state_transitions[ i+j ] + ' &nbsp; <a href="#" class="recover_transition" value=' + data.state_transitions[ i ] + '>Recover</a></td>';
                                }
                                else {
                                    html += '<td>' + data.state_transitions[ i+j ] + '</td>';
                                }
                            }
                        }

                        html += '</tr>' +
                            '<tr style="display:none;" id="actions_row_' + i + '"><td><div id="div_actions_loading_' + i + '"></div></td>' +
                            '<td colspan="7" style="padding: 0px; padding-top: 0px; padding-bottom: 0px; padding-right: 0px; padding-left: 0px;">' +
                            '<div id="div_actions_target_' + i + '" style="margin: 0 0 0 0; padding: 0px; padding-top: 0px; padding-bottom: 0px; padding-right: 0px; padding-left: 0px; border-width: 0px;">' +
                            '</div></td></tr>';
                    }


                    vm.transitionCount(counter);
                    vm.transitionsHtml(1);

                    $('#transitionsTbody').html(html);

                    var element = $('#transitionsDiv')[0];
                    ko.cleanNode(element);
                    ko.applyBindings(vm, document.getElementById('transitionsDiv'));



                    // associate show_actions_for_transition with transition_uuid click
                    $('a.state_transition').click(function() {
                        show_actions_for_transition( $(this).text() , $(this).attr("value") );
                        return false;
                    });

                    // associate recover transition
                    $('a.recover_transition').click(function() {
                        recover_transition( $(this) );
                        return false;
                    });

                }
            }
        });
    }



    vm.getState = function() { // internal__GetMVNEDetails

        if (!vm.foundCustomerId()) return false; //TODO - couldn't call this, no customer_id

        vm.think(1);

        var apiName = 'internal__GetState';
        vm.startApi(apiName);
        //vm.sourceObj3(1);

        $.ajax({
            type: 'POST',
            url:      "/pr/internal/2/ultra/api/internal__GetState",
            dataType: 'json',
            data: { customer_id : vm.foundCustomerId() },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    vm.custStateObj(data);


                }
            }
        });
    }



    vm.searchCustomers = function() { // internal__GetMVNEDetails


        vm.think(1);


        var apiName = 'customercare__SearchCustomers';
        vm.startApi(apiName);
        //vm.sourceObj4(1);

        $.ajax({
            type: 'POST',
            url:      "/pr/customercare/2/ultra/api/customercare__SearchCustomers",
            dataType: 'json',


            data: { iccid : vm.foundICCID(), name : '', email : '', actcode : '' },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {


                    if (data.customers.length && data.customers[0]) {

                        if (!vm.foundMsisdn()) {
                            vm.foundMsisdn(data.customers[0].current_mobile_number);
                            vm.customerInfoByMsisdn();
                            vm.getCustomerAggregatedSoapLog();
                        }
                        if (!vm.foundCustomerId()) {
                            vm.foundCustomerId(data.customers[0].customer_id);
                            vm.getAllCustomerStateTransitions();
                            vm.getState();
                            vm.listCustomerInvocations();
                            vm.flexInfo();
                            vm.listCustomerMakeitso();
                        }

                    }


                }
            }
        });
    }

    vm.flexInfo = function() {

        vm.think(1);

        var apiName = 'customercare__FlexInfo';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url:      "/pr/customercare/2/ultra/api/customercare__FlexInfo",
            dataType: 'json',
            data: { customer_id : vm.foundCustomerId() },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    data.shared_intl = vm.fDollars(data.shared_intl/60000);
                    vm.flexInfoObj(data);
                }
            }
        });
    }



    vm.customerInfoByMsisdn = function() { // internal__GetMVNEDetails

        if (!vm.foundMsisdn()) return false; //TODO - couldn't call this

        vm.think(1);

        var apiName = 'customercare__CustomerInfo';
        vm.startApi(apiName);
        //vm.sourceObj5(1);

        $.ajax({
            type: 'POST',
            url:      "/pr/customercare/2/ultra/api/customercare__CustomerInfo",
            dataType: 'json',

            data: { phone_number : vm.foundMsisdn(), mode: '' },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    if (!vm.foundICCID()) {
                        vm.foundICCID(data.current_iccid);
                        vm.SIMOrigin();
                        vm.getActCode();
                        if (vm.foundMsisdn()) vm.getCustomerAggregatedSoapLog();
                    }
                    if (!vm.foundCustomerId()) {
                        vm.foundCustomerId(data.customer_id);
                        vm.getAllCustomerStateTransitions();
                        vm.getState();
                        vm.listCustomerInvocations();
                        vm.flexInfo();
                        vm.listCustomerMakeitso();
                    }

                    vm.custInfoObj(data);

                    vm.custPlanStarted(filters.formatDate(data.customer_plan_start));
                    vm.custPlanEnded(filters.formatDate(data.customer_plan_expires));

                    if (data.paid_through)
                        vm.custMultiMonthEnds(filters.formatDate(data.paid_through));

                    if (data.duration)
                        vm.custMultiMonthDuration(data.duration);

                    var ultra_customer_options = '';
                    for ( var i=0 ; i<data.ultra_customer_options.length ; i+=2 ) {
                        if (data.ultra_customer_options[i].indexOf("LTE_SELECT") > -1 && data.customer_plan == 'Univision $30') {
                            ultra_customer_options += "";
                        } else {
                            ultra_customer_options += data.ultra_customer_options[i] + ' => ' + data.ultra_customer_options[i+1] + " &nbsp; &nbsp; &nbsp; &nbsp; ";
                        }
                    }

                    vm.custPlanOptions(ultra_customer_options);

                    // BOLT-26 begin
                    if (data.bolt_ons.length)
                    {
                      document.getElementById('bolt_ons').innerHTML = '';
                      data.bolt_ons.forEach(function(value) { document.getElementById('bolt_ons').innerHTML += value.description + '<br>'; });
                    }
                    else
                      document.getElementById('bolt_ons').innerHTML = 'none';
                    getBoltOnHistory(data.customer_id);

                    if (data.wholesale_plan)
                    {
                      document.getElementById('wholesale_plan').innerHTML =data.wholesale_plan;
                    }
                }
            }
        });
    }


    vm.SIMOrigin = function() { // internal__GetMVNEDetails

        if (!vm.foundICCID()) return false; //TODO do something here

        vm.think(1);

        var apiName = 'customercare__ValidateSIMOrigin';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url: "/pr/customercare/2/ultra/api/customercare__ValidateSIMOrigin",
            dataType: 'json',
            data: { iccid : vm.foundICCID() },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    vm.simOriginArr.push(vm.makeRow('Inventory Master', vm.fNotSet(data.inventory_master)));
                    vm.simOriginAuxArr.push(vm.makeRow('Inventory Distributor', vm.fNotSet(data.inventory_distributor)));
                    vm.simOriginAuxArr.push(vm.makeRow('Inventory Dealer', vm.fNotSet(data.inventory_dealer)));
                    vm.simOriginAuxArr.push(vm.makeRow('Activated Master', vm.fNotSet(data.activated_master)));
                    vm.simOriginAuxArr.push(vm.makeRow('Activated Distributor', vm.fNotSet(data.activated_distributor)));
                    vm.simOriginAuxArr.push(vm.makeRow('Activated Dealer', vm.fNotSet(data.activated_dealer)));
                    vm.simOriginAuxArr.push(vm.makeRow('MVNE', vm.fNotSet(data.mvne)));
                    vm.simOriginAuxArr.push(vm.makeRow('Product', vm.fNotSet(data.product)));
                    vm.simOriginAuxArr.push(vm.makeRow('SIM Enabled', vm.fNotSet(data.enabled)));
                    vm.simOriginAuxArr.push(vm.makeRow('PIN, PUK', vm.fNotSet(data.pin_and_puk_info.replace(', PIN2','<br>PIN2'))));
                    if (data.product=='ORANGE') vm.simOriginAuxArr.push(vm.makeRow('Orange Stored Value', data.stored_value));
                    vm.simOriginAuxArr.push(vm.makeRow('SIM Order', vm.fNotSet(data.iccid_batch_id)));
                    vm.simOriginAuxArr.push(vm.makeRow('SIM Order Type', vm.fNotSet(data.sku)));
                    //vm.simOriginAuxArr.push(vm.makeRow('Activation ICCID', '')); TODO where is Activation ICCID supposed to come from?


                }
            }
        });
    }


    vm.getActCode = function() {
        if (!vm.foundICCID()) return false;

        vm.think(1);

        var apiName = 'internal__GetActCodeFromICCID';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url: "/pr/internal/2/ultra/api/internal__GetActCodeFromICCID",
            dataType: 'json',
            data: { iccid : vm.foundICCID() },

            success:  function(data) {
                if (vm.responseOk(data,apiName)) {

                    if (data.actcode)
                    {
                        $('#actcode').html(data.actcode);
                        $('#actcode').parent().parent().show();
                    }
                    else
                        $('#actcode').parent().parent().hide();
                }
            }
        });
    }


    vm.listCustomerInvocations = function() { // internal__GetMVNEDetails

        if (!vm.foundCustomerId()) return false; //TODO - couldn't call this, no customer_id

        vm.think(1);

        var apiName = 'mvnecare__ListCustomerInvocations';
        vm.startApi(apiName);

        var now = moment().unix() + vm.timeAdjust;
        var date_from = moment.unix(now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
        var date_to = moment.unix(now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');

        $.ajax({
            type: 'POST',
            url:      "/pr/mvnecare/2/ultra/api/mvnecare__ListCustomerInvocations",
            dataType: 'json',
            data: { customer_id : vm.foundCustomerId(), date_from: date_from, date_to: date_to },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    vm.invocationsArr(data.records);



                }
            }
        });
    }

    vm.dynamicClass = function(obj) {

        if (obj.STATUS=='REPORTED ERROR' || obj.STATUS=='REPORTED MISSING') {
            if (parseInt(vm.invocTimeDiff(obj)) >= 12) {
                return 'statusRed status';
            } else {
                return 'statusYellow status';
            }
        }
        else if (obj.STATUS=='INITIATED' || obj.STATUS=='OPEN' || obj.STATUS=='RESOLVED' || obj.STATUS=='COMPLETED') {
            return 'statusGreen status';
        }
        else if (obj.STATUS=='ERROR' || obj.STATUS=='MISSING' || obj.STATUS=='FAILED') {
            return 'statusRed status';
        }
        else if (obj.STATUS=='COMPLETED DELAYED') {
            return 'statusBlue status';
        }

    }



    vm.invocTimeDiff = function(obj) {

        var last = moment.utc(obj.LAST_STATUS_DATE_TIME, "MMM DD YYYY hh:mm:ss:SSSA").zone('+0000');
        var diffHours = ((vm.now - last.unix())  / 60 / 60).toFixed(1);
        return diffHours;
    }


    vm.timeToPST = function(dateString) {
        return dateString ? moment.utc(dateString, "MMM DD YYYY hh:mm:ss:SSSA").tz("America/Los_Angeles").format("MM/DD/YY hh:mm:ssA") : '';

    }

   vm.listCustomerMakeitso = function() { // internal__GetMVNEDetails

        if (!vm.foundCustomerId()) return false; //TODO - couldn't call this, no customer_id

        vm.think(1);

        var apiName = 'mvnecare__ListCustomerMakeitso';
        vm.startApi(apiName);
        //vm.sourceObj8(1);

        var now = moment().unix() + vm.timeAdjust;

        var date_from = moment.unix(now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
        var date_to = moment.unix(now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');

        $.ajax({
            type: 'POST',
            url:      "/pr/mvnecare/2/ultra/api/mvnecare__ListCustomerMakeitso",
            dataType: 'json',
            data: { customer_id : vm.foundCustomerId(), date_from: date_from, date_to: date_to },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    vm.makeitsoArr(data.records);

                }
            }
        });
    }

    vm.getCustomerAggregatedSoapLog = function() { // internal__GetMVNEDetails

        if (!vm.foundMsisdn() && !vm.foundICCID()) return false; //TODO - couldn't call this, no customer_id

        vm.think(1);

        var apiName = 'internal__GetCustomerAggregatedSoapLog';
        vm.startApi(apiName);
        //vm.sourceObj9(1);


        var now = moment().unix() + vm.timeAdjust;
        var date_from = moment.unix(now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
        var date_to = moment.unix(now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');


        $.ajax({
            type: 'POST',
            url:      "/pr/internal/2/ultra/api/internal__GetCustomerAggregatedSoapLog",
            dataType: 'json',
            data: { ICCID : vm.foundICCID(), MSISDN: vm.foundMsisdn(), date_from: date_from, date_to: date_to },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {


                    vm.soapLogArrAll = data.records.reverse();
                    vm.soapLogPage(0);
                    if (vm.soapLogArrAll.length > vm.soapLogPageSize) {
                        vm.soapLogHasNext(1);
                    } else {
                        vm.soapLogHasNext(0);
                    }
                    vm.paginateSoapLog();



                }
            }
        });
    }

    vm.queryPortIn = function() { // internal__GetMVNEDetails

        vm.think(1);

        var apiName = 'internal__QueryPortIn';
        vm.startApi(apiName);

        $.ajax({
            type: 'POST',
            url:      "/pr/internal/2/ultra/api/internal__QueryPortIn",
            dataType: 'json',
            data: { msisdn: vm.foundMsisdn() },

            success:  function(data) {

                if (vm.responseOk(data,apiName)) {

                    vm.queryPortInObj(data);
                }
            }
        });
    }


    vm.cancelPort = function() {

        if (confirm("Are you sure?")) {
            vm.cancelPortResult('');
            vm.think(1);

            var apiName = 'customercare__CancelPortIn';
            vm.startApi(apiName);

            $.ajax({
                type: 'POST',
                url:      "/pr/customercare/2/ultra/api/customercare__CancelPortIn",
                dataType: 'json',
                data: { msisdn: vm.foundMsisdn(), customer_id: vm.foundCustomerId() },

                success:  function(data) {

                    if (vm.responseOk(data,apiName)) {
                        vm.cancelPortResult('Success');
                    } else {
                        vm.cancelPortResult(data.errors[0]);
                    }
                }
            });
        }
    }

    vm.portStatus = function() {
        vm.resetUI()
        vm.initialized(1);
        vm.showPortInfo(1);
        vm.foundMsisdn(vm.inputMsisdn().replace(/\s|\(|\)|\-/g, ''));
        vm.queryPortIn();
        //vm.isEligiblePortIn();
    }

    vm.toggleE911Form = function() {
        if (!vm.E911FormVisible())
        {
          vm.E911FormAddress1('');
          vm.E911FormAddress2('');
          vm.E911FormCity('');
          vm.E911FormState('');
          vm.E911FormZip('');
          vm.E911FormVisible(1);
        }
        else
          vm.E911FormVisible(0);
    }

    vm.toggleSMSForm = function() {
        if (!vm.smsFormVisible()) {
            vm.smsSent(0);
            vm.agent('');
            vm.smsMessage('');
            vm.smsFormVisible(1);
        } else {
            vm.smsFormVisible(0);
        }


    }


    vm.E911FormSubmit = function() {
      if (confirm('Are you sure?'))
      {
        var address1 = vm.E911FormAddress1();
        var address2 = vm.E911FormAddress2();
        var city     = vm.E911FormCity();
        var state    = vm.E911FormState();
        var zip      = vm.E911FormZip();

        vm.E911FormIsWorking(1);
        $('#E911FormSubmitBtn').text('Working');

        $.ajax({
          type: 'POST',
          url:  '/pr/internal/2/ultra/api/internal__SetE911SubscriberAddress',
          dataType: 'json',
          data: { customer_id: vm.foundCustomerId(), address: { addressLine1:address1, addressLine2:address2, city:city, state:state, zip:zip } },
          success: function(data) {
            if (data.success)
            {
              vm.E911FormSuccessful(1);
              vm.E911FormIsWorking(0);
              $('#E911FormSubmitBtn').text('Submit');
            }
            else
            {
              var error = data.errors[0] || 'There was a problem updating the E911 address, please try again or contact support';
              alert(error);
              vm.E911FormIsWorking(0);
              $('#E911FormSubmitBtn').text('Submit');
            }
          }
        })
      }
    }


    vm.sendSMS = function() { // internal__GetMVNEDetails

        if (confirm("Are you sure?")) {
            var agent = vm.agent();

            if (isNaN(parseInt(agent))) {
                alert('Agent must be an integer');
            } else if (agent.length < 1) {
                alert('Agent must not be empty');
            } else  {
                $.ajax({
                    type: 'POST',
                    url:      "/pr/customercare/2/ultra/api/customercare__CustomerDirectSMS",
                    dataType: 'json',
                    data: { customer_id : vm.foundCustomerId() , agent : agent , message : vm.smsMessage() },

                    success:  function(data) {
                        if (data.success) {
                            vm.smsSent(1);
                        } else {
                            var error = data.error[0] || 'There was a problem sending the SMS, please try again or contact support';
                            alert(error);
                            vm.smsSent(0);
                        }
                    }
                });
            }
        }
    }


    vm.getApiTime = function() { // internal__GetMVNEDetails


        $.ajax({
            type: 'POST',
            url: "/pr/customercare/2/ultra/api/customercare__ValidateSIMOrigin",
            dataType: 'json',
            data: { iccid: '' },

            success:  function(data) {

                vm.apiTime = data.ultra_trans_epoch;
                var browserTime = moment().unix();
                vm.timeAdjust = vm.apiTime - browserTime;
            }
        });
    }

    vm.paginateSoapLog = function() {
        vm.soapLogArr(vm.soapLogArrAll.slice(vm.soapLogPage()*vm.soapLogPageSize, vm.soapLogPage()*vm.soapLogPageSize+vm.soapLogPageSize));
    }

    vm.soapLogNext = function() {
        vm.soapLogPage(vm.soapLogPage()+1);
        if (vm.soapLogArrAll.length > vm.soapLogPageSize*(vm.soapLogPage()+1)) {
            vm.soapLogHasNext(1);
        } else {
            vm.soapLogHasNext(0);
        }
        vm.paginateSoapLog();
    }

    vm.soapLogPrev = function() {
        vm.soapLogPage(vm.soapLogPage()-1);
        vm.soapLogHasNext(1);
        vm.paginateSoapLog();
    }

    /***** aux functions for getMVNEDetails *****/

    vm.flattenBalanceValue = function( arrayBalanceValue ) {
        var html = '';//<table><thead><tr><th>Name</th><th>Type</th><th>Value</th></tr></thead><tbody>';

        for ( var i=0 ; i<arrayBalanceValue.length ; i++ ) {

            var item = arrayBalanceValue[i];
            //if (vm.validateBalanceValue(item)) {

            if (!item.UltraName) item.UltraName = item.Name;
            if (!item.UltraType) item.UltraType = item.Type;

            // API-415: DA7 value is cents, show as dolares
            if (item.Type == 'DA7') {
                item.Value = '$' + (item.Value / 100).toFixed(2);
                item.UltraName = 'INTL Roaming Voice + SMS Wallet';
                item.UltraType = 'Credit Available';
            }

            html+='<tr>';
            html += ('<td>' + (typeof item.UltraName == 'object' ? '' : item.UltraName.replace(/\//g, ' / ')) + '</td>');
            html += ('<td>' + item.UltraType + '</td>');
            html += ('<td>' + item.Value + (item.Unit=='Unit' ? '' : ' ' + item.Unit) + '</td>');
            html+='</tr>';

        }

        return html;
    }
    vm.flattenNAPFeatureList = function( arrayNAPFeature ) {
        if (!arrayNAPFeature) return '';
        var html = '', separator = '';

        for ( var i=0 ; i<arrayNAPFeature.length ; i++ ) {
            html += separator + arrayNAPFeature[ i ].FeatureName;
            separator = ' ; ';
        }

        return html;
    }
    vm.flattenHLRFeatureList = function( arrayHLRFeature ) {
        if (!arrayHLRFeature) return '';
        var html = '';
        var separator = '';

        for ( var i=0 ; i<arrayHLRFeature.length ; i++ ) {
            html += separator + arrayHLRFeature[ i ].FeatureName;
            separator = ' ; ';
        }

        return html;
    }

    vm.flattenAddOnFeatureInfo = function( arrayAddOnFeature ) {
        var html = '';//<table><thead><tr><th>ID</th><th>Description</th><th>Service Name</th><th>Feature Date</th><th>Exp Date</th></tr></thead><tbody>';
        var separator = '';

        for ( var i=0 ; i<arrayAddOnFeature.length ; i++ ) {

            var item = arrayAddOnFeature[i];
            html+='<tr>';
            html += '<td>' + item.FeatureID + '</td>';
            html += '<td>' + item.UltraDescription + '</td>';
            html += '<td>' + item.UltraServiceName + '</td>';
            html += '<td>' + item.FeatureDate.replace('T',' ') + '</td>';
            html += '<td>' + item.ExpirationDate.replace('T',' ') + '</td>';
            //html += '<td>' + item.AutoRenewalIndicator + '</td>';
            html+='</tr>';

        }
        // html += '</tbody></table>';

        return html;
    }

    vm.formatNAPStatus = function(status) {
        if (status=='A') return 'Active';
        if (status=='C') return 'Cancelled';
        if (status=='S') return 'Suspended';

    }

    vm.validateBalanceValue = function(item) {
        var okDA = ['DA1', 'DA2', 'DA14', 'DA15'];
        if (okDA.indexOf(item.Type) >= 0 || parseInt(item.Value)) {
            return true;
        } else {
            return false;
        }
    }


    /***** EO aux functions for getMVNEDetails *****/


    /***** Save to File *****/

    vm.container = document.querySelector('#container');
//var typer = container.querySelector('[contenteditable]');
    vm.output = vm.container.querySelector('output');

    vm.MIME_TYPE = 'text/html';

    vm.downloadFile = function() {
        window.URL = window.webkitURL || window.URL;

        var prevLink = vm.output.querySelector('a');
        if (prevLink) {
            window.URL.revokeObjectURL(prevLink.href);
            vm.output.innerHTML = '';
        }

        var theContent = $('#theContent').html();
        var fauxHtml = '<!doctype html><html class="no-js" lang="en"><head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><title>DIG | ACC</title><link rel="stylesheet" href="https://seanapi-dev.ultra.me//css/acc-foundation-5.css" /></head><body>' + theContent  + '</body>';

        var bb = new Blob([fauxHtml], {type: vm.MIME_TYPE});

        var a = document.createElement('a');
        a.download = vm.container.querySelector('input[type="text"]').value;
        a.href = window.URL.createObjectURL(bb);
        a.textContent = 'Download ready';

        a.dataset.downloadurl = [vm.MIME_TYPE, a.download, a.href].join(':');
        a.draggable = true; // Don't really need, but good practice.
        a.classList.add('dragout');

        vm.output.appendChild(a);

        a.onclick = function(e) {
            $('output').html('');
        };
    };


    vm.resetUI = function() {

        vm.path = 0;

        vm.errors([]);
        vm.foundMsisdn(''); //the authoritative MSISDN (either searched by, or queried via the ICCID)
        vm.foundICCID(''); //the authoritative ICCID (either searched by, or queried via the MSISDN)
        vm.foundCustomerId('');

        vm.errors([]); //list of errors at the top of the page

        vm.showCustomerAux(0);
        vm.showSimOriginAux(0);
        vm.showMoreTransitions(0);

        vm.simOriginArr([]);
        vm.simOriginAuxArr([]);
        vm.invocationsArr([]);
        vm.makeitsoArr([]);
        vm.soapLogArr([]);

        vm.invokedAppArr([]);
        vm.dependencyErrArr([]);

        vm.transitionCount(0);
        vm.mvneDetailsObj({ GetNetworkDetails_APNList: { APN: { APNName: null, APNValue: null} }, GetNetworkDetails_NAPFeatureList: {NAPFeature: null}, GetNetworkDetails_HLRFeatureList: {HLRFeature: null} });
        vm.networkDetailsArr([]);
        vm.querySubArr([]);
        vm.checkBalanceArr([]);
        vm.addOnList('');
        vm.balanceValueList('');
        vm.custPlanOptions('');
        vm.custStateObj({customers:[]});
        vm.custTransObj({});
        vm.custInfoObj({});
        vm.flexInfoObj({});
        vm.custPlanStarted('');
        vm.custPlanEnded('');
        vm.custMultiMonthEnds('');
        vm.custMultiMonthDuration('');

        // getIMEICustomerData
        vm.imeiList('');
        vm.makeList('');
        vm.modelList('');
        vm.modelNameList('');
        vm.osList('');
        vm.deviceTypeList('');

        vm.showMvneDetails(0);
        vm.thinking(0);
        vm.transitionsHtml('');

        vm.cancelPortResult('');

        vm.smsSent(0);
        vm.smsFormVisible(0);
        vm.agent('');
        vm.smsMessage('');

        vm.loader1(0);
        vm.loader2(0);
        vm.loader3(0);
        vm.loader4(0);
        vm.loader5(0);
        vm.loader6(0);
        vm.loader7(0);
        vm.loader8(0);
        vm.loader9(0);
        vm.loader10(0);
        vm.loader11(0);
        vm.loader12(0);
        vm.loader13(0);
        vm.loader14(0);
        vm.loader15(0);

        $('#transitionsTbody').html('');

        vm.errObj1([]);
        vm.errObj2([]);
        vm.errObj3([]);
        vm.errObj4([]);
        vm.errObj5([]);
        vm.errObj6([]);
        vm.errObj7([]);
        vm.errObj8([]);
        vm.errObj9([]);
        vm.errObj10([]);
        vm.errObj11([]);
        vm.errObj12([]);
        vm.errObj13([]);
        vm.errObj14([]);
        vm.errObj15([]);

        vm.hasError(0);
        vm.sourceObj1(0);
        vm.sourceObj2(0);
        vm.sourceObj3(0);
        vm.sourceObj4(0);
        vm.sourceObj5(0);
        vm.sourceObj6(0);
        vm.sourceObj7(0);
        vm.sourceObj8(0);
        vm.sourceObj9(0);
        vm.sourceObj10(0);
        vm.sourceObj11(0);
        vm.sourceObj12(0);
        vm.sourceObj13(0);
        vm.sourceObj14(0);
        vm.sourceObj15(0);


        vm.showPortInfo(0);
        vm.queryPortInObj({});
        //vm.isEligiblePortInResult('');

        $('#transitionsTable tbody:eq(0)').html('');

    }



    vm.checkQueryString = function() {
        var query_string = get_query_string();

        if (query_string['customer_id']) {
            vm.inputCustomerId(query_string['customer_id']);
            vm.doSearch();

        } else if (query_string['msisdn']) {
            vm.inputMsisdn(query_string['msisdn']);
            vm.doSearch();

        } else if (query_string['search']) {
            if (query_string['search'].length < 8) {
                vm.inputCustomerId(query_string['search']);
                vm.doSearch();
            }
            else if (query_string['search'].length < 10) {
                // do nothing
            } else if (query_string['search'].length < 12) {
                vm.inputMsisdn(query_string['search']);
                vm.doSearch();
            } else if ( query_string['search'].length < 18 ) {
                //do nothing
            } else if ( query_string['search'].length < 20 ) {
                vm.inputIccid(query_string['search']);
                vm.doSearch();
            }
        }
    }

    vm.checkQueryString();
    vm.getApiTime();




    function get_query_string() {

        var query_string = {};
        var query = window.location.search.substring(1);
        var cleanQueryString;
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if (typeof query_string[pair[0]] === "undefined") {
                if(typeof pair[1] != "undefined"){
                    cleanQueryString = pair[1].replace(new RegExp("(\s|%20|-|\\)|\\()", "gi" ),'');
                    query_string[pair[0]] = cleanQueryString;
                }
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]], pair[1] ];
                query_string[pair[0]] = arr;
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }

        return query_string;
    }

    function show_actions_for_transition( transition_uuid , row_position )
    {
        $("#actions_row_"+row_position).show();
        $("#div_actions_target_"+row_position).html('l o a d i n g');

        $.ajax({
            type:     'POST',
            url:      "/pr/internal/2/ultra/api/internal__GetActionsByTransition",
            dataType: 'json',
            data:     { transition_uuid : transition_uuid },
            success:  function callback_get_actions_by_transition(data) {

                html = '';

                if ( data == null ) {
                    html = 'internal__GetActionsByTransition returned no data :_(';
                } else if ( data.success ) {
                    html = '<table class="table table-bordered table-striped">' +
                        '<thead><tr><th>Action ID</th><th>Seq</th><th>Status</th><th>Type</th><th class="p30">Name</th><th class="p40">Result</th></tr></thead><tbody>';

                    for ( var i=0 ; i<data.actions.length ; i+=6 )
                    {
                        html += "<tr>";
                        for ( var j=0 ; j<6 ; j++ )
                        {
                            if (j==5) {
                                html += '<td class="word-break p40" style="line-height: 14px; font-size: 11px;">'+data.actions[i+j]+"</td>";
                            } else if (j==4) {
                                html += '<td class="word-break p30" style="line-height: 14px; font-size: 11px;">'+data.actions[i+j]+"</td>";
                            } else {
                                html += '<td style="line-height: 14px; font-size: 11px;">'+data.actions[i+j]+"</td>";
                            }

                        }
                        html += "</tr>";
                    }

                    html += '</tbody></table>';
                } else {
                    html = '<b>internal__GetActionsByTransition Errors:</b><br>';

                    for ( var i=0 ; i<data.errors.length ; i++ )
                    { html += data.errors[i]+"<br>"; }
                }

                $("#div_actions_loading_"+row_position).hide();
                $("#div_actions_target_"+row_position).html( html );
            }
        });

    }

    function recover_transition( el )
    {

        var transition_uuid = el.attr("value");




        el.hide();
        el.parent().find('span').remove()

        //alert('Attempting '+transition_uuid+' recovery');

        el.parent().append('<span><br><i>Attempting Recovery</i></span>');

        $.ajax({
            type:     'POST',
            url:      "/pr/internal/2/ultra/api/internal__RecoverFailedTransition",
            dataType: 'json',
            data:     { transition_uuid : transition_uuid },
            error:    function error_recover_transition(data) {
                $("#ajax_feedback").append( "<br>internal__RecoverFailedTransition CGI error" );
            },
            success:  function callback_recover_transition(data) {

                html = '';

                if ( data == null ) {

                    html = '<br><b>internal__RecoverFailedTransition returned no data</b>';
                    $(el).html('Try Again').show();

                } else if ( data.success ) {

                    //html = '<br>internal__RecoverFailedTransition notes : ' + data.notes;
                    html = '<br><b>Recovery Succeeded</b>';

                } else {

                    html = '<br><b>Recovery Failed</b>';
                    $(el).html('Try Again').show();

                }

                //$("#ajax_feedback").append( html );
                el.parent().find('span').remove();
                el.parent().append('<span>' + html + '</span>');
            }
        });

    }

}

moment.tz.add({
    "zones": {
        "America/Los_Angeles": [
            "-7:52:58 - LMT 1883_10_18_12_7_2 -7:52:58",
            "-8 US P%sT 1946 -8",
            "-8 CA P%sT 1967 -8",
            "-8 US P%sT"
        ]

    },
    "rules": {
        "US": [
            "1918 1919 2 0 8 2 0 1 D",
            "1918 1919 9 0 8 2 0 0 S",
            "1942 1942 1 9 7 2 0 1 W",
            "1945 1945 7 14 7 23 1 1 P",
            "1945 1945 8 30 7 2 0 0 S",
            "1967 2006 9 0 8 2 0 0 S",
            "1967 1973 3 0 8 2 0 1 D",
            "1974 1974 0 6 7 2 0 1 D",
            "1975 1975 1 23 7 2 0 1 D",
            "1976 1986 3 0 8 2 0 1 D",
            "1987 2006 3 1 0 2 0 1 D",
            "2007 9999 2 8 0 2 0 1 D",
            "2007 9999 10 1 0 2 0 0 S"
        ],
        "CA": [
            "1948 1948 2 14 7 2 0 1 D",
            "1949 1949 0 1 7 2 0 0 S",
            "1950 1966 3 0 8 2 0 1 D",
            "1950 1961 8 0 8 2 0 0 S",
            "1962 1966 9 0 8 2 0 0 S"
        ]
    },
    "links": {}
});


function printJson(obj) {
    var str = JSON.stringify(obj, undefined, 2); // indentation level = 2
    alert(str);
}


// BOLT-26
function getBoltOnHistory(cid)
{
  var url = '/ultra_api.php';
  var params = 'bath=rest&partner=customercare&version=2&command=customercare__GetBoltOnHistoryByCustomerId&customer_id=' + cid;
  ajax = new XMLHttpRequest();
  ajax.open('POST', url, true);
  ajax.onreadystatechange = function()
  {
    if (ajax.readyState == 4 && ajax.status==200)
    {
      var table = document.getElementById('bolt_on_history');
      var head = ['REQUEST_DATE', 'SKU', 'PRODUCT', 'STATUS', 'TRANSACTION_ID', 'REQUEST_TYPE'];

      try
      {
        while (table.rows.length)
          table.deleteRow(-1);

        var response = JSON.parse(ajax.responseText);
        if (response.errors.length)
          throw response.errors[0];
        
        if (response.bolt_on_history.length)
        {
          response.bolt_on_history.forEach(function(bolt_on)
          {
            var row = table.insertRow(-1);
            row.className = 'row_transition';
            head.forEach(function(name)
            {
              var cell = row.insertCell(-1);
              cell.innerHTML = bolt_on[name];
            });
          });
        }
        else
        {
          var row = table.insertRow(-1);
          var cell = row.insertCell(-1);
          cell.colSpan = head.length;
          cell.className = 'noRecords'
          cell.innerHTML = 'No Records Found';
        }
      }
      catch (error)
      {
        var row = table.insertRow(-1);
        var cell = row.insertCell(-1);
        cell.colSpan = head.length;
        cell.className = 'statusError';
        cell.style.display = 'table-cell';
        cell.innerHTML = 'Error';
      }
    }
  }
  ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajax.setRequestHeader('Content-length', params.length);
  ajax.send(params);
}

// return a css color based on plan type
function getPlanTypeColor()
{
  var planPrefixes = ['S', 'D'];

  if (vm.custStateObj().plan_name != undefined)
  {
    if (planPrefixes.indexOf(vm.custStateObj().plan_name[0]) > -1)
      return 'blue';
  }

  return 'black';
}
