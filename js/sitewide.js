//functionality needed by all pages 7
$(document).ready(function() {
	$('#footer_fee').click(function(e) {
		e.preventDefault();
		$.get('fee_schedule.php', function(data) {
			$('#footer_fee_modal .modal-body').html(data);
		}).success(function() { $('#footer_fee_modal').modal('show'); });
		
		return false;
	});
	
	$('#footer_site').click(function(e) {
		e.preventDefault();
		$.get('site_terms_of_use.php', function(data) {
			$('#footer_site_modal .modal-body').html(data);
		}).success(function() { $('#footer_site_modal').modal('show'); });
		
		return false;
	});
	
	$('#footer_card').click(function(e) {
		e.preventDefault();
		$.get('card_terms_of_use.php', function(data) {
			$('#footer_card_modal .modal-body').html(data);
		}).success(function() { $('#footer_card_modal').modal('show'); });
		
		return false;
	});
	
	$('#footer_privacy').click(function(e) {
		e.preventDefault();
		$.get('privacy_policy.php', function(data) {
			$('#footer_privacy_modal .modal-body').html(data);
		}).success(function() { $('#footer_privacy_modal').modal('show'); });
		
		return false;
	});

	$('.faq-title a').toggle(
		function() {
			$(this).parent().next().find('p,ol').fadeIn(700);
		},
		function() {
			$(this).parent().next().find('p,ol').fadeOut(700);
		}
	);
	
	
/*	$('.faq-title').live('click', function() {
		alert('!');
	    if ($(this).next().find('p').eq(0).is(':visible')){
	      $(this).next().find('p,ol').fadeOut(700);
	    } else {
	      $(this).next().find('p,ol').fadeIn(700);
	    };
	});*/
	
});


function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);
   
}

function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function printJson(obj) {
	var str = JSON.stringify(obj, undefined, 2); // indentation level = 2
	alert(str);
}

function zeroFill( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number;
}

function callback_login(p_bSuccess) {
	if (p_bSuccess==0 && l_xSSO.l_bShouldHaveWallet) {
	//	alert('sso error');
		l_xAccountModals.override_modals_sso_error();
	} else if (p_bSuccess==1 && l_xSSO.l_bShouldHaveWallet) {
	//	alert('logged in normal');
	}// else {
	//	alert('hmmmm');
	//}
}

function callback_logout() {
	//alert('logged out mfunds');
	if (l_xSSO.l_sCallbackPage!='') window.location.href = l_xSSO.l_sCallbackPage;
}


function dalert(what) {
	if (l_xDS.l_bDebug) alert(what);
}

function printJson2(what) {
	if (l_xDS.l_bDebug) printJson(what);
}
