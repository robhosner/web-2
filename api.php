<?php
header("content-type: text/javascript");

putenv("UNIT_TESTING=0");

include_once((getenv('HTT_CONFIGROOT') . '/e.php'));

$site =$_SERVER["SERVER_NAME"];
$uvsite = FALSE;

if (preg_match("/uv/", $site))
{
  $uvsite = '(545487|530132)';
}

//set_tz_by_offset(-8);
date_default_timezone_set("America/Los_Angeles");
$global_tz_offset = -8; // offset from UTC
$global_activation_tz_offset = -7; // offset from UTC for broken activation dates
$global_dblocal_tz_offset = 0; // offset from UTC for DB-local dates

$conversion_factor = "0.0135";

$aws_key    = $e_config['aws/key'];
$aws_secret = trim(file_get_contents($e_config['aws/pfile']),"\n\r"); // TODO: error handling for missing $aws_secret
$aws_bucket = $e_config['aws/uvbucket'];

include_once("core.php");
include_once("cosid_constants.php");
include_once("db.php");
include_once("db/accounts.php");
include_once("db/customer_misc.php");
include_once("db/customers.php");
include_once("db/htt_mobile_country.php");
include_once("db/mfunds.php");
include_once("db/parent_cos.php");
include_once("db/webcc.php");
include_once("session.php");
include_once 'lib/util-common.php';

$request_id = create_guid('PHPAPI');
// Make this available as an environment variable
putenv ('REQUEST_ID=' . $request_id);

$out = array( 'request_id' => $request_id,
              'json' => array_key_exists("json", $_REQUEST),
              'sql_log' => array(),
              'errors' => array(),
              'query' => array());

teldata_change_db();

$do_callback = array_key_exists("callback",  $_REQUEST);

if (array_key_exists("zsession", $_COOKIE))
{
  $_REQUEST['zsession'] = $_COOKIE['zsession'];
}

if (array_key_exists("zsessionC", $_COOKIE) &&
    ! array_key_exists("customer", $_REQUEST))
{
  $_REQUEST['customer'] = $_COOKIE['zsessionC'];
}

$verified = array_key_exists("zsession", $_REQUEST) ? verify_session() : NULL;

/* $password_plain      = '4b5d91746feae5a13b45ce4732f34297d3e75ca1'; */
/* $password_priv       = 'aa86d6710904e7cb5c3b22826bc3902dd60de6af'; */
/* $password_transferto = '76b52f35ba35be09d3b15184f062e91c2fe9fb56'; */
/* $password_sms        = '1606062e21600e8b2113a3c1702c3f186cfdeac3'; */
/* $password_fields     = 'a5483755dc9e7a86c7a9eda35d171cd7967719e9'; */
/* $password_reports    = '4890f02fd85a26596f48bc0185c87a7b18d6c932'; */
/* $password_login      = '9644a828c3009e0dcf25714d2235b848e248facb'; */

$password_reset      = '6afa7ef54836c89ba3f7defee413d0e4';
$password_postage    = '019b6ae0e44f6614f2103fc82c842ed8d960f026';
$password_signup     = 'fa36d91f7ca4997f9e95cdc9430e157d48738a6a';
$password_sql        = 'ea43095d03f49b57ef853bb54a642c8af13f98a4';
$password_upload     = 'upload95d083f49b057ef8b54a642c8af13f98a4';
$password_mfunds     = 'mfunds95d083f49b057ef8b54a642c8af13f98a4';

if (!array_key_exists("password", $_REQUEST))
{
  $_REQUEST['password'] = 'ungiven, unforgiven';
}
$passthrough = ($_REQUEST['password'] === $password_signup ||
                $_REQUEST['password'] === $password_postage ||
                $_REQUEST['password'] === $password_upload ||
                $_REQUEST['password'] === $password_mfunds ||
				$_REQUEST['password'] === $password_reset ||
                $_REQUEST['password'] === $password_sql);

if (NULL == $verified && !$passthrough)
{
  $out['errors'][] = 'zsession was missing or invalid';

  if ($out['json'])
  {
    unset($out['sql_log']);
    unset($out['query']);
    unset($out['utmz']);
    unset($out['homeboy']);
    unset($out['transition']);

    if ($do_callback)
    {
      echo $out['callback'] . '(' . json_encode($out) . ');';
    }
    else
    {
      echo json_encode($out);
    }
  }
  else
  {
    dlog("", "api.php session verification failed; redirecting: " . json_encode($_REQUEST));
    header("Location: login.php");
  }
  exit;
}

// GRANT SELECT, UPDATE ON [dbo].[BILLING] TO [api_accounts]
//REVOKE DELETE on dbo.customers to api_accounts
//GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[customers] TO [api_accounts]
include_once("lib/indiald.php");
include_once("web.php");
include_once("fraud.php");
include_once("transferto.php");
include_once("sms.php");
include_once("switcher.php");
include_once("fields.php");
include_once("classes/postageapp.inc.php");
include_once((getenv('HTT_CONFIGROOT') . '/mfunds.php'));
include_once("mfunds.php");
require_once("Text/Password.php");
require_once("S3.php");

$transition_blocks = get_transition_blocks($conversion_factor);
$transition_table  = get_transition_table();

$out['mode']        = array_key_exists("mode", $_REQUEST)        ? $_REQUEST['mode']        : "none";
// TODO: limit certain queries to only internal IPs (173.205.112.0/25, 70.39.130.0/25)

$plans = find_plans();

$out['zsession'] = $verified[2];
$out['customer'] = $verified[0];

$customer_full = NULL;

if (is_numeric($out['customer']))
{
  $customer_full = find_customer(make_find_customer_query_anycosid($out['customer']));
}
else
{
  $customer_full = find_customer(make_find_customer_query_anycosid(-1, $out['customer']));
  if ($customer_full) $out['customer'] = $customer_full->CUSTOMER;
}

/* refresh the session */
if ($customer_full != NULL)
{
  dlog("", "Refresssshing the zsessssssion $verified[2]");
  session_save($customer_full, $verified[2]);
}

$out['callback']    = array_key_exists("callback",    $_REQUEST) ? $_REQUEST['callback']    : "nocallback";
$out['disabled']    = array_key_exists("fersure",     $_REQUEST) ? ($_REQUEST['fersure'] == "1" ? "" : "disabled") : "disabled";
$out['session']     = array_key_exists("session",     $_REQUEST) ? $_REQUEST['session']     : '';
$out['utmz']        = array_key_exists("utmz",        $_REQUEST) ? urldecode($_REQUEST['utmz']) : "";
$out['debug']       = array_key_exists("debug",       $_REQUEST) ? $_REQUEST['debug']       : FALSE;
$out['homeboy']     = array_key_exists("homeboy",     $_REQUEST) ? $_REQUEST['homeboy'] : 0;
$debug = $out['debug'];

if ($_REQUEST['password'] === $password_postage)
{
  switch ($out['mode'])
  {
  case "postage_notify_expiring":
  case "postage_zero_expired":
  case "postage_have_certs_p":
  case "postage_allocate_certs":
  case "postage_CC_will_expire":
  case "postage_CC_has_expired":
  case "postage_uv_abandoned":
  case "postage_uv_unfunded":
  case "feedback":
    break;

  default:
    $out['mode'] = "Postage mode $out[mode] requested by user without postage auth";
    break;
  }
}
else if ($_REQUEST['password'] === $password_sql)
{
  switch ($out['mode'])
  {
  case "sql":
  case "playground":
    break;

  default:
    $out['mode'] = "privileged mode $out[mode] requested by user without auth";
    break;
  }
}
else if ($_REQUEST['password'] === $password_signup)
{
  switch ($out['mode'])
  {
  case "study_signup":
  case "signup":
    break;

  default:
    $out['mode'] = "privileged mode $out[mode] requested by user without auth";
    break;
  }
}
else if ($_REQUEST['password'] === $password_reset)
{
  switch ($out['mode'])
  {
  case "reset_password":
  case "sms_password":
  case "send_username":  
	break;

  default:
    $out['mode'] = "privileged mode $out[mode] requested by user without auth";
    break;
  }
}
else if ($_REQUEST['password'] === $password_upload)
{
  switch ($out['mode'])
  {
  case "upload":
  case "upload_jumio":
  case "download":
  case "mfunds_auth_image":
    break;

  default:
    $out['mode'] = "privileged mode $out[mode] requested by user without auth";
    break;
  }
}
else if ($_REQUEST['password'] === $password_mfunds)
{
  switch ($out['mode'])
  {
  case "mfunds_reviewable":
  case "mfunds_reviewable_action":
    break;

  default:
    $out['mode'] = "privileged mode $out[mode] requested by user without auth";
    break;
  }
}

switch ($out['mode'])
{
  /* Misc functionality */
case "version":
  $out['query'] = 'select @@VERSION';
  $res = logged_mssql_query($out['query']);
  break;
case "us_customer_count":
  $out['query'] = us_customer_count_query();
  break;

case "login_status":
  $verify = verify_session();

  if ($verify)
  {
    $out['zsession'] = $verify;
  }
  else
  {
    $out['errors'][] = 'invalid session';
  }

  break;

case "sms_password":
  
  $out['username'] = $_REQUEST['username'];

  if (is_numeric($out['username']))
  {
    $q = ultra_customer_select_query(
      array(
        'select_fields' => array(
          'c.CUSTOMER',
          'u.current_mobile_number'
        ),
        'WHERE:current_mobile_number' => $out['username']
      )
    );

    $row = mssql_fetch_object(logged_mssql_query($q));
  }
  else
  {
    $q = ultra_customer_select_query(
      array(
        'select_fields' => array(
          'c.CUSTOMER',
          'u.current_mobile_number'
        ),
        'WHERE:login_name' => $out['username']
      )
    );

    $row = mssql_fetch_object(logged_mssql_query($q));
  }
  
  $out['customer'] = $row->CUSTOMER;
  
  if (isset($out['customer']) && $out['customer']!='')
  {

    $out['msisdn'] = $row->current_mobile_number;

    $customer_full = find_customer(make_find_customer_query_anycosid($out['customer']));

    $new_password = Text_Password::create();
    $fields = array('account_password' => $new_password);
    if (validate_fields($fields))
    {
      fields($fields, $customer_full, FALSE);
   
      $out['reset_clamputed'] = 'yay';
      $out['sms_text'] = 'Your new Ultra.me password is ' . $new_password;
      $out['sms_dest'] = '1' . $out['msisdn'];
      $out['query'] = send_ultra_password_sms($out);
    }
    else
    {
      $out['errors'][] = "new password validation error";
      $out['reset_clamputed'] = 'server error resetting the password';
    }
  }
  else
  {
    $out['errors'][] = $q;
	$out['reset_clamputed'] = 'server error resetting the password';
  }
  break;


case "reset_password":
  $out['username'] = $_REQUEST['username'];
  
  if (is_numeric($out['username']))
  {
    $q = ultra_customer_select_query(
      array(
        'select_fields' => array( 'c.CUSTOMER' ),
        'WHERE:current_mobile_number' => $out['username']
      )
    );

    $row = mssql_fetch_object(logged_mssql_query($q));

    $out['customer'] = $row->CUSTOMER;
  }
  else
  {
    $q = ultra_customer_select_query(
      array(
        'select_fields' => array( 'c.CUSTOMER' ),
        'WHERE:login_name' => $out['username']
      )
    );

    $row = mssql_fetch_object(logged_mssql_query($q));

    $out['customer'] = $row->CUSTOMER;
  }
	
  if (isset($out['customer']) && $out['customer']!='')
  {
    // TODO: do fraud control here
    $template_name = 'uv-reset-password';
    if (strpos(getenv('HTT_ENV'),'ultra') !== false) {
  	  $template_name = 'ultra-forgot-password';
    }
    reset_password($template_name, find_customer(make_find_customer_query_anycosid($out['customer'])));
  }
  else
  {
    $out['errors'][] = $q;
    $out['reset_clamputed'] = 'server error resetting password';
  }
  break;

case "send_username":
  $out['email'] = $_REQUEST['email'];
  $q = customers_select_query(
    array(
      'select_fields' => array('CUSTOMER', 'LOGIN_NAME'),
      'e_mail'        => $out['email']
    )
  );
  $row = mssql_fetch_object(logged_mssql_query($q));
  $out['customer'] = $row->CUSTOMER;
  $customer_full = find_customer(make_find_customer_query_anycosid($out['customer']));

  if (isset($out['customer']) && $out['customer']!='')
  {
	
	$template_name = 'uv-send-username';
	  if (strpos(getenv('HTT_ENV'),'ultra') !== false) {
		$template_name = 'ultra-send-username';
	  }
    $out['postage'] = postage_post_template($template_name,
                                            array($customer_full),
                                            FALSE,
                                            $row->LOGIN_NAME);
    $out['reset_clamputed'] = 'yay';
    $out['username'] = $row->LOGIN_NAME;
  }
  else
  {
    $out['errors'][] = $q;
    $out['reset_clamputed'] = 'server error sending the username';
  }
  break;


case "reports":
  $from = array_key_exists("from", $_REQUEST) ? $_REQUEST['from'] : 0;
  $to = array_key_exists("to", $_REQUEST) ? $_REQUEST['to'] : 8000000000;
  $max = array_key_exists("max", $_REQUEST) ? $_REQUEST['max'] : 2000;
  $credit = array_key_exists("credit", $_REQUEST) ? $_REQUEST['credit'] : NULL;
  $id = $out['customer'];

  $q = make_report_query($id, $credit, $from, $to, $max);
  $out['reports'] = mssql_fetch_all_rows(logged_mssql_query($q));

  foreach ($out['reports'] as &$row)
  {
    $l_nSeconds = $row[1];
    $l_xDate = new DateTime();
    $l_xDate->setTimestamp($l_nSeconds);
    $l_xDate->setTimezone(new DateTimeZone('PDT'));
    $row[] = $l_xDate->getOffset()/60;
  }

  break;

case "fields":
  /* $out['mfunds_child_id'] = array_key_exists("mfunds_child_id", $_REQUEST) ? $_REQUEST['mfunds_child_id'] : NULL; */
  $out['mfunds_child_null'] = array_key_exists("mfunds_child_null", $_REQUEST) ? $_REQUEST['mfunds_child_null'] : FALSE;

  $fields = array_key_exists("fields", $_REQUEST) ? $_REQUEST['fields'] : array();

  if(is_array($fields))
  {
    // nothing to be done yet
  }
  else
  {
    $fields = json_decode($fields, TRUE);
    $fields_decode_error = debug_json_decode();

    if ($fields_decode_error) $out['fields_error'] = $fields_decode_error;
  }

  if(is_array($fields))
  {
    $fields['pin_number'] = array_key_exists("pin_number", $_REQUEST) ? $_REQUEST['pin_number'] : $out['customer'];
  }

  $out['fields'] = fields($fields, $customer_full, $out['disabled']);
  break;

case "study_signup":
  $out['source_id'] = array_key_exists("source", $_REQUEST) ? $_REQUEST['source'] : "-1";
  $out['dest_id'] = array_key_exists("dest", $_REQUEST) ? $_REQUEST['dest'] : "-1";
  $out['bifurcate'] = array_key_exists("bifurcate", $_REQUEST) ? $_REQUEST['bifurcate'] : "-1";
  $out['targets'] = find_targets($out['source_id']);
  $out['tax_rate'] = tax_rate($out['source_id']);
  $out['all_targets'] = find_targets(NULL);

  $out['study'] = study_transition($out['source_id'], $out['dest_id'], $plans, 0, 0, $out['source_id']);
  $out['chargeup_options'] = study_chargeup(0, 0, $out['source_id']);
  break;

case "signup":
  $out['uvcard'] = array_key_exists("uvcard", $_REQUEST) ? $_REQUEST['uvcard'] : FALSE;
  $now = time();
  while (find_customer(make_find_customer_query_anycosid($now)))
  {
    $now++;
  }

  $fields = array_key_exists("fields", $_REQUEST) ? $_REQUEST['fields'] : array();

  if(is_array($fields))
  {
    $missing_fields = array();
    foreach (array(
                   'account_password', 'account_login',
                   'account_first_name', 'account_last_name',
                   'account_number_email',
                   'account_country') as $required_field)
    {
      if (!array_key_exists($required_field, $fields))
      {
        $missing_fields[] = $required_field;
      }
    }

    if (count($missing_fields) < 1)
    {
      if (validate_fields($fields))
      {
        $out['customer_inserted'] = $now;

        $q = make_signup_query($now);

        if ($out['disabled'])
        {
          $out['customer'] = $now;
        }
        else
        {
          mssql_fetch_all_objects(logged_mssql_query($q));
          $customer = find_customer(make_find_new_signup_status_customer_query($now));
          if ($customer)
          {
            $country = $fields['account_country'];
            $out['fields'] = fields($fields, $customer, FALSE);
            $out['customer'] = find_customer(make_find_status_customer_query($now));
            $out['signup_cos'] = COSID_NO_PLAN;
            $out['customer_id'] = $customer->CUSTOMER_ID;
            if ($out['uvcard'])
            {
              $out['postage'] = postage_post_template('uv-account-created', array($out['customer']), false);
            }
          }
          else
          {
            $out['customer'] = NULL;
            $out['signup_error'] = "Could not create customer $now";
          }
        }
      }
      else
      {
        $out['customer'] = NULL;
        $out['signup_error'] = array('failed_validation' => fields($fields, NULL, TRUE));
      }
    }
    else
    {
      $out['customer'] = NULL;
      $out['signup_error'] = array('missing_fields' => $missing_fields);
    }
  }
  else
  {
      $out['customer'] = NULL;
      $out['signup_error'] = 'Sorry, no fields were provided as an assoc.array';
  }
  break;

case "sql":
  $sql_mode = array_key_exists("sql_mode", $_REQUEST) ? $_REQUEST['sql_mode'] : "(not given)";
  $sql_param_1 = array_key_exists("sql_param_1", $_REQUEST) ? $_REQUEST['sql_param_1'] : "-1";
  $q = NULL;
  $preview_q = NULL;
  switch ($sql_mode)
  {
  case 'webcc_rerun':
    $preview_q = webcc_rerun_select_query();
    $q         = webcc_rerun_update_query();
    break;
  case 'zero_credit_limit':
    $preview_q = accounts_zero_credit_limit_select_query();
    $q         = accounts_zero_credit_limit_update_query();
    break;
  case 'reset_user_8':
    $preview_q = reset_user_8_select_query();
    $q         = reset_user_8_update_query();
    break;
  case 'resignup_user':
    $preview_q = accounts_select_query(
      array(
        'select_fields' => array('cos_id', 'packaged_balance1', 'account_id'),
        'account_id'    => $sql_param_1
      )
    );
    $q = sprintf("UPDATE accounts
SET cos_id = ".COSID_NO_PLAN.", packaged_balance1 = 0 WHERE account_id = %s;",
      mssql_escape($sql_param_1));
    break;
  }

  if ($q != NULL)
  {
    $out['preview'] = mssql_fetch_all_objects(logged_mssql_query($preview_q));
    if ($out['disabled'])
    {
      $out['disabled_query'] = $q;
    }
    else
    {
      $out['results'] = mssql_fetch_all_objects(logged_mssql_query($q));
    }
  }
  else
  {
    $out['results'] = array();
    $out['sql_error'] = "Invalid sql_mode $sql_mode";
  }
  break;

case "postage_uv_abandoned":
  $topost = mssql_fetch_all_objects(logged_mssql_query($uv_abandoned_query));

  if ($out['disabled'])
  {
    $out['postage_todo'] = $topost;
  }
  else
  {
    $out['postage'] = postage_post_template('uv-abandoned',
                                            $topost,
                                            $out['disabled']);
  }
  break;

case "ultra_tos_accepted":
	$row = mssql_fetch_object(logged_mssql_query(sprintf("UPDATE htt_customers_overlay_ultra SET tos_accepted = CURRENT_TIMESTAMP WHERE customer_id = (SELECT c.customer_id FROM customers c, htt_customers_overlay_ultra u WHERE u.customer_id = c.customer_id AND c.CUSTOMER =  '%s')", mssql_escape($out['customer']))));
	
	break;


case "ultra_toggle_auto_recharge":
  //UPDATE IT
  mssql_fetch_object(logged_mssql_query(sprintf("UPDATE htt_customers_overlay_ultra SET monthly_cc_renewal = %s WHERE customer_id = (SELECT c.customer_id FROM customers c, htt_customers_overlay_ultra u WHERE u.customer_id = c.customer_id AND c.CUSTOMER =  '%s')", mssql_escape($_REQUEST['new_val']), mssql_escape($out['customer']))));
	
  //SELECT AND RETURN, JUST TO BE SURE...
  $row = mssql_fetch_object(logged_mssql_query(sprintf("SELECT monthly_cc_renewal FROM htt_customers_overlay_ultra WHERE customer_id = (SELECT c.customer_id FROM customers c, htt_customers_overlay_ultra u WHERE u.customer_id = c.customer_id AND c.CUSTOMER =  '%s')",mssql_escape($out['customer']))));
	
  $out['result'] = $row->monthly_cc_renewal;
	
	break;


case "postage_uv_unfunded":
  $candidates = mssql_fetch_all_objects(logged_mssql_query($uv_unfunded_query));
  
  $topost = array();
  
  $params = array(
	'personIdentifier' => 'MFUNDS:mfunds_person_id',
	'last4' => 'MFUNDS:last_four'
  );
  
  foreach ($candidates as $candidate) {
	  
	$balance = mfunds_AccountBalance($params, null, $candidate->mfunds_person_id);
    $available_balance = intval($balance->AccountBalanceResult->Available);
	$lifecycle = $candidate->application_lifecycle;
	
	if ($lifecycle==50 && ($available_balance < 10 || is_null($available_balance))) {
		array_push($topost,$candidate);
	}	
	
}

  if ($out['disabled'])
  {
    $out['postage_todo'] = $topost;
  }
  else
  {
    $out['postage'] = postage_post_template('uv-unfunded',
                                            $topost,
                                            $out['disabled']);
  }
  break;

case "postage_notify_expiring":
  $topost = mssql_fetch_all_objects(logged_mssql_query($upcoming_expirations_query));

  if ($out['disabled'])
  {
    $out['postage_todo'] = $topost;
  }
  else
  {
    $out['postage'] = postage_post_template('balance-expire',
                                            $topost,
                                            $out['disabled']);
  }
  break;

case "postage_CC_will_expire":
case "postage_CC_has_expired":

  $q = (($out['mode'] === "postage_CC_will_expire") ?
        $customers_CC_will_expire_query :
        $customers_CC_has_expired_query);

  $topost = mssql_fetch_all_objects(logged_mssql_query($q));

  if ($out['disabled'])
  {
    $out['postage_todo'] = $topost;
  }
  else
  {
    $tname = (($out['mode'] === "postage_CC_will_expire") ?
              'CreditCardWillExpire' :
              'CreditCardHasExpired');
    $out['postage'] = postage_post_template($tname,
                                            $topost,
                                            $out['disabled']);
  }
  break;

case "postage_zero_expired":
  $out['expired'] = mssql_fetch_all_objects(logged_mssql_query($accounts_to_zero_query));

  $out['postage'] = postage_clear_balance($out['expired'],
                                          $out['disabled']);

  break;

case "postage_have_certs_p":
  $out['min'] = array_key_exists("min", $_REQUEST) ? $_REQUEST['min'] : -1;
  $out['type'] = array_key_exists("type", $_REQUEST) ? $_REQUEST['type'] : '(no type given)';

  $pq = sprintf("select count(certificate) from htt_gift_certificates where account is null and certificate_type = %s", 
                mssql_escape($out['type']));
  $out['cert_count'] = mssql_fetch_all_rows(logged_mssql_query($pq));
  if (is_array($out['cert_count']) && is_array($out['cert_count'][0]))
  {
    $out['have_certs_p'] = $out['cert_count'][0][0] >= $out['min'];
  }

  break;

case "postage_allocate_certs":
  $out['type'] = array_key_exists("type", $_REQUEST) ? $_REQUEST['type'] : '(no type given)';

  $out['postage'] = postage_allocate_certs($out['type'],
                                           $out['disabled']);

  break;

case "feedback":
  $out['feedback'] = array_key_exists("feedback", $_REQUEST) ? $_REQUEST['feedback'] : '(no feedback provided)';
  $out['feedback_subject'] = array_key_exists("feedback_subject", $_REQUEST) ? $_REQUEST['feedback_subject'] : '(no feedback subject provided)';
  $out['postage'] = postage_post_template('feedback',
                                          array($customer_full),
                                          $out['disabled'],
                                          $out['feedback_subject'],
                                          $out['feedback']);
  $out['feedback_back'] = '01';
  break;

  /* TransferTo functionality */
case "transferto_msisdn_info":
  $out['destination_msisdn'] = array_key_exists("destination_msisdn", $_REQUEST) ? $_REQUEST['destination_msisdn'] : 'invalid number';
  $out['operatorid'] = array_key_exists("operatorid", $_REQUEST) ? $_REQUEST['operatorid'] : NULL;

  $data = transferto_msisdn_info($out);
  $fraud = fraud_detect($customer_full, 'transferto_msisdn_info', $data);
  if ($fraud)
  {
    $out['errors'][] = $fraud;
  }
  else
  {
    $out['tto'] = transferto_interaction($data);
    $final_tto = $out['tto'];
    if (is_array($final_tto) && array_key_exists('operatorid', $final_tto) &&
        array_key_exists('destination_currency', $final_tto) &&
        ($final_tto['destination_currency'] === 'INR' ||
         $final_tto['destination_currency'] === 'IDR'))
    {
      $out['tto']['prices'] = array_combine(
        explode(',', $out['tto']['product_list']),
        explode(',', $out['tto']['wholesale_price_list']));

      foreach ($out['tto']['prices'] as $product => $price)
      {
        if ($product > 350 && ! ($final_tto['destination_currency'] === 'IDR'))
        {
          unset($out['tto']['prices'][$product]);
        }
        else
        {
         $out['tto']['prices'][$product] =
           transferto_cost($customer_full, $final_tto['operatorid'], $product, $out['tto']['prices'][$product]);
          mc_write("TTO:$final_tto[operatorid] $product", $out['tto']['prices'][$product]);
        }
      }

      //$out['tto'][] = transferto_build_pricelist($final_tto['operatorid']);
    }

    unset($out['tto']['retail_price_list']);
    unset($out['tto']['wholesale_price_list']);
  }

  break;

case "transferto_simulate_topup":
  $out['disabled'] = TRUE;
  /* NOTE THERE IS NO BREAK HERE!!! */
case "transferto_topup":
  $out['msisdn'] = array_key_exists("msisdn", $_REQUEST) ? $_REQUEST['msisdn'] : 'invalid number';
  $out['destination_msisdn'] = array_key_exists("destination_msisdn", $_REQUEST) ? $_REQUEST['destination_msisdn'] : 'invalid number';
  $out['operatorid'] = array_key_exists("operatorid", $_REQUEST) ? $_REQUEST['operatorid'] : NULL;
  $out['product'] = array_key_exists("product", $_REQUEST) ? $_REQUEST['product'] : NULL;
  $out['sms'] = array_key_exists("sms", $_REQUEST) ? $_REQUEST['sms'] : NULL;

  $topup = transferto_topup($out, $customer_full, create_guid('TTO'));
  $out['tto_uuid'] = $topup['cid1'];
  //dlog("", "$request_id got topup " . json_encode($topup));

  $out['tto_queued'] = $topup;
  $out['tto_enqueue'] = transferto_enqueue($topup);
  break;

case "transferto_status":

  $uuid = array_key_exists("tto_uuid", $_REQUEST) ? $_REQUEST['tto_uuid'] : NULL;
  $cancel = array_key_exists("tto_cancel", $_REQUEST) ? $_REQUEST['tto_cancel'] : NULL;

  // $cancel and the $uuid can be NULL, but not the customer
  $out['tto_status'] = transferto_queue_check($customer_full, $uuid, $cancel);

/*
  if (is_array($final_tto) &&
      array_key_exists('error_code', $final_tto) &&
      $final_tto['error_code'] == 0)
  {
    if (array_key_exists('sms_sent', $final_tto))
    {
      $out['sms_sent'] = $final_tto['sms_sent'];
    }
    if (array_key_exists('transactionid', $final_tto))
    {
      $out['transactionid'] = $final_tto['transactionid'];
    }
  }
*/
  break;

case 'mfunds_status':
  $out['customer_record_mfunds'] = mfunds_status($customer_full->CUSTOMER_ID);
  $out['customer_record_mfunds_child'] = mfunds_status($customer_full->CUSTOMER_ID, FALSE, TRUE);
  $out['mfunds_children'] = mfunds_children($customer_full->CUSTOMER_ID);
  if ($out['customer_record_mfunds'])
  {
    $out['mfunds_password'] = md5(mfunds_password($out['customer_record_mfunds']->mfunds_person_id));
  }
  else
  {
    $out['mfunds_password'] = md5(mfunds_password($out['customer_record_mfunds_child']->mfunds_person_id));
  }
  break;

case 'mfunds_status_mperson':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  $out['customer_record_mfunds'] = mfunds_status_mperson($out['person_id']);
  $out['mfunds_password'] = md5(mfunds_password($out['person_id']));
  break;

case 'mfunds_card_image':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    );

  $details = mfunds_CardDetails($params, $customer_full, $out['person_id']);
  $out['customer_record_mfunds'] = mfunds_status_mperson($out['person_id']);

  $blank = 'images/mfunds/virtual_prod.jpg';
  $file = mfunds_stamp_image($blank, '/var/tmp', $customer_full, $out['person_id'], $details);

  if (!file_exists($file))
  {
    $file = $blank;
    dlog("", sprintf("Serving empty image for customer '%s' (person ID '%s')",
                      $customer_full->CUSTOMER_ID,
                      $out['person_id']));
  }

  stream_image($file, $file != $blank);
  break;

case 'mfunds_auth_image':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  if ($_REQUEST['password'] === $password_upload)
  {
    $out['full'] = $customer_full = find_customer(make_find_customer_query_anycosid($_REQUEST['customer']));
  }

  $out['customer_record_mperson'] = mfunds_status_mperson($out['person_id']);
  $out['fname'] = $out['customer_record_mperson']->image_file_path;

  if ($out['fname'])
  {
    $s3 = new S3($aws_key, $aws_secret);
    $file = "/var/tmp/$out[fname]";
    $s3->getObject($aws_bucket, $out['fname'], $file);
    stream_image($file, TRUE);
  }
  else
  {
    $out['errors'][] = "Could not get a image file path";
  }

  break;

case 'mfunds_upload_auth_image':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  $out['customer_record_mfunds'] = mfunds_status_mperson($out['person_id']);
  $out['old_auth_image'] = $out['customer_record_mfunds']->image_file_path;
  $out['basename'] = array_key_exists('basename', $_REQUEST) ? $_REQUEST['basename'] : '';

  $s3 = new S3($aws_key, $aws_secret);

  if ($out['basename'])
  {
    $fullname = "/var/tmp/$out[basename]";
    $fname = mfunds_image_name($out['basename']);
    $data = file_get_contents($fullname);
    if ($data)
    {
      $s3->putObject($data, $aws_bucket, $fname, S3::ACL_PRIVATE);
      $out['available'] = array($out['basename'], $fname, $s3->getObject($aws_bucket, $fname));
      $out['fields'] = fields(array("mfunds_image" => $fname), $customer_full, FALSE);
      $out['customer_record_mfunds'] = mfunds_status_mperson($out['person_id']);
      $out['new_auth_image'] = $out['customer_record_mfunds']->image_file_path;
      //$out['new_auth_image'] = $out['old_auth_image'];
      if ($out['new_auth_image'] === $out['old_auth_image'])
      {
         $out['errors'][] = "Sorry, the new auth image $fname was not recorded";
      }
      else
      {
         $out['auth_upload_success'] = TRUE;
         if (mfunds_set_lifecycle($customer_full->CUSTOMER_ID, $out['person_id'], 30))
         {
         }
         else
         {
           $out['errors'][] = "Sorry, we couldn't set the lifecycle";
         }
      }
    }
    else
    {
      $out['errors'][] = "Sorry, we couldn't read $out[basename]";
    }
  }
  else
  {
    $out['errors'][] = "Sorry, you didn't provide a basename parameter";
  }

  break;

case 'mfunds_set_card_active':

  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'username' => 'HTT-MFUNDS:mfunds_person_id',
    'password' => md5(mfunds_password($out['person_id'])),
    'mdn' => '0',
    'last4' => 'MFUNDS:last_four',
    'mobilePin' => 'MFUNDS:last_four',
    'clientRefId' => 'HTT-create-wallet-MFUNDS:application_id',
    );

  $out['mfunds']['CreateMobileWallet'] = mfunds_CreateMobileWallet($params, $customer_full, $out['person_id']);

  mfunds_update_expiration($customer_full, $out['person_id']);
  
  if ($out['mfunds']['CreateMobileWallet']) {
    if (mfunds_set_lifecycle($customer_full->CUSTOMER_ID, $out['person_id'], 65))
    {
      $out['lifecycle_updated'] = 1;
    }
    else
    {
	  $out['lifecycle_updated'] = 0;
    }
  }
  else 
  {
    $out['lifecycle_updated'] = 0;
  }

break;


case 'mfunds_upload_auth_image2':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  $out['child_customer'] = array_key_exists("child_customer", $_REQUEST) ? $_REQUEST['child_customer'] : NULL;
  if ($out['child_customer']) {
    $customer_full = find_customer(make_find_customer_query_anycosid($out['child_customer']));	
  }

  $out['basename'] = array_key_exists('basename', $_REQUEST) ? $_REQUEST['basename'] : '';

  $s3 = new S3($aws_key, $aws_secret);

  if ($out['basename'])
  {
    $fullname = "/var/tmp/$out[basename]";
    $fname = mfunds_image_name($out['basename']);
    $data = file_get_contents($fullname);
    if ($data)
    {
      $s3->putObject($data, $aws_bucket, $fname, S3::ACL_PRIVATE);
      $out['available'] = array($out['basename'], $fname, $s3->getObject($aws_bucket, $fname));
      $out['fields'] = fields(array("mfunds_image" => $fname), $customer_full, FALSE);
      $out['auth_upload_success'] = TRUE;
      
	  if (mfunds_set_lifecycle($customer_full->CUSTOMER_ID, $out['person_id'], 30))
      {
	  }
      else
      {
        $out['errors'][] = "Sorry, we couldn't set the lifecycle";
      }
      
    }
    else
    {
      $out['errors'][] = "Sorry, we couldn't read $out[basename]";
    }
  }
  else
  {
    $out['errors'][] = "Sorry, you didn't provide a basename parameter";
  }

  break;

case 'mfunds_kyc_notify':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  if (mfunds_set_lifecycle($customer_full->CUSTOMER_ID, $out['person_id'], 40))
  {
    $out['customer_record_mfunds'] = mfunds_status_mperson($out['person_id']);

    $out['postage'] = postage_uv_message('uv-kyc-notify',
                                         $customer_full,
                                         array('auth_url' => sprintf('https://services.uvnv.com/api.php?mode=mfunds_auth_image&person_id=%s&customer=%s&password=upload95d083f49b057ef8b54a642c8af13f98a4', $out['person_id'],$customer_full->CUSTOMER),
                                               'mfunds_person_id' => $out['customer_record_mfunds']->mfunds_person_id,
                                               'client_ref_id' => $out['customer_record_mfunds']->application_id,
                                           ),
                                         array('tzz@lifelogs.com', 'enright.sean@gmail.com', 'cs@mfundsglobal.com'));
  }
  else
  {
    $out['errors'][] = "Sorry, we couldn't advance the lifecycle";
  }

  break;

case 'mfunds_order_card_remove':
    $q = sprintf("DELETE FROM htt_mfunds_applications WHERE customer_id = %d;",
                 $customer_full->CUSTOMER_ID);

    run_sql_and_check($q);
    break;

  case 'mfunds_order_card_remove_mperson':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

    $q = sprintf("DELETE FROM htt_mfunds_applications WHERE mfunds_person_id = %s AND customer_id = %d;",
                 mssql_escape_with_zeroes($out['person_id']),
                 $customer_full->CUSTOMER_ID);

    run_sql_and_check($q);
    break;

case 'mfunds_order_card_prep':
  $out['parent_id'] = array_key_exists("parent_id", $_REQUEST) ? $_REQUEST['parent_id'] : NULL;
  $out['customer_id'] = array_key_exists("customer_id", $_REQUEST) ? $_REQUEST['customer_id'] : $customer_full->CUSTOMER_ID;

  $q = sprintf("DELETE FROM htt_mfunds_applications WHERE mfunds_person_id IS NULL AND parent_mfunds_person_id = %s",
               mssql_escape($out['parent_id']));
  $out['nullchild_cleanup'] = run_sql_and_check($q);

  $out['customer_record_mfunds'] = mfunds_status($customer_full->CUSTOMER_ID);

  $parent = NULL == $out['parent_id'] ? 'NULL' : mssql_escape_with_zeroes($out['parent_id']);
  /* TODO: remove dead code */
  /* if ($out['customer_record_mfunds'] && $out['parent_id']) */
  /* { */
  /*   $parent = interpolate_fields($customer_full, */
  /*                                'MFUNDS:mfunds_person_id', */
  /*                                array()); */
  /*   if ($parent == NULL) $parent = 'NULL'; */
  /* } */

  /*if ($out['customer_record_mfunds'])
  {
    $out['errors'][] = "User already has a mfunds record";
    break;
  }*/

    $q = htt_mfunds_applications_insert_query( $parent , $out['customer_id'] );

    if (! run_sql_and_check($q)) break;

    $out['customer_record_mfunds'] = mfunds_status($customer_full->CUSTOMER_ID);
    $out['mfunds_children'] = mfunds_children($customer_full->CUSTOMER_ID);

    break;

case 'mfunds_order_card_do':
  $out['parent_id'] = array_key_exists("parent_id", $_REQUEST) ? $_REQUEST['parent_id'] : NULL;

  $out['customer_record_parent'] = mfunds_status_mperson($out['parent_id']);
  $out['customer_record_child'] = mfunds_status($customer_full->CUSTOMER_ID, TRUE); /* find a child with NULL person_id */

  $order_type = 'OrderCardHTT';
  $params = array(
    'FirstName' => 'MFUNDS:first_name',
    'MidInitial' => 'MFUNDS:middle_initial',
    'LastName' => 'MFUNDS:last_name',
    'Address1' => 'MFUNDS:address1',
    'Address2' => 'MFUNDS:address2',
    'City' => 'MFUNDS:city',
    'State' => 'MFUNDS:state_region',
    'Zip' => 'MFUNDS:postal_code',
    'HomePhone' => 'MFUNDS:home_phone',
    'WorkPhone' => 'MFUNDS:work_phone',
    'Email' => 'MFUNDS:e_mail',
    'DateOfBirth' => 'MFUNDS:date_of_birth',
    'DriversLicenseState' => 'MFUNDS:state_region',
    'DriversLicense' => 'MFUNDS:alternate_id_number',
    'SocialSecurity' => 'MFUNDS:social_security',
    'Amount' => '0',
    'clientRefId' => 'HTT-MFUNDS:application_id',
    );

  if ($out['parent_id'] && !$out['customer_record_child'])
  {
    $out['errors'][] = "We could not find a child record with parent $out[parent_id], you need to run 'mfunds_order_card_prep'.";
    break;
  }

  if ($out['parent_id'] && !$out['customer_record_parent'])
  {
    $out['errors'][] = "We could not find a parent record with parent ID $out[parent_id].";
    break;
  }

  if (NULL != $out['parent_id'])
  {
    if (NULL != $out['customer_record_child']->mfunds_person_id)
    {
      $out['errors'][] = "Specified parent ID $out[parent_id] has no nullchild, sorry.";
      break;
    }

    if ($out['parent_id'] != $out['customer_record_child']->parent_mfunds_person_id)
    {
      $out['errors'][] = json_encode($out['customer_record_child']);
      $out['errors'][] = "Specified parent ID $out[parent_id] doesn't match '" . $out['customer_record_child']->parent_mfunds_person_id . "', sorry.";
      break;
    }

    if ($out['parent_id'] != $out['customer_record_parent']->mfunds_person_id)
    {
      $out['errors'][] = "Specified parent ID $out[parent_id] is not a parent, sorry.";
      break;
    }
  }

  $child_order = FALSE;
  /* this is the child order mode */
  if ($out['customer_record_parent'] && $out['customer_record_child'])
  {
    $out['child_null'] = TRUE;          /* dirty hack: this indirectly influences mfunds.php:interpolate_fields() */
    $order_type = 'OrderFamilyCardHTT';
    $params['parentPersonIdentifier'] = $out['parent_id'];
    $params['parentLast4'] = $out['customer_record_parent']->last_four;
    $params['familyCardFees'] = "5.99";
    $params['SubProgramId'] = mfunds_config('id/sub/c');
    $params['PackageId'] = mfunds_config('id/pkg/cp');

    $child_order = TRUE;
  }

  $customer_id = $child_order ? $out['customer_record_child']->customer_id : $customer_full->CUSTOMER_ID;

  //$mfunds_soap_nonet = TRUE;
  $method = "mfunds_$order_type";
  $ret = $order_type . 'Result';
  $order = $method($params, $customer_full, NULL);
  $out['mfunds'][$order_type] = $order;

  $got_order_result = $order;

  $got_instant_approval = $got_order_result &&
    $order->$ret->CardIdentifier &&
    $order->$ret->Last4 &&
    0 == $order->$ret->ResponseCode &&
    $order->$ret->Success &&
    $order->$ret->ConfirmationNumber;

  /* if (array_key_exists("simulate_reject", $_REQUEST)) $got_order_result = FALSE; */
  /* if (array_key_exists("simulate_decline", $_REQUEST)) $got_instant_approval = FALSE; */

  if ($got_order_result)
  {
    if (mfunds_set_lifecycle($customer_id, NULL, 20))
    {
      $result = $order->$ret;

      $q = '';

      if ($got_instant_approval)
      {
        //SENRIGHT - ATTEMPT AT CreateMobileWallet
        // we advance lifecycle to 50 here
        $q = sprintf("
                UPDATE htt_mfunds_applications SET
                  mfunds_person_id = %s, last_four = %s,
                  card_type = 'mFunds',
                  status = NULL, application_lifecycle = 50,
                  application_date_time = GETUTCDATE(),
                  approval_date_time = GETUTCDATE(),
                  cs_last_activity = GETUTCDATE(),
                  first_name = %s, middle_initial = %s, last_name = %s,
                  address1 = %s, address2 = %s, city = %s,
                  state_region = %s, postal_code = %s, country = 'USA',
                  home_phone = %s, work_phone = %s, e_mail = %s,
                  date_of_birth = %s,
                  agent_code = 'WebUI', alternate_id_number = %s,
                  alternate_id_type = %s,
                  social_security = %s,
                  load_amount = 0, subprogram_id = '', package_id = '',
                  decline_cause = NULL,
                  notes = ''
                WHERE customer_id = %d AND mfunds_person_id IS NULL;",
                     mssql_escape_with_zeroes($result->CardIdentifier),
                     mssql_escape_with_zeroes($result->Last4),
                     mssql_escape_with_zeroes($mfunds_soap_params['FirstName']),
                     mssql_escape_with_zeroes($mfunds_soap_params['MidInitial']),
                     mssql_escape_with_zeroes($mfunds_soap_params['LastName']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Address1']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Address2']),
                     mssql_escape_with_zeroes($mfunds_soap_params['City']),
                     mssql_escape_with_zeroes($mfunds_soap_params['State']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Zip']),
                     mssql_escape_with_zeroes($mfunds_soap_params['HomePhone']),
                       mssql_escape_with_zeroes($mfunds_soap_params['WorkPhone']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Email']),
                     mssql_escape_with_zeroes($mfunds_soap_params['DateOfBirth']),
                     mssql_escape_with_zeroes($mfunds_soap_params['DriversLicense']),
                     mssql_escape_with_zeroes('driver license ' . $mfunds_soap_params['DriversLicenseState']),
                     mssql_escape_with_zeroes($mfunds_soap_params['SocialSecurity']),
                     $customer_id);
      }
      else
      {
        $q = sprintf("
        UPDATE htt_mfunds_applications SET
          mfunds_person_id = %s,
          card_type = 'mFunds',
          status = NULL, application_lifecycle = 30,
          application_date_time = GETUTCDATE(),
          first_name = %s, middle_initial = %s, last_name = %s,
          address1 = %s, address2 = %s, city = %s,
          state_region = %s, postal_code = %s, country = 'USA',
          home_phone = %s, work_phone = %s, e_mail = %s,
          date_of_birth = %s,
          agent_code = 'WebUI', alternate_id_number = %s,
          alternate_id_type = %s,
          social_security = %s,
          load_amount = 0, subprogram_id = '', package_id = '',
          decline_cause = NULL,
          notes = ''
        WHERE customer_id = %d AND mfunds_person_id IS NULL;",
                     mssql_escape_with_zeroes($result->CardIdentifier),
                     mssql_escape_with_zeroes($mfunds_soap_params['FirstName']),
                     mssql_escape_with_zeroes($mfunds_soap_params['MidInitial']),
                     mssql_escape_with_zeroes($mfunds_soap_params['LastName']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Address1']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Address2']),
                     mssql_escape_with_zeroes($mfunds_soap_params['City']),
                     mssql_escape_with_zeroes($mfunds_soap_params['State']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Zip']),
                     mssql_escape_with_zeroes($mfunds_soap_params['HomePhone']),
                     mssql_escape_with_zeroes($mfunds_soap_params['WorkPhone']),
                     mssql_escape_with_zeroes($mfunds_soap_params['Email']),
                     mssql_escape_with_zeroes($mfunds_soap_params['DateOfBirth']),
                     mssql_escape_with_zeroes($mfunds_soap_params['DriversLicense']),
                     mssql_escape_with_zeroes('driver license ' . $mfunds_soap_params['DriversLicenseState']),
                     mssql_escape_with_zeroes($mfunds_soap_params['SocialSecurity']),
                     $customer_id);
      }

      $check = run_sql_and_check($q);

      if ($check)
      {
        $out['child_null'] = FALSE;  /* dirty hack: this indirectly influences mfunds.php:interpolate_fields() */
        mfunds_update_expiration($customer_full, $result->CardIdentifier);
        $out['customer_record_mperson'] = mfunds_status_mperson($result->CardIdentifier);
        //senright -> ADDING CreatedMobileWallet to ConvertCardHTT
        if ($child_order)
        {
          if (mfunds_set_lifecycle($customer_id, $result->CardIdentifier, 60, $result->Last4))
          {
            $child_customer = find_customer(make_find_status_customer_query($customer_id));
            reset_password('uv-child-approved', $child_customer);

            /*$params = array(
              'personIdentifier' => $result->CardIdentifier,
              'username' => 'HTT-' . $result->CardIdentifier,
              'password' => md5(mfunds_password($result->CardIdentifier)),
              'mdn' => '0',
              'last4' => $result->Last4,
              'mobilePin' => $result->Last4,
              'clientRefId' => 'HTT-create-wallet-MFUNDS:application_id',
              );

            $out['mfunds']['CreateMobileWallet'] = mfunds_CreateMobileWallet($params, $customer_full, $result->CardIdentifier);*/
          }
          else
          {
            $out['errors'][] = "Sorry, could not step forward the child lifecycle";
          }
        }
        else                            /* non-child success */
        {
          $out['postage'] = postage_post_template('uv-account-approved', array($customer_full), false);
        }
      }
      else
      {
        $out['errors'][] = "Sorry, the order update failed";
      }
    }
    else
    {
      $out['errors'][] = "Sorry, could not step forward the lifecycle";
    }
  }
  else
  {
    logged_mssql_query("Malformed response to ordering card: " . json_encode($order), TRUE);
    $out['errors'][] = "Malformed response to ordering card";
  }

  $out['customer_record_mfunds'] = mfunds_status($customer_full->CUSTOMER_ID);

  break;

case 'mfunds_UpdateCardholderDetails':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'cardIdentifier' => 'MFUNDS:mfunds_person_id',
    'Address1' => 'CUSTOMERS:ADDRESS1',
    'Address2' => 'CUSTOMERS:ADDRESS2',
    'City' => 'CUSTOMERS:CITY',
    'State' => 'CUSTOMERS:STATE_REGION',
    'Zip' => 'CUSTOMERS:POSTAL_CODE',
    'HomePhone' => 'CUSTOMERS:LOCAL_PHONE',
    'WorkPhone' => 'MFUNDS:work_phone',
    'Email' => 'CUSTOMERS:E_MAIL',
    'DateOfBirth' => 'MFUNDS:date_of_birth',
    'DriversLicenseState' => 'MFUNDS:state_region',
    'DriversLicenseNo' => 'MFUNDS:alternate_id_number',
    'OtherInfo' => '',
    );

  $out['mfunds']['UpdateCardholderDetails'] = mfunds_UpdateCardholderDetails($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_CardStatus':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    );

  $out['mfunds']['CardStatus'] = mfunds_CardStatus($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_CardStatus2':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => '76943445',
    'last4' => '2883',
    );

  $out['mfunds']['CardStatus'] = mfunds_CardStatus($params, $customer_full, $out['person_id']);
  break;


case 'mfunds_CardDetails':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    );

  $out['mfunds']['CardDetails'] = mfunds_CardDetails($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_AccountBalance':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    );

  $out['mfunds']['AccountBalance'] = mfunds_AccountBalance($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_CreateMobileWallet':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'username' => 'HTT-MFUNDS:mfunds_person_id',
    'password' => md5(mfunds_password($out['person_id'])),
    'mdn' => '0',
    'last4' => 'MFUNDS:last_four',
    'mobilePin' => 'MFUNDS:last_four',
    'clientRefId' => 'HTT-create-wallet-MFUNDS:application_id',
    );

  $out['mfunds']['CreateMobileWallet'] = mfunds_CreateMobileWallet($params, $customer_full, $out['person_id']);

  mfunds_update_expiration($customer_full, $out['person_id']);

  break;

case 'mfunds_ChangeMobileNumber':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $out['mdn'] = array_key_exists('mdn', $_REQUEST) ? $_REQUEST['mdn'] : '-1';

  $params = array(
    'username' => 'HTT-MFUNDS:mfunds_person_id-MFUNDS:last_four',
    'mdn' => $out['mdn'],
    );

  $out['mfunds']['ChangeMobileNumber'] = mfunds_ChangeMobileNumber($params, $customer_full, $out['person_id']);

  mfunds_update_field($customer_full, $out['person_id'], 'mobile_number', $out['mdn']);
  mfunds_update_expiration($customer_full, $out['person_id']);
  break;

case 'mfunds_GetMobileNumber':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    );

  $out['mfunds']['GetMobileNumber'] = mfunds_GetMobileNumber($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_DirectAccessNumber':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    );

  $out['mfunds']['DirectAccessNumber'] = mfunds_DirectAccessNumber($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_LoadAccountHTT':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    'amount' => '0',
    'description' => 'faux GD Load',
    );

  $out['mfunds']['LoadAccountHTT'] = mfunds_LoadAccountHTT($params, $customer_full, $out['person_id']);
  break;

case 'mfunds_ConvertCardHTT':
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;
  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    'description' => 'physical conversion',
    'newSubProgramId' => $mfunds_config['id/sub/p'], // same value as the virtual card
    'newPackageId' => $mfunds_config['id/pkg/pp'], // Personalized physical package
    'clientRefId' => 'HTT-convert-' . time() . '-MFUNDS:application_id'
    );

  $order = mfunds_ConvertCardHTT($params, $customer_full, $out['person_id']);
  $out['mfunds']['ConvertCardHTT'] = $order;

  $got_order_result = $order;

  $got_instant_approval = $got_order_result &&
    $order->ConvertCardHTTResult->CardIdentifier &&
    $order->ConvertCardHTTResult->Last4 &&
    0 == $order->ConvertCardHTTResult->ResponseCode;

  /* if (array_key_exists("simulate_reject", $_REQUEST)) $got_order_result = FALSE; */
  /* if (array_key_exists("simulate_decline", $_REQUEST)) $got_instant_approval = FALSE; */

  if ($got_instant_approval)
  {
    if (mfunds_set_lifecycle($customer_full->CUSTOMER_ID, $out['person_id'], 60, $order->ConvertCardHTTResult->Last4))
    {
      //senright -> ADDING CreatedMobileWallet to ConvertCardHTT
      /*$params = array(
        'personIdentifier' => 'MFUNDS:mfunds_person_id',
        'username' => 'HTT-MFUNDS:mfunds_person_id',
        'password' => md5(mfunds_password($out['person_id'])),
        'mdn' => '0',
        'last4' => 'MFUNDS:last_four',
        'mobilePin' => 'MFUNDS:last_four',
        'clientRefId' => 'HTT-create-wallet-MFUNDS:application_id',
        );

      $out['mfunds']['CreateMobileWallet'] = mfunds_CreateMobileWallet($params, $customer_full, $out['person_id']);
	  */
	
      mfunds_update_expiration($customer_full, $out['person_id']);
    }
    else
    {
      $out['errors'][] = "Sorry, physical order could not step forward the lifecycle";
    }
  }
  else
  {
    logged_mssql_query("Declined or malformed response to ordering physical card: " . json_encode($order), TRUE);
    $out['errors'][] = "Declined or malformed response to ordering physical card";
  }

  $out['customer_record_mperson'] = mfunds_status_mperson($out['person_id']);

  break;

case "mfunds_reviewable":
  $out['person_id'] = array_key_exists("person_id", $_REQUEST) ? $_REQUEST['person_id'] : NULL;

  $out['preview'] = mssql_fetch_all_objects(logged_mssql_query( mfunds_reviewable_query() ));

  break;

case "playground":
  $out['full'] = $customer_full = find_customer(make_find_customer_query_anycosid($_REQUEST['customer']));

  mfunds_update_expiration($customer_full);

  break;

case "mobile_autodialer":
  if (! array_key_exists('source', $_REQUEST) ||
      ! array_key_exists('destination', $_REQUEST))
  {
    $out['errors'][] = "Sorry, the source and destination must be provided.";
    break;
  }

  $numbers = get_account_aliases($customer_full);

  $numbers[] = $_REQUEST['source'];
  $out['fields'] = fields(array('dialer' => array('alias' => $numbers)),
                          $customer_full,
                          FALSE);

  $numbers = get_account_aliases($customer_full);
  $key = array_search($_REQUEST['source'], $numbers);

  if(isset($key) && $key != FALSE)
  {
  }
  else
  {
    $out['errors'][] = "Sorry, the source could not be saved as an alias.";
    break;
  }

  $autoq = sprintf("SELECT *
FROM autodialer
WHERE account='%s' AND description LIKE 'htt_special_%%'",
               $customer_full->ACCOUNT);
  $existing = mssql_fetch_all_objects(logged_mssql_query($autoq));

  if (is_array($existing))
  {
    $existing_count = count($existing);

    if ($existing_count > 0)
    {
      $q = sprintf("UPDATE autodialer SET destination=%s
 WHERE autodialer_id IN (%d); ",
                   mssql_escape_with_zeroes($_REQUEST['destination']),
                   $existing[0]->AUTODIALER_ID);
    }
    else
    {
      $q = sprintf("INSERT INTO autodialer
      (dnis, account, destination, description)
VALUES ((%s), '%s', %s, 'htt_special_mobile_0'); ",
                   '6193458809',
                   $customer_full->ACCOUNT,
                   mssql_escape_with_zeroes($_REQUEST['destination']));
    }
  }

  $check = run_sql_and_check($q);

  if ($check)
  {
    $out['mobile'] = mssql_fetch_all_objects(logged_mssql_query($autoq));
  }
  else
  {
    $out['errors'][] = "Sorry, the autodialer update failed";
  }

  break;

case "upload":
  $out['url'] = array_key_exists('url', $_REQUEST) ? $_REQUEST['url'] : '';

  $s3 = new S3($aws_key, $aws_secret);

  if ($out['url'])
  {
    $fname = mfunds_image_name($out['url']);
    $data = file_get_contents($out['url']);
    if ($data)
    {
      $s3->putObject($data, $aws_bucket, $fname, S3::ACL_PRIVATE);
      $out['available'] = array($out['url'], $fname, $s3->getObject($aws_bucket, $fname));
    }
    else
    {
      $out['errors'][] = "Sorry, we couldn't read $out[url]";
    }
  }
  else
  {
    $out['errors'][] = "Sorry, you didn't provide a url parameter";
  }

  break;

case "upload_jumio":
  $out['url'] = array_key_exists('url', $_REQUEST) ? $_REQUEST['url'] : '';

  $s3 = new S3($aws_key, $aws_secret);

  if ($out['url'])
  {
	$url = $out['url'];

	$out['customer'] = $_REQUEST['customer'];

	$fname = 'mfunds-jumio-' . $out['customer'] . '.jpeg';
	$fname = md5($url) . "-" . $fname;

	$customer_full = find_customer(make_find_customer_query_anycosid($out['customer']));

	$out['fields'] = fields(array("mfunds_image" => $fname), $customer_full, FALSE);
	
	//AUTHENTICATE WITH JUMIO
	$auth = base64_encode('732ec267-52dc-443f-9e52-442606271e38:9bb99a98b8d7541641e40f09237b7b5c');
	$header = array("Authorization: Basic $auth");
	$opts = array( 'http' => array ('method'=>'GET', 'header'=>$header));
	$ctx = stream_context_create($opts);
	$data = file_get_contents($url,false,$ctx);

	if ($data)
    {
      //IF IMAGE SUCCESSFULLY FETCHED FROM JUMIO, UPLOAD TO S3
	  $s3->putObject($data, $aws_bucket, $fname, S3::ACL_PRIVATE);
      $out['available'] = array($out['url'], $fname, $s3->getObject($aws_bucket, $fname));
	  
	  //KYC NOTIFY IF IT HASN'T ALREADY BE TRIGGERED BY CLIENT-SIDE CODE
	  $out['customer_record_mfunds'] = mfunds_status($customer_full->CUSTOMER_ID);
	  if ($out['customer_record_mfunds']->application_lifecycle==30) {

	  	postage_uv_message('uv-kyc-notify',
	                                         $customer_full,
	                                         array('auth_url' => sprintf('https://services.uvnv.com/api.php?mode=mfunds_auth_image&customer=%s&password=upload95d083f49b057ef8b54a642c8af13f98a4', $customer_full->CUSTOMER),
	                                               'mfunds_person_id' => $out['customer_record_mfunds']->mfunds_person_id,
	                                               'client_ref_id' => $out['customer_record_mfunds']->application_id,
	                                           ),
	                                         array('tzz@lifelogs.com', 'enright.sean@gmail.com', 'cs@mfundsglobal.com'));
	   }
	   //EO KYC NOTIFY
    }
    else
    {
      $out['errors'][] = "Sorry, we couldn't read $out[url]";
    }
  }
  else
  {
    $out['errors'][] = "Sorry, you didn't provide a url parameter";
  }

  break;


case "download":
  $out['url'] = array_key_exists('url', $_REQUEST) ? $_REQUEST['url'] : '';
  $out['fname'] = array_key_exists('fname', $_REQUEST) ? $_REQUEST['fname'] : mfunds_image_name($out['url']);

  $s3 = new S3($aws_key, $aws_secret);
  $file = "/var/tmp/$out[fname]";
  $s3->getObject($aws_bucket, $out['fname'], $file);

  stream_image($file, TRUE);
  break;

  /* Country+code list functionality */
case "country_list":
  $out['countries'] = array();
  foreach (mssql_fetch_all_objects(logged_mssql_query(htt_mobile_country_select_query())) as $cc)
  {
    $out['countries'][] = $cc;
  }

  break;

  /* SMS functionality */
case "sms":
  $out['sms_text'] = array_key_exists("sms_text", $_REQUEST) ? $_REQUEST['sms_text'] : NULL;
  $out['sms_dest'] = array_key_exists("sms_dest", $_REQUEST) ? $_REQUEST['sms_dest'] : NULL;
  $out['query'] = send_sms($out);
  break;

  /* Customer functionality */
case "customer_status":
  $id = $out['customer'];
  $query = make_find_status_customer_query($id);
  $customer = find_customer($query);

case "customer_full":
  $customer = $customer_full;

  $out['customer_record'] = $customer;
  if ($out['customer_record'])
  {
    $cos_id = $customer->COS_ID;
    $out['targets'] = find_targets($cos_id);

    $tax_rate = tax_rate($customer->COS_ID);
    $out['tax_rate'] = $tax_rate;

    log_acquisition($customer->ACCOUNT);
  }

  $out['all_targets'] = find_targets(NULL);
  break;

case "customer_status_ext":
  $id = $out['customer'];

  $out['customer_record_ext'] = array();
  foreach (make_find_status_customer_query_ext($id) as $query)
  {
    $out['customer_record_ext'][] = mssql_fetch_all_objects(logged_mssql_query($query));
  }

  break;

case "transition":
  $out['fields']             = array_key_exists("fields", $_REQUEST)             ? $_REQUEST['fields']             : NULL;
  $out['source_id']          = array_key_exists("source", $_REQUEST)             ? $_REQUEST['source']             : "-1";
  $out['dest_id']            = array_key_exists("dest", $_REQUEST)               ? $_REQUEST['dest']               : "-1";
  $out['bifurcate']          = array_key_exists("bifurcate", $_REQUEST)          ? $_REQUEST['bifurcate']          : "-1";
  $out['confirm_loss']       = array_key_exists("confirm_loss", $_REQUEST)       ? $_REQUEST['confirm_loss']       : "-1";
  $out['charge_description'] = array_key_exists("charge_description", $_REQUEST) ? $_REQUEST['charge_description'] : '';
  $out['suspend']            = array_key_exists("suspend", $_REQUEST)            ? $_REQUEST['suspend']            : FALSE;

  $out['query'] = transition($out);
  break;

case "study_plans":
  $all_targets = find_targets(NULL);

  $all_cosids = array();
  foreach ($all_targets as $target)
  {
    $all_cosids[] = $target['id'];
  }

  $out['cosids'] = array_key_exists("cosids", $_REQUEST) ? $_REQUEST['cosids'] : $all_cosids;

  $out['study_plans'] = study_plans(is_array($out['cosids']) ? $out['cosids'] : array($out['cosids']));

  if (is_array($out['study_plans']) && array_key_exists('error', $out['study_plans']) && $out['study_plans']['error'] == TRUE)
  {
    $result = $out['study_plans']['result'];
    $out['errors'][] = $result;
  }
  break;

case "study_transition":
  $id = $out['customer'];
  $query = make_find_status_customer_query($id);

  $customer = find_customer($query);
  $out['source_id'] = array_key_exists("source", $_REQUEST) ? $_REQUEST['source'] : "-1";
  $out['dest_id'] = array_key_exists("dest", $_REQUEST) ? $_REQUEST['dest'] : "-1";
  $out['bifurcate'] = array_key_exists("bifurcate", $_REQUEST) ? $_REQUEST['bifurcate'] : "-1";

  $out['study'] = study_transition($out['source_id'], $out['dest_id'], $plans, $customer->BALANCE, $customer->PACKAGED_BALANCE1, $customer->COS_ID);
  break;

case "study_chargeup":
  $id = $out['customer'];
  $query = make_find_status_customer_query($id);
  $customer = find_customer($query);
  $out['chargeup_options'] = study_chargeup(
    NULL == $customer ? 0 : $customer->BALANCE,
    NULL == $customer ? 0 : $customer->PACKAGED_BALANCE1,
    array_key_exists("cos_id", $_REQUEST) ? $_REQUEST['cos_id'] : $customer->COS_ID);
  break;

case "chargeup":
case "charge":
  $out['chargeup'] = array_key_exists("chargeup", $_REQUEST) ? $_REQUEST['chargeup'] : '';
  $out['amount'] = array_key_exists("amount", $_REQUEST) ? $_REQUEST['amount'] : '';
  $out['charge_description'] = array_key_exists("charge_description", $_REQUEST) ? $_REQUEST['charge_description'] : '';

  $out['customer_record'] = $customer_full;

  log_acquisition($customer_full->ACCOUNT);

  $out['query'] = ($out['mode'] === "chargeup") ? chargeup($out) : charge($out);
  break;

case "testall":
  // one invalid cosid
  $all = array(100, COSID_INDIALD_MONTHLY_1000, COSID_INDIALD_MONTHLY_2000, COSID_INDIALD_MONTHLY_3000, COSID_INDIALD_MONTHLY_500, COSID_INDIALD_MONTHLY_1500, COSID_INDIALD_MONTHLY_250, COSID_INDIALD_MONTHLY_5000, COSID_INDIA_LD_MONTHLY_250, COSID_INDIA_LD_MONTHLY_500, COSID_INDIA_LD_MONTHLY_1000, COSID_INDIA_LD_MONTHLY_1500, COSID_INDIA_LD_MONTHLY_2000, COSID_INDIA_LD_MONTHLY_3000, COSID_INDIA_LD_MONTHLY_5000, COSID_INDIALD_USA_MONTHLY_1000, COSID_INDIALD_USA_MONTHLY_1500, COSID_INDIALD_USA_MONTHLY_2000, COSID_INDIALD_USA_MONTHLY_500, COSID_1_5_CALLING_CARD, COSID_1_5_CALLING_CARD_T, COSID_QUARTER_CALLS, COSID_QUARTER_CALLS_T, COSID_PAYG_MINUTES, COSID_PAYG_MINUTES_T);
  // all the bifurcations possible plus -1 for disabled
  $bifurcations = array("Nonzero Minutes", "Immediate", "-1");
  $plans = find_plans();
  foreach ($all as $source)
  {
    foreach ($all as $dest)
    {
      foreach ($bifurcations as $b)
      {
        $runthis = array("customer_id" => $customer_full->CUSTOMER_ID, "source_id" => $source, "dest_id" => $dest, "bifurcate" => $b, 'confirm_loss' => 1, 'session' => '123456');
        print json_encode($runthis) . "\n";
        $out['query'] = transition($runthis);
        print json_encode($out) . "\n\n";
      }
    }
  }
  break;

default:
  $res = FALSE;
  $out['errors'][] = "Invalid mode " . $out['mode'];
  break;
}

if ($out['query'])
{
  $out[$out['mode']] = array();

  $out['query'] = is_string($out['query']) ? array($out['query']) : $out['query'];

  foreach ($out['query'] as $q)
  {
    if (is_string($q) && preg_match("/^\s*--/", $q))
    {
    }
    else
    {
      // translate transitions
      $todo = '';
      if (is_array($q))
      {
        if (array_key_exists("set", $q))
        {
          foreach ($q["set"] as $k => $v)
          {
            $todo .= sprintf("UPDATE ACCOUNTS SET $k=$v WHERE customer_id = %s OR account = '%s'; ", $customer_full->CUSTOMER_ID, mssql_escape($out['customer']));
          }
        }

        if (array_key_exists("block", $q))
        {
          $todo .= sprintf("--BLOCK_WAIT_FOR %s ; ", $q['block']);
        }

        if (array_key_exists("transition_clamputed", $q))
        {
          $out["transition_clamputed"] = $q["transition_clamputed"];
        }

        if (array_key_exists("chargeup_clamputed", $q))
        {
          $out["chargeup_clamputed"] = $q["chargeup_clamputed"];
        }

        if (array_key_exists("zero_bill", $q))
        {
          $customer = $q['zero_bill'][0];
          $description = $q['zero_bill'][1];
          //$todo .= sprintf("--ZERO BILLING INSERT; ");
          $todo .= sprintf("
INSERT INTO billing
( LOGIN_NAME, ENTRY_TYPE,
  account_id, ACCOUNT, account_group,
  node, node_type,
  START_DATE_TIME, CONNECT_DATE_TIME, DISCONNECT_DATE_TIME,
  DESCRIPTION, DETAIL, AMOUNT, MODULE_NAME,
  PER_CALL_CHARGE, Per_minute_charge, PER_CALL_SURCHARGE, PER_MINUTE_SURCHARGE,
  ACTUAL_DURATION, QUANTITY, CURRENCY, CONVERSION_RATE, ANI, DNIS,
  SALES_GROUP, TAX_GROUP,
  USER_1, USER_2, USER_3, USER_4, USER_5, USER_6, USER_7, USER_8, USER_9, USER_10,
  INFO_DIGITS, RATE_INTERVAL, DISCONNECT_CHARGE, BILLING_DELAY,
  GRACE_PERIOD, ACCOUNT_TYPE, PACKAGED_BALANCE_INDEX,
  CALL_SESSION_ID, CALL_ID )
VALUES ( 'api_accounts', 10,
         %d, '%s', %d,
         'TMC', 3,
         getutcdate(),getutcdate(),getutcdate(),
         %s,'Plan Change',%d,'PHP API',
         0,0,0,0,
         0,0,0,0,0,0,
         0,0,
         0,0,0,0,0,0,0,0,0,0,
         0,0,0,0,
         0,0,0,
         newid(), newid()); ",
                           $customer->ACCOUNT_ID, $customer->ACCOUNT, $customer->ACCOUNT_GROUP_ID, mssql_escape($description), 0);
        }

        $q = $todo;
      }

      $check = run_sql_and_check($q);

      if ($check)
      {
        $out[$out['mode']][$q] = $check;
      }
      else
      {
        $mssql_last_message = mssql_get_last_message();
        $out['errors'][] = ($out['debug'] ? "The [$q] query failed with result [$check]; $mssql_last_message.\n" : "The query failed with result [$check]; $mssql_last_message.\n");
      }
    }
  }
}

$fields = array_key_exists("fields", $_REQUEST) ? $_REQUEST['fields'] : NULL;

// if this is a signup transition, charge something
if ($out['mode'] === 'transition' &&
    $fields != NULL &&
    array_key_exists("chargeup", $_REQUEST))
{
  $out['chargeup'] = $_REQUEST['chargeup'];
  $out['amount'] = 0;
  $out['charge_description'] = '';
  $out['session'] = '' . $customer_full->CUSTOMER . $out['dest_id'];
  $out['customer_record'] = $customer_full;

  $out['chargeup_query'] = chargeup($out, $out['dest_id']);

  $good_chargeup = FALSE;
  foreach ($out['chargeup_query'] as $chargeup_entry)
  {
    if (is_array($chargeup_entry) &&
        array_key_exists('chargeup_clamputed', $chargeup_entry) &&
        $chargeup_entry['chargeup_clamputed'] === 'yay')
    {
      $good_chargeup = TRUE;
    }
  }

  if (!$good_chargeup)
  {
    if ($fields != NULL)            /* this is a signup charge */
    {
      $out['u8'] = revert_cc_fields_and_dec_u8($customer_full, $fields);
    }
    else
    {
      $out['u8'] = FALSE;
    }
  }
}

// make sure customer data doesn't leak out
if (array_key_exists("customer_record", $out))
{
  if ($out['mode'] === "customer_full")
  {
  }
  else
  {
    $id = $out['customer_record']->CUSTOMER_ID;
    $out['customer_record'] = find_customer(make_find_status_customer_query($id));
  }
}

if ($customer_full) $out['customer'] = $customer_full->CUSTOMER;

switch ($out['mode'])
{
  /* turn off these requests, too much noise */
case 'customer_status':
case 'customer_status_ext':
case 'study_transition':
case 'study_chargeup':
  break;
default:
  $sep = '---------------------';

  $encoded_request = json_encode($_REQUEST);

  dlog("", $request_id . ' ' . "$sep$out[mode]:REQUEST$sep");
  dlog("", $request_id . ' ' . cleanse_credit_card_string( $encoded_request ));

  if (!$debug)
  {
    dlog("", $request_id . ' ' . "$sep$out[mode]:SQL$sep");
    foreach ($out['sql_log'] as $query)
    {
      dlog("", $request_id . ' ' . $query);
    }
    dlog("", "$request_id $sep$out[mode]:OUT$sep");
    dlog("", $request_id . ' ' . json_encode($out));
  }
  break;
}

unset($out['sql_log']);
unset($out['query']);
unset($out['utmz']);
unset($out['homeboy']);
unset($out['transition']);

mc_close();

?><?php
if ($debug)
{
  print "\n/*\n";

  foreach ($out['query'] as $q)
  {
    if (is_string($q) && preg_match("/^\s*--/", $q))
    {
      print "$q\n";
    }
  }

  foreach ($out['sql_log'] as $q)
  {
    print "SQL log: $q</p>";
  }

  print "\n*/\n";
}

if ($do_callback)
{
  echo $out['callback'] . '(' . json_encode($out) . ');';
}
else
{
  echo json_encode($out);
}
?>
