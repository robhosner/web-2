use strict;
use warnings;

my $commit  = '';
my $author  = '';
my $date    = '';
my $ticket  = '';
my $comment = '';
my $separator = "\t";

# loop through standard input
while(<>)
{
  chomp;
  if ( $_ =~ /^commit\s+(\S+)\s*$/ )
  {
    if ( $author || $date || $comment )
    {
      print_all();

      $commit  = '';
      $author  = '';
      $date    = '';
      $ticket  = '';
      $comment = '';
    }

    $commit = $1;
    #print "$1\n";
  }
  elsif ( $_ =~ /^Author\:\s+([^\<]+)/ )
  {
    #print "$1\n";
    $author = $1;
  }
  elsif ( $_ =~ /^Date\:\s+(.+)$/ )
  {
    #print "$1\n";
    $date = $1;
  }
  elsif ( ! $_ )
  {
  }
  elsif ( $_ =~ /\s*([\w\-\d]+)\:\s*(.+)$/ )
  {
    $ticket = $1;
    $comment = $2;
    #print "$1\n";
    #print "$2\n";
  }
  else
  {
    $comment = $_;
  }
}

print_all();

sub print_all
{
  print join($separator,$ticket,$commit,$author,$date,$comment)."\n";
}

__END__

See ticket EA-154

