{
  "commands":
  {
      "provisioning__requestProvisionOrangeCustomerAsync":
      {
          "desc": "This API call is used to create a Orange SIM customer and 'activate' his SIM.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "actcode",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 11, "max_strlen": 11 , "actcode": "valid_actcode" },
                  "desc": "Customer's Actcode"
              },
              {
                  "name": "preferred_language",
                  "type": "string",
                  "required": false,
                  "validation": { "matches": [ "EN", "ES", "ZH" ] },
                  "desc": "Preferred language"
              },
              {
                  "name": "customerZip",
                  "type": "string",
                  "required": true,
                  "validation": { "zipcode": "US" },
                  "desc": "Billing/Customer Zipcode. Used for number allocation."
              },
              {
                  "name": "activation_channel",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Activation Caller"
              },
              {
                  "name": "activation_info",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "ID of the activator"
              },
              {
                  "name": "dealer_code",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 1, "max_strlen": 16 },
                  "desc": "tblDealerSite.Dealercd for dealer attribution by CRM"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request_id",   "type": "string",   "desc": "Asynchronous request identifier. To be used in verifyProvisionNewCustomerAsync" }
          ]
      },

      "provisioning__CheckOrangeActCode":
      {
          "desc": "This API call is used to validate and check the status of an Orange Actcode.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "actcode",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 11 , "max_strlen": 11 , "actcode": "valid_actcode" },
                  "desc": "11 digits Actcode."
              },
              {
                  "name": "mode",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Segment out certain responses"
              },
              {
                "name": "brand",
                "type": "string",
                "required": false,
                "validation": { "matches": [ "ULTRA", "UNIVISION", "MINT"] },
                "default": "ULTRA",
                "desc":"The BRAND client belongs to"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request_id",         "type": "string",    "desc": "The UUID of the transition initiated by requestProvisionOrangeCustomerAsync, if OPEN." },
              { "name": "zsession",           "type": "string",    "desc": "zsession ID"                                                                     },
              { "name": "customer_id",        "type": "integer",   "desc": "The customer ID"                                                                 },
              { "name": "sim_hot",            "type": "boolean",   "desc": "Is the SIM Hot? (ready to activate?)"                                            },
              { "name": "sim_used",           "type": "boolean",   "desc": "Has the sim been activated/assigned a number?"                                   },
              { "name": "user_state",         "type": "string",    "desc": "The state of the user associated with the sim, if any."                          },
              { "name": "mins_since_act",     "type": "integer",   "desc": "Minutes between user's associated creation date time (if any) and current time." },
              { "name": "sim_ready_activate", "type": "boolean",   "desc": "If in PREACTIVATION mode, confirms that sim is ready to be activated."           },
              { "name": "stored_value",       "type": "boolean",   "desc": "The stored_value on the SIM, helps UI determine which monthly plan it is for"    },
              { "name": "reservation_time",   "type": "string",    "desc": "MSSQL datetime value of when the activation process began"    },
              { "name": "msisdn",             "type": "string",    "desc": "Valid MSISN, if user_state is Active; otherwise, numeric zero."    },
              { "name": "duration",           "type": "integer",   "desc": "number of prepaid months"    },
              { "name": "master_agent",       "type": "string",    "desc": "SIM card master agent"       }
          ]
      },

      "provisioning__checkZipCode":
      {
          "desc": "Check to see if the ZIP Code is in the Activation Footprint.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "zip_code",
                  "type": "string",
                  "required": true,
                  "validation": { "zipcode": "US" },
                  "desc": "Zip code"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                   },
              { "name": "service_expires_epoch", "type": "epoch",    "desc": "UNIX Epoch timestamp of service expiry (implicit UTC) (if success)" },
              { "name": "valid",                 "type": "boolean",  "desc": "true if the zipcode is in the activation profile"                   }
          ]
      },

      "provisioning__checkSIMCard":
      {
          "desc": "Check if the ICCID is unused for activation.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19 , "iccid": "activate_type_purple" },
                  "desc": "ICCID to Activate (19 digits)."
              },
              {
                  "name": "mode",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Segment out certain responses."
              }
          ],
              
          "returns":
          [
              "common-returns.json",

              { "name": "sim_ready_activate",  "type": "string",  "desc": "If in PREACTIVATION mode, confirms that sim is ready to be activated." },
              { "name": "valid_ext",           "type": "string",  "desc": "'VALID','INVALID','RESERVED','USED'" },
              { "name": "valid",               "type": "string",  "desc": "'VALID','INVALID','USED'" },
              { "name": "expires_date",        "type": "string",  "desc": "expires date from HTT_INVENTORY_SIMBATCHES"},
              { "name": "master_agent",        "type": "integer", "desc": "SIM card's master agent ID"}
          ]
      },

      "provisioning__requestActivateShippedNewCustomerAsync":
      {
          "desc": "This API call is used by the website to load a 'Shipped' SIM customer 'activate' their SIM. This call uses an existing Ultra Customer.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19, "iccid": "activate_type_purple", "MVNE": "2" },
                  "desc": "ICCID to Activate (19 digits)."
              },
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                                     },
              { "name": "request_id",            "type": "string",   "desc": "Asynchronous request identifier. To be used in verifyActivateShippedNewCustomerAsync" }
          ]
      },
  
      "provisioning__verifyActivateShippedNewCustomerAsync":
      {
          "desc": "This is the API to be invoked after requestActivateShippedNewCustomerAsync in order to get the complete response.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "request_id",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 128 },
                  "desc": "Asynchronous request identifier obtained with requestActivateShippedNewCustomerAsync."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "pending",               "type": "boolean",  "desc": "True if the request has not been completed yet."                        },
              { "name": "phone_number",          "type": "string",   "desc": "phone number of the activated customer"                                 },
              { "name": "status",                "type": "string",   "desc": "'ACTIVATED' if success"                                                 }
          ]
      },

      "provisioning__verifyProvisionPortedCustomerAsync":
      {
          "desc": "This is the API to be invoked after requestProvisionPortedCustomerAsync in order to get the complete response",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "request_id",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 128 },
                  "desc": "Asynchronous request identifier obtained with requestProvisionPortedCustomerAsync."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                               },
              { "name": "pending",               "type": "boolean",  "desc": "True if the request has not been completed yet."                                },
              { "name": "service_expires_epoch", "type": "epoch",    "desc": "UNIX Epoch timestamp of service expiry (implicit UTC) (if success)"             },
              { "name": "phone_number",          "type": "string",   "desc": "phone number of the activated customer"                                         },
              { "name": "porting_success",       "type": "boolean",  "desc": "True if the porting has been completed without errors."                         },
              { "name": "porting_status",        "type": "string",   "desc": "Current porting status"                                                         },
              { "name": "resolution",                "type": "string[]", "desc": "MSISDN: R-code"                                                                 }
          ]
      },

      "provisioning__requestResubmitProvisionPortedCustomerAsync":
      {
          "desc": "Used to resume an already failed port. Funding and plan already exists for the customer; the only information that is necessary is a resubmission of the phone number.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19 },
                  "desc": "ICCID to Activate (19 digits)."
              },
              {
                  "name": "numberToPort",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 10, "min_strlen": 10 },
                  "desc": "Phone number to light up on our system."
              },
              {
                  "name": "portAccountNumber",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 20 , "regexp" : "^[A-Za-z0-9]+$" },
                  "desc": "The account number of the account to be ported."
              },
              {
                  "name": "portAccountPassword",
                  "type": "string",
                  "required": false,
                  "validation": { "max_strlen": 15 , "regexp" : "^[A-Za-z0-9]+$" },
                  "desc": "The account password of the account to be ported."
              },
              {
                  "name": "portAccountZipcode",
                  "type": "string",
                  "required": false,
                  "validation": { "zipcode": "US" },
                  "desc": "The original/billing zip code of the account to be ported."
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "provisioning__GetPortingState":
      {
          "desc": "Get a customer's porting state",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 10, "min_strlen": 10 },
                  "desc": "Phone number of the customer."
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "resolution", "type": "string[]", "desc": "MSISDN: R-code" }
          ]
      }
  }
}
