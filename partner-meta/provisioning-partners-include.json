{
  "commands":
  {
      "provisioning__requestProvisionNewCustomerAsync":
      {
          "desc": "This API call is used to create a customer and 'activate' his SIM.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19 , "iccid": "activate_type_purple" , "MVNE": "2" },
                  "desc": "ICCID to Activate (19 digits)."
              },
              {
                  "name": "loadPayment",
                  "type": "string",
                  "required": true,
                  "validation": { "matches": [ "CCARD", "PINS", "NONE" ] },
                  "desc": "Decides if the account will be loaded and activated or left in a provisioned state."
              },
              {
                  "name": "targetPlan",
                  "type": "string",
                  "required": true,
                  "validation": { "active_plan": 1 },
                  "desc": "Plan to select"
              },
              {
                  "name": "pinsToApply",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 10, "max_strlen": 94 },
                  "desc": "Up to 5 10/18 Digit PIN numbers, comma delimited. Required if loadPayment=PINS"
              },
              {
                  "name": "creditCard",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 15, "max_strlen": 16 },
                  "desc": "Credit Card Number. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditExpiry",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 4 },
                  "desc": "Credit Card Expiry in MMYY format. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditCVV",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 3, "max_strlen": 4 },
                  "desc": "Credit Card CVV code. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditAutoRecharge",
                  "type": "boolean",
                  "required": false,
                  "validation": { },
                  "desc": "Credit Card - Authorized to charge for monthly topup. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditAmount",
                  "type": "integer",
                  "required": false,
                  "validation": { "min_value": 1900 },
                  "desc": "Credit card charge amount. Must be at least the amount of the plan. Express as cents: $29 is 2900. Required if loadPayment=CCARD"
              },
              {
                  "name": "customerZip",
                  "type": "string",
                  "required": true,
                  "validation": { "zipcode": "US" },
                  "desc": "Billing/Customer Zipcode. Used for number allocation."
              },
              {
                  "name": "customerAddressOne",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 40 },
                  "desc": "Billing/Customer Address 1. Required if loadPayment=CCARD."
              },
              {
                  "name": "customerAddressTwo",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Billing/Customer Address 2."
              },
              {
                  "name": "customerCity",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 40 },
                  "desc": "Billing/Customer City. Required if loadPayment=CCARD."
              },
              {
                  "name": "customerState",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 2 },
                  "desc": "Billing/Customer State. Required if loadPayment=CCARD."
              },
              {
                  "name": "customerFirstName",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer First Name"
              },
              {
                  "name": "customerLastName",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer Last Name"
              },
              {
                  "name": "customerEMail",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer E-Mail Address"
              },
              {
                  "name": "activation_masteragent",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Celluphone Tag describing which MA did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_distributor",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Celluphone Tag describing which D did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_agent",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Celluphone Tag describing which A did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_store",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Celluphone Tag describing which Store did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_userid",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Celluphone Tag describing which userid did the Activation. If CustomerService, use value \"\" for IVR and value \"\" Online."
              },
              {
                  "name": "preferred_language",
                  "type": "string",
                  "required": false,
                  "validation": { "matches": [ "EN", "ES", "ZH" ] },
                  "desc": "Preferred language"
              },
              {
                  "name": "dealer_code",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 1, "max_strlen": 16 },
                  "desc": "tblDealerSite.Dealercd for dealer attribution by CRM"
              },
              {
                  "name": "bolt_ons",
                  "type": "string[]",
                  "required": false,
                  "validation": { },
                  "desc": "The Bolt On Ids to be associated with the customer's plan."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                               },
              { "name": "request_id",            "type": "string",   "desc": "Asynchronous request identifier. To be used in verifyProvisionNewCustomerAsync" },
              { "name": "cc_errors",             "type": "string[]", "desc": "Detailed errors in case of Credit Card charge failure."                         }
          ]
      },

      "provisioning__verifyProvisionNewCustomerAsync":
      {
          "desc": "This is the API to be invoked after requestProvisionNewCustomerAsync in order to get the complete response",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "request_id",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 128 },
                  "desc": "Asynchronous request identifier obtained with requestProvisionNewCustomerAsync."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "pending",               "type": "boolean",  "desc": "True if the request has not been completed yet."                                },
              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                               },
              { "name": "service_expires_epoch", "type": "epoch",    "desc": "UNIX Epoch timestamp of service expiry (implicit UTC) (if success)"             },
              { "name": "phone_number",          "type": "string",   "desc": "phone number of the activated customer"                                         },
              { "name": "status",                "type": "string",   "desc": "'ACTIVATED' or 'PROVISIONED'"                                                   }
          ]
      },

      "provisioning__requestProvisionPortedCustomerAsync":
      {
          "desc": "This API call is used to create a customer and 'activate' their SIM. This call is ONLY for ported in customers.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19 , "iccid": "activate_type_purple" , "MVNE": "2" },
                  "desc": "ICCID to Activate (19 digits)."
              },
              {
                  "name": "loadPayment",
                  "type": "string",
                  "required": true,
                  "validation": { "matches": [ "CCARD", "PINS", "NONE" ] },
                  "desc": "Decides if the account will be loaded and activated or left in a provisioned state."
              },
              {
                  "name": "targetPlan",
                  "type": "string",
                  "required": true,
                  "validation": { "active_plan": 1 },
                  "desc": "Plan to select"
              },
              {
                  "name": "numberToPort",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 10, "min_strlen": 10 },
                  "desc": "Phone number to light up on our system."
              },
              {
                  "name": "portAccountNumber",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 20 , "regexp" : "^[A-Za-z0-9]+$" },
                  "desc": "The account number of the account to be ported."
              },
              {
                  "name": "portAccountPassword",
                  "type": "string",
                  "required": false,
                  "validation": { "max_strlen": 15 , "regexp" : "^[A-Za-z0-9]+$" },
                  "desc": "The account password of the account to be ported."
              },
              {
                  "name": "portAccountZipcode",
                  "type": "string",
                  "required": false,
                  "validation": { "zipcode": "US" },
                  "desc": "The original/billing zip code of the account to be ported."
              },
              {
                  "name": "pinsToApply",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 10, "max_strlen": 94 },
                  "desc": "Up to 5 10/18 Digit PIN numbers, comma delimited. Required if loadPayment=PINS"
              },
              {
                  "name": "creditCard",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 15, "max_strlen": 16 },
                  "desc": "Credit Card Number. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditExpiry",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 4 },
                  "desc": "Credit Card Expiry in MMYY format. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditCVV",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 3, "max_strlen": 4 },
                  "desc": "Credit Card CVV code. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditAutoRecharge",
                  "type": "boolean",
                  "required": false,
                  "validation": { },
                  "desc": "Credit Card - Authorized to charge for monthly topup. Required if loadPayment=CCARD."
              },
              {
                  "name": "creditAmount",
                  "type": "integer",
                  "required": false,
                  "validation": { "min_value": 1900 },
                  "desc": "Credit card charge amount. Must be at least the amount of the plan. Express as cents: $29 is 2900. Required if loadPayment=CCARD"
              },
              {
                  "name": "customerZip",
                  "type": "string",
                  "required": true,
                  "validation": { "zipcode": "US" },
                  "desc": "Billing/Customer Zipcode. Used for number allocation."
              },
              {
                  "name": "customerAddressOne",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 40 },
                  "desc": "Billing/Customer Address 1. Required if loadPayment=CCARD."
              },
              {
                  "name": "customerAddressTwo",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Billing/Customer Address 2."
              },
              {
                  "name": "customerCity",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 40 },
                  "desc": "Billing/Customer City. Required if loadPayment=CCARD."
              },
              {
                  "name": "customerState",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 2 },
                  "desc": "Billing/Customer State. Required if loadPayment=CCARD."
              },
              {
                  "name": "customerFirstName",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer First Name"
              },
              {
                  "name": "customerLastName",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer Last Name"
              },
              {
                  "name": "customerEMail",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer E-Mail Address" 
              },             
              {
                  "name": "activation_masteragent",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Celluphone Tag describing which MA did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_distributor",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Celluphone Tag describing which D did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_agent",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Celluphone Tag describing which A did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_store",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Celluphone Tag describing which Store did the Activation. If CustomerService, use value \"\"."
              },
              {
                  "name": "activation_userid",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Celluphone Tag describing which userid did the Activation. If CustomerService, use value \"\" for IVR and value \"\" Online."
              },
              {
                  "name": "preferred_language",
                  "type": "string",
                  "required": false,
                  "validation": { "matches": [ "EN", "ES", "ZH" ] },
                  "desc": "Preferred language"
              },
              {
                  "name": "dealer_code",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 1, "max_strlen": 16 },
                  "desc": "tblDealerSite.Dealercd for dealer attribution by CRM"
              },
              {
                  "name": "bolt_ons",
                  "type": "string[]",
                  "required": false,
                  "validation": { },
                  "desc": "The Bolt On Ids to be associated with the customer's plan."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                                  },
              { "name": "request_id",            "type": "string",   "desc": "Asynchronous request identifier. To be used in verifyProvisionPortedCustomerAsync" },
              { "name": "cc_errors",             "type": "string[]", "desc": "Detailed errors in case of Credit Card charge failure."                            }
          ]
      },

      "provisioning__CancelRequestedPort":
      {
          "desc": "Cancels a Port Request. Then does a QueryPortIn to send the customer to Port-In Denied.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19 },
                  "desc": "ICCID to Activate (19 digits)."
              },
              {
                  "name": "numberToPort",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 10, "min_strlen": 10 },
                  "desc": "Phone number for which to cancel the Port request."
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "provisioning__checkPINCard":
      {
          "desc": "Verify that the PIN card is in our systemm, it has not been used and not disabled.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "pin",
                  "type": "string",
                  "required": true,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 pr 18 Digit PIN number"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "pin_value",         "type": "integer",   "desc": "PIN Value"             },
              { "name": "used_date",         "type": "string",    "desc": "Used on which date"    },
              { "name": "used_msisdn",       "type": "string",    "desc": "Used for which msisdn" }
          ]
      },

      "provisioning__checkPINCards":
      {
          "desc": "Validates 1 or more PINS.",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "pin",
                  "type": "string",
                  "required": true,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 or 18 Digit PIN number"
              },
              {
                  "name": "pin2",
                  "type": "string",
                  "required": false,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 or 18 Digit PIN number"
              },
              {
                  "name": "pin3",
                  "type": "string",
                  "required": false,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 or 18 Digit PIN number"
              },
              {
                  "name": "pin4",
                  "type": "string",
                  "required": false,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 or 18 Digit PIN number"
              },
              {
                  "name": "pin5",
                  "type": "string",
                  "required": false,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 or 18 Digit PIN number"
              },
              {
                  "name": "pin6",
                  "type": "string",
                  "required": false,
                  "validation": { "PIN": [ 10, 18 ] },
                  "desc": "10 or 18 Digit PIN number"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      }

  }
}
