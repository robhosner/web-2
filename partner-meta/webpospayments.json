{
  "meta": { "version": "1" },
  "security_meta": { "RBAC": "webpospayments group" },

  "commands":
  {
    "externalpayments__WebPosActivateCustomer":
    {
      "desc": "WebPOS SIM activation via EPP", 
      "parameters":
      [
        "common-parameters.json",
        {
          "name": "request_epoch",
          "type": "epoch", 
          "required": true,
          "validation": { "max_seconds_age": 300 },
          "desc": "UNIX time of original request"
        },
        {
          "name": "iccid",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 18, "max_strlen": 19 },
          "desc": "ICCID to activate"
        },
        {
          "name": "zipcode",
          "type": "string", 
          "required": true,
          "validation": { "zipcode": "US" },
          "desc": "zip code where to request new MSISDN"
        },
        {
          "name": "bolt_on_data",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 32 },
          "desc": "recurring data bolt on"
        },
        {
          "name": "bolt_on_intl",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 32 },
          "desc": "recurring international bolt on"
        },
        {
          "name": "language",
          "type": "string",
          "required": false,
          "validation": { "matches": [ "EN", "ES", "ZH" ] },
          "desc": "subscribers preferred language"
        },
        {
          "name": "provider_name",
          "type": "string", 
          "required": true,
          "validation": { "matches": [ "EPAY" ] },
          "desc": "provider name"
        },
        {
          "name": "provider_trans_id",
          "type": "string", 
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 32 },
          "desc": "payment provider transaction ID"
        },
        {
          "name": "sku",
          "required": true,
          "type": "string",
          "validation": { "min_strlen": 14, "max_strlen": 14 },
          "desc": "payment provider product SKU (14 digit EAN)"
        },
        {
          "name": "load_amount",
          "type": "integer",
          "required": true,
          "validation": { "min_value": 1900 , "max_value": 20000 },
          "desc": "value in cents to load"
        },
        {
          "name": "plan_name",
          "desc": "Ultra plan name",
          "required": true, 
          "type": "string", 
          "validation": { "min_strlen": 6, "max_strlen": 32 }
        },
        {
          "name": "subproduct_id", 
          "required": false,
          "type": "string",
          "validation": { "min_strlen": 3, "max_strlen": 6 },
          "desc": "Ultra product SKU"
        },
        {
          "name": "tmo_id",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4 , "max_strlen": 24 },
          "desc": "dealer TMO code"
        },
        {
          "name": "brand",
          "type": "string",
          "required": true,
          "validation": { "matches": ["ULTRA", "UNIVISION"] },
          "desc": "brand to which the ICCID belongs"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "ultra_payment_trans_id", "type": "string", "desc": "Ultra payment transaction ID (if success)" },
        { "name": "provider_trans_id", "type": "string", "desc": "payment provider transaction ID (same as input)" },
        { "name": "iccid", "type": "string", "desc": "subscribers ICCID (as validated)" }
      ]
    },


    "externalpayments__WebPosPortInCustomer":
    {
      "desc": "WebPOS SIM port in activation via EPP", 
      "parameters":
      [
        "common-parameters.json",
        {
          "name": "request_epoch",
          "type": "epoch", 
          "required": true,
          "validation": { "max_seconds_age": 300 },
          "desc": "UNIX time of original request"
        },
        {
          "name": "iccid",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 18, "max_strlen": 19 },
          "desc": "ICCID to activate"
        },
        {
          "name": "zipcode",
          "type": "string", 
          "required": true,
          "validation": { "zipcode": "US" },
          "desc": "zip code where to request new MSISDN"
        },
        {
          "name": "bolt_on_data",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 32 },
          "desc": "recurring data bolt on"
        },
        {
          "name": "bolt_on_intl",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 32 },
          "desc": "recurring international bolt on"
        },
        {
          "name": "language",
          "type": "string",
          "required": false,
          "validation": { "matches": [ "EN", "ES", "ZH" ] },
          "desc": "subscribers preferred language"
        },
        {
          "name": "port_carrier",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 60 },
          "desc": "subscribers preferred language"
        },
        {
          "name": "port_account_number",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "subscribers preferred language"
        },
        {
          "name": "port_account_password",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 15, "regexp" : "^[A-Za-z0-9]+$" },
          "desc": "subscribers preferred language"
        },
        {
          "name": "port_account_zipcode",
          "type": "string", 
          "required": true,
          "validation": { "zipcode": "US" },
          "desc": "zip code where to request new MSISDN"
        },
        {
           "name": "port_phone_number",
           "type": "string", 
           "required": true, 
           "validation": { "min_strlen": 10, "max_strlen": 10 }, 
           "desc": "mobile number to port in"
        },
        {
          "name": "provider_name",
          "type": "string", 
          "required": true,
          "validation": { "matches": [ "EPAY" ] },
          "desc": "provider name"
        },
        {
          "name": "provider_trans_id",
          "type": "string", 
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 32 },
          "desc": "payment provider transaction ID"
        },
        {
          "name": "sku",
          "required": true,
          "type": "string",
          "validation": { "min_strlen": 14, "max_strlen": 14 },
          "desc": "payment provider product SKU (14 digit EAN)"
        },
        {
          "name": "load_amount",
          "type": "integer",
          "required": true,
          "validation": { "min_value": 1900 , "max_value": 20000 },
          "desc": "value in cents to load"
        },
        {
          "name": "plan_name",
          "desc": "Ultra plan name",
          "required": true, 
          "type": "string", 
          "validation": { "min_strlen": 6, "max_strlen": 32 }
        },
        {
          "name": "subproduct_id", 
          "required": false,
          "type": "string",
          "validation": { "min_strlen": 3, "max_strlen": 6 },
          "desc": "Ultra product SKU"
        },
        {
          "name": "tmo_id",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4 , "max_strlen": 24 },
          "desc": "dealer TMO code"
        },
        {
          "name": "brand",
          "type": "string",
          "required": true,
          "validation": { "matches": ["ULTRA", "UNIVISION"] },
          "desc": "brand to which the ICCID belongs"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "ultra_payment_trans_id", "type": "string", "desc": "Ultra payment transaction ID (if success)" },
        { "name": "provider_trans_id", "type": "string", "desc": "payment provider transaction ID (same as input)" },
        { "name": "iccid", "type": "string", "desc": "subscribers ICCID (as validated)" }
      ]
    },


    "externalpayments__WebPosCancelCustomerTransaction":
    {
      "desc": "Cancel WebPos / EPP new SIM Activation or Port-In attempt.",
      "parameters":
      [
        "common-parameters.json",
        {
          "name": "request_epoch",
          "type": "epoch",
          "required": true,
          "validation": { "max_seconds_age": 300 },
          "desc": "UNIX Epoch timestamp of original request (implicit UTC) (at most 300 seconds ago)"
        },
        {
          "name": "iccid",
          "type": "string",
          "required": true,
          "validation": {  "min_strlen": 18, "max_strlen": 19 },
          "desc": "Unique ICCD length and identifier"
        },
        {
          "name": "cancel_type",
          "desc": "Cancellation type (VOID or REFUND)",
          "required": true,
          "type": "string",
          "validation": { "matches": [ "VOID", "REFUND" ] }
        },
        {
          "name": "provider_trans_id",
          "desc": "Transaction ID from Payment Provider",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3 }
        },
        {
          "name": "ultra_payment_trans_id",
          "desc": "Ultra Payment Transaction ID",
          "required": true,
          "type": "string",
          "validation": { "min_strlen": 3 }
        }
      ],

      "returns":
      [
        "common-returns.json",

        { "name": "ultra_payment_trans_id", "type": "string", "desc":  "Ultra Payment Transaction ID (if found)" },
        { "name": "request",                "type": "string", "desc": "Original request encoded as JSON"         }
      ]
    }
  }
}
