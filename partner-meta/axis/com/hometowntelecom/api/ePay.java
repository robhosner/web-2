package com.hometowntelecom.api;

public interface ePay
{
  public externalpayments__ApplyRealtimeReload__ret externalpayments__ApplyRealtimeReload(String widgetName, String price);
  public int[] externalpayments__ReturnsIntArray(String[] takesStringArray, String price, int myval);
  public String externalpayments__ReturnsString();
}
