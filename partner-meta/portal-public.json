{
  "commands":
  {

      "portal__GetIDDHistory":
      {
          "desc": "Obtain the rows in HTT_BILLING_HISTORY for a customer.",
          "parameters":
          [
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "start_epoch",
                  "type": "epoch",
                  "required": true,
                  "validation": { },
                  "desc": "Start Date (UNIX Epoch timestamp)"
              },
              {
                  "name": "end_epoch",
                  "type": "epoch",
                  "required": true,
                  "validation": { },
                  "desc": "End Date (UNIX Epoch timestamp)"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "idd_history",  "type": "string[]",  "desc": "flattened array." }
          ]
      },

      "portal__CheckVoiceRechargeByCustomerId":
      {
          "desc": "Return to the caller info about customer and his/her Voice Recharges.",
          "parameters":
          [
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "days_plan_expires",  "type": "integer", "desc": "Number of days until the plan would expire." },
              { "name": "voice_recharge",     "type": "string",  "desc": "JSON-encoded list of Data Recharge items"    }
          ]
      },

      "portal__ApplyVoiceRecharge":
      {
          "desc": "Debit the wallet and add Voice Balances.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 10, "max_strlen": 10 },
                  "desc": "Ultra Mobile Phone Number (10 digits)"
              },
              {
                  "name": "voice_soc_id",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Identifier of the SOC we wish to add"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "portal__CheckDataRechargeByMSISDN":
      {
          "desc": "Return to the caller info about customer and his/her Data Recharges.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 10, "max_strlen": 10 },
                  "desc": "Ultra Mobile Phone Number (10 digits)"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "days_plan_expires",  "type": "integer", "desc": "Number of days until the plan would expire." },
              { "name": "data_recharge",      "type": "string",  "desc": "JSON-encoded list of Data Recharge items"    }
          ]
      },
      "portal__CheckDataRechargeByCustomerId":
      {
          "desc": "Return to the caller info about customer and his/her Data Recharges.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "days_plan_expires",  "type": "integer", "desc": "Number of days until the plan would expire." },
              { "name": "data_recharge",      "type": "string",  "desc": "JSON-encoded list of Data Recharge items"    }
          ]
      },
      "portal__GetAllowedDataRechargeByPlan":
      {
          "desc": "Return the allowed Data Recharge items which the customer can buy given the plan.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "plan",
                  "type": "string",
                  "required": true,
                  "validation": { "active_plan": 2 },
                  "desc": "The plan"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "data_recharge",      "type": "string",  "desc": "JSON-encoded list of Data Recharge items" }
          ]
      },
      "portal__ApplyDataRecharge":
      {
          "desc": "Applies the given Data Recharge to the customer account.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "data_soc_id",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Data Recharge identifier."
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "plan",      "type": "string",  "desc": "Customer's current plan" }
          ]
      },
      "portal__ChangePlanSuspended":
      {
          "desc": "Change the plan of a Suspended customer. This will allow them to re-activate under a different plan. Should allow for plan upgrades and downgrades.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "targetPlan",
                  "type": "string",
                  "required": true,
                  "validation": { "active_plan": 1 },
                  "desc": "Plan to select"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__SetCustomerFields":
      {
          "desc": "Set address fields and credit card fields.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "first_name",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer First Name"
              },
              {
                  "name": "last_name",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer Last Name"
              },
              {
                  "name": "cc_name",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 1 },
                  "desc": "Customer billing name"
              },
              {
                  "name": "cc_address1",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 3, "max_strlen": 80 },
                  "desc": "Customer billing address"
              },
              {
                  "name": "cc_address2",
                  "type": "string",
                  "required": false,
                  "validation": { "max_strlen": 40 },
                  "desc": "Customer billing address part 2"
              },
              {
                  "name": "cc_city",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 25 },
                  "desc": "Customer billing city"
              },
              {
                  "name": "cc_country",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 25 },
                  "desc": "Customer billing country"
              },
              {
                  "name": "cc_state_or_region",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 40 },
                  "desc": "Customer billing state or region"
              },
              {
                  "name": "cc_postal_code",
                  "type": "integer",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 20 },
                  "desc": "Customer billing postal code"
              },
              {
                  "name": "account_number_phone",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 10 },
                  "desc": "Customer phone number"
              },
              {
                  "name": "account_number_email",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Customer email address"
              },
              {
                  "name": "account_cc_exp",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 4, "max_strlen": 4 },
                  "desc": "Credit Card Expiration Date - format is MMYY."
              },
              {
                  "name": "account_cc_cvv",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 3, "max_strlen": 4 },
                  "desc": "Customer CVV."
              },
              {
                  "name": "account_cc_number",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 15, "max_strlen": 16 },
                  "desc": "Credit Card Number."
              },
              {
                  "name": "address1",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 3, "max_strlen": 80 },
                  "desc": "Customer billing address"
              },
              {
                  "name": "address2",
                  "type": "string",
                  "required": false,
                  "validation": { "max_strlen": 40 },
                  "desc": "Customer billing address part 2"
              },
              {
                  "name": "city",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 25 },
                  "desc": "Customer billing city"
              },
              {
                  "name": "country",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 25 },
                  "desc": "Customer billing country"
              },
              {
                  "name": "state_or_region",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 40 },
                  "desc": "Customer billing state or region"
              },
              {
                  "name": "postal_code",
                  "type": "integer",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 20 },
                  "desc": "Customer billing postal code"
              },
              {
                  "name": "preferred_language",
                  "type": "string",
                  "required": false,
                  "validation": { "matches": [ "EN", "ES", "ZH" ] },
                  "desc": "Preferred language"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__ReplaceSIMCard":
      {
          "desc": "Replaces SIM card for another.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "old_ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 18, "max_strlen": 18, "MVNE": "2" },
                  "desc": "Old ICCID (18 digits)."
              },
              {
                  "name": "new_ICCID",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 19, "max_strlen": 19, "verify_luhn_substr": 18, "iccid": "type_purple" },
                  "desc": "New ICCID (19 digits)."
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__CheckSession":
      {
          "desc": "Checks session status of currently logged in customer.",
          "parameters":
          [
              "common-parameters.json"
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "zsession",              "type": "string",  "desc": "zsession ID" },
              { "name": "expire_timestamp",      "type": "integer", "desc": "unix timestamp of when zsession is set to expire" },
              { "name": "bypass_claim_account",  "type": "boolean", "desc": "If true, UI will bypass the Claim Account screen - used for DealerLoginAsCustomer" },
              { "name": "password_is_empty",     "type": "boolean", "desc": "True if customers password was cleared by portal__ForgotPassword" }
          ]
      },
      "portal__ExtendSession":
      {
          "desc": "Extend the lifespan of current zsession by 30 minutes.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__KillSession":
      {
          "desc": "Clear the current zsession.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__SetLoginName":
      {
          "desc": "Set Customer's Login.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "account_login",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 6 },
                  "desc": "Login Name"
              },
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__SetPassword":
      {
          "desc": "Set Customer's Password.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "account_password",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 6 },
                  "desc": "Password"
              },
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__ForgotUsername":
      {
          "desc": "Get a customer's email address and email them their username.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "email",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "Email address"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__ClearCreditCard":
      {
          "desc": "Remove customer credit card info.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__UpdateAutoRecharge":
      {
          "desc": "Update the monthly_cc_renewal column in htt_customers_overlay_ultra table.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "auto_recharge",
                  "type": "integer",
                  "required": true,
                  "validation": { "matches": [ "0", "1" ] },
                  "desc": "monthly_cc_renewal value"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__VerifyZSession":
      {
          "desc": "Checks the zsession",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "customer",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "customer locator"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__ActivateSuspendedCustomer":
      {
          "desc": "Try to activate a suspended customer.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__ChangePlanImmediate":
      {
          "desc": "Change the plan immediately for a customer.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "targetPlan",
                  "type": "string",
                  "required": true,
                  "validation": { "active_plan": 1 },
                  "desc": "Plan to select"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__ChangePlanFuture":
      {
          "desc": "Sets the plan to be changed in the future.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "targetPlan",
                  "type": "string",
                  "required": true,
                  "validation": { "active_plan": 1 },
                  "desc": "Plan to select"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },
      "portal__CreateWebUser":
      {
          "desc": "Creates a Customer, with a proper login name and password. Sets Neutral COS and customer info (should match old API call)",
          "parameters":
          [
              "common-parameters.json",
              "common-portal-createUser-parameters.json"
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "portal__CustomerDetail":
      {
          "desc": "Provides all the useful details on a customer based upon a customer zsession.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "mode",
                  "type": "string",
                  "required": false,
                  "validation": { "matches": [ "OCS_BALANCE" , "DATA_USAGE" ] },
                  "desc": "Segment out certain responses"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "current_msisdn",      "type": "string",  "desc": "Phone Number" },
              { "name": "first_name",          "type": "string",  "desc": "First Name" },
              { "name": "last_name",           "type": "string",  "desc": "Last Name" },
              { "name": "email",               "type": "string",  "desc": "Email Address" },
              { "name": "preferred_language",  "type": "string",  "desc": "Preferred Language" },
              { "name": "local_phone",         "type": "string",  "desc": "Local Phone Number" },
              { "name": "cos",                 "type": "string",  "desc": "Current plan" },
              { "name": "customer_plan_start", "type": "epoch",   "desc": "UNIX Epoch timestamp of the start of the customers plan." },
              { "name": "plan_expires",        "type": "epoch",   "desc": "UNIX Epoch timestamp of the end of the customers plan." },
              { "name": "monthly_renewal_target", "type": "string", "desc": "Monthly renewal target plan" },
              { "name": "stored_value",        "type": "integer", "desc": "Balance stored for paying plan renewals" },
              { "name": "balance",             "type": "integer", "desc": "Balance held for paying plan renewals and for international calling and data purchases." },
              { "name": "packaged_balance1",   "type": "integer", "desc": "Balance held" },
              { "name": "customer_status",     "type": "string",  "desc": "Current Customer Status" },
              { "name": "customer_loginname",  "type": "string",  "desc": "Login name" },
              { "name": "customer",            "type": "integer", "desc": "Customer unique session identifier" },
              { "name": "auto_recharge",       "type": "boolean", "desc": "For monthly plans, is auto recharge turned on?" },
              { "name": "address1",            "type": "string",  "desc": "Billing address 1" },
              { "name": "address2",            "type": "string",  "desc": "Billing address 2" },
              { "name": "city",                "type": "string",  "desc": "Billing address city" },
              { "name": "state_region",        "type": "string",  "desc": "Billing address state" },
              { "name": "postal_code",         "type": "string",  "desc": "Billing address postal code" },
              { "name": "country",             "type": "string",  "desc": "Billing address country" },
              { "name": "cc_name",             "type": "string",  "desc": "Name On Credit Card" },
              { "name": "cc_address1",         "type": "string",  "desc": "Credit Card - Owner's address 1" },
              { "name": "cc_address2",         "type": "string",  "desc": "Credit Card - Owner's address 2" },
              { "name": "cc_city",             "type": "string",  "desc": "Credit Card - Owner's address city" },
              { "name": "cc_state_region",     "type": "string",  "desc": "Credit Card - Owner's address state" },
              { "name": "cc_postal_code",      "type": "string",  "desc": "Credit Card - Owner's address postal code" },
              { "name": "cc_country",          "type": "string",  "desc": "Credit Card - Owner's address country" },
              { "name": "cc_exp_date",         "type": "string",  "desc": "Credit Card Expiration Date" },
              { "name": "cc_last_4",           "type": "integer", "desc": "Last 4 digits of Credit Card Number." },
              { "name": "iccid",               "type": "string",  "desc": "Customer's current ICCID" },
              { "name": "activate_request_id", "type": "string",  "desc": "request_id response value from the customer's previous activation." },
              { "name": "porting_request_id",       "type": "string",  "desc": "request_id response value from the customer's previous porting." },
              { "name": "porting_msisdn",           "type": "string",  "desc": "Phone number we tried to port for this customer." },
              { "name": "porting_account_number",   "type": "string",  "desc": "Porting data: account number." },
              { "name": "porting_account_password", "type": "string",  "desc": "Porting data: account password." },
              { "name": "porting_account_zipcode",  "type": "string",  "desc": "Porting data: account zipcode." },
              { "name": "last_transition_name",     "type": "string",  "desc": "Name of last transition, so UI knows if they orded a sim or not." },
              { "name": "voice_minutes",            "type": "integer", "desc": "If on the $19 plan, minutes remaining" },
              { "name": "transition_in_progress",   "type": "boolean", "desc": "Returns true if customer has an OPEN transition."},
              { "name": "originalsimproduct",       "type": "string",  "desc": "Orange or Purple: product of the activation_iccid." },
              { "name": "4g_lte_remaining",         "type": "string",  "desc": "4G LTE remaining data."                                       },
              { "name": "4g_lte_usage",             "type": "string",  "desc": "4G LTE data usage."                                           },
              { "name": "4g_lte_last_applied",      "type": "epoch",   "desc": "UNIX Epoch timestamp of the last time that data was applied." },
              { "name": "4g_lte_bolton_percent_used", "type": "integer", "desc": "Percentage of Mint Data Add On used."                       },
              { "name": "monthly_data_purchases",   "type": "integer", "desc": "data purchases during the current cycle"                      },
              { "name": "bolt_ons",                 "type": "string[]", "desc": "Array of objects"                                            },
              { "name": "brand",                    "type": "string",   "desc": "brand to which the customer ICCID belongs"                   }
          ]
      },

      "portal__GetMarketingSettings":
      {
          "desc": "Retrieves Marketing Settings for a logged in customer.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "marketing_settings",       "type": "string[]",  "desc": "Customer Marketing Settings." }
          ]
      },

      "portal__SetMarketingSettings":
      {
          "desc": "Overwrites Marketing Settings for a logged in customer.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "marketing_settings",
                  "type": "string[]",
                  "required": true,
                  "validation": { },
                  "desc": "List of settings to be updated"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "portal__GetTransactionHistory":
      {
          "desc": "Retrieves transaction history for a customer.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "start_epoch",
                  "type": "epoch",
                  "required": true,
                  "validation": { },
                  "desc": "Start Date (UNIX Epoch timestamp)"
              },
              {
                  "name": "end_epoch",
                  "type": "epoch",
                  "required": true,
                  "validation": { },
                  "desc": "End Date (UNIX Epoch timestamp)"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "transaction_history", "type": "string[]",  "desc": "Flattened array containing the customer transaction history." }
          ]
      },

      "portal__CustomerInfo":
      {
          "desc": "Provides all the useful details on a customer based upon a customer zsession.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "get_voice_minutes",
                  "type": "boolean",
                  "required": false,
                  "validation": { },
                  "desc": "If true, retrieves voice balance info from ACC."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "current_msisdn",         "type": "string",  "desc": "Phone Number" },
              { "name": "first_name",             "type": "string",  "desc": "First Name" },
              { "name": "last_name",              "type": "string",  "desc": "Last Name" },
              { "name": "email",                  "type": "string",  "desc": "Email Address" },
              { "name": "cos",                    "type": "string",  "desc": "Current plan" },
              { "name": "customer_plan_start",    "type": "epoch",   "desc": "UNIX Epoch timestamp of the start of the customers plan." },
              { "name": "plan_expires",           "type": "epoch",   "desc": "UNIX Epoch timestamp of the end of the customers plan." },
              { "name": "paid_through",           "type": "string",  "desc": "UNIX time of multi month expiration date" },
              { "name": "duration",               "type": "string",  "desc": "number of months in the multi month plan" },
              { "name": "monthly_renewal_target", "type": "string",  "desc": "Monthly renewal target plan" },
              { "name": "stored_value",           "type": "integer", "desc": "Balance stored for paying plan renewals" },
              { "name": "balance",                "type": "integer", "desc": "Balance held for paying plan renewals and for international calling and data purchases." },
              { "name": "packaged_balance1",      "type": "integer", "desc": "Balance held" },
              { "name": "zero_minutes",           "type": "integer", "desc": "How many minutes are remaining for Ultra Zero Plans." },
              { "name": "customer_status",        "type": "string",  "desc": "Current Customer Status" },
              { "name": "customer_loginname",     "type": "string",  "desc": "Login name" },
              { "name": "customer_id",            "type": "integer", "desc": "Customer unique DB identifier" },
              { "name": "customer",               "type": "integer", "desc": "Customer unique session identifier" },
              { "name": "originalsimproduct",     "type": "string",  "desc": "Orange or Purple: product of the activation_iccid." },
              { "name": "tos_accepted",           "type": "boolean", "desc": "Were Terms of Service accepted by the customer?" },
              { "name": "recharge_status",        "type": "boolean", "desc": "Recharge status" },
              { "name": "auto_recharge",          "type": "boolean", "desc": "For monthly plans, is auto recharge turned on?"   },
              { "name": "transition_in_progress", "type": "boolean", "desc": "Returns true if customer has an OPEN transition." },
              { "name": "bolt_ons",               "type": "string[]", "desc": "Array of objects"                                },
              { "name": "voice_minutes",          "type": "integer",  "desc": "remaining voice minutes" }
          ]
      },

      "portal__CalculateTaxesFees":
      {
          "desc": "Determine the Sales Tax and Regulatory fees that would be charged for a potential Credit Card purchase, provided a purchase amount.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "zsession",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Cookie zsession"
              },
              {
                  "name": "charge_amount",
                  "type": "integer",
                  "required": true,
                  "validation": { "min_value": 1 },
                  "desc": "The amount of the potential credit card purchase, represented in cents."
              },
              {
                  "name": "zip",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "If provided, calculate based on this value. Otherwise use the customer's billing zip code."
              },
              {
                  "name": "check_taxes",
                  "type": "boolean",
                  "required": false,
                  "default":  true,
                  "validation": { },
                  "desc": "is the purchase subject to sales tax?"
              },
              {
                  "name": "check_recovery_fee",
                  "type": "boolean",
                  "required": false,
                  "default":  true,
                  "validation": { },
                  "desc": "is the purchase subject to recovery fee?"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "sales_tax",               "type": "integer", "desc": "Sales tax in cents"    },
              { "name": "recovery_fee",            "type": "integer", "desc": "Recovery fee in cents" }
          ]
      },

      "portal__ResetPasswordEmail":
      {
          "desc": "Resets the user's password to a temporary memcache password and sends it by email to the customer. Temp password should be valid for 72 hours.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "login_string",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Either the MSISDN or the customer Login."
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "portal__ResetPasswordSMS":
      {
          "desc": "Generates a password for the customer_id associated with the MSISDN and SMS's it to the customer. This is a temp password like the LoginAs, but it should work on login.php.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "login_string",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Either the MSISDN or the customer Login."
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      }
  }
}
