<?php

use Ultra\Lib\Flex;

include_once('db.php');
include_once('db/webcc.php');
include_once('lib/util-common.php');
include_once('Ultra/Lib/DB/CCTransactions.php');
require_once 'classes/Flex.php';


// public domain from Simon Schick
// <CAJWj5+F8J_Cdnyi3T=2zhoaifa04Dm79M+7Mt8JuZ78SBgoffQ@mail.gmail.com>
function get_ip_address()
{
  $params = array(
    "HTTP_CLIENT_IP",
    "HTTP_X_FORWARDED_FOR",
    "HTTP_X_FORWARDED",
    "HTTP_FORWARDED_FOR",
    "HTTP_FORWARDED",
    "REMOTE_ADDR"
    );

  foreach($params as $param)
  {
    if ($val = filter_input(INPUT_SERVER, $param, FILTER_VALIDATE_IP))
      return $val;
  }

  return false;
}

function fraud_detect_customer_login()
{
  global $out;

  $type = 'customer_login';

  $data = null;

  // do not block in development
  if (\Ultra\UltraConfig\isDevelopmentDB())
    return NULL;

  // purposely distinct from function fraud_detect: we are not logged in and we have no customer data (yet)

  if ( ! isset($out['customer']) )
    $out['customer'] = 'not_set';

  $scope = fraud_fill_scope(null, $type, $data);

  if ( whitelisted_for_fraud( $scope['net'] ) )
    return NULL;

  // block failed login attempts by IP after 5 attempts. (ICLEAN-147)

  $checks = array(
    "max_failed_login_attempts_by_ip" => check_max_failed_login_attempts_by_ip_query($scope),
  );

  foreach ($checks as $reason => $q)
    if (mssql_has_rows($q))
      return $reason;

  return NULL;
}

function check_max_failed_login_attempts_by_ip_query($scope)
{
  $window = 4*60*60; // within the last 4 hours
  $window_exp = "DATEDIFF(ss, event_time, GETUTCDATE()) < $window";

  $check_max_failed_login_attempts_by_ip_query =
    sprintf(
      "
SELECT   scope_net , count(*) 
FROM     htt_auditlog 
WHERE    scope_net = %s 
AND      $window_exp 
GROUP BY scope_net 
HAVING   count(*) >= 5",
      mssql_escape($scope['net'])
    );

  return $check_max_failed_login_attempts_by_ip_query;
}

function fraud_detect_chargeup($customer, $data)
{
  // Track plan purchase attempts/failures/successes and block IPs after 3 failed credit cards or 2 successes. (ICLEAN-148)

  global $out;
  if ($out['homeboy']) return NULL;

  $type = 'chargeup';

  $scope = fraud_fill_scope($customer, $type, $data);

  $window = 6*60*60; // within the last 6 hours
  $window_exp = "DATEDIFF(ss, event_time, GETUTCDATE()) < $window";

  $checks = array(
             // max 3 credit cards failures in the last 6 hours
             "max_credit_cards_failures" => sprintf("
SELECT   scope_net , count(*) 
FROM     htt_auditlog 
WHERE    scope_net = %s
AND      event_result = '%s'
AND      $window_exp
GROUP BY scope_net 
HAVING   count(*) >= 3 ",
  mssql_escape($scope['net']),
  'rejected'),
             // max 2 credit cards successes in the last 6 hours
             "max_credit_cards_successes" => sprintf("
SELECT   scope_net , count(*) 
FROM     htt_auditlog 
WHERE    scope_net = %s
AND      event_result = '%s'
AND      $window_exp
GROUP BY scope_net 
HAVING   count(*) >= 2 ",
  mssql_escape($scope['net']),
  'ok')
  );

  foreach ($checks as $reason => $q)
  {
    if (mssql_has_rows($q))
      return $reason;
  }

  return NULL;
}

function fraud_detect($customer, $type, $data)
{
  // fraud_detect($customer_full, 'transferto_msisdn_info', $data);
  // fraud_detect($customer, 'transferto_topup', $data);

  global $out;
  if ($out['homeboy']) return NULL;

  $scope = fraud_fill_scope($customer, $type, $data);

  $bad = array('+917386265519',
               '+917893303487',
               '+918099940662',
               '+918142368167',
               '+918978297553',
               '+919052604794',
               '+919492688224',
               '+919492752383');

  if (in_array($scope['dest'], $bad))
  {
    return "blocked destination $scope[dest]";
  }

  $id = $scope['customer'];
  // 24 hour window = 24*3600 seconds
  $window = 24*60*60;
  $window_exp = "DATEDIFF(ss, event_time, GETUTCDATE()) < $window";

  $checks = array(
              /* Max send amount in 24 hours: 800 INR */
             "over max send" => "SELECT customer_id
FROM (SELECT customer_id, SUM(CAST(scope_dollar_cost AS FLOAT)) as agg
      FROM htt_auditlog
      WHERE customer_id = $id AND $window_exp
      GROUP BY customer_id) as au
WHERE agg > 16",

             /* Max number of transactions in 24 hours: 3 */
             "over max trans" => "SELECT customer_id
FROM (SELECT customer_id, COUNT(scope_dollar_cost) as agg
      FROM htt_auditlog
      WHERE customer_id = $id AND $window_exp
      GROUP BY customer_id) as au
WHERE agg > 3",

             /* Max times a destination can be sent to in 24 hours: 2 */
             "over max dest-trans" => sprintf("SELECT customer_id
FROM (SELECT customer_id, COUNT(scope_dest) as agg
      FROM htt_auditlog
      WHERE customer_id = $id AND
            scope_dest = %s AND
            $window_exp
      GROUP BY customer_id) as au
WHERE agg > 2", mssql_escape($scope['dest'])),

             /* Max 200 INR for accounts less than 7 days old */
             "young hearts" => sprintf("SELECT a.customer_id
FROM (SELECT customer_id, COUNT(scope_dest) as agg
      FROM htt_auditlog
      WHERE customer_id = $id AND
            $window_exp
      GROUP BY customer_id) as au, accounts a
WHERE au.customer_id = a.customer_id AND
      $scope[dollar_cost] >= 4 AND
      DATEDIFF(dd, a.creation_date_time, GETUTCDATE()) < 8",
                                            mssql_escape($scope['dest'])),
    );

  foreach ($checks as $reason => $q)
  {
    if (mssql_has_rows($q))
      return $reason;
  }

  return NULL;
}

function fraud_fill_scope($customer, $type, $data)
{
  global $out;

  $scope = array(
    'net' => get_ip_address()
  );

  switch ($type)
  {
    case 'customercare':
      $scope['site']     = get_htt_env();
      $out['request_id'] = create_guid('customercare');
      $scope['source']       = $data['source'];
      $scope['dest']         = $data['dest'];
      $scope['channel']      = $data['channel'];
      $scope['channel_type'] = $data['channel_type'];
      $scope['customer']     = $customer->CUSTOMER_ID;
      break;
    case 'inventory':
      $scope['site']     = get_htt_env();
      $out['request_id'] = create_guid('inventory');
      $scope['customer'] = 'NULL';
      $scope['source']       = $data['source'];
      $scope['dest']         = $data['dest'];
      $scope['channel']      = $data['channel'];
      $scope['channel_type'] = $data['channel_type'];
      break;
    case 'transferto_topup':
      $out['request_id'] = create_guid('transferto');
      $scope['site'] = get_htt_env();
      $scope['customer'] = $customer->CUSTOMER_ID;
      $scope['source'] = $data['msisdn'];
      $scope['dest'] = $data['destination_msisdn'];
      $scope['channel'] = $data['operatorid'];
      $scope['channel_type'] = $data['operatorid'];
      $scope['dollar_cost'] = $data['amount'];
      break;
    case 'transferto_msisdn_info':
      $out['request_id'] = create_guid('transferto');
      $scope['site'] = get_htt_env();
      $scope['customer'] = $customer->CUSTOMER_ID;
      $scope['source'] = 'unknown';
      $scope['dest'] = $data['destination_msisdn'];
      $scope['channel'] = isset($data['operatorid']) ? $data['operatorid'] : 0;
      $scope['channel_type'] = $data['operatorid'];
      $scope['dollar_cost'] = 0;
      break;
    case 'customer_login':
      $scope['site'] = get_htt_env();
      if ( ! empty($out['customer']) )
        $scope['dest'] = $out['customer'];
      $scope['customer'] = ( $customer && is_object($customer) ) ? $customer->CUSTOMER_ID : 0;
      $scope['source'] = 'customer_login';
      $scope['channel'] = 'customer_login';
      $scope['channel_type'] = 'customer_login';
      $out['request_id'] = create_guid('PHPAPI');
      break;
    case 'chargeup':
      $out['request_id'] = create_guid('chargeup');
      $scope['site'] = get_htt_env();
      $scope['customer'] = $customer->CUSTOMER_ID;
      $scope['source'] = $customer->CUSTOMER_ID;
      $scope['dest'] = $customer->CUSTOMER_ID;
      $scope['channel'] = '' ;
      $scope['channel_type'] = $data['channel_type'];
      $scope['dollar_cost'] = $out['amount'];
      break;
    case 'sms':
      $out['request_id'] = create_guid('sms');
      $scope['site'] = get_htt_env();
      $scope['customer'] = $customer->CUSTOMER_ID;
      $scope['source'] = $customer->CUSTOMER_ID;
      $scope['dest'] = $data['sms_dest'];
      $scope['dollar_cost'] = 0.007; // EUR 0.006
      break;
    case 'externalpayments':
      $out['request_id'] = create_guid('externalpayments');
      $scope['site']     = get_htt_env();
      $scope['customer'] = ( $customer && is_object($customer) ) ? $customer->CUSTOMER_ID : '0' ;
      $scope['source']   = "{$data['provider']}|{$data['store_id']}|{$data['clerk_id']}|{$data['terminal_id']}";
      $scope['dest']     = ( $customer && is_object($customer) ) ? $customer->CUSTOMER_ID : '0' ;
      $scope['channel'] = '' ;
      $scope['dollar_cost'] = $data['load_amount'];
      $scope['channel_type'] = $data['subproduct_id'];
      break;
    case 'provisioning':
      $out['request_id']     = $data['transition_uuid'];
      $scope['site']         = get_htt_env();
      $scope['customer']     = ( $customer && is_object($customer) ) ? $customer->CUSTOMER_ID : '0' ;
      $scope['source']       = $data['activation_userid'];
      $scope['dest']         = ( $customer && is_object($customer) ) ? $customer->CUSTOMER_ID : '' ;
      $scope['dollar_cost']  = $data['amount'];
      $scope['channel'] = '' ;
      $scope['channel_type'] = $data['cos_id'];
      break;
    case 'messaging':
      $out['request_id'] = create_guid('messaging');
      $scope['site'] = get_htt_env();
      $scope['customer'] = $customer->CUSTOMER_ID;
      $scope['source'] = $data['source'];
      $scope['dest']   = $data['destination'];
      $scope['dollar_cost'] = 0;
      $scope['channel'] = '' ;
      $scope['channel_type'] = $data['cos_id'];
      break;
    case 'dealerportal':
      $scope['site'] = get_htt_env();
      $out['request_id'] = create_guid('dealerportal');
      $scope['source'] = $data['source'];
      $scope['dest'] = ($customer && is_object($customer)) ? $customer->CUSTOMER_ID : 0;
      $scope['channel'] = '';
      $scope['channel_type'] = 'dealerportal';
      $scope['customer'] = ($customer && is_object($customer)) ? $customer->CUSTOMER_ID : 0;
      break;
    case 'portal':
      $scope['site'] = get_htt_env();
      $out['request_id'] = create_guid('portal');
      $scope['source'] = $data['source'];
      $scope['dest'] = ($customer && is_object($customer)) ? $customer->CUSTOMER_ID : 0;
      $scope['channel'] = '';
      $scope['channel_type'] = 'portal';
      $scope['customer'] = ($customer && is_object($customer)) ? $customer->CUSTOMER_ID : 0;
      break;
    case 'interactivecare':
      $scope['site'] = get_htt_env();
      $out['request_id'] = create_guid('interactivecare');
      $scope['source'] = $data['source'];
      $scope['dest'] = ($customer && is_object($customer)) ? $customer->CUSTOMER_ID : 0;
      $scope['channel'] = '';
      $scope['channel_type'] = 'interactivecare';
      $scope['customer'] = ($customer && is_object($customer)) ? $customer->CUSTOMER_ID : 0;
      break;
    default:
      return array('error' => "Unknown a-f detection type $type");
  }

  if ((!isset($scope['dollar_cost'])) || (!$scope['dollar_cost']))
  {
    $scope['dollar_cost'] = 0;
  }

  return $scope;
}

function fraud_event($customer, $type, $event, $status, $data)
{
  global $out;

  // record an event into htt_auditlog for fraud detection purposes

  // invocation examples:
  // fraud_event($customer, 'transferto_topup', 'charge', ['error'|'rejected'|'ok'],    $data);
  // fraud_event($customer, 'customer_login',   'login',  'ok',       $data); // login success
  // fraud_event(null,      'customer_login',   'login',  'failed',   $data); // login failure
  // fraud_event($customer, 'chargeup',         $type,    'error',    $data);
  // fraud_event($customer, 'chargeup',         $type,    'ok',       $data);
  // fraud_event($customer, 'chargeup',         $type,    'block',    $data);
  // fraud_event($customer, 'sms',              'send',   'block',    $data);
  // fraud_event($customer, 'externalpayments', 'SubmitRealtimeReload', 'error',  $data); // where $data contains 'load_amount','subproduct_id','store_id','clerk_id','terminal_id'
  // fraud_event($customer, 'externalpayments', 'SubmitRealtimeReload', 'success, $data); // where $data contains 'load_amount','subproduct_id','store_id','clerk_id','terminal_id'
  // fraud_event($customer, 'externalpayments', 'CancelRealtimeReload', ['error'|'success'],  $data);
  // fraud_event($customer, 'externalpayments', 'CheckMobileBalance',   ['error'|'success'],  $data);
  // fraud_event($customer, 'inventory',        'ShipSIMMaster',        ['warning'|'error'|'success'],$data);
  // fraud_event($customer, 'inventory',        'StockSIMCelluphone',   ['warning'|'error'|'success'],$data);
  // fraud_event($customer, 'messaging',        'funcSendExemptCustomerSMS, ['error'|'success'], $data);
  // fraud_event($customer, 'messaging',        'funcSendExemptCustomerEmail, ['error'|'success'], $data);
  // fraud_event($customer, 'provisioning',     'requestProvisionNewCustomerAsync', ['error'|'success'], $data);
  // fraud_event($customer, 'provisioning',     'requestProvisionPortedCustomerAsync', ['error'|'success'], $data);
  // fraud_event($customer, 'provisioning',     'requestActivateShippedNewCustomerAsync', ['error'|'success'], $data);

  if ( whitelisted_for_fraud( get_htt_env() ) )
  { return TRUE; }

  $scope = fraud_fill_scope($customer, $type, $data);

  if ( empty($scope['dest']) )
    $scope['dest'] = '';

  $q = sprintf("INSERT INTO htt_auditlog (
 [EVENT_UUID], [EVENT_MODULE], [EVENT_NAME], [EVENT_RESULT],
 [SITE_ID], [CUSTOMER_ID], [SCOPE_NET],
 [SCOPE_SOURCE], [SCOPE_DEST],
 [SCOPE_CHANNEL], [SCOPE_DOLLAR_COST], [SCOPE_CHANNEL_TYPE])
VALUES ('%s', '%s', '%s', '%s',
        '%s', %s, '%s', '%s', '%s',
        '%s', %s, '%s')",
               $out['request_id'], $type, substr($event, 0, 20), $status,
               substr($scope['site'], 0, 10), $scope['customer'], $scope['net'],
               substr($scope['source'], 0, 32), $scope['dest'],
               $scope['channel'], $scope['dollar_cost'], $scope['channel_type']);

  return is_mssql_successful(logged_mssql_query($q));
}

/**
 * whitelisted_for_fraud
 *
 * check given IP against white list
 * @param string IP address
 * @return boolean TRUE when found, FALSE othersise
 * @todo: implement this in cfengine
 */
function whitelisted_for_fraud( $ip )
{
  // to enter a subnet simply omit the last digits that change
  $ip_whitelist = array(
    '69.86.226.45',     // ?
    '70.39.130',        // coresite-old
    '73.205.112',       // ?
    '107.194.26.122',   // ?
    '162.222.64',       // 
    '172.16.200',       // coresite-old-vpn
    '172.16.205',       // coresite-new-vpn
    '184.152.77.57',    // ?
    '192.168.100',      // CM office
    '192.168.200',      // CM Office VPN
    '203.177.98.203',   // Shore
    '208.105.5.30',     // NYC office
  );

  // compare only the smaller part so that subnets can be used
  foreach($ip_whitelist as $address)
    if (substr($ip, 0, strlen($address)) == $address)
      return TRUE;
  
  // not found
  return FALSE;
    
}


/**
 * log_cc_transaction_fraud_event
 *
 * Input:
 *  - 'customer'
 *  - 'session'
 *  - 'charge_amount'
 *  - 'store_zipcode'          ( not required )
 *  - 'store_id'               ( not required )
 *  - 'clerk_id'               ( not required )
 *  - 'terminal_id'            ( not required )
 *  - 'target_cos_id'          ( not required )
 *
 * @return NULL
 */
function log_cc_transaction_fraud_event( $params , $fraud_code )
{
  $cos_id = $params['customer']->COS_ID;
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) // COS_ID associated with the reason of the transaction
    $cos_id = $params['target_cos_id'];

  $history_params = array(
    'entry_type'                   => 'LOAD',
    'stored_value_change'          => '0',
    'balance_change'               => '0',
    'package_balance_change'       => '0',
    'reference'                    => $params['session'],
    'reference_source'             => get_reference_source('WEBCC'),
    'description'                  => $params['reason'],
    'detail'                       => $params['detail'],
    'charge_amount'                => $params['charge_amount'],
    'commissionable_charge_amount' => 0,
    'surcharge_amount'             => 0,
    'customer_id'                  => $params['customer']->CUSTOMER_ID,
    'cos_id'                       => $cos_id,
    'date'                         => 'now',
    'result'                       => 'FRAUD_'.$fraud_code,
    'source'                       => 'WEBCC',
    'is_commissionable'            => 0
  );

  foreach ( array('store_zipcode','store_id','clerk_id','terminal_id') as $nullable_field )
    if ( isset( $params[$nullable_field] ) && $params[$nullable_field] != '' )
      $history_params[$nullable_field] = $params[$nullable_field];

  $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

  if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
    dlog('','DB error while adding billing history');

  return NULL;
}

/**
 * cc_transactions_fraud_check
 *
 * Fraud filter for CC transactions
 *
 * @return array
 */
function cc_transactions_fraud_check( $customer_id, $cos_id )
{
  $fraud_error = FALSE;
  $fraud_code  = FALSE;

  try
  {
    // TODO: cfengine
    if (Flex::isFlexPlan($cos_id)) {
      $F01Limit = 220;
      $F02Limit = 450;
      $F04Limit = 1;
      $fraudCodePrefix = 'FLEX_';

      // get number of family members for F03 limit
      $familyInfo = Flex::getInfoByCustomerId($customer_id);
      if (!empty($familyInfo)) {
        $F03Limit = $familyInfo['memberCount'] > 1 ? 5 : 2;
      } else {
        $F03Limit = 2;
      }
    } else if (get_brand_id_from_cos_id($cos_id) == 3) {
      $F01Limit = 500;
      $F02Limit = 600;
      $F03Limit = 2;
      $F04Limit = 1;
      $fraudCodePrefix = 'MINT_';
    } elseif (\Ultra\UltraConfig\isBPlan($cos_id) && get_plan_cost_from_cos_id($cos_id) >= 60) {
      $F01Limit = 200;
      $F02Limit = 350;
      $F03Limit = 2;
      $F04Limit = 1;
      $fraudCodePrefix = '';
    } else {
      $F01Limit = 75;
      $F02Limit = 149;
      $F03Limit = 2;
      $F04Limit = 1;
      $fraudCodePrefix = '';
    }

    // F01 - more than X in the past 24 hours
    $amount = \get_ultra_cc_transaction_sum( $customer_id , 1 );
    if ( $amount > $F01Limit )
    {
      dlog('',"FRAUD ALERT : cc transactions amount by period = 1 day ; amount = $amount ; CUSTOMER_ID = ".$customer_id);
      $fraud_code = 'F01';
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. ".$fraud_code);
    }

    // F02 - more than X in the past 30 days
    $amount = \get_ultra_cc_transaction_sum( $customer_id , 30 );
    if ( $amount > $F02Limit )
    {
      dlog('',"FRAUD ALERT : cc transactions amount by period = 30 days ; amount = $amount ; CUSTOMER_ID = ".$customer_id);
      $fraud_code = 'F02';
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. ".$fraud_code);
    }

    // F03 - approved charges on X distinct credit cards in the past 30 days
    $distinct_cc_holder_tokens_id = get_distinct_cc_holder_tokens_id( $customer_id , 30 );
    if ( count($distinct_cc_holder_tokens_id) > $F03Limit )
    {
      dlog('',"FRAUD ALERT : approved charges by period = 30 days ; ".count($distinct_cc_holder_tokens_id)." distinct credit cards ; CUSTOMER_ID = ".$customer_id);
      $fraud_code = 'F03';
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. ".$fraud_code);
    }

    // F04 - X REJECTED CC attempts in the past 24 hours, across different cards
    $distinct_cc_holder_tokens_id = get_distinct_cc_holder_tokens_id( $customer_id , 1 , 'COMPLETE' , 'FAILED' );
    if ( count($distinct_cc_holder_tokens_id) > $F04Limit )
    {
      dlog('',"FRAUD ALERT : rejected charges by period = 1 day ; ".count($distinct_cc_holder_tokens_id)." REJECTED credit card attempts in the past 24 hours ; CUSTOMER_ID = ".$customer_id);
      $fraud_code = 'F04';
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. ".$fraud_code);
    }
  }
  catch(Exception $e)
  {
    $fraud_error = $e->getMessage();

    if (!empty($fraud_code)) {
      $fraud_code = $fraudCodePrefix . $fraud_code;
    }
  }

  return array( $fraud_error , $fraud_code );
}

function webcc_fraud_check( $customer )
{
  $fraud_error = FALSE;

return $fraud_error; #DEACTIVATED

/*
  try
  {
    // more than $60 in the past 24 hours
    $amount = webcc_amount_by_period( $customer->CUSTOMER_ID , 1 );

    if ( $amount > 75 )
    {
      dlog('',"FRAUD ALERT : webcc_amount_by_period - period = 1 day ; amount = $amount ; CUSTOMER_ID = ".$customer->CUSTOMER_ID);
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. F01");
    }

    // more than $75 in the past 30 days
    $amount = webcc_amount_by_period( $customer->CUSTOMER_ID , 30 );

    if ( $amount > 149 )
    {
      dlog('',"FRAUD ALERT : webcc_amount_by_period - period = 30 days ; amount = $amount ; CUSTOMER_ID = ".$customer->CUSTOMER_ID);
      // Kelvin is concerned about legit customers.
      //throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. F02");
    }

    // 3 distinct credit cards in the past 30 days will FAIL after the first time. A bit weird.
    $distinct_cc_count = webcc_distinct_cc_count_by_period( $customer->CUSTOMER_ID , 30 );

    if ( $distinct_cc_count > 2 )
    {
      dlog('',"FRAUD ALERT : webcc_distinct_cc_count_by_period - period = 30 days ; $distinct_cc_count distinct credit cards ; CUSTOMER_ID = ".$customer->CUSTOMER_ID);
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. F03");
    }

    // 3 REJECTED credit card attempts in the past 24 hours on different cards will FAIL
    $rejected_cc_count = webcc_cc_count_by_period( $customer->CUSTOMER_ID , 1 , 'REJECTED' );

    if ( $rejected_cc_count > 2 )
    {
      dlog('',"FRAUD ALERT : webcc_cc_count_by_period - $rejected_cc_count REJECTED credit card attempts in the past 24 hours ; CUSTOMER_ID = ".$customer->CUSTOMER_ID);
      throw new Exception("ERR_API_FRAUD : Your current charge has been blocked due to suspicious activity. F04");
    }
  }
  catch(Exception $e)
  {
    $fraud_error = $e->getMessage();
  }

  return $fraud_error;
*/
}

