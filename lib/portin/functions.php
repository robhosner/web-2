<?php


require_once 'db/htt_portin.php';
require_once 'lib/portal/functions.php';


function log_port_request($customer_id, $msisdn, $ocn, $result, $carrier_name=NULL, $request_date='getutcdate()', $activation_masteragent=NULL, $activation_distributor=NULL, $activation_agent=NULL, $activation_store=NULL, $activation_userid=NULL)
{
  # to be used whenever a port is requested
  # logs the customer_id requesting the port; the number, the carrier details (if carrier details fails, just places null)

  $funresult = array( 'success' => FALSE , 'errors' => array() );

  $insert_params = array(
    "customer_id"  => $customer_id,
    "msisdn"       => $msisdn,
    "ocn"          => $ocn,
    "result"       => $result,
    "request_date" => 'getutcdate()',
    "activation_masteragent" => $activation_masteragent,
    "activation_distributor" => $activation_distributor,
    "activation_agent"       => $activation_agent,
    "activation_store"       => $activation_store,
    "activation_user_id"     => $activation_userid
  );

  if ( isset($carrier_name) && $carrier_name )
    $insert_params["carrier_name"] = $carrier_name;

  $htt_portin_log_insert_query = htt_portin_log_insert_query($insert_params);

  if ( is_mssql_successful(logged_mssql_query($htt_portin_log_insert_query)) )
    $funresult['success'] = TRUE;
  else
    $funresult['errors'][] = "DB error";

  return $funresult;
}

function log_port_request_from_number($numberToPort, $customer, $activation_masteragent=NULL, $activation_distributor=NULL, $activation_agent=NULL, $activation_store=NULL, $activation_userid=NULL)
{
  $numberToLookup = $numberToPort;

  if ( strlen($numberToLookup) == 10 )
    $numberToLookup = '1' . $numberToLookup;

  $carrier_data = funcCarrierLookup(array( 'msisdn_list' => $numberToLookup )); # $numberToPort with the leading 1

  $ocn          = '';
  $carrier_name = '';

  if ( $carrier_data && ( ! count($carrier_data['errors']) ) && isset($carrier_data['portability']) && is_array($carrier_data['portability']) )
  {
    $ocn          = $carrier_data['portability'][0][$numberToLookup][0];
    $carrier_name = $carrier_data['portability'][0][$numberToLookup][1];
  }

  $result_status =
    log_port_request(
      $customer->CUSTOMER_ID, $numberToPort, $ocn, '', $carrier_name, 'getutcdate()',
      $activation_masteragent, $activation_distributor, $activation_agent, $activation_store, $activation_userid
    );

  return $result_status;
}


/**
 * validatePortability
 *
 * validate number portability against existing Ultra subscribers
 * @param String MSISDN to port in
 * @param String taget plan name
 * @param String account number
 * @return Array(Boolean: is intra-brand port in, String error code, String specific error message)
 */
function validatePortability($msisdn, $targetPlan, $account)
{
  if ( ! get_cos_id_from_plan($targetPlan))
    return array(FALSE, 'MP0013', "unknown plan $targetPlan");

  if ($customer = get_customer_from_msisdn($msisdn, 'u.CUSTOMER_ID, u.PLAN_STATE, a.COS_ID, a.ACCOUNT'))
  {
    logInfo("found subscriber for MSISDN $msisdn: " . json_encode($customer));

    if ($customer->PLAN_STATE == STATE_PORT_IN_REQUESTED)
      return array(FALSE, 'PO0005', "$msisdn is being ported");

    if ($customer->PLAN_STATE == STATE_PROVISIONED)
      return array(FALSE, 'PO0006', "$msisdn is provisioned and awaiting payment");

    if ($customer->PLAN_STATE == STATE_PORT_IN_DENIED || $customer->PLAN_STATE == STATE_CANCELLED)
    {
      // previously failed attempt: clear HTT_CUSTOMERS_OVERLAY_ULTRA.CURRENT_MOBILE_NUMBER and allow port
      $sql = htt_customers_overlay_ultra_update_query(array('customer_id' => $customer->CUSTOMER_ID, 'current_mobile_number' => 'NULL'));
      if ( ! run_sql_and_check($sql))
        return array(FALSE, 'DB0001', 'failed to remove existing current mobile number');
      return array(FALSE, NULL, NULL);
    }

    // only Active and Suspended are allowed to port
    if ($customer->PLAN_STATE != STATE_ACTIVE && $customer->PLAN_STATE != STATE_SUSPENDED)
      return array(TRUE, 'PO0010', 'port denied due to customer state');

    // customer exists in some other state: this is an intra-brand port
    if ( ! $currentPlan = get_plan_name_from_short_name(get_plan_from_cos_id($customer->COS_ID)))
      return array(TRUE, 'BI0001', "unable to identify customer current plan ($customer->COS_ID)");
    if ($targetPlan == $currentPlan)
      return array(TRUE, 'PO0008', "customer is already on plan $targetPlan");

    $currentBrand = Ultra\UltraConfig\getUltraPlanConfigurationItem($currentPlan, 'brand_id');
    $targetBrand = Ultra\UltraConfig\getUltraPlanConfigurationItem($targetPlan, 'brand_id');
    if ($currentBrand == $targetBrand)
      return array(TRUE, 'PO0008', "customer on plan $currentPlan already exists, change plan to $targetPlan instead");

    if ($customer->ACCOUNT != $account)
      return array(TRUE, 'PO0009', "invalid account");

    // allow intra-brand port
    return array(TRUE, NULL, NULL);
  }
  else // allow MVNE port in
    return array(FALSE, NULL, NULL);
}

/**
 * provisionInterBrandCustomer
 *
 * @param String MSISDN
 * @param String plan name
 * @param Object SIM
 * return Array (Object customer, String error code, String error message)
 */
function provisionInterBrandCustomer($msisdn, $plan, $sim)
{
  $customer = get_customer_from_msisdn($msisdn, 'u.CUSTOMER_ID,u.BRAND_ID,plan_state');
  if ( ! $customer)
    return array(NULL, 'VV0031', "cannot find existing customer for MSISDN $msisdn");

  $result = \Provisioning::intraBrandPortOperations($customer->CUSTOMER_ID, $sim);

  if ($errors = $result->get_errors())
  {
    clear_redis_provisioning_values($customer->CUSTOMER_ID);
    list ($message, $code) = explode('|', $errors[0]);
    return array(NULL, $code, $message);
  }

  return array($result->get_data_key('customer'), NULL, NULL);

  /*
  $customer = $result->get_data_key('customer');
  $changeStateResult = $result->get_data_key('result_status');
        $context['customer_id'] = $customer->CUSTOMER_ID;

        set_redis_provisioning_values(
          $customer->CUSTOMER_ID,
          $activation_masteragent,
          $activation_agent,
          $activation_distributor,
          $activation_store,
          $activation_user_id
        );

        $this->updateBoltOns($customer->CUSTOMER_ID, $configureBoltonsResult->get_data_key('bolt_on_configuration'));

        // perform transition from neutral to active for new customer id
        // makeitso
  */
}