<?php

include_once('db.php');
include_once('db/htt_action_log.php');
include_once('db/htt_cancellation_reasons.php');
include_once('db/htt_customers_notes.php');
include_once('db/htt_transition_log.php');
include_once('db/ultra_customer_options.php');
include_once('lib/internal/functions.php');
include_once('lib/messaging/functions.php');
include_once('lib/transitions.php');
include_once('lib/util-common.php');
include_once('classes/postageapp.inc.php');
include_once('Ultra/Lib/Util/Settings.php');
include_once('Ultra/Lib/Util/Redis/functions.php');
require_once 'classes/Flex.php';

/**
 * Promotional SIM initialization
 *
 * - create a new Ultra customer using some default values
 * - associate this customer with the promo ID in ULTRA.CUSTOMER_OPTIONS
 * - transition from 'Neutral' to 'Promo Unused'
 * - do NOT wait for the transition to complete, simply create the transition and move on
 * - skips Activate/Can
 *
 * returns a Result object.
 */
function internal_func_promotional_sim_init( $params )
{
  dlog('',"params = %s",$params);

  $result = new Result();

  try
  {
    $plan = get_plan_name_from_short_name( get_plan_from_cos_id( $params['ultra_promotional_data']->TARGET_PLAN ) );

    if ( ! $plan )
      throw new Exception("ERR_API_INTERNAL: could not extract plan from TARGET_PLAN ".$params['ultra_promotional_data']->TARGET_PLAN);

    $iccid_19 = $params['iccid'];
    $iccid_18 = $params['iccid'];

    if (strlen($params['iccid']) == 18)
    { $iccid_19 = luhnenize($iccid_18);   }
    else
    { $iccid_18 = substr($iccid_19,0,-1); }

    $activation_masteragent = $params['ultra_promotional_data']->ACTIVATION_MASTERAGENT;
    $activation_store       = $params['ultra_promotional_data']->ACTIVATION_DEALER;
    $activation_userid      = $params['ultra_promotional_data']->ACTIVATION_USERID;

    $customer_source = get_customer_source( $activation_masteragent , $activation_store , $activation_userid );

    $customer_params = array(
      "masteragent"           => $activation_masteragent,
      "userid"                => $activation_userid,
      "dealer"                => $activation_store,
      "distributor"           => $params['ultra_promotional_data']->ACTIVATION_DISTRIBUTOR,
      "preferred_language"    => ( isset($params['preferred_language']) ) ? $params['preferred_language'] : 'EN' ,
      'cos_id'                => get_cos_id_from_plan('STANDBY'),
      'first_name'            => 'Promotional',
      'last_name'             => 'Customer',
      'postal_code'           => $params['postal_code'],
      'country'               => 'USA',
      'e_mail'                => 'null@null',
      "plan_state"            => 'Neutral',
      "plan_started"          => 'NULL',
      "plan_expires"          => 'NULL',
      "customer_source"       => $customer_source,
      "current_iccid"         => $iccid_18,
      "current_iccid_full"    => $iccid_19
    );

    // run DB queries which will create a new Ultra customer
    $return = create_ultra_customer_db_transaction($customer_params);

    $customer = $return['customer'];

    if ( ! $customer )
      throw new Exception("ERR_API_INTERNAL: DB error, could create new customer");

    // associate this customer with the promo ID in ULTRA.CUSTOMER_OPTIONS
    if ( ! ultra_customer_options_insert( 'PROMO_ASSIGNED' , $params['ultra_promotional_data']->ULTRA_PROMOTIONAL_PLANS_ID , $customer->CUSTOMER_ID ) )
      throw new Exception("ERR_API_INTERNAL: DB error, could create new ULTRA.CUSTOMER_OPTIONS row");

    log_funding_in_activation_history( $customer->CUSTOMER_ID , 'PREPAID' , substr( get_plan_from_cos_id( $params['ultra_promotional_data']->TARGET_PLAN ) , -2 ) );

    $context         = array( 'customer_id' => $customer->CUSTOMER_ID );
    $dry_run         = FALSE;
    $resolve_now     = FALSE;
    $max_path_depth  = 1;
    $transition_name = 'Promo Prepare '.$plan;

    // asynch transition from 'Neutral' to 'Promo Unused'
    $result_change_state = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    if ( ! $result_change_state['success'] )
      throw new Exception("ERR_API_INTERNAL: state machine error");

    // Capture the Activation Attribution information
    $transition_uuid = $result_change_state['transitions'][0];

    $redis = new \Ultra\Lib\Util\Redis;

    $redis_provisioning_key = "provisioning/".$transition_uuid.'/';

    $redis->set($redis_provisioning_key.'activation_masteragent', $params['ultra_promotional_data']->ACTIVATION_MASTERAGENT, 60*60*24*14);
    $redis->set($redis_provisioning_key.'activation_distributor', $params['ultra_promotional_data']->ACTIVATION_DISTRIBUTOR, 60*60*24*14);
    $redis->set($redis_provisioning_key.'activation_userid',      $params['ultra_promotional_data']->ACTIVATION_USERID,      60*60*24*14);
    $redis->set($redis_provisioning_key.'activation_store',       $params['ultra_promotional_data']->ACTIVATION_DEALER,      60*60*24*14);
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $result->add_error( $e->getMessage() );
  }

  return $result;
}


/**
 * internal_func_delayed_provisioning_or_activation
 *
 * Adds a new transtition which contain the action 'internal_func_delayed_ensure_provisioning_or_activation'
 */
function internal_func_delayed_provisioning_or_activation( $customer_id )
{
  $result = internal_func_delayed_action( $customer_id , 'internal_func_delayed_ensure_provisioning_or_activation' );
  if ( ! empty($result['transitions'][0]))
    unreserve_transition_uuid_by_pid($result['transitions'][0]);

  return $result;

}

/**
 * internal_func_delayed_provisioning_suspend
 *
 * Adds a new transtition which contain the action 'internal_func_delayed_ensure_suspend'
 */
function internal_func_delayed_provisioning_suspend( $customer_id )
{
  $result = internal_func_delayed_action( $customer_id , 'internal_func_delayed_ensure_suspend' );
  if ( ! empty($result['transitions'][0]))
    unreserve_transition_uuid_by_pid($result['transitions'][0]);

  return $result;
}

/**
 * internal_func_delayed_action
 */
function internal_func_delayed_action( $customer_id , $action_name )
{
  dlog('',"customer_id = $customer_id ; action_name = $action_name");

  $context = array(
    'customer_id' => $customer_id
  );

  return append_action($context,
                       array('type' => 'funcall',
                             'name' => $action_name,
                             'transaction' => NULL,
                             'seq'  => NULL),
                       array( $customer_id ),
                       FALSE);
}


/**
 * internal_func_delayed_ensure_provisioning_or_activation
 *
 * @return array
 */
function internal_func_delayed_ensure_provisioning_or_activation( $customer_id )
{
  dlog('',"customer_id = $customer_id");

  $success = FALSE;
  $errors  = array();

  try
  {
    // load customer data from DB
    $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number,plan_state'));
    $action_uuid = getNewActionUUID(time());

    if ( ! $customer )
      throw new Exception("ERR_API_INTERNAL: could not find customer (id = $customer_id)");

    if ( ! $customer->current_mobile_number )
      throw new Exception("ERR_API_INTERNAL: customer id $customer_id has not MSISDN");

    // verify customer is active on the network

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwQuerySubscriber(
      array(
        'actionUUID' => $action_uuid,
        'msisdn'     => $customer->current_mobile_number
      )
    );

    if ( !$result->is_success() || !isset($result->data_array['body']) || !is_object($result->data_array['body']) )
      throw new Exception("ERR_API_INTERNAL: QuerySubscriber failed");

    if ( ! property_exists( $result->data_array['body'] , 'SubscriberStatus' )
      || ( $result->data_array['body']->SubscriberStatus != 'Active' )
    )
      throw new Exception("ERR_API_INTERNAL: Subscriber is not Active");

    // set plan state to 'Port-In Requested' if 'Port-In Denied'

    if ( $customer->plan_state == 'Port-In Denied' )
    {
      $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
        array(
          'customer_id'  => $customer_id,
          'plan_state'   => 'Port-In Requested'
        )
      );

      if ( ! run_sql_and_check($htt_customers_overlay_ultra_update_query) )
        throw new Exception("ERR_API_INTERNAL: DB error - 01");

      $customer->plan_state = 'Port-In Requested';

      sleep(1);
    }

    // transition the customer to Active or Provisioned

    $context = array(
      'customer_id' => $customer_id
    );

    // [ Port-In Requested ] => [ Active ]

    $result_status = change_state($context, FALSE, 'Port Activated', 'take transition', FALSE, 1);

    if ( ! $result_status['success'] )
    {
      dlog('',"'Port-In Requested' => 'Active' failed");

      // [ Port-In Requested ] => [ Provisioned ]

      $result_status = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

      if ( ! $result_status['success'] )
        dlog('',"'Port-In Requested' => 'Provisioned' failed");
    }

    $success  = TRUE;
  }
  catch( Exception $e )
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return array( 'success' => $success , 'errors' => $errors );
}

// Delayed ensure_suspend for provisioning and promo:
// - NOOP is state is not "Provisioned" nor "Promo Unused";
// - NOOP is there is an "Activate Provisioned" transition in the DB;
// - NOOP is there is an "Activate Promo" transition in the DB;
// - returns error if customer has no ICCID, no MSISND or invalid MSISDN.
// - invokes mwMakeitsoSuspend
function internal_func_delayed_ensure_suspend( $customer_id )
{
  dlog('',"customer_id = $customer_id");

  $success = FALSE;
  $errors  = array();

  try
  {
    # load customer data from DB
    $customer = get_customer_from_customer_id($customer_id);

    if ( ! $customer )
      throw new Exception("ERR_API_INTERNAL: could not find customer (id = $customer_id)");

    if ( ! $customer->current_mobile_number )
      throw new Exception("ERR_API_INTERNAL: customer id $customer_id has not MSISDN");

    if ( strlen( $customer->current_mobile_number ) != 10 )
      throw new Exception("ERR_API_INTERNAL: MSISDN length is not 10 : ".$customer->current_mobile_number);

    if ( ! $customer->current_iccid )
      throw new Exception("ERR_API_INTERNAL: customer id $customer_id has not ICCID");

    # get customer state
    $state = internal_func_get_state_from_customer_id($customer_id);

    if ( ! $state )
      throw new Exception('ERR_API_INTERNAL: customer state could not be determined.');

    if ( ( $state['state'] == "Provisioned" ) || ($state['state'] == "Promo Unused" ) )
    {
      dlog('',"Customer state is ".$state['state']);

      # get customer transitions
      $result = get_all_customer_state_transitions( $customer_id );

      if ( count($result['errors']) )
        throw new Exception( $result['errors'][0] );

      $activating = FALSE;

      # check if there is a 'Activate Provisioned' transition or a 'Activate Promo' transition
      foreach( $result['state_transitions'] as $state_transition )
      {
        if ( ( preg_match("/Activate Provisioned /",$state_transition->TRANSITION_LABEL) )
             ||
             ( preg_match("/Activate Promo /",$state_transition->TRANSITION_LABEL) ) )
          $activating = TRUE;
      }

      if ( $activating )
        dlog('','Customer is being activated ; nothing to do');
      else
      {
          dlog('','invoking mwMakeitsoSuspend');

          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $customer = get_customer_from_customer_id($customer_id);

          $result = $mwControl->mwMakeitsoSuspend(
            array(
              'actionUUID'         => getNewActionUUID('mvne ' . time()),
              'customer_id'        => $customer_id,
              'msisdn'             => normalize_msisdn($customer->current_mobile_number, FALSE), // 10 digits
              'iccid'              => $customer->CURRENT_ICCID_FULL,
              'preferred_language' => $customer->preferred_language,
              'ultra_plan'         => get_plan_from_cos_id($customer->cos_id),
              'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer_id),
              'plan_tracker_id'    => NULL // no plan_tracker yet
            )
          );

          // switch back to default DB
          teldata_change_db();

          // ignore result
      }
    }
    else
      dlog('',"Customer state is ".$state['state']." ; nothing to do");

    $success  = TRUE;
  }
  catch( Exception $e )
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return array( 'success' => $success , 'errors' => $errors );
}

// obsolete
function internal_func_adjust_OCSSOC($params)
{
  return array( 'success' => TRUE , 'errors' => array() );
}

function internal_func_get_state_from_customer_id($customer_id)
{
  $context = array(
    'customer_id' => $customer_id
  );

  $state = internal_func_get_state(array(
                                  'states'  => NULL,
                                  'context' => $context,
                                 ));

  if ( !$state || !isset($state['state']) || ( $state['state'] == '' ) )
    dlog('',"ESCALATION ALERT DAILY CUSTOMER STATE NOT FOUND - ".$customer_id);

  return $state;
}


function internal_func_get_state($params)
{
  # return the result of load_state()

  $state = load_state(
    $params['states'],
    $params['context'],
    isset($params['given_plan']) ? $params['given_plan'] : NULL,
    isset($params['given_state_name']) ? $params['given_state_name'] : NULL
  );

  return $state;
}


function internal_func_check_transition($params)
{
  # Given a TRANSITION_UUID, provides the whole HTT_TRANSITION_LOG row, also the boolean flag is_closed.

  $htt_transition_log_select_query = htt_transition_log_select_query(
    array(
      'transition_uuid' => fix_transition_uuid_format($params['transition_uuid'])
    )
  );

  $results = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

  if (is_array($results))
    return $results;

  return NULL;
}


function internal_func_get_all_pending_transitions()
{
  # Provides a list of TRANSITION_UUIDs:
  # return transitions with state != 'CLOSED' and state != 'ABORTED'

  return get_transition_uuid_list(
    array(
      'NOT#CLOSED'         => TRUE,
      'NOT#ABORTED'        => TRUE
    )
  );
}


function internal_func_get_pending_transitions($params)
{
  # Given a CUSTOMER_ID, provides a list of TRANSITION_UUIDs associated to CUSTOMER_ID:
  # return transitions with state != 'CLOSED' and state != 'ABORTED'

  if ( isset( $params['customer_id'] ) )
  {
    return get_transition_uuid_list(
      array(
        'customer_id'        => $params['customer_id'],
        'NOT#CLOSED'         => TRUE,
        'NOT#ABORTED'        => TRUE
      )
    );
  }
  else
  {
    return NULL;
  }
}

function internal_func_resolve_pending_transitions($params)
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('ultra/transitions/enabled');

  if ( $value == 'FALSE' )
  {
    return array(
      'success' => FALSE,
      'errors'  => array( "ERR_API_INTERNAL: execution halted since 'ultra/transitions/enabled' is set to FALSE" )
    );
  }
  else
  {
    return run_db_transitions($params['customer_id'], $params['environments']);
  }
}

function recover_failed_transition($transition_uuid)
{
  $success  = FALSE;
  $errors   = array();
  $warnings = array();
  $notes    = array();

  try
  {
    if (Ultra\Lib\Util\Redis\get_transition_recover_attempted($transition_uuid))
      throw new \Exception("ERR_API_INTERNAL: Recovery attempted too soon after previous attempt.");

    teldata_change_db();

    $errors_found = array();

    $actions_and_transitions = get_actions_and_transitions(
      array(
        "transition_uuid" => $transition_uuid
      )
    );

    if ( ( ! $actions_and_transitions ) || ( ! is_array($actions_and_transitions) ) || ( ! count($actions_and_transitions) ) )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: transition_uuid not found");

    if ( $actions_and_transitions[0]->STATUS != 'ABORTED' )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: transition_uuid is not Aborted (".$actions_and_transitions[0]->STATUS.")");

    $customer = get_customer_from_customer_id( $actions_and_transitions[0]->CUSTOMER_ID );

    if ( ! $customer )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: no customer associated with customer id ".$actions_and_transitions[0]->CUSTOMER_ID);

    if ( ! $customer->current_iccid )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer id ".$actions_and_transitions[0]->CUSTOMER_ID." has no current_iccid");

    if ( ! $customer->CURRENT_ICCID_FULL )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer id ".$actions_and_transitions[0]->CUSTOMER_ID." has no CURRENT_ICCID_FULL");

    foreach( $actions_and_transitions as $action )
    {
      if ( ( $action->ACTION_STATUS == 'ABORTED' ) && ( $action->ACTION_RESULT ) )
      {
        $errors_found[] = $action->ACTION_RESULT;

        dlog('',"%s ; %s ; %s",$action->ACTION_STATUS,$action->ACTION_NAME,$action->ACTION_RESULT);
      }

      if ( ! $success )
      {
        if ( ( $action->ACTION_STATUS == 'ABORTED' )
              && ( $action->ACTION_NAME   == 'mvneRequestPortIn' )
              && preg_match('/waitForCommandMessage/',$action->ACTION_RESULT )
              && ( $actions_and_transitions[0]->FROM_PLAN_STATE == 'Neutral' )
              && ( $actions_and_transitions[0]->TO_PLAN_STATE   == 'Port-In Requested' )
        )
        {
          // port in timed out

          // 1 - we check if there is a row in PORTIN_QUEUE

          require_once 'classes/PortInQueue.php';

          $portInQueue = new \PortInQueue();

          $result = $portInQueue->loadByCustomerId( $actions_and_transitions[0]->CUSTOMER_ID );

          teldata_change_db();

          if ( $result->is_success() )
          {
            $notes[]  = 'PORTIN_QUEUE_ID = '.$portInQueue->portin_queue_id;

            // 2 - perform a query subscriber

            $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

            $result = $mwControl->mwQuerySubscriber(
              array(
                'actionUUID' => __FUNCTION__ . time(),
                'msisdn'     => $portInQueue->msisdn
              )
            );

            dlog('',"result data = %s",$result->data_array);

            teldata_change_db();

            if ( !$result->is_success() )
            {
              $success  = FALSE;
              $errors[] = 'QuerySubscriber failed for msisdn '.$portInQueue->msisdn;
            }
            else
            {
              if ( ( $result->data_array['ResultCode'] == '372' )
                && ( $result->data_array['ResultMsg']  == 'Subscriber is being ported in' )
              )
              {
                dlog('',"Subscriber is being ported in, he should be in state ".$actions_and_transitions[0]->TO_PLAN_STATE);

                $q1 = "update htt_customers_overlay_ultra set plan_state = '".$actions_and_transitions[0]->TO_PLAN_STATE."' where customer_id = ".$actions_and_transitions[0]->CUSTOMER_ID;
                $q2 = "update accounts set cos_id = ".$actions_and_transitions[0]->TO_COS_ID." where customer_id = ".$actions_and_transitions[0]->CUSTOMER_ID;

                $success = exec_queries_in_transaction( array ( $q1 , $q2 ) );
              }
              else
              {
                $success  = FALSE;
                $errors[] = 'QuerySubscriber returned '.$result->data_array['ResultCode'].' '.$result->data_array['ResultMsg'];
              }
            }
          }
          else
          {
            $errors[] = 'We could not find a PORTIN_QUEUE row for CUSTOMER_ID '.$actions_and_transitions[0]->CUSTOMER_ID;
            $notes[]  = 'We could not find a PORTIN_QUEUE row for CUSTOMER_ID '.$actions_and_transitions[0]->CUSTOMER_ID;
            $success  = FALSE;
          }

        } 
        elseif ( ( $action->ACTION_STATUS == 'ABORTED' )
              && ( ( preg_match('/Internal System Error/',$action->ACTION_RESULT ) )
                || ( preg_match('/Time Out issue Please Retry/',$action->ACTION_RESULT ) ) )
              && ( in_array( $action->ACTION_NAME , array( 'mvneProvisionSIM','mvneActivateSIM' ) ) )
        )
        {
          $notes[] = "Attempted redo_transition for ".$action->TRANSITION_UUID;

          $success = redo_transition( $action->TRANSITION_UUID );
        }
        elseif ( ( $action->ACTION_STATUS == 'ABORTED' )
              && ( preg_match('/\"Could not log transition\!\"/',$action->ACTION_RESULT ) )
              && ( $action->ACTION_NAME   == 'internal_func_delayed_provisioning_suspend' ) )
        {
          // INSERT INTO HTT_TRANSITION_LOG or HTT_ACTION_LOG failed

          $success = redo_transition( $action->TRANSITION_UUID );
        }
        elseif ( ( $action->ACTION_STATUS == 'ABORTED' )
              && ( preg_match('/waitForCommandMessage timed out after /',$action->ACTION_RESULT )
                || preg_match('/Activation Failed/',$action->ACTION_RESULT )
                || preg_match('/There is a pending ACC asynchronous API call for customer_id/',$action->ACTION_RESULT )
                || preg_match('/ActivateSubscriber \- 1042 \: SIM status invalid/',$action->ACTION_RESULT )
                || preg_match('/waitForACCMWControlMessage failed/',$action->ACTION_RESULT )
                || preg_match('/amdocsOutboundCall request failed/',$action->ACTION_RESULT )
                || preg_match('/500 Internal Server Error/',$action->ACTION_RESULT )
                 )
              && ( in_array( $action->ACTION_NAME , array('mvneProvisionSIM','mvneActivateSIM') ) )
              #&& ( ( $action->ACTION_NAME   == 'mvneProvisionSIM' ) )
        )
        {
          $notes[] = "Attempted redo_activation_action for mvneProvisionSIM";

          dlog('',"redo_activation_action CUSTOMER_ID = %s , TRANSITION_UUID = %s , ACTION_UUID = %s" , $action->CUSTOMER_ID , $action->TRANSITION_UUID , $action->ACTION_UUID );

          redo_activation_action( $action->CUSTOMER_ID , $action->TRANSITION_UUID , $action->ACTION_UUID );

          // check if redo_activation_action was successful

          $actions = get_actions(
            array(
              "action_uuid" => $action->ACTION_UUID
            )
          );

          if ( $actions && is_array($actions) && count($actions) && ( $actions[0]->STATUS == 'CLOSED' ) )
          {
            $notes[] = "Attempted redo_transition for ".$action->TRANSITION_UUID;

            $success = redo_transition( $action->TRANSITION_UUID );
          }
        }
        else
          dlog(''," * unhandled * ACTION_NAME = %s , ACTION_STATUS = %s , ACTION_RESULT = %s",$action->ACTION_NAME,$action->ACTION_STATUS,$action->ACTION_RESULT);
      }
    }

    if ( ! $success )
    {
      $msg = "ERR_API_INTERNAL: recovery logic not implemented";

      if ( count( $errors_found ) )
        $msg .= " (".implode(" ; ",$errors_found).")";

      $errors[] = $msg;
    }

    Ultra\Lib\Util\Redis\set_transition_recover_attempted($transition_uuid);
  }
  catch(Exception $e)
  {
    $success = FALSE;
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return array($success,$errors,$warnings,$notes);
}

/**
 * activate_customer
 *
 * Attempts to activate a Provisioned or Suspended customer, if funds are sufficient
 *
 * @return array
 */
function activate_customer( $customer=NULL , $customer_id=NULL )
{
  $result_status = array( 'success' => FALSE , 'errors' => array() , 'activated' => FALSE );

  try
  {
    if ( ! $customer )
      $customer = get_customer_from_customer_id( $customer_id );

    if ( ! $customer )
      throw new \Exception("ERR_API_INTERNAL: customer not found");

    // get current state
    $state = internal_func_get_state_from_customer_id( $customer->CUSTOMER_ID );

    if ( ! $state || empty($state['state']) || ! $state['state'] )
      throw new \Exception("ERR_API_INTERNAL: customer state could not be determined");

    if ( ( $state['state'] != STATE_PROVISIONED ) && ( $state['state'] != STATE_SUSPENDED ) )
      throw new \Exception("ERR_API_INTERNAL: customer could not be reactivated from ".$state['state']);

    // check wallet vs plan cost

    $plan_cost = get_plan_cost_by_cos_id( $customer->COS_ID );

    dlog('',"plan_cost = %s , balance = %s , stored_value = %s",$plan_cost,$customer->BALANCE,$customer->stored_value);

    if ( $plan_cost > ( $customer->BALANCE + $customer->stored_value ) )
      throw new \Exception("ERR_API_INTERNAL: Insufficient funds");

    $context = array(
      'customer_id' => $customer->CUSTOMER_ID
    );

    if ( $state['state'] == STATE_SUSPENDED )
    {
      // 'Suspended' => 'Active'
      $result_status = reactivate_suspended_account($state,$customer,$context);

      if ( $result_status['reactivated'] )
        $result_status['activated'] = TRUE;
    }
    else
      // 'Provisioned' => 'Active'
      $result_status = activate_provisioned_account($state,$customer,$context);
  }
  catch( \Exception $e )
  {
    dlog( '' , $e->getMessage() );

    $result_status['errors'][] = $e->getMessage();
  }

  return $result_status;
}

function activate_provisioned_account($state,$customer,$context)
{
  $result_status = array( 'success' => FALSE , 'errors' => array() , 'activated' => FALSE );

  dlog("","customer state is ".$state['state']);

  if ( $state['state'] == "Provisioned" )
  {
    /* *** test if we can activate the account *** */

    $resolve_now      = FALSE;
    $max_path_depth   = 1;
    $dry_run          = TRUE;
    $state_name       = "Active";
    $plan             = get_plan_from_cos_id( $customer->cos_id );

    $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

    if ( $result_status['success'] != 1 )
    {
      # we cannot activate the account

      $result_status['activated'] = FALSE;
      $result_status['success']   = TRUE;
    }
    else
    {

      /* *** activate the account *** */

      $resolve_now  = FALSE;
      $dry_run      = FALSE;

      $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

      dlog('',"change_state returned result_status = %s",$result_status);

      if ( $result_status['success'] == 1 )
      {
        $result_status['success']   = TRUE;
        $result_status['activated'] = TRUE;
      }
      else
      {
        $result_status['success']   = FALSE;
        $result_status['activated'] = FALSE;
        $result_status['errors'][]  = "ERR_API_INTERNAL: state transition error (1)";
      }
    }
  }
  else { $result_status['success'] = TRUE; }

  dlog('',"returning result_status = %s",$result_status);

  return $result_status;
}


/**
 * customer_plan_transitions
 * return a 3D array of possible plan change transition labels
 * [Mid-Cycle] =>
 *   [L19 to L29] => Change Plan Mid-Cycle L19 to L29
 *   ...
 * [Monthly] =>
 *   [L19 to L29] => Monthly Renewal and Change Plan L19 to L29
     ...
 */
function customer_plan_transitions()
{
  $transition_names = array();
  $planSize = [
    'S' => 0,
    'M' => 1,
    'L' => 2
  ];

  foreach (get_ultra_plans_short() as $from)
  {
    foreach (get_ultra_plans_short() as $to)
    {
      $mintPlanDurationFrom = substr($from, -3, 2);
      $mintPlanDurationTo = substr($to, -3, 2);
      $mintPlanSizeFrom = in_array(substr($from, -1), array_keys($planSize)) ? $planSize[substr($from, -1)] : 0;
      $mintPlanSizeTo = in_array(substr($to, -1), array_keys($planSize)) ? $planSize[substr($to, -1)] : 0;

      if ($from != $to && ! is_grandfathered_plan($to))
      {
        if (substr($from, -2) <= substr($to, -2)) // no plan downgrades Mid-Cycle
        {
          $transition_names['Mid-Cycle']["$from to $to"] = "Change Plan Mid-Cycle $from to $to";
        }
        // Allow Mint plan change mid cycle for data upgrades
        elseif ($mintPlanDurationFrom == $mintPlanDurationTo && $mintPlanSizeTo > $mintPlanSizeFrom)
        {
          $transition_names['Mid-Cycle']["$from to $to"] = "Change Plan Mid-Cycle $from to $to";
        }

        $transition_names['Monthly']["$from to $to"] = "Monthly Renewal and Change Plan $from to $to";
      }
    }
  }

  return $transition_names;
}


function transition_customer_plan($customer,$current_plan,$targetPlan,$type)
{
  $transition_names = customer_plan_transitions();

  $transition_name = ( isset ( $transition_names[$type]["$current_plan to $targetPlan"] ) )
                     ?
                     $transition_names[$type]["$current_plan to $targetPlan"]
                     :
                     ''
                     ;

  dlog('',"type = $type ; current_plan = $current_plan ; targetPlan = $targetPlan ; transition_name = $transition_name");

  return transition_customer_state($customer,$transition_name);
}

function transition_customer_state($customer,$transition_name)
{
  $resolve_now      = FALSE;
  $max_path_depth   = 1;
  $dry_run          = TRUE;

  $context = array(
    'customer_id' => $customer->customer_id
  );

  // test State Transition
  $result = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

  if ( $result['success'] == 1 )
  {
    $dry_run = FALSE;

    // perform State Transition
    $result  = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);
  }

  return $result;
}

function suspend_account($customer,$context)
{
  return change_customer_state($customer,$context,'Suspended');
}

// revert customer transition the customer to 'Cancelled'
function undo_cancel_account($params)
{
  dlog('',"params = %s",$params);

  $error = '';

  try
  {
    // get customer data
    if ( ! isset($params['customer']) )
      $params['customer'] = get_customer_from_customer_id( $params['customer_id'] );

    if ( ! $params['customer'] )
      throw new Exception("ERR_API_INTERNAL: No customer found");
    
    dlog('',"customer = %s",$params['customer']);

    #if ( ! $params['customer']->CURRENT_ICCID_FULL )
    #  throw new Exception("ERR_API_INTERNAL: No ICCID found");

    // get COS_ID and PLAN_STATE
    $sql = sprintf(
      "SELECT FROM_COS_ID , FROM_PLAN_STATE FROM HTT_TRANSITION_LOG WHERE CUSTOMER_ID = %d AND  TO_PLAN_STATE = 'Cancelled'",
      $params['customer']->CUSTOMER_ID
    );

    dlog('',$sql);

    $transition_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ( ! $transition_data ) || ( ! is_array($transition_data) ) || ( ! count($transition_data) ) )
      throw new Exception("ERR_API_INTERNAL: No Cancelled transition found");

    $cos_id     = $transition_data[0]->FROM_COS_ID;
    $plan_state = $transition_data[0]->FROM_PLAN_STATE;

    dlog('',"cos_id = $cos_id ; plan_state = $plan_state");

    // get data from HTT_CANCELLATION_REASONS
    $sql = sprintf(
      "SELECT * from HTT_CANCELLATION_REASONS WHERE CUSTOMER_ID = %d",
      $params['customer']->CUSTOMER_ID
    );

    $htt_cancellation_reasons_data = mssql_fetch_all_objects(logged_mssql_query($sql));;

    if ( ( ! $htt_cancellation_reasons_data ) || ( ! is_array($htt_cancellation_reasons_data) ) || ( ! count($htt_cancellation_reasons_data) ) )
      throw new Exception("ERR_API_INTERNAL: No HTT_CANCELLATION_REASONS data found");

    // get MSISDN and wholesalePlan
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwQuerySubscriber(
      array(
        'actionUUID' => __FUNCTION__ . time(),
        'iccid'      => $htt_cancellation_reasons_data[0]->ICCID
      )
    );

    if ( !$result->is_success() || !isset($result->data_array['body']) || !is_object($result->data_array['body']) )
      throw new Exception("QuerySubscriber failed for ".$htt_cancellation_reasons_data[0]->ICCID);

    dlog('',"data_array = %s",$result->data_array);

    if ( ! property_exists( $result->data_array['body'] , 'MSISDN' ) || ! $result->data_array['body']->MSISDN )
      throw new Exception("ERR_API_INTERNAL: no msisdn obtained from QuerySubscriber");

    $msisdn_10 = $result->data_array['body']->MSISDN;

    if ( $params['customer']->current_mobile_number
      && ( $msisdn_10 != $params['customer']->current_mobile_number )
    )
      throw new Exception("ERR_API_INTERNAL: msisdn in DB is wrong : ".$msisdn_10." , ".$params['customer']->current_mobile_number);

    // get wholesale plan from network or COS_ID
    if (property_exists($result->data_array['body'], 'wholesalePlan'))
    {
      if ($result->data_array['body']->wholesalePlan == 32)
        $wholesale = PRIMARY_WHOLESALE_PLAN;
      elseif ($result->data_array['body']->wholesalePlan == 111)
        $wholesale = SECONDARY_WHOLESALE_PLAN;
      else
        dlog('', 'WARNING: unknown wholesalePlan %s', $result->data_array['body']->wholesalePlan);
    }
    else
      $wholesale = getUltraPlanConfigurationItem(get_plan_from_cos_id($cos_id), 'wholesale');
    \Ultra\Lib\DB\Customer\setWholesalePlan($params['customer']->CUSTOMER_ID, $wholesale);

    // update ACCOUNTS.COS_ID
    $sql = accounts_update_query( array('cos_id' => $cos_id , 'customer_id' => $params['customer']->CUSTOMER_ID ) );

    dlog('',$sql);

    if ( ! run_sql_and_check($sql) )
      throw new Exception("ERR_API_INTERNAL: cannot update ACCOUNTS.COS_ID");

    // update HTT_CUSTOMERS_OVERLAY_ULTRA CURRENT_MOBILE_NUMBER, PLAN_STATE
    $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
      array(
        #'current_mobile_number' => $msisdn_10,
        'customer_id'           => $params['customer']->CUSTOMER_ID,
        'plan_state'            => $plan_state
      )
    );

    dlog('',$htt_customers_overlay_ultra_update_query);

    if ( ! run_sql_and_check($htt_customers_overlay_ultra_update_query) )
      throw new Exception("ERR_API_INTERNAL: cannot update HTT_CUSTOMERS_OVERLAY_ULTRA");

    $plan = get_plan_name_from_short_name( get_plan_from_cos_id( $cos_id ) );

    if ( ! $plan )
      throw new Exception("Cannot compute plan from cos id $cos_id");

    // undo UPDATE HTT_ULTRA_MSISDN SET MSISDN_ACTIVATED = 0 , LAST_TRANSITION_UUID = '$TID' WHERE MSISDN = $MSISDN"
    $sql = sprintf(
      "UPDATE HTT_ULTRA_MSISDN SET MSISDN_ACTIVATED = 1 , LAST_TRANSITION_UUID = NULL WHERE MSISDN = %s",
      mssql_escape_with_zeroes( $msisdn_10 )
    );

    dlog('',$sql);

    if ( ! run_sql_and_check($sql) )
      throw new Exception("ERR_API_INTERNAL: cannot update HTT_ULTRA_MSISDN");

    // undo DELETE FROM ACCOUNT_ALIASES
    $return = msisdn_activation_handler($params['customer']->CUSTOMER_ID);
    if ( ! $return['success'] )
      throw new Exception("ERR_API_INTERNAL: error after msisdn_activation_handler");

    // undo UPDATE HTT_INVENTORY_SIM SET EXPIRES_DATE = getutcdate() WHERE ICCID_NUMBER = $ICCID

    $sql = sprintf(
      "UPDATE HTT_INVENTORY_SIM SET EXPIRES_DATE = NULL WHERE ICCID_NUMBER = %s",
      mssql_escape_with_zeroes( $params['customer']->current_iccid )
    );

    dlog('',$sql);

    if ( ! run_sql_and_check($sql) )
      throw new Exception("ERR_API_INTERNAL: UPDATE HTT_INVENTORY_SIM not executed");
    
    // undo disable ACCOUNTS WHERE customer_id = $ID
    $sql = sprintf(
      'UPDATE ACCOUNTS SET %s WHERE customer_id = %d',
      account_enabled_assignment(TRUE),
      $params['customer']->CUSTOMER_ID
    );

    dlog('',$sql);
    
    if ( ! run_sql_and_check($sql) )
      throw new Exception("ERR_API_INTERNAL: UPDATE ACCOUNTS not executed");
    
    $sql = sprintf(
        "delete from HTT_CANCELLATION_REASONS where CUSTOMER_ID = %d",
          $params['customer']->CUSTOMER_ID
     );
    
    dlog('',$sql);
    
    if ( ! run_sql_and_check($sql) )
      throw new Exception("ERR_API_INTERNAL: delete from HTT_CANCELLATION_REASONS not executed");
              
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $error = $e->getMessage();
  }

  return $error;
}

/**
 * cancel_account
 *
 * Transition the customer to 'Cancelled'
 *
 * @return array
 */
function cancel_account($customer,$context,$port_out_info=NULL,$agent=NULL,$notes='no_notes_provided',$source='NONE')
{
  dlog('', '(%s)', func_get_args());

  // get current state
  $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

  dlog('', "customer state is %s", $state);

  if ( $state && ( $state['state'] == 'Port-In Requested' ) )
  {
    // cannot go directly from 'Port-In Requested' to 'Cancelled': we have to transition to 'Port-In Denied' before

    $result_status = change_customer_state($customer,$context,'Port-In Denied');

    if ( count( $result_status['errors'] ) )
    {
      $result = array( 'success' => FALSE , 'errors' => $result_status['errors'] );
    }
    else
    {
      sleep(1);

      $customer = get_customer_from_customer_id($customer->CUSTOMER_ID);

      $result = change_state_cancel_unportable($customer,$context,$agent);
    }
  }
  elseif ( $state && ( $state['state'] == 'Port-In Denied' ) )
  {
    $result = change_state_cancel_unportable($customer,$context,$agent);
  }
  elseif ( $state && ( $state['state'] == 'Suspended' ) )
  {
    $result = change_state_cancel_suspended($customer,$context,$agent);
  }
  elseif ( $state && ( $state['state'] == 'Neutral' ) )
  {
    $result = change_state_cancel_neutral($customer,$context,$agent);
  }
  elseif ( $state && ( $state['state'] == 'Active' ) )
  {
    $result = change_state_cancel_active($customer,$context,$port_out_info,$agent);
  }
  else
  {
    $result = change_customer_state( $customer , $context , 'Cancelled' , 'STANDBY' , FALSE );

    if ( $result['success'] )
    {
      $iccid = '';

      if ( property_exists( $customer , 'current_iccid_full' ) )
        $iccid = $customer->current_iccid_full;

      if ( property_exists( $customer , 'CURRENT_ICCID_FULL' ) )
        $iccid = $customer->CURRENT_ICCID_FULL;

      if ( property_exists( $customer , 'current_iccid' ) )
        $iccid = luhnenize( $customer->current_iccid );

      if ( property_exists( $customer , 'CURRENT_ICCID' ) )
        $iccid = luhnenize( $customer->CURRENT_ICCID );

      $check = htt_cancellation_reasons_add(
        array(
          'customer_id' => $customer->CUSTOMER_ID,
          'reason'      => 'Cancel '.$state['state'],
          'msisdn'      => $customer->current_mobile_number,
          'iccid'       => $iccid,
          'cos_id'      => $customer->COS_ID,
          'type'        => 'CANCELLED',
          'status'      => $state['state'],
          'agent'       => $agent
        )
      );

      if ( ! $check ) // non-fatal error
        dlog('',"Cannot insert into HTT_CANCELLATION_REASONS");
    }
  }

  if ( $result['success'] )
  {
    $user = ( isset($_SERVER) && is_array($_SERVER) && isset($_SERVER['PHP_AUTH_USER']) )
          ?
          $_SERVER['PHP_AUTH_USER']
          :
          'no_user'
          ;

    // add info to DB table HTT_CUSTOMERS_NOTES
    $sql = htt_customers_notes_insert_query(
      array(
        'customer_id'  => $customer->CUSTOMER_ID,
        'type'         => 'CANCEL',
        'source'       => $source,
        'user'         => $user,
        'contents'     => $notes
      )
    );

    if ( ! is_mssql_successful(logged_mssql_query($sql)) ) // non-fatal error
      dlog('',"Cannot insert into HTT_CUSTOMERS_NOTES");

    $familyResult = \Ultra\Lib\Flex::removeFromFamily($customer->CUSTOMER_ID);
    if ($familyResult->is_failure())
      \logError("ESCALATION FLEX error removing cancelled customer from family {$customer->CUSTOMER_ID}");
  }

  return $result;
}

function change_state_cancel_unportable($customer,$context,$agent=NULL)
{
  // currently [ 'Port-In Denied' ] => [ 'Cancelled' ] works only with 'take transition'

  $resolve_now     = FALSE;
  $dry_run         = FALSE;
  $max_path_depth  = 1;
  $transition_name = 'Cancel Unportable';

  $result_status = change_state( $context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth );

  if ( $result_status['success'] )
  {
    $iccid = '';

    if ( property_exists( $customer , 'current_iccid_full' ) )
      $iccid = $customer->current_iccid_full;

    if ( property_exists( $customer , 'CURRENT_ICCID_FULL' ) )
      $iccid = $customer->CURRENT_ICCID_FULL;

    if ( property_exists( $customer , 'current_iccid' ) )
      $iccid = luhnenize( $customer->current_iccid );

    if ( property_exists( $customer , 'CURRENT_ICCID' ) )
      $iccid = luhnenize( $customer->CURRENT_ICCID );

    $check = htt_cancellation_reasons_add(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'reason'      => 'Cancel Unportable',
        'msisdn'      => $customer->current_mobile_number,
        'iccid'       => $iccid,
        'type'        => 'CANCELLED',
        'cos_id'      => $customer->COS_ID,
        'status'      => 'Port-In Denied'
      )
    );

    if ( ! $check ) // non-fatal error
    { dlog('',"Cannot insert into HTT_CANCELLATION_REASONS"); }
  }

  return $result_status;
}

function change_state_cancel_suspended($customer,$context,$agent=NULL)
{
  // currently [ 'Suspended' ] => [ 'Cancelled' ] works only with 'take transition'

  $iccid = '';

  if ( property_exists( $customer , 'current_iccid_full' ) )
    $iccid = $customer->current_iccid_full;

  if ( property_exists( $customer , 'CURRENT_ICCID_FULL' ) )
    $iccid = $customer->CURRENT_ICCID_FULL;

  if ( property_exists( $customer , 'current_iccid' ) )
    $iccid = luhnenize( $customer->current_iccid );

  if ( property_exists( $customer , 'CURRENT_ICCID' ) )
    $iccid = luhnenize( $customer->CURRENT_ICCID );

  list ($transition_name, $reason) = array('Cancel Suspended', 'Cancel Suspended');

  $resolve_now     = FALSE;
  $dry_run         = FALSE;
  $max_path_depth  = 1;

  $result_status = change_state( $context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth );

  if ( $result_status['success'] )
  {
    $check = htt_cancellation_reasons_add(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'reason'      => $reason,
        'msisdn'      => $customer->current_mobile_number,
        'iccid'       => $iccid,
        'type'        => 'CANCELLED',
        'cos_id'      => $customer->COS_ID,
        'status'      => 'Suspended',
        'ever_active' => 1
      )
    );

    if ( ! $check ) // non-fatal error
    { dlog('',"Cannot insert into HTT_CANCELLATION_REASONS"); }
  }

  return $result_status;
}

function change_state_cancel_active($customer,$context,$port_out_info,$agent=NULL)
{
  // [ 'Active' ] => [ 'Cancelled' ]

  $iccid = '';

  if ( property_exists( $customer , 'current_iccid_full' ) )
    $iccid = $customer->current_iccid_full;

  if ( property_exists( $customer , 'CURRENT_ICCID_FULL' ) )
    $iccid = $customer->CURRENT_ICCID_FULL;

  if ( property_exists( $customer , 'current_iccid' ) )
    $iccid = luhnenize( $customer->current_iccid );

  if ( property_exists( $customer , 'CURRENT_ICCID' ) )
    $iccid = luhnenize( $customer->CURRENT_ICCID );

  list ($transition_name, $reason) = array('Cancel Active', 'Cancel Active');

  $resolve_now     = FALSE;
  $dry_run         = FALSE;
  $max_path_depth  = 1;

  $result_status = change_state( $context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth );

  if ( $result_status['success'] )
  {
    $htt_cancellation_reasons_data = array(
      'customer_id' => $customer->CUSTOMER_ID,
      'reason'      => $reason,
      'msisdn'      => $customer->current_mobile_number,
      'iccid'       => $iccid,
      'type'        => 'CANCELLED',
      'status'      => 'Active',
      'cos_id'      => $customer->COS_ID,
      'ever_active' => 1
    );

    if ( $port_out_info && is_array($port_out_info) )
    {
      $htt_cancellation_reasons_data = array_merge( $htt_cancellation_reasons_data , $port_out_info );
    }

    $check = htt_cancellation_reasons_add( $htt_cancellation_reasons_data );

    if ( ! $check ) // non-fatal error
    { dlog('',"Cannot insert into HTT_CANCELLATION_REASONS"); }
  }

  return $result_status;
}

/**
 * change_state_cancel_intra_port
 *
 * change customer state with Intra brand cancellation
 * which will not cancel on the network
 *
 * @param  Object $customer
 * @param  Array  $context
 * @param  Array  $port_out_info
 * @return array
 */
function change_state_cancel_intra_port($customer,$context,$port_out_info=NULL)
{
  // [ 'Active' ] => [ 'Cancelled' ]

  $iccid = '';

  if ( property_exists( $customer , 'current_iccid_full' ) )
    $iccid = $customer->current_iccid_full;

  if ( property_exists( $customer , 'CURRENT_ICCID_FULL' ) )
    $iccid = $customer->CURRENT_ICCID_FULL;

  if ( property_exists( $customer , 'current_iccid' ) )
    $iccid = luhnenize( $customer->current_iccid );

  if ( property_exists( $customer , 'CURRENT_ICCID' ) )
    $iccid = luhnenize( $customer->CURRENT_ICCID );

  \logit('COS_ID: ' . $customer->cos_id);

  list ($transition_name, $reason) = ($customer->plan_state == STATE_ACTIVE)
    ? array('Intra brand cancellation ' . get_plan_name_from_short_name(get_plan_from_cos_id($customer->cos_id)) . ' Active', 'CANCELLED Active INTRA-BRAND')
    : array('Intra brand cancellation ' . get_plan_name_from_short_name(get_plan_from_cos_id($customer->cos_id)) . ' Suspended', 'CANCELLED Suspended INTRA-BRAND');

  \logit('TRANSITION_NAME: ' . $transition_name);
  \logit('TRANSITION_REASON: ' . $reason);

  $resolve_now     = TRUE;
  $dry_run         = FALSE;
  $max_path_depth  = 1;

  $result_status = change_state( $context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth );

  if ( $result_status['success'] )
  {
    $htt_cancellation_reasons_data = array(
      'customer_id' => $customer->CUSTOMER_ID,
      'reason'      => $reason,
      'msisdn'      => $customer->current_mobile_number,
      'iccid'       => $iccid,
      'type'        => 'CANCELLED',
      'status'      => $customer->plan_state,
      'cos_id'      => $customer->COS_ID,
      'ever_active' => 1
    );

    if ( $port_out_info && is_array($port_out_info) )
    {
      $htt_cancellation_reasons_data = array_merge( $htt_cancellation_reasons_data , $port_out_info );
    }

    $check = htt_cancellation_reasons_add( $htt_cancellation_reasons_data );

    if ( ! $check ) // non-fatal error
    { dlog('',"Cannot insert into HTT_CANCELLATION_REASONS"); }

    $result = \Ultra\Lib\Flex::removeFromFamily($customer->CUSTOMER_ID);
    if ($result->is_failure())
      \logError("ESCALATION FLEX error removing cancelled customer from family {$customer->CUSTOMER_ID}");
  }

  return $result_status;
}


function change_state_cancel_neutral($customer,$context,$agent=NULL)
{
  // currently [ 'Neutral' ] => [ 'Cancelled' ] works only with 'take transition'

  $resolve_now     = FALSE;
  $dry_run         = FALSE;
  $max_path_depth  = 1;
  $transition_name = 'Cancel Neutral';

  $result_status = change_state( $context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth );

  if ( $result_status['success'] )
  {
    $iccid = '';

    if ( property_exists( $customer , 'current_iccid_full' ) )
      $iccid = $customer->current_iccid_full;

    if ( property_exists( $customer , 'CURRENT_ICCID_FULL' ) )
      $iccid = $customer->CURRENT_ICCID_FULL;

    if ( property_exists( $customer , 'current_iccid' ) )
      $iccid = luhnenize( $customer->current_iccid );

    if ( property_exists( $customer , 'CURRENT_ICCID' ) )
      $iccid = luhnenize( $customer->CURRENT_ICCID );

    $check = htt_cancellation_reasons_add(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'reason'      => 'Cancel Neutral',
        'msisdn'      => $customer->current_mobile_number,
        'iccid'       => $iccid,
        'type'        => 'CANCELLED',
        'cos_id'      => $customer->COS_ID,
        'status'      => 'Neutral'
      )
    );

    if ( ! $check ) // non-fatal error
    { dlog('',"Cannot insert into HTT_CANCELLATION_REASONS"); }
  }

  return $result_status;
}

/**
 * change_customer_state
 *
 * @return array
 */
function change_customer_state( $customer , $context , $state_name , $plan=NULL , $now=TRUE )
{
  $result_status = array( 'success' => FALSE , 'errors' => array() );

  /* *** test if we can change the state to $state_name *** */

  $resolve_now      = FALSE;
  $max_path_depth   = 1;
  $dry_run          = TRUE;

  if (!isset($plan))
  {
    $plan = get_plan_from_cos_id( $customer->cos_id );
  }

  $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

  if ( $result_status['success'] == 1 )
  {
    /* *** change the state to $state_name *** */

    $resolve_now  = $now;
    $dry_run      = FALSE;

    $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

    $result_status['success'] = ! ! $result_status['success'];
  }

  return $result_status;
}

function change_suspended_plan($state,$customer,$context,$plan)
{
  dlog('',"customer state is ".$state['state']);

  /* *** test if we can perform the transition *** */

  $resolve_now      = FALSE;
  $max_path_depth   = 1;
  $dry_run          = TRUE;
  $state_name       = "Suspended";

  $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

  if ( $result_status['success'] == 1 )
  {
    /* *** perform the transition *** */

    $resolve_now  = TRUE;
    $dry_run      = FALSE;

    $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

    $result_status['success'] = ! ! $result_status['success'];
  }

  return $result_status;
}

function activate_promo_account($state,$customer,$context)
{
  $result_status = array( 'success' => FALSE , 'errors' => array() );
 
  try
  {
    // there should not be pending transitions for this customer
 
    $count_customer_open_transitions = count_customer_open_transitions( $customer->CUSTOMER_ID );
 
    if ( $count_customer_open_transitions )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer ".$customer->CUSTOMER_ID." has $count_customer_open_transitions pending transitions");
 
    // get data from ULTRA.PROMOTIONAL_PLANS through ULTRA.CUSTOMER_OPTIONS
    $ultra_promotional_plan_data = get_ultra_promotional_plan_by_customer_id( $customer->CUSTOMER_ID );
 
    if ( ! ( $ultra_promotional_plan_data && is_array($ultra_promotional_plan_data) && count($ultra_promotional_plan_data) ) )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer is not associated to any promotional plan");
 
    dlog('',"%s",$ultra_promotional_plan_data);
 
    $ultra_promotional_plan_data = $ultra_promotional_plan_data[0];
 
    // rules to be applied before initiating the 'Promo Unused' => 'Active' transition
    $return = ultra_promotional_plans_pre_activation( $customer->CUSTOMER_ID , $ultra_promotional_plan_data->ULTRA_PROMOTIONAL_PLANS_ID , $customer->CURRENT_ICCID_FULL );
 
    if ( ! $return["success"] )
      throw new Exception( $return['errors'][0] );
 
    $plan = get_plan_name_from_short_name( get_plan_from_cos_id( $ultra_promotional_plan_data->TARGET_PLAN ) );
 
    // transition from 'Promo Unused' to 'Active'
 
    $transition_name = 'Activate Promo '.$plan;
    $resolve_now     = TRUE;
    $dry_run         = FALSE;
    $max_path_depth  = 1;

    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $result_status['errors'][] = $e->getMessage();
  }
 
  return $result_status;
}

function reactivate_suspended_account($state,$customer,$context,$reactivate_now=TRUE)
{
  $result_status = array( 'success' => FALSE , 'errors' => array() , 'reactivated' => FALSE );

  dlog('',"customer state is ".$state['state']);

  if ( $state['state'] == "Suspended" )
  {
    /* *** test if there is a state change already running *** */

    $htt_transition_log_select_query = htt_transition_log_select_query(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'status'      => 'OPEN'
      )
    );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

    if ( $query_result && is_array($query_result) && count($query_result) )
    {
      dlog('',"There are pending transitions for customer id ".$customer->CUSTOMER_ID);
      $result_status['success']     = TRUE;
      return $result_status;
    }

    /* *** test if we can re-activate the account *** */

    $resolve_now      = FALSE;
    $max_path_depth   = 1;
    $dry_run          = TRUE;
    $state_name       = "Active";
    $plan             = get_plan_from_cos_id( $customer->cos_id );

    $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

    if ( $result_status['success'] != 1 )
    {
      # we cannot re-activate the account

      $result_status['success']     = TRUE;
      $result_status['reactivated'] = FALSE;
    }
    else
    {

      /* *** re-activate the account *** */

      $resolve_now  = $reactivate_now;
      $dry_run      = FALSE;

      $result_status = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

      if ( $result_status['success'] == 1 )
      {
        $result_status['success']     = TRUE;
        $result_status['reactivated'] = TRUE;
      }
      else
      {
        $result_status['success'] = FALSE;

        # TODO: is this an error case?
        $result_status['errors'][] = "ERR_API_INTERNAL: state transition error (1)";
      }
    }
  }
  else { $result_status['success'] = TRUE; }

  return $result_status;
}


# secondary functions #


function get_transition_uuid_list($params)
{
  $result = array();

  $htt_transition_log_select_query = htt_transition_log_select_query($params);

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

  if (! is_array($query_result) ) return NULL;

  foreach ($query_result as $row)
  {
    $result[] = $row->TRANSITION_UUID;
  }

  return $result;
}


function append_transition_failure_reason($errors,$request_id)
{
  # this has to be invoked in each asych verify request in order to append the transition error message (MVNO-529)

  if ( count($errors) > 0 )
  {
    $transition_failure_reason = func_get_transition_failure_reason( $request_id );

    if ( $transition_failure_reason )
    {
      $decode = disclose_json($transition_failure_reason);
      if ($decode)
      {
        $r = new Result($decode);
        $errors = $r->get_errors();
      }
      else
      {
        $errors[] = $transition_failure_reason;

        if ( preg_match("/request aborted/",$errors[0]) )
        {
          // Example: "message":"Other logical error: SIM is Invalid"
          if ( preg_match("/\"message\"\:\"([^\"]+)\"/",$transition_failure_reason,$matches) )
          {
            $errors[0] .= " ".$matches[1];
          }
        }
      }
    }
  }

  return $errors;
}


function get_last_transition_name_by_customer_id($customer_id)
{
  $transition_uuid = '';

  $query = htt_transition_log_last_closed($customer_id);

  $query_result = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( is_array($query_result) && ( count($query_result) > 0 ) )
  {
    $transition_uuid = $query_result[0]->TRANSITION_LABEL;
  }

  return $transition_uuid;
}


/**
 * redo_activation_action
 *
 * Adjust DB fields when an activation failed for some reasons but we can recover it
 *
 * @return NULL
 */
function redo_activation_action($customer_id, $transition_uuid=NULL, $action_uuid=NULL)
{
  // get HTT_CUSTOMERS_OVERLAY_ULTRA.CURRENT_ICCID_FULL
  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields' => array("CURRENT_ICCID_FULL"),
      'customer_id'   => $customer_id
    )
  );

  $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

  dlog('',"%s",$ultra_customer_data);

  if ( ! ( $ultra_customer_data && is_array($ultra_customer_data) && count($ultra_customer_data) ) )
    return make_error_Result("Customer $customer_id not found");

  if ( ! $ultra_customer_data[0]->CURRENT_ICCID_FULL )
    return make_error_Result("Customer $customer_id has no ICCID");

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $mwResult = $mwControl->mwGetNetworkDetails(
    array(
      'iccid'      => $ultra_customer_data[0]->CURRENT_ICCID_FULL,
      'actionUUID' => __FUNCTION__ . time()
    )
  );

  if ( $mwResult->is_failure() )
    return $mwResult;

  if ( !is_array($mwResult->data_array)
    || !isset($mwResult->data_array['body'])
    || !is_object($mwResult->data_array['body'])
    || !property_exists( $mwResult->data_array['body'] , 'MSISDN' )
    || !$mwResult->data_array['body']->MSISDN
  )
  {
    dlog('','mwGetNetworkDetails did not return a MSISDN');

    if ( $transition_uuid && $action_uuid )
      abort_action_and_transition($transition_uuid, $action_uuid, "aborted by redo_activation_action",$customer_id);

    return make_error_Result("mwGetNetworkDetails did not return a MSISDN for ICCID ".$ultra_customer_data[0]->CURRENT_ICCID_FULL);
  }

  dlog('',"mwResult data body = %s",$mwResult->data_array['body']);

  $msisdn = $mwResult->data_array['body']->MSISDN;

  dlog('',"about the record msisdn");

  // activation succeeded: we record the msisdn
  $record_ret = record_msisdn($customer_id, $msisdn, $transition_uuid);

  if ( $record_ret->is_failure() )
    return $record_ret;

  // finally set STATUS = 'CLOSED' for this action
  $success = close_action($action_uuid, "CLOSED", "closed by redo_activation_action");

  if ( $success )
    return make_ok_Result();
  else
    return make_error_Result("close_action failed");
}

/**
 * callbackChangeSIM
 *
 * 1. update HTT_CUSTOMERS_OVERLAY_ULTRA
 * 2. associate $customer_id with $iccid in HTT_INVENTORY_SIM
 * 3. associate $customer_id with $iccid in Redis
 * 
 * $iccid is the new iccid
 * @return array
 */
function callbackChangeSIM($iccid,$customer_id,$store_id='',$user_id='')
{
  $errors = array();

  if (NULL == $iccid)
  {
    $e = "callbackChangeSIM invoked with no ICCID!";
    dlog('', $e);
    $errors[] = $e;
  }
  else if (NULL == $customer_id)
  {
    $e = "callbackChangeSIM invoked with no customer_id!";
    dlog('', $e);
    $errors[] = $e;
  }
  else
  {
    $iccid_19 = $iccid;
    $iccid_18 = $iccid;

    if (strlen($iccid) == 18)
    { $iccid_19 = luhnenize($iccid_18);   }
    else
    { $iccid_18 = substr($iccid_19,0,-1); }

    // sets
    // .  htt_customers_overlay_ultra.current_iccid
    // .. htt_customers_overlay_ultra.current_iccid_full
    $check = run_sql_and_check(
      htt_customers_overlay_ultra_update_query(
        array(
          'customer_id'        => $customer_id,
          'current_iccid'      => $iccid_18,
          'current_iccid_full' => $iccid_19
        )
      )
    );

    if ( ! $check ) { dlog('',"Error while trying to update htt_customers_overlay_ultra"); }

    // set ICCID as activated
    $check = run_sql_and_check(
      htt_inventory_sim_update_query(
        array(
          'batch_sim_set'         => array( $iccid_18 ),
          'customer_id'           => $customer_id,
          'sim_activated'         => 1,
          'last_changed_date'     => 'getutcdate()',
          'last_changed_by'       => $user_id
        )
      )
    );

    if ( ! $check ) { dlog('',"Error while trying to update HTT_INVENTORY_SIM"); }
    
  }

  return $errors;
}

?>
