<?php

/* NOTE: you should only use this as an include to lib/transitions.php */
include_once('db/destination_origin_map.php');
include_once('db/htt_action_log.php');
include_once('db/htt_bucket_event_log.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_inventory_sim.php');
include_once('db/htt_portin.php');
include_once('db/htt_ultra_msisdn.php');
include_once('lib/messaging/functions.php');
include_once('lib/state_machine/functions.php');
include_once('lib/provisioning/functions.php');
include_once('classes/Result.php');
include_once('Ultra/Lib/Util/Redis.php');
require_once 'lib/inventory/functions.php';
require_once 'Ultra/Lib/DB/Customer.php';
include_once('db/account_aliases.php');
# require_once 'Ultra/Lib/DB/DestinationOriginMap.php';

/**
 * record_msisdn
 *
 * Save MSISDN to
 * - HTT_ULTRA_MSISDN
 * - HTT_CUSTOMERS_OVERLAY_ULTRA
 * - ULTRA.HTT_ACTIVATION_HISTORY
 */
function record_msisdn($customer_id, $msisdn, $transition_uuid, $check=FALSE)
{
  if (!isset($msisdn))          return make_error_Result("No MSISDN given to record_msisdn()");
  if (!isset($customer_id))     return make_error_Result("No customer ID given to record_msisdn()");
  if (!isset($transition_uuid)) return make_error_Result("No transition_uuid given to record_msisdn()");

  if (strlen($msisdn) == 11)
  {
    $msisdn = substr($msisdn, 1);
  }

  // Ted: the purpose of this block is simply to verify htt_customers_overlay_ultra.current_mobile_number ?
  //      I am asking because I would like to replace make_find_ultra_customer_query_from_customer_id with a lighter query (see MVNO-1004)
  if ($check)
  {
    $query = make_find_ultra_customer_query_from_customer_id($customer_id);

    $customer = find_customer( $query );

    if (!$customer) return make_error_Result("Customer $customer_id not found");

    if ($customer->current_mobile_number === $msisdn)
    {
      return make_ok_Result(array(),
                            "Customer $customer_id already has MSISDN $msisdn");
    }
  }

  dlog('', 'About to save MSISDN %s', $msisdn);

  $check = run_sql_and_check(
    htt_customers_overlay_ultra_update_query(
      array(
        'customer_id'           => $customer_id,
        'current_mobile_number' => $msisdn
      )
    )
  );

  // give up if we couldn't UPDATE
  if (!$check)
  {
    dlog('','Could not save MSISDN');
    return make_error_Result("ERR_API_INTERNAL: could not save MSISDN $msisdn");
  }

  // record new msisdn into HTT_ULTRA_MSISDN
  if ( !add_to_htt_ultra_msisdn( $customer_id, $msisdn, 1, $transition_uuid, '2' ) )
    dlog('',"add_to_htt_ultra_msisdn failed");

  // record new msisdn into ULTRA.HTT_ACTIVATION_HISTORY
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id' => $customer_id,
      'msisdn'      => $msisdn,
    )
  );

  if ( !is_mssql_successful(logged_mssql_query($sql)) )
    dlog('',"ULTRA.HTT_ACTIVATION_HISTORY update failed");

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return make_ok_Result();
}

// insert into ACCOUNT_ALIASES
// insert into DESTINATION_ORIGIN_MAP
function msisdn_activation_handler($customer_id)
{
  dlog('', 'customer_id: %d', $customer_id);

  $return = array(
    'success' => FALSE,
    'errors'  => array(),
  );

  $query    = make_find_ultra_customer_query_from_customer_id($customer_id);
  $customer = find_customer( $query );


  if ($customer)
  {
    if ( ! isset($customer->current_mobile_number) || strlen($customer->current_mobile_number) !== 10 )
    {
      dlog('', 'missing or invalid MSISDN');
      $return['errors'][] = "ERR_API_INTERNAL: missing or invalid MSISDN";
      return $return;
    }

    if ( ! isset($customer->ACCOUNT_ID) || ! $customer->ACCOUNT_ID )
    {
      dlog('', 'could not find ACCOUNT_ID for customer');
      $return['errors'][] = "ERR_API_INTERNAL: could not find ACCOUNT_ID for customer";
      return $return;
    }

    # insert into ACCOUNT_ALIASES
    $account_aliases_params = array(
      'msisdn'     => $customer->current_mobile_number,
      'account_id' => $customer->ACCOUNT_ID
    );

    if ( \Ultra\Lib\DB\AccountAliases\insertAccountAliases($account_aliases_params) == 0 )
    {
      # insert into DESTINATION_ORIGIN_MAP
      # $check = \Ultra\Lib\DB\DestinationOriginMap\insertAfterActivation($customer);
      $check = destination_origin_map_insert_after_activation($customer);

      if ( $check )
      {
        $return['success'] = TRUE;
      }
      else
      {
        dlog('', "DESTINATION_ORIGIN_MAP insert error");
        $return['errors'][] = "ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP insert error";
      }
    }
    else
    {
      dlog('', "account_aliases insert error");
      $return['errors'][] = "ERR_API_INTERNAL: account_aliases insert error";
    }
  }
  else
  {
    dlog('', "customer not found");
    $return['errors'][] = "ERR_API_INTERNAL: customer not found";
  }

  return $return;
}

// invoke record_ICCID_activation
// update ACTIVATION_ICCID
// reserve iccid
function sim_activation_handler($customer_id, $transition_uuid, $cos_id)
{
  dlog('',"sim_activation_handler (".$customer_id.",".$transition_uuid.")");

  $return = array(
    'success' => FALSE,
    'errors'  => array(),
  );

  $query = make_find_ultra_customer_query_from_customer_id($customer_id);

  $customer = find_customer( $query );

  if ($customer)
  {
    $record_ICCID_activation_params = array(
      'iccid'           => substr($customer->CURRENT_ICCID_FULL,0,-1), # I assume iccid is in htt_customers_overlay_ultra
      'customer_id'     => $customer_id,
      'transition_uuid' => $transition_uuid
    );

    # for ported customers, activation attribution info are stored in HTT_PORTIN_LOG
    $porting_request_data = get_porting_request_data( $customer->CUSTOMER_ID );

    if ( $porting_request_data && is_object($porting_request_data) )
    {
      $record_ICCID_activation_params['masteragent']  = $porting_request_data->ACTIVATION_MASTERAGENT ? $porting_request_data->ACTIVATION_MASTERAGENT : '0';
      $record_ICCID_activation_params['distributor']  = $porting_request_data->ACTIVATION_DISTRIBUTOR ? $porting_request_data->ACTIVATION_DISTRIBUTOR : '0';
      $record_ICCID_activation_params['agent']        = $porting_request_data->ACTIVATION_AGENT       ? $porting_request_data->ACTIVATION_AGENT       : '0';
      $record_ICCID_activation_params['store']        = $porting_request_data->ACTIVATION_STORE       ? $porting_request_data->ACTIVATION_STORE       : '0';
      $record_ICCID_activation_params['user_id']      = $porting_request_data->ACTIVATION_USER_ID     ? $porting_request_data->ACTIVATION_USER_ID     : '0';
    }
    else
    {
      // try redis first
      list($masteragent, $agent, $distributor, $store, $userid) = get_redis_provisioning_values($customer_id);
      if (empty($masteragent) && empty($agent) && empty($distributor) && empty($store) && empty($userid))
      {
        // try to load from transition context
        $query = htt_transition_log_select_query(array(
          'sql_limit'       => 1,
          'customer_id'     => $customer_id,
          'from_plan_state' => 'neutral'
        ));

        $data = mssql_fetch_all_objects(logged_mssql_query($query));
        $data = json_decode($data[0]->CONTEXT);

        if ( ! is_object($data) && ! empty($data[0]) )
        {
          $data = $data[0];
          if ( ! isset($data->masteragent) && ! isset($data->agent) && ! isset($data->user_id) && ! isset($data->distributor) && ! isset($data->store))
          {
            dlog('',"ERROR ESCALATION ALERT DAILY - we could not retrieve attribution info for {$customer_id}");
          }
          else
          {
            $masteragent  = $data->masteragent;
            $agent        = $data->agent;
            $userid       = $data->user_id;
            $distributor  = $data->distributor;
            $store        = $data->store;
          }
        }
        else
        {
          dlog('',"ERROR ESCALATION ALERT DAILY - we could not retrieve transition context info for {$customer_id}");
        }
      }

      // provide '0' as defaults for all these values for the cases
      // where we're not activating from a provisioning API command
      $record_ICCID_activation_params['masteragent'] = empty($masteragent)  ? '0' : $masteragent;
      $record_ICCID_activation_params['agent']       = empty($agent)        ? '0' : $agent;
      $record_ICCID_activation_params['user_id']     = empty($userid)       ? '0' : $userid;
      $record_ICCID_activation_params['distributor'] = empty($distributor)  ? '0' : $distributor;
      $record_ICCID_activation_params['store']       = empty($store)        ? '0' : $store;
    }

    // BOLT-38
    $plan_cost = get_plan_cost_by_cos_id($cos_id);
    $bolt_ons = get_bolt_ons_by_customer_id($customer->CUSTOMER_ID);
    $bolt_ons_cost = 0;
    $bolt_ons_names = NULL;
    foreach($bolt_ons as $bolt_on => $details)
    {
      $bolt_ons_cost += $details['cost'];
      $bolt_ons_names .= ($bolt_ons_names ? ',' : NULL) . $bolt_on;
    }
    dlog('', "plan_cost: $plan_cost, bolt_on_cost: $bolt_ons_cost, bolt_ons_names: $bolt_ons_names");
    $record_ICCID_activation_params['promised_amount'] = $plan_cost + $bolt_ons_cost;
    $record_ICCID_activation_params['initial_cos_id'] = $cos_id;
    $record_ICCID_activation_params['initial_bolt_ons'] = $bolt_ons_names;

    $record_ICCID_activation_result = record_ICCID_activation( $record_ICCID_activation_params );

    if ( $record_ICCID_activation_result['success'] )
    {
      # update ACTIVATION_ICCID

      $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
        array(
          'activation_iccid'      => substr($customer->CURRENT_ICCID_FULL,0,-1), # 18 digits
          'activation_iccid_full' => $customer->CURRENT_ICCID_FULL,              # 19 digits
          'customer_id'           => $customer->CUSTOMER_ID
        )
      );

      if ( run_sql_and_check($htt_customers_overlay_ultra_update_query) )
      {
        $check = reserve_iccid($customer->CURRENT_ICCID_FULL);

        if ($check)
        {
          $return['success'] = TRUE;
        }
        else
        {
          dlog('', "HTT_INVENTORY_SIM update error (recording the reservation time)");
          $return['errors'][] = "ERR_API_INTERNAL: could not save ICCID reservation time";
        }
      }
      else
      {
        dlog('', "ACTIVATION_ICCID update error");
        $return['errors'][] = "ACTIVATION_ICCID update error";
      }
    }
    else
    {
      dlog('', "record_ICCID_activation failed");
      $return['errors'] = $record_ICCID_activation_result['errors'];
    }
  }
  else
  {
    dlog('', "customer not found");
    $return['errors'][] = "ERR_API_INTERNAL: customer not found";
  }

  /* Still a problem, even after delays, on Oct 21.
     sleep(120); # Handling for Aspider Issues. MVNO-528
  */

  return $return;
}
