<?php

function get_transition_blocks($conversion_factor)
{
  return array(
  "ZeroMinutesBillingType1ZeroCreditLimit" => array("packaged_balance1" => 0, "billing_type" => 1, "credit_limit" => "0", ),
  "ZeroMinutesAndBalanceBillingType1" => array("balance" => 0, "packaged_balance1" => 0, "billing_type" => 1, ),
  // note the current balance is used in the conversion first, THEN it's set to 0
  "RoundMinutesZeroBalanceBillingType1" => array("packaged_balance1" => "floor((balance/$conversion_factor)*60)", "balance" => 0, "billing_type" => 1, ),

  "ZeroBalanceBillingType2" => array("balance" => 0, "billing_type" => 2, ),

  "ChargeRemainderAndSetActivationDate" => array("block" => "=charge_remainder", "activation_date_time" => "getdate()", ),
// unused  "ChargeRemainder" => array("block" => "=charge_remainder", ),
  );
}

function get_transition_table()
{
  return array(
  "IndiaLD USA Monthly" => array(
    "cos_ids" => array( COSID_INDIALD_USA_MONTHLY_1000 => 1000, COSID_INDIALD_USA_MONTHLY_1500 => 1500, COSID_INDIALD_USA_MONTHLY_2000 => 2000, COSID_INDIALD_USA_MONTHLY_500 => 500, COSID_INDIALD_USA_MONTHLY_250 => 250 ),
    "IndiaLD USA Monthly"                 => array( "bifurcate" => "Immediate", "packaged_balance1" => "=delta_minutes", "target" => "=minutes", "block" => "=if,Immediate==charge_remainder" ),
    "PAYG Minutes T"      => array( "target" => COSID_PAYG_MINUTES_T, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    "Quarter Calls T"     => array( "target" => COSID_QUARTER_CALLS_T, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    "1.5 Calling Card T"  => array( "target" => COSID_1_5_CALLING_CARD_T, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    ),

  "IndiaLD Monthly" => array(
    "cos_ids" => array( COSID_INDIALD_MONTHLY_1000 => 1000, COSID_INDIALD_MONTHLY_2000 => 2000, COSID_INDIALD_MONTHLY_3000 => 3000, COSID_INDIALD_MONTHLY_500 => 500, COSID_INDIALD_MONTHLY_1500 => 1500, COSID_INDIALD_MONTHLY_250 => 250, COSID_INDIALD_MONTHLY_5000 => 5000 ),
    "IndiaLD Monthly" => array( "bifurcate" => "Immediate", "packaged_balance1" => "=delta_minutes", "target" => "=minutes", "block" => "=if,Immediate==charge_remainder" ),
    "PAYG Minutes"        => array( "target" => COSID_PAYG_MINUTES, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    "Quarter Calls"       => array( "target" => COSID_QUARTER_CALLS, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    "1.5 Calling Card"    => array( "target" => COSID_1_5_CALLING_CARD, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    ),

  "India LD Monthly" => array(
    "cos_ids" => array( COSID_INDIA_LD_MONTHLY_250 => 250, COSID_INDIA_LD_MONTHLY_500 => 500, COSID_INDIA_LD_MONTHLY_1000 => 1000, COSID_INDIA_LD_MONTHLY_1500 => 1500, COSID_INDIA_LD_MONTHLY_2000 => 2000, COSID_INDIA_LD_MONTHLY_3000 => 3000, COSID_INDIA_LD_MONTHLY_5000 => 5000 ),
    "India LD Monthly"    => array( "bifurcate" => "Immediate", "packaged_balance1" => "=delta_minutes", "target" => "=minutes", "block" => "=if,Immediate==charge_remainder" ),
    "PAYG Minutes T"      => array( "target" => COSID_PAYG_MINUTES_T, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    "Quarter Calls T"     => array( "target" => COSID_QUARTER_CALLS_T, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    "1.5 Calling Card T"  => array( "target" => COSID_1_5_CALLING_CARD_T, "blocks" => "ZeroMinutesBillingType1ZeroCreditLimit", ),
    ),

  "1.5 Calling Card" => array(
    "cos_ids" => array( COSID_1_5_CALLING_CARD => -1 ),
    "PAYG Minutes"           => array( "target" => COSID_PAYG_MINUTES, "blocks" => "RoundMinutesZeroBalanceBillingType1", "credit_limit" => 0, "packaged_balance1" => "=delta_minutes", ),
    "IndiaLD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes",),
    "Quarter Calls"          => array( "target" => COSID_QUARTER_CALLS ),
    ),

  "1.5 Calling Card T" => array(
    "cos_ids" => array( COSID_1_5_CALLING_CARD_T => -1),
    "India LD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes",),
    "IndiaLD USA Monthly"             => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes",),
    "PAYG Minutes T"      => array( "target" => COSID_PAYG_MINUTES_T, "blocks" => "RoundMinutesZeroBalanceBillingType1", ),
    "Quarter Calls T"     => array( "target" => COSID_QUARTER_CALLS_T ),
    ),

  "Quarter Calls" => array(
    "cos_ids" => array( COSID_QUARTER_CALLS => -1 ),
    "IndiaLD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", ),
    "PAYG Minutes"        => array( "target" => COSID_PAYG_MINUTES, "blocks" => "RoundMinutesZeroBalanceBillingType1", ),
    "1.5 Calling Card"    => array( "target" => COSID_1_5_CALLING_CARD ),
    ),

  "Quarter Calls T" => array(
    "cos_ids" => array( COSID_QUARTER_CALLS_T => -1 ),
    "India LD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", ),
    "IndiaLD USA Monthly"             => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes",),
    "PAYG Minutes T"        => array( "target" => COSID_PAYG_MINUTES_T, "blocks" => "RoundMinutesZeroBalanceBillingType1", ),
    "1.5 Calling Card T"    => array( "target" => COSID_1_5_CALLING_CARD_T ),
    ),

  "PAYG Minutes" => array(
    "cos_ids" => array( COSID_PAYG_MINUTES => -1 ),
    "IndiaLD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", ),
    "Quarter Calls"       => array( "target" => COSID_QUARTER_CALLS_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    "1.5 Calling Card"    => array( "target" => COSID_1_5_CALLING_CARD_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    ),

  "PAYG Minutes T" => array(
    "cos_ids" => array( COSID_PAYG_MINUTES_T => -1 ),
    "India LD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", ),
    "IndiaLD USA Monthly"             => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes",),
    "Quarter Calls T"       => array( "target" => COSID_QUARTER_CALLS_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    "1.5 Calling Card T"    => array( "target" => COSID_1_5_CALLING_CARD, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    ),

  /* no transitions out of UV card are allowed */
  "UV Card" => array(
    "cos_ids" => array( COSID_UV_CARD => -1 ),
    ),

  // special plan to manage users just signed up
  "No Plan" => array(
    "cos_ids" => array( COSID_NO_PLAN => -1 ),
    "No Plan" => array( "target" => COSID_NO_PLAN, "blocks" => array()),

    "UV Card"        => array( "target" => COSID_UV_CARD, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),

    "IndiaLD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", "billing_type" => 2 ),

    "Quarter Calls"       => array( "target" => COSID_QUARTER_CALLS_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    "1.5 Calling Card"    => array( "target" => COSID_1_5_CALLING_CARD_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    "PAYG Minutes"        => array( "target" => COSID_PAYG_MINUTES, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),

    "India LD Monthly" => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", "billing_type" => 2 ),
    "IndiaLD USA Monthly"             => array( "target" => "=minutes", "blocks" => array("ChargeRemainderAndSetActivationDate", "ZeroBalanceBillingType2", ), "packaged_balance1" => "=dest_minutes", "billing_type" => 2),

    "Quarter Calls T"       => array( "target" => COSID_QUARTER_CALLS_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    "1.5 Calling Card T"    => array( "target" => COSID_1_5_CALLING_CARD, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    "PAYG Minutes T"        => array( "target" => COSID_PAYG_MINUTES_T, "blocks" => "ZeroMinutesAndBalanceBillingType1", ),
    ),
  );
}

?>
