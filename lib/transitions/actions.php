<?php

// Code specifically related with 'actions'

function departure_actions_active( $plan_info )
{
  return array();
}

function arrival_actions_active( $plan_info , $plan_name )
{
  return array(
    make_action('active->maybe_set_ACTIVATION_DATE_TIME'),

    // send email for activation // TODO: this should not be here for Active to Active
    array("funcall"     => "send_activation_email",
          'fparams'     => array( "__customer_id__" ),
          'transaction' => "__customer_id__"
    )
  );
}

function arrival_actions_cancelled()
{
  return array(
    make_action('log_cancellation_transition_in_activation_history'),
    make_action('disable_account'),
    make_action('nullify_sim_and_msisdn')
  );
}

function actions_suspended_downgrade( $suspended , $to_suspended )
{
  return array(
    array('pfuncall' => 'funcSendExemptCustomerSMSPlanSuspendedDowngraded',
          'params'   => array(
            'customer_id' => '__customer_id__',
            'plan_cost'   => $to_suspended['info']['cost']
          )
    )
  );
}

function actions_suspended_upgrade( $suspended , $to_suspended )
{
  return array(
    array('pfuncall' => 'funcSendExemptCustomerSMSPlanSuspendedUpgraded',
          'params'   => array(
            'customer_id' => '__customer_id__',
            'plan_cost'   => $to_suspended['info']['cost']
          )
    )
  );
}

function actions_change_plan_on_renewal($active,$to_active,$to_plan_info)
{
  $aka_to      = $to_active['info']['aka'];

  return array(
    array('funcall' => 'mvneMakeitsoRenewPlan',
          'fparams' => array(
             '__customer_id__',
             $aka_to,
             '__this_transition__',
             '__this_action__'
          ),
    ),
    array('funcall' => "func_sweep_stored_value_explicit",
          'fparams' => array(
            "__customer_id__",
            "SPEND",
            "Monthly Fee",
          )
    ),
    array('funcall' => "func_spend_from_balance_explicit",
          'fparams' => array(
            "__customer_id__",
            ( $to_active['info']['cost'] / 100 ),
            "Plan Change Renewal",
            "Monthly Renewal into Plan $aka_to",
            "__this_transition__",
            "SPEND",
            "RENEWAL_PAYMENT",
            $to_active['info']['cos_id']
            )
    ),
    make_action('active->maybe_set_ACTIVATION_DATE_TIME'), #RIZ: shouldn't be necessary
    make_action('active->plan_started=X', 'plan_expires'),
    make_action('renewal->active.plan_expires', 'plan_started', 30),
    array('funcall' => "func_reset_zero_minutes",
          'fparams' => array(
            "__customer_id__",
            $to_active['info']['cos_id']
          )
    ),
    array('funcall' => 'func_htt_plan_tracker_plan_renewal',
          'fparams' => array( '__customer_id__' )
    ),
    array('funcall' => "func_reset_monthly_renewal_target",
          'fparams' => array(
            "__customer_id__"
          )
    ),
    array('funcall' => "func_set_minutes",
          'fparams' => array(
            "__customer_id__",
            $to_plan_info['intl_minutes'],
            $to_plan_info['unlimited_intl_minutes']
          )
    ),
    array('pfuncall' => "funcSendExemptCustomerSMSMonthlyPlanChange",
          'params'   => array(
            'customer_id' => "__customer_id__",
            'plan_to'     => $aka_to
          )
    ),
    make_action('process_recurring_bolt_ons', $aka_to)
  );
}

function action_midcycle_packaged_balance_check()
{
  return
    array('funcall' => 'internal_func_midcycle_packaged_balance_check',
          'fparams' => array(
            '__customer_id__'
          )
    );
}

function actions_change_plan_midcycle($delta_cost,$active,$to_active,$to_plan_info)
{
  $aka_from = $active['info']['aka'];
  $aka_to   = $to_active['info']['aka'];

  $actions = array(
    array('funcall' => 'func_sweep_stored_value_explicit',
          'fparams' => array(
            '__customer_id__',
            'SPEND',
            'PLAN UPGRADE'
          ) ,
    ),
    array('funcall' => 'func_spend_from_balance_explicit',
          'fparams' => array(
            '__customer_id__',
            $delta_cost,
            "Change Plan to $aka_to",
            "Change Plan to $aka_to",
            'DELTA_PAYMENT',
            'SPEND',
            '__this_transition__',
            $to_active['info']['cos_id']
          ),
    ),
    array('funcall' => 'set_delta_minutes',
          'fparams' => array('__customer_id__',
            0,#$to_plan_info['talk_minutes'],
            $to_plan_info['intl_minutes']),
            'transaction' => '__customer_id__'
    ),
    array('funcall' => 'func_reset_monthly_renewal_target',
          'fparams' => array(
            '__customer_id__'
          )
    ),
    array('funcall' => 'func_htt_plan_tracker_plan_change',
          'fparams' => array(
            '__customer_id__',
            get_cos_id_from_plan( $aka_to )
          )
    ));

  // MVNO-2310,DOP-111: conditional action on [L19,L24] -> L[2345]9 plan change only
  if (in_array($active['plan'], array('NINETEEN','TWENTY_FOUR', 'THIRTY_FOUR', 'FORTY_FOUR')))
  {
    $actions[] = array(
      'funcall' => 'func_reset_zero_minutes',
      'fparams' => array(
        '__customer_id__',
        $to_active['info']['cos_id'])
    );
  }

  if (in_array($aka_to, array('L34', 'L44')))
  {
    $actions[] = array(
      'funcall' => 'func_set_minutes',
      'fparams' => array(
        '__customer_id__',
        $to_active['info']['intl_minutes'],
        $to_active['info']['unlimited_intl_minutes']
      )
    );
  }

  // remaining unconditional actions
  $actions[] = array('pfuncall' => 'funcSendExemptCustomerSMSImmediatePlanChange',
        'params'   => array(
          'customer_id' => '__customer_id__',
          'plan_from'   => $aka_from,
          'plan_to'     => $aka_to
        )
  );

  $actions[] = array('funcall' => 'mvneChangePlanImmediate',
        'fparams' => array(
          '__customer_id__',
          $aka_to,
          '__this_transition__',
          '__this_action__'
        )
  );

  return $actions;
}

function actions_list( $active , $plan_info )
{
  $ultra_plans = ultra_plans();

  $actions_list = array(


    'port_add_to_htt_ultra_msisdn' => array(
      array( 'funcall'  => 'port_add_to_htt_ultra_msisdn',
             'fparams'  => array(
               '__customer_id__',
               '__this_transition__',
             )
      )
    ),


    'post_portindenied_actions' => array(
      make_action('log_transition_in_activation_history'),
      array(
        'funcall' => 'refundWebPosCustomer',
        'fparams' => array('__customer_id__')),
      array(
        'funcall' => 'send_portin_failure_email',
        'fparams' => array('__customer_id__'))
    ),


    'ship_sim_actions' => array(
      make_action('log_transition_in_activation_history'),
      array(
        'funcall' => 'inventory_individual_ship_sim',
        'fparams' => array(
          '__customer_id__',
          '__this_transition__'
        )
      )
    ),


    'provisioned_to_cancelled_actions' => array(
      make_action('mvneEnsureCancel'),
      make_action('deactivate_ultra_msisdn'),
      make_action('delete_account_aliases_by_msisdn'),
      make_action('delete_account_aliases_by_account_id'),
      make_action('disable_cc_holders_entries'),
    ),


    'active_to_cancelled_actions' => array(
      make_action('mvneEnsureCancel'),
      make_action('deactivate_ultra_msisdn'), // Expire Phone Number
      make_action('delete_account_aliases_by_msisdn'), // Expire account
      make_action('delete_account_aliases_by_account_id'), // Expire account
      make_action('expire_sim_card'),
      make_action('disable_cc_holders_entries'),
      array('funcall' => 'destination_origin_map_cancellation_by_customer_id',
            'fparams' => array( '__customer_id__' )
      )
    ),


    'orange_activation_actions' => array(
      array('funcall' => 'func_validate_orange_sim',
            'fparams' => array(
              '__customer_id__'
            )
      ),
      array( 'funcall' => 'funcMultiMonthEnrollment',
             'fparams' => array(
               '__customer_id__',
               '__customer:ICCID__'
            )
      ),
      make_action('log_new_activation_in_activation_history'),
      make_action('save_gross_add_date')
    ),


    // Monthly Renewal same plan
    'monthly_renewal_actions' => array(
      array('funcall' => 'mvneMakeitsoRenewPlan',
            'fparams' => array(
               '__customer_id__',
               $active['info']['aka'],
               '__this_transition__',
               '__this_action__'
            ),
      ),
      array('funcall' => 'func_sweep_stored_value_explicit',
            'fparams' => array(
              '__customer_id__',
              'SPEND',
              'Monthly Fee',
            )
      ),
      array('funcall' => 'func_spend_from_balance_explicit',
            'fparams' => array(
              '__customer_id__',
              ( $active['info']['cost'] / 100 ),
              'Monthly Renewal',
              'Monthly Renewal',
              '__this_transition__',
              'SPEND',
              'RENEWAL_PAYMENT'
            )
      ),
      make_action('empty_minutes'),
      make_action('active->maybe_set_ACTIVATION_DATE_TIME'),
      make_action('active->plan_started=X', 'plan_expires'),
      make_action('renewal->active.plan_expires', 'plan_started', 30),
      array('funcall' => "func_reset_zero_minutes",
            'fparams' => array(
              "__customer_id__",
              $active['info']['cos_id']
            )
      ),
      array('funcall' => 'func_htt_plan_tracker_plan_renewal',
            'fparams' => array( '__customer_id__' )
      ),
      array('funcall' => 'func_reset_monthly_renewal_target',
            'fparams' => array(
              '__customer_id__'
            ),
      ),
      array('funcall' => 'func_set_minutes',
            'fparams' => array(
              '__customer_id__',
              $plan_info['intl_minutes'],
              $plan_info['unlimited_intl_minutes']
            )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSMonthlyPlanRenewal',
            'params'   => array(
              'customer_id' => '__customer_id__'
            )
      ),
      make_action('process_recurring_bolt_ons', $active['info']['aka']),
      array( 'funcall' => 'funcCheckFinalMultiMonth',
             'fparams' => array(
               '__customer_id__'
            )
      )
    ),


    'suspended_to_active_actions' => array(
      array('funcall' => 'func_sweep_stored_value_explicit',
            'fparams' => array(
              '__customer_id__',
              'SPEND',
              'REACTIVATION_PAYMENT'
            )
      ),
      array('funcall' => 'func_spend_from_balance_explicit',
            'fparams' => array(
              '__customer_id__',
              ( $active['info']['cost'] / 100 ),
              'Reactivation',
              'Reactivation',
              'REACTIVATION_PAYMENT',
              'SPEND',
              '__this_transition__'
              )
      ),
      array('funcall' => 'mvneMakeitsoRenewPlan',
            'fparams' => array(
               '__customer_id__',
               $active['info']['aka'],
               '__this_transition__',
               '__this_action__'
            ),
      ),
      make_action('active->plan_started=X', 'GETUTCDATE()'),
      make_action('renewal->active.plan_expires', 'plan_started', 30),
      array('funcall' => "func_reset_zero_minutes",
            'fparams' => array(
              "__customer_id__",
              $active['info']['cos_id']
            )
      ),
      array('funcall' => 'func_set_minutes',
            'fparams' => array(
              '__customer_id__',
              $plan_info['intl_minutes'],
              $plan_info['unlimited_intl_minutes']
            )
      ),
      array('funcall' => 'func_htt_plan_tracker_plan_renewal',
            'fparams' => array(
              '__customer_id__',
              $active['info']['cos_id']
            )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSUnsuspend',
            'params'   => array( 'customer_id' => '__customer_id__' )
      ),
      make_action('process_recurring_bolt_ons', $active['info']['aka'])
    ),

    'intra_brand_cancellation_actions' => array(
      make_action('deactivate_ultra_msisdn'), // Expire Phone Number
      make_action('delete_account_aliases_by_msisdn'), // Expire account
      make_action('delete_account_aliases_by_account_id'), // Expire account
      make_action('expire_sim_card'),
      make_action('disable_cc_holders_entries'),
      array('funcall' => 'destination_origin_map_cancellation_by_customer_id',
            'fparams' => array( '__customer_id__' )
      )
    ),

    'suspended_to_cancelled_actions' => array(
      make_action('mvneEnsureCancel'),
      make_action('deactivate_ultra_msisdn'), // Expire Phone Number
      make_action('delete_account_aliases_by_msisdn'), // Expire account
      make_action('delete_account_aliases_by_account_id'), // Expire account
      make_action('expire_sim_card'),
      make_action('disable_cc_holders_entries'),
      array('funcall' => 'destination_origin_map_cancellation_by_customer_id',
            'fparams' => array( '__customer_id__' )
      )
    ),


    'promo_activation_actions' => array(
      make_action('log_transition_in_activation_history'),
      make_action('log_new_activation_in_activation_history'),
      array('funcall' => 'func_sweep_stored_value_explicit',
            'fparams' => array(
              '__customer_id__',
              'SPEND',
              'Monthly Fee',
            )
      ),
      array('funcall' => 'func_spend_from_balance_explicit',
            'fparams' => array(
              '__customer_id__',
              ( $active['info']['cost'] / 100 ),
              'Reactivation',
              'Reactivation',
              'REACTIVATION_PAYMENT',
              'SPEND',
              '__this_transition__'
              )
      ),
      array('funcall' => 'mvneMakeitsoRenewPlan',
            'fparams' => array(
               '__customer_id__',
               $active['info']['aka'],
               '__this_transition__',
               '__this_action__'
            ),
      ),
      make_action('active->plan_started=X', 'GETUTCDATE()'),
      make_action('initial->active.plan_expires', 'plan_started', 30),
      array('funcall' => "func_reset_zero_minutes",
            'fparams' => array(
              "__customer_id__",
              $active['info']['cos_id']
            )
      ),
      make_action('log_plan_started_in_activation_history'),
      make_action('save_gross_add_date'),
      make_action('sleep', 5),
      array('funcall' => 'func_htt_plan_tracker_first_activation',
            'fparams' => array(
              '__customer_id__',
              get_cos_id_from_plan( $active['plan'] )
            )
      ),
      array('funcall' => 'func_set_minutes',
            'fparams' => array(
              '__customer_id__',
              $active['info']['intl_minutes'],
              $active['info']['unlimited_intl_minutes']
            )
      ),
      array('funcall' => 'ultra_promotional_plans_post_activation',
            'fparams' => array(
              '__customer_id__'
            )
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSActivationNewPhone',
             'params'   => array(
               'customer_id' => '__customer_id__'
             )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSActivateHelp',
            'params'   => array(
              'customer_id' => '__customer_id__'
            )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSActivateDataHelp',
            'params'   => array(
              'customer_id' => '__customer_id__'
            )
      )
    ),


    'provisioned_to_active_actions' => array(
      make_action('log_transition_in_activation_history'),
      array('funcall' => 'func_sweep_stored_value_explicit',
            'fparams' => array(
              '__customer_id__',
              'SPEND',
              'Monthly Fee',
            )
      ),
      array('funcall' => 'func_spend_from_balance_explicit',
            'fparams' => array(
              '__customer_id__',
              ( $active['info']['cost'] / 100 ),
              'Activation',
              'Provisioned Activation',
              'REACTIVATION_PAYMENT',
              'SPEND',
              '__this_transition__'
              )
      ),
      array('funcall' => 'mvneMakeitsoRenewPlan',
            'fparams' => array(
               '__customer_id__',
               $active['info']['aka'],
               '__this_transition__',
               '__this_action__'
            ),
      ),
      make_action('active->plan_started=X', 'GETUTCDATE()'),
      make_action('initial->active.plan_expires', 'plan_started', 30),
      array('funcall' => "func_reset_zero_minutes",
            'fparams' => array(
              "__customer_id__",
              $active['info']['cos_id']
            )
      ),
      make_action('log_plan_started_in_activation_history'),
      make_action('save_gross_add_date'),
      make_action('sleep', 5),
      array('funcall' => 'func_htt_plan_tracker_first_activation',
            'fparams' => array(
              '__customer_id__',
              get_cos_id_from_plan( $active['plan'] )
            )
      ),
      array( 'funcall' => 'funcBogoEnrollment',
             'fparams' => array(
               '__customer_id__',
               '__customer:ICCID__',
               $active['info']['cos_id'] )
      ),
      array('funcall' => 'func_set_minutes',
            'fparams' => array(
              '__customer_id__',
              $active['info']['intl_minutes'],
              $active['info']['unlimited_intl_minutes']
            )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSActivateHelp',
            'params'   => array(
              'customer_id' => '__customer_id__'
            )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSActivatePlanInfo',
            'params'   => array(
              'customer_id' => '__customer_id__',
              'cos_id'      => $active['info']['cos_id']
            )
      ),
      array('pfuncall' => 'funcSendExemptCustomerSMSActivateDataHelp',
            'params'   => array(
              'customer_id' => '__customer_id__'
            )
      ),
      make_action('process_recurring_bolt_ons', $active['info']['aka'])
    ),


    'post_promo_prepare_actions' => array(
      array( 'funcall' => 'internal_func_delayed_provisioning_suspend',
             'fparams' => array(
               '__customer_id__'
             )
      ),
      // record_ICCID_activation ; update ACTIVATION_ICCID ; reserve iccid
      array( 'funcall' => 'sim_activation_handler',
             'fparams' => array(
               '__customer_id__',
               '__this_transition__',
               $plan_info['cos_id']
             )
      ),
      // insert into ACCOUNT_ALIASES ; insert into DESTINATION_ORIGIN_MAP
      array('funcall' => 'msisdn_activation_handler',
            'fparams' => array('__customer_id__')
      ),
    ),


    'post_provisioning_actions' => array(
      array( 'funcall' => 'internal_func_delayed_provisioning_suspend',
             'fparams' => array(
               '__customer_id__'
             )
      ),
      // record_ICCID_activation ; update ACTIVATION_ICCID ; reserve iccid
      array( 'funcall' => 'sim_activation_handler',
             'fparams' => array(
               '__customer_id__',
               '__this_transition__',
               $plan_info['cos_id']
             )
      ),
      // insert into ACCOUNT_ALIASES ; insert into DESTINATION_ORIGIN_MAP
      array('funcall' => 'msisdn_activation_handler',
            'fparams' => array('__customer_id__')
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSProvisionedReminder',
             'params' => array(
               'customer_id'   => '__customer_id__',
               'cos_id'        => $active['info']['cos_id']
             )
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSActivationNewPhone',
             'params'   => array(
               'customer_id' => '__customer_id__'
             )
      )
    ),


    'post_activation_actions' => array(
      array( 'funcall' => 'func_sweep_stored_value_explicit',
             'fparams' => array(
               '__customer_id__',
               'SPEND',
               'Monthly Fee'
             )
      ),
      array( 'funcall' => 'func_spend_from_balance_explicit',
             'fparams' => array(
               '__customer_id__',
               ( $active['info']['cost'] / 100 ),
               'First Monthly Fee',
               'Provisioned Activation',
               'ACTIVATION_PAYMENT',
               'SPEND',
               '__this_transition__',
               $active['info']['cos_id']
             )
      ),
      make_action('active->plan_started=X', 'GETUTCDATE()'),
      make_action('initial->active.plan_expires', 'plan_started', 30),
      make_action('log_plan_started_in_activation_history'),
      make_action('sleep', 5),
      array( 'funcall' => 'func_htt_plan_tracker_first_activation',
             'fparams' => array(
               '__customer_id__',
               get_cos_id_from_plan( $active['plan'] )
             )
      ),
      // record_ICCID_activation ; update ACTIVATION_ICCID ; reserve iccid
      array( 'funcall' => 'sim_activation_handler',
             'fparams' => array(
               '__customer_id__',
               '__this_transition__',
               $plan_info['cos_id']
             )
      ),
      // insert into ACCOUNT_ALIASES ; insert into DESTINATION_ORIGIN_MAP
      array('funcall' => 'msisdn_activation_handler',
            'fparams' => array('__customer_id__')
      ),
      array( 'funcall' => 'func_set_minutes',
             'fparams' => array(
               '__customer_id__',
               $plan_info['intl_minutes'],
               $plan_info['unlimited_intl_minutes']
             )
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSActivationNewPhone',
             'params'   => array(
               'customer_id' => '__customer_id__'
             )
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSActivateHelp',
             'params'   => array(
               'customer_id' => '__customer_id__'
             )
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSActivatePlanInfo',
             'params'   => array(
               'customer_id' => '__customer_id__',
               'cos_id'      => $active['info']['cos_id']
             )
      ),
      array( 'pfuncall' => 'funcSendExemptCustomerSMSActivateDataHelp',
             'params'   => array(
               'customer_id' => '__customer_id__'
             )
      ),
      array('funcall' => 'online_auto_enroll',
        'fparams' => array(
          '__customer_id__',
          '__customer:ICCID__'
        )
      )
    )


  );

  $actions_list['provisioning_actions'] = array();

  // $plan can be [ STANDBY , NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE ]
  foreach($ultra_plans as $plan => $plan_config)
  {
    if ( $plan != 'STANDBY' )
    {
      $actions_list['provisioning_actions'][ $plan ] = array_merge(
        array(
          make_action('log_transition_in_activation_history'),
          make_action('log_new_activation_in_activation_history'),
          make_action('mvne_provision', $plan ),
          make_action('sleep', 5)
        ),
        $actions_list['post_provisioning_actions']
      );

      $actions_list['promo_prepare_actions'][ $plan ] = array_merge(
        array(
          make_action('log_transition_in_activation_history'),
          make_action('log_new_activation_in_activation_history'),
          make_action('mvne_activate', $plan ),
          make_action('sleep', 5)
        ),
        $actions_list['post_promo_prepare_actions']
      );
    }
  }

  $actions_list['port_activated_actions'] = array_merge(
    array(
      make_action('log_transition_in_activation_history'),
      make_action('save_gross_add_date')
    ),
    $actions_list['post_activation_actions'],
    array(
      array( 'funcall'  => 'mvneMakeitsoRenewPlan',
             'fparams' => array(
               '__customer_id__',
               $active['info']['aka'],
               '__this_transition__',
               '__this_action__'
            )
      ),
      array( 'funcall'  => 'send_portin_success_email',
             'fparams'  => array( '__customer_id__' )
      ),
      make_action('process_recurring_bolt_ons', $active['info']['aka']),
      array( 'funcall' => 'funcMultiMonthEnrollment',
             'fparams' => array(
               '__customer_id__',
               '__customer:ICCID__'
            )
      ),
      array('funcall' => 'online_auto_enroll',
            'fparams' => array(
              '__customer_id__',
              '__customer:ICCID__'
            )
      )
    )
  );

  $actions_list['activation_actions'] = array();

  // $plan can be [ STANDBY , NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE ]
  foreach($ultra_plans as $plan => $plan_config)
  {
    if ( $plan != 'STANDBY' )
    {
      $actions_list['activation_actions'][ $plan ] = array_merge(
        array(
          make_action('log_transition_in_activation_history'),
          make_action('mvne_activate', $plan ),
          make_action('sleep', 10)
        ),
        $actions_list['post_activation_actions']
      );
    }
  }

  return $actions_list;
}

function active_to_suspended_actions( $active )
{
  return array(
    array( 'pfuncall' => 'funcSendExemptCustomerSMSDisabling',
           'params'   => array(
             'customer_id' => '__customer_id__'
           )
    ),
    make_action('mvneEnsureSuspend'),
    array('funcall' => 'func_reset_monthly_renewal_target', 'fparams' => array('__customer_id__')),
    make_action('empty_minutes')
  );
}

?>
