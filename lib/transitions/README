  HOWTO

  1. State machine knowledge

  internal__States returns:
    "plans" => ultra_plans(),
    "states" => ultra_states(),
    "transitions" => ultra_transitions(),

  Plans are an array; ultra_plans returns a KV array with the plan
  name as the key.  For example (in JSON):

  "FORTY_NINE":
  {
    "has_calling_card_balance":"0",
    "states":["Active","Provisioned","Suspended",...],
    "plan_group":"Ultra",
    "cost":"4900",
    "intl_minutes":"2000",
    "talk_minutes":"inf",
    "monthly":"1",
    "rechargeable":"0",
    "name":"Ultra $49",
    "taxable":"1",
    "cos_id":"98276"
  }

  This data, except for the state list, is coming from cfengine (ht.cf).

  States are a list of arrays with a plan and a specific state name,
  and you can get them with ultra_states().  Here's an example of the
  Suspended state for the FORTY_NINE plan, omitting the plan info
  already shown above, in JSON:

  {
    "plan":"FORTY_NINE", # plan name
    "info": [plan info from ultra_plans()], # full plan
    "name":"state_FORTY_NINE_Suspended",
    "print":"Ultra $49/Suspended", # printable version of the state
    "state":"Suspended",
    "extract":[]
  }

  States can have arrival_actions and departure_actions, whose format
  is same as transition actions which see below.

  Finally, transitions use the states above as source and destination,
  adding "label" and "desc" to describe the transition specifics, and
  defining departure and arrival actions:

  {
    "source": [state info from ultra_plans()],
    "dest": [state info from ultra_plans()],
    "requires":{"whole_value_min":49},
    "label":"Fund Port-In Request",
    "desc":"Port initiated by CustomerService or Dealer Portal, and is now being funded.",
    "actions":["(INCOMPLETE) actions for beginning a port."],
    "todo":["Verify valid ICCID for porting.","Verify (INCOMPLETE) actions for IsEligiblePortIn."],
  }

  JSON actions have the format:

  a) a string, this is just a TODO item
  b) an array (funcall => function name, fparams => [ array of function parameters ])
  c) an array (pfuncall => function name, params => { key-val array of command parameters })
  d) an array (command => command name, params => { key-val array of command parameters })
  e) an array (async_command => command name, params => { key-val array of async command's parameters })
  f) an array (sql => "SQL statement using sprintf formats", fparams => [ sprintf parameters ])


  2. Changing customer state: the internal__ChangeState command and
  the change_state() function.  Takes parameters customer_id,
  resolve_now (if true, the transition will be resolved now as far as
  possible), and desired plan and state_name.  The list of new
  transitions is returned in the 'transitions' key of the return, but
  this is not promised by the return definition.

  internal__ChangeState and change_state take an optional dry_run
  parameter that, when TRUE, will just return the path to the desired
  state but not actually create the transitions or try to resolve
  them.  They also take an optional max_path_depth parameter (default
  to 99) to limit the number of steps in a path; with 1 this
  effectively limits any transitions to a single state change.

  3.  Updating the result of an action: internal__UpdateActionResult
  and update_action_result.  Takes an action ID, a customer ID, a
  status (CLOSED or ABORTED), the action results as a JSON string, and
  resolve_now that tells the command and function whether they should
  try to resolve the action's parent transition now.

  4. Appending a new action: internal__AppendAction and append_action.
  Take all the information necessary to create a new action.  It can
  be in an existing transition or in a new one, if the transition_id
  is not given.  Also takes a boolean "pending" which is true if the
  status should be PENDING (meaning the action has been run and we
  wait for a result), and false if it should be OPEN (meaning someone
  has to pick up the action and run it).  Calls log_action() behind
  the scenes.

  5. Misc. accessors, already discussed in MVNO-326 and so on:
 internal__GetState/internal_func_get_state/load_state,
 internal__CheckTransition/internal_func_check_transition,
 internal__GetAllPendingTransitions/internal_func_get_all_pending_transitions,
 internal__GetPendingTransitions/internal_func_get_pending_transitions


=====================================================================
A documentation of how a transition is added ( funtion change_state )
=====================================================================

How to test : php test/test_transitions.php change_state Suspended L29 2147 'NOT_A_DRY_RUN'

 BEGIN TRAN
 INSERT INTO htt_transition_log ( CUSTOMER_ID, TRANSITION_UUID, FROM_COS_ID, FROM_PLAN_STATE, TO_COS_ID, TO_PLAN_STATE, CONTEXT, HTT_ENVIRONMENT , TRANSITION_LABEL) VALUES ( 2147, '{TX-545950C8FCC66771-280A9FFAD7A064CB}', 98274, 'Active', 98274, 'Suspended', 0x5b7b22637573746f6d65725f6964223a2232313437227d5d, 'rgalli3_dev', 'Suspend' );
 INSERT INTO htt_action_log ( ACTION_UUID, TRANSITION_UUID, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, STATUS, RETRY, ERROR_OK, ERROR_COND ) VALUES ( '{AX-1C35C8BF1702A6E7-9D6CD098FD679F44}', '{TX-545950C8FCC66771-280A9FFAD7A064CB}', COALESCE(1+(SELECT MAX(action_seq) FROM htt_action_log WHERE transition_uuid = '{TX-545950C8FCC66771-280A9FFAD7A064CB}'), 0), 'funcall', 'test', 'OPEN', 0x5b5b5d5d, 0, 0x5b6e756c6c5d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-1C35C8BF1702A6E7-9D6CD098FD679F44}', '0', 0x5b225f5f637573746f6d65723a4d534953444e5f5f225d );
 INSERT INTO htt_action_log ( ACTION_UUID, TRANSITION_UUID, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, STATUS, RETRY, ERROR_OK, ERROR_COND ) VALUES ( '{AX-CE2CF87805AD9C82-BAA49BD9B2A45E97}', '{TX-545950C8FCC66771-280A9FFAD7A064CB}', 100, 'pfuncall', 'funcSendExemptCustomerSMSDisabling', 'OPEN', 0x5b5b5d5d, 0, 0x5b6e756c6c5d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-CE2CF87805AD9C82-BAA49BD9B2A45E97}', 'customer_id', 0x5b225f5f637573746f6d65725f69645f5f225d );
 INSERT INTO htt_action_log ( ACTION_UUID, TRANSITION_UUID, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, STATUS, RETRY, ERROR_OK, ERROR_COND ) VALUES ( '{AX-592C274619B9D944-432EAAEF3924C945}', '{TX-545950C8FCC66771-280A9FFAD7A064CB}', 101, 'funcall', 'mvne_ServiceRemove', 'OPEN', 0x5b5b5d5d, 0, 0x5b6e756c6c5d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-592C274619B9D944-432EAAEF3924C945}', '0', 0x5b225f5f637573746f6d65723a4d534953444e5f5f225d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-592C274619B9D944-432EAAEF3924C945}', '1', 0x5b22554c54554e4c4d54225d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-592C274619B9D944-432EAAEF3924C945}', '2', 0x5b225f5f746869735f7472616e736974696f6e5f5f225d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-592C274619B9D944-432EAAEF3924C945}', '3', 0x5b225f5f746869735f616374696f6e5f5f225d );
 INSERT INTO htt_action_log ( ACTION_UUID, TRANSITION_UUID, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, STATUS, RETRY, ERROR_OK, ERROR_COND ) VALUES ( '{AX-815AF04F65AEA337-FA0D6D39BD6632B6}', '{TX-545950C8FCC66771-280A9FFAD7A064CB}', 102, 'funcall', 'mvne_EnsureSuspendMSISDN', 'OPEN', 0x5b5b5d5d, 0, 0x5b6e756c6c5d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-815AF04F65AEA337-FA0D6D39BD6632B6}', '0', 0x5b225f5f637573746f6d65723a4d534953444e5f5f225d );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-815AF04F65AEA337-FA0D6D39BD6632B6}', '1', 0x5b225f5f637573746f6d65723a49434349445f5f225d );
 INSERT INTO htt_action_log ( ACTION_UUID, TRANSITION_UUID, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, STATUS, RETRY, ERROR_OK, ERROR_COND , ACTION_SQL ) VALUES ( '{AX-709B63AC881EDBE2-57F4C112F7DA2C2D}', '{TX-545950C8FCC66771-280A9FFAD7A064CB}', 103, 'sql', 'sql', 'OPEN', 0x5b5b5d5d, 0, 0x5b6e756c6c5d , 0x555044415445204143434f554e545320534554207061636b616765645f62616c616e636531203d203020574845524520435553544f4d45525f4944203d202564 );
 INSERT INTO htt_action_parameter_log ( ACTION_UUID, PARAM, VAL ) VALUES ( '{AX-709B63AC881EDBE2-57F4C112F7DA2C2D}', '0', 0x5b225f5f637573746f6d65725f69645f5f225d );
 COMMIT



======================================================================================
A documentation of function run_db_transitions ( internal__ResolvePendingTransitions )
*OBSOLETE* see http://wiki.hometowntelecom.com:8090/display/SPEC/How+it+works+-+Transition+Log
======================================================================================


1) get_next_transition

SELECT TOP 1 * 
 FROM htt_transition_log 
 WHERE status = 'OPEN' 
 AND HTT_ENVIRONMENT = 'riz_dev_live' 
 AND (
    SELECT count(action_uuid) 
    FROM htt_action_log a 
    WHERE htt_transition_log.transition_uuid = a.transition_uuid 
    AND (a.status = 'PENDING' OR a.status LIKE 'RUNNING%')
 ) = 0
 AND DATEDIFF(ss, htt_transition_log.created, GETUTCDATE()) > 2 
 ORDER BY created

2)

a) mark htt_action_log row as running (reserve a row of the first available action which belong to our transaction)

UPDATE htt_action_log
SET status = 'RUNNING {C01C1F69-72B2-D1D6-7AA4-AA4AACAB5E18} 1358369732',
PENDING_SINCE = getutcdate()
WHERE action_uuid = (
 SELECT TOP 1 action_uuid
 FROM htt_action_log
 WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
 AND status = 'OPEN'
 ORDER BY action_seq
)

b) if a) fails

UPDATE htt_transition_log
SET status = 'ABORTED'  
WHERE transition_uuid = ‘{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}’


3) get the reserved htt_action_log row ( get_next_action )

SELECT TOP 1 *
FROM htt_action_log
WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
AND status = 'RUNNING {C01C1F69-72B2-D1D6-7AA4-AA4AACAB5E18} 1358369732'
ORDER BY action_seq


4) run_or_enqueue_db_action (inside a try-catch)

4a) get action’s parameters

 SELECT *
 FROM htt_action_parameter_log
 WHERE action_uuid = '{E1BD1F2C-587B-71D5-C9D7-506EED8E960C}'
 ORDER BY param

4b) get customer’s data if we have a customer id

 SELECT *, RIGHT(c.CC_NUMBER, 4) AS CC_LAST_4, DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch, DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch
 FROM customers c, accounts a, parent_cos pc, htt_customers_overlay_ultra u
 WHERE c.customer_id = a.customer_id
 AND u.customer_id = c.customer_id
 AND a.cos_id=pc.cos_id
 AND u.customer_id = 36735

4c) perform the action. Action types: [TODO|funcall|pfuncall|command|async_command|sql]


5) update action status

 UPDATE htt_action_log
 SET status = 'CLOSED', CLOSED = GETUTCDATE()
 WHERE action_uuid = '{E1BD1F2C-587B-71D5-C9D7-506EED8E960C}'


6) update action result if ($result_type == 'result')

 UPDATE htt_action_log
 SET action_result = 0xsomething
 WHERE action_uuid = '{E1BD1F2C-587B-71D5-C9D7-506EED8E960C}'


7) add row into htt_action_result_log if ($result_type != 'result')

 INSERT INTO htt_action_result_log
 ( ACTION_UUID, PARAM, VAL )
 VALUES ( '{96EDCED4-5390-7F21-C5ED-82752D3C44CF}', 'async_result', 0xsomething);


8) if the action was aborted, abort the next OPEN actions

 UPDATE htt_action_log
 SET status = 'ABORTED', CLOSED = GETUTCDATE()
 WHERE transition_uuid = 'TEST'
 AND status = 'OPEN'


9) cleanup_transition

 9a) get data from this transition

 SELECT TOP 1 *
 FROM htt_transition_log
 WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'

 9b) abort the transition if needed

 UPDATE htt_transition_log
 SET status = 'ABORTED', closed = GETUTCDATE()
 WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
 AND status = 'OPEN'
 AND 0 = (
   SELECT COUNT(action_uuid)
   FROM htt_action_log
   WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
   AND status <> 'CLOSED'
   AND status <> 'ABORTED'
 )
 AND 0 < (
   SELECT COUNT(action_uuid)
   FROM htt_action_log
   WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
   AND status = 'ABORTED'
 )

 9c) close the transition if needed

 UPDATE htt_transition_log
 SET status = 'CLOSED', closed = GETUTCDATE()
 WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
 AND status = 'OPEN'
 AND 0 = (
   SELECT COUNT(action_uuid)
   FROM htt_action_log
   WHERE transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
   AND status <> 'CLOSED'
 )

 9d) abort OPEN actions if we have an aborted ACTION for this transition

 UPDATE htt_action_log
 SET status = 'ABORTED', closed = GETUTCDATE()
 WHERE transition_uuid IN (
   SELECT transition_uuid
   FROM htt_transition_log
   WHERE status = 'ABORTED'
   AND transition_uuid = '{41ABE8D5-AC68-CBCB-3CAF-07ED6137D11D}'
 )
 AND status = 'OPEN'

 9e) update cos_id and plan_state for customer if needed

 BEGIN TRAN
 UPDATE accounts SET cos_id = 2 WHERE CUSTOMER_ID = 36735
 UPDATE htt_customers_overlay_ultra SET plan_state = 'Provisioned' WHERE CUSTOMER_ID = 36735
 COMMIT

