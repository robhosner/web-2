<?php

/*

AMDOCS-469

Given a file name, process all MSISDNS in the file.

 - send the given template according to the customer's language
 - save in Redis that we attempted the delivery.
 - check queue length and sleep if it's above 150

Usage:
 php lib/messaging/bulk_send.php $FILENAME $TEMPLATE $LANG $CYCLES $SHUFFLING

Example:
 php lib/messaging/bulk_send.php /home/rgalli/test_EN.txt julyplanrefresh EN 1 0

*/


require_once 'db.php';
require_once 'lib/messaging/functions.php';
require_once 'classes/postageapp.inc.php';
require_once 'Ultra/Lib/MQ/ControlChannel.php';
require_once 'classes/Messenger.php';

$file_name    = $argv[1];
$sms_template = $argv[2];
$language     = $argv[3];
$cycles       = $argv[4];
$shuffling    = $argv[5];

dlog('',"file_name = $file_name ; sms_template = $sms_template ; language = $language ; cycles = $cycles");

$list = file($file_name);

if ( $shuffling )
  shuffle($list);

$count = 0;

$sleep_seconds = 1;

teldata_change_db();

$SMS_templates = \Ultra\Messaging\Templates\SMS_templates( '2' );

if ( ! isset( $SMS_templates[ $sms_template . '__' . $language ] ) )
  die("Template does not exist");

$message = $SMS_templates[ $sms_template . '__' . $language ];

dlog('',"message template = $sms_template" . '__' . $language);
dlog('',"message = $message");

$controlChannel = new \Ultra\Lib\MQ\ControlChannel;

foreach ( $list as $msisdn )
{
  delay_due_to_queue_size( $controlChannel );

  $msisdn = trim($msisdn);
  $msisdn = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $msisdn);

  if ( $msisdn && ( $count < $cycles ) )
  {
    dlog('',"msisdn = $msisdn");

    #$customer_id = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'customer_id');

    #if ( $customer_id )

    $sql = htt_customers_overlay_ultra_select_query(
      array(
        'select_fields'           => array('customer_id','current_mobile_number'),
        'current_mobile_number'   => $msisdn
      )
    );

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( $data && is_array($data) && count($data) )
    {
      $customer    = $data[0];
      $customer_id = $data[0]->customer_id;

      if ( verify_sms_delivery( $customer_id , $sms_template , 'BULK_SMS' ) )
      {
        dlog('',"message already sent to $msisdn");
      }
      else
      {
        $count++;

        #$customer = new stdClass();
        #$customer->current_mobile_number = $msisdn;

        $r = funcSendCustomerSMS_internal(
          array(
            'customer'   => $customer,
            'message'    => $message,
            'client_tag' => 'client_tag'
          )
        );

        dlog('',"result = %s",$r);

        if ( $r['sent'] )
        {
          echo "$msisdn success\n"; dlog('',"$msisdn success");

          log_sms_delivery( $customer_id , $sms_template , 'BULK_SMS' );
        }
        else
        {
          echo "$msisdn failure\n"; dlog('',"$msisdn failure");
        }

        if ( $count >= $cycles )
        {
          echo "Cycles are over\n"; dlog('',"Cycles are over"); exit;
        }

        dlog('',"sleeping $sleep_seconds seconds");

        sleep($sleep_seconds);
      }
    }
    else
      dlog('',"customer id not found for $msisdn");
  }
}

/**
 * delay_due_to_queue_size
 *
 * Pauses execution until SMS queue has less than $max_size elements
 *
 * @return NULL
 */
function delay_due_to_queue_size( $controlChannel )
{
  $sleep_wait = 30;
  $max_sleep  = 300;
  $max_size   = 100;

  $size = $controlChannel->getSMSQueueLength();
  dlog('',"size = $size");

  while ( $size > $max_size )
  {
    dlog('',"sleeping $sleep_wait seconds");
    $max_sleep -= $sleep_wait;
    sleep($sleep_wait);

    $size = $controlChannel->getSMSQueueLength();
    dlog('',"size = $size");

    if ( $max_sleep <= 0 )
    {
      dlog('',"giving up");
      exit;
    }
  }
}

?>
