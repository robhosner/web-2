<?php


use function Ultra\UltraConfig\isBPlan;
use function Ultra\UltraConfig\isFlexPlan;

include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_messaging_queue.php');
include_once('fraud.php');
include_once('lib/messaging/functions_3cinteractive.php');
include_once('lib/state_machine/functions.php');
include_once('lib/util-common.php');
include_once('sms.php');
include_once('web.php');
include_once('Ultra/Lib/MiddleWare/Adapter/Control.php');
include_once('Ultra/Lib/MiddleWare/ACC/Control.php');
include_once('Ultra/Lib/MVNE/Adapter.php');
include_once('Ultra/Messaging/Templates.php');
require_once 'classes/Session.php';
require_once 'Ultra/Lib/Services/FamilyAPI.php';
require_once 'Ultra/Messaging/SMSSender.php';

/**
 * sendSMSWifiCallingActivation 
 *
 * Deliver message with details about WIFI calling
 * 
 * @param  array  $customer_id
 * @return array  {sent=>,errors=>array()}
 */
function sendSMSWifiCallingActivation($customer_id)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $result_object = enqueue_immediate_sms(
    $customer_id,
    'wifi_calling_activation',
    []
  );
  
  if ($errors = $result_object->get_errors())
    $result['errors'] = $errors;
  else
    $result['sent'] = true;

  return $result;
}

/**
 * sendSMSE911AddressUpdated 
 *
 * Deliver message when E911 address is updated
 * 
 * @param  array  $customer_id
 * @return array  {sent=>,errors=>array()}
 */
function sendSMSE911AddressUpdated($customer_id)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $result_object = enqueue_daytime_sms(
    $customer_id,
    'e911_updated',
    []
  );
  
  if ($errors = $result_object->get_errors())
    $result['errors'] = $errors;
  else
    $result['sent'] = true;

  return $result;
}

/**
 * sendSMSWifiSocUpdated 
 *
 * Deliver message when WIFI soc is added or removed
 * 
 * @param  array  $customer_id
 * @return array  {sent=>,errors=>array()}
 */
function sendSMSWifiSocUpdated($customer_id, $action)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $action = strtolower($action);
  if ( ! in_array($action, ['add', 'remove']))
  {
    $result['errors'][] = "Invalid action ($action) for sendSMSWifiSocUpdated";
    return $result;
  }

  $result_object = enqueue_daytime_sms(
    $customer_id,
    "wifi_calling_$action",
    []
  );

  $result['errors'] = $result_object->get_errors();
  $result['sent']   = true;

  return $result;
}

/**
 * funcSendSMSBOGOActivation 
 *
 * Deliver message, customer activated a BOGO ICCID
 * 
 * @param  array $params [customer_id]
 * @param  string $bogo_type type of bogo promotion (customer option value)
 * @return array {sent=>,errors=>array()}
 */
function funcSendSMSBOGOActivation($params, $bogo_type)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $template = NULL;

  switch ($bogo_type)
  {
    case BILLING_OPTION_ATTRIBUTE_BOGO_MONTH:
      $template = 'bogo_activation';
      break;
    case BILLING_OPTION_ATTRIBUTE_7_11_BOGO:
      $template = '7_11_bogo_activation';
      break;
  }

  if ( ! $template)
  {
    $result['errors'][] = "BOGO SMS template not found for bogo type: $bogo_type";
    return $result;
  }

  if ( isset( $params['customer_id'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id($params['customer_id'], array('CUSTOMER_ID'));

  if ( $params['customer'] )
  {
    $result_object = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      $template,
      array()
    );

    $result['errors'] =  $result_object->get_errors();
  }
  else
  { 
    $result['errors'][] = "Customer not found";
  }

  return $result;
}

/**
 * funcSendSMSBOGORedeemed 
 *
 * Deliver message, customer redeemed the BOGO promotion
 * 
 * @param  array  $params [customer_id]
 * @param  string $bogo_type type of bogo promotion (customer option value)
 * @return array  {sent=>,errors=>array()}
 */
function funcSendSMSBOGORedeemed($params, $bogo_type)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $template = NULL;

  switch ($bogo_type)
  {
    case BILLING_OPTION_ATTRIBUTE_BOGO_MONTH:
      $template = 'bogo_redeemed';
      break;
    case BILLING_OPTION_ATTRIBUTE_7_11_BOGO:
      $template = '7_11_bogo_second_month';
      break;
  }

  if ( ! $template)
  {
    $result['errors'][] = "BOGO SMS template not found for bogo type: $bogo_type";
    return $result;
  }

  if ( isset( $params['customer_id'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id($params['customer_id'], array('CUSTOMER_ID','plan_expires'));

  if ( $params['customer'] )
  {
    $result_object = enqueue_daytime_sms(
      $params['customer']->CUSTOMER_ID,
      $template,
      array(
        'renewal_date' => date('m/d', strtotime('+30 days', strtotime($params['customer']->plan_expires)))
      )
    );

    $result['errors'] =  $result_object->get_errors();
  }
  else
  { 
    $result['errors'][] = "Customer not found";
  }

  return $result;
}

/**
 * send_promotional_plans_additional_messages
 *
 * Delivers messages specified in [ULTRA].[PROMOTIONAL_PLANS].[ADDITIONAL_MESSAGES]
 */
function send_promotional_plans_additional_messages( $ultra_promotional_plan , $customer_id )
{
  $messages = explode( '|' , $ultra_promotional_plan->ADDITIONAL_MESSAGES );

  if ( $messages && count($messages) )
  {
    $plan_amount = get_plan_cost_from_cos_id($ultra_promotional_plan->TARGET_PLAN) / 100;

    $message_params = array(
      'plan_amount' => $plan_amount,
      'active_days' => $ultra_promotional_plan->INCLUDED_PLAN_CYCLES * 30
    );

    foreach ( $messages as $message )
    {
      $return = enqueue_immediate_sms(
        $customer_id,
        $message,
        $message_params
      );

      if ( $return->is_failure() )
        dlog('',"Message $message could not be delivered.");
    }
  }
  else
  {
    dlog('',"No messages found.");
  }
}

function send_message_resolution_required( $customer_id )
{
  // if source is ultra.me send an email instead of SMS

  dlog('', '(%s)', func_get_args());

  $customer = get_ultra_customer_from_customer_id($customer_id,array('CUSTOMER_SOURCE'));

  if ( $customer )
  {
    if ( $customer->CUSTOMER_SOURCE && $customer->CUSTOMER_SOURCE === 'ULTRAME' )
    {
      // send_Email to the customer
      send_portin_resubmittable_email( $customer_id );
    }
    else
    {
      // SMS the customer, asking them to call into care or go to their dealer to finish updating the port.
      send_sms_resolution_required( $customer_id );
    }
  }
  else
  { dlog('',"ERROR: customer not found"); }
}

function send_sms_resolution_required( $customer_id )
{
  // send this message only once per 3 days

  $result = enqueue_once_immediate_external_sms(
    $customer_id,
    'port_resolution_required',
    array(),
    (24*3)
  );

  if ( $result->is_success() )
  {
    dlog('',"enqueue_once_immediate_external_sms OK");
  }
  else
  {
    dlog('',"enqueue_once_immediate_external_sms ERRORS: ".json_encode($return->get_errors()));
  }
}

// Temporary Password (dealer)
function funcSendExemptCustomerSMSTempPasswordDealer($params)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $customer_id = ( isset($params['customer']) )
                 ?
                 $params['customer']->CUSTOMER_ID
                 :
                 $params['customer_id']
                 ;

  $return = enqueue_immediate_sms(
    $customer_id,
    'temp_password_dealer',
    array(
      'temp_password' => $params['temp_password']
    )
  );

  if ( $return->is_failure() )
  {
    $result['errors'] = $return->get_errors();
  }
  else
  {
    $result['sent'] = TRUE;
  }

  return $result;
}

// Temporary Password
function funcSendExemptCustomerSMSTempPassword($params)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $customer_id = ( isset($params['customer']) )
                 ?
                 $params['customer']->CUSTOMER_ID
                 :
                 $params['customer_id']
                 ;

  $return = enqueue_immediate_sms(
    $customer_id,
    'temp_password',
    array(
      'temp_password' => $params['temp_password']
    )
  );

  if ( $return->is_failure() )
  {
    $result['errors'] = $return->get_errors();
  }
  else
  {
    $result['sent'] = TRUE;
  }

  return $result;
}

// Unsuspend - Activation
function funcSendExemptCustomerSMSUnsuspend($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  $params['customer'] = get_ultra_customer_from_customer_id($params['customer_id'], array(
    'CUSTOMER_ID',
    'latest_plan_date'
  ));

  $account = get_account_from_customer_id($params['customer_id'], array('COS_ID'));

  if ( $params['customer'] && $account )
  {
    $plan_cost = get_plan_costs_by_customer_id($params['customer']->CUSTOMER_ID, $account->COS_ID);

    $renewal_date = get_date_from_full_date( $params['customer']->latest_plan_date );
    $renewal_date = substr($renewal_date,0,strlen($renewal_date)-5);

    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'unsuspend_activation',
      array(
        'plan_name'     => $plan_cost['name'],
        'renewal_date'  => $renewal_date
      )
    );

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

// Port-In Requested => Active
function funcSendExemptCustomerSMSPortSuccess($params, $cosId)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
  {
    $params['customer'] = get_ultra_customer_from_customer_id($params['customer_id'], array(
      'CUSTOMER_ID',
      'current_mobile_number',
    ));
  }

  if ( $params['customer'] )
  {
    $template = (\Ultra\Lib\Flex::isFlexPlan($cosId))
        ? 'flex_activate_portin'
        : 'activate_portin';

    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      $template,
      array(
        'msisdn' => $params['customer']->current_mobile_number
      )
    );

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

function funcSendExemptCustomerSMSImmediatePlanChange($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  $customer_id = ( isset($params['customer']) )
                 ?
                 $params['customer']->CUSTOMER_ID
                 :
                 $params['customer_id']
                 ;

  $old_plan_amount = get_plan_cost_from_cos_id(get_cos_id_from_plan($params['plan_from'])) / 100;
  $new_plan_amount = get_plan_cost_from_cos_id(get_cos_id_from_plan($params['plan_to']))   / 100;

  $bolt_ons_cost = get_bolt_ons_costs_by_customer_id($customer_id);
  $return = enqueue_immediate_sms(
    $customer_id,
    'immediate_plan_change',
    array(
      'old_plan_amount' => $old_plan_amount + $bolt_ons_cost,
      'new_plan_amount' => $new_plan_amount + $bolt_ons_cost
    )
  );

  if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }

  return $result;
}

// renewal_target that failed to take place BUT there were enough funds to go active -> active within the same COS_ID
function funcSendExemptCustomerSMSRenewalPlanFailChange($params)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  if ( isset( $params['customer_id'] ) )
  {
    $customer_query = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 1,
      array('CUSTOMER_ID','MONTHLY_RENEWAL_TARGET','DATEADD( dd , -1 , PLAN_EXPIRES ) latest_plan_date'), 
      array('CUSTOMER_ID' => $params['customer_id'])
    );

    $customer_result = mssql_fetch_all_objects(logged_mssql_query($customer_query));
    if ( ! empty($customer_result) && is_array($customer_result) )
    {
      $params['customer'] = $customer_result[0];
      $params['customer']->COS_ID = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $params['customer']->CUSTOMER_ID, 'COS_ID', NULL);
    }
  }

  if ( $params['customer'] )
  {
    $plan_cost = get_plan_costs_by_customer_id($params['customer']->CUSTOMER_ID, $params['customer']->COS_ID);

    $plan_amount_upgrade = get_plan_cost_from_cos_id(get_cos_id_from_plan($params['customer']->MONTHLY_RENEWAL_TARGET)) / 100;

    $result_object = enqueue_daytime_sms(
      $params['customer']->CUSTOMER_ID,
      'renewal_plan_fail_change',
      array(
        'current_plan_amount' => $plan_cost['plan'],
        'plan_amount_upgrade' => $plan_amount_upgrade,
        'renewal_date'        => get_date_from_full_date( $params['customer']->latest_plan_date ),
        'personal'            => $plan_cost['personal']
      )
    );

    $result = $result_object->to_array();
  }
  else
  { $result['errors'][] = "Customer not found"; }

  return $result;
}

function send_sms_bolt_on_data_immediate($customer_id, $boltOn = null)
{
  return funcSendExemptCustomerSMSDataBoltOnSuccess(
    array(
      'customer_id'  => $customer_id,
      'template' => 'immediate',
      'boltOn' => $boltOn,
    )
  );
}

function send_sms_bolt_on_data_recurring($customer_id, $boltOn = null)
{
  return funcSendExemptCustomerSMSDataBoltOnSuccess(
    array(
      'customer_id'  => $customer_id,
      'template' => 'recurring',
      'boltOn' => $boltOn,
    )
  );
}

// funcSendExemptCustomerSMSDataRecharge wrapper
function send_sms_data_recharge($customer_id,$data_MB)
{
  return funcSendExemptCustomerSMSDataRecharge(
    array(
      'customer_id' => $customer_id,
      'data_MB'     => $data_MB
    )
  );
}


// enqueue SMS and does not fail
function funcSendExemptCustomerSMSNoFail($params)
{
  // does not return errors or failures because it may be used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID') );

  if ( $params['customer'] )
  {
    $return = '';

    if ( $params['enqueue_sms_when'] == 'immediate' )
    {
      $return = enqueue_immediate_sms(
        $params['customer']->CUSTOMER_ID,
        $params['sms_template'],
        $params['sms_params']
      );
    }
    else
    {
      $return = enqueue_daytime_sms(
        $params['customer']->CUSTOMER_ID,
        $params['sms_template'],
        $params['sms_params']
      );
    }

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

// UpData Bolt On processed successfully
function funcSendExemptCustomerSMSDataBoltOnSuccess($params)
{
  return funcSendExemptCustomerSMSBoltOnSuccess($params, 'UpData');
}

// UpIntl Bolt On processed successfully
function funcSendExemptCustomerSMSIntlBoltOnSuccess($params)
{
  return funcSendExemptCustomerSMSBoltOnSuccess($params, 'UpIntl');
}

// Bolt On processed successfully
function funcSendExemptCustomerSMSBoltOnSuccess($params, $bolt_on_type = 'UpData')
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID','current_mobile_number') );

  if ( $params['customer'] )
  {
    $template = 'bolton_success_' . $params['template'];
    $smsParams = [
      'msisdn'  => $params['customer']->current_mobile_number,
      'bolt_on' => $bolt_on_type
    ];

    if (isset($params['boltOn']) && is_array($params['boltOn']) && !empty($params['boltOn'])) {
      if ($params['boltOn']['product'] == 'SHAREDDATA') {
        $template = $params['template'] == 'recurring' ? 'flex_shared_data_bolton_success_recurring' : 'flex_shared_data_bolton_success_immediate';
        $smsParams = ['shared_data' => beautify_4g_lte_string($params['boltOn']['get_value'], false)];
      } elseif ($params['boltOn']['product'] == 'SHAREDILD') {
        $template = $params['template'] == 'recurring' ? 'flex_shared_ild_bolton_success_recurring' : 'flex_shared_ild_bolton_success_immediate';
        $smsParams = ['shared_ild' => $params['boltOn']['get_value']];
      }
    }

    $return = enqueue_immediate_sms($params['customer']->CUSTOMER_ID, $template, $smsParams);

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

// Unable to process recurring Bolt On due to a system error
// Populates __params__bolt_ons
function funcSendExemptCustomerSMSRecurringBoltOnsError($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'recurring_boltons_error',
      array(
        'msisdn'   => $params['customer']->current_mobile_number,
        'bolt_ons' => $params['bolt_ons']
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

// Unable to process recurring Bolt Ons due to lack of funds
// Populates __params__bolt_ons
function funcSendExemptCustomerSMSRecurringBoltOnsNoFunds($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'recurring_boltons_no_funds',
      array(
        'msisdn'   => $params['customer']->current_mobile_number,
        'bolt_ons' => $params['bolt_ons']
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

/**
 * funcSendExemptCustomerSMSTemplateNoFail
 *
 * Enqueue SMS and does not fail
 * To be used when no parameters are necessary
 *
 * @return array
 */
function funcSendExemptCustomerSMSTemplateNoFail( $customer_id , $sms_template , $enqueue_sms_when='immediate', $params=array() )
{
  $params = array(
    'customer_id'      => $customer_id,
    'sms_template'     => $sms_template,
    'sms_params'       => $params,
    'enqueue_sms_when' => $enqueue_sms_when
  );

  return funcSendExemptCustomerSMSNoFail($params);
}

// The customer attempted to redeem a promotion which is limited to Active customers
function funcSendExemptCustomerSMSPromoRedeemInactive($params)
{
  $params['sms_template']     = 'promo_redeem_inactive';
  $params['sms_params']       = array();
  $params['enqueue_sms_when'] = 'immediate';

  return funcSendExemptCustomerSMSNoFail($params);
}

// Data 100% usage reached
function funcSendExemptCustomerSMSData100PercentUsed($params)
{
  $params['sms_template']     = 'data_block_100_percent';
  $params['sms_params']       = array();
  $params['enqueue_sms_when'] = 'immediate';

  return funcSendExemptCustomerSMSNoFail($params);
}

// Data 100% Throttled
function funcSendExemptCustomerSMSDataThrottle100PercentUsed($params)
{
  $params['sms_template']     = 'data_throttle_100_percent';
  $params['sms_params']       = array();
  $params['enqueue_sms_when'] = 'immediate';

  return funcSendExemptCustomerSMSNoFail($params);
}

// Data 95% usage reached
function funcSendExemptCustomerSMSData95PercentUsed($params)
{
  $params['sms_template']     = 'data_95_percent';
  $params['sms_params']       = array();
  $params['enqueue_sms_when'] = 'immediate';

  return funcSendExemptCustomerSMSNoFail($params);
}

// Data Recharge
function funcSendExemptCustomerSMSDataRecharge($params)
{
  $params['sms_template']     = 'data_recharged';
  $params['sms_params']       = array( 'data_MB' => $params['data_MB'] );
  $params['enqueue_sms_when'] = 'immediate';

  return funcSendExemptCustomerSMSNoFail($params);
}

// Voice Recharge
function funcSendExemptCustomerSMSVoiceRecharge($params)
{
  return array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
}

// Voice Recharge Error
function funcSendExemptCustomerSMSVoiceRechargeError($params)
{
  return array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
}

// Add Balance
function funcSendExemptCustomerSMSBalanceAdd($params)
{
  $params['sms_params'] = array(
    'renewal_date'  => $params['renewal_date'],
    'reload_amount' => $params['reload_amount'],
    'gizmo_amount'  => $params['gizmo_amount']
  );

  $params['enqueue_sms_when'] = 'immediate';

  return funcSendExemptCustomerSMSNoFail($params);
}

// AMDOCS-276 - [6700] reply with "Customer Self Care is currently unavailable. Please call 611 for any questions."
function funcSendExemptCustomerSMSSelfCareUnavailable($params)
{
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['msisdn'] ) )
  {
    $result = array(
      'sent' => FALSE, 'success' => FALSE, 'errors' => array('funcSendExemptCustomerSMSSelfCareUnavailable - msisdn cannot be empty')
    );
    return $result;
  }

  $customer = get_customer_from_msisdn( $params['msisdn'] );

  if ( ! $customer )
  {
    $result = array(
      'sent' => FALSE, 'success' => FALSE, 'errors' => array('funcSendExemptCustomerSMSSelfCareUnavailable - msisdn '.$params['msisdn'].' is not associated with a customer')
    );
    return $result;
  }

  $return = enqueue_immediate_sms(
    $customer->CUSTOMER_ID,
    'self_care_unavailable',
    array(
      'msisdn' => $params['msisdn']
    )
  );

  if ( $return->is_failure() )
    $result = array(
      'sent' => FALSE, 'success' => FALSE, 'errors' => array('funcSendExemptCustomerSMSSelfCareUnavailable - enqueue_immediate_sms failed')
    );

  return $result;
}

// MVNO-2466
function funcSendExemptCustomerSMSJulyPlanRefresh($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID','current_mobile_number') );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'julyplanrefresh',
      array(
        'msisdn' => $params['customer']->current_mobile_number
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = $return->get_errors();
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'][] = "Customer not found";

  return $result;
}

// Activation New Phone
function funcSendExemptCustomerSMSActivationNewPhone($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID','current_mobile_number') );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'activate_newphone',
      array(
        'msisdn' => $params['customer']->current_mobile_number
      )
    );

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

function funcSendExemptCustomerSMSPromotionCredit($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'promotion_credit',
      array(
        'msisdn' => $params['customer']->current_mobile_number,
        'amount' => $params['amount']
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

function funcSendExemptCustomerSMSPromotionNotEligible($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'promotion_not_eligible',
      array(
        'msisdn' => $params['customer']->current_mobile_number
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

function funcSendExemptCustomerSMSPromotionBlock($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'promotion_block',
      array(
        'msisdn' => $params['customer']->current_mobile_number
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

function funcSendExemptCustomerSMSPromotionInactive($params)
{
  $result = array( 'sent' => FALSE, 'success' => FALSE, 'errors' => array() );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'promotion_inactive',
      array(
        'msisdn' => $params['customer']->current_mobile_number
      )
    );

    if ( $return->is_failure() )
      $result['errors'] = 'ERROR: MW failure';
    else
      $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }
  else
    $result['errors'] = 'ERROR: customer not found';

  return $result;
}

// Plan upgraded (status = 'Suspended')
function funcSendExemptCustomerSMSPlanSuspendedUpgraded($params)
{
  $params['sms_template'] = 'plan_upgraded';

  return funcSendExemptCustomerSMSPlanSuspendedChanged($params);
}

// Plan downgraded (status = 'Suspended')
function funcSendExemptCustomerSMSPlanSuspendedDowngraded($params)
{
  $params['sms_template'] = 'plan_downgraded';

  return funcSendExemptCustomerSMSPlanSuspendedChanged($params);
}

function funcSendExemptCustomerSMSPlanSuspendedChanged($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id($params['customer_id'], array('CUSTOMER_ID'));

  if ( $params['customer'] )
  {
    $bolt_ons_cost = get_bolt_ons_costs_by_customer_id($params['customer']->CUSTOMER_ID);
    $amount = substr($params['plan_cost'], 0, -2) + $bolt_ons_cost;

    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      $params['sms_template'],
      array(
        'plan_amount' => $amount . '.00',
        'personal'    => $bolt_ons_cost ? 'Personal' : NULL,
        'amount'      => $amount));

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

// Activation Plan Info
function funcSendExemptCustomerSMSActivatePlanInfo($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('plan_expires','CUSTOMER_ID','DATEADD( dd , -1 , plan_expires ) latest_plan_date') );

  if ( $params['customer'] )
  {
    if (billing_mrc_promo_plan_info($params['customer']->CUSTOMER_ID))
      dlog('', 'skipped due to billing_mrc_promo_plan');
    elseif (is_multi_month_plan($params['cos_id']))
      dlog('', 'skipped due to multi month plan');
    elseif (count($bogo_info = bogo_month_info($params['customer']->CUSTOMER_ID)))
    {
      $return = funcSendSMSBOGOActivation(array('customer_id' => $params['customer']->CUSTOMER_ID), $bogo_info[0]);
      if ( count($return['errors']) ) { dlog('',"ERRORS: ".json_encode($return['errors'])); }
    }
    else
    {
      if (\Ultra\UltraConfig\isMintPlan($params['cos_id']))
      {
        $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($params['customer']->CUSTOMER_ID);
        $renewal_date      = $multiMonthOverlay->CYCLE_EXPIRES;

        $total_months = \Ultra\UltraConfig\planMonthsByCosId($params['cos_id']);
        $days         = \Ultra\UltraConfig\planDaysByCosId($params['cos_id']);

        $planKey = get_plan_name_from_short_name(get_plan_from_cos_id($params['cos_id']));
        $planName = '$' . \Ultra\UltraConfig\find_config('plans/Ultra/' . $planKey . '/cost') / 100;
      } else if (isBPlan($params['cos_id'])) {
        $planName = '';
        $overlayRow  = ultra_multi_month_overlay_from_customer_id($params['customer']->CUSTOMER_ID);
        $renewal_date = $overlayRow->CYCLE_EXPIRES;
        $days = $overlayRow->TOTAL_MONTHS * 30;
      }
      else
      {
        $plan_cost    = get_plan_costs_by_customer_id($params['customer']->CUSTOMER_ID, $params['cos_id']);
        $planName     = $plan_cost['name'];
        $renewal_date = $params['customer']->latest_plan_date;
        $days         = 30;
      }

      // flex switch
      $template = (\Ultra\Lib\Flex::isFlexPlan($params['cos_id']))
        ? 'flex_activate_planinfo'
        : 'activate_planinfo';

      $return = enqueue_immediate_sms(
        $params['customer']->CUSTOMER_ID,
        $template,
        array(
          'plan_name'    => $planName,
          'renewal_date' => get_date_from_full_date( $renewal_date ),
          'days'         => $days
        )
      );

      if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
    }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

/**
 * funcSendExemptCustomerSMSProjectW
 *
 * UV migration SMS
 *
 * @return boolean
 */
function funcSendExemptCustomerSMSProjectW( $customer_id=NULL )
{
  // validate customer_id
  if ( empty( $customer_id ) )
    return \logError( 'customer_id missing' );

  $msisdn = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer_id, 'current_mobile_number');

  // validate msisdn
  if ( empty( $msisdn ) )
    return \logError( "No msisdn found for customer_id $customer_id" );

  $result = enqueue_daytime_sms(
    $customer_id,
    'activate_univision',
    [
      'login_token' => Session::encryptToken( $msisdn , Session::TOKEN_V3 , [Session::SYSTEM_TOKEN] )
    ]
  );

  return $result->is_success();
}

// Activation Help
function funcSendExemptCustomerSMSActivateHelp($params)
{
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_ultra_customer_from_customer_id($params['customer_id'], array('CUSTOMER_ID'));

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'activate_help',
      array('login_token' => Session::encryptToken(\Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $params['customer']->CUSTOMER_ID, 'current_mobile_number'), Session::TOKEN_V3, array(Session::SYSTEM_TOKEN)))

    );

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

// Activation Data Help
function funcSendExemptCustomerSMSActivateDataHelp($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_account_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID','COS_ID') );

  if ( $params['customer'] )
  {
    $return = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      'activate_datahelp',
      array()
    );

    if ( $return->is_failure() ) { dlog('',"ERRORS: ".json_encode($return->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

// Disabling SMS
function funcSendExemptCustomerSMSDisabling($params)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_account_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID','COS_ID') );

  if ( $params['customer'] )
  {
    $sms_template = null;
    $sms_params = array();

    $redis = new \Ultra\Lib\Util\Redis;

    $bogo_suspended_params = $redis->get('ultra/bogo_suspended/customer_id/' . $params['customer']->CUSTOMER_ID);
    if ($bogo_suspended_params)
    {
      list ($added_amount, $owed_amount) = explode(',', $bogo_suspended_params);

      $sms_template = 'bogo_suspended';
      $sms_params = array(
        'added_amount' => $added_amount,
        'owed_amount'  => $owed_amount
      );
    }
    else
    {
      $plan_amount  = get_plan_cost_from_cos_id($params['customer']->COS_ID) / 100;
      $plan_amount += get_bolt_ons_costs_by_customer_id($params['customer']->CUSTOMER_ID);
      $plan_amount .= '.00';

      $sms_template = 'plan_suspended_now';

      if (isFlexPlan($params['customer']->COS_ID)) {
        $sms_template = 'flex_plan_suspended_now';
      }

      $sms_params = array(
        'plan_amount' => $plan_amount,
        'login_token' => Session::encryptToken(\Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $params['customer']->CUSTOMER_ID, 'current_mobile_number'), Session::TOKEN_V3, array(Session::SYSTEM_TOKEN))
      );
    }

    $result_object = enqueue_immediate_sms(
      $params['customer']->CUSTOMER_ID,
      $sms_template,
      $sms_params
    );

    $result = $result_object->to_array();
  }
  else
  { $result['errors'][] = "Customer not found"; }

  return $result;
}

// MINT Suspended SMS
function funcSendMintExemptCustomerSMSDisabling($params)
{
  $customer = get_ultra_customer_from_customer_id($params['customer_id'], [
    'CUSTOMER_ID',
    'MONTHLY_CC_RENEWAL',
    'current_mobile_number'
  ]);
  
  if ($customer->MONTHLY_CC_RENEWAL)
  {
    $template = 'mint_monthly_suspend_has_recharge';
  }
  else
  {
    $template = 'mint_monthly_suspend_no_balance';
  }

  $result_object = enqueue_immediate_sms(
    $customer->CUSTOMER_ID,
    $template,
    ['login_token' => Session::encryptToken($customer->current_mobile_number, Session::TOKEN_V3, [Session::SYSTEM_TOKEN])]
  );

  return $result_object->to_array();
}

// Monthly Plan Renewal and plan change
function funcSendExemptCustomerSMSMonthlyPlanChange($params)
{
  $result = array( 'success' => FALSE, 'sent' => FALSE, 'errors' => array() );

  $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('plan_expires','CUSTOMER_ID','DATEADD( dd , -1 , plan_expires ) latest_plan_date') );

  if ( $params['customer'] )
  {
    $renewal_date = get_date_from_full_date( $params['customer']->latest_plan_date );
    $renewal_date = substr($renewal_date,0,strlen($renewal_date)-5);

    $cos_id = ( ! empty($params['plan_to'])) ? get_cos_id_from_plan($params['plan_to']) : NULL;

    if (isBPlan($cos_id)) {
      $overlayRow  = ultra_multi_month_overlay_from_customer_id($params['customer']->CUSTOMER_ID);
      $renewal_date = $overlayRow->CYCLE_EXPIRES;
      $days = $overlayRow->TOTAL_MONTHS * 30;
    } else {
      $days = 30;
    }

    $plan_cost = get_plan_costs_by_customer_id($params['customer']->CUSTOMER_ID, $cos_id);
    $result_object = enqueue_daytime_sms(
      $params['customer']->CUSTOMER_ID,
      'renewal_plan_change',
      array(
        'renewal_date' => $renewal_date,
        'plan_amount'  => ($plan_cost['plan'] + $plan_cost['bolt_ons']) . '.00',
        'personal'     => $plan_cost['personal'],
        'days'         => $days
      )
    );

    $result = $result_object->to_array();
  }
  else
  { $result['errors'][] = "Customer not found"; }

  return $result;
}

// MINT Monthly Plan Renewal and plan change
function funcMintSendExemptCustomerSMSMonthlyPlanChange($params)
{
  $customer = get_ultra_customer_from_customer_id($params['customer_id'], [
    'plan_expires',
    'CUSTOMER_ID',
    'DATEADD( dd , -1 , plan_expires ) latest_plan_date',
    'MONTHLY_CC_RENEWAL',
    'MONTHLY_RENEWAL_TARGET',
    'current_mobile_number'
  ]);

  $planFrom = $params['plan_from'];
  $planTo = $params['plan_to'];
  $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);

  $message_type = null;

  // plan switch and auto recharge is enabled
  if ($planFrom != $planTo && $customer->MONTHLY_CC_RENEWAL)
  {
    $message_type = 'mint_monthly_plan_change_and_recharge';
  }
  // no plan switch and auto recharge is enabled
  else if ($planFrom == $planTo && $customer->MONTHLY_CC_RENEWAL)
  {
    $message_type = 'mint_monthly_no_plan_change_and_recharge';
  }
  // plan switch and auto recharge is not enabled
  else if ($planFrom != $planTo && !$customer->MONTHLY_CC_RENEWAL)
  {
    $message_type = 'mint_monthly_plan_change_and_no_recharge';
  }
  // no plan switch and auto recharge is not enabled
  else if ($planFrom == $planTo && !$customer->MONTHLY_CC_RENEWAL)
  {
    $message_type = 'mint_monthly_no_plan_change_and_no_recharge';
  }
  else
  {
    $message_type = null;
  }

  // error out if message type not found
  if (!$message_type)
  {
    return make_error_Result('unable to determine message_type');
  }
  
  $planKey = get_plan_name_from_short_name(get_plan_from_cos_id($planTo));
  $data = \Ultra\UltraConfig\find_config('plans/Ultra/' . $planKey . '/cycle_data');
  $futurePlan = $multiMonthOverlay->TOTAL_MONTHS . ' Month ' . $data . 'GB';

  $params = [
    'future_plan'  => $futurePlan,
    'renewal_date' => date('M j', strtotime($customer->latest_plan_date)),
    'login_token'  => Session::encryptToken($customer->current_mobile_number, Session::TOKEN_V3, [Session::SYSTEM_TOKEN]),
  ];

  $result_object = enqueue_daytime_sms(
    $customer->CUSTOMER_ID,
    $message_type,
    $params
  );

  return $result = $result_object->to_array();
}

// Monthly Plan Renewal
function funcSendExemptCustomerSMSMonthlyPlanRenewal($params)
{
  $result = array( 'sent' => FALSE, 'errors' => array() );

  $params['customer'] = get_ultra_customer_from_customer_id( $params['customer_id'] , array('plan_expires','CUSTOMER_ID','DATEADD( dd , -1 , plan_expires ) latest_plan_date') );

  if ( $params['customer'] )
  {
    $renewal_date = get_date_from_full_date( $params['customer']->latest_plan_date );
    $renewal_date = substr($renewal_date,0,strlen($renewal_date)-5);

    $message_type = 'monthly_renewal';

    // is it a demo/promo account?
    $ultra_customer_options = get_ultra_customer_options_by_customer_id( $params['customer']->CUSTOMER_ID );

    if ( is_array($ultra_customer_options) )
    {
      // promo_plan message
      $index_of_promo_plan_info = array_search('BILLING.MRC_PROMO_PLAN', $ultra_customer_options);

      if ( $index_of_promo_plan_info !== FALSE )
      {
        if (isset( $ultra_customer_options[$index_of_promo_plan_info + 1] ))
          dlog('',"billing_mrc_promo_plan_info = %s", $ultra_customer_options[$index_of_promo_plan_info + 1]);

        // TODO: how can we know if this is the last month or there are no months available?

        $message_type = 'monthly_renewal_promo';
      }

      // dealer_promo message
      else if ( in_array(BILLING_OPTION_DEMO_LINE, $ultra_customer_options) )
      {
        $message_type = 'monthly_renewal_demo';
      }

      // multi_month message
      else if ( in_array(BILLING_OPTION_MULTI_MONTH, $ultra_customer_options) )
      {
        $message_type = 'multi_month_reset';
      }
    }

    $result_object = enqueue_daytime_sms(
      $params['customer']->CUSTOMER_ID,
      $message_type,
      array(
        'renewal_date' => $renewal_date
      )
    );

    $result = $result_object->to_array();
  }
  else
  { $result['errors'][] = "Customer not found"; }

  return $result;
}

// MINT Monthly Plan Renewal
function sendSMSMonthlyPlanRenewalMint($params)
{
  $customer = get_ultra_customer_from_customer_id($params['customer_id'], [
    'plan_expires',
    'CUSTOMER_ID',
    'DATEADD( dd , -1 , plan_expires ) latest_plan_date',
    'MONTHLY_CC_RENEWAL',
    'MONTHLY_RENEWAL_TARGET',
    'current_mobile_number'
  ]);

  $message_type = null;

  $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);
  $renewAndResetDate = date('M j', strtotime($customer->latest_plan_date));

  // determinte message_type
  // renewal
  if ($multiMonthOverlay->MONTHS_REMAINING === 0)
  {
    $message_type = ($customer->MONTHLY_CC_RENEWAL) ? 'mint_monthly_recharge_enabled' : 'mint_monthly';
  }
  // reset, last month
  else if ($multiMonthOverlay->MONTHS_REMAINING === 1)
  {
    if ($customer->MONTHLY_RENEWAL_TARGET && $customer->MONTHLY_CC_RENEWAL)
    {
      $message_type = 'mint_monthly_last_switch_and_recharge';
    }
    else if ($customer->MONTHLY_CC_RENEWAL)
    {
      $message_type = 'mint_monthly_last_recharge_enabled';
    }
    else if ($customer->MONTHLY_RENEWAL_TARGET)
    {
      $message_type = 'mint_monthly_last_switch_enabled';
    }
    else
    {
      $message_type = 'mint_monthly_last';
    }
  }
  // reset
  else
  {
    $message_type = 'mint_monthly_reset';
    $offset = \Ultra\UltraConfig\mintTwelveMonthRenewalOffsets($multiMonthOverlay->UTILIZED_MONTHS);
    $renewAndResetDate = date('M j', strtotime($customer->latest_plan_date . " +$offset days"));
  }

  // error out if message type not found
  if (!$message_type)
  {
    return make_error_Result('unable to determine message_type');
  }

  $creditCardInfo = get_cc_info_from_customer_id($customer->CUSTOMER_ID);

  $data = \Ultra\UltraConfig\find_config('plans/Ultra/' . get_plan_name_from_short_name($params['plan']) . '/cycle_data');
  $futurePlan = \Ultra\UltraConfig\planMonthsByCosId(get_cos_id_from_plan($params['plan'])) . ' Month ' . $data . 'GB';

  $params = [
    'future_plan'  => $futurePlan,
    'renewal_date' => $renewAndResetDate,
    'last_four'    => $creditCardInfo->LAST_FOUR,
    'login_token'  => Session::encryptToken($customer->current_mobile_number, Session::TOKEN_V3, [Session::SYSTEM_TOKEN]),
    'reset_date'   => $renewAndResetDate
  ];

  $result_object = enqueue_daytime_sms(
    $customer->CUSTOMER_ID,
    $message_type,
    $params
  );

  return $result = $result_object->to_array();
}

function sendMintMonthlyPlanRenewalEmail($params)
{
  $customer = get_customer_from_customer_id($params['customer_id']);
  $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);
  $cost = \Ultra\UltraConfig\find_config('plans/Ultra/' . get_plan_name_from_short_name($params['plan']) . '/cost');
  
  return funcSendCustomerEmail([
    'customer'        => $customer,
    'postage_app_key' => POSTAGE_API_KEY_MINT,
    'template_params' => [
      'email'         => $customer->E_MAIL,
      'first_name'    => $customer->FIRST_NAME,
      'last_name'     => $customer->LAST_NAME,
      'msisdn'        => $customer->current_mobile_number,
      'amount'        => "$" . number_format($cost/100, 2, '.', ','),
      'recharge_date' => date('m/d/y', strtotime($multiMonthOverlay->CYCLE_EXPIRES)),
      'renewal_date'  => date('m/d/y', strtotime($customer->plan_expires))
    ],
    'template_name'   => 'mint-term-recharge-successful',
  ]);
}

// Provisioned
function funcSendExemptCustomerSMSProvisionedReminder($params)
{
  // does not return errors or failures because it's used in the state machine
  $result = array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );

  if ( ! isset( $params['customer'] ) )
    $params['customer'] = get_account_from_customer_id( $params['customer_id'] , array('CUSTOMER_ID','COS_ID') );

  if ( $params['customer'] )
  {
    $cos_id = ( isset( $params['cos_id'] ) ) ? $params['cos_id'] : $params['customer']->COS_ID ;

    $plan_amount  = get_plan_cost_from_cos_id($cos_id) / 100;
    $plan_amount += get_bolt_ons_costs_by_customer_id($params['customer']->CUSTOMER_ID);
    $plan_amount .= '.00';

    $template = (\Ultra\Lib\Flex::isFlexPlan($params['cos_id']))
      ? 'flex_plan_provisioned'
      : 'plan_provisioned';

    $result1 = enqueue_conditional_sms(
                 $params['customer']->CUSTOMER_ID,
                 'plan_provisioned_reminder',
                 6,
                 array(
                   'message_type'  => $template,
                   'plan_amount'   => $plan_amount
                 ),
                 TRUE // send daytime
    );

    $result2 = enqueue_conditional_sms(
                 $params['customer']->CUSTOMER_ID,
                 'plan_provisioned_reminder',
                 24,
                 array(
                   'message_type'  => $template,
                   'plan_amount'   => $plan_amount
                 ),
                 TRUE // send daytime
    );

    if ( $result1->is_failure() ) { dlog('',"ERRORS: ".json_encode($result1->get_errors())); }
    if ( $result2->is_failure() ) { dlog('',"ERRORS: ".json_encode($result2->get_errors())); }
  }
  else
  { dlog('',"ERROR: customer not found"); }

  return $result;
}

// Sends an SMS to one of our customers - Internal. Only to be used for transactional or renewal messages.
function funcSendExemptCustomerSMS($params)
{
  $customer = $params['customer'];

  if ( ! $customer )
    return array( 'sent' => FALSE, 'errors' => array('funcSendExemptCustomerSMS invoked with no customer') );

  # wraps around funcSendCustomerSMS

  $params['type'] = 'internal';

  dlog('',"funcSendCustomerSMS sending SMS to ".$customer->current_mobile_number." (".$customer->COS_ID.")");

  $result = funcSendCustomerSMS( $params );

  $outcome = ( $result['sent'] ) ? 'success' : 'error';

  #$fraud_data = array(
  #  'customer'    => $customer,
  #  'cos_id'      => $customer->COS_ID,
  #  'source'      => 'funcSendExemptCustomerSMS',
  #  'destination' => $customer->current_mobile_number
  #);
  #fraud_event($customer, 'messaging', 'funcSendExemptCustomerSMS', $outcome, $fraud_data);

  return $result;
}

function funcSendExemptCustomerEmail_ultra_portin_success($params)
{
  if ( ! $params['customer']->E_MAIL ) { return TRUE; } // skip if no email address

  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME
  );

  // identify full template name for this brand and language
  $brand = \Ultra\UltraConfig\getShortNameFromBrandId($params['customer']->BRAND_ID);
  if ( ! $template_name = getPostageAppTemplateName('porting-success', $brand, $params['customer']->preferred_language))
    return logError('unable to determinate template name');

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => $template_name,
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_mint_portin_success($params)
{
  return funcSendCustomerEmail([
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_MINT,
    'template_params' => [
      'email'      => $params['customer']->E_MAIL,
      'first_name' => $params['customer']->FIRST_NAME,
      'last_name'  => $params['customer']->LAST_NAME
    ],
    'template_name'   => 'mint-porting-success',
  ]);
}

function funcSendExemptCustomerEmail_ultra_portin_fail_resubmit($params)
{
  if ( ! $params['customer']->E_MAIL ) { return TRUE; } // skip if no email address

  $customer_id            = $params['customer']->CUSTOMER_ID;
  $message_type           = 'porting-failure-resubmit';
  $hours_period           = (24*3);
  $messaging_queue_params = array(
    'template_name' => 'ultra-auto-recharge-on',
    'email'         => $params['customer']->E_MAIL,
    'first_name'    => $params['customer']->FIRST_NAME,
    'last_name'     => $params['customer']->LAST_NAME
  );

  $result = enqueue_once_immediate_email($customer_id,$message_type,$messaging_queue_params,$hours_period);

  return $result->is_success();
}

function funcSendExemptCustomerEmail_ultra_portin_fail_denied($params)
{
  if ( ! $params['customer']->E_MAIL ) { return TRUE; } // skip if no email address

  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'porting-failure-denied'
  );

  return funcSendCustomerEmail($params);
}

/**
 * funcSendExemptCustomerEmail_ultra_forgot_password
 *
 * Input:
 *  - customer ( object with E_MAIL , FIRST_NAME , LAST_NAME )
 *  - new_password
 *
 * @return boolean
 */
function funcSendExemptCustomerEmail_ultra_forgot_password($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'new_password'   => $params['new_password']
  );

  $params = array(
    'customer'         => $params['customer'],
    'postage_app_key'  => POSTAGE_API_KEY_ULTRA,
    'template_params'  => $template_params,
    'template_name'    => 'ultra-forgot-password',
    'skip_preferences' => 1,
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_mint_forgot_password($params)
{
  return funcSendCustomerEmail([
    'customer'         => $params['customer'],
    'postage_app_key'  => POSTAGE_API_KEY_MINT,
    'template_params'  => [
      'email'          => $params['customer']->E_MAIL,
      'first_name'     => $params['customer']->FIRST_NAME,
      'last_name'      => $params['customer']->LAST_NAME,
      'new_password'   => $params['new_password']
    ],
    'template_name'    => 'mint-forgot-password',
    'skip_preferences' => 1,
  ]);
}

function funcSendExemptCustomerEmail_ultra_auto_recharge_on($params)
{
  $template_params = array_merge(
    array(
      'email'          => $params['customer']->E_MAIL,
      'first_name'     => $params['customer']->FIRST_NAME,
      'last_name'      => $params['customer']->LAST_NAME
      ),
    $params);

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-auto-recharge-on'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_auto_recharge_off($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'plan_name'      => $params['plan_name'],
    'plan_cost'      => $params['plan_cost'],
    'renewal_date'   => $params['renewal_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-auto-recharge-off'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_replacement_sim_ordered($params)
{
  $template_params = array(
    'email'           => $params['customer']->E_MAIL,
    'first_name'      => $params['customer']->FIRST_NAME,
    'last_name'       => $params['customer']->LAST_NAME,
    'address1'        => $params['customer']->ADDRESS1,
    'address2'        => $params['customer']->ADDRESS2,
    'city'            => $params['customer']->CITY,
    'state'           => $params['customer']->STATE_REGION,
    'zip'             => $params['customer']->POSTAL_CODE,
    'tracking_number' => $params['tracking_number']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-replacement-sim-ordered'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_recharge_successful($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'msisdn'         => prettifyMsisdn($params['customer']->current_mobile_number),
    'amount'         => $params['amount'],
    'recharge_date'  => $params['recharge_date'],
    'renewal_date'   => $params['renewal_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-recharge-successful'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_purchase_intl($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'amount'         => $params['amount'],
    'order_date'     => $params['order_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-purchase-intl'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_paygo_topup($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'amount'         => $params['amount'],
    'order_date'     => $params['order_date'],
    'renewal_date'   => $params['renewal_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-paygo-topup'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_plan_switched($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'old_plan_name'  => $params['old_plan_name'],
    'new_plan_name'  => $params['new_plan_name'],
    'plan_cost'      => $params['plan_cost'],
    'renewal_date'   => $params['renewal_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-plan-switched'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_replacement_sim_activated($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'msisdn'         => $params['customer']->current_mobile_number,
    'plan_name'      => $params['plan_name'],
    'renewal_date'   => $params['renewal_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-replacement-sim-activated'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_send_username($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'login_name'     => $params['customer']->LOGIN_NAME
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-send-username'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_purchase_data($params)
{
  $template_params = array(
    'email'           => $params['customer']->E_MAIL,
    'first_name'      => $params['customer']->FIRST_NAME,
    'last_name'       => $params['customer']->LAST_NAME,
    'renewal_date'   => $params['renewal_date'],
    'amount'         => $params['amount'],
    'order_date'     => $params['order_date']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-purchase-data'
  );

  return funcSendCustomerEmail($params);
}

function send_portin_resubmittable_email( $customer_id )
{
  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $check = funcSendExemptCustomerEmail_ultra_portin_fail_resubmit( array( 'customer' => $customer ) );

    if ( ! $check ) { dlog('',"ERROR after funcSendExemptCustomerEmail_ultra_portin_fail_resubmit"); }
  }
  else { dlog('',"ERROR: Cannot load customer"); }

  return TRUE;
}

function send_portin_failure_email( $customer_id )
{
  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $check = funcSendExemptCustomerEmail_ultra_portin_fail_denied( array( 'customer' => $customer ) );

    if ( ! $check ) { dlog('',"ERROR after funcSendExemptCustomerEmail_ultra_portin_fail_denied"); }
  }
  else { dlog('',"ERROR: Cannot load customer"); }

  // does not return errors or failures because it's used in the state machine
  return TRUE;
}

function send_portin_success_email($customer_id)
{
  $customer = get_customer_from_customer_id($customer_id, [
    'u.CUSTOMER_ID',
    'u.BRAND_ID',
    'E_MAIL',
    'FIRST_NAME',
    'LAST_NAME',
    'u.preferred_language'
  ]);

  if ($customer)
  {
    if ($customer->BRAND_ID == 3)
    {
      $brand = 'mint';
      $check = funcSendExemptCustomerEmail_mint_portin_success(['customer' => $customer]);
    }
    else
    {
      $brand = 'ultra';
      $check = funcSendExemptCustomerEmail_ultra_portin_success(['customer' => $customer]);
    }

    if (!$check)
    {
      dlog('', "ERROR after funcSendExemptCustomerEmail_" . $brand . "_portin_success");
    }
  }
  else
  {
    dlog('', "ERROR: Cannot load customer");
  }

  // does not return errors or failures because it's used in the state machine
  return TRUE;
}

function send_activation_email($customer_id, $cos_id = null, $transitionLabel = null)
{
  # send activation email to customer only if
  # { Provisioned or Neutral } => Active

  $sent = FALSE;

  if ( ! $customer_id || ! is_numeric( $customer_id ) )
    return $sent;

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $state = internal_func_get_state_from_customer_id($customer_id);

    if (($state['state'] == "Provisioned") || ($state['state'] == "Neutral"))
    {
      if ($customer->BRAND_ID == 3) {
        $sent = funcSendExemptCustomerEmail_mint_activated(['customer' => $customer]);
      } elseif ($transitionLabel == 'Port Provisioned') {
        $sent = funcSendExemptCustomerEmail_ultra_activated(['customer' => $customer], $cos_id, true);
      } else {
        $sent = funcSendExemptCustomerEmail_ultra_activated( array( 'customer' => $customer ) , $cos_id );
      }
    }
    else
    {
      dlog('',"message not sent because plan_state = %s",$state['state']);
    }
  }
  else
  {
    dlog('',"send_activation_email could not load customer $customer_id");
  }

  return $sent;
}

function send_flex_activation_email($customer_id, $isPortIn=false, $planState='Active')
{
  if ($planState != "Provisioned" && $planState != "Neutral")
    return true;

  $familyApi = new \Ultra\Lib\Services\FamilyAPI();
  $familyResult = $familyApi->getFamilyByCustomerID($customer_id);

  if ( !$familyResult->is_success()) {
    return false;
  }

  $parentCustomerID = $familyResult->data_array['parentCustomerId'];
  $inviteCode = $familyResult->data_array['inviteCode'];
  if ($parentCustomerID == $customer_id) {
    // parent activating
    $parentOverlay = get_ultra_customer_from_customer_id(
      $customer_id, [ 'current_mobile_number', 'plan_expires', 'preferred_language' ]
    );
    if (empty($parentOverlay)) {
      dlog('', 'Failed to lookup parent overlay');
      return false;
    }

    $parentCustomer = customers_get_customer_by_customer_id(
      $customer_id, [ 'FIRST_NAME', 'LAST_NAME' ]
    );
    if (empty($parentCustomer)) {
      dlog('', 'Failed to lookup parent customer');
      return false;
    }

    $language = $parentOverlay->preferred_language == 'ES' ? 'es' : 'en';

    // send SMS
    \enqueue_immediate_sms($parentCustomerID, 'flex_invite_code', ['invite_code' => $inviteCode]);

    // send email
    $template = $isPortIn ? 'flex-parent-portin-success_' . $language : 'flex-parent-activation_' . $language;
    \enqueue_immediate_email($parentCustomerID, $template, [
      'first_name'      => $parentCustomer->FIRST_NAME,
      'last_name'       => $parentCustomer->LAST_NAME,
      'msisdn'          => $parentOverlay->current_mobile_number,
      'expiration_date' => date('M d Y', strtotime($parentOverlay->plan_expires)),
      'invite_code'     => $inviteCode
    ]);
  } else {
    // child activating
    $parentOverlay = get_ultra_customer_from_customer_id(
      $parentCustomerID, [ 'current_mobile_number', 'preferred_language' ]
    );
    if (empty($parentOverlay)) {
      dlog('', 'Failed to lookup parent overlay');
      return false;
    }

    $parentCustomer = customers_get_customer_by_customer_id(
      $parentCustomerID, [ 'FIRST_NAME', 'LAST_NAME' ]
    );
    if (empty($parentCustomer)) {
      dlog('', 'Failed to lookup parent customer');
      return false;
    }

    $childOverlay = get_ultra_customer_from_customer_id(
      $customer_id, [ 'current_mobile_number', 'plan_expires' ]
    );
    if (empty($childOverlay)) {
      dlog('', 'Failed to lookup child overlay');
      return false;
    }

    $childCustomer = customers_get_customer_by_customer_id(
      $customer_id, [ 'FIRST_NAME', 'LAST_NAME' ]
    );
    if (empty($childCustomer)) {
      dlog('', 'Failed to lookup child customer');
      return false;
    }

    $language = $parentOverlay->preferred_language == 'ES' ? 'es' : 'en';

    // send email
    $template = $isPortIn ? 'flex-child-portin-success_' . $language : 'flex-child-activation-success_' . $language;
    \enqueue_immediate_email($customer_id, $template, [
      'first_name'        => $childCustomer->FIRST_NAME,
      'last_name'         => $childCustomer->LAST_NAME,
      'msisdn'            => $childOverlay->current_mobile_number,
      'expiration_date'   => date('M d Y', strtotime($childOverlay->plan_expires)),
      'parent_first_name' => $parentCustomer->FIRST_NAME,
      'parent_last_name'  => $parentCustomer->LAST_NAME,
      'parent_msisdn'     => $parentOverlay->current_mobile_number
    ]);

    // send SMS notifying parent
    \enqueue_immediate_sms($parentCustomerID, 'flex_child_activated', ['child_msisdn' => $childOverlay->current_mobile_number]);
  }

  return true;
}

function funcSendExemptCustomerEmail_ultra_activated($params, $cos_id, $isPortIn = false)
{
  dlog('', '(%s)', func_get_args());

  if ( ! $cos_id )
    $cos_id = $params['customer']->cos_id;

  if ( $cos_id == COSID_ULTRA_STANDBY )
  {
    // The customer is still in NEUTRAL state. Retrieving cos_id from latest transition.
    $result = get_all_customer_state_transitions( $params['customer']->CUSTOMER_ID );

    dlog('',"transitions = %s",$result);

    if ( $result['state_transitions'] && count( $result['state_transitions'] ) )
      foreach( $result['state_transitions'] as $transition )
        if ( $transition->TO_COS_ID && ( $transition->TO_COS_ID != COSID_ULTRA_STANDBY ) )
          $cos_id = $transition->TO_COS_ID;
  }

  // default plan name is same as brand
  $brand = \Ultra\UltraConfig\getShortNameFromBrandId($params['customer']->BRAND_ID);
  $plan_name = ucfirst(strtolower($brand));

  if ( $cos_id != COSID_ULTRA_STANDBY )
  {
    $costs = get_plan_costs_by_customer_id($params['customer']->CUSTOMER_ID, $cos_id);
    $plan_name = '$' . ($costs['plan'] + $costs['bolt_ons']) . ' ' . ($costs['personal'] ? $costs['personal'] : 'Monthly') . ' Plan';
  }

  dlog('',"cos_id = %s ; plan_name = %s",$cos_id,$plan_name);

  $msisdn = ( empty( $params['customer']->current_mobile_number ) || !is_numeric( $params['customer']->current_mobile_number ) ) ? ' not available yet ' : $params['customer']->current_mobile_number  ;

  // if customer has the MULTI MONTH option, adjusts renewal date to + (30 * months_left) of expiration date
  if ($multi_month = multi_month_info($params['customer']->CUSTOMER_ID))
    $params['customer']->plan_expires = date('M j Y h:i:s:uA', strtotime($params['customer']->plan_expires . ' + ' . (30 * $multi_month['months_left']) . ' days'));

  $template_params = array(
    'email'           => $params['customer']->E_MAIL,
    'first_name'      => $params['customer']->FIRST_NAME,
    'last_name'       => $params['customer']->LAST_NAME,
    'msisdn'          => $msisdn,
    'plan_name'       => $plan_name,
    'expiration_date' => get_date_from_full_date($params['customer']->plan_expires)
  );

  // identify full template name for this brand and language
  $template_name = getPostageAppTemplateName($isPortIn ? 'porting-success' : 'ultra-activated', $brand, $params['customer']->preferred_language);

  if (!$template_name)
    return logError('unable to determinate template name');

  if ($isPortIn) {
    $template_params = [
      'first_name' => $params['customer']->FIRST_NAME,
      'last_name'  => $params['customer']->LAST_NAME,
    ];
  }

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => $template_name
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_mint_activated(array $params)
{
  dlog('', '(%s)', func_get_args());

  $customer = $params['customer'];
  $cos_id = $customer->cos_id;
  $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);

  if ($cos_id == COSID_ULTRA_STANDBY)
  {
    // The customer is still in NEUTRAL state. Retrieving cos_id from latest transition.
    $result = get_all_customer_state_transitions($customer->CUSTOMER_ID);

    dlog('',"transitions = %s",$result);

    if ($result['state_transitions'] && count($result['state_transitions']))
    {
      foreach($result['state_transitions'] as $transition )
      {
        if ($transition->TO_COS_ID && ($transition->TO_COS_ID != COSID_ULTRA_STANDBY))
        {
          $cos_id = $transition->TO_COS_ID;
        }
      }
    }
  }

  // default plan name is same as brand
  $brand = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
  $planName = ucfirst(strtolower($brand));

  if ($cos_id != COSID_ULTRA_STANDBY)
  {
    $planKey = get_plan_name_from_short_name(get_plan_from_cos_id($cos_id));
    $plan = 'plans/Ultra/' . $planKey;
    $months = \Ultra\UltraConfig\find_config($plan . '/months');
    $data = \Ultra\UltraConfig\find_config($plan . '/cycle_data');
    $planName = $months . ' month ' . $data . 'GB Plan';
  }

  dlog('', "cos_id = %s ; plan_name = %s", $cos_id, $planName);

  if ($multiMonthOverlay->TOTAL_MONTHS > 1)
  {
    $offset = \Ultra\UltraConfig\mintTwelveMonthRenewalOffsets($multiMonthOverlay->UTILIZED_MONTHS);
    $expirationDate = date('M j, Y', strtotime($customer->plan_expires . " +$offset days"));
  }
  else
  {
    $expirationDate = date('M j, Y', strtotime($customer->plan_expires));
  }

  $params = [
    'customer'        => $customer,
    'postage_app_key' => POSTAGE_API_KEY_MINT,
    'template_params' => [
      'email'           => $customer->E_MAIL,
      'first_name'      => $customer->FIRST_NAME,
      'last_name'       => $customer->LAST_NAME,
      'msisdn'          => empty($customer->current_mobile_number) ? ' not available yet ' : $customer->current_mobile_number,
      'plan_name'       => $planName,
      'expiration_date' => $expirationDate
    ],
    'template_name'   => 'mint-activated'
  ];

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_account_created($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'login_name'     => $params['customer']->LOGIN_NAME,
    'login_password' => $params['customer']->LOGIN_PASSWORD
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-account-created'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_ultra_sim_shipped($params)
{
  $template_params = array(
    'email'          => $params['customer']->E_MAIL,
    'first_name'     => $params['customer']->FIRST_NAME,
    'last_name'      => $params['customer']->LAST_NAME,
    'address1'       => $params['customer']->ADDRESS1,
    'address2'       => $params['customer']->ADDRESS2,
    'city'           => $params['customer']->CITY,
    'state_region'   => $params['customer']->STATE_REGION,
    'postal_code'    => $params['customer']->POSTAL_CODE,
    'tracking_number' => $params['tracking_number']
  );

  $params = array(
    'customer'        => $params['customer'],
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_params' => $template_params,
    'template_name'   => 'ultra-sim-shipped'
  );

  return funcSendCustomerEmail($params);
}

function funcSendExemptCustomerEmail_demo_line_confirmation($params)
{
  $template_params = array(
    'msisdn'             => $params['msisdn'],
    'iccid'              => $params['iccid'],
    'activations_needed' => $params['activations_needed']
  );

  $postage_app_params = array(
      'postage_app_key' => POSTAGE_API_KEY_ULTRA,
      'template_params' => $template_params,
      'template_name'   => 'ultra-demo-line',
      'email'           => $params['email'],
      'subject'         => NULL,
      'header'          => array()
    );

  return send_postage_app_mail( $postage_app_params );
}

// Sends an email to one of our customers. Only to be used for transactional or renewal messages.
function funcSendExemptCustomerEmail($params)
{
  $customer = $params['customer'];
  $email    = $customer->E_MAIL;

  # template only
  $success = funcSendCustomerEmail($params);

  $outcome = ( $success ) ? 'success' : 'error';

  $fraud_data = array(
    'customer'    => $customer,
    'cos_id'      => $customer->COS_ID,
    'source'      => 'funcSendExemptCustomerEmail',
    'destination' => $customer->current_mobile_number
  );

  #fraud_event($customer, 'messaging', 'funcSendExemptCustomerEmail', $outcome, $fraud_data);

  return $success;
}

function validateSMSLength( $message )
{
  $error = '';

  // VYT @ 2014-12-18: raised max lenth to 320 given successull ACC tests of long SMS
  if ( strlen( $message ) > 320 )
  {
    dlog('',"ERROR ESCALATION ALERT DAILY - SMS message too long - $message");
    $error = 'SMS message too long';
  }

  return $error;
}

/**
 * funcSendCustomerSMS
 *
 * Sends an SMS to a customer
 *
 * @return array
 */
function funcSendCustomerSMS($params)
{
  $language = 'EN';

  if ( isset($params['customer'])
    && is_object($params['customer'])
    && property_exists($params['customer'],'preferred_language')
    && $params['customer']->preferred_language
  )
    $language = $params['customer']->preferred_language;

  # compose message
  $params['message'] = \Ultra\Messaging\Templates\SMS_by_language(
    $params,
    $language,
    $params['customer']->BRAND_ID
  );

  $result = array(
    'sent'   => FALSE,
    'errors' => array()
  );

  $error = validateSMSLength( $params['message'] );

  if ( $error )
  {
    $result['errors'][] = $error;
    return $result;
  }

  if ( sms_marketing_preferences_check($params) )
  {
    switch ($params['type'])
    {
      case 'internal': # USA
        $result = funcSendCustomerSMS_internal($params);
        break;

      case 'external': # not USA
        $result = funcSendCustomerSMS_external($params);
        break;

      default:
        $result['errors'][] = "invalid parameters for funcSendCustomerSMS (allowed: 'internal' or 'external')";
        break;
    }
  }

  return $result;
}

/**
 * funcSendCustomerEmail
 *
 * Sends an Email to one of our customers, validating against Marketing Preferences to see if the customer is interested.
 *
 * @return boolean
 */
function funcSendCustomerEmail($params)
{
  $customer = $params['customer'];
  $success  = FALSE;

  # currently a wrapper for send_postage_app_mail: maybe in the future we may need an alternative method to send emails.

  if ( ! $customer->E_MAIL )
    dlog('',"Customer has no email address");
  elseif ( ! validate_email_address( $customer->E_MAIL ) )
    dlog('',"Customer's email address is not valid");
  elseif ( ( isset($params['skip_preferences']) && $params['skip_preferences'] != 0 ) || email_marketing_preferences_check($customer->CUSTOMER_ID) )
  {
    $postage_app_params = array(
      'postage_app_key' => $params['postage_app_key'],
      'template_params' => $params['template_params'],
      'template_name'   => $params['template_name'],
      'email'           => $customer->E_MAIL,
      'subject'         => NULL,
      'header'          => array()
    );

    $success = send_postage_app_mail( $postage_app_params );
  }

  return $success;
}

/**
 * funcSendCustomerSMS_internal
 *
 * Send an SMS to the given customer using the Amdocs API SendSMS
 *
 * @return array
 */
function funcSendCustomerSMS_internal($params)
{
  $result = array(
    'sent'   => FALSE,
    'errors' => array()
  );

  if ( ( ! isset($params['customer']) ) || ( ! $params['customer'] ) )
  {
    $result['errors'][] = "funcSendCustomerSMS_internal invoked with no 'customer'";
    dlog('',"funcSendCustomerSMS_internal invoked with no 'customer'");
    return $result;
  }

  if ( ! $params['customer']->current_mobile_number )
  {
    $result['errors'][] = "customer has no current_mobile_number";
    dlog('',"customer has no current_mobile_number");
    return $result;
  }

  \logDebug("Sending message ".$params['message']." to ".$params['customer']->current_mobile_number);

  #$result = mvneBypassSendSMS( $params['message'] , $params['customer']->current_mobile_number );
  $mvneResult = mvneBypassListSynchronousSendSMS( $params['message'] , $params['customer']->current_mobile_number );

  $result['sent']       = $mvneResult->is_success();
  $result['errors']     = $mvneResult->get_errors();
  $result['ResultCode'] = $mvneResult->data_array['ResultCode'];

  teldata_change_db();

/*
  { // MVNE1

    if ( ( ! isset( $params['client_tag'] ) ) || ( $params['client_tag'] == '' ) )
      $params['client_tag'] = 'client_tag';

    $three_cinteractive_output = three_cinteractive_send_message(
      array(
        'message'      => $params['message'],
        'msisdn'       => $params['customer']->current_mobile_number, # current_mobile_number is 10 digits
        'client_tag'   => teldata_db_name() . '|' . $params['client_tag'] # we use this to match sent messages with HTT_MESSAGING_QUEUE
      )
    );

    $result['sent']   = $three_cinteractive_output['success'];
    $result['errors'] = $three_cinteractive_output['errors'];
  }
*/

  if ( ! $result['sent'] )
    dlog('',"funcSendCustomerSMS_internal could not send SMS to ".$params['customer']->current_mobile_number);

  return $result;
}

function funcSendCustomerSMS_external($params)
{
  // send SMS to a non-Ultra number using Silverstreet
  $result = array(
    'sent'    => TRUE,
    'success' => TRUE,
    'errors'  => array()
  );

  $toMSISDN = !empty($params['params']['_to_msisdn']) ? $params['params']['_to_msisdn'] : $params['customer']->current_mobile_number;
  if (strlen($toMSISDN) == 10) {
    $toMSISDN = '1' . $toMSISDN;
  }

  dlog('', "msisdn = " . $toMSISDN);
  dlog('', "message = " . $params['message']);

  $smsObj = \Ultra\Messaging\SMSSender::getInstance();
  if (!$smsObj->send($toMSISDN, $params['message'])) {
    $result['success'] = FALSE;
    $result['sent']    = FALSE;
    $result['errors'][] = "SMS delivery failed";
  }

  return $result;
}

function email_marketing_preferences_check($customer_id)
{
  $settings = get_marketing_settings(array('customer_id' => $customer_id));
  return $settings['marketing_settings']['marketing_email_option'];
}

function sms_marketing_preferences_check($params)
{
  # returns TRUE if we can send the SMS according to Marketing Preferences

  #TODO:

  return TRUE;
}

/**
 * send_postage_app_mail
 *
 * See https://hometowntelecom.postageapp.com/login
 *
 * @return boolean
 */
function send_postage_app_mail($params)
{
  $success = FALSE;

  $postage_app_key = $params['postage_app_key'];
  $template_params = $params['template_params'];
  $template_name   = $params['template_name'];
  $email           = $params['email'];
  $subject         = $params['subject'];
  $header          = $params['header'];

  # do not send emails to ivr-activated@null
  if ( 'email' == 'ivr-activated@null' ) { return TRUE; }

  dlog('','send_postage_app_mail template_params = '.json_encode($template_params));
  dlog('','send_postage_app_mail template_name   = '.$template_name);

  $result = PostageApp::mail($postage_app_key,
                             $email,
                             $subject,
                             $template_name,
                             $header,
                             $template_params);
  logInfo('result: ' . json_encode($result));

  if ( $result->response->status == 'ok' )
  {
    $success = TRUE;
  }

  return $success;
}

/**
 * enqueue_conditional_sms
 *
 * add a conditional SMS_INTERNAL to HTT_MESSAGING_QUEUE to be sent after $hours_delay
 * @return object of class Result
 */
function enqueue_conditional_sms($customer_id, $message_type, $hours_delay, $messaging_queue_params, $daytime = FALSE)
{
  dlog('', '(%s)', func_get_args());

  if ( ! $customer_id)
    return make_error_Result(__FUNCTION__ . ': invalid customer_id');

  // compute delivery time
  $timestamp = time() + $hours_delay * 60 * 60;
  if ($daytime) // daytime (08:00 - 22:00 customer's time based on ZIP time zone)
    list($delivery_datetime, $delivery_timestamp) = compute_delivery_daytime(
      $timestamp,
      get_customer_time_zone($customer_id),
      extrapolate_message_priority($message_type));
  else // now + $hours_delay
  {
    $delivery_datetime = timestamp_to_date($timestamp, MSSQL_DATE_FORMAT, 'UTC');
    $delivery_timestamp = $timestamp;
  }

  $params = array(
    'customer_id'                 => $customer_id,
    'reason'                      => $message_type,
    'expected_delivery_datetime'  => $delivery_datetime,
    'next_attempt_datetime'       => $delivery_datetime,
    'type'                        => 'SMS_INTERNAL',
    'messaging_queue_params'      => $messaging_queue_params,
    'conditional'                 => 1,
    'expected_delivery_timestamp' => $delivery_timestamp);

  return htt_messaging_queue_add($params);
}

/**
 * enqueue_immediate_sms
 *
 * add a SMS_INTERNAL to HTT_MESSAGING_QUEUE to be sent ASAP
 * @return object of class Result
 */
function enqueue_immediate_sms($customer_id,$message_type,$messaging_queue_params)
{
  dlog('', '(%s)', func_get_args());

  if ( ! $customer_id)
    return make_error_Result(__FUNCTION__ . ': invalid customer_id');

  $params = array(
    'customer_id'                 => $customer_id,
    'reason'                      => $message_type,
    'expected_delivery_datetime'  => 'getutcdate()',
    'next_attempt_datetime'       => 'getutcdate()',
    'type'                        => 'SMS_INTERNAL',
    'messaging_queue_params'      => $messaging_queue_params
  );

  $params['messaging_queue_params']['message_type'] = $message_type;

  return htt_messaging_queue_add($params);
}

/**
 * enqueue_daytime_sms
 *
 * add SMS_INTERNAL to HTT_MESSAGING_QUEUE to be sent during customer's daytime
 * @return object of class Result
 */
function enqueue_daytime_sms($customer_id, $message_type, $messaging_queue_params)
{
  dlog('', '(%s)', func_get_args());

  if ( ! $customer_id)
    return make_error_Result(__FUNCTION__ . ': invalid customer_id');

  // compute daytime delivery time
  list($delivery_datetime, $delivery_timestamp) = compute_delivery_daytime(
    time(),
    get_customer_time_zone($customer_id),
    extrapolate_message_priority($message_type)
  );

  $params = array(
    'customer_id'                 => $customer_id,
    'reason'                      => $message_type,
    'type'                        => 'SMS_INTERNAL',
    'messaging_queue_params'      => $messaging_queue_params,
    'expected_delivery_datetime'  => $delivery_datetime,
    'next_attempt_datetime'       => $delivery_datetime,
    'next_attempt_timestamp'      => $delivery_timestamp);

  $params['messaging_queue_params']['message_type'] = $message_type;

  return htt_messaging_queue_add($params);
}

/**
 * enqueue_immediate_email
 */
function enqueue_immediate_email($customer_id,$template_name,$messaging_queue_params)
{
  // add a EMAIL_POSTAGEAPP to HTT_MESSAGING_QUEUE to be sent ASAP

  $params = array(
    'customer_id'                 => $customer_id,
    'reason'                      => $template_name,
    'type'                        => 'EMAIL_POSTAGEAPP',
    'expected_delivery_datetime'  => 'getutcdate()',
    'next_attempt_datetime'       => 'getutcdate()',
    'messaging_queue_params'      => $messaging_queue_params
  );

  $params['messaging_queue_params']['template_name'] = $template_name;

  return htt_messaging_queue_add( $params );
}

function enqueue_immediate_external_sms($customer_id,$message_type,$messaging_queue_params)
{
  // add a SMS_EXTERNAL to HTT_MESSAGING_QUEUE to be sent ASAP

  $params = array(
    'customer_id'                 => $customer_id,
    'reason'                      => $message_type,
    'type'                        => 'SMS_EXTERNAL',
    'expected_delivery_datetime'  => 'getutcdate()',
    'next_attempt_datetime'       => 'getutcdate()',
    'messaging_queue_params'      => $messaging_queue_params
  );

  $params['messaging_queue_params']['message_type'] = $message_type;

  return htt_messaging_queue_add( $params );
}

function enqueue_once_immediate_external_sms($customer_id,$message_type,$messaging_queue_params,$hours_period)
{
  // use enqueue_immediate_external_sms only if not already enqueued in the last $hours_period hours

  $htt_messaging_queue_select_query = htt_messaging_queue_select_query(
    array(
      'sql_limit'            => 1,
      'customer_id'          => $customer_id,
      'type'                 => 'SMS_EXTERNAL',
      'reason'               => $message_type,
      'created_datetime_gte' => 'DATEADD(hh, -'.$hours_period.', getutcdate())' // now minus $hours_period hours
    )
  );

  $queue_data = mssql_fetch_all_objects(logged_mssql_query($htt_messaging_queue_select_query));

  if ( $queue_data && is_array($queue_data) && count($queue_data) )
  {
    dlog('',"$message_type message already sent");

    return make_ok_Result(NULL);
  }
  else
  {
    dlog('',"$message_type message will be enqueued to be sent immediately");

    return enqueue_immediate_external_sms($customer_id,$message_type,$messaging_queue_params);
  }
}

function enqueue_once_immediate_email($customer_id,$message_type,$messaging_queue_params,$hours_period)
{
  // use enqueue_immediate_email only if not already enqueued in the last $hours_period hours

  $htt_messaging_queue_select_query = htt_messaging_queue_select_query(
    array(
      'sql_limit'            => 1,
      'customer_id'          => $customer_id,
      'type'                 => 'EMAIL_POSTAGEAPP',
      'reason'               => $message_type,
      'created_datetime_gte' => 'DATEADD(hh, -'.$hours_period.', getutcdate())' // now minus $hours_period hours
    )
  );

  $queue_data = mssql_fetch_all_objects(logged_mssql_query($htt_messaging_queue_select_query));

  if ( $queue_data && is_array($queue_data) && count($queue_data) )
  {
    dlog('',"$message_type EMAIL_POSTAGEAPP message already sent");

    return make_ok_Result(NULL);
  }
  else
  {
    dlog('',"$message_type EMAIL_POSTAGEAPP message will be enqueued to be sent immediately");

    return enqueue_immediate_email($customer_id,$message_type,$messaging_queue_params);
  }
}

