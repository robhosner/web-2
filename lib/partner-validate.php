<?php

include_once('db.php');
include_once('session.php');
include_once('db/customers.php');
include_once('lib/util-common.php');
include_once('classes/Result.php');
include_once('lib/state_machine/aspider.php');
require_once 'Ultra/Lib/DB/Getter.php';

$validations_found = array();

function get_found_validation($validation_key)
{
  global $validations_found;

  if (!isset($validations_found[$validation_key])) return NULL;

  return $validations_found[$validation_key];
}

function fill_return($p, $command, $params, $ret=array())
{
  $def = $p['commands'][$command];

  $byname = array();
  $q = 0;

  foreach ($def['parameters'] as $pdef)
  {
    $byname[$pdef['name']] = $params[$q];
    $q++;
  }

  if ($ret instanceof Result)
  {
    $ret = $ret->to_array();
  }

  foreach ($def['returns'] as $rdef)
  {
    $rname = $rdef['name'];
    $rtype = $rdef['type'];

    if (!isset($ret[$rname]) || NULL == $ret[$rname])
    {
      if (isset($rdef['default']))
      {
        $ret[$rname] = $rdef['default'];
      }
      else if ($rname === 'partner_tag')
      {
        $i = 0;
        foreach ($def['parameters'] as $pdef)
        {
          if ($rname === $pdef['name']) /* it's partner_tag presumably */
          {
            $ret[$rname] = $params[$i];
            break;
          }
          $i++;
        }

        if (!isset($ret[$rname]))
        {
          $ret[$rname] = '0';
        }
      }
      else if ($rname === 'request')
      {
        $ret[$rname] = json_encode($byname);
      }
      else if (isset($rdef['sql']) || isset($rdef['sql_eval']))
      {
        if (isset($rdef['context']))
        {
          eval($rdef['context']);
        }

        if (isset($rdef['includes']))
        {
          foreach ($rdef['includes'] as $inc)
          {
            include_once($inc);
          }
        }

        $query = 'BUG BUG BUG: notify Ultra of this error';
        if (isset($rdef['sql']))
        {
          $query = stringExpandDangerous($rdef['sql'], $byname);
        }
        else if (isset($rdef['sql_eval']))
        {
          $code = 'return ' . $rdef['sql_eval'];
          /* dlog('', "Executed code '$code' to fill in query for $rname"); */
          $query = eval_dangerous($code, $byname);
        }

        /* dlog('', "Filling in sql/sql_eval return $rname with $query"); */

        $data = mssql_fetch_all_objects(logged_mssql_query($query));

        $success = $data && is_array($data) && count($data) > 0;

        if (!$success)
        {
          $ret['errors'][] = "ERR_API_INTERNAL: data not found";
        }

        $ret[$rname] = $data;
      }
      /* start type defaults */
      else if ($rtype === 'integer' || $rtype === 'real')
      {
        $ret[$rname] = 0;
      }
      else if ($rtype === 'string')
      {
        $ret[$rname] = '';
      }
      else if ($rtype === 'string[]' || $rtype === 'integer[]' || $rtype === 'real[]' || $rtype === 'array')
      {
        $ret[$rname] = array();
      }
      else if ($rtype === 'epoch')
      {
        $ret[$rname] = time();
      }
      else if ($rtype === 'boolean')
      {
        $ret[$rname] = FALSE;
      }
      else
      {
        dlog('', "Uh-oh, unknown $command return type $rtype.  Leaving it as is.");
      }
    }
  }

  return $ret;
}

function validate_params($p, $command, $params, $mock=FALSE)
{
  global $debug;
  
  $errors = array();

  if (!verify_request_source())
  {
    /* TODO: enable this for real */
    dlog('', "ERR_API_ACCESS_DENIED: $command denied access from " . getip());

    $errors[] = "ERR_API_ACCESS_DENIED: denied access from " . getip();
    return $errors;
  }

  $def = $p['commands'][$command];
  $i = 0;
  foreach ($def['parameters'] as $pdef)
  {
    $db_validations = array();
    $validation = is_array($pdef['validation']) ? $pdef['validation'] : array();
    $type = $pdef['type'];
    $value = $params[$i];
    $pname = $pdef['name'];
    $required = $pdef['required'];

    if ($debug || $mock)
    {
      dlog('', sprintf("$command:$i: Looking at %s = %s %s (req: %s) [%s] [%s]",
                        $value,
                        $pname,
                        $type,
                        $required ? 'required' : 'no',
                        $pdef['desc'],
                        $validation ? str_replace(array('{','}'),'',json_encode($validation)) : 'none'));
    }

    if ($mock)
    {
      // pass all validations!  note above we log the values
    }
    else if ($required && NULL == $value)
    {
      $errors[] = "ERR_API_INVALID_ARGUMENTS: missing required $pname";
    }
    else if (!$required && NULL == $value)
    {
      // do nothing, the parameter is not required
    }
    else // value is not NULL
    {
      teldata_change_db();

      dlog(array('f' => 'validations', 'brief' => TRUE),
           'Validating parameter %s (%s) [%s]', $pname, $type, $value);
      switch ($type)
      {
      case 'string':
        if ($value != NULL && is_string($value))
        {
        }
        else if ($required)
        {
          $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a $type";
        }
        break;

      case 'string[]':
        if ($value != NULL && is_array($value))
        {
          foreach ($value as $s)
          {
            if ($s != NULL && is_string($s))
            {
            }
            else
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname contains a non-string";
            }
          }
        }
        else if ($required)
        {
          $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not an array";
        }
        break;

      case 'real':
        $test = 0+$value;
        if ( preg_match('/^[\d\.\-\+]+$/', $value) && ($value != NULL) && abs("$test"-"$value") < 0.00001 ) /* TODO: a smaller epsilon would probably be prudent */
        {
        }
        else if ($required)
        {
          $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a $type";
        }
        break;

      case 'integer':
      case 'epoch':
        if ($value != NULL && (!preg_match('/^\d+$/', $value)) )
        {
          $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a $type";
        }
        break;
             
      case 'boolean':
        //only accept 0 or 1 as a valid boolean
        if ($value != NULL && ($value == 0 || $value == 1) )
        {
        }
        else if ($required)
        {
          $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a $type (must be 0 or 1)";
        }
        break;

      case 'zsession':
        // use the zsession cookie unless a password is provided
        if (array_key_exists("zsession", $_COOKIE) &&
            ! array_key_exists("zsession", $_REQUEST))
        {
          $_REQUEST['zsession'] = $_COOKIE['zsession'];
        }

        if (! array_key_exists("zsession", $_REQUEST))
        {
          $_REQUEST['zsession'] = $zsession;
        }

        if (array_key_exists("zsessionC", $_COOKIE) &&
            ! array_key_exists("customer", $_REQUEST))
        {
          $_REQUEST['customer'] = $_COOKIE['zsessionC'];
        }

        if (! array_key_exists("customer", $_REQUEST))
        {
          $_REQUEST['customer'] = -1;
        }

        $verified = array_key_exists("zsession", $_REQUEST) ? verify_session() : NULL;

        $zsession = $verified[2];
        $customer_locator = $verified[0];

        $customer_full = NULL;

        if (is_numeric($customer_locator))
        {
          $customer_full = find_customer(make_find_customer_query_anycosid($customer_locator));
        }
        else
        {
          $customer_full = find_customer(make_find_customer_query_anycosid(-1, $customer_locator));
        }

        save_zsession_customer($customer_full);

        if (NULL == $customer_full)
        {
          $errors[] = "AUTH_NOZSESSION: you need a valid zsession to login";
        }

        break;

      default:
        $errors[] = "ERR_API_INTERNAL: unknown validation type $type";
        break;
      }

      foreach ($validation as $vk => $vv)
      {
        dlog(array('f' => 'validations', 'brief' => TRUE),
             'Validating [%s] [%s] on %s', $vk, $vv, $value);
        switch ($vk)
        {

        // validates against active plans in configuration
        case 'active_plan':
          $planConfig = \PlanConfig::Instance();
          $activePlans = $planConfig->getActivePlansAka();

          if ($vv == 2)
            $activePlans = array_merge($activePlans, $planConfig->getActiveLongNames());

          if ( ! in_array($value, $activePlans))
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname has no matches";
          break;

        // validates against all plans in configuration
        case 'plan_exists':
          $planConfig = \PlanConfig::Instance();
          $allPlans   = $planConfig->getAllPlansAka();
          if ( ! in_array($value, $allPlans))
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname has no matches";
          break;
        
        case 'matches':
          if ($vv && is_array($vv) && in_array($value, $vv))
          {
          }
          else
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value has no matches";
          }
          break;
          
          case 'numeric':
         
              if ($vv === 'integer')
              {
                  if ( !preg_match('/^\d+$/', $value ))
                  {
                     $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails integer format validation, must be a string containing only digits";
                  }
          
              } 
              elseif ($vv === 'float') 
              {    // Checks flaoting point and scientific notation
          
                  if ($value != NULL && (! is_float($value)))         // Floating point and Scientific notation allowed
                  {
                      $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails float format validation";
                  }
              } 
              elseif ($vv === 'currency_US')     // Checks for valid currency values,the "$" is optional
              {
                  if ($value != NULL &&
                  (! preg_match(
                          '/^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$/',
                          $value)))
                  {
                      $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails USA Currency format validation";
                  }
              } 
              else
              {
                  $errors[] = "ERR_API_INTERNAL: Unknown validation $vv for $pname";
              }
          
        break;

        case 'regexp':
          if ($vv && preg_match("/$vv/", $value))
          {
          }
          else
          {
            if ( $vv == '^[A-Za-z0-9]+$' )
            { $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value must contain only digits and letters";    }
            else
            { $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is not valid (fails regular expression)"; }
          }
          break;

        case 'zipcode':
          if ($vv === 'US')
          {
            if (preg_match("/^[0-9]{5}$/", $value)) // { "zipcode": "US" }
            {
            }
            else
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is not a '$vv' zipcode";
            }
          }
          elseif ($vv === 'in_coverage') // { "zipcode": "in_coverage" }
          {
            include_once('db/htt_coverage_info.php');

            if (!mssql_has_rows(htt_coverage_info_select_query(
                                  array('zip_code' => $value))))
            {
              $errors[] = "ERR_API_INVALID_ZIP: ZIP Code $value is not in the Ultra Footprint. Choose a valid Zip.";
            }
          }
          break;

        case 'date_format':
          if ($vv === 'dd-mm-yyyy')
          {
            if ( preg_match("/^(\d\d)\-(\d\d)\-(\d\d\d\d)$/", $value, $matches) )
            {
              if ( ( $matches[1] < 1 ) || ( $matches[1] > 31 ) || ( $matches[2] < 1 ) || ( $matches[2] > 12 ) || ( $matches[3] <= 0 ) )
              {
                $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a valid date";
              }
            }
            else
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a date in the format dd-mm-yyyy";
            }
          }
          elseif ($vv === 'mm-dd-yyyy')
          {
            if ( preg_match("/^(\d\d)\-(\d\d)\-(\d\d\d\d)$/", $value, $matches) )
            {
              if ( ( $matches[1] < 1 ) || ( $matches[1] > 12 ) || ( $matches[2] < 1 ) || ( $matches[2] > 31 ) || ( $matches[3] <= 0 ) )
              {
                $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a valid date";
              }
            }
            else
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not a date in the format mm-dd-yyyy";
            }
          }
          else
          {
            $errors[] = "ERR_API_INTERNAL: $pname validation type $vv is unknown!";
          }
          break;

        case 'min_strlen':
          if (strlen($value) >= $vv)
          {
          }
          else
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is shorter than min_strlen";
          }
          break;

        case 'max_strlen':
          if (strlen($value) <= $vv)
          {
          }
          else
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is longer than max_strlen";
          }
          break;

        case 'min_value':
          if ($value >= $vv)
          {
          }
          else
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is under min_value";
          }
          break;

        case 'max_value':
          if ($value <= $vv)
          {
          }
          else
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is over max_value";
          }
          break;

        case 'max_seconds_age':
          /* note the window extends forwards too */
          if (abs($value - time()) < $vv)
          {
          }
          else
          {
            //$errors[] = "$vv diff is " . ($value - time());
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value is older than max_seconds_age; now is " . time();
          }
          break;

        case 'format':
          if ($vv === 'JSON')
          {
            if (NULL == json_decode($value))
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname is not valid $vv!";
            }
          }
          else
          {
            $errors[] = "ERR_API_INTERNAL: $pname validation type $vv is unknown!";
          }
          break;

        case 'verify_luhn':
          if ($vv && luhn_checksum($value) == $vv)
          {
          }
          else
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails the LUHN checksum";
          }
          break;

        case 'verify_luhn_substr':
          if ($vv && luhn_checksum(substr($value, 0, $vv)) == substr($value, $vv))
          {
          }
          else
          {
            dlog('', "ERR_API_INVALID_ARGUMENTS: $pname value fails the LUHN/$vv checksum" . '/' . luhn_checksum(substr($value, 0, $vv)) . '/' . substr($value, $vv));
            /* $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails the LUHN/$vv checksum" . '/' . luhn_checksum(substr($value, 0, $vv)) . '/' . substr($value, $vv); */
            /* $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails the LUHN/$vv checksum"; */
          }
          break;

        case 'promo_status': # { "promo_status": "STATUS" }  or  { "promo_status": ["VIRGIN","PREPARATION"] }
          $promoStatusValidateResult = validate_ultra_promotional_plans_status( $value , $vv );

          if ( ! $promoStatusValidateResult->is_success() )
          {
            $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails status validation";
          }
          break;

        case 'actcode':
          if ( $vv === 'valid_actcode' ) # { "actcode": "valid_actcode" }
          {
            // ActCodes are 11 digits, with the 11th digit being a checkdigit - the usual LUHN algorithm check.
            // ActCodes are always provided with all 11 digits.
            // Actcodes will ALWAYS be prepended by [2-9]
            if ( ! preg_match("/^[2-9]\d{10}/", $value) )
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails Actcode format validation";
            }
            elseif( ! validate_act_code( $value ) )
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname value fails LUHN checksum";
            }
          }
          else
          {
            $errors[] = "ERR_API_INTERNAL: Unknown validation $vk ,  $vv for $pname";
          }
          break;
          
        case 'iccid':
          if($vv === 'type_purple') # { "iccid": "type_purple" }
          {
            $validateResult = validate_sim_product_type('PURPLE',$value);
            if ( $validateResult->is_failure() )
            {
              $validateErrors = $validateResult->get_errors();
              if ( is_array($validateErrors) && count($validateErrors) && ( $validateErrors[0] == "ERR_API_INVALID_ARGUMENTS: Incorrect ICCID Product type" ) )
                $errors[] = "ERR_API_PRODUCT: This SIM is not valid for your current plan.";
            }
          }
          elseif($vv === 'activate_type_purple') # { "iccid": "activate_type_purple" }
          {
            $validateResult = validate_sim_product_type('PURPLE',$value);
            if ( $validateResult->is_failure() )
            {
              $validateErrors = $validateResult->get_errors();
              if ( is_array($validateErrors) && count($validateErrors) && ( $validateErrors[0] == "ERR_API_INVALID_ARGUMENTS: Incorrect ICCID Product type" ) )
                $errors[] = "ERR_API_PRODUCT: This SIM cannot be activated by this process.";
            }
          }
          else
          {
            $errors[] = "ERR_API_INTERNAL: Unknown validation $vk ,  $vv for $pname";
          }
          break;

        case 'msisdn':
          if ($vv === 'portin_eligible') # { "msisdn": "portin_eligible" }
          {
            if ( is_porting_disabled() )
            {
              $errors[] = "ERR-API-UNAVAILABLE : Porting is temporarily unavaiable.";
            }
            else
            {
              $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

              $result = $mwControl->mwPortInEligibility(
                array(
                  'actionUUID' => getNewActionUUID('validate ' . time()),
                  'msisdn'     => normalize_msisdn($value, TRUE)
                )
              );

              if ( $result && $result->is_success() && $result->get_data_key('eligible') )
              { }
              else
              {
                $errors = array_merge( $errors , $result->get_errors() );
                $errors[] = "ERR_API_INVALID_ARGUMENTS: $pname $value is not eligible for PortIn";
              }
            }
          }
          else if ($vv === 'always_fail')
          {
            $errors[] = "ERR-API-UNAVAILABLE : Porting is temporarily unavaiable.";
          }
          else
          {
            $errors[] = "ERR_API_INTERNAL: Unknown validation $vk , $vv for $pname";
          }
          break;

        case 'customer_exists':
        case 'mfunds_exists':
        case 'transition_exists':
        case 'portin_exists':

          if ( ( $vv == 'msisdn' ) && ( $vk == 'portin_exists' ) )
          {
            // determine MVNE2 for MSISDN $value
            $customers = get_ultra_customers_from_msisdn($value,array('customer_id','MVNE'));

            // check PORTIN_QUEUE
            $portInQueue = new \PortInQueue();

            $loadByMsisdnResult = $portInQueue->loadByMsisdn( $value );

            if ( $loadByMsisdnResult->is_failure() )
              $errors[] = "ERR_API_INVALID_ARGUMENTS: [msisdn] fails portin_exists";

            break;
          }

          $column = $vv;
          $extra_clauses = array();

          if (is_array($vv))
          {
            $column = array_shift($vv);
            $extra_clauses = $vv;
          }

          $db_validations[$vk][] = array("column" => $column,
                                         "value" => $value,
                                         "extra_clauses" => $extra_clauses,
                                         "parameter" => $pname,
                                         "required" => $required);
          break;

        case 'MVNE': // AMDOCS-122: MVNE validation

          if ( $value && is_numeric($value) )
          {
            $mvne = NULL;

            if ( ( strlen($value) > 17 ) && ( strlen($value) < 20 ) )
              // ICCID (18 or 19 digits)
              $mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', luhnenize($value), 'MVNE');
            elseif ( ( strlen($value) > 9 ) && ( strlen($value) < 12 ) )
              // MSISDN (10 or 11 digits)
              $mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', normalize_msisdn_10($value), 'MVNE');
            elseif ( strlen($value) <= 9 )
              // CUSTOMER_ID
              $mvne = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $value, 'MVNE');

           if ($mvne && $vv != $mvne)
             $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given '.$pname.' does not belong to MVNE'.$vv;
          }

          break;

        case 'PIN': // INCOM-7: PIN validataion

          // check if string contains only digits
          if (! ctype_digit($value))
            $errors[] = 'ERR_API_INVALID_ARGUMENTS: invalid PIN';

          // check against multiple possible lengths
          elseif (is_array($vv))
          {
            if (! in_array(strlen($value), $vv))
              $errors[] = 'ERR_API_INVALID_ARGUMENTS: invalid PIN format';
          }

          else // check against a single length
          {
            if ($vv != strlen($value))
              $errors[] = 'ERR_API_INVALID_ARGUMENTS: invalid PIN format';
          }

          break;

        default:
          $errors[] = "ERR_API_INTERNAL: unknown validation constraint $vk";
          break;
        }
      }
    }

    if ($db_validations && count($db_validations) > 0)
    {
      $underscoreObject = new __ ;

//TODO: remove as per EA-47
      \logInfo('TO BE REMOVED AS PER EA-47 '.json_encode($db_validations));

      teldata_change_db();
      foreach ($db_validations as $dbk => $dbv)
      {
        $table = 'BUG BUG BUG UNKNOWN_TABLE';
        if ($dbk === 'customer_exists')
        {
          $table = 'customers';
        }
        else if ($dbk === 'mfunds_exists')
        {
          $table = 'htt_mfunds_applications';
        }
        else if ($dbk === 'transition_exists')
        {
          $table = 'htt_transition_log';
        }
        else if ($dbk === 'portin_exists')
        {
          $table = 'htt_portin_log';
        }

        $where = array();
        foreach ($dbv as $check)
        {

          if ( preg_match('/^\d+$/',$check['value']) )
          {
            # if the value is numeric, we must not use clause_mssql_escape_or_null

            $where[] = sprintf("%s = '%d'",
                               $check['column'],
                               $check['value']);
          }
          else
          {
            $where[] = sprintf("%s %s",
                               $check['column'],
                               clause_mssql_escape_or_null($check['value']));
          }

          foreach ($check['extra_clauses'] as $extra)
          {
            $where[] = $extra;
          }
        }

        $where_clause = '';
        if ( count($where) > 0 )
          $where_clause = ' WHERE ' . implode(" AND ", $where);

        $query = "SELECT TOP 1 * FROM $table $where_clause";

        $found = find_first($query);
        if ($found)
        {
          global $validations_found;
          $validations_found[$dbk] = $found;
        }
        else
        {
          $pnames = implode(", ", $underscoreObject->pluck($dbv, 'parameter'));

          $errors[] = "ERR_API_INVALID_ARGUMENTS: [$pnames] fails $dbk";
        }
      }
    }

    $i++;
  }

  return $errors;
}

function validate_requires($context, $requirements, $customer, $explicit=FALSE)
{
  if (!isset($context['customer_id']))
  {
    dlog('', "ERROR: No customer ID available");
    return FALSE;
  }

  if (NULL == $customer)
  {
    dlog('', "ERROR: No customer available");
    return FALSE;
  }

  $balance = $customer->BALANCE;
  $whole_value = $balance + $customer->stored_value;
  $plan_renewal_due = 1;                /* TODO */

  dlog('transitions',
    "Checking prerequisites for customer ID %d (balance %s, whole value %s): %s",
    $context['customer_id'],
    $balance,
    $whole_value,
    json_encode($requirements));

  $check = TRUE;
  foreach ($requirements as $rname => $rval)
  {
    if (!$check) break;

    if ('whole_value_min' === $rname)
    {
      $check = $whole_value >= $rval;
      if ( ! $check ) { dlog('', "PREREQUISITE stop: whole_value_min ($whole_value,$rval)"); }
    }
    else if ('whole_value_under' === $rname)
    {
      $check = $whole_value < $rval;
      if ( ! $check ) { dlog('', "PREREQUISITE stop: whole_value_under ($whole_value,$rval)"); }
    }
    else if ('balance_min' === $rname)
    {
      $check = $balance >= $rval;
      if ( ! $check ) { dlog('', "PREREQUISITE stop: balance_min ($balance,$rval)"); }
    }
    else if ('plan_renewal_due' === $rname)
    {
      $check = (! ! $rval == ! ! $plan_renewal_due);
      if ( ! $check ) { dlog('', "PREREQUISITE stop: plan_renewal_due ($rval,$plan_renewal_due)"); }
    }
    else if ('has_shipping_address' === $rname)
    {
      $check = ! ! customer_has_shipping_address( $customer );
      if ( ! $check ) { dlog('', "PREREQUISITE stop: has_shipping_address"); }
    }
    else if ('last_shipped_days_ago_min' === $rname)
    {
      $check = ! ! validate_last_shipped_days_ago_min( $context['customer_id'] , $rval );
      if ( ! $check ) { dlog('', "PREREQUISITE stop: last_shipped_days_ago_min"); }
    }
    else if ('sim_shipments_max' === $rname)
    {
      $check = ! ! validate_sim_shipments_max( $context['customer_id'] , $rval );
      if ( ! $check ) { dlog('', "PREREQUISITE stop: sim_shipments_max"); }
    }
    else if ('debug' === $rname)
    {
      global $debug;
      dlog('transitions', "Checking debug ($debug ?= $rval)");
      $check = $rval == $debug;
    }
    else if ('explicit' === $rname)
    {
      dlog('transitions', "Checking explicit '$context[explicit]' == $rval");
      $check = isset($context['explicit']) && $rval == $context['explicit'];
    }
    else if ('credential_off' === $rname)
    {
      dlog('transitions', "Checking that credential $rval is missing or false");
      $check = ! find_credential($rval);
    }
    else if ('credential_on' === $rname)
    {
      dlog('transitions', "Checking that credential $rval is true");
      $check = ! ! find_credential($rval);
    }
    else
    {
      dlog('', "ERROR: Unknown transition requirements $rname");
      return FALSE;
    }

    ## if ( ! $check ) { dlog('', "Requirement fail for $rname : $rval"); }
    ## if (   $check ) { dlog('', "Requirement  OK  for $rname : $rval"); }
  }

  ## dlog('', "Returned: " . json_encode($check));

  dlog('transitions',
       "Returned: %s", json_encode($check));

  return $check;
}

$zsession_saved_customer = NULL;

function save_zsession_customer($customer_full)
{
  global $zsession_saved_customer;
  $zsession_saved_customer = $customer_full;
}

function get_zsession_customer()
{
  global $zsession_saved_customer;
  return $zsession_saved_customer;
}

function customer_has_shipping_address( $customer )
{
  require_once("fields.php");

  $fields = array(
    'shipping_address1'        => $customer->ADDRESS1,
    'shipping_address2'        => $customer->ADDRESS2,
    'shipping_city'            => $customer->CITY,
    'shipping_state_or_region' => $customer->STATE_REGION,
    'shipping_postal_code'     => $customer->POSTAL_CODE,
    'shipping_country'         => $customer->COUNTRY,
    'account_first_name'       => $customer->FIRST_NAME,
    'account_last_name'        => $customer->LAST_NAME
  );

  return validate_fields($fields);
}

