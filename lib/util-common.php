<?php

// general purpose code which could be shared between heterogeneous applications

include_once 'UltraLogger.php';
include_once 'db/customers.php';
include_once 'db/htt_shipwire_log.php';
include_once 'lib/state_machine/functions.php';
include_once 'lib/underscore/underscore.php';
include_once 'Ultra/Lib/DB/Merchants/Tokenizer.php';
require_once 'Ultra/Lib/Util/Locale.php';
require_once 'Ultra/Lib/Util/Redis.php';
include_once 'Ultra/UltraConfig.php';
require_once 'Ultra/BrandConfig.php';
require_once 'Ultra/MvneConfig.php';


// Needed for dlog and accessing current configuration


define('MSSQL_DATE_FORMAT', 'M d Y h:i:sA');
define('AMERICAN_DATE_FORMAT', 'm/d/y h:i:sA');

global $ultra_logger_object;

/**
 * json_decode_nice
 *
 * Adjust JSON format and decode
 *
 * @param string $json
 * @param string $assoc
 * @return array
 */
function json_decode_nice($json, $assoc = FALSE)
{
  $json = str_replace(array(
    "\n","\r"
  ), "", $json);
  $json = preg_replace('/([{,])(\s*)([^"]+?)\s*:/', '$1"$3":', $json);
  return json_decode($json, $assoc);
}

/**
 * indent__JSON
 *
 * indents json string
 * http://www.daveperrett.com/articles/2008/03/11/format-json-with-php/
 *
 * @param string $json
 * @return string
 */
function indent__JSON($json)
{
  $result = '';
  $pos = 0;
  $strLen = strlen($json);
  $indentStr = '  ';
  $newLine = "\n";
  $prevChar = '';
  $outOfQuotes = true;

  for ($i = 0; $i <= $strLen; $i ++)
  {

    // Grab the next character in the string.
    $char = substr($json, $i, 1);

    // Are we inside a quoted string?
    if ($char == '"' && $prevChar != '\\')
    {
      $outOfQuotes = ! $outOfQuotes;

      // If this character is the end of an element,
      // output a new line and indent the next line.
    }
    else
      if (($char == '}' || $char == ']') && $outOfQuotes)
      {
        $result .= $newLine;
        $pos --;
        for ($j = 0; $j < $pos; $j ++)
        {
          $result .= $indentStr;
        }
      }

    // Add the character to the result string.
    $result .= $char;

    // If the last character was the beginning of an element,
    // output a new line and indent the next line.
    if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes)
    {
      $result .= $newLine;
      if ($char == '{' || $char == '[')
      {
        $pos ++;
      }

      for ($j = 0; $j < $pos; $j ++)
      {
        $result .= $indentStr;
      }
    }

    $prevChar = $char;
  }

  return $result;
}

/**
 * enclose_JSON
 *
 * enclose json in array
 *
 * @param string $data
 * @return string
 */
function enclose_JSON($data)
{
  return json_encode(array(
    $data
  ));
}

/**
 * disclose__JSON
 *
 * release json from array
 *
 * @param string $enc enclosed json
 * @return string
 */
function disclose_JSON($enc)
{
  if (NULL == $enc)
    return NULL;

  $val = json_decode($enc, 1);
  if (is_array($val) && isset($val[0]))
  {
    return $val[0];
  }

  return NULL;
}

/**
 * dlog_allowed_regexp
 *
 * @return string - global $debug or NULL
 */
function dlog_allowed_regexp()
{
  // TODO: this should return from an environment variable or a "cfengine config thing"

  // suggested tags:
  // ERROR , SHOULDNOTHAPPEN , QUERY , INVENTORY , PAYMENTS , CUSTOMERCARE , PORTAL , INTERNAL , ...
  global $debug;

  if (! isset($debug))
    return NULL;
  if (isset($debug))
    return $debug;

  return '.';
}

/**
 * dlog_allowed
 *
 * returns whether dlog configuration is allowed
 *
 * @param string $dlog_config
 * @return boolean
 */
function dlog_allowed($dlog_config)
{
  global $e_config;

  // log 'DEBUG' and 'TRACE' only if the environment is configured as a master branch
  if ( in_array( $dlog_config , array( 'DEBUG','TRACE' ) ) )
    return ! ! preg_match("/master/", $e_config['git/branch'] ) ;

  if ( is_null($dlog_config) || ( $dlog_config === '' ) || ( $dlog_config === '_ALWAYS_LOG_' ) )
    return TRUE;

  // determined if dlog is activated
  $dlog_allowed_regexp = dlog_allowed_regexp();

  return ! ! ( ! is_array($dlog_config) && isset($dlog_allowed_regexp) && preg_match("/$dlog_allowed_regexp/", $dlog_config) );
}

/**
 * getReferringIPAddress
 *
 * @return string -- IP Address, even if behind a proxy
 */
function getReferringIPAddress()
{
  $IP = '';

  if (getenv('HTTP_CLIENT_IP'))
    $IP = getenv('HTTP_CLIENT_IP');
  elseif (getenv('HTTP_X_FORWARDED_FOR'))
    $IP = getenv('HTTP_X_FORWARDED_FOR');
  elseif (getenv('HTTP_X_FORWARDED'))
    $IP = getenv('HTTP_X_FORWARDED');
  elseif (getenv('HTTP_FORWARDED_FOR'))
    $IP = getenv('HTTP_FORWARDED_FOR');
  elseif (getenv('HTTP_FORWARDED'))
    $IP = getenv('HTTP_FORWARDED');
  else
    $IP = "";

  return $IP;
}

function logit( $message )
{
  dlog('', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function logFatal( $message )
{
  dlog('', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function logError( $message )
{
  dlog('', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function logWarn( $message )
{
  dlog('', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function logInfo( $message )
{
  dlog('', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function logDebug( $message )
{
  dlog('DEBUG', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function logTrace( $message )
{
  dlog('TRACE', build_log_preface( debug_backtrace() ) . $message);
  return NULL;
}

function build_log_preface( $trace )
{
  $preface = "main::() ";
  if ($trace && count($trace) > 1)
  {
    $caller = $trace[1];
    $preface = (isset($caller['class']) ? "{$caller['class']}::" : '') . $caller['function'] . "(): ";
  }

  return $preface;
}

/**
 * dlog
 *
 * logs a message
 *
 * @param string $options
 * @param string $message
 */
function dlog($options, $message)
{
  global $ultra_logger_object;

  $loggerFlag = \Ultra\UltraConfig\loggerFlag();

  $args = func_get_args();

  $options = array_shift($args);
  $facility = '';

  // string to be appended to the log
  $output = FALSE;

  if (! is_array($options))
    $options = array( 'f' => $options );

  $facility = isset($options['f']) ? $options['f'] : '';
  $brief = isset($options['brief']) || in_array('brief', $options);

  // should we append $output to the log ?
  if (dlog_allowed($facility))
  {
    $trace = debug_backtrace();
    $preface = "main::() ";
    if ($trace && count($trace) > 1)
    {
      $caller = $trace[1];
      $preface = (isset($caller['class']) ? "{$caller['class']}::" : '') . $caller['function'] . "(): ";
    }

    if (is_array($args[0]) || is_object($args[0]))
      $args[0] = json_encode($args[0]);

    if (is_null($args[0]))
      $args[0] = enclose_JSON($args[0]);

    global $request_id;
    $request_prefix = '';

    if (isset($request_id))
      $request_prefix = $request_id . " ";

    $args[0] = $request_prefix . "[" . getmypid() . "] " . $preface . $args[0];

    /* error_log("dlog() calling sprintf() with parameters " . json_encode($args)); */
    if (count($args) > 1)
    {
      // encode any arrays with json_encode
      $encoded_args = array(
        array_shift($args)
      );

      while (count($args) > 0)
      {
        $arg = array_shift($args);

        if (is_array($arg) || is_object($arg))
        {
          if ( $loggerFlag == 2 )
            $arg = \toKeyValString($arg);
          else
            $arg = json_encode($arg);
        }

        if (is_null($arg))
          $arg = enclose_JSON($arg);

        if ($brief && strlen($arg) > 50)
          $arg = substr($arg, 0, 22) . '...' . substr($arg, - 22);

        $encoded_args[] = $arg;
      }

      $output = call_user_func_array('sprintf', $encoded_args);
    }
    else
      $output = $args[0];
  }

  // PROD-1782: preserve application time zone and restore default
  $appTZ = date_default_timezone_get();
  if ($appTZ != 'America/Los_Angeles')
    date_default_timezone_set('America/Los_Angeles');

  try
  {
    $loggerFlag = \Ultra\UltraConfig\loggerFlag();

    if ( $output )
    {
      if ( $loggerFlag ) // Logger is enabled
      {
        if ( empty( $ultra_logger_object ) || ! $ultra_logger_object )
          $ultra_logger_object = new UltraLogger(getReferringIPAddress(), FALSE);

        $ultra_logger_object->logDebug(cleanse_credit_card_string($output));
      }
      else // $loggerFlag 0 or not set: Log to error_log()
        error_log(cleanse_credit_card_string($output));
    }
  }
  catch (Exception $e)
  {
    error_log("FATAL LOGGING ERROR " . $e->getMessage() . " Original Error message: " . PHP_EOL . cleanse_credit_card_string($output));
  }
  
  // PROD-1782: restore application time zone if needed
  if ($appTZ != 'America/Los_Angeles')
    date_default_timezone_set($appTZ);
}

/**
 * cleanse_credit_card_string
 *
 * cleanses confidential information from credit card strings
 *
 * @param string $string
 * @return string
 */
function cleanse_credit_card_string($string)
{
  $string = preg_replace("/\"CC_NUMBER\"\:\"\!\w{32}\d\d\d\d\"/", '"CC_NUMBER":"!xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"', $string);

  $string = preg_replace("/\"creditCard\"\:\"?\d{15,16}\"?/", '""creditCard":"XXXXXXXXXXXXXXX"', $string);

  $string = preg_replace("/\"account_cc_number\"\:\"\d{15,16}\"/", '""account_cc_number":"XXXXXXXXXXXXXXX"', $string);

  $string = preg_replace("/xxxx xxxx xxxx \d\d\d\d/", 'xxxx xxxx xxxx xxxx', $string);

  $string = preg_replace("/xxxx xxxxxx x\d\d\d\d/", 'xxxx xxxxxx xxxxx', $string);

  $string = preg_replace("/(customercare__SetCustomerCreditCard.+\")\d{15,16}(\")/", '$1xxxxxxxxxxxxxxx$2', $string);

  $string = preg_replace("/('CC_NUMBER'.+\"field_value\"\:\")\d{15,16}(\")/", '$1xxxxxxxxxxxxxxx$2', $string);

  $string = preg_replace("/\d{15,16}(\<\\\\\/creditCard\>)/", 'xxxxxxxxxxxxxxx$1', $string);

  $string = preg_replace("/\d{15,16}(\<\/creditCard)/", 'xxxxxxxxxxxxxxx$1xx', $string);

  $string = preg_replace("/'\d+'\,\s+\-\-\s+cc_number/", 'xxxxxxxxxxxxxxx,', $string);

  if ( ! preg_match("/\"IMSI\"\:\"\d{11,12}(\d\d\d\d)\"/",$string)
    && ! preg_match("/\"IMEI\"\:\"\d{11,12}(\d\d\d\d)\"/",$string) )
    $string = preg_replace("/\"\d{11,12}(\d\d\d\d)\"/", 'xxxxxxxxxxxxxxx$1', $string);

  return $string;
}

/**
 * get_mcrypt_encrypt_key
 *
 * @return string
 */
function get_mcrypt_encrypt_key()
{
  $key = '_unsolocapitanodelpiero_';

  return $key;
}

/**
 * perform_mcrypt_encryption
 *
 * @param string $string
 * @return string
 */
function perform_mcrypt_encryption($input)
{
  $key = get_mcrypt_encrypt_key();

  $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $input, MCRYPT_MODE_CBC, md5(md5($key))));

  return $encrypted;
}

/**
 * perform_mcrypt_decryption
 *
 * @param string $string
 * @return string
 */
function perform_mcrypt_decryption($input)
{
  $key = get_mcrypt_encrypt_key();

  $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($input), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

  return $decrypted;
}

/**
 * hash_hmac_sha256
 *
 * Generate a keyed hash value using the HMAC method and the sha256 algorithm
 *
 * @param string $string to be hashed
 * @param string $hex_hmac_key hexadecimal key ( length 32 )
 * @return string
 */
function hash_hmac_sha256( $string , $hex_hmac_key )
{
  return hash_hmac( 'sha256' , $string , hex2bin($hex_hmac_key) );
}

/**
 * getNewTransitionUUID
 *
 * construct a new transition UUID
 *
 * @param string $namespace
 * @return string
 */
function getNewTransitionUUID($namespace = '')
{
  return getNewUUID('TX', $namespace);
}

/**
 * getNewActionUUID
 *
 * construct a new action UUID
 *
 * @param string $namespace
 * @return string
 */
function getNewActionUUID($namespace = '')
{
  return getNewUUID('AX', $namespace);
}

/**
 * getNewUUID
 *
 * construct a new UUID
 *
 * @param string $prefix
 * @param string $namespace
 * @param boolean $parentheses - whether to enclose in {}
 * @return string
 */
function getNewUUID($prefix, $namespace = '', $parentheses=TRUE)
{
  $uid = uniqid("", true);
  $data = $namespace;
  $data .= $_SERVER['REQUEST_TIME'];

  if (isset($_SERVER['HTTP_USER_AGENT']))
  {
    $data .= $_SERVER['HTTP_USER_AGENT'];
  }
  if (isset($_SERVER['REMOTE_ADDR']))
  {
    $data .= $_SERVER['REMOTE_ADDR'];
  }
  if (isset($_SERVER['REMOTE_PORT']))
  {
    $data .= $_SERVER['REMOTE_PORT'];
  }

  $hash = strtoupper(hash('ripemd128', $uid . md5($data)));

  $uuid = substr($hash, 0, 16) . '-' . substr($hash, 16, 32);

  return ( $parentheses )
         ?
         "{" . $prefix . "-" . $uuid . "}"
         :
         $prefix . "-" . $uuid
         ;
}

/**
 * create_guid
 *
 * public domain from http://php.net/manual/en/function.uniqid.php
 *
 * @param string $namespace
 * @return string
 */
function create_guid($namespace = '')
{
  $uid = uniqid("", true);
  $data = $namespace;
  $data .= $_SERVER['REQUEST_TIME'];

  if (isset($_SERVER['HTTP_USER_AGENT']))
  {
    $data .= $_SERVER['HTTP_USER_AGENT'];
  }
  if (isset($_SERVER['REMOTE_ADDR']))
  {
    $data .= $_SERVER['REMOTE_ADDR'];
  }
  if (isset($_SERVER['REMOTE_PORT']))
  {
    $data .= $_SERVER['REMOTE_PORT'];
  }

  $hash = strtoupper(hash('ripemd128', $uid . md5($data)));

  return '{' . substr($hash, 0, 8) . '-' . substr($hash, 8, 4) . '-' . substr($hash, 12, 4) . '-' . substr($hash, 16, 4) . '-' . substr($hash, 20, 12) . '}';
}

/**
 * luhn_checksum
 *
 * public domain from https://gist.github.com/1287893
 * echo luhn_checksum(890126084210225427); I get 9 but it should be 4
 *
 * @param int $number
 * @return int
 */
function luhn_checksum($number)
{
  $even = 0;
  $sum = 0;

  foreach (str_split(strrev($number)) as $n)
    $sum += ($even = ! $even) ? $n : (($n < 5) ? $n * 2 : ($n * 2) - 9);
  return $sum % 10;
}

/**
 * luhn_checksum_check
 *
 * returns whether luhn_checksum($number) == 0
 *
 * @param int $number
 * @return boolean
 */
function luhn_checksum_check($number)
{
  return luhn_checksum($number) == 0;
}

/**
 * luhn_append
 *
 * Append luhn checksum to a string
 *
 * @param int $input
 * @return string
 */
function luhn_append($input)
{
  $check_digit = luhn_checksum($input * 10);
  dlog('luhn', "Luhn input $input, check digit $check_digit");
  $check_digit = $check_digit == 0 ? $check_digit : (10 - $check_digit);
  dlog('luhn', "Luhn input $input, check digit2 $check_digit");
  return "$input$check_digit";
}

/**
 * This function checks the length, if = 18, computes and appends the checksum
 * otherwise returns the original value.
 *
 * @param string $iccid
 * @return string 19 character ICCID
 */
function luhnenize($iccid)
{
  if (strlen($iccid) == 18)
  {
    return luhn_append($iccid);
  }

  return $iccid;
}

/**
 * This function validates the 11th digit of the given $act_code.
 *
 * @param integer $act_code
 * @return boolean - is the $act_code valid?
 */
function validate_act_code($act_code)
{
  dlog('', "act_code    = $act_code");
  $act_code_10 = substr($act_code, 0, - 1);
  dlog('', "act_code_10 = $act_code_10");
  $check_digit = luhn_checksum($act_code_10 * 10);
  $check_digit = $check_digit == 0 ? $check_digit : (10 - $check_digit);
  dlog('', "check_digit = $check_digit");
  return ("$act_code" == "$act_code_10" . "$check_digit");
}

/* // Luhn (mod 10) algorithm */
/* function luhn_algorithm($input) */
/* { */
/*   $sum = 0; */
/*   $odd = strlen($input) % 2; */

/*   // Remove any non-numeric characters. */
/*   if (!is_numeric($input)) */
/*   { */
/*     eregi_replace("D", "", $input); */
/*   } */

/*   // Calculate sum of digits. */
/*   for($i = 0; $i < strlen($input); $i++) */
/*   { */
/*     $sum += $odd ? $input[$i] : (($input[$i] * 2 > 9) ? $input[$i] * 2 - 9 : $input[$i] * 2); */
/*     $odd = !$odd; */
/*   } */

/*   return ($sum % 10); */
/* } */

/**
 * curry
 *
 * public domain from http://commonphp.blogspot.com/2009/07/curry-in-php-53.html
 * $add20 = curry(function($a, $b){return $a + $b;}, 20);
 * echo $add20(5) . "\n"; #25
 *
 * @return function
 */
function curry()
{
  $args = func_get_args();
  $fn = array_shift($args);
  return function () use(&$fn, &$args)
  {
    $nargs = func_get_args();
    foreach ($nargs as $narg)
      $args[] = $narg;
    return call_user_func_array($fn, $args);
  };
}

/**
 * canonify
 *
 * [1234]AB[56]C => _1234_AB_56_C
 *
 * @return string
 */
function canonify($string)
{
  return preg_replace('/[^a-zA-Z0-9\']/', '_', $string);
}

/**
 * proc_open_wrapper
 *
 * adapted from http://www.php.net/manual/en/function.proc-open.php
 *
 * @param string $send
 * @param string $command
 * @return NULL or output
 */
function proc_open_wrapper($send, $command)
{
  //  @formatter:off
  $descriptorspec = array(
    0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
    1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
    2 => array("pipe", "w") // stderr
  );

  $cwd = '/tmp';
  $env = array(
     //'some_option' => 'aeiou';
  );
  // @formatter:on
  $process = proc_open($command, $descriptorspec, $pipes, $cwd, $env);

  $ret = NULL;

  if (is_resource($process))
  {
    stream_set_blocking($pipes[2], 0);
    if ($err = stream_get_contents($pipes[2]))
    {
      return NULL;
    }

    // $pipes now looks like this:
    // 0 => writeable handle connected to child stdin
    // 1 => readable handle connected to child stdout
    // 1 => writeable handle connected to child stderr

    fwrite($pipes[0], $send);
    fclose($pipes[0]);

    $ret = stream_get_contents($pipes[1]);
    fclose($pipes[1]);

    // It is important that you close any pipes before calling
    // proc_close in order to avoid a deadlock
    $return_value = proc_close($process);
  }

  return $ret;
}

/**
 * eval_dangerous
 *
 * @param string $code
 * @param array $vars key/value
 * @return result of evaluated $code
 */
function eval_dangerous($code, $vars)
{
  /* make every var usable as $varname */
  extract($vars);
  return eval($code);
}

/**
 * stringExpandDangerous
 *
 * public domain code form http://php.net/manual/en/language.types.string.php
 *
 * @param string $subject
 * @param array $vars
 * @param boolean $random
 * @return string
 */
function stringExpandDangerous($subject, array $vars = array(), $random = true)
{
  // extract $vars into current symbol table
  extract($vars);

  $delim = '';
  // if requested to be random (default), generate delim, otherwise use predefined (trivially faster)
  if ($random)
    $delim = '___' . chr(mt_rand(65, 90)) . chr(mt_rand(65, 90)) . chr(mt_rand(65, 90)) . chr(mt_rand(65, 90)) . chr(mt_rand(65, 90)) . '___';
  else
    $delim = '__ASDFZXCV1324ZXCV__'; // button mashing...

  // built the eval code
  $statement = "return <<<$delim\n\n" . $subject . "\n$delim;\n";

  // execute statement, saving output to $result variable
  $result = eval($statement);

  // if eval() returned FALSE, throw a custom exception
  if ($result === false)
    throw new EvalException($statement);

    // return variable expanded string
  return $result;
}

/**
 * debug_json_decode
 *
 * @return string last json_decode error
 */
function debug_json_decode()
{
  switch (json_last_error())
  {
    case JSON_ERROR_NONE:
      return FALSE;
    case JSON_ERROR_DEPTH:
      return ' Maximum stack depth exceeded';
    case JSON_ERROR_STATE_MISMATCH:
      return 'Underflow or the modes mismatch';
    case JSON_ERROR_CTRL_CHAR:
      return 'Unexpected control character found';
    case JSON_ERROR_SYNTAX:
      return 'Syntax error, malformed JSON';
    case JSON_ERROR_UTF8:
      return 'Malformed UTF-8 characters, possibly incorrectly encoded';
    default:
      return 'Unknown error';
  }
}

/**
 * mssql_escape_raw
 *
 * http://forums.phpfreaks.com/topic/119590-mssql-escape/
 *
 * @param string $data
 * @return string
 */
function mssql_escape_raw($data)
{
  return str_replace("'", "''", $data);
}

/**
 * mssql_escape
 *
 * TODO: revise these so when $data is empty they don't return '0x'
 *
 * @param string $data
 * @return string
 */
function mssql_escape($data)
{
  if (is_numeric($data))
    return $data;
  $unpacked = unpack('H*hex', $data);
  return '0x' . $unpacked['hex'];
}


/**
 * mssql_escape_with_zeroes
 * convert a text string into hexadecimal MSSQL representation to be used safely in dynamic SQL queries
 * @param String data to encode
 * @param Boolean TRUE to encode for Unicode MSSQL datatypes (nvarchar) or false non-unicode data types (char, varchar)
 * @returns String
 */
function mssql_escape_with_zeroes($data, $unicode = FALSE)
{
  // VYT @ 2015-08-10: Unicode types (such as nvarchar) must be first converted to double-byte represenation
  if ($unicode)
    $data = iconv('UTF-8', 'UCS-2', $data);

  $unpacked = unpack('H*hex', $data);
  return '0x' . $unpacked['hex'];
}


/**
 * mssql_escape_or_null
 *
 * @param string $data
 * @return string 'NULL' or escaped $data
 */
function mssql_escape_or_null($data)
{
  return $data == NULL ? 'NULL' : mssql_escape($data);
}

/**
 * clause_mssql_escape_or_null
 *
 * @param string $data
 * @return string ' IS NULL' or ' = ' . escaped $data
 */
function clause_mssql_escape_or_null($data)
{
  return $data == NULL ? ' IS NULL' : ' = ' . mssql_escape($data);
}

/**
 * get_htt_env
 *
 * @return string
 */
function get_htt_env()
{
  $env = getenv('HTT_ENV');
  // TODO: add a default value if $HTT_ENV is not defined?

  return $env;
}

/**
 * var_log
 *
 * var_dump($target) and log output buffer
 *
 * @param array $target
 */
function var_log($target)
{
  ob_start();
  var_dump($target);
  $ob_get_clean = ob_get_clean();
  if (isset($ob_get_clean))
  {
    dlog('', $ob_get_clean);
  }
}

/**
 * stack_log
 *
 * var_log on stack trace
 */
function stack_log()
{
  // ive us a trace baby - http://stackoverflow.com/questions/1423157/print-php-call-stack
  $e = new Exception();
  var_log($e->getTraceAsString());
}

/**
 * caption_amount
 *
 * Amount pretty printer
 *
 * @return string
 */
function caption_amount($amount)
{
  $chars = preg_split('//', $amount, - 1, PREG_SPLIT_NO_EMPTY);

  $n = count($chars);

  $chars[$n] = $chars[$n - 1];
  $chars[$n - 1] = $chars[$n - 2];
  $chars[$n - 2] = '.';

  $caption_amount = implode('', $chars);

  return $caption_amount;
}

/**
 * getTimeStamp
 *
 * Returns a timestamp in the format YYYY-MM-DDThh:mm:ss
 *
 * @return string
 */
function getTimeStamp()
{
  return date('Y-m-d\TH:i:s\Z');
}

/**
 * get_date_from_full_date
 *
 * @param String $date
 *          The date is in the format of "Oct 2 2012 08:39:38:233PM" returned by SQL Server
 * @return String Oct 2 2012
 */
function get_date_from_full_date($date)
{
  // Oct 2 2012 08:39:38:233PM => Oct 2 2012
  if (preg_match("/^(\w+)\s+(\d+)\s+(2\d\d\d)/", $date, $matches)) {
    return $matches[1] . " " . $matches[2] . " " . $matches[3];
  } else if (preg_match("/^(\d{4}+)[-](\d{2}+)[-](\d{2})/", $date, $matches)) {
    return (new \DateTime($date))->format('M d Y');
  } else {
    dlog('', 'No matches found.');
    return '';
  }
}

/**
 * fix_renewal_date
 *
 * @param String $date_input
 *          The date is in the format "Dec 08, 2012"
 * @return String date in in the format "Dec 8th"
 */
function fix_renewal_date($date_input)
{
  // Dec 08, 2012 => Dec 8th
  if (! $date_input)
  {
    return '';
  }

  preg_match("/^(\w\w\w) (\d)(\d)/", $date_input, $matches);

  $day = $matches[3];

  if ($matches[2] != 1)
  {
    if ( $day == 0 )
    {
      $day .= 'th';
    }
    elseif ($day < 2)
    {
      $day = '1st';
    }
    elseif ($day < 3)
    {
      $day = '2nd';
    }
    elseif ($day < 4)
    {
      $day = '3rd';
    }
    else
    {
      $day .= 'th';
    }
  }
  else
  {
    $day .= 'th';
  }

  if ($matches[2])
  {
    $day = $matches[2] . $day;
  }

  return $matches[1] . " " . $day;
}

/**
 * fix_transition_uuid_format
 *
 * @param String $request_id
 *          in the format "8D8BE06C-1C86-1AA0-9DA7-F67C941C83FA"
 * @return String $transition_uuid in in the format "{8D8BE06C-1C86-1AA0-9DA7-F67C941C83FA}"
 */
function fix_transition_uuid_format($request_id)
{
  // 8D8BE06C-1C86-1AA0-9DA7-F67C941C83FA => {8D8BE06C-1C86-1AA0-9DA7-F67C941C83FA}
  // TX-3816F9AF3EEE6BBB-5640A74675049529 => {TX-3816F9AF3EEE6BBB-5640A74675049529}
  $transition_uuid = $request_id;

  if (preg_match("/^\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12}$/", $transition_uuid))
    $transition_uuid = '{' . $transition_uuid . '}';
  elseif (preg_match("/^\w\w\-\w{16}\-\w{16}$/", $transition_uuid))
    $transition_uuid = '{' . $transition_uuid . '}';

  return $transition_uuid;
}

/**
 * get_customer_from_zsession
 *
 * @param String $zsession
 *          is a string we use for unique login sessions
 * @return Array $return is an array with keys : 'customer' (customer data object) ; 'errors' (array of errors - if any)
 */
function get_customer_from_zsession($zsession)
{
  $check_session = session_read($zsession);

  $return = array(
    'errors' => array()
  );

  dlog("", "get_customer_from_zsession check session = " . json_encode($check_session));

  $customer_string = $check_session[0];

  dlog("", "get_customer_from_zsession customer_string = $customer_string");

  $return['customer'] = get_customer_from_customer($customer_string);

  if ($return['customer'])
  {
    dlog("", "get_customer_from_zsession found a customer from $customer_string");

    $customer_id = $return['customer']->CUSTOMER_ID;
    $return['customer'] = get_customer_from_customer_id($customer_id);

    if (! $return['customer'])
    {
      $return['errors'][] = "ERR_API_INTERNAL: DB error (20)";
    }
  }
  else
  {
    $return['errors'][] = "ERR_API_INTERNAL: user not logged in";
  }

  return $return;
}

/**
 * validate_date_dd_mm_yyyy
 *
 * @param String $string
 *          is a string we wish to validate
 * @return Boolean $valid is true if the $string matches ^[0123]\d\-[01]\d\-[12][90]\d\d$/
 */
function validate_date_dd_mm_yyyy($string)
{
  $valid = (preg_match('/^[0123]\d\-[01]\d\-[12][90]\d\d$/', $string));

  return $valid;
}

/**
 * validate_port_account_number
 *
 * validation for port in account number
 *
 * @param string $string is a string we wish to validate
 * @return boolean $valid is true if the $string matches /^[A-Za-z0-9]+$/
 */
function validate_port_account_number($string)
{
  $valid = (preg_match('/^[A-Za-z0-9]+$/', $string));

  return $valid;
}

/**
 * validate_username_characters
 *
 * @param string $login we wish to validate
 * @return array $errors populated if $login does not match /^[A-Za-z0-9]+$/
 */
function validate_username_characters($login)
{
  $errors = array();

  if (! preg_match('/^[A-Za-z0-9]+$/', $login))
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_login login contains invalid characters";
  }

  return $errors;
}

/**
 * validate_unique_username
 *
 * verify that the username is unique
 *
 * @param string $login we wish to validate
 * @return array $errors populated if $login is not unique
 */
function validate_unique_username($login)
{
  $errors = array();

  $customers_select_query = customers_select_query(array(
    'login_name' => $login
  ));

  $query_result = mssql_fetch_all_objects(logged_mssql_query($customers_select_query));

  if ((is_array($query_result)) && count($query_result) > 0)
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_login duplicate login";
  }

  return $errors;
}

/**
 * validate_unique_e_mail
 *
 * @param string $email we wish to validate
 * @param string $mock
 * @return array $errors populated if $email is not unique
 */
function validate_unique_e_mail($email, $mock = '')
{
  $errors = array();

  if (preg_match("/allow_duplicate_email/", $mock))
    return $errors;

  // the environment allows duplicate emails
  if ((isset($e_config) && isset($e_config['f/ok_email_dup'])) ? $e_config['f/ok_email_dup'] : NULL)
    return $errors;

  $customers_select_query = customers_select_query(array(
    'e_mail' => $email
  ));

  $query_result = mssql_fetch_all_objects(logged_mssql_query($customers_select_query));

  if ((is_array($query_result)) && count($query_result) > 0)
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_number_email duplicate email address";

  return $errors;
}

/**
 * validate_last_shipped_days_ago_min
 *
 * checks that we don't have a row in HTT_SHIPWIRE_LOG in the last $last_days days
 *
 * @param int $customer_id
 * @param int $last_days
 * @return boolean $valid
 */
function validate_last_shipped_days_ago_min($customer_id, $last_days)
{
  $valid = FALSE;

  $htt_shipwire_log_select_query = htt_shipwire_log_select_query(array(
    'customer_id' => $customer_id,'sql_limit' => 1,'order_by' => 'LOG_DATE DESC','last_days' => $last_days
  ));

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_shipwire_log_select_query));

  if ((is_array($query_result)) && count($query_result) == 0)
  {
    $valid = TRUE;
  }

  return $valid;
}

/**
 * validate_sim_shipments_max
 *
 * checks that we don't have more than $max shipments already for this customer
 *
 * @param int $customer_id
 * @param int $max
 * @return boolean $valid
 */
function validate_sim_shipments_max($customer_id, $max)
{
  $valid = FALSE;

  $htt_shipwire_log_select_query = htt_shipwire_log_select_query(array(
    'customer_id' => $customer_id,'sql_limit' => $max + 1,'order_by' => 'LOG_DATE DESC'
  ));

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_shipwire_log_select_query));

  if ((is_array($query_result)) && count($query_result) < $max)
  {
    $valid = TRUE;
  }

  return $valid;
}

/**
 * validate_first_last_name
 *
 * validates whether first and last name are formatted correctly
 *
 * @param string $name
 * @return boolean $valid
 */
function validate_first_last_name($name)
{
  $valid = (preg_match("/^[A-Za-z\ \'\`]+$/", $name));

  dlog('', "validate_first_last_name valid = " . ($valid ? '1' : '0'));

  return $valid;
}

/**
 * get_customer_state
 *
 * @param object $customer
 * @return array $results
 */
function get_customer_state($customer)
{
  $results = array(
    'errors' => array(),'state' => ''
  );

  $customer_id = $customer->CUSTOMER_ID;

  if ( ! $customer_id )
    $customer_id = $customer->customer_id;

  $results['state'] = internal_func_get_state_from_customer_id( $customer_id );

  if ($results['state'])
  {
    $results['recharge_status'] = 'NONE';

    $plan_cost = get_plan_from_cos_id($customer->COS_ID);
    $plan_cost = substr($plan_cost, - 2); // L[12345]9 => [12345]9

    if (($results['state']['state'] == 'Suspended') || ($results['state']['state'] == 'Provisioned'))
    {
      $results['recharge_status'] = 'DUE';
    }
    else
      if (is_numeric($plan_cost) && (($customer->BALANCE + $customer->stored_value) >= ($plan_cost * 100)))
      {
        $results['recharge_status'] = 'PAID';
      }
      else
        if ($customer->monthly_cc_renewal == 1)
        {
          $results['recharge_status'] = 'AUTORENEW';
        }
  }
  else
    $results['errors'] = "ERR_API_INTERNAL: customer state could not be determined";

  return $results;
}

/**
 * amount_cents_to_dollars_string
 *
 * 12345 => $123.45
 *
 * @param string $amount
 * @return string
 */
function amount_cents_to_dollars_string($amount)
{
  $part1 = substr($amount, 0, strlen($amount) - 2);
  $part2 = substr($amount, strlen($amount) - 2);

  if (($part2 + 0) && ($part2 < 10))
  {
    $part2 = '0' . $part2;
  }
  if (! $part1)
  {
    $part1 = '0';
  }

  $amount_dollars = '$' . $part1 . '.' . $part2;

  return $amount_dollars;
}

/**
 * shipping_cost
 *
 * SIM Shipping Cost
 *
 * @return int
 */
function shipping_cost()
{
  return 995; // $9.95
}

/**
 * sim_cost
 *
 * Physical SIM cost
 *
 * @return int
 */
function sim_cost()
{
  return 0; // free for now
}

/**
 * get_customer_source
 *
 * @param int $activation_masteragent
 * @param int $activation_store
 * @param int $activation_userid
 * @return string
 */
function get_customer_source($activation_masteragent, $activation_store, $activation_userid)
{
  $customer_source = 'DEALER';

  if (($activation_masteragent == 54) && ($activation_store == 28) && ($activation_userid == 67))
  {
    $customer_source = 'CSIVR';
  }
  else
    if (($activation_masteragent == 54) && ($activation_store == 1032) && ($activation_userid == 1154))
    {
      $customer_source = 'CSCRM';
    }

  return $customer_source;
}

/**
 * find_data_recharge
 *
 * @param string $plan
 * @param string $data_soc_id
 * @return NULL or array
 */
function find_data_recharge($plan, $data_soc_id)
{
  $data_recharge = NULL;

  $data_recharge_by_plan = get_data_recharge_by_plan($plan);

  foreach ($data_recharge_by_plan as $data)
  {
    if ($data['data_soc_id'] == $data_soc_id)
    {
      $data_recharge = $data;
    }
  }

  return $data_recharge;
}

/**
 * find_voice_recharge
 *
 * example: find_voice_recharge('L19','50_100');
 *
 * @param string $plan
 * @param string $voice_soc_id
 * @return NULL or array
 */
function find_voice_recharge($plan, $voice_soc_id)
{
  $voice_recharge = NULL;

  $voice_recharge_by_plan = get_voice_recharge_by_plan($plan);

  foreach ($voice_recharge_by_plan as $voice)
  {
    if ($voice['voice_soc_id'] == $voice_soc_id)
    {
      $voice_recharge = $voice;
    }
  }

  return $voice_recharge;
}

/**
 * get_data_recharge_by_plan
 *
 * @param string $plan
 * @return array
 */
function get_data_recharge_by_plan($plan)
{
  $data_recharge_by_plan = array();

  $plan_name_from_short_name = get_plan_name_from_short_name($plan);

  if ($plan_name_from_short_name)
    $plan = $plan_name_from_short_name;

  $cred_data_recharge = find_credential("plans/Ultra/" . $plan . "/data_recharge");

  if ($cred_data_recharge)
  {
    $data_product_list = data_product_list();

    $cred_data_recharge_list = explode(',', $cred_data_recharge);

    foreach ($cred_data_recharge_list as $data_recharge)
      $data_recharge_by_plan[] = array(
        'data_soc_id' => $data_recharge,'data_soc' => $data_product_list[$data_recharge][0],'MB' => $data_product_list[$data_recharge][1],'cost' => $data_product_list[$data_recharge][2] // cost in $
      );
  }

  return $data_recharge_by_plan;
}

/**
 * get_voice_recharge_by_plan
 *
 * @param string $plan
 * @return array
 */
function get_voice_recharge_by_plan($plan)
{
  $voice_recharge_by_plan = array();

  $plan_name_from_short_name = get_plan_name_from_short_name($plan);

  if ($plan_name_from_short_name)
  {
    $plan = $plan_name_from_short_name;
  }

  $cred_voice_recharge = find_credential("plans/Ultra/" . $plan . "/voice_recharge");

  if ($cred_voice_recharge)
  {
    $voice_product_list = voice_product_list();

    $cred_voice_recharge_list = explode(',', $cred_voice_recharge);

    foreach ($cred_voice_recharge_list as $voice_recharge)
    {
      $voice_recharge_by_plan[] = array(
        'voice_soc_id' => $voice_recharge,'voice_minutes' => $voice_product_list[$voice_recharge][0],'cost' => $voice_product_list[$voice_recharge][1]
      );
    }
  }

  return $voice_recharge_by_plan;
}

/**
 * get_plan_costs_by_customer_id
 *
 * Returns plan costs for a customer (upon renewal), with details about Bolt Ons and plan name
 *
 * @return array (int cost of monthly plan, int cost of recurring bolt ons, string marketing plan name (e.g. 'Personal' or '$19')
 */
function get_plan_costs_by_customer_id( $customer_id , $cos_id=NULL )
{
  $costs = array(
    'plan'     => NULL,
    'bolt_ons' => NULL,
    'name'     => NULL);

  // get customer's cos_id if not provided
  if ( ! $cos_id )
  {
    $plan_data = get_customer_plan( $customer_id );

    if ( ! $plan_data )
      return $costs;

    $cos_id = $plan_data['cos_id'];
  }

  $costs['plan']     = get_plan_cost_by_cos_id( $cos_id );
  $costs['bolt_ons'] = get_bolt_ons_costs_by_customer_id( $customer_id );
  $costs['name']     = $costs['bolt_ons'] ? 'Personal' : "\${$costs['plan']}";

  $latest_plan   = get_last_plan_tracker( $customer_id );

  if ($latest_plan && is_multi_month_plan($latest_plan->cos_id))
    $costs['personal'] = 'Multi-Month';
  else
    $costs['personal'] = $costs['bolt_ons'] ? 'Personal' : NULL;

  dlog('',"customer_id = $customer_id ; cos_id = $cos_id, result: %s", $costs);

  return $costs;
}

/**
 * get_bolt_ons_costs_by_customer_id
 *
 * Returns Bolt Ons costs for a customer upon renewal
 *
 * @return integer
 */
function get_bolt_ons_costs_by_customer_id( $customer_id )
{
  $bolt_ons = get_bolt_ons_by_customer_id( $customer_id );

  $cost = 0;

  if ( ! $bolt_ons || ! is_array( $bolt_ons ) || ! count( $bolt_ons ) )
    return $cost;

  dlog('',"%s",$bolt_ons);

  foreach( $bolt_ons as $bolt_on => $info )
    $cost += $info['cost'];

  return $cost;
}

/**
 * get_bolt_ons_by_customer_id
 *
 * Returns Bolt Ons for a customer upon renewal
 *
 * @return array
 */
function get_bolt_ons_by_customer_id( $customer_id )
{
  $customer_bolt_ons = get_bolt_ons_values_from_customer_options( $customer_id );

  if ( ! $customer_bolt_ons || ! is_array( $customer_bolt_ons ) || ! count( $customer_bolt_ons ) )
    return array();

  $map = \Ultra\UltraConfig\getBoltOnsMapping();
  $bolt_ons = array();
  foreach( $map as $bolt_on => $info )
  {
    foreach( $customer_bolt_ons as $attribute => $value )
    {
      if ( ( $attribute == $info['option_attribute'] ) && ( $value == $info['cost'] ) )
        $bolt_ons[ $bolt_on ] = $info;
    }
  }

  return $bolt_ons;
}

/**
 * get_plan_cost_by_cos_id
 *
 * Returns plan cost for a $cos_id
 *
 * @return integer
 */
function get_plan_cost_by_cos_id( $cos_id )
{
  return get_plan_cost_from_cos_id($cos_id) / 100;
}

/**
 * set_data_recharge_notification_delay
 *
 * Set Redis semaphore to block SMS from notification__DataNotificationHandler for $ttl minutes (default = 5 minutes).
 *
 * @param object $redis
 * @param int $customer_id
 * @param int $ttl
 * @return NULL
 */
function set_data_recharge_notification_delay($redis,$customer_id,$ttl=300)
{
  $redis->set( 'semaphore/data_notification/' . $customer_id , 1 , $ttl );
}

/**
 * get_data_recharge_notification_delay
 *
 * Get Redis semaphore to check if there has been Data Recharge calls by the same customer for 5 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @return int
 */
function get_data_recharge_notification_delay($redis,$customer_id)
{
  return $redis->get( 'semaphore/data_notification/' . $customer_id );
}

/**
 * set_data_recharge_semaphore
 *
 * Set Redis semaphore to block Data Recharge calls by the same customer for 15 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @return NULL
 */
function set_data_recharge_semaphore($redis,$customer_id)
{
  $redis->set( 'semaphore/data_recharge/' . $customer_id , 1 , 60 * 15 );
}

/**
 * get_data_recharge_semaphore
 *
 * Get Redis semaphore to check if there has been Data Recharge calls by the same customer for 15 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @return int
 */
function get_data_recharge_semaphore($redis,$customer_id)
{
  return $redis->get( 'semaphore/data_recharge/' . $customer_id );
}

/**
 * set_voice_recharge_semaphore
 *
 * Set Redis semaphore to block Voice Recharge calls by the same customer for 15 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @return NULL
 */
function set_voice_recharge_semaphore($redis,$customer_id)
{
  $redis->set( 'semaphore/voice_recharge/' . $customer_id , 1 , 60 * 15 );
}

/**
 * get_voice_recharge_semaphore
 *
 * Get Redis semaphore to check if there has been Voice Recharge calls by the same customer for 15 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @return int
 */
function get_voice_recharge_semaphore($redis,$customer_id)
{
  return $redis->get( 'semaphore/voice_recharge/' . $customer_id );
}

/**
 * set_bolt_on_semaphore
 *
 * Set Redis semaphore to block Bolt Ons by the same customer for 15 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @param string DATA | IDDCA
 * @return NULL
 */
function set_bolt_on_semaphore($redis, $customer_id, $type)
{
  $redis->set( "semaphore/bolt_on/$customer_id/$type" , 1 , 60 * 15 );
}

/**
 * get_bolt_on_semaphore
 *
 * Get Redis semaphore to check if there has been Bolt Ons requests by the same customer for 15 minutes.
 *
 * @param object $redis
 * @param int $customer_id
 * @param string DATA | IDDCA
 * @return int
 */
function get_bolt_on_semaphore($redis, $customer_id, $type)
{
  return $redis->get( "semaphore/bolt_on/$customer_id/$type" );
}

/**
 * is_porting_disabled
 *
 * @return boolean
 */
function is_porting_disabled()
{
  return find_credential("ultra/disable/porting");
}

/**
 * overridden_cc_dupe_check
 *
 * See http://wiki.hometowntelecom.com:8090/pages/viewpage.action?title=Test+Payment+and+SIM+Info&spaceKey=ULTRA
 * If the given credit card info match with our special test ones, return TRUE
 *
 * @return boolean
 */
function overridden_cc_dupe_check($customer_cvv, $customer_cc_n, $customer_cc_exp, $last_name, $tokenizer=NULL)
{
  if ( ! $tokenizer )
    $tokenizer = new \Ultra\Lib\DB\Merchants\Tokenizer();

  $overridden_cc_dupe_check = $tokenizer->alwaysSucceedAccount(
    array(
      'cc_number'    => $customer_cc_n,
      'last_name'    => $last_name,
      'expires_date' => $customer_cc_exp
    )
  );

  if ( ! $overridden_cc_dupe_check )
    $overridden_cc_dupe_check = $tokenizer->alwaysFailAccount(
      array(
        'cc_number'    => $customer_cc_n,
        'last_name'    => $last_name,
        'expires_date' => $customer_cc_exp
      )
    );

  return $overridden_cc_dupe_check;
}

/**
 * validate_email_address
 *
 * http://www.linuxjournal.com/article/9585?page=0,0 (modified)
 *
 * @return boolean
 */
function validate_email_address($email)
{
  $isValid = true;
  $atIndex = strrpos($email, "@");
  if (is_bool($atIndex) && ! $atIndex)
  {
    $isValid = false;
  }
  else
  {
    $domain = substr($email, $atIndex + 1);
    $local = substr($email, 0, $atIndex);
    $localLen = strlen($local);
    $domainLen = strlen($domain);
    if ($localLen < 1 || $localLen > 64)
    {
      // local part length exceeded
      $isValid = false;
    }
    else
      if ($domainLen < 1 || $domainLen > 255)
      {
        // domain part length exceeded
        $isValid = false;
      }
      else
        if ($local[0] == '.' || $local[$localLen - 1] == '.')
        {
          // local part starts or ends with '.'
          $isValid = false;
        }
        else
          if (preg_match('/\\.\\./', $local))
          {
            // local part has two consecutive dots
            $isValid = false;
          }
          else
            if (! preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
            {
              // character not valid in domain part
              $isValid = false;
            }
            else
              if (preg_match('/\\.\\./', $domain))
              {
                // domain part has two consecutive dots
                $isValid = false;
              }
              else
                if (! preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local)))
                {
                  // character not valid in local part unless
                  // local part is quoted
                  if (! preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local)))
                  {
                    $isValid = false;
                  }
                }

    if ($isValid && ! preg_match('/\w\\..\w/', $domain))
    {
      // domain not valid
      $isValid = false;
    }
    // f ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
    //
    // // domain not found in DNS
    // $isValid = false;
    //
  }
  return $isValid;
}

/**
 * beautify_address_strtoupper_list
 *
 * Beautify address string
 *
 * @return string
 */
function beautify_address_strtoupper_list()
{
  $strtoupper_list = array(
    'Mar','Amj','Aaa','Abc','Ada','Ahs','Ait','Ala','Alz','Ama','Amc','Amg','Ams','Apc','Ari','Ati','Bbb','Bbc','Bfj','Cha','Cnm','Csf','Ctw','Dia','Dre','Drr','Dsj','Dvd','Ebs','Ecj','Fmp','Fpp','Gmx','Gnc','Gnj','Gsk','Gsm','Gtc','Gwp','Hbd','Hsg','Htc','Htg','Htw','Ibb','Iso','Jac','Jdo','Jeg','Jhc','Jmd','Khw','Klk','Ksa','Lam','Lbc','Lbj','Lic','Lsg','Mbb','Mbc','Mcd','Mjs','Mkb','Mmg','Mtm','Mtr','Ngw','Nph','Nts','Nyc','Ojt','Omg','Pcr','Pcs','Pfk','Qbo','Rbw','Rcc','Rmc','Rnr','Roc','Rps','Rui','Sna','Spc','Sri','Sts','Tcc','Tcy','Tdd','Tmc','Ucw','Usa','Vip','Vvm','Wtl','Wyr','Xyz','Yde','Yon','Zms'
  );

  return $strtoupper_list;
}

/**
 * beautify_address
 *
 * Improve snail mail address appearance
 *
 * @param string $address
 * @return string
 */
function beautify_address($address)
{
  $strtoupper_list = beautify_address_strtoupper_list();

  $pretty_address = trim(strtolower($address));

  $pretty_address = str_replace("  ", " ", $pretty_address);

  $strings = explode(' ', $pretty_address);

  $beautified_address = '';
  $space = '';

  foreach ($strings as $string)
  {
    if ((strlen($string) == 2) && ($space == ''))
    {
      // Any retailer name that starts with two letters then a space, i.e. Mr Wireless or Cs Technology,
      // should have the first two letters in caps (MR Wirless, CS Technology).

      $beautified_address .= $space . strtoupper($string);
    }
    else
      if (strlen($string) == 3)
      {
        if ((in_array(ucfirst($string), $strtoupper_list)) || (preg_match('/[\-\&]/', $string)))
        {
          $beautified_address .= $space . strtoupper($string);
        }
        else
        {
          $beautified_address .= $space . ucfirst($string);
        }
      }
      else
        if (preg_match('/[\-\&\.]/', $string)) // strings containing '-' , '.' , '&' should be left uppercase
        {
          $beautified_address .= $space . strtoupper($string);
        }
        else
        {
          $beautified_address .= $space . ucfirst($string);
        }

    $space = ' ';
  }

  return $beautified_address;
}

/**
 * deunicodify_characters
 *
 * this is not supposed to be a full unicode cleanup function,
 * its sole purpose is to clean up unicode characters introduced by foreigner users which use non-english keyboards
 *
 * @param string $string
 * @return string
 */
function deunicodify_characters($string)
{
  $conversion_array = array(
    "à" => "a","á" => "a","å" => "a","â" => "a","ä" => "a","ă" => "a","ā" => "a","ą" => "a","À" => "A","Á" => "A","Ā" => "A","Ą" => "A","ç" => "c","ć" => "c","č" => "c","Ć" => "C","Č" => "C","đ" => "d","ď" => "d","Đ" => "D","Ď" => "D","è" => "e","é" => "e","æ" => "e","ë" => "e","ē" => "e","ĕ" => "e","ė" => "e","È" => "E","É" => "E","Ę" => "E","Ē" => "E","Ě" => "E","ģ" => "g","ğ" => "g","Ģ" => "G","ì" => "i","í" => "i","ı" => "i","î" => "i","ï" => "i","į" => "i","Ì" => "I","Í" => "I","Ī" => "I","Į" => "I","ķ" => "k","Ķ" => "K","ĺ" => "l","ļ" => "l","ľ" => "l","ł" => "l","Ĺ" => "L","Ľ" => "L","Ļ" => "L","Ł" => "L","ñ" => "n","Ń" => "N","Ņ" => "N","Ň" => "N","ò" => "o","ó" => "o","ö" => "o","õ" => "o","ø" => "o","œ" => "o","ō" => "o","Ò" => "O","Ó" => "O","Ō" => "O","Ő" => "O","Œ" => "O","ŕ" => "r","ŗ" => "r","Ŗ" => "R","Ř" => "R","ş" => "s","ß" => "s","ś" => "s","š" => "s","Ś" => "S","Ş" => "S","Š" => "S","ţ" => "t","ť" => "t","Ţ" => "T","ù" => "u","ú" => "u","ü" => "u","û" => "u","ų" => "u","Ù" => "U","Ú" => "U","Ű" => "U","Ű" => "U","Ų" => "U","ÿ" => "y","ý" => "y","Ÿ" => "Y","Ź" => "Z","Ż" => "Z","Ž" => "Z","ź" => "z","ż" => "z","ž" => "z"
  );

  foreach ($conversion_array as $unicode => $ascii)
  {
    $string = str_replace($unicode, $ascii, $string);
  }

  return $string;
}

 /**
 * javaEscapedToAscii
 *
 * Converts java escaped unicode characters to their ascii representations
 *
 * @param string $str
 * @return string
 */
function javaEscapedToAscii( $str )
{
  $unicodeMap = array(
    '\\u00e0' => 'a',
    '\\u00e1' => 'a',
    '\\u00e5' => 'a',
    '\\u00e2' => 'a',
    '\\u00e4' => 'a',
    '\\u0103' => 'a',
    '\\u0101' => 'a',
    '\\u0105' => 'a',
    '\\u00c0' => 'A',
    '\\u00c1' => 'A',
    '\\u0100' => 'A',
    '\\u0104' => 'A',
    '\\u00e7' => 'c',
    '\\u0107' => 'c',
    '\\u010d' => 'c',
    '\\u0106' => 'C',
    '\\u010c' => 'C',
    '\\u0111' => 'd',
    '\\u010f' => 'd',
    '\\u0110' => 'D',
    '\\u010e' => 'D',
    '\\u00e8' => 'e',
    '\\u00e9' => 'e',
    '\\u00e6' => 'e',
    '\\u00eb' => 'e',
    '\\u0113' => 'e',
    '\\u0115' => 'e',
    '\\u0117' => 'e',
    '\\u00c8' => 'E',
    '\\u00c9' => 'E',
    '\\u0118' => 'E',
    '\\u0112' => 'E',
    '\\u011a' => 'E',
    '\\u0123' => 'g',
    '\\u011f' => 'g',
    '\\u0122' => 'G',
    '\\u00ec' => 'i',
    '\\u00ed' => 'i',
    '\\u0131' => 'i',
    '\\u00ee' => 'i',
    '\\u00ef' => 'i',
    '\\u012f' => 'i',
    '\\u00cc' => 'I',
    '\\u00cd' => 'I',
    '\\u012a' => 'I',
    '\\u012e' => 'I',
    '\\u0137' => 'k',
    '\\u0136' => 'K',
    '\\u013a' => 'l',
    '\\u013c' => 'l',
    '\\u013e' => 'l',
    '\\u0142' => 'l',
    '\\u0139' => 'L',
    '\\u013d' => 'L',
    '\\u013b' => 'L',
    '\\u0141' => 'L',
    '\\u00f1' => 'n',
    '\\u0143' => 'N',
    '\\u0145' => 'N',
    '\\u0147' => 'N',
    '\\u00f2' => 'o',
    '\\u00f3' => 'o',
    '\\u00f6' => 'o',
    '\\u00f5' => 'o',
    '\\u00f8' => 'o',
    '\\u0153' => 'o',
    '\\u014d' => 'o',
    '\\u00d2' => 'O',
    '\\u00d3' => 'O',
    '\\u014c' => 'O',
    '\\u0150' => 'O',
    '\\u0152' => 'O',
    '\\u0155' => 'r',
    '\\u0157' => 'r',
    '\\u0156' => 'R',
    '\\u0158' => 'R',
    '\\u015f' => 's',
    '\\u00df' => 's',
    '\\u015b' => 's',
    '\\u0161' => 's',
    '\\u015a' => 'S',
    '\\u015e' => 'S',
    '\\u0160' => 'S',
    '\\u0163' => 't',
    '\\u0165' => 't',
    '\\u0162' => 'T',
    '\\u00f9' => 'u',
    '\\u00fa' => 'u',
    '\\u00fc' => 'u',
    '\\u00fb' => 'u',
    '\\u0173' => 'u',
    '\\u00d9' => 'U',
    '\\u00da' => 'U',
    '\\u0170' => 'U',
    '\\u0172' => 'U',
    '\\u00ff' => 'y',
    '\\u00fd' => 'y',
    '\\u0178' => 'Y',
    '\\u0179' => 'Z',
    '\\u017b' => 'Z',
    '\\u017d' => 'Z',
    '\\u017a' => 'z',
    '\\u017c' => 'z',
    '\\u017e' => 'z',
  );
  
  foreach ( $unicodeMap as $unicode => $ascii )
    $str = str_replace( $unicode, $ascii, $str );

  return $str;
}

/**
 * customer_has_credit_card
 *
 * Returns TRUE if the customer has credit card values stored in our DB
 *
 * @param object or integer
 * @return boolean
 */
function customer_has_credit_card($customer)
{
  $customer_id = NULL;

  if ( empty($customer) )
    return FALSE;

  if ( is_object( $customer ) )
    $customer_id = empty($customer->CUSTOMER_ID) ? NULL : $customer->CUSTOMER_ID ;

  if ( is_numeric( $customer ) )
    $customer_id = $customer;

  if ( empty($customer_id) )
    return FALSE;

  return ! ! count( get_cc_info_and_tokens_by_customer_id( $customer_id ) );
}

/**
 * languageIETF
 *
 * Convert 2 character language to IETF
 *
 * @return string
 */
function languageIETF($language)
{
  switch ($language)
  {
    case 'en':
      return 'en-US';
    case 'es':
      return 'es-US';
    default:
      return $language;
  }
}

/**
 * func_validate_orange_sim
 *
 * @param int $customer_id
 */
function func_validate_orange_sim($customer_id)
{
  $result = new Result();

  $sql = htt_inventory_sim_select_query(array(
    "customer_id" => $customer_id
  ));

  $sim_data = mssql_fetch_all_objects(logged_mssql_query($sql));

  // customer may not be activated yet
  if (! ($sim_data && is_array($sim_data) && count($sim_data)))
  {
    $customer = get_ultra_customer_from_customer_id($customer_id, array(
      "customer_id","current_iccid"
    ));

    if ( ! $customer )
    {
      $result->add_error("Customer not found");
      return $result;
    }

    if ( ! $customer->current_iccid )
    {
      $result->add_error("Customer has no ICCID");
      return $result;
    }

    if ( ! preg_match('/^\d+$/', $customer->current_iccid ) )
    {
      $result->add_error("Customer has an invalid ICCID");
      return $result;
    }

    $sql = htt_inventory_sim_select_query(array(
      "iccid" => $customer->current_iccid
    ));

    $sim_data = mssql_fetch_all_objects(logged_mssql_query($sql));
  }

  if ($sim_data && is_array($sim_data) && count($sim_data))
  {
    if (count($sim_data) > 1)
    {
      dlog('', "Multiple SIMs found for customer_id $customer_id");

      $customer = get_ultra_customer_from_customer_id($customer_id, array(
        "customer_id","current_iccid"
      ));

      if ( ! $customer )
      {
        $result->add_error("Customer not found");
        return $result;
      }

      if ( ! $customer->current_iccid )
      {
        $result->add_error("Customer has no ICCID");
        return $result;
      }

      if ( ! preg_match('/^\d+$/', $customer->current_iccid ) )
      {
        $result->add_error("Customer has an invalid ICCID");
        return $result;
      }

      $sql = htt_inventory_sim_select_query(array(
        "iccid" => $customer->current_iccid
      ));

      $sim_data = mssql_fetch_all_objects(logged_mssql_query($sql));
    }

    $result->add_data_array(array(
      'PRODUCT_TYPE' => $sim_data[0]->PRODUCT_TYPE
    ));

    if ($sim_data[0]->PRODUCT_TYPE === 'ORANGE')
    {
      $result->succeed();
    }
    else
    {
      dlog('', "SIM is not ORANGE for customer_id $customer_id");
      $result->add_error("SIM is not ORANGE for customer_id $customer_id");
    }
  }
  else
  {
    dlog('', "No SIM data found for customer_id $customer_id");
    $result->add_error("No SIM data found for customer_id $customer_id");
  }

  return $result;
}

/**
 * validateUSZipcode
 *
 * Verifies that $zipcode is a valid US zipcode
 *
 * @param string $zipcode
 */
function validateUSZipcode($zipcode)
{
  // it can be improved :p
  return preg_match('/^\d{5}$/', $zipcode);
}

/**
 * extractZipcodesFromString
 *
 * Returns a list of valid zip codes extracted from $string
 *
 * @param string $string
 */
function extractZipcodesFromString($string)
{
  $zipcodes = array();
  $error = '';

  $list = preg_split('/[\,\;\-]/', $string);

  foreach ($list as $zipcode)
  {
    if (! $error)
    {
      $zipcode = trim($zipcode);

      if (validateUSZipcode($zipcode))
      {
        $zipcodes[] = $zipcode;
      }
      else
      {
        dlog('', "$zipcode failed validateUSZipcode");
        $error = "ERR_API_INVALID_ARGUMENTS: Zip code $zipcode is invalid";
      }
    }
  }

  return array(
    $zipcodes,$error
  );
}

/**
 * get_reference_source
 *
 * Ultra internal source mapping
 *
 * @return integer
 */
function get_reference_source($source)
{
  $default_source = 3;

  $sources = array(
    'WEBCC'      => 0,
    'EPAY'       => 1,
    'PIN'        => 2,
    'VOID'       => 4,
    'MANUAL'     => 5,
    'ORANGE_SIM' => 6,
    'PROMO'      => 7,
    'EPAYPQ'     => 8,
    'WEBPOS'     => 9,
    'UVIMPORT'   => 10,
    'INTRAPORT'  => 11
  );

  return isset($sources[$source]) ? $sources[$source] : $default_source;
}


/**
 * get_utc_date
 * returns the current date in UTC
 * @return string
 */
function get_utc_date()
{
  return gmdate('M j Y H:i:s');
}


/**
 * search_data_structure
 *
 * @param string $field
 * @param mixed $data_structure
 * @return mixed found value
 */
function search_data_structure( $field , $data_structure )
{
  $value = '';

  if ( is_array( $data_structure ) )
  {
    if ( isset( $data_structure[ $field ] ) )
    {
      $value = $data_structure[ $field ];
    }
    else
    {
      foreach( $data_structure as $key => $lock )
      {
        $v = search_data_structure( $field , $lock );

        if ( $v )
          $value = $v;
      }
    }
  }
  elseif ( is_object( $data_structure ) )
  {
    $members = get_object_vars( $data_structure );

    # TODO
    #foreach( $members as $member )
    #{
    #}
  }
  elseif($data_structure)
  {
    $object = json_decode($data_structure);

    if ( is_object( $object ) || is_array( $object ) )
    {
      $value = search_data_structure( $field , $object );
    }
  }

  return $value;
}

/**
 * paginate_array
 *
 * Utility function to split a big array in smaller ones of lenght $page_size
 *
 * @return array
 */
function paginate_array( $array , $page_size=1 )
{
  if ( ! $page_size )
    return array();

  $c = 0;

  $paginated_array = array();

  while( count($array)/$page_size > $c )
    $paginated_array[] = array_slice( $array , $page_size*($c++) , $page_size );

  return $paginated_array;
}

/**
 * paginate_output
 *
 * Utility function to paginate an array in chunks of size $page_size
 *
 * @return array
 */
function paginate_output( $data , $page_size , $page_ord )
{
  $data_page = array();

  if ( count( $data ) )
    $data_page = array_slice( $data , ( $page_size * ( $page_ord - 1 ) ) , $page_size );

  return $data_page;
}

/**
 * remove_duplicates_by_field
 *
 * @param array $data
 * @param string $field
 * @return array
 */
function remove_duplicates_by_field( $data , $field )
{
  $deduped_data = array();

  $duplicates   = array();

  foreach( $data as $row )
  {
    $unique_id = '';

    if ( is_object($row) )
      $unique_id = $row->$field;

    if ( is_array($row) )
      $unique_id = $row[$field];

    if ( $unique_id )
    {
      if ( !isset($duplicates[ $unique_id ] ) )
      {
        $deduped_data[] = $row;
        $duplicates[ $unique_id ] = 1;
      }
    }
  }

  return $deduped_data;
}

/**
 * date_from_tz_to_tz
 *
 * @param string $date
 * @param string $source_timezone
 * @param string $dest_timezone
 * @param int $add_days
 * @return string
 */
function date_from_tz_to_tz( $date , $source_timezone , $dest_timezone , $add_days=0 )
{
  #$dt = new DateTime( $date . ' 00:00:00', $source_timezone);
  $dt = ( strlen($date) <= 10 )
        ?
        # date is in the format 'yyyy-mm-dd'
        new DateTime( $date . ' 00:00:00', $source_timezone)
        :
        # date is in the format 'yyyy-mm-dd HH-MM-SS'
        new DateTime( $date , $source_timezone)
        ;

  date_add( $dt , date_interval_create_from_date_string($add_days.' days'));

  $dt->setTimeZone($dest_timezone);

  return $dt->format('Y-m-d H:i:s');
}

/**
 * date_from_pst_to_utc
 *
 * @param string $date
 * @param int $add_days
 * @return string
 */
function date_from_pst_to_utc( $date , $add_days=0 )
{
  $source_timezone = new DateTimeZone('America/Los_Angeles');
  $dest_timezone   = new DateTimeZone('UTC');

  return date_from_tz_to_tz( $date , $source_timezone , $dest_timezone , $add_days );
}

/**
 * date_from_utc_to_pst
 *
 * @param string $date
 * @param int $add_days
 * @return string
 */
function date_from_utc_to_pst( $date , $add_days=0 )
{
  $source_timezone = new DateTimeZone('UTC');
  $dest_timezone   = new DateTimeZone('America/Los_Angeles');

  return date_from_tz_to_tz( $date , $source_timezone , $dest_timezone , $add_days );
}

/**
 * timestamp_to_date
 * convert UNIX epoch to date string and shift to given time zone (default is PT)
 * @return string
 */
function timestamp_to_date($timestamp, $format = AMERICAN_DATE_FORMAT, $timezone = 'America/Los_Angeles')
{
  try
  {
    $date = new DateTime(NULL, new DateTimeZone($timezone));
    $date->setTimestamp($timestamp);
  }
  catch (\Exception $e)
  {
    dlog('', 'EXCEPTION: %s', $e->getMessage());
    $date = new DateTime(); // default time in PT
  }

  return $date->format($format);
}

/**
 * date_to_next_daytime
 *
 * @param int $timestamp
 * @return string
 */
function date_to_next_daytime( $timestamp )
{
  $timestamp_to_next_daytime = timestamp_to_next_daytime( $timestamp );

  dlog('',"timestamp = $timestamp ; to_next_daytime = $timestamp_to_next_daytime ; diff = ".( $timestamp_to_next_daytime - $timestamp ));

  $d = new DateTime( date("Y-m-d H:i:s", $timestamp_to_next_daytime ) );

  $date_to_next_daytime = $d->format( "Y-m-d H:i:s" );

  dlog('',"$date_to_next_daytime");

  return $date_to_next_daytime;
}

/**
 * timestamp_to_next_daytime
 *
 * returns the closest future daytime time ( between 11 AM EST and 10 PM EST)
 *
 * @param int $timestamp
 * @return int
 */
function timestamp_to_next_daytime( $timestamp )
{
  $d = new DateTime( date("Y-m-d H:i:s", $timestamp) );

  $H = $d->format("H");

  if ( $H > 21 )
    $H = $H - 24;

  if ( $H < 11 )
  {
    // we should jump to the next day

    $s = $d->format("s");
    $i = $d->format("i");

    $timestamp -= ( $i*60 + $s );
    $timestamp += ( 11 - $H ) * 60 * 60 ;
  }

  return $timestamp;
}

/**
 * int_positive_or_zero
 *
 * @param int $value
 * @return int
 */
function int_positive_or_zero( $value )
{
  $value = intval( $value );

  return ( $value < 0 ) ? 0 : $value ;
}

/**
 * compute_imei
 *
 * IMEISV : 3559720503124605 - 16 digits. 35597205031246 is the base IMEI and 05 is software version.
 * IMEI   : 355972050312462  - 15 digits. 35597205031246 is the base IMEI and 2 is the check digit.
 *
 * @return string
 */
function compute_imei( $imei )
{
  if ( strlen($imei) == 16 )
  {
    // convert IMEISV to 14 digits IMEI
    $imei = substr( $imei , 0 , 14 );
  }

  if ( strlen($imei) == 14 )
  {
    // convert 14 digits IMEI to 15 digits IMEI
    $imei = luhn_append( $imei );
  }

  return $imei;
}

/**
 * normalize_msisdn
 *
 * fix variable MSISDN length to 10 or 11 characters
 * @param string msisdn
 * @param boolean international: make 11 digits
 * @return string
 */
function normalize_msisdn($msisdn, $international=FALSE)
{
  return ($international ? '1' : '') . substr($msisdn, -10);
}

function normalize_msisdn_11($msisdn)
{ return normalize_msisdn($msisdn,TRUE); }

function normalize_msisdn_10($msisdn)
{ return normalize_msisdn($msisdn,FALSE); }

/**
 * beautify_4g_lte_string
 *
 * If the result is under 1GB, then return as MB rounded up to the nearest MB i.e. 120MB
 * If the result is over 1GB, then show it as the GB number, the decimal, and then the MB amount out to three digits rounded up to the nearest MB i.e. 1.329GB
 * @param string float; data value
 * @param boolean: TRUE for used data, FALSE for remaining data
 * @return string
 */
function beautify_4g_lte_string( $string, $used )
{
  if ( ! $string )
    return '0MB';
  elseif ( ! is_numeric( $string ))
    return $string;
  elseif ($string < 1024)
  {
    // PROD-595: calcualte remaining and used data differently
    return ($used ? floor($string) : ceil($string)) . 'MB';
  }
  else
  {
    $beautiful_string = ( $string / 1024 ).'GB';
    $beautiful_string = preg_replace("/\.(\d\d\d)\d+/", '.$1', $beautiful_string);
    $beautiful_string = preg_replace("/\.000/", '', $beautiful_string);
    return $beautiful_string;
  }
}

/**
 * date_to_datetime
 *
 * convert just about any date string format into MSSQL datetime string
 * if time zone is given then conver to UTC
 *
 * @param string date (e.g. '2001-02-29')
 * @param boolean full_day: true if should we extend the time to 23:59:59
 *   explanation: MSSQL sets dates without time to 12:00AM which,
 *   when used in BETWEEN query effectively eiminates all records of the second date;
 *   setting this flag to TRUE will correct this behavior to include second date's records
 * @param boolean time: include time in output
 * @return string (e.g. 'Feb 02 2001 12:00:00AM')
 */
function date_to_datetime($date, $full_day = FALSE, $time = FALSE)
{
  try
  {
    $datetime = new \DateTime($date);

    // check if time zone was given and conver to UTC
    $tz = $datetime->getTimezone();
    if ($tz->getName() != 'America/Los_Angeles') // non-default: time zone was given
      $datetime->setTimezone(new DateTimeZone('UTC'));

    if ($time)
      return $datetime->format(MSSQL_DATE_FORMAT);
    else
      return $datetime->format('M d Y') . ($full_day ? ' 12:59:59:999PM' : NULL);
  }
  catch (\Exception $e)
  {
    dlog('', 'ERROR: ' . $e->getMessage());
    return NULL;
  }
}

/**
  * tmo_dummymsisdn
  *
  * calcualte dummy ICCID, IMSI and MSISDN values given one of them; value of ICCID is queried from HTT_INVENTORY_SIM
  * @parameter: string ICCID (18 or 19 digits) or IMSI (15 digits) or dummy MSISDN (14 digits)
  * @return array['msisdn', 'imsi', 'iccid', 'error']
  */
function tmo_dummymsisdn($input)
{
  $result = array(
    'msisdn'  =>  NULL,
    'imsi'    =>  NULL,
    'iccid'   =>  NULL,
    'error'   =>  NULL);
  $derived = 'derived_';

  // check input
  if (! ctype_digit($input))
  {
    $result['error'] = "Value '$input' is not numeric";
    return $result;
  }

  // determine the type of input and de-compose into parts
  switch (strlen($input))
  {
    case 14:
      $result['msisdn'] = $input;
      list($prefix, $root, $suffix) = array(substr($input, 0, 2), substr($input, 2, 2), substr($input, 7, 7));
      break;

    case 15:
      $result['imsi'] = $input;
      list($prefix, $root, $suffix) = array(substr($input, 3, 2), substr($input, 6, 2), substr($input, 8, 7));
      break;

    case 18:
      $input = luhn_append($input);

    case 19:
      $result['iccid'] = $input;
      list($prefix, $root, $suffix) = array(substr($input, 4, 2), substr($input, 7, 2), substr($input, 11, 7));
      break;

    default:
      $result['error'] = "Cannot determine type of '$input'";
      return $result;
  }

  // re-compose and query ICCID
  if (! $result['iccid'])
  {
    $iccid = '8901' . $prefix . '0' . $root . '__' . $suffix;
    if ($sql = \Ultra\Lib\DB\makeSelectQuery('HTT_INVENTORY_SIM', NULL, NULL, array("ICCID_NUMBER LIKE '$iccid'")))
    {
      if ($row = find_first($sql))
        $result['iccid'] = $derived . $row->ICCID_FULL;
      else
        $result['error'] = "Cannot find ICCID '$iccid' in DB";
    }
    else
      $result['error'] = 'Failed to compose ICCID SQL query';
  }

  // re-compose IMSI
  if (! $result['imsi'])
    $result['imsi'] = $derived . '310' . $prefix . '0' . $root . $suffix;

  // re-compose MSISDN
  if (! $result['msisdn'])
    $result['msisdn'] = $derived . $prefix . $root . '911' . $suffix;

  // output
  return $result;
}

/**
 * date_sql_to_usa
 *
 * convert an SQL formatted datetime to American style date time
 * optionally apply UTC->PST time zone conversion
 * e.g. 'Apr 4 2014 03:02:44:387PM' -> '04/04/14 08:02:44AM'
 * @param string SQL date in UTC
 * @param boolean convert
 * @return string American date in PST
 */
function date_sql_to_usa($date, $utc_to_pst = FALSE)
{
  $result = NULL;

  if (empty($date))
    return $result;

  try
  {
    $datetime = new \DateTime($date, new DateTimeZone('UTC'));
    if ($utc_to_pst)
      $datetime->setTimezone(new DateTimeZone('America/Los_Angeles'));
    $result = $datetime->format(AMERICAN_DATE_FORMAT);
  }
  catch(Exception $exc)
  {
    dlog('', 'ERROR: ' . $exc->getMessage());
  }

  return $result;
}

/**
 * compute_delivery_daytime
 *
 * calculate next available SMS daytime delivery time based on customer UTC hour offset
 * daytime window is from 08:00 to 22:00
 *
 * @param int timestamp: desired delivery time, UNIX epoch
 * @param int utc_offset: customer time zone difference from UTC, hours
 * @param int priority: 1 deliver now, 2 deliver within 1 hour, 3+ deliver within 12 hours
 * @return array(string UTC datetime 'MM/DD/YYYY HH:MM:SS AP', int timestamp)
 */
function compute_delivery_daytime($timestamp, $utc_offset, $priority)
{
  // init
  dlog('', 'parameters: ' . implode(', ', func_get_args()));
  if (! $timestamp)
    $timestamp = time();

  try
  {
    // determine customer's time zone from offset, default to PT
    $tz_name = timezone_name_from_abbr(null, $utc_offset * 3600, false);
    if (! $tz_name)
      $tz_name = 'America/Los_Angeles';

    // create desired delivery DateTime in customer's time zone
    $date = new DateTime(NULL, new DateTimeZone($tz_name));
    if (! $date)
      throw new Exception('Failed to create DateTime object');
    $date->setTimestamp($timestamp);

    // get hours, minutes and seconds
    $hour = $date->format('H');
    $minute = $date->format('i');
    $second = $date->format('s');

    // check if DateTime is within daytime and adjust it if needed
    // preserve minute:second to avoid morning rush of messages
    if ($hour < 8) // too early, push to 8 AM
      $date->modify('+' . (8 - $hour) . 'hour');
    if ($hour > 21) // to late, push to 8 AM next day
      $date->modify('+' . (24 - $hour + 8) . 'hour');

    // compute length of remaining daytime window of the adjusted DateTime, seconds
    $window = ((22 - $date->format('H')) * 60 - $minute) * 60 - $second;

    // adjust daytime window based on priority
    $start = $date->getTimestamp();
    if ($priority == 1) // immediate delivery: do not use window
      $end = $start;
    elseif ($priority == 2) // deliver within 1 hour or the remaining window, whichever is less
      $end = $start + min(60 * 60, $window);
    else // deliver within 12 hours or the remaining window, whichever is less
      $end = $start + min(60 * 60 * 12, $window);

    // generate new delivery time within the new window
    $date->setTimestamp(mt_rand($start, $end));
    $local_time = $date->format(MSSQL_DATE_FORMAT);
  }
  catch (\Exception $e)
  {
    dlog('', 'EXCEPTION: ' . $e->getMessage());

    // failed result: use current time which will cause immediate delivery
    $date = new DateTime();
  }

  // save local time zone name for debugging
  $tz = $date->getTimezone();
  $tz_name = $tz->getName();

  // convert final delivery DateTime to UTC
  $date->setTimezone(new DateTimeZone('UTC'));
  $utc_time = $date->format(MSSQL_DATE_FORMAT);
  $timestamp = $date->getTimestamp();
  dlog('', "result TZ: $tz_name, local: '$local_time', UTC: '$utc_time', timestamp: $timestamp");
  return array($utc_time, $timestamp);
}

/**
 * convert_value_to_boolean
 *
 * convert any pseudo-boolean value to boolean; useful for converting values obtained from cfengine or database
 * @return TRUE, FALSE or NULL if not recognized
 */
function convert_value_to_boolean($value)
{
  switch (gettype($value))
  {
    case 'boolean':
      return $value;

    case 'integer':
    case 'double':
      return $value ? TRUE : FALSE;

    case 'string':
      $value = trim(strtolower($value));
      if ($value === 'false' || $value === '0')
        return FALSE;
      elseif ($value === 'true' || $value === '1')
        return TRUE;
      else
      {
        dlog('', "ERROR: cannot convert string '$value' into boolean");
        return NULL;
      }

    default: // NULL, object, array
      dlog('', "ERROR: cannot convert value type " . gettype($value));
      return NULL;
  }
}

/**
 * ultraErrorHandler
 *
 * custom error handler to trap (mostly) MSSQL and other errors instead of dumping whole stack into logs
 * @see https://localhost:8443/browse/MVNO-2416
 */
function ultraErrorHandler($errNumber, $errMessage, $errFile, $errLine, $errContext)
{
  dlog('', "Fatal PHP error: $errNumber, message: $errMessage, file: $errFile, line $errLine");
  return TRUE; // we have handled the error
}

/**
 * time_zone_mapping
 *
 * Map of TZ symbols to difference in hours from UTC
 *
 * @return array
 */
function time_zone_mapping()
{
  return array(
    'PST-7'   => -5,    // Armed Forces Pacific
    'PST-6'   => 10,    // Guam
    'PST-5'   => 14,    // Caroline Islands
    'PST-4'   => 12,    // Marshall Islands
    'PST-3'   => -11,   // American Samoa
    'PST-2'   => -10,   // Hawaii
    'PST-1'   => -9,    // Alaska
    'GMT+1'   => 1,
    'EST+1'   => -4,
    'EST'     => -5,
    'CST'     => -6,
    'MST'     => -7,
    'PST'     => -8
  );
}

/**
 * Given a unix timestamp (i/e: time()), will return a UTC formatted date
 *
 * @param integer $timestamp
 * @return string
 */
function get_utc_date_formatted_for_mssql($timestamp = false)
{
  if ( ! $timestamp )
    $timestamp = time();

  return gmdate("Y-m-d H:i:s.", $timestamp);
}

/**
 * Given a microtime() value, will return milliseconds (rounded to precision 3)
 *
 * @param integer $microtime
 * @return integer
 */
function extract_milliseconds_from_microtime($microtime = false)
{
  if (!$microtime)
  {
    list($microtime, $sec) = explode(" ", microtime());
    $microtime = (float) $microtime + (float) $sec;
  }

  $milliseconds = explode(".", round($microtime, 3));
  $milliseconds = isset($milliseconds[1]) ? $milliseconds[1] : 0;

  return $milliseconds;
}

/**
 * date_to_epoch
 * convert human readable date (such as SQL) into UNIX epoch timestamp
 */
function date_to_epoch($date, $tz = 'UTC')
{
  if ( ! $date)
  {
    dlog('', 'ERROR: invalid parameter date');
    return NULL;
  }
  try
  {
    // this may throw an exception in case of badly formatted date
    $epoch = strtotime("$date $tz");
  }
  catch(Exception $e)
  {
    dlog('', $e->getError());
    $epoch = NULL;
  }
  return $epoch;
}

function prettifyMsisdn( $msisdn )
{
  return '(' . substr($msisdn,0,3) . ') ' . substr($msisdn,3,3) . '-' . substr($msisdn,6,4);
}

/**
 * Given a key and clear_text, will return encrypted text
 * @param string $clear_text
 * @param string $key
 * @return string $encrypted_text
 */
function perform_aes_encryption( $clear_text, $key )
{
  return rtrim(
    base64_encode(
      mcrypt_encrypt(
        MCRYPT_RIJNDAEL_256,
        $key, $clear_text,
        MCRYPT_MODE_ECB,
        mcrypt_create_iv(
          mcrypt_get_iv_size(
            MCRYPT_RIJNDAEL_256,
            MCRYPT_MODE_ECB
          ),
          MCRYPT_RAND
        )
      )
    ), "\0"
  );
}

/**
 * Given a key and encrypted_text, will return clear text
 * @param string $encrypted_text
 * @param string $key
 * @return string $clear_text
 */
function perform_aes_decryption($encrypted_text, $key)
{
  return rtrim(
    mcrypt_decrypt(
      MCRYPT_RIJNDAEL_256,
      $key,
      base64_decode($encrypted_text),
      MCRYPT_MODE_ECB,
      mcrypt_create_iv(
        mcrypt_get_iv_size(
          MCRYPT_RIJNDAEL_256,
          MCRYPT_MODE_ECB
        ),
        MCRYPT_RAND
      )
    ), "\0"
  );
}

/**
 * has_non_ascii_characters
 *
 * Returns true if the string contains non ASCII characters
 *
 * @return boolean
 */
function has_non_ascii_characters( $string )
{
  $has_non_ascii_characters = ! ! preg_match('/[^\x20-\x7e]/', $string);

  if ( $has_non_ascii_characters )
    dlog('',"$string contains non ASCII characters");

  return $has_non_ascii_characters;
}

/**
 * get_all_http_headers
 *
 * @return array
 */
function get_all_http_headers()
{
  if (function_exists('getallheaders'))
    return getallheaders();
  else
    return $_SERVER;
}

/**
 * map_array_keys
 *
 * @return array
 */
function map_array_keys( $map , $array )
{
  $mapped = array();

  if ( is_array( $map ) && is_array( $array ) )
  {
    $c = count( $map );

    for ( $i = 0 ; $i < $c ; $i++ )
      $mapped[ $map[$i] ] = $array[ $i ];
  }

  return $mapped;
}

/**
 * map_object_keys
 *
 * @return Object
 */
function map_object_keys( $map , $array )
{
  $mapped = new stdClass();

  if ( is_array( $map ) && is_array( $array ) )
  {
    $c = count( $map );

    for ( $i = 0 ; $i < $c ; $i++ )
    {
      $name = $map[$i];

      $mapped->$name = $array[ $i ];
    }
  }

  return $mapped;
}

/**
 * map_object_keys_array
 *
 * @return array
 */
function map_object_keys_array( $map , $array )
{
  $mapped = array();

  foreach( $array as $innerArray )
    $mapped[] = map_object_keys( $map , $innerArray );

  return $mapped;
}

/**
 * remove_empty_values
 *
 * @return array
 */
function remove_empty_values( $array )
{
  foreach( $array as $key => $value )
    if ( ( $value !== false && $value !== 0 && $value !== '0' ) && ( empty( $value ) || ( $value == ' ' ) ) )
      unset($array[ $key ]);

  return $array;
}

/**
 * strvalArrayValues
 *
 * Recursively apply strval to all values of a (deep) associative array
 *
 * @return array
 */
function strvalArrayValues( array $array , $skip=NULL )
{
  foreach( $array as $key => $value )
    if ( ! is_null($skip) && ( ( !is_array( $skip ) && ( $key === $skip ) ) || ( is_array( $skip ) && in_array( $key, $skip ) ) ) )
      $array[ $key ] = $value;
    elseif ( is_array( $value ) )
      $array[ $key ] = strvalArrayValues( $value, $skip );
    elseif ( is_object( $value ) || is_resource( $value ) )
      $array[ $key ] = $value;
    elseif ( is_null( $value ) )
      $array[ $key ] = NULL;
    else
      $array[ $key ] = strval($value);

  return $array;
}

/**
 * toKeyValString
 *
 * Recursively builds a key value string from an array or object
 *
 * @return array
 */
function toKeyValString( $arr, $maxDepth=10, array $visitedArr=array() )
{
  $resultStr = '';

  if ($maxDepth-- <= 0) return '* MAX DEPTH *';
  
  if ( !is_object( $arr ) && !is_array( $arr ) )
    return $arr;

  if ( is_object($arr) )
    $arr = (array)$arr;

  if ( isAssociative($arr) )
  {
    foreach( $arr as $key => &$val )
    {
      if ( $key == '__toKeyValString_visited__' )
        continue;

      if ( is_object( $val ) )
      {
        // check for cycle
        if ( in_array( $val, $visitedArr, true ) )
        {
          $resultStr .= $key . ' = "*"';
        }
        else
        {
          $visitedArr[] = $val;
          $resultStr .= $key . ' = [{' . toKeyValString( $val, $maxDepth, $visitedArr ) . '}]';
        }
      }
      else if ( is_array( $val ) )
      {
        if ( isset( $val['__toKeyValString_visited__'] ) || in_array( '__toKeyValString_visited__', $val ) )
        {
          $resultStr .= $key . ' = "*"';
        }
        else
        {
          if ( isAssociative( $val ) )
          {
            $val['__toKeyValString_visited__'] = true;
          }
          else
          {
            $val[] = '__toKeyValString_visited__';
          }
          $resultStr .= $key . ' = [{' . toKeyValString( $val, $maxDepth, $visitedArr ) . '}]';
        }
      }
      elseif ( is_null( $val ) )
      {
        $resultStr .= $key . ' = ""';
      }
      else
      {
        $resultStr .= $key . ' = "' . htmlspecialchars( strval( $val ) ) . '"';
      }

      $resultStr .= ', ';
    }
  }
  else
  {
    foreach( $arr as &$val )
    {
      if ( $val == '__toKeyValString_visited__' )
        continue;
      
      if ( is_object( $val ) )
      {
        // check for cycle
        if ( in_array( $val, $visitedArr, true ) )
        {
          $resultStr .= '*';
        }
        else
        {
          $visitedArr[] = $val;
          $resultStr .= '[{' . toKeyValString( $val, $maxDepth, $visitedArr ) . '}]';
        }
      }
      else if ( is_array( $val ) )
      {
        if ( isset( $val['__toKeyValString_visited__'] ) || in_array( '__toKeyValString_visited__', $val ) )
        {
          $resultStr .= '*';
        }
        else
        {
          if ( isAssociative( $val ) )
          {
            $val['__toKeyValString_visited__'] = true;
          }
          else
          {
            $val[] = '__toKeyValString_visited__';
          }
          $resultStr .= '[{' . toKeyValString( $val, $maxDepth, $visitedArr ) . '}]';
        }
      }
      elseif ( is_null( $val ) )
      {
        $resultStr .= "";
      }
      else
      {
        $resultStr .= htmlspecialchars( strval( $val ) );
      }

      $resultStr .= ', ';
    }
  }

  // remove non-printable characters
  $resultStr = preg_replace( '/[^[:print:]]/', '', $resultStr );

  return rtrim( $resultStr, ', ' );
}

/**
 * timeUTC
 *
 * Returns UTC timestamp
 *
 * @return int
 */
function timeUTC()
{
  $oldTZ = date_default_timezone_get();

  date_default_timezone_set('UTC');
  $ts = time();

  date_default_timezone_set($oldTZ);

  return $ts;
}

/**
 * getSMSQualityOfService
 *
 * Returns sms quality service grade code determined by configuration and smsText
 *
 * @return Integer
 */
function getSMSQualityOfService($smsText)
{
  $allowedQualities = explode(' ', \Ultra\UltraConfig\amdocs_sms_qualityofservice());

  return (mb_check_encoding($smsText, 'ASCII') && in_array(SMS_QUALITY_OF_SERVICE_ASCII, $allowedQualities))
    ? SMS_QUALITY_OF_SERVICE_ASCII
    : SMS_QUALITY_OF_SERVICE_UNICODE;
}

function find_first_value( $fieldName , $arrayOfAssociativeArrays )
{
  $value = NULL;

  foreach( $arrayOfAssociativeArrays as $associativeArray )
    if ( ! empty( $associativeArray[ $fieldName ] ) )
      $value = $associativeArray[ $fieldName ];

  return $value;
}



/**
 * jsonEncode
 * wrapper for json_encode that catches and logs any errors
 * needed mostly in production since dirty data from DB often results in encoding failures:
 * splunk query for 'ultraErrorHanlder json_encode' will produce plenty of candidates
 * @param Mixed data to encode
 * @return String encoded data or NULL on error
 */
function jsonEncode($data)
{
  $result = NULL;

  try
  {
    $encoded = json_encode($data);
    if ($encoded === FALSE)
      throw new Exception(json_last_error_msg());
    $result = $encoded;
  }

  catch(Exception $e)
  {
    $message = $e->getMessage();
    logError('EXCEPTION: ' . ($message ? $message : 'no message returned') . ' when encoding data: ' . print_r($data, TRUE));
  }

  return $result;
}

/**
 * isSameBrand
 * Check if plans belong to the same brand by comparing plan prefix
 * @param  String  $aka_1 ex. L19 || UV30
 * @param  String  $aka_2
 * @return Boolean if $aka_1 and $aka_2 belong to same brand
 */
function isSameBrand($aka_1, $aka_2)
{
  $aka1Config = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($aka_1));
  $aka2Config = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($aka_2));

  if (!empty($aka1Config['brand_id']) && !empty($aka2Config['brand_id']))
  {
    if ($aka1Config['brand_id'] == $aka2Config['brand_id'])
      return TRUE;
  }

  return FALSE;
}

/**
 * isSameBrandByICCID
 * Check if iccids belong to the same brand by comparing ICCID BRAND_ID
 * @param  String  $iccid_1
 * @param  String  $iccid_2
 * @return Boolean if $iccid_1 and $iccid_2 belong to same brand
 */
function isSameBrandByICCID($iccid_1, $iccid_2)
{
  $iccid_1_brand = get_brand_id_from_iccid($iccid_1);
  $iccid_2_brand = get_brand_id_from_iccid($iccid_2);

  \logDebug("Checking ICCID brands: OLD_ICCID $iccid_1_brand , NEW_ICCID $iccid_2_brand");

  if ( ! $iccid_1_brand || ! $iccid_2_brand || $iccid_1_brand != $iccid_2_brand)
    return FALSE;

  return TRUE;
}

/**
 * parseSmsIntoParts
 *
 * clean and split inbound SMS message into parts
 * at this time we support inbound SMS in ASCII only (no foreign languages)
 * @param String message
 * @return Array message parts or empty Array on failure
 */
function parseSmsIntoParts($message)
{
  // clean message
  $clean = preg_replace('/[^a-zA-Z0-9]/', ' ', $message);
  logInfo("clean: '$message' -> '$clean'");

  // split on white spaces and analyze the result
  $parts = explode(' ', $clean);
  $result = array();
  foreach ($parts as $part)
    if ( ! empty($part))
      $result[] = $part;
  logInfo('parsed: ' . json_encode($result));

  return $result;
}

/**
 * monthNamesShort
 *
 * List of months, 3 chars length
 *
 * @return array
 */
function monthNamesShort()
{
  return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
}

/**
 * monthNamesToDigit
 *
 * Maps 3 chars length months to digit ( padded with 0 )
 *
 * @return string
 */
function monthNamesToDigit( $monthNameShort )
{
  $monthNamesShort = monthNamesShort();

  $i = 0;
  $n = count( $monthNamesShort );

  while( ( $i < $n ) && ( strtoupper( $monthNameShort ) != strtoupper( $monthNamesShort[$i] ) ) )
    $i++;

  return str_pad(($i+1),2,'0',STR_PAD_LEFT);
}

/**
 * lowercase all array keys
 *
 * @param  array
 * @return array
 */
function array_change_key_case_recursive($arr)
{
  return array_map(function($item) {
    if (is_array($item))
      $item = array_change_key_case_recursive($item);
      return $item;
  }, array_change_key_case($arr));
}

/**
 * randomAlphanumericString
 *
 * Returns a random alphanumeric string
 *
 * @param  int
 * @return string
 */
function randomAlphanumericString( $length )
{
  $str = '';
  $alphabet = 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789';
  for ( $i = 0; $i < $length; $i++ )
  {
    $n = rand( 0, strlen( $alphabet ) - 1 );
    $str .= $alphabet[$n];
  }

  return $str;
}
