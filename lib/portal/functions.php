<?php


# synopsis:
#$r = funcCarrierLookup(array( 'msisdn_list' => '19142551841,19142551840' ));

# output:
# Array
#(
# [portability] => Array
# (
#  [0] => Array
#  (
#   [$msisdn] => Array
#   (
#    [0] => 6959
#    [1] => VERIZON WIRELESS-NY
#    [2] => WIRELESS
#   )
#  )
# )
# [errors] => Array
# (
# )
#)


include_once('db.php');
include_once('core.php');
include_once('lib/provisioning/functions.php');
include_once('web.php');
include_once 'lib/util-common.php';

// connection parameters to improve code robustness: a network call lasting longer than 4 secons
// usually indicates network or server problems, so cut it short ASAP
define('OCN_CONNECT_TIMEOUT', 4);   // give up if cannot connect, seconds
define('OCN_RESPONSE_TIMEOUT', 4);  // give up if cannot receive response, seconds


function funcCarrierLookup($params)
{
  # Given a comma-separated list of MSISDNs, performs a lookup against the numberportabilitylookup.com API.
  # Response Codes: North American Numbering Plan
  # Use 1 at the beginning of USA numbers.
  # If your query was successful, and you are using the latest version of the API (by passing apiver=2.4 in your query string), the response code will consist of three values separated by commas:
  # 1. The OCN Code of the network to which the number belongs
  # 2. The name of the provider
  # 3. MOBILE or LANDLINE
  # Return the carrier name and OCN code as well as echo to log.
  # curl -i 'https://secure.comcetera.com/npl' -d 'user=&pass=&apiver=2.4&msisdn=19142551841'

  $result = array(
    'portability' => array(),
    'errors'      => array(),
    'timeout'     => FALSE
  );

  try
  {
    // API specs: http://www.numberportabilitylookup.com/api
    # $url = 'https://secure.comcetera.com/npl';
    $url = 'http://api.comcetera.com/npl';

    // init HTTP session
    $conn = curl_init($url);
    if (! $conn)
      throw new \Exception("failed session initialization for URL: $url");

    $credentials = get_comcetera_credentials();
    if (empty($credentials['user']) || empty($credentials['pass']))
      throw new \Exception('failed to get Comcetera credentials');

    $post = array(
      'user'   => $credentials['user'],
      'pass'   => $credentials['pass'],
      'apiver' => '2.5',
      'msisdn' => $params['msisdn_list'] # comma-separated list of phone numbers
    );

    $options = array(
      CURLOPT_HEADER          => FALSE,
      CURLOPT_RETURNTRANSFER  => TRUE,
      CURLOPT_POST            => TRUE,
      CURLOPT_POSTFIELDS      => $post,
      CURLOPT_CONNECTTIMEOUT  => OCN_CONNECT_TIMEOUT,
      CURLOPT_TIMEOUT         => OCN_RESPONSE_TIMEOUT,
      CURLOPT_HTTPHEADER      => array('Expect:')
    );

    if (! curl_setopt_array($conn, $options))
      throw new \Exception('failed session configuration for options: ' .  print_r($options, TRUE));

    // call Comcetera API
    $response = curl_exec($conn);
    if (! $response)
      throw new \Exception('failed Comcetera API call: ' . curl_error($conn), curl_errno($conn));

    // check API result
    $code = curl_getinfo($conn, CURLINFO_HTTP_CODE);
    if ($code != 200)
      throw new \Exception("failed Comcetera API call with HTTP code: $code");
    curl_close($conn);

    // check that results contains start and end keywords
    if (! preg_match('/QUERYOK/', $response) || ! preg_match('/ENDBATCH/', $response))
    {
      if (preg_match("/FAIL Insufficient credit/", $response, $matches))
        dlog('','ERROR ESCALATION ALERT DAILY - Comcetera API Failed due to insufficient credit');

      throw new \Exception('received invalid API response: ' . print_r($response, TRUE));
    }

    // parse result
    $lines = preg_split('/\n|\r/', $response, -1, PREG_SPLIT_NO_EMPTY);
    foreach( $lines as $line )
      if ( ( $line != 'QUERYOK' ) && ( $line != 'ENDBATCH' ) ) // response begings with QUERYOK and ends with ENDBATCH
      {
        $data = explode(' ', $line, 2);
        $result['portability'][] = array($data[0] => explode(',', $data[1]));
      }
  }
  catch (\Exception $except)
  {
    // handle transient network errors
    if (in_array($except->getCode(), array(
      CURLE_COULDNT_RESOLVE_PROXY,
      CURLE_COULDNT_RESOLVE_HOST,
      CURLE_COULDNT_CONNECT,
      CURLE_OPERATION_TIMEDOUT)))
      $result['timeout'] = TRUE;

    // log error message
    $error = $except->getMessage();
    dlog('', "ERROR: $error");
    $result['errors'][] = $error;
  }

  return $result;
}


function get_comcetera_credentials()
{
  return array(
    'user'   => find_credential('numberportability/u'),
    'pass'   => find_credential('numberportability/p')
  );
}

