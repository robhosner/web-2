<?php


// module to interface with Shipwire APIs


// shipwire API invoked in a state change to Shipped or in a 'reship' transitions from shipped->shipped
// Order id:
// Customer-number(hyphen)order# where order# is 1,2,3


// FulfillmentServices : submit order(s)
// http://www.shipwire.com/w/developers/xml-order-submitter/

// InventoryServices : inventory information
// http://www.shipwire.com/w/developers/xml-real-time-inventory-2/

// TrackingServices : get status of submitted orders
// http://www.shipwire.com/w/developers/xml-tracking-status/

// RateServices : shipping quotes for orders
// http://www.shipwire.com/w/developers/xml-shipping-rate-api/


include_once('db.php');
include_once('db/htt_shipwire_log.php');
include_once('lib/util-common.php');


function api_InventoryServices($params)
{
  $xml_content = build_xml_InventoryServices( $params );

  $api_InventoryServices_params = array(
    'xml_content'  => $xml_content,
    'url'          => 'https://api.shipwire.com/exec/InventoryServices.php',
    'api_xml_name' => 'InventoryUpdateXML'
  );

  $output = api_call_shipwire($api_InventoryServices_params);

  $output['xml_result_as_object'] = simplexml_load_string($output['xml_result']);

  if ( ! $output['xml_result_as_object'] )
  {
    $output['success'] = FALSE;
    $output['error']   = 'Cannot parse XML output';
  }
  else
  {
    unset($output['xml_result']);

    $output['data'] = get_data_api_InventoryServices( $output['xml_result_as_object'] );
  }

  return $output;
}


function api_TrackingServices($params)
{
  $xml_content = build_xml_TrackingServices( $params );

  $api_TrackingServices_params = array(
    'xml_content'  => $xml_content,
    'url'          => 'https://api.shipwire.com/exec/TrackingServices.php',
    'api_xml_name' => 'TrackingUpdateXML'
  );

  $output = api_call_shipwire($api_TrackingServices_params);

  $output['xml_result_as_object'] = simplexml_load_string($output['xml_result']);

  if ( ! $output['xml_result_as_object'] )
  {
    $output['success'] = FALSE;
    $output['error']   = 'Cannot parse XML output';
  }
  else
  {
    unset($output['xml_result']);

    $output['data'] = get_data_api_TrackingServices( $output['xml_result_as_object'] );
  }

  return $output;
}


function api_FulfillmentServices($params)
{
  $xml_content = build_xml_FulfillmentServices( $params );

  $api_FulfillmentServices_params = array(
    'xml_content'  => $xml_content,
    'url'          => 'https://api.shipwire.com/exec/FulfillmentServices.php',
    'api_xml_name' => 'OrderListXML'
  );

  $output = api_call_shipwire($api_FulfillmentServices_params);

  $output['xml_result_as_object'] = simplexml_load_string($output['xml_result']);

  if ( ! $output['xml_result_as_object'] )
  {
    $output['success'] = FALSE;
    $output['error']   = 'Cannot parse XML output';
  }
  else
  {
    unset($output['xml_result']);

    $output['data'] = get_data_api_FulfillmentServices( $output['xml_result_as_object'] );

    foreach( $params['orders'][0]['items'] as $item )
    {

      $htt_shipwire_log_insert_query = htt_shipwire_log_insert_query(
        array(
          "customer_id"     => $params['customer_id'],
          "order_id"        => $output['data']['order_id'],
          "order_number"    => $output['data']['order_number'],
          "order_status"    => $output['data']['order_status'],
          "transaction_id"  => $output['data']['transaction_id'],
          "shipwire_status" => $output['data']['status'],
          "warehouse"       => $output['data']['warehouse'],
          "service"         => $output['data']['service'],
          "cost"            => $output['data']['cost'],
          "code"            => $item['code'],
          "quantity"        => $item['quantity']
        )
      );

      if ( ! is_mssql_successful(logged_mssql_query($htt_shipwire_log_insert_query)) )
      {
        $output['warnings'][] = 'Cannot insert into DB table htt_shipwire_log';
      }

    }
  }

  return $output;
}


function get_data_api_InventoryServices( $xml_result_as_object )
{

  $data = array(
    'status'               => (string)$xml_result_as_object->Status,
    'total_products'       => (string)$xml_result_as_object-> TotalProducts,
    'product_data'         => array()
  );

  $attributes = array(
    'code',
    'quantity',
    'good',
    'pending',
    'backordered',
    'reserved',
    'shipping',
    'shipped',
    'consuming',
    'consumed',
    'creating',
    'created',
    'availableDate',
    'shippedLastDay',
    'shippedLastWeek',
    'shippedLast4Weeks',
    'orderedLastDay',
    'orderedLastWeek',
    'orderedLast4Weeks'
  );

  foreach( $xml_result_as_object->Product as $product)
  {
    $product_data = array();

    foreach ($attributes as $key)
    {
      $product_data[ $key ] = (string)$product[ $key ];
    }

    $data['product_data'][] = $product_data;
  }

  return $data;
}


function get_data_api_TrackingServices( $xml_result_as_object )
{
  $data = array(
    'status'               => (string)$xml_result_as_object->Status,
    'total_orders'         => (string)$xml_result_as_object->TotalOrders,
    'total_shipped_orders' => (string)$xml_result_as_object->TotalShippedOrders,
    'total_products'       => (string)$xml_result_as_object->TotalProducts,
    'bookmark'             => (string)$xml_result_as_object->Bookmark,
    'orders'               => get_order_data_api_TrackingServices( $xml_result_as_object )
  );

  return $data;
}

function get_order_data_api_TrackingServices( $xml_element )
{
  $order_data = array();

  $attributes = array(
    'id',
    'shipwireId',
    'shipped',
    'shipper',
    'shipperFullName',
    'shipDate',
    'expectedDeliveryDate',
    'handling',
    'shipping',
    'packaging',
    'total',
    'returned',
    'returnDate',
    'returnCondition',
    'href',
    'affiliateStatus',
    'manuallyEdited'
  );

  foreach( $xml_element->Order as $order)
  {
    $data = array(
      'tracking_number' => trim( (string)$order->TrackingNumber )
    );

    foreach ($attributes as $key)
    {
      $data[ $key ] = (string)$order[ $key ];
    }

    $order_data[]= $data;
  }

  return $order_data;
}


function get_data_api_FulfillmentServices( $xml_result_as_object )
{
  # here we assume a single order per FulfillmentServices API call

  $data = array(
    'status'         => (string)$xml_result_as_object->Status,
    'transaction_id' => (string)$xml_result_as_object->TransactionId,
    'warehouse'      => (string)$xml_result_as_object->OrderInformation->Order->Shipping->Warehouse,
    'service'        => (string)$xml_result_as_object->OrderInformation->Order->Shipping->Service,
    'cost'           => (string)$xml_result_as_object->OrderInformation->Order->Shipping->Cost,
    'order_number'   => (string)$xml_result_as_object->OrderInformation->Order['number'],
    'order_id'       => (string)$xml_result_as_object->OrderInformation->Order['id'],
    'order_status'   => (string)$xml_result_as_object->OrderInformation->Order['status']
  );

  return $data;
}


function api_call_shipwire($params)
{
  $result = array(
    'success'  => FALSE,
    'warnings' => array()
  );

  $xml_content = $params['xml_content'];

  $api_xml_name = $params['api_xml_name'];

  $url = $params['url'];

  dlog('',"api_call_shipwire url = ".$url);
  dlog('',"api_call_shipwire xml_name = ".$api_xml_name);
  dlog('',"api_call_shipwire xml_content = ".$xml_content);

  $xml_content = urlencode($xml_content);

  $urlConn = curl_init($url);

  curl_setopt($urlConn, CURLOPT_POST,           1);
  curl_setopt($urlConn, CURLOPT_HTTPHEADER,     array("Content-type", "application/x-www-form-urlencoded"));
  curl_setopt($urlConn, CURLOPT_POSTFIELDS,     $api_xml_name."=".$xml_content);
  curl_setopt($urlConn, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($urlConn, CURLOPT_SSL_VERIFYPEER, 0);

  $curl_result = curl_exec($urlConn);

  if (empty($curl_result))
  {
    $result['error'] = curl_error($urlConn);
  }
  else
  {
    $result['success'] = TRUE;
    $result['xml_result']  = $curl_result;
  }

  return $result;
}


function get_shipwire_credentials()
{
  # credentials I am currently using for testing

  return array(
    'email'  => 'rgalli@ultra.me',
    'passwd' => 'aab5ef524778cb45'
  );
}


function get_shipwire_server_name()
{
  # either Test or Production.  Requests posted with the “Test” server environment specified simply return well-formatted responses for basic testing of your parser.  Requests posted with the “Production” server specified return actual data based on information in the associated Shipwire account.

  return find_credential('shipwire/env');
}


/**
 * build_xml_orders_address
 *
 * AddressN values are limited to 40 characters, this function split the address to up to 3 fields
 *
 * @return string
 */
function build_xml_orders_address( $address_1 , $address_2 )
{
  $address_1 = trim( $address_1 );
  $address_2 = trim( $address_2 );

  if ( ( strlen($address_1) <= 40 ) && ( strlen($address_2) <= 40 ) )
    return '
      <Address1>'.$address_1.'</Address1>
      <Address2>'.$address_2.'</Address2>';

  $full_address = $address_1.$address_2;

  $address_parts = str_split($full_address, 40);

  if ( count($address_parts) == 2 )
    return '
      <Address1>'.$address_parts[0].'</Address1>
      <Address2>'.$address_parts[1].'</Address2>';

  return '
    <Address1>'.$address_parts[0].'</Address1>
    <Address2>'.$address_parts[1].'</Address2>
    <Address3>'.$address_parts[2].'</Address3>';
}


function build_xml_orders($params)
{
  $xml_orders = '';

  foreach($params as $order_params)
  {
    $xml_items = build_xml_items($order_params['items']);

    if ( ! isset($order_params['warehouse']) )
    {
      $order_params['warehouse'] = '00';
    }

    $xml_address = build_xml_orders_address( $order_params['address_1'] , $order_params['address_2'] );

    $xml_orders .= '
  <Order id="'.$order_params['order_id'].'">
    <Warehouse>'. $order_params['warehouse'].'</Warehouse>
    <AddressInfo type="ship">
      <Name>
        <Full>'.  $order_params['full_name'].'</Full>
      </Name>
      '.$xml_address.'
      <City>'.    $order_params['city'].     '</City>
      <State>'.   $order_params['state'].    '</State>
      <Country>'. $order_params['country'].  '</Country>
      <Zip>'.     $order_params['zip_code']. '</Zip>
      <Phone>'.   $order_params['phone'].    '</Phone>
      <Email>'.   $order_params['email'].    '</Email>
    </AddressInfo>'.$xml_items.'
  </Order>';
  }

  return $xml_orders;
}


function build_xml_items($params)
{
  $xml_items = '';

  foreach($params as $item_params)
  {
    $xml_items .= '
    <Item num="0">
      <Code>'.    $item_params['code'].    '</Code>
      <Quantity>'.$item_params['quantity'].'</Quantity>
    </Item>';
  }

  return $xml_items;
}


function build_xml_TrackingServices($params)
{
  $shipwire_credentials = get_shipwire_credentials();

  # http://www.shipwire.com/exec/download/TrackingUpdate.dtd
  # possible tracking parameters: Bookmark , OrderNo , ShipwireId

  $email  = $shipwire_credentials['email'];
  $passwd = $shipwire_credentials['passwd'];

  $server = get_shipwire_server_name();

  $tracking_xml_content = '';

  if ( isset( $params['bookmark'] ) )
  {
    $tracking_xml_content .= '<Bookmark>'.$params['bookmark'].'</Bookmark>';
  }

  if ( isset( $params['order_no'] ) )
  {
    $tracking_xml_content .= '<OrderNo>'.$params['order_no'].'</OrderNo>';
  }

  if ( isset( $params['shipwire_id'] ) )
  {
    $tracking_xml_content .= '<ShipwireId>'.$params['shipwire_id'].'</ShipwireId>';
  }

  # normally I would not include an XML document like this. Time constraints. |[
  $xml_content = '<TrackingUpdate>
  <Username>'.$email.'</Username>
  <Password>'.$passwd.'</Password>
  <Server>'.$server.'</Server>
  '.$tracking_xml_content.'
</TrackingUpdate>';

  return $xml_content;
}


function build_xml_InventoryServices($params)
{
  $shipwire_credentials = get_shipwire_credentials();

  $email  = $shipwire_credentials['email'];
  $passwd = $shipwire_credentials['passwd'];

  $server = get_shipwire_server_name();

  # normally I would not include an XML document like this. Time constraints. |[
  $xml_content = '<InventoryUpdate>
  <EmailAddress>'.$email.'</EmailAddress>
  <Password>'.$passwd.'</Password>
  <Server>'.$server.'</Server>
</InventoryUpdate>';

  return $xml_content;
}


function build_xml_FulfillmentServices($params)
{
  $shipwire_credentials = get_shipwire_credentials();

  $email  = $shipwire_credentials['email'];
  $passwd = $shipwire_credentials['passwd'];

  $server = get_shipwire_server_name();

  $warehouse = "00"; # constant for now

  $referer   = $params['referer'];

  $store_account_name = 'YAHOOSTORES';

  $xml_orders = build_xml_orders($params['orders']);

  # normally I would not include an XML document like this. Time constraints. |[
  $xml_content = '<OrderList StoreAccountName="'.$store_account_name.'">
  <EmailAddress>'.$email.'</EmailAddress>
  <Password>'.$passwd.'</Password>
  <Server>'.$server.'</Server>
  <Referer>'.$referer.'</Referer>
  '.$xml_orders.'

</OrderList>';

  return $xml_content;
}


/**
 * get_shipment_product_code
 * returns Shipwire product ID for a given brand
 * @see API-298
 * @param Integer brand ID (HTT_CUSTOMERS_OVERLAY_ULTRA.BRAND_ID)
 * @return String product ID or NULL on failure
 */
function get_shipment_product_code($brandId)
{
  // Ultra Mobile is default
  if (empty($brandId))
    $brandId = 1;

  // get brand name
  if ( ! $brand = \Ultra\UltraConfig\getBrandFromBrandId($brandId))
    return logError("unknown brand ID $brandId");

  // Shipwire product ID by brand name
  switch ($brand['short_name'])
  {
    case 'ULTRA':
      return 'ULTRA-KIT1-DUAL';

    case 'UNIVISION':
      return 'UNI-PURPLE-DUAL';

    case 'MINT':
      return 'UM001059';

    default:
      return logError("unknown brand name {$brand['short_name']}");
  }
}

