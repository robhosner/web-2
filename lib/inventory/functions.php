<?php

require_once 'db.php';
require_once 'db/htt_inventory_sim.php';
require_once 'lib/util-common.php';
require_once 'lib/inventory/functions_shipwire.php';
require_once 'classes/Result.php';
require_once 'Ultra/Lib/DB/Cache.php';


# synopsis

#list ($valid,$warning) = validate_start_end_ICCID("890126084210225638","890126084210225639",2500,2100)."\n";

#$valid = validate_ICCID('890126084210225639',1);

#$iccid_user_status = get_ICCID_user_status('890126084210225639');

#$errors = batch_update_htt_inventory_sim(
#  array(
#    'startICCID'       => '890126084210225630',
#    'endICCID'         => '890126084210225634',
#    'inventory_status' => 'TEST'
#  )
#);

#$errors = batch_update_htt_inventory_sim(
#  array(
#    'startICCID'            => '890126084210225630',
#    'endICCID'              => '890126084210225634',
#    'inventory_masteragent' => 'TEST'
#  )
#);

#$errors = batch_update_htt_inventory_sim(
#  array(
#    'startICCID'            => '800000000000000001',
#    'endICCID'              => '800000000000000003',
#    'inventory_masteragent' => 543210,
#    'inventory_status'      => 'TEST STATUS',
#    'last_changed_by'       => 'TEST BY',
#    'last_changed_date'     => 'getutcdate()'
#  )
#);

#$errors = batch_verify_htt_inventory_sim_status(
#  array(
#    'startICCID'           => '890126084210225630',
#    'endICCID'             => '890126084210225634',
#    'NOT#inventory_status' => 'AT_FOUNDRY'
#  )
#);

# echo descriptive_ICCID_range('890126084210225630','890126084210225634')."\n";


/**
 * get_sim_info_from_fake_msisdn
 *
 * 1 - first convert the fake msisdn to an IMSI. Check htt_inventory_sim for that IMSI (most of the time, this will return a row). If found, skip to step 3
 * 2 - if could not find a row in htt_inventory_sim based on the IMSI, we can find the ICCID by using the formula in the above link, and then wildcarding the vendor digits and checksum. Search for that partially-wildcarded ICCID in htt_inventory_sim. For example, 890126084**1200029*
 * 3 - once a row has been located, utilize either the ACT_CODE or ICCID_FULL column
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/Dummy+MSISDN
 *
 * @return Result object
 */
function get_sim_info_from_fake_msisdn( $fake_msisdn )
{
  dlog('',"fake_msisdn = $fake_msisdn");

  // sanity check
  if ( ! $fake_msisdn || ! is_numeric($fake_msisdn) || ( strlen( $fake_msisdn ) != 14 ) )
    return make_error_Result( 'ERR_API_INVALID_ARGUMENTS: invalid input' , array( 'error_code' => 'NS0002' ) );

  $digits = preg_split( "//" , $fake_msisdn );

  $imsi = '310'.$digits[1].$digits[2].'0'.$digits[3].$digits[4].'1'.$digits[9].$digits[10].$digits[11].$digits[12].$digits[13].$digits[14];

  dlog('',"imsi = $imsi");

  $sim_data = get_htt_inventory_sim_from_imsi( $imsi );

  if ( $sim_data && is_array($sim_data) && count($sim_data) )
  {
    // we found the SIM in HTT_INVENTORY_SIM

    return make_ok_Result(
      array(
        'sim_data' => $sim_data[0]
      )
    );
  }
  else
  {
    // we could not find the SIM in HTT_INVENTORY_SIM , search again by using a wildcarded ICCID

    $wildcarded_iccid = '8901'.$digits[1].$digits[2].'0'.$digits[3].$digits[4].'[0-9][0-9]'.'1'.$digits[9].$digits[10].$digits[11].$digits[12].$digits[13].$digits[14];

    $sim_data = get_htt_inventory_sim_from_wildcarded_iccid( $wildcarded_iccid );

    if ( $sim_data && is_array($sim_data) && count($sim_data) )
      return make_ok_Result(
        array(
          'sim_data' => $sim_data[0]
        )
      );
    else
      return make_error_Result( 'ERR_API_INVALID_ARGUMENTS: ICCID not found' , array( 'error_code' => 'IC0001' ) );
  }
}


function descriptive_ICCID_range($startICCID,$endICCID)
{
  # returns a string describing the range between $startICCID and $endICCID

  return
    substr($startICCID,0,14) . ' ' . substr($startICCID,-4) . '-' . substr($endICCID,-4);
}


function validate_start_end_ICCID($startICCID,$endICCID,$max_ICCID_range,$warning_ICCID_range)
{
  # prevent accidental overscanning.
  # 2500 is our leeway
  # but if they ship 10 boxes they will make 10 calls
  # rejection at 2500 and warning to log at 2100

  $valid   = FALSE;
  $warning = FALSE;

  $resource_diff = gmp_sub($endICCID,$startICCID);

  if ( gmp_sign($resource_diff) > -1 )
  {
    # $startICCID <= $endICCID

    if ( gmp_cmp( $resource_diff , "$max_ICCID_range" ) == 1 )
    {
      # $endICCID - $startICCID > $max_ICCID_range
    }
    else
    {
      # $endICCID - $startICCID <= $max_ICCID_range

      $valid = TRUE;

      if ( gmp_cmp( $resource_diff , "$warning_ICCID_range" ) == 1 )
      {
        # $endICCID - $startICCID > $warning_ICCID_range

        $warning = TRUE;
      }
    }
  }
  else
  {
    # $startICCID > $endICCID
  }

  return array($valid,$warning);
}

/**
 * get_ICCID_user_status
 *
 * @return string
 */
function get_ICCID_user_status($ICCID, $allow_extended = NULL, &$record = NULL)
{
  $status = 'INVALID';

  $htt_inventory_sim_select_query = '';

  if ( strlen($ICCID) == 19 )
  {
    $htt_inventory_sim_select_query = htt_inventory_sim_select_query(
      array(
        "iccid_full" => $ICCID
      )
    );
  }
  elseif (strlen($ICCID) == 18)
  {
    $htt_inventory_sim_select_query = htt_inventory_sim_select_query(
      array(
        "iccid_number" => $ICCID
      )
    );
  }

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    // return full row if requested
    if ($record)
      $record = $query_result[0];

    if ($query_result[0]->SIM_ACTIVATED == 1)
      $status = 'USED';
    elseif (NULL != $query_result[0]->RESERVATION_TIME)
      $status = NULL == $allow_extended ? 'USED' : 'RESERVED';
    else
      $status = 'VALID';
  }
  else
    \logDebug( $ICCID . ' not found in htt_inventory_sim' );

  return $status;
}

/**
 * validate_ICCID
 *
 * Check if ICCID exists
 *
 * @param iccid string or object: if string then ICCID (18 or 19 chars), if object then HTT_INVENTORY_SIM row (avoid DB query)
 * @param check_unused boolean: optionally check if is not used or reserved
 * @return boolean
 */
function validate_ICCID($ICCID, $check_unused)
{
  $valid = FALSE;
  $sim = NULL;

  // fetch from DB is ICCID number is given
  if (! is_object($ICCID))
  {
    if ( strlen($ICCID) == 19 )
      $ICCID = substr($ICCID, 0, 18);

    $htt_inventory_sim_select_query = htt_inventory_sim_select_query(array('iccid_number' => $ICCID));

    $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

    if ( ( is_array($query_result) ) && count($query_result) > 0 )
      $sim = $query_result[0];
    else
      return $valid;
  }
  else
    $sim = $ICCID;

  // verify that the ICCID is not used if requested
  if ( isset($check_unused) && $check_unused )
  {
    if ( override_test_iccid($sim->ICCID_FULL) )
    {
      dlog('',"(*) ICCID validation overridden (*)");

      return TRUE;
    }

    $valid = ( $sim->SIM_ACTIVATED == 0 );

    /* this SIM is reserved */
    if (NULL != $sim->RESERVATION_TIME)
    {
      dlog('', 'Failing validation of ICCID %s because it is reserved', $sim->ICCID_FULL);
      $valid = FALSE;
    }
  }
  else
  {
    $valid = TRUE;
  }

  return $valid;
}

/**
 * override_test_iccid
 *
 * In DEV environments we override checks for ICCID in the given range:
 * 101010101010101000 - 101010101010101100
 *
 * @return boolean
 */
function override_test_iccid($iccid)
{
  if ( ! \Ultra\UltraConfig\isDevelopmentDB() )
    return FALSE;

  return ! ! ( substr($iccid,0,7) == '1010101' );
}

/**
 * override_test_msisdn
 *
 * In DEV environments we override checks for MSISDN if equal to 1001001000
 *
 * @return boolean
 */
function override_test_msisdn($msisdn)
{
  if ( ! \Ultra\UltraConfig\isDevelopmentDB() )
    return FALSE;

  return ! ! ( $msisdn == '1001001000' );
}

function batch_verify_htt_inventory_sim_status($params)
{
  $errors = array();

  dlog("","batch_verify_htt_inventory_sim_status");

  $startICCID       = $params['startICCID'];
  $endICCID         = $params['endICCID'];

  # divide the select query in batches: select 50 sims at a time

  $current_iccid = $startICCID;

  $batch_sim_set = array($startICCID);

  while (
    ( gmp_cmp( $current_iccid , $endICCID ) != 0 ) # ( $current_iccid != $endICCID )
    &&
    ( ! count($errors) )
  )
  {
    $current_iccid = gmp_add( $current_iccid , 1 ); # $current_iccid++;

    $batch_sim_set[] = gmp_strval($current_iccid);

    if ( count( $batch_sim_set ) == 50 )
    {
      $errors = perform_batch_verify_htt_inventory_sim_status( $batch_sim_set , $params );

      $batch_sim_set = array();
    }
  }

  if ( ( ! count($errors) ) && ( count($batch_sim_set) ) )
  {
    $errors = perform_batch_verify_htt_inventory_sim_status( $batch_sim_set , $params );
  }

  return $errors;
}


function batch_update_htt_inventory_sim($params)
{
  $errors = array();

  dlog("","batch_update_htt_inventory_sim");

  $startICCID       = $params['startICCID'];
  $endICCID         = $params['endICCID'];

  # divide the update query in batches: update 50 sims at a time

  $current_iccid = $startICCID;

  $batch_sim_set = array($startICCID);

  while (
    ( gmp_cmp( $current_iccid , $endICCID ) != 0 ) # ( $current_iccid != $endICCID )
    &&
    ( ! count($errors) )
  )
  {
    $current_iccid = gmp_add( $current_iccid , 1 ); # $current_iccid++;

    $batch_sim_set[] = gmp_strval($current_iccid);

    if ( count( $batch_sim_set ) == 50 )
    {
      $errors = perform_batch_update_htt_inventory_sim( $batch_sim_set , $params );

      $batch_sim_set = array();
    }
  }

  if ( ( ! count($errors) ) && ( count($batch_sim_set) ) )
  {
    $errors = perform_batch_update_htt_inventory_sim( $batch_sim_set , $params );
  }

  return $errors;
}


function perform_batch_verify_htt_inventory_sim_status( $batch_sim_set , $params )
{
  dlog("","perform_batch_verify_htt_inventory_sim_status");

  $errors = array();

  $query_params = array( "iccid_numbers" => $batch_sim_set);

  if ( isset($params['NOT#inventory_status']) )
  {
    $query_params["NOT#inventory_status"] = $params['NOT#inventory_status'];
  }

  if ( isset($params['ShipSIMMaster']) )
  {
    $query_params["ShipSIMMaster"] = $params['ShipSIMMaster'];
  }

  $htt_inventory_sim_select_query = htt_inventory_sim_select_query( $query_params );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

  if ( is_array($query_result) )
  {
    if ( count($query_result) > 0 )
    {
      $errors[] = "ERR_INV_INVALID_STATE_CHANGE: Found SIMs with invalid status";
    }
  }
  else
  { $errors[] = "ERR_API_INTERNAL: Error while querying DB"; }

  return $errors;
}


function perform_batch_update_htt_inventory_sim( $batch_sim_set , $params )
{
  $errors = array();  

  $update_parms = $params;

  $update_parms['batch_sim_set'] = $batch_sim_set;

  $htt_inventory_sim_update_query = htt_inventory_sim_update_query( $update_parms );

  if ( is_mssql_successful(logged_mssql_query($htt_inventory_sim_update_query)) )
  {
    # success
  }
  else
  { $errors[] = "ERR_API_INTERNAL: Error while updating DB"; }

  return $errors;
}


function generate_shipsim_order_id($customer_id)
{
  $sub_order_id = generate_shipsim_sub_order_id($customer_id);

  $shipsim_order_id = $customer_id.'-'.$sub_order_id;

  $shipwire_server_name = get_shipwire_server_name();

  if ( $shipwire_server_name == "Production" ) { $shipsim_order_id = 'P'.$shipsim_order_id; }

  return $shipsim_order_id;
}

/**
 * inventory_individual_ship_sim
 * create a replacement SIM shipment order for a customer via default fullfillment provider
 * validate subscriber shipping address, ship product type based on subscriber's brand
 * @param Integer customer ID
 * @param String calling function name
 * @return Array ('success' => Boolean, 'errors' => Array(String error message), 'order_id' => String, codes => Array(String error code))
 */
### function inventory_individual_shipsim($customer_id, $warehouse, $referrer, $code, $quantity=1, $customer=NULL)
### inventory_individual_shipsim($customer->CUSTOMER_ID, '00', __FUNCTION__, get_shipment_product_code(), 1, $customer);
function inventory_individual_ship_sim($customer_id, $referrer)
{
  // initialization
  $cache = new \Ultra\Lib\DB\Cache();
  $result = array(
    'success'   => FALSE,
    'error'     => NULL,
    'code'      => NULL,
    'order_id'  => NULL);

  try
  {
    // fetch customer shipping record
    if ( ! $customer = $cache->selectRow('CUSTOMERS', 'CUSTOMER_ID', $customer_id, SECONDS_IN_DAY, FALSE))
      throw new \Exception('VV0031');

    // validate shipping address
    if (empty($customer->ADDRESS1) || empty($customer->CITY) || empty($customer->STATE_REGION) || empty($customer->POSTAL_CODE))
      throw new \Exception('VV0008');
    $address = array(
      'street'  => $customer->ADDRESS1,
      'suite'   => $customer->ADDRESS2,
      'city'    => $customer->CITY,
      'state'   => $customer->STATE_REGION,
      'zipcode' => $customer->POSTAL_CODE,
      'country' => $customer->COUNTRY);
    list($matches, $error) = Ultra\Lib\Util\validateStreetAddress($address);

    // check result: multiple matches mean address did not validate, however allow shipment if no matches found (address may be incomplete)
    if ( ! empty($error) && count($matches) > 1)
      throw new \Exception('VV0009');

    // use corrected shipping address if matched exactly
    if (count($matches) == 1)
    {
      $match = $matches[0];
      $customer->ADDRESS1 = $match->street;
      $customer->ADDRESS2 = $match->suite;
      $customer->CITY = $match->city;
      $customer->STATE_REGION = $match->state;
      $customer->POSTAL_CODE = $match->zipcode . (empty($match->zip_plus) ? NULL : "-{$match->zip_plus}");
    }

    // fetch customer overlay record
    if ( ! $overlay = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', $customer_id, SECONDS_IN_DAY, FALSE))
      throw new \Exception('VV0031');

    // prepare shipment order parameters
    $order_id = generate_shipsim_order_id($customer_id);
    $product = get_shipment_product_code($overlay->BRAND_ID);
    $warehouse = '00';
    $quantity  = 1;

    // create shipment order and check result
    $status = inventory_individual_sim_shipwire(
      array(
        'customer'  => $customer,
        'warehouse' => $warehouse,
        'order_id'  => $order_id,
        'referrer'  => $referrer,
        'code'      => $product,
        'quantity'  => $quantity)
    );
    logInfo('status: ' . json_encode($status));
    if ( ! $status['success'])
      throw new \Exception('IN0011');
    $result['order_id'] = $order_id;
    $result['success'] = TRUE;
  }

  catch(\Exception $exp)
  {
    $code = $exp->getMessage();
    $error = $cache->selectUserError($code);
    $result['error'] = $error->EN_MESSAGE;
    $result['code'] = $code;
  }

  return $result;
}


function inventory_individual_sim_shipwire($params)
{
  // Website or customer service triggers a shipment message to Shipwire to send off a SIM.

  $customer  = $params['customer'];
  $warehouse = $params['warehouse'];
  $order_id  = $params['order_id']; # $customer_id . '-' . $order_ord ; where $order_ord is 1,2,3,...
  $referrer  = $params['referrer'];
  $code      = $params['code'];
  $quantity  = $params['quantity'];

  # updated_by  ?
  # masteragent ?

  // set default name and email address
  if (!empty($customer->FIRST_NAME) && !empty($customer->LAST_NAME)) {
    $name = $customer->FIRST_NAME . ' ' . $customer->LAST_NAME;
  } else if (!empty($customer->FIRST_NAME)) {
    $name = $customer->FIRST_NAME;
  } else if (!empty($customer->LAST_NAME)) {
    $name = $customer->LAST_NAME;
  } else {
    $name = 'Ultra Subscriber';
  }
  $email = !empty($customer->E_MAIL) ? $customer->E_MAIL : 'inventory@ultra.me';

  $shipment_params =
    array(
      'customer_id' => $customer->CUSTOMER_ID,
      'referer'     => $referrer,
      'orders'      => array(
        array(
          'order_id'  => $order_id,
          'warehouse' => $warehouse,
          'full_name' => $name,
          'address_1' => $customer->ADDRESS1,
          'address_2' => $customer->ADDRESS2,
          'city'      => $customer->CITY,
          'state'     => $customer->STATE_REGION,
          'country'   => $customer->COUNTRY,
          'zip_code'  => $customer->POSTAL_CODE,
          'phone'     => $customer->LOCAL_PHONE,
          'email'     => $email,
          'items'     => array(
            array( 'code' => $code , 'quantity' => $quantity ),
          )
        )
      )
    );

  $result = api_FulfillmentServices($shipment_params);

  return $result;
}


function get_shipping_data($customer_id)
{
  # retrieve SIM shipping data from DB

  $return_values = array( 'shipping_estimate' => NULL, 'errors' => array() );

  # most recent shipment
  $htt_shipwire_log_select_query = htt_shipwire_log_select_query(
    array(
      'customer_id' => $customer_id,
      'sql_limit'   => 1,
      'order_by'    => 'ORDER_NUMBER DESC'
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_shipwire_log_select_query));

  if ( is_array($query_result) )
  {
    if ( count($query_result) == 0 )
    {
      $return_values['errors'][] = "ERR_API_INTERNAL: DB error";
    }
    else
    {
      $return_values['shipping_estimate'] = $query_result[0]->LOG_DATE; # TODO: FIXME: we don't store shipping estimate in htt_shipwire_log !!!
      $return_values['tracking_number']   = $query_result[0]->TRANSACTION_ID;
    }
  }

  return $return_values;
}


function get_shipwire_log_data_by_customer_id( $customer_id )
{
  $result = array(
    'success'     => FALSE,
    'errors'      => array(),
    'order_ids'   => '',
    'order_dates' => ''
  );

  $htt_shipwire_log_select_query = htt_shipwire_log_select_query(
    array(
      'customer_id' => $customer_id,
      'order_by'    => 'ORDER_NUMBER DESC'
    )
  );

  $htt_shipwire_data = mssql_fetch_all_objects(logged_mssql_query($htt_shipwire_log_select_query));

  if ( $htt_shipwire_data && is_array($htt_shipwire_data) && count($htt_shipwire_data) )
  {
    $comma = '';

    for ( $i=0 ; $i< count($htt_shipwire_data) ; $i++ )
    {
      $result['order_ids']   .= $comma . $htt_shipwire_data[ $i ]->ORDER_NUMBER;
      $result['order_dates'] .= $comma . get_date_from_full_date($htt_shipwire_data[ $i ]->LOG_DATE);

      $comma = ',';
    }

    $result['success'] = TRUE;
  }
  else
  { $result['errors'][] = "ERR_API_INVALID_ARGUMENTS: no data found"; }

  return $result;
}


/**
 * Ensures there are at least N zipcodes are in the list
 * Algorithm:
 * Count the number of SIMs in *htt_inventory_sim* from $startICCID to $endICCID
 * N = ( sqrt ( count of sims in range ) ) / 3
 * The count of zip codes must be >= N
 * @param string $startICCID
 * @param string $endICCID
 * @param array  $zipCodeList
 */
function validate_sim_zip_code_distribution( $startICCID, $endICCID, array $zipCodeList )
{
    // If an 18 digit ICCID, then make one with checksum, and escape the data in the query
    $fullStartICCID  = luhnenize($startICCID); 
    $fullEndICCID    = luhnenize($endICCID);
    $status          = new Result(); // Create our function return object
    
    // De-duplicate Array, make sure all zip codes are unique
    $uniqueArrayZips = array_unique( $zipCodeList );
    
    // Count the SIMS in Inventory in the specified Start and End Range
    $query = sprintf( "select count(*) as 'SIMS'
            from htt_inventory_sim
            where 
              ICCID_FULL BETWEEN %s and %s", mssql_escape_with_zeroes($fullStartICCID), mssql_escape_with_zeroes($fullEndICCID));
    
    $rowSet = mssql_fetch_all_objects(logged_mssql_query($query));

    if ( $rowSet && is_array($rowSet) && count($rowSet) == 1 ) // should always happen if database is up
    {
        $simCount   = $rowSet[0]->SIMS;
        $uniqueZips = count( $uniqueArrayZips );
        $N          = ( sqrt( $simCount ) ) / 3;
        
        if( $simCount == 0 ) 
        {
            $errorMsg = sprintf("ERR_API_INTERNAL: No Sims in range returned: Count of Unique Zips=%d. Sims in range specified=%d", $uniqueZips, $simCount);
            $status->add_error($errorMsg);
            return $status;
        }

        if ( $uniqueZips >= $N && ( $uniqueZips !== 0 ) ) // Guard against no valid sims in range and zero zip codes as a success case.
        {
            // Success
            $status->succeed();
        }
        else 
        {
            // 'Need More Zips'
            $errorMsg = sprintf("ERR_API_INTERNAL: Need more Zips. Count of Unique Zips=%d. The number of zip codes must be > %d", $uniqueZips, $N );
            $status->add_error($errorMsg);
        }
    }
    else 
    {
      // Fatal Error
      $status->add_error("ERR_API_INTERNAL: No records returned for ICCID range");
    } 

    return $status;
}


/**
 * reserve_iccid
 */
function reserve_iccid($iccid)
{
  $iccid_18 = $iccid;
  $iccid_19 = $iccid;

  if (strlen($iccid) == 18)
    $iccid_19 = luhnenize($iccid_18);
  else
    $iccid_18 = substr($iccid_19,0,-1);

  return update_column2(
    'htt_inventory_sim',
    array('iccid_number' => array($iccid_18, $iccid_19)),
    array('reservation_time' => array('GETUTCDATE()')));
}


/**
 * unreserve_iccid
 */
function unreserve_iccid($iccid)
{
  $iccid_18 = $iccid;
  $iccid_19 = $iccid;

  if (strlen($iccid) == 18)
    $iccid_19 = luhnenize($iccid_18);
  else
    $iccid_18 = substr($iccid_19,0,-1);

  return update_column2(
    'htt_inventory_sim',
    array('iccid_number' => array($iccid_18, $iccid_19)),
    array('reservation_time' => NULL));
}


/**
 * resurrect_iccid
 *
 * Unblocks an inactive ICCID so that it can be used for subsequent activations
 *
 * @return object of class \Result
 */
function resurrect_iccid( $iccid , $msisdn=NULL )
{
  $iccid = luhnenize($iccid);

  dlog('',"iccid = $iccid");
  dlog('',"msisdn = $msisdn");

  // make sure the ICCID is in our DB

  $htt_inventory_sim_select_query = htt_inventory_sim_select_query( array( "iccid_full" => $iccid ) );

  $htt_inventory_sim_data = \Ultra\Lib\DB\fetch_objects($htt_inventory_sim_select_query);

  if ( ! ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) ) )
    return make_error_Result('ICCID not found');

  dlog('',"HTT_INVENTORY_SIM data = %s",$htt_inventory_sim_data);

  // make sure the ICCID is not Active

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $querySubInput = array('actionUUID' => __FUNCTION__.time());
  ($msisdn ? $querySubInput['msisdn'] = $msisdn : $querySubInput['iccid'] = $iccid);

  $result = $mwControl->mwQuerySubscriber($querySubInput);

  if ( $result->is_failure() )
    return $result;

  dlog('',"mwQuerySubscriber data = %s",$result->data_array);

  if ( isset($result->data_array['body'])
    && property_exists( $result->data_array['body'] , 'SubscriberStatus' )
    && $result->data_array['body']->SubscriberStatus
  )
    return make_error_Result('mwQuerySubscriber returned SubscriberStatus = '.$result->data_array['body']->SubscriberStatus);

  if ( ! isset($result->data_array['ResultCode'])
    || ! isset($result->data_array['ResultMsg'])
  )
    return make_error_Result('mwQuerySubscriber invalid output');

  if ( isset($result->data_array['CurrentAsyncService']) 
    && $result->data_array['CurrentAsyncService']
  )
    return make_error_Result('There is a stuck asynch command ('.$result->data_array['body']->CurrentAsyncService.'). Please escalate');

  if ( ! in_array( $result->data_array['ResultCode'] , array(200,202,347) )
    || ! in_array( $result->data_array['ResultMsg']  , array('Invalid ICCID','The MSISDN does not exist','The MSISDN does not belong to the Channel id') )
  )
    return make_error_Result('ICCID cannot be resurrected because mwQuerySubscriber returned '.$result->data_array['ResultMsg']);

  // update HTT_INVENTORY_SIM

  if ( ! htt_inventory_sim_resurrect_iccid( $iccid ) )
    return make_error_Result('DB error while updating HTT_INVENTORY_SIM');

  // get customers with this ICCID

  $overlay_data = get_ultra_customers_from_iccid( $iccid , array('CUSTOMER_ID','PLAN_STATE','CURRENT_MOBILE_NUMBER') );

  if ( $overlay_data && is_array($overlay_data) && count($overlay_data) )
  {
    // fix customers with this ICCID

    dlog('',"overlay_data %s",$overlay_data);

    foreach( $overlay_data as $customer )
    {
      $check = htt_cancellation_reasons_add(
        array(
          'customer_id' => $customer->CUSTOMER_ID,
          'reason'      => __FUNCTION__,
          'msisdn'      => $customer->CURRENT_MOBILE_NUMBER,
          'iccid'       => $iccid,
          'type'        => 'CANCELLED',
          'cos_id'      => '0',
          'status'      => $customer->PLAN_STATE,
          'agent'       => __FUNCTION__
        )
      );

      dlog('',"insert into HTT_CANCELLATION_REASONS result = ".($check?'OK':'KO'));

      $sql = sprintf(
        "update htt_customers_overlay_ultra set plan_state = 'Cancelled' , current_iccid = NULL , CURRENT_ICCID_FULL = NULL , ACTIVATION_ICCID = null , ACTIVATION_ICCID_FULL = NULL , current_mobile_number = NULL where customer_id = %d",
        $customer->CUSTOMER_ID
      );

      $check = is_mssql_successful(logged_mssql_query($sql));

      dlog('',"HTT_CUSTOMERS_OVERLAY_ULTRA update result = ".($check?'OK':'KO'));
    }
  }

  return make_ok_Result();
}


?>
