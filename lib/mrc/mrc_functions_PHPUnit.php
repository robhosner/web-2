<?php

global $credentials;

require_once 'cosid_constants.php';
require_once 'db.php';

require_once 'classes/PHPUnitBase.php';
require_once 'lib/mrc/functions.php';
require_once 'classes/CustomerOptions.php';

class mrcFunctionsTest extends PHPUnitBase
{
  public function __construct()
  {
    teldata_change_db();

    $this->customer_option_sql = '
      SELECT TOP 1 co.CUSTOMER_ID 
      FROM ULTRA.CUSTOMER_OPTIONS as co with (nolock)
      JOIN htt_customers_overlay_ultra as hcou with (nolock)  ON hcou.CUSTOMER_ID = co.CUSTOMER_ID
      WHERE OPTION_ATTRIBUTE = \'%s\' ORDER BY co.CUSTOMER_ID DESC';
  }

  // customer stored value should be += plan cost of plan
  public function test__process_house_account()
  {
    $res = find_first(sprintf($this->customer_option_sql, MRC_HOUSE_ACCOUNT));

    $customer = get_customer_from_customer_id($res->CUSTOMER_ID);

    $start_stored_value = $customer->stored_value;

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);

    list ($error, $customer) = process_house_account($customer, $customer_options);

    $this->assertEquals($customer->stored_value, $start_stored_value + $customer->plan_cost);
  }

  // customer stored value should be += plan cost of plan
  public function test__process_dealer_promo()
  {
    $res = find_first(sprintf($this->customer_option_sql, BILLING_OPTION_DEMO_LINE));

    $customer = get_customer_from_customer_id($res->CUSTOMER_ID);

    $start_stored_value = $customer->stored_value;

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);

    list ($error, $customer) = process_dealer_promo($customer, $customer_options);

    $this->assertEquals($customer->stored_value, $start_stored_value + $customer->plan_cost);
  }

  // customer stored value should be += plan cost of plan
  public function test__process_promo_plan()
  {
    $res = find_first(sprintf($this->customer_option_sql, BILLING_OPTION_PROMO_PLAN));

    $customer = get_customer_from_customer_id($res->CUSTOMER_ID);

    $start_stored_value = $customer->stored_value;

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);

    list ($error, $customer) = process_promo_plan($customer, $customer_options);

    $this->assertEquals($customer->stored_value, $start_stored_value + $customer->plan_cost);
  }

  public function test__process_bogo_month()
  {
    $res = find_first(sprintf($this->customer_option_sql, BILLING_OPTION_ATTRIBUTE_BOGO_MONTH));
    $success  = (is_object($res)) ? TRUE : FALSE;

    $this->assertTrue($success);

    $customer = get_customer_from_customer_id($res->CUSTOMER_ID);

    // set OPTION_VALUE to 0
    $sql = 'UPDATE ULTRA.CUSTOMER_OPTIONS SET OPTION_VALUE = 0 WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE = \'%s\'';
    $res = logged_mssql_query(sprintf($sql, $customer->CUSTOMER_ID, BILLING_OPTION_ATTRIBUTE_BOGO_MONTH));

    // should remove option value after calling \redeemed_bogo_month
    $redis = new \Ultra\Lib\Util\Redis;

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);
    list ($error, $customer) = process_bogo_month($customer, $redis, $customer_options);

    // with option removed, find_first should return false
    $sql = 'SELECT TOP 1 * FROM ULTRA.CUSTOMER_OPTIONS WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE = \'%s\'';
    $res = find_first(logged_mssql_query(sprintf($sql, $customer->CUSTOMER_ID, BILLING_OPTION_ATTRIBUTE_BOGO_MONTH)));

    $this->assertFalse($res);
  }

  public function test__process_promo_7_11()
  {
    // customer plan period is outside of promo eligibility
    $customer = get_customer_from_customer_id(18812);

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);

    list ($error, $customer) = process_promo_7_11($customer, $customer_options);

    $this->assertEquals($error, 'CUSTOMER ' . $customer->CUSTOMER_ID . ' has 7-11 promo option but is outside plan period eligibility');
  }

  public function test__process_multi_month()
  {
    $res = find_first(sprintf($this->customer_option_sql, BILLING_OPTION_MULTI_MONTH));

    $customer = get_customer_from_customer_id($res->CUSTOMER_ID);

    $start_stored_value = $customer->stored_value;

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);

    list ($error, $customer) = process_multi_month($customer, $customer_options);

    $this->assertEquals($customer->stored_value, $start_stored_value + $customer->plan_cost);
  }

  public function test__process_uv_subplan()
  {
    // customer in subplan A
    $customer = get_customer_from_customer_id(20116);

    $customer_options = new \CustomerOptions($customer->CUSTOMER_ID);

    list ($error, $customer) = process_uv_subplan($customer, $customer_options);

    $this->assertEquals($error, null);
  }
}
