<?php

// $data is a list of array with the following fields
// msisdn , deactivation_date , carrier_name , carrier_spid
function cancel_ported_out($cancel_ported_out_data)
{
  $errors = array();

  foreach($cancel_ported_out_data as $id => $data)
  {
    $cancel_errors = cancel_ported_out_customer($data);

    if ( $cancel_errors )
    { $errors = array_merge($cancel_errors,$errors); }
  }

  return $errors;
}

function cancel_ported_out_customer($data)
{
  $errors = array();

  $customer = get_customer_from_msisdn($data['msisdn']);

  if ( $customer )
  {
    $results = get_customer_state($customer);

    if ( count($results['errors']) == 0 )
    {
      if ( $results['state']['state'] == 'Cancelled' )
      {
        // customer already cancelled, we will insert a row into HTT_CANCELLATION_REASONS if not there already

// TODO
      }
      else
      {

      }
    }
    else
    {
      $errors = $results['errors'];
    }
  }

  return $errors;
}

?>
