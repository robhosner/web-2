<?php

/**
 * utility functions for PHP runners
 */


/**
 * readCommandLineArguments
 * read runner command line arguments in format 'param=value parame=value'
 * @param Array of default arguments with their values (e.g. array('mode' => 'execute', 'children' => 10, ...))
 * @param Boolean if TRUE then we ignore malformatted parameters, if FALSE then return NULL if a parameter found not formatted as value=pair
 * @returns Array of updated values as read from the command line arguments (e.g. array('mode' => 'sleep', 'children' => 0, ...)) or NULL on failure
 */
function readCommandLineArguments($params = NULL, $ignore = TRUE)
{
  // check default parameters
  if ( ! is_array($params))
  {
    if ($params === NULL)
      $params = array();
    else
    {
      logError('invalid default parameters');
      return NULL;
    }
  }
  logInfo('input parameters: ' . jsonEncode($params));

  // read all command line argument pairs
  global $argc, $argv;
  for ($i = 1; $i < $argc; $i++)
  {
    $param = $argv[$i];
    $pair = explode('=', $param);
    if (count($pair) != 2)
    {
      logError("malformed parameter '$param'");
      if ( ! $ignore)
        return NULL;
    }
    else
      $params[$pair[0]] = $pair[1];
  }

  logInfo('output parameters: ' . jsonEncode($params));
  return $params;
}


/**
 * readNormalizedCommandLineArguments
 * read command line arguments and process them according to given type and default values
 * arguments with missing descriptions will be added as strings
 * similar to function readCommandLineArguments but with additional functionality
 * @see util-runners_PHPUnit.php for use examples
 * @param Array (String NAME => array('default' => VALUE, 'type' => TYPE))
 * @param Boolean TRUE to ingore malformed arguments, FALSE othewise
 * @return Array (String NAME => VALUE) or NULL on error
 */
function readNormalizedCommandLineArguments($input, $ignore)
{
  // validate input
  if ( ! is_array($input))
  {
    logError('input is not array');
    return NULL;
  }

  // compose default input array of NAME => VALUE pairs
  $defaults = array();
  foreach ($input as $name => $description)
    if (array_key_exists('default', $description))
      $defaults[$name] = $description['default'];

  // read comamnd line arguments
  if ( ! $result = readCommandLineArguments($defaults, $ignore))
    return $result;

  // normalize arguments
  foreach ($result as $name => $value)
    if (isset($input[$name]) && isset($input[$name]['type']) && gettype($value) != $input[$name]['type'])
    {
      logInfo("param $name, value: $value, expected type: {$input[$name]['type']}, actual type: " . gettype($value));
      switch ($input[$name]['type'])
      {
        case 'array':
          $result[$name] = explode(',', $value);
          break;
        case 'boolean':
          $result[$name] = in_array(strtoupper($value), array('TRUE', 'YES', '1'));
          break;
        case 'integer':
          $result[$name] = (integer)$value;
          break;
        case 'string':
          break;
        default:
          logError("unknown type {$input[$name]['type']}");
      }
    }

  logInfo('result: ' . jsonEncode($result));
  return $result;
}


/**
 * createPidFile
 *
 * check if another process with the same name is already running, create a PID file if not
 * @param string pid file location
 * @param string process name
 * @return boolean TRUE on success (we created our own PID file) or FALSE on failure
 */
function createPidFile($dir, $name)
{
  $path = "$dir/$name.pid";
  $pid  = NULL;

  // check if PID file is present
  if (file_exists($path) && $pid = file_get_contents($path))
  {
    $pid = (int)trim($pid);
    logInfo("found existing PID file $path with PID $pid");

    // check if process PID inside the file is still running
    if (file_exists("/proc/$pid"))
      logInfo("process $pid is still running");
    else
    {
      logInfo("process $pid is not running anymore");
      unlinkPidFile($dir, $name);
      return FALSE;
    }

    return FALSE;
  }

  // open our PID file
  if ( ! $file = fopen($path, 'w'))
  {
    logError("failed to open PID file $path for writing");
    return FALSE;
  }

  // write our PID to file
  if ( ! fwrite($file, sprintf('%s', getmypid())))
  {
    logError("failed to write our PID to file $path");
    return FALSE;
  }

  logInfo("saved our PID to $path");
  fclose($file);
  return TRUE;
}


/**
 * unlinkPidFile
 *
 * @return boolean
 */
function unlinkPidFile($dir, $name)
{
  $path = "$dir/$name.pid";

  return unlink( $path );
}


/**
 * readOrderedCommandLineArguments
 *
 * read unnamed command line arguments in the order provided
 * @param Array names that command line arguments will be assigned in the result
 * @param Integer command line argument offset (position in $argv to start reading from)
 * @param Boolean TRUE if arguments are required (generate an error) or optional (set to NULL if missing)
 * @return Array associative of read parameters or NULL on failure
 */
function readOrderedCommandLineArguments($names, $offset, $required)
{
  global $argv;
  $result = array();
  foreach ($names as $index => $name)
  {
    if ($required && ! isset($argv[$index + $offset]))
      return logError("missing required parameter '$name'");
    $result[$name] = isset($argv[$index + $offset]) ? $argv[$index + $offset] : NULL;
  }
  return $result;
}
