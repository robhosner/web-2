<?php

/**
 * utility functions for working with files
 */


/**
 * removeDirectory
 * remove a directory and its content recursively while logging everything
 * stops at first errror
 * @param String full path
 * @returns Boolean TRUE on success, FALSE on failre
 */
function removeDirectory($dir)
{
  if (substr($dir, -1) == '/')
    $dir = substr($dir, 0, -1);

  if ( ! file_exists($dir))
  {
    \logError("directory $dir does not exist");
    return FALSE;
  }

  $items = array_diff(scandir($dir), array('.', '..'));
  foreach ($items as $item)
    if (is_dir("$dir/$item") && ! is_link("$dir/$item"))
    {
      if ( ! removeDirectory("$dir/$item"))
      {
        \logError("failed to delete $dir/$item");
        return FALSE;
      }
    }
    else
    {
      \logInfo("removing file $dir/$item");
      if ( ! unlink("$dir/$item"))
      {
        \logError("failed to delete $dir/$item");
        return FALSE;
      }
    }

  \logInfo("removing directory $dir");
  if ( ! rmdir($dir))
  {
    \logError("failed to delete $dir");
    return FALSE;
  }

  return TRUE;
}
