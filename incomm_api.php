<?php

/**
 * InComm phase 2
 * same as ultra_api.php but uses InComm\Lib\Api class to parse input and return results in InComm XML format
 * @see https://issues.hometowntelecom.com:8443/browse/RTRIN-1
 * @author VYT, 2015-01
 */

putenv("UNIT_TESTING=0");

require_once 'core.php';
require_once 'session.php';
require_once 'web.php';
require_once 'lib/util-common.php';
require_once 'classes/postageapp.inc.php';
require_once 'Ultra/Lib/Api.php';
require_once 'InComm/Lib/Api.php';

$api = new \InComm\Lib\Api;
if ($api->result->has_errors())
{
  dlog('', 'API error: %s', $api->result->get_errors());
  $api->terminate( array() );
}
else
  $api->execute();

