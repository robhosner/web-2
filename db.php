<?php

include_once('core.php');
include_once('db/billing.php');
require_once 'db/htt_inventory_pin.php';
include_once('db/ultra_cc_holders.php');
include_once('db/ultra_cc_holder_tokens.php');
require_once 'db/ultra_cc_transactions.php';
include_once('db/customer_misc.php');
require_once 'db/monthly_service_charge_log.php';
require_once 'db/ultra_demo_line.php';
include_once('db/htt_accounts_acquisition.php');
include_once('db/htt_billing_actions.php');
include_once 'db/htt_inventory_simbatches.php';
include_once('db/ultra_incomm_inventory_sim.php');
include_once('db/ultra_acc_migration_tracker.php');
require_once 'db/ultra_bolton_tracker.php';
require_once 'db/ultra_brands.php';
require_once 'db/ultra_intl_fixed.php';
require_once 'db/ultra_multi_month_overlay.php';
require_once 'db/ultra_promo_broadcast_attempt_log.php';
require_once 'db/ultra_promo_broadcast_campaign.php';
require_once 'db/ultra_promo_broadcast_log.php';
require_once 'db/ultra_summary_dealer_recharge_rate.php';
include_once('db/ultra_surcharge_history.php');
require_once 'db/ultra_webpos_actions.php';
require_once 'db/ultra_acc/soap_log.php';
require_once 'db/ultra_acc/mrc_throttle_updates.php';
require_once 'db/ani_format.php';
include_once('Ultra/Lib/Util/Redis.php');

require_once 'classes/Rodeo.php';
require_once 'classes/PlanConfig.php';

$teldata_connected = FALSE;

// MSSQL options used in mssql_fetch_all_objects (bit flags)
define('MSSQL_FLAG_CAPITALIZE',  0b00000001); // capitalize all schema of result objects and associative arrays
define('MSSQL_FLAG_CLEAN_PRINT', 0b00000010); // remove all non-printable characters from strings
define('MSSQL_FLAG_TRIM_SPACES', 0b00000100); // trim all strings of leading and trailing spaces
// combinations of the flags above
define('MSSQL_FLAGS_TPC',        0b00000111); // trim, printable, capitalize (not safe - changes schema)
define('MSSQL_FLAGS_TP',         0b00000110); // trim, printable (safe)

function connect_db($params)
{
  $db_name = $params['db_name'];

  $OVERRIDE_DB_NAME = getenv('OVERRIDE_DB_NAME');

  if ( isset( $OVERRIDE_DB_NAME ) && $OVERRIDE_DB_NAME )
    $db_name = $OVERRIDE_DB_NAME;

  return \Ultra\Lib\DB\connect($db_name,find_credential('db/host'));
}

function teldata_change_db()
{
  global $teldata_connected;

  $db_name = find_credential('db/name');

  if ( empty( $db_name ) )
  {
    \logError("No db/name available for DB connection!");
    return NULL;
  }

  $link = connect_db(
    array(
      'db_name' => $db_name
    )
  );

  $teldata_connected = TRUE;

  // Return the link so it can be used elsewhere
  return $link;
}

function mssql_quote_or_null($data)
{
  return $data == NULL ? 'NULL' : "'$data'";
}

function is_mssql_successful($res)
{
  $result = NULL;

  if (is_resource($res))
  {
    if (\Ultra\Lib\DB\isMssqlExtensionMissing()) {
      $result = !empty(mssql_get_last_message()) ? false : true;
    } else {
      $result = mssql_fetch_array($res);
    }

    // Clean up
    mssql_free_result($res);
  }
  else if (TRUE == $res)
  {
    $result = $res;
  }

  return $result;
}

/**
 * mssql_fetch_all_objects
 * @param Resource
 * @param Integer flags (see MSSQL_FLAG_ flags above)
 * @return Array of Objects
 */
function mssql_fetch_all_objects($res, $flags = 0)
{
  $result = array();

  if ( ! is_resource($res))
  {
    logError('invalid parameter given: not resource');
    return $result;
  }

  while ($row = mssql_fetch_object($res))
  {
    // capitalize all schema of result objects
    if ($flags & MSSQL_FLAG_CAPITALIZE)
    {
      $upper = new StdClass();
      foreach ($row as $property => $value)
        $upper->{strtoupper($property)} = $value;
      $row = $upper;
    }

    // flags applicable to strings only
    if ($flags & MSSQL_FLAG_CLEAN_PRINT || $flags & MSSQL_FLAG_TRIM_SPACES)
    {
      foreach ($row as $property => $value)
        if (gettype($value) == 'string')
        {
          if ($flags & MSSQL_FLAG_CLEAN_PRINT)
            $value = preg_replace('/[^[:print:]]/', '', $value);
          if ($flags & MSSQL_FLAG_TRIM_SPACES)
            $value = trim($value);
          $row->$property = $value;
        }
    }

    $result[] = $row;
  }

  mssql_free_result($res);
  return $result;
}

function mssql_count_rows($query)
{
  $results = mssql_fetch_all_objects(logged_mssql_query($query));
  if (NULL == $results) return 0;

  return count($results);
}

function mssql_has_rows($query)
{
  $count = mssql_count_rows($query);
  return ($count > 0);
}

/**
 * mssql_fetch_all_flatten
 *
 * @returns array
 */
function mssql_fetch_all_flatten($res, $limit=999999999)
{
  $result = mssql_fetch_all_rows($res, $limit);

  $underscoreObject = new \__ ;

  return $underscoreObject->flatten( $result );
}

/**
 * mssql_fetch_all_rows
 *
 * @returns array
 */
function mssql_fetch_all_rows($res, $limit=999999999)
{
  $result = array();
  $count = 0;

  if (is_resource($res))
  {
    do
    {
      while ($row = mssql_fetch_row($res))
      {
        //var_dump($row);
        $result[] = $row;
        $count++;
        if ($count >= $limit) break;
      }

      if ($count >= $limit) break;
    }
    while ( mssql_next_result($res) );
    // Clean up
    mssql_free_result($res);
  }

  return $result;
}

/**
 * batch_mssql_query
 *
 * From technet.microsoft.com :
 *  A batch is a group of one or more Transact-SQL statements sent at the same time from an application to SQL Server for execution.
 *  SQL Server compiles the statements of a batch into a single executable unit, called an execution plan. The statements in the
 *  execution plan are then executed one at a time. Each Transact-SQL statement should be terminated with a semicolon.
 *  This requirement is not enforced, but the ability to end a statement without a semicolon is deprecated and may be removed in a future version of Microsoft SQL Server.
 */
function batch_mssql_query($query, $limit=99999)
{
  $start = intval(microtime(TRUE) * 1000);

  $logged_query = cleanse_credit_card_string( preg_replace("/[ \n\r]+/"," ",$query) );

  $return = NULL;

  try
  {
    $res = mssql_query($query);

    if (is_resource($res))
    {
      $return = array();
      $count  = 0;

      do
      {
        while ($row = mssql_fetch_object($res))
        {
          $return[] = $row;
          $count++;
          if ($count >= $limit) break;
        }

        if ($count >= $limit) break;
      }
      while ( mssql_next_result($res) );

      // Clean up
      mssql_free_result($res);
    }
  }
  catch (Exception $e)
  {
    \logError( json_encode($e) );
    stack_log();
  }

  $end = intval(microtime(TRUE) * 1000);

  \logInfo( sprintf('query_time:%03d - %s', $end - $start, $logged_query) );

  return $return;
}

/**
 * logged_mssql_query
 */
function logged_mssql_query($query, $justprint=FALSE)
{
  global $teldata_connected;

  $logged_query = cleanse_credit_card_string( preg_replace("/[ \n\r]+/"," ",$query) );
  $start = intval(microtime(TRUE) * 1000);

  $ran = FALSE;
  $ret = NULL;
  $try_count = 0;

  if ($justprint)
  {
    $ret = '';
    $ran = TRUE;
  }
  else
  {
    while (!$ran)
    {
      $mssql_last_message = NULL;
      try
      {
        $ret = mssql_query($query);
        # ret should be FALSE on failure, but there exists deadlock results where this is true.
        
        $mssql_last_message = mssql_get_last_message();
        # returns '' , which == NULL. 

        if ($mssql_last_message != NULL)
        {
          if ( strpos($mssql_last_message,"Changed database context to") === false )
            dlog("", "mssql_get_last_message = ($mssql_last_message), mssql_query returns ($ret). # $try_count [$logged_query]");
#strpos requires !== and === according to : http://php.net/manual/en/function.strpos.php. Yes, PHP sucketh.
          if ( strpos($mssql_last_message,"was deadlocked on lock resources with another process and has been chosen as the deadlock victim") !== false ) {
            throw new Exception('MSSQLERROR Deadlocked Query');
          } else if ( strpos($mssql_last_message,"The ROLLBACK TRANSACTION request has no corresponding BEGIN TRANSACTION.") !== false ) {
            throw new Exception('MSSQLERROR ROLLBACK Trans without BEGIN Trans');
          } else if (strpos ($mssql_last_message,"Changed database context to") !== false ) {
            // do nothing 
          } else {
            dlog("", "MSSQLdebug : Unknown Error Message of ($mssql_last_message) with query success of ($ret). Attempt #$try_count, on [$logged_query]");
          }
        }
        else if (! $ret)
        {
          dlog ("", "MSSQLdebug : Query Failed, but without an error message. Query [$logged_query]");
        }

        $ran = TRUE;
      }
      catch (Exception $E)
      {
        $delay_length = 3000000 + rand(0, 3000000);
        dlog("", "MSSQLdebug : retry attempt #$try_count, waiting $delay_length us, exception $E on [$logged_query]");
        stack_log();
        usleep($delay_length);
      }
      $try_count++;

      if ( $try_count > 2 )
      {
        dlog("", "MSSQLdebug : gave up retries after $try_count attempts [$logged_query]");
        break;
      }
    }

    if (!$ran)
    {
      $ret = FALSE;
    }
  }

  $end = intval(microtime(TRUE) * 1000);

  if (!$justprint && $ret == FALSE)
    stack_log();

  \logInfo( sprintf('query_time:%03d ms: %d attempts : %s', $end - $start, $try_count, $logged_query) );

  return $ret;
}

function run_sql_and_check_result($query)
{
  $result = new Result();

  $check = run_sql_and_check($query);

  if ( $check )
  {
    $result->succeed();
  }
  else
  {
    $result->fail();
    $result->add_error("SQL Error");
  }

  return $result;
}

function run_sql_and_check($query)
{
  $check = is_mssql_successful(logged_mssql_query($query));
  if ($check)
  {
  }
  else
  {
    $mssql_last_message = mssql_get_last_message();
    logged_mssql_query("The [$query] query failed with result [$check]; $mssql_last_message.\n", TRUE);
  }

  return $check;
}

function make_find_status_customer_query($v)
{
  global $global_tz_offset;
  global $global_activation_tz_offset;
  global $global_dblocal_tz_offset;

  return make_find_status_customer_select_query($v,
                                                $global_tz_offset,
                                                $global_activation_tz_offset,
                                                $global_dblocal_tz_offset
  );
}

function find_first($query)
{
  $all = mssql_fetch_all_objects(logged_mssql_query($query));

  if (count($all) > 0)
  {
    return $all[0];
  }

  return FALSE;
}

// OBSOLETE
function log_acquisition($customer)
{
  global $out;

  // "44793638.1326335135.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
  $utmz = $out['utmz'];

  $utm = array();

  $matches = array();
  if (preg_match("/^[0-9.]+(utm.+)/", $utmz, $matches))
  {
    foreach (explode('|', $matches[1]) as $utmkv)
    {
      $kv = explode('=', $utmkv);
      $utm[$kv[0]] = $kv[1];
    }

    $q = htt_accounts_acquisition_insert_query($customer,$utm);

    $out['acquired'] = is_mssql_successful(logged_mssql_query($q));
  }
}

function null_or_mssql_escape_string($params,$field_name)
{
  $value = 'NULL';

  if ( isset($params[$field_name]) )
  {
    $value = mssql_escape($params[$field_name]);
  }

  return $value;
}

function build_set_statement($values)
{
  $underscoreObject = new \__ ;
  return implode(", ", $underscoreObject->map($values, function($v, $k)
                               {
                                 if (is_array($v) && count($v) > 0) // literal
                                 {
                                   $v = array_shift($v);
                                 }
                                 elseif (is_array($v))
                                 {
                                   dlog('',
                                        "BAD values: empty array %s",
                                        $values);
                                   $v = '[[[BAD VALUES: empty array]]]';
                                 }
                                 elseif (NULL != $v && preg_match('/^\d+$/',$v))
                                 {
                                   // leave numeric $v as is, unquoted
                                 }
                                 elseif (NULL != $v && preg_match('/^[-a-zA-Z0-9_=+!@#&:;, ]+$/', $v))
                                 {
                                   $v = "'$v'";
                                 }
                                 elseif (NULL == $v)
                                 {
                                   $v = 'NULL';
                                 }
                                 else // regular, non-numeric value
                                 {
                                   $v = mssql_escape_with_zeroes($v);
                                 }

                                 return "$k = $v";
                               }));
}

function build_where_clause($where_array, $scalarwanted=NULL)
{
  $clauses = array();

  foreach ($where_array as $column => $where)
  {
    if (is_array($where))
    {
      /* we will get no returns on an empty IN clause */
      if (count($where) == 0) return $scalarwanted == NULL ? array() : '1=0';

      $where_quoted = array();
      foreach ($where as $w)
      {
        $where_quoted[] = "'$w'";
      }

      $clauses[] = sprintf("$column IN (%s)", implode(', ', $where_quoted));
    }
    elseif (NULL == $where)
    {
      $clauses[] = sprintf("$column IS NULL");
    }
    elseif (TRUE === $where)
    {
      $clauses[] = sprintf("$column IS NOT NULL");
    }
    else
    {
      $clauses[] = sprintf("$column = '%s'", $where);
    }
  }

  return implode(" AND ", $clauses);
}

function get_by_column($table, $where, $extra='', $top1='TOP 1', $select_fields=NULL)
{
  if (NULL == $top1) $top1 = '';

  $clause = build_where_clause($where, $top1);
  if (NULL == $clause) return NULL;

  if ( ! $select_fields )
    $select_fields = '*';

  $q = sprintf("SELECT $top1 $select_fields FROM $table WHERE %s $extra", $clause);
  dlog('sql', $q);
  $results = mssql_fetch_all_objects(logged_mssql_query($q));

  if ( $results && is_array($results) )
  {
    if (NULL == $top1 || '' === $top1) return $results;

    foreach ($results as $row)
    {
      return $row;
    }
  }

  return NULL;
}

# an array of WHERE clauses: *a => b* or *a => NULL* or *a => TRUE* (not NULL) or *a => array(...)* (IN (...))
# an array of SET statements: *p => q* or *p => NULL* or *p => array('LITERAL THING IN QUERY')* (the SET clauses will use mssql_escape only if necessary to make the UPDATEs more readable)
# a string appended to the SET statements, default ''
# a string appended to the whole UPDATE statement, default ''

function update_column2($table, $where, $values, $extra_set='', $extra='')
{
  $clause = build_where_clause($where);
  if (NULL == $clause) return NULL;

  $set = build_set_statement($values);
  if (NULL == $set) return NULL;

  $q = "UPDATE $table SET $set $extra_set WHERE $clause $extra";
  dlog('sql', $q);
  return run_sql_and_check($q);
}

function update_column($table, $where_column, $where, $column, $val,
                       $old_val=NULL, $extra_set='', $literal_value=FALSE)
{
  $extra = $old_val ? sprintf("AND $column = '%s'", $old_val) : '';

  if (NULL == $val)
  {
    // $val is numeric, it must be quoted
    return run_sql_and_check(sprintf("UPDATE $table
SET $column = NULL $extra_set
WHERE $where_column = '%s' $extra",
                                     $where));
  }
  elseif ($literal_value)
  {
    // $val is numeric, it must be quoted
    return run_sql_and_check(sprintf("UPDATE $table
SET $column = %s $extra_set
WHERE $where_column = '%s' $extra",
                                     $val,
                                     $where));
  }
  elseif ( preg_match('/^\d+$/',$val) )
  {
    // $val is numeric, it must be quoted
    return run_sql_and_check(sprintf("UPDATE $table
SET $column = '%s' $extra_set
WHERE $where_column = '%s' $extra",
                                   $val,
                                   $where));
  }
  else
  {
    return run_sql_and_check(sprintf("UPDATE $table
SET $column = %s $extra_set
WHERE $where_column = '%s' $extra",
                                   mssql_escape($val),
                                   $where));
  }
}

function get_sql_order_by_clause($params)
{
  $order_by_clause = '';

  if ( isset($params['order_by']) && $params['order_by'] )
  {
    $order_by_clause = ' ORDER BY '.$params['order_by'];
  }

  return $order_by_clause;
}

function get_sql_top_clause($params)
{
  $top_clause = '';

  if ( isset($params['sql_limit']) && is_numeric($params['sql_limit']) )
  {
    $top_clause = ' TOP('.$params['sql_limit'].') ';
  }

  return $top_clause;
}

function get_scope_identity()
{
  $scope_identity = '';

  $sql = "select scope_identity() SCOPE_IDENTITY"; # I have to do this manually, that's sad

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    $scope_identity = $query_result[0]->SCOPE_IDENTITY;
  }

  dlog('',"scope_identity = $scope_identity");

  return $scope_identity;
}

function exec_queries_in_transaction( $sql_queries )
{
  // executes a list of queries in a transaction

  $return = array( 'errors' => array() , 'success' => FALSE );

  if ( start_mssql_transaction() )
  {

    // loop through $sql_queries
    foreach($sql_queries as $sql)
    {
      if ( count($return['errors']) == 0 )
      {
        if ( ! is_mssql_successful(logged_mssql_query($sql)) )
        { $return['errors'][] = "ERR_API_INTERNAL: Database error"; }
      }
    }

    if ( count($return['errors']) )
    {
      if ( ! rollback_mssql_transaction() )
      { $return['errors'][] = "ERR_API_INTERNAL: Database error"; }
    }
    else
    {
      if ( ! commit_mssql_transaction() )
      { $return['errors'][] = "ERR_API_INTERNAL: Database error"; }
    }

  }
  else # cannot start transaction
  { $return['errors'][] = "ERR_API_INTERNAL: Database error"; }

  if ( ! count($return['errors']) ) { $return['success'] = TRUE; }

  return $return;
}

function start_mssql_transaction()
{
  $start_mssql_transaction_query = "BEGIN TRAN";

  if (\Ultra\Lib\DB\isMssqlExtensionMissing()) {
    $check = sqlsrv_begin_transaction(\Ultra\Lib\DB\getDatabaseConnection());
  } else {
    $check = is_mssql_successful(logged_mssql_query($start_mssql_transaction_query));
  }

  if ( !$check )
    dlog('', "Cannot start mssql transaction");

  return $check;
}

function rollback_mssql_transaction()
{
  $rollback_mssql_transaction = "ROLLBACK";

  if (\Ultra\Lib\DB\isMssqlExtensionMissing()) {
    $check = sqlsrv_rollback(\Ultra\Lib\DB\getDatabaseConnection());
  } else {
    $check = is_mssql_successful(logged_mssql_query($rollback_mssql_transaction));
  }

  if ( !$check )
    dlog('', "Cannot rollback mssql transaction");

  return $check;
}

function commit_mssql_transaction()
{
  $commit_mssql_transaction = "COMMIT";

  if (\Ultra\Lib\DB\isMssqlExtensionMissing()) {
    $check = sqlsrv_commit(\Ultra\Lib\DB\getDatabaseConnection());
  } else {
    $check = is_mssql_successful(logged_mssql_query($commit_mssql_transaction));
  }

  if ( !$check )
    dlog('', "Cannot commit mssql transaction");

  return $check;
}

function set_mssql_options()
{
  return '
SET ANSI_NULLS ON;
SET ANSI_PADDING ON;
SET CONCAT_NULL_YIELDS_NULL ON;
SET ANSI_WARNINGS ON;
SET QUOTED_IDENTIFIER ON;
  ';
}

/**
 * make_query_clause_member
 *
 * create SQL clause from column name, its value and type (e.g. 'CUSTOMER_ID = 12345')
 * @input string name: column name
 * @input value: column value
 * @input char type: supported sprintf() parameter type, strings of type 'S' will be escape-encoded
 * @input string operator if not '='
 * @result string of type 'NAME = VALUE'
 * @author: VYT 14-02-11
 */
function make_query_clause_member($name, $value, $type, $operator = '=')
{
  // check input
  if (empty($name) || empty($type))
    return NULL;

  // escape special strings
  if ($type == 'S')
  {
    $value = mssql_escape_with_zeroes($value);
    $type = 's';
  }

  // return result
  return sprintf("$name $operator %$type", $value);
}

