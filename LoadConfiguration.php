<?php
class LoadConfiguration
{
  public $config;
  public $environment;
  public $configDirectory;

  public function execute(array $params)
  {
    if (empty($params[1])) {
      echo "Missing the configuration environment parameter.\n";
      exit;
    }

    $this->environment = $params[1];
    $this->configDirectory = "/home/ht/config/$this->environment";
    $ch = curl_init(getenv('CONFIG_SERVICE') . "/config/ultra-php/q/$this->environment");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    $this->validateResponse($response);
    $this->createConfigurationDirectoryStructure();
  }

  public function validateResponse($response)
  {
    $this->config = json_decode($response, true);

    if (empty($this->config['conf'])) {
      echo "\nThere was no configuration found for environment $this->environment.\n";
      exit;
    } else {
      $this->config = $this->config['conf'];
    }
  }

  public function createConfigurationDirectoryStructure()
  {
    foreach (explode(" ", $this->config['credentials']) as $credential) {
      mkdir("$this->configDirectory/$credential", 0755, true);
      file_put_contents("$this->configDirectory/$credential/cred.json", json_encode($this->config[$credential], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }

    $this->createEnvironmentFile();
  }

  public function createEnvironmentFile()
  {
    $environmentKeys = [
      "aws/key",
      "aws/pfile",
      "aws/uvbucket",
      "build/product",
      "build/stage",
      "credentials",
      "db/TSIP",
      "db/ccv",
      "git/branch",
      "git/chown/g",
      "git/chown/o",
      "git/deploy",
      "git/repo",
      "git/runas",
      "git/schedule",
      "logger/log4php",
      "mc/port",
      "mc/prefix",
      "mc/server",
      "partner/api/allow",
      "security/env",
      "www/sites",
    ];

    $environmentArray = [];

    foreach ($environmentKeys as $key) {
      if (isset($this->config[$key])) {
        $environmentArray[$key] = $this->config[$key];
      }
    }

    file_put_contents("$this->configDirectory/environment.json", json_encode($environmentArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
  }
}

(new LoadConfiguration())->execute($argv);
