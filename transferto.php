<?php
include 'lib/util-common.php';

function transferto_parse($data)
{
  if ($data) return doubleExplode("\r\n", "=", $data);

  return array();
}

function transferto_pricelist_data()
{
  return array("action" => "pricelist", "xml" => TRUE);
}

function transferto_change_db()
{
  $link = mssql_connect("db.hometowntelecom.com:1433", "transferto", "Vah}vaich9");
  mssql_select_db("TEL_DATA", $link);

  if (!$link || !mssql_select_db("TEL_DATA", $link))
  {
    die('Unable to connect or select TransferTo database!');
  }
}

function transferto_pricelist_dbupdates($pricelist)
{
  $ret = array();

  $ret[] = sprintf("DELETE FROM htt_transferto");

  foreach ($pricelist as $entry)
  {
    if (!is_array($entry)) die("unexpected string entry in pricelist: $entry");

    if (is_array($entry) &&
        array_key_exists('@attributes', $entry) &&
        array_key_exists("Product", $entry))
    {
      $id = $entry['@attributes']['id'];
      $name = $entry['@attributes']['name'];
      $country = $entry['@attributes']['country'];
      $currency = $entry['@attributes']['currency'];
      $logos = array();                 /* TODO: use LogoUrl{Png,Jpg}[123] */
      if ($id && $name && $country && $currency)
      {
        foreach ($entry['Product'] as $product)
        {
          $ret[] = sprintf("INSERT INTO htt_transferto" .
                           "(operator, country, operator_id, currency, price_name, price_value, buying_price)" .
                           "VALUES('%s','%s',%d,'%s','%s','%s',%f)",
                           mssql_escape($name), mssql_escape($country), $id,
                           mssql_escape($currency),
                           mssql_escape($product['PriceName']),
                           mssql_escape($product['PriceValue']),
                           $product['BuyingPrice']);
        }
      }
      else
      {
        return array('error' => '--entry did not have the ID, name, country, or currency',
                     'entry' => $entry);
      }
    }
    else
    {
      return array('error' => '--entry did not have the attributes or product keys',
                   'entry' => $entry);
    }
  }
}

function transferto_msisdn_info($params)
{
  $data = array('action' => 'msisdn_info',
                'destination_msisdn' => $params['destination_msisdn']);
  if (array_key_exists("operatorid", $params))
  {
    $data['operatorid'] = $params['operatorid'];
  }

  return $data;
}

function transferto_build_pricelist($operatorid)
{
  transferto_change_db();
  $products = array();
  /* this was a temporary fix for Airtel India failures, left in case
   * we need it again -tzz */
  if (TRUE || $operatorid != 1371)
  {
    $query = sprintf("select country, operator, operator_id, logo_png_1, logo_png_2, logo_png_3, price_name, price_value, ((buying_price * (1+margin))+surcharge) as cost from htt_transferto where operator_id = %d",
                     $operatorid);
    foreach (mssql_fetch_all_objects(logged_mssql_query($query)) as $product)
    {
      // fraud control: nothing over 350 INR -tzz
      if ($product->price_value <= 350)
      {
        $products[] = $product;
      }
    }
  }
  else
  {
    return array("error" => "Airtel India is disabled");
  }

  teldata_change_db();
  return $products;
}

function transferto_cost($customer, $operatorid, $product, $given=NULL)
{
  // we don't apply the tax rate if the cost is given from msisdn_info, only on topup
  if ($given) return $given;

  $tax_rate = tax_rate($customer->COS_ID);
  $wholesale = mc_read("TTO:$operatorid $product");

  if (!$wholesale) return -1;      /* should not happen */

  return sprintf("%.2f", $wholesale * (1+$tax_rate));

  /* transferto_change_db(); */
  /* $query = sprintf("select ((buying_price * (1+margin))+surcharge) as cost from htt_transferto where operator_id = %d and price_name = '%s'", */
  /*                  $operatorid, */
  /*                  mssql_escape($product) */
  /*   ); */

  /* foreach (mssql_fetch_all_objects(logged_mssql_query($query)) as $product) */
  /* { */
  /*   teldata_change_db(); */
  /*   $tax_rate = tax_rate($customer->COS_ID); */
  /*   return sprintf("%.2f", $product->cost * (1+$tax_rate)); */
  /* } */

  /* teldata_change_db(); */
  /* return -1; */
}

function transferto_topup($data, $customer, $uuid)
{
  global $out;

  if (!array_key_exists('msisdn', $data) || !$data['msisdn'])
  {
    $out['errors'][] = "Enter Airtime sender number, please.";
    return array("error" => "No MSISDN given");
  }

  if (!array_key_exists('destination_msisdn', $data) || !$data['destination_msisdn'])
  {
    $out['errors'][] = "Enter Airtime recipient number, please.";
    return array("error" => "No destination MSISDN given");
  }

  if (!array_key_exists('operatorid', $data) || !$data['operatorid'])
  {
    $out['errors'][] = "Enter Airtime destination operator, please.";
    return array("error" => "No operator ID given");
  }

  if (!array_key_exists('product', $data) || !$data['product'])
  {
    $out['errors'][] = "Enter Airtime amount, please.";
    return array("error" => "No operator product given");
  }

  $out['transferto_cost'] = $data['amount'] = transferto_cost($customer, $data['operatorid'], $data['product']);

  if ($data['amount'] < 0)
  {
    $out['errors'][] = "Unknown product $data[product] or operator $data[operatorid].";
    return array("error" => "Invalid product or operator");
  }

  $fraud = fraud_detect($customer, 'transferto_topup', $data);
  if ($fraud)
  {
    $out['errors'][] = $fraud;
    dlog("", "TRANSFERTO BLOCK $fraud " . json_encode($customer) . json_encode($data) );
    return array("error" => $fraud);
  }

  //return "--FRAUD ABOTT";

  // note we always adjust the balance so we add the COST not the TOTAL (which is COST plus TAX)
  $data['customer_record'] = $customer;
  $data['session'] = $out['session'];
  $data['adjust_balance'] = TRUE;
  $out['customer_status'] = find_customer(make_find_status_customer_query($customer->CUSTOMER));
  $data['charge_description'] = sprintf("Send %s airtime to %s (%s)",
                                        $data['product'],
                                        $data['destination_msisdn'],
                                        $uuid);

  log_acquisition($customer->ACCOUNT);
  $charge = charge($data);
  //$out['topup_charge'] = $charge;

  foreach ($charge as $charge_entry)
  {
    if (is_array($charge_entry) && array_key_exists('error', $charge_entry))
    {
      $out['errors'][] = "Your charge failed, please verify your payment information.";
      $out['f-event'] = fraud_event($customer,
                                    'transferto_topup',
                                    'charge',
                                    'error',
                                    $data);
      return "--CHARGE ERROR: " . $charge_entry['error'];
    }

    if (is_array($charge_entry) && array_key_exists('charge_check_error', $charge_entry))
    {
      $out['errors'][] = "Your charge failed, please check your payment information.";
      $out['f-event'] = fraud_event($customer,
                                    'transferto_topup',
                                    'charge',
                                    'rejected',
                                    $data);
      return "--CHARGE CHECK ERROR: " . $charge_entry['charge_check_error'];
    }
  }

  $out['f-event'] = fraud_event($customer,
                                'transferto_topup',
                                'charge',
                                'ok',
                                $data);

/*
  $description = sprintf('SENDING %s Airtime Transfer to %s', $data['amount'], $data['destination_msisdn']);
  $q = sprintf("
INSERT into BILLING
( LOGIN_NAME, ENTRY_TYPE,
  account_id, ACCOUNT, account_group,
  node, node_type,
  START_DATE_TIME, CONNECT_DATE_TIME, DISCONNECT_DATE_TIME,
  DESCRIPTION, DETAIL, AMOUNT, MODULE_NAME,
  PER_CALL_CHARGE, Per_minute_charge, PER_CALL_SURCHARGE, PER_MINUTE_SURCHARGE,
  ACTUAL_DURATION, QUANTITY, CURRENCY, CONVERSION_RATE, ANI, DNIS,
  SALES_GROUP, TAX_GROUP,
  USER_1, USER_2, USER_3, USER_4, USER_5, USER_6, USER_7, USER_8, USER_9, USER_10,
  INFO_DIGITS, RATE_INTERVAL, DISCONNECT_CHARGE, BILLING_DELAY,
  GRACE_PERIOD, ACCOUNT_TYPE, PACKAGED_BALANCE_INDEX,
  CALL_SESSION_ID, CALL_ID )
VALUES ( 'api_accounts', 10,
         %d, '%s', %d,
         'TMC', 3,
         getdate(),getdate(),getdate(),
         %s,'Airtime Transfer',%d,'PHP API',
         0,0,0,0,
         0,0,0,0,0,0,
         0,0,
         0,0,0,0,0,0,0,0,0,0,
         0,0,0,0,
         0,0,0,
         newid(), newid()); ",
               $customer->ACCOUNT_ID, $customer->ACCOUNT, $customer->ACCOUNT_GROUP_ID, mssql_escape($data['charge_description']), 0);

  $check = is_mssql_successful(logged_mssql_query($q));
*/

/*
        if ($check)
        {
          $done[] = $check;
        }
        else
        {
          $mssql_last_message = mssql_get_last_message();
          $done[] = array('error' => ($out['debug'] ? "The [$q] query failed with result [$check]; $mssql_last_message.\n" : "The query failed with result [$check]; $mssql_last_message.\n"));
        }
*/

  $ret = array(
    'action' => $data['disabled'] ? 'simulation' : 'topup',
    'operatorid' => $data['operatorid'],
    'product' => $data['product'],
    'msisdn' => $data['msisdn'],
    'destination_msisdn' => $data['destination_msisdn'],
    'sms' => $data['sms'],
    'cid1' => $uuid,
    'cid2' => $customer->CUSTOMER_ID,
    'cid3' => $data['amount'],
    );

  return $ret;
}

function transferto_enqueue($topup)
{
  $q = sprintf("
INSERT INTO dbo.htt_transferto_queue
(
  customer_id, uuid, dollar_cost,
  operator_id, product, msisdn,
  destination_msisdn,
  sms
)
VALUES
(
   %d, '%s', '%s',
   '%s', '%s', '%s',
   '%s',
   %s
);",
               $topup['cid2'], $topup['cid1'], $topup['cid3'],
               $topup['operatorid'], $topup['product'], $topup['msisdn'],
               $topup['destination_msisdn'],
               $topup['sms'] ? mssql_escape_with_zeroes($topup['sms']) : "''");

  return is_mssql_successful(logged_mssql_query($q));
}

function transferto_queue_check($customer, $uuid, $cancel)
{
  if (!$customer) return array("error" => "Invalid customer");

  // note the UUID will be escape from now on, so it's not quoted
  if ($uuid) $uuid = mssql_escape_with_zeroes($uuid);

  if ($uuid && $cancel)
  {
    $q = sprintf("UPDATE dbo.htt_transferto_queue SET status = 'CANCELLED'
WHERE customer_id = %d AND status <> 'CLOSED' AND uuid = $uuid",
                 $customer->CUSTOMER_ID);

    logged_mssql_query($q);
  }

  // get open and closed requests
  $q = sprintf("SELECT

 customer_id, uuid, status, created, closed, next_attempt,
 last_attempt, attempt_log, operator_id, product, msisdn,
 destination_msisdn, sms, transaction_id,
 DATEDIFF(ss, '1970-01-01', created) AS created_epoch,
 DATEDIFF(ss, '1970-01-01', closed) AS closed_epoch,
 DATEDIFF(ss, '1970-01-01', next_attempt) AS next_attempt_epoch,
 DATEDIFF(ss, '1970-01-01', last_attempt) AS last_attempt_epoch,
 dollar_cost

 FROM dbo.htt_transferto_queue
WHERE customer_id = %d AND DATEDIFF(ss, created, GETUTCDATE()) < 7*24*60*60 %s",
               $customer->CUSTOMER_ID,
               $uuid ? "AND uuid = $uuid" : '');

  $queue = mssql_fetch_all_objects(logged_mssql_query($q));

  foreach ($queue as $record)
  {
    $record->{'attempt_log_split'} = array();

    if (NULL != $record->{'attempt_log'})
    {
      $record->{'attempt_log_split'} = explode('; ', $record->{'attempt_log'});
      unset($record->{'attempt_log'});
    }

    $l_nSeconds = $record->{'created_epoch'};
    $l_xDate = new DateTime();
    $l_xDate->setTimestamp($l_nSeconds);
    $l_xDate->setTimezone(new DateTimeZone('PDT'));
    $record->{'created_tz_offset'} = $l_xDate->getOffset()/60;

    if ($uuid && $cancel)
    {
      //dlog("", "cancel e-mail " . json_encode($record));
      postage_topup_cancel_report($customer, $record, array('tzz@lifelogs.com', 'hometown-topup-failures@hometowntelecom.com', 'dschofield@hometowntelecom.com', 'cfurlong@gmail.com'));
    }

  }

  return $queue;
}

// this will be integer
$transferto_key = intval(microtime(TRUE) * 1000000);
function transferto_build_request($data)
{
  global $transferto_key;
  $transferto_key++;

  $u = 'indiald';
  $p = 'zwmvcnsdLn';

  $data['key'] = $transferto_key;
  $data['md5'] = md5("$u$p" . $data['key']);
  $data['login'] = $u;

  dlog("", "Built TTO request " . json_encode($data));
  return $data;
}

function transferto_interaction($data)
{
  global $request_id;

  $todo = array();
  $root = 'http://fm.transfer-to.com/cgi-bin/shop/topup';
  $options = array(CURLOPT_TIMEOUT => 3*60); // time out the request after 3 minutes

  switch ($data['action'])
  {
  case 'topup':
    dlog("", "$request_id Reserving TTO ID for topup " . json_encode($data));
    $info = transferto_interaction(array('action' => 'reserve_id'));
    dlog("", "$request_id Got TTO reservation " . json_encode($info));
    if (array_key_exists("reserved_id", $info))
    {
      $data['reserved_id'] = $info['reserved_id'];
    }
    else
    {
      dlog("", "$request_id error reserving a transaction ID: " . json_encode($info));
    }

    break;
  default:
    break;
  }

  $req = transferto_build_request($data);
  dlog("", "$request_id Running TTO transaction " . json_encode($req));

  if (array_key_exists('xml', $req))
  {
    /* TODO: this may be broken but we don't use the pricelist from PHP */
    unset($req['xml']);
    $info2_string = curl_post($root, NULL, $options, to_xml($req, 'xml'));
    $info2 = $info2_string ? simplexml_load_string($info2_string) : array();
  }
  else
  {
    dlog("", "$request_id Posting TTO transaction " . json_encode($req));
    $info2 = transferto_parse(curl_post($root, $req, $options));
  }

  return $info2;
}

?>
