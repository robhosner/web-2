<?php

require_once 'classes/PHPUnitBase.php';

class Test extends PHPUnitBase
{
  public function test_get_base_plan_name()
  {
    // test full plan name changed
    $plan = PLAN_M29;
    $base = get_base_plan_name($plan);
    $this->assertEquals(PLAN_L29, $base);

    // test full plan name unchanged
    $plan = PLAN_L19;
    $base = get_base_plan_name($plan);
    $this->assertEquals($plan, $base);

    // test short plan name changed
    $plan = 'M29';
    $base = get_base_plan_name($plan);
    $this->assertEquals('L29', $base);

    // test short plan name unchanged
    $plan = 'L44';
    $base = get_base_plan_name($plan);
    $this->assertEquals($plan, $base);

    // test something else
    $plan = 'unknown';
    $base = get_base_plan_name($plan);
    $this->assertEquals($plan, $base);
  }

  public function test_get_cos_id_from_plan()
  {
    $cos_id = \get_cos_id_from_plan('a');
    $this->assertEmpty( $cos_id );
  }

  public function test_data_socs_list()
  {
    $data_socs_list = data_socs_list();

    print_r( $data_socs_list );
  }
}

