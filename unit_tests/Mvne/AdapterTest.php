<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Mvne\Adapter;

class AdapterTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var Adapter
   */
  private $class;
  private $config;
  private $customerRepo;
  private $control;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->control = $this->getMock(Control::class);
    $this->class = new Adapter($this->control, $this->customerRepo, $this->config);
  }

  public function testRemoveFromBanCantFindCustomer()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue(false));

    $result = $this->class->mvneMakeitsoRemoveFromBan(1, 1, null, 1);
    $this->assertEquals(['success' => false, 'errors' => ['Could not load data for customer id 1 in mvneMakeitsoRemoveFromBan']], $result);
  }

  public function testRemoveFromBanSucceeds()
  {
    $customer = (object) ['current_mobile_number' => 1, 'CURRENT_ICCID_FULL' => 1, 'preferred_language' => 'EN'];
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue($customer));

    $result = new \Result();
    $result->succeed();

    $this->control->expects($this->any())
      ->method('mwMakeitsoRemoveFromBan')
      ->will($this->returnValue($result));

    $result = $this->class->mvneMakeitsoRemoveFromBan(1, 1, null, 1);
    $this->assertEquals(['success' => true, 'errors' => []], $result);
  }
}
