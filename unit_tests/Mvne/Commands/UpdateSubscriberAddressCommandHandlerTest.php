<?php

use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Mvne\Commands\UpdateSubscriberAddressCommand;
use Ultra\Mvne\Commands\UpdateSubscriberAddressCommandHandler;

class UpdateSubscriberAddressCommandHandlerTest extends PHPUnit_Framework_TestCase
{
  private $middleWareController;

  public function setUp()
  {
    $this->middleWareController = $this->getMock(Control::class);

    parent::setUp();
  }

  public function testUpdateSubscriberAddressCommandHandlerThrowsMiddleWareException()
  {
    $commandInfo = [
      'customer_id' => 1234,
      'actionUUID'  => '1qwqwer123',
      'msisdn'      => 123124124,
      'iccid'       => 12341235125,
      'E911Address' => [
        'addressLine1' => 'asdfa',
        'addressLine2' => 'ag asd ',
        'City'         => 'as asdgasdfga',
        'State'        => 'asdg ',
        'Zip'          => '09123',
      ]
    ];

    $result = new Result();
    $result->fail();

    $this->middleWareController->expects($this->any())
      ->method('mwUpdateSubscriberAddress')
      ->with($commandInfo)
      ->will($this->returnValue($result));

    $this->setExpectedException(MiddleWareException::class);

    $handler = new UpdateSubscriberAddressCommandHandler($this->middleWareController);
    $handler->handle(new UpdateSubscriberAddressCommand($commandInfo));
  }

  public function testUpdateSubscriberAddressCommandHandlerSucceeds()
  {
    $commandInfo = [
      'customer_id' => 1234,
      'actionUUID'  => '1qwqwer123',
      'msisdn'      => 123124124,
      'iccid'       => 12341235125,
      'E911Address' => [
        'addressLine1' => 'asdfa',
        'addressLine2' => 'ag asd ',
        'City'         => 'as asdgasdfga',
        'State'        => 'asdg ',
        'Zip'          => '09123',
      ]
    ];

    $result = new Result();
    $result->succeed();
    $result->add_data_array(['success' => true, 'body' => 'response stuff']);

    $this->middleWareController->expects($this->any())
      ->method('mwUpdateSubscriberAddress')
      ->with($commandInfo)
      ->will($this->returnValue($result));

    $handler = new UpdateSubscriberAddressCommandHandler($this->middleWareController);
    $handler->handle(new UpdateSubscriberAddressCommand($commandInfo));
  }
}
