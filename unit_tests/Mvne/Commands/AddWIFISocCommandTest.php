<?php

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Mvne\Commands\AddWIFISocCommand;

class AddWIFISocCommandTest extends PHPUnit_Framework_TestCase
{
  public function testFailsToCreateAddWIFISocCommand()
  {
    $commandInfo = [
      'actionUUID'         => 1234,
      'msisdn'             => 12345,
      'iccid'              => 123456,
      'customer_id'        => 123,
      'wholesale_plan'     => 1,
      'ultra_plan'         => 22,
    ];

    $this->setExpectedException(InvalidObjectCreationException::class);

    new AddWIFISocCommand($commandInfo);
  }

  public function testCreateAddWIFISocCommand()
  {
    $commandInfo = [
      'actionUUID'         => 1234,
      'msisdn'             => 12345,
      'iccid'              => 123456,
      'customer_id'        => 123,
      'wholesale_plan'     => 1,
      'ultra_plan'         => 22,
      'preferred_language' => 1,
    ];

    $command = new AddWIFISocCommand($commandInfo);
    $this->assertEquals('N-WFC|ADD', $command->option);
  }
}
