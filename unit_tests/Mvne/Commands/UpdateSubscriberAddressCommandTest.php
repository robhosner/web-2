<?php

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Mvne\Commands\UpdateSubscriberAddressCommand;

class UpdateSubscriberAddressCommandTest extends PHPUnit_Framework_TestCase
{
  public function testFailsToCreateAddWIFISocCommand()
  {
    $commandInfo = [
      'customer_id' => 0,
      'actionUUID'  => '1qwqwer123',
      'msisdn'      => 123124124,
      'iccid'       => 12341235125,
      'E911Address' => []
    ];

    $this->setExpectedException(InvalidObjectCreationException::class);

    new UpdateSubscriberAddressCommand($commandInfo);
  }

  public function testCreateAddWIFISocCommand()
  {
    $commandInfo = [
      'customer_id' => 1234,
      'actionUUID'  => '1qwqwer123',
      'msisdn'      => 123124124,
      'iccid'       => 12341235125,
      'E911Address' => [
        'addressLine1' => '123 St Name',
        'addressLine2' => 'ag asd ',
        'City'         => 'as asdgasdfga',
        'State'        => 'asdg ',
        'Zip'          => '09123',
      ]
    ];

    $command = new UpdateSubscriberAddressCommand($commandInfo);
    $this->assertEquals(1234, $command->customer_id);
    $this->assertEquals('123 St Name', $command->E911Address['addressLine1']);
  }
}
