<?php

use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Mvne\Commands\AddWIFISocCommand;
use Ultra\Mvne\Commands\AddWIFISocCommandHandler;

class AddWIFISocCommandHandlerTest extends PHPUnit_Framework_TestCase
{
  private $middleWareController;

  public function setUp()
  {
    $this->middleWareController = $this->getMock(Control::class);

    parent::setUp();
  }

  public function testAddWIFISocCommandHandlerThrowsMiddleWareException()
  {
    $commandInfo = [
      'actionUUID'         => 1234,
      'msisdn'             => 12345,
      'iccid'              => 123456,
      'customer_id'        => 123,
      'wholesale_plan'     => 1,
      'ultra_plan'         => 22,
      'preferred_language' => 1,
    ];

    $command = $commandInfo;
    $command['option'] = 'N-WFC|ADD';
    $command['extra_options'] = [ 'channel' => 'WIFI_CALLING.UNKNOWN' ];

    $result = new Result();
    $result->succeed();
    $result->add_data_array(['success' => true, 'body' => 'response stuff']);

    $this->middleWareController->expects($this->any())
      ->method('mwQuerySubscriber')
      ->with([
        'actionUUID' => $commandInfo['actionUUID'],
        'iccid'      => $commandInfo['iccid'],
        'msisdn'     => $commandInfo['msisdn']
      ])
      ->will($this->returnValue($result));

    $result = new Result();
    $result->add_error('big fail');

    $this->middleWareController->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->with($command)
      ->will($this->returnValue($result));

    $this->setExpectedException(MiddleWareException::class);

    $handler = new AddWIFISocCommandHandler($this->middleWareController);
    $handler->handle(new AddWIFISocCommand($commandInfo));
  }

  public function testAddWIFISocCommandHandlerSucceeds()
  {
    $commandInfo = [
      'actionUUID'         => 1234,
      'msisdn'             => 12345,
      'iccid'              => 123456,
      'customer_id'        => 123,
      'wholesale_plan'     => 1,
      'ultra_plan'         => 22,
      'preferred_language' => 1,
    ];

    $command = $commandInfo;
    $command['option'] = 'N-WFC|ADD';
    $command['extra_options'] = [ 'channel' => 'WIFI_CALLING.UNKNOWN' ];

    $result = new Result();
    $result->succeed();
    $result->add_data_array(['success' => true, 'body' => 'response stuff']);

    $this->middleWareController->expects($this->any())
      ->method('mwQuerySubscriber')
      ->with([
        'actionUUID' => $commandInfo['actionUUID'],
        'iccid'      => $commandInfo['iccid'],
        'msisdn'     => $commandInfo['msisdn']
      ])
      ->will($this->returnValue($result));

    $result = new Result();
    $result->succeed();

    $this->middleWareController->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->with($command)
      ->will($this->returnValue($result));

    $handler = new AddWIFISocCommandHandler($this->middleWareController);
    $handler->handle(new AddWIFISocCommand($commandInfo));
  }
}
