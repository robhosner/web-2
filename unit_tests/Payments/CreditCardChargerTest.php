<?php

use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Lib\Util\Redis;
use Ultra\Messaging\Messenger;
use Ultra\Payments\CreditCardCharger;
use Ultra\Payments\Payment;
use Ultra\Utilities\SessionUtilities;

class CreditCardChargerTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var CreditCardCharger
   */
  private $api;
  private $payment;
  private $config;
  private $customerRepo;
  private $stateAction;
  private $featureFlag;
  private $redis;
  private $messenger;
  private $sessionUtil;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->stateAction = $this->getMock(StateActions::class);
    $this->featureFlag = $this->getMock(FeatureFlagsClientWrapper::class);
    $this->redis = $this->getMock(Redis::class);
    $this->messenger = $this->getMock(Messenger::class);
    $this->sessionUtil = $this->getMock(SessionUtilities::class);
    $this->payment = $this->getMock(Payment::class);

    $this->api = new CreditCardCharger($this->featureFlag, $this->customerRepo, $this->sessionUtil, $this->redis, $this->stateAction, $this->config, $this->payment, $this->messenger);
  }

  public function testCustomerNotFound()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(false));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INVALID_ARGUMENTS: customer not found.');
    $this->api->chargeCreditCard(1, 1, 1, 1, 1, 1);
  }

  public function testApiAbuse()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue((object) ['customer_id' => 1]));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(true));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INTERNAL: command disabled; please try again later');
    $this->api->chargeCreditCard(1, 1, 1, 1, 1, 1);
  }

  public function testDuplicateCharge()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue((object) ['customer_id' => 1]));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(true));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INTERNAL: duplicate charge; please try again later');
    $this->api->chargeCreditCard(1, 1, 1, 1, 1, 1);
  }

  public function testCustomerHasNoCreditCard()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INVALID_ARGUMENTS: No credit card on file.');
    $this->api->chargeCreditCard(1, 1, 1, 1, 1, 1);
  }

  public function testCantFindCustomerState()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(false));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INTERNAL: customer state could not be determined');
    $this->api->chargeCreditCard(1, 1, 1, 1, 1, 1);
  }

  public function testCustomerStateIsCancelled()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(['state' => 'Cancelled']));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command');
    $this->api->chargeCreditCard(1, 1, 1, 1, 1, 1);
  }

  public function testWalletChargeIsOverAllowedMaximum()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(['state' => 'Active']));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet');
    $this->api->chargeCreditCard(1, 200000, 'WALLET', 1, 1, 1);
  }

  public function testStoredValueChargeIsOverAllowedMaximum()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(['state' => 'Active']));

    $this->config->expects($this->any())
      ->method('getPlanCostFromCosId')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getBoltOnsCostsByCustomerId')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCosIdFromPlan')
      ->will($this->returnValue(1));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INVALID_ARGUMENTS: Charge would exceed maximum allowed stored value amount');
    $this->api->chargeCreditCard(1, 200000, 'STORED_VALUE', 1, 1, 1);
  }

  public function testCreditCardChargeFails()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(['state' => 'Active']));

    $this->config->expects($this->any())
      ->method('getPlanCostFromCosId')
      ->will($this->returnValue(1900));

    $this->config->expects($this->any())
      ->method('getBoltOnsCostsByCustomerId')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCosIdFromPlan')
      ->will($this->returnValue(1));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(1));

    $this->payment->expects($this->any())
      ->method('funcAddFundsByTokenizedCC')
      ->will($this->returnValue(new Result()));

    $this->setExpectedException(CustomErrorCodeException::class, 'ERR_API_INTERNAL: credit card transaction not successful');
    $this->api->chargeCreditCard(1, 1900, 'STORED_VALUE', 1, 1, 1);
  }

  public function testCreditCardChargeSuccedsFailToSendSms()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(['state' => 'Active']));

    $this->config->expects($this->any())
      ->method('getPlanCostFromCosId')
      ->will($this->returnValue(1900));

    $this->config->expects($this->any())
      ->method('getBoltOnsCostsByCustomerId')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCosIdFromPlan')
      ->will($this->returnValue(1));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(1));

    $this->payment->expects($this->any())
      ->method('funcAddFundsByTokenizedCC')
      ->will($this->returnValue(new Result(null, true)));

    $error = new Result();
    $error->add_error("err");

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($error));

    $result = $this->api->chargeCreditCard(1, 1900, 'STORED_VALUE', 1, 1, 1);

//    print_r($result);

    $this->assertEquals('Failed to send SMS', $result->get_warnings()[0]);
  }

  public function testCreditCardChargeSucceds()
  {
    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->getMock();

    $customer->expects($this->any())
      ->method('hasCreditCard')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->sessionUtil->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->will($this->returnValue(false));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(false));

    $this->redis->expects($this->any())
      ->method('get')
      ->will($this->returnValue(false));

    $this->stateAction->expects($this->any())
      ->method('getStateFromCustomerId')
      ->will($this->returnValue(['state' => 'Active']));

    $this->config->expects($this->any())
      ->method('getPlanCostFromCosId')
      ->will($this->returnValue(1900));

    $this->config->expects($this->any())
      ->method('getBoltOnsCostsByCustomerId')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCosIdFromPlan')
      ->will($this->returnValue(1));

    $this->featureFlag->expects($this->any())
      ->method('disableRateLimits')
      ->will($this->returnValue(1));

    $this->payment->expects($this->any())
      ->method('funcAddFundsByTokenizedCC')
      ->with([
        'customer'            => $customer,
        'charge_amount'       => 19,
        'detail'              => 'HELLO',
        'include_taxes_fees'  => true,
        'description'         => 'Wallet Balance Added Using Credit Card',
        'reason'              => 'ADD_BALANCE',
        'session'             => 1,
        'fund_destination'    => 'balance',
        'product_type'        => 'DATA',
      ])
      ->will($this->returnValue(new Result(null, true)));

    $result = $this->api->chargeCreditCard(1, 1900, 'WALLET', 'DATA', 'HELLO', 1);

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
