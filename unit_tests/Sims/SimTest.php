<?php

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Sims\Sim;

class SimTest extends PHPUnit_Framework_TestCase
{
  public function testMissingParametersThrowsException()
  {
    $this->setExpectedException(InvalidObjectCreationException::class);
    new Sim([]);
  }

  public function testAllowsWifiSocIsTrue()
  {
    $sim = new Sim([
      'iccid_full' => 1234,
      'iccid_number' => 1234,
      'brand_id' => 1,
      'tmoprofilesku' => 'ULTRASIM'
    ]);

    $this->assertTrue($sim->allowsWifiSoc());
  }

  public function testAllowsWifiSocIsFalse()
  {
    $sim = new Sim([
      'iccid_full' => 1234,
      'iccid_number' => 1234,
      'brand_id' => 1,
      'tmoprofilesku' => 'NOT A GOOD SKU'
    ]);

    $this->assertFalse($sim->allowsWifiSoc());
  }
}
