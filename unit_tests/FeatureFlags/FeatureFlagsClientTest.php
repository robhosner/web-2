<?php

require_once 'vendor/autoload.php';
use LaunchDarkly\LDClient;
use LaunchDarkly\LDUserBuilder;
use Ultra\FeatureFlags\FeatureFlagsClient;

class FeatureFlagsClientTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var FeatureFlagsClient
   */
  private $class;
  private $ldClient;

  public function setUp()
  {
    $this->ldClient = $this->getMockBuilder(LDClient::class)
      ->setConstructorArgs(['SDK_KEY'])
      ->setMethods(['variation'])
      ->getMock();
    $this->class = FeatureFlagsClient::getInstance();
    $this->class->setClient($this->ldClient);
    $_SERVER['REMOTE_ADDR'] = '162.222.65.27';
    $_SERVER['SERVER_ADDR'] = '162.222.65.27';
  }

  public function testSingletonOfFeatureFlagsClient()
  {
    $a = FeatureFlagsClient::getInstance();
    $b = FeatureFlagsClient::getInstance();
    $c = FeatureFlagsClient::getInstance();
    $d = FeatureFlagsClient::getInstance();

    $hashA = spl_object_hash($a);
    $hashB = spl_object_hash($b);
    $hashC = spl_object_hash($c);
    $hashD = spl_object_hash($d);

    $this->assertTrue(
      $a === $b && $hashA == $hashB &&
      $b === $c && $hashB == $hashC &&
      $c === $d && $hashC == $hashD
    );
  }

  public function testSingletonOfLDClient()
  {
    $a = FeatureFlagsClient::getInstance();
    $b = FeatureFlagsClient::getInstance();
    $c = FeatureFlagsClient::getInstance();
    $d = FeatureFlagsClient::getInstance();

    $clientA = $a->setClient($this->ldClient);
    $clientB = $b->setClient($this->ldClient);
    $clientC = $c->setClient($this->ldClient);
    $clientD = $d->setClient($this->ldClient);

    $clientHashA = spl_object_hash($clientA);
    $clientHashB = spl_object_hash($clientB);
    $clientHashC = spl_object_hash($clientC);
    $clientHashD = spl_object_hash($clientD);

    $this->assertTrue(
      $clientA === $clientB && $clientHashA === $clientHashB &&
      $clientB === $clientC && $clientHashB === $clientHashC &&
      $clientC === $clientD && $clientHashC === $clientHashD
    );
  }

  public function testShowFlexPlansFalse()
  {
    $this->ldClient->expects($this->any())
      ->method('variation')
      ->with('show-flex-plans', (new LDUserBuilder(hash('md5', 'api')))->ip($_SERVER['REMOTE_ADDR'])->build(), false)
      ->will($this->returnValue(false));

    $class = FeatureFlagsClient::getInstance();
    $class->setClient($this->ldClient);
    $this->assertFalse($this->class->showFlexPlans());
  }
}
