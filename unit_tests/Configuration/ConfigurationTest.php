<?php

use Ultra\Configuration\Configuration;

class ConfigurationTest extends PHPUnit_Framework_TestCase
{
  public function testGetThrottlingSpeedMap()
  {
    $array = [
      'plan'  => [
        'full' => ['feature_id' => 12909, 'type' => 'WPRBLK20' ],
        '1536' => ['feature_id' => 12917, 'type' => 'WPRBLK33S'],
        '1024' => ['feature_id' => 12919, 'type' => 'WPRBLK35S'],
      ],
      'block' => [
        'full' => ['feature_id' => 12907, 'type' => 'WPRBLK30' ],
        '1536' => ['feature_id' => 12918, 'type' => 'WPRBLK34S'],
        '1024' => ['feature_id' => 12920, 'type' => 'WPRBLK36S'],
      ]
    ];

    $this->assertTrue($array === (new Configuration())->getThrottlingSpeedMap());
  }

  public function testGetThrottleSpeedFromKeyNotFound()
  {
    $this->assertFalse((new Configuration())->getThrottleSpeedFromKey('NOT FOUND'));
  }

  public function testGetThrottleSpeedFromKeyFound()
  {
    $this->assertEquals('full', (new Configuration())->getThrottleSpeedFromKey('2.0'));
  }

  public function testGetThrottleSpeedNameByFeatureIdThrowsException()
  {
    $featureId = 123;
    $this->setExpectedException('Exception', "No throttle option found for feature_id = $featureId");

    (new Configuration())->getThrottleSpeedNameByFeatureId($featureId);
  }

  public function testGetThrottleSpeedNameByFeatureIdReturnsName()
  {
    $this->assertEquals('full', (new Configuration())->getThrottleSpeedNameByFeatureId(12909));
  }
}
