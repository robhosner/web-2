<?php

use Ultra\CreditCards\CreditCard;
use Ultra\Exceptions\InvalidObjectCreationException;

class CreditCardTest extends PHPUnit_Framework_TestCase
{
  public function testFailingToCreateCreditCard()
  {
    $this->setExpectedException(InvalidObjectCreationException::class);
    new CreditCard([]);
  }

  public function testSuccessfulCreationOfCreditCard()
  {
    $card = new CreditCard([
      'customer_id' => 123,
      'token' => '23i1hih123',
      'bin' => 123456,
      'last_four' => 1234,
      'expires_date' => '12/12/2100'
    ]);

    $this->assertEquals(123, $card->customer_id);
  }
}
