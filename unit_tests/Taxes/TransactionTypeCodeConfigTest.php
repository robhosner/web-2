<?php

use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Taxes\TransactionTypeCodeConfig;

class TransactionTypeCodeConfigTest extends PHPUnit_Framework_TestCase
{
  public function testMissingParametersThrowsException()
  {
    $config = $this->getMock(ConfigurationRepository::class);
    $config->expects($this->any())
      ->method('findConfigByKey')
      ->will($this->returnValue(''));

    $this->setExpectedException(MissingRequiredParametersException::class);
    new TransactionTypeCodeConfig($config);
  }

  public function testTransactionTypeCodeConfig()
  {
    $brandConf = [
      'ULTRA' => [
        'PLAN'        => 1,
        'DATA'        => 1,
        'IDDCA'       => 1,
        'ROAM'        => 1,
        'GLOBE'       => 1,
        'ADD_BALANCE' => 1,
        'LINE_CREDIT' => 1,
        'REPLACE_SIM' => 1
      ],
      'MINT' => [
        'PLAN'        => 1,
        'DATA'        => 1,
        'ROAM'        => 1,
        'ADD_BALANCE' => 1,
        'REPLACE_SIM' => 1
      ]
    ];

    $config = $this->getMock(ConfigurationRepository::class);
    $config->expects($this->any())
      ->method('findConfigByKey')
      ->will($this->returnValue(1));

    $this->assertEquals($brandConf, (new TransactionTypeCodeConfig($config))->toArray());
  }
}
