<?php

namespace unit_tests;

use Ultra\Plans\Flex;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Mvne\Adapter;
use Ultra\Lib\MiddleWare\Adapter\Control;

class FlexTest extends \PHPUnit_Framework_TestCase
{
  private $Flex;
  private $config;
  private $familyAPI;
  private $accountsRepo;
  private $customerRepo;
  private $mwAdapter;
  private $mwControl;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->familyAPI = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config
      ])
      ->getMock();

    $this->accountsRepo = $this->getMock(AccountsRepository::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->mwControl = $this->getMock(Control::class);
    $this->mwAdapter = $this->getMockBuilder(Adapter::class)
      ->setConstructorArgs([
        $this->mwControl,
        $this->customerRepo,
        $this->config
      ])
      ->getMock();

    $this->Flex = new Flex(
        $this->config,
        $this->familyAPI,
        $this->accountsRepo,
        $this->customerRepo,
        $this->mwAdapter
      );
  }

  public function test_queueFamilyAddToBanMISOs_FamilyAPIFails()
  {
    $this->familyAPI->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue(new \Result(null, false)));
    
    $result = $this->Flex->queueFamilyAddToBanMISOs(1, 1);
    
    $this->assertFalse($result->is_success());
    $this->assertContains('Get family API call failed', $result->get_errors());
  }

  public function test_queueFamilyAddToBanMISOs_GetCustomerByIDFails()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['members'] = [
      ['customerId' => 1],
      ['customerId' => 2]
    ];

    $this->familyAPI->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($familyResult));

    $this->customerRepo->expects($this->once())
      ->method('getCustomerById')
      ->will($this->returnValue(false));

    $result = $this->Flex->queueFamilyAddToBanMISOs(1, 1);

    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to lookup CUSTOMER record where customer_id = 2', $result->get_errors());
  }

  public function test_queueFamilyAddToBanMISOs_getAccountFromCustomerIdFails()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['members'] = [
      ['customerId' => 1],
      ['customerId' => 2]
    ];

    $this->familyAPI->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($familyResult));

    $customer = (object)[
      'customerId'  => 2,
      'plan_state'  => 'Active'
    ];

    $this->customerRepo->expects($this->once())
      ->method('getCustomerById')
      ->will($this->returnValue($customer));

    $this->accountsRepo->expects($this->once())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue(false));

    $result = $this->Flex->queueFamilyAddToBanMISOs(1, 1);

    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to lookup ACCOUNTS record where customer_id = 2', $result->get_errors());
  }

  public function test_queueFamilyAddToBanMISOs_mvneMakeitsoAddToBanFails()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['members'] = [
      ['customerId' => 1],
      ['customerId' => 2]
    ];

    $this->familyAPI->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($familyResult));

    $customer = (object)[
      'customerId'  => 2,
      'plan_state'  => 'Active'
    ];

    $this->customerRepo->expects($this->once())
      ->method('getCustomerById')
      ->will($this->returnValue($customer));

    $account = (object)[
      'cos_id'  => 123
    ];

    $this->accountsRepo->expects($this->once())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $this->mwAdapter->expects($this->once())
      ->method('mvneMakeitsoAddToBan')
      ->will($this->returnValue([
        'success' => false,
        'errors' => ['Middleware error']
      ]));

    $result = $this->Flex->queueFamilyAddToBanMISOs(1, 1);

    $this->assertFalse($result->is_success());
    $this->assertContains('Middleware error', $result->get_errors());
  }

  public function test_queueFamilyAddToBanMISOs_skipInactiveUsers()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['members'] = [
      ['customerId' => 1],
      ['customerId' => 2]
    ];

    $this->familyAPI->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($familyResult));

    $customer = (object)[
      'customerId'  => 2,
      'plan_state'  => 'Suspended'
    ];

    $this->customerRepo->expects($this->once())
      ->method('getCustomerById')
      ->will($this->returnValue($customer));


    $this->accountsRepo->expects($this->never())
      ->method('getAccountFromCustomerId');

    $this->mwAdapter->expects($this->never())
      ->method('mvneMakeitsoAddToBan');

    $result = $this->Flex->queueFamilyAddToBanMISOs(1, 1);

    $this->assertTrue($result->is_success());
  }


  public function test_queueFamilyAddToBanMISOs_success()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['members'] = [
      ['customerId' => 1],
      ['customerId' => 2]
    ];

    $this->familyAPI->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($familyResult));

    $customer = (object)[
      'customerId'  => 2,
      'plan_state'  => 'Active'
    ];

    $this->customerRepo->expects($this->once())
      ->method('getCustomerById')
      ->will($this->returnValue($customer));

    $account = (object)[
      'cos_id'  => 123
    ];

    $this->accountsRepo->expects($this->once())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $this->mwAdapter->expects($this->once())
      ->method('mvneMakeitsoAddToBan')
      ->will($this->returnValue([
        'success' => true,
        'errors' => []
      ]));

    $result = $this->Flex->queueFamilyAddToBanMISOs(1, 1);

    $this->assertTrue($result->is_success());
  }
}

