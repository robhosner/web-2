<?php

use Ultra\Configuration\Configuration;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Plans\Plan;
use Ultra\Plans\Repositories\Cfengine\PlansRepository;

class PlansRepositoryTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var PlansRepository
   */
  private $class;

  /**
   * @var Configuration
   */
  private $config;
  private $featureFlagClient;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->featureFlagClient = $this->getMock(FeatureFlagsClientWrapper::class);
    $this->featureFlagClient->expects($this->any())
      ->method('showFlexPlans')
      ->will($this->returnValue(false));
    $this->class = new PlansRepository($this->config, $this->featureFlagClient);
  }

  public function testGetAllPlansByBrandReturnsFalse()
  {
    $this->config->expects($this->any())
    ->method('getShortNameFromBrandId')
    ->with($this->equalTo(3))
    ->will($this->returnValue(null));
    
    $this->assertFalse($this->class->getAllPlansByBrand('MINT'));
  }

  public function testGetAllMintPlans()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(3))
      ->will($this->returnValue('MINT'));

    $this->assertNotEmpty($this->class->getAllPlansByBrand('MINT'));
  }
  
  public function testGetPlanByBrandAndPlanIdReturnsFalse()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(3))
      ->will($this->returnValue(null));

    $this->assertFalse($this->class->getPlanByBrandAndPlanId('MINT', 'FAKE'));
  }

  public function testGetPlanByBrandAndPlanIdThrowsException()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(3))
      ->will($this->returnValue('MINT'));

    $this->config->expects($this->any())
      ->method('getPlanNameFromShortName')
      ->with($this->equalTo('L19'))
      ->will($this->returnValue('NINETEEN'));

    $this->setExpectedException('Exception');

    $this->class->getPlanByBrandAndPlanId('MINT', 'L19');
  }

  public function testGetMintPlan()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(3))
      ->will($this->returnValue('MINT'));

    $this->config->expects($this->any())
      ->method('getPlanNameFromShortName')
      ->with($this->equalTo('M01L'))
      ->will($this->returnValue('MINT_ONE_MONTH_L'));

    $plan = $this->class->getPlanByBrandAndPlanId('MINT', 'M01L');
    $this->assertNotEmpty($plan);
    $this->assertInstanceOf(Plan::class, $plan);
    $this->assertNotEmpty($plan->planId);
  }
  
  public function testGetAllUnivisionPlans()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(2))
      ->will($this->returnValue('UNIVISION'));

    $this->assertNotEmpty($this->class->getAllPlansByBrand('UNIVISION'));
  }

  public function testGetUnivisionPlan()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(2))
      ->will($this->returnValue('UNIVISION'));

    $this->config->expects($this->any())
      ->method('getPlanNameFromShortName')
      ->with($this->equalTo('UV20'))
      ->will($this->returnValue('UVTWENTY'));

    $plan = $this->class->getPlanByBrandAndPlanId('UNIVISION', 'UV20');
    $this->assertNotEmpty($plan);
    $this->assertInstanceOf(Plan::class, $plan);
    $this->assertNotEmpty($plan->planId);
  }

  public function testGetAllUltraPlans()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(1))
      ->will($this->returnValue('ULTRA'));

    $this->assertNotEmpty($this->class->getAllPlansByBrand('ULTRA'));
  }

  public function testGetUltraPlan()
  {
    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->equalTo(1))
      ->will($this->returnValue('ULTRA'));

    $this->config->expects($this->any())
      ->method('getPlanNameFromShortName')
      ->with($this->equalTo('L19'))
      ->will($this->returnValue('NINETEEN'));

    $plan = $this->class->getPlanByBrandAndPlanId('ULTRA', 'L19');
    $this->assertNotEmpty($plan);
    $this->assertInstanceOf(Plan::class, $plan);
    $this->assertNotEmpty($plan->planId);
  }
}