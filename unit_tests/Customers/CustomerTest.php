<?php

use Ultra\Customers\Customer;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\MiddleWare\Adapter\Control;

class CustomerTest extends PHPUnit_Framework_TestCase
{
  public function testExceptionThrownWhileTryingToCreateCustomer()
  {
    $this->setExpectedException(MissingRequiredParametersException::class);
    new Customer([]);
  }

  public function testUpdateThrottleSpeedNotFound()
  {
    $customer = new Customer(['customer_id' => 123]);
    $result = $customer->canUpdateToThrottleSpeed('Does not exists.');
    $this->assertFalse($result);
  }

  public function testCustomerIsNotAllowedToThrottleToThisSpeed()
  {
    $customer = new Customer(['customer_id' => 123]);
    $customer->throttleSpeed = 'full';
    $result = $customer->canUpdateToThrottleSpeed('full');
    $this->assertFalse($result);
  }

  public function testCustomerIsAllowedToThrottleToThisSpeed()
  {
    $customer = new Customer(['customer_id' => 123]);
    $customer->throttleSpeed = 'full';
    $result = $customer->canUpdateToThrottleSpeed('1536');
    $this->assertTrue($result);
  }

  public function testCustomerHasWifiSocThrowsException()
  {
    $this->setExpectedException(MissingRequiredParametersException::class);
    $customer = new Customer(['customer_id' => 123]);
    $customer->hasTheWifiSoc();
  }

  public function testCustomerHasWifiSocIsFalse()
  {
    $customer = new Customer(['customer_id' => 123, 'current_iccid' => 1234567890, 'current_mobile_number' => 1234567890]);
    $middleware = $this->getMock(Control::class);
    $result = new Result();
    $result->fail();

    $middleware->expects($this->any())
      ->method('mwQuerySubscriber')
      ->will($this->returnValue($result));

    $customer->setMiddleWareAdapter($middleware);
    $this->assertFalse($customer->hasTheWifiSoc());
  }

  public function testCustomerHasWifiSocIsTrue()
  {
    $customer = new Customer(['customer_id' => 123, 'current_iccid' => 1234567890, 'current_mobile_number' => 1234567890]);
    $middleware = $this->getMock(Control::class);
    $result = new Result();

    $featureInfo = new stdClass();
    $featureInfo->FeatureID = 12809;
    $featureInfo->UltraServiceName = 'N-WFC';

    $features = new stdClass();
    $features->AddOnFeatureInfoList = new stdClass();
    $features->AddOnFeatureInfoList->AddOnFeatureInfo = [$featureInfo];

    $result->add_data_array([
      'body' => $features
    ]);

    $result->succeed();

    $middleware->expects($this->any())
      ->method('mwQuerySubscriber')
      ->will($this->returnValue($result));

    $customer->setMiddleWareAdapter($middleware);
    $this->assertTrue($customer->hasTheWifiSoc());
  }

  public function testCustomerHasWifiSocIsFalseBecauseMiddleWareDoesntReturnSoc()
  {
    $customer = new Customer(['customer_id' => 123, 'current_iccid' => 1234567890, 'current_mobile_number' => 1234567890]);
    $middleware = $this->getMock(Control::class);
    $result = new Result();

    $featureInfo = new stdClass();
    $featureInfo->FeatureID = 12809;
    $featureInfo->UltraServiceName = 'Wrong Soc';

    $features = new stdClass();
    $features->AddOnFeatureInfoList = new stdClass();
    $features->AddOnFeatureInfoList->AddOnFeatureInfo = [$featureInfo];

    $result->add_data_array([
      'body' => $features
    ]);

    $result->succeed();

    $middleware->expects($this->any())
      ->method('mwQuerySubscriber')
      ->will($this->returnValue($result));

    $customer->setMiddleWareAdapter($middleware);
    $this->assertFalse($customer->hasTheWifiSoc());
  }
}
