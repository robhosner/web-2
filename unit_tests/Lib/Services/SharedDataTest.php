<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\SharedData;

class SharedDataTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var SharedData
   */
  private $class;
  private $config;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getSharedDataConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->class = $this->getMockBuilder(SharedData::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get', 'post', 'put', 'delete'])
      ->getMock();
  }

  public function testMissingConfiguration()
  {
    $this->setExpectedException(\Exception::class);
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getSharedDataConfig')
      ->will($this->returnValue([]));
    new SharedData($this->config);
  }

  public function testGetBucketIDByCustomerIDFails()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 500,
        'curl_errno' => 1
      ]));

    $result = $this->class->getBucketIDByCustomerID(1);
    $this->assertFalse($result->is_success());
  }

  public function testGetBucketIDByCustomerIDParseError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => ''
      ]));

    $result = $this->class->getBucketIDByCustomerID(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse SharedData response', $result->get_errors());
  }

  public function testGetBucketIDByCustomerIDSucceeds()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"bucketId": 1}'
      ]));

    $result = $this->class->getBucketIDByCustomerID(1);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['bucket_id' => 1], $result->data_array);
  }

  public function testGetBucketByBucketIDFails()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 500,
      ]));

    $result = $this->class->getBucketByBucketID(1);
    $this->assertFalse($result->is_success());
  }

  public function testGetBucketByBucketIDParseError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => ''
      ]));

    $result = $this->class->getBucketByBucketID(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse SharedData response', $result->get_errors());
  }

  public function testGetBucketByBucketIDSucceeds()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"bucketId": 1, "ban": 1}'
      ]));

    $result = $this->class->getBucketByBucketID(1);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['bucket_id' => 1, 'ban' => 1], $result->data_array);
  }

  public function testSetCustomerDataLimitFails()
  {
    $result = new \Result();
    $result->fail();

    $this->class->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->will($this->returnValue($result));

    $result = $this->class->setCustomerDataLimit(1, 1);
    $this->assertFalse($result->is_success());
  }

  public function testSetCustomerDataLimitPutFails()
  {
    $result = new \Result();
    $result->succeed();
    $result->add_to_data_array('bucket_id', 1);

    $this->class = $this->getMockBuilder(SharedData::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get', 'post', 'put', 'delete', 'getBucketIDByCustomerID'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->will($this->returnValue($result));

    $this->class->expects($this->any())
      ->method('put')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": ["errrr"]}'
      ]));

    $result = $this->class->setCustomerDataLimit(1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('errrr', $result->get_errors());
  }

  public function testSetCustomerDataLimitSucceeds()
  {
    $result = new \Result();
    $result->succeed();
    $result->add_to_data_array('bucket_id', 1);

    $this->class = $this->getMockBuilder(SharedData::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get', 'post', 'put', 'delete', 'getBucketIDByCustomerID'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->will($this->returnValue($result));

    $this->class->expects($this->any())
      ->method('put')
      ->will($this->returnValue([
        'code' => 200,
      ]));

    $result = $this->class->setCustomerDataLimit(1, 1);
    $this->assertTrue($result->is_success());
  }
}
