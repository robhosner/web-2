<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\FamilyAPI;

class FamilyAPITest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var FamilyAPI
   */
  private $class;
  private $config;
  private $familyId = 1;
  private $customerId = 1;
  private $inviteCode = 1;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get', 'post', 'put', 'delete', 'getCustomerFamilyID', 'applyFamilyBoltOns'])
      ->getMock();
  }

  public function testMissingConfiguration()
  {
    $this->setExpectedException(\Exception::class);
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue([]));
    new FamilyAPI($this->config);
  }

  public function testGetOrCreateFamilyIDGetsFamily()
  {
    $result = new \Result();
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->will($this->returnValue($result));

    $result = $this->class->getOrCreateFamilyID($this->customerId);
    $this->assertTrue($result->is_success());
  }

  public function testGetOrCreateFamilyIDCreatesFamily()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['getCustomerFamilyID', 'createCustomerFamily'])
      ->getMock();

    $result = new \Result();
    $result->fail();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();

    $this->class->expects($this->any())
      ->method('createCustomerFamily')
      ->will($this->returnValue($result));

    $result = $this->class->getOrCreateFamilyID($this->customerId);
    $this->assertTrue($result->is_success());
  }

  public function testCreateCustomerFamilyFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 400]));

    $result = $this->class->createCustomerFamily($this->customerId);
    $this->assertFalse($result->is_success());
  }

  public function testCreateCustomerFamilyFailedToParseResult()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 200, 'body' => '{}']));

    $result = $this->class->createCustomerFamily($this->customerId);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Failed to parse Family API response', $result->get_first_error());
  }

  public function testCreateCustomerFamilySucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 200, 'body' => '{"familyId": 1, "inviteCode": 1}']));

    $result = $this->class->createCustomerFamily($this->customerId);
    $this->assertTrue($result->is_success());
    $this->assertEquals(["family_id" => 1, "invite_code" => 1], $result->data_array);
  }

  public function testGetCustomerFamilyIDFails()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 400]));

    $result = $this->class->getCustomerFamilyID($this->customerId);
    $this->assertFalse($result->is_success());
  }

  public function testGetCustomerFamilyIDFailedToParseResult()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 200, 'body' => '']));

    $result = $this->class->getCustomerFamilyID($this->customerId);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Failed to parse Family API response', $result->get_first_error());
  }

  public function testGetCustomerFamilyIDSucceeds()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 200, 'body' => '{"familyId": 1}']));

    $result = $this->class->getCustomerFamilyID($this->customerId);
    $this->assertTrue($result->is_success());
    $this->assertEquals(["family_id" => 1], $result->data_array);
  }

  public function testGetFamilyByCustomerIDFails()
  {
    $result = new \Result();
    $result->fail();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->will($this->returnValue($result));

    $result = $this->class->getFamilyByCustomerID($this->customerId);
    $this->assertFalse($result->is_success());
  }

  public function testGetFamilyByCustomerIDFailsNoFamily()
  {
    $result = new \Result();
    $result->add_data_array(['family_id' => 1]);
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->will($this->returnValue($result));

    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 400]));

    $result = $this->class->getFamilyByCustomerID($this->customerId);
    $this->assertFalse($result->is_success());
  }

  public function testGetFamilyByCustomerIDFailedToParseResult()
  {
    $result = new \Result();
    $result->add_data_array(['family_id' => 1]);
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->will($this->returnValue($result));

    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 200, 'body' => '']));

    $result = $this->class->getFamilyByCustomerID($this->customerId);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Failed to parse Family API response', $result->get_first_error());
  }

  public function testGetFamilyByCustomerIDSucceeds()
  {
    $result = new \Result();
    $result->add_data_array(['family_id' => 1]);
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->will($this->returnValue($result));

    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 200, 'body' => '{"item":1}']));

    $result = $this->class->getFamilyByCustomerID($this->customerId);
    $this->assertTrue($result->is_success());
    $this->assertEquals(["item" => 1], $result->data_array);
  }

  public function testApplyFamilyBoltOnsFails()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['post'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 400]));

    $result = $this->class->applyFamilyBoltOns($this->familyId, [], false);
    $this->assertFalse($result->is_success());
  }

  public function testApplyFamilyBoltOnsFailedToParseResult()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['post'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 200, 'body' => '{}']));

    $result = $this->class->applyFamilyBoltOns($this->familyId, [], false);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Failed to parse Family API response', $result->get_errors()[0]);
  }

  public function testApplyFamilyBoltOnsSucceeds()
  {
    $this->class = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['post'])
      ->getMock();

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 200, 'body' => '{"familyId": 1, "createdSharedDataBucket": false}']));

    $result = $this->class->applyFamilyBoltOns($this->familyId, [], false);
    $this->assertTrue($result->is_success());
    $this->assertEquals(1, $result->data_array['family_id']);
  }

  public function testApplyFamilyBoltOnsByCustomerIDFails()
  {
    $result = new \Result();
    $result->fail();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->with($this->customerId)
      ->will($this->returnValue($result));

    $result = $this->class->applyFamilyBoltOnsByCustomerID($this->customerId, [], false);
    $this->assertFalse($result->is_success());
  }

  public function testApplyFamilyBoltOnsByCustomerIDMissingFamilyId()
  {
    $result = new \Result();
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->with($this->customerId)
      ->will($this->returnValue($result));

    $result = $this->class->applyFamilyBoltOnsByCustomerID($this->customerId, [], false);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Missing family_id in response', $result->get_errors()[0]);
  }

  public function testApplyFamilyBoltOnsByCustomerIDSucceeds()
  {
    $result = new \Result();
    $result->add_data_array(['family_id' => 1]);
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getCustomerFamilyID')
      ->with($this->customerId)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();

    $this->class->expects($this->any())
      ->method('applyFamilyBoltOns')
      ->with(1, [], false)
      ->will($this->returnValue($result));

    $result = $this->class->applyFamilyBoltOnsByCustomerID($this->customerId, [], false);
    $this->assertTrue($result->is_success());
  }

  public function testGetFamilyIDByInviteCodeFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 400]));

    $result = $this->class->getFamilyIDByInviteCode($this->inviteCode);
    $this->assertFalse($result->is_success());
  }

  public function testGetFamilyIDByInviteCodeFailedToParseResult()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 200, 'body' => '{}']));

    $result = $this->class->getFamilyIDByInviteCode($this->inviteCode);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Failed to parse Family API response', $result->get_errors()[0]);
  }

  public function testGetFamilyIDByInviteCodeSucceeds()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 200, 'body' => '{"familyId": 1}']));

    $result = $this->class->getFamilyIDByInviteCode($this->inviteCode);
    $this->assertTrue($result->is_success());
    $this->assertEquals(1, $result->data_array['family_id']);
  }

  public function testJoinFamilyFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 400, 'body' => '{"errors": ["boo"]}']));

    $result = $this->class->joinFamily($this->customerId, $this->inviteCode);
    $this->assertFalse($result->is_success());
    $this->assertEquals('boo', $result->get_errors()[0]);
  }

  public function testJoinFamilySucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 200]));

    $result = $this->class->joinFamily($this->customerId, $this->inviteCode);
    $this->assertTrue($result->is_success());
  }


  public function testActivateFamilyMemberFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 400, 'curl_errno' => 1]));

    $result = $this->class->activateFamilyMember($this->customerId);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Family API curl failed - 1', $result->get_errors()[0]);
  }

  public function testActivateFamilyMemberSucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 200]));

    $result = $this->class->activateFamilyMember($this->customerId);
    $this->assertTrue($result->is_success());
  }

  public function testActivateFamilyMemberSucceedsConflict()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue(['code' => 409]));

    $result = $this->class->activateFamilyMember($this->customerId);
    $this->assertTrue($result->is_success());
  }

  public function testRemoveFamilyMemberFails()
  {
    $this->class->expects($this->any())
      ->method('delete')
      ->will($this->returnValue(['code' => 400, 'curl_errno' => 1]));

    $result = $this->class->removeFamilyMember($this->familyId, $this->customerId);
    $this->assertFalse($result->is_success());
    $this->assertEquals('Family API curl failed - 1', $result->get_errors()[0]);
  }

  public function testRemoveFamilyMemberSucceeds()
  {
    $this->class->expects($this->any())
      ->method('delete')
      ->will($this->returnValue(['code' => 200]));

    $result = $this->class->removeFamilyMember($this->familyId, $this->customerId);
    $this->assertTrue($result->is_success());
  }
}
