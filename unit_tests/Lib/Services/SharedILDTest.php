<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\SharedILD;

class SharedILDTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var SharedILD
   */
  private $class;
  private $config;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getSharedILDConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->class = $this->getMockBuilder(SharedILD::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get', 'post', 'put', 'delete'])
      ->getMock();
  }

  public function testMissingConfiguration()
  {
    $this->setExpectedException(\Exception::class);
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getSharedILDConfig')
      ->will($this->returnValue([]));
    new SharedILD($this->config);
  }

  public function testCreateCustomerBucketFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'curl_errno' => 1
      ]));

    $result = $this->class->createCustomerBucket(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Shared ILD API curl failed - 1', $result->get_errors());
  }

  public function testCreateCustomerBucketParseError()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '',
      ]));

    $result = $this->class->createCustomerBucket(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse SharedILD response', $result->get_errors());
  }

  public function testCreateCustomerBucketSucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"bucketId": 1}'
      ]));

    $result = $this->class->createCustomerBucket(1);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['bucket_id' => 1], $result->data_array);
  }

  public function testGetBucketIDByCustomerIDFails()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": ["errr"]}'
      ]));

    $result = $this->class->getBucketIDByCustomerID(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('errr', $result->get_errors());
  }

  public function testGetBucketIDByCustomerIDParseError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '',
      ]));

    $result = $this->class->getBucketIDByCustomerID(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse SharedILD response', $result->get_errors());
  }

  public function testGetBucketIDByCustomerIDSucceeds()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"bucketId": 1}'
      ]));

    $result = $this->class->getBucketIDByCustomerID(1);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['bucket_id' => 1], $result->data_array);
  }

  public function testCreditBucketFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": ["errr"]}'
      ]));

    $result = $this->class->creditBucket(1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('errr', $result->get_errors());
  }

  public function testCreditBucketParseError()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '',
      ]));

    $result = $this->class->creditBucket(1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse SharedILD response', $result->get_errors());
  }

  public function testCreditBucketSucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"bucketId": 1}'
      ]));

    $result = $this->class->creditBucket(1, 1);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['bucket_id' => 1], $result->data_array);
  }

  public function testCreditBucketByCustomerIDFails()
  {
    $result = new \Result();
    $this->class->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->will($this->returnValue($result));

    $result = $this->class->creditBucketByCustomerID(1, 1);
    $this->assertFalse($result->is_success());
  }

  public function testCreditBucketByCustomerIDMissingBucketIdInResult()
  {
    $this->class = $this->getMockBuilder(SharedILD::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['getBucketIDByCustomerID'])
      ->getMock();

    $result = new \Result();
    $result->succeed();

    $this->class->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->will($this->returnValue($result));

    $result = $this->class->creditBucketByCustomerID(1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing bucket_id in result', $result->get_errors());
  }

  public function testCreditBucketByCustomerIDSucceeds()
  {
    $this->class = $this->getMockBuilder(SharedILD::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['getBucketIDByCustomerID', 'creditBucket'])
      ->getMock();

    $result = new \Result();
    $result->succeed();
    $result->add_to_data_array('bucket_id', 1);

    $this->class->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->will($this->returnValue($result));

    $this->class->expects($this->any())
      ->method('creditBucket')
      ->will($this->returnValue($result));


    $result = $this->class->creditBucketByCustomerID(1, 1);
    $this->assertTrue($result->is_success());
  }
}
