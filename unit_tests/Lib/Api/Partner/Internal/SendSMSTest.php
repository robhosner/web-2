<?php
namespace unit_tests\Lib\Api\Partner\Interal;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal\SendSMS;
use Ultra\Lib\ApiErrors;
use Ultra\Messaging\Messenger;

class SendSMSTest extends \PHPUnit_Framework_TestCase
{
  private $customerRepo;
  private $messenger;

  /**
   * @var SendSMS
   */
  private $api;

  public function setUp()
  {
    $this->messenger = $this->getMock(Messenger::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->api = new SendSMS($this->messenger, $this->customerRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues(['customer_id' => '123', 'template_name' => 'template', 'msisdn' => '']);
  }

  public function testMissingParams()
  {
    $this->api->setInputValues(['customer_id' => '', 'template_name' => 'template', 'msisdn' => '']);
    $result = $this->api->internal__SendSMS();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue(false));

    $result = $this->api->internal__SendSMS();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testSendSmsFails()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1]));

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue(new \Result()));

    $result = $this->api->internal__SendSMS();

//    print_r($result);

    $this->assertContains('SM0002', $result->data_array['error_codes']);
  }

  public function testSendSmsSucceeds()
  {
    $this->api->setInputValues(['customer_id' => '', 'template_name' => 'template', 'msisdn' => '1234']);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1]));

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue(new \Result(null, true)));

    $result = $this->api->internal__SendSMS();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
