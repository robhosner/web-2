<?php

namespace unit_tests\Lib\Api\Partner\Interal;

use Ultra\Lib\Api\Partner\Internal\ApplyOrder;
use Ultra\Lib\ApiErrors;
use Ultra\Plans\Flex;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Mvne\Adapter;
use Ultra\Lib\MiddleWare\Adapter\Control;

class ApplyOrderTestTest extends \PHPUnit_Framework_TestCase
{
  private $config;
  private $familyAPI;
  private $accountsRepo;
  private $customerRepo;
  private $mwAdapter;
  private $mwControl;
  private $flex;
  private $params = [];

  /**
   * @var ApplyOrder
   */
  private $api;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->familyAPI = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config
      ])
    ->getMock();

    $this->accountsRepo = $this->getMock(AccountsRepository::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->mwControl = $this->getMock(Control::class);
    $this->mwAdapter = $this->getMockBuilder(Adapter::class)
      ->setConstructorArgs([
        $this->mwControl,
        $this->customerRepo,
        $this->config
      ])
      ->getMock();

    $this->flex = $this->getMockBuilder(Flex::class)
      ->setConstructorArgs([
        $this->config,
        $this->familyAPI,
        $this->accountsRepo,
        $this->customerRepo,
        $this->mwAdapter,
        $this->mwControl
      ])
      ->getMock();

    $this->api = new ApplyOrder($this->flex, $this->customerRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->params = ['customer_id' => 123];
    $this->api->setInputValues($this->params);
  }

  public function testCustomerNotFound()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue(new \Result()));

    $result = $this->api->internal__ApplyOrder();

//    print_r($result);

    $this->assertFalse($result->is_success());
  }

  public function testFailedToApplyCustomerOrder()
  {
    $result = new \Result();
    $result->fail();
    $result->add_error("FAIL");

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object)['plan_state' => STATE_ACTIVE]));

    $this->flex->expects($this->any())
      ->method('applyCustomerOrder')
      ->will($this->returnValue($result));

    $result = $this->api->internal__ApplyOrder();

//    print_r($result);

    $this->assertFalse($result->is_success());
    $this->assertContains('IN0002', $result->data_array['error_codes']);
    $this->assertContains('FAIL', $result->get_errors());
  }

  public function testSuccessfullyAppliedCustomerOrder()
  {
    $result = new \Result();
    $result->succeed();

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object)['plan_state' => STATE_ACTIVE]));

    $this->flex->expects($this->any())
      ->method('applyCustomerOrder')
      ->will($this->returnValue($result));

    $result = $this->api->internal__ApplyOrder();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
