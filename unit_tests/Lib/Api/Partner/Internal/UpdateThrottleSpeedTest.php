<?php
namespace unit_tests\Lib\Api\Partner\Interal;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal\UpdateThrottleSpeed;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Mvne\Adapter;

class UpdateThrottleSpeedTest extends \PHPUnit_Framework_TestCase
{
  private $customerRepo;
  private $adapter;
  private $configuration;
  /**
   * @var UpdateThrottleSpeed
   */
  private $api;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->configuration = $this->getMock(Configuration::class);
    $this->adapter = $this->getMockBuilder(Adapter::class)
      ->setConstructorArgs([
        $this->getMock(Control::class),
        $this->customerRepo,
        $this->configuration
      ])
      ->getMock();

    $this->api = new UpdateThrottleSpeed($this->customerRepo, $this->adapter, $this->configuration);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues(['customer_id' => '123', 'throttle_speed' => '1536', 'channel' => '']);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue(false));

    $result = $this->api->internal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testMvneMakeitsoUpdateThrottleSpeedFails()
  {
    $this->api->setInputValues(['customer_id' => '123', 'throttle_speed' => '1536', 'channel' => '']);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue((object) ['customer_id' => 123, 'cos_id' => 321, 'plan_state' => 'Active']));

    $this->configuration->expects($this->any())
      ->method('getPlanFromCosId')
      ->with(321)
      ->will($this->returnValue(123));

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoUpdateThrottleSpeed')
      ->with(123, 123, '1536', 'LTE_SELECT.GW_GUI')
      ->will($this->returnValue(['success' => 0, 'errors' => []]));

    $result = $this->api->internal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testSuccesfulThrottleUpdate()
  {
    $this->api->setInputValues(['customer_id' => '123', 'throttle_speed' => '1536', 'channel' => '']);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue((object) ['customer_id' => 123, 'cos_id' => 321, 'plan_state' => 'Active']));

    $this->configuration->expects($this->any())
      ->method('getPlanFromCosId')
      ->with(321)
      ->will($this->returnValue(123));

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoUpdateThrottleSpeed')
      ->with(123, 123, '1536', 'LTE_SELECT.GW_GUI')
      ->will($this->returnValue(['success' => 1]));

    $result = $this->api->internal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
