<?php

use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCard;
use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Dealers\Repositories\Mssql\DealersRepository;
use Ultra\Lib\Api\Partner\Dealerportal\ChargeAndSaveCustomerCC;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Common;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;

class dealerportal__ChargeAndSaveCustomerCCTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var DealersRepository
   */
  private $dealersRepo;

  /**
   * @var ChargeAndSaveCustomerCC
   */
  private $chargeAndSaveCustomerCC;
  
  /**
   * @var Redis
   */
  private $redis;

  private $apiParams = [
    'security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F',
    'customer_id' => 12341234,
    'remove_card' => 0,
    'charge_amount' => 1000
  ];

  /**
   * @var Configuration
   */
  private $config;
  private $utilities;
  private $customerRepo;
  private $creditCardRepository;
  private $flagsObj;

  public function setUp()
  {
    // API setup
    $this->dealersRepo = $this->getMock(DealersRepository::class);
    $this->config = $this->getMockBuilder(Configuration::class)
      ->setConstructorArgs([])
      ->setMethods(['redisClusterInfo', 'redisHost', 'redisPassword'])
      ->getMock();

    $this->config->expects($this->any())
      ->method('redisClusterInfo')
      ->will($this->returnValue([]));

    $this->config->expects($this->any())
      ->method('redisHost')
      ->will($this->returnValue([]));

    $this->config->expects($this->any())
      ->method('redisPassword')
      ->will($this->returnValue([]));

    $this->redis = $this->getMockBuilder(Ultra\Lib\Util\Redis::class)
      ->setConstructorArgs([true])
      ->setMethods(['get'])
      ->getMock();

    $this->redis->configuration = $this->config;

    $this->utilities = $this->getMock(Common::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->creditCardRepository = $this->getMock(CreditCardRepository::class);
    $this->flagsObj = $this->getMock(FeatureFlagsClientWrapper::class);
    
    $this->chargeAndSaveCustomerCC = $this->getMockBuilder(ChargeAndSaveCustomerCC::class)
      ->setConstructorArgs([$this->utilities, $this->customerRepo, $this->redis, $this->creditCardRepository, $this->flagsObj])
      ->setMethods(['getValidUltraSessionData', 'getRechargeDetailsByDealer', 'checkUltraSessionAllowedAPICustomer', 'getRequestId'])
      ->getMock();

    $this->chargeAndSaveCustomerCC->result = new Result();
    $this->chargeAndSaveCustomerCC->defaultValues = [];

    $this->chargeAndSaveCustomerCC->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMissingParameters()
  {
    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], 103, 'Error']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $this->chargeAndSaveCustomerCC->setInputValues($this->apiParams);
    $result = $this->chargeAndSaveCustomerCC->dealerportal__ChargeAndSaveCustomerCC();

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testNoCustomerFound()
  {
    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(false));

    $this->chargeAndSaveCustomerCC->setInputValues($this->apiParams);
    $result = $this->chargeAndSaveCustomerCC->dealerportal__ChargeAndSaveCustomerCC();

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testCustomerNotAllowedToRunApi()
  {
    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(new Customer(['customer_id' => 123, 'plan_state' => 'active'])));

    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->with([], 123, 'active', 'dealerportal__ChargeAndSaveCustomerCC')
      ->will($this->returnValue(false));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $this->chargeAndSaveCustomerCC->setInputValues($this->apiParams);
    $result = $this->chargeAndSaveCustomerCC->dealerportal__ChargeAndSaveCustomerCC();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testAddBalanceFails()
  {
    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customer = new Customer(['customer_id' => 123, 'plan_state' => 'active']);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->with([], 123, 'active', 'dealerportal__ChargeAndSaveCustomerCC')
      ->will($this->returnValue(true));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $result = new Result();
    $result->add_errors(['error', 'error']);
    $result->add_data_array(['error_codes' => [123, 123]]);

    $card = $this->getMockBuilder(CreditCard::class)
      ->setConstructorArgs([['customer_id' => 123, 'token' => '1234', 'bin' => 12341, 'last_four' => 1234, 'expires_date' => 24]])
      ->setMethods(['initAddBalanceFromTokenizedCreditCard'])
      ->getMock();

    $card->expects($this->any())
      ->method('initAddBalanceFromTokenizedCreditCard')
      ->with($customer, 10, 'dealerportal__ChargeAndSaveCustomerCC', 'Initial Activation Charge', 'Initial Activation Charge', null)
      ->will($this->returnValue($result));

    $this->creditCardRepository->expects($this->any())
      ->method('getCCInfoAndTokenByCustomerId')
      ->will($this->returnValue($card));

    $this->chargeAndSaveCustomerCC->setInputValues($this->apiParams);
    $result = $this->chargeAndSaveCustomerCC->dealerportal__ChargeAndSaveCustomerCC();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testCustomerPlanNotInValidStateAndRemoveCardFailed()
  {
    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customer = new Customer(['customer_id' => 123, 'plan_state' => 'fail']);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->with([], 123, 'fail', 'dealerportal__ChargeAndSaveCustomerCC')
      ->will($this->returnValue(true));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $result = new Result();
    $result->succeed();

    $card = $this->getMockBuilder(CreditCard::class)
      ->setConstructorArgs([['customer_id' => 123, 'token' => '1234', 'bin' => 12341, 'last_four' => 1234, 'expires_date' => 24]])
      ->setMethods(['initAddBalanceFromTokenizedCreditCard', 'disableCreditCard'])
      ->getMock();

    $card->expects($this->at(0))
      ->method('initAddBalanceFromTokenizedCreditCard')
      ->with($customer, 10, 'dealerportal__ChargeAndSaveCustomerCC', 'Initial Activation Charge', 'Initial Activation Charge', null)
      ->will($this->returnValue($result));

    $result = new Result();
    $result->add_error('big failure');

    $card->expects($this->at(1))
      ->method('disableCreditCard')
      ->with()
      ->will($this->returnValue($result));

    $this->creditCardRepository->expects($this->any())
      ->method('getCCInfoAndTokenByCustomerId')
      ->will($this->returnValue($card));

    $this->apiParams['remove_card'] = 1;
    $this->chargeAndSaveCustomerCC->setInputValues($this->apiParams);
    $result = $this->chargeAndSaveCustomerCC->dealerportal__ChargeAndSaveCustomerCC();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testSucceed()
  {
    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customer = new Customer(['customer_id' => 123, 'plan_state' => 'fail']);

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue($customer));

    $this->chargeAndSaveCustomerCC->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->with([], 123, 'fail', 'dealerportal__ChargeAndSaveCustomerCC')
      ->will($this->returnValue(true));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->chargeAndSaveCustomerCC, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $result = new Result();
    $result->succeed();

    $card = $this->getMockBuilder(CreditCard::class)
      ->setConstructorArgs([['customer_id' => 123, 'token' => '1234', 'bin' => 12341, 'last_four' => 1234, 'expires_date' => 24]])
      ->setMethods(['initAddBalanceFromTokenizedCreditCard', 'disableCreditCard'])
      ->getMock();

    $card->expects($this->at(0))
      ->method('initAddBalanceFromTokenizedCreditCard')
      ->with($customer, 10, 'dealerportal__ChargeAndSaveCustomerCC', 'Initial Activation Charge', 'Initial Activation Charge', null)
      ->will($this->returnValue($result));

    $result = new Result();
    $result->succeed();

    $card->expects($this->at(1))
      ->method('disableCreditCard')
      ->with()
      ->will($this->returnValue($result));

    $this->creditCardRepository->expects($this->any())
      ->method('getCCInfoAndTokenByCustomerId')
      ->will($this->returnValue($card));

    $this->apiParams['remove_card'] = 1;
    $this->chargeAndSaveCustomerCC->setInputValues($this->apiParams);
    $result = $this->chargeAndSaveCustomerCC->dealerportal__ChargeAndSaveCustomerCC();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}