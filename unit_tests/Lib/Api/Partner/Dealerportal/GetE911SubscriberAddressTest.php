<?php
namespace unit_tests\Lib\Api\Partner\Dealerportal;

use Result;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Dealerportal\GetE911SubscriberAddress;
use Ultra\Lib\ApiErrors;

class GetE911SubscriberAddressTest extends \PHPUnit_Framework_TestCase
{
  public $customerRepo;

  /**
   * @var GetE911SubscriberAddress
   */
  public $api;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->api = $this->getMockBuilder(GetE911SubscriberAddress::class)
      ->setConstructorArgs([$this->customerRepo,])
      ->setMethods(['getValidUltraSessionData'])
      ->getMock();

    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues(['security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F', 'customer_id' => 0]);
    $this->api->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], 103, 'Error']));

    $getValidUltraSessionDataReflection = new \ReflectionMethod($this->api, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $result = $this->api->dealerportal__GetE911SubscriberAddress();

//    print_r($result);

    $this->assertContains('103', $result->data_array['error_codes']);
  }

  public function testCustomerHasNoE911Address()
  {
    $this->api->setInputValues(['security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F', 'customer_id' => 123]);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getAccAddressE911')
      ->with()
      ->will($this->returnValue(null));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($customer));

    $result = $this->api->dealerportal__GetE911SubscriberAddress();

//    print_r($result);

    $this->assertEmpty($result->data_array['address']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues(['security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F', 'customer_id' => 123]);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getAccAddressE911')
      ->with()
      ->will($this->returnValue(null));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->throwException(new MissingRequiredParametersException()));

    $result = $this->api->dealerportal__GetE911SubscriberAddress();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }
}
