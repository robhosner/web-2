<?php

use Ultra\Dealers\Repositories\Mssql\DealersRepository;
use Ultra\Lib\Api\Partner\Dealerportal\GetRechargeDetailDealer;
use Ultra\Lib\ApiErrors;

class GetRechargeDetailDealerTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var DealersRepository
   */
  private $dealersRepo;

  /**
   * @var GetRechargeDetailDealer
   */
  private $getRechargeDetailDealer;
  
  /**
   * @var Redis
   */
  private $redis;

  private $apiParams = [
    'security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F',
    'recharge_period' => '0315'
  ];

  private $redisKey = 'dealerportal__GetRechargeDetailDealer/12345/3/1/2015';


  public function setUp()
  {
    // API setup
    $this->dealersRepo = $this->getMock(DealersRepository::class);
    $this->redis = $this->getMock(Ultra\Lib\Util\Redis::class);
    $this->getRechargeDetailDealer = $this->getMockBuilder(GetRechargeDetailDealer::class)
      ->setConstructorArgs([$this->dealersRepo, $this->redis])
      ->setMethods(['getValidUltraSessionData', 'getRechargeDetailsByDealer'])
      ->getMock();

    $this->getRechargeDetailDealer->result = new Result();
    $this->getRechargeDetailDealer->defaultValues = [];
    $this->getRechargeDetailDealer->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMissingParameters()
  {
    $this->getRechargeDetailDealer->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], 103, 'Error']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailDealer, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $this->getRechargeDetailDealer->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailDealer->dealerportal__GetRechargeDetailDealer();

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testGetCachedDetailsByPlan()
  {
    $this->getRechargeDetailDealer->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([['dealer' => 12345], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailDealer, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);
    
    $this->redis->expects($this->any())
      ->method('get')
      ->with($this->redisKey)
      ->will($this->returnValue('[{"plan_name": "Ultra $29"}]'));
    
    $this->getRechargeDetailDealer->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailDealer->dealerportal__GetRechargeDetailDealer();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['details_by_plan']);
    $this->assertGreaterThan(0, $result->data_array['record_count']);

  }
  
  public function testGetDetailsByPlanFromDB()
  {
    $this->getRechargeDetailDealer->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([['dealer' => 12345], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailDealer, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $details = new stdClass();
    $details->plan_name = "Ultra $29";

    $this->redis->expects($this->any())
      ->method('get')
      ->with($this->redisKey)
      ->will($this->returnValue(false));

    $this->dealersRepo->expects($this->any())
      ->method('getParentDealerChildren')
      ->with('12345')
      ->will($this->returnValue(['12345']));

    $this->getRechargeDetailDealer->expects($this->any())
      ->method('getRechargeDetailsByDealer')
      ->with($this->redis, $this->redisKey, [12345], '3/1/2015')
      ->will($this->returnValue($details));

    $this->getRechargeDetailDealer->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailDealer->dealerportal__GetRechargeDetailDealer();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['details_by_plan']);
    $this->assertGreaterThan(0, $result->data_array['record_count']);
  }

  public function testGetDetailsByPlanFromDBWithNoDealer()
  {
    $this->getRechargeDetailDealer->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([['dealer' => 12345], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailDealer, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $details = new stdClass();
    $details->plan_name = "Ultra $29";

    $this->redis->expects($this->any())
      ->method('get')
      ->with($this->redisKey)
      ->will($this->returnValue(false));

    $this->dealersRepo->expects($this->any())
      ->method('getParentDealerChildren')
      ->with('12345')
      ->will($this->returnValue([]));

    $this->getRechargeDetailDealer->expects($this->any())
      ->method('getRechargeDetailsByDealer')
      ->with($this->redis, $this->redisKey, ['12345'], '3/1/2015')
      ->will($this->returnValue($details));

    $this->getRechargeDetailDealer->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailDealer->dealerportal__GetRechargeDetailDealer();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['details_by_plan']);
    $this->assertGreaterThan(0, $result->data_array['record_count']);
  }
}