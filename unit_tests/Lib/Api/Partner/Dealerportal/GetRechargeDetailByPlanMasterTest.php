<?php

use Ultra\Configuration\Configuration;
use Ultra\Dealers\Repositories\Mssql\DealersRepository;
use Ultra\Lib\Api\Partner\Dealerportal\GetRechargeDetailByPlanMaster;
use Ultra\Lib\ApiErrors;

class GetRechargeDetailByPlanMasterTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var DealersRepository
   */
  private $dealersRepo;

  /**
   * @var GetRechargeDetailByPlanMaster
   */
  private $getRechargeDetailByPlanMaster;
  
  /**
   * @var Redis
   */
  private $redis;

  private $apiParams = [
    'security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F',
    'recharge_period' => '0315'
  ];

  private $redisKey = 'dealerportal__GetRechargeDetailByPlanMaster/12345/3/1/2015';

  private $sessionData = [
    'role' => 'SubDistributor',
    'distributor' => 12345,
    'masteragent' => 1234
  ];

  /**
   * @var Configuration
   */
  private $config;

  public function setUp()
  {
    // API setup
    $this->dealersRepo = $this->getMock(DealersRepository::class);
    $this->config = $this->getMockBuilder(Configuration::class)
      ->setConstructorArgs([])
      ->setMethods(['redisClusterInfo', 'redisHost', 'redisPassword'])
      ->getMock();

    $this->config->expects($this->any())
      ->method('redisClusterInfo')
      ->will($this->returnValue([]));

    $this->config->expects($this->any())
      ->method('redisHost')
      ->will($this->returnValue([]));

    $this->config->expects($this->any())
      ->method('redisPassword')
      ->will($this->returnValue([]));

    $this->redis = $this->getMockBuilder(Ultra\Lib\Util\Redis::class)
      ->setConstructorArgs([true])
      ->setMethods(['get'])
      ->getMock();
    
    
    
    $this->redis->configuration = $this->config;
    
    $this->getRechargeDetailByPlanMaster = $this->getMockBuilder(GetRechargeDetailByPlanMaster::class)
      ->setConstructorArgs([$this->dealersRepo, $this->redis])
      ->setMethods(['getValidUltraSessionData', 'getRechargeDetailsByDealer'])
      ->getMock();
    
    $this->getRechargeDetailByPlanMaster->redis = $this->redis;

    $this->getRechargeDetailByPlanMaster->result = new Result();
    $this->getRechargeDetailByPlanMaster->defaultValues = [];

    $this->getRechargeDetailByPlanMaster->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMissingParameters()
  {
    $this->getRechargeDetailByPlanMaster->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], 103, 'Error']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailByPlanMaster, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $this->getRechargeDetailByPlanMaster->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailByPlanMaster->dealerportal__GetRechargeDetailByPlanMaster();

    $this->assertNotEmpty($result->data_array['error_codes']);
  }

  public function testGetCachedDetailsByPlan()
  {
    $this->getRechargeDetailByPlanMaster->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([$this->sessionData, '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailByPlanMaster, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);
    
    $this->redis->expects($this->any())
      ->method('get')
      ->with($this->redisKey)
      ->will($this->returnValue('[{"plan_name": "Ultra $29"}]'));
    
    $this->getRechargeDetailByPlanMaster->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailByPlanMaster->dealerportal__GetRechargeDetailByPlanMaster();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['details_by_plan']);
    $this->assertGreaterThan(0, $result->data_array['record_count']);

  }
  
  public function testGetDetailsByPlanFromDB()
  {
    $this->getRechargeDetailByPlanMaster->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([$this->sessionData, '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailByPlanMaster, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $details = new stdClass();
    $details->plan_name = "Ultra $29";

    $this->redis->expects($this->any())
      ->method('get')
      ->with($this->redisKey)
      ->will($this->returnValue(false));

    $this->dealersRepo->expects($this->any())
      ->method('getDealersFromMaster')
      ->with('12345')
      ->will($this->returnValue(['12345']));

    $this->getRechargeDetailByPlanMaster->expects($this->any())
      ->method('getRechargeDetailsByDealer')
      ->with($this->redis, $this->redisKey, [12345], '3/1/2015')
      ->will($this->returnValue($details));

    $this->getRechargeDetailByPlanMaster->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailByPlanMaster->dealerportal__GetRechargeDetailByPlanMaster();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['details_by_plan']);
    $this->assertGreaterThan(0, $result->data_array['record_count']);
  }

  public function testGetDetailsByPlanFromDBWithNoDealer()
  {
    $this->getRechargeDetailByPlanMaster->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([$this->sessionData, '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getRechargeDetailByPlanMaster, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $details = new stdClass();
    $details->plan_name = "Ultra $29";

    $this->redis->expects($this->any())
      ->method('get')
      ->with($this->redisKey)
      ->will($this->returnValue(false));

    $this->dealersRepo->expects($this->any())
      ->method('getDealersFromMaster')
      ->with('12345')
      ->will($this->returnValue(['12345']));

    $this->getRechargeDetailByPlanMaster->expects($this->any())
      ->method('getRechargeDetailsByDealer')
      ->with($this->redis, $this->redisKey, ['12345'], '3/1/2015')
      ->will($this->returnValue($details));

    $this->getRechargeDetailByPlanMaster->setInputValues($this->apiParams);
    $result = $this->getRechargeDetailByPlanMaster->dealerportal__GetRechargeDetailByPlanMaster();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['details_by_plan']);
    $this->assertGreaterThan(0, $result->data_array['record_count']);
  }
}