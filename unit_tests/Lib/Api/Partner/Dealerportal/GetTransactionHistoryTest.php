<?php

use Ultra\Lib\Api\Partner\Dealerportal\GetTransactionHistory;
use Ultra\Billing\Repositories\Mssql\BillingHistoryRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\ApiErrors;

class GetTransactionHistoryTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var BillingHistoryRepository
   */
  private $billingHistoryRepo;

  /**
   * @var CustomerRepository
   */
  private $customerRepo;

  /**
   * @var GetTransactionHistory
   */
  private $getTransactionHistory;

  private $apiParams = [
    'security_token' => 'test',
    'customer_id' => '',
    'date_from' => '',
    'date_to' => ''
  ];

  public function setUp()
  {
    // API setup
    $this->billingHistoryRepo = $this->getMock(BillingHistoryRepository::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->getTransactionHistory = $this->getMockBuilder(GetTransactionHistory::class)
      ->setConstructorArgs([$this->billingHistoryRepo, $this->customerRepo])
      ->setMethods(['getValidUltraSessionData', 'checkUltraSessionAllowedAPICustomer'])
      ->getMock();

    $this->getTransactionHistory->result = new Result();
    $this->getTransactionHistory->defaultValues = [];
    $this->getTransactionHistory->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMissingParameters()
  {
    $customerId = 6;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($customerId)
      ->will($this->returnValue(true));

    $this->apiParams['customer_id'] = $customerId;
    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

    //missing date_from param
    $this->assertContains('MP0001', $result->data_array['error_codes']);

    $this->apiParams['date_from'] = '1234';
    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

//    print_r($result);

    //missing date_to param
    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testInvalidUltraSession()
  {
    $this->getTransactionHistory->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], 1234, 'Invalid Session']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getTransactionHistory, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

    // print_r($result);

    $this->assertContains(1234, $result->data_array['error_codes']);
  }

  public function testCustomerNotFoundError()
  {
    $customerId = 6;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($customerId)
      ->will($this->returnValue(false));

    $this->apiParams['customer_id'] = $customerId;
    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

//     print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testIfUltraSessionAllowsCustomerToRunApi()
  {
    $this->getTransactionHistory->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getTransactionHistory, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customerId = 6;

    $customer = new stdClass();
    $customer->plan_state = '';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($customerId)
      ->will($this->returnValue($customer));

    $this->apiParams['customer_id'] = $customerId;
    $this->apiParams['date_from'] = '1234';
    $this->apiParams['date_to'] = '1234';

    $this->getTransactionHistory->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->will($this->returnValue(false));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->getTransactionHistory, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

//    print_r($result);

    $this->assertContains('SE0005', $result->data_array['error_codes']);

  }

  public function testCannotInterpretInputDates()
  {
    $this->getTransactionHistory->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getTransactionHistory, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customerId = 6;

    $customer = new stdClass();
    $customer->plan_state = '';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($customerId)
      ->will($this->returnValue($customer));

    $this->apiParams['customer_id'] = $customerId;
    $this->apiParams['date_from'] = true;
    $this->apiParams['date_to'] = true;

    $this->getTransactionHistory->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->will($this->returnValue(true));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->getTransactionHistory, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

//    print_r($result);

    $this->assertContains('VV0035', $result->data_array['error_codes']);
  }

  public function testErrorsReceivedWhenGettingBillingTransactionHistory()
  {
    $this->getTransactionHistory->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getTransactionHistory, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customerId = 6;

    $customer = new stdClass();
    $customer->plan_state = '';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($customerId)
      ->will($this->returnValue($customer));

    $this->getTransactionHistory->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->will($this->returnValue(true));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->getTransactionHistory, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $this->billingHistoryRepo->expects($this->any())
      ->method('getBillingTransactionHistory')
      ->will($this->returnValue(['errors' => ['error1']]));

    $this->apiParams['customer_id'] = $customerId;
    $this->apiParams['date_from'] = '1234';
    $this->apiParams['date_to'] = '1234';

    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testReceivingBillingTransactionHistory()
  {
    $this->getTransactionHistory->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], '', '']));

    $getValidUltraSessionDataReflection = new ReflectionMethod($this->getTransactionHistory, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $customerId = 6;

    $customer = new stdClass();
    $customer->plan_state = '';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($customerId)
      ->will($this->returnValue($customer));

    $this->getTransactionHistory->expects($this->any())
      ->method('checkUltraSessionAllowedAPICustomer')
      ->will($this->returnValue(true));

    $checkUltraSessionAllowedAPICustomer = new ReflectionMethod($this->getTransactionHistory, 'checkUltraSessionAllowedAPICustomer');
    $checkUltraSessionAllowedAPICustomer->setAccessible(true);

    $item = new stdClass();
    $item->history_epoch = '1455019556';
    $item->order_id = '13007';
    $item->type = 'Add';
    $item->history_source = 'Credit Card';
    $item->AMOUNT = '10.50';
    $item->description = 'RECHARGE (includes $0.50 recovery fee)';

    $item2 = new stdClass();
    $item2->history_epoch = '1455019556';
    $item2->order_id = '13007';
    $item2->type = 'Charge';
    $item2->history_source = 'Charge';
    $item2->AMOUNT = '-19.00';
    $item2->description = 'Provisioned Activation';

    $item3 = new stdClass();
    $item3->history_epoch = '1444748084';
    $item3->order_id = '11174';
    $item3->type = 'Add';
    $item3->history_source = 'Credit Card';
    $item3->AMOUNT = '19.00';
    $item3->description = 'Initial Activation Charge';

    $history = [
      'billing_transaction_history' => [
        $item,
        $item2,
        $item3
      ],
      'errors' => []
    ];

    $this->billingHistoryRepo->expects($this->any())
      ->method('getBillingTransactionHistory')
      ->will($this->returnValue($history));

    $this->apiParams['customer_id'] = $customerId;
    $this->apiParams['date_from'] = '1234';
    $this->apiParams['date_to'] = '1234';

    $this->getTransactionHistory->setInputValues($this->apiParams);
    $result = $this->getTransactionHistory->dealerportal__GetTransactionHistory();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['transaction_history']);
  }
}