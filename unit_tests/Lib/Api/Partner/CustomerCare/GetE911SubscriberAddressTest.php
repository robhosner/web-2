<?php
namespace unit_tests\customercare;

use Result;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\CustomerCare\GetE911SubscriberAddress;
use Ultra\Lib\ApiErrors;

class GetE911SubscriberAddressTest extends \PHPUnit_Framework_TestCase
{
  public $customerRepo;

  /**
   * @var GetE911SubscriberAddress
   */
  public $api;

  public function setUp()
  {
    $this->customerRepo = $this->getMockBuilder(CustomerRepository::class)
      ->setConstructorArgs([])
      ->setMethods(['getCustomerById'])
      ->getMock();
    $this->api = new GetE911SubscriberAddress($this->customerRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testCustomerHasNoE911Address()
  {
    $this->api->setInputValues(['customer_id' => 123]);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getAccAddressE911')
      ->will($this->returnValue(null));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($customer));

    $result = $this->api->customercare__GetE911SubscriberAddress();
    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testCustomerHasE911Address()
  {
    $this->api->setInputValues(['customer_id' => 123]);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getAccAddressE911')
      ->will($this->returnValue('12345 street'));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($customer));

    $result = $this->api->customercare__GetE911SubscriberAddress();
    $this->assertNotEmpty($result->data_array['address']);
  }

  public function testCustomerHasE911AddressThrowsException()
  {
    $this->api->setInputValues(['customer_id' => 123]);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->throwException(new MissingRequiredParametersException()));

    $result = $this->api->customercare__GetE911SubscriberAddress();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }
}
