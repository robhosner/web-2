<?php

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Lib\Api\Partner\CustomerCare\ChargeCreditCard;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\CreditCardCharger;
use Ultra\Payments\Payment;
use Ultra\Utilities\SessionUtilities;
use Ultra\Lib\Util\Redis;
use Ultra\Messaging\Messenger;

class ChargeCreditCardTest extends PHPUnit_Framework_TestCase
{
  public $creditCardCharger;
  public $params = [
    'customer_id' => 1,
    'charge_amount' => 1900,
    'destination' => 12345,
    'product_type' => 'ADD_BALANCE'
  ];

  /**
   * @var ChargeCreditCard
   */
  public $api;

  public function setUp()
  {
    $this->creditCardCharger = $this->getMockBuilder(CreditCardCharger::class)
      ->setConstructorArgs([
        $this->getMock(FeatureFlagsClientWrapper::class),
        $this->getMock(CustomerRepository::class),
        $this->getMock(SessionUtilities::class),
        $this->getMock(Redis::class),
        $this->getMock(StateActions::class),
        $this->getMock(Configuration::class),
        $this->getMock(Payment::class),
        $this->getMock(Messenger::class)
      ])
      ->getMock();

    $this->api = new ChargeCreditCard($this->creditCardCharger);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues($this->params);
  }

  public function testChargeCreditCardFails()
  {
    $this->creditCardCharger->expects($this->any())
      ->method('chargeCreditCard')
      ->will($this->throwException(new CustomErrorCodeException('BOO', 123)));
    $result = $this->api->customercare__ChargeCreditCard();

//    print_r($result);

    $this->assertContains('123', $result->data_array['error_codes']);
    $this->assertContains('BOO', $result->get_errors());
  }

  public function testChargeCreditCardSucceeds()
  {
    $result = $this->api->customercare__ChargeCreditCard();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
