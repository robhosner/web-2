<?php
namespace unit_tests;

use Ultra\Lib\Api\Traits\CCHandler;
use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCardValidator;
use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\CustomerCare\SetCustomerCCToken;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\DB\Merchants\MeS;
use Ultra\Lib\DB\Merchants\Tokenizer;
use Ultra\Utilities\Common;
use Ultra\Utilities\Validator;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\UnhandledCCProcessorException;
use Ultra\Exceptions\CustomErrorCodeException;

class SetCustomerCCTokenTest extends \PHPUnit_Framework_TestCase
{
  private $api;
  private $utilities;
  private $customerRepository;
  private $configuration;
  private $merchant;
  private $validator;
  private $tokenizer;
  private $ccRepo;
  private $creditCardValidator;

  public function setUp()
  {
    $this->utilities = $this->getMock(Common::class);
    $this->customerRepository = $this->getMock(CustomerRepository::class);
    $this->configuration = $this->getMock(Configuration::class);

    $brandConfig = [
      'name'  => 'MeS',
      'id'    => 1
    ];
    $this->configuration->expects($this->any())
      ->method('getCCProcessorByName')
      ->with('mes')
      ->will($this->returnValue($brandConfig));

    $paymentAPIConfig = [
      'host'      => 'web-qa-08:3040',
      'basepath'  => '/v1',
      'jwtSecret' => 'shhhhhhared-secret'
    ];
    $this->configuration->expects($this->any())
      ->method('getPaymentAPIConfig')
      ->will($this->returnValue($paymentAPIConfig));

    $this->merchant = $this->getMock(MeS::class, [], [$this->configuration]);
    $this->validator = $this->getMock(Validator::class);
    $this->tokenizer = $this->getMock(Tokenizer::class);
    $this->ccRepo = $this->getMock(CreditCardRepository::class);

    $this->creditCardValidator = new CreditCardValidator($this->merchant, $this->validator, $this->configuration, $this->tokenizer);

    $this->api = $this->getMockBuilder(SetCustomerCCToken::class)
      ->setConstructorArgs([
        $this->utilities,
        $this->customerRepository,
        $this->configuration,
        $this->ccRepo,
        $this->creditCardValidator])
      ->setMethods(['getValidUltraSessionData', 'validateAndSaveToken'])
      ->getMock();

    $this->api->expects($this->any())
      ->method('getProcessorValidator')
      ->with($brandConfig)
      ->will($this->returnValue($this->creditCardValidator));
    $getProcessorValidatorReflection = new \ReflectionMethod($this->api, 'getProcessorValidator');
    $getProcessorValidatorReflection->setAccessible(true);

    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues([
      'security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F',
      'customer_id' => 123,
      'cc_name' => '',
      'cc_address1' => '',
      'cc_address2' => '',
      'cc_city' => '',
      'cc_country' => '',
      'cc_state_or_region' => '',
      'cc_postal_code' => '',
      'account_cc_exp' => '',
      'account_cc_cvv' => '',
      'token' => '',
      'bin' => '',
      'last_four' => '',
      'processor' => ''
    ]);

    $this->api->expects($this->once())
      ->method('validateAndSaveToken')
      ->will($this->throwException(new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031')));

    $result = $this->api->customercare__SetCustomerCCToken();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testInvalidObjectCreationException()
  {
    $inputs = [
      'security_token' => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F',
      'customer_id' => 123,
      'cc_name'            => 'Sharayu Ultrasmith',
      'cc_address1'        => '1550 Scenic Ave',
      'cc_address2'        => '100',
      'cc_city'            => 'Costa Mesa',
      'cc_country'         => 'US',
      'cc_state_or_region' => 'CA',
      'cc_postal_code'     => '92626',
      'account_cc_exp'     => '1216',
      'account_cc_cvv'     => '123',
      'token'              => '6dd058adde483774b8424ed3554e6da7',
      'bin'                => '123454',
      'last_four'          => '1234',
      'processor'          => ''
    ];

    $this->api->setInputValues($inputs);

    $customer = new Customer(['customer_id' => 123, 'preferred_language' => 'EN', 'brand_id' => 1]);

    $this->customerRepository->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['brand_id', 'preferred_language'], true)
      ->will($this->returnValue($customer));

    $this->api->expects($this->once())
      ->method('validateAndSaveToken')
      ->will($this->throwException(new InvalidObjectCreationException('testInvalidObjectCreationException', 'TEST00')));

    $result = $this->api->customercare__SetCustomerCCToken();
    $this->assertContains('TEST00', $result->data_array['error_codes']);
  }
}
