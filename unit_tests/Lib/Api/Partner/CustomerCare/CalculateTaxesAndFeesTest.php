<?php
namespace unit_tests;

use Result;
use Ultra\Configuration\Configuration;
use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\CustomerCare\CalculateTaxesAndFees;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\TaxCalculator\SureTax;
use Ultra\Taxes\TaxesAndFeesCalculator;
use Ultra\Taxes\TransactionTypeCodeConfig;

class CalculateTaxesAndFeeseTest extends \PHPUnit_Framework_TestCase
{
  public $config;
  public $session;
  public $customerRepo;
  public $taxCalc;
  public $params = [
    'msisdn' => 2345678896,
    'charge_amount' => 1900,
    'zip' => 12345,
    'product_type' => 'ADD_BALANCE'
  ];

  /**
   * @var CalculateTaxesAndFees
   */
  public $api;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $configRepo = $this->getMock(ConfigurationRepository::class);
    $configRepo->expects($this->any())
      ->method('findConfigByKey')
      ->will($this->returnValue(1));

    $this->taxCalc = $this->getMockBuilder(TaxesAndFeesCalculator::class)
      ->setConstructorArgs([
          $this->getMock(SureTax::class),
          $this->getMockBuilder(TransactionTypeCodeConfig::class)
            ->setConstructorArgs([$configRepo])
            ->getMock(),
          $this->config
        ])
      ->getMock();

    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->api = new CalculateTaxesAndFees($this->taxCalc, $this->customerRepo, $this->config);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues($this->params);
  }

  public function testCustomerNotFound()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue(false));

    $result = $this->api->customercare__CalculateTaxesAndFees();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCalculateTaxesAndFeesFails()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue(new Customer(['customer_id' => 123])));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(false));

    $result = new Result();
    $result->succeed();
    $result->add_data_array(['sales_tax' => 100, 'mts_tax' => 150, 'recovery_fee' => 123]);

    $this->taxCalc->expects($this->any())
      ->method('calculateTaxesAndFees')
      ->will($this->returnValue($result));

    $result = $this->api->customercare__CalculateTaxesAndFees();

//    print_r($result);

    $this->assertFalse($result->is_success());
  }

  public function testCalculateTaxesAndFeesSucceeds()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue(new Customer(['customer_id' => 123])));

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(new Customer(['customer_id' => 123, 'cos_id' => 1, 'monthly_renewal_target' => 123, 'brand_id' => 1])));

    $result = new Result();
    $result->succeed();
    $result->add_data_array(['sales_tax' => 100, 'mts_tax' => 150, 'recovery_fee' => 123]);

    $this->taxCalc->expects($this->any())
      ->method('calculateTaxesAndFees')
      ->will($this->returnValue($result));

    $result = $this->api->customercare__CalculateTaxesAndFees();

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals([
      'sales_tax' => 250,
      'recovery_fee' => 123,
      'error_codes' => [],
      'user_errors' => [],
    ], $result->data_array);
  }
}
