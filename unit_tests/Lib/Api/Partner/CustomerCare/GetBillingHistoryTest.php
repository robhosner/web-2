<?php

use Ultra\Lib\Api\Partner\CustomerCare\GetBillingHistory;
use Ultra\Billing\Repositories\Mssql\BillingHistoryRepository;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Common;

class GetBillingHistoryTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var GetBillingHistory
   */
  private $api;
  
  /**
   * @var BillingHistoryRepository
   */
  private $billingHistoryRepo;

  public function setUp()
  {
    // API setup
    $this->billingHistoryRepo = $this->getMock(BillingHistoryRepository::class);
    $this->api = $this->getMockBuilder(GetBillingHistory::class)
      ->setConstructorArgs([$this->billingHistoryRepo, new Common()])
      ->setMethods(['dlog'])
      ->getMock();
    
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $this->api->expects($this->any())
      ->method('dlog')
      ->with('', 'ERR_API_INVALID_ARGUMENTS: missing customer_id parameter')
      ->will($this->returnValue([]));
  }

  public function testMissingCustomerIdParameter()
  {
    $this->api->setInputValues(['customer_id' => '']);
    $result = $this->api->customercare__GetBillingHistory();
    $this->assertContains('MP0001', $result->data_array['error_codes']);

//    print_r($result);
  }

  public function testNoFoundBillingHistoryData()
  {
    $customerId = 19338;

    $this->billingHistoryRepo->expects($this->any())
      ->method('getBillingTransactionHistoryByCustomerId')
      ->with($this->equalTo($customerId))
      ->will($this->returnValue([]));

    $this->api->setInputValues(['customer_id' => $customerId]);

    $result = $this->api->customercare__GetBillingHistory();

    $resultWarnings = new ReflectionProperty(Result::class, 'warnings');
    $resultWarnings->setAccessible(true);
    $warnings = $resultWarnings->getValue($result);
    $this->assertContains('No Data Found', $warnings);

//    print_r($result);
  }

  public function testAddCaMtsToBillingHistoryDescription()
  {
    $customerId = 19338;
    
    $historyItem = new stdClass();
    $historyItem->TRANSACTION_DATE = 'Oct 2 2012 08:39:38:233PM';
    $historyItem->ENTRY_TYPE = 'SPEND';
    $historyItem->STORED_VALUE_CHANGE = '0.0000';
    $historyItem->BALANCE_CHANGE = '2.5000';
    $historyItem->DESCRIPTION = 'ADD_BALANCE';
    $historyItem->SOURCE = 'WEBCC';
    $historyItem->SURCHARGE_HISTORY_ID = '100';
    $historyItem->AMOUNT = '1.0000';
    $historyItem->SURCHARGE_TYPE = 'RECOVERY FEE';
    $historyItem->order_id = '1000';

    $historyItem2 = new stdClass();
    $historyItem2->TRANSACTION_DATE = 'Oct 2 2012 08:39:38:233PM';
    $historyItem2->ENTRY_TYPE = 'SPEND';
    $historyItem2->STORED_VALUE_CHANGE = '0.0000';
    $historyItem2->BALANCE_CHANGE = '5.0000';
    $historyItem2->DESCRIPTION = 'ADD_BALANCE';
    $historyItem2->SOURCE = 'WEBCC';
    $historyItem2->SURCHARGE_HISTORY_ID = '102';
    $historyItem2->AMOUNT = '3.5400';
    $historyItem2->SURCHARGE_TYPE = 'MTS TAX';
    $historyItem2->order_id = '1000';

    $historyItem3 = new stdClass();
    $historyItem3->TRANSACTION_DATE = 'Oct 2 2012 08:39:38:233PM';
    $historyItem3->ENTRY_TYPE = 'SPEND';
    $historyItem3->STORED_VALUE_CHANGE = '0.0000';
    $historyItem3->BALANCE_CHANGE = '5.0000';
    $historyItem3->DESCRIPTION = 'ADD_BALANCE';
    $historyItem3->SOURCE = 'WEBCC';
    $historyItem3->SURCHARGE_HISTORY_ID = '101';
    $historyItem3->AMOUNT = '2.0000';
    $historyItem3->SURCHARGE_TYPE = 'SALES TAX';
    $historyItem3->order_id = '1001';

    $historyItem4 = new stdClass();
    $historyItem4->TRANSACTION_DATE = 'Oct 3 2012 08:39:38:233PM';
    $historyItem4->ENTRY_TYPE = 'SPEND';
    $historyItem4->STORED_VALUE_CHANGE = '29.0000';
    $historyItem4->BALANCE_CHANGE = '0.0000';
    $historyItem4->DESCRIPTION = '4G LTE UpData - 500MB';
    $historyItem4->SOURCE = 'WEBCC';
    $historyItem4->SURCHARGE_HISTORY_ID = '';
    $historyItem4->AMOUNT = '';
    $historyItem4->SURCHARGE_TYPE = '';
    $historyItem4->order_id = '1002';

    $this->billingHistoryRepo->expects($this->any())
      ->method('getBillingTransactionHistoryByCustomerId')
      ->with($this->equalTo($customerId))
      ->will($this->returnValue([$historyItem, $historyItem2, $historyItem3, $historyItem4]));

    $this->api->setInputValues(['customer_id' => $customerId]);

    $result = $this->api->customercare__GetBillingHistory();

    $this->assertContains('Oct 2 2012|SPEND|0.0000|5.0000|ADD_BALANCE (includes $1.00 recovery fee, $3.54 mts tax)|WEBCC', $result->data_array['billing_history']);

//    print_r($result);
  }
}
