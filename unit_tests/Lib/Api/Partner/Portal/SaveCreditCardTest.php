<?php

use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCard;
use Ultra\CreditCards\CreditCardValidator;
use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Portal\SaveCreditCard;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\DB\Merchants\MeS;
use Ultra\Lib\DB\Merchants\Tokenizer;
use Ultra\Utilities\Validator;
use Ultra\Exceptions\CustomErrorCodeException;

class SaveCreditCardTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var SaveCreditCard
   */
  private $api;
  private $session;
  private $customerRepository;
  private $configuration;
  private $merchant;
  private $validator;
  private $tokenizer;
  private $ccRepo;
  private $creditCardValidator;

  public function setUp()
  {
    $this->session = $this->getMock(Session::class);
    $this->customerRepository = $this->getMock(CustomerRepository::class);
    $this->configuration = $this->getMock(Configuration::class);

    $paymentAPIConfig = [
      'host'      => 'web-qa-08:3040',
      'basepath'  => '/v1',
      'jwtSecret' => 'shhhhhhared-secret'
    ];
    $this->configuration->expects($this->any())
      ->method('getPaymentAPIConfig')
      ->will($this->returnValue($paymentAPIConfig));

    $this->merchant = $this->getMock(MeS::class, [], [$this->configuration]);
    $this->validator = $this->getMock(Validator::class);
    $this->tokenizer = $this->getMock(Tokenizer::class);
    $this->ccRepo = $this->getMock(CreditCardRepository::class);

    $brandConfig = [
      'name'  => 'MeS',
      'id'    => 1
    ];
    $this->configuration->expects($this->any())
      ->method('getCCProcessorByName')
      ->with('mes')
      ->will($this->returnValue($brandConfig));

    $this->creditCardValidator = new CreditCardValidator($this->merchant, $this->validator, $this->configuration, $this->tokenizer);

    // API setup
    $this->api = new SaveCreditCard(
      $this->session,
      $this->customerRepository,
      $this->configuration,
      $this->ccRepo,
      $this->creditCardValidator
    );
    $this->api = $this->getMockBuilder(SaveCreditCard::class)
      ->setConstructorArgs([
        $this->session,
        $this->customerRepository,
        $this->configuration,
        $this->ccRepo,
        $this->creditCardValidator
      ])
      ->setMethods(['validateAndSaveToken'])
      ->getMock();

    $this->api->expects($this->any())
      ->method('getProcessorValidator')
      ->with($brandConfig)
      ->will($this->returnValue($this->creditCardValidator));
    $getProcessorValidatorReflection = new \ReflectionMethod($this->api, 'getProcessorValidator');
    $getProcessorValidatorReflection->setAccessible(true);

    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues([
      'cc_name' => '',
      'cc_address1' => '',
      'cc_address2' => '',
      'cc_city' => '',
      'cc_country' => '',
      'cc_state_or_region' => '',
      'cc_postal_code' => '',
      'account_cc_exp' => '',
      'account_cc_cvv' => '',
      'token' => '',
      'bin' => '',
      'last_four' => '',
      'processor' => ''
    ]);

    $this->session->customer_id = false;
    $result = $this->api->portal__SaveCreditCard();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues([
      'cc_name' => '',
      'cc_address1' => '',
      'cc_address2' => '',
      'cc_city' => '',
      'cc_country' => '',
      'cc_state_or_region' => '',
      'cc_postal_code' => '',
      'account_cc_exp' => '',
      'account_cc_cvv' => '',
      'token' => '',
      'bin' => '',
      'last_four' => '',
      'processor' => ''
    ]);

    $this->session->customer_id = 123;

    $this->api->expects($this->once())
      ->method('validateAndSaveToken')
      ->will($this->throwException(new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031')));

    $result = $this->api->portal__SaveCreditCard();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  
  public function testCreditCardVerificationFailsFromCvvValidation()
  {
    $inputs = [
      'cc_name'            => 'Sharayu Ultrasmith',
      'cc_address1'        => '1550 Scenic Ave',
      'cc_address2'        => '100',
      'cc_city'            => 'Costa Mesa',
      'cc_country'         => 'US',
      'cc_state_or_region' => 'CA',
      'cc_postal_code'     => '92626',
      'account_cc_exp'     => '1216',
      'account_cc_cvv'     => '123',
      'token'              => '6dd058adde483774b8424ed3554e6da7',
      'bin'                => '123454',
      'last_four'          => '1234',
      'processor'          => 'mes'
    ];

    $this->api->setInputValues($inputs);

    $this->session->customer_id = 123;

    $customer = new Customer(['customer_id' => 123, 'preferred_language' => 'EN', 'brand_id' => 1]);

    $this->customerRepository->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['brand_id', 'preferred_language'], true)
      ->will($this->returnValue($customer));

    $this->api->expects($this->once())
      ->method('validateAndSaveToken')
      ->will($this->throwException(new InvalidObjectCreationException('testInvalidObjectCreationException', 'TEST00')));

    $result = $this->api->portal__SaveCreditCard();
    $this->assertContains('TEST00', $result->data_array['error_codes']);

//    print_r($result);
  }

/*
  public function testCreditCardVerificationFailsWithUserErrors()
  {
    $inputs = [
      'cc_name'            => 'Sharayu Ultrasmith',
      'cc_address1'        => '1550 Scenic Ave',
      'cc_address2'        => '100',
      'cc_city'            => 'Costa Mesa',
      'cc_country'         => 'US',
      'cc_state_or_region' => 'CA',
      'cc_postal_code'     => '92626',
      'account_cc_exp'     => '1216',
      'account_cc_cvv'     => '123',
      'token'              => '6dd058adde483774b8424ed3554e6da7',
      'bin'                => '123454',
      'last_four'          => '1234',
      'processor'          => ''
    ];

    $this->api->setInputValues($inputs);

    $this->session->customer_id = 123;

    $customer = new Customer(['customer_id' => 123, 'preferred_language' => 'EN', 'brand_id' => 1]);

    $this->customerRepository->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['brand_id', 'preferred_language'], true)
      ->will($this->returnValue($customer));

    $this->configuration->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $result = new \Result;
    $result->add_errors([
      'user_errors' => [
        'EN' => 'We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer',
      ]
    ]);

    $this->merchant->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $this->setExpectedException(\Exception::class, 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed - (We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer)');

    $result = $this->api->portal__SaveCreditCard();

//    print_r($result);
  }

  public function testCreditCardVerificationFailsWithNoUserErrors()
  {
    $inputs = [
      'cc_name'            => 'Sharayu Ultrasmith',
      'cc_address1'        => '1550 Scenic Ave',
      'cc_address2'        => '100',
      'cc_city'            => 'Costa Mesa',
      'cc_country'         => 'US',
      'cc_state_or_region' => 'CA',
      'cc_postal_code'     => '92626',
      'account_cc_exp'     => '1216',
      'account_cc_cvv'     => '123',
      'token'              => '6dd058adde483774b8424ed3554e6da7',
      'bin'                => '123454',
      'last_four'          => '1234',
      'processor'          => ''
    ];

    $this->api->setInputValues($inputs);

    $this->session->customer_id = 123;

    $customer = new Customer(['customer_id' => 123, 'preferred_language' => 'EN', 'brand_id' => 1]);

    $this->customerRepository->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['brand_id', 'preferred_language'], true)
      ->will($this->returnValue($customer));

    $this->configuration->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $result = new \Result;
    $result->add_error('We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer');

    $this->merchant->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $this->setExpectedException(\Exception::class, 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed - (We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer)');

    $result = $this->api->portal__SaveCreditCard();

//    print_r($result);
  }

  public function testCustomerUpdateFails()
  {
    $inputs = [
      'cc_name'            => 'Sharayu Ultrasmith',
      'cc_address1'        => '1550 Scenic Ave',
      'cc_address2'        => '100',
      'cc_city'            => 'Costa Mesa',
      'cc_country'         => 'US',
      'cc_state_or_region' => 'CA',
      'cc_postal_code'     => '92626',
      'account_cc_exp'     => '1216',
      'account_cc_cvv'     => '123',
      'token'              => '6dd058adde483774b8424ed3554e6da7',
      'bin'                => '123454',
      'last_four'          => '1234',
      'processor'          => ''
    ];

    $this->api->setInputValues($inputs);

    $this->session->customer_id = 123;

    $customer = new Customer(['customer_id' => 123, 'preferred_language' => 'EN', 'brand_id' => 1]);

    $this->customerRepository->expects($this->at(0))
      ->method('getCustomerById')
      ->with(123, ['brand_id', 'preferred_language'], true)
      ->will($this->returnValue($customer));

    $this->configuration->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->ccRepo->expects($this->any())
      ->method('saveCreditCardInformation')
      ->will($this->returnValue(true));

    $this->customerRepository->expects($this->at(1))
      ->method('updateCustomerByCustomerId')
      ->will($this->returnValue(false));

    $result = new \Result;
    $result->add_data_array([
      'cvv_validation' => 'Y',
      'avs_validation' => 'N'
    ]);
    $result->succeed();

    $this->merchant->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $this->setExpectedException(\Exception::class, 'Failed to save the customer\'s information');

    $result = $this->api->portal__SaveCreditCard();

//    print_r($result);
  }

  public function testSaveCustomerCCSuccess()
  {
    $inputs = [
      'cc_name'            => 'Sharayu Ultrasmith',
      'cc_address1'        => '1550 Scenic Ave',
      'cc_address2'        => '100',
      'cc_city'            => 'Costa Mesa',
      'cc_country'         => 'US',
      'cc_state_or_region' => 'CA',
      'cc_postal_code'     => '92626',
      'account_cc_exp'     => '1216',
      'account_cc_cvv'     => '123',
      'token'              => '6dd058adde483774b8424ed3554e6da7',
      'bin'                => '123454',
      'last_four'          => '1234',
      'processor'          => ''
    ];

    $this->api->setInputValues($inputs);

    $this->session->customer_id = 123;

    $customer = new Customer(['customer_id' => 123, 'preferred_language' => 'EN', 'brand_id' => 1]);

    $this->customerRepository->expects($this->at(0))
      ->method('getCustomerById')
      ->with(123, ['brand_id', 'preferred_language'], true)
      ->will($this->returnValue($customer));

    $this->configuration->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->ccRepo->expects($this->any())
      ->method('saveCreditCardInformation')
      ->will($this->returnValue(true));

    $this->customerRepository->expects($this->at(1))
      ->method('updateCustomerByCustomerId')
      ->will($this->returnValue(true));

    $result = new \Result;
    $result->add_data_array([
      'cvv_validation' => 'Y',
      'avs_validation' => 'N'
    ]);
    $result->succeed();

    $this->merchant->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SaveCreditCard();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
  */
}
