<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\UnsubscribeFromEmail;
use Ultra\Lib\ApiErrors;

class UnsubscribeFromEmailTest extends PHPUnit_Framework_TestCase
{
  public $utils;
  public $session;
  public $customerRepo;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * @var UnsubscribeFromEmail
   */
  public $api;

  public $apiParams = [
    'customer_id' => 123,
    'msisdn' => '1234',
  ];

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->api = new UnsubscribeFromEmail($this->customerRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['current_mobile_number'], true)
      ->will($this->returnValue(false));

    $result = $this->api->portal__UnsubscribeFromEmail();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testFailToUpdateMarketingSettings()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['setMarketingSettings'])
      ->getMock();

    $this->customer->current_mobile_number = '1234';

    $this->customer->expects($this->any())
      ->method('setMarketingSettings')
      ->with(['customer_id' => 123, 'marketing_email_option' => 0])
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['current_mobile_number'], true)
      ->will($this->returnValue($this->customer));

    $result = $this->api->portal__UnsubscribeFromEmail();

//    print_r($result);

    $this->assertFalse($result->is_success());
  }

  public function testSuccess()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['setMarketingSettings'])
      ->getMock();

    $this->customer->current_mobile_number = '1234';

    $this->customer->expects($this->any())
      ->method('setMarketingSettings')
      ->with(['customer_id' => 123, 'marketing_email_option' => 0])
      ->will($this->returnValue([]));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['current_mobile_number'], true)
      ->will($this->returnValue($this->customer));

    $result = $this->api->portal__UnsubscribeFromEmail();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
