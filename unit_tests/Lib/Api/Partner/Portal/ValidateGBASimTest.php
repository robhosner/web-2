<?php
namespace unit_tests\Portal;

use Session;
use Ultra\Billing\Repositories\Mssql\BillingHistoryRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Portal\ValidateGBASim;
use Ultra\Lib\ApiErrors;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use Ultra\Sims\Sim;

class ValidateGBASimTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var ValidateGBASim
   */
  public  $api;
  private $customerRepo;
  private $simRepo;
  private $session;
  private $billingRepo;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->simRepo = $this->getMock(SimRepository::class);
    $this->session = $this->getMock(Session::class);
    $this->billingRepo = $this->getMock(BillingHistoryRepository::class);

    $this->api = new ValidateGBASim($this->session, $this->customerRepo, $this->simRepo, $this->billingRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues([]);

    $this->session->customer_id = false;

    $result = $this->api->portal__ValidateGBASim();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues([]);

    $this->session->customer_id = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['current_iccid_full', 'brand_id'], true)
      ->will($this->returnValue(false));

    $result = $this->api->portal__ValidateGBASim();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerDoesNotHaveASim()
  {
    $this->api->setInputValues([]);

    $this->session->customer_id = 1;


    $customer = new \stdClass();
    $customer->customer_id = 1;
    $customer->current_iccid_full = 1234577;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['current_iccid_full', 'brand_id'], true)
      ->will($this->returnValue($customer));

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($customer->current_iccid_full )
      ->will($this->throwException(new InvalidObjectCreationException('message', 'MP0001')));

    $result = $this->api->portal__ValidateGBASim();
    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testCustomerDoesNotHaveAValidGBASim()
  {
    $this->api->setInputValues([]);

    $this->session->customer_id = 1;


    $customer = new \stdClass();
    $customer->customer_id = 1;
    $customer->current_iccid_full = 1234577;
    $customer->brand_id = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['current_iccid_full', 'brand_id'], true)
      ->will($this->returnValue($customer));

    $sim = new Sim([
      'iccid_number' => 1234,
      'brand_id' => 1,
      'tmoprofilesku' => 123456789,
      'iccid_full' => 1234,
    ]);

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($customer->current_iccid_full )
      ->will($this->returnValue($sim));

    $result = $this->api->portal__ValidateGBASim();

//    print_r($result);

    $this->assertFalse($result->data_array['valid']);
  }
}
