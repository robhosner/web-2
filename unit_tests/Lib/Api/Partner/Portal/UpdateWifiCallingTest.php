<?php
namespace unit_tests\Lib\Api\Partner\Portal;

use CommandInvocation;
use Session;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Address;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\Api\Partner\Portal\UpdateWifiCalling;
use Ultra\Lib\ApiErrors;
use Ultra\Mvne\Commands\UpdateSubscriberAddressCommand;
use Ultra\Mvne\MakeItSo\Repositories\Mssql\MakeItSoRepository;
use Ultra\Mvne\UpdateWifiCallingService;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use Ultra\Sims\Sim;
use Ultra\Utilities\Common;

class UpdateWifiCallingTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var UpdateWifiCalling
   */
  private $api;
  private $customerRepository;
  private $configuration;
  private $simRepository;
  private $session;
  private $utilities;
  private $commandInvocation;
  private $makeItSoRepository;

  public function setUp()
  {
    $this->customerRepository = $this->getMock(CustomerRepository::class);
    $this->configuration = $this->getMock(Configuration::class);
    $this->simRepository = $this->getMock(SimRepository::class);
    $this->session = $this->getMock(Session::class);
    $this->utilities = $this->getMock(Common::class);
    $this->makeItSoRepository  = $this->getMock(MakeItSoRepository::class);

    $this->commandInvocation = $this->getMockBuilder(CommandInvocation::class)
      ->setConstructorArgs([])
      ->setMethods(['isOccupied'])
      ->getMock();

    $this->commandInvocation->expects($this->any())
      ->method('isOccupied')
      ->will($this->returnValue(false));

    $this->api = new UpdateWifiCalling($this->session, $this->customerRepository, $this->configuration, $this->simRepository, $this->utilities, $this->commandInvocation, $this->makeItSoRepository);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues([
      'address1' => '123 Street',
      'address2' => '',
      'city' => '',
      'state' => '',
      'zipcode' => '',
      'enable_wifi_calling' => 1,
    ]);

    $this->session->customer_id = false;

    $result = $this->api->portal__UpdateWifiCalling();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testUpdateWifiCallingThrowsMiddleWareErrorException()
  {
    $this->api->setInputValues([
      'address1' => '1234 st',
      'address2' => '',
      'city' => '',
      'state' => '',
      'zipcode' => '',
      'enable_wifi_calling' => 0,
    ]);

    $this->session->customer_id = 12345;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([[
        'current_mobile_number' => 123456789,
        'customer_id' => 12345,
        'plan_state' => 'Active',
        'current_iccid_full' => 1234,
        'preferred_language' => 'EN',
        'cos_id' => 1
      ]])
      ->setMethods(['getWholesalePlan', 'removeAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getWholesalePlan')
      ->will($this->returnValue(1234));

    $result = new \Result();
    $result->fail();

    $customer->expects($this->any())
      ->method('removeAccAddressE911')
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(12345,
        [
          'u.CUSTOMER_ID',
          'current_mobile_number',
          'CURRENT_ICCID_FULL',
          'preferred_language',
          'plan_state',
          'u.BRAND_ID',
          'a.COS_ID'
        ])
      ->will($this->returnValue($customer));

    $sim = new Sim([
      'iccid_number' => $customer->current_iccid_full,
      'iccid_full' => $customer->current_iccid_full,
      'brand_id' => 2,
      'tmoprofilesku' => 'SKU'
    ]);

    $this->simRepository->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($customer->current_iccid_full)
      ->will($this->returnValue($sim));

    $this->api->updateWifiCallingService = $this->getMockBuilder(UpdateWifiCallingService::class)
      ->setConstructorArgs([
        new Address([
          'address1' => '1234 st',
          'address2' => '',
          'city' => '',
          'state_region' => '',
          'postal_code' => '',
        ]),
        $this->customerRepository,
        $this->configuration,
        $this->simRepository,
        $this->utilities,
        12345,
        0,
        123,
        $this->commandInvocation,
        $this->makeItSoRepository
      ])
      ->setMethods(['executeCommand'])
      ->getMock();

    $this->api->updateWifiCallingService->expects($this->any())
      ->method('executeCommand')
      ->will($this->throwException(new MiddleWareException('message', 'MW0001')));

    $result = $this->api->portal__UpdateWifiCalling();

//    print_r($result);

    $this->assertContains('MW0001', $result->data_array['error_codes']);
  }

  public function testMissingE911Address()
  {
    $this->api->setInputValues([
      'address1' => '',
      'address2' => '',
      'city' => '',
      'state' => '',
      'zipcode' => '',
      'enable_wifi_calling' => 1,
    ]);

    $this->session->customer_id = 12345;

    $result = $this->api->portal__UpdateWifiCalling();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testUpdateWifiCallingThrowsDatabaseErrorException()
  {
    $this->api->setInputValues([
      'address1' => '1234 st',
      'address2' => '',
      'city' => '',
      'state' => '',
      'zipcode' => '',
      'enable_wifi_calling' => 0,
    ]);

    $this->session->customer_id = 12345;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([[
        'current_mobile_number' => 123456789,
        'customer_id' => 12345,
        'plan_state' => 'Active',
        'current_iccid_full' => 1234,
        'preferred_language' => 'EN',
        'cos_id' => 1
      ]])
      ->setMethods(['getWholesalePlan', 'removeAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getWholesalePlan')
      ->will($this->returnValue(1234));

    $result = new \Result();
    $result->fail();

    $customer->expects($this->any())
      ->method('removeAccAddressE911')
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(12345,
        [
          'u.CUSTOMER_ID',
          'current_mobile_number',
          'CURRENT_ICCID_FULL',
          'preferred_language',
          'plan_state',
          'u.BRAND_ID',
          'a.COS_ID'
        ])
      ->will($this->returnValue($customer));

    $sim = new Sim([
      'iccid_number' => $customer->current_iccid_full,
      'iccid_full' => $customer->current_iccid_full,
      'brand_id' => 2,
      'tmoprofilesku' => 'SKU'
    ]);

    $this->simRepository->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($customer->current_iccid_full)
      ->will($this->returnValue($sim));

    $this->api->updateWifiCallingService = $this->getMockBuilder(UpdateWifiCallingService::class)
      ->setConstructorArgs([
        new Address([
          'address1' => '1234 st',
          'address2' => '',
          'city' => '',
          'state_region' => '',
          'postal_code' => '',
        ]),
        $this->customerRepository,
        $this->configuration,
        $this->simRepository,
        $this->utilities,
        12345,
        0,
        123,
        $this->commandInvocation,
        $this->makeItSoRepository
      ])
      ->setMethods(['executeCommand'])
      ->getMock();

    $result = $this->api->portal__UpdateWifiCalling();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testUpdateWifiCallingSuccess()
  {
    $this->api->setInputValues([
      'address1' => '1234 st',
      'address2' => '',
      'city' => '',
      'state' => '',
      'zipcode' => '',
      'enable_wifi_calling' => 1,
    ]);

    $this->session->customer_id = 12345;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([[
        'current_mobile_number' => 123456789,
        'customer_id' => 12345,
        'plan_state' => 'Active',
        'current_iccid_full' => 1234,
        'preferred_language' => 'EN',
        'cos_id' => 1
      ]])
      ->setMethods(['getWholesalePlan'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getWholesalePlan')
      ->will($this->returnValue(1234));

    $this->customerRepository->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(12345,
        [
          'u.CUSTOMER_ID',
          'current_mobile_number',
          'CURRENT_ICCID_FULL',
          'preferred_language',
          'plan_state',
          'u.BRAND_ID',
          'a.COS_ID'
        ])
      ->will($this->returnValue($customer));

    $sim = new Sim([
      'iccid_number' => $customer->current_iccid_full,
      'iccid_full' => $customer->current_iccid_full,
      'brand_id' => 2,
      'tmoprofilesku' => 'SKU'
    ]);

    $this->simRepository->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($customer->current_iccid_full)
      ->will($this->returnValue($sim));

    $this->api->updateWifiCallingService = $this->getMockBuilder(UpdateWifiCallingService::class)
      ->setConstructorArgs([
        new Address([
          'address1' => '1234 st',
          'address2' => '',
          'city' => '',
          'state_region' => '',
          'postal_code' => '',
        ]),
        $this->customerRepository,
        $this->configuration,
        $this->simRepository,
        $this->utilities,
        12345,
        1,
        123,
        $this->commandInvocation,
        $this->makeItSoRepository
      ])
      ->setMethods(['executeCommand'])
      ->getMock();

    $result = new \Result();
    $result->succeed();

    $this->api->updateWifiCallingService->expects($this->at(0))
      ->method('executeCommand')
      ->with(UpdateSubscriberAddressCommand::class, [
        'input' => [
          'customer_id' => 12345,
          'actionUUID'  => 123,
          'msisdn'      => $customer->current_mobile_number,
          'iccid'       => $customer->current_iccid_full,
          'E911Address' => [
            'addressLine1' => '1234 st',
            'addressLine2' => '',
            'City'         => '',
            'State'        => '',
            'Zip'          => ''
          ]
        ]
      ])
      ->will($this->returnValue($result));

    $result = $this->api->portal__UpdateWifiCalling();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }

  public function testSetWifiCallingService()
  {
    $this->api->setInputValues([
      'address1' => '123456',
      'address2' => '23456',
      'city' => 'asdasd',
      'state' => 'ca',
      'zipcode' => '1234567',
      'enable_wifi_calling' => 1,
    ]);

    $this->session->customer_id = 12345;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([[
        'current_mobile_number' => 123456789,
        'customer_id' => 12345,
        'plan_state' => 'Active',
        'current_iccid_full' => 1234,
        'preferred_language' => 'EN',
        'cos_id' => 1
      ]])
      ->setMethods(['getWholesalePlan'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getWholesalePlan')
      ->will($this->returnValue(1234));

    $this->customerRepository->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(12345,
        [
          'u.CUSTOMER_ID',
          'current_mobile_number',
          'CURRENT_ICCID_FULL',
          'preferred_language',
          'plan_state',
          'u.BRAND_ID',
          'a.COS_ID'
        ])
      ->will($this->returnValue($customer));

    $result = $this->api->setWifiCallingService(123456, 23456, 'asdasd', 'asdasd', 234567, 1);

//    print_r($result);

    $this->assertInstanceOf(UpdateWifiCallingService::class, $result);
  }

  public function testNonGBASimThrowsInvalidSimStateException()
  {
    $this->api->setInputValues([
      'address1' => '123456',
      'address2' => '23456',
      'city' => 'asdasd',
      'state' => 'ca',
      'zipcode' => '1234567',
      'enable_wifi_calling' => 1,
    ]);

    $this->session->customer_id = 12345;

    $this->customerRepository->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(12345,
        [
          'u.CUSTOMER_ID',
          'current_mobile_number',
          'CURRENT_ICCID_FULL',
          'preferred_language',
          'plan_state',
          'u.BRAND_ID',
          'a.COS_ID'
        ])
      ->will($this->returnValue(new Customer([
        'current_mobile_number' => 123456789,
        'customer_id' => 12345,
        'plan_state' => 'Active',
        'current_iccid_full' => 1234,
        'preferred_language' => 'EN',
        'cos_id' => 1
      ])));

    $this->simRepository->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with(1234)
      ->will($this->returnValue(new Sim(['brand_id' => 1, 'iccid_number' => 1234, 'iccid_full' => 1234])));

    $this->api->setWifiCallingService(123456, 23456, 'asdasd', 'asdasd', 234567, 1);
    $result = $this->api->portal__UpdateWifiCalling();

//    print_r($result);

    $this->assertContains('VV0016', $result->data_array['error_codes']);
  }
}
