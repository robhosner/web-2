<?php
namespace unit_tests\Portal;

use Session;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Portal\Login;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Common;
use Ultra\Utilities\SessionUtilities;

class LoginTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var Login
   */
  public  $api;
  private $params = ['username' => '', 'password' => '', 'token' => '', 'error_language' => ''];
  private $customerRepo;
  private $sessionUtils;
  private $session;
  private $config;
  private $state;
  private $utils;

  public function setUp()
  {
    $this->session = $this->getMock(Session::class, ['verifyToken', 'getTokenMsisdn', 'confirm']);
    $this->sessionUtils = $this->getMock(SessionUtilities::class);
    $this->config = $this->getMock(Configuration::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->state = $this->getMock(StateActions::class);
    $this->utils = $this->getMock(Common::class);

    $this->api = $this->getMockBuilder(Login::class)
      ->setConstructorArgs([$this->session, $this->sessionUtils, $this->config, $this->customerRepo, $this->state, $this->utils])
      ->setMethods(['getUltraCustomerFromMsisdn'])
      ->getMock();

    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testCrmIpConfigurationError()
  {
    $this->api->setInputValues($this->params);

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(''));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('IN0004', $result->data_array['error_codes']);
  }

  public function testUserIsBlockedFromApi()
  {
    $this->api->setInputValues($this->params);

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 1, 6)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('AP0002', $result->data_array['error_codes']);
  }

  public function testLoginTokenIsExpired()
  {
    $this->params['token'] = 123;
    $this->api->setInputValues($this->params);

    $this->session->expects($this->any())
      ->method('verifyToken')
      ->with(123)
      ->will($this->returnValue(false));

    $this->session->expects($this->any())
      ->method('getTokenMsisdn')
      ->with(123)
      ->will($this->returnValue(true));

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 1, 6)
      ->will($this->returnValue(false));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('SE0003', $result->data_array['error_codes']);
  }

  public function testNoSessionFoundForToken()
  {
    $this->params['token'] = 123;
    $this->api->setInputValues($this->params);

    $this->session->expects($this->any())
      ->method('verifyToken')
      ->with(123)
      ->will($this->returnValue(false));

    $this->session->expects($this->any())
      ->method('getTokenMsisdn')
      ->with(123)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 1, 6)
      ->will($this->returnValue(false));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('SE0010', $result->data_array['error_codes']);
  }

  public function testInvalidLoginInformation()
  {
    $this->params['token'] = 123;
    $this->api->setInputValues($this->params);

    $this->session->msisdn = false;

    $this->session->expects($this->any())
      ->method('verifyToken')
      ->with(123)
      ->will($this->returnValue(true));

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 1, 6)
      ->will($this->returnValue(false));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('SE0010', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->params['token'] = 123;
    $this->api->setInputValues($this->params);

    $this->session->msisdn = 1234;
    $this->session->expects($this->any())
      ->method('verifyToken')
      ->with(123)
      ->will($this->returnValue(true));

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 1, 6)
      ->will($this->returnValue(false));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->api->expects($this->any())
      ->method('getUltraCustomerFromMsisdn')
      ->with(1234)
      ->will($this->returnValue(false));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('VV0108', $result->data_array['error_codes']);
  }

  public function testCustomerNumericUsernameAndPasswordInvalidInformation()
  {
    $this->params['username'] = 123;
    $this->params['password'] = 1234;
    $this->api->setInputValues($this->params);

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(true));

    $customer = new \stdClass();
    $customer->CUSTOMER_ID = 123;
    $customer->login_password = 123;

    $this->api->expects($this->any())
      ->method('getUltraCustomerFromMsisdn')
      ->with(123)
      ->will($this->returnValue($customer));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromCustomersTable')
      ->with($customer->CUSTOMER_ID, ['LOGIN_PASSWORD'])
      ->will($this->returnValue($customer));

    $this->utils->expects($this->any())
      ->method('authenticatePasswordHS')
      ->with(123, 1234)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 123, 6)
      ->will($this->returnValue(false));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('SE0011', $result->data_array['error_codes']);
  }

  public function testCustomerUsernameAndPasswordInvalidInformation()
  {
    $this->params['username'] = 'test123';
    $this->params['password'] = 1234;
    $this->api->setInputValues($this->params);

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(true));

    $customer = new \stdClass();
    $customer->CUSTOMER_ID = 123;
    $customer->login_password = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromUsername')
      ->with('test123')
      ->will($this->returnValue($customer));

    $this->utils->expects($this->any())
      ->method('authenticatePasswordHS')
      ->with(123, 1234)
      ->will($this->returnValue(false));

    $this->sessionUtils->expects($this->any())
      ->method('checkIfUserIsBlockedFromApi')
      ->with('portal__Login', 123, 6)
      ->will($this->returnValue(false));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('SE0011', $result->data_array['error_codes']);
  }

  public function testCustomerMissingLoginInformation()
  {
    $this->api->setInputValues($this->params);
    $this->session->customer_id = false;

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(true));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('MP0027', $result->data_array['error_codes']);
  }

  public function testApiIsNotAllowedForBrand()
  {
    $this->session->customer_id = 1;
    $this->api->setInputValues($this->params);

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(true));

    $customer = new \stdClass();
    $customer->CUSTOMER_ID = 123;
    $customer->BRAND_ID = 1;
    $customer->login_password = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['BRAND_ID'])
      ->will($this->returnValue($customer));

    $this->config->expects($this->any())
      ->method('isBrandAllowedByAPIPartner')
      ->with(1, null)
      ->will($this->returnValue(false));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('FA0004', $result->data_array['error_codes']);
  }

  public function testCustomerHasCancelledState()
  {
    $this->getCustomerToStateCheck();

    $this->state->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Cancelled']));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('IN0003', $result->data_array['error_codes']);
  }

  public function testCustomerHasNeutralState()
  {
    $this->getCustomerToStateCheck();

    $this->state->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Neutral']));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('VV0052', $result->data_array['error_codes']);
  }

  public function testCustomerHasProvisionedState()
  {
    $this->getCustomerToStateCheck();

    $this->state->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Provisioned']));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('VV0069', $result->data_array['error_codes']);
  }

  public function testCustomerHasInvalidState()
  {
    $this->getCustomerToStateCheck();

    $this->state->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('VV0071', $result->data_array['error_codes']);
  }

  public function testFailSessionConfirmed()
  {
    $this->getCustomerToStateCheck();

    $this->state->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Active']));

    $this->state->expects($this->any())
      ->method('confirm')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('MP0027', $result->data_array['error_codes']);
  }

  public function testSuccessfulLogin()
  {
    $this->getCustomerToStateCheck();

    $this->state->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Active']));

    $this->session->expects($this->any())
      ->method('confirm')
      ->with(123)
      ->will($this->returnValue(true));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }

  public function testCustomerCantBeFound()
  {
    $this->params['username'] = 123;
    $this->params['password'] = 1234;
    $this->api->setInputValues($this->params);

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(true));

    $this->api->expects($this->any())
      ->method('getUltraCustomerFromMsisdn')
      ->with(123)
      ->will($this->throwException(new MissingRequiredParametersException('')));

    $result = $this->api->portal__Login();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  private function getCustomerToStateCheck()
  {
    $this->session->customer_id = 1;
    $this->api->setInputValues($this->params);

    $this->sessionUtils->expects($this->any())
      ->method('getClientIp')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('getCrmIpAddresses')
      ->will($this->returnValue(1));

    $this->sessionUtils->expects($this->any())
      ->method('isIpAddressInArray')
      ->with(1, 1)
      ->will($this->returnValue(true));

    $customer = new \stdClass();
    $customer->CUSTOMER_ID = 123;
    $customer->BRAND_ID = 1;
    $customer->login_password = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['BRAND_ID'])
      ->will($this->returnValue($customer));

    $this->config->expects($this->any())
      ->method('isBrandAllowedByAPIPartner')
      ->with(1, null)
      ->will($this->returnValue(true));
  }
}
