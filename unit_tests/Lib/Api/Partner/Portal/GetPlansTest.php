<?php

use Ultra\Configuration\Configuration;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Lib\Api\Partner\Portal\GetPlans;
use Ultra\Lib\ApiErrors;
use Ultra\Plans\Plan;
use Ultra\Plans\Repositories\Cfengine\PlansRepository;

class GetPlansTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var PlansRepository
   */
  private $plansRepo;
  
  /**
   * @var GetPlans
   */
  private $api;

  public function setUp()
  {
    $this->plansRepo = $this->getMockBuilder(PlansRepository::class)
      ->setConstructorArgs([
        $this->getMock(Configuration::class),
        $this->getMock(FeatureFlagsClientWrapper::class)
      ])
      ->getMock();
    
    $this->api = new GetPlans($this->plansRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }
  
  public function testPlanIdAndBrandThrowsException()
  {
    $this->api->setInputValues(['brand' => 'MINT', 'plan_id' => '1234']);
    $result = $this->api->portal__GetPlans();
    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testPlanIdAndBrandReturnsPlan()
  {
    $this->plansRepo->expects($this->any())
      ->method('getPlanByBrandAndPlanId')
      ->with('MINT', '123')
      ->will($this->returnValue(new Plan([])));
    
    $this->api->setInputValues(['brand' => 'MINT', 'plan_id' => '123']);
    $result = $this->api->portal__GetPlans();
    $this->assertNotEmpty($result->data_array['plans']);
  }

  public function testBrandThrowsException()
  {
    $this->api->setInputValues(['brand' => 'FAKE', 'plan_id' => '']);
    $result = $this->api->portal__GetPlans();
    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testBrandReturnsPlans()
  {
    $this->plansRepo->expects($this->any())
      ->method('getAllPlansByBrand')
      ->with('MINT')
      ->will($this->returnValue(new Plan(['planId' => 'M01S'])));

    $this->api->setInputValues(['brand' => 'MINT', 'plan_id' => '']);
    $result = $this->api->portal__GetPlans();
    $this->assertNotEmpty($result->data_array['plans']);
  }
}