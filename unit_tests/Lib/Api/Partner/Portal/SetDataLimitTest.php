<?php
namespace unit_tests\Lib\Api\Partner\Portal;

use Session;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Api\Partner\Portal\SetDataLimit;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;

class SetDataLimitTest extends \PHPUnit_Framework_TestCase
{

  private $api;
  private $familyApi;
  private $config;
  private $customer_id = 123;
  private $sharedData;
  private $session;

  public function setUp()
  {
    $this->config = $this->getMockBuilder(Configuration::class)
      ->setConstructorArgs([])
      ->setMethods(['getFamilyAPIConfig', 'getPlanFromCosId', 'getSharedDataConfig'])
      ->getMock();

    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->config->expects($this->any())
      ->method('getSharedDataConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->familyApi = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([$this->config])
      ->setMethods(['getFamilyByCustomerID', 'removeFamilyMember'])
      ->getMock();

    $this->sharedData = $this->getMockBuilder(SharedData::class)
      ->setConstructorArgs([$this->config])
      ->setMethods(['getBucketIDByCustomerID', 'getBucketByBucketID'])
      ->getMock();

    $this->session = $this->getMock(Session::class);
    $this->session->customer_id = 123;

    $this->api = $this->getMockBuilder(SetDataLimit::class)
      ->setConstructorArgs([$this->familyApi, $this->session, $this->sharedData])
      ->setMethods(['setDataLimit'])
      ->getMock();

    $this->api->result          = new \Result();
    $this->api->defaultValues   = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $this->api->setInputValues([]);
  }

  public function test_InvalidSession()
  {
    $this->api->setInputValues([
      'customer_id' => null,
      'percent'     => 50
    ]);

    $this->session->customer_id = false;
    $result = $this->api->portal__SetDataLimit();

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function test_GetFamilyFailed()
  {
    $this->api->setInputValues([
      'customer_id' => 2,
      'percent'     => 50
    ]);

    $this->session->customer_id = 1;

    $familyResult = new \Result();

    $this->familyApi->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->with(2)
      ->will($this->returnValue($familyResult));

    $result = $this->api->portal__SetDataLimit();
    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function test_InsufficientPrivileges()
  {
    $this->api->setInputValues([
      'customer_id' => 2,
      'percent'     => 50
    ]);

    $this->session->customer_id = 1;

    $familyResult = new \Result(null, true);
    $familyResult->data_array = [
      'parentCustomerId'  => 3
    ];

    $this->familyApi->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->with(2)
      ->will($this->returnValue($familyResult));

    $result = $this->api->portal__SetDataLimit();
    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function test_SetDataLimitThrows()
  {
    $this->api->setInputValues([
      'customer_id' => 2,
      'percent'     => 50
    ]);

    $this->session->customer_id = 1;

    $familyResult = new \Result(null, true);
    $familyResult->data_array = [
      'parentCustomerId'  => 1
    ];

    $this->familyApi->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->with(2)
      ->will($this->returnValue($familyResult));

    $this->api->expects($this->once())
      ->method('setDataLimit')
      ->will($this->throwException(new \Exception));

    $result = $this->api->portal__SetDataLimit();
    $this->assertFalse($result->is_success());
  }

  public function test_Success()
  {
    $this->api->setInputValues([
      'customer_id' => 2,
      'percent'     => 50
    ]);

    $this->session->customer_id = 1;

    $familyResult = new \Result(null, true);
    $familyResult->data_array = [
      'parentCustomerId'  => 1
    ];

    $this->familyApi->expects($this->once())
      ->method('getFamilyByCustomerID')
      ->with(2)
      ->will($this->returnValue($familyResult));

    $this->api->expects($this->once())
      ->method('setDataLimit')
      ->will($this->returnValue(null));

    $result = $this->api->portal__SetDataLimit();
    $this->assertTrue($result->is_success());
  }
}
