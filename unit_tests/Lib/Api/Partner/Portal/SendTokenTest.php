<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\SendToken;
use Ultra\Lib\ApiErrors;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\SessionUtilities;

class SendTokenTest extends PHPUnit_Framework_TestCase
{
  public $messenger;
  public $session;
  public $customerRepo;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * @var SendToken
   */
  public $api;

  public $apiParams = [
    'url' => 'my.ultra.com',
    'route' => 't',
    'msisdn' => '123456789',
    'token' => 'TOKEN',
    'error_language' => '',
    'brand' => '',
  ];

  public function setUp()
  {
    $this->session = $this->getMock(SessionUtilities::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->messenger = $this->getMock(Messenger::class);
    $this->api = new SendToken($this->customerRepo, $this->session, $this->messenger);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMissingUpdateParams()
  {
    $this->apiParams['msisdn'] = '';
    $this->apiParams['token'] = '';
    $this->api->setInputValues($this->apiParams);

    $result = $this->api->portal__SendToken();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testApiIsDisabled()
  {
    $this->api->setInputValues($this->apiParams);

    $session = new stdClass();
    $session->ip = '10.10.10.10';

    $this->session->expects($this->any())
      ->method('getSession')
      ->with($this->apiParams['msisdn'], $this->apiParams['token'])
      ->will($this->returnValue($session));

    $this->session->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__SendToken', $session->ip, 6, false)
      ->will($this->returnValue(true));

    $result = $this->api->portal__SendToken();

//    print_r($result);

    $this->assertContains('AP0001', $result->data_array['error_codes']);
  }

  public function testFailedToGenerateToken()
  {
    $this->api->setInputValues($this->apiParams);

    $session = new stdClass();
    $session->ip = '10.10.10.10';
    $session->token = false;

    $this->session->expects($this->any())
      ->method('getSession')
      ->with($this->apiParams['msisdn'], $this->apiParams['token'])
      ->will($this->returnValue($session));

    $this->session->expects($this->at(1))
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__SendToken', $session->ip, 6, false)
      ->will($this->returnValue(false));

    $result = $this->api->portal__SendToken();

//    print_r($result);

    $this->assertContains('SE0001', $result->data_array['error_codes']);
  }

  public function testCustomerIsNotFound()
  {
    $this->api->setInputValues($this->apiParams);

    $session = new stdClass();
    $session->ip = '10.10.10.10';
    $session->token = true;
    $session->msisdn = $this->apiParams['msisdn'];

    $this->session->expects($this->any())
      ->method('getSession')
      ->with($this->apiParams['msisdn'], $this->apiParams['token'])
      ->will($this->returnValue($session));

    $this->session->expects($this->at(2))
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__SendToken', $session->ip, 6, true)
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($session->msisdn)
      ->will($this->returnValue(false));

    $result = $this->api->portal__SendToken();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testSMSDeliveryFails()
  {
    $this->api->setInputValues($this->apiParams);

    $session = new stdClass();
    $session->ip = '10.10.10.10';
    $session->token = true;
    $session->msisdn = $this->apiParams['msisdn'];

    $this->session->expects($this->any())
      ->method('getSession')
      ->with($this->apiParams['msisdn'], $this->apiParams['token'])
      ->will($this->returnValue($session));

    $customer = new stdClass();
    $customer->CUSTOMER_ID = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($session->msisdn)
      ->will($this->returnValue($customer));

    $result = new Result();
    $result->fail();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->with($customer->CUSTOMER_ID, 'send_login_token', ['url' => $this->apiParams['url'] . "/$session->token/" . $this->apiParams['route']])
      ->will($this->returnValue($result));

    $result = $this->api->portal__SendToken();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->api->setInputValues($this->apiParams);

    $session = new stdClass();
    $session->ip = '10.10.10.10';
    $session->token = true;
    $session->msisdn = $this->apiParams['msisdn'];

    $this->session->expects($this->any())
      ->method('getSession')
      ->with($this->apiParams['msisdn'], $this->apiParams['token'])
      ->will($this->returnValue($session));

    $customer = new stdClass();
    $customer->CUSTOMER_ID = 123;
    $customer->current_mobile_number = 1234;
    $customer->LOGIN_NAME = 'NAME';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($session->msisdn)
      ->will($this->returnValue($customer));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->with($customer->CUSTOMER_ID, 'send_login_token', ['url' => $this->apiParams['url'] . "/$session->token/" . $this->apiParams['route']])
      ->will($this->returnValue($result));

    $result = $this->api->portal__SendToken();

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertTrue($result->data_array['token']);
  }
}
