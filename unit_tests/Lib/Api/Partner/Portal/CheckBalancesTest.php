<?php
namespace unit_tests\Portal;

use Session;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\CheckBalances;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\CheckBalance;

class CheckBalancesTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var CheckBalances
   */
  public  $api;
  private $params = [];
  private $session;
  private $customerRepo;
  private $checkBalance;

  public function setUp()
  {
    $this->session = $this->getMock(Session::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->checkBalance = $this->getMock(CheckBalance::class, ['byMSISDN']);
    $this->api = new CheckBalances($this->session, $this->customerRepo, $this->checkBalance);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues($this->params);
  }

  public function testCustomerIsNotLoggedIn()
  {
    $this->session->customer_id = 0;
    $result = $this->api->portal__CheckBalances();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerIsNotFound()
  {
    $this->session->customer_id = 123;
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['current_mobile_number'], true)
      ->will($this->returnValue(false));

    $result = $this->api->portal__CheckBalances();

    $this->assertContains('VV0031', $result->data_array['error_codes']);

    $customer = new \stdClass();
    $customer->current_mobile_number = 0;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['current_mobile_number'], true)
      ->will($this->returnValue($customer));

    $result = $this->api->portal__CheckBalances();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testGetCustomerBalances()
  {
    $this->session->customer_id = 123;
    $customer = new \stdClass();
    $customer->current_mobile_number = 1234;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['current_mobile_number'], true)
      ->will($this->returnValue($customer));

    $balances = ['items'];

    $this->checkBalance->expects($this->any())
      ->method('byMSISDN')
      ->with($customer->current_mobile_number)
      ->will($this->returnValue(['items']));

    $result = $this->api->portal__CheckBalances();

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals($balances, $result->data_array['balances']);
  }
}
