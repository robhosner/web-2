<?php

use Ultra\Lib\Api\Partner\Portal\PayForOrder;
use Ultra\Lib\ApiErrors;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Services\OnlineSalesAPI;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Payments\Payment;


class PayForOrderTest extends PHPUnit_Framework_TestCase
{
  private $api;
  private $session;
  private $configuration;
  private $customerRepo;
  private $payment;
  private $ordersAPI;

  private $validRequest = [
    'order_id'      => 1,
    'funds_source'  => 'CC',
    'zip_code'      => null
  ];
  private $validOrder = [
    'items' => [
      [ 'item1' ]
    ],
    'status'  => 'created',
    'cost' => [
      'tsp'         => [ 'totalAmount' => 1],
      'wallet'      => [ 'totalAmount' => 1],
      'creditCard'  => [ 'totalAmount' => 1]
    ]
  ];

  public function setUp()
  {
    $this->session = $this->getMock(Session::class);
    $this->configuration = $this->getMock(Configuration::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->payment = $this->getMock(Payment::class);

    $onlineSalesConfig = [
      'host'      => 'web-qa-08:3040',
      'basepath'  => '/v1',
      'jwtSecret' => 'shhhhhhared-secret'
    ];
    $this->configuration->expects($this->any())
      ->method('getOnlineSalesAPIConfig')
      ->will($this->returnValue($onlineSalesConfig));

    $this->ordersAPI = $this->getMock(OnlineSalesAPI::class, [], [$this->configuration]);

    $this->api = new PayForOrder(
        $this->session,
        $this->configuration,
        $this->customerRepo,
        $this->ordersAPI,
        $this->payment
    );

    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->session->customer_id = null;

    $this->api->setInputValues($this->validRequest);
    $result = $this->api->portal__PayForOrder();

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->session->customer_id = 1;

    // getCombinedCustomerByCustomerId returns null
    $this->customerRepo->expects($this->once())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(null));

    $this->api->setInputValues($this->validRequest);
    $result = $this->api->portal__PayForOrder();

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testOrderNotFound()
  {
    $this->session->customer_id = 1;

    $this->customerRepo->expects($this->once())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(
        (object)[
          'customer_id' => 1
        ]
      ));

    $result = new Result();
    $result->add_error('testing error');
    $this->ordersAPI->expects($this->once())
      ->method('getOrderByOrderID')
      ->will($this->returnValue($result));

    $this->api->setInputValues($this->validRequest);
    $result = $this->api->portal__PayForOrder();

    $this->assertFalse($result->is_success());
  }

  public function testInvalidOrder()
  {
    $this->session->customer_id = 1;

    $this->customerRepo->expects($this->once())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(
        (object)[
          'customer_id' => 1
        ]
      ));

    $result = new Result();
    $result->data_array = [];
    $result->succeed();
    $this->ordersAPI->expects($this->once())
      ->method('getOrderByOrderID')
      ->will($this->returnValue($result));

    $this->api->setInputValues($this->validRequest);
    $result = $this->api->portal__PayForOrder();

    $this->assertFalse($result->is_success());
  }

  public function testCCChargeFails()
  {
    $this->session->customer_id = 1;

    $this->customerRepo->expects($this->once())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(
        (object)[
          'customer_id' => 1
        ]
      ));

    $result = new Result();
    $result->data_array = $this->validOrder;
    $result->succeed();
    $this->ordersAPI->expects($this->once())
      ->method('getOrderByOrderID')
      ->will($this->returnValue($result));

    $result = new Result();
    $result->add_error('cc transaction failed');
    $this->payment->expects($this->once())
      ->method('funcAddFundsByTokenizedCC')
      ->will($this->returnValue($result));

    $this->api->setInputValues($this->validRequest);
    $result = $this->api->portal__PayForOrder();

    $this->assertFalse($result->is_success());
    $this->assertContains('CC0001', $result->data_array['error_codes']);
  }

  public function testSpendFromWalletFails()
  {
    $this->session->customer_id = 1;

    $this->customerRepo->expects($this->once())
      ->method('getCombinedCustomerByCustomerId')
      ->will($this->returnValue(
        (object)[
          'customer_id' => 1
        ]
      ));

    $result = new Result();
    $result->data_array = $this->validOrder;
    $result->succeed();
    $this->ordersAPI->expects($this->once())
      ->method('getOrderByOrderID')
      ->will($this->returnValue($result));

    $this->payment->expects($this->any())
      ->method('funcSpendFromBalance')
      ->will($this->returnValue(['errors' => ['errr']]));

    $this->api->setInputValues([
      'order_id'      => 1,
      'funds_source'  => 'WALLET',
      'zip_code'      => null
    ]);
    $result = $this->api->portal__PayForOrder();

    $this->assertFalse($result->is_success());
  }
}