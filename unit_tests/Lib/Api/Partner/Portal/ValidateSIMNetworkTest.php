<?php
namespace unit_tests\Portal;

use Ultra\Lib\Api\Partner\Portal\ValidateSIMNetwork;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;

class ValidateSIMNetworkTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var ValidateSIMNetwork
   */
  public  $api;
  private $mwControl;
  private $params = ['ICCID' => 12345];

  public function setUp()
  {
    $this->mwControl = $this->getMock(Control::class);
    $this->api = new ValidateSIMNetwork($this->mwControl);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMiddlewareFailsWithTimeout()
  {
    $this->api->setInputValues($this->params);

    $result = new \Result();
    $result->fail();
    $result->timeout();

    $this->mwControl->expects($this->any())
      ->method('mwCanActivate')
      ->with(['actionUUID' => $this->api->actionId, 'iccid'  => $this->params['ICCID']])
      ->will($this->returnValue($result));

    $result = $this->api->portal__ValidateSIMNetwork();

//    print_r($result);

    $this->assertContains('MW0002', $result->data_array['error_codes']);
  }

  public function testMiddlewareFails()
  {
    $this->api->setInputValues($this->params);

    $result = new \Result();
    $result->fail();

    $this->mwControl->expects($this->any())
      ->method('mwCanActivate')
      ->with(['actionUUID' => $this->api->actionId, 'iccid'  => $this->params['ICCID']])
      ->will($this->returnValue($result));

    $result = $this->api->portal__ValidateSIMNetwork();

//    print_r($result);

    $this->assertContains('MW0001', $result->data_array['error_codes']);
  }

  public function testMiddlewareResultDoesntHaveAvailableKey()
  {
    $this->api->setInputValues($this->params);

    $result = new \Result();
    $result->succeed();

    $this->mwControl->expects($this->any())
      ->method('mwCanActivate')
      ->with(['actionUUID' => $this->api->actionId, 'iccid'  => $this->params['ICCID']])
      ->will($this->returnValue($result));

    $result = $this->api->portal__ValidateSIMNetwork();

//    print_r($result);

    $this->assertContains('MW0001', $result->data_array['error_codes']);
  }

  public function testValidNetworkReturnsTrue()
  {
    $this->api->setInputValues($this->params);

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['available' => true]);

    $this->mwControl->expects($this->any())
      ->method('mwCanActivate')
      ->with(['actionUUID' => $this->api->actionId, 'iccid'  => $this->params['ICCID']])
      ->will($this->returnValue($result));

    $result = $this->api->portal__ValidateSIMNetwork();

//    print_r($result);

    $this->assertTrue($result->data_array['valid_network']);
  }
}
