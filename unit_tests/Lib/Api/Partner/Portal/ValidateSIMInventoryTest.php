<?php
namespace unit_tests\Portal;

use Ultra\Configuration\Configuration;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Portal\ValidateSIMInventory;
use Ultra\Lib\ApiErrors;
use Ultra\Sims\Repositories\Mssql\SimRepository;

class ValidateSIMInventoryTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var ValidateSIMInventory
   */
  public  $api;
  private $config;
  private $simRepo;
  private $params = ['ICCID' => 12345, 'brand' => 1];

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->simRepo = $this->getMock(SimRepository::class);
    $this->api = new ValidateSIMInventory($this->simRepo, $this->config);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testBrandIdDoesNotMatchSimBrandId()
  {
    $this->api->setInputValues($this->params);

    $sim = new \stdClass();
    $sim->brand_id = 1;

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($this->params['ICCID'])
      ->will($this->returnValue($sim));

    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->params['brand'])
      ->will($this->returnValue(3));

    $result = $this->api->portal__ValidateSIMInventory();

//    print_r($result);

    $this->assertContains('IC0003', $result->data_array['error_codes']);
  }

  public function testSimValidUsed()
  {
    $this->api->setInputValues($this->params);

    $sim = new \stdClass();
    $sim->brand_id = 1;
    $sim->sim_activated = 1;
    $sim->expires_date = null;
    $sim->reservation_time = false;

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($this->params['ICCID'])
      ->will($this->returnValue($sim));

    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->params['brand'])
      ->will($this->returnValue(1));

    $result = $this->api->portal__ValidateSIMInventory();

//    print_r($result);

    $this->assertEquals('USED', $result->data_array['valid']);
    $this->assertEquals('USED', $result->data_array['valid_ext']);
    $this->assertContains('VV0065', $result->data_array['error_codes']);
  }

  public function testSimValid()
  {
    $this->api->setInputValues($this->params);

    $sim = new \stdClass();
    $sim->brand_id = 1;
    $sim->sim_activated = 0;
    $sim->expires_date = null;
    $sim->reservation_time = false;

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($this->params['ICCID'])
      ->will($this->returnValue($sim));

    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->params['brand'])
      ->will($this->returnValue(1));

    $result = $this->api->portal__ValidateSIMInventory();

//    print_r($result);

    $this->assertEquals('VALID', $result->data_array['valid']);
    $this->assertEquals('VALID', $result->data_array['valid_ext']);
    $this->assertTrue($result->is_success());
  }

  public function testSimValidExtReserved()
  {
    $this->api->setInputValues($this->params);

    $sim = new \stdClass();
    $sim->brand_id = 1;
    $sim->sim_activated = 0;
    $sim->expires_date = null;
    $sim->reservation_time = 1;

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($this->params['ICCID'])
      ->will($this->returnValue($sim));

    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->params['brand'])
      ->will($this->returnValue(1));

    $result = $this->api->portal__ValidateSIMInventory();

//    print_r($result);

    $this->assertEquals('USED', $result->data_array['valid']);
    $this->assertEquals('RESERVED', $result->data_array['valid_ext']);
    $this->assertContains('VV0065', $result->data_array['error_codes']);
  }

  public function testSimNotFound()
  {
    $this->api->setInputValues($this->params);
    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($this->params['ICCID'])
      ->will($this->throwException(new InvalidObjectCreationException('message', 'MP0001')));

    $this->config->expects($this->any())
      ->method('getShortNameFromBrandId')
      ->with($this->params['brand'])
      ->will($this->returnValue(1));

    $result = $this->api->portal__ValidateSIMInventory();

//    print_r($result);

    $this->assertContains('IC0001', $result->data_array['error_codes']);
  }
}
