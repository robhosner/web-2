<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\AcceptTermsOfService;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Common;

class AcceptTermsOfServiceTest extends PHPUnit_Framework_TestCase
{
  public $utils;
  public $session;
  public $customerRepo;

  /**
   * @var AcceptTermsOfService
   */
  public $api;

  public function setUp()
  {
    $this->utils = $this->getMock(Common::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->session = $this->getMock(Session::class);

    $this->api = new AcceptTermsOfService($this->utils, $this->session, $this->customerRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->session->customer_id = false;
    $result = $this->api->portal__AcceptTermsOfService();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->session->customer_id = 123;
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['preferred_language'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__AcceptTermsOfService();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerAcceptTermsOfServiceFails()
  {
    $this->session->customer_id = 123;
    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['acceptTermsOfService'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('acceptTermsOfService')
      ->with()
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['preferred_language'])
      ->will($this->returnValue($this->customer));

    $this->api->customer = $this->customer;

    $result = $this->api->portal__AcceptTermsOfService();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->session->customer_id = 123;
    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['acceptTermsOfService'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('acceptTermsOfService')
      ->with()
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['preferred_language'])
      ->will($this->returnValue($this->customer));

    $this->api->customer = $this->customer;

    $result = $this->api->portal__AcceptTermsOfService();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
