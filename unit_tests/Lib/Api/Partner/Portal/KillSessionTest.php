<?php
namespace unit_tests\Portal;

use Session;
use Ultra\Lib\Api\Partner\Portal\KillSession;
use Ultra\Lib\ApiErrors;

class KillSessionTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var KillSession
   */
  public  $api;
  private $params = [];
  private $session;

  public function setUp()
  {
    $this->session = $this->getMock(Session::class, ['kill']);
    $this->api = new KillSession($this->session);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testFailedToEndSession()
  {
    $this->api->setInputValues($this->params);

    $this->session->expects($this->any())
      ->method('kill')
      ->will($this->returnValue(false));

    $result = $this->api->portal__KillSession();

//    print_r($result);

    $this->assertContains('SE0003', $result->data_array['error_codes']);
  }

  public function testSuccessfullyEndSession()
  {
    $this->api->setInputValues($this->params);

    $this->session->expects($this->any())
      ->method('kill')
      ->will($this->returnValue(true));

    $result = $this->api->portal__KillSession();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
