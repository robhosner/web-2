<?php

use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\SendFamilyReplenishmentSms;
use Ultra\Lib\Api\Partner\Portal\SetCustomerPreferences;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\Common;


class SetCustomerPreferencesTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var SetCustomerPreferences
   */
  public $api;
  public $session;
  public $customerRepo;
  public $control;
  public $config;
  public $utilities;
  public $accountRepo;
  public $customer;
  public $cardRepo;
  public $messenger;
  public $sendFamilyRemplishment;

  public function setUp()
  {
    // API setup
    $this->session = $this->getMock(Session::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->control = $this->getMock(Control::class);
    $this->config = $this->getMock(Configuration::class);
    $this->utilities = $this->getMock(Common::class);
    $this->accountRepo = $this->getMock(AccountsRepository::class);
    $this->cardRepo = $this->getMock(CreditCardRepository::class);
    $this->messenger = $this->getMock(Messenger::class);

    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $familyApi = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $config,
      ])
      ->getMock();

    $this->sendFamilyRemplishment = $this->getMockBuilder(SendFamilyReplenishmentSms::class)
      ->setConstructorArgs([$familyApi, $this->customerRepo, $this->messenger])
      ->getMock();

    $this->api = new SetCustomerPreferences(
      $this->session,
      $this->customerRepo,
      $this->control,
      $this->config,
      $this->utilities,
      $this->accountRepo,
      $this->cardRepo,
      $this->messenger,
      $this->sendFamilyRemplishment
    );
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues([
      'preferred_language' => '',
      'auto_recharge'      => '',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = false;
    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues([
      'preferred_language' => '',
      'auto_recharge'      => '',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue(false));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testAccountNotFound()
  {
    $this->api->setInputValues([
      'preferred_language' => '',
      'auto_recharge'      => '',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;
    $customer = new stdClass();
    $customer->customer_id = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($customer));

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerStateIsCancelled()
  {
    $this->api->setInputValues([
      'preferred_language' => '',
      'auto_recharge'      => '',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;
    $customer = new stdClass();
    $customer->customer_id = 123;
    $customer->plan_state = 'Cancelled';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($customer));

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(true));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('IN0001', $result->data_array['error_codes']);
  }

  public function testMissingARequiredParam()
  {
    $this->api->setInputValues([
      'preferred_language' => '',
      'auto_recharge'      => '',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;
    $customer = new stdClass();
    $customer->customer_id = 123;
    $customer->plan_state = 'Active';

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($customer));

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(true));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testFailedChangingVoiceMailLanguage()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->fail();
    $result->add_error('Fail');

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->api->customer = $this->customer;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->fail();

    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testCustomerHasNoCreditCardOnFile()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '0',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo', 'hasCreditCard'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->fail();
    $result->add_error('Fail');

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->customer->expects($this->any())
      ->method('hasCreditCard')
      ->with()
      ->will($this->returnValue(false));

    $this->api->customer = $this->customer;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->fail();

    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $cardInfo = new stdClass();
    $cardInfo->LAST_FOUR = 1234;

    $this->cardRepo->expects($this->any())
      ->method('getCcInfoFromCustomerId')
      ->will($this->returnValue($cardInfo));

    $result = new Result();
    $result->fail();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('CC0005', $result->data_array['error_codes']);
  }

  public function testSendAutoRechargeSmsFails()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '1',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo', 'hasCreditCard'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->fail();
    $result->add_error('Fail');

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->customer->expects($this->any())
      ->method('hasCreditCard')
      ->with()
      ->will($this->returnValue(true));

    $this->api->customer = $this->customer;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->succeed();

    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $cardInfo = new stdClass();
    $cardInfo->LAST_FOUR = 1234;

    $this->cardRepo->expects($this->any())
    ->method('getCcInfoFromCustomerId')
    ->will($this->returnValue($cardInfo));

    $result = new Result();
    $result->fail();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('SM0002', $result->data_array['error_codes']);
  }

  public function testCustomerHasCreditCardOnFileAndChangingVoiceMailFails()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '0',
      'marketing_sms'      => '',
      'marketing_email'    => '',
      'marketing_voice'    => '',
      'party_sms'          => '',
      'party_email'        => '',
      'party_voice'        => '',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo', 'hasCreditCard'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->fail();
    $result->add_error('Fail');

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->customer->expects($this->any())
      ->method('hasCreditCard')
      ->with()
      ->will($this->returnValue(true));

    $this->api->customer = $this->customer;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->fail();

    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $cardInfo = new stdClass();
    $cardInfo->LAST_FOUR = 1234;

    $this->cardRepo->expects($this->any())
      ->method('getCcInfoFromCustomerId')
      ->will($this->returnValue($cardInfo));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testCustomerMarketSettingsUpdateFails()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '0',
      'marketing_sms'      => 'Hello',
      'marketing_email'    => 'Hello',
      'marketing_voice'    => 'Hello',
      'party_sms'          => 'Hello',
      'party_email'        => 'Hello',
      'party_voice'        => 'Hello',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo', 'hasCreditCard', 'setMarketingSettings', 'setUltraCustomerOptionsByArray'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->succeed();

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->customer->expects($this->any())
      ->method('hasCreditCard')
      ->with()
      ->will($this->returnValue(true));

    $this->customer->expects($this->any())
      ->method('setMarketingSettings')
      ->with()
      ->will($this->returnValue(['Error']));

     $this->customer->expects($this->any())
      ->method('setUltraCustomerOptionsByArray')
      ->with()
      ->will($this->returnValue(1));

    $this->api->customer = $this->customer;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->succeed();


    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $cardInfo = new stdClass();
    $cardInfo->LAST_FOUR = 1234;

    $this->cardRepo->expects($this->any())
      ->method('getCcInfoFromCustomerId')
      ->will($this->returnValue($cardInfo));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testCustomerMarketVoiceOptInFails()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '1',
      'marketing_sms'      => 'Hello',
      'marketing_email'    => 'Hello',
      'marketing_voice'    => 'Hello',
      'party_sms'          => 'Hello',
      'party_email'        => 'Hello',
      'party_voice'        => 'Hello',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo', 'hasCreditCard', 'setMarketingSettings', 'optInVoicePreference', 'setUltraCustomerOptionsByArray'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->succeed();

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->customer->expects($this->any())
      ->method('hasCreditCard')
      ->with()
      ->will($this->returnValue(true));

    $this->customer->expects($this->any())
      ->method('setMarketingSettings')
      ->with()
      ->will($this->returnValue([[ 'error error' ]]));

    $result = new Result();
    $result->fail();

    $this->customer->expects($this->any())
      ->method('setUltraCustomerOptionsByArray')
      ->with()
      ->will($this->returnValue(0));

    $this->api->customer = $this->customer;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->succeed();


    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $cardInfo = new stdClass();
    $cardInfo->LAST_FOUR = 1234;

    $this->cardRepo->expects($this->any())
      ->method('getCcInfoFromCustomerId')
      ->will($this->returnValue($cardInfo));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

// print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->api->setInputValues([
      'preferred_language' => 'EN',
      'auto_recharge'      => '1',
      'marketing_sms'      => '1',
      'marketing_email'    => '1',
      'marketing_voice'    => '0',
      'party_sms'          => '0',
      'party_email'        => '0',
      'party_voice'        => '0',
      'tag'                => 'CUSTOMER_PORTAL'
    ]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getWholesalePlan', 'updateUltraInfo', 'hasCreditCard', 'setMarketingSettings', 'optInVoicePreference', 'optOutVoicePreference', 'setUltraCustomerOptionsByArray'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('getWholesalePlan')
      ->with()
      ->will($this->returnValue('Plan'));

    $result = new Result();
    $result->succeed();

    $this->customer->expects($this->any())
      ->method('updateUltraInfo')
      ->with()
      ->will($this->returnValue($result));

    $this->customer->expects($this->any())
      ->method('hasCreditCard')
      ->with()
      ->will($this->returnValue(true));

    $this->customer->expects($this->any())
      ->method('setMarketingSettings')
      ->with()
      ->will($this->returnValue([]));

    $result = new Result();
    $result->succeed();

    $this->customer->expects($this->any())
      ->method('optInVoicePreference')
      ->with()
      ->will($this->returnValue($result));

    $result = new Result();
    $result->succeed();

    $this->customer->expects($this->any())
      ->method('optOutVoicePreference')
      ->with()
      ->will($this->returnValue($result));

    $this->api->customer = $this->customer;

    $options = $this->getMockBuilder(Options::class)
      ->setConstructorArgs([])
      ->getMock();

    $options->expects($this->any())
      ->method('setUltraCustomerOptionsByArray')
      ->with()
      ->will($this->returnValue(1));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ])
      ->will($this->returnValue($this->customer));

    $account = new stdClass();
    $account->cos_id = 1234;

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue($account));


    $this->utilities->expects($this->any())
      ->method('getNewActionUUID')
      ->with()
      ->will($this->returnValue('1234'));

    $result = new Result();
    $result->succeed();

    $this->control->expects($this->any())
      ->method('mwMakeitsoUpgradePlan')
      ->will($this->returnValue($result));

    $cardInfo = new stdClass();
    $cardInfo->LAST_FOUR = 1234;

    $this->cardRepo->expects($this->any())
      ->method('getCcInfoFromCustomerId')
      ->will($this->returnValue($cardInfo));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue($result));

    $result = $this->api->portal__SetCustomerPreferences();

// print_r($result);

    $this->assertTrue($result->is_success());
  }
}
