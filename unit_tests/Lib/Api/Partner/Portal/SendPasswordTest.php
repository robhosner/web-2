<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\SendPassword;
use Ultra\Lib\ApiErrors;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\Common;

class SendPasswordTest extends PHPUnit_Framework_TestCase
{
  public $messenger;
  public $utilities;
  public $customerRepo;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * @var SendPassword
   */
  public $api;

  public $apiParams = [
    'msisdn' => '123456789',
    'preferred_language' => '',
  ];

  public function setUp()
  {
    $this->utilities = $this->getMock(Common::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->messenger = $this->getMock(Messenger::class);
    $this->api = new SendPassword($this->customerRepo, $this->messenger, $this->utilities);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testNoCustomerFound()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($this->apiParams['msisdn'], ['u.CUSTOMER_ID'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__SendPassword();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testSMSDeliveryFails()
  {
    $this->api->setInputValues($this->apiParams);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['createTemporaryPassword'])
      ->getMock();

    $customer->expects($this->any())
      ->method('createTemporaryPassword')
      ->will($this->returnValue('test'));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($this->apiParams['msisdn'], ['u.CUSTOMER_ID'])
      ->will($this->returnValue($customer));

    $result = new Result();
    $result->fail();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->with($customer->customer_id, 'temp_password', ['temp_password' => 'test'])
      ->will($this->returnValue($result));

    $result = $this->api->portal__SendPassword();

//    print_r($result);

    $this->assertContains('SM0002', $result->data_array['error_codes']);
  }

  public function testFailedUpdatingPassword()
  {
    $this->api->setInputValues($this->apiParams);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['createTemporaryPassword'])
      ->getMock();

    $customer->expects($this->any())
      ->method('createTemporaryPassword')
      ->will($this->returnValue('test'));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->with($customer->customer_id, 'temp_password', ['temp_password' => 'test'])
      ->will($this->returnValue($result));

    $this->utilities->expects($this->any())
      ->method('encryptPasswordHS')
      ->with('test')
      ->will($this->returnValue('test123'));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($this->apiParams['msisdn'], ['u.CUSTOMER_ID'])
      ->will($this->returnValue($customer));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with($customer->customer_id, ['login_password' => 'test123'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__SendPassword();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->api->setInputValues($this->apiParams);

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['createTemporaryPassword'])
      ->getMock();

    $customer->expects($this->any())
      ->method('createTemporaryPassword')
      ->will($this->returnValue('test'));

    $result = new Result();
    $result->succeed();

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->with($customer->customer_id, 'temp_password', ['temp_password' => 'test'])
      ->will($this->returnValue($result));

    $this->utilities->expects($this->any())
      ->method('encryptPasswordHS')
      ->with('test')
      ->will($this->returnValue('test123'));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($this->apiParams['msisdn'], ['u.CUSTOMER_ID'])
      ->will($this->returnValue($customer));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with($customer->customer_id, ['login_password' => 'test123'])
      ->will($this->returnValue(true));

    $result = $this->api->portal__SendPassword();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
