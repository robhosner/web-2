<?php

use Ultra\Configuration\Configuration;
use Ultra\Lib\Api\Partner;

use Ultra\Lib\Api\Partner\ExternalPayments\SubmitRealtimeReload;

use Ultra\Activations\Repositories\Mssql\ActivationHistoryRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Dealers\Repositories\Mssql\DealersRepository;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\Payment;
use Ultra\Sims\Repositories\Mssql\SimRepository;

class SubmitRealtimeReloadTest extends PHPUnit_Framework_TestCase
{
  private $api;
  protected $dealersRepo;
  protected $customerRepo;
  private $payment;

  public function setUp()
  {
    $this->activationHistoryRepo = $this->getMock(ActivationHistoryRepository::class);
    $this->customerRepo          = $this->getMock(CustomerRepository::class);
    $this->dealersRepo           = $this->getMock(DealersRepository::class);
    $this->simRepo               = $this->getMock(SimRepository::class);
    $this->payment = $this->getMock(Payment::class);
    $this->utilities = $this->getMock(Configuration::class);

    $this->api = new SubmitRealtimeReload(
      $this->activationHistoryRepo,
      $this->customerRepo,
      $this->dealersRepo,
      $this->simRepo,
      $this->payment,
      $this->utilities
    );

    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->payment = $this->payment;
  }

  public function getDefaults()
  {
    // [invalid,valid]
    return [
      'request_epoch'        => '',
      'phone_number'         => '',
      'dealer_code'          => '',
      'store_zipcode'        => '',
      'store_zipcode_extra4' => '',
      'clerk_id'             => '',
      'terminal_id'          => '',
      'product_id'           => '',
      'subproduct_id'        => '',
      'upc'                  => '',
      'load_amount'          => '',
      'provider_trans_id'    => '',
      'provider_name'        => ''
    ];
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function validateProviderNameFail()
  {
    $provider_name = 'INVALID';
    $this->api->validateProvider($provider_name);
  }

  /**
   * @test
   */
  public function validateProviderNamePass()
  {
    $provider_name = 'TCETRA';
    $this->api->validateProvider($provider_name);
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function validateCommisionCanBeFactoredFailInvalidIccid()
  {
    $plan_state    = STATE_PORT_IN_REQUESTED;
    $current_iccid = '123456789123456789';

    // valid SIM
    $this->simRepo->expects($this->any())
      ->method('getSimFromIccid')
      ->with($current_iccid)
      ->will($this->returnValue(FALSE));

    $this->api->validateCommisionCanBeFactored($plan_state, $current_iccid);
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function validateCommisionCanBeFactoredFailInvalidMasterAgent()
  {
    $plan_state    = STATE_PORT_IN_REQUESTED;
    $current_iccid = '123456789123456789';

    $this->payment->expects($this->any())
      ->method('validateFactoredCommission')
      ->with(487)
      ->will($this->returnValue(true));

    // valid SIM
    $this->simRepo->expects($this->any())
      ->method('getSimFromIccid')
      ->with($current_iccid)
      ->will($this->returnValue((object)[
        'INVENTORY_MASTERAGENT' => 487
      ]));

    $this->api->validateCommisionCanBeFactored($plan_state, $current_iccid);
  }

  /**
   * @test
   */
  public function validateCommisionCanBeFactoredPass()
  {
    try
    {
      $plan_state    = STATE_PORT_IN_REQUESTED;
      $current_iccid = '123456789123456789';

      // valid SIM
      $this->simRepo->expects($this->any())
        ->method('getSimFromIccid')
        ->with($current_iccid)
        ->will($this->returnValue((object)[
          'INVENTORY_MASTERAGENT' => 123
        ]));

      $this->api->validateCommisionCanBeFactored($plan_state, $current_iccid);
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
      $this->fail();
    }
  }

  /**
   * @test
   */
  public function verifyUPCFailInvalidUPC()
  {
    $provider_name = 'TCETRA';
    $upc           = '123456';
    $load_amount   = 190;
    $customer_id   = 31;

    try
    {
      $this->payment->expects($this->any())
        ->method('verifyProviderSku')
        ->with($provider_name, $upc, $load_amount)
        ->will($this->returnValue(false));

      $this->api->verifyUPC($provider_name, $upc, $load_amount, $customer_id);
      $this->fail();
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
    }
  }

  /**
   * @test
   */
  public function verifyUPCFailInvalidAmount()
  {
    $provider_name = 'TCETRA';
    $upc           = '1806';
    $load_amount   = 190;
    $subscriber    = (object)[
      'customer_id'            => 31,
      'COS_ID'                 => 98280,
      'MONTHLY_RENEWAL_TARGET' => 'L19'
    ];

    try
    {
      $this->api->verifyUPC($provider_name, $upc, $load_amount, $subscriber);
      $this->fail();
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
    }
  }

  /**
   * @test
   */
  public function verifyUPCPass()
  {
    $provider_name = 'TCETRA';
    $upc           = '3970';
    $load_amount   = 250;
    $subscriber    = (object)[
      'customer_id'            => 31,
      'COS_ID'                 => 98280,
      'MONTHLY_RENEWAL_TARGET' => 'L19'
    ];

    try
    {
      $this->payment->expects($this->any())
        ->method('verifyProviderSku')
        ->with($provider_name, $upc, $subscriber)
        ->will($this->returnValue(true));

      $this->payment->expects($this->any())
        ->method('verifyProviderSkuAmount')
        ->with($provider_name, $upc, $load_amount)
        ->will($this->returnValue('WALLET'));

      $this->payment->expects($this->any())
        ->method('verifyAmountDestination')
        ->with($subscriber, 'WALLET', $load_amount)
        ->will($this->returnValue(true));

      $this->api->verifyUPC($provider_name, $upc, $load_amount, $subscriber);
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
      $this->fail();
    }
  }

  /**
   * @test
   */
  public function logFundingInActivationHistory()
  {
    $customer_id   = 31;
    $provider_name = 'EPAY';
    $load_amount   = '1900';

    // valid SIM
    $this->activationHistoryRepo->expects($this->any())
      ->method('getHistory')
      ->with($customer_id)
      ->will($this->returnValue((object)[
        'FINAL_STATE'    => STATE_ACTIVE,
        'FUNDING_SOURCE' => null
      ]));

    $this->activationHistoryRepo->expects($this->any())
      ->method('logFunding')
      ->with($customer_id, $provider_name, $load_amount / 100)
      ->will($this->returnValue(TRUE));

    $this->api->logFundingInActivationHistory($customer_id, $provider_name, $load_amount);
  }

  /**
   * @test
   */
  public function storeTrackingParamsInRedis()
  {
    $redis = $this->getMock(\Ultra\Lib\Util\Redis::class);
    $redis->expects($this->any())
      ->method('set')
      ->will($this->returnValue(null));

    try
    {
      $provider_trans_id    = 'test_trans_' . time();
      $store_id             = 123;
      $store_zipcode        = '92683';
      $store_zipcode_extra4 = '';
      $clerk_id             = 'test_clerk';
      $terminal_id          = 'test_terminal';
      $source               = 'test_source';
      $detail               = __FUNCTION__;

      $this->api->storeTrackingParamsInRedis(
        $provider_trans_id,
        $store_id,
        $store_zipcode,
        $store_zipcode_extra4,
        $clerk_id,
        $terminal_id,
        $source,
        $detail,
        $redis
      );
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
      $this->fail();
    }
  }
}