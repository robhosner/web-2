<?php

use Ultra\Lib\Api\Partner\Ultrainfo\CalculateTax;
use Ultra\Lib\ApiErrors;

class CalculateTaxTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var CalculateTax
   */
  private $api;

  private $defaultParams = [
    'amount' => 1000,
    'zipCode' => 12345,
    'fees' => 10
  ];

  private $calc;

  public function setUp()
  {
    // API setup
    $this->calc = $this->getMock(SalesTaxCalculator::class, ['setAmount', 'setBillingZipCode', 'setAccount', 'getSalesTax']);
    $this->api = new CalculateTax($this->calc, '1234');
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testGetSalesTaxSuccessful()
  {
    $result = new Result();
    $result->data_array['Successful'] = 'Y';
    $result->data_array['TotalTax'] = 6.2000;
    $result->succeed();
    $this->calc->expects($this->any())
      ->method('getSalesTax')
      ->will($this->returnValue($result));

    $this->api->setInputValues($this->defaultParams);
    $result = $this->api->ultrainfo__CalculateTax();
//    print_r($result);

    $this->assertEquals(620, $result->data_array['tax']);
  }

  public function testGetSalesTaxFails()
  {
    $result = new Result();
    $result->fail();
    $this->calc->expects($this->any())
      ->method('getSalesTax')
      ->will($this->returnValue($result));

    $this->api->setInputValues($this->defaultParams);
    $result = $this->api->ultrainfo__CalculateTax();
//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
    $this->assertContains('Error calculating taxes', $result->data_array['user_errors']);
  }

  public function testGetSalesTaxFindsNoZipCode()
  {
    $result = new Result();
    $result->add_error('Zip Code not found');
    $result->fail();
    $this->calc->expects($this->any())
      ->method('getSalesTax')
      ->will($this->returnValue($result));

    $this->api->setInputValues($this->defaultParams);
    $result = $this->api->ultrainfo__CalculateTax();
//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
    $this->assertContains('No zip code found for ' . $this->defaultParams['zipCode'], $result->data_array['user_errors']);
  }
}
