<?php

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Messaging\FlexMessenger;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\Common;
use Ultra\Utilities\SessionUtilities;

require_once 'classes/LineCredits.php';

class FlexMessengerTest extends PHPUnit_Framework_TestCase
{
  private $customerRepo;
  private $familyApi;
  private $lineCredits;
  private $messenger;
  private $sessionUtils;
  private $commonUtils;
  private $familyRenewalList = [
    664 => [
      'totalSupportedMembers' => 2,
      'parentLineCredits' => 0,
      'parentId' => 34435,
      'parentAutoRecharge' => 0,
      'parentMsisdn' => 123,
      'customerInfo' => [
        [
          'customerId' => 35010,
          'msisdn' => 1233,
          'renewalDate' => 'Jun 14th',
          'renewalTarget' => '',
          'autoRecharge' => 1,
          'lineCredits' => 0,
          'isParent' => false,
          'payEntire' => true,
          'members' => 4,
        ],
        [
          'customerId' => 35012,
          'msisdn' => 12345,
          'renewalDate' => 'Jun 14th',
          'renewalTarget' => '',
          'autoRecharge' => 0,
          'lineCredits' => 0,
          'isParent' => false,
          'payEntire' => true,
          'members' => 4,
        ],
        [
          'customerId' => 34435,
          'msisdn' => 12346,
          'renewalDate' => 'Jun 14th',
          'renewalTarget' => '',
          'autoRecharge' => 0,
          'lineCredits' => 0,
          'isParent' => true,
          'payEntire' => true,
          'members' => 4,
        ]
      ]
    ]
  ];

  private $messageParams;
  private $date;

  /**
   * @var FlexMessenger
   */
  private $flexMessenger;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->lineCredits = $this->getMock(LineCredits::class);
    $this->messenger = $this->getMock(Messenger::class);
    $this->sessionUtils = $this->getMock(SessionUtilities::class);
    $this->commonUtils = $this->getMock(Common::class);

    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));
    $this->familyApi = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([$config,])
      ->setMethods(['get', 'getCustomers'])
      ->getMock();

    $this->flexMessenger = new FlexMessenger(
      $this->customerRepo,
      $this->familyApi,
      $this->lineCredits,
      $this->messenger ,
      $this->sessionUtils,
      $this->commonUtils
    );

    $this->date = new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 2, date("Y")));
    $this->messageParams = [
      'message_type' => '',
      'renewal_date' => 'Jun 14th',
      'login_token' => ''
    ];
  }

  public function testNoRenewalCustomers()
  {
    $this->customerRepo->expects($this->any())
      ->method('getFlexMessagingCustomers')
      ->will($this->returnValue([]));
    $formattedCustomers = $this->flexMessenger->getRenewalCustomers(new DateTime());
    $this->assertEquals([], $formattedCustomers);
  }

  public function testFormattedRenewalCustomers()
  {
    $customers = [
      (object) [
        'customer_id' => 35010,
        'current_mobile_number' => 1233,
        'renewal_date' => 'Jun 14, 2017',
        'monthly_renewal_target' => '',
        'monthly_cc_renewal' => 1,
        'Balance' => 0
      ],
      (object) [
        'customer_id' => 35012,
        'current_mobile_number' => 12345,
        'renewal_date' => 'Jun 14, 2017',
        'monthly_renewal_target' => '',
        'monthly_cc_renewal' => 0,
        'Balance' => 0
      ],
      (object) [
        'customer_id' => 35013,
        'current_mobile_number' => 12345,
        'renewal_date' => 'Jun 14, 2017',
        'monthly_renewal_target' => '',
        'monthly_cc_renewal' => 0,
        'Balance' => 0
      ],
      (object) [
        'customer_id' => 34435,
        'current_mobile_number' => 12346,
        'renewal_date' => 'Jun 14, 2017',
        'monthly_renewal_target' => '',
        'monthly_cc_renewal' => 0,
        'Balance' => 0
      ],
    ];

    $this->customerRepo->expects($this->any())
      ->method('getFlexMessagingCustomers')
      ->will($this->returnValue($customers));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['monthly_cc_renewal' => 0, 'current_mobile_number' => 123]));

    $this->lineCredits->expects($this->any())
      ->method('getBalance')
      ->will($this->returnValue(0));

    $result = new \Result(null, true);
    $result->add_to_data_array('customerIds', [
      [
        "Family_ID" => 664,
        "Parent_ID" => 34435,
        "Customer_ID" => 35010,
        "IsParent" => false,
        "PayEntire" => true,
        "TotalMembers" => 4
      ],
      [
        "Family_ID" => 664,
        "Parent_ID" => 34435,
        "Customer_ID" => 35012,
        "IsParent" => false,
        "PayEntire" => true,
        "TotalMembers" => 4
      ],
      [
        "Family_ID" => 664,
        "Parent_ID" => 34435,
        "Customer_ID" => 34435,
        "IsParent" => true,
        "PayEntire" => true,
        "TotalMembers" => 4
      ]
    ]);

    $this->familyApi->expects($this->any())
      ->method('getCustomers')
      ->will($this->returnValue($result));

    $formattedCustomers = $this->flexMessenger->getRenewalCustomers(new DateTime());

//    print_r($formattedCustomers);

    $this->assertEquals($this->familyRenewalList, $formattedCustomers);
  }

  public function testMessageApproachingRenewDate()
  {
    $this->flexMessenger = $this->getMockBuilder(FlexMessenger::class)
      ->setConstructorArgs([
        $this->customerRepo,
        $this->familyApi,
        $this->lineCredits,
        $this->messenger,
        $this->sessionUtils,
        $this->commonUtils
      ])
      ->setMethods(['setRenewalMessaging'])
      ->getMock();

    $this->flexMessenger->expects($this->at(0))
      ->method('setRenewalMessaging')
      ->with(new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 5, date("Y"))));

    $this->flexMessenger->expects($this->at(1))
      ->method('setRenewalMessaging')
      ->with(new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 2, date("Y"))));

    $this->flexMessenger->setMessageApproachingRenewDate();
  }

  public function testMessageOnRenewDate()
  {
    $this->flexMessenger = $this->getMockBuilder(FlexMessenger::class)
      ->setConstructorArgs([
        $this->customerRepo,
        $this->familyApi,
        $this->lineCredits,
        $this->messenger,
        $this->sessionUtils,
        $this->commonUtils
      ])
      ->setMethods(['setRenewalMessaging'])
      ->getMock();

    $this->flexMessenger->expects($this->at(0))
      ->method('setRenewalMessaging')
      ->with(new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"))));

    $this->flexMessenger->setMessageOnRenewDate();
  }

  public function testParentCloseToExpiration()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34435,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => true,
      'payEntire' => true,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_parent_close_to_expiration';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34435, $this->messageParams, 3);

    $this->flexMessenger->setRenewalMessaging($this->date, false);
  }

  public function testParentExpires()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34435,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => true,
      'payEntire' => true,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_parent_plan_expires_today';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34435, $this->messageParams, 2);

    $this->flexMessenger->setRenewalMessaging($this->date, true);
  }

  public function testMemberCloseToExpireRechargeOff()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34435,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => false,
      'payEntire' => true,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_member_recharge_off_close_to_expiration';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34435, $this->messageParams, 3);

    $this->flexMessenger->setRenewalMessaging($this->date, false);
  }

  public function testMemberExpiresRechargeOff()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34435,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => false,
      'payEntire' => true,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_member_recharge_off_plan_expires_today';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34435, $this->messageParams, 2);

    $this->flexMessenger->setRenewalMessaging($this->date, true);
  }

  public function testMemberExpiresSoonRechargeOn()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['parentAutoRecharge'] = true;
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34435,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => false,
      'payEntire' => true,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_member_recharge_on_close_to_expiration';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34435, $this->messageParams, 3);

    $this->flexMessenger->setRenewalMessaging($this->date, false);
  }

  public function testMemberExpiresSoonPayForFamilyOff()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['parentAutoRecharge'] = null;
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34434,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => false,
      'payEntire' => false,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_member_pay_for_family_off_close_to_expiration';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34434, $this->messageParams, 3);

    $this->flexMessenger->setRenewalMessaging($this->date, false);
  }

  public function testMemberExpiresPayForFamilyOff()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['parentAutoRecharge'] = null;
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34434,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 0,
      'isParent' => false,
      'payEntire' => false,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->messageParams['message_type'] = 'flex_member_pay_for_family_off_plan_expires_today';
    $this->flexMessenger->expects($this->any())
      ->method('enqueueMessage')
      ->with(34434, $this->messageParams, 2);

    $this->flexMessenger->setRenewalMessaging($this->date, true);
  }

  public function testNoMessageToSend()
  {
    $this->setupTemplateTests();
    $this->familyRenewalList[664]['parentAutoRecharge'] = true;
    $this->familyRenewalList[664]['customerInfo'] = [[
      'customerId' => 34434,
      'msisdn' => 12346,
      'renewalDate' => 'Jun 14th',
      'renewalTarget' => '',
      'autoRecharge' => 0,
      'lineCredits' => 1,
      'isParent' => false,
      'payEntire' => false,
      'members' => 4,
    ]];

    $this->flexMessenger->expects($this->any())
      ->method('getRenewalCustomers')
      ->with($this->date)
      ->will($this->returnValue($this->familyRenewalList));

    $this->flexMessenger->expects($this->exactly(0))
      ->method('enqueueMessage');

    $this->flexMessenger->setRenewalMessaging($this->date, true);
  }

  public function setupTemplateTests()
  {
    $this->flexMessenger = $this->getMockBuilder(FlexMessenger::class)
      ->setConstructorArgs([
        $this->customerRepo,
        $this->familyApi,
        $this->lineCredits,
        $this->messenger,
        $this->sessionUtils,
        $this->commonUtils
      ])
      ->setMethods(['getRenewalCustomers', 'enqueueMessage'])
      ->getMock();
  }
}
