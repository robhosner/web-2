<?php

/*
ULTRA_ACC..MAKEITSO_QUEUE

STATUS -- PENDING , FAILED , DONE , TIMEOUT
*/

/**
 * get_stuck_makeitso
 *
 * Get all MAKEITSO_QUEUE rows which should have been processed more than $hours hours ago
 *
 * @param  integer $hours
 * @return object[]
 */
function get_stuck_makeitso($hours=1)
{
  return mssql_fetch_all_objects(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery(
        'MAKEITSO_QUEUE',
        NULL,
        array( 'MAKEITSO_QUEUE_ID' , 'DATEDIFF( mi, NEXT_ATTEMPT_DATE_TIME , getutcdate() ) MINUTES_AGO' ),
        array( 'NEXT_ATTEMPT_DATE_TIME' => 'past_'.$hours.'_hours' , 'STATUS' => 'PENDING' )
      )
    )
  );
}

/**
 * get_makeitso
 * $params: get_next
 *          status
 *          next_attempt_date_time
 *          order_by
 *
 * @param  array $params
 * @param  integer $limit
 * @param  string $attributes
 * @return object[]
 */
function get_makeitso( $params, $limit=NULL, $attributes='*')
{
  $makeitso_rows = array();

  if ( isset($params['get_next']) && $params['get_next'] )
  {
    $limit = 1;
    $params['get_next'] = NULL;
    $params['status']   = 'PENDING';
    $params['next_attempt_date_time'] = 'past';
  }

  $table_name = 'MAKEITSO_QUEUE';

  $order_by = ' MAKEITSO_QUEUE_ID DESC ';

  if ( isset($params['order_by']) )
  {
    $order_by = $params['order_by'];

    unset( $params['order_by'] );
  }

  if ( is_array( $attributes ) )
    $attributes = implode(',',$attributes);

  $sql = \Ultra\Lib\DB\makeSelectQuery($table_name, $limit, array($attributes,'DATEDIFF(d, CREATED_DATE_TIME, GETUTCDATE()) CREATED_DAYS_AGO'), $params, NULL, $order_by, TRUE);

  // sanity check
  if ( !$sql )
    return $makeitso_rows;

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $makeitso_rows = $query_result;

  return $makeitso_rows;
}

/**
 * get_pending_makeitso_by_customer_id
 *
 * There may be more PENDING tasks per customer_id
 *
 * @param  integer $customer_id
 * @return object[]
 */
function get_pending_makeitso_by_customer_id( $customer_id )
{
  if ( ! $customer_id )
    return NULL;

  $params['status']      = 'PENDING';
  $params['order_by']    = 'MAKEITSO_QUEUE_ID DESC';
  $params['customer_id'] = $customer_id;

  return get_makeitso( $params , NULL , array('MAKEITSO_QUEUE_ID','ACTION_UUID','CUSTOMER_ID') );
}

/**
 * get_latest_makeitso_queue_id
 * $params: status
 *          order_by
 *
 * TO BE RETIRED
 *
 * @param  array $params
 * @return integer
 */
function get_latest_makeitso_queue_id( $params )
{
  $makeitso_queue_id = NULL;

  $params['status']   = 'PENDING';
  $params['order_by'] = 'MAKEITSO_QUEUE_ID DESC';

  $query_result = get_makeitso( $params );

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $makeitso_queue_id = $query_result[0]->MAKEITSO_QUEUE_ID;

  return $makeitso_queue_id;
}

/**
 * update_makeitso_fatal_error
 * $params: see update_makeitso
 *          + error
 *
 * @param  array $params
 * @return boolean
 */
function update_makeitso_fatal_error( $params )
{
  $params['status']                 = 'FAILED';
  $params['completed_date_time']    = TRUE;
  $params['next_attempt_date_time'] = 'NULL';
  $params['attempt_history_append'] = ';'.$params['status'].'('.$params['error'].')|'.date_from_pst_to_utc(date("Y-m-d h:i:s"));

  return update_makeitso( $params );
}

/**
 * update_makeitso_delay
 * $params: see update_makeitso
 *          + reason # should contain the reason of the delay
 *          + defer_seconds
 *
 * @param  array $params
 * @return boolean
 */
function update_makeitso_delay( $params )
{
  // min defer seconds
  if ( ( ! $params['defer_seconds'] ) || ( $params['defer_seconds'] < 0 ) || !is_numeric($params['defer_seconds']) )
    $params['defer_seconds'] = 60;

  $params['next_attempt_date_time'] = sprintf( " DATEADD( ss , %d , GETUTCDATE() ) " , $params['defer_seconds'] );

  $params['attempt_history_append'] = ';DELAY('.$params['reason'].')|'.date_from_pst_to_utc(date("Y-m-d h:i:s"));

  return update_makeitso( $params );
}

/**
 * update_makeitso_success
 * $params: see update_makeitso
 * 
 * @param  array $params
 * @return boolean
 */
function update_makeitso_success( $params )
{
  $params['status']                 = 'DONE';
  $params['completed_date_time']    = TRUE;
  $params['next_attempt_date_time'] = 'NULL';
  $params['attempt_history_append'] = ';'.$params['status'].'|'.date_from_pst_to_utc(date("Y-m-d h:i:s"));

  return update_makeitso( $params );
}

/**
 * hide_makeitso
 * hides makeitso entry from miso tool
 *
 * @param  array $params
 * @return boolean
 */
function hide_makeitso( $params )
{
  $params['attempt_history_append'] = ';HIDDEN'.'|'.date_from_pst_to_utc(date("Y-m-d h:i:s"));

  return update_makeitso( $params );
}

/**
 * withdraw_makeitso
 *
 * @param  integer $customer_id
 * @param  string $makeitso_subtype
 * @return boolean
 */
function withdraw_makeitso( $customer_id , $makeitso_subtype=NULL, $makeitso_type_not=NULL )
{
  return update_makeitso(
    array(
      'customer_id'            => $customer_id,
      'makeitso_subtype'       => $makeitso_subtype,
      'next_attempt_date_time' => 'NULL',
      'completed_date_time'    => 'NULL',
      'status_is_pending'      => TRUE,
      'status'                 => 'WITHDRAWN',
      'attempt_history_append' => ';WITHDRAWN|'.date_from_pst_to_utc(date("Y-m-d h:i:s")),
      'makeitso_type_not'      => $makeitso_type_not
    )
  );
}

/**
 * update_makeitso
 * $params: makeitso_queue_id
 *          customer_id
 *          status
 *          attempt_count
 *          attempt_history
 *          attempt_history_append
 *          makeitso_options
 *          created_date_time
 *          status
 *          last_error_code
 *          last_acc_api
 *          next_attempt_date_time
 *          completed_date_time
 *          status_is_pending
 *          status_not_withdrawn
 *          makeitso_subtype
 *
 * @param  array $params
 * @return boolean
 */
function update_makeitso( $params )
{
  // 'makeitso_queue_id' or 'customer_id' must be defined
  if (
      ( !isset($params['makeitso_queue_id']) || !$params['makeitso_queue_id'] )
    &&
      ( !isset($params['customer_id'])       || !$params['customer_id']       )
  )
  {
    dlog('',"Error: makeitso_queue_id or customer_id not provided");
    return FALSE;
  }

  if (isset($params['attempt_count']) && $params['attempt_count'] > 255)
  {
    dlog('','Error: Cannot handle attempt_count greater than 255, makeitso_queue_id: %d', $params['makeitso_queue_id']);

    $fail_params = array(
      'makeitso_queue_id' => $params['makeitso_queue_id'],
      'attempt_count'     => 255,
      'error'             => 'attempt_count greater than 255',
      'last_error_code'   => 'NULL',
      'last_acc_api'      => $params['last_acc_api']
    );

    if (!update_makeitso_fatal_error($fail_params))
      dlog('', 'Error: Failed hard failing MISO on attempt_count greater than 255, makeitso_queue_id: %d', $params['makeitso_queue_id']);

    return FALSE;
  }

  $set_clause = array();

  // 'WITHDRAWN' is not an attempt
  if (!( isset($params['status']) && ( $params['status'] == 'WITHDRAWN' ) ))
    $set_clause = array( " LAST_ATTEMPT_DATE_TIME = GETUTCDATE() " );

  if ( isset($params['attempt_count']) )
    $set_clause[] = sprintf( " ATTEMPT_COUNT    = %d "   , $params['attempt_count'] );

  if ( isset($params['attempt_history']) )
    $set_clause[] = sprintf( " ATTEMPT_HISTORY  = '%s' " , mssql_escape_raw( $params['attempt_history'] ) );

  if ( isset($params['attempt_history_append']) )
    $set_clause[] = sprintf( " ATTEMPT_HISTORY  = CAST(ATTEMPT_HISTORY AS NVARCHAR(MAX)) + '%s' " , mssql_escape_raw( $params['attempt_history_append'] ) );

  if ( isset($params['makeitso_options']) )
    $set_clause[] = sprintf( " MAKEITSO_OPTIONS = '%s' " , $params['makeitso_options'] );

  if ( isset($params['created_date_time']) && ( $params['created_date_time'] == 'NOW' ) )
    $set_clause[] = " CREATED_DATE_TIME = GETUTCDATE() ";

  if ( isset($params['status']) )
    $set_clause[] = sprintf( " STATUS           = %s "   , mssql_escape_with_zeroes($params['status']) );

  if ( isset($params['last_error_code']) && ( $params['last_error_code'] != '' ) )
  {
    $value = ( $params['last_error_code'] == 'NULL' ) ? 'NULL' : mssql_escape_with_zeroes($params['last_error_code']);

    $set_clause[] = sprintf( " LAST_ERROR_CODE  = %s "   , $value );
  }

  if ( isset($params['last_acc_api'])    && ( $params['last_acc_api'] != '' ) )
  {
    $value = ( $params['last_acc_api'] == 'NULL' ) ? 'NULL' : mssql_escape_with_zeroes($params['last_acc_api']);

    $set_clause[] = sprintf( " LAST_ACC_API     = %s "   , $value );
  }

  if ( isset($params['next_attempt_date_time']) && ( $params['next_attempt_date_time'] == 'NULL' ) )
    $set_clause[] = " NEXT_ATTEMPT_DATE_TIME = NULL ";

  if ( isset($params['next_attempt_date_time']) && ( $params['next_attempt_date_time'] != 'NULL' ) )
    $set_clause[] = " NEXT_ATTEMPT_DATE_TIME = ".$params['next_attempt_date_time'];

  if ( isset($params['completed_date_time']) )
    $set_clause[] = ( $params['completed_date_time'] == 'NULL' )
                    ?
                    ' COMPLETED_DATE_TIME    = NULL '
                    :
                    ' COMPLETED_DATE_TIME    = GETUTCDATE() ';

  $table_name = 'MAKEITSO_QUEUE';

  $where_clause = array();

  if ( isset($params['status_is_pending']) && $params['status_is_pending'] )
    $where_clause[] = " STATUS = 'PENDING' ";

  if ( isset($params['status_not_withdrawn']) && $params['status_not_withdrawn'] )
    $where_clause[] = " STATUS != 'WITHDRAWN' ";

  if ( isset($params['makeitso_queue_id']) && $params['makeitso_queue_id'] )
    $where_clause[] = sprintf(" MAKEITSO_QUEUE_ID = %d ",$params['makeitso_queue_id']);

  if ( isset($params['customer_id'])       && $params['customer_id'] )
    $where_clause[] = sprintf(" CUSTOMER_ID       = %d ",$params['customer_id']);

  if ( ! empty($params['makeitso_type_not']) && is_array($params['makeitso_type_not']) )
  {
    $makeitso_type_not = implode(',', array_map("mssql_escape_with_zeroes", $params['makeitso_type_not']));
    $where_clause[] = " MAKEITSO_TYPE NOT IN ($makeitso_type_not) ";
  }

  if ( isset($params['makeitso_subtype']) )
    if ( is_array($params['makeitso_subtype']) && count($params['makeitso_subtype']) )
      $where_clause[] = ' MAKEITSO_SUBTYPE IN (' . implode(',', array_map("mssql_escape_with_zeroes", $params['makeitso_subtype'])) . ')';
    elseif( $params['makeitso_subtype'] )
    {
      if ( preg_match("/\%$/", $params['makeitso_subtype'], $matches) )
        $where_clause[] = " MAKEITSO_SUBTYPE like '".$params['makeitso_subtype']."' ";
      else
        $where_clause[] = sprintf(" MAKEITSO_SUBTYPE = %s " , mssql_escape_with_zeroes($params['makeitso_subtype']) );
    }

  $sql = "
UPDATE $table_name
SET    ".implode(',',$set_clause).'
WHERE  '.implode(' AND ', $where_clause);

  return run_sql_and_check( $sql );
}

/**
 * add_to_makeitso
 * $params: see ultra_acc_makeitso_insert_query
 *
 * @param  array $params
 * @return boolean
 */
function add_to_makeitso( $params )
{
  dlog('', "(%s)", func_get_args());

  $sql = ultra_acc_makeitso_insert_query( $params );

  return run_sql_and_check( $sql );
}

/**
 * ultra_acc_makeitso_insert_query
 * $params: action_uuid
 *          customer_id
 *          makeitso_type
 *          makeitso_options
 *          makeitso_subtype
 *
 * @param  array $params
 * @return string SQL
 */
function ultra_acc_makeitso_insert_query( $params )
{
  $action_uuid            = mssql_escape_with_zeroes($params['action_uuid']);
  $customer_id            = sprintf("%d",$params['customer_id']);
  $created_date_time      = 'GETUTCDATE()';
  $next_attempt_date_time = 'GETUTCDATE()'; // do we want to delay this?
  $makeitso_type          = mssql_escape_with_zeroes($params['makeitso_type']);
  $status                 = "'PENDING'";
  $attempt_history        = "'PENDING|" . date_from_pst_to_utc(date("Y-m-d h:i:s")) . "'";

  $makeitso_options       = '';
  if ( isset($params['makeitso_options']) && $params['makeitso_options'] )
    $makeitso_options     = sprintf("'%s'",$params['makeitso_options']);

  $makeitso_subtype       = 'NULL';
  if ( isset($params['makeitso_subtype']) && $params['makeitso_subtype'] )
    $makeitso_subtype    = mssql_escape_with_zeroes($params['makeitso_subtype']);

  $table_name = 'MAKEITSO_QUEUE';

  return "INSERT INTO $table_name
   (
ACTION_UUID,
CUSTOMER_ID,
CREATED_DATE_TIME,
NEXT_ATTEMPT_DATE_TIME,
MAKEITSO_TYPE,
MAKEITSO_SUBTYPE,
MAKEITSO_OPTIONS,
ATTEMPT_HISTORY,
STATUS
   ) VALUES (
$action_uuid,
$customer_id,
$created_date_time,
$next_attempt_date_time,
$makeitso_type,
$makeitso_subtype,
$makeitso_options,
$attempt_history,
$status
   )";
}

