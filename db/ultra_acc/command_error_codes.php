<?php

/*
DB table COMMAND_ERROR_CODES

All errors should be considered fatal if no info is found in the DB.

UNAVAILABLE_ATTEMPTS    currently unused
ERROR_TYPE              0 == 'unavailable' ; 1 == 'fatal'
*/

/**
 * get_command_error_codes_by_command 
 * @param  string $command
 * @param  object $redis Redis
 * @return array
 */
function get_command_error_codes_by_command( $command , $redis=NULL )
{
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  // check cache first
  $redis_key = 'ultra/command/amdocs/error_codes/'.$command;

  $data = $redis->get( $redis_key );

  if ( $data )
  {
    // data is cached

    $data = json_decode( $data );

    if ( !$data )
      $data = array();
  }
  else
  {
    // data is not cached

    $data = get_command_error_codes_from_db(
      array(
        'command' => $command
      )
    );

    if ( $data && is_array($data) && count($data) )
    {
      $redis->set( $redis_key , json_encode($data) , 300 );
    }
    else
      // no data found
      $data = array();
  }

  return $data;
}

/**
 * get_command_error_codes_from_db 
 * @param  array $params
 * @return object[]
 */
function get_command_error_codes_from_db( $params )
{
  // connect to ULTRA_ACC DB
  \Ultra\Lib\DB\ultra_acc_connect();

  $sql = \Ultra\Lib\DB\makeSelectQuery('COMMAND_ERROR_CODES', NULL, array('*'), $params);

  if ( !$sql )
    return array();

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  // connect to default DB
  teldata_change_db();

  return $data;
}

?>
