<?php

/**
 * add_to_ultra_acc_portin_queue
 *
 * Add a row into ULTRA_ACC..PORTIN_QUEUE
 * $params: see ultra_acc_portin_queue_insert_query
 *
 * @param  array $params
 * @return object Result
 */
function add_to_ultra_acc_portin_queue( $params )
{
  dlog('', '(%s)', func_get_args());

  $sql = ultra_acc_portin_queue_insert_query( $params );

  return run_sql_and_check_result( $sql );
}

/**
 * update_ultra_acc_portin_queue
 *
 * @param  array $paramsSet
 * @param  array $paramsWhere
 * @return object Result
 */
function update_ultra_acc_portin_queue( $paramsSet , $paramsWhere )
{
  dlog('', '(%s)', func_get_args());

  $table_name = 'PORTIN_QUEUE';

  $sql = \Ultra\Lib\DB\makeUpdateQuery( $table_name , $paramsSet , $paramsWhere );

  return run_sql_and_check_result( $sql );
}

/**
 * ultra_acc_portin_queue_insert_query
 *
 * PORTIN_QUEUE INSERT query
 * $params: customer_id
 *          msisdn
 *          port_request_id
 *          due_date_time
 *          last_acc_api
 *          account_number
 *          account_password
 *          account_zipcode
 *          old_service_provider
 *
 * @param  array $params
 * @return string
 */
function ultra_acc_portin_queue_insert_query( $params )
{
  $customer_id      = sprintf("%d",$params['customer_id']);
  $msisdn           = sprintf("%s",mssql_escape_with_zeroes($params['msisdn']));
  $start_date_time  = 'GETUTCDATE()';
  $account_number   = 'NULL';
  $account_password = 'NULL';
  $account_zipcode  = 'NULL';
  $last_acc_api     = 'NULL';
  $port_request_id  = 'NULL';
  $due_date_time    = 'NULL';
  $old_service_provider = 'NULL';

  if ( isset($params['port_request_id']) && $params['port_request_id'] )
    $port_request_id  = sprintf("%s",mssql_escape_with_zeroes($params['port_request_id']));

  if ( isset($params['due_date_time']) && $params['due_date_time'] )
    $due_date_time    = "'".$params['due_date_time']."'";

  if ( isset($params['last_acc_api']) && $params['last_acc_api'] )
    $last_acc_api     = sprintf("%s",mssql_escape_with_zeroes($params['last_acc_api']));

  if ( isset($params['account_number']) && $params['account_number'] )
    $account_number   = sprintf("%s",mssql_escape_with_zeroes($params['account_number']));

  if ( isset($params['account_password']) && $params['account_password'] )
    $account_password = sprintf("%s",mssql_escape_with_zeroes($params['account_password']));

  if ( isset($params['account_zipcode']) && $params['account_zipcode'] )
    $account_zipcode  = sprintf("%s",mssql_escape_with_zeroes($params['account_zipcode']));

  if ( isset($params['old_service_provider']) && $params['old_service_provider'] )
    $old_service_provider = sprintf("%s",mssql_escape_with_zeroes($params['old_service_provider']));

  $table_name = 'PORTIN_QUEUE';

  return "INSERT INTO $table_name
   (
CUSTOMER_ID,
MSISDN,
ACCOUNT_NUMBER,
ACCOUNT_PASSWORD,
ACCOUNT_ZIPCODE,
LAST_ACC_API,
PORT_REQUEST_ID,
DUE_DATE_TIME,
START_DATE_TIME,
PORT_STATUS,
OLD_SERVICE_PROVIDER
   ) VALUES (
$customer_id,
$msisdn,
$account_number,
$account_password,
$account_zipcode,
$last_acc_api,
$port_request_id,
$due_date_time,
$start_date_time,
'SUBMITTED',
$old_service_provider
   )";
}

/**
 * get_portin_queue_by_msisdn
 *
 * return the most recent row from PORTIN_QUEUE given a MSISDN
 *
 * @param string: MSISDN
 * @return object: table row
 * @author: VYT 2014-03-10
 */
function get_portin_queue_by_msisdn($msisdn)
{
  return get_portin_queue(
    array(
      'MSISDN' => $msisdn
    )
  );
}

/**
 * get_portin_queue_by_customer_id
 *
 * return the most recent row from PORTIN_QUEUE given a CUSTOMER_ID
 *
 * @param  integer $customer_id
 * @return object
 */
function get_portin_queue_by_customer_id($customer_id)
{
  return get_portin_queue(
    array(
      'CUSTOMER_ID' => $customer_id
    )
  );
}

/**
 * get_portin_queue
 *
 * return the most recent row from PORTIN_QUEUE
 *
 * @param  array $params
 * @return object
 */
function get_portin_queue( $params )
{
  $table = 'PORTIN_QUEUE';

  // make select query
  $query = \Ultra\Lib\DB\makeSelectQuery(
    $table,  // table
    1,       // limit
    array(
      ' * ',
      ' DATEDIFF( ss , \'1970-01-01\' , PROVSTATUS_DATE ) AS PROVSTATUS_EPOCH ',
      ' DATEDIFF( ss , \'1970-01-01\' , START_DATE_TIME ) AS START_EPOCH ',
      ' DATEDIFF( ss , UPDATED_DATE_TIME , GETUTCDATE())  AS UPDATED_SECONDS_AGO '
    ), // attributes
    $params, // where clause
    NULL,    // having clause
    array('PORTIN_QUEUE_ID DESC')); // order by clause

  // execute query and return result
  return find_first($query);
}

/**
 * cancel_old_port_requests
 *
 * cancel all port request $days from the inital port-in request
 *
 * @param  integer $days
 * @return object Result
 */
function cancel_old_port_requests($days)
{
  return update_ultra_acc_portin_queue(
    array( // SET
      'PORT_STATUS'       => 'TIMEOUT',
      'PORT_ERROR'        => 'TIMEOUT',
      'DUE_DATE_TIME'     => 'NULL',
      'LAST_ACC_API'      => 'NULL',
      'UPDATED_DATE_TIME' => 'NOW'),
    array( // WHERE
      "START_DATE_TIME < DATEADD(DAY, -$days, GETUTCDATE())",
      "PORT_STATUS NOT IN ('CANCELLED', 'COMPLETED', 'COMPLETE', 'TIMEOUT')"));
}

?>
