<?php

/**
 * update_ultra_acc_command_invocations
 * Updates are only possible with a given COMMAND_INVOCATIONS_ID or with STATUS and CUSTOMER_ID
 * $params: command_invocations_id
 *          customer_id
 *          from_status
 *          ticket_details
 *          last_status_updated_by
 *          to_status
 *          acc_transition_id
 *          reported_date_time
 *          completed_date_time
 *          error_date_time
 *          reply_date_time
 *          error_code_and_message
 *          status_history
 *
 * @param  array $params
 * @return string
 */
function update_ultra_acc_command_invocations_query( $params )
{
  $set_clause = array();

  dlog('',"params = %s",$params);

  if ( ! isset($params['command_invocations_id']) && ( !isset($params['customer_id']) || !isset($params['from_status']) ) )
  {
    dlog('',"Not enough parameters");

    return '';
  }

  $where_clause = ( isset($params['command_invocations_id']) )
                  ?
                  sprintf( " COMMAND_INVOCATIONS_ID = %d",$params['command_invocations_id'] )
                  :
                  sprintf( " CUSTOMER_ID = %d AND STATUS = %s ",$params['customer_id'],mssql_escape_with_zeroes($params['from_status']) )
                  ;

  $set_clause = array(
    ' LAST_STATUS_DATE_TIME = GETUTCDATE() '
  );

  if ( isset($params['ticket_details']) )
    $set_clause[] = sprintf( " TICKET_DETAILS         = %s " , mssql_escape_with_zeroes($params['ticket_details']) );

  if ( isset($params['last_status_updated_by']) )
    $set_clause[] = sprintf( " LAST_STATUS_UPDATED_BY = %s " , mssql_escape_with_zeroes($params['last_status_updated_by']) );

  if ( isset($params['to_status']) )
    $set_clause[] = sprintf( " STATUS                 = %s " , mssql_escape_with_zeroes($params['to_status']) );

  if ( isset($params['acc_transition_id']) )
    $set_clause[] = sprintf( " ACC_TRANSITION_ID      = %s " , mssql_escape_with_zeroes($params['acc_transition_id']) );

  if ( isset($params['reported_date_time']) )
    $set_clause[] = sprintf( " REPORTED_DATE_TIME     = %s " , $params['reported_date_time'] );

  if ( isset($params['completed_date_time']) )
    $set_clause[] = sprintf( " COMPLETED_DATE_TIME    = %s " , $params['completed_date_time'] );

  if ( isset($params['error_date_time']) )
    $set_clause[] = sprintf( " ERROR_DATE_TIME        = %s " , $params['error_date_time'] );

  if ( isset($params['reply_date_time']) )
    $set_clause[] = sprintf( " REPLY_DATE_TIME        = %s " , $params['reply_date_time'] );

  if ( isset($params['error_code_and_message']) )
    $set_clause[] = sprintf( " ERROR_CODE_AND_MESSAGE = %s " , mssql_escape_with_zeroes($params['error_code_and_message']) );

  if ( isset($params['status_history']) )
    $set_clause[] = sprintf( " STATUS_HISTORY         = '%s' " , $params['status_history'] );

  $table_name = 'COMMAND_INVOCATIONS';

  return
    " UPDATE $table_name SET ".
    implode(',',$set_clause).
    " WHERE $where_clause";
}

/**
 * update_ultra_acc_command_invocations
 * $params: see update_ultra_acc_command_invocations_query
 * 
 * @param  array $params
 * @return object of class \Result
 */
function update_ultra_acc_command_invocations( $params )
{
  \Ultra\Lib\DB\ultra_acc_connect();

  $sql = update_ultra_acc_command_invocations_query( $params );

  if ( $sql )
    return run_sql_and_check_result( $sql );
  else
    return make_error_Result( 'ERROR: missing command_invocations_id' );
}

/**
 * get_ultra_acc_command_invocations
 *
 * @param  array $params
 * @param  string $limit
 * @param  string $order
 * @param  boolean $nolock
 * @return object[]
 */
function get_ultra_acc_command_invocations( $params, $limit=NULL, $order=NULL, $nolock=TRUE )
{
  $command_invocations = array();

  $table_name = 'COMMAND_INVOCATIONS';

  $sql = \Ultra\Lib\DB\makeSelectQuery($table_name, $limit, array('*','DATEDIFF(hh,START_DATE_TIME,GETUTCDATE()) STARTED_HOURS_AGO'), $params, NULL, $order, $nolock);

  // sanity check
  if ( !$sql )
    return $command_invocations;

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $command_invocations = $query_result;

  return $command_invocations;
}

/**
 * add_to_ultra_acc_command_invocations
 * $params: see ultra_acc_command_invocations_insert_query
 * 
 * @param  array $params
 * @return object of class \Result
 */
function add_to_ultra_acc_command_invocations( $params )
{
  dlog('', '(%s)', func_get_args());

  $sql = ultra_acc_command_invocations_insert_query( $params );

  return run_sql_and_check_result( $sql );
}

/**
 * gone_missing_ultra_acc_command_invocations
 *
 * If a command invocation is OPEN for more than 480 seconds and is not a Port Request, update status to MISSING
 *
 * @return object of class \Result
 */
function gone_missing_ultra_acc_command_invocations()
{
  $table_name = 'COMMAND_INVOCATIONS';

  $sql =
  " UPDATE $table_name
    SET    STATUS   = 'MISSING'
    WHERE  STATUS   = 'OPEN'
    AND    COMMAND != 'PortIn'
    AND    DATEDIFF( ss , START_DATE_TIME , getutcdate() ) > 480
  ";

  return run_sql_and_check_result( $sql );
}

/**
 * ultra_acc_command_invocations_insert_query
 * $params: action_uuid
 *          customer_id
 *          correlation_id
 *          acc_transition_id
 *          acc_api
 *          command
 *          last_status_updated_by
 *          status
 *          ticket_details
 *          reply_date_time
 *          error_date_time
 *          completed_date_time
 *          status_history
 *
 * @param  array $params
 * @return string SQL
 */
function ultra_acc_command_invocations_insert_query( $params )
{
  if ( empty($params['action_uuid']) )
    $params['action_uuid'] = 'no_action_uuid';

  $action_uuid = sprintf("%s",mssql_escape_with_zeroes($params['action_uuid']));
  $customer_id = sprintf("%d",$params['customer_id']);

  $correlation_id = 'NULL';
  if ( isset($params['correlation_id']) && $params['correlation_id'] )
    $correlation_id = sprintf("%s",mssql_escape_with_zeroes($params['correlation_id']));

  $acc_transition_id = 'NULL';
  if ( isset($params['acc_transition_id']) && $params['acc_transition_id'] )
    $acc_transition_id = sprintf("%s",mssql_escape_with_zeroes($params['acc_transition_id']));

  $acc_api = '"UNKNOWN"';
  if ( isset($params['acc_api']) && $params['acc_api'] )
    $acc_api = sprintf("%s",mssql_escape_with_zeroes($params['acc_api']));

  $command = '"UNKNOWN"';
  if ( isset($params['command']) && $params['command'] )
    $command = sprintf("%s",mssql_escape_with_zeroes($params['command']));

  $last_status_updated_by = 'NULL';
  if ( isset($params['last_status_updated_by']) && $params['last_status_updated_by'] )
    $last_status_updated_by = sprintf("%s",mssql_escape_with_zeroes($params['last_status_updated_by']));

  $status = '"OPEN"';
  if ( isset($params['status']) && $params['status'] )
    $status = sprintf("%s",mssql_escape_with_zeroes($params['status']));

  $ticket_details = 'NULL';
  if ( isset($params['ticket_details']) && $params['ticket_details'] )
    $ticket_details = sprintf("%s",mssql_escape_with_zeroes($params['ticket_details']));

  $reply_date_time = 'NULL';
  if ( isset($params['reply_date_time']) && $params['reply_date_time'] )
    $reply_date_time = $params['reply_date_time'];

  $error_date_time = 'NULL';
  if ( isset($params['error_date_time']) && $params['error_date_time'] )
    $error_date_time = $params['error_date_time'];

  $completed_date_time = 'NULL';
  if ( isset($params['completed_date_time']) && $params['completed_date_time'] )
    $completed_date_time = $params['completed_date_time'];

  $status_history = '""';
  if ( isset($params['status_history']) && $params['status_history'] )
    $status_history = sprintf("'%s'",$params['status_history']);

  $table_name = 'COMMAND_INVOCATIONS';

  return "INSERT INTO $table_name
   (
ACTION_UUID,
CUSTOMER_ID,
CORRELATION_ID,
ACC_TRANSITION_ID,
ACC_API,
COMMAND,
LAST_STATUS_UPDATED_BY,
STATUS,
TICKET_DETAILS,
REPLY_DATE_TIME,
ERROR_DATE_TIME,
COMPLETED_DATE_TIME,
STATUS_HISTORY
   ) VALUES (
$action_uuid,
$customer_id,
$correlation_id,
$acc_transition_id,
$acc_api,
$command,
$last_status_updated_by,
$status,
$ticket_details,
$reply_date_time,
$error_date_time,
$completed_date_time,
$status_history
   )";
}

?>
