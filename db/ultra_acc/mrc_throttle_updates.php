<?php

function mrcThrottleUpdatesInsert($customer_id, $plan_tracker_id, $soc_name, $used, $limit)
{
  $sql = "INSERT INTO MRC_THROTTLE_UPDATES (CUSTOMER_ID,PLAN_TRACKER_ID,END_DATE,BUCKET,USED,GRANTED)
          VALUES (%d, %d, GETUTCDATE(),'%s', %d, %d)";
  $sql = sprintf($sql,
    $customer_id,
    $plan_tracker_id,
    $soc_name,
    $used,
    $limit
  );

  return run_sql_and_check($sql);
}

function mrcGetThrottleUpdatesByPlanTrackerId($plan_tracker_id)
{
  $sql = sprintf("SELECT * FROM MRC_THROTTLE_UPDATES with (nolock) WHERE PLAN_TRACKER_ID = %d", $plan_tracker_id);
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

function mrcGetThrottleUpdatesByCustomerId($customer_id)
{
  $sql = sprintf("SELECT * FROM MRC_THROTTLE_UPDATES with (nolock) WHERE CUSTOMER_ID = %d", $customer_id);
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}
