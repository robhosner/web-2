<?php

/**
 * add_activity
 *
 * Add an activity (DB table ACTIVITY)
 *
 * @param  string $activity_name
 * @param  string $username
 * @return Result object
 */
function add_activity( $activity_name , $username='php' )
{
  $result = new Result();

  // check $activity_name
  if (
    mssql_has_rows(
      sprintf(
        " SELECT ACTIVITY_NAME FROM DP_RBAC.ACTIVITY WHERE ACTIVITY_NAME = %s ",
        mssql_escape_with_zeroes( $activity_name )
      )
    )
  )
    return make_error_Result( "Activity name <$activity_name> already in DB." );

  $sql = sprintf("
    INSERT INTO DP_RBAC.ACTIVITY
    (
      ACTIVITY_NAME,
      ACTIVITY_TYPE_ID,
      LAST_MOD_TIMESTAMP,
      LAST_MOD_USERNAME
    )
    VALUES
    (
      %s,
      1,
      GETUTCDATE(),
      %s
    )",
    mssql_escape_with_zeroes( $activity_name ),
    mssql_escape_with_zeroes( $username      )
  );

  return run_sql_and_check_result( $sql );
}

/**
 * remove_role_activity
 *
 * Remove an association between a Role and an Activity from the DB table ROLE_ACTIVITY
 *
 * @param  string $activity_name
 * @param  integer $role_id
 * @return Result object
 */
function remove_role_activity( $activity_name , $role_id )
{
  $result = new Result();

  $sql = sprintf("
    DELETE FROM DP_RBAC.ROLE_ACTIVITY
    WHERE  ROLE_ID     = %d
    AND    ACTIVITY_ID = (
      SELECT ACTIVITY_ID FROM DP_RBAC.ACTIVITY WHERE ACTIVITY_NAME = %s
    )
    ",
    $role_id,
    mssql_escape_with_zeroes( $activity_name )
  );

  return run_sql_and_check_result( $sql );
}

/**
 * add_role_activity
 *
 * Add an association between a Role and an Activity in the DB table ROLE_ACTIVITY
 *
 * @param  string $activity_name
 * @param  integer $role_id
 * @param  integer $active_flag
 * @param  string $username
 * @return Result object
 */
function add_role_activity( $activity_name , $role_id , $active_flag=1 , $username='php' )
{
  $result = new Result();

  // check $activity_name
  if (
    !mssql_has_rows(
      sprintf(
        " SELECT ACTIVITY_NAME FROM DP_RBAC.ACTIVITY WHERE ACTIVITY_NAME = %s ",
        mssql_escape_with_zeroes( $activity_name )
      )
    )
  )
    return make_error_Result( "Activity name <$activity_name> not found." );

  $sql = sprintf("
    INSERT INTO DP_RBAC.ROLE_ACTIVITY
    (
      ROLE_ID,
      ACTIVITY_ID,
      LAST_MOD_TIMESTAMP,
      LAST_MOD_USERNAME,
      ACTIVE_FLAG
    )
    SELECT
      %d,
      ACTIVITY_ID,
      GETUTCDATE(),
      %s,
      %d
    FROM
      DP_RBAC.ACTIVITY
    WHERE
      ACTIVITY_NAME = %s
    ",
    $role_id,
    mssql_escape_with_zeroes( $username ),
    $active_flag,
    mssql_escape_with_zeroes( $activity_name )
  );

  return run_sql_and_check_result( $sql );
}

/**
 * add_api_activity
 *
 * Add an association between an API and an Activity in the DB table API_ACTIVITY
 *
 * @param  string $activity_name
 * @param  string $api_name
 * @param  integer $active_flag
 * @param  string $username
 * @return Result object
 */
function add_api_activity( $activity_name , $api_name , $active_flag=1 , $username='php' )
{
  $result = new Result();

  // check $activity_name
  if (
    !mssql_has_rows(
      sprintf(
        " SELECT ACTIVITY_NAME FROM DP_RBAC.ACTIVITY WHERE ACTIVITY_NAME = %s ",
        mssql_escape_with_zeroes( $activity_name )
      )
    )
  )
    return make_error_Result( "Activity name <$activity_name> not found." );

  // check $api_name
  if (
    !mssql_has_rows(
      sprintf(
        " SELECT API_NAME FROM DP_RBAC.API WHERE API_NAME = %s ",
        mssql_escape_with_zeroes( $api_name )
      )
    )
  )
    return make_error_Result( "API name <$api_name> not found." );

  $sql = sprintf("
    INSERT INTO DP_RBAC.API_ACTIVITY
    (
      API_ID,
      ACTIVITY_ID,
      ACTIVE_FLAG,
      LAST_MOD_TIMESTAMP,
      LAST_MOD_USERNAME
    )
    SELECT
      API_ID,
      ACTIVITY_ID,
      %d,
      GETUTCDATE(),
      %s
    FROM
      DP_RBAC.API,
      DP_RBAC.ACTIVITY
    WHERE
      API_NAME = %s
    AND
      ACTIVITY_NAME = %s
    ",
    $active_flag,
    mssql_escape_with_zeroes( $username ),
    mssql_escape_with_zeroes( $api_name ),
    mssql_escape_with_zeroes( $activity_name )
  );

  return run_sql_and_check_result( $sql );
}

/**
 * deploy_rbac_queries
 * @return string[] SQL
 */
function deploy_rbac_queries()
{
  return array(
'DELETE FROM ULTRA_DATA.DP_RBAC.API_ACTIVITY',
'DELETE FROM ULTRA_DATA.DP_RBAC.PLAN_STATE_ACTIVITY',
'DELETE FROM ULTRA_DATA.DP_RBAC.ROLE_ACTIVITY',
'DELETE FROM ULTRA_DATA.DP_RBAC.ACTIVITY',
'DELETE FROM ULTRA_DATA.DP_RBAC.API',
'Set Identity_Insert DP_RBAC.API ON',
'INSERT INTO DP_RBAC.API (
  API_ID,
  API_NAME,
  API_DESC,
  API_ACTIVE_FLAG,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 )
 SELECT
  API_ID,
  API_NAME,
  API_DESC,
  API_ACTIVE_FLAG,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 FROM
  ULTRA_DEVELOP_TEL.DP_RBAC.API',
'Set Identity_Insert DP_RBAC.API OFF',

'Set Identity_Insert DP_RBAC.ACTIVITY ON',
'INSERT INTO DP_RBAC.ACTIVITY (
  ACTIVITY_ID,
  ACTIVITY_NAME,
  ACTIVITY_TYPE_ID,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 )
 SELECT
  ACTIVITY_ID,
  ACTIVITY_NAME,
  ACTIVITY_TYPE_ID,
  LAST_MOD_TIMESTAMP,
  LAST_MOD_USERNAME
 FROM
  ULTRA_DEVELOP_TEL.DP_RBAC.ACTIVITY',
'Set Identity_Insert DP_RBAC.ACTIVITY OFF',

'INSERT INTO DP_RBAC.ROLE_ACTIVITY (
 ROLE_ID,
 ACTIVITY_ID,
 LAST_MOD_TIMESTAMP,
 LAST_MOD_USERNAME,
 ACTIVE_FLAG
)
SELECT
 devra.ROLE_ID,
 devra.ACTIVITY_ID,
 devra.LAST_MOD_TIMESTAMP,
 devra.LAST_MOD_USERNAME,
 devra.ACTIVE_FLAG
FROM
 ULTRA_DEVELOP_TEL.DP_RBAC.ROLE_ACTIVITY devra',

'INSERT INTO DP_RBAC.PLAN_STATE_ACTIVITY (
 PLAN_STATE_ID,
 ACTIVITY_ID,
 ACTIVE_FLAG,
 LAST_MOD_TIMESTAMP,
 LAST_MOD_USERNAME
)
SELECT
 devpa.PLAN_STATE_ID,
 devpa.ACTIVITY_ID,
 devpa.ACTIVE_FLAG,
 devpa.LAST_MOD_TIMESTAMP,
 devpa.LAST_MOD_USERNAME
FROM
 ULTRA_DEVELOP_TEL.DP_RBAC.PLAN_STATE_ACTIVITY devpa',

'INSERT INTO DP_RBAC.API_ACTIVITY (
 API_ID,
 ACTIVITY_ID,
 ACTIVE_FLAG,
 LAST_MOD_TIMESTAMP,
 LAST_MOD_USERNAME
)
SELECT
 devaa.API_ID,
 devaa.ACTIVITY_ID,
 devaa.ACTIVE_FLAG,
 devaa.LAST_MOD_TIMESTAMP,
 devaa.LAST_MOD_USERNAME
FROM
 ULTRA_DEVELOP_TEL.DP_RBAC.API_ACTIVITY devaa'
  );
}

?>
