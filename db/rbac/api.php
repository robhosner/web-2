<?php

/**
 * can_map_plan_state_to_api
 *
 * Verifies if the given API can be performed for a customer in $plan_state
 *
 * @param  string $plan_state
 * @param  string $api_name
 * @param  object $redis Redis
 * @return boolean
 */
function can_map_plan_state_to_api( $plan_state , $api_name , $redis=NULL )
{
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $can_map_plan_state_to_api = $redis->get('ultra/api/'.$api_name.'/'.$plan_state);

  if ( $can_map_plan_state_to_api )
    return TRUE;

  $can_map_plan_state_to_api = check_map_plan_state_to_api_from_db( $plan_state , $api_name );

  return $can_map_plan_state_to_api;
}

/**
 * check_map_plan_state_to_api_from_db
 *
 * Verifies if the given API can be performed for a customer in $plan_state by checking the RBAC tables in DB
 *
 * @param  string $plan_state
 * @param  string $api_name
 * @param  object $redis Redis
 * @return boolean
 */
function check_map_plan_state_to_api_from_db( $plan_state , $api_name , $redis=NULL )
{
  $can_map_plan_state_to_api = FALSE;

  if ( $plan_state && $api_name )
  {
    dlog('',"plan_state = $plan_state ; api_name = $api_name");

    $sql = sprintf(
      "
      SELECT
        ps.PLAN_STATE_NAME
      FROM
        DP_RBAC.ACTIVITY            ty
      JOIN
        DP_RBAC.API_ACTIVITY        aa ON aa.ACTIVITY_ID   = ty.ACTIVITY_ID
      JOIN
        DP_RBAC.API                 ap ON ap.API_ID        = aa.API_ID
      LEFT OUTER JOIN
        DP_RBAC.PLAN_STATE_ACTIVITY pa ON pa.ACTIVITY_ID   = ty.ACTIVITY_ID
      LEFT OUTER JOIN
        DP_RBAC.PLAN_STATE          ps ON ps.PLAN_STATE_ID = pa.PLAN_STATE_ID
      WHERE
        ap.API_NAME = %s
      " , mssql_escape_with_zeroes( $api_name )
    );

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    dlog('',"data = %s",$data);

    if ( $data && is_array($data) && count($data) )
    {
      $all_null = TRUE;
      $found    = FALSE;

      foreach( $data as $row )
      {
        if (!( is_null( $row->PLAN_STATE_NAME ) || ( $row->PLAN_STATE_NAME == '' ) ))
        {
          $all_null = FALSE;

          if ( $row->PLAN_STATE_NAME == $plan_state )
            $found = TRUE;
        }
      }

      $can_map_plan_state_to_api = ! ! ( $found || $all_null );
    }
    else
      $can_map_plan_state_to_api = TRUE;

    if ( $can_map_plan_state_to_api )
    {
      if ( is_null($redis) )
        $redis = new \Ultra\Lib\Util\Redis;

      $redis->set( 'ultra/api/'.$api_name.'/'.$plan_state , 1 , 60*60*24 );
    }
  }

  return $can_map_plan_state_to_api;
}

/**
 * add_api
 *
 * Add an api (DB table API)
 *
 * @param  string $api_name
 * @param  string $api_desc
 * @param  string $username
 * @return Result object
 */
function add_api( $api_name , $api_desc , $username='php' )
{
  $result = new Result();

  // check for duplicates
  if (
    mssql_has_rows(
      sprintf(
        " SELECT API_NAME FROM DP_RBAC.API WHERE API_NAME = %s ",
        mssql_escape_with_zeroes( $api_name )
      )
    )
  )
    return make_error_Result( "API <$api_name> already in DB." );

  $sql = sprintf("
    INSERT INTO DP_RBAC.API
    (
      API_NAME,
      API_DESC,
      API_ACTIVE_FLAG,
      LAST_MOD_TIMESTAMP,
      LAST_MOD_USERNAME
    )
    VALUES
    (
      %s,
      %s,
      1,
      GETUTCDATE(),
      %s
    )",
    mssql_escape_with_zeroes( $api_name ),
    mssql_escape_with_zeroes( $api_desc ),
    mssql_escape_with_zeroes( $username )
  );

  return run_sql_and_check_result( $sql );
}


/**
 * add_plan_state_activity
 *
 * create a row in DP_RBAC.PLAN_STATE_ACTIVITY based on values of DP_RBAC.ACTIVITY and DP_RBAC.PLAN_STATE
 * @param string activity: DP_RBAC.ACTIVITY.ACTIVITY_NAME
 * @param string plan: DP_RBAC.PLAN_STATE.PLAN_STATE_NAME
 * @return Result object
 * @author: VYT, 2014-02-19
 */
function add_plan_state_activity($activity, $plan)
{
  // default return
  $result = new Result();

  if (! isset($activity) || ! isset($plan))
  {
    $result->add_error('invalid input parameters');
    return $result;
  }

  // verify activity name
  $sql = Ultra\Lib\DB\makeSelectQuery('DP_RBAC.ACTIVITY', NULL, NULL, array('ACTIVITY_NAME' => $activity));
  if ( ! mssql_has_rows($sql))
  {
    $result->add_error("failed to locate ACTIVITY_NAME $activity");
    return $result;
  }

  // verify state name
  $sql = Ultra\Lib\DB\makeSelectQuery('DP_RBAC.PLAN_STATE', NULL, NULL, array('PLAN_STATE_NAME' => $plan));
  if ( ! mssql_has_rows($sql))
  {
    $result->add_error("ERROR: failed to locate PLAN_STATE_NAME $plan");
    return $result;
  }

  // prepare and execute query
  $activity = mssql_escape_with_zeroes($activity);
  $plan = mssql_escape_with_zeroes($plan);
  $sql = "insert into DP_RBAC.PLAN_STATE_ACTIVITY 
    (PLAN_STATE_ID,
    ACTIVITY_ID,
    ACTIVE_FLAG,
    LAST_MOD_TIMESTAMP,
    LAST_MOD_USERNAME)
    SELECT s.PLAN_STATE_ID,
    a.ACTIVITY_ID,
    s.ACTIVE_FLAG,
    getutcdate(),
    'php'
    from DP_RBAC.ACTIVITY a, DP_RBAC.PLAN_STATE s
    WHERE a.ACTIVITY_NAME = $activity and s.PLAN_STATE_NAME = $plan";
  
  return run_sql_and_check_result($sql);
}


/**
 * get_activities_for_plan_state
 *
 * return a list of allowed UI activities for a given customer plan state
 *
 * @param string state: DP_RBAC.PLAN_STATE.PLAN_STATE_NAME
 * @return array of DP_RBAC.ACTIVITY.ACTIVITY_NAME
 * @author: VYT, 2014-02-20
 */
function get_activities_from_plan_state( $plan_state , $redis=NULL )
{
  // check input
  if (empty($plan_state))
  {
    dlog('', 'ERROR: plan state is required');
    return array();
  }

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $activities = $redis->get('ultra/api/activities_by_plan_state/'.$plan_state);

  if ( $activities )
  {
    $activities = json_decode($activities);

    if ( is_array($activities) && count($activities) )
      return $activities;
  }

  return get_activities_from_plan_state_from_db( $plan_state );
}


/**
 * get_activities_from_plan_state_from_db
 *
 * return a list of allowed UI activities for a given customer plan state
 *
 * @param string state: DP_RBAC.PLAN_STATE.PLAN_STATE_NAME
 * @return array of DP_RBAC.ACTIVITY.ACTIVITY_NAME
 * @author: VYT, 2014-02-20
 */
function get_activities_from_plan_state_from_db( $state , $redis=NULL )
{
  // default return
  $activities = array();

  // check input
  if (empty($state))
  {
    dlog('', 'ERROR: invalid input parameter');
    return $activities;
  }

  // prepare and execute query
  $sql = "SELECT a.ACTIVITY_NAME
    FROM DP_RBAC.ACTIVITY a
    LEFT JOIN DP_RBAC.PLAN_STATE_ACTIVITY psa
      ON a.ACTIVITY_ID = psa.ACTIVITY_ID
    LEFT JOIN DP_RBAC.PLAN_STATE s
      ON psa.PLAN_STATE_ID = s.PLAN_STATE_ID
    WHERE
      s.PLAN_STATE_NAME = ".mssql_escape_with_zeroes($state)."
      OR s.PLAN_STATE_NAME IS NULL";

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  // convert array of objects into a flat array
  if ($result && is_array($result))
    foreach ($result as $activity)
      $activities[] = $activity->ACTIVITY_NAME;

  if ( count($activities) )
  {
    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    $redis->set( 'ultra/api/activities_by_plan_state/'.$state , json_encode($activities) , 30*60 ); // 1/2 hour
  }

  return $activities;
}

?>
