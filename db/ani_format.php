<?php

/**
 * ani_format_insert_query
 * 
 * @param  array $params
 * @return boolean
 */
function ani_format_insert(array $params)
{
  // default values
  $defaultValues = [
    'node_name'          => '*',
    'dnis_prefix'        => '*',
    'ani_prefix'         => '*',
    'add_ani_prefix'     => '',
    'description'        => null,
    'origin'             => '*',
    'strip_ani_suffix'   => '',
    'add_ani_suffix'     => '',
    'remove_tags'        => false,
    'priority'           => 5,
    'trim_ani_digits'	   => 0,
    'add_dnis_prefix'    => '',
    'strip_dnis_suffix'  => '',
    'add_dnis_suffix'    => '',
    'trim_dnis_digits'   => 0
  ];
  foreach ($defaultValues as $key => $val)
    if (!array_key_exists($key, $params))
      $params[$key] = $val;

  $query = sprintf(
    'INSERT INTO ANI_FORMAT (
      node_name, dnis_prefix, ani_prefix, add_ani_prefix, description, origin, strip_ani_suffix, add_ani_suffix,
      remove_tags, priority, trim_ani_digits, add_dnis_prefix, strip_dnis_suffix, add_dnis_suffix, trim_dnis_digits
    ) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s, %d )',
    mssql_escape_with_zeroes($params['node_name']),
    mssql_escape_with_zeroes($params['dnis_prefix']),
    mssql_escape_with_zeroes($params['ani_prefix']),
    mssql_escape_with_zeroes($params['add_ani_prefix']),
    mssql_escape_with_zeroes($params['description']),
    mssql_escape_with_zeroes($params['origin']),
    mssql_escape_with_zeroes($params['strip_ani_suffix']),
    mssql_escape_with_zeroes($params['add_ani_suffix']),
    $params['remove_tags'],
    $params['priority'],
    $params['trim_ani_digits'],
    mssql_escape_with_zeroes($params['add_dnis_prefix']),
    mssql_escape_with_zeroes($params['strip_dnis_suffix']),
    mssql_escape_with_zeroes($params['add_dnis_suffix']),
    $params['trim_dnis_digits']
  );
  $result = run_sql_and_check_result($query);
  
  return $result->is_success();
}

/**
 * ani_format_remove_upglobe
 * 
 * Removes the ANI_FORMAT records related to UpGlobe bolt on
 * for a given MSISDN
 *
 * @param  array $msisdn
 * @return boolean
 */
function ani_format_remove_upglobe($msisdn)
{
  $query = sprintf(
    "DELETE FROM ANI_FORMAT
      WHERE ANI_PREFIX IN (%s, %s, %s) AND ORIGIN = '6309'",
    mssql_escape_with_zeroes($msisdn),
    mssql_escape_with_zeroes('1' . $msisdn),
    mssql_escape_with_zeroes('+1' . $msisdn)
  );
  return run_sql_and_check($query);
}

/**
 * ani_format_select_upglobe
 * 
 * Retrieves the ANI_FORMAT records related to UpGlobe bolt on
 * for a given MSISDN
 *
 * @param  array $msisdn
 * @return array
 */
function ani_format_select_upglobe($msisdn)
{
  $query = sprintf(
    "SELECT * FROM ANI_FORMAT
      WHERE ANI_PREFIX IN (%s, %s, %s) AND ORIGIN = '6309'",
    mssql_escape_with_zeroes($msisdn),
    mssql_escape_with_zeroes('1' . $msisdn),
    mssql_escape_with_zeroes('+1' . $msisdn)
  );
  return mssql_fetch_all_objects(logged_mssql_query($query));
}
