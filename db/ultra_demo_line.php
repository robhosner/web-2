<?php

/**
 * ultra_get_demo_lines_for_eligibility_check description
 * retrieve all lines for $dealer_id not in status ('unclaimed','invalid')
 * @param  Integer $dealer_id
 * @return Object results
 */
function ultra_get_demo_lines_for_eligibility_check($dealer_id)
{
  $query = sprintf(
    "SELECT * FROM ULTRA.DEMO_LINE with (nolock)
    WHERE DEALER_ID = %d
    AND STATUS NOT IN ('" . DEMO_STATUS_UNCLAIMED . "','" . DEMO_STATUS_INVALID . "')
    ORDER BY DEMO_LINE_ID ASC",
    $dealer_id
  );

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/**
 * ultra_get_eligible_demo_lines
 * retrieve all eligible demo lines
 * @return  Object results
 */
function ultra_get_eligible_demo_lines()
{
  $query ="SELECT * FROM ULTRA.DEMO_LINE with (nolock)
    WHERE STATUS = '" . DEMO_STATUS_ELIGIBLE . "'
    ORDER BY DEMO_LINE_ID ASC";

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/**
 * ultra_demo_line_get_info
 * detailed demo line info
 *
 * $params contains one of (dealer_code, customer_id, msisdn)
 *
 * @param   Array  $params
 * @return  Object results
 */
function ultra_demo_line_get_info($params)
{
  $declare = NULL;
  $clause  = NULL;

  if ( ! empty($params['dealer_code']))
  {
    $declare = "DECLARE @DEALERCD VARCHAR(8) = '" . $params['dealer_code'] . "';";
    $clause  = 'ds.dealercd = @DEALERCD';
  }
  elseif ( ! empty($params['customer_id']))
  {
    $declare = "DECLARE @CUSTOMER_ID BIGINT = " . $params['customer_id'] . ";";
    $clause  = 'dl.customer_id = @CUSTOMER_ID';
  }
  elseif ( ! empty ($params['msisdn']))
  {
    $declare = "DECLARE @CURRENT_MOBILE_NUMBER VARCHAR(16) = '" . $params['msisdn'] . "';";
    $clause  = 'o.current_mobile_number = @CURRENT_MOBILE_NUMBER';
  }

  if ($declare == NULL || $clause == NULL)
    return NULL;

  $sql = "$declare

    SELECT
    ds.dealercd,
    dl.demo_line_id,
    dl.status AS demo_status,
    dl.activations_needed,
    dl.claim_by_date,
    dl.customer_id,
    o.current_mobile_number,
    co.last_mod_timestamp AS customer_option_inserted,
    co.last_mod_username AS entered_by

    FROM ultra.demo_line dl WITH (NOLOCK)

    JOIN ultra_celluphone_channel..tbldealersite ds WITH (NOLOCK)
    ON dl.dealer_id = ds.dealersiteid

    LEFT JOIN htt_customers_overlay_ultra o WITH (NOLOCK)
    ON o.customer_id = dl.customer_id

    LEFT JOIN ultra.customer_options co WITH (NOLOCK)
    ON co.customer_id = dl.customer_id

    WHERE $clause

    ORDER BY claim_by_date DESC
  ";

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

function ultra_demo_line_update_status($demoLineId, $status)
{
  $query = sprintf("
    DECLARE @DEMO_LINE_ID INT = %d;
    DECLARE @STATUS VARCHAR(16) = '%s';

    UPDATE ULTRA.DEMO_LINE SET STATUS = @STATUS WHERE DEMO_LINE_ID = @DEMO_LINE_ID",
    $demoLineId, $status);

  return run_sql_and_check($query);
}

/**
 * ultra_demo_line_delete_customer_option
 *
 * delete customer option relating to demo line $demoLineId
 *
 * @param  Integer $demoLineId
 * @return Boolean result
 */
function ultra_demo_line_delete_customer_option($demoLineId)
{
  $query = sprintf("
    DECLARE @DEMO_LINE_ID INT = %d;

    DELETE co
    FROM ultra.customer_options co
    INNER JOIN ultra.demo_line dl
    ON co.customer_id = dl.customer_id
    WHERE co.option_attribute = '" . BILLING_OPTION_DEMO_LINE . "'
    AND dl.demo_line_id = @DEMO_LINE_ID
  ", $demoLineId);

  return run_sql_and_check($query);
}

function ultra_demo_line_assign_new_customer($demoLineId, $customerId)
{
  $sql = sprintf("
    DECLARE @DEMO_LINE_ID INT = %d;
    DECLARE @CUSTOMER_ID BIGINT = %d;

    SELECT TOP 1 STATUS FROM ULTRA.DEMO_LINE WHERE DEMO_LINE_ID = @DEMO_LINE_ID AND STATUS = '%s'",
    $demoLineId, $customerId, DEMO_STATUS_ELIGIBLE);
  
  if ( ! $result = find_first($sql))
    return make_error_Result("DEMO_LINE_ID $demoLineId is not an eligible demo line");

  $sql = sprintf("
    DECLARE @DEMO_LINE_ID INT    = %d;
    DECLARE @CUSTOMER_ID  BIGINT = %d;

    UPDATE ULTRA.DEMO_LINE
    SET STATUS = '%s', CUSTOMER_ID = @CUSTOMER_ID
    WHERE DEMO_LINE_ID = @DEMO_LINE_ID AND STATUS = '%s'
  ", $demoLineId, $customerId, DEMO_STATUS_ACTIVE, DEMO_STATUS_ELIGIBLE);

  if ( ! $result = is_mssql_successful(logged_mssql_query($sql)))
    return make_error_Result('FAILED updating ULTRA.DEMO_LINE status for DEMO_LINE_ID ' . $demoLineId);

  return ultra_demo_line_add_missing_customer_option($demoLineId, $customerId);
}

function ultra_demo_line_add_missing_customer_option($demoLineId, $customerId = NULL)
{
  $sql = sprintf("DECLARE @DEMO_LINE_ID INT = %d; SELECT TOP 1 * FROM ULTRA.DEMO_LINE WITH (NOLOCK) WHERE DEMO_LINE_ID = @DEMO_LINE_ID",$demoLineId);

  $demoLineInfo = find_first($sql);
  if (! $demoLineInfo || empty($demoLineInfo->DEALER_ID) || empty($demoLineInfo->CUSTOMER_ID))
    return make_error_Result(__FUNCTION__ . ': FAILED retrieving demo line info for DEMO_LINE_ID ' . $demoLineId);

  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return make_error_Result(__FUNCTION__ . ': failed to connect to dealer portal DB');

  $sql = sprintf("DECLARE @DEALER_ID INT = %d; SELECT TOP 1 * FROM TBLDEALERSITE WITH (NOLOCK) WHERE DEALERSITEID = @DEALER_ID", $demoLineInfo->DEALER_ID);

  $dealerSiteInfo = find_first($sql);
  if (! $dealerSiteInfo || empty($dealerSiteInfo->Dealercd))
    return make_error_Result(__FUNCTION__ . ': FAILED retrieving dealer site info for DEALER_ID ' . $demoLineInfo->DEALER_ID);

  teldata_change_db();

  $customerId = (empty($customerId)) ? $demoLineInfo->CUSTOMER_ID : $customerId;

  $sql = sprintf("
    DECLARE @CUSTOMER_ID BIGINT = %d;
    DECLARE @DEALER_CODE VARCHAR(8) = '%s';

    INSERT INTO ultra.customer_options (CUSTOMER_ID, OPTION_ATTRIBUTE, OPTION_VALUE, LAST_MOD_TIMESTAMP, LAST_MOD_USERNAME)
    VALUES (@CUSTOMER_ID, '%s', @DEALER_CODE, getutcdate(), 'ultra_api')",
    $customerId,
    $dealerSiteInfo->Dealercd,
    BILLING_OPTION_DEMO_LINE
  );

  if ( ! is_mssql_successful(logged_mssql_query($sql)))
    return make_error_Result(__FUNCTION__ . ': FAILED inserting CUSTOMER_OPTION for DEMO_LINE_ID ' . $demoLineId);

  return make_ok_result();
}