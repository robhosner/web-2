<?php

# php component to access DB table htt_billing_history #

# synopsis

/*

echo htt_billing_history_insert_query(
  array(
   "customer_id"=>'10',
   "date"=>'now',
   "cos_id"=>'1234',
   "entry_type"=>'test',
   "stored_value_change"=>'0',
   "balance_change"=>'0',
   "package_balance_change"=>'0',
   "charge_amount"=>'0',
   "reference"=>'TEST',
   "reference_source"=>'',
   "detail"=>'',
   "description"=>'',
   "result"=>'',
   "source"=>'',
   "store_zipcode"=>'2345',
   "store_id"=>'',
   "clerk_id"=>'',
   "terminal_id"=>'',
   "is_commissionable"=>'',
   "commissionable_charge_amount" => 21.99,
   "surcharge_amount" => 29.11
  )
)."\n";

*/

#echo htt_billing_history_select_query(
#  array(
#    'customer_id'       => 10,
#    'entry_type'        => 'A',
#    'balance_change'    => 10,
#    'reference'         => 'C',
#    'reference_source'  => 3,
#    'source'            => 'E',
#    'detail'            => 'Courtesy Wallet Balance Added'
#  )
#)."\n";

/**
 * get_billing_transaction_history
 * $params: start_epoch
 *          end_epoch
 *          customer_id
 *         
 * @param  array $params
 * @return array (errors=>[], billing_transaction_history=>[])
 */
function get_billing_transaction_history($params)
{
  $results = array( 'errors' => array() , 'billing_transaction_history' => array() );

  // MVNO-2261: convert seconds to minutes to prevent MSQL server arithmetic overflow for epoch after year 2050
  $start = $params['start_epoch'] / 60;
  $end = $params['end_epoch'] / 60 + 1;

  $query = sprintf("
    SELECT TOP %d
    DATEDIFF(ss, '1970-01-01', dbo.UTC_TO_PT(TRANSACTION_DATE) ) as history_epoch,
    bh.TRANSACTION_ID   as order_id,
    CASE entry_type
      when 'LOAD'  THEN 'Add'
      when 'SPEND' THEN 'Charge'
      ELSE 'Other'
    END as type,
    CASE source
      when 'SHIP' then 'Shipment'
      when 'EPAY' then 'In Store'
      when 'WEBPIN' then 'Airtime Recharge Card'
      when 'PHONEPIN' then 'Airtime Recharge Card'
      when 'SPEND' then 'Charge'
      when 'CSCOURTESY' then 'Customer Care'
      when 'WEBCC' then 'Credit Card'
      when 'TCETRA' then 'In Store'
      ELSE 'Other'
    END as history_source,
    CASE entry_type
      when 'LOAD' THEN convert(varchar(10),CAST(CHARGE_AMOUNT AS money),1)
      ELSE convert(varchar(10),CAST(BALANCE_CHANGE AS money),1)
    END as AMOUNT,
    description,
    result,
    s.SURCHARGE_HISTORY_ID, s.AMOUNT as surcharge, s.SURCHARGE_TYPE
    FROM HTT_BILLING_HISTORY bh
    LEFT JOIN ULTRA.SURCHARGE_HISTORY s ON bh.TRANSACTION_ID = s.TRANSACTION_ID
    WHERE customer_id = %d",
      empty($params['recent']) ? 50 : $params['recent'],
      $params['customer_id']);

  if ( ! empty($params['start_epoch']) && ! empty($params['end_epoch']))
    $query .= sprintf(" AND TRANSACTION_DATE between dateadd(n, %d, '19700101') and dateadd(n, %d, '19700101') AND SOURCE != 'EPAYPQ' ORDER BY order_id DESC", $start, $end);
  elseif ( ! empty($params['recent']))
    $query .= " AND SOURCE != 'EPAYPQ' ORDER BY TRANSACTION_DATE DESC";
  else // no parameters: this should not happen
    $query = NULL;

  $query_result = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    // remove duplicate rows and append surcharge details to description
    $cleaned = array();
    for ($i = 0, $details = array(), $j = count($query_result); $i < $j; $i++)
    {
      // save surcharge detail
      $row = $query_result[$i];
      if ($row->SURCHARGE_HISTORY_ID)
          $details[] = sprintf('$%.2f %s', $row->surcharge, strtolower($row->SURCHARGE_TYPE));

      // check if we are at the end of result set or current transaction
      if ($i == ($j - 1) || $row->order_id != $query_result[$i + 1]->order_id)
      {
        // apend surcharge details to description
        if (count($details))
        {
          $row->description .= ' (includes ' . implode(', ', $details) . ')';
          unset($details);
          $details = array();
        }
        
        // PROD-442 - if fraud, remove FRAUD_ so FRAUD_F04 becomes the fraud rule (F04)
        if (strpos($row->result, 'FRAUD_') !== FALSE) {
          $row->description = str_replace('FRAUD_', '', $row->result);
        }
        
        // remove surcharge DB data and add row to cleaned
        unset($row->SURCHARGE_HISTORY_ID);
        unset($row->surcharge);
        unset($row->SURCHARGE_TYPE);
        $cleaned[] = $row;
      }
    }
    $results['billing_transaction_history'] = $cleaned;
  }
  else
  {
    $results['errors'][] = "ERR_API_INTERNAL: No data found";
  }
  return $results;
}

/**
 * htt_billing_history_select_query 
 * $params: customer_id
 *          terminal_id
 *          entry_type
 *          balance_charge
 *          charge_amount
 *          result
 *          reference
 *          reference_source
 *          source
 *          description
 *          detail
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_billing_history_select_query($params)
{
  $where_clause = '';
  $and          = '';

  if ( isset($params['customer_id']) )
  {
    $where_clause .= $and.sprintf(" CUSTOMER_ID = %d ",$params['customer_id']);
    $and           = ' AND ';
  }

  if ( isset($params['terminal_id']) )
  {
    $where_clause .= $and.sprintf(" TERMINAL_ID = %s ",mssql_escape_with_zeroes($params['terminal_id']));
    $and           = ' AND ';
  }

  if ( isset($params['entry_type']) )
  {
    $where_clause .= $and.sprintf(" ENTRY_TYPE = %s ",mssql_escape_with_zeroes($params['entry_type']));
    $and           = ' AND ';
  }

  if ( isset($params['balance_change']) )
  {
    $where_clause .= $and.sprintf(" BALANCE_CHANGE = %d ",$params['balance_change']);
    $and           = ' AND ';
  }

  if ( isset($params['charge_amount']) )
  {
    $where_clause .= $and.sprintf(" CHARGE_AMOUNT = %d ",$params['charge_amount']);
    $and           = ' AND ';
  }

  if ( isset($params['result']) )
  {
    $where_clause .= $and.sprintf(" RESULT = %s ",mssql_escape_with_zeroes($params['result']));
    $and           = ' AND ';
  }

  if ( isset($params['reference']) )
  {
    $where_clause .= $and.sprintf(" REFERENCE = %s ",mssql_escape_with_zeroes($params['reference']));
    $and           = ' AND ';
  }

  if ( isset($params['reference_source']) )
  {
    $where_clause .= $and.sprintf(" REFERENCE_SOURCE = %d ",$params['reference_source']);
    $and           = ' AND ';
  }

  if ( isset($params['source']) )
  {
    $where_clause .= $and.sprintf(" SOURCE = %s ",mssql_escape_with_zeroes($params['source']));
    $and           = ' AND ';
  }

  if ( isset($params['description']) )
  {
    $where_clause .= $and.sprintf(" DESCRIPTION = %s ",mssql_escape_with_zeroes($params['description']));
    $and           = ' AND ';
  }

  if ( isset($params['detail']) )
  {
    $where_clause .= $and.sprintf(" DETAIL = %s ",mssql_escape_with_zeroes($params['detail']));
    $and           = ' AND ';
  }

  $query = "
SELECT *
FROM   HTT_BILLING_HISTORY
WHERE  $where_clause";

  return $query;
}

/**
 * htt_billing_history_reference_select 
 * @param  integer $customer_id
 * @param  string $reference
 * @return string SQL
 */
function htt_billing_history_reference_select( $customer_id , $reference )
{
  return sprintf(
"
(
  SELECT TOP 1 TRANSACTION_ID
  FROM   HTT_BILLING_HISTORY
  WHERE  CUSTOMER_ID = %d
  AND    REFERENCE   = %s
  ORDER BY TRANSACTION_ID DESC
)
", $customer_id , mssql_escape_with_zeroes($reference)
  );
}

/**
 * htt_billing_history_reference 
 * @param  integer $customer_id
 * @param  string $reference_source
 * @return string SQL
 */
function htt_billing_history_last_reference_source( $customer_id , $reference_source )
{
  return find_first(sprintf("
    SELECT TOP 1 TRANSACTION_ID
    FROM   HTT_BILLING_HISTORY
    WHERE  CUSTOMER_ID = %d
    AND    REFERENCE_SOURCE = %d
    ORDER BY TRANSACTION_ID DESC
    ",
    $customer_id,
    $reference_source
  ));
}

/**
 * htt_billing_history_insert_query 
 * $params: cc_transactions_id
 *          reference_select
 *          reference
 *          store_zipcode
 *          commissionable_charge_amount
 *          charge_amount
 *          surcharge_amount
 *          customer_id
 *          cos_id
 *          entry_type
 *          stored_value_change
 *          balance_change
 *          package_balance_change
 *          reference_source
 *          detail
 *          description
 *          result
 *          source
 *          source_id 
 *          clerk_id
 *          terminal_id
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_billing_history_insert_query($params)
{
  $date = $params['date'];

  if ( $date == 'now' )
    $date = 'getutcdate()';
  else
    $date = "'".$date."'";

  if ( ! empty($params['cc_transactions_id']) )
    $params['reference_select'] = sprintf(
"
(
  SELECT TOP 1 MERCHANT_TRANSACTION_ID
  FROM   ULTRA.CC_TRANSACTIONS
  WHERE  CC_TRANSACTIONS_ID = %d
)
",
      $params['cc_transactions_id']
    );

  $reference = '';
  if ( isset($params['reference_select']) )
    $reference = $params['reference_select'];
  else
    $reference = mssql_escape_with_zeroes($params['reference']);

  $store_zipcode = "''";
  if ( isset($params["store_zipcode"]) )
  {
    if ( is_numeric($params["store_zipcode"]) )
      $store_zipcode = "'".str_pad($params["store_zipcode"], 5, "0", STR_PAD_LEFT)."'";
    else
      $store_zipcode = null_or_mssql_escape_string($params,"store_zipcode");
  }

  if ( ! isset( $params["commissionable_charge_amount"] ) )
    $params["commissionable_charge_amount"] = $params["charge_amount"];

  if ( ! isset( $params["surcharge_amount"] ) )
    $params["surcharge_amount"] = 0;

  $output = empty($params['output_identity']) ? NULL : 'OUTPUT INSERTED.TRANSACTION_ID';

  $query = sprintf(
"INSERT INTO htt_billing_history
 (
   customer_id,
   transaction_date,
   cos_id,
   entry_type,
   stored_value_change,
   balance_change,
   package_balance_change,
   charge_amount,
   reference,
   reference_source,
   detail,
   description,
   result,
   source,
   store_zipcode,
   store_id,
   clerk_id,
   terminal_id,
   is_commissionable,
   COMMISSIONABLE_CHARGE_AMOUNT,
   SURCHARGE_AMOUNT
 )
 $output
 VALUES
 (
   %d,
   %s,
   %d,
   %s,
   %s,
   %s,
   %s,
   %s,
   %s,
   %d,
   %s,
   %s,
   %s,
   %s,
   %s,
   %s,
   %s,
   %s,
   %d,
   %s,
   %s
 )
",
 $params["customer_id"],
 $date,
 $params["cos_id"],
 mssql_escape($params["entry_type"]),
 $params["stored_value_change"],
 $params["balance_change"],
 $params["package_balance_change"],
 $params["charge_amount"],
 $reference,
 $params["reference_source"],
 mssql_escape($params["detail"]),
 mssql_escape($params["description"]),
 mssql_escape($params["result"]),
 mssql_escape($params["source"]),
 $store_zipcode,
 null_or_mssql_escape_string($params,"store_id"),
 null_or_mssql_escape_string($params,"clerk_id"),
 null_or_mssql_escape_string($params,"terminal_id"),
 $params["is_commissionable"],
 $params["commissionable_charge_amount"],
 $params["surcharge_amount"]
  );

  return $query;
}

/**
 * get_current_cycle_purchase_history
 * return rows from HTT_BILLING_HISTORY since the beginning of subscriber's current cycle
 * @see MVNO-2592
 * 
 * @param Integer customer_id
 * @param String entry_type (HTT_BILLING_HISTORY.ENTRY_TYPE)
 * @param String description (HTT_BILLING_HISTORY.DESCRIPTION)
 * @return Array of Objects or NULL on failure
 */
function get_current_cycle_purchase_history($customer_id, $entry_type, $description = NULL)
{
  if (empty($customer_id) || ! in_array($entry_type, array('SPEND')))
    return logError('invalid arguments: ' . jsonEncode(func_get_args()));

  $descriptionClause = NULL;
  if ($description)
  {
    if ( ! in_array($description, array('DATA Purchase')))
      return logError("invalid description $description");
    $descriptionClause = sprintf('AND h.DESCRIPTION = ' . mssql_escape_with_zeroes($description));
  }

  $query = sprintf("SELECT * FROM HTT_BILLING_HISTORY h WITH (NOLOCK)
    JOIN HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK) ON h.CUSTOMER_ID = u.CUSTOMER_ID
    WHERE h.TRANSACTION_DATE > CAST(u.PLAN_STARTED AS DATE)
      AND h.CUSTOMER_ID = %d
      AND h.ENTRY_TYPE = %s
      $descriptionClause",
      $customer_id,
      mssql_escape_with_zeroes($entry_type));

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/**
 * returns most current MSISDN_REPLACEMENT row for customer_id
 *
 * @param  Integer customer_id
 * @param  Integer days_ago
 * @return Array of Objects or NULL on failure
 */
function htt_billing_history_get_recent_msisdn_replacement($customer_id, $days_ago)
{
  $date = date('Y-m-d H:i:s', strtotime("-$days_ago days"));
  $sql = "SELECT TOP 1 * FROM HTT_BILLING_HISTORY with (nolock)
          WHERE entry_type = 'MSISDN_REPLACEMENT'
          AND TRANSACTION_DATE > '$date'
          AND CUSTOMER_ID = $customer_id
          ORDER BY TRANSACTION_ID DESC";
  return find_first($sql);
}