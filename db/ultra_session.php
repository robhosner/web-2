<?php

/**
 * get_celluphone_distributor
 * @param  integer $user_id
 * @return array $distributor_info
 */
function get_celluphone_distributor( $user_id )
{
  $distributor_info = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("
SELECT
 c.user_id,
 c.user_name,
 a.masterid         distributor,
 a.businessname     distributor_name,
 a.parentmasterid   masteragent,
 a.mastercd         master_dealer_code, -- for masters and subdistributors this is the DEALER_CODE
 a.businessname     master_store_name,  -- for masters and subdistributors this is the STORE_NAME
 a.MasterId         master_id,          -- for masters and subdistributors this is the MASTER_ID
 a.ParentMasterID   parent_master_id    -- for masters and subdistributors this is the PARENT_MASTER_ID
FROM
 ".$db."..tblmaster a,
 ".$db."..tblusermaster b,
 ".$db."..tbl_User c
WHERE a.masterid     = b.masterid
AND   b.userid       = c.user_id
AND   a.masterid    != a.parentmasterid
AND   is_active_flag = 1
AND   expirationdt IS NULL
AND   ( user_expiration_dt > GETDATE() or user_expiration_dt IS NULL )
AND   b.userid       = %d", $user_id );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $distributor_info = $data[0];

  return $distributor_info;
}

/**
 * get_celluphone_masteragent
 * @param  integer $user_id
 * @return array $masteragent_info
 */
function get_celluphone_masteragent( $user_id )
{
  $masteragent_info = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("
SELECT
 c.user_id,
 c.user_name,
 a.masterid         masteragent,
 a.businessname     master_name,
 a.mastercd         master_dealer_code, -- for masters and subdistributors this is the DEALER_CODE
 a.businessname     master_store_name,  -- for masters and subdistributors this is the STORE_NAME
 a.MasterId         master_id,          -- for masters and subdistributors this is the MASTER_ID
 a.ParentMasterID   parent_master_id    -- for masters and subdistributors this is the PARENT_MASTER_ID
FROM
 ".$db."..tblmaster     a WITH (NOLOCK) ,
 ".$db."..tblusermaster b WITH (NOLOCK) ,
 ".$db."..tbl_User      c WITH (NOLOCK)
WHERE a.masterid     = b.masterid
AND   b.userid       = c.user_id
AND   a.masterid     = a.parentmasterid
AND   is_active_flag = 1
AND   expirationdt IS NULL
AND   ( user_expiration_dt > GETDATE() OR user_expiration_dt IS NULL )
AND   b.userid       = %d", $user_id );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $masteragent_info = $data[0];

  return $masteragent_info;
}

/**
 * get_session_info_from_user_id
 * @param  integer $user_id
 * @return array $session_info
 */
function get_session_info_from_user_id( $user_id )
{
  $session_info = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("
SELECT
 r.OrglevelID       ORG_LEVEL_ID,
 r.access_role_name ROLE_NAME,
 l.Descr            ROLE_DESCRIPTION,
 s.DealerSiteID     DEALER,
 d.MasterID         DISTRIBUTOR,
 d.MasterID         DEALER_MASTER_ID, -- for dealers this is the MASTER_ID
 d.Dealercd         DEALER_CODE,
 d.BusinessName     STORE_NAME, -- warning, it may contain unicode characters
 m.ParentMasterID   MASTERAGENT,
 m.businessname     BUSINESSNAME,
 m.mastercd         MASTER_DEALER_CODE, -- for masters and subdistributors this is the DEALER_CODE
 m.businessname     MASTER_STORE_NAME,  -- for masters and subdistributors this is the STORE_NAME
 m.MasterId         MASTER_ID,          -- for masters and subdistributors this is the MASTER_ID
 m.ParentMasterID   PARENT_MASTER_ID    -- for masters and subdistributors this is the PARENT_MASTER_ID
FROM
 ".$db."..tbl_user u
LEFT OUTER JOIN
 ".$db."..tbl_access_role r
ON
 u.access_role_id = r.access_role_id
LEFT OUTER JOIN
 ".$db."..tblOrgLevel l
ON
 l.OrgLevelID = r.OrgLevelID
LEFT OUTER JOIN
 ".$db."..tblUserDealerSite s
ON
 s.UserID = u.user_id
LEFT OUTER JOIN
 ".$db."..tblDealerSite d
ON
 d.DealerSiteID = s.DealerSiteID
LEFT OUTER JOIN
 ".$db."..tblMaster m
ON
 m.MasterID = d.MasterID
WHERE
 u.user_id = %d", $user_id );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
  {
    $session_info = $data[0];

    // encode as UTF-8 to avoid json_encode errors
    $session_info->STORE_NAME = utf8_encode( $session_info->STORE_NAME );
  }

  return $session_info;
}

/**
 * get_session_user_dealer_info
 * @param  integer $user_id
 * @return array $user_dealer_info
 */
function get_session_user_dealer_info( $user_id )
{
  $user_dealer_info = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("
select *
from  ".$db."..tbl_user u
inner join ".$db."..tblUserDealerSite uds
      on u.user_id = uds.UserID
inner join ".$db."..tblDealerSite ds
      on uds.DealerSiteID = ds.DealerSiteID
where u.user_id = %d",
  $user_id );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $user_dealer_info = $data[0];

  return $user_dealer_info;
}

/**
 * get_session_user_master_info
 * @param  integer $user_id
 * @return array $user_master_info
 */
function get_session_user_master_info( $user_id )
{
  $user_master_info = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("
select *
from  ".$db."..tbl_user u
inner join ".$db."..tblUserMaster um
      on u.user_id = um.UserID
inner join ".$db."..tblMaster m
      on um.MasterID = m.MasterID
where u.user_id = %d",
  $user_id );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $user_master_info = $data[0];

  return $user_master_info;
}

/**
 * get_org_level
 * @return array $org_level
 */
function get_org_level()
{
  $org_level = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = " SELECT * FROM ".$db."..tblOrgLevel ";

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $org_level = $data[0];

  return $org_level;
}

/**
 * get_session_user_info
 * @param  integer $user_id
 * @return array $user_info
 */
function get_session_user_info( $user_id )
{
  $user_info = NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("
select u.user_id
,      u.first_name
,      u.last_name
,      u.access_role_id
,      ar.access_role_name
,      ol.OrgLevelID       org_level_id
,      ol.Descr            description
from  ".$db."..tbl_user u
inner join
      ".$db."..tbl_access_role ar
      on
      u.access_role_id = ar.access_role_id
inner join 
      ".$db."..tblOrgLevel ol
      on ol.OrgLevelID = ar.OrglevelID
where u.user_id = %d",
  $user_id );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $user_info = $data[0];

  return $user_info;
}

/**
 * get_php_user_log
 * Queries tblPHPUserLog (replicated DB) or CAPTNJACK.UV.dbo.tblPHPUserLog (Live via Linked Server)
 * 
 * @param  string $session_id
 * @return array $php_user_log
 */
function get_php_user_log( $session_id )
{
  // avoid SQL injections - apparently function mssql_escape_with_zeroes does not work with SessionID
  $session_id = preg_replace("/[\'\"\;\:\.]/", '', $session_id);

  $php_user_log = get_php_user_log_replicated_db( $session_id );

  if ( !$php_user_log )
  {
    dlog('','no celluphone session found in replicated DB - '.\Ultra\UltraConfig\celluphoneDb());

    if ( ! \Ultra\UltraConfig\isDevelopmentDB() )
    {
      dlog('','attempting Linked Server query');

      $sql = set_mssql_options();

      dlog('',$sql);

      if ( ! run_sql_and_check_result($sql) )
      {
        dlog('','cannot set mssql options');

        return $php_user_log;
      }

      $php_user_log = get_php_user_log_linked_server( $session_id );

      if ( !$php_user_log )
        dlog('','no celluphone session found in Linked Server');
    }
  }

  return $php_user_log;
}

/**
 * get_php_user_log_replicated_db
 * Queries tblPHPUserLog (replicated DB) by SessionID
 * 
 * @param  string $session_id
 * @return array php_user_log
 */
function get_php_user_log_replicated_db( $session_id )
{
  $db = \Ultra\UltraConfig\celluphoneDb();

  return get_php_user_log_from_db( $session_id , $db.'.' );
}

/**
 * get_php_user_log_linked_server
 * Queries CAPTNJACK.UV.dbo.tblPHPUserLog (Live via Linked Server) by SessionID
 * 
 * @param  string $session_id
 * @return array php_user_log
 */
function get_php_user_log_linked_server( $session_id )
{
  return get_php_user_log_from_db( $session_id , 'CAPTNJACK.UV.dbo' );
}

/**
 * get_php_user_log_from_db
 * Queries tblPHPUserLog by SessionID
 * 
 * @param  string $session_id
 * @param  string $db
 * @return array php_user_log
 */
function get_php_user_log_from_db( $session_id , $db )
{
  $php_user_log = NULL;

  $sql = sprintf("
SELECT ID,
       Guid,
       UserID,
       LoginTime,
       SessionID
FROM   ".$db.".tblPHPUserLog
WHERE  SessionID = '%s' ",
    $session_id
  );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    $php_user_log = $data[0];

  return $php_user_log;
}

/**
 * append_activation_activity_celluphone
 * Appends data from celluphone DB to activation activity report
 *
 * @param  array $data
 * @return array appended $data
 */
function append_activation_activity_celluphone( $data )
{
  if ( $data && count( $data ) )
  {
    $redis_celluphone = new \Ultra\Lib\Util\Redis\Celluphone();

    $n = count($data);

    for ( $i = 0; $i < $n ; $i++ )
    {
/*
1 - Master Agent: This is the name of the master agent. It comes from the businessname column on tblmaster.
2 - Distrbutor Name: It comes from the businessname column on tblmaster. It is obtained by joining htt_inventory_sim.inventory_distributor to tblmaster.masterid
3 - Retailer (INV): It comes from the dealercd column on tblDealerSite. It is obtained by joining htt_inventory_sim.inventory_dealer to tbldealer.dealerID.
4 - Retailer (ACT): It comes from the dealercd column on tblDealerSite. It is obtained by joining htt_activation_history.dealer to tbldealer.dealerID. NOTE: THE DIFFERENT JOIN VS. RETAILER(INV).

1 - get from cached info
2 - get from cached info
3 - DEALER_PORTAL..tblDealerSite.DealerSiteID == HTT_INVENTORY_SIM.inventory_dealer
4 - DEALER_PORTAL..tblDealerSite.DealerSiteID == ULTRA.HTT_ACTIVATION_HISTORY.dealer
*/
      $data[ $i ]->master_agent_name   = $redis_celluphone->getMasterName( $data[ $i ]->INVENTORY_MASTERAGENT );
      $data[ $i ]->distrbutor_name     = $redis_celluphone->getMasterName( $data[ $i ]->INVENTORY_DISTRIBUTOR );
      $data[ $i ]->retailer_inventory  = $redis_celluphone->getDealerCDById( $data[ $i ]->INVENTORY_DEALER );
      $data[ $i ]->retailer_activation = $redis_celluphone->getDealerCDById( $data[ $i ]->DEALER );
    }
  }

  return $data;
}

/**
 * get_celluphone_dealer_site
 * Returns celluphone dealer site from given dealer_id or dealer_cd
 * 
 * @param  integer  $dealer_id
 * @param  string  $dealercd
 * @param  boolean $allow_cache
 * @param  object  $redis
 * @return array $celluphone_dealer_site
 */
function get_celluphone_dealer_site( $dealer_id = NULL, $dealercd = NULL, $allow_cache = FALSE, $redis = NULL)
{
  if (is_null($dealer_id) && is_null($dealercd))
    return '';

  $celluphone_dealer_site = '';

  if ($allow_cache)
  {
    $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis\Celluphone() : $redis;
    $celluphone_dealer_site = (!is_null($dealer_id)) ? $redis->getDealerSiteByDealerId($dealer_id) : $redis->getDealerSiteByDealerCD($dealercd);
    $celluphone_dealer_site = (!is_null($celluphone_dealer_site)) ? json_decode($celluphone_dealer_site) : NULL;
  }

  if (!$celluphone_dealer_site)
  {
    list ($where, $is) = (!is_null($dealer_id)) ? array('DealerSiteID', $dealer_id) : array('Dealercd', $dealercd);

    $db = \Ultra\UltraConfig\celluphoneDb();

    $sql = sprintf("
      SELECT
        DealerSiteID,
        BusinessName,
        Dealercd,
        MasterID
      FROM
        ".$db."..tblDealerSite
      WHERE
        %s = '%s' ",
      $where,
      $is
    );

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( $data && is_array( $data ) && count( $data ) )
      $celluphone_dealer_site = $data[0];

    if ($allow_cache)
    {
      if (!is_null($dealer_id))
        $redis->setDealerSiteByDealerId($dealer_id, json_encode($celluphone_dealer_site));
      else
        $redis->setDealerSiteByDealerCD($dealercd, json_encode($celluphone_dealer_site));
    }
  }

  return $celluphone_dealer_site;
}

/**
 * get_celluphone_master
 * Returns a tblMaster row from MasterID ( which can be a master ID or a distributor ID )
 *
 * @param  integer $id, MasterID
 * @return array $celluphone_master
 */
function get_celluphone_master( $id )
{
  $celluphone_master = '';

  if ( $id )
  {
    $db = \Ultra\UltraConfig\celluphoneDb();

    $sql = sprintf("
SELECT
  MasterID,
  BusinessName,
  MasterCd,
  ParentMasterID
FROM
 ".$db."..tblmaster a
WHERE
  MasterID = %d ",
      $id
    );

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( $data && is_array( $data ) && count( $data ) )
      $celluphone_master = $data[0];
  }

  return $celluphone_master;
}


/**
 * get_all_celluphone_masters
 * return all parent master agents
 *
 * @return array all celluphone masters data
 */
function get_all_celluphone_masters()
{
  $db = \Ultra\UltraConfig\celluphoneDb();
  $sql = "SELECT MasterID, BusinessName from $db..tblMaster WITH (NOLOCK) WHERE masterID = ParentMasterID";
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


/**
 * search_dealers_by_business_name
 * return all active dealer records matching part of BusinessName
 * since we cannot use msssq_escape with LIKE clause, be sure that $business_name contains alphanumeric and spaces only
 * @see MVNO-1441, customercare__SearchDealers
 *
 * @param string $business_name
 * @return array dealers data
 */
function search_dealers_by_business_name($business_name)
{
  // replace all non-alphanumerics in $business_name with wildcards to prevent SQL injection
  $business_name = preg_replace('/[^a-zA-Z0-9 \&]+/', '_', $business_name);

  // first attempt: exact match
  $db = \Ultra\UltraConfig\celluphoneDb();
  $sql = sprintf("SELECT aa.BusinessName as business_name, aa.Dealercd as dealer_code,
      cc.Address as address, cc.City as city, cc.Zip as zip, cc.State as state
    FROM $db..tblDealerSite aa WITH (NOLOCK)
    JOIN $db..tblDealerSiteAddress bb WITH (NOLOCK) ON aa.DealerSiteID = bb.dealerSiteID
    JOIN $db..tblAddress cc WITH (NOLOCK) on bb.AddressID = cc.AddressID
    WHERE aa.ActiveFlag = 1 AND aa.BusinessName LIKE '%s%%'
    ORDER BY business_name", $business_name);
  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  // second attempt if nothing found: loose match of first 3 characters
  if (! count($result))
  {
    $sql = sprintf("SELECT aa.BusinessName as business_name, aa.Dealercd as dealer_code,
      cc.Address as address, cc.City as city, cc.Zip as zip, cc.State as state
    FROM $db..tblDealerSite aa WITH (NOLOCK)
    JOIN $db..tblDealerSiteAddress bb WITH (NOLOCK) ON aa.DealerSiteID = bb.dealerSiteID
    JOIN $db..tblAddress cc WITH (NOLOCK) on bb.AddressID = cc.AddressID
    WHERE aa.ActiveFlag = 1 AND aa.BusinessName LIKE '%s%%'
    ORDER BY business_name", substr($business_name, 0, 3));
    $result = mssql_fetch_all_objects(logged_mssql_query($sql));
  }

  return $result;
}

/**
 * get_dealer_from_code
 * get dealer record tblDealerSite by dealer code (Dealercd)
 * @see MVNO-1441
 *
 * @param  string $code
 * @return array dealer data
 */
function get_dealer_from_code($code)
{
  $code = trim($code);
  if (empty($code))
  {
    dlog('', 'ERROR: empty parameter code');
    return NULL;
  }

  $db = \Ultra\UltraConfig\celluphoneDb();
  $sql = sprintf("SELECT TOP 1 * FROM $db..tblDealerSite WITH (NOLOCK) WHERE Dealercd = '%s'", $code);
  $dealer = mssql_fetch_all_objects(logged_mssql_query($sql));

  if (count($dealer))
    return $dealer[0];
  else
    return NULL;
}


 /**
 * getDealerInfoFromDb
 * WARNING: do not use this function, instead use getDealerInfo()
 * get various dealer details either from dealer code or ID
 * @param string dealer code
 * @param int dealer ID
 * @return object or NULL on failure
 */
function getDealerInfoFromDb($dealer_code = NULL, $dealer_id = NULL)
{
  if ( ! $dealer_code && ! $dealer_id)
  {
    dlog('', 'ERROR: empty parameters');
    return NULL;
  }

  /* we can have two possibilities when getting dealer's master and distributor:
      dealer has a distributor and distributor has a master: dealer <-> distributor <-> master
      dealer has only master (no distributor): dealer <-> master
    both distributor and master are stored in the same table - tblMaster,
    we distinquish distributor from master by comparing values of MasterID ParentMasterID:
      MasterID == ParentMasterID: this is a master, no distributor exists
      MasterID != ParentMasterID: this is a distributor, we find master by looking up tblMaster again where MasterID = ParentMasterID
  */
  $db = \Ultra\UltraConfig\celluphoneDb();
  $sql = "SELECT TOP 1
    d.DealerSiteID as dealerId,
    d.Dealercd as dealerCode,
    d.BusinessName as dealerName,
    s.MasterID as distributorId,
    s.BusinessName as distributorName,
    m.MasterID as masterId,
    m.BusinessName as masterName
    FROM $db..tblDealerSite d WITH (NOLOCK)
    LEFT JOIN $db..tblMaster s WITH (NOLOCK) ON d.MasterID = s.MasterID
    LEFT JOIN $db..tblMaster m WITH (NOLOCK) ON s.ParentMasterID = m.MasterID
    WHERE " . ($dealer_id ? sprintf('d.DealerSiteID = %d', $dealer_id) : sprintf("d.Dealercd = '%s'", $dealer_code));
  $info = mssql_fetch_all_objects(logged_mssql_query($sql));

  // remove redundant master record if distributor is missing
  if (count($info))
  {
    $info = $info[0];
    if (empty($info->masterId) || $info->distributorId == $info->masterId)
    {
      $info->masterId = $info->distributorId;
      $info->distributorId = NULL;
      $info->masterName = $info->distributorName;
      $info->distributorName = NULL;
    }
  }
  else
    $info = NULL;
  return $info;
}
