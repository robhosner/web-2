<?php

include_once('db.php');

# php component to access DB table htt_country_sms_carriers #

# synopsis

#echo htt_country_sms_carriers_select_query( array('carrier'=>'c') )."\n";
#echo htt_country_sms_carriers_select_query( array('country'=>'g') )."\n";
#echo htt_country_sms_carriers_select_query( array() )."\n";

/**
 * htt_country_sms_carriers_select_query
 * $params: country
 *          carrier
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_country_sms_carriers_select_query($params)
{
  $where = array();

  if ( isset($params['country']) )
  {
    $where[] = "country = ".mssql_escape($params["country"]);
  }

  if ( isset($params['carrier']) )
  {
    $where[] = "carrier = ".mssql_escape($params["carrier"]);
  }

  $where_clause = "";

  if ( count($where) > 0 )
  {
    $where_clause = ' WHERE '.implode(" AND ", $where);
  }

  $query = " SELECT COUNTRY , CARRIER COLLATE SQL_Latin1_General_CP1253_CI_AI FROM htt_country_sms_carriers $where_clause ORDER BY COUNTRY,CARRIER";

  return $query;
}

/**
 * htt_country_sms_carriers_get_country_list 
 * @return object[]
 */
function htt_country_sms_carriers_get_country_list()
{
  $query = htt_country_sms_carriers_select_query(array());

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

?>
