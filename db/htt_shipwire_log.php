<?php


include_once('db.php');
include_once 'lib/util-common.php';


# php component to access DB table htt_shipwire_log #

# synopsis

#echo htt_shipwire_log_insert_query(
#  array(
#    "order_id" => '1',
#    "order_number" => '2',
#    "order_status" => '3',
#    "transaction_id" => '4',
#    "shipwire_status" => '5',
#    "warehouse" => '6',
#    "service" => '7',
#    "cost" => '8',
#    "code" => '9',
#    "quantity" => 10,
#  )
#)."\n\n";

#echo htt_shipwire_log_select_query(
#  array(
#    'customer_id' => 123,
#    'sql_limit'   => 1,
#    'order_by'    => 'ORDER_NUMBER DESC',
#    'last_days'   => 5
#  )
#)."\n\n";

#$i = generate_shipsim_sub_order_id(123);

/**
 * generate_shipsim_sub_order_id 
 * @param  integer $customer_id
 * @return string $sub_order_id
 */
function generate_shipsim_sub_order_id($customer_id)
{
  $sub_order_id = '00';

  dlog('',"generate_shipsim_sub_order_id sub_order_id = $sub_order_id");

  $query = "SELECT top 1 ORDER_NUMBER from HTT_SHIPWIRE_LOG where CUSTOMER_ID = $customer_id order by ORDER_ID DESC";

  # example: 01

  $results = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $results && is_array($results) && $results[0] && $results[0]->ORDER_NUMBER )
  {
     dlog('',"generate_shipsim_sub_order_id previous value = ".$results[0]->ORDER_NUMBER);

    $characters = str_split($results[0]->ORDER_NUMBER);

    if ( is_numeric( $characters[ (count($characters)-1) ] ) )
    {
      if ( is_numeric( $characters[ (count($characters)-2) ] ) )
      {
        $sub_order_id = $characters[ (count($characters)-2) ] . $characters[ (count($characters)-1) ];
      }
      else
      {
        $sub_order_id = $characters[ (count($characters)-1) ];
      }

      $sub_order_id = ( (int)$sub_order_id ) + 1;

      if ( $sub_order_id < 10 )
      {
        $sub_order_id = "0".$sub_order_id;
      }
    }
  }

  dlog('',"generate_shipsim_sub_order_id sub_order_id = $sub_order_id");

  return $sub_order_id;
}

/**
 * htt_shipwire_log_select_query 
 * $params: last_days
 *          customer_id
 * @param  array $params
 * @return string SQL
 */
function htt_shipwire_log_select_query($params)
{
  $top_clause      = get_sql_top_clause($params);
  $order_by_clause = get_sql_order_by_clause($params);

  $additional_clauses = "";

  if ( isset($params['last_days']) )
  {
    $additional_clauses .= " AND DATEDIFF(dd , LOG_DATE, GETUTCDATE() ) <= ".$params['last_days'];
  }

  $query = "
SELECT $top_clause *
FROM   HTT_SHIPWIRE_LOG
WHERE  CUSTOMER_ID = ".$params["customer_id"].
  " $additional_clauses ".
  " $order_by_clause    ";

  return $query;
}

/**
 * htt_shipwire_log_insert_query
 * $params: order_id
 *          order_number
 *          order_status
 *          transaction_id
 *          shipwire_status
 *          warehouse
 *          service
 *          cost
 *          code
 *          quantity
 *          customer_id
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_shipwire_log_insert_query($params)
{
  # fix $params["service"]
  $params["service"] = preg_replace('/[\n\r\s]+/'," ", $params["service"]);

  $input_params = array(
    "order_id", "order_number", "order_status", "transaction_id", "shipwire_status", "warehouse", "service", "cost", "code", "quantity", "customer_id"
  );

  foreach($input_params as $input_p)
  { dlog("","htt_shipwire_log_insert_query $input_p = ".$params[$input_p]); }

  $query = sprintf(
    "INSERT INTO HTT_SHIPWIRE_LOG
(
  ORDER_ID,
  ORDER_NUMBER,
  ORDER_STATUS,
  TRANSACTION_ID,
  SHIPWIRE_STATUS,
  WAREHOUSE,
  SERVICE,
  COST,
  CODE,
  QUANTITY,
  CUSTOMER_ID
)
VALUES
(
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %d,
  %d
)",
    mssql_escape($params["order_id"]),
    mssql_escape($params["order_number"]),
    mssql_escape($params["order_status"]),
    mssql_escape($params["transaction_id"]),
    mssql_escape($params["shipwire_status"]),
    mssql_escape($params["warehouse"]),
    mssql_escape($params["service"]),
    mssql_escape($params["cost"]),
    mssql_escape($params["code"]),
    $params["quantity"],
    $params["customer_id"]
  );

  return $query;
}

?>
