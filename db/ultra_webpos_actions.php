<?php

require_once 'Ultra/Lib/Util/Redis/WebPos.php';

/*
[ULTRA].[WEBPOS_ACTIONS]
*/

/**
 * ultra_webpos_actions_get_transaction
 * @param  String $uuid
 * @return Object||False transaction row object
 */
function ultra_webpos_actions_get_transaction($uuid)
{
  $sql = sprintf('
    DECLARE @UUID  VARCHAR(128) = \'%s\';
    SELECT TOP 1 * FROM ULTRA.WEBPOS_ACTIONS WITH (NOLOCK) 
    WHERE UUID = @UUID
    ORDER BY WEBPOS_ACTIONS_ID DESC',
    $uuid
  );

  return find_first($sql);
}

/**
 * ultra_webpos_actions_update_transaction_status
 * @param  String $ultra_payment_trans_id
 * @param  String $status
 * @return BOOL
 */
function ultra_webpos_actions_update_transaction_status($ultra_payment_trans_id, $status)
{
  $dateset = '';
  switch ($status)
  {
    case 'VOID': $dateset = ', VOIDED = GETUTCDATE() '; break;
    case 'DONE': $dateset = ', CLOSED = GETUTCDATE() '; break;
  }

  $sql = sprintf("
    DECLARE @UUID   VARCHAR(128) = '%s';
    DECLARE @STATUS VARCHAR(32)  = '%s';
    UPDATE ULTRA.WEBPOS_ACTIONS SET STATUS = @STATUS
    $dateset
    WHERE UUID = @UUID
    ",
    $ultra_payment_trans_id,
    $status
  );

  return run_sql_and_check_result($sql);
}

/**
 * ultra_webpos_actions_insert
 * create a new row in ULTRA.WEBPOS_ACTIONS
 * @param Array key => values
 * @return transaction UUID on success, NULL otherwise
 */
function ultra_webpos_actions_insert($params)
{
  // generate UUID
  $params['UUID'] = create_guid(__FUNCTION__);

  // prepare and execute INSERT
  if ( ! $sql = Ultra\Lib\DB\makeParameterizedInsertStatement('ULTRA.WEBPOS_ACTIONS', $params))
    return logError('failed to prepare INSERT statement');
  if ( ! is_mssql_successful(logged_mssql_query($sql)))
    return logError('failed to execute INSERT statement');

  // success
  return $params['UUID'];
}


/**
 * ultra_webpos_actions_select
 * select rows with given criteria
 * @param Array name => value
 * @return Array of Objects
 */
function ultra_webpos_actions_select($params)
{
  if ( ! $sql = Ultra\Lib\DB\makeParameterizedSelectStatement('ULTRA.WEBPOS_ACTIONS', NULL, NULL, $params))
    return logError('failed to prepare SELECT statement');

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


/**
 * get_webpos_customer
 * get possibly cached Web POS customer record
 * this function is called multipe times by Ultra\Lib\MiddleWare\Adapter\NotificationPortStatusUpdate
 * @param Integer customer ID
 * @return Object ULTRA.WEBPOS_ACTIONS row on success, NULL on failure
 */
function get_webpos_customer($customerId)
{
  $customer = NULL;

  // try getting from cache first
  $cache = new Ultra\Lib\Util\Redis\WebPos;

  if ( ! $customer = $cache->getCustomer($customerId))
  {
    // try getting from DB
    $data = ultra_webpos_actions_select(array('CUSTOMER_ID' => $customerId));

    // cache for susequent queries
    if ( ! empty($data))
    {
      $customer = $data[0];
      $cache->setCustomer($customer);
    }
  }

  return $customer;
}
