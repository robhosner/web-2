<?php

include_once('db.php');

# php component to access DB table htt_action_parameter_log #

# synopsis

#echo htt_action_parameter_log_insert_query(
#  'id',
#  'p',
#  'v'
#);

/**
 * htt_action_parameter_log_insert_query 
 * @param  string $action_uuid
 * @param  string $param
 * @param  string $val
 * @return string SQL
 */
function htt_action_parameter_log_insert_query($action_uuid,$param,$val)
{
  $query = sprintf("INSERT INTO htt_action_parameter_log
( ACTION_UUID, PARAM, VAL )
VALUES ( '%s', '%s', %s );",
               $action_uuid,
               $param,
               mssql_escape(json_encode(array($val))));

  return $query;
}

/**
 * htt_action_parameter_log_insert_multiple_query
 * @param  array ($action_uuid, $param, $val)
 * @return string SQL
 */
function htt_action_parameter_log_insert_multiple_query(array $values)
{
  $inserts = '';
  $i = 0;
  foreach ($values as $param => $val)
  {
    $string = sprintf("('%s', '%s', %s)",
      $action_uuid,
      $param,
      mssql_escape(json_encode(array($val)))
    );

    if ($i) $inserts .= ',';
    else $i++;

    $inserts .= $string;
  }

  $query = "INSERT INTO htt_action_parameter_log (ACTION_UUID, PARAM, VAL) VALUES $inserts";

  return $query;
}

/**
 * log_action_parameter 
 * @param  string $action_uuid
 * @param  string $param
 * @param  string $val
 * @return boolean
 */
function log_action_parameter($action_uuid, $param, $val)
{
  $q = htt_action_parameter_log_insert_query( $action_uuid, $param, $val );

  return run_sql_and_check($q);
}

/**
 * get_action_parameter 
 * @param  string $action
 * @param  string $param
 * @return mixed || NULL
 */
function get_action_parameter($action, $param)
{
  if (NULL == $action) return NULL;

  if (NULL == $param) return NULL;

  $p = get_by_column('htt_action_parameter_log',
                     array('action_uuid' => $action->ACTION_UUID,
                           'param' => $param));

  if (NULL != $p)
  {
    return disclose_JSON($p->VAL);
  }

  return NULL;
}

?>
