<?php

/*
[ULTRA].[CC_HOLDER_TOKENS]
*/

/**
 * ultra_cc_holder_tokens_disable_query
 * $params: bin
 *          last_four
 *          expires_date
 *
 * @param  array $params
 * @return string
 */
function ultra_cc_holder_tokens_disable_query( $params )
{
  return sprintf("
  UPDATE     ULTRA.CC_HOLDER_TOKENS
  SET        ENABLED            = 0
  FROM       ULTRA.CC_HOLDER_TOKENS t
  INNER JOIN ULTRA.CC_HOLDERS       h
  ON         h.CC_HOLDERS_ID    = t.CC_HOLDERS_ID
  WHERE      h.CUSTOMER_ID      = %d
  AND NOT    (
    h.BIN               = %s AND
    h.LAST_FOUR         = %s AND
    h.EXPIRES_DATE      = %s AND
    t.CC_PROCESSORS_ID  = %d
  )
  AND        t.ENABLED          = 1",
    $params['customer_id'],
    mssql_escape_with_zeroes($params['bin']),
    mssql_escape_with_zeroes($params['last_four']),
    mssql_escape_with_zeroes($params['expires_date']),
    $params['cc_processor_id']
  );
}

/**
 * disable_ultra_cc_holder_tokens
 *
 * Disable all ULTRA.CC_HOLDER_TOKENS rows for the given $customer_id
 *
 * @param  integer $customer_id
 * @return boolean
 */
function disable_ultra_cc_holder_tokens( $customer_id )
{
  $sql = sprintf("
  UPDATE     ULTRA.CC_HOLDER_TOKENS
  SET        ENABLED            = 0
  FROM       ULTRA.CC_HOLDER_TOKENS t
  INNER JOIN ULTRA.CC_HOLDERS       h
  ON         h.CC_HOLDERS_ID    = t.CC_HOLDERS_ID
  WHERE      h.CUSTOMER_ID      = %d
  AND        t.ENABLED          = 1",
    $customer_id
  );

  return run_sql_and_check( $sql );
}

/**
 * ultra_cc_holder_tokens_insert_from_customer_id_query
 * $params: bin
 *          last_four
 *          expires_date
 *          gateway
 *          merchant_account
 *          token
 *
 * @param  integer $customer_id
 * @param  array $merchantData kva
 * @param  array $params
 * @return string
 */
function ultra_cc_holder_tokens_insert_from_customer_id_query( $customer_id , $merchantData , $params )
{
  $clientIP = Session::getClientIp();

  return sprintf("
IF EXISTS (
  SELECT t.CC_HOLDER_TOKENS_ID
  FROM   ULTRA.CC_HOLDER_TOKENS t
  JOIN   ULTRA.CC_HOLDERS       h
  ON     h.CC_HOLDERS_ID    = t.CC_HOLDERS_ID
  WHERE  h.BIN              = %s
  AND    h.LAST_FOUR        = %s
  AND    h.EXPIRES_DATE     = %s
  AND    h.CUSTOMER_ID      = %d
  AND    t.GATEWAY          = %s
  AND    t.MERCHANT_ACCOUNT = %s
  AND    t.TOKEN            = %s
  AND    t.CC_PROCESSORS_ID  = %d
)

BEGIN

  UPDATE     ULTRA.CC_HOLDER_TOKENS
  SET        ENABLED            = 1,
             CUSTOMER_IP        = '%s'
  FROM       ULTRA.CC_HOLDER_TOKENS t
  INNER JOIN ULTRA.CC_HOLDERS       h
  ON         h.CC_HOLDERS_ID    = t.CC_HOLDERS_ID
  WHERE      h.BIN              = %s
  AND        h.LAST_FOUR        = %s
  AND        h.EXPIRES_DATE     = %s
  AND        h.CUSTOMER_ID      = %d
  AND        t.GATEWAY          = %s
  AND        t.MERCHANT_ACCOUNT = %s
  AND        t.TOKEN            = %s
  AND        t.CC_PROCESSORS_ID  = %d

END

  ELSE

BEGIN

  INSERT INTO ULTRA.CC_HOLDER_TOKENS
(
  CC_HOLDERS_ID,
  GATEWAY,
  MERCHANT_ACCOUNT,
  TOKEN,
  CC_PROCESSORS_ID,
  ENABLED,
  CUSTOMER_IP
)
SELECT
  h.CC_HOLDERS_ID,
  %s,
  %s,
  %s,
  %d,
  1,
  '%s'
FROM  ULTRA.CC_HOLDERS h
WHERE h.CUSTOMER_ID  = %d
AND   h.BIN          = %s
AND   h.LAST_FOUR    = %s
AND   h.EXPIRES_DATE = %s
AND   h.ENABLED      = 1

END

  ",
    mssql_escape_with_zeroes($params['bin']),
    mssql_escape_with_zeroes($params['last_four']),
    mssql_escape_with_zeroes($params['expires_date']),
    $customer_id,
    mssql_escape_with_zeroes($merchantData['gateway']),
    mssql_escape_with_zeroes($merchantData['merchant_account']),
    mssql_escape_with_zeroes($merchantData['token']),
    $merchantData['cc_processor_id'],
    $clientIP,
    mssql_escape_with_zeroes($params['bin']),
    mssql_escape_with_zeroes($params['last_four']),
    mssql_escape_with_zeroes($params['expires_date']),
    $customer_id,
    mssql_escape_with_zeroes($merchantData['gateway']),
    mssql_escape_with_zeroes($merchantData['merchant_account']),
    mssql_escape_with_zeroes($merchantData['token']),
    $merchantData['cc_processor_id'],
    mssql_escape_with_zeroes($merchantData['gateway']),
    mssql_escape_with_zeroes($merchantData['merchant_account']),
    mssql_escape_with_zeroes($merchantData['token']),
    $merchantData['cc_processor_id'],
    $clientIP,
    $customer_id,
    mssql_escape_with_zeroes($params['bin']),
    mssql_escape_with_zeroes($params['last_four']),
    mssql_escape_with_zeroes($params['expires_date'])
  );
}

/**
 * ultra_cc_holder_tokens_insert_query
 * $params: cc_holders_id
 *          gateway
 *          merchant_account
 *          token
 *
 * @param  array $params
 * @return string
 */
function ultra_cc_holder_tokens_insert_query( $params )
{
  return sprintf("
IF NOT EXISTS (
  SELECT CC_HOLDERS_ID
  FROM   ULTRA.CC_HOLDER_TOKENS
  WHERE  CC_HOLDERS_ID    = %d
  AND    GATEWAY          = %s
  AND    MERCHANT_ACCOUNT = %s
  AND    TOKEN            = %s
)
BEGIN
  INSERT INTO ULTRA.CC_HOLDER_TOKENS
(
  CC_HOLDERS_ID,
  GATEWAY,
  MERCHANT_ACCOUNT,
  TOKEN,
  ENABLED,
  CUSTOMER_IP
)
VALUES
(
  %d,
  %s,
  %s,
  %s,
  1,
  '%s'
)
END
  ",
  $params['cc_holders_id'],
  mssql_escape_with_zeroes($params['gateway']),
  mssql_escape_with_zeroes($params['merchant_account']),
  mssql_escape_with_zeroes($params['token']),
  $params['cc_holders_id'],
  mssql_escape_with_zeroes($params['gateway']),
  mssql_escape_with_zeroes($params['merchant_account']),
  mssql_escape_with_zeroes($params['token']),
  Session::getClientIp()
  );
}

/**
 * add_to_ultra_cc_holder_tokens
 * Add a row into [ULTRA].[CC_HOLDER_TOKENS]
 * $params: see ultra_cc_holder_tokens_insert_query
 *
 * @param  array $params
 * @return boolean result of SQL INSERT
 */
function add_to_ultra_cc_holder_tokens( $params )
{
  dlog('', '(%s)', func_get_args());

  return run_sql_and_check( ultra_cc_holder_tokens_insert_query( $params ) );
}


/**
 * get_cc_info_and_tokens_by_customer_id
 * retrieve enabled rows from ULTRA.CC_HOLDERS and ULTRA.CC_HOLDER_TOKENS for given CUSTOMER_ID
 * used to confirm that customer has a credit card on file
 * @param int CUSTOMER_ID
 * @param bool ULTRA.CC_HOLDER_TOKENS.ENABLED or NULL to omit the clause
 * @param bool ULTRA.CC_HOLDER_TOKENS.ENABLED or NULL to omit the clause
 * @param bool nolock = 'WITH (NOLOCK)'
 * @return array of objects of DB rows or NULL on failure
 */
function get_cc_info_and_tokens_by_customer_id($customer_id, $cc_holders_enabled = TRUE, $cc_holder_tokens_enabled = TRUE, $nolock = FALSE)
{
  if (! is_numeric($customer_id))
  {
    dlog('', 'WARNING: invalid parameter customer_id');
    return FALSE;
  }

  $holders = $cc_holders_enabled === NULL ? NULL : ('AND h.ENABLED = ' . ($cc_holders_enabled ? 1 : 0));
  $tokens = $cc_holder_tokens_enabled === NULL ? NULL : ('AND t.ENABLED = ' . ($cc_holder_tokens_enabled ? 1 : 0));
  $lock = $nolock ? 'WITH (NOLOCK)' : NULL;

  $sql = sprintf("SELECT * FROM ULTRA.CC_HOLDERS h $lock
    JOIN ULTRA.CC_HOLDER_TOKENS t $lock ON h.CC_HOLDERS_ID = t.CC_HOLDERS_ID
    WHERE h.CUSTOMER_ID = %d $holders $tokens", $customer_id);

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_cc_info_and_tokens_by_customer_id
 *
 * @param Integer $customer_id
 * @return Boolean successful?
 */
function disable_ultra_cc_holder_tokens_by_customer_id($customer_id)
{
  if ( ! is_numeric($customer_id))
  {
    dlog('', 'WARNING: invalid parameter customer_id');
    return FALSE;
  }

  $sql = sprintf("UPDATE ULTRA.CC_HOLDER_TOKENS SET ENABLED = 0 WHERE CC_HOLDERS_ID IN 
      (SELECT CC_HOLDERS_ID FROM ULTRA.CC_HOLDERS WITH (nolock) WHERE CUSTOMER_ID = %d)", $customer_id);
  return is_mssql_successful(logged_mssql_query($sql));
}
