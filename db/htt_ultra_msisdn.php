<?php

require_once 'Ultra/Lib/DB/Schema.php';

# php component to access DB table htt_ultra_msisdn #

# synopsis

# $success = add_to_htt_ultra_msisdn( 123 , '1000100100', 1, 'xxx' );

/*
echo htt_ultra_msisdn_insert_query(
  array(
    'msisdn'               => '7776665555',
    'msisdn_activated'     => 1,
    'last_changed_date'    => 'getutcdate()',
    'created_by_date'      => 'getutcdate()',
    'customer_id'          => 1212,
    'expires_date'         => 'getutcdate()',
    'last_transition_uuid' => 'xyz',
    'mvne'                 => 1
  )
)."\n\n";
*/

#echo htt_ultra_msisdn_update_query(
#  array(
#    'msisdn_activated'     => 1,
#    'msisdn'               => '1000100100',
#    'replaced_by_msisdn'   => '2000100100',
#    'last_transition_uuid' => 'xyz'
#  )
#)."\n\n";


/**
 * htt_ultra_msisdn_update_query
 * SQL UPDATE statement for HTT_ULTRA_MSISDN
 * $params: LAST_TRANSITION_UUID
 *          REPLACED_BY_MSISDN
 *          MVNE
 *          msisdn_activated
 *          msisdn
 *
 * @param  array $params
 * @return string
 */
function htt_ultra_msisdn_update_query($params)
{
  $set_clause_array = array(
    ' LAST_CHANGED_DATE = getutcdate() '
  );

  $string_attributes = array('LAST_TRANSITION_UUID','REPLACED_BY_MSISDN','MVNE');

  foreach( $string_attributes as $field )
    if ( isset($params[ strtolower($field) ]) )
      $set_clause_array[] = sprintf(" $field = %s ",mssql_escape_with_zeroes($params[ strtolower($field) ]));

  if ( isset($params['msisdn_activated']) )
    $set_clause_array[] = sprintf(" MSISDN_ACTIVATED = %d ",$params['msisdn_activated']);

  $set_clause   = ' SET '.implode(' , ',$set_clause_array);

  $where_clause = sprintf(" MSISDN = %s ",mssql_escape_with_zeroes($params['msisdn']));

  $query =
    "UPDATE HTT_ULTRA_MSISDN
     $set_clause
     WHERE $where_clause";

  return $query;
}

/**
 * htt_ultra_msisdn_update_query_by_customer_id
 *
 * create query string for UPDATE HTT_ULTRA_MSISDN
 * column names with type = 'S' will be escaped
 * strings that need to be enclosed in '' (such as dates) should be passed already enclosed
 *
 * @param array of parameters (e.g: name => value || !name => value)
 * @return string SQL query
 * @author: VYT 14-02-12
 */
function htt_ultra_msisdn_update_query_by_customer_id($params)
{
  // normalize SQL parameters and remove all white spaces
  foreach ($params as $name => $value)
    $params[strtoupper(str_replace(' ', '', $name))] = $value;

  // possible SET parameters and their types (as passed to sprintf)
  $set_params = array(
    'MSISDN_ACTIVATED' => 'd',
    'EXPIRES_DATE' => 's',
    'REPLACED_BY_MSISDN' => 'S');

  // prepare set clause
  $set_array = array('LAST_CHANGED_DATE = GETUTCDATE()');
  foreach ($set_params as $name => $type)
    if (isset($params[$name]))
      $set_array[] = make_query_clause_member($name, $params[$name], $type);
  $set_clause = 'SET ' . implode(', ', $set_array);

  // possible WHERE parameters and their types
  $where_params = array(
    'CUSTOMER_ID' => 'd',
    'MSISDN' => 'S');

  // prepare where clause: here we also handle the special case of NOT parameters
  $where_array = array();
  foreach ($where_params as $name => $type)
    if (isset($params[$name]))
      $where_array[] = make_query_clause_member($name, $params[$name], $type);
    elseif (isset($params['!' . $name]))
      $where_array[] = make_query_clause_member($name, $params['!' . $name], $type, '<>');

  // verify that WHERE clause exists
  if (count($where_array))
    $where_clause = 'WHERE ' . implode(' AND ', $where_array);
  else
  {
    dlog('', 'ERROR: no WHERE clause parameters given');
    return NULL;
  }

  // all done
  return "UPDATE HTT_ULTRA_MSISDN $set_clause $where_clause";
}

/**
 * htt_ultra_msisdn_insert_query
 * SQL INSERT statement for HTT_ULTRA_MSISDN
 * $params: msisdn
 *          customer_id
 *          mvne
 *          msisdn_activated
 *          last_changed_date
 *          created_by_date
 *          customer_id
 *          expires_date
 *          last_transition_uuid
 *          mvne
 *
 * @param array $params
 * @return string
 */
function htt_ultra_msisdn_insert_query($params)
{
  $query = sprintf(
    "IF NOT EXISTS ( SELECT * FROM HTT_ULTRA_MSISDN WHERE MSISDN = %s AND CUSTOMER_ID = %d AND MVNE = '%d' )
BEGIN
INSERT INTO HTT_ULTRA_MSISDN
(
  MSISDN,
  MSISDN_ACTIVATED,
  LAST_CHANGED_DATE,
  CREATED_BY_DATE,
  CUSTOMER_ID,
  EXPIRES_DATE,
  LAST_TRANSITION_UUID,
  MVNE
)
VALUES
(
  %s,
  %d,
  %s,
  %s,
  %d,
  %s,
  %s,
  '%d'
)
END",
    mssql_escape_with_zeroes($params['msisdn']),
    $params['customer_id'],
    $params['mvne'],
    mssql_escape_with_zeroes($params['msisdn']),
    $params['msisdn_activated'],
    $params['last_changed_date'],
    $params['created_by_date'],
    $params['customer_id'],
    $params['expires_date'],
    mssql_escape_with_zeroes($params['last_transition_uuid']),
    $params['mvne']
  );

  return $query;
}

/**
 * port_add_to_htt_ultra_msisdn
 *
 * Add a new row into HTT_ULTRA_MSISDN and ULTRA.HTT_ACTIVATION_HISTORY
 *
 * @param  integer $customer_id
 * @param  string $transition_uuid
 * @return boolean
 */
function port_add_to_htt_ultra_msisdn($customer_id,$transition_uuid)
{
  $customer = get_ultra_customer_from_customer_id( $customer_id , array('CUSTOMER_ID','current_mobile_number') );

  if ( $customer && $customer->current_mobile_number && is_numeric( $customer->current_mobile_number ) )
  {
    // add to HTT_ULTRA_MSISDN
    $check_htt_ultra_msisdn = add_to_htt_ultra_msisdn($customer_id,$customer->current_mobile_number,1,$transition_uuid);

    // record new msisdn into ULTRA.HTT_ACTIVATION_HISTORY
    $sql = ultra_activation_history_update_query(
      array(
        'customer_id' => $customer_id,
        'msisdn'      => $customer->current_mobile_number
      )
    );

    $check_ultra_activation_history = is_mssql_successful(logged_mssql_query($sql));

    flush_ultra_activation_history_cache_by_customer_id( $customer_id );

    return ( $check_htt_ultra_msisdn && $check_ultra_activation_history );
  }
  else
    return FALSE;
}

/**
 * add_to_htt_ultra_msisdn
 *
 * Add a new row into HTT_ULTRA_MSISDN
 *
 * @param  integer $customer_id
 * @param  string $msisdn
 * @param  boolean $msisdn_activated
 * @param  string $transition_uuid
 * @param  string $mvne
 * @return boolean
 */
function add_to_htt_ultra_msisdn($customer_id,$msisdn,$msisdn_activated,$transition_uuid,$mvne='2')
{
  $htt_ultra_msisdn_insert_query = htt_ultra_msisdn_insert_query(
    array(
      'msisdn'               => $msisdn,
      'msisdn_activated'     => $msisdn_activated,
      'last_changed_date'    => 'getutcdate()',
      'created_by_date'      => 'getutcdate()',
      'customer_id'          => $customer_id,
      'expires_date'         => 'getutcdate()',
      'last_transition_uuid' => $transition_uuid,
      'mvne'                 => $mvne
    )
  );

  $check_htt_ultra_msisdn = is_mssql_successful(logged_mssql_query($htt_ultra_msisdn_insert_query));

/*
  if ( $check_htt_ultra_msisdn && ( $mvne == '2' ) )
    // update HTT_ULTRA_MSISDN if MVNE2
    $check_htt_ultra_msisdn = run_sql_and_check(
      sprintf(
        "UPDATE HTT_ULTRA_MSISDN SET mvne = '2' WHERE customer_id = %d AND msisdn = '%s' ",
        $customer_id,
        $msisdn
      )
    );
*/

  return $check_htt_ultra_msisdn;
}

/**
 * get_ultra_msisdn_from_msisdn
 *
 * fetch one record from HTT_ULTRA_MSISDN by MSISDN primary key
 * @param string $msisdn
 * @return array of values
 * @author: VYT, 14-01-23
 */
function get_ultra_msisdn_from_msisdn($msisdn)
{
  // prepare query
  $result = NULL;
  $query = \Ultra\Lib\DB\makeSelectQuery('HTT_ULTRA_MSISDN', NULL, NULL, array('MSISDN' => $msisdn), NULL, array('LAST_CHANGED_DATE desc'));

  if ($query)
  {
    // execute and check result
    $msisdn_data = mssql_fetch_all_objects(logged_mssql_query($query));
    if ($msisdn_data && is_array($msisdn_data) && count($msisdn_data))
      $result = $msisdn_data[0];
  }

  return $result;
}


/**
 * get_ultra_msisdn_from_customer_id
 *
 * fetch one record from HTT_ULTRA_MSISDN by customer_id
 * @param integer csutomer_id
 * @return array row of HTT_ULTRA_MSISDN
 * @author VYT, 14-07-18
 */
function get_ultra_msisdn_from_customer_id($customer_id)
{
  // prepare query
  $result = NULL;
  $query = \Ultra\Lib\DB\makeSelectQuery('HTT_ULTRA_MSISDN', 1, NULL, array('CUSTOMER_ID' => $customer_id), NULL, 'LAST_CHANGED_DATE DESC', TRUE);

  if ($query)
  {
    // execute and check result
    $msisdn_data = mssql_fetch_all_objects(logged_mssql_query($query));
    if ($msisdn_data && is_array($msisdn_data) && count($msisdn_data))
      $result = $msisdn_data[0];
  }

  return $result;
}

?>
