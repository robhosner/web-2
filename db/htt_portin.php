<?php

/*
php component to access DB table HTT_PORTIN_LOG

synopsis

echo htt_portin_log_insert_query(
  array(
    "customer_id"  => 123,
    "msisdn"       => '1234567890',
    "ocn"          => 8,
    "carrier_name" => 'TMOBILE',
    "result"       => 'test',
    "request_date" => 'getutcdate()',
    "activation_distributor" => 888
  )
)."\n\n";

echo htt_portin_log_update_query(
  array(
    "customer_id"  => 31,
    "result"       => 'OK',
    "port_status"  => 'test'
  )
)."\n\n";

echo htt_portin_log_update_query(
  array(
    'port_query_status:NULL' => TRUE,
    'port_query_date:NULL'   => TRUE,
    'msisdn'                 => '1001001000'
  )
)."\n\n";

echo htt_portin_log_update_query(
  array(
    'port_query_status:NULL' => TRUE,
    'port_query_date:NULL'   => TRUE,
    'customer_id'            => 123
  )
)."\n\n";

echo htt_portin_log_select_query(
  array( "customer_id"  => 123)
)."\n\n";
*/

/**
 * get_porting_r_codes 
 * @param  integer $customer_id
 * @param  object $redis Redis
 * @return array $r_codes
 */
function get_porting_r_codes( $customer_id , $redis=NULL )
{
  // instantiates $redis
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis();

  // get MSISDN for customer
  $msisdn = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer_id, 'current_mobile_number');

  if ( $msisdn )
  {
    // check if we have r_codes in Redis
    $r_code = $redis->get( 'ultra/port/provstatus_error_msg/' . $msisdn );

    if ( $r_code )
    {
      $r_codes = array( $r_code );

      dlog('',"customer_id = $customer_id ; r_codes = %s",$r_codes);

      return $r_codes;
    }
  }

  $r_codes = array();

  // check if we have r_codes in HTT_PORTIN_LOG
  $sql = sprintf(
    " SELECT TOP 1 MSISDN , PORT_QUERY_STATUS FROM HTT_PORTIN_LOG WHERE CUSTOMER_ID = %d ORDER BY portin_log_id DESC ",
    $customer_id
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) && $result[0]->PORT_QUERY_STATUS )
  {
    $r_codes = array( sprintf("%s: %s", $result[0]->MSISDN, $result[0]->PORT_QUERY_STATUS) );

    // store r_codes in Redis
    if ( $msisdn )
      $redis->set( 'ultra/port/provstatus_error_msg/' . $msisdn , $r_codes[0] , 60*60*24*7 );
  }

  dlog('',"customer_id = $customer_id ; r_codes = %s",$r_codes);

  return $r_codes;
}

/**
 * get_porting_request_data
 *
 * Loads data from HTT_PORTIN_LOG
 *
 * @param  integer $customer_id
 * @param  object $redis Redis
 * @return stdClass object or NULL
 */
function get_porting_request_data( $customer_id , $redis=NULL )
{
  $porting_request_data = FALSE;

  $htt_portin_log_select_query = htt_portin_log_select_query(
    array( "customer_id" => $customer_id )
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($htt_portin_log_select_query));

  if ( $result && is_array($result) && count($result) )
  {
    $porting_request_data = $result[0];

    if ( $porting_request_data->MSISDN )
    {
      // instantiates $redis
      if ( is_null($redis) )
        $redis = new \Ultra\Lib\Util\Redis();

      // save some fields in Redis

      if ( $porting_request_data->PORT_STATUS )
        $redis->set( 'ultra/port/status/' . $porting_request_data->MSISDN , $porting_request_data->PORT_STATUS , 60*60*24*7 );

      if ( $porting_request_data->UPDATED_SECONDS_AGO )
        $redis->set( 'ultra/port/update_timestamp/' . $porting_request_data->MSISDN , $porting_request_data->UPDATED_SECONDS_AGO , 60*60*24*7 );

      if ( $porting_request_data->PORT_QUERY_STATUS )
        $redis->set( 'ultra/port/provstatus_error_msg/' . $porting_request_data->MSISDN , $porting_request_data->MSISDN.': '.$porting_request_data->PORT_QUERY_STATUS , 60*60*24*7 );
    }
  }

  return $porting_request_data;
}

/**
 * get_porting_status 
 * @param  integer $customer_id
 * @param  object $redis Redis
 * @return string $porting_status
 */
function get_porting_status( $customer_id , $redis=NULL )
{
  // get MSISDN for customer
  $msisdn = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer_id, 'current_mobile_number');

  if ( $msisdn )
  {
    // instantiates $redis
    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis();

    // check if we have port_status in Redis
    $porting_status = $redis->get( 'ultra/port/status/' . $msisdn );

    if ( $porting_status )
      return $porting_status;
  }

  $porting_status = '';

  $htt_portin_log_select_query = htt_portin_log_select_query(
    array( "customer_id" => $customer_id )
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($htt_portin_log_select_query));

  if ( $result && is_array($result) && count($result) )
  {
    // most recent row in HTT_PORTIN_LOG for this customer
    $porting_status = $result[0]->PORT_STATUS;

    if ( $msisdn )
    {
      // instantiates $redis
      if ( is_null($redis) )
        $redis = new \Ultra\Lib\Util\Redis();

      // save some fields in Redis

      if ( $porting_status )
        $redis->set( 'ultra/port/status/' . $msisdn , $porting_status , 60*60*24*7 );

      if ( $result[0]->UPDATED_SECONDS_AGO )
        $redis->set( 'ultra/port/update_timestamp/' . $msisdn , $result[0]->UPDATED_SECONDS_AGO , 60*60*24*7 );

      if ( $result[0]->PORT_QUERY_STATUS )
        $redis->set( 'ultra/port/provstatus_error_msg/' . $msisdn , $result[0]->PORT_QUERY_STATUS , 60*60*24*7 );
    }
  }

  return $porting_status;
}

/**
 * htt_portin_log_select_query
 * $params: customer_id
 *          msisdn
 *           
 * @param  array $params
 * @return string SQL
 */
function htt_portin_log_select_query($params)
{
  $where_clause = '';

  if ( isset($params['customer_id']) )
    $where_clause = sprintf(" CUSTOMER_ID = %d ",$params['customer_id']);
  else
    $where_clause = sprintf(" MSISDN = %s ",mssql_escape_with_zeroes($params['msisdn']));

  $query =
    "SELECT TOP(1) *,
            CASE WHEN PORT_STATUS_DATE IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', dbo.UTC_TO_PT( PORT_STATUS_DATE ) ) END PORT_STATUS_DATE_PST,
            CASE WHEN PORT_QUERY_DATE  IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', dbo.UTC_TO_PT( PORT_QUERY_DATE  ) ) END PORT_QUERY_DATE_PST,
            Datediff(ss, PORT_STATUS_DATE , GETUTCDATE()) UPDATED_SECONDS_AGO
     FROM   HTT_PORTIN_LOG
     WHERE  $where_clause
     ORDER BY PORTIN_LOG_ID DESC";

  return $query;
}

/**
 * htt_portin_log_insert_query 
 * $params: carrier_name
 *          ocn
 *          customer_id
 *          msisdn
 *          result
 *          request_date
 *          activation_masteragent
 *          activation_distributor
 *          activation_agent
 *          activation_store
 *          activation_user_id
 * 
 * @param  array $params
 * @return string $query
 */
function htt_portin_log_insert_query($params)
{
  $carrier_name = 'NULL';
  $ocn          = 'NULL';

  if ( isset($params["carrier_name"]) && ( $params["carrier_name"] != '' ) )
    $carrier_name = mssql_escape($params["carrier_name"]);

  if ( isset($params["ocn"]) && ( $params["ocn"] != '' ) )
    $ocn = $params["ocn"];

  $activation_fields = array(
    'activation_masteragent',
    'activation_distributor',
    'activation_agent',
    'activation_store',
    'activation_user_id'
  );

  foreach( $activation_fields as $id => $field )
    if ( ! isset( $params[ $field ] ) )
      $params[ $field ] = "NULL";

  $query = sprintf(
"INSERT INTO htt_portin_log
(
 CUSTOMER_ID,
 MSISDN,
 OCN,
 CARRIER_NAME,
 RESULT,
 OCN_REQUEST_DATE,
 ACTIVATION_MASTERAGENT,
 ACTIVATION_DISTRIBUTOR,
 ACTIVATION_AGENT,
 ACTIVATION_STORE,
 ACTIVATION_USER_ID
)
VALUES
(
 %d,
 %s,
 %d,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s
)
",
  $params["customer_id"],
  mssql_escape($params["msisdn"]),
  $ocn,
  $carrier_name,
  mssql_escape($params["result"]),
  $params["request_date"],
  empty($params["activation_masteragent"]) ? 'NULL' : $params["activation_masteragent"],
  empty($params["activation_distributor"]) ? 'NULL' : $params["activation_distributor"],
  empty($params["activation_agent"]) ? 'NULL' : $params["activation_agent"],
  empty($params["activation_store"]) ? 'NULL' : $params["activation_store"],
  empty($params["activation_user_id"]) ? 'NULL' : $params["activation_user_id"]
  );

  return $query;
}

?>
