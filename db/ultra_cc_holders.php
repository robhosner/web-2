<?php

/*
[ULTRA].[CC_HOLDERS]
*/

/**
 * ultra_cc_holders_count_by_token
 *
 * @param  string $token
 * @param  integer $customer_id
 * @return integer $count
 */
function ultra_cc_holders_count_by_token( $token, $customer_id = NULL)
{
  $count = 0;

  $sql = sprintf("
    DECLARE @TOKEN VARCHAR(128) = %s;
    SELECT COUNT(*) FROM ULTRA.CC_HOLDER_TOKENS AS CCHT with (nolock)
    INNER JOIN ULTRA.CC_HOLDERS AS CCH with (nolock)
    ON CCHT.CC_HOLDERS_ID = CCH.CC_HOLDERS_ID
    AND TOKEN        = @TOKEN
    AND CCH.ENABLED  = 1
    AND CCHT.ENABLED = 1",
    mssql_escape_with_zeroes($token)
  );

  if ( $customer_id )
    $sql .= sprintf(" AND CUSTOMER_ID != %d ", $customer_id);

  $result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
    $count = $result[0][0];

  return $count;
}

/**
 * ultra_cc_holders_count
 *
 * @param  string $cc_number
 * @param  integer $customer_id
 * @return integer $count
 */
function ultra_cc_holders_count( $cc_number , $customer_id=NULL )
{
  $count = 0;

  $bin       = substr( $cc_number , 0, 6);
  $last_four = substr( $cc_number ,-4);

  $sql = sprintf("
    SELECT COUNT(*) FROM ULTRA.CC_HOLDER_TOKENS AS CCHT with (nolock)
    INNER JOIN ULTRA.CC_HOLDERS AS CCH with (nolock)
    ON CCHT.CC_HOLDERS_ID = CCH.CC_HOLDERS_ID
    AND BIN          = %s
    AND LAST_FOUR    = %s
    AND CCH.ENABLED  = 1
    AND CCHT.ENABLED = 1",
    $bin,
    $last_four
  );

  if ( $customer_id )
    $sql .= sprintf(" AND CUSTOMER_ID != %d ",$customer_id);

  $result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
    $count = $result[0][0];

  return $count;
}

/**
 * ultra_cc_holders_update_query
 * $params: customer_id
 *          bin
 *          last_four
 *          expires_date
 *          enabled
 *          NOT_bin
 *          NOT_last_four
 *          NOT_expires_date
 *          SET_enabled
 *
 * @param  array $params
 * @return string
 */
function ultra_cc_holders_update_query( $params )
{
  $set_clauses       = array();
  $where_clauses     = array();
  $not_where_clauses = array();

  if ( isset($params['customer_id'] ) )
    $where_clauses[]     = sprintf( " CUSTOMER_ID  = %d " , $params['customer_id'] );

  if ( isset($params['bin'] ) )
    $where_clauses[]     = sprintf( " BIN          = %s " , mssql_escape_with_zeroes( $params['bin'] ) );

  if ( isset($params['last_four'] ) )
    $where_clauses[]     = sprintf( " LAST_FOUR    = %s " , mssql_escape_with_zeroes( $params['last_four'] ) );

  if ( isset($params['expires_date'] ) )
    $where_clauses[]     = sprintf( " EXPIRES_DATE = %s " , mssql_escape_with_zeroes( $params['expires_date'] ) );

  if ( isset($params['enabled'] ) )
    $where_clauses[]     = sprintf( " ENABLED      = %d " , $params['enabled'] );

  if ( isset($params['NOT_bin'] ) )
    $not_where_clauses[] = sprintf( " BIN          = %s " , mssql_escape_with_zeroes( $params['NOT_bin'] ) );

  if ( isset($params['NOT_last_four'] ) )
    $not_where_clauses[] = sprintf( " LAST_FOUR    = %s " , mssql_escape_with_zeroes( $params['NOT_last_four'] ) );

  if ( isset($params['NOT_expires_date'] ) )
    $not_where_clauses[] = sprintf( " EXPIRES_DATE = %s " , mssql_escape_with_zeroes( $params['NOT_expires_date'] ) );

  if ( isset($params['SET_enabled'] ) )
    $set_clauses[]       = sprintf( " ENABLED      = %d " , $params['SET_enabled'] );

  if ( count($not_where_clauses) )
    $where_clauses[] = ' NOT ( ' . implode( 'AND' , $not_where_clauses ) . ' ) ';

  return '
UPDATE ULTRA.CC_HOLDERS
SET    ' . implode( ','   , $set_clauses   ) . '
WHERE  ' . implode( 'AND' , $where_clauses ) ;
}

/**
 * ultra_cc_holders_insert_query
 * $params: customer_id
 *          bin
 *          last_four
 *          expires_date
 *          ccv_validation
 *          avs_validation
 *
 * @param  array $params
 * @return string
 */
function ultra_cc_holders_insert_query( $params )
{
  if ( ! isset( $params['cvv_validation'] ) )
    $params['cvv_validation'] = ' ';

  if ( ! isset( $params['avs_validation'] ) )
    $params['avs_validation'] = ' ';

  return sprintf("
IF EXISTS (
  SELECT CUSTOMER_ID
  FROM   ULTRA.CC_HOLDERS WITH (NOLOCK)
  WHERE  CUSTOMER_ID  = %d
  AND    BIN          = %s
  AND    LAST_FOUR    = %s
  AND    EXPIRES_DATE = %s
)
BEGIN
  UPDATE ULTRA.CC_HOLDERS
  SET    ENABLED = 1
  WHERE  CUSTOMER_ID  = %d
  AND    BIN          = %s
  AND    LAST_FOUR    = %s
  AND    EXPIRES_DATE = %s
END
  ELSE
BEGIN
  INSERT INTO ULTRA.CC_HOLDERS
(
  CUSTOMER_ID,
  BIN,
  LAST_FOUR,
  EXPIRES_DATE,
  CVV_VALIDATION,
  AVS_VALIDATION
)
VALUES
(
  %d,
  %s,
  %s,
  %s,
  %s,
  %s
)
END ",
  $params['customer_id'],
  mssql_escape_with_zeroes($params['bin']),
  mssql_escape_with_zeroes($params['last_four']),
  mssql_escape_with_zeroes($params['expires_date']),
  $params['customer_id'],
  mssql_escape_with_zeroes($params['bin']),
  mssql_escape_with_zeroes($params['last_four']),
  mssql_escape_with_zeroes($params['expires_date']),
  $params['customer_id'],
  mssql_escape_with_zeroes($params['bin']),
  mssql_escape_with_zeroes($params['last_four']),
  mssql_escape_with_zeroes($params['expires_date']),
  mssql_escape_with_zeroes($params['cvv_validation']),
  mssql_escape_with_zeroes($params['avs_validation'])
  );
}

/**
 * add_to_ultra_cc_holders
 *
 * Add a row into [ULTRA].[CC_HOLDERS]
 * $params: see ultra_cc_holders_insert_query
 *          customer_id
 *          expires_date (format is mmyy)
 *          cc_number
 *
 * @param  array $params
 * @return boolean
 */
function add_to_ultra_cc_holders( $params )
{
  dlog('', '(%s)', func_get_args());

  $params['bin']       = substr( $params['cc_number'] , 0, 6);
  $params['last_four'] = substr( $params['cc_number'] ,-4);

  return run_sql_and_check( ultra_cc_holders_insert_query( $params ) );
}

/**
 * get_cc_info_from_customer_id
 *
 * get credit card info from ULTRA.CC_HOLDERS based on customer ID
 *
 * @param  integer $customer_id
 * @return  object cc info
 */
function get_cc_info_from_customer_id($customer_id)
{
  $sql = sprintf("SELECT * FROM ULTRA.CC_HOLDERS WITH (NOLOCK) WHERE CUSTOMER_ID = %d AND ENABLED = 1", $customer_id);

  $rows = mssql_fetch_all_objects(logged_mssql_query($sql));

  if (count($rows))
  {
    // warn if multiple rows found
    if (count($rows) > 1)
      dlog('', "ESCALATION ALERT IMMEDIATE found multiple enabled ULTRA.CC_HOLDERS for CUSTOMER_ID = %d", $customer_id);

    return $rows[0];
  }

  return NULL;
}

/**
 * disable_ultra_cc_holders
 * $params: see ultra_cc_holders_update_query
 *          cc_number
 *
 * @param  array $params
 * @return boolean result of SQL UPDATE
 */
function disable_ultra_cc_holders( $params )
{
  dlog('', '(%s)', func_get_args());

  if ( isset( $params['cc_number'] ) )
  {
    $params['bin']         = substr( $params['cc_number'] , 0, 6);
    $params['last_four']   = substr( $params['cc_number'] ,-4);
  }

  $params['SET_enabled'] = '0';
  $params['enabled']     = '1';

  return run_sql_and_check( ultra_cc_holders_update_query( $params ) );
}

/**
 * get_cc_info_expiring_next_month
 *
 * @return array of objects containing customer_ids
 */
function get_cc_info_expiring_next_month()
{
  $date = date('my', strtotime('+1 month'));
  $sql  = sprintf("DECLARE @MONTHYEAR VARCHAR(4) = '%s';
    SELECT DISTINCT cch.CUSTOMER_ID FROM ULTRA.CC_HOLDERS AS cch WITH (NOLOCK)
    INNER JOIN htt_customers_overlay_ultra AS hcou WITH (NOLOCK) ON cch.CUSTOMER_ID = hcou.CUSTOMER_ID
    WHERE cch.EXPIRES_DATE = @MONTHYEAR AND cch.ENABLED = 1 AND hcou.plan_state = '%s'", $date, STATE_ACTIVE);
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_cc_info_expiring_this_month
 *
 * @return array of objects containing customer_ids
 */
function get_cc_info_expiring_this_month()
{
  $date = date('my', strtotime('-1 month'));
  $sql  = sprintf("DECLARE @MONTHYEAR VARCHAR(4) = '%s';
    SELECT DISTINCT cch.CUSTOMER_ID FROM ULTRA.CC_HOLDERS AS cch WITH (NOLOCK)
    INNER JOIN htt_customers_overlay_ultra AS hcou WITH (NOLOCK) ON cch.CUSTOMER_ID = hcou.CUSTOMER_ID
    WHERE cch.EXPIRES_DATE = @MONTHYEAR AND cch.ENABLED = 1 AND hcou.plan_state = '%s'", $date, STATE_ACTIVE);
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * disable_ultra_cc_holders_by_customer_id
 *
 * @return Boolean successful?
 */
function disable_ultra_cc_holders_by_customer_id($customer_id)
{
  if ( ! is_numeric($customer_id))
  {
    dlog('', 'WARNING: invalid parameter customer_id');
    return FALSE;
  }

  $sql = sprintf("UPDATE ULTRA.CC_HOLDERS SET ENABLED = 0 WHERE CUSTOMER_ID = %d", $customer_id);
  return is_mssql_successful(logged_mssql_query($sql));
}