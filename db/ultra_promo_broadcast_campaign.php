<?php

// [ULTRA].[PROMO_BROADCAST_CAMPAIGN]

/**
 * get_customers_batch_for_promo_broadcast_campaign
 *
 * @param  array $buffer array of customer Ids
 * @param  integer SQL TOP
 * @return object of class \Result
 */
function get_customers_batch_for_promo_broadcast_campaign( $buffer , $top=1000 )
{
  $sql = sprintf(
" SELECT TOP %d o.CUSTOMER_ID,
                o.CURRENT_MOBILE_NUMBER,
                o.PREFERRED_LANGUAGE,
                z.TZ,
                o.monthly_cc_renewal,
                o.BRAND_ID
FROM            CUSTOMERS                     c WITH (NOLOCK)
JOIN            HTT_CUSTOMERS_OVERLAY_ULTRA   o WITH (NOLOCK)
ON              o.CUSTOMER_ID = c.CUSTOMER_ID
LEFT OUTER JOIN HTT_LOOKUP_REFERENCE_ZIP      z WITH (NOLOCK)
ON              z.ZIP = c.POSTAL_CODE
WHERE           o.CUSTOMER_ID IN ( ".implode(',',$buffer)." )
AND             o.CURRENT_MOBILE_NUMBER != ''
AND             o.CURRENT_MOBILE_NUMBER != ' '
AND             o.CURRENT_MOBILE_NUMBER > '0'
 ",
    $top
  );

  return make_ok_Result( mssql_fetch_all_rows( logged_mssql_query( $sql ) ) );
}

/**
 * get_customers_for_promo_broadcast_campaign
 * Admittedly risky function to retrieve customer data
 *
 * @param  string $where_clause
 * @param  integer $top SQL TOP
 * @return object of class \Result
 */
function get_customers_for_promo_broadcast_campaign( $where_clause , $top=NULL )
{
  dlog('',"where_clause = $where_clause");

  // sanity cleanup
  $where_clause = preg_replace('/[\-\;\@]/', '', $where_clause );
  $where_clause = preg_replace('/DROP|ALTER|INSERT|DELETE|UPDATE/i', '', $where_clause );
  $where_clause = preg_replace('/\s+/', ' ', $where_clause );

  if ( ! empty($top) && is_numeric($top) )
    $top = "TOP $top";

  $sql = "
  SELECT $top
       o.CUSTOMER_ID,
       o.CURRENT_MOBILE_NUMBER,
       o.PREFERRED_LANGUAGE
  FROM HTT_CUSTOMERS               c
  JOIN HTT_CUSTOMERS_OVERLAY_ULTRA o
  ON   o.CUSTOMER_ID = c.CUSTOMER_ID
  WHERE
  $where_clause ";

#TEST
/*
  $sql = "
  SELECT top 100 o.CUSTOMER_ID,
       o.CURRENT_MOBILE_NUMBER,
       o.PREFERRED_LANGUAGE
  FROM  HTT_CUSTOMERS_OVERLAY_ULTRA o
WHERE o.CURRENT_MOBILE_NUMBER is not null and o.PREFERRED_LANGUAGE = 'ES'
and  o.CURRENT_MOBILE_NUMBER != ''
and  o.CURRENT_MOBILE_NUMBER != ' '
and  o.CURRENT_MOBILE_NUMBER > '0'
";
*/

  return make_ok_Result( mssql_fetch_all_rows( logged_mssql_query( $sql ) ) );
}

/**
 * update_ultra_promo_broadcast_campaign
 * Updates a ULTRA.PROMO_BROADCAST_CAMPAIGN row
 * $params: promo_broadcast_campaign_id
 *          status
 *
 * @param  array $params
 * @return object of class \Result
 */
function update_ultra_promo_broadcast_campaign( $params )
{
  if ( empty( $params['promo_broadcast_campaign_id'] ) )
    return make_error_Result('promo_broadcast_campaign_id missing');

  $set_clause = '';

  if ( isset($params['status']) && !empty($params['status']) )
    $set_clause .= sprintf(" STATUS = %s ",mssql_escape_with_zeroes($params['status']));

  if ( isset( $params['volume_count'] ) && ! empty( $params['volume_count'] ) )
  {
    if (strlen($set_clause)) $set_clause .= ',';
    $set_clause .= sprintf(" VOLUME_COUNT = %d ",$params['volume_count']);
  }

  if ( isset($params['name']) && !empty($params['name']) )
  {
    if (strlen($set_clause)) $set_clause .= ',';
    $set_clause .= sprintf(" NAME = %s ", mssql_escape_with_zeroes($params['name']));
  }

  $sql = sprintf(
    "UPDATE ULTRA.PROMO_BROADCAST_CAMPAIGN
     SET   $set_clause
     WHERE PROMO_BROADCAST_CAMPAIGN_ID = %d
    ", $params['promo_broadcast_campaign_id']
  );

  return run_sql_and_check_result($sql);
}

/**
 * get_ultra_promo_broadcast_campaign
 * Get rows from ULTRA.PROMO_BROADCAST_CAMPAIGN
 * $params: top
 *          name
 *          status
 *          promo_broadcast_campaign_id
 *          promo_shortcode
 *          promo_enabled
 *          created_days_ago
 *          time_hours
 *          time_minutes
 *          order_by
 *
 * @param  array $params
 * @return object of class \Result
 */
function get_ultra_promo_broadcast_campaign( $params )
{
  dlog('', '(%s)', func_get_args());

  $where = array();

  $top = ( ( empty( $params['top'] ) || ! is_numeric( $params['top'] ) ) ? 1000 : $params['top'] );

  if ( ! empty( $params['name'] ) )
    $where['NAME']                        = $params['name'];

  if ( ! empty( $params['status'] ) )
    $where['STATUS']                      = $params['status'];

  if ( ! empty( $params['promo_broadcast_campaign_id'] ) )
    $where['PROMO_BROADCAST_CAMPAIGN_ID'] = $params['promo_broadcast_campaign_id'];

  if ( ! empty( $params['promo_shortcode'] ) )
    $where['PROMO_SHORTCODE']             = $params['promo_shortcode'];

  if ( ! empty( $params['promo_enabled'] ) )
    $where['PROMO_ENABLED']               = $params['promo_enabled'];

  if ( ! empty( $params['created_days_ago'] ) && is_numeric( $params['created_days_ago'] ) && ( $params['created_days_ago'] > 0 ) )
    $where['CREATED_DATE']                = 'last_'.$params['created_days_ago'].'_days';

  // current time (PST)
  if ( ! empty( $params['time_hours'] ) )
  {
    $where['EARLIEST_DELIVERY_TIME'] = 'lteq_'.sprintf("%02s",( $params['time_hours'] + 3 )).sprintf("%02s", $params['time_minutes'] );
    $where['LATEST_DELIVERY_TIME']   = 'gteq_'.sprintf("%02s",$params['time_hours']        ).sprintf("%02s", $params['time_minutes'] );
  }

  // do not return ONGOING campaigns which start in the future
  if ( !empty($where['STATUS']) && ( $where['STATUS'] == 'ONGOING' ) )
    $where['STARTED_DATE']                = 'past';

  // PROD-1556: default order is newest first
  if (empty($params['order_by']))
    $params['order_by'] = 'PROMO_BROADCAST_CAMPAIGN_ID DESC';

  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'ULTRA.PROMO_BROADCAST_CAMPAIGN',
    $top,
    array(
      'PROMO_BROADCAST_CAMPAIGN_ID',
      'NAME',
      'STATUS',
      'CONVERT(varchar(max),MESSAGE_EN) MESSAGE_EN',
      'CONVERT(varchar(max),MESSAGE_ES) MESSAGE_ES',
      'CONVERT(varchar(max),MESSAGE_ZH) MESSAGE_ZH',
      'CASE WHEN CREATED_DATE = NULL THEN NULL ELSE dbo.UTC_TO_PT(CREATED_DATE) END AS CREATED_DATE',
      'CASE WHEN STARTED_DATE = NULL THEN NULL ELSE dbo.UTC_TO_PT(STARTED_DATE) END AS STARTED_DATE',
      'CUSTOMER_CLAUSE',
      'PROMO_SHORTCODE',
      'PROMO_ENABLED',
      'PROMO_DESCRIPTION',
      'CASE WHEN PROMO_EXPIRES_DATE = NULL THEN NULL ELSE dbo.UTC_TO_PT(PROMO_EXPIRES_DATE) END AS PROMO_EXPIRES_DATE',
      'PROMO_ACTION',
      'PROMO_VALUE',
      'EARLIEST_DELIVERY_TIME',
      'LATEST_DELIVERY_TIME',
      'MESSAGES_PER_HOUR',
      'VOLUME_COUNT',
      'CASE WHEN ( PROMO_EXPIRES_DATE IS NOT NULL AND PROMO_EXPIRES_DATE <= GETUTCDATE() ) THEN 1 ELSE 0 END is_expired',
      'SEND_TO_ACTIVE',
      'SEND_TO_SUSPENDED',
      'SEND_TO_MULTI_MONTH',
      'SEND_TO_OPT_OUT',
      'SEND_TO_AUTO_RENEWAL',
      'BRAND_ID'
    ),
    $where,
    NULL,
    $params['order_by']);

  return make_ok_Result( mssql_fetch_all_objects( logged_mssql_query( $sql ) ) );
}

/**
 * add_to_ultra_promo_broadcast_campaign
 * Add a new row to ULTRA.PROMO_BROADCAST_CAMPAIGN
 * $params: name
 *          message_en
 *          message_es
 *          message_zh
 *          promo_shortcode
 *          promo_enabled
 *          promo_desciption
 *          promo_expires_date
 *          promo_action
 *          promo_value
 *          started_date
 *          earliest_delivery_time
 *          latest_delivery_time
 *          messages_per_hour
 *
 * @param  array $params
 * @return object of class \Result
 */
function add_to_ultra_promo_broadcast_campaign( $params )
{
  if ( empty( $params['name'] ) )
    return make_error_Result('name missing');

  if ( empty( $params['message_en'] ) )
    return make_error_Result('message_en missing');

  if ( empty( $params['message_es'] ) )
    $params['message_es'] = $params['message_en'];

  if ( empty( $params['message_zh'] ) )
    $params['message_zh'] = $params['message_en'];

  if ( empty( $params['earliest_delivery_time'] ) || ! is_numeric( $params['earliest_delivery_time'] ) )
    $params['earliest_delivery_time'] = '0900';

  if ( empty( $params['latest_delivery_time'] ) || ! is_numeric( $params['latest_delivery_time'] ) )
    $params['latest_delivery_time'] = '2100';

  $sql = sprintf("
INSERT INTO ULTRA.PROMO_BROADCAST_CAMPAIGN
(
NAME,
MESSAGE_EN,
MESSAGE_ES,
MESSAGE_ZH,
PROMO_SHORTCODE,
PROMO_ENABLED,
PROMO_DESCRIPTION,
PROMO_EXPIRES_DATE,
PROMO_ACTION,
PROMO_VALUE,
CREATED_DATE,
STARTED_DATE,
EARLIEST_DELIVERY_TIME,
LATEST_DELIVERY_TIME,
MESSAGES_PER_HOUR,
SEND_TO_ACTIVE,
SEND_TO_SUSPENDED,
SEND_TO_MULTI_MONTH,
SEND_TO_OPT_OUT,
SEND_TO_AUTO_RENEWAL,
BRAND_ID
)
VALUES
(
%s,
%s,
%s,
%s,
%s,
%d,
%s,
%s,
%s,
%s,
GETUTCDATE(),
'%s',
'%s',
'%s',
%d,
%d,
%d,
%d,
%d,
%d,
%d
) ",
    mssql_escape_with_zeroes($params['name']),
    mssql_escape_with_zeroes($params['message_en']),
    mssql_escape_with_zeroes($params['message_es']),
    mssql_escape_with_zeroes($params['message_zh']),
    mssql_escape_with_zeroes($params['promo_shortcode']),
    $params['promo_enabled'] ? 1 : 0,
    mssql_escape_with_zeroes($params['promo_description']),
    $params['promo_expires_date'] ? "'{$params['promo_expires_date']}'" : 'NULL',
    mssql_escape_with_zeroes($params['promo_action']),
    mssql_escape_with_zeroes($params['promo_value']),
    $params['started_date'],
    $params['earliest_delivery_time'],
    $params['latest_delivery_time'],
    $params['messages_per_hour'],
    $params['send_to_active'],
    $params['send_to_suspended'],
    $params['send_to_multi_month'],
    $params['send_to_opt_out'],
    $params['send_to_auto_renewal'],
    $params['brand_id']
  );

  return run_sql_and_check_result($sql);
}

?>
