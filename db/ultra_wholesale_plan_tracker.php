<?php

/*
 * STUB file, no functions here
 *
 * to get the current wholesale plan tracker for a customer use 
 *    require_once 'Ultra/Lib/DB/Customer.php';
 *    $wholesalePlan = \Ultra\Lib\DB\Customer\getWholesalePlan(CUSTOMER_ID, $redis);
 *
 * recording of the current wholesale plan has been moved to the middleware layer
 *
 * @see DATAQ project
 * @author VYT @ 2015-04-17
*/


?>
