<?php

include_once('db.php');

# php component to access DB table htt_ultra_locations #

# synopsis

#echo htt_ultra_locations_select_query(
#  array(
#    "latitude" => -73.94402505,
#    "longitude" => 40.7181058,
#    "max_entries" => 2,
#    "location_type" => "DEALER",
#    "radius" => 2,
#  )
#)."\n";

#echo htt_ultra_locations_temp_update_query(
#  array(
#    'location_id' => 1,
#    'address1'    => 'test',
#    'address2'    => 'TEST'
#  )
#)."\n";

/**
 * htt_ultra_locations_create_temp_table
 * @return string SQL
 */
function htt_ultra_locations_create_temp_table()
{
  return '
CREATE TABLE #TEMP_HTT_ULTRA_LOCATIONS (
  [LOCATION_ID]                 int NOT NULL,
  [RETAILER_NAME]               varchar(45) NOT NULL,
  [ADDRESS1]                    varchar(45) NOT NULL,
  [ADDRESS2]                    varchar(30) NULL,
  [CITY]                        varchar(24) NOT NULL,
  [STATE]                       varchar(2) NOT NULL,
  [ZIPCODE]                     varchar(9) NOT NULL,
  [RETAILER_CONTACT]            varchar(40) NULL,
  [RETAILER_PHONE_NUMBER]       varchar(10) NULL,
  [RETAILER_FAX_NUMBER]         varchar(10) NULL,
  [RETAILER_EMAIL_ADDRESS]      varchar(40) NULL,
  [LOCATION]                    geography NULL,
  [LOCATION_TYPE]               varchar(15) NOT NULL,
  [ACTIVE]                      bit NOT NULL,
  [EXPIRES_DATE]                datetime NULL,
  [LOCATION_SOURCE]             varchar(15) NOT NULL,
  [SOURCE_ID]                   varchar(15) NOT NULL
);
  ';
}

/**
 * htt_ultra_locations_temp_import 
 * @return string SQL
 */
function htt_ultra_locations_temp_import()
{
  $dealerPortal = \Ultra\UltraConfig\celluphoneDb();

  return "
INSERT INTO #TEMP_HTT_ULTRA_LOCATIONS (
  LOCATION_ID,
  RETAILER_NAME,
  ADDRESS1,
  ADDRESS2,
  CITY,
  STATE,
  ZIPCODE,
  RETAILER_CONTACT,
  RETAILER_PHONE_NUMBER,
  LOCATION_TYPE,
  ACTIVE,
  LOCATION_SOURCE,
  SOURCE_ID,
  LOCATION
)
SELECT
  d.DealerSiteID as LOCATION_ID,
  left(BusinessName,45) as RETAILER_NAME,
  left(Address,45) as ADDRESS1,
  left(Suite,30) as ADDRESS2,
  left(CITY,24) as CITY,
  left(STATE,2) as STATE,
  left([Zip],5) as ZIPCODE,
  left(FirstName+' '+LastName,39) as RETAILER_CONTACT,
  left([Phone],10) as RETAILER_PHONE_NUMBER,
  'DEALER' AS LOCATION_TYPE,
  d.ActiveFlag AS ACTIVE,
  'PORTAL_DEALER' as LOCATION_SOURCE,
  ad.AddressID as SOURCE_ID,
  geography::Point(ad.[Latitude] ,ad.[Longitude], 4326) as LOCATION
FROM $dealerPortal..[tblDealerSite] d
JOIN $dealerPortal..[tblDealerSiteAddress] a
ON   d.[DealerSiteID]=a.[dealerSiteID]
JOIN $dealerPortal..[tblAddress] ad
ON   ad.[AddressID]=a.[AddressID]
JOIN $dealerPortal..tblDealerSitePerson dsp
ON   d.DealerSiteID = dsp.DealerSiteID
INNER JOIN $dealerPortal..tblperson p
ON   dsp.PersonID = p.PersonID
WHERE cast(ad.[Latitude] as float) between -90 and 90
AND d.[LocationPortalFlag]=1
AND   a.[effectivedt] is not null
AND   (a.[expirationdt] is null or a.[expirationdt] > getutcdate())
AND   [Latitude] is not null
AND   [Longitude] is not null  
AND   [Zip] is not null
AND   persontypecd = 'OW'
AND   addressTypeCd = 'MA'
AND   d.ActiveFlag = 1

AND NOT EXISTS (
  SELECT
    d1.DealerSiteID as LOCATION_ID
  FROM $dealerPortal..[tblDealerSite] d1
  JOIN $dealerPortal..[tblDealerSiteAddress] a1
  ON   d1.[DealerSiteID]=a1.[dealerSiteID]
  JOIN $dealerPortal..[tblAddress] ad1
  ON   ad1.[AddressID]=a1.[AddressID]
  JOIN $dealerPortal..tblDealerSitePerson dsp1
  ON   d1.DealerSiteID = dsp1.DealerSiteID
  INNER JOIN $dealerPortal..tblperson p1
  ON   dsp1.PersonID = p1.PersonID
  WHERE d1.[LocationPortalFlag]=1
  AND   a1.[effectivedt] is not null
  AND   (a1.[expirationdt] is null or a1.[expirationdt] > getutcdate())
  AND   [Latitude] is not null
  AND   [Longitude] is not null
  AND   [Zip] is not null
  AND   persontypecd = 'OW'
  AND   addressTypeCd = 'MA'
  AND   d1.ActiveFlag = 1
  AND d1.dealersiteID = d.dealerSiteID
  AND a1.addressID < a.addressID
);
  ";
}

/**
 * perform_htt_ultra_locations_temp_import 
 * import raw locations data into #TEMP_HTT_ULTRA_LOCATION
 * @return object Result
 */
function perform_htt_ultra_locations_temp_import()
{
  $sql = set_mssql_options();

  dlog('',$sql);

  $check = run_sql_and_check_result($sql);

  if ( ! $check ) { return FALSE; }

  $sql = htt_ultra_locations_create_temp_table().
         htt_ultra_locations_temp_import();

  dlog('',$sql);

  return run_sql_and_check_result($sql);
}

/**
 * perform_htt_ultra_locations_import 
 * 
 * import HTT_ULTRA_LOCATIONS
 * @return array $errors
 */
function perform_htt_ultra_locations_import()
{
  $errors = array();

  if ( perform_htt_ultra_locations_temp_import() )
  {
    dlog('',"perform_htt_ultra_locations_temp_import SUCCESS");

    list( $errors , $duplicate_location_ids ) = perform_htt_ultra_locations_temp_cleanup($errors);

    if ( ! count($errors) )
    {
      $errors = perform_htt_ultra_locations_temp_switch($errors,$duplicate_location_ids);
    }
  }
  else
  {
    $errors[]= "perform_htt_ultra_locations_temp_import error";
  }

  return $errors;
}

/**
 * perform_htt_ultra_locations_temp_cleanup 
 * De-Duplicate/Beautify #TEMP_HTT_ULTRA_LOCATIONS content
 * @param  array $errors
 * @return array
 */
function perform_htt_ultra_locations_temp_cleanup($errors)
{
  $continue = TRUE;
  $row_from = 1;
  $offset   = 100; // query 100 rows at a time

  $addresses = array(); // here we store addresses to check for duplicates
  $duplicate_location_ids = array(); // here we store location IDs which will not be imported into HTT_ULTRA_LOCATIONS since they are duplicate

  while($continue)
  {
    $row_to = $row_from + $offset - 1;

    $sql = "WITH SUBSELECT AS
    (
      SELECT
        ROW_NUMBER() OVER (ORDER BY t.LOCATION_ID) AS row,
        t.LOCATION_ID,
        t.RETAILER_NAME,
        t.ADDRESS1,
        t.ADDRESS2,
        t.ZIPCODE
      FROM
        #TEMP_HTT_ULTRA_LOCATIONS t
    )
    SELECT
      LOCATION_ID, RETAILER_NAME, ADDRESS1, ADDRESS2, ZIPCODE
    FROM
      SUBSELECT
    WHERE
      row BETWEEN $row_from AND $row_to
    ";

    $temp_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( $temp_data && is_array($temp_data) && count($temp_data) )
    {
      // Beautify
      list( $errors , $addresses , $duplicate_location_ids ) = beautify_htt_ultra_locations_temp_rows( $temp_data , $addresses , $duplicate_location_ids );
    }
    else
    {
      // all #TEMP_HTT_ULTRA_LOCATIONS rows have been processed
      $continue = FALSE;
    }

    $row_from += $offset;
  }

  dlog('',"Duplicate locations : ".implode(',',$duplicate_location_ids));

  return array(
    $errors,
    $duplicate_location_ids
  );
}

/**
 * beautify_htt_ultra_locations_temp_rows 
 * @param  array $temp_data
 * @param  array $addresses
 * @param  array $duplicate_location_ids
 * @return array ($errors, $addresses, $duplicated_location_ids)
 */
function beautify_htt_ultra_locations_temp_rows($temp_data, $addresses, $duplicate_location_ids)
{
  $errors = array();

  foreach ($temp_data as $id => $data)
  {
    if (!count($errors))
    {
      $full_address = $data->ZIPCODE . ' ' . beautify_address($data->ADDRESS1) . ' ' . beautify_address($data->ADDRESS2);

      $beautified_data = array(
        'location_id'   => $data->LOCATION_ID,
        'retailer_name' => beautify_address($data->RETAILER_NAME),
        'address1'      => beautify_address($data->ADDRESS1),
        'address2'      => $data->ADDRESS2
      );

      if (isset($addresses[$full_address]))
      {
        dlog('', "duplicate address $full_address");
        $duplicate_location_ids[] = $data->LOCATION_ID;
      }
      else
      {
        $addresses[$full_address] = 1;
      }

      $update_sql = htt_ultra_locations_temp_update_query($beautified_data);

      if (!is_mssql_successful(logged_mssql_query($update_sql)))
      {
        $errors[] = "Cannot update #TEMP_HTT_ULTRA_LOCATIONS";
      }
    }
  }

  return array(
    $errors,
    $addresses,
    $duplicate_location_ids
  );
}

/**
 * perform_htt_ultra_locations_temp_switch 
 * switch the content of #TEMP_HTT_ULTRA_LOCATIONS to HTT_ULTRA_LOCATIONS
 * @param  array $errors
 * @param  array $duplicate_location_ids
 * @return array $errors
 */
function perform_htt_ultra_locations_temp_switch($errors,$duplicate_location_ids)
{
  if ( ! start_mssql_transaction() )
  {
    $errors[] = "start_mssql_transaction_query error";
    return $errors;
  }

  $exclude_locations_clause =
    (count($duplicate_location_ids))
    ?
    "WHERE LOCATION_ID NOT IN (".implode(',',$duplicate_location_ids).")"
    :
    ''
    ;

  $sql = "
DELETE FROM HTT_ULTRA_LOCATIONS;
INSERT INTO HTT_ULTRA_LOCATIONS SELECT * FROM #TEMP_HTT_ULTRA_LOCATIONS $exclude_locations_clause;
  ";

  if ( is_mssql_successful(logged_mssql_query($sql)) )
  {
    if ( ! commit_mssql_transaction() )
    { $errors[] = "commit_mssql_transaction error"; }
  }
  else if ( ! rollback_mssql_transaction() )
  { $errors[] = "rollback_mssql_transaction error"; }

  return $errors;
}

/**
 * htt_ultra_locations_temp_update_query
 * $params: retailer_name
 *          address1
 *          address2
 *          location_id
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_ultra_locations_temp_update_query($params)
{
  $set = array();

  if ( isset($params['retailer_name']) )
  {
    $set[] = sprintf(" RETAILER_NAME = %s",mssql_escape_with_zeroes($params['retailer_name']));
  }

  if ( isset($params['address1']) )
  {
    $set[] = sprintf(" ADDRESS1 = %s",mssql_escape_with_zeroes($params['address1']));
  }

  if ( isset($params['address2']) )
  {
    $set[] = sprintf(" ADDRESS2 = %s",mssql_escape_with_zeroes($params['address2']));
  }

  return
    "UPDATE #TEMP_HTT_ULTRA_LOCATIONS
     SET    ".implode(',',$set)."
     WHERE  LOCATION_ID = ".$params['location_id'];
}

/**
 * htt_ultra_locations_select_query 
 * $params: radius
 *          location_type
 *          longitude
 *          latitude
 *          max_entries
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_ultra_locations_select_query($params)
{
  return sprintf("
    EXEC [ULTRA].[GetRetailLocationActivations]
      @TOP = %d,
      @Brand_ID = %d,
      @Radius = %f,
      @Longitude = %f,
      @Latitude = %f,
      @Location_Type = %s",
    empty($params["max_entries"]) ? 50 : $params["max_entries"],
    $params['brand_id'],
    $params["radius"],
    $params["longitude"],
    $params["latitude"],
    $params["location_type"]
  );
}

