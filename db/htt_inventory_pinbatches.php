<?php

# php component to access DB table htt_inventory_pinbatches #

# synopsis

#echo htt_inventory_pinbatches_insert_query(
#  array(
#    'serial_number_id' => '1234567890abcd',
#    'sku' => '123456789012345678901234567890',
#    'upc' => '1234567890ab',
#    'channel' => 'test__channel'
#  )
#)."\n";

#echo htt_inventory_pinbatches_select_query(
#  array(
#    'pin_batch_id' => '1234567890abcd'
#  )
#)."\n";

/**
 * htt_inventory_pinbatches_insert_query
 * $params: serial_number_id
 *          sku
 *          upc
 *          channel
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_inventory_pinbatches_insert_query($params)
{
  $query = sprintf(
"INSERT INTO htt_inventory_pinbatches
 (
  serial_number_id,
  sku,
  upc,
  channel
 )
 VALUES
 (
  %s,
  %s,
  %s,
  %s)",
    mssql_escape($params["serial_number_id"]),
    mssql_escape($params["sku"]),
    mssql_escape($params["upc"]),
    mssql_escape($params["channel"])
  );

  return $query;
}

/**
 * htt_inventory_pinbatches_select_query 
 * $params: serial_number_id
 * @param  array $params
 * @return string SQL
 */
function htt_inventory_pinbatches_select_query($params)
{
  $query = sprintf(
    "SELECT * FROM htt_inventory_pinbatches WHERE serial_number_id = %s",
    mssql_escape($params["serial_number_id"])
  );

  return $query;
}

?>
