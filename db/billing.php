<?php

# php component to access DB table billing #

# synopsis

#class test_customer_data
#{
#  public $COS_ID      = 1010;
#  public $CUSTOMER_ID = 19;
#  public $FIRST_NAME  = 'Adam';
#  public $LAST_NAME   = 'Smith';
#  public $CUSTOMER    = 19;
#  public $COMPANY     = 'Umbrella Corp.';
#  public $ADDRESS1    = '1 York Ave';
#  public $ADDRESS2    = 'Apt 1A';
#  public $CITY        = 'New York';
#  public $STATE_REGION = 'NY';
#  public $COUNTRY      = 'USA';
#  public $POSTAL_CODE  = '10128';
#  public $LOCAL_PHONE  = '9009230000';
#  public $E_MAIL       = 'umbrella@example.com';
#  public $CC_NUMBER    = '1234';
#  public $CC_EXP_DATE  = '';
#  public $ACCOUNT_GROUP_ID = '1';
#  public $CCV          = '1234';
#  public $ACCOUNT      = 24;
#  public $ACCOUNT_ID   = 24; 
#}
#
#$customer = new test_customer_data;
#
#echo billing_insert_query(
#  array(
#    'customer' => $customer,
#    'login_name' => 'api_accounts',
#    'entry_type' => 1,
#    'node' => 'n_test',
#    'node_type' => 1,
#    'call_session_id' => 's_test',
#    'call_id' => '1234',
#    'quantity' => 1,
#    'description' => 'd_test',
#    'amount' => 0
#  )
#)."\n\n";

/**
 * billing_insert_query 
 * $params: description
 *          login_name
 *          entry_type
 *          node
 *          node_type
 *          call_session_id
 *          call_id
 *          quantity
 *          amount
 *         
 * @param  array $params
 * @return string SQL
 */
function billing_insert_query($params)
{
  $customer = $params['customer'];

  $params['detail'] = !empty( $params['detail'] ) ? mssql_escape_with_zeroes( $params['detail'] ) : 'NULL';
  $params['country_code'] = !empty( $params['country_code'] ) ? sprintf( '%d', $params['country_code'] ) : 'NULL';
  $params['rate_plan'] = !empty( $params['rate_plan'] ) ? mssql_escape( $params['rate_plan'] ) : 'NULL';

  $query = sprintf(
  "INSERT INTO BILLING
    (
    description,
    login_name,
    entry_type,
    account_id,
    account,
    account_group,
    node,
    node_type,
    start_date_time,
    connect_date_time,
    disconnect_date_time,
    call_session_id,
    call_id,
    quantity,
    amount,
    per_call_charge , per_minute_charge , per_call_surcharge , per_minute_surcharge ,
    actual_duration , conversion_rate , country_code , rate_plan , detail
    )
  VALUES
    (
    %s,
    %s,
    %d,
    %d,
    %s,
    %s,
    %s,
    %d,
    getutcdate(),
    getutcdate(),
    getutcdate(),
    %s,
    %s,
    %d,
    %d,
    0 , 0 , 0 , 0 ,
    0 , 0 , %s , %s , %s
    )",
    mssql_escape($params['description']),
    mssql_escape($params['login_name']),
    $params['entry_type'],
    $customer->ACCOUNT_ID,
    mssql_escape($customer->ACCOUNT),
    mssql_escape($customer->ACCOUNT_GROUP_ID),
    mssql_escape($params['node']),
    $params['node_type'],
    mssql_escape($params['call_session_id']),
    mssql_escape($params['call_id']),
    $params['quantity'],
    $params['amount'],
    $params['country_code'],
    $params['rate_plan'],
    $params['detail']
  );

  return $query;
}

/**
 * make_report_query 
 * @param  string $customer
 * @param  boolean $credit
 * @param  string $from
 * @param  string $to
 * @param  string $max
 * @return string SQL
 */
function make_report_query($customer, $credit, $from, $to, $max)
{
  $credit_extra = '';

  /* switch($credit) */
  /* { */
  /* case TRUE: */
  /*   //$credit_extra = "AND entry_types.description LIKE '%Credit%'"; */
  /*   $credit_extra = "AND amount < 0"; */
  /*   break; */
  /* case FALSE: */
  /*   //$credit_extra = "AND entry_types.description NOT LIKE '%Credit%'"; */
  /*   $credit_extra = "AND amount >= 0"; */
  /*   break; */
  /* } */

  return sprintf("SELECT TOP %s start_date_time as time,
  DATEDIFF(ss, '1970-01-01', start_date_time) as start_epoch,
  actual_duration, detail, billing.description,billing.ani, quantity,
  per_minute_charge as rate, amount,
  entry_types.description as category, billing.entry_type as category_order,
  billing.node_type
FROM billing JOIN entry_types ON entry_types.entry_type=billing.entry_type
WHERE
  account_id IN (SELECT account_id FROM accounts WHERE account = %s)
 AND
  DATEDIFF(ss, '1970-01-01', START_DATE_TIME) >= %s
 AND
  DATEDIFF(ss, '1970-01-01', START_DATE_TIME) <= %s
 %s",
                 mssql_escape($max),
                 mssql_escape_with_zeroes($customer),
                 mssql_escape($from),
                 mssql_escape($to),
                 $credit_extra);
}

?>
