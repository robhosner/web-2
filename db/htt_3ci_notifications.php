<?php

// HTT_3CI_NOTIFICATIONS

/**
 * htt_3ci_notifications_insert_query
 * $batch_data: MESSAGE_ID
 *              STATUS_ID
 *              STATUS_ID_DETAILED
 *              DATE_SUBMITTED
 *              HTT_MESSAGING_QUEUE_ID
 *              
 * @param  array $batch_data
 * @return string SQL
 */
function htt_3ci_notifications_insert_query($batch_data) {

  $separator = '';

  $sql = "
INSERT INTO HTT_3CI_NOTIFICATIONS (
  MESSAGE_ID,
  STATUS_ID,
  STATUS_ID_DETAILED,
  DATE_SUBMITTED,
  HTT_MESSAGING_QUEUE_ID
) VALUES ";

  foreach( $batch_data as $params ) {

    $sql .= $separator . sprintf("
(
  %s,
  %d,
  %d,
  %s,
  %d
) ",
      mssql_escape_with_zeroes($params['MESSAGE_ID']),
      $params['STATUS_ID'],
      $params['STATUS_ID_DETAILED'],
      $params['DATE_SUBMITTED'],
      $params['HTT_MESSAGING_QUEUE_ID']
    );

    $separator = ',';

  }

  return $sql;
}

/**
 * batch_insert_htt_3ci_notifications 
 * @param  array $batch_data
 * @return boolean
 */
function batch_insert_htt_3ci_notifications($batch_data) {

  $sql = htt_3ci_notifications_insert_query($batch_data);

  dlog('',$sql);

  return is_mssql_successful(logged_mssql_query($sql));
}

?>
