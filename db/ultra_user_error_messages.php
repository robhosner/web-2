<?php

/*
DB table [ULTRA].[USER_ERROR_MESSAGES]

Synopsis:

$x = get_ultra_user_error_messages('IT');
$x = get_ultra_user_error_messages('EN');
$x = get_ultra_user_error_messages('ES');
*/

/**
 * get_ultra_user_error_messages
 *
 * Retrieve error messages from DB table ULTRA.USER_ERROR_MESSAGES
 * If the message is not available in $language , default to 'EN'
 *
 * @return array
 */
function get_ultra_user_error_messages($language='EN')
{
  $ultra_user_error_messages = array();

  $language = strtoupper( $language );

  if ( ! in_array( $language , array('EN','ES','ZH') ) )
  {
    dlog('',"Language $language is not valid");
    return $ultra_user_error_messages;
  }

  $sql = ( $language == 'EN' )
         ?
         " SELECT ERROR_CODE , ".$language."_MESSAGE FROM ULTRA.USER_ERROR_MESSAGES "
         :
         " SELECT ERROR_CODE , CASE WHEN ".$language."_MESSAGE IS NULL THEN EN_MESSAGE ELSE ".$language."_MESSAGE END FROM ULTRA.USER_ERROR_MESSAGES "
         ;

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    foreach ( $query_result as $row )
      $ultra_user_error_messages[ $row[0] ] = $row[1] ;
  }

  return $ultra_user_error_messages;
}

/**
 * store_ultra_user_error_message
 * TODO
 * 
 * @param array $params
 */
function store_ultra_user_error_message($params)
{
 // TODO
}

/**
 * update_ultra_user_error_message
 * Updates ULTRA.USER_ERROR_MESSAGES with values in kva messages
 * 
 * @param  string $error_code Error code to update
 * @param  array $messages kva array (language => msg)
 * @return array (success => boolean, errors => array)
 */
function update_ultra_user_error_message($error_code, $messages)
{
  $return = array('errors' => array() , 'success' => FALSE);

  $clause = array('ERROR_CODE' => $error_code);
  $errors = search_ultra_user_error_messages($clause);

  if (count($errors))
    dlog('', 'Previous messages for ERROR_CODE: '. $error_code, $errors[0]);
  else
  {
    $return['errors'][] = "ERR_API_INTERNAL: Error code not found.";
    return $return;
  }

  $query = Ultra\Lib\DB\makeUpdateQuery('ULTRA.USER_ERROR_MESSAGES', $messages, $clause);

  dlog('', $query);

  $check = run_sql_and_check($query);

  if ($check)
    $return['success'] = TRUE;
  else
    $return['errors'][] = "ERR_API_INTERNAL: DB error.";

  return $return;
}

/**
 * search_ultra_user_error_messages
 * 
 * @param  array $params     kva array of clauses
 * @param  array $attributes attributes to select
 * @return object[] searched data from ULTRA.USER_ERROR_MESSAGES
 */
function search_ultra_user_error_messages($params, $attributes = NULL)
{
  $attrString = (!is_null($attributes) && is_array($attributes) && count($attributes))
    ? $attrString = implode(',', $attributes)
    : $attrString = '*';

  $query = 'SELECT ' . $attrString . ' FROM ULTRA.USER_ERROR_MESSAGES';

  if (count($params))
  {
    $query .= ' WHERE ';

    $i = 0;
    foreach ($params as $key => $val)
    {
      if ($i)
        $query .= ' AND ';
      $i++;

      $query .= ($key == 'en_message') 
        ? '(' . $key . ' LIKE "%' . $val . '%" OR DETAILS LIKE "%' . $val . '%")'
        : $key . ' = \'' . $val . '\'';
    }
  }

  $query .= ' ORDER BY ERROR_CODE ASC';

  dlog('', $query);

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

function get_ultra_user_error_message_by_error_code($error_code)
{
  $sql = \Ultra\Lib\DB\makeParameterizedSelectStatement(
    'ULTRA.USER_ERROR_MESSAGES',
    1,
    '*',
    array('ERROR_CODE' => $error_code)
  );

  return find_first($sql);
}

?>
