<?php

include_once('db.php');

# php component to access DB table CALL_SESSION_LOG

# synopsis

#echo call_session_log_query(
#  array(
#    'select_fields' => array('DATE_TIME','CALL_SESSION_CODE','DESCRIPTION','dbo.UTC_TO_PT(DATE_TIME) as DATE_TIME_PST','DETAIL as DIALED_NUMBER'),
#    'account'       => 1356937591,
#    'sql_limit'     => 10,
#    'days_ago'      => 30
#  )
#)."\n\n";

/**
 * call_session_log_query
 * $params: select_fields
 *          account
 *          days_ago
 * 
 * @param  array $params
 * @return string SQL
 */
function call_session_log_query($params)
{
  $select_fields = '*';

  $top_clause = get_sql_top_clause($params);

  $where = array();

  if ( isset($params['select_fields']) )
  {
    $select_fields = implode(',',$params['select_fields']);
  }

  if ( isset($params['account']) )
  {
    $where[] = sprintf(" ACCOUNT = %s ",mssql_escape_with_zeroes($params['account']));
  }

  if ( isset($params['days_ago']) )
  {
    $where[] = sprintf("DATE_TIME >= DATEADD(DD, -%d, GETDATE())",$params['days_ago']);
  }

  $where_clause = ' WHERE '.implode(" AND ", $where);

  return "
    SELECT   $top_clause $select_fields
    FROM     CALL_SESSION_LOG
    $where_clause
    ORDER BY DATE_TIME DESC
  ";
}

/**
 * call_session_log_data 
 * @param  string  $account
 * @param  integer $sql_limit
 * @param  integer $days_ago
 * @return object[]
 */
function call_session_log_data($account, $sql_limit = 50, $days_ago = 30)
{
  $call_session_log_query = call_session_log_query(
    array(
      'select_fields' => array('DATE_TIME',
                               'CALL_SESSION_CODE',
                               'DESCRIPTION as ERROR',
                               'dbo.UTC_TO_PT(DATE_TIME) as DATE_TIME_PST',
                               'DETAIL as DIALED_NUMBER'),
      'account'   => $account,
      'sql_limit' => $sql_limit,
      'days_ago'  => $days_ago
    )
  );

  return mssql_fetch_all_objects(logged_mssql_query($call_session_log_query));
}

?>
