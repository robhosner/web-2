<?php

/**
 * find_plans
 * @return array $plans
 */
function find_plans()
{
  $plans = array();
  foreach (mssql_fetch_all_objects(logged_mssql_query("select * from parent_cos")) as $plan)
  {
    $plans[$plan->cos_id] = $plan;
  }

  return $plans;
}

?>
