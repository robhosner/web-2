<?php

/**
 * record_customer_id
 *
 * @return boolean
 */
function record_customer_id( $customer_id , $univision_import_id )
{
  return update_univision_import(
    array(
      'customer_id'         => $customer_id,
      'univision_import_id' => $univision_import_id
    )
  );
}

/**
 * get_candidate_rows_to_suspend
 *
 * Get next rows to suspend.
 * We want to suspend customers with
 *  - STATUS     = 'ACTIVATED'
 *  - PLAN_STATE = 'SUSPENDED'
 *
 * @return array
 */
function get_candidate_rows_to_suspend( $day )
{
  $date = sprintf('Feb %d 2016 08:00:00:000AM',$day);

  $sql =
  "SELECT  TOP 2000  *,".mep_eligible_attribute()."
    FROM   ULTRA.UNIVISION_IMPORT i
    JOIN   HTT_CUSTOMERS_OVERLAY_ULTRA u
    ON     u.CUSTOMER_ID = i.CUSTOMER_ID
    WHERE  i.STATUS                = '".UNIVISION_IMPORT_STATUS_ACTIVATED."'
    AND    u.PLAN_STATE            = '".STATE_ACTIVE."'
    AND    i.PLAN_STATE            = '".STATE_SUSPENDED."'
    AND    i.IMPORT_FILE_DATE_TIME = '$date'";

  return mssql_fetch_all_objects( logged_mssql_query( $sql ) );
}

/**
 * get_candidate_rows_to_process
 *
 * Get next rows to process.
 * We want to process rows with status = 'ACTIVATED_TEMP' ( if enough time elapsed ) before processing rows with status = 'TODO'
 *
 * @return array
 */
function get_candidate_rows_to_process()
{
  $delayChangeSimMinutes = 30;

  $sql = "
    SELECT TOP 10 u.ULTRA_CODE, i.*,".mep_eligible_attribute()."
    FROM   ULTRA.UNIVISION_IMPORT i
    LEFT OUTER JOIN ULTRA.UNIVISION_CODE_TO_DEALER_CODE u
    ON     i.UNIVISION_CODE = u.UNIVISION_CODE
    WHERE  i.IMPORT_FILE_STATUS = '".UNIVISION_FILE_STATUS_ONGOING."'
    AND
    (
           ( i.STATUS = '".UNIVISION_IMPORT_STATUS_TODO."' )
           OR
           ( i.STATUS = '".UNIVISION_IMPORT_STATUS_ACTIVATED_TEMP."' AND DATEDIFF( minute , i.LAST_STATUS_DATE , getutcdate() ) >= $delayChangeSimMinutes )
    )
    ORDER BY
      i.STATUS           ASC, -- we want to process rows with status = 'ACTIVATED_TEMP' first
      i.LAST_STATUS_DATE ASC
  ";

  $result = mssql_fetch_all_objects( logged_mssql_query( $sql ) );

  shuffle( $result );

  return $result;
}

/**
 * get_candidate_rows_to_send_sms
 *
 * Return successfully migrated customers to which we should send SMS
 *
 * @return Array of Objects
 */
function get_candidate_rows_to_send_sms($day, $max = 2000)
{
  $sql = sprintf('SELECT TOP %d
    CUSTOMER_ID, MSISDN, LANGUAGE
    FROM ULTRA.UNIVISION_IMPORT WITH (NOLOCK) WHERE
    STATUS = \'%s\' AND SMS_SENT <> 1
    AND CAST(IMPORT_FILE_DATE_TIME AS DATE) = CAST(\'Feb %d 2016 08:00:00:000AM\' AS DATE)
    ORDER BY UNIVISION_IMPORT_ID',
    $max,
    UNIVISION_IMPORT_STATUS_ACTIVATED,
    $day
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_candidate_rows_to_adjust_data
 *
 * return successfully migrated customers whose data allotment should be adjusted
 * @param Integer migration calendar day
 * @param Integer max number of rows to fetch (default 3000 should completely cover daily batch)
 * @return Array of Objects
 */
function get_candidate_rows_to_adjust_data($day, $max = 2000)
{
  $sql = sprintf('SELECT TOP %d
    i.CUSTOMER_ID, i.MSISDN, i.ICCID_FULL, i.LANGUAGE, i.PLAN_NAME, i.DATA_ALLOTTED, i.DATA_USED, i.DATA_REMAINING
    FROM  ULTRA.UNIVISION_IMPORT i WITH (NOLOCK)
    JOIN  HTT_CUSTOMERS_OVERLAY_ULTRA u
    ON     u.CUSTOMER_ID = i.CUSTOMER_ID
    WHERE i.STATUS = \'%s\'
    AND   u.PLAN_STATE = \'Active\'
    AND   i.DATA_ADJUSTED <> 1
    AND   CAST(i.IMPORT_FILE_DATE_TIME AS DATE) = CAST(\'Feb %d 2016 08:00:00:000AM\' AS DATE)
    ORDER BY UNIVISION_IMPORT_ID',
    $max,
    UNIVISION_IMPORT_STATUS_ACTIVATED,
    $day
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * update_univision_file_status_paused
 *
 * Set IMPORT_FILE_STATUS to PAUSED
 *
 * @return boolean
 */
function update_univision_file_status_paused( $file_name )
{
  return update_univision_import(
    array(
      'file_name'          => $file_name,
      'import_file_status' => UNIVISION_FILE_STATUS_PAUSED
    )
  );
}

/**
 * update_univision_file_status_ongoing
 *
 * Set IMPORT_FILE_STATUS to ONGOING
 *
 * @return boolean
 */
function update_univision_file_status_ongoing( $file_name )
{ 
  return update_univision_import(
    array(
      'file_name'          => $file_name,
      'import_file_status' => UNIVISION_FILE_STATUS_ONGOING
    )
  );
} 
  
/**
 * update_univision_file_status_completed
 *
 * Set IMPORT_FILE_STATUS to COMPLETED
 *
 * @return boolean
 */
function update_univision_file_status_completed( $file_name )
{
  return update_univision_import(
    array(
      'file_name'          => $file_name,
      'import_file_status' => UNIVISION_FILE_STATUS_COMPLETED
    )
  );
}

/**
 * update_univision_file_status_aborted
 *
 * Set IMPORT_FILE_STATUS to ABORTED
 *
 * @return boolean
 */
function update_univision_file_status_aborted( $file_name )
{
  return update_univision_import(
    array(
      'file_name'          => $file_name,
      'import_file_status' => UNIVISION_FILE_STATUS_ABORTED
    )
  );
}

/**
 * update_univision_import_invalid
 *
 * Set STATUS to INVALID after static validation failed (example: bad zip code)
 *
 * @return boolean
 */
function update_univision_import_invalid( $univision_import_id , $last_error=NULL )
{
  if ( empty( $last_error ) )
    $last_error = UNIVISION_IMPORT_STATUS_INVALID;

  return update_univision_import(
    array(
      'univision_import_id' => $univision_import_id,
      'status'              => UNIVISION_IMPORT_STATUS_INVALID,
      'last_error'          => $last_error
    )
  );
}

/**
 * update_univision_import_port_called
 *
 * Set STATUS to PORT_CALLED: ActivatedSubscriber has been invoked and we are waiting for the inbound notification
 *
 * @return boolean
 */
function update_univision_import_port_called( $univision_import_id )
{
  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_PORT_CALLED,
      'last_process_id'       => getmypid(),
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ActivatedSubscriber synch success.'
    )
  );
}

/**
 * update_univision_import_activated_temp
 *
 * Set STATUS to ACTIVATED_TEMP: ActivatedSubscriber has been invoked and we received a successful inbound notification
 *
 * @return boolean
 */
function update_univision_import_activated_temp( $univision_import_id )
{
  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_ACTIVATED_TEMP,
      'last_process_id'       => getmypid(),
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ActivatedSubscriber asynch success.'
    )
  );
}

/**
 * update_univision_import_port_error
 *
 * Set STATUS to PORT_ERROR: ActivatedSubscriber has been invoked and it failed
 *
 * @return boolean
 */
function update_univision_import_port_error( $univision_import_id , $last_error=NULL )
{
  if ( empty( $last_error ) )
    $last_error = UNIVISION_IMPORT_STATUS_PORT_ERROR;

  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_PORT_ERROR,
      'last_process_id'       => getmypid(),
      'last_error'            => $last_error,
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ActivatedSubscriber synch failure.'
    )
  );
}

/**
 * update_univision_import_port_failed
 *
 * Set STATUS to PORT_FAILED: ActivatedSubscriber has been invoked but received a failure with inbound notification
 *
 * @return boolean
 */
function update_univision_import_port_failed( $univision_import_id , $last_error=NULL )
{
  if ( empty( $last_error ) )
    $last_error = UNIVISION_IMPORT_STATUS_PORT_FAILED;

  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_PORT_FAILED,
      'last_process_id'       => getmypid(),
      'last_error'            => $last_error,
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ActivatedSubscriber asynch failure.'
    )
  );
}

/**
 * update_univision_import_changesim_called
 *
 * Set STATUS to CHANGESIM_CALLED: ChangeSIM has been invoked and we are waiting for the inbound notification
 *
 * @return boolean
 */
function update_univision_import_changesim_called( $univision_import_id )
{
  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_CHANGESIM_CALLED,
      'last_process_id'       => getmypid(),
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ChangeSIM synch success.'
    )
  );
}

/**
 * update_univision_import_activated
 *
 * Set STATUS to ACTIVATED: ActivatedSubscriber and ChangeSIM have been successful
 *
 * @return boolean
 */
function update_univision_import_activated( $univision_import_id )
{
  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_ACTIVATED,
      'last_process_id'       => getmypid(),
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] Completed.'
    )
  );
}

/**
 * update_univision_import_changesim_error
 *
 * Set STATUS to CHANGESIM_ERROR: ChangeSIM has been invoked and it failed
 *
 * @return boolean
 */
function update_univision_import_changesim_error( $univision_import_id , $last_error=NULL )
{
  if ( empty( $last_error ) )
    $last_error = UNIVISION_IMPORT_STATUS_CHANGESIM_ERROR;

  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_CHANGESIM_ERROR,
      'last_error'            => $last_error,
      'last_process_id'       => getmypid(),
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ChangeSIM synch failure.'
    )
  );
}

/**
 * update_univision_import_changesim_failed
 *
 * Set STATUS to CHANGESIM_FAILED: ChangeSIM have been successful but we received a failure with inbound notification
 *
 * @return boolean
 */
function update_univision_import_changesim_failed( $univision_import_id , $last_error=NULL )
{
  if ( empty( $last_error ) )
    $last_error = UNIVISION_IMPORT_STATUS_CHANGESIM_FAILED;

  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_CHANGESIM_FAILED,
      'last_error'            => $last_error,
      'last_process_id'       => getmypid(),
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] ChangeSIM asynch failure.'
    )
  );
}

/**
 * update_univision_import_changesim_retry
 *
 * Set STATUS to ACTIVATED_TEMP: Attempt to retry change SIM
 *
 * @return boolean
 */
function update_univision_import_changesim_retry( $univision_import_id )
{
  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_ACTIVATED_TEMP,
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] Set to retry ChangeSIM.'
    )
  );
}

/**
 * update_univision_import_port_retry
 *
 * Set STATUS to TODO: Attempt to retry port
 *
 * @return boolean
 */
function update_univision_import_port_retry( $univision_import_id )
{
  return update_univision_import(
    array(
      'univision_import_id'   => $univision_import_id,
      'status'                => UNIVISION_IMPORT_STATUS_TODO,
      'import_history_append' => ' [' . gmdate("Y-m-d H:i:s", time() ) . '] Set to retry port.'
    )
  );
}

/**
 * select_univision_import_by_import_file_status
 * 
 * Returns a univision_import record as obj where IMPORT_FILE_STATUS = $status
 *
 * @param  string $status
 * @return Object || NULL
 */
function select_univision_import_by_import_file_status($status)
{
  $sql = sprintf("DECLARE @IMPORT_FILE_STATUS VARCHAR(50) = '%s';
    SELECT IMPORT_FILE_STATUS
    FROM   ULTRA.UNIVISION_IMPORT with (nolock)
    WHERE  IMPORT_FILE_STATUS = @IMPORT_FILE_STATUS",
    $status
  );

  return find_first($sql);
}

/**
 * select_univision_import_file_by_filename
 * 
 * Returns univision_import records where IMPORT_FILE_NAME = $filename
 *
 * @param  string $filename
 * @return array
 */
function select_univision_import_file_by_filename($filename)
{
  $sql = sprintf(
    "DECLARE @FILENAME VARCHAR(50) = '%s';
      SELECT *
      FROM   ULTRA.UNIVISION_IMPORT with (nolock)
      WHERE  IMPORT_FILE_NAME = @FILENAME",
    $filename
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * select_univision_import_file_status_by_filename
 * 
 * Returns a univision_import record as obj where IMPORT_FILE_NAME = $filename
 *
 * @param  string $filename
 * @return Object || NULL
 */
function select_univision_import_file_status_by_filename($filename)
{
  $sql = sprintf("DECLARE @FILENAME VARCHAR(50) = '%s';
    SELECT IMPORT_FILE_STATUS
    FROM   ULTRA.UNIVISION_IMPORT with (nolock)
    WHERE  IMPORT_FILE_NAME = @FILENAME GROUP BY IMPORT_FILE_STATUS",
    $filename
  );

  return find_first($sql);
}

/**
 * select_univision_import_by_date_query
 * 
 * Returns SQL statement to retrieve univision_import records within a given datetime range
 *
 * @param  string $start_datetime
 * @param  string $end_datetime
 * @return string
 */
function select_univision_import_by_date_query( $start_datetime, $end_datetime )
{
  return sprintf( 'DECLARE @START_DATETIME DATETIME = "%s";', $start_datetime ).
         sprintf( 'DECLARE @END_DATETIME   DATETIME = "%s";', $end_datetime   ).
         'SELECT IMPORT_FILE_NAME, IMPORT_FILE_STATUS, IMPORT_FILE_DATE_TIME, CONVERT(date, IMPORT_FILE_DATE_TIME) AS IMPORT_FILE_DATE, COUNT(*) AS NUM_IMPORTED
            FROM ULTRA.UNIVISION_IMPORT with (nolock)
            WHERE IMPORT_FILE_DATE_TIME BETWEEN @START_DATETIME AND @END_DATETIME
            GROUP BY IMPORT_FILE_NAME, IMPORT_FILE_STATUS, IMPORT_FILE_DATE_TIME, CONVERT(date, IMPORT_FILE_DATE_TIME)';
}

/**
 * univision_import_select_query
 *
 * Generic SQL query to retrieve ULTRA.UNIVISION_IMPORT rows
 *
 * @return string
 */
function univision_import_select_query( $params )
{
  $where = array();

  if ( ! empty($params['univision_import_id']) )
    $where[] = sprintf( " UNIVISION_IMPORT_ID = %d " , intval($params['univision_import_id']) );

  if ( ! empty($params['msisdn']) )
    $where[] = sprintf( " MSISDN              = %s " , mssql_escape_with_zeroes($params['msisdn']) );

  if ( ! empty($params['iccid_full']) )
    $where[] = sprintf( " ICCID_FULL          = %s " , mssql_escape_with_zeroes($params['iccid_full']) );

  if ( ! empty($params['temp_iccid_full']) )
    $where[] = sprintf( " TEMP_ICCID_FULL     = %s " , mssql_escape_with_zeroes($params['temp_iccid_full']) );

  return "SELECT *,".mep_eligible_attribute()."
    FROM   ULTRA.UNIVISION_IMPORT
    WHERE ".implode(' AND ', $where);
}

/**
 * mep_eligible_attribute
 *
 * Computed attribute MEP_ELIGIBLE
 *
 * @return string
 */
function mep_eligible_attribute()
{
  return "
           CASE WHEN
             RENEWAL_DATE BETWEEN 'Feb  1 2016 00:00:00:001AM' AND 'Feb 11 2016 11:59:59:999PM'
           THEN 1
           ELSE 0
           END AS MEP_ELIGIBLE";
}

/**
 * update_univision_import
 *
 * $params: 
 *   - file_name
 *   - univision_import_id
 *   - status
 *   - import_file_status
 *   - last_process_id
 *   - last_error
 *
 * @param  array $params
 * @return boolean
 */
function update_univision_import( $params )
{
  $set_clause   = array();
  $where_clause = array();

  if ( ! empty( $params['file_name'] ) )
    $where_clause[] = ' IMPORT_FILE_NAME = '.mssql_escape_with_zeroes($params['file_name']);

  if ( ! empty( $params['univision_import_id'] ) )
    $where_clause[] = sprintf(" UNIVISION_IMPORT_ID = %d ",$params['univision_import_id']);

  if ( isset($params['status']) )
  {
    $set_clause[] = sprintf( " STATUS = %s " , mssql_escape_with_zeroes($params['status']) );
    $set_clause[] = ' LAST_STATUS_DATE = GETUTCDATE() ';
  }

  if ( isset($params['import_file_status']) )
  {
    $set_clause[] = sprintf( " IMPORT_FILE_STATUS = %s " , mssql_escape_with_zeroes($params['import_file_status']) );

    if ( $params['import_file_status'] == 'COMPLETED' )
      $set_clause[] = ' COMPLETED_MIGRATION_DATE = GETUTCDATE() ';
  }

  if ( isset($params['last_process_id']) )
    $set_clause[] = sprintf( " LAST_PROCESS_ID = %d ",$params['last_process_id']);

  if ( ! empty($params['data_adjusted']) && ! empty($params['customer_id']))
  {
    $set_clause[] = ' DATA_ADJUSTED = ' . ($params['data_adjusted'] ? '1' : '0');
    $where_clause[] = sprintf(" CUSTOMER_ID = %d ", $params['customer_id']);
    unset($params['customer_id']);
  }

  if ( ! empty($params['sms_sent']) && ! empty($params['customer_id']))
  {
    $set_clause[] = ' SMS_SENT = ' . ($params['sms_sent'] ? '1' : '0');
    $where_clause[] = sprintf(" CUSTOMER_ID = %d ", $params['customer_id']);
    unset($params['customer_id']);
  }

  if ( isset($params['customer_id']) )
    $set_clause[] = sprintf( " CUSTOMER_ID = %d ",$params['customer_id']);

  if ( array_key_exists('last_error', $params) )
  { 
    $set_clause[] = sprintf( " LAST_ERROR = %s " , mssql_escape_with_zeroes($params['last_error']) );

    if ( empty( $params['last_error'] ) )
      $set_clause[] = ' LAST_ERROR_DATE = NULL ';
    else
      $set_clause[] = ' LAST_ERROR_DATE = GETUTCDATE() ';
  }

  if ( isset($params['import_history_append']) )
    $set_clause[] = sprintf( " IMPORT_HISTORY  = CAST(IMPORT_HISTORY AS NVARCHAR(MAX)) + '%s' " , mssql_escape_raw( $params['import_history_append'] ) );

  $sql = "
UPDATE ULTRA.UNIVISION_IMPORT
SET    ".implode(',',$set_clause).'
WHERE  '.implode(' AND ', $where_clause);

  return run_sql_and_check( $sql );
}

/**
 * add_to_univision_import
 *
 * Function to update UNIVISION_IMPORT
 *
 * @return boolean
 */
function add_to_univision_import( $params )
{ 
  dlog('', "(%s)", func_get_args());
  
  $sql = univision_import_insert_query( $params );
  
  dlog('', "$sql" );

  return run_sql_and_check( $sql );
}

/**
 * univision_import_insert_query
 *
 * SQL statement to update UNIVISION_IMPORT
 *
 * @return string
 */
function univision_import_insert_query( $params )
{
  $balance        = sprintf( "%d" , $params['balance']        );
  $data_allotted  = sprintf( "%d" , $params['data_allotted']  );
  $data_used      = sprintf( "%d" , $params['data_used']      );
  $data_remaining = sprintf( "%d" , $params['data_remaining'] );
  $house_account  = sprintf( "%d" , $params['house_account']  );
  $demo_account   = sprintf( "%d" , $params['demo_account']   );
  $migration_date = empty( $params['migration_date'] ) ? 'NULL' : "'{$params['migration_date']}'" ;
  $address        = empty( $params['address']        ) ? 'NULL' : mssql_escape_with_zeroes($params['address']) ;
  $city           = empty( $params['city']           ) ? 'NULL' : mssql_escape_with_zeroes($params['city']) ;
  $state          = empty( $params['state']          ) ? 'NULL' : mssql_escape_with_zeroes($params['state']) ;
  $first_name     = empty( $params['first_name']     ) ? 'NULL' : mssql_escape_with_zeroes($params['first_name']) ;
  $last_name      = empty( $params['last_name']      ) ? 'NULL' : mssql_escape_with_zeroes($params['last_name']) ;
  $email          = empty( $params['email']          ) ? 'NULL' : mssql_escape_with_zeroes($params['email']) ;
  $file_date_time = empty( $params['file_date_time'] ) ? 'GETDATE()' : "'".$params['file_date_time']."'" ;
  $monthly_renewal_target_request = empty( $params['monthly_renewal_target'] ) ? 'NULL' : 'GETUTCDATE()' ;
  $monthly_renewal_target         = empty( $params['monthly_renewal_target'] ) ? 'NULL' : mssql_escape_with_zeroes($params['monthly_renewal_target']) ;
  $renewal_date    = "'".$params['renewal_date']." 8:00:00:000AM'";

  if ( empty($params['activation_date']) )
    $activation_date = 'NULL';
  else
    $activation_date = "'".$params['activation_date']." 8:00:00:000AM'";

  return "IF NOT EXISTS
          (
            SELECT UNIVISION_IMPORT_ID
            FROM   ULTRA.UNIVISION_IMPORT
            WHERE  ICCID_FULL      = ".mssql_escape_with_zeroes($params['iccid_full'])."
            OR     TEMP_ICCID_FULL = ".mssql_escape_with_zeroes($params['temp_iccid_full'])."
            OR     MSISDN          = ".mssql_escape_with_zeroes($params['msisdn'])."
          )
  BEGIN
  INSERT INTO ULTRA.UNIVISION_IMPORT
   (
STATUS,
LAST_PROCESS_ID,
LAST_ERROR,
LAST_ERROR_DATE,
IMPORT_FILE_NAME,
IMPORT_FILE_DATE_TIME,
IMPORT_FILE_STATUS,
COMPLETED_MIGRATION_DATE,
ICCID_FULL,
TEMP_ICCID_FULL,
MSISDN,
IMEI,
BALANCE,
FEATURE,
ZIP_CODE,
ADDRESS,
CITY,
STATE,
FIRST_NAME,
LAST_NAME,
EMAIL,
ACCOUNT_NUMBER,
ACCOUNT_PIN,
PLAN_NAME,
LANGUAGE,
PLAN_STATE,
DATA_ALLOTTED,
DATA_USED,
DATA_REMAINING,
UNIVISION_CODE,
SUBS_NUMBER,
SUBS_ID,
RENEWAL_DATE,
HOUSE_ACCOUNT,
DEMO_ACCOUNT,
MONTHLY_RENEWAL_TARGET,
MONTHLY_RENEWAL_TARGET_REQUEST,
ACTIVATION_DATE
   ) VALUES (
'TODO',
NULL,
NULL,
NULL,
".mssql_escape_with_zeroes($params['file_name']).",
$file_date_time,
'INITIALIZED',
NULL,
".mssql_escape_with_zeroes($params['iccid_full']).",
".mssql_escape_with_zeroes($params['temp_iccid_full']).",
".mssql_escape_with_zeroes($params['msisdn']).",
".mssql_escape_with_zeroes($params['imei']).",
$balance,
".mssql_escape_with_zeroes($params['feature']).",
".mssql_escape_with_zeroes($params['zip_code']).",
$address,
$city,
$state,
$first_name,
$last_name,
$email,
".mssql_escape_with_zeroes($params['account_number']).",
".mssql_escape_with_zeroes($params['account_pin']).",
".mssql_escape_with_zeroes($params['plan_name']).",
".mssql_escape_with_zeroes($params['language']).",
".mssql_escape_with_zeroes($params['plan_state']).",
$data_allotted,
$data_used,
$data_remaining,
".mssql_escape_with_zeroes($params['univision_code']).",
".mssql_escape_with_zeroes($params['subs_number']).",
".mssql_escape_with_zeroes($params['subs_id']).",
$renewal_date,
$house_account,
$demo_account,
$monthly_renewal_target,
$monthly_renewal_target_request,
$activation_date
   )
  END";
}

// ULTRA.UNIVISION_CODE_TO_DEALER_CODE
function get_dealer_code_from_univision_code( $univision_code )
{
  $sql = sprintf("
    SELECT ULTRA_CODE
    FROM   ULTRA.UNIVISION_CODE_TO_DEALER_CODE
    WHERE  UNIVISION_CODE = %s",
    mssql_escape_with_zeroes($univision_code)
  );

  $result = find_first($sql);

  if ( empty( $result ) )
    return NULL;
  else
    return $result->ULTRA_CODE;
}

/**
 * get_univision_import_by_date
 *
 * @param  string $date Jan 11 2016
 * @return array
 */
function get_univision_import_by_date($date)
{
  $sql = "DECLARE @IMPORT_DATE DATETIME = CONVERT(DATETIME, '$date', 109);
    SELECT * FROM ULTRA.UNIVISION_IMPORT with (nolock)
    WHERE CAST(IMPORT_FILE_DATE_TIME as DATE) = CAST(@IMPORT_DATE as DATE)";

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_univision_import_statuses_by_filename
 *
 * @param  string $filename
 * @return array
 */
function get_univision_import_statuses_by_filename($filename)
{
  $sql = sprintf('
    DECLARE @FILENAME VARCHAR(50) = %s;
    SELECT STATUS, COUNT(*) AS COUNT FROM ULTRA.UNIVISION_IMPORT with (nolock)
    WHERE IMPORT_FILE_NAME = @FILENAME
    GROUP BY STATUS
    ',
    mssql_escape_with_zeroes($filename)
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * check_if_univision_code_exists
 *
 * @param  string $univision_code
 * @return bool
 */
function check_if_univision_code_exists($univision_code)
{
  $sql = sprintf("SELECT TOP 1 UNIVISION_CODE
    FROM ULTRA.UNIVISION_CODE_TO_DEALER_CODE with (nolock)
    WHERE UNIVISION_CODE = %s", mssql_escape_with_zeroes($univision_code));

  return (find_first($sql)) ? TRUE : FALSE;
}

