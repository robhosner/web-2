<?php

// Those queries join customer data tables.
// They belong to the pre-DB layer logic, or lack of it.

/*
$x = find_ultra_customers_from_details( array(
  'email' => 'tester@hometowntelecom.com'
  'first_name' => 'Bara%'
  'city' => 'New York'
  'zipcode' => '12345'
  'address' => 'test a 1'
) );


$x = find_ultra_customers_from_plan_and_date_range(
  array(
    #'plan_state' => 'Neutral',
    'plan_state' => 'Active',
    #'plan_state' => 'Cancelled',
    #'plan_state' => 'Port-In Denied',
    #'plan_state' => 'Port-In Requested',
    #'plan_state' => 'Pre-Funded',
    #'plan_state' => 'Promo Unused',
    #'plan_state' => 'Provisioned',
    #'plan_state' => 'Suspended',
    'date_from'  => '12-05-2013',
    'date_to'    => '12-05-2013'
  )
);
*/

require_once 'Ultra/Lib/Util/Hash.php';
require_once 'lib/util-common.php';

/**
 * make_find_status_customer_select_query 
 * @param  string $v
 * @param  integer $global_tz_offset
 * @param  integer $global_activation_tz_offset
 * @param  integer $global_dblocal_tz_offset
 * @return string SQL
 */
function make_find_status_customer_select_query($v,
                                                $global_tz_offset,
                                                $global_activation_tz_offset,
                                                $global_dblocal_tz_offset)
{
  $reverse_dblocal_tz_offset = -1 * ($global_tz_offset-$global_dblocal_tz_offset);
  $reverse_tz_offset = 0;
  $reverse_activation_tz_offset = -1 * $global_activation_tz_offset;

  return sprintf("
  select right(c.CC_NUMBER, 4) AS CC_LAST_4,
         e_mail as email,
         e_mail as EMAIL,
         c.CUSTOMER_ID, c.CUSTOMER, c.FIRST_NAME, c.LAST_NAME, a.COS_ID,
         c.ADDRESS1, c.ADDRESS2, c.CITY, c.STATE_REGION, c.POSTAL_CODE,
         c.CC_ADDRESS1, c.CC_ADDRESS2, c.CC_CITY, c.CC_STATE_REGION, c.CC_POSTAL_CODE, c.CC_COUNTRY,
         c.LOCAL_PHONE, c.CC_EXP_DATE, c.LOGIN_NAME,
         a.PACKAGED_BALANCE1, a.ACCOUNT, a.ACCOUNT_ID,
         (a.PACKAGED_BALANCE1/60) AS PACKAGED_BALANCE1_OVER_60,

--         DATEADD(second, 1326304856, '1970-01-01') as TESTTIME,
--         DATEDIFF(ss, '1970-01-01', GETUTCDATE()) as revtime,

         CASE
          WHEN last_recharge_date_time IS NULL THEN CONVERT(VARCHAR, DATEADD(d, 91, DATEADD(hh, $global_dblocal_tz_offset, creation_date_time)), 110)
          ELSE CONVERT(VARCHAR, DATEADD(d, 91, DATEADD(hh, $global_tz_offset, last_recharge_date_time)), 110)
         END AS NEXT_RECHARGE_90,

         CASE
          WHEN last_recharge_date_time IS NULL THEN DATEDIFF(ss, '1970-01-01', DATEADD(d, 91, DATEADD(hh, $reverse_dblocal_tz_offset, creation_date_time)))
          ELSE DATEDIFF(ss, '1970-01-01', DATEADD(d, 91, DATEADD(hh, $reverse_tz_offset, last_recharge_date_time)))
         END AS NEXT_RECHARGE_90_epoch,

        CONVERT(VARCHAR,
                DATEADD(m,
                        CASE WHEN DATEPART(d, DATEADD(hh, $global_activation_tz_offset, ACTIVATION_DATE_TIME)) > DATEPART(d, DATEADD(hh, $global_tz_offset, GETUTCDATE())) THEN 0
                        ELSE 1
                        END + DATEDIFF(month,
                                       DATEADD(hh, $global_activation_tz_offset, ACTIVATION_DATE_TIME),
                                       DATEADD(hh, $global_tz_offset, GETUTCDATE())),
                        DATEADD(hh, $global_activation_tz_offset, ACTIVATION_DATE_TIME)),
                110) AS NEXT_RENEWAL_DATE,

-- TODO DATEDIFF(ss, '1970-01-01', NEXT_RENEWAL_DATE) AS NEXT_RENEWAL_DATE_epoch,

         DATEDIFF(ss, '1970-01-01', DATEADD(m, 1, GETUTCDATE())) AS HEY_ITS_A_MONTH_LATER_epoch,
         DATEDIFF(ss, '1970-01-01', GETUTCDATE()) AS HEY_ITS_TODAY_epoch,

         CONVERT(VARCHAR, DATEADD(d, 91, CAST(DATEADD(hh, $global_tz_offset, GETUTCDATE()) AS date)), 110) AS HEY_ITS_91_DAYS_LATER,
         CONVERT(VARCHAR, DATEADD(m, 1, CAST(DATEADD(hh, $global_tz_offset, GETUTCDATE()) AS date)), 110) AS HEY_ITS_A_MONTH_LATER,
         CONVERT(VARCHAR, CAST(DATEADD(hh, $global_tz_offset, GETUTCDATE()) AS date), 110) AS HEY_ITS_TODAY,

         DATEDIFF(ss, '1970-01-01', DATEADD(hh, $reverse_dblocal_tz_offset, creation_date_time)) as CREATION_DATE_TIME_epoch,
         CONVERT(VARCHAR, DATEADD(hh, $global_dblocal_tz_offset, creation_date_time), 110) as CREATION_DATE_TIME,

         DATEDIFF(ss, '1970-01-01', ACTIVATION_DATE_TIME) as ACTIVATION_DATE_TIME_epoch,
         DATEADD(hh, $global_activation_tz_offset, ACTIVATION_DATE_TIME) as ACTIVATION_DATE_TIME_adjusted,

         (select HTT_ACCESSNUMBER_NEWDIDMAP.new_dnis from HTT_ACCESSNUMBER_NEWDIDMAP where ani_mod = RIGHT(cast('%s' as varchar(50)),2) and incoming_dnis='HTT-MAY-NUMBERS') as ACCESS_NUMBER,

         a.BALANCE, a.CREDIT_LIMIT, c.user_8, c.COUNTRY, pc.plan_cost, pc.minutes, pc.cos
FROM customers c, accounts a
LEFT OUTER JOIN parent_cos pc ON a.cos_id=pc.cos_id
WHERE c.customer_id=a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s')
",
                 mssql_escape($v),
                 mssql_escape($v),
                 mssql_escape($v));
}

/**
 * us_customer_count_query 
 * @return string SQL
 */
function us_customer_count_query()
{
  return "
select count(*)
from customers c, accounts a, parent_cos pc
where c.customer_id=a.customer_id
and a." . account_enabled_clause(TRUE) . " 
and c.country in ('US','USA')
and a.cos_id=pc.cos_id
and pc.site='IndiaLD'
";
}

/**
 * ultra_customer_select_query 
 * get customer data from CUSTOMERS and HTT_CUSTOMERS_OVERLAY_ULTRA
 * $params: select_fields
 *          WHERE:current_mobile_number
 *          WHERE:login_name
 * 
 * @param  array $params
 * @return string SQL
 */
function ultra_customer_select_query($params)
{
  $select_fields = implode(',',$params['select_fields']);

  $where[] = " u.customer_id = c.customer_id ";

  if ( isset($params['WHERE:current_mobile_number']) )
  {
    $where[] = sprintf(" u.current_mobile_number = '%s' ",mssql_escape($params['WHERE:current_mobile_number']));
  }

  if ( isset($params['WHERE:login_name']) )
  {
    $where[] = sprintf(" LOGIN_NAME = %s ",mssql_escape_with_zeroes($params['WHERE:login_name']));
  }

  if ( isset($params['WHERE:customer_id']) )
  {
    $where[] = sprintf(' c.customer_id = %d', $params['WHERE:customer_id']);
  }

  $where_clause = ' WHERE '.implode(" AND ", $where);

  return "
    SELECT $select_fields
    FROM   customers c,
           htt_customers_overlay_ultra u
    $where_clause
  ";
}

/**
 * make_find_status_customer_query_ext
 *
 * note that here and elsewhere mssql_escape will corrupt a
 * non-numeric customer ID because it will pop an escaped string
 * inside quotes, which is *exactly* what we want
 * 
 * @param  string $v
 * @return array (SQL)
 */
function make_find_status_customer_query_ext($v)
{
  /*  */
  return array(
    sprintf("SELECT a.account_id, aa.alias
FROM account_aliases aa, accounts a, customers c
WHERE aa.dnis = '*' AND aa.account_alias_type = 1
AND aa.account_id = a.account_id AND c.customer_id=a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s')
",
            mssql_escape($v),
            mssql_escape($v)),
    sprintf("SELECT a.account_id, speed.entry, speed.number, speed.description
FROM speed, accounts a, customers c
WHERE speed.account = a.account AND c.customer_id=a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s')
",
            mssql_escape($v),
            mssql_escape($v)),
    sprintf("SELECT a.account_id, ad.autodialer_id, ad.dnis, ad.destination, ad.description
FROM autodialer ad, accounts a, customers c
WHERE ad.account = a.account AND c.customer_id=a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s')
",
            mssql_escape($v),
            mssql_escape($v)),
    );
}

/**
 * minimal_customers_select_attributes 
 * @return string SQL
 */
function minimal_customers_select_attributes()
{
  // PROD-2332: use ENABLED or ACCOUNT_STATUS_TYPE
  list($enabled) = array_map('trim', explode('=', account_enabled_assignment(TRUE)));

  # see MVNO-1015
  return "
  c.CUSTOMER_ID,
  CUSTOMER,
  FIRST_NAME,
  LAST_NAME,
  ADDRESS1,
  ADDRESS2,
  CITY,
  STATE_REGION,
  POSTAL_CODE,
  COUNTRY,
  LOCAL_PHONE,
  E_MAIL,
  LOGIN_NAME,
  LOGIN_PASSWORD,
  CC_NUMBER,
  CC_EXP_DATE,
  CC_NAME,
  CC_ADDRESS1,
  CC_ADDRESS2,
  CC_CITY,
  CC_COUNTRY,
  CC_STATE_REGION,
  CC_POSTAL_CODE,
  CCV,
  ACCOUNT_ID,
  ACCOUNT,
  $enabled,
  BILLING_TYPE,
  CREATION_DATE_TIME,
  ACTIVATION_DATE_TIME,
  BALANCE,
  PACKAGED_BALANCE1,
  a.COS_ID,
  DATEDIFF(ss, '1970-01-01', u.plan_started ) plan_started_epoch,
  u.*";
}

/**
 * find_ultra_customers_from_plan_and_date_range 
 *
 * $params['date_from'] is required and format should be dd-mm-yyyy (PST)
 * $params['date_to']   is required and format should be dd-mm-yyyy (PST)
 * $params['plan_state'] is required
 * $params['select_attributes'] is not required
 * 
 * @param  array $params
 * @return object Result
 */
function find_ultra_customers_from_plan_and_date_range( $params )
{
  $sql = make_find_ultra_customers_from_plan_and_date_range( $params );

  if ( ! $sql )
    return make_error_Result('Invalid search parameters');

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
  {
    // remove duplicates by customer_id due to HTT_PORTIN_LOG join
    $data = remove_duplicates_by_field( $data , 'customer_id' );

    // compute plan info from cos_id
    foreach( $data as $row )
    {
      $row->plan_amount = '';
      $row->plan_name   = '';

      if ( isset($row->COS_ID) )
      {
        $row->plan_amount = get_plan_cost_from_cos_id($row->COS_ID) / 100;
        $row->plan_name   = cos_id_plan_description( $row->COS_ID );
      }
    }

    return make_ok_Result( $data );
  }

  return make_error_Result('No data found');
}

/**
 * find_ultra_customers_from_details
 * see make_find_ultra_customers_from_details
 *         
 * @param  array $params
 * @return object Result
 */
function find_ultra_customers_from_details( $params )
{
  $sql = make_find_ultra_customers_from_details( $params );

  if ( ! $sql )
    return make_error_Result('No search parameters found');

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) && count($data) )
  {
    // remove duplicates by customer_id due to HTT_PORTIN_LOG join
    $data = remove_duplicates_by_field( $data , 'customer_id' );

    return make_ok_Result( $data );
  }

  return make_error_Result('No data found');
}

/**
 * make_find_ultra_customers_from_plan_and_date_range
 *
 * $params['date_from'] is required and format should be dd-mm-yyyy (PST)
 * $params['date_to']   is required and format should be dd-mm-yyyy (PST)
 * $params['plan_state'] is required
 * $params['select_attributes'] is not required
 *
 * @param  $params
 * @return string SQL
 */
function make_find_ultra_customers_from_plan_and_date_range( $params )
{
  $select_attributes = minimal_customers_select_attributes();

  $additional_join   = '';

  if ( isset( $params['select_attributes'] ) )
    $select_attributes = implode(" , ", $params['select_attributes']);

  $clause = ( $params['plan_state'] == 'Port-in Attempted' )
            ?
            " AND plan_state IN ( 'Port-In Requested' , 'Port-In Denied') "
            :
            sprintf(" AND plan_state = %s ",
              mssql_escape_with_zeroes( $params['plan_state'] )
            );

  $params['date_from'] = date_from_pst_to_utc( $params['date_from'] , 0 );
  $params['date_to']   = date_from_pst_to_utc( $params['date_to']   , 1 );

  if ( ( $params['plan_state'] == 'Neutral' ) || ( $params['plan_state'] == 'Provisioned' ) || ( $params['plan_state'] == 'Promo Unused' ) || ( $params['plan_state'] == 'Pre-Funded' ) )
  {
    // check ACCOUNTS.CREATION_DATE_TIME

    $clause .= " AND CREATION_DATE_TIME BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' ";
  }
  elseif ( $params['plan_state'] == 'Cancelled' )
  {
    // check HTT_CANCELLATION_REASONS.DEACTIVATION_DATE

    $additional_join .=
      ' LEFT OUTER JOIN htt_cancellation_reasons r ON c.customer_id = r.customer_id ';

    $clause .= " AND r.DEACTIVATION_DATE IS NOT NULL AND r.DEACTIVATION_DATE BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' ";
  }
  elseif ( $params['plan_state'] == 'Active' )
  {
    // check HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STARTED

    $clause .= " AND PLAN_STARTED BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' ";
  }
  elseif ( $params['plan_state'] == 'Suspended' )
  {
    // check HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_EXPIRES

    $clause .= " AND PLAN_EXPIRES BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' ";
  }
  elseif ( ( $params['plan_state'] == 'Port-In Requested' )
        || ( $params['plan_state'] == 'Port-in Attempted' ) // Port-In Requested OR Port-In Denied
        )
  {
    // port_status_date (from htt_portin_log). If that value is null, then return the creation_date_time on customers.

    $clause .= "
AND (
    ( PORT_STATUS_DATE IS NOT NULL AND PORT_STATUS_DATE BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' )
    OR
    ( PORT_STATUS_DATE IS NULL AND CREATION_DATE_TIME BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' )
    )
";
  }
  elseif ( $params['plan_state'] == 'Port-In Denied' )
  {
    // check HTT_PORTIN_LOG.PORT_STATUS_DATE

    $clause .= " AND PORT_STATUS_DATE BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' ";
  }
  else
    dlog('',"plan_state not valid");

  if ( isset($params['masteragent']) && ( $params['masteragent'] != '' ) )
    $clause .= sprintf( " AND h.MASTERAGENT = %d ", $params['masteragent'] );

  if ( isset($params['distributor']) && ( $params['distributor'] != '' ) )
    $clause .= sprintf( " AND h.DISTRIBUTOR = %d ", $params['distributor'] );

  if ( isset($params['dealer'])      && ( $params['dealer']      != '' ) )
  {
    $dealer_list = make_where_clause_for_dealer_and_children($params['dealer']);

    if ( empty($dealer_list) )
    {
      dlog('',"No dealer found");

      return '';
    }

    $clause .= ' AND h.DEALER IN (' . $dealer_list . ') ';
  }

  if ( isset($params['user_id'])     && ( $params['user_id']     != '' ) )
    $clause .= sprintf( " AND h.USERID      = %d ", $params['user_id'] );

  $order_by_clause = "u.customer_id ASC";

  if ( isset( $params['sort_by'] ) && is_array( $params['sort_by'] ) && count( $params['sort_by'] ) )
    $order_by_clause = implode( " , " , $params['sort_by'] );

  // precautional
  $order_by_clause = preg_replace("/[\'\"]/", '', $order_by_clause);

  return "
SELECT $select_attributes
FROM  CUSTOMERS                          c
JOIN  ACCOUNTS                           a
ON    c.customer_id = a.customer_id
JOIN  HTT_CUSTOMERS_OVERLAY_ULTRA        u
ON    u.customer_id = c.customer_id
JOIN  ULTRA.HTT_ACTIVATION_HISTORY       h
ON    u.customer_id = h.customer_id
LEFT OUTER JOIN HTT_PORTIN_LOG           p
ON    p.customer_id = c.customer_id
$additional_join
WHERE u.customer_id > 0
$clause
ORDER BY
$order_by_clause";
}

/**
 * make_find_ultra_customers_from_details
 * $params: selected_attributes
 *          first_name
 *          last_name
 *          email
 *          city
 *          address
 *          zipcode
 *          masteragent
 *          distributor
 *          dealer
 *          user_id
 *          account
 *          sort_by
 * 
 * @param  array $params
 * @return string SQL
 */
function make_find_ultra_customers_from_details( $params )
{
  $select_attributes = minimal_customers_select_attributes();

  if ( isset( $params['select_attributes'] ) )
    $select_attributes = implode(" , ", $params['select_attributes']);

  $select_attributes .=
    ' , '.customer_plan_amount_attribute().
    ' , '.customer_plan_name_attribute();

  $clause = '';

  if ( isset( $params['first_name'] ) && ( $params['first_name'] != '' ) )
  {
    if ( preg_match("/^[\w ]+\%$/", $params['first_name'] ) )
      $clause .= " AND FIRST_NAME LIKE '".$params['first_name']."' ";
    else
      $clause .= sprintf( " AND FIRST_NAME = %s ", mssql_escape_with_zeroes( $params['first_name'] ) );
  }

  if ( isset( $params['last_name'] ) && ( $params['last_name'] != '' ) )
  {
    if ( preg_match("/^[\w ]+\%$/", $params['last_name'] ) )
      $clause .= " AND LAST_NAME LIKE '".$params['last_name']."' ";
    else
      $clause .= sprintf( " AND LAST_NAME = %s ", mssql_escape_with_zeroes( $params['last_name'] ) );
  }

  if ( isset( $params['email'] )     && ( $params['email']       != '' ) )
    $clause .= sprintf( " AND E_MAIL        = %s ", mssql_escape_with_zeroes( $params['email'] ) );

  if ( isset( $params['city'] ) && ( $params['city'] != '' ) )
  {
    if ( preg_match("/^[\w ]+\%$/", $params['city'] ) )
      $clause .= " AND CITY LIKE '" . $params['city'] . "' ";
    else
      $clause .= sprintf(" AND CITY = %s ", mssql_escape_with_zeroes($params['city']));
  }

  if ( isset( $params['address'] )   && ( $params['address']     != '' ) )
    $clause .= sprintf( " AND ADDRESS1      = %s ", mssql_escape_with_zeroes( $params['address'] ) );

  if ( isset( $params['zipcode'] )   && ( $params['zipcode']     != '' ) )
    $clause .= sprintf( " AND POSTAL_CODE   = %s ", mssql_escape_with_zeroes( $params['zipcode'] ) );

  if ( isset($params['masteragent']) && ( $params['masteragent'] != '' ) )
    $clause .= sprintf( " AND h.MASTERAGENT = %d ", $params['masteragent'] );

  if ( isset($params['distributor']) && ( $params['distributor'] != '' ) )
    $clause .= sprintf( " AND h.DISTRIBUTOR = %d ", $params['distributor'] );

  if ( isset($params['dealer'])      && ( $params['dealer']      != '' ) )
  {
    $dealer_list = make_where_clause_for_dealer_and_children($params['dealer']);

    if ( empty($dealer_list) )
    {
      dlog('',"No dealer found");

      return '';
    }

    $clause .= ' AND h.DEALER IN (' . $dealer_list . ') ';
  }

  if ( isset($params['user_id'])     && ( $params['user_id']     != '' ) )
    $clause .= sprintf( " AND h.USERID      = %d ", $params['user_id'] );

  if ( isset($params['account'])     && ( $params['account']     != '' ) )
    $clause .= sprintf( " AND a.ACCOUNT      = %s ", mssql_escape_with_zeroes( $params['account'] ) );

  // sanity check
  if ( ! $clause )
    return '';

  $order_by_clause = "u.customer_id ASC";

  if ( isset( $params['sort_by'] ) && is_array( $params['sort_by'] ) && count( $params['sort_by'] ) )
    $order_by_clause = implode( " , " , $params['sort_by'] );

  // precautional
  $order_by_clause = preg_replace("/[\'\"]/", '', $order_by_clause);

  return "
SELECT $select_attributes
FROM  CUSTOMERS                          c
JOIN  ACCOUNTS                           a
ON    c.customer_id = a.customer_id
JOIN  HTT_CUSTOMERS_OVERLAY_ULTRA        u
ON    u.customer_id = c.customer_id
JOIN  ULTRA.HTT_ACTIVATION_HISTORY       h
ON    u.customer_id = h.customer_id
LEFT OUTER JOIN HTT_PORTIN_LOG           p
ON    p.customer_id = c.customer_id
LEFT OUTER JOIN htt_cancellation_reasons r
ON    c.customer_id = r.customer_id
WHERE u.customer_id > 0
$clause
ORDER BY
$order_by_clause";
}

/**
 * make_find_ultra_customer_query_from_email 
 * @param  string $email
 * @return string SQL
 */
function make_find_ultra_customer_query_from_email($email)
{
  $minimal_customers_select_attributes = minimal_customers_select_attributes();

  return sprintf("
    SELECT $minimal_customers_select_attributes
    FROM dbo.customers  AS c  WITH (NOLOCK)
    JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
    JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON u.customer_id = c.customer_id
    WHERE E_MAIL = %s",
    mssql_escape_with_zeroes($email)
  );
}

/**
 * make_find_ultra_customer_query_from_name 
 * @param  string $name
 * @return string SQL
 */
function make_find_ultra_customer_query_from_name($name)
{
  $minimal_customers_select_attributes = minimal_customers_select_attributes();

  return sprintf("
    SELECT $minimal_customers_select_attributes
    FROM dbo.customers  AS c  WITH (NOLOCK)
    JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
    JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON u.customer_id = c.customer_id
    WHERE ( FIRST_NAME = %s OR LAST_NAME = %s )",
    mssql_escape_with_zeroes($name),
    mssql_escape_with_zeroes($name)
  );
}

/**
 * make_find_ultra_customer_query_from_iccid 
 * @param  string $iccid
 * @param  string $select_attributes
 * @return string SQL
 */
function make_find_ultra_customer_query_from_iccid($iccid,$select_attributes=NULL)
{
  if ( ! $select_attributes )
    $select_attributes = "
      *,
      DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch,
      DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch,
      DATEDIFF(Day, getutcdate() , u.plan_expires) days_before_expire,
      u.BRAND_ID as BRAND_ID
    ";

  return sprintf("
SELECT $select_attributes
FROM  htt_customers_overlay_ultra u WITH (NOLOCK)
INNER JOIN customers  c  WITH (NOLOCK) ON c.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN accounts   a  WITH (NOLOCK) ON a.CUSTOMER_ID = u.CUSTOMER_ID
INNER JOIN parent_cos pc WITH (NOLOCK) on pc.COS_ID     = a.COS_ID
AND   ( u.ACTIVATION_ICCID      = '%s'
     OR u.current_iccid         = '%s'
     OR u.ACTIVATION_ICCID_FULL = '%s'
     OR u.CURRENT_ICCID_FULL    = '%s' )
",  mssql_escape($iccid),
    mssql_escape($iccid),
    mssql_escape($iccid),
    mssql_escape($iccid)
  );
}

/**
 * customer_plan_amount_attribute 
 * @param  string $alias
 * @return string SQL
 */
function customer_plan_amount_attribute($alias='a')
{
  $sql = " CASE ".$alias.".COS_ID \n";

  $plan_cost_map = get_plan_cost_map();
  foreach ($plan_cost_map as $cos_id => $cost)
    $sql .= " WHEN $cos_id THEN " . ($cost / 100) . "\n";

  $sql .= " ELSE 0 \n END plan_amount ";

  return $sql;
}

/**
 * customer_plan_name_attribute 
 * @param  string $alias [description]
 * @return string SQL
 */
function customer_plan_name_attribute($alias='a')
{
  $sql = " CASE ".$alias.".COS_ID \n";

  $plan_name_map = get_plan_name_map();
  foreach ($plan_name_map as $cos_id => $name)
    $sql .= " WHEN $cos_id THEN '$name'\n";

  $sql .= " ELSE '' \n END plan_name ";

  return $sql;
}

/**
 * make_find_ultra_customer_query_from_conditions 
 * @param  array $conditions
 * @param  integer $customer_id
 * @return string SQL
 */
function make_find_ultra_customer_query_from_conditions($conditions,$customer_id=NULL)
{
  dlog('',json_encode($conditions));

  $additional_where_clause = make_customer_where_clause_from_conditions($conditions);

  if ( $customer_id )
    $additional_where_clause .= " AND u.customer_id = $customer_id ";

  return "
    SELECT c.customer_id,
           a.COS_ID,
           convert(varchar, DATEADD( dd , -1 , u.plan_expires ) , 107) renewal_date,
           ".customer_plan_amount_attribute().','.
             customer_plan_name_attribute() . ",
           u.current_mobile_number,
           u.MONTHLY_RENEWAL_TARGET,
           u.BRAND_ID
    FROM  HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK)
    INNER JOIN CUSTOMERS  c  WITH (NOLOCK) ON c.CUSTOMER_ID  = u.CUSTOMER_ID
    INNER JOIN ACCOUNTS   a  WITH (NOLOCK) ON a.CUSTOMER_ID  = u.CUSTOMER_ID
    INNER JOIN PARENT_COS pc WITH (NOLOCK) ON pc.COS_ID      = a.COS_ID
    $additional_where_clause";
}

/**
 * make_find_customer_query
 *
 * @param  string $v customer || customer_id
 * @param  string $name login+name
 * @return string
 */
function make_find_customer_query($v, $name=NULL)
{
  $extra = $name == NULL ? '' : ' OR c.login_name = ' . mssql_escape_with_zeroes($name);

  return sprintf("
SELECT *
FROM customers c, accounts a, parent_cos pc
WHERE c.customer_id=a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s' $extra)
AND a.cos_id=pc.cos_id
", mssql_escape($v), mssql_escape($v));
}

/**
 * make_find_ultra_customer_query_from_cancelled_msisdn
 *
 * @param  string $msisdn
 * @return string SQL
 */
function make_find_ultra_customer_query_from_cancelled_msisdn($msisdn)
{
  return sprintf("
SELECT
 *,
 DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch,
 DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch,
 DATEDIFF(Day, getutcdate() , u.plan_expires) days_before_expire
FROM dbo.HTT_CANCELLATION_REASONS AS r WITH (NOLOCK)
JOIN dbo.customers  AS c  WITH (NOLOCK) ON r.customer_id = c.customer_id
JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
JOIN dbo.parent_cos AS pc WITH (NOLOCK) ON a.cos_id = pc.cos_id
JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON u.customer_id = c.customer_id
WHERE r.msisdn = '%s'
", mssql_escape($msisdn));
}

/**
 * make_find_ultra_customer_query_from_msisdn
 *
 * @param  strng $msisdn
 * @param  string $selected_attributes
 * @return string SQL
 */
function make_find_ultra_customer_query_from_msisdn($msisdn,$select_attributes=NULL)
{
  if ( ! $select_attributes )
    $select_attributes = "
      *,
      DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch,
      DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch,
      DATEDIFF(Day, getutcdate() , u.plan_expires) days_before_expire,
      DATEADD( dd , -1 , plan_expires )            latest_plan_date, -- plan_expires minus a day
      u.BRAND_ID as BRAND_ID
    ";

  return sprintf("
    SELECT $select_attributes
    FROM dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK)
    JOIN dbo.customers  AS c  WITH (NOLOCK) ON u.customer_id = c.customer_id
    JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
    JOIN dbo.parent_cos AS pc WITH (NOLOCK) ON a.cos_id = pc.cos_id
    WHERE u.current_mobile_number = '%s'
", mssql_escape($msisdn));
}

/**
 * make_find_ultra_customer_query_from_customer
 *
 * @param  string $customer
 * @return string SQL
 */
function make_find_ultra_customer_query_from_customer($customer)
{
  return sprintf("
SELECT *
  FROM dbo.customers  AS c  WITH (NOLOCK)
  JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
  JOIN dbo.parent_cos AS pc WITH (NOLOCK) ON a.cos_id = pc.cos_id
  JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON u.customer_id = c.customer_id
  WHERE c.customer = %s
", mssql_escape_with_zeroes($customer));
}

/**
 * make_find_ultra_customer_query_from_actcode
 *
 * @param  string $actcode
 * @return string SQL
 */
function make_find_ultra_customer_query_from_actcode($actcode)
{
  return sprintf("
    SELECT *,
      RIGHT(c.CC_NUMBER, 4) AS CC_LAST_4,
      DATEDIFF(mi, u.plan_started , getutcdate() ) mins_since_act,
      DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch,
      DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch,
      DATEDIFF(Day, getutcdate() , u.plan_expires) days_before_expire
    FROM dbo.HTT_INVENTORY_SIM AS s WITH (NOLOCK)
    JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON s.ICCID_NUMBER = u.CURRENT_ICCID
    JOIN dbo.customers  AS c  WITH (NOLOCK) ON u.customer_id = c.customer_id
    JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
    JOIN dbo.parent_cos AS pc WITH (NOLOCK) ON a.cos_id = pc.cos_id
    WHERE s.ACT_CODE = %s
  ", mssql_escape_with_zeroes($actcode));
}

/**
 * make_find_ultra_customer_query_from_customer_id
 *
 * latest_plan_date is plan_expires minus a day
 *
 * @param  integer $customer_id
 * @param  string $select_fields
 * @return string SQL
 */
function make_find_ultra_customer_query_from_customer_id($customer_id,$select_fields=NULL)
{
  $select_columns = "*";

  if ( $select_fields && is_array($select_fields) && count($select_fields) )
    $select_columns = implode(' , ',$select_fields);

  return sprintf("
DECLARE @customer_id BIGINT = %d;
SELECT $select_columns,
       RIGHT(c.CC_NUMBER, 4) AS CC_LAST_4,
       DATEADD( d , 1 , plan_expires ) plan_expires_effective,
       DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch,
       DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch,
       DATEDIFF(Day, getutcdate() , u.plan_expires) days_before_expire,
       DATEADD( dd , -1 , plan_expires )            latest_plan_date
FROM  dbo.customers c, dbo.accounts a, dbo.parent_cos pc, dbo.htt_customers_overlay_ultra u
WITH (NOLOCK)
WHERE c.customer_id = a.customer_id
AND   u.customer_id = c.customer_id
AND   a.cos_id=pc.cos_id
AND   u.customer_id = @customer_id
", $customer_id);
}

/**
 * make_find_ultra_customer_query_from_login
 *
 * @param  string $login
 * @return string SQL
 */
function make_find_ultra_customer_query_from_login($login)
{
  return sprintf("
SELECT *,
       RIGHT(c.CC_NUMBER, 4) AS CC_LAST_4,
       DATEDIFF(ss, '1970-01-01', plan_started ) plan_started_epoch,
       DATEDIFF(ss, '1970-01-01', plan_expires ) plan_expires_epoch
  FROM dbo.customers  AS c  WITH (NOLOCK)
  JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
  JOIN dbo.parent_cos AS pc WITH (NOLOCK) ON a.cos_id = pc.cos_id
  JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON u.customer_id = c.customer_id
  WHERE c.LOGIN_NAME = %s
", mssql_escape_with_zeroes($login));
}

/**
 * make_find_ultra_customer_query_anycosid
 *
 * @param  string $v customer || customer_id
 * @param  string $name
 * @return string SQL
 */
function make_find_ultra_customer_query_anycosid($v, $name=NULL)
{
  $extra = $name == NULL ? '' : ' OR c.login_name = ' . mssql_escape_with_zeroes($name);

  return sprintf("
SELECT *
  FROM dbo.customers  AS c  WITH (NOLOCK)
  JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
  JOIN dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK) ON u.customer_id = c.customer_id
  WHERE ( c.customer = '%s' OR c.customer_id = '%s' $extra )
", mssql_escape($v), mssql_escape($v));
}

/**
 * make_find_customer_query_anycosid
 *
 * @param  string $v customer || customer_id
 * @param  string $name
 * @return string SQL
 */
function make_find_customer_query_anycosid($v, $name=NULL)
{
  $extra = $name == NULL ? '' : ' OR c.login_name = ' . mssql_escape_with_zeroes($name);

  return sprintf("
SELECT *
FROM customers c, accounts a
WHERE c.customer_id=a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s' $extra)
", mssql_escape($v), mssql_escape($v));
}

/**
 * make_find_new_signup_status_customer_query
 *
 * @param  string $v customer || customer_id
 * @return string SQL
 */
function make_find_new_signup_status_customer_query($v)
{
  return sprintf("
SELECT c.CUSTOMER_ID, c.CUSTOMER, c.FIRST_NAME, c.LAST_NAME, a.COS_ID,
       c.ADDRESS1, c.ADDRESS2, c.CITY, c.STATE_REGION, c.POSTAL_CODE,
       c.LOCAL_PHONE, c.CC_EXP_DATE, c.LOGIN_NAME,
       a.PACKAGED_BALANCE1, a.ACCOUNT, a.ACCOUNT_ID,
       (a.PACKAGED_BALANCE1/60) AS PACKAGED_BALANCE1_OVER_60,
       a.BALANCE, a.CREDIT_LIMIT, c.user_8, c.COUNTRY
FROM customers c, accounts a
WHERE c.customer_id = a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s')
", mssql_escape($v), mssql_escape($v));
}

/**
 * make_find_new_signup_customer_query
 *
 * @param  string $v customer || customer_id
 * @return string SQL
 */
function make_find_new_signup_customer_query($v)
{
  return sprintf("
SELECT *
FROM customers c, accounts a
WHERE c.customer_id = a.customer_id
AND (c.customer = '%s' OR c.customer_id = '%s')
", mssql_escape($v), mssql_escape($v));
}

/* $v is an account number */
function make_find_autodial_numbers_for_customer_query($v, $type='indiald')
{
  $m = $v % 10;
  return "SELECT TOP 1 number FROM [htt_autodialer_assignment]
WHERE ani_mod='$m' AND country='USA' AND type = '$type'
      AND number NOT IN (SELECT dnis FROM autodialer WHERE account = '$v')";
}

/**
 * find_customer 
 * @param  string $query
 * @return object
 */
function find_customer($query)
{
  return find_first($query);
}

/**
 * make_customer_where_clause_from_conditions 
 * @param  array $conditions kva
 * @return string $additional_where_clause
 */
function make_customer_where_clause_from_conditions($conditions)
{
  $additional_where_clause = '';

  foreach( $conditions as $name => $value )
  {
    switch($name)
    {
      case 'customer_stored_value_gte':
        if ( $value === '_renewal_cost_' )
        {
          $plan_cost_map = get_plan_cost_map();

          $additional_where_clause .= " AND (\n";
          $i = 0;
          foreach ($plan_cost_map as $cos_id => $cost)
          {
            if ($i) $additional_where_clause .= "OR\n";
            $additional_where_clause .= "( a.COS_ID = $cos_id AND u.stored_value >= " . ($cost / 100) . ')' . "\n";
            $i++;
          }
          $additional_where_clause .= ") \n";
        }
        else
        {
          $additional_where_clause .= sprintf(" AND u.stored_value >= %d",$value);
        }
      break;

      case 'customer_stored_value_lt':
        if ( $value === '_renewal_cost_' )
        {
          $plan_cost_map = get_plan_cost_map();

          $additional_where_clause .= " AND (\n";
          $i = 0;
          foreach ($plan_cost_map as $cos_id => $cost)
          {
            if ($i) $additional_where_clause .= "OR\n";
            $additional_where_clause .= "( a.COS_ID = $cos_id AND u.stored_value < " . ($cost / 100) . ')' . "\n";
            $i++;
          }
          $additional_where_clause .= ") \n";
        }
        else
        {
          $additional_where_clause .= sprintf(" AND u.stored_value < %d",$value);
        }
      break;

      case 'customer_balance_gte':
        if ( $value === '_renewal_cost_' )
        {
          $plan_cost_map = get_plan_cost_map();

          $additional_where_clause .= " AND (\n";
          $i = 0;
          foreach ($plan_cost_map as $cos_id => $cost)
          {
            if ($i) $additional_where_clause .= "OR\n";
            $additional_where_clause .= "( a.COS_ID = $cos_id AND a.balance >= " . ($cost / 100) . ')' . "\n";
            $i++;
          }
          $additional_where_clause .= ") \n";
        }
        else
        {
          $additional_where_clause .= sprintf(" AND a.balance >= %d",$value);
        }
      break;

      case 'customer_balance_lt':
        if ( $value === '_renewal_cost_' )
        {
          $plan_cost_map = get_plan_cost_map();

          $additional_where_clause .= " AND (\n";
          $i = 0;
          foreach ($plan_cost_map as $cos_id => $cost)
          {
            if ($i) $additional_where_clause .= "OR\n";
            $additional_where_clause .= "( a.COS_ID = $cos_id AND a.balance < " . ($cost / 100) . ')' . "\n";
            $i++;
          }
          $additional_where_clause .= ") \n";
        }
        else
        {
          $additional_where_clause .= sprintf(" AND a.balance < %d",$value);
        }
      break;

      case 'customer_state':
        $additional_where_clause .= sprintf(" AND u.plan_state = %s",mssql_escape_with_zeroes($value));
      break;

      case 'customer_plan_expires_dd_mm_yyyy':
        $additional_where_clause .= sprintf(" AND convert(varchar, u.plan_expires, 105) = %s",mssql_escape_with_zeroes($value));
      break;

      case 'customer_monthly_cc_renewal':
        $additional_where_clause .= ( $value ) // a BIT strange
          ?
          " AND ( u.monthly_cc_renewal = 1 )"
          :
          " AND ( u.monthly_cc_renewal = 0 OR CAST(u.monthly_cc_renewal AS CHAR(1)) IS NULL )";
      break;

      case 'customer_transition_dd_mm_yyyy':
        # handled together with 'customer_transition_to'
      break;

      case 'customer_transition_to':
        if ( isset($conditions['customer_transition_dd_mm_yyyy']) )
        {
          $additional_where_clause .= sprintf(" AND u.customer_id IN (
  SELECT t.CUSTOMER_ID
  FROM   HTT_TRANSITION_LOG t
  WHERE  t.STATUS = 'CLOSED'
  AND    t.TO_PLAN_STATE = %s
  AND    convert(varchar, t.CREATED, 105) = %s
) ",
            mssql_escape_with_zeroes( $value ),
            mssql_escape_with_zeroes( $conditions['customer_transition_dd_mm_yyyy'] )
          );
        }
        else
        {
          dlog('',"ERROR: condition 'customer_transition_to' needs condition 'customer_transition_dd_mm_yyyy'");
        }
      break;

      default:
        dlog('',"ERROR: condition name $name not handled");
      break;
    }
  }

  return $additional_where_clause;
}

/**
 * make_signup_query 
 * @param  string $customer_n
 * @return string SQL
 */
function make_signup_query($customer_n)
{
  $account_group = find_credential('constants/account_group_id');
  if (NULL == $account_group)
  {
    $account_group = '70';
  }

  $signup_cos_id = find_credential('constants/signup_cos_id');
  if (NULL == $signup_cos_id)
  {
    $signup_cos_id = '94916';
  }

  $cdate = find_credential('constants/creation_date');
  if (NULL == $cdate)
  {
    $cdate = 'GETDATE()';
  }

  // PROD-2332: use ENABLED or ACCOUNT_STATUS_TYPE
  list($enabled, $value) = array_map('trim', explode('=', account_enabled_assignment(ACCOUNT_STATUS_TYPE_ACTIVATED)));

  $password = \Ultra\Lib\Util\encryptPasswordHS('password');

  return "INSERT INTO [dbo].[customers]
   (CUSTOMER,
FIRST_NAME, LAST_NAME, COMPANY, ADDRESS1, ADDRESS2,
CITY, STATE_REGION, POSTAL_CODE, COUNTRY, LOCAL_PHONE, FAX, E_MAIL,
DATE_OF_BIRTH, SIGNUP_DATE, LOGIN_NAME, LOGIN_PASSWORD,
CC_NUMBER, CC_EXP_DATE, CC_NAME, CC_ADDRESS1, CC_ADDRESS2,
CC_CITY, CC_COUNTRY, CC_STATE_REGION, CC_POSTAL_CODE,
NOTES, USER_1, USER_2, USER_3, USER_4, USER_5,
USER_6, USER_7, USER_8,
USER_9, USER_10,
CCV, PUBLIC_CUSTOMER)
   VALUES(
$customer_n,
'', '', '', '', '',
'', '', '', 'US', '', '', '',
NULL, NULL, '$customer_n', '$password',
'', '', '', '', '',
'', '', '', '',
'', NULL, NULL, NULL, NULL, NULL,
NULL, NULL, '0',
NULL, NULL,
NULL, 0);

INSERT INTO [dbo].[accounts]
   (ACCOUNT,
PIN,
CUSTOMER_ID,
PARENT_ACCOUNT_ID, BATCH_ID, SEQUENCE_NUMBER, ACCOUNT_GROUP_ID, ACCOUNT_TYPE,
CALLBACK_NUMBER, $enabled, BILLING_TYPE,
CREATION_DATE_TIME, ACTIVATION_DATE_TIME,
STARTING_BALANCE, CREDIT_LIMIT, BALANCE,
STARTING_PACKAGED_BALANCE1, PACKAGED_BALANCE1,
SERVICE_CHARGE_DATE, COS_ID, WRITE_CDR, SERVICE_CHARGE_STATUS,
CALLS_TO_DATE, LAST_CALL_DATE_TIME, MINUTES_TO_DATE_BILLED,
MINUTES_TO_DATE_ACTUAL, LAST_CALLER, LAST_CALLED_PARTY, LAST_CREDIT_DATE_TIME,
LAST_DEBIT_DATE_TIME, LAST_RECHARGE_DATE_TIME, LAST_TRANSFER_DATE_TIME,
LAST_AUTHENTICATED_DATE_TIME,
PACKAGED_BALANCE2, PACKAGED_BALANCE3, PACKAGED_BALANCE4, PACKAGED_BALANCE5,
STARTING_PACKAGED_BALANCE2, STARTING_PACKAGED_BALANCE3,
STARTING_PACKAGED_BALANCE4, STARTING_PACKAGED_BALANCE5)

  VALUES(
'$customer_n',
'',
(SELECT TOP 1 customer_id FROM customers WHERE customer = '$customer_n'),
0, 0, 0, $account_group, 1, -- account_group_id and account_type to review
'', $value, 1,
$cdate, GETUTCDATE(), -- dates to review
0, 0, 0,
0, 0,
NULL, $signup_cos_id, 1, 0,
0, NULL, 0,
0, NULL, '', GETUTCDATE(), -- date to review
NULL, GETUTCDATE(), NULL, -- date to review
GETUTCDATE(), -- date to review
0, 0, 0, 0, 0, 0, 0, 0);

SELECT * FROM customers, accounts
WHERE customers.customer = '$customer_n' AND
      accounts.customer_id = customers.customer_id;
";
}

/**
 * get_customer_time_zone
 *
 * returns hours difference from UTC for given customer
 * cache results for 1 hour since we often queue multiple SMS to same customr
 * @param int customer_id
 * @return int hours difference from UTC
 */
function get_customer_time_zone($customer_id)
{
  $redis = new \Ultra\Lib\Util\Redis();
  $key = "timezone/$customer_id";
  $timezone = $redis->get($key);

  if (! $timezone)
  {
    $sql = "SELECT z.TZ FROM CUSTOMERS c WITH (NOLOCK) JOIN HTT_LOOKUP_REFERENCE_ZIP z WITH (NOLOCK) ON z.ZIP = c.POSTAL_CODE WHERE c.CUSTOMER_ID = $customer_id";
    $result = mssql_fetch_all_rows(logged_mssql_query($sql));
    if (empty($result[0][0]))
      dlog('', "WARNING: no time zone found for customer $customer_id, using default PST");
    else
      $timezone = $result[0][0];
  }

  if ($timezone)
    $redis->set($key, $timezone, 3600); // cache for 1 hour
  else
    $timezone = 'PST';

  // difference in hours from UTC
  $difference = time_zone_mapping();

  // default to PST
  return isset($difference[$timezone]) ? $difference[$timezone] : $difference['PST'];
}

/**
 * get_idd_history_from_account_id
 *
 * @param  string account
 * @return array
 */
function get_idd_history_from_account_id( $account_id , $start_epoch , $end_epoch )
{
  $idd_history_select_query = idd_history_select_query(
    array(
      'account_id'  => $account_id,
      'start_epoch' => $start_epoch,
      'end_epoch'   => $end_epoch
    )
  );

  if ( empty( $idd_history_select_query ) )
    return array();
  else
    return mssql_fetch_all_objects(logged_mssql_query($idd_history_select_query));
}

/**
 * idd_history_select_query 
 * $params: start_epoch
 *          end_epoch
 *          recent
 * 
 * @param  array $params
 * @return string SQL
 */
function idd_history_select_query( $params )
{
  $sql = sprintf("
SELECT top %d

rate_plan,
DATEDIFF(ss, '1970-01-01', dbo.UTC_TO_PT(START_DATE_TIME) ) as history_epoch_pst, -- PST
description as destination,
detail      as destination_number,
case
 WHEN ACTUAL_DURATION < 30 THEN 0
 ELSE CEILING(ACTUAL_DURATION/60.0) -- the .0 makes this floating point, and is required
END as minutes,
CASE
 WHEN PACKAGED_BALANCE_INDEX = 0 THEN PER_MINUTE_CHARGE
 WHEN PACKAGED_BALANCE_INDEX = 1 and BILLING_ID <= 1084496 THEN PER_MINUTE_CHARGE/100
 WHEN PACKAGED_BALANCE_INDEX = 1 and BILLING_ID >  1084496 THEN PER_MINUTE_CHARGE/1000
END AS rate

FROM  [BILLING]
WHERE [ACCOUNT_ID] = %d",
  empty($params['recent']) ? 2000 : $params['recent'],
  $params['account_id']);

  if ( ! empty($params['start_epoch']) && ! empty($params['end_epoch']))
    $sql .= sprintf(" AND dbo.UTC_TO_PT(START_DATE_TIME) BETWEEN DATEADD(s, %d, '19700101') AND DATEADD(s, %d, '19700101')", $params['start_epoch'], $params['end_epoch']);
  elseif ( ! empty($params['recent']))
    $sql .= sprintf(' ORDER BY START_DATE_TIME DESC');
  else // no parameters: this should not happen
    $sql = NULL;

  return $sql;
}

/**
 * get_next_action_from_plan_state
 *
 * Given a plan_state, returns a string token which signifies what is the next action to be performed
 *
 * @param  string $plan_state
 * @param  string $pending_transition
 * @param  string $port_status
 * @param  integer $created_seconds_ago
 * @param  integer $port_updated_seconds_ago
 * @return string next_action
 */
function get_next_action_from_plan_state( $plan_state , $pending_transition , $port_status , $created_seconds_ago , $port_updated_seconds_ago )
{
  $next_action = NULL;

  $neutral_seconds_limit = 60*30; // 30 minutes

  switch ($plan_state)
  {
    case "":
    case NULL:
      $next_action = "";
    case "Provisioned":
      if (!$pending_transition) $next_action = 'ADD_FUNDS';
      break;
    case "Neutral":
      $next_action = ( ( $created_seconds_ago < $neutral_seconds_limit ) ? 'PROVISION_SIM' : 'POTENTIAL_ACTIVATION_ISSUES' );
      break;
    case "Port-In Requested":
      // CONCURRENCE    ???
      // CONFIRMED      ???
      // FAILED TO PORT ???
      // CANCELLED      ???
      if ( ( $port_status == 'IN PROGRESS' ) || ( $port_status == 'CONCURRENCE') || ( $port_status == 'COMPLETE' ) )
        $next_action = 'PORT_WAITING_FOR_RESPONSE';
      if ( $port_status == 'RESOLUTION REQUIRED' )
        $next_action = 'UPDATE_PORT';
      if ( $port_status == 'TIMEOUT' )
        $next_action = 'PORT_CONTACT_CARE';
      if ( $port_status == 'DELAY' )
      {
        if ( $port_updated_seconds_ago > 60*60*24 )
        { $next_action = 'PORT_CONTACT_CARE';         }
        else
        { $next_action = 'PORT_WAITING_FOR_RESPONSE'; }
      }
      break;
    case "Port-In Denied":
      $next_action = 'COULD_NOT_PORT';
      break;
    case 'Activated':
    case FINAL_STATE_COMPLETE:
      $next_action = 'SUCCESS';
      break;
    case "Cancelled":
      $next_action = '';
      break;
    default:
      dlog('',"case not handled for $plan_state");
      break;
  }

  return $next_action;
}

/**
 * get_amount_needed_from_customer
 *
 * Given a customer, returns the amount needed to fund the customer plan (in $)
 *
 * @param  object $customer
 * @return integer
 */
function get_amount_needed_from_customer( $customer )
{
  $amount_needed = 0;

  if ( $customer->COS_ID != COSID_ULTRA_STANDBY )
  {
    $amount_needed = get_plan_cost_from_cos_id($customer->COS_ID) / 100;
    $amount_needed -= ( $customer->stored_value + $customer->BALANCE );

    if ( $amount_needed < 0 ) $amount_needed = 0;
  }

  // if the customer already fully funded the plan, $amount_needed will be 0
  if ( $amount_needed )
  {
    $activation_history = get_ultra_activation_history( $customer->CUSTOMER_ID );
    if ( $activation_history )
      $amount_needed -= $activation_history->FUNDING_AMOUNT;
    if ( $amount_needed < 0 ) $amount_needed = 0;
  }

  return $amount_needed;
}

/**
 * check_duplicate_email
 *
 * @param  string $email
 * @return boolean
 */
function check_duplicate_email( $email )
{
  if ( ! $email )
    return TRUE;

  $sql = sprintf('
SELECT TOP 1 e_mail
FROM   customers c,
       accounts a
WHERE  c.customer_id = a.customer_id
AND    a.' . account_enabled_clause(TRUE) . ' 
AND    e_mail = %s',
    mssql_escape($email)
  );

  if (mssql_has_rows($sql))
    return FALSE;

  return TRUE;
}

/**
 * get_expiring_subscribers_from_dealer
 * return a list of subscribers who have been recently suspended or are expiring soon
 * @see SMR-15
 * @see dealerportal__SearchCustomersExpiring
 * @param integer days_since_suspend
 * @param integer days_until_expire
 * @return array of objects
 */
function get_expiring_subscribers_from_dealer($dealer, $days_since_suspend = 0, $days_until_expire = 0)
{
  if (empty($dealer))
  {
    dlog('', 'ERROR: invalid parameters %s', func_get_args());
    NULL;
  }

  $celluphone = \Ultra\UltraConfig\celluphoneDb();
  $sql = sprintf("SELECT
      u.customer_id,
      c.FIRST_NAME AS first_name,
      c.LAST_NAME AS last_name,
      c.E_MAIL AS email,
      u.CURRENT_MOBILE_NUMBER AS msisdn,
      u.CURRENT_ICCID_FULL AS iccid,
      a.COS_ID AS plan_name,
      DATEDIFF(s, '19700101', a.CREATION_DATE_TIME) AS created_date,
      DATEDIFF(s, '19700101', u.plan_started) AS plan_started,
      DATEDIFF(s, '19700101', u.plan_expires) AS plan_expires,
      u.plan_state,
      u.monthly_cc_renewal,
      a.BALANCE AS balance,
      u.stored_value
    FROM HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK)
    JOIN ACCOUNTS a WITH (NOLOCK) ON u.CUSTOMER_ID = a.CUSTOMER_ID
    JOIN CUSTOMERS c WITH (NOLOCK) ON u.CUSTOMER_ID = c.CUSTOMER_ID
    JOIN ULTRA.HTT_ACTIVATION_HISTORY h WITH (NOLOCK) ON u.CUSTOMER_ID = h.CUSTOMER_ID
    JOIN $celluphone..tblDealerSite d WITH (NOLOCK) ON h.DEALER = d.DealerSiteID
    WHERE ((u.PLAN_STATE = 'Suspended' AND u.PLAN_EXPIRES BETWEEN DATEADD(day, -%d, GETUTCDATE()) AND GETUTCDATE())
      OR (u.PLAN_STATE = 'Active' AND u.PLAN_EXPIRES BETWEEN GETUTCDATE() AND DATEADD(day, %d, GETUTCDATE())))
      AND (d.DealerSiteID = %d OR d.ParentDealerSiteID = %d)", $days_since_suspend, $days_until_expire, $dealer, $dealer);
  $subscribers =  mssql_fetch_all_objects(logged_mssql_query($sql));
  foreach ($subscribers as &$subscriber)
    $subscriber->plan_name = get_plan_from_cos_id($subscriber->plan_name);
  return $subscribers;
}

/**
 * get_idd_history
 *
 * return a list of subscribers who have been recently suspended or are expiring soon
 * 
 * @param integer customer_id
 * @param epoch start_epoch
 * @param epoch end_epoch
 * @return array of objects
 */
function get_idd_history($account_id, $start_epoch, $end_epoch, $recent)
{
  $idd_history_select_query = idd_history_select_query(
    array(
      'account_id'  => $account_id,
      'start_epoch' => $start_epoch,
      'end_epoch'   => $end_epoch,
      'recent'      => $recent
    )
  );

  dlog('',$idd_history_select_query);

  return mssql_fetch_all_objects(logged_mssql_query($idd_history_select_query));
}

/**
 * is_dev_customer
 *
 * Returns TRUE if the customer is to be used by developers and QA
 * [MVNO-2157] HTT_INVENTORY_SIM.LAST_CHANGED_BY should be 'DONOTCANCEL'
 * 
 * @param integer customer_id
 * @param string iccid
 * @return boolean
 */
function is_dev_customer( $customer_id=NULL , $iccid=NULL )
{
  // this feature is only available in DEV
  if ( ! \Ultra\UltraConfig\isDevelopmentDB() )
    return FALSE;

  if ( ! $customer_id )
    return is_dev_iccid( $iccid );

  $sql = sprintf("
SELECT s.LAST_CHANGED_BY
FROM   HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK)
JOIN   HTT_INVENTORY_SIM s           WITH (NOLOCK)
ON     u.CURRENT_ICCID_FULL = s.ICCID_FULL
WHERE  u.CUSTOMER_ID = %d",
    $customer_id
  );

  $result = mssql_fetch_all_rows( logged_mssql_query( $sql ) );

  if ( $result && is_array($result) && count($result) && ( $result[0][0] == 'DONOTCANCEL' ) )
    return TRUE;

  return FALSE;
}

/**
 * is_dev_iccid
 *
 * Returns TRUE if the ICCID is to be used by developers and QA
 * [MVNO-2157] HTT_INVENTORY_SIM.LAST_CHANGED_BY should be 'DONOTCANCEL'
 * 
 * @param string iccid
 * @return boolean
 */
function is_dev_iccid( $iccid=NULL )
{
  // this feature is only available in DEV
  if ( ! \Ultra\UltraConfig\isDevelopmentDB() )
    return FALSE;

  if ( ! $iccid )
    return FALSE;

  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'HTT_INVENTORY_SIM',
    NULL,
    array( 'LAST_CHANGED_BY' ),
    array( 'ICCID_FULL' => $iccid ),
    NULL,
    NULL,
    TRUE // WITH (NOLOCK)
  );

  $result = mssql_fetch_all_rows( logged_mssql_query( $sql ) );

  if ( $result && is_array($result) && count($result) && ( $result[0][0] == 'DONOTCANCEL' ) )
    return TRUE;

  return FALSE;
}

/**
 * is_eligible_for_plan_change
 *
 * checks if customer is eligible for plan change
 * checks against array : $ineligible_options
 * 
 * @param  int $customer_id
 * @param  array $ineligible_options
 * @return boolean
 */
function is_eligible_for_plan_change($customer_id, $ineligible_options)
{
  $customer_options = \get_ultra_customer_options_by_customer_id($customer_id);

  for ($i = 0; $i < count($customer_options); $i++)
  {
    if ( $customer_options && is_array( $customer_options ) && in_array($customer_options[$i], $ineligible_options))
    {
      if ($customer_options[$i] == 'BILLING.MRC_PROMO_PLAN')
      {
        $promo_plan_info = format_billing_mrc_promo_plan_info(array($customer_options[$i], $customer_options[$i + 1]));
        if ( $promo_plan_info['months'] < 1 )
          continue;
      }

      return FALSE;
    }
  }

  return TRUE;
}

/**
 * disable_cc_holders_entries
 *
 * disables rows related to cc_holders
 * 
 * @param  Integer $customer_id
 * @return Object Result
 */
function disable_cc_holders_entries($params)
{
  dlog('', '(%s)', func_get_args());

  if ( ! is_numeric($params['customer_id']))
  {
    $error = 'invalid parameter customer_id';
    dlog('', 'ERROR: ' . $error);
    return \make_error_Result($error);
  }

  $result = disable_ultra_cc_holders_by_customer_id($params['customer_id']);
  if ( ! $result) dlog('', 'WARNING: DB error disabling in ultra.cc_holders');

  $result = disable_ultra_cc_holder_tokens_by_customer_id($params['customer_id']);
  if ( ! $result) dlog('', 'WARNING: DB error disabling in ultra.cc_holder_tokens');

  return make_ok_Result();
}

function is_campus_sim_customer($customer_id)
{
  $sql = sprintf(
    'SELECT s.INVENTORY_MASTERAGENT FROM HTT_CUSTOMERS_OVERLAY_ULTRA o
      LEFT JOIN HTT_INVENTORY_SIM s on o.CURRENT_ICCID_FULL = s.ICCID_FULL
      WHERE o.customer_id = %d',
    $customer_id
  );
  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if (count($result) == 1) {
    if ($result[0]->INVENTORY_MASTERAGENT == 74) {
      return true;
    }
  }

  return false;
}
