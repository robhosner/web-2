<?php

# php component to access DB table htt_messaging_queue #

include_once('db.php');
include_once('Ultra/Lib/Util/Redis/Messaging.php');

# synopsis

# HTT_MESSAGING_QUEUE.EXPECTED_DELIVERY_DATETIME -- 
# HTT_MESSAGING_QUEUE.NEXT_ATTEMPT_DATETIME      -- 

#echo htt_messaging_queue_update_query(
#  array(
#    'htt_messaging_queue_id'     => 7,
#    'expected_delivery_datetime' => 'getutcdate()',
#    'next_attempt_datetime'      => 'getutcdate()',
#    'attempt_count'              => '+1',
#    'attempt_log'                => 'log test',
#    'status'                     => 'SKIPPED'
#  )
#)."\n\n";

#echo htt_messaging_queue_insert_query(
#  array(
#    'customer_id'                 => 123,
#    'reason'                      => 'test',
#    'expected_delivery_datetime'  => 'getutcdate()',
#    'next_attempt_datetime'       => 'getutcdate()',
#    'type'                        => 'SMS_INTERNAL', # [SMS_INTERNAL|SMS_EXTERNAL|EMAIL_POSTAGEAPP]
#    'status'                      => 'TO_SEND'       # [TO_SEND|SENT|SKIPPED|SENDING+PID|ERROR]
#  )
#)."\n\n";

#$r = htt_messaging_queue_add(
#  array(
#    'customer_id'                 => 123,
#    'reason'                      => 'test',
#    'expected_delivery_datetime'  => 'getutcdate()',
#    'next_attempt_datetime'       => 'getutcdate()',
#    'type'                        => 'SMS_INTERNAL',
#    'messaging_queue_params'      => array(
#      'message' => 'Howdy 2'
#    )
#  )
#);

#echo htt_messaging_queue_select_query(
#  array(
#    'sql_limit'                     => 1,
#    'customer_id'                   => 123,
#    'status'                        => 'SKIPPED',
#    'expected_delivery_datetime_lt' => 'getutcdate()',
#    'next_attempt_datetime_lt'      => 'getutcdate()',
#    'customers_overlay_ultra_fields' => array('o.current_mobile_number')
#  )
#)."\n\n";

/**
 * htt_messaging_queue_update_query 
 * $params: expected_delivery_datetime
 *          next_attempt_datetime
 *          attempt_count
 *          attempt_log
 *          status
 *          htt_messaging_queue_id
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_messaging_queue_update_query($params)
{
  $set_clause = '';
  $comma = '';

  if ( isset($params['expected_delivery_datetime']) )
  {
    $set_clause .= $comma.sprintf(" EXPECTED_DELIVERY_DATETIME = %s",$params['expected_delivery_datetime']);
    $comma = ',';
  }

  if ( isset($params['next_attempt_datetime']) )
  {
    $set_clause .= $comma.sprintf(" NEXT_ATTEMPT_DATETIME = %s",$params['next_attempt_datetime']);
    $comma = ',';
  }

  if ( isset($params['attempt_count']) && ( $params['attempt_count'] === '+1' ) )
  {
    $set_clause .= $comma." ATTEMPT_COUNT = ATTEMPT_COUNT+1";
    $comma = ',';
  }

  if ( isset($params['attempt_log']) )
  {
    $set_clause .= $comma.sprintf(" ATTEMPT_LOG = '%s'",$params['attempt_log']);
    $comma = ',';
  }

  if ( isset($params['status']) )
  {
    $set_clause .= $comma.sprintf(" STATUS = %s",mssql_escape_with_zeroes($params['status']));
    $comma = ',';
  }

  $htt_messaging_queue_update_query = sprintf(
    "UPDATE HTT_MESSAGING_QUEUE SET $set_clause WHERE HTT_MESSAGING_QUEUE_ID = %d",
    $params['htt_messaging_queue_id']
  );

  return $htt_messaging_queue_update_query;
}

/**
 * htt_messaging_queue_param_add
 * $params: param
 *          value
 *          type
 *          customer_id
 *          status
 *          htt_messaging_queue_id
 *          
 * @param  array $params
 * @return object Result
 */
function htt_messaging_queue_param_add($params)
{
  // insert into HTT_MESSAGING_QUEUE_PARAM

  $sql = sprintf(
    "INSERT INTO HTT_MESSAGING_QUEUE_PARAM
    (
      HTT_MESSAGING_QUEUE_ID,
      PARAM,
      VALUE
    )
    SELECT
      HTT_MESSAGING_QUEUE_ID,
      %s,
      %s
    FROM
      HTT_MESSAGING_QUEUE
    WHERE
      TYPE        = %s AND
      CUSTOMER_ID = %d AND
      STATUS      = %s AND
      HTT_MESSAGING_QUEUE_ID = %d
    ",
    mssql_escape_with_zeroes($params['param']),
    mssql_escape_with_zeroes($params['value']),
    mssql_escape_with_zeroes($params['type']),
    $params['customer_id'],
    mssql_escape_with_zeroes($params['status']),
    $params['htt_messaging_queue_id']
  );

  return run_sql_and_check_result($sql);
}

/**
 * htt_messaging_queue_param_add_multiple
 * @param  array (param => value)
 * @return object Result
 */
function htt_messaging_queue_param_add_multiple($params, array $messaging_queue_params)
{
  $query = 'INSERT INTO HTT_MESSAGING_QUEUE_PARAM ( HTT_MESSAGING_QUEUE_ID, PARAM, VALUE ) ';
  $i = 0;
  foreach ($messaging_queue_params as $param => $value)
  {
    $string = sprintf(
      "SELECT HTT_MESSAGING_QUEUE_ID, %s, %s FROM HTT_MESSAGING_QUEUE with (nolock)
      WHERE
      TYPE        = %s AND
      CUSTOMER_ID = %d AND
      STATUS      = %s AND
      HTT_MESSAGING_QUEUE_ID = %d",
      mssql_escape_with_zeroes($param),
      mssql_escape_with_zeroes($value),
      mssql_escape_with_zeroes($params['type']),
      $params['customer_id'],
      mssql_escape_with_zeroes($params['status']),
      $params['htt_messaging_queue_id']
    );

    if ($i) $query .= ' UNION ';
    else $i++;

    $query .= $string;
  }

  return run_sql_and_check_result($query);
}

/**
 * htt_messaging_queue_add
 * $params: see htt_messaging_queue_insert_query
 *          + messaging_queue_params
 *          
 * @param  array $params
 * @param  object $redis_messaging \Ultra\Lib\Util\Redis\Messaging
 * @return object Result
 */
function htt_messaging_queue_add($params,$redis_messaging=NULL)
{
  // add a new message to HTT_MESSAGING_QUEUE and HTT_MESSAGING_QUEUE_PARAM in a transaction

  if ( ! start_mssql_transaction() )
  {
    return make_error_Result("Cannot start a transaction");
  }

  $params['status'] = 'TO_SEND';

  $htt_messaging_queue_insert_query = htt_messaging_queue_insert_query( $params );

  $result = run_sql_and_check_result($htt_messaging_queue_insert_query);

  $htt_messaging_queue_id = NULL;

  if ($result->is_success())
  {
    $htt_messaging_queue_id = get_scope_identity();

    if ( ! $htt_messaging_queue_id || ! is_numeric($htt_messaging_queue_id) )
      $result = make_error_Result("Cannot retrieve HTT_MESSAGING_QUEUE_ID as scope_identity() after inserting into HTT_MESSAGING_QUEUE");

    if (isset($params['messaging_queue_params']) && count($params['messaging_queue_params']))
    {
      dlog('',"adding messaging_queue_params : ".json_encode($params['messaging_queue_params']));

      $param_add_array = array(
        'type'        => $params['type'],
        'customer_id' => $params['customer_id'],
        'status'      => $params['status'],
        'htt_messaging_queue_id' => $htt_messaging_queue_id
      );

      $result = htt_messaging_queue_param_add_multiple($param_add_array, $params['messaging_queue_params']);
    }
  }

  if ( $result->is_success() )
  {
    if ( ! commit_mssql_transaction() )
    {
      $result = make_error_Result("Cannot commit transaction");
    }
  }
  else
  {
    if ( ! rollback_mssql_transaction() )
    {
      $result->add_error("Cannot rollback transaction");
    }
  }

  if ( $result->is_success() )
  {
    if ( is_null($redis_messaging) )
      $redis_messaging = new \Ultra\Lib\Util\Redis\Messaging();

/*

worth noting: $params will be

for ``immediate`` messages : {"customer_id":31,"reason":"activate_planinfo","expected_delivery_datetime":"getutcdate()","next_attempt_datetime":"getutcdate()","type":"SMS_INTERNAL","messaging_queue_params":{"plan_name":"Ultra $29","renewal_date":"Mar 11 2013","message_type":"activate_planinfo"},"status":"TO_SEND"}

for ``daily`` messages : {"customer_id":31,"reason":"renewal_plan_fail_change","type":"SMS_INTERNAL","messaging_queue_params":{"plan_amount":"39.00","plan_amount_upgrade":"49.00","message_type":"renewal_plan_fail_change"},"expected_delivery_datetime":"getutcdate()","next_attempt_datetime":"\n CASE\n WHEN\n  ( DATEPART(hour,getutcdate()) > 12 AND DATEPART(hour,getutcdate()) < 3 )\n THEN\n  getutcdate()\n ELSE\n  dateadd(second, - DATEPART( second , getutcdate() ),\n    dateadd(minute, - DATEPART( minute , getutcdate() ),\n      dateadd(hh , 13-DATEPART( hour , getutcdate() ), getutcdate() ) \n    )\n  )\n END\n","status":"TO_SEND","next_attempt_timestamp":1379098174}

for ``initialized`` messages : {"customer_id":2147,"reason":"plan_expires_in_5_days","expected_delivery_datetime":["2013","09","12",13,0],"next_attempt_datetime":["2013","09","12",13,0],"type":"SMS_INTERNAL","conditional":1,"messaging_queue_params":{"message_type":"plan_close_to_expiration","plan_amount":49,"renewal_date":"Sep 17th"},"status":"TO_SEND"}

*/

    htt_messaging_queue_add_to_redis( $redis_messaging , $htt_messaging_queue_id , $params );
  }

  return $result;
}

/**
 * messaging_queue_add_to_redis
 *
 * Adds $htt_messaging_queue_id to the appropriate Redis data structure
 * $params: reason
 *          next_attempt_timestamp
 *          expected_delivery_timestamp
 *
 * @param  object $redis_messaging \Ultra\Lib\Util\Redis\Messaging
 * @param  integer $htt_messaging_queue_id
 * @param  array $params
 * @return NULL
 */
function htt_messaging_queue_add_to_redis( $redis_messaging , $htt_messaging_queue_id , $params )
{
  dlog('', '(%s)', func_get_args());

  # priority is extrapolated from $params['reason']
  $priority = extrapolate_message_priority( $params['reason'] );

  if ( isset($params['next_attempt_timestamp']) )
  {
    $timestamp = $params['next_attempt_timestamp'];
  }
  elseif( isset($params['expected_delivery_timestamp']) )
  {
    $timestamp = extrapolate_delivery_timestamp( $params['expected_delivery_timestamp'] );
  }
  else
  {
    $timestamp = time();
  }

  $redis_messaging->addMessageId( $htt_messaging_queue_id , $priority , $timestamp );
}

/**
 * extrapolate_delivery_timestamp
 *
 * $expected_delivery_datetime contain array( $YEAR , $MONTH , $DAY , $HOUR , $SECONDS ) or "getutcdate()"
 *
 * @param  mixed $expected_delivery_datetime (timestamp || (YYYY-MM-DD))
 * @return integer $timestamp
 */
function extrapolate_delivery_timestamp( $expected_delivery_datetime )
{
  $timestamp = time();

  if ( is_int( $expected_delivery_datetime ) )
  {
    $timestamp = $expected_delivery_datetime;
  }
  elseif ( is_array($expected_delivery_datetime) )
  {
    $date = new DateTime( sprintf ("%04d-%02d-%02d", $expected_delivery_datetime[0] , $expected_delivery_datetime[1], $expected_delivery_datetime[2]) );

    dlog('',"expected_delivery_datetime = %s",$expected_delivery_datetime);
    dlog('',"date = %s",$date);

    # -7 because we are PST
    $timestamp = $date->getTimestamp() + ( ( $expected_delivery_datetime[3] - 7 ) * 60 * 60 ) + $expected_delivery_datetime[4];
  }

  dlog('',"returning $timestamp (".timestamp_to_date( $timestamp ).")");

  return $timestamp;
}

/**
 * extrapolate_message_priority
 *
 * @param  string $reason
 * @return integer [1..3]
 */
function extrapolate_message_priority( $reason )
{
  $priority = 1;

  $priority_by_reason = array(
    'auto_recharge_in_2_days'     => 3,
    'auto_monthly_cc_renewal'     => 3,
    'plan_expires_in_2_days'      => 3,
    'plan_expires_in_5_days'      => 3,
    'plan_suspended_two_days_ago' => 3,
    'plan_close_to_expiration'    => 3,
    'plan_provisioned_reminder'   => 3,
    'monthly_renewal'             => 2,
    'plan_expires_today'          => 2,
    'immediate_plan_change'       => 2,
    'balanceadd_reload'           => 2,
    'plan_provisioned'            => 2,    
    'renewal_plan_change'         => 2,
    'renewal_plan_fail_change'    => 2,
    'balanceadd_recharge'         => 2
  );

  if ( isset($priority_by_reason[ $reason ]) )
    $priority = $priority_by_reason[ $reason ];

  return $priority;
}

/**
 * htt_messaging_queue_next_message_to_send_query 
 * @return string SQL
 */
function htt_messaging_queue_next_message_to_send_query()
{
  // we may need a condition for EXPECTED_DELIVERY_DATETIME (gt)

  // 3 is low priority, 1 is high priority

  $query =
    "SELECT   TOP(1)
              HTT_MESSAGING_QUEUE_ID,
              REASON,
              CREATED_DATETIME,
              EXPECTED_DELIVERY_DATETIME,
              NEXT_ATTEMPT_DATETIME,
              TYPE,
              ATTEMPT_COUNT,
              CUSTOMER_ID,
              ATTEMPT_LOG,
              CONDITIONAL,
              STATUS,

CASE
WHEN ( q.REASON = 'auto_monthly_cc_renewal'     OR
       q.REASON = 'plan_expires_today'          OR
       q.REASON = 'plan_suspended_two_days_ago' OR
       q.REASON = 'plan_close_to_expiration'    )
THEN 3
WHEN ( q.REASON = 'immediate_plan_change'       OR
       q.REASON = 'balanceadd_reload'           OR
       q.REASON = 'plan_provisioned'            OR
       q.REASON = 'monthly_renewal'             OR
       q.REASON = 'renewal_plan_change'         OR
       q.REASON = 'renewal_plan_fail_change'    OR
       q.REASON = 'balanceadd_recharge'         )
THEN 2
ELSE 1
END PRIORITY

     FROM     HTT_MESSAGING_QUEUE q
     WHERE    ( q.STATUS = 'TO_SEND' OR ( q.STATUS = 'ERROR' AND q.ATTEMPT_COUNT < 4 ) )
     AND      q.NEXT_ATTEMPT_DATETIME < getutcdate()
     ORDER BY PRIORITY ASC , q.CREATED_DATETIME ASC
    ";

  return $query;
}

/**
 * htt_messaging_queue_select_query 
 * $params: htt_messaging_queue_id
 *          customer_id
 *          type
 *          status
 *          reason
 *          expected_delivery_datetime_lt
 *          next_attempt_datetime_lt
 *          created_datetime_gte
 *          attempt_count_lt
 *          customers_overlay_ultra_fields
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_messaging_queue_select_query($params)
{
  $top_clause = get_sql_top_clause($params);
  $select_fields = "
              q.HTT_MESSAGING_QUEUE_ID,
              q.REASON,
              q.CREATED_DATETIME,
              q.EXPECTED_DELIVERY_DATETIME,
              q.NEXT_ATTEMPT_DATETIME,
              q.TYPE,
              q.ATTEMPT_COUNT,
              q.CUSTOMER_ID,
              q.ATTEMPT_LOG,
              q.CONDITIONAL,
              q.STATUS
  ";
  $where_clause = 'WHERE ';
  $additional_join = '';
  $and = '';

  if ( isset($params['htt_messaging_queue_id']) )
  {
    $where_clause .= $and.sprintf("q.HTT_MESSAGING_QUEUE_ID = %d",$params['htt_messaging_queue_id']);
    $and = ' AND ';
  }

  if ( isset($params['customer_id']) )
  {
    $where_clause .= $and.sprintf("q.CUSTOMER_ID = %d",$params['customer_id']);
    $and = ' AND ';
  }

  if ( isset($params['type']) )
  {
    $where_clause .= $and.sprintf("q.TYPE = %s",mssql_escape_with_zeroes($params['type']));
    $and = ' AND ';
  }

  if ( isset($params['status']) )
  {
    $where_clause .= $and.sprintf("q.STATUS = %s",mssql_escape_with_zeroes($params['status']));
    $and = ' AND ';
  }

  if ( isset($params['reason']) )
  {
    $where_clause .= $and.sprintf("q.REASON = %s",mssql_escape_with_zeroes($params['reason']));
    $and = ' AND ';
  }

  if ( isset($params['expected_delivery_datetime_lt']) )
  {
    $where_clause .= $and.sprintf("q.EXPECTED_DELIVERY_DATETIME < %s",$params['expected_delivery_datetime_lt']);
    $and = ' AND ';
  }

  if ( isset($params['next_attempt_datetime_lt']) )
  {
    $where_clause .= $and.sprintf("q.NEXT_ATTEMPT_DATETIME < %s",$params['next_attempt_datetime_lt']);
    $and = ' AND ';
  }

  if ( isset($params['created_datetime_gte']) )
  {
    $where_clause .= $and.sprintf("q.CREATED_DATETIME >= %s",$params['created_datetime_gte']);
    $and = ' AND ';
  }

  if ( isset($params['attempt_count_lt']) )
  {
    $where_clause .= $and.sprintf("q.ATTEMPT_COUNT < %d",$params['attempt_count_lt']);
    $and = ' AND ';
  }

  if ( isset($params['customers_overlay_ultra_fields']) )
  {
    $additional_join .= " JOIN htt_customers_overlay_ultra o ON o.customer_id = q.customer_id ";
    $select_fields   .= ' , ' . implode(',', $params['customers_overlay_ultra_fields'] );
  }

  $query =
    "SELECT $top_clause $select_fields
     FROM   HTT_MESSAGING_QUEUE q with (nolock)
     $additional_join
     $where_clause
     ORDER BY q.CREATED_DATETIME ASC
    ";

  return $query;
}

/**
 * htt_messaging_queue_param_select_query 
 * $params: htt_messaging_queue_id
 * @param  array $params
 * @return string SQL
 */
function htt_messaging_queue_param_select_query($params)
{
  $query = sprintf("
    SELECT *
    FROM   HTT_MESSAGING_QUEUE_PARAM with (nolock)
    WHERE  HTT_MESSAGING_QUEUE_ID = %d",
    $params['htt_messaging_queue_id']
  );

  return $query;
}

/**
 * htt_messaging_queue_insert_query 
 * @param  Integer $params
 * @return string SQL
 */
function htt_messaging_queue_insert_query($params)
{
  $insert_init = "";

  if ( ! isset($params['conditional']) )
  {
    $params['conditional'] = 0;
  }

  if ( is_array($params['expected_delivery_datetime']) )
  {
    // $params['expected_delivery_datetime'] contain array( $YEAR , $MONTH , $DAY , $HOUR , $SECONDS )

    $insert_init .= sprintf("
Declare @expected_delivery_datetime_dd   TinyInt Set @expected_delivery_datetime_dd   = %d;
Declare @expected_delivery_datetime_mm   TinyInt Set @expected_delivery_datetime_mm   = %d;
Declare @expected_delivery_datetime_YYYY Integer Set @expected_delivery_datetime_YYYY = %d;
      ",
      $params['expected_delivery_datetime'][2],
      $params['expected_delivery_datetime'][1],
      $params['expected_delivery_datetime'][0]
    );

    $expected_delivery_datetime = sprintf("
DateAdd(hh, %d, DateAdd(yy, @expected_delivery_datetime_YYYY-1900, DateAdd(m, @expected_delivery_datetime_mm - 1, @expected_delivery_datetime_dd - 1)))
    ",$params['expected_delivery_datetime'][3]);

    if ( array_key_exists( 4 , $params['expected_delivery_datetime'] ) )
    {
      $expected_delivery_datetime = sprintf("DateAdd(ss, %d , $expected_delivery_datetime)",$params['expected_delivery_datetime'][4]);
    }

    $params['expected_delivery_datetime'] = $expected_delivery_datetime;
  }
  elseif( $params['expected_delivery_datetime'] && !preg_match("/getutcdate/i", $params['expected_delivery_datetime'] ) )
  {
    $params['expected_delivery_datetime'] = "'".$params['expected_delivery_datetime']."'";
  }

  if ( is_array($params['next_attempt_datetime']) )
  {
    // $params['next_attempt_datetime'] contain array( $YEAR , $MONTH , $DAY , $HOUR , $SECONDS )

    $insert_init .= sprintf("
Declare @next_attempt_datetime_dd   TinyInt Set @next_attempt_datetime_dd   = %d;
Declare @next_attempt_datetime_mm   TinyInt Set @next_attempt_datetime_mm   = %d;
Declare @next_attempt_datetime_YYYY Integer Set @next_attempt_datetime_YYYY = %d;
      ",
      $params['next_attempt_datetime'][2],
      $params['next_attempt_datetime'][1],
      $params['next_attempt_datetime'][0]
    );

    $next_attempt_datetime = sprintf("
DateAdd(hh, %d, DateAdd(yy, @next_attempt_datetime_YYYY-1900, DateAdd(m, @next_attempt_datetime_mm - 1, @next_attempt_datetime_dd - 1)))
    ",$params['next_attempt_datetime'][3]);

    if ( array_key_exists( 4 , $params['next_attempt_datetime'] ) )
    {
      $next_attempt_datetime = sprintf("DateAdd(ss, %d , $next_attempt_datetime)",$params['next_attempt_datetime'][4]);
    }

    $params['next_attempt_datetime'] = $next_attempt_datetime;
  }
  elseif( $params['next_attempt_datetime'] && !preg_match("/getutcdate/i", $params['next_attempt_datetime'] ) )
  {
    $params['next_attempt_datetime'] = "'".$params['next_attempt_datetime']."'";
  }

  $query = sprintf(
    "$insert_init
INSERT INTO HTT_MESSAGING_QUEUE
(
  REASON,
  CREATED_DATETIME,
  EXPECTED_DELIVERY_DATETIME,
  NEXT_ATTEMPT_DATETIME,
  TYPE,
  ATTEMPT_COUNT,
  CUSTOMER_ID,
  ATTEMPT_LOG,
  STATUS,
  CONDITIONAL
)
VALUES
(
  %s,
  getutcdate(),
  %s,
  %s,
  %s,
  0,
  %d,
  '',
  %s,
  %d
)",
  mssql_escape_with_zeroes($params['reason']),
  $params['expected_delivery_datetime'],
  $params['next_attempt_datetime'],
  mssql_escape_with_zeroes($params['type']),
  $params['customer_id'],
  mssql_escape_with_zeroes($params['status']),
  $params['conditional']
  );

  return $query;
}

/**
 * verify_sms_delivery 
 * @param  integer $customer_id [description]
 * @param  string $reason
 * @param  string $type
 * @return boolean
 */
function verify_sms_delivery( $customer_id , $reason , $type )
{
  $sql = htt_messaging_queue_select_query(
    array(
      'sql_limit'                 => 1,
      'customer_id'               => $customer_id,
      'reason'                    => $reason,
      'type'                      => $type,
      'status'                    => 'SENT'
    )
  );

  $results = mssql_fetch_all_objects(logged_mssql_query($sql));

  dlog('',"results = %s",$results);

  return ( $results && is_array($results) && count($results) );
}

/**
 * log_sms_delivery 
 * @param  integer $customer_id
 * @param  string $reason
 * @param  string $type
 * @param  string $error
 * @return boolean
 */
function log_sms_delivery( $customer_id , $reason , $type , $error=NULL )
{
  $attempt_log = 'delivered';
  $status      = 'SENT';

  if ( $error )
  {
    $attempt_log = $error;
    $status      = 'ERROR';
  }

  $sql = sprintf("
    INSERT INTO HTT_MESSAGING_QUEUE
(
  REASON,
  CREATED_DATETIME,
  EXPECTED_DELIVERY_DATETIME,
  NEXT_ATTEMPT_DATETIME,
  TYPE,
  ATTEMPT_COUNT,
  CUSTOMER_ID,
  ATTEMPT_LOG,
  STATUS,
  CONDITIONAL
)
VALUES
(
  %s,
  getutcdate(),
  getutcdate(),
  NULL,
  %s,
  1,
  %d,
  '%s',
  %s,
  0
) ",
    mssql_escape_with_zeroes($reason),
    mssql_escape_with_zeroes($type),
    $customer_id,
    $attempt_log,
    mssql_escape_with_zeroes($status)
  );

  return run_sql_and_check($sql);
}

/**
 * archive_messaging_queue_query 
 * @return string SQL
 */
function archive_messaging_queue_query()
{
  return "
  BEGIN TRANSACTION ArchiveMessagingQueue
  BEGIN TRY

  IF OBJECT_ID('tempdb..#MESSAGING_Q_TO_ARCHIVE', 'U') IS NOT NULL
  DROP TABLE #MESSAGING_Q_TO_ARCHIVE

  IF OBJECT_ID('tempdb..#MESSAGING_P_TO_ARCHIVE', 'U') IS NOT NULL
  DROP TABLE #MESSAGING_P_TO_ARCHIVE

  SELECT TOP 5000 HTT_MESSAGING_QUEUE_ID
  INTO   #MESSAGING_Q_TO_ARCHIVE
  FROM   HTT_MESSAGING_QUEUE
  WHERE  CREATED_DATETIME < DATEADD(DD,-30,GETUTCDATE())
  AND    STATUS IN ('SENT')

  SELECT HTT_MESSAGING_QUEUE_ID
  INTO   #MESSAGING_P_TO_ARCHIVE
  FROM   HTT_MESSAGING_QUEUE_PARAM
  WHERE  HTT_MESSAGING_QUEUE_ID IN ( SELECT * FROM #MESSAGING_Q_TO_ARCHIVE )

  SELECT COUNT(*) FROM #MESSAGING_Q_TO_ARCHIVE -- why?
  SELECT COUNT(*) FROM #MESSAGING_P_TO_ARCHIVE -- why?

  DELETE HTT_MESSAGING_QUEUE_PARAM
         OUTPUT
           DELETED.HTT_MESSAGING_QUEUE_ID,
           DELETED.PARAM,
           DELETED.VALUE
         INTO dbo.HTT_MESSAGING_PARAM_ARCHIVE
           WHERE HTT_MESSAGING_QUEUE_ID IN ( SELECT * FROM #MESSAGING_P_TO_ARCHIVE )

  DELETE HTT_MESSAGING_QUEUE
         OUTPUT
           DELETED.HTT_MESSAGING_QUEUE_ID,
           DELETED.REASON,
           DELETED.CREATED_DATETIME,
           DELETED.EXPECTED_DELIVERY_DATETIME,
           DELETED.NEXT_ATTEMPT_DATETIME,
           DELETED.TYPE,
           DELETED.ATTEMPT_COUNT,
           DELETED.CUSTOMER_ID,
           DELETED.ATTEMPT_LOG,
           DELETED.CONDITIONAL,
           DELETED.STATUS
         INTO dbo.HTT_MESSAGING_ARCHIVE
           WHERE HTT_MESSAGING_QUEUE_ID IN ( SELECT * FROM #MESSAGING_Q_TO_ARCHIVE )

  END TRY
  BEGIN CATCH
      ROLLBACK TRANSACTION ArchiveMessagingQueue
  END CATCH
  COMMIT TRANSACTION ArchiveMessagingQueue
";
}

?>
