<?php

require_once 'Ultra/Lib/DB/Customer.php';
require_once 'Ultra/Lib/DB/MSSQL.php';

/*
DB table ULTRA.CUSTOMERS_DEALERS
*/

/**
 * add_to_ultra_customers_dealers
 * $params: see ultra_customers_dealers_insert_query
 *
 * @param  array $params
 * @return boolean
 */
function add_to_ultra_customers_dealers( $params )
{
  $ultra_customers_dealers_insert_query = ultra_customers_dealers_insert_query( $params );

  return is_mssql_successful(logged_mssql_query($ultra_customers_dealers_insert_query));
}

/**
 * ultra_customers_dealers_insert_query
 * $params: customer_id
 *          dealer
 *          association_type
 *
 * @param  array $params
 * @return string
 */
function ultra_customers_dealers_insert_query( $params )
{
  $customer_id      = sprintf("%d",$params['customer_id']);
  $dealer           = sprintf("%d",$params['dealer']);
  $association_type = sprintf("%s",mssql_escape_with_zeroes($params['association_type']));

  return "INSERT INTO ULTRA.CUSTOMERS_DEALERS
   (
 CUSTOMER_ID,
 DEALER,
 ASSOCIATION_TYPE
   ) VALUES (
 $customer_id,
 $dealer,
 $association_type
  )";
}

/**
 * can_map_dealer_to_customer
 *
 * Verify if $dealer is associated to $customer_id
 * Checks Redis and - if necessary - check ULTRA.CUSTOMERS_DEALERS and ULTRA.HTT_ACTIVATION_HISTORY
 *
 * @param  integer $dealer
 * @param  integer $customer_id
 * @return boolean
 */
function can_map_dealer_to_customer( $dealer , $customer_id , $redis=NULL )
{
  dlog('',"dealer = $dealer ; customer_id = $customer_id");

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $redis_dealer = $redis->get('ultra/api/dealer_by_customer_id/'.$customer_id.'/'.$dealer);

  if ( $redis_dealer )
  {
    $redis->set( 'ultra/getter/ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID/'.$customer_id.'/DEALER' , $dealer , 10800 );

    return TRUE;
  }

  return can_map_dealer_to_customer_db_check( $dealer , $customer_id , $redis );
}

/**
 * can_map_dealer_to_customer_db_check
 *
 * Verify if $dealer is associated to $customer_id by querying the DB
 * Queries ULTRA.CUSTOMERS_DEALERS and ULTRA.HTT_ACTIVATION_HISTORY separately.
 *
 * @param  integer $dealer
 * @param  integer $customer_id
 * @param  $redis Redis object
 * @return boolean
 */
function can_map_dealer_to_customer_db_check( $dealer , $customer_id , $redis=NULL )
{
  dlog('',"dealer = $dealer ; customer_id = $customer_id");

  $can_map_dealer_to_customer_db_check = FALSE;

  // executes stored procedure [ultra].[Get_Dealers_For_Customer]
  $query_result = \Ultra\Lib\DB\Customer\getDealersForCustomer( $customer_id );

  dlog('',"query_result = %s",$query_result);

  if ( !( $query_result && is_array($query_result) && count($query_result) ) )
  {
    dlog('',"We could not find any dealer info associated to customer_id $customer_id");

    return FALSE;
  }

  $underscoreObject = new \__ ;

  $query_result = $underscoreObject->flatten( $query_result );

  if ( in_array( $dealer , $query_result ) )
    $can_map_dealer_to_customer_db_check = TRUE;
  else
  {
    // check "child dealers" of $dealer according to the 1:1 logic

    $db  = \Ultra\UltraConfig\celluphoneDb();

    $sql = sprintf("
      SELECT DealerSiteID
      FROM   ".$db."..tblDealerSite
      WHERE  ParentDealerSiteID = %d ",
      $dealer
    );

    $parent_result = mssql_fetch_all_rows(logged_mssql_query($sql));

    dlog('',"parent_result = %s",$parent_result);

    if ( $parent_result && is_array($parent_result) && count($parent_result) )
    {
      $intersection = $underscoreObject->intersection( $query_result , $underscoreObject->flatten( $parent_result ) );

      dlog('',"intersection = %s",$intersection);

      $can_map_dealer_to_customer_db_check = ! ! count($intersection);
    }
  }

  if ( $can_map_dealer_to_customer_db_check )
  {
    // keep in redis for 3 hours

    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    $redis->set( 'ultra/api/dealer_by_customer_id/'.$customer_id.'/'.$dealer , TRUE , 10800 );
    $redis->set( 'ultra/getter/ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID/'.$customer_id.'/DEALER' , $dealer , 10800 );
  }

  return $can_map_dealer_to_customer_db_check;
}

/**
 * get_parent_dealer_children
 *
 * Get dealer_ids due to the parent-child relationship between dealers
 *
 * @param  integer $dealer
 * @return array
 */
function get_parent_dealer_children( $dealer )
{
  if ( !$dealer || !is_numeric($dealer) )
    return array();

  $db  = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf(
    "SELECT DealerSiteID FROM ".$db."..tblDealerSite WHERE ParentDealerSiteID = %d ",
    $dealer
  );

  $data = mssql_fetch_all_rows(logged_mssql_query($sql));

  dlog('',"data = %s",$data);

  $underscoreObject = new \__ ;

  if ( $data && is_array($data) && count($data) )
    $data = $underscoreObject->flatten($data);
  else
    $data = array();

  return $data;
}


/**
 * get_dealers_from_master
 * @see SMR-8
 *
 * @param integer $master
 * @param array of DealerSiteID's or NULL
 */
function get_dealers_from_master($master)
{
  if (empty($master))
    return NULL;

  $db  = \Ultra\UltraConfig\celluphoneDb();
  $sql = sprintf("
    SELECT DealerSiteID
    FROM $db..tblDealerSite a
    WHERE a.MasterID = %d AND a.ActiveFlag = 1
    OR (a.DealerSiteID <> a.ParentDealerSiteID AND (SELECT b.MasterID FROM $db..tblDealerSite b WHERE b.DealerSiteID = a.ParentDealerSiteID) = %d)",
    $master, $master);

  $underscoreObject = new \__ ;

  $dealers = mssql_fetch_all_rows(logged_mssql_query($sql));
  if ($dealers && is_array($dealers) && count($dealers))
    return $underscoreObject->flatten($dealers);
  else
    return NULL;
}

/**
 * get_dealer_groups_from_master
 * given a master agent return an array of objects for each parent dealer with the children
 * @see SMR-16
 * @param integer master agent ID
 * @return $result array[object(parent, business, children)]
 */
function get_dealer_groups_from_master($master)
{
  $result = array();

  if (empty($master))
  {
    dlog('', 'ERROR: invalid parameters %s', func_get_args());
    return $result;
  }

  // get all active dealers for this master and join their children into one big happy family
  $db = \Ultra\UltraConfig\celluphoneDb();
  $sql = sprintf("SELECT a.DealerSiteID AS parent, b.DealerSiteID AS child, a.BusinessName as business, a.Dealercd AS dealer_code
    FROM $db..tblDealerSite a WITH (NOLOCK)
    JOIN $db..tblDealerSite b WITH (NOLOCK) ON a.DealerSiteID = b.ParentDealerSiteID OR a.DealerSiteID = b.DealerSiteID
    WHERE a.MasterID = %d AND a.ActiveFlag = 1
    ORDER BY a.DealerSiteID", $master);
  $dealers = mssql_fetch_all_objects(logged_mssql_query($sql));

  // post-process to split into objects of parents with their respective children
  $group = NULL;
  foreach ($dealers as $dealer)
  {
    // next parent: create a new group
    if ( ! $group || $group->parent != $dealer->parent)
    {
      // save the previous group and create a new one for this parent
      if ($group)
        $result[] = $group;
      $group = clone $dealer;
      $group->children = array($dealer->child);
    }
    else
      $group->children[].= $dealer->child;
  }

  if ($group)
    $result[] = $group;

  return $result;
}

/**
 * get_dealer_from_customer_id
 * get dealer who activated the customer
 * @see MVNO-2595
 * @param integer customer_id
 * @return object containing minimum needed fields
 */
function get_dealer_from_customer_id($customer_id)
{
  if (empty($customer_id))
  {
    dlog('', 'ERROR: invalid paramters %s', func_get_args());
    return NULL;
  }

  // PROD-1149: we cannot use ULTRA.ACTIVATION_DETAILS because it returns multiple rows if the same ICCID was re-used for a different subscriber (i.e. failed port followed by an activation)
  $db = \Ultra\UltraConfig\celluphoneDb();
  $sql = sprintf('SELECT d.Dealercd
    FROM (ULTRA.HTT_ACTIVATION_HISTORY AS h WITH (NOLOCK)
    LEFT JOIN ULTRA.ACTIVATION_LOG_OVERRIDE AS o WITH (NOLOCK) ON h.ICCID_FULL = o.ACTIVATION_ICCID)
    JOIN %s.DBO.tblDealerSite AS d WITH (NOLOCK)
    ON d.DealerSiteID = (CASE WHEN o.NEW_STORE IS NULL THEN h.DEALER ELSE o.NEW_STORE END)
    WHERE h.CUSTOMER_ID = %d', $db, $customer_id);
  $dealer = mssql_fetch_all_objects(logged_mssql_query($sql));

  // sanity check
  if (count($dealer) > 1)
  {
    dlog('', "ERROR: found multiple activating dealers for customer_id $customer_id");
    return NULL;
  }

  if (count($dealer) && ! empty($dealer[0]))
    return $dealer[0];
  else
  {
    dlog('', "ERROR: no dealer found for customer_id $customer_id");
    return NULL;
  }

}

/**
 * get_dealer_email_from_dealer_site_id
 * @param  integer $dealer_site_id
 * @return string email address
 */
function get_dealer_email_from_dealer_site_id($dealer_site_id)
{
  $db = \Ultra\UltraConfig\celluphoneDb();

  $sql = sprintf("SELECT email FROM %s.DBO.tblDealerSite d, %s.DBO.tblPerson p, %s.DBO.tbldealersiteperson dsp
    WHERE d.dealersiteid = dsp.dealersiteid
    AND dsp.personid = p.personid
    AND dsp.persontypecd = 'OW'
    AND (dsp.expirationdt is NULL OR dsp.expirationdt > CURRENT_TIMESTAMP)
    AND d.dealersiteid = %d", $db, $db, $db, $dealer_site_id);

  $dealer = mssql_fetch_all_objects(logged_mssql_query($sql));

  if (empty($dealer[0]))
  {
    dlog('', "ERROR: no email found for dealer ID $dealer_site_id");
    return NULL;
  }

  return $dealer[0]->email;
}

