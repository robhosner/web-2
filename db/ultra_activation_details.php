<?php

/**
 * make_find_ultra_activation_details
 * $params: date_selected
 *          date_from
 *          date_to
 *          epoch_from
 *          epoch_to
 *          customer_id
 *          iccid
 *          msisdn
 *          cos_id
 *          masteragent
 *          distributor
 *          dealer
 *          userid
 *          activation_type
 *          final_state
 *          funding_source
 * @param  array $params
 * @return string SQL
 */
function make_find_ultra_activation_details( $params )
{
  $where = array();

  if ( !isset($params['date_selected']) || !$params['date_selected'] )
    $params['date_selected'] = 'LAST_UPDATED';

  if ( isset($params['date_from']) && isset($params['date_to']) )
  {
    if ( ( $params['date_selected'] != 'LAST_UPDATED' ) && ( $params['date_selected'] != 'CREATION' ) )
    {
      dlog('',"date_selected is not valid : ".$params['date_selected']);
      return '';
    }

    $params['date_from'] = date_from_pst_to_utc( $params['date_from'] , 0 );
    $params['date_to']   = date_from_pst_to_utc( $params['date_to']   , 1 );

    $where[] = " ".$params['date_selected']."_DATE BETWEEN '".$params['date_from']."' AND '".$params['date_to']."' ";
  }

  if ( isset($params['epoch_from']) && isset($params['epoch_to']) )
  {
    if ( ( $params['date_selected'] != 'LAST_UPDATED' ) && ( $params['date_selected'] != 'CREATION' ) )
    {
      dlog('',"date_selected is not valid : ".$params['date_selected']);
      return '';
    }

    $where[] = $params['date_selected']."_DATE between dateadd(s, ".$params['epoch_from'].", '19700101') and dateadd(s, ".$params['epoch_to'].", '19700101') ";
  }

  if ( isset($params['customer_id']) )
    $where[] = sprintf( " a.CUSTOMER_ID = %d " , $params['customer_id'] );

  if ( isset($params['iccid']) )
    $where[] = sprintf( " ICCID_FULL  = %s " , mssql_escape_with_zeroes( luhnenize( $params['iccid'] ) ) );

  if ( isset($params['msisdn']) )
    $where[] = sprintf( " MSISDN      = %s " , mssql_escape_with_zeroes($params['msisdn']) );

  if ( isset($params['cos_id']) )
    $where[] = sprintf( " COS_ID      = %d " , $params['cos_id'] );

  if ( isset($params['masteragent']) )
    $where[] = sprintf( " MASTERAGENT = %d " , $params['masteragent'] );

  if ( isset($params['distributor']) )
    $where[] = sprintf( " DISTRIBUTOR = %d " , $params['distributor'] );

  if ( isset($params['dealer']) && $params['dealer'] )
  {
    $dealer_list = make_where_clause_for_dealer_and_children($params['dealer']);

    if ( empty($dealer_list) )
    {
      dlog('',"No dealer found");

      return '';
    }

    $where[] = " (DEALER IN ($dealer_list) OR NEW_STORE IN ($dealer_list)) "; // PROD-1228: return all activations, both by the dealer and dealer override
  }

  if ( isset($params['userid']) )
    $where[] = sprintf( " USERID      = %d " , $params['userid'] );

  if ( isset($params['activation_type']) )
    $where[] = sprintf( " ACTIVATION_TYPE = %s " , mssql_escape_with_zeroes($params['activation_type']) );

  if ( isset($params['final_state']) )
    $where[] = sprintf( " FINAL_STATE     = %s " , mssql_escape_with_zeroes($params['final_state']) );

  if ( isset($params['funding_source']) )
    $where[] = sprintf( " FUNDING_SOURCE  = %s " , mssql_escape_with_zeroes($params['funding_source']) );

  // sanity check
  if ( ! count($where) )
    return '';

  return "SELECT
c.first_name,
c.last_name,
u.plan_state,
a.customer_id,
a.ICCID_FULL                        iccid,
a.msisdn,
a.cos_id,
COALESCE (alo.new_store,  a.dealer) dealer,
COALESCE (alo.new_userid, a.userid) userid,
ISNULL( DATEDIFF(ss, '1970-01-01', dbo.UTC_TO_PT(a.CREATION_DATE)            ) , '' ) created_date,
ISNULL( DATEDIFF(dd, '1970-01-01', a.PLAN_STARTED_DATE_TIME                    ) , '' ) plan_started,
ISNULL( DATEDIFF(ss, '1970-01-01', dbo.UTC_TO_PT(a.LAST_UPDATED_DATE)        ) , '' ) history_last_updated,
a.activation_type,
a.activation_transition,
a.final_state,
a.funding_source,
a.funding_amount,
a.last_attempted_transition,
Datediff( ss, a.CREATION_DATE    , Getutcdate() ) created_seconds_ago,
a.promised_amount
  FROM
ULTRA.HTT_ACTIVATION_HISTORY   a WITH (NOLOCK)
  JOIN
CUSTOMERS c WITH (NOLOCK)
  ON
a.CUSTOMER_ID = c.CUSTOMER_ID
  JOIN
HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK)
  ON
u.CUSTOMER_ID = c.CUSTOMER_ID
  LEFT OUTER JOIN
ULTRA.ACTIVATION_LOG_OVERRIDE  alo WITH (NOLOCK)
  ON
a.ICCID_FULL = alo.ACTIVATION_ICCID
  WHERE ".implode(" AND ", $where)."
  ORDER BY ".$params['date_selected']."_DATE DESC";
}

