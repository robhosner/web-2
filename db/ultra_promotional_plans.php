<?php


require_once 'db/htt_billing_history.php';
require_once 'lib/messaging/functions.php';


# php component to access DB table ULTRA.PROMOTIONAL_PLANS #


/*
Examples:


echo ultra_promotional_plans_insert_query(
  array(
    "project_name" => 'project_name TEST 3',
    "channel"      => 'channel TEST 3',
    "subchannel"   => 'subchannel TEST 3',
    "microchannel" => 'microchannel TEST 3',
    "iccid_start"  => '1234512345123451236',
    "iccid_end"    => '1234512345123451237',
    "ziprange"     => '12345',
    "must_activate_before" => 'GETUTCDATE()',
    "target_plan"  => '0',
    "include_plan_cycles" => '0',
    "promo_status" => 'VIRGIN',
    "masteragent"  => '11',
    "distributor"  => '12',
    "dealer"       => '13',
    "userid"       => '14',
    "created_by"   => 'Raf'
  )
)."\n\n";


echo ultra_promotional_plans_select_query(
  array(
  	'expired' => TRUE
  )
)."\n\n";
*/

/**
 * ultra_promotional_plans_insert_query
 * 
 * Query used to insert rows into ULTRA.PROMOTIONAL_PLANS
 * $params: project_name
 *          channel
 *          subchannel
 *          microchannel
 *          iccid_start
 *          iccid_end
 *          ziprange
 *          must_activate_before
 *          target_plan
 *          include_plan_cycles
 *          promo_status
 *          masteragent
 *          distributor
 *          dealer
 *          userid
 *          created_by
 * 
 * @param array $params
 * @return string -- Formatted query ready for execution
 */
function ultra_promotional_plans_insert_query ($params)
{
  $query = sprintf(
    "INSERT INTO ULTRA.PROMOTIONAL_PLANS
(
  PROJECT_NAME,
  CHANNEL,
  SUBCHANNEL,
  MICROCHANNEL,
  ICCID_START,
  ICCID_END,
  ZIPRANGE,
  MUST_ACTIVATE_BEFORE,
  TARGET_PLAN,
  INCLUDED_PLAN_CYCLES,
  PROMO_STATUS,
  ACTIVATION_MASTERAGENT,
  ACTIVATION_DISTRIBUTOR,
  ACTIVATION_DEALER,
  ACTIVATION_USERID,
  CREATED_BY
) VALUES (
  %s, -- PROJECT_NAME
  %s, -- CHANNEL
  %s, -- SUBCHANNEL
  %s, -- MICROCHANNEL
  %s, -- ICCID_START
  %s, -- ICCID_END
  %s, -- ZIPRANGE
  %s, -- MUST_ACTIVATE_BEFORE
  %d, -- TARGET_PLAN
  %d, -- INCLUDED_PLAN_CYCLES
  %s, -- PROMO_STATUS
  %d, -- ACTIVATION_MASTERAGENT
  %d, -- ACTIVATION_DISTRIBUTOR
  %d, -- ACTIVATION_DEALER
  %d, -- ACTIVATION_USERID
  %s  -- CREATED_BY
)",
      mssql_escape_with_zeroes($params["project_name"]),
      mssql_escape_with_zeroes($params["channel"]),
      mssql_escape_with_zeroes($params["subchannel"]),
      mssql_escape_with_zeroes($params["microchannel"]),
      mssql_escape_with_zeroes($params["iccid_start"]),
      mssql_escape_with_zeroes($params["iccid_end"]),
      mssql_escape_with_zeroes($params["ziprange"]),
      $params["must_activate_before"],
      $params["target_plan"],
      $params["include_plan_cycles"],
      mssql_escape_with_zeroes($params["promo_status"]),
      $params["masteragent"],
      $params["distributor"],
      $params["dealer"],
      $params["userid"],
      mssql_escape_with_zeroes($params["created_by"])
  );

  return $query;
}

/**
 * set_ultra_promotional_plan_status
 *
 * @param integer $ultra_promotional_plans_id
 * @param string $status
 * @return boolean result of SQL UPDATE
 */
function set_ultra_promotional_plan_status( $ultra_promotional_plans_id , $status )
{
  $sql = sprintf(
    "UPDATE ULTRA.PROMOTIONAL_PLANS
     SET    PROMO_STATUS = %s
     WHERE  ULTRA_PROMOTIONAL_PLANS_ID = %d",
    mssql_escape_with_zeroes($status),
    $ultra_promotional_plans_id
  );

  dlog('',"$sql");

  return run_sql_and_check($sql);
}

/**
 * validate_ultra_promotional_plans_status
 * $status can be an array of strings or a string
 *
 * @param integer $ultra_promotional_plans_id
 * @param mixed $status
 * @return object Result
 */
function validate_ultra_promotional_plans_status( $ultra_promotional_plans_id , $status )
{
  $result = new Result;

  $ultra_promotional_plan_data = get_ultra_promotional_plans_by_id( $ultra_promotional_plans_id );

  if ( $ultra_promotional_plan_data && is_array($ultra_promotional_plan_data) && count($ultra_promotional_plan_data) )
  {
    if ( is_array( $status ) )
    {
      if ( in_array( $ultra_promotional_plan_data[0]->PROMO_STATUS , $status ) )
      {
        $result->succeed();
      }
      else
      {
        $result->add_error("ERR_API_INVALID_ARGUMENTS: status is ".$ultra_promotional_plan_data[0]->PROMO_STATUS);
      }
    }
    else
    {
      if ( $status == $ultra_promotional_plan_data[0]->PROMO_STATUS )
      {
        $result->succeed();
      }
      else
      {
        $result->add_error("ERR_API_INVALID_ARGUMENTS: status is ".$ultra_promotional_plan_data[0]->PROMO_STATUS);
      }
    }
  }
  else
  {
    $result->add_error("ERR_API_INVALID_ARGUMENTS: no promotional plans found with id $ultra_promotional_plans_id");
  }

  return $result;
}

/**
 * get_ultra_promotional_plans_by_id
 *
 * @param integer $ultra_promotional_plans_id
 * @return objects[] promotional plans data
 */
function get_ultra_promotional_plans_by_id( $ultra_promotional_plans_id )
{
  $sql = ultra_promotional_plans_select_query(
    array(
      'ultra_promotional_plans_id' => $ultra_promotional_plans_id
    )
  );
  
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_ultra_promotional_plan_by_customer_id
 * Returns ULTRA.PROMOTIONAL_PLANS data given a customer id who is associated with such promotional plan
 *
 * @param integer $customer_id
 * @return objects[] promotional plans data
 */
function get_ultra_promotional_plan_by_customer_id( $customer_id )
{
  $sql = sprintf("
    SELECT p.*
    FROM   ULTRA.PROMOTIONAL_PLANS p
    JOIN   ULTRA.CUSTOMER_OPTIONS o
    ON     p.ULTRA_PROMOTIONAL_PLANS_ID = o.OPTION_VALUE
    WHERE  o.OPTION_ATTRIBUTE = 'PROMO_ASSIGNED'
    AND    o.CUSTOMER_ID      = %d",
    $customer_id
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * ultra_promotional_plans_select_query
 * Query used to select rows from ULTRA.PROMOTIONAL_PLANS
 * $params: select_fields
 *          ultra_promotional_plans_id
 *          expired
 * 
 * @param array $params
 * @return string -- Formatted query ready for execution
 */
function ultra_promotional_plans_select_query ($params)
{
  $select_fields = '*';

  $top_clause = get_sql_top_clause($params);

  $where = array();

  if ( isset($params['select_fields']) )
  {
    $select_fields =
      ( is_array( $params['select_fields'] ) )
      ?
      implode(',',$params['select_fields'])
      :
      $params['select_fields']
      ;
  }
  
  if ( isset($params['ultra_promotional_plans_id']) )
  {
    $where[] = sprintf(" ULTRA_PROMOTIONAL_PLANS_ID = %d ",$params['ultra_promotional_plans_id']);
  }
  
  if ( isset($params['expired']) && ( $params['expired'] === TRUE ) )
  {
    $where[] = " MUST_ACTIVATE_BEFORE IS NOT NULL AND MUST_ACTIVATE_BEFORE <= GETUTCDATE() ";
  }

  $where_clause =
    ( count($where) )
    ?
    " WHERE ".implode(" AND ", $where)
    :
    ''
    ;

  $query =
    "SELECT $top_clause $select_fields
     FROM   ULTRA.PROMOTIONAL_PLANS
     $where_clause";

  return $query;
}

/**
 * ultra_promotional_plans_pre_activation
 *
 * Must be invoked before initiating the 'Promo Unused' => 'Active' transition
 * ULTRA.PROMOTIONAL_PLANS.MUST_ACTIVATE_BEFORE . If value is < now, block activation
 * ULTRA.PROMOTIONAL_PLANS.INCLUDED_PLAN_CYCLES . If > 0, add one months value to stored_value.
 *
 * @param  integer $customer_id
 * @param  integer $ultra_promotional_plans_id
 * @param  string $iccid
 * @return array (success=>bool,errors=>array)
 */
function ultra_promotional_plans_pre_activation( $customer_id , $ultra_promotional_plans_id , $iccid )
{
  $return = array('success'=>FALSE,'errors'=>array());

  $sql = ultra_promotional_plans_select_query(
    array(
      'ultra_promotional_plans_id' => $ultra_promotional_plans_id,
      'select_fields'              => array(
'CASE WHEN ( MUST_ACTIVATE_BEFORE IS NOT NULL AND MUST_ACTIVATE_BEFORE <= GETUTCDATE() )
      THEN 1
      ELSE 0
 END is_expired',
'MUST_ACTIVATE_BEFORE',
'INCLUDED_PLAN_CYCLES',
'TARGET_PLAN'
      )
    )
  );

  $ultra_promotional_plans = mssql_fetch_all_objects(logged_mssql_query($sql));

  dlog('',"%s",$ultra_promotional_plans);

  try
  {
    if ( ! ( $ultra_promotional_plans && is_array( $ultra_promotional_plans ) && count($ultra_promotional_plans) ) )
      throw new Exception("ERR_API_INTERNAL: No data found");

    // If ULTRA.PROMOTIONAL_PLANS.MUST_ACTIVATE_BEFORE < now, block activation
    if ( $ultra_promotional_plans[0]->is_expired )
      throw new Exception("ERR_API_INTERNAL: Promotional Plan expired on ".$ultra_promotional_plans[0]->MUST_ACTIVATE_BEFORE);

    // If ULTRA.PROMOTIONAL_PLANS.INCLUDED_PLAN_CYCLES > 0, add one months value to customer's stored_value.
    if ( $ultra_promotional_plans[0]->INCLUDED_PLAN_CYCLES )
    {
      $plan   = get_plan_from_cos_id( $ultra_promotional_plans[0]->TARGET_PLAN ); # L[12345]9
      $amount = substr($plan,-2); # L[12345]9 => [12345]9

      dlog('',"INCLUDED_PLAN_CYCLES = %s ; TARGET_PLAN = %s ; plan = $plan ; amount = $amount ",
              $ultra_promotional_plans[0]->INCLUDED_PLAN_CYCLES,
              $ultra_promotional_plans[0]->TARGET_PLAN
      );

      # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value += $amount
      $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
        array(
          'balance_to_add' => $amount,
          'customer_id'    => $customer_id
        )
      );

      if ( ! run_sql_and_check_result($htt_customers_overlay_ultra_update_query) )
        throw new Exception("DB error : could not update htt_customers_overlay_ultra");

      // add a row to HTT_BILLING_HISTORY

      $source = 'PROMO';

      $history_params = array(
        "customer_id"            => $customer_id,
        "date"                   => 'now',
        "cos_id"                 => $ultra_promotional_plans[0]->TARGET_PLAN,
        "entry_type"             => 'LOAD',
        "stored_value_change"    => $amount,
        "balance_change"         => 0,
        "package_balance_change" => 0,
        "charge_amount"          => 0,
        "reference"              => luhnenize( $iccid ),
        "reference_source"       => get_reference_source( $source ),
        "detail"                 => 'Added Stored Value Promo Pre-Activation',
        "description"            => 'Promo - Included Plan',
        "result"                 => 'COMPLETE',
        "source"                 => $source,
        "is_commissionable"      => 0
      );

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
        throw new Exception("DB error : could not add a row to HTT_BILLING_HISTORY");
    }

    $return["success"] = TRUE;
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $return['errors'][] = $e->getMessage();
  }

  return $return;
}

/**
 * ultra_promotional_plans_post_activation
 *
 * ULTRA.PROMOTIONAL_PLANS.INCLUDED_PLAN_CYCLES : If > 1, add cycles-1 months value to stored_value
 * ULTRA.PROMOTIONAL_PLANS.ADDITIONAL_MESSAGES  : send all additional messages
 *
 * @param  integer $customer_id
 * @return array (success=>bool,errors=>array)
 */
function ultra_promotional_plans_post_activation( $customer_id )
{
  // customer -> ULTRA.CUSTOMER_OPTIONS -> ULTRA.PROMOTIONAL_PLANS

  $return = array('success'=>FALSE,'errors'=>array());

  $sql = sprintf(
    " SELECT u.CURRENT_ICCID,
             u.CURRENT_ICCID_FULL,
             o.OPTION_VALUE,
             p.*
      FROM   ULTRA.CUSTOMER_OPTIONS o
      JOIN   HTT_CUSTOMERS_OVERLAY_ULTRA u
      ON     o.CUSTOMER_ID      = u.CUSTOMER_ID
      JOIN   ULTRA.PROMOTIONAL_PLANS p
      ON     p.ULTRA_PROMOTIONAL_PLANS_ID = o.OPTION_VALUE
      WHERE  o.OPTION_ATTRIBUTE = 'PROMO_ASSIGNED'
      AND    u.CURRENT_ICCID_FULL BETWEEN p.ICCID_START and p.ICCID_END
      AND    u.CUSTOMER_ID      = %d",
    $customer_id
  );

  $ultra_promotional_plans = mssql_fetch_all_objects(logged_mssql_query($sql));

  dlog('',"%s",$ultra_promotional_plans);

  if ( $ultra_promotional_plans && is_array($ultra_promotional_plans) && count($ultra_promotional_plans) )
  {
    if ( count($ultra_promotional_plans) == 1 )
    {
      // send all additional messages (ignore errors)
      if ( $ultra_promotional_plans[0]->ADDITIONAL_MESSAGES )
        send_promotional_plans_additional_messages( $ultra_promotional_plans[0] , $customer_id );

      // included_plan_cycles . If > 1, add cycles-1 months value to stored_value
      if ( $ultra_promotional_plans[0]->INCLUDED_PLAN_CYCLES > 1 )
      {
        $plan        = get_plan_from_cos_id( $ultra_promotional_plans[0]->TARGET_PLAN ); # L[12345]9
        $plan_amount = substr($plan,-2); # L[12345]9 => [12345]9
        $amount      = ( $ultra_promotional_plans[0]->INCLUDED_PLAN_CYCLES - 1 ) * $plan_amount ;

        dlog('',"INCLUDED_PLAN_CYCLES = %s ; TARGET_PLAN = %s ; plan = $plan ; plan_amount = $plan_amount ; amount = $amount ",
                $ultra_promotional_plans[0]->INCLUDED_PLAN_CYCLES,
                $ultra_promotional_plans[0]->TARGET_PLAN
        );

        # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value += $amount
        $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
          array(
            'balance_to_add' => $amount,
            'customer_id'    => $customer_id
          )
        );

        if ( run_sql_and_check_result($htt_customers_overlay_ultra_update_query) )
        {
          // add a row to HTT_BILLING_HISTORY

          $source = 'PROMO';

          $history_params = array(
            "customer_id"            => $customer_id,
            "date"                   => 'now',
            "cos_id"                 => $ultra_promotional_plans[0]->TARGET_PLAN,
            "entry_type"             => 'LOAD',
            "stored_value_change"    => $amount,
            "balance_change"         => 0,
            "package_balance_change" => 0,
            "charge_amount"          => 0,
            "reference"              => $ultra_promotional_plans[0]->CURRENT_ICCID_FULL,
            "reference_source"       => get_reference_source( $source ),
            "detail"                 => 'Added Stored Value Promo Post-Activation',
            "description"            => 'Promo - Included Plan',
            "result"                 => 'COMPLETE',
            "source"                 => $source,
            "is_commissionable"      => 0
          );

          $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

          if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
          {
            $return["success"] = TRUE;
          }
          else
          { $return['errors'][] = "DB error : could not add a row to HTT_BILLING_HISTORY"; }
        }
        else
        {
          $return['errors'][] = "DB error : could not update htt_customers_overlay_ultra";
        }
      }
      else
      {
        dlog('',"no effect");

        $return['success'] = TRUE;
      }
    }
    else
    {
      $return['errors'][] = "Customer belongs to more that one promotional plan within the same ICCID range.";
    }
  }
  else
  {
    $return['errors'][] = "No data found.";
  }

  return $return;
}

?>
