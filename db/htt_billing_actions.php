<?php

# php component to access DB table htt_billing_actions #

# synopsis

#echo htt_billing_actions_insert_query(
#  array(
#    "customer_id" => '20',
#    "request_id" => 'r',
#    "status" => 'test',
#    "attempt_log" => 'test',
#    "transaction_id" => '1',
#    "product_id" => '1',
#    "subproduct_id" => '1',
#    "next_attempt" => 'dateadd(ss, 120, getutcdate())',
#    "amount" => 1000
#  )
#)."\n\n";

#echo htt_billing_actions_update_query(
#  array(
#    "status_from"    => 'FROM',
#    "status_to"      => 'TO',
#    "customer_id"    => 123,
#    "transaction_id" => 't',
#    "amount"         => 4400
#  )
#)."\n\n";

#echo htt_billing_actions_select_query(
#  array(
#    "status"         => 'OPEN',
#    "customer_id"    => 123,
#    "transaction_id" => 't'
#  )
#)."\n\n";

/**
 * htt_billing_actions_select_query
 * $params: customer_id
 *          transaction_id
 *          status
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_billing_actions_select_query($params)
{
  $declare = array();
  $where   = array();

  if ( empty($params['customer_id']) && empty($params['transaction_id']) && empty($params['status']))
    return NULL;

  if (isset($params['customer_id']))
  {
    $declare[] = sprintf('DECLARE @CUSTOMER_ID BIGINT = %d;', $params['customer_id']);
    $where[]   = 'CUSTOMER_ID = @CUSTOMER_ID';
  }

  if (isset($params['transaction_id']))
  {
    $declare[] = sprintf('DECLARE @TRANSACTION_ID VARCHAR(160) = \'%s\';', $params['transaction_id']);
    $where[]   = 'TRANSACTION_ID = @TRANSACTION_ID';
  }

  if (isset($params['status']))
  {
    $declare[] = sprintf('DECLARE @STATUS VARCHAR(32) = \'%s\';', $params['status']);
    $where[]   = 'STATUS = @STATUS';
  }

  $declare_clause = implode(' ', $declare);
  $where_clause   = ' WHERE '.implode(" AND ", $where);

  $query =
    "$declare_clause
     SELECT *
     FROM htt_billing_actions
     $where_clause";

  return $query;
}

function htt_billing_actions_zombie_transactions_select()
{
  $sql = "SELECT UUID FROM HTT_BILLING_ACTIONS with (nolock)
    WHERE DATEDIFF( mi, CREATED, getutcdate() ) > 30 AND status = 'OPEN'";

  return  mssql_fetch_all_rows(logged_mssql_query($sql));
}

/**
 * htt_billing_actions_update_success
 *
 * Updates HTT_BILLING_ACTIONS after a success
 *
 * @param  string $uuid
 * @return boolean
 */
function htt_billing_actions_update_success( $uuid )
{
  $query = sprintf("
    UPDATE HTT_BILLING_ACTIONS
    SET    STATUS         = 'DONE',
           CLOSED         = GETUTCDATE(),
           LAST_ATTEMPT   = GETUTCDATE(),
           NEXT_ATTEMPT   = NULL,
           ATTEMPT_LOG    = CAST ( ATTEMPT_LOG AS NVARCHAR(MAX) ) + '%s'
    WHERE  UUID           = %s
    ",
    '; success '.getmypid().' '.time(),
    mssql_escape_with_zeroes( $uuid )
  );

  return is_mssql_successful(logged_mssql_query($query));
}

/**
 * htt_billing_actions_update_retry
 *
 * Updates HTT_BILLING_ACTIONS after a failure so that we can retry
 *
 * @param  string $uuid
 * @param  integer $delay
 * @return boolean
 */
function htt_billing_actions_update_retry( $uuid , $delay )
{
  $query = sprintf("
    UPDATE HTT_BILLING_ACTIONS
    SET    STATUS         = 'OPEN',
           ATTEMPT_LOG    = CAST ( ATTEMPT_LOG AS NVARCHAR(MAX) ) + '%s',
           LAST_ATTEMPT   = GETUTCDATE(),
           NEXT_ATTEMPT   = DATEADD(second, %d, '1970-01-01')
    WHERE  UUID           = %s
    ",
    '; failure '.getmypid().' '.time(),
    ( time() + $delay ),
    mssql_escape_with_zeroes( $uuid )
  );

  return is_mssql_successful(logged_mssql_query($query));
}

/**
 * htt_billing_actions_update_rejected
 *
 * Updates HTT_BILLING_ACTIONS after a failure, no more retries
 *
 * @param  string $uuid
 * @return boolean
 */
function htt_billing_actions_update_rejected( $uuid )
{
  $query = sprintf("
    UPDATE HTT_BILLING_ACTIONS
    SET    STATUS         = 'REJECTED',
           LAST_ATTEMPT   = GETUTCDATE(),
           ATTEMPT_LOG    = CAST ( ATTEMPT_LOG AS NVARCHAR(MAX) ) + '%s'
    WHERE  UUID           = %s
    ",
    '; failure '.getmypid().' '.time(),
    mssql_escape_with_zeroes( $uuid )
  );

  return is_mssql_successful(logged_mssql_query($query));
}

/**
 * htt_billing_actions_update_query
 *
 * Generic query to update HTT_BILLING_ACTIONS fields
 * $params: status_to
 *          customer_id
 *          transaction_id
 *          amount
 *          status_from
 *
 * @param  array $params
 * @return string
 */
function htt_billing_actions_update_query($params)
{
  $query = sprintf(
    "UPDATE htt_billing_actions
     SET   status         = %s
     WHERE customer_id    = %d
     AND   transaction_id = %s
     AND   amount         = %d
     AND   status         = %s", 
    mssql_escape($params["status_to"]),
    $params["customer_id"],
    mssql_escape($params["transaction_id"]),
    $params["amount"],
    mssql_escape($params["status_from"])
  );

  return $query;
}

/**
 * htt_billing_actions_insert_query 
 * $params: customer_id
 *          request_id
 *          status
 *          attempt_log
 *          transaction_id
 *          product_id
 *          subproduct_id
 *          next_attempt
 *          amount
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_billing_actions_insert_query($params)
{
  $next_attempt = 'NULL';
  $amount       = 'NULL';

  if ( $params['next_attempt'] )
  {
    $next_attempt = $params['next_attempt'];
  }

  if ( $params['amount'] )
  {
    $amount = $params['amount'];
  }

  $query = sprintf(
"INSERT INTO htt_billing_actions
 (customer_id,
  uuid,
  status,
  attempt_log,
  transaction_id,
  product_id,
  subproduct_id,
  next_attempt,
  amount,
  provider_sku
 )
 VALUES
 (
  %d,
  %s,
  %s,
  '%s',
  '%s',
  '%s',
  '%s',
  %s,
  %s,
  '%s'
  )",
    $params["customer_id"],
    mssql_escape_with_zeroes($params["request_id"]),
    mssql_escape_with_zeroes($params["status"]),
    $params["attempt_log"],
    $params["transaction_id"],
    $params["product_id"],
    $params["subproduct_id"],
    $next_attempt,
    $amount,
    $params['provider_sku']
  );

  return $query;
}

?>
