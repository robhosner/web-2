<?php

/*

php component to access DB table HTT_INVENTORY_PIN

 'AT_MASTER' = Hot
 'AT_FOUNDRY = Cold

*/

/**
 * htt_inventory_pin_update_query 
 * $params: status
 *          customer_used
 *          last_changed_by
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_inventory_pin_update_query($params)
{
  $pin = $params['pin'];

  $pin_encrypted = "'".perform_mcrypt_encryption($pin)."'";

  $where_clause = " pin_crypted = $pin_encrypted";

  $set_clause_array = array( " LAST_CHANGED_DATE = getutcdate() " );

  if ( isset($params['status']) )
  {
    $set_clause_array[] = sprintf(" STATUS = %s ",mssql_escape($params["status"]));
  }

  if ( isset($params['customer_used']) )
  {
    $set_clause_array[] = sprintf(" CUSTOMER_USED = %d ",$params['customer_used']);
  }

  if ( isset($params['last_changed_by']) )
  {
    $set_clause_array[] = sprintf(" LAST_CHANGED_BY = %s ",mssql_escape($params["last_changed_by"]));
  }

  $set_clause = implode(" , ", $set_clause_array);

  $query =
    "UPDATE htt_inventory_pin
     SET    $set_clause
     WHERE  $where_clause";

  return $query;
}

/**
 * htt_inventory_pin_insert_query
 * $params: serial_number
 *          pin
 *          pin_value
 *          customer_used
 *          method_used
 *          inventory_masteragent
 *          inventory_distributor
 *          inventory_dealer
 *          status
 *          last_changed_by
 *          created_by
 *          serial_number_id
 *          expires_date
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_inventory_pin_insert_query($params)
{
  $pin_crypted = perform_mcrypt_encryption($params['pin']);

  $query = sprintf(
"INSERT INTO htt_inventory_pin
 (
  serial_number,
  pin_crypted,
  pin_value,
  customer_used,
  method_used,
  inventory_masteragent,
  inventory_distributor,
  inventory_dealer,
  status,
  last_changed_date,
  last_changed_by,
  created_by_date,
  created_by,
  serial_number_id,
  expires_date
 )
 VALUES
 (
  %s,
  '%s',
  %d,
  %d,
  %s,
  %d,
  %d,
  %d,
  %s,
  getutcdate(),
  %s,
  getutcdate(),
  %s,
  %s,
  %s
 )",
    mssql_escape($params["serial_number"]),
    $pin_crypted,
    $params["pin_value"],
    $params["customer_used"],
    mssql_escape($params["method_used"]),
    $params["inventory_masteragent"],
    $params["inventory_distributor"],
    $params["inventory_dealer"],
    mssql_escape($params["status"]),
    mssql_escape($params["last_changed_by"]),
    mssql_escape($params["created_by"]),
    mssql_escape($params["serial_number_id"]),
    $params["expires_date"]
  );

  return $query;
}

/**
 * htt_inventory_pin_select_query 
 * $params: pin
 * @param  array $params
 * @return string SQL
 */
function htt_inventory_pin_select_query($params)
{
  $pin = $params['pin'];

  $pin_encrypted = "'".perform_mcrypt_encryption($pin)."'";

  $where_clause = " pin_crypted = $pin_encrypted";

  # TODO: join with htt_inventory_pinbatches ?

  $query =
    "SELECT
  '$pin' as pin_decrypted,
  serial_number,
  pin_crypted,
  pin_value,
  customer_used,
  method_used,
  inventory_masteragent,
  inventory_distributor,
  inventory_dealer,
  status,
  last_changed_date,
  last_changed_by,
  created_by_date,
  created_by,
  serial_number_id,
  expires_date
    FROM  htt_inventory_pin
    WHERE $where_clause";

  return $query;
}

/**
 * htt_inventory_pin_set_range_hot 
 * @param  integer $masteragent
 * @param  string $startPinSerialNumber
 * @param  string $endPinSerialNumber
 * @param  string $user
 * @return boolean success/fail
 */
function htt_inventory_pin_set_range_hot($masteragent, $startPinSerialNumber, $endPinSerialNumber, $user)
{
  $stmt = mssql_init('[DBO].[ACTIVATE_SCRATCHCARDS]');

  mssql_bind($stmt, '@MasterCd', $masteragent, SQLVARCHAR, false, false, 8);
  mssql_bind($stmt, '@RangeStart', $startPinSerialNumber, SQLVARCHAR, false, false, 12);
  mssql_bind($stmt, '@RangeEnd', $endPinSerialNumber, SQLVARCHAR, false, false, 12);
  mssql_bind($stmt, '@LastChangedBy', $user, SQLVARCHAR, false, false, 25);
      
  $result = mssql_execute($stmt);
  mssql_free_statement($stmt);

  return $result;
}

/**
 * htt_inventory_pin_get_pin_range_status 
 * @param  string $startPinSerialNumber
 * @param  string $endPinSerialNumber
 * @return object[]
 */
function htt_inventory_pin_get_pin_range_status($startPinSerialNumber, $endPinSerialNumber)
{
  if (! $startPinSerialNumber || ! $endPinSerialNumber)
    return FALSE;

  $query = sprintf("SELECT COUNT (*) AS count, MAX(serial_number) as max_serial, status
    FROM HTT_INVENTORY_PIN
    WHERE serial_number >= '%s'
    AND serial_number <= '%s'
    GROUP BY status",
    $startPinSerialNumber,
    $endPinSerialNumber);

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

?>
