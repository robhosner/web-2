<?php
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="utf-8">
    <script language="javascript">
      // redirect to DIG2
      var current_url = document.URL;
      var new_url = current_url.replace("dig.php", "dig2.html"); 
      window.location = new_url;
    </script>
  </head>
</html>