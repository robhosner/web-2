FROM centos:latest

ENV HOME=/home/ht/www/api

# Adding the configuration file of the nginx
ADD DockerDependencies/nginx.conf /etc/nginx/nginx.conf

RUN mkdir /etc/nginx/sites-available && mkdir /etc/nginx/sites-enabled

# Add api configuration dependencies
ADD DockerDependencies/api.ultra.me.conf /etc/nginx/sites-available/api.ultra.me.conf
ADD DockerDependencies/api.ultra.me.conf /etc/nginx/sites-enabled/api.ultra.me.conf
ADD DockerDependencies/supervisord.conf /etc/supervisord.conf

WORKDIR $HOME/web

# Install PHP
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
    rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
    yum update -y && yum install -y git && yum -y install nginx && \
    curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/mssql-release.repo && \
    ACCEPT_EULA=Y yum install -y msodbcsql && yum remove -y unixODBC && \
    yum install -y unixODBC-utf16-devel && \
    yum install -y gcc openssl-devel php71-php php71-php-fpm php71-php-pecl-redis php71-php-opcache \
    php71-php-mcrypt php71-php-pecl-mongodb php71-php-xml php71-php-sqlsrv php71-php-zip php71-php-soap && \
    mv /usr/bin/php71 /usr/bin/php && mv /opt/remi/php71/root/usr/sbin/php-fpm /usr/sbin/php-fpm && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    mkdir /var/log/htt && chown apache:apache /var/log/htt && \
    chown apache:apache /var/log/httpd && localedef -i en_US -f UTF-8 en_US.UTF-8

# Installing supervisor, logrotate, and cron
RUN yum install -y supervisor && yum install -y logrotate && yum install -y cronie

# Add php.ini
ADD DockerDependencies/php.ini /etc/opt/remi/php71/php.ini
ADD DockerDependencies/php-fpm.conf /etc/opt/remi/php71/php-fpm.conf

ADD DockerDependencies/logrotate.conf /etc/logrotate.conf
ADD DockerDependencies/htt /etc/logrotate.d/htt

RUN chmod -R 644 /etc/logrotate.d && chmod -R 644 /etc/logrotate.conf

COPY . $HOME/web

RUN composer update && \
    cp -a -r vendor/apache/log4php/src/main/php/. $HOME/web/log4php && \
    cp -a -r vendor/phpseclib/phpseclib/phpseclib/Crypt $HOME/web/Crypt && \
    cp -a -r vendor/pear/validate/Validate.php $HOME/web/ && \
    cp -a -r vendor/pear/validate_finance_creditcard/Validate $HOME/web/Validate && \
    mkdir -p /opt/php/vendor/ && cp -a -r vendor/predis/predis/ /opt/php/vendor/ && \
    chmod +x dockerRunPhpunit.sh && chmod +x DockerRunApi.sh

RUN yum -y install php71-php-pecl-xdebug

CMD ["supervisord", "-n"]