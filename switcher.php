<?php
include 'lib/util-common.php';
include_once('session.php');

function bifurcate($yesno, $params, $text)
{
  $matches = array();
  if (preg_match("/^=if,([^=]+)=(.+)/", $text, $matches))
  {
    $choice = $yesno && array_key_exists("bifurcate", $params) && $params["bifurcate"] === $matches[1];
    return array(
      'bif' => $choice,
      'desc' => $choice ? "--bifurcated on " . $params['bifurcate'] . " === $matches[1]" : "--bifurcated on " . $params['bifurcate'] . " != $matches[1]",
      'matches' => $matches
      );
  }

  return FALSE;
}

function collect_target($root, $cosid, $minutes)
{
  if ($minutes < 0)
  {
    $print_minutes = '';
  }
  else
  {
    $print_minutes = " $minutes";
  }

  return array('id' => $cosid, 'root' => $root, 'minutes' => $minutes, 'print' => $print_minutes);
}

function find_targets($id)
{
  global $transition_table;

  $targets = array();

  foreach ($transition_table as $plan => $data)
  {
    if (NULL == $id || array_key_exists("$id",$data['cos_ids']))
    {
      foreach ($data as $target => $target_data)
      {
        if ($target != "cos_ids")
        {
          if (array_key_exists($target, $transition_table))
          {
            foreach ($transition_table[$target]['cos_ids'] as $cosid => $minutes)
            {
              if (NULL == $id || $cosid != $id)
              {
                $collected = collect_target($target, $cosid, $minutes);
                $targets[$target . $collected['print']] = $collected;
              }
            }
          }
        }
      }
    }
  }

  return $targets;
}

function find_target_prop($targets, $id, $prop)
{
  foreach ($targets as $name => $item)
  {
    if ($item['id'] == $id)
    {
      return $item[$prop];
    }
  }

  return NULL;
}

function transition($params)
{
  global $transition_blocks;
  global $transition_table;
  global $out;
  global $plans;

  $source_plan = NULL;
  $dest_plan = NULL;

  //var_dump($params);

  $upgrade_seconds_gain = 0;
  $balance = 0;

  $customer_locator = $params[array_key_exists("customer", $params) ? 'customer' : 'customer_id'];
  $customer = find_customer( make_find_customer_query($customer_locator));
  if (!$customer)
  {
    $customer = find_customer( make_find_new_signup_status_customer_query($customer_locator));
    if (!$customer)
    {
      return array("--INVALID CUSTOMER $customer_locator");
    }
  }

  $all_validated = TRUE;

  $fields = NULL;

  /* this is a temporary cos_id */
  if ($customer->COS_ID == 94916)
  {
    if (array_key_exists("fields", $params))
    {
      $fields = $params['fields'];
      /* dlog("", "Got fields " . json_encode($fields)); */
      if(is_array($fields))
      {
        $fields['pin_number'] = array_key_exists("pin_number", $_REQUEST) ? $_REQUEST['pin_number'] : -1;
        $missing_fields = array();

        $required_fields = array(
          'account_cc_number', 'account_cc_exp', 'account_cc_cvv',
          'account_address1', 'account_city',
          'account_state_or_region', 'account_postal_code',
          'account_number_phone');

        if ($params['dest_id'] == 100729)
        {
          $required_fields = array();
        }

        foreach ($required_fields as $required_field)
        {
          if (!array_key_exists($required_field, $fields))
          {
            $missing_fields[] = $required_field;
          }
        }

        if (count($missing_fields) < 1)
        {
          $fcheck = fields($fields, $customer, TRUE); // just verify the creation fields

          $all_validated = FALSE;
          if (is_array($fcheck))
          {
            foreach ($fcheck as $checkpoint)
            {
              if (is_array($checkpoint) &&
                  array_key_exists('all_validated', $checkpoint))
              {
                $all_validated = $checkpoint['all_validated'];
              }
            }

            if (!$all_validated)
            {
              $out['customer'] = NULL;
              $out['signup_error'] = array('failed_validation' => $fcheck);
            }
            else
            {
              $out['fields'] = fields($fields, $customer, FALSE);
              is_mssql_successful(logged_mssql_query(sprintf("
UPDATE customers
SET user_8 = 4
WHERE user_8 = 0 AND customer = '%s'",
                                                   $customer->CUSTOMER)));
            }
          }
        }
        else
        {
          $out['customer'] = NULL;
          $out['signup_error'] = array('missing_fields' => $missing_fields);
        }
      }
      else
      {
        $out['customer'] = NULL;
        $out['signup_error'] = 'Sorry, no fields were provided as an assoc.array';
      }
    }

    if ($fields != NULL && array_key_exists('signup_error', $out))
    {
      return array("--SIGNUP ERROR: " . $out['signup_error']);
    }
    else if (array_key_exists('signup_error', $out))
    {
      return array("--MISC SIGNUP ERROR: " . $out['signup_error']);
    }

    // if we get here, the provided fields were OK and the edit succeeded
    sleep(3); // give the IPCOMMAND a chance to catch up, TODO: see if we can go lower
    $customer_locator = $customer->CUSTOMER;

    $out['signup_customer'] = find_customer(make_find_status_customer_query($customer_locator));

    $customer = find_customer( make_find_new_signup_customer_query($customer_locator));
    if (!$customer)
    {
      return array("--INVALID CUSTOMER post-fields application $customer_locator");
    }

    $params['bifurcate'] = "Immediate";
  }

  $params['customer_record'] = $customer; // this may be passed to charge()
  $params['amount'] = 0;                  // start at 0

  $balance = $customer->BALANCE;
  $seconds = $customer->PACKAGED_BALANCE1;
  $source_seconds = 0;
  $dest_seconds = 0;

  if ($params['source_id'] == $params['dest_id'])
  {
    return array("--INVALID TRANSITION TO ITSELF");
  }

  // find the plan
  foreach ($transition_table as $plan => $data)
  {
    if (array_key_exists($params['source_id'],$data['cos_ids']))
    {
      $source_plan = $plan;
      foreach ($data as $target => $target_data)
      {
        if ($target != "cos_ids")
        {
          if (array_key_exists($target, $transition_table))
          {
            foreach ($transition_table[$target]['cos_ids'] as $cosid => $minutes)
            {
              $source_seconds = $data['cos_ids'][$params['source_id']] * 60;
              if ($source_seconds < 0)
              {
                $source_seconds = 0;
              }
            }
          }
          else
          {
            return array("--INVALID TRANSITION TABLE DESTINATION $target");
          }
        }
      }
    }

    if (array_key_exists($params['dest_id'],$data['cos_ids']))
    {
      $dest_plan = $plan;
      $dest_seconds = $data['cos_ids'][$params['dest_id']] * 60;
      if ($dest_seconds < 0)
      {
        $dest_seconds = 0;
      }
    }
  }

  if (!$source_plan)
  {
    return array("--INVALID_SOURCE_PLAN--");
  }

  $site = 'IndiaLD'; // TODO: get from the source_plan
  if (!preg_match("/IndiaLD/i", $site))
  {
    return array("--WRONGSITE--");
  }

  if (!$dest_plan)
  {
    return array("--INVALID_TARGET_PLAN--");
  }

  $out['source_plan_name'] = "$source_plan " . $source_seconds/60;
  $out['dest_plan_name'] = "$dest_plan " . $dest_seconds/60;

  $transition = $transition_table[$source_plan];
  if (!array_key_exists($dest_plan, $transition))
  {
    return array("--INVALID_TRANSITION--");
  }

  $study = study_transition($params['source_id'], $params['dest_id'],
                            $plans,
                            $customer->BALANCE,
                            $customer->PACKAGED_BALANCE1,
                            property_exists($customer, 'cos_id') ? $customer->cos_id : $customer->COS_ID);
  if ($study['error'])
  {
    return $study['result'];
  }

  $upgrade_cost = $study['upgrade_cost'];
  $upgrade_seconds_cost = $study['upgrade_seconds_cost'];
  $upgrade_seconds_gain = $study['upgrade_seconds_gain'];
  $source_monthly = $study['source_monthly'];
  $dest_monthly = $study['dest_monthly'];

  $todo = array();

  $todo[] = sprintf("--INFO: customer %s has balance %s and packaged_balance %s", $customer->CUSTOMER_ID, $customer->BALANCE, $customer->PACKAGED_BALANCE1);

  if ($fields == NULL && $params['source_id'] != $customer->cos_id)
  {
    return array(sprintf("--ERROR: customer %s has plan cos_id %s but you specified source_id %s", $customer->CUSTOMER_ID, $customer->cos_id, $params['source_id']));
  }

  $out['targets'] = find_targets($params['source_id']);

  $transition = $transition[$dest_plan];

  //dlog("", "Raw transition = " . json_encode($transition));

  $bifurcate = array_key_exists("bifurcate", $transition) ? $transition["bifurcate"] : NULL;

  if (array_key_exists("blocks", $transition))
  {
    $b = $transition["blocks"];

    foreach (is_string($b) ? array($b) : $b as $block_name)
    {
      if (array_key_exists($block_name, $transition_blocks))
      {
        $transition = array_merge($transition, $transition_blocks[$block_name]);
      }
      else
      {
        $todo[] = "--BAD TRANSITION $block_name";
      }
    }
  }

  foreach ($transition as $k => $v)
  {
    switch ($k)
    {
    case "packaged_balance1":
    case "balance":
    case "credit_limit":
    case "billing_type":
    case "activation_date_time":
      $bif = bifurcate($bifurcate, $params, $v);
    //$todo[] = array('zifurcate' => array(bifurcate($bifurcate, $params, $v), $bifurcate, $params['bifurcate'], $v));

// TODO: get rid of the whole bifurcation structure
    if ($bif)
    {
      $todo[] = $bif['desc'];
      $todo[] = $bif['bif']  ? array("set" => array($k => $bif['matches'][2])) : "--DO NOT set $k to " . $bif['matches'][2];
    }
    else if ($params['bifurcate'] === "Immediate")
    {
      if ($k === "packaged_balance1" && $v === "=delta_minutes")
      {
        $v = $upgrade_seconds_gain;

        if ($dest_monthly && $source_plan === $dest_plan && $v < 0)
        {
          $v = 0;
        }

        $todo[] = "--ADDING DELTA MINUTES $v seconds";
        $v = "packaged_balance1 + $v";
      }

      if ($k === "packaged_balance1" && $v === "=dest_minutes")
      {
        $v = $dest_seconds;

        if ($v < 0)
        {
          $v = 0;
        }

        $todo[] = "--SETTING DEST MINUTES $v seconds";
      }

      $todo[] = array("set" => array($k => $v));
    }
    break;
    case "target":
      $todo[] = array("set" => array("cos_id" => $params["dest_id"]));
      break;
    case "bifurcate":
      $todo[] = "--(noted bifurcation elsewhere)";
      break;
    case "blocks":
      $todo[] = "-- inserted blocks " . implode(', ', is_string($v) ? array($v) : $v);
      break;
    case "block":
      $bif = bifurcate($bifurcate, $params, $v);
      if ($bif)
      {
        $todo[] = $bif['desc'];
        $todo[] = $bif['bif']  ? array("block" => $bif['matches'][2]) : "--DO NOT block $k on " . $bif['matches'][2];
      }
      else
      {
        $todo[] = array("block" => $v);
      }
      break;
    default:
      $todo[] =  "--INVALID_TRANSITION_KEY_$k--";
      break;
    }
  }

  foreach ($todo as $rewrite)
  {
    $matches = array();
    if (is_array($rewrite) && array_key_exists('set', $rewrite))
    {
      foreach ($rewrite['set'] as $k => $v)
      {
        if (preg_match("/^=(.+)/", $v, $matches))
        {
          switch ("$k $matches[1]")
          {
          default:
            return array("--INVALID set REWRITE $k = $v");
            break;
          }
        }
      }
    }

    if (is_array($rewrite) && array_key_exists('block', $rewrite))
    {
      $charge = NULL;
      $v = $rewrite['block'];
      if (preg_match("/^=(.+)/", $v, $matches))
      {
          switch ("block $matches[1]")
          {
          case "block charge_remainder":
            $todo = array_merge($todo, $study['result']);

            if ($upgrade_cost < 0 && $params['confirm_loss'] != 1)
            {
              return array("--LOSS OF $upgrade_cost MUST BE CONFIRMED");
            }
            else
            {
              $todo[] = "--INFO: upgrade cost = $upgrade_cost was confirmed with confirm_loss=1 or was not a loss";
            }

// 1. do the balance

            // we will set the balance to 0 so use as much as possible here
            if ($dest_monthly)
            {
              if ($upgrade_cost > 0)
              {
                $todo[] = "--INFO: charging the upgrade cost = $upgrade_cost";
                $params['amount'] = $upgrade_cost;
                // if we're going from PAYG or signup to monthly, show
                // just the absolute gain in BILLING
                if ($dest_monthly &&
                    ($params['source_id'] == 69143 ||
                     $params['source_id'] == 69146))
                {
                  $todo[] = "--INFO: faking upgrade seconds = $dest_seconds";
                  $params['upgrade_seconds_gain'] = $dest_seconds;
                }
                else if ($dest_monthly && $params['source_id'] == 94916)
                {
                  $todo[] = "--INFO: faking upgrade seconds = $dest_seconds";
                  $params['upgrade_seconds_gain'] = $dest_seconds;
                }
                else
                {
                  $todo[] = "--INFO: upgrade seconds = $upgrade_seconds_gain";
                  $params['upgrade_seconds_gain'] = $upgrade_seconds_gain;
                }

                $params['adjust_balance'] = TRUE;
                $charge = charge($params);
// check for a returned error
                foreach ($charge as $charge_entry)
                {
                  if (is_array($charge_entry) && array_key_exists('error', $charge_entry))
                  {
                    if ($fields != NULL)            /* this is a signup charge */
                    {
                      $out['u8'] = revert_cc_fields_and_dec_u8($customer, $fields);
                    }
                    else
                    {
                      $out['u8'] = FALSE;
                    }

                    return array("--CHARGE ERROR: " . $charge_entry['error']);
                  }

                  if (is_array($charge_entry) && array_key_exists('charge_check_error', $charge_entry))
                  {
                    if ($fields != NULL)            /* this is a signup charge */
                    {
                      $out['u8'] = revert_cc_fields_and_dec_u8($customer, $fields);
                    }
                    else
                    {
                      $out['u8'] = FALSE;
                    }

                    return array("--CHARGE CHECK ERROR: " . $charge_entry['charge_check_error']);
                  }
                }

                $todo = array_merge($todo, $charge);
              }
              else
              {
                $todo[] = "--INFO: loss cost = $upgrade_cost taken";
              }
            }
            else
            {
              return array("--UNHANDLED CASE: charge remainder of balance on transition to a non-monthly plan");
            }

// 2. do the seconds calculation
            if ($dest_monthly)
            {
              if (!$source_monthly)
              {
                $todo[] = "--INFO: from non-monthly to monthly, the minutes are adjusted outside charge_remainder";
              }
              else
              {
                if ($upgrade_seconds_gain > 0)
                {
                  $todo[] = "--INFO: incrementing packaged_balance1 by the upgrade seconds = $upgrade_seconds_gain";
                  $todo[] = array("set" => array("packaged_balance1" => "packaged_balance1 + $upgrade_seconds_gain"));
                }
                else
                {
                  $todo[] = "--INFO: downgrading to a monthly plan, keeping current packaged_balance1";
                }
              }
            }
            else
            {
              return array("--UNHANDLED CASE: calculate packaged_balance on transition to a non-monthly plan");
            }

            break;
          default:
            return array("--INVALID block REWRITE $k = $v");
            break;
          }
      }
    }
    //print_r ($rewrite);
    //print ("\n");
  }

// 3. if zero amount, insert note in billing
  if (array_key_exists('charge_description', $params) &&
      0 == $params['amount'])
  {
    $todo[] = array('zero_bill' => array($customer,
                                         $params['charge_description']));
  }

  log_acquisition($customer->ACCOUNT);

  is_mssql_successful(logged_mssql_query(sprintf("
UPDATE accounts
SET credit_limit = 0
WHERE customer_id = %s",
                                                 $customer->CUSTOMER_ID)));
  $todo[] = array('transition_clamputed' => 'yay');

  return $todo;
}

function revert_cc_fields_and_dec_u8 ($customer, $fields)
{
  /* extra caution */
  if (NULL == $fields) return;

  //dlog("", "Saw CC number " . $customer->CC_NUMBER);
  $q = sprintf("UPDATE accounts SET cos_id = 94916
WHERE account_id = '%s';

UPDATE customers SET ADDRESS1 = '', ADDRESS2 = '',
 city = '', state_region = '', postal_code = '',
 local_phone = '', fax = '', date_of_birth = '',
 ccv = '', cc_number = '', cc_exp_date = '', cc_name = '',
 cc_address1 = '', cc_address2 = '', cc_city = '', cc_country = '',
 cc_state_region = '', cc_postal_code = ''
WHERE customer_id = '%s';

UPDATE customers SET user_8 = user_8 - 1
WHERE user_8 > 1 AND customer_id = '%s'",
               $customer->ACCOUNT_ID,
               $customer->CUSTOMER_ID,
               $customer->CUSTOMER_ID);

  return is_mssql_successful(logged_mssql_query($q));
}

function study_transition($source, $dest, $plans,
                          $customer_balance, $customer_packaged_balance,
                          $customer_cos_id )
{
  global $conversion_factor;

  $ret = array('error' => FALSE, 'result' => array());
  $balance = $customer_balance;
  $seconds = $customer_packaged_balance;
  $source_cost = 0;

  $targets = find_targets(NULL);

  $source_minutes = find_target_prop($targets, $source, 'minutes');

  if (NULL == $source_minutes)
  {
    return array ('error' => TRUE, result => array("--INVALID SOURCE PLAN $source"));
  }

  if ($source_minutes < 0)
  {
    $source_minutes = 0;
  }

  $source_seconds = $source_minutes *60;

  $dest_minutes = find_target_prop($targets, $dest, 'minutes');

  if (NULL == $dest_minutes)
  {
    return array ('error' => TRUE, result => array("--INVALID DEST PLAN $dest"));
  }

  if ($dest_minutes < 0)
  {
    $dest_minutes = 0;
  }

  $dest_seconds = $dest_minutes *60;

  $tax_rate = tax_rate($customer_cos_id, $dest);
  $ret['tax_rate'] = $tax_rate;

  // exclude the two PAYG and the one signup plans explicitly, they
  // have no cost in the plans table
  if ($source != 69143 && $source != 69146 && $source != 94916 && $dest != 100729)
  {
    $source_cost = $plans[$source]->plan_cost ? $plans[$source]->plan_cost : 0;
  }

  $dest_cost = 0;
  // exclude the two PAYG and the one signup plans explicitly, they
  // have no cost in the plans table
  if ($dest != 69143 && $dest != 69146 && $dest != 94916 && $dest != 100729)
  {
    $dest_cost = $plans[$dest]->plan_cost ? $plans[$dest]->plan_cost : 0;
  }

  $source_monthly = $source_seconds != 0;
  $dest_monthly = $dest_seconds != 0;

  if ($source_monthly && $dest_monthly)
  {
    $upgrade_cost_pre_tax = $dest_cost - $source_cost;
    $upgrade_tax = floatval(sprintf("%.2f", $tax_rate * $upgrade_cost_pre_tax));
    $upgrade_cost_post_tax = $upgrade_cost_pre_tax + $upgrade_tax;
    $upgrade_cost = $upgrade_cost_post_tax;

    $upgrade_seconds_gain =  $dest_seconds - $source_seconds;
    $upgrade_seconds_cost =  -1 * $upgrade_seconds_gain;
    $ret['result'][] = "--INFO: source plan cost is $source_cost, destination plan cost is $dest_cost";
    $ret['result'][] = "--INFO: upgrade cost pre-tax is $upgrade_cost_pre_tax, tax on that is $upgrade_tax, upgrade cost is $upgrade_cost";
    $ret['result'][] = "--INFO: source plan seconds = $source_seconds ($source_minutes minutes), destination plan seconds = $dest_seconds ($dest_minutes minutes), seconds upgrade cost (destination seconds - source seconds) is $upgrade_seconds_cost";
  }
  else
  {
    $upgrade_cost_pre_tax = $dest_cost;
    $upgrade_tax = floatval(sprintf("%.2f", $tax_rate * $upgrade_cost_pre_tax));
    $upgrade_cost_post_tax = $upgrade_cost_pre_tax + $upgrade_tax;
    $upgrade_cost = $upgrade_cost_post_tax - $balance;

    $upgrade_seconds_gain =  $dest_seconds - $seconds;
    $upgrade_seconds_cost =  -1 * $upgrade_seconds_gain;
    $ret['result'][] = "--INFO: destination plan cost is $dest_cost, balance is $balance";
    $ret['result'][] = "--INFO: upgrade cost pre-tax is $upgrade_cost_pre_tax (the destination cost), tax on that is $upgrade_tax, upgrade cost is $upgrade_cost after applying $balance to $upgrade_cost_post_tax";
    $ret['result'][] = "--INFO: source plan seconds = $source_seconds ($source_minutes minutes), destination plan seconds = $dest_seconds ($dest_minutes minutes), seconds upgrade cost (destination seconds - current) is $upgrade_seconds_cost";
  }

  $ret['target_only_transition'] = FALSE;

  // this transition flag goes both ways
  if ( ($source+$dest == 29955+53788) ||
       ($source+$dest == 30456+53789) )
  {
    $ret['target_only_transition'] = TRUE;
  }

  $ret['source'] = $source;
  $ret['dest'] = $dest;

  $ret['result'][] = "--INFO: source plan is " . ($source_monthly ? "monthly" : "pay-as-you-go");
  $ret['result'][] = "--INFO: destination plan is " . ($dest_monthly ? "monthly" : "pay-as-you-go");

  $ret['conversion_factor'] = $conversion_factor;
  $ret['upgrade_cost_pre_tax'] =  sprintf("%.2f", $upgrade_cost_pre_tax);
  $ret['upgrade_cost_post_tax'] =  sprintf("%.2f", $upgrade_cost_post_tax);
  $ret['upgrade_cost'] = sprintf("%.2f", $upgrade_cost);
  $ret['upgrade_abs'] = sprintf("%.2f", abs($upgrade_cost));
  $ret['upgrade_loss'] = ($upgrade_cost > 0);

  $ret['upgrade_seconds_gain'] = $upgrade_seconds_gain;
  $ret['upgrade_seconds_cost'] = $upgrade_seconds_cost;
  $ret['upgrade_seconds_abs'] = abs($upgrade_seconds_cost);
  $ret['upgrade_seconds_loss'] = ($upgrade_seconds_cost > 0);
  $ret['upgrade_minutes_gain'] = sprintf("%d", $upgrade_seconds_gain / 60);
  $ret['upgrade_minutes_cost'] = sprintf("%d", $upgrade_seconds_cost / 60);
  $ret['upgrade_minutes_abs'] = sprintf("%d", abs($upgrade_seconds_cost) / 60);

  $ret['dest_monthly'] = $dest_monthly;
  $ret['source_monthly'] = $source_monthly;

  $ret['dest_cost'] = sprintf("%.2f", $dest_cost);
  $ret['source_cost'] = sprintf("%.2f", $source_cost);

  $ret['upgrade_tax'] =  sprintf("%.2f", $upgrade_tax);

  $ret['dest_seconds'] = $dest_seconds;
  $ret['dest_minutes'] = sprintf("%d", $dest_minutes);
  $ret['source_seconds'] = $source_seconds;
  $ret['source_minutes'] = sprintf("%d", $source_minutes);

  $ret['minutes'] = sprintf("%d", $seconds / 60);
  $ret['balance'] = sprintf("%.2f", $balance);
  $ret['seconds'] = $seconds;

  return $ret;
}

function study_plans($cosids)
{
  $targets = find_targets(NULL);
  $ret = array();

  foreach ($cosids as $cosid)
  {
    $minutes = find_target_prop($targets, $cosid, 'minutes');

    if (NULL == $minutes)
    {
      return array ('error' => TRUE, result => array("--INVALID PLAN cosid"));
    }

    if ($minutes < 0)
    {
      $minutes = 0;
    }

    $ret[$cosid]['data']['minutes'] = $minutes;
    $ret[$cosid]['data']['tax_rate'] = tax_rate($cosid, $cosid);

    $ret[$cosid]['plan_name'] = NULL;

    foreach ($targets as $name => $collected)
    {
      if ($collected['id'] == $cosid)
      {
        $ret[$cosid]['plan_name'] = $name;
      }
    }

    if (NULL == $ret[$cosid]['plan_name'])
    {
      return array ('error' => TRUE, result => array("--INVALID PLAN cosid, couldn't get the name"));
    }

    $ret[$cosid]['flags']['monthly'] = ($minutes > 0);
    $ret[$cosid]['flags']['balance'] = in_array($cosid, array(29955, 30456, 53788, 53789));
    $ret[$cosid]['flags']['payg'] = in_array($cosid, array(69146, 69143));
    $ret[$cosid]['flags']['qcall'] = in_array($cosid, array(53788, 53789));
    $ret[$cosid]['flags']['ccard'] = in_array($cosid, array(30456, 29955));
    $ret[$cosid]['flags']['usa'] = in_array($cosid, array(49361, 49362, 49363, 49360, 88261));
    $ret[$cosid]['flags']['international'] = in_array($cosid, array(12069, 12070, 12071, 16727, 16728, 25720, 25786, 27373, 27374, 27375, 27376, 27377, 27378, 29133));
    $ret[$cosid]['flags']['uvcard'] = in_array($cosid, array(100729));
    $ret[$cosid]['flags']['taxable'] = ($ret[$cosid]['data']['tax_rate'] > 0);

    $ret[$cosid]['flags']['rechargeable'] = ($ret[$cosid]['flags']['ccard'] || $ret[$cosid]['flags']['qcall'] || $ret[$cosid]['flags']['uvcard']);
    $ret[$cosid]['flags']['topup'] = ! $ret[$cosid]['flags']['rechargeable'];

    $ret[$cosid]['fields']['account_balance'] = $ret[$cosid]['flags']['balance'] ? 'BALANCE' : 'PACKAGED_BALANCE1_OVER_60';

    $ret[$cosid]['fields']['account_value'] = ($ret[$cosid]['flags']['monthly'] || $ret[$cosid]['flags']['payg']) ? 'minutes' : 'BALANCE';

    $ret[$cosid]['fields']['next_event'] = NULL;

// TODO: the rest...
    if ($ret[$cosid]['flags']['ccard'])
      $ret[$cosid]['fields']['next_event'] = 'NEXT_RECHARGE_90_epoch';

    if ($ret[$cosid]['flags']['monthly'])
      $ret[$cosid]['fields']['next_event'] = 'HEY_ITS_A_MONTH_LATER_epoch';
  }

  return $ret;
}

function tax_rate($cos_id, $fallback = -1)
{
  // for signup customers, explore the alternate cos_id
  if ($cos_id == 94916 || $cos_id == 100729) $cos_id = $fallback;

  $taxable_q = sprintf('SELECT a.tax_rate FROM htt_tax_rates a, htt_tax_cos_mapping b where substring(a.country_id, 0, 3) = substring(b.country_id, 0, 3) and taxable_cos = %s', $cos_id);
  $tax_rates = mssql_fetch_all_objects(logged_mssql_query($taxable_q));
  $tax_rate = 0;
  if (is_array($tax_rates) && count($tax_rates) > 0)
  {
    $tax_rate = $tax_rates[0]->tax_rate;
  }

  return $tax_rate;
}

function study_chargeup($balance, $seconds, $cos_id)
{
  $topup_nonusa_cost = 0.0135;
  $topup_usa_cost    = 0.01;

  $ret = array('error' => FALSE, 'result' => array());

  $source = $cos_id;

  $targets = find_targets(NULL);

  $tax_rate = tax_rate($cos_id);
  $ret['tax_rate'] = $tax_rate;

  $type = "";
  switch ($source)
  {
    // 1.5 and QC
  case 29955:
  case 30456:
  case 53788:
  case 53789:
    $type = 'Recharge';
    $ret['amounts'] = array(5, 10, 15, 20, 25);
    $ret['chargeups'] = array();
    foreach ($ret['amounts'] as $a)
    {
      $cost = sprintf("%.2f", $a);
      $tax = sprintf("%.2f", $tax_rate * $a);
      $total = sprintf("%.2f", $a + $tax);
      $m = $cost / 0.0135; // we may need to make this variable per plan
      $ret['chargeups'][] = array('cost' => $cost,
                                  'tax'  => $tax,
                                  'total' => $total,
                                  'minutes' => $m,
                                  'text' => "$" . $cost);
    }
    break;
    // PAYG
  case 69143:
  case 69146:
    $type = 'TopUp';
    $ret['minutes'] = array(250, 500, 1000, 1500, 2000, 2500);
    $ret['chargeups'] = array();
    foreach ($ret['minutes'] as $m)
    {
      $a = 0.0135 * $m; // we may need to make this variable per plan
      $cost = sprintf("%.2f", $a);
      $tax = sprintf("%.2f", $tax_rate * $a);
      $total = sprintf("%.2f", $a + $tax);
      $ret['chargeups'][] = array('cost' => $cost,
                                  'tax'  => $tax,
                                  'total' => $total,
                                  'minutes' => $m,
                                  'text' => "$m minutes: $" . $cost);
    }
    break;

  // UV card
  case 100729:
    $type = 'Recharge';
    $ret['amounts'] = array(3, 5, 10, 15, 20, 25);
    $minute_cost = $topup_usa_cost;
    foreach ($ret['amounts'] as $a)
    {
      $cost = sprintf("%.2f", $a);
      $tax = sprintf("%.2f", $tax_rate * $a);
      $total = sprintf("%.2f", $a + $tax);
      $m = $cost / 0.0135; // we may need to make this variable per plan
      $ret['chargeups'][] = array('cost' => $cost,
                                  'tax'  => $tax,
                                  'total' => $total,
                                  'minutes' => $m,
                                  'text' => "$" . $cost);
    }
    break;

    // International
  case 12069:
  case 12070:
  case 12071:
  case 16727:
  case 16728:
  case 25720:
  case 25786:
    $type = 'TopUp';
    $minute_cost = $topup_nonusa_cost;
    break;
    // International T:
  case 27373:
  case 27374:
  case 27375:
  case 27376:
  case 27377:
  case 27378:
  case 29133:
    // USA:
  case 49361:
  case 49362:
  case 49363:
  case 49360:
  case 88261:
    $type = 'TopUp';
    $minute_cost = $topup_usa_cost;
    break;
  default:
    $type = 'unknown!';
    return array('error' => TRUE, 'result' => array());
    break;
  }

  if ($type === 'TopUp')
  {
    $ret['minutes'] = array(100, 200, 300, 400, 500, 600, 700, 800, 900, 1000);
    $ret['chargeups'] = array();
    foreach ($ret['minutes'] as $m)
    {
      $a = $m * $minute_cost;
      /* surcharge for less than 500 minutes */
      if ($m < 500) $a += 0.00;

      $cost = sprintf("%.2f", $a);
      $tax = sprintf("%.2f", $tax_rate * $a);
      $total = sprintf("%.2f", $a + $tax);
      $ret['chargeups'][] = array('cost' => $cost,
                                  'tax'  => $tax,
                                  'minutes' => $m,
                                  'total' => $total,
                                  'text' => "$m minutes");
    }
  }

  $ret['type'] = $type;

  return $ret;
}

function chargeup($params, $fallback=-1)
{
  global $out;

  $todo2 = array();

  if (!array_key_exists('session', $params))
  {
    return array("--NO SESSION ID GIVEN TO chargeup()");
  }

  if (!array_key_exists('chargeup', $params))
  {
    return array("--NO AMOUNT GIVEN TO chargeup()");
  }

  if (!array_key_exists('customer_record', $params))
  {
    return array("--NO CUSTOMER RECORD GIVEN TO chargeup()");
  }

  if (!array_key_exists('charge_description', $params))
  {
    return array("--NO CHARGE DESCRIPTION GIVEN TO chargeup()");
  }

  $customer = $params['customer_record'];
  $chargeup = $params['chargeup'];

  $chargeups = study_chargeup($customer->BALANCE,
                              $customer->PACKAGED_BALANCE1,
                              /* the signup plan is no good */
                              $fallback > 0 ? $fallback : $customer->COS_ID);

  $found = null;

  $type = $chargeups['type'];
  foreach ($chargeups['chargeups'] as $c)
  {
    if ($c['cost'] === $chargeup)
    {
      $found = $c;
    }
  }

  if (!$found)
  {
    return array("--COULD NOT FIND chargeup() cost $chargeup!");
  }

  $desc = "ERROR ERROR ERROR: unhandled chargeup() type $type!";

  // adjust the minutes and balance here
  switch ($type)
  {
  case 'Recharge':
    $desc = "Add Balance";
    $q = sprintf("UPDATE accounts SET balance = balance + $found[cost], last_recharge_date_time = getdate() WHERE CUSTOMER_ID = %d", $customer->CUSTOMER_ID);
    break;
  case 'PAYGMinutes':
    $desc = "PAYG Minutes Purchase";
    $q = sprintf("UPDATE accounts
SET packaged_balance1 = packaged_balance1 + $found[minutes]*60,
    last_recharge_date_time = getdate()
WHERE CUSTOMER_ID = %d", $customer->CUSTOMER_ID);
    break;
  case 'TopUp':
    $desc = "Minutes Top Up";
    $q = sprintf("UPDATE accounts SET packaged_balance1 = packaged_balance1 + $found[minutes]*60 WHERE CUSTOMER_ID = %d; ", $customer->CUSTOMER_ID);
    break;
  default:
    fraud_event($customer, 'chargeup', strtolower($type), 'error', null);
    return array("--INVALID CHARGEUP TYPE $type!");
  }


  // Track plan purchase attempts/failures/successes. (ICLEAN-148)
  $fraud = fraud_detect_chargeup($customer, null);

  if ($fraud)
  {
    dlog("","CHARGE BLOCK $fraud type=$type " . json_encode($customer) );
    record_blocked_fraud_chargeup( get_ip_address() );
    record_fraud_chargeup($customer,strtolower($type),get_ip_address());
    return array("--CHARGE BLOCK " . $fraud);
  }


  // note we always adjust the balance so we add the COST not the TOTAL (which is COST plus TAX)
  $out['adjust_balance'] = TRUE;
  $out['amount'] = $found['total'];
  $out['charge_description'] = $desc;

  $charge = charge($out);

  foreach ($charge as $charge_entry)
  {
    if (is_array($charge_entry) && array_key_exists('error', $charge_entry))
    {
      fraud_event($customer, 'chargeup', strtolower($type), 'error', null);

      return array("--CHARGE ERROR: " . $charge_entry['error']);
    }

    if (is_array($charge_entry) && array_key_exists('charge_check_error', $charge_entry))
    {
      fraud_event($customer, 'chargeup', strtolower($type), 'error', null);

      return array("--CHARGE CHECK ERROR: " . $charge_entry['charge_check_error']);
    }
  }

  $todo2 = array_merge($todo2, $charge);

  $q2 = sprintf("UPDATE billing SET quantity=%d WHERE billing_id= (SELECT top 1 billing_id FROM billing WHERE description='%s' AND account_id=%d ORDER BY billing_id DESC)",
                $found['minutes'], $desc, $customer->ACCOUNT_ID);

  if (is_mssql_successful(logged_mssql_query($q)))
  {
    // it's OK if q2 fails
    is_mssql_successful(logged_mssql_query($q2));
    $todo2[] = "--CHARGEUP query = [$q]";
    $todo2[] = "--CHARGEUP query2 = [$q2]";
    $todo2[] = array('chargeup_update' => $q);
    $todo2[] = array('chargeup_billing_update' => $q2);
    $todo2[] = array('chargeup_clamputed' => 'yay');

    fraud_event($customer, 'chargeup', strtolower($type), 'ok', null);
  }
  else
  {
    $mssql_last_message = mssql_get_last_message();

    fraud_event($customer, 'chargeup', strtolower($type), 'error', null);

    return array('error' => "The chargeup update [$q] query failed: $mssql_last_message");
  }

  return $todo2;
}

function record_fraud_chargeup($customer,$type,$ip)
{
  // ICLEAN-148

  if ( should_audit_fraud_chargeup( $ip ) )
  {
    // store in HTT_AUDITLOG only after count reaches a series of thresholds

    $todays_blocks_count_key = "blocked_chargeups_count/".date("d.m.y")."/".$ip;

    $count = mc_read($todays_blocks_count_key);

    fraud_event($customer, 'chargeup', strtolower($type), 'block_' . $count, null);
  }
}

function should_audit_fraud_chargeup( $ip )
{
  # this series begins with the following values:
  # 5 10 20 40 80 160 320 640 1280 2560 5120 ...

  $threshold = 5;
  $found = FALSE;

  $todays_blocks_count_key = "blocked_chargeups_count/".date("d.m.y")."/".$ip;

  $count = mc_read($todays_blocks_count_key);

  while(
    ( $count >= $threshold )
    &&
    ( ! $found )
  )
  {
    if ( $count == $threshold )
    {
      $found = TRUE;
    }
    $threshold *= 2;
  }

  return $found;
}

function record_blocked_fraud_chargeup( $ip )
{
  // use memcached for counting blocked chargeups by IP address

  $todays_blocks_count_key = "blocked_chargeups_count/".date("d.m.y")."/".$ip;

  // mc_increment does not create an item if it doesn't already exist.

  $count_blocks = mc_read($todays_blocks_count_key);

  if ( $count_blocks )
  {
    mc_increment($todays_blocks_count_key);
  }
  else
  {
    mc_write($todays_blocks_count_key, 1);
  }
}

function charge($params)
{
  global $out;
  $todo2 = array();

  if (!array_key_exists('session', $params))
  {
    return array("--NO SESSION ID GIVEN TO charge()");
  }

  if (!array_key_exists('amount', $params))
  {
    return array("--NO AMOUNT GIVEN TO charge()");
  }

  if (!array_key_exists('customer_record', $params))
  {
    return array("--NO CUSTOMER RECORD GIVEN TO charge()");
  }

  if (!array_key_exists('charge_description', $params))
  {
    return array("--NO CHARGE DESCRIPTION GIVEN TO charge()");
  }

  if (!array_key_exists('charge_detail', $params))
  {
    $params['charge_detail'] = 'Web Interface';
  }

  $customer = $params['customer_record'];

  $amount = $params['amount'];
  $params['upgrade_seconds_gain'] = array_key_exists('upgrade_seconds_gain', $params) ? $params['upgrade_seconds_gain'] : 0;
  $params['adjust_balance'] = array_key_exists('adjust_balance', $params) ? $params['adjust_balance'] : FALSE;

// TODO: format the date and time so we can get them later

//  set_tz_by_offset(-8);

//  dlog("", json_encode($customer));

  $date = date('Y-m-d H:i:s');
  $time = date('12/30/1899 H:i:s');
  //dlog("", "for WEBCC insert, generated date $date and time $time");
  $q = sprintf("INSERT INTO WebCC (SessionId, date, time, Account, First_Name, Last_Name, Company, Address, Address2, City, State, Country, ZipCode, Telephone, Email, CreditCard, CreditExpiry, Charge_Amount, Credit_Amount, Status, Account_Group_Id, COS_ID, CCV, Description, Detail, Packaged_Balance1) values (".
               "'%s'," . //session
               "'%s',". // date
               "'%s',". // time
               "'%s',". // customer
               "'%s',". // first
               "'%s',". // last
               "'%s',". // company
               "'%s',". // address1
               "'%s',". // address2
               "'%s',". // city
               "'%s',". // state_region
               "'%s',". // country
               "'%s',". // postal_code
               "'%s',". // local_phone
               "'%s',". // e_mail
               "'%s',". // cc_number
               "'%s',". // cc_exp_date
               "%.2f,%.2f," . // charge and credit amounts
               "'WPENDING',". // status
               "%s,%s,'%s',". // account_group_id, cos_id, CCV
               "%s," . // charge_description
               "%s, -1*%d)", // detail and packaged_balance1

               mssql_escape($params['session']),
               $date,
               $time,
               $customer->CUSTOMER,
               $customer->FIRST_NAME,
               $customer->LAST_NAME,
               $customer->COMPANY,
               $customer->ADDRESS1,
               $customer->ADDRESS2,
               $customer->CITY,
               $customer->STATE_REGION,
               $customer->COUNTRY,
               $customer->POSTAL_CODE,
               $customer->LOCAL_PHONE,
               $customer->E_MAIL,
               $customer->CC_NUMBER,
               $customer->CC_EXP_DATE,
               $amount,                 /* charge amount */
               $amount,                 /* credit amount */
               $customer->ACCOUNT_GROUP_ID,
               $customer->COS_ID,
               $customer->CCV,
               mssql_escape($params['charge_description']),
               mssql_escape($params['charge_detail']),
               $params['upgrade_seconds_gain']);

  //print_r("WebCC insert = [$q]");
  if (is_mssql_successful(logged_mssql_query($q)))
  {
    $todo2[] = "--WEBCC query = [$q]";
    $todo2[] = array('charge_insert' => $q);
  }
  else
  {
    $mssql_last_message = mssql_get_last_message();
    return array('error' => "The charge insert [$q] query failed: $mssql_last_message");
  }

  for ($i = 0; $i < 10; $i++)
  {
    sleep(5);
    $q = sprintf("SELECT transactionid, STATUS, RESULT From WebCC WHERE SessionId = '%s' AND date = '%s' AND time = '%s' ",
                 mssql_escape($params['session']),
                 $date,
                 $time);
    $cc = mssql_fetch_all_objects(logged_mssql_query($q));

    $todo2[] = "--WEBCC query2 = [$q]";
    $todo2[] = array('ccheck' => $cc);

    if ($cc && is_array($cc) && count($cc) > 0)
    {
      $status = strtoupper($cc[0]->STATUS);
      if (!array_key_exists('charge_log', $out))
      {
        $out['charge_log'] = array();
      }

      $out['charge_log'][$cc[0]->transactionid] = $cc[0]->STATUS;
      switch ($status)
      {
      case 'APPROVED':
        if ($params['adjust_balance'])
        {
          $q = sprintf("UPDATE ACCOUNTS SET balance = balance - %f WHERE account = '%s'; ", $amount, $customer->ACCOUNT);
          $todo2[] = "--WEBCC balance_adjust_query = [$q]";
          if (is_mssql_successful(logged_mssql_query($q)))
          {
            $todo2[] = array('charge_adjust_balance_done' => $amount);
          }
        }
        // note there is no break here!
      case 'REJECTED':
        $todo2[] = array('charge_check' => $cc[0]->RESULT);
        if ($status === 'REJECTED')
        {
          $todo2[] = array('charge_check_error' => $cc[0]->RESULT);
        }
        return $todo2;
        break;

      case "PENDING":
      case "WPENDING":
      case "LOOKUP":
        // do nothing, continue the loop
        break;

      default:
        $todo2[] = array('charge_check_unhandled' => $cc[0], 'charge_check_error' => $cc[0]);
        return $todo2;
        break;
      }
    }
    else
    {
      $todo2[] = array('charge_check_error' => mssql_get_last_message());
      return $todo2;
    }
  }

  return $todo2;
}

?>
