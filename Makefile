all: composer syntax
composer:
	php composer.phar self-update
	php composer.phar update

syntax:
	find . -path './vendor' -prune -o -name "*.php" | xargs -n1 php -l
