<?php

define( 'FILE_PATH', '/var/tmp/Ultra/ProjectW/' );

// determine date to use
if ( time() < strtotime( 'today 8am' ) )
{
  // if it's before 8 AM PST, use yesterday's date
  $date = date( 'Y-m-d',  strtotime( 'yesterday' ) );
}
else
{
  $date = date( 'Y-m-d' );
}

// check for existing files
$existingFiles = glob( FILE_PATH . 'ProjectW-' . $date . '*.csv' );

?>
<!DOCTYPE html>
<html lang='en'>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <style>
  #files-table { border-collapse:collapse; }
  #files-table tr td, #files-table tr th { padding: 8px 16px 8px 16px; }
  #files-table .num-imported, #files-table .import, #files-table .delete, #files-table .import-log, #files-table .error-log, #files-table .view-stats { text-align: center; }
  #files-table tbody tr:nth-child(odd) { background: #f1f1f1; }
  .stats-table-holder table tr td:first-child { text-align: right; padding-right: 8px !important; }
  .stats-table-holder table tr td { padding: 0 !important; background-color: white !important }
  </style>
</head>
<body>
	<div style="width: 1024px; margin: 0px auto;">
    <script type="text/javascript">
    // check imported files
    $(function() {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=publicprojw&command=publicprojw__GetTodaysFiles',
        success: function(response) {
          var r = $.parseJSON(response);
          if (r.success) {
            // update files table for imported files
            for (var filename in r.files) {
              var file = r.files[filename];
              var row = $('#files-table .filename:contains("' + filename + '")').parent();
              // set imported class
              row.attr('imported', 1);

              row.children('.status').html(file.IMPORT_FILE_STATUS);
              var importLogURL = '/ultra_api.php?bath=rest&version=2&partner=publicprojw&format=mime&mime=csv&command=publicprojw__ExportFile';
              importLogURL += '&filename=' + filename;
              var errorsLogURL = '/ultra_api.php?bath=rest&version=2&partner=publicprojw&format=mime&mime=csv&command=publicprojw__ExportErrors';
              errorsLogURL += '&filename=' + filename;
              row.children('.import-log').html('<a href="' + importLogURL + '" target="_blank">View</a>');
              row.children('.error-log').html('<a href="' + errorsLogURL + '" target="_blank">View</a>');
            }

            // update files table for non-imported files
            $('#files-table .file:not([imported])').each(function() {
              var filename = $(this).children('.filename').html();
              $(this).children('.status').html('Not Imported');
            });

          } else {
            alert('API call publicprojw__GetTodaysFiles failed');
          }
        }
      });

      // handle view stats link
      $('.view-stats-link').click(function() {
        $(this).html('Refresh');
        var filename = $(this).parent().parent().children('.filename').html();
        var tableHolderElement = $(this).siblings('.stats-table-holder');

        setStatsTable(filename, tableHolderElement);
      });
    });

    function setStatsTable(filename, tableHolderElement)
    {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=publicprojw&command=publicprojw__GetFileStats',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors while retrieving stats for file ' + filename);
          } else {
            console.log(r);
            if (r.statuses.length == 0) {
              tableHolderElement.html('No data');
            } else {
              var str = '<table>';
              for (var i=0; i < r.statuses.length; i++) {
                var status = r.statuses[i];
                str += '<tr><td>' + status.STATUS + '</td><td>' + status.COUNT + '</td></tr>';
              }
              str += '</table>';

              tableHolderElement.html(str);
            }
          }
        }
      });
    }
    </script>
	<table id="files-table">
		<thead>
			<tr>
				<th>Filename</th>
				<th>Status</th>
				<th>Import Log</th>
				<th>Error Log</th>
        <th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($existingFiles as $file) { ?>
			<?php $filename = str_replace( FILE_PATH, '', $file ); ?>
          <?php $isRemoved = strstr( $filename, 'REMOVED' ) !== false; ?>

        	<?php if ($isRemoved) continue; ?>
			<tr class="file">
				<td class="filename"><?php echo $filename; ?></td>
				<td class="status"></td>
				<td class="import-log"></td>
				<td class="error-log"></td>
        <td class="view-stats">
          <a href="#" class="view-stats-link">View Import Stats</a>
          <br />
          <div class="stats-table-holder"></div>
        </td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	</div>
</body>
