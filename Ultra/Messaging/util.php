<?php

/**
 * Ultra/Messaging/util.php
 * 
 * Command line interface for retrieving, removing, and updating
 * SMS messaging templates
 *
 * Examples:
 * php Ultra/Messaging/util.php updateOrAddTemplate temp_password EN 'Your temporary password is __params__temp_password'
 * php Ultra/Messaging/util.php removeTemplate temp_password
 * php Ultra/Messaging/util.php getTemplate temp_password EN
 * php Ultra/Messaging/util.php getAllTemplates
 */

require_once 'db.php';
require_once 'Ultra/Messaging/Templates.php';

function runCommand($command, $template, $language, $text)
{
  $output = NULL;

  switch ($command)
  {
    case 'updateOrAddTemplate':
      if ( !$template || !$language || !$text )
        $output = 'Missing required parameter(s) [template,language,text]';
      else
      {
        if (!\Ultra\Messaging\Templates\updateOrAddTemplate($template, $language, $text))
          $output = 'FAILURE';
      }
      break;

    case 'removeTemplate':
      if ( !$template )
        $output = 'Missing required parameter(s) [template]';
      else
      {
        if (!\Ultra\Messaging\Templates\removeTemplate($template))
          $output = 'FAILURE';
      }
      break;

    case 'getTemplate':
      if ( !$template || !$language )
        $output = 'Missing required parameter(s) [template,language]';
      else
        $output = \Ultra\Messaging\Templates\getTemplate($template, $language);
      break;

    case 'getAllTemplates':
      $output = \Ultra\Messaging\Templates\getAllTemplates();
      break;

    default:
      $output = 'Unsupported command';
      break;
  }

  if ($output == NULL)
    $output = 'Success';

  return $output;
}

/*
$arr = array();

$sms_templates = \Ultra\Messaging\Templates\SMS_templates();
foreach ($sms_templates as $key => $val)
{
  list ($template, $language) = explode('__', $key);
  $arr[$template][$language] = convertUnicodeToJavaEscape($val);
}

$jsonPretty = new \Camspiers\JsonPretty\JsonPretty();
file_put_contents(MESSAGING_TEMPLATES_FILE, $jsonPretty->prettify($arr));

exit;
*/

$command  = (isset($argv[1])) ? $argv[1] : NULL;
$template = (isset($argv[2])) ? $argv[2] : NULL;
$language = (isset($argv[3])) ? $argv[3] : NULL;
$text     = (isset($argv[4])) ? $argv[4] : NULL;

echo 'Executing with params:' . PHP_EOL . PHP_EOL;
echo 'command: '  . $command  . PHP_EOL;
echo 'template: ' . $template . PHP_EOL;
echo 'language: ' . $language . PHP_EOL;
echo 'text: '     . $text     . PHP_EOL;

echo PHP_EOL;
$output = runCommand($command, $template, $language, $text);

if (!is_array($output)) echo $output;
else  print_r($output);

echo PHP_EOL . PHP_EOL;
