<?php

/**
 * Dealer Portal E-mail Messaging
 * @see https://issues.hometowntelecom.com:8443/browse/DEAL
 * @author VYT, 2015
 */

namespace Ultra\Messaging\Email\DealerPortal;


/**
 * sendTemplateEmail
 * send an email based on a given tempate and its parameters
 * @param Array of parameters
 * @returns String NULL on success or error message on failure
 */
function sendTemplateEmail($template, $params)
{
  // validate template and prepare its required parameters
  dlog('', 'params: %s', func_get_args());
  if ( ! $required = getRequiredParameters($template))
    return "Unknown template $template";
  $values = array();
  foreach ($required as $name)
    if (empty($params[$name]))
      return "Missing required parameter $name";
    else
      $values[$name] = $params[$name];

  // prepare postage app default values
  $postage = array(
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_name'   => $template,
    'email'           => $params['email'],
    'subject'         => NULL,
    'header'          => empty($params['headers']) ? array() : $params['headers'],
    'template_params' => $values);

  // send email
  return send_postage_app_mail($postage) ? NULL : 'Failed to send e-mail message';
}


/**
 * getRequiredParameters
 * given an e-mail template return an array of all its required parameters
 * @param String Postage App template name (as configured by administrator at www.postageapp.com)
 * @returns Array or NULL on failure
 */
function getRequiredParameters($template)
{
  // email templates and their required parameters
  $params = array(
    'dealer-new-retailer' => array(
      'email',
      'business_name',
      'business_code',
      'master_name',
      'username',
      'password'),
    'dealer-new-master' => array(
      'email',
      'business_name',
      'business_code',
      'username',
      'password'),
    'dealer-new-user' => array(
      'email',
      'first_name',
      'last_name',
      'username',
      'password'),
    'dealer-forgot-password' => array(
      'email',
      'first_name',
      'last_name',
      'password'),
    'dealer-send-username' => array(
      'email',
      'first_name',
      'last_name',
      'username'));

  return empty($params[$template]) ? NULL : $params[$template];
}
