<?php

require_once 'classes/PHPUnitBase.php';
require_once 'Ultra/Messaging/Email/DealerPortal.php';

class DealerPortalTest extends PHPUnitBase
{
  public function test_sendWelcomeEmail()
  {
    // test unknown template
    $error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail('bad-template', array());
    // print_r($error);
    $this->assertNotEmpty($error);

    // test missing parameters
    $error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail('dealer-new-retailer', array());
    // print_r($error);
    $this->assertNotEmpty($error);

    // successfull test
    $params = array(
      'username'      => 'UNEDEMO2',
      'password'      => '5fiqgqvf',
      'first_name'    => 'Vitaly',
      'last_name'     => 'Tarasov',
      'email'         => 'vtarasov@ultra.me');
    $error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail('dealer-new-user', $params);
    print_r($error);
    $this->assertEmpty($error);

    // PROD-1827: test master
    $params = array(
      'email'         => array('vtarasov@ultra.me', 'mariel@ultra.me'), // must use array for BCC mode and list all recipients
      'business_name' => 'Mordor Telecom',
      'business_code' => 'SPIFF',
      'username'      => 'sauron',
      'password'      => 'ring' . rand(100, 999),
      'headers'       => array('cc' => 'mariel@ultra.me'));
    $error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail('dealer-new-master', $params);
    print_r($error);
    $this->assertEmpty($error);

    // PROD-1827: test dealer
    $params = array(
      'email'         => array('vtarasov@ultra.me', 'mariel@ultra.me'), // must use array for BCC mode and list all recipients
      'business_name' => 'Hobbit Wireless',
      'business_code' => 'FFIPS',
      'username'      => 'frodo',
      'password'      => 'ring' . rand(100, 999),
      'headers'       => array('cc' => 'mariel@ultra.me, bilbo@shire.com')); // unline recipients, CC list must be specified as comma + space delimited lists
    $error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail('dealer-new-retailer', $params);
    print_r($error);
    $this->assertEmpty($error);

  }

}
