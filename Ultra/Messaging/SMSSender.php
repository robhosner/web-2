<?php

namespace Ultra\Messaging;

require_once 'Ultra/Configuration/Configuration.php';

use \Ultra\Configuration\Configuration;
use Twilio\Rest\Client;

class SMSSender
{
  private static $instance;
  private $configObj;
  private $config;
  private $clientObj;

  /**
   * SMSSender constructor.
   * @param Configuration|null $configObj
   * @throws \Exception
   */
  private function __construct(Configuration $configObj=null)
  {
    // get config
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();
    $this->config = $this->configObj->getTwilioConfig();
    if (empty($this->config)) {
      throw new \Exception('Missing Twilio configuration');
    }

    // create client object
    $this->clientObj = new Client($this->config['account_sid'], $this->config['auth_token']);
  }

  /**
   * @param Configuration|null $configObj
   * @return SMSSender
   */
  public static function getInstance(Configuration $configObj=null)
  {
    if (empty(self::$instance)) {
      self::$instance = new self($configObj);
    }

    return self::$instance;
  }

  /**
   * @param $toMSISDN
   * @param $body
   * @return bool
   */
  public function send($toMSISDN, $body)
  {
    try {
      $this->clientObj->messages->create(
        $toMSISDN,
        [
          'from'  => $this->config['from_msisdn'],
          'body'  => $body
        ]
      );
    } catch (\Exception $e) {
      \logError($e->getMessage());

      return false;
    }

    \logInfo('Sent "' . $body . '" to ' . $toMSISDN);

    return true;
  }
}
