<?php
namespace Ultra\Messaging;

use DateTime;
use LineCredits;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Utilities\Common;
use Ultra\Utilities\SessionUtilities;

class FlexMessenger
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var FamilyAPI
   */
  private $familyAPI;

  /**
   * @var LineCredits
   */
  private $lineCredits;

  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var SessionUtilities
   */
  private $sessionUtilities;

  /**
   * @var Common
   */
  private $commonUtilities;

  // DBA request to only ask for this amount of customers at a time.
  public $maxNumberOfCustomersToQuery = 1000;

  /**
   * FlexMessenger constructor.
   * @param CustomerRepository $customerRepository
   * @param FamilyAPI $familyAPI
   * @param LineCredits $lineCredits
   * @param Messenger $messenger
   * @param SessionUtilities $sessionUtilities
   * @param Common $commonUtilities
   */
  public function __construct(
    CustomerRepository $customerRepository,
    FamilyAPI $familyAPI,
    LineCredits $lineCredits,
    Messenger $messenger,
    SessionUtilities $sessionUtilities,
    Common $commonUtilities
  ) {
    $this->customerRepository = $customerRepository;
    $this->familyAPI = $familyAPI;
    $this->lineCredits = $lineCredits;
    $this->messenger = $messenger;
    $this->sessionUtilities = $sessionUtilities;
    $this->commonUtilities = $commonUtilities;

    teldata_change_db();
  }

  public function setMessageApproachingRenewDate()
  {
    $renewDates = [
      new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 5, date("Y"))),
      new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 2, date("Y"))),
    ];

    foreach ($renewDates as $renewDate) {
      $this->setRenewalMessaging($renewDate);
    }
  }

  public function setMessageOnRenewDate()
  {
    $this->setRenewalMessaging(new DateTime('@' . mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"))), true);
  }

  public function setRenewalMessaging(DateTime $renewDate, $onRenewDate = false)
  {
    $familyRenewalList = $this->getRenewalCustomers($renewDate);

    foreach ($familyRenewalList as $family) {
      foreach ($family['customerInfo'] as $customer) {
        $customerId = $family['parentId'];
        $msisdn = $family['parentMsisdn'];

        if (
          $customer['isParent'] &&
          $customer['payEntire'] &&
          $customer['lineCredits'] < $family['totalSupportedMembers'] &&
          !$customer['autoRecharge']
        ) {
          $template = $onRenewDate ? 'flex_parent_plan_expires_today' : 'flex_parent_close_to_expiration';
        } elseif ($customer['payEntire'] && $family['parentLineCredits'] < $family['totalSupportedMembers']) {
          if (empty($family['parentAutoRecharge'])) {
            $template = $onRenewDate ? 'flex_member_recharge_off_plan_expires_today' : 'flex_member_recharge_off_close_to_expiration';
          } else {
            $template = 'flex_member_recharge_on_close_to_expiration';
          }
        } elseif (
          $family['parentLineCredits'] == 0 &&
          $customer['lineCredits'] == 0 &&
          !$customer['payEntire'] &&
          empty($family['parentAutoRecharge'])
        ) {
          $template = $onRenewDate ? 'flex_member_pay_for_family_off_plan_expires_today' : 'flex_member_pay_for_family_off_close_to_expiration';
          $customerId = $customer['customerId'];
          $msisdn = $customer['msisdn'];
        } else {
          continue;
        }

        $messageParams = [
          'message_type' => $template,
          'renewal_date' => $customer['renewalDate'],
          'login_token' => $this->sessionUtilities->encryptToken($msisdn)
        ];

        $priority = $onRenewDate ? 2 : 3;
        $this->enqueueMessage($customerId, $messageParams, $priority);
      }
    }
  }

  public function getRenewalCustomers(DateTime $renewDate)
  {
    $customerResult = $this->customerRepository->getFlexMessagingCustomers($renewDate);
    $customerIds = [];
    $customerList = [];
    $familyResults = [];
    $familyCustomers = [];
    $customersNotFoundInFamily = [];
    $familyRenewalList = [];

    if (!$customerResult || count($customerResult) == 0) {
      return [];
    }

    foreach ($customerResult as $customer) {
      $customerIds[] = $customer->customer_id;
      $customerList[$customer->customer_id] = $customer;
    }

    $numberOfCustomerIds = count($customerIds);

    // how many times to call api
    $numberOfCustomerIdGroups = ceil($numberOfCustomerIds / $this->maxNumberOfCustomersToQuery);
    $groupedCustomerIds = array_chunk($customerIds, ceil($numberOfCustomerIds / $numberOfCustomerIdGroups));

    // call api with each group of customers
    foreach ($groupedCustomerIds as $customerIdsGroup) {
      $getCustomersResult = $this->familyAPI->getCustomers($customerIdsGroup);

      if ($getCustomersResult->is_success()) {
        $familyResults[] = $getCustomersResult->data_array['customerIds'];
      }
    }

    // flatten the group of customers to one list
    foreach ($familyResults as $familyResult) {
      foreach ($familyResult as $customer) {
        $familyCustomers[$customer['Customer_ID']] = $customer;
      }
    }

    // assemble family and members
    foreach ($customerList as $customerId => $customer) {
      if (!empty($familyCustomers[$customerId])) {
        $customerFamilyInfo = $familyCustomers[$customerId];
        $familyId = $customerFamilyInfo['Family_ID'];

        if (empty($familyRenewalList[$familyId])) {
          $familyRenewalList[$familyId]['totalSupportedMembers'] = 0;
          $familyRenewalList[$familyId]['parentLineCredits'] = null;
          $familyRenewalList[$familyId]['parentId'] = $customerFamilyInfo['Parent_ID'];

          $parent = $this->customerRepository->getCustomerById($familyRenewalList[$familyId]['parentId'], ['monthly_cc_renewal', 'current_mobile_number']);
          $familyRenewalList[$familyId]['parentAutoRecharge'] = $parent ? $parent->monthly_cc_renewal : 0;
          $familyRenewalList[$familyId]['parentMsisdn'] = $parent ? $parent->current_mobile_number : 0;
        }

        if ($customerFamilyInfo['IsParent']) {
          $familyRenewalList[$familyId]['parentLineCredits'] = $customer->Balance;
          $familyRenewalList[$familyId]['totalSupportedMembers'] += 1;
        } elseif (
          !$customerFamilyInfo['IsParent'] &&
          $customer->Balance == 0 &&
          ($customer->monthly_cc_renewal == null || !$customer->monthly_cc_renewal)
        ) {
          $familyRenewalList[$familyId]['totalSupportedMembers'] += 1;
        } elseif ($familyRenewalList[$familyId]['parentLineCredits'] == null) {
          $this->lineCredits->dbConnect();
          $familyRenewalList[$familyId]['parentLineCredits'] = $this->lineCredits->getBalance($familyRenewalList[$familyId]['parentId']);
          teldata_change_db();
        }

        $familyRenewalList[$familyId]['customerInfo'][] = [
          'customerId' => $customer->customer_id,
          'msisdn' => $customer->current_mobile_number,
          'renewalDate' => (new DateTime($customer->renewal_date))->format('M jS'),
          'renewalTarget' => $customer->monthly_renewal_target,
          'autoRecharge' => $customer->monthly_cc_renewal,
          'lineCredits' => $customer->Balance,
          'isParent' => (bool) $customerFamilyInfo['IsParent'],
          'payEntire' => (bool) $customerFamilyInfo['PayEntire'],
          'members' => $customerFamilyInfo['TotalMembers'],
        ];
      } else {
        // customer was probably removed from a family, not found in family db
        $customersNotFoundInFamily[] = $customerId;
      }
    }

    return $familyRenewalList;
  }

  public function enqueueMessage($customerId, array $messageParams, $priority)
  {
    // compute proper delivery time
    list($delivery_datetime, $delivery_timestamp) = $this->commonUtilities->computeDeliveryDaytime(time(), $this->customerRepository->getCustomerTimeZone($customerId), $priority);

    $result = $this->messenger->addToMessagingQueue([
      'customer_id'                 => $customerId,
      'reason'                      => $messageParams['message_type'],
      'expected_delivery_datetime'  => $delivery_datetime,
      'next_attempt_datetime'       => $delivery_datetime,
      'next_attempt_timestamp'      => $delivery_timestamp,
      'type'                        => 'SMS_INTERNAL',
      'conditional'                 => 0,
      'messaging_queue_params'      => $messageParams
    ]);

    if ($result->is_success()) {
      \dlog('', "setRenewalMessaging added message in queue for customer_id " . $customerId);
    } else {
      \dlog('', "ERROR: setRenewalMessaging could not add message in queue for customer_id " . $customerId);
    }
  }
}
