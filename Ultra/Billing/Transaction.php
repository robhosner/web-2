<?php
namespace Ultra\Billing;

use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Hydrator;

/**
 * Class Transaction
 * @package Ultra\Billing
 */
class Transaction
{
  use Hydrator;

  /**
   * @var
   */
  public $transaction_id;

  /**
   * @var
   */
  public $customer_id;

  /**
   * @var
   */
  public $transaction_date;

  /**
   * @var
   */
  public $cos_id;

  /**
   * @var
   */
  public $entry_type;

  /**
   * @var
   */
  public $stored_value_change;

  /**
   * @var
   */
  public $balance_change;

  /**
   * @var
   */
  public $package_balance_change;

  /**
   * @var
   */
  public $charge_amount;

  /**
   * @var
   */
  public $reference;

  /**
   * @var
   */
  public $reference_source;

  /**
   * @var
   */
  public $detail;

  /**
   * @var
   */
  public $description;

  /**
   * @var
   */
  public $result;

  /**
   * @var
   */
  public $source;

  /**
   * @var
   */
  public $store_zipcode;

  /**
   * @var
   */
  public $store_id;

  /**
   * @var
   */
  public $clerk_id;

  /**
   * @var
   */
  public $terminal_id;

  /**
   * @var
   */
  public $is_commissionable;

  /**
   * @var
   */
  public $surcharge_amount;

  /**
   * @var
   */
  public $commissionable_charge_amount;

  /**
   * Transaction constructor.
   * @param array $transactionInfo
   * @throws MissingRequiredParametersException
   */
  public function __construct(array $transactionInfo)
  {
    $this->hydrate($transactionInfo, true);

    if (empty($this->transaction_id))
    {
      throw new MissingRequiredParametersException(__CLASS__ . '::' . __FUNCTION__, ['transaction_id']);
    }
  }
}
