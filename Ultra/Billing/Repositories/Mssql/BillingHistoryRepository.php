<?php
namespace Ultra\Billing\Repositories\Mssql;

require_once __DIR__ . '/../../Interfaces/BillingHistoryRepository.php';

use Ultra\Billing\Interfaces\BillingHistoryRepository as BillingHistoryRepositoryInterface;
use Ultra\Billing\Transaction;

/**
 * Class BillingHistoryRepository
 * @package Ultra\Billing\Repositories\Mssql
 */
class BillingHistoryRepository implements BillingHistoryRepositoryInterface
{
  /**
   * Get billing transaction history by customer id.
   *
   * @param $customerId
   * @return array
   */
  public function getBillingTransactionHistoryByCustomerId($customerId)
  {
    $query = sprintf("
      SELECT TRANSACTION_DATE,
        ENTRY_TYPE,
        STORED_VALUE_CHANGE,
        BALANCE_CHANGE,
        DESCRIPTION,
        SOURCE,
        SURCHARGE_HISTORY_ID,
        AMOUNT,
        SURCHARGE_TYPE,
        bh.TRANSACTION_ID as order_id
      FROM HTT_BILLING_HISTORY bh
      LEFT JOIN ULTRA.SURCHARGE_HISTORY s ON bh.TRANSACTION_ID = s.TRANSACTION_ID
      WHERE customer_id = %d
      ORDER BY order_id desc", $customerId);

    return mssql_fetch_all_objects(logged_mssql_query($query));
  }

  /**
   * Get billing transaction history.
   *
   * @param $customerId
   * @param $startEpoch
   * @param $endEpoch
   * @return \array[], billing_transaction_history=>[])
   */
  public function getBillingTransactionHistory($customerId, $startEpoch, $endEpoch)
  {
    return get_billing_transaction_history([
      'customer_id' => $customerId,
      'start_epoch' => $startEpoch,
      'end_epoch'   => $endEpoch
    ]);
  }

  /**
   * @param $customerId
   * @param array $selectFields
   * @param $description
   * @param int $selectAmount
   * @return array
   */
  public function getBillingTransactionsByDescription($customerId, array $selectFields, $description, $selectAmount = -1)
  {
    $selectFields[] = 'TRANSACTION_ID';
    $selectFields = array_unique($selectFields);
    $selectFields = implode(', ', $selectFields);
    $selectAmount = $selectAmount > 0 ? "TOP $selectAmount " : '';
    $transactionCollection = [];

    $query = sprintf("
      SELECT $selectAmount $selectFields
      FROM HTT_BILLING_HISTORY
      WHERE CUSTOMER_ID = %d
      AND DESCRIPTION = %s",
      $customerId,
      mssql_escape_with_zeroes($description)
    );

    $result = mssql_fetch_all_objects(logged_mssql_query($query));

    foreach ($result as $transaction) {
      $transactionCollection[] = new Transaction((array) $transaction);
    }

    return $transactionCollection;
  }
}