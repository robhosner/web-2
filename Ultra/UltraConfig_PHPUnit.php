<?php

require_once 'Ultra/UltraConfig.php';

class testUltraConfig extends PHPUnit_Framework_testCase
{
  public function setUp()
  {
  }

  public function tearDown()
  {
  }

  public function test_isRoamingSoc()
  {
    $this->assertFalse(\Ultra\UltraConfig\isRoamingSoc('not_a_roaming_soc'));
    $this->assertTrue (\Ultra\UltraConfig\isRoamingSoc('n_roam_smpglb'));
    $this->assertTrue (\Ultra\UltraConfig\isRoamingSoc(12814));
  }

  public function test_isRoamingWalletSoc()
  {
    $this->assertFalse(\Ultra\UltraConfig\isRoamingWalletSoc('not_a_roaming_soc'));
    $this->assertTrue (\Ultra\UltraConfig\isRoamingWalletSoc('a_voicesmswallet_ir'));
    $this->assertTrue (\Ultra\UltraConfig\isRoamingWalletSoc(12707));
  }

  public function test_brands()
  {
    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrandName('dummy');
    $this->assertEmpty( $brand_id );

    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrandName('Ultra_Mobile');
    $this->assertEquals( 1 , $brand_id );

    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrandName('Univision');
    $this->assertEquals( 2 , $brand_id );

    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrandName('Mint');
    $this->assertEquals( 3 , $brand_id );


    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrand('dummy');
    $this->assertEmpty( $brand_id );

    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrand('ULTRA');
    $this->assertEquals( 1 , $brand_id );

    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrand('UNIVISION');
    $this->assertEquals( 2 , $brand_id );

    $brand_id = \Ultra\UltraConfig\getBrandIdFromBrand('MINT');
    $this->assertEquals( 3 , $brand_id );


    $brand = \Ultra\UltraConfig\getBrandFromBrandId( 4 );
    $this->assertEmpty( $brand );

    $brand = \Ultra\UltraConfig\getBrandFromBrandId( 3 );
    $this->assertEquals( 3      , $brand['id'] );
    $this->assertEquals( 'Mint' , $brand['brand_name'] );
    $this->assertEquals( 'MINT' , $brand['short_name'] );

    $brand = \Ultra\UltraConfig\getBrandFromBrandId( 2 );
    $this->assertEquals( 2           , $brand['id'] );
    $this->assertEquals( 'Univision' , $brand['brand_name'] );
    $this->assertEquals( 'UNIVISION' , $brand['short_name'] );

    $brand = \Ultra\UltraConfig\getBrandFromBrandId( 1 );
    $this->assertEquals( 1              , $brand['id'] );
    $this->assertEquals( 'Ultra_Mobile' , $brand['brand_name'] );
    $this->assertEquals( 'ULTRA'        , $brand['short_name'] );


    $this->assertFalse( \Ultra\UltraConfig\existsBrandId( 4 ) );
    $this->assertTrue( \Ultra\UltraConfig\existsBrandId( 3 ) );
    $this->assertTrue( \Ultra\UltraConfig\existsBrandId( 2 ) );
    $this->assertTrue( \Ultra\UltraConfig\existsBrandId( 1 ) );
  }

  public function test_all()
  {
    // integers

    $this->assertTrue( in_array( \Ultra\UltraConfig\loggerFlag() , [0,1,2] ) );
    $statusList = [
      'neutral', 'portindenied', 'portinrequested', 'provisioned', 'suspended'
    ];
    foreach( $statusList as $status )
      $this->assertNotEmpty( \Ultra\UltraConfig\monthly_charge_cancellation_delay( $status ) );

    $this->assertNotEmpty( \Ultra\UltraConfig\monthly_charge_exec_minutes() );

    // strings

    $this->assertNotEmpty( \Ultra\UltraConfig\acc_notification_wsdl()        );
    $this->assertNotEmpty( \Ultra\UltraConfig\acc_control_wsdl()             );
    $this->assertNotEmpty( \Ultra\UltraConfig\amdocs_sms_qualityofservice()  );
    $this->assertNotEmpty( \Ultra\UltraConfig\getConfigNamespace()           );
    $this->assertNotEmpty( \Ultra\UltraConfig\getCCCredentialsURL( 'MeS' )   );
    $this->assertNotEmpty( \Ultra\UltraConfig\getCCCredentialsKey( 'MeS' )   );
    $this->assertNotEmpty( \Ultra\UltraConfig\getDBName()                    );
    $this->assertNotEmpty( \Ultra\UltraConfig\getTSIPDBVersion()             );
    $this->assertNotEmpty( \Ultra\UltraConfig\incommURL()                    );
    $this->assertNotEmpty( \Ultra\UltraConfig\incommPartnerName()            );
    $this->assertNotEmpty( \Ultra\UltraConfig\celluphoneDb()                 );
    $this->assertNotEmpty( \Ultra\UltraConfig\accessTokenExacaster()         );
    $this->assertNotEmpty( \Ultra\UltraConfig\getTMOEncryptionKey()          );
    $this->assertNotEmpty( \Ultra\UltraConfig\getSessionTokenEncryptionKey() );

    // arrays

    $this->assertTrue( is_array( \Ultra\UltraConfig\redisClusterInfo()    ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\redisClusterInfo()          );

    $this->assertTrue( is_array( \Ultra\UltraConfig\solrCredentials()    ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\solrCredentials()          );

    $this->assertTrue( is_array( \Ultra\UltraConfig\amazonS3Credentials()    ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\amazonS3Credentials()          );

    $this->assertTrue( is_array( \Ultra\UltraConfig\getPaymentProviderSkuMap('TCETRA') ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\getPaymentProviderSkuMap('TCETRA') );

    $this->assertTrue( is_array( \Ultra\UltraConfig\acc_certificate_info()    ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\acc_certificate_info()          );

    $this->assertTrue( is_array( \Ultra\UltraConfig\ultra_api_internal_info() ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\ultra_api_internal_info()       );

    $this->assertTrue( is_array( \Ultra\UltraConfig\ultra_api_epay_info()    ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\ultra_api_epay_info()          );

    $this->assertTrue( is_array( \Ultra\UltraConfig\ccAlwaysFailTokens()    ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\ccAlwaysFailTokens()          );

    $this->assertTrue( is_array( \Ultra\UltraConfig\ccAlwaysSucceedTokens() ) );
    $this->assertNotEmpty( \Ultra\UltraConfig\ccAlwaysSucceedTokens()       );
  }
}

