<?php
namespace Ultra\Accounts\Repositories\Mssql;

use Ultra\Accounts\Account;

require_once __DIR__ . '/../../Interfaces/AccountsRepository.php';
require_once 'Ultra/Accounts/Account.php';

/**
 * Class AccountsRepository
 * @package Ultra\Accounts\Mssql
 */
class AccountsRepository implements \Ultra\Accounts\Interfaces\AccountsRepository
{
  /**
   * @param $customerId
   * @param array $selectFields
   * @return Account
   */
  public function getAccountFromCustomerId($customerId, array $selectFields)
  {
    $account = \get_account_from_customer_id($customerId, $selectFields);
    return new Account((array) $account);
  }

  /**
   * @param $customerID
   * @param array $updateFields
   * @return bool
   */
  public function updateAccount($customerID, array $updateFields)
  {
    $updateFields['customer_id'] = $customerID;

    $sql = \accounts_update_query($updateFields);
    return run_sql_and_check($sql);
  }
}
