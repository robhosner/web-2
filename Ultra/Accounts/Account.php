<?php
namespace Ultra\Accounts;

use Ultra\Hydrator;

require_once 'Ultra/Hydrator.php';

/**
 * Class Account
 * @package Ultra\Accounts
 */
class Account
{
  use Hydrator;

  /**
   * @var
   */
  public $cos_id;

  /**
   * @var
   */
  public $account;

  /**
   * Account constructor.
   * @param array $accountInfo
   */
  public function __construct(array $accountInfo)
  {
    $this->hydrate($accountInfo, true);
  }
}
