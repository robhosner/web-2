<?php

include_once('db.php');
include_once('Ultra/UltraConfig.php');

/*

$wholesalePlanIdMap = \Ultra\UltraConfig\getWholesalePlanIdMap();

print_r($wholesalePlanIdMap );

foreach( $wholesalePlanIdMap as $name => $id )
{
  echo \Ultra\UltraConfig\getwholesalePlanIdFromName( $name )."\n";
  echo \Ultra\UltraConfig\getWholesalePlanNameFromId( $id )."\n\n";
}

echo \Ultra\UltraConfig\getwholesalePlanIdFromName( 'dummy' )."\n";
echo \Ultra\UltraConfig\getWholesalePlanNameFromId( 'dummy' )."\n";

*/

teldata_change_db();

/*
echo \Ultra\UltraConfig\existsBrandId(1);
echo "<- 1\n";

echo \Ultra\UltraConfig\existsBrandId(2);
echo "<- 2\n";

echo \Ultra\UltraConfig\existsBrandId(3);
echo "<- 3\n";

echo \Ultra\UltraConfig\existsBrandId(4);
echo "<- 4\n";
*/

/*
  abstract testing
    php Ultra/UltraConfig_test.php $FUNCTION $PARAM1 $PARAM2 ...

  examples
    php Ultra/UltraConfig_test.php getBoltOnsMappingByType NINETEEN
    php Ultra/UltraConfig_test.php getBoltOnsByPlan TWENTY_FOUR
    php Ultra/UltraConfig_test.php getUltraPlanConfiguration FORTY_FOUR
    php Ultra/UltraConfig_test.php isBrandIdAllowed $BRAND_ID
    php Ultra/UltraConfig_test.php isBrandNameAllowed $NAME
*/

// first parameter is function name to test
$test = '\Ultra\UltraConfig\\' . (empty($argv[1]) ? NULL : $argv[1]);
if (is_callable($test))
{
  // collect script arguments into parameters array
  $info = new ReflectionFunction($test);
  for ($i = 2, $count = $info->getNumberOfParameters(), $params = array(); $i < 2 + $count; $i++)
    if (empty($argv[$i]))
    {
      echo "ERROR: function $test expects $count parameters\n";
      exit(1);
    }
    else
      $params[] = $argv[$i];

  // execute and print
  echo "TESTING: $test" . json_encode($params) . ":\n";
  echo 'RESULT: ' . print_r(call_user_func_array($test, $params), TRUE) . "\n";
}
else
  echo "ERROR: function '$test' does not exist\n";
exit(0);


////////////////   THESE ARE OLD TESTS - USE NEW MODEL ABOVE  /////////////////


$tmo_encryption_key =  \Ultra\UltraConfig\getTMOEncryptionKey();
echo "tmo_encryption_key: ".$tmo_encryption_key."\n";

$token_encryption_key = \Ultra\UltraConfig\getSessionTokenEncryptionKey();
echo "token_encryption_key: ".$token_encryption_key."\n";

// run boolean checks;
$boolean_checks = array(
  'cc_transactions_enabled',
  'makeitso_enabled',
  'invocations_resync_enabled',
  'amdocs_autofail',
  'amdocs_query_only',
  'middleware_enabled_amdocs',
  'are_dealer_portal_activations_enabled',
  'isDevelopmentDB',
  'isDevelopmentHost'
);

foreach ( $boolean_checks as $check )
{
  $function = '\Ultra\UltraConfig\\' . $check;
  echo "$function" . PHP_EOL;

  if ( $function() )
    echo "Y" . PHP_EOL;
  else
    echo "N" . PHP_EOL;
}

// getUltraPlanConfigurationItem TESTS OK


// RTRIN-18
$tests = array(
  //     provider    SKU              propery         expected result
  array('INVALID',  '12345',          'PROPERTY',     NULL),
  array('TCETRA',   '12345',          'PROPERTY',     NULL),
  array('TCETRA',   '1806',           'PROPERTY',     NULL),
  array('TCETRA',   '1806',           'DESTINATION',  'MONTHLY'),
  array('INCOMM',   '799366321149',   'SOURCE',       'QPAY'),
  array('EPAY',     '00843788026387', 'MAXIMUM',      10000),
  array('EPAY',     '00843788028886', 'SOURCE',       NULL),
  array('INCOMM',   '799366270560',   'SOURCE',       'INCOMM'));
foreach ($tests as $test)
{
  $result = \Ultra\UltraConfig\getPaymentProviderSkuAttribue($test[0], $test[1], $test[2]);
  echo "RESULT: '$result': " . ($result == $test[3] ? 'good' : 'bad') . "\n";
}
exit(0);

$dealers = \Ultra\UltraConfig\dealer_portal_demo_line_dealers();
print_r($dealers);
exit;

print_r(\Ultra\UltraConfig\getDefaultMongoDBCredentials());
exit();

print_r(\Ultra\UltraConfig\getAccCanActivateMessages(200));

echo 'handoff: ' . (\Ultra\UltraConfig\isPaymentProviderHandoffEnabled('INCOMM') ? 'TRUE' : 'FALSE') . "\n";


print_r(\Ultra\UltraConfig\getPaymentProviderCredentials('INCOMM'));

print_r(\Ultra\UltraConfig\getPaymentProviderSkuMap('TCETRA'));

print_r(\Ultra\UltraConfig\getUltraPlanConfigurationItem('LXX', 'name'));
exit(0);

foreach (array('L19','L29' ,'L39','L49','L59','NINETEEN','TWENTY_NINE','THIRTY_NINE','FORTY_NINE' ,'FIFTY_NINE') as $plan)
{
  echo "input: $plan, output: " . \Ultra\UltraConfig\getUltraPlanConfigurationItem($plan, 'name');
  echo "\n";
}

list($ln,$bin,$lf,$ed) = \Ultra\UltraConfig\ccAlwaysFailTokens();

echo "Last Name = $ln ; bin = $bin ; last four = $lf ; expiry date = $ed\n\n";

list($ln,$bin,$lf,$ed) = \Ultra\UltraConfig\ccAlwaysSucceedTokens();

echo "Last Name = $ln ; bin = $bin ; last four = $lf ; expiry date = $ed\n\n";

print_r( \Ultra\UltraConfig\call_anywhere_additional_credit_options() );

echo "\n";

echo get_plan_name_from_short_name('L29');

echo "\n";

print_r( \Ultra\UltraConfig\getBoltOnsByPlan('TWENTY_NINE') );

echo "\n";

print_r( \Ultra\UltraConfig\getBoltOnsMapping() );

echo "\n";

print_r( \Ultra\UltraConfig\getBoltOnsMappingByType() );

echo "\n";

print_r( \Ultra\UltraConfig\getBoltOnsOptionAttributes() );

echo "\n";

print_r( \Ultra\UltraConfig\getBoltOnInfo('IDDCA_12.5_10') );

echo "\n";

print_r( \Ultra\UltraConfig\getBoltOnInfoByOption('BOLTON.MRC_CALLANYWHERE',10) );

echo "\n";

print_r( \Ultra\UltraConfig\acc_certificate_info() );

echo "\n";

print_r( \Ultra\UltraConfig\ultra_api_internal_info() );

echo "\n";

print_r( \Ultra\UltraConfig\ultra_api_epay_info() );

echo "\n";

echo \Ultra\UltraConfig\acc_notification_wsdl();

echo "\n";

echo \Ultra\UltraConfig\acc_control_wsdl();

echo "\n";

echo \Ultra\UltraConfig\getCCCredentialsURL( 'MeS' );

echo "\n";

echo \Ultra\UltraConfig\getCCCredentialsProfile( 'MeS' );

echo "\n";

echo \Ultra\UltraConfig\getCCCredentialsKey( 'MeS' );

echo "\n";

echo \Ultra\UltraConfig\getPrimaryCCGateway();

echo "\n";

echo \Ultra\UltraConfig\getSecondaryCCGateway();

echo "\n";

echo \Ultra\UltraConfig\dealer_portal_activations_disabled_msg();

echo "\n";

echo \Ultra\UltraConfig\zeroUsedMinutes('NINETEEN');

echo "\n";

echo \Ultra\UltraConfig\zeroUsedMinutes('TWENTY_NINE');

echo "\n";

echo \Ultra\UltraConfig\getDBName();

echo "\n";

list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\UltraConfig\getAccSocsDefinitions();

print_r($accSocsDefinition);

echo "\n";

print_r($accSocsDefinitionByUltraSoc);

echo "\n";

print_r($ultraSocsByPlanId);

echo "\n";

$languages = array( 'it' , 'en-us' , 'en' , 'zh' , 'es-us' , 'en' );

foreach( $languages as $preferred_language )
{
  $languageSOC = \Ultra\UltraConfig\mapLanguageToVoicemailSoc( $preferred_language );

  echo "preferred_language = $preferred_language ; languageSOC = $languageSOC\n";
}

echo "\n";

$preferred_language = 'EN';

$languageSOC = \Ultra\UltraConfig\mapLanguageToVoicemailSoc( $preferred_language );

echo "preferred_language = $preferred_language ; languageSOC = $languageSOC\n";

print_r( $accSocsDefinitionByUltraSoc[ $languageSOC ] );

echo $accSocsDefinitionByUltraSoc[ $languageSOC ]['ultra_service_name']."\n";

list($host,$port) = explode(':',\Ultra\UltraConfig\redisHost());

echo " ($host,$port)\n";

echo \Ultra\UltraConfig\channelsDefaultReplyTimeoutSeconds();

echo "\n";

echo \Ultra\UltraConfig\celluphoneDb();

echo "\n";

echo \Ultra\UltraConfig\celluphoneUltrasessionOverride();

echo "\n";

echo \Ultra\UltraConfig\maxDuplicateCCCount();

echo "\n";

echo \Ultra\UltraConfig\allowInfiniteCCCount();

echo "\n";

echo \Ultra\UltraConfig\allowInfiniteEmailCount();

echo "\n";

$d = \Ultra\UltraConfig\getAccSocByPlanId( 32 );

print_r($d);

echo "\n";

$d = \Ultra\UltraConfig\getAccSocByName( 'W-PRIMARY' );

print_r($d);

echo "\n";

$d = \Ultra\UltraConfig\getAccSocByPlanId( 999 );

print_r($d);

echo "\n";

$d = \Ultra\UltraConfig\getAccSocByPlanId( 12006 );

print_r($d);

echo "\n";

$d = \Ultra\UltraConfig\getAccSocByName( 'test' );

print_r($d);

echo "\n";

$plans = array( 'NINETEEN', 'TWENTY_NINE', 'THIRTY_NINE', 'FORTY_NINE', 'FIFTY_NINE' , 'STANDBY' );

foreach( $plans as $plan )
{
  echo "$plan:\n";
  $c = \Ultra\UltraConfig\getAccSocsPlanConfig( $plan );
  print_r($c);
  $c = \Ultra\UltraConfig\getUltraPlanConfiguration( $plan );
  print_r($c);
}

echo "\n";

$p = \Ultra\UltraConfig\getAccSocDefinitionField( 'r_unlimited' , 'plan_id' );

print_r($p);

echo "\n";

$p = \Ultra\UltraConfig\getAccSocDefinitionField( 'r_limited' , 'plan_id' );

print_r($p);

echo "\n";

$x = \Ultra\UltraConfig\getAccSocsPlanConfigSwitch( 'NINETEEN', 'TWENTY_NINE' );

print_r($x);

echo "\n";

echo find_credential('sms/3ci/clientid');

echo "\n";

$types = array( 'DA1' , 'DA2' , 'DA15' , 'DA14' , '1000' );

foreach( $types as $type )
{
  echo $type." -> ".\Ultra\UltraConfig\getAccBalanceConfig($type)."\n";
}

echo "7001 | Used -> ".\Ultra\UltraConfig\getAccBalanceConfig( '7001' , 'Used' )."\n";

echo 'en    = '.\Ultra\UltraConfig\mapLanguageToVoicemailSoc( 'en' )."\n";
echo 'ES-US = '.\Ultra\UltraConfig\mapLanguageToVoicemailSoc( 'ES-US' )."\n";

echo \Ultra\UltraConfig\incommURL();

echo "\n";

echo \Ultra\UltraConfig\incommPartnerName();

echo "\n";

echo 'accessToken3ci = '.\Ultra\UltraConfig\accessToken3ci();

echo "\n";

echo 'msisdnAlwaysSucceed = '.\Ultra\UltraConfig\msisdnAlwaysSucceed();

echo "\n";

?>
