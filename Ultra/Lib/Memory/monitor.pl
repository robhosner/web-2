use strict;
use warnings;

# script output is the percentage of memory used according to top
my $percentage = 0;

my $top_data = `top -n1 -c -b | grep -A1 'Mem:'`;

my $date_time = ( scalar localtime ) . ' ' ;

# we assume $top_data will contain 2 strings like
#  Mem:   8053844k total,  4624980k used,  3428864k free,   678572k buffers
#  Swap:  2097144k total,    27536k used,  2069608k free,  1996880k cached

my $data = [ split(/[\n\r]/,$top_data) ];

# we consider only the first 2 rows

open (LOGFILE, '>> /var/log/htt/Memory_monitor.log'); 

print LOGFILE $date_time . $data->[0] . "\n";
print LOGFILE $date_time . $data->[1] . "\n";

if ( $data->[0] =~ /Mem\:\s+(\d+)k total\,\s+(\d+)k used/ )
{
  if ( $1 )
  {
    $percentage = int ( ( $2 * 100 ) / $1 ) ;

    print LOGFILE $date_time . "$1 , $2 , $percentage %\n";
  }
  else
  {
    print LOGFILE $date_time . "0 memory ???\n";
  }
}
else
{
  print LOGFILE $date_time . "cannot parse top data\n";
}

close LOGFILE;

print STDOUT "$percentage\n";

__END__

cd /home/ht/www/rgalli_acc_dev/web

/usr/bin/perl Ultra/Lib/Memory/monitor.pl

tail -f /var/log/htt/Memory_monitor.log

