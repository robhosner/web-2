<?php

namespace Ultra\Lib;

use Ultra\Container\AppContainer;

require_once 'classes/CommandInvocation.php';
require_once 'classes/Outcome.php';
require_once 'classes/PortInQueue.php';
require_once 'classes/RoamCreditPromo.php';

require_once 'db/ani_format.php';
require_once 'db/htt_card_calling_rates.php';
require_once 'db/htt_country_sms_carriers.php';
require_once 'db/htt_cancellation_reasons.php';
require_once 'db/htt_data_event_log.php';
require_once 'db/rbac/activity.php';
require_once 'db/rbac/role.php';
require_once 'db/rbac/api.php';
require_once 'db/ultra_session.php';
require_once 'db/ultra_acc/soap_log.php';
require_once 'db/ultra_acc/command_invocations.php';

require_once 'lib/inventory/functions_shipwire.php';
require_once 'lib/util-common.php';
require_once 'lib/portin/functions.php';
require_once 'lib/provisioning/functions.php';
require_once 'partner-face/provisioning-partners-include.php';

require_once 'fields.php';

require_once 'Ultra/Lib/Api/HTML.php';
require_once 'Ultra/Lib/Api/JSON.php';
require_once 'Ultra/Lib/Api/JSONP.php';
require_once 'Ultra/Lib/Api/MIME.php';
require_once 'Ultra/Lib/Api/PHP.php';
require_once 'Ultra/Lib/Api/QA.php';
require_once 'Ultra/Lib/Api/WIKI.php';
require_once 'Ultra/Lib/Api/WSDL.php';
require_once 'Ultra/Lib/Api/XML.php';

require_once 'Ultra/Lib/Api/PartnerBase.php';

require_once 'Ultra/Lib/Api/Partner/ApiPublic.php';
require_once 'Ultra/Lib/Api/Partner/ApiPublic/GetLocations.php';
require_once 'Ultra/Lib/Api/Partner/ApiPublic/ZIPCoverageInfo.php';
require_once 'Ultra/Lib/Api/Partner/ApiPublic/InternationalCallingRates.php';
require_once 'Ultra/Lib/Api/Partner/ApiPublic/SMSCountryList.php';

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/AddPlanCredits.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckCallCreditOptions.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckBalances.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckSession.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckDataRechargeByCustomerId.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckDealerPortalAvailable.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckVoiceRechargeByCustomerId.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CheckZipCode.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ClearCreditCard.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CreateBusiness.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CreateUser.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/CreateMiscellaneousAdjustment.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/DeleteMiscellaneousAdjustment.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetBoltOns.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetBusinessInfo.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetPlanCreditBalance.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetUserInfo.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListBusinessReports.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListMiscellaneousAdjustments.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListReportBusinesses.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListReportPeriods.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListIDDRates.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListSMSCountries.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListZIPCoverage.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/LoginUser.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/PlanCreditsCosts.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/RequestBusinessReport.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SearchBusinesses.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SearchUsers.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SendCredentials.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/TransferPlanCredits.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/UpdateBusiness.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/UpdateBusinessParent.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/UpdateMiscellaneousAdjustment.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/UpdateUser.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidateActivationsAvailable.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidateGBASim.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidateCelluphoneSession.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidatePortInEligibility.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidateSIM.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidateSIMInventory.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidateSIMNetwork.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/VerifyPortInStatus.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/VerifyProvisionNewCustomer.php';

require_once 'Ultra/Lib/Api/Partner/Dealerportal/ActivateDemoLine.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ApplyBalanceToStoredValue.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ApplyBoltOn.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ApplyCallCredit.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ApplyDataRecharge.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ApplyVoiceRecharge.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/AuthorizeCustomerAccess.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ChangePlanFuture.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ChangePlanImmediate.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ChangePlanSuspended.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ChargeAndSaveCustomerCC.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ChargeCustomerPINs.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ConfigureBoltOns.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/DashboardStatistics.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/DisableMarketingSettings.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetCustomerActions.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetDemoLineStatus.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetCustomerInfo.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetRechargeDetailByDealerMaster.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetRechargeDetailByPlanMaster.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetRechargeDetailDealer.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetRechargeSummaryDealer.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetRechargeSummaryMaster.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/GetTransactionHistory.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListCustomerICCID.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ListCustomerMSISDN.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ReplaceSIMCard.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ReportActivationActivity.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/RequestProvisionNewCustomer.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/RequestProvisionPortedCustomer.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/RequestProvisionPortedCustomerCancel.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/RequestProvisionPortedCustomerUpdate.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SearchActivationHistoryDealer.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SearchCustomersByDetails.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SearchCustomersByPlanDetails.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SearchCustomersExpiring.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SendCustomerPasswordSMS.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SetCustomerInfo.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/SetDataLimit.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/ValidatePIN.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/VerifyAndSaveCustomerCC.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/VerifyProvisionStatus.php';
require_once 'Ultra/Lib/Api/Partner/Dealerportal/VerifyRBAC.php';

require_once 'Ultra/Lib/Api/Partner/Exacaster/GetCustomerInfo.php';
require_once 'Ultra/Lib/Api/Partner/Exacaster/RedeemPromoCampaign.php';

require_once 'Ultra/Lib/Api/Partner/ExternalPayments.php';
require_once 'Ultra/Lib/Api/Partner/ExternalPayments/CheckMobileBalance.php';
require_once 'Ultra/Lib/Api/Partner/ExternalPayments/SubmitRealtimeReload.php';
require_once 'Ultra/Lib/Api/Partner/ExternalPayments/CancelRealtimeReload.php';
require_once 'Ultra/Lib/Api/Partner/Internal.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetEnvironmentInfo.php';
require_once 'Ultra/Lib/Api/Partner/Internal/MvneRenewPlan.php';

require_once 'Ultra/Lib/Api/Partner/Internal/AddIntakeEntry.php';
require_once 'Ultra/Lib/Api/Partner/Internal/AddPromoBroadcast.php';
require_once 'Ultra/Lib/Api/Partner/Internal/AddVertexOnlineOrder.php';
require_once 'Ultra/Lib/Api/Partner/Internal/AddWIFISoc.php';
require_once 'Ultra/Lib/Api/Partner/Internal/CancelDeviceLocation.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ChangeWholesaleService.php';
require_once 'Ultra/Lib/Api/Partner/Internal/Crone.php';
require_once 'Ultra/Lib/Api/Partner/Internal/DemoLineTools.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ForceExpireCustomerPlan.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetActCodeFromICCID.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetActionsByTransition.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetAllAbortedTransitions.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetAllMessagingTemplates.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetAllStuckTransitions.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetAPIErrorMessages.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetCancellationReasons.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetCustomerAggregatedSoapLog.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetCustomerSoapLog.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetE911SubscriberAddress.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetEnvironmentStage.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetIntakeData.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetMasterAgents.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetPromoBroadcastInfo.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetRolesActionsApis.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetState.php';
require_once 'Ultra/Lib/Api/Partner/Internal/GetIMEICustomerData.php';
require_once 'Ultra/Lib/Api/Partner/Internal/IsEligiblePortIn.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ListPromoBroadcast.php';
require_once 'Ultra/Lib/Api/Partner/Internal/LookUpDemoLine.php';
require_once 'Ultra/Lib/Api/Partner/Internal/NetworkRestore.php';
require_once 'Ultra/Lib/Api/Partner/Internal/NetworkSuspend.php';
require_once 'Ultra/Lib/Api/Partner/Internal/QueryStatus.php';
require_once 'Ultra/Lib/Api/Partner/Internal/QuerySubscriber.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ReconcileICCID.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ReconcileMessages.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ReconcileMSISDN.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RecoverAbortedTransition.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RecoverStuckTransition.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RecoverAllStuckTransitions.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RecoverEpay.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RecoverFailedTransition.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RedisRead.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RemoveWIFISoc.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RenamePromoBroadcast.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ResetUltraZeroMinutes.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RestoreAccount.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RestoreCancelledPortedCustomer.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ResurrectIccid.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RunCustomTransition.php';
require_once 'Ultra/Lib/Api/Partner/Internal/RunMonthlyCharge.php';
require_once 'Ultra/Lib/Api/Partner/Internal/SearchIccid.php';
require_once 'Ultra/Lib/Api/Partner/Internal/SendNotificationReceived.php';
require_once 'Ultra/Lib/Api/Partner/Internal/SetE911SubscriberAddress.php';
require_once 'Ultra/Lib/Api/Partner/Internal/SuspendAccount.php';
require_once 'Ultra/Lib/Api/Partner/Internal/TestOutput.php';
require_once 'Ultra/Lib/Api/Partner/Internal/TestValidation.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UnarchiveTransition.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UndoActivationLogOverride.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UpdateActivationLogOverride.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UpdateAPIErrorMessage.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UpdatePlanAndFeatures.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UpdatePromoBroadcast.php';
require_once 'Ultra/Lib/Api/Partner/Internal/UpdateThrottleSpeed.php';
require_once 'Ultra/Lib/Api/Partner/Internal/ValidateAccountMSISDN.php';

require_once 'Ultra/Lib/Api/Partner/InteractiveCare.php';

require_once 'Ultra/Lib/Api/Partner/Inventory.php';
require_once 'Ultra/Lib/Api/Partner/Inventory/GetPINRangeStatus.php';
require_once 'Ultra/Lib/Api/Partner/Inventory/GetSIMRangeInfo.php';
require_once 'Ultra/Lib/Api/Partner/Inventory/AssignPINRangeToHot.php';

require_once 'Ultra/Lib/Api/Partner/Messaging.php';

require_once 'Ultra/Lib/Api/Partner/Portal.php';
require_once 'Ultra/Lib/Api/Partner/Portal/AcceptTermsOfService.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ChangePlan.php';
require_once 'Ultra/Lib/Api/Partner/Portal/CheckCallCreditOptions.php';
require_once 'Ultra/Lib/Api/Partner/Portal/CheckBalances.php';
require_once 'Ultra/Lib/Api/Partner/Portal/CheckSession.php';
require_once 'Ultra/Lib/Api/Partner/Portal/CheckZipCode.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ClaimCreditCardByActCode.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ClearCreditCard.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ExtendSession.php';
require_once 'Ultra/Lib/Api/Partner/Portal/GetBoltOns.php';
require_once 'Ultra/Lib/Api/Partner/Portal/GetCustomerPreferences.php';
require_once 'Ultra/Lib/Api/Partner/Portal/GetTransactionHistory.php';
require_once 'Ultra/Lib/Api/Partner/Portal/GetIDDHistory.php';
require_once 'Ultra/Lib/Api/Partner/Portal/GetToken.php';
require_once 'Ultra/Lib/Api/Partner/Portal/KillSession.php';
require_once 'Ultra/Lib/Api/Partner/Portal/SendPassword.php';
require_once 'Ultra/Lib/Api/Partner/Portal/SetCustomerInfo.php';
require_once 'Ultra/Lib/Api/Partner/Portal/SetCustomerPreferences.php';
require_once 'Ultra/Lib/Api/Partner/Portal/SendToken.php';
require_once 'Ultra/Lib/Api/Partner/Portal/UnsubscribeFromEmail.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ValidatePIN.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ValidateSIMInventory.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ValidateSIMNetwork.php';
require_once 'Ultra/Lib/Api/Partner/Portal/VerifyAndSaveCreditCard.php';
require_once 'Ultra/Lib/Api/Partner/Portal/VerifyAndSaveCustomerCC.php';
require_once 'Ultra/Lib/Api/Partner/Portal/VerifyProvisionNewCustomer.php';

require_once 'Ultra/Lib/Api/Partner/Portal/ApplyBalanceToStoredValue.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ApplyBoltOn.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ApplyCallCredit.php';
require_once 'Ultra/Lib/Api/Partner/Portal/CalculateTaxesFees.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ChargeAndSaveCustomerCC.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ChargeCreditCard.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ChargeCustomerPINs.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ChargeCustomerProvidedCC.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ConfigureBoltOns.php';
require_once 'Ultra/Lib/Api/Partner/Portal/CustomerDetail.php';
require_once 'Ultra/Lib/Api/Partner/Portal/GetMarketingSettings.php';
require_once 'Ultra/Lib/Api/Partner/Portal/Login.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ReplaceSIMCard.php';
require_once 'Ultra/Lib/Api/Partner/Portal/RequestProvisionNewCustomer.php';
require_once 'Ultra/Lib/Api/Partner/Portal/RequestProvisionOrangePortedCustomer.php';
require_once 'Ultra/Lib/Api/Partner/Portal/RequestProvisionPortedCustomer.php';
require_once 'Ultra/Lib/Api/Partner/Portal/RequestProvisionPortedCustomerUpdate.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ResumeActivationSession.php';
require_once 'Ultra/Lib/Api/Partner/Portal/SetDataLimit.php';
require_once 'Ultra/Lib/Api/Partner/Portal/ValidateGBASim.php';
require_once 'Ultra/Lib/Api/Partner/Portal/VerifyPortInStatus.php';
require_once 'Ultra/Lib/Api/Partner/Portal/VerifyProvisionStatus.php';

require_once 'Ultra/Lib/Api/Partner/Rainmaker.php';

require_once 'Ultra/Lib/Api/Partner/Settings.php';
require_once 'Ultra/Lib/Api/Partner/Settings/GetAllSettings.php';
require_once 'Ultra/Lib/Api/Partner/Settings/TriggerSetting.php';

require_once 'Ultra/Lib/Api/Partner/MvneCare.php';
require_once 'Ultra/Lib/Api/Partner/MvneCare/ListCustomerInvocations.php';
require_once 'Ultra/Lib/Api/Partner/MvneCare/ListCustomerMakeitso.php';
require_once 'Ultra/Lib/Api/Partner/MvneCare/ListOpenInvocations.php';
require_once 'Ultra/Lib/Api/Partner/MvneCare/ListMakeitso.php';
require_once 'Ultra/Lib/Api/Partner/MvneCare/UpdateEscalation.php';

require_once 'Ultra/Lib/Api/Partner/Mvneinternal.php';
require_once 'Ultra/Lib/Api/Partner/Mvneinternal/HideMakeitso.php';
require_once 'Ultra/Lib/Api/Partner/Mvneinternal/NetworkUpdatePlanAndFeaturesRaw.php';
require_once 'Ultra/Lib/Api/Partner/Mvneinternal/RemoveBoltonData.php';
require_once 'Ultra/Lib/Api/Partner/Mvneinternal/SetIntlRoaming.php';
require_once 'Ultra/Lib/Api/Partner/Tweakker.php';

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/AddCourtesyCash.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/AddPlanCredits.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ApplyBalanceToStoredValue.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ApplyBoltOn.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ApplyCallCredit.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/CancelPortIn.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/CancelDeviceLocation.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/CheckBalances.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/CheckCallCreditOptions.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/CustomerDirectSMS.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/CustomerInfo.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/EligibleChangeNumber.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/FlexInfo.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/GetBillingHistory.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/GetBoltOns.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/GetBoltOnHistoryByCustomerId.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/GetCustomerCycleHistory.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/GetCustomerPreferences.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/FlushStoredBalance.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ForceJoinFamily.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/LoginForCustomer.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/PromoEnroll.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/RunFlexSetup.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/SearchDealers.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/SearchCustomers.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/SetCustomerInfo.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/SetCustomerCreditCard.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/SetCustomerPreferences.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ShowInternationalDirectDialErrors.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/VerifyInternationalDirectDialSettings.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ValidateGBASim.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ValidateSIMOrigin.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/ValidatePortInEligibility.php';
require_once 'Ultra/Lib/Api/Partner/CustomerCare/UnblockPortalLogin.php';

require_once 'Ultra/Lib/Api/Partner/Testbrandultra.php';
require_once 'Ultra/Lib/Api/Partner/Testbrandunivision.php';

require_once 'Ultra/Lib/Api/Partner/Mint.php';
require_once 'Ultra/Lib/Api/Partner/Univision.php';

require_once 'Ultra/Lib/Api/Partner/Ultrainfo.php';
require_once 'Ultra/Lib/Api/Partner/Ultrainfo/GetLocations.php';
require_once 'Ultra/Lib/Api/Partner/Ultrainfo/FindDealersByZip.php';

require_once 'Ultra/Lib/Api/Partner/Provisioning.php';
require_once 'Ultra/Lib/Api/Partner/Promotional.php';

require_once 'Ultra/Lib/Api/Partner/ProjectW.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/ImportFile.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/RetryChangeSIM.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/RetryPort.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/SetStatusOngoing.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/SetStatusPaused.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/SetStatusCompleted.php';
require_once 'Ultra/Lib/Api/Partner/ProjectW/ValidateFile.php';

require_once 'Ultra/Lib/Api/Partner/Publicprojw.php';
require_once 'Ultra/Lib/Api/Partner/Publicprojw/ExportErrors.php';
require_once 'Ultra/Lib/Api/Partner/Publicprojw/ExportFile.php';
require_once 'Ultra/Lib/Api/Partner/Publicprojw/GetFileStats.php';
require_once 'Ultra/Lib/Api/Partner/Publicprojw/GetTodaysFiles.php';
require_once 'Ultra/Lib/Api/Partner/Publicprojw/ExportCDR.php';

# require_once 'Ultra/Lib/Api/Partner/Test.php';
# require_once 'Ultra/Lib/Api/Partner/Test/Test.php';

require_once 'Ultra/Lib/DB/Setter/Customer.php';
require_once 'Ultra/Lib/DB/Customer.php';

require_once('Ultra/Lib/MiddleWare/Adapter/Control.php');

require_once('Ultra/Lib/BoltOn/functions.php');
require_once('Ultra/Lib/PromoCampaign/functions.php');

require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/Util/Redis/Celluphone.php';
require_once 'Ultra/Lib/Util/Redis/functions.php';
require_once 'Ultra/Lib/Util/Redis/Port.php';
require_once 'Ultra/Lib/Util/Redis/UltraSession.php';
require_once 'Ultra/Lib/Util/Validator.php';

/**
 * Main class for Ultra API v2.0
 *
 * https://issues.hometowntelecom.com:8443/browse/MVNO-1572
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Api
{
  public $result; // object of class \Result

  protected $partner;
  protected $requestId;
  protected $format;    // ( 'wsdl' , 'html' , 'wiki' , 'php' , 'json' , 'jsonp' , 'xml', 'mime' )
  protected $bath;      // ( 'soap' , 'rest' )
  protected $wsdl_file;
  protected $always;
  protected $version;
  protected $command;
  protected $meta;
  protected $mime;      // output MIME type
  protected $site;
  protected $e_config;
  protected $apiDefinition;
  protected $apiParameters;
  protected $metaDirectory = 'Ultra/Lib/Api/Meta';
  protected $apiNameSpace  = '\Ultra\Lib\Api';
  protected $jsonpCallback;
  protected $apiFormatObject;
  protected $error_code;
  protected $start; // exectution start time stamp
  protected $errorCodes; // cached error codes
  protected $language;

  const MAX_LOG_DATA_SET = 1024; // the max size of output data set to dlog

  /**
   * Class constructor
   */
  public function __construct ()
  {
    // initialize protected variables
    $this->start = microtime(TRUE);

    // ``I have a dream that every global will be wiped out``
    global $e_config;

    $this->e_config        = $e_config;

    $this->apiParameters = array();
    $this->result        = new \Result;

    // default format is 'json'
    if ( ! $this->format)
      $this->format    = (array_key_exists("format", $_REQUEST) && $_REQUEST['format'] !== '' ) ? $_REQUEST['format'] : 'json' ;
    // default bath is 'rest'
    $this->bath      = array_key_exists("bath",    $_REQUEST) ? $_REQUEST['bath']    : 'rest' ;
    $this->always    = array_key_exists("always",  $_REQUEST) ? $_REQUEST['always']  : '' ;
    $this->version   = array_key_exists("version", $_REQUEST) ? $_REQUEST['version'] : NULL;
    $this->meta      = array_key_exists("meta",    $_REQUEST) ? $_REQUEST['meta']    : NULL;
    $this->mime      = array_key_exists('mime',    $_REQUEST) ? $_REQUEST['mime']    : NULL;
    if ( ! $this->command)
      $this->command   = array_key_exists("command", $_REQUEST) ? $_REQUEST['command'] : NULL;
    if ( ! $this->partner)
      $this->partner   = array_key_exists("partner", $_REQUEST) ? $_REQUEST['partner'] : '' ;

    // shortcut: retrieve partner from command
    if ( !$this->partner && $this->command && preg_match("/^(\S+)__/", $this->command, $matches))
      $this->partner = $matches[1];

    $this->jsonpCallback = array_key_exists("callback", $_REQUEST) ? $_REQUEST['callback'] : NULL;
    $this->wsdl_file     = $this->metaDirectory."/".$this->partner.".wsdl";
    $this->site          = $_SERVER["SERVER_NAME"];

    $this->setRequestId();

    dlog('',"input = %s",$_REQUEST);

    // basic validations

    if ( ! $this->validateBath () )
      \logError('Bath validation failed.');

    if ( ! $this->validateSecurity () )
      \logError('Security validation failed.');

    if ( ! $this->validatePartner () )
      \logError('Partner validation failed.');

    if ( $this->partner && ! $this->loadAPIDefinition() )
      $this->result->add_error('ERR_API_INTERNAL: Could not load API definition');

    if ( $this->partner && ! $this->validateAPIDefinition () )
      \logError('API definition validation failed.');

    if ( ! $this->validateBrand())
    {
      $this->error_code = 'FA0004';
      $this->result->add_error('ERR_API_INTERNAL: The API is not available for this brand');
    }

    // set outout format object
    $this->setFormatObject ();

    // set default output language for errors
    $this->setOutputLanguage();
  }

  /**
   * setRequestId
   *
   * Set the global $request_id ( unique UUID for a single request ) which will appear at the beginning of each log entry
   *
   * @return NULL
   */
  protected function setRequestId()
  {
    global $request_id;

    $this->requestId = getNewUUID('UA','Partner PHPAPI');

    $request_id = $this->requestId;
  }

  /**
   * setOutputLanguage
   *
   * 'user_errors' are returned in the specified language if the translated message is available
   *
   * @return NULL
   */
  public function setOutputLanguage( $language=NULL )
  {
    $this->language = 'EN';

    if ( ! empty($language) )
    {
      if ( in_array( strtolower($language) , array('en','es','zh') ) )
        $this->language = strtoupper( $language );
      elseif( $language )
        dlog('',"Language %s not supported, using EN",$language);
    }
  }

  /**
   * Magic method to be used when an object of this class is treates as a string
   * http://php.net/manual/en/language.oop5.magic.php
   */
  public function __toString()
  {
    return
      "format    = ".$this->format."\n".
      "bath      = ".$this->bath."\n".
      "wsdl_file = ".$this->wsdl_file."\n".
      "partner   = ".$this->partner."\n".
      "always    = ".$this->always."\n".
      "version   = ".$this->version."\n".
      "command   = ".$this->command."\n".
      "meta      = ".$this->meta."\n".
      'mime      = '.$this->mime."\n".
      "requestId = ".$this->requestId."\n";
  }

  /**
   * validateAPIDefinition
   *
   * Validates API definition imported from JSON file
   * @return bool
   */
  protected function validateAPIDefinition ()
  {
    $validateAPIDefinition = TRUE;

    if ( ! $this->apiDefinition['commands'] )
    {
      $this->result->add_error( "ERR_API_INTERNAL: No commands found in API definition." );
      $validateAPIDefinition = FALSE;
    }

    return $validateAPIDefinition;
  }

  /**
   * validateVersion
   *
   * Validates version
   * @return bool
   */
  protected function validateVersion ()
  {
    $validateVersion = TRUE;

    if ( ! $this->version )
    {
      $this->result->add_error( "ERR_API_INTERNAL: No version provided." );
      $validateVersion = FALSE;
    }
    elseif ( ! ( $this->version && ( $this->version === $this->apiDefinition['meta']['version'] ) ) )
    {
      $this->result->add_error( "ERR_API_INTERNAL: Version prefix does not match." );
      $validateVersion = FALSE;
    }

    return $validateVersion;
  }

  /**
   * validatePartner
   *
   * Validates ``partner``
   * @return bool
   */
  protected function validatePartner ()
  {
    $validatePartner = TRUE;

    if ( ! $this->partner )
    {
      $this->error_code = "FA0005";
      $this->result->add_error("ERR_API_INVALID_ARGUMENTS: No partner specified");

      $validatePartner = FALSE;
    }
    else
    {
      // check allowed partners
      $allowedPartners = \Ultra\UltraConfig\getAllowedPartners();
      if ( !empty( $allowedPartners ) && ( !in_array( $this->partner, $allowedPartners ) || in_array( 'FORBIDDEN', $allowedPartners ) ) )
      {
        dlog('',"ERROR: Partner is not allowed - %s - %s - %s ",\getenv('HTT_CONFIGROOT'),$this->partner,$allowedPartners);

        $this->error_code = "FA0005";
        $this->result->add_error("ERR_API_INVALID_ARGUMENTS: Partner is not allowed");

        $validatePartner = FALSE;
      }
    }

    return $validatePartner;
  }


  /**
   * validateBrand
   * validate API partner brand against allowed environment brands
   * @return Boolean TRUE on successfull validation, FALSE otherwise
   */
  protected function validateBrand()
  {

    // allow if JSON brand definition is missing
    if (empty($this->apiDefinition['brands']))
      return TRUE;

    // check against the allowed environemnt brands
    foreach ($this->apiDefinition['brands'] as $brand)
      if (\Ultra\UltraConfig\isBrandNameAllowed($brand))
        return TRUE;

    // API partner brand is not allowed
    return FALSE;
  }


  /**
   * validateBath
   *
   * Checks for allowed API ``bath``
   * @return bool
   */
  protected function validateBath ()
  {
    $allowedBaths = array( 'soap' , 'rest' );

    return ( ! ! in_array( $this->bath , $allowedBaths ) );
  }

  /**
   * validateSecurity
   *
   * Checks for security configurations
   * @return bool
   */
  protected function validateSecurity ()
  {
    $validateSecurity = TRUE;

    // MVNO-2288: validate IP access
    if ( ! verify_request_source() )
    {
      dlog('',"Security forbids source IP");

      $this->result->add_error("ERR_API_ACCESS_DENIED: acccess denied by IP.");

      $validateSecurity = FALSE;
    }

    return $validateSecurity;
  }

  /**
   * output
   *
   * Sets output string
   *
   * @return NULL
   */
  protected function output ($output)
  {
    $this->result->data_array['output'] = $output;
  }

  /**
   * partnerClass
   *
   * Example: internal => \$product\Lib\Api\Partner\Internal
   *
   * @param string $partner
   * @return string
   */
  protected function partnerClass ()
  {
    $partnerClass = '';

    $partner = explode('__', $this->command);
    // command prefix
    $partner = $partner[0];
    $partnerClass = ucfirst( $partner );
    $partnerClass = $partnerClass == 'Customercare' ? 'CustomerCare' : $partnerClass;

    if ( class_exists( $this->apiNameSpace . '\Partner\\' . $partnerClass ) )
      $partnerClass = $this->apiNameSpace . '\Partner\\' . $partnerClass;
    else
    {
      if (strpos($this->partner, '_'))
      {
        $partnerClass = '';

        $partnerStringComponents = explode('_', $partner);

        foreach ( $partnerStringComponents as $component )
          $partnerClass .= ucfirst( strtolower($component) );

        $partnerClass = $this->apiNameSpace . '\Partner\\' . $partnerClass ;
      }
      else
        // partner is namespace
        $partnerClass = $this->apiNameSpace . '\Partner\\' . ucfirst( $this->partner );
    }

    return $partnerClass;
  }

  /**
   * commandClass
   *
   * Example: internal__ListPromoBroadcast => \$product\Lib\Api\Partner\Internal\ListPromoBroadcast
   *
   * @param string $partner_class
   * @param string $command
   * @return string
   */
  protected function commandClass ( $partner_class = NULL, $command = NULL )
  {
    if ( empty($command) || empty($partner_class) ) return '';

    $command_components = explode('__', $command);

    if ( empty($command_components[1]) || empty($command_components[0]) ) return '';

    $partner_components = explode('\\', $partner_class);
    $partner_class = $partner_components[(count($partner_components) - 1)];

    $path = $this->apiNameSpace . '\Partner\\' . $partner_class;
    if (class_exists($path . '\\' . $command_components[1]))
      $path = $path . '\\' . $command_components[1];

    return $path;
  }

  /**
   * apiExecute
   *
   * Executes the API
   * Implements a factory which instantiates \$product\Lib\Api\Partner\$PARTNERCLASS
   *
   * @return array
   */
  protected function apiExecute ()
  {
    $result = new \Result();

    $command = $this->command;

    // partner classes are under \$product\Lib\Api\Partner\
    $partnerClass = $this->partnerClass();

    dlog('',"executing command $command of API Partner ".$this->partner." from class $partnerClass");

    // is $partnerClass defined ?
    if (class_exists($partnerClass))
    {
      $commandClassPath = $this->commandClass($partnerClass, $command);
      $commandClass = NULL;

      if ($commandClassPath && class_exists($commandClassPath))
      {
        $commandClass = (new AppContainer())->make($commandClassPath);
        $commandClass->setUp($this->apiDefinition['commands'][$command], $this->requestId, $this->partner);
      }

      if (empty($commandClass))
      {
        \logError("Cannot instantiate class $partnerClass");
        $this->error_code = "IN0003";
        $result->add_error("ERR_API_INTERNAL: API definition error");
      }
      // Does $command belong to $commandClass ?
      elseif (is_callable([$commandClass, $command]))
      {
        dlog('',"apiParameters = %s", cleanse_credit_card_string(json_encode($this->apiParameters)));

        $commandClass->setInputValues($this->apiParameters);

        // invoke API method $command which belongs to the $commandClass object
        $result = $commandClass->$command();

        // set output language for errors
        $commandClass->setOutputLanguage($commandClass->getOutputLanguage());
      }
      else
      {
        \logError("Command $command is not a method of class $partnerClass");
        $this->error_code = "FA0006";
        $result->add_error("ERR_API_INTERNAL: command not defined in partner ".$this->partner);
      }
    }
    else
    {
      $this->error_code = "FA0007";
      $result->add_error("ERR_API_INTERNAL: partner ".$this->partner." not handled");
    }

    return $this->extractApiResult ( $result );
  }

  /**
   * extractApiResult
   *
   * @return array
   */
  protected function extractApiResult ( $result )
  {
    $arrayResult = $result->to_array();

    if ( isset( $arrayResult['data'] ) && is_array( $arrayResult['data'] ) )
      $arrayResult = array_merge( $arrayResult , $arrayResult['data'] );

    if ( $this->error_code )
    {
      $arrayResult['user_errors'] = array( $this->decodeToUserError( $this->error_code, $this->language ) );
      $arrayResult['error_codes'] = array( $this->error_code );
    }

    unset($arrayResult['data']);
    unset($arrayResult['log']);
    unset($arrayResult['pending']);
    unset($arrayResult['timeout']);

    return $arrayResult;
  }

  /**
   * setFormatObject
   *
   * Instantiates API Format object
   * Default format is JSON
   *
   * @return object
   */
  protected function setFormatObject ()
  {
    // API Format Factory
    $apiFormatClass = $this->apiNameSpace . '\\' . strtoupper($this->format);

    // Does $apiFormatClass exist ?
    if ( ! class_exists( $apiFormatClass ) )
    {
      $this->format = 'JSON'; # use JSON format as a default output format

      $apiFormatClass = '\Ultra\Lib\Api\\'.$this->format;

      $this->error_code = "FA0002";
    }

    // Instantiates API Format object
    $this->apiFormatObject = new $apiFormatClass(
      $this->site,
      $this->partner,
      $this->e_config,
      $this->apiDefinition,
      $this->jsonpCallback,
      $this->command,
      $this->metaDirectory
    );
  }

  /**
   * execute
   *
   * Execute and terminate
   *
   * @return NULL
   */
  public function execute ()
  {
    $logOutput = array();
    $logInput = array();

    try
    {
      if ( ! $this->validateVersion () )
      {
        $this->error_code = "FA0003";
        throw new \Exception("ERR_API_INTERNAL: Version validation failed.");
      }

      if ( $this->error_code == "FA0002" )
        throw new \Exception("ERR_API_INTERNAL: format not handled");

      if ( $this->apiFormatObject->isPresentation() )
      {
        // this is actually not an API call, but we want to display some info about the given partner APIs
        $this->output( $this->apiFormatObject->getPresentation() );
        $this->result->succeed();
      }
      elseif( $this->bath == 'soap' )
      {
        $this->format = 'xml';

        ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

        $server = new \SoapServer($this->wsdl_file);

        $server->setClass( $this->partnerClass () );

        dlog('', "handling SOAP call: site    = ".$this->site);
        dlog('', "handling SOAP call: partner = ".$this->partner);
        dlog('', "handling SOAP call: request = %s", json_encode($_REQUEST));
        if ( isset($HTTP_RAW_POST_DATA) ) { dlog('', cleanse_credit_card_string($HTTP_RAW_POST_DATA)); }

        dlog('', "invoking server handle");
        $server->handle();
        dlog('', "invoked server handle");
        exit;
      }
      else
      {
        // this is an actual API call we need to execute

        $apiExecutionResult = $this->initializeApiExecutionResult();

        if ( $this->command )
        {
          if ( $this->always === 'fail' )
          {
            // override failure
            $this->error_code = "AF0001";
            $apiExecutionResult['errors'][]      = "ERR_API_INTERNAL: always fail";
            $apiExecutionResult['user_errors'][] = $this->decodeToUserError( $this->error_code, $this->language );
            $apiExecutionResult['error_codes'][] = $this->error_code;
          }
          elseif ( $this->always === 'succeed' )
          {
            // override success

            $apiExecutionResult['warnings'][] = "ERR_API_INTERNAL: always_succeed";

            $apiExecutionResult['success'] = TRUE;
          }
          else
          {
            // Loads API input parameters from $_REQUEST into the $this->apiParameters array
            $logInput = $this->loadApiParameters();

            if ( $this->result->has_errors() )
            {
              $apiExecutionResult['errors'] = $this->result->get_errors();

              if ( is_array( $this->error_code ) )
              {
                foreach( $this->error_code as $error_code )
                  $apiExecutionResult['user_errors'][] = $this->decodeToUserError( $error_code, $this->language );

                $apiExecutionResult['error_codes'] = $this->error_code;
              }
              else
              {
                $apiExecutionResult['user_errors'][] = $this->decodeToUserError( $this->error_code, $this->language );
                $apiExecutionResult['error_codes'][] = $this->error_code;
              }
            }
            else
            {
              // Validates API input parameters
              $this->validateApiParameters();

              if ( $this->result->has_errors() )
              {
                $apiExecutionResult['errors'] = $this->result->get_errors();
                $apiExecutionResult['user_errors'] = $this->result->data_array['output']['user_errors'];
                $apiExecutionResult['error_codes'] = $this->result->data_array['output']['error_codes'];
              }
              else
                $apiExecutionResult = $this->apiExecute();
            }
          }
        }

        // default output fields
        $apiExecutionResult['ultra_trans_epoch'] = time();
        $apiExecutionResult['partner_tag']       = ( isset($_REQUEST['partner_tag']) ) ? preg_replace('/[^[:print:]]/', '', $_REQUEST['partner_tag']) : '0' ;

        // assign default values if not set
        if ( ! isset($apiExecutionResult['user_errors']) )
          $apiExecutionResult['user_errors'] = array();

        // assign default values if not set
        if ( ! isset($apiExecutionResult['error_codes']) )
          $apiExecutionResult['error_codes'] = array();

        if (isset($this->apiDefinition['commands'][$this->command]['disable']))
        {
          foreach ($this->apiDefinition['commands'][$this->command]['disable'] as $disable)
            unset($apiExecutionResult[$disable]);
        }

        // logged output and actual output will diverge if we want to truncate any ouput value
        if (isset($this->apiDefinition['commands'][$this->command]['truncate']))
        {
          foreach ($apiExecutionResult as $resultField => $resultValue)
          {
            if ( in_array( $resultField , $this->apiDefinition['commands'][$this->command]['truncate'] ) )
            {
              // truncate long array ?
              if ( is_array( $apiExecutionResult[ $resultField ] ) )
              {
                $encodedArray = json_encode( $apiExecutionResult[ $resultField ] );
                if ( strlen( $encodedArray ) > 16 )
                  $logOutput[ $resultField ] = substr( $encodedArray , 0 , 4 ) . '*TRUNCATED*';
                else
                  $logOutput[ $resultField ] = $resultValue;
              }
              // truncate long string ?
              elseif ( strlen( $apiExecutionResult[ $resultField ] ) > 16 )
                $logOutput[ $resultField ] = substr( $apiExecutionResult[ $resultField ] , 0 , 4 ) . '*TRUNCATED*';
              // don't truncate
              else
                $logOutput[ $resultField ] = $resultValue;
            }
            else
              $logOutput[ $resultField ] = $resultValue;
          }
        }

        if ($this->format == 'mime' && $this->mime)
          $this->output( $this->apiFormatObject->output( $apiExecutionResult, $this->command, $this->mime ) );
        else
          $this->output( $this->apiFormatObject->output( $apiExecutionResult ) );
      }
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      if ( is_a( $e, 'UltraException' ) && !empty( $e->get_error_code() ) )
        $this->error_code = $e->get_error_code();
      elseif ( is_a( $e, 'UserErrorException' ) && !empty( $e->getUserErrorCode() ) )
        $this->error_code = $e->getUserErrorCode();
      else
        $this->error_code = !empty( $this->error_code ) ? $this->error_code : 'IN0002';

      $apiExecutionResult = $this->defaultApiExecutionError ( $e->getMessage() );
      $this->result->add_error( $e->getMessage() );
      $this->output( $this->apiFormatObject->output( $apiExecutionResult ) );
    }

    $this->terminate( $logInput, $logOutput );
  }

  /**
   * defaultApiExecutionError
   *
   * Default API error result
   *
   * @return array
   */
  protected function defaultApiExecutionError ( $error_message )
  {
    $apiExecutionResult = $this->defaultApiExecutionResult();
    $apiExecutionResult['errors'][] = $error_message;
    $apiExecutionResult['error_codes'][] = $this->error_code;
    $apiExecutionResult['user_errors'][] = $this->decodeToUserError( $this->error_code, $this->language );

    return $apiExecutionResult;
  }

  /**
   * defaultApiExecutionResult
   *
   * Default API result
   *
   * @return array
   */
  protected function defaultApiExecutionResult ()
  {
    return array(
      'errors'            => array(),
      'warnings'          => array(),
      'success'           => 'false',
      'user_errors'       => array(),
      'error_codes'       => array()
    );
  }

  /**
   * initializeApiExecutionResult
   *
   * Initializes API result values
   *
   * @return array
   */
  protected function initializeApiExecutionResult ()
  {
    $apiExecutionResult = $this->defaultApiExecutionResult();

    if ( $this->command && isset($this->apiDefinition['commands']) && isset($this->apiDefinition['commands'][$this->command]) )
    {
      // add empty output for each return value
      foreach ($this->apiDefinition['commands'][$this->command]['returns'] as $paramDefinition)
        $apiExecutionResult[ $paramDefinition['name'] ] = [];
    }
    else
    {
      $this->error_code = 'FA0001';
      $apiExecutionResult[ 'errors' ][] = "ERR_API_INTERNAL: No command provided";
      $apiExecutionResult[ 'error_codes' ][] = $this->error_code;
      $apiExecutionResult[ 'user_errors' ][] = $this->decodeToUserError( $this->error_code, NULL );
    }

    $apiExecutionResult['success'] = false;

    return $apiExecutionResult;
  }

  /**
   * loadApiParameters
   *
   * Loads API parameters from $_REQUEST into the $this->apiParameters array
   *
   * @return array logInput
   */
  public function loadApiParameters ()
  {
    $logInput = array();

    if ( ! isset( $this->apiDefinition['commands'][$this->command] ) )
    {
      $this->error_code = "FA0006";
      $this->result->add_error( "ERR_API_INTERNAL: command not defined in partner ".$this->partner );
      return $logInput;
    }

    // loop through input parameters
    foreach ($this->apiDefinition['commands'][$this->command]['parameters'] as $paramDefinition)
    {
      $pname = $paramDefinition['name'];

      if ( $pname != 'partner_tag' )
      {
        if ( isset ( $_REQUEST[$pname] ) && ( $_REQUEST[$pname] != '' ) )
        {
          $value = $_REQUEST[$pname];

          // perform sanitization on paramter
          if ( isset( $paramDefinition['filters'] ) && is_array( $paramDefinition['filters'] ) )
            $value = $this->filterApiParameter( $value, $paramDefinition['filters'] );

          $this->apiParameters[$pname] = $value;

          // truncate input for logging
          if ( isset( $paramDefinition['truncate'] ) && $paramDefinition['truncate'] && strlen( (string)$value ) > 16 )
            $logInput[$pname] = substr( $value , 0 , 4 ) . '*TRUNCATED*';
          else
            $logInput[$pname] = $value;

          // hidden from logs
          if ( isset( $paramDefinition['hide_log'] ) && $paramDefinition['hide_log'] )
            $logInput[$pname] = '_';
        }
        else if ( $paramDefinition['required'] )
        {
          $this->error_code[] = \Ultra\Lib\Api\Errors\missingParamCode( $pname );
          $this->result->add_error( "API_ERR_PARAMETER: Missing ".$this->command." REST parameter $pname!" );
        }
        else // parameter not required and not provided
        {
          $this->apiParameters[$pname] = '';
          $logInput[$pname] = '';
        }
      }
    }

    return $logInput;
  }

  /**
   * filterApiParameter
   *
   * Returns a filtered value according to the rule provided
   *
   * @return string
   */
  public function filterApiParameter( $value, array $filtersArr )
  {
    if ( in_array( 'trimspaces', $filtersArr ) )
      $value = trim( $value );
    if ( in_array( 'lowercase', $filtersArr ) )
      $value = strtolower( $value );
    if ( in_array( 'email', $filtersArr ) )
      $value = filter_var( $value, FILTER_SANITIZE_EMAIL );
    if ( in_array( 'number_float', $filtersArr ) )
      $value = filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND | FILTER_FLAG_ALLOW_SCIENTIFIC );
    if ( in_array( 'number_int', $filtersArr ) )
      $value = filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
    if ( in_array( 'string', $filtersArr ) )
      $value = filter_var($value , FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_ENCODE_LOW | FILTER_FLAG_ENCODE_AMP );
    if ( in_array( 'url', $filtersArr ) )
      $value = filter_var( $value, FILTER_SANITIZE_URL );
    if ( in_array( 'special_chars', $filtersArr ) )
      $value = filter_var( $value, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_ENCODE_HIGH );

    return $value;
  }

  /**
   * aesEncrypted
   *
   * Returns true if the parameter is supposed to be encrypted
   *
   * @return boolean
   */
  public function aesEncrypted( $paramDefinition )
  {
    return ! ! ( ! empty( $paramDefinition['aes_encrypted'] ) && $paramDefinition['aes_encrypted'] );
  }

  /**
   * performAESDecryption
   *
   * Performs AES decryption for the given value
   *
   * @return string
   */
  public function performAESDecryption( $value )
  {
    $key = $this->getAESKey();

    $decryptedValue = '';

    try
    {
      $decryptedValue = \perform_aes_decryption( $value , $key );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      $decryptedValue = '';
    }

    // the presence of non ASCII characters is a hint of failed decryption
    if ( $decryptedValue && \has_non_ascii_characters( $decryptedValue ) )
      $decryptedValue = '';

    dlog('',"decryptedValue = %s",$decryptedValue);

    return $decryptedValue;
  }

  /**
   * getAESKey
   *
   * Returns the AES encryption key
   *
   * @return string
   */
  protected function getAESKey()
  {
    $command_components = explode('__', $this->command);

    return 'ULTRA' . $this->version . $command_components[1] ;
  }

  /**
   * aesDecrypt
   *
   * If "aes_encrypted" is true for the given parameter, attempts to perform decryption
   *
   * @return array
   */
  public function aesDecrypt( $paramDefinition )
  {
    $validationErrors     = array();
    $validationErrorCodes = array();
    $value                = ( empty($_REQUEST[ $paramDefinition['name'] ]) ? '' : $_REQUEST[ $paramDefinition['name'] ] );

    // should $value be decrypted?
    if ( ! empty( $value ) && $this->aesEncrypted( $paramDefinition ) )
    {
      dlog('',"about to attempt AES decryption for %s = %s",$paramDefinition['name'],$value);

      // decrypt $value
      $decryptedValue = $this->performAESDecryption( $value );

      if ( $decryptedValue )
      {
        $value = $decryptedValue;
      }
      else
      {
        $validationErrors[]     = 'Decryption error';
        $validationErrorCodes[] = 'DE0001';
        $value                  = '';
      }
    }

    return array( $validationErrors , $validationErrorCodes , $value );
  }

  /**
   * validateApiParameters
   *
   * Validates API input parameters, following the JSON definition directives
   *
   * @return NULL
   */
  public function validateApiParameters ()
  {
    $error_codes = array();
    $user_errors = array();

    teldata_change_db();

    // loop through input parameters
    foreach ( $this->apiDefinition['commands'][$this->command]['parameters'] as $paramDefinition )
    {
      if ( $paramDefinition['name'] != 'partner_tag' )
      {
        if ( isset( $_REQUEST[ $paramDefinition['name'] ] ) )
        {
          // perform decryption if needed for the given input parameter
          list( $validationErrors , $validationErrorCodes , $value ) = $this->aesDecrypt( $paramDefinition );

          if ( ! count($validationErrors) )
            // perform validation for the given input parameter
            list( $validationErrors , $validationErrorCodes ) =
              \Ultra\Lib\Util\Validator::validate( $paramDefinition['name'] , $paramDefinition['type'] , $paramDefinition['validation'] , $value );

          if ( count($validationErrors) )
            $this->result->add_errors( $validationErrors );

          // append errors
          $error_codes = array_merge( $error_codes , $validationErrorCodes );
          $user_errors = array_merge( $user_errors , $validationErrors     );
        }
      }
    }

    if ( count($error_codes) )
    {
      $count = count($user_errors);
      for ( $i=0 ; $i < $count ; $i++ )
        $user_errors[$i] = preg_replace("/^[^\:]+\:\s*/",'',$user_errors[$i]);

      $this->result->data_array['output']['error_codes'] = $error_codes;
      $this->result->data_array['output']['user_errors'] = $user_errors;
    }
  }

  /**
   * loadAPIDefinition
   *
   * Loads API definition from JSON files in directory $this->metaDirectory
   * This logic has been copied ``as-is`` from partner.php
   * Some variables have been renamed.
   *
   * @return boolean - TRUE in case of success
   */
  protected function loadAPIDefinition ()
  {
    $dir = $this->e_config['git/deploy']."/".$this->metaDirectory;

    // JSON definition file for $partner
    $file = "$dir/".$this->partner.".json";

    if ( ! file_exists($file) )
    {
      dlog('',"file $file does not exist");
      $this->error_code = 'FA0006';
      $this->result->add_error("ERR_API_INTERNAL: Partner definition does not exist");
      return NULL;
    }

    // read the JSON definition file
    $fileContents = file_get_contents($file);

    // decode JSON definition
    $this->apiDefinition = json_decode_nice($fileContents, true); # $p

    // validate JSON definition
    if ( ( NULL == $this->apiDefinition )
      || ( ! is_array($this->apiDefinition) )
      || ( ! isset($this->apiDefinition['commands']) )
      || ( ! is_array($this->apiDefinition['commands']) )
    )
    {
      \logDebug( json_encode( $this->apiDefinition ) );
      \logDebug( "Could not load partner definition from file $file" );
      $this->result->add_error( "ERR_API_INTERNAL: Could not load partner definition" );

      // assign an empty list of commands for the rest of the code execution
      $this->apiDefinition = array('commands' => array());
    }

    // JSON actually contains commands definitions
    if (isset($this->apiDefinition['commands']))
    {
      // we have to import all include files specified in $file
      foreach ($this->apiDefinition['commands'] as $include => $definition)
      {
        // it's an include file, not a command definition
        if (is_string($definition))
        {
          // include file
          $include_file = "$dir/$include";

          // read the include file
          $include_file_content = file_get_contents($include_file);

          // decode JSON include file
          $include_definition = json_decode_nice($include_file_content, true);

          if ( ( NULL == $include_definition )
            || ( ! is_array($include_definition) )
            || ( ! isset($include_definition['commands']) )
            || ( ! is_array($include_definition['commands']) )
          )
          {
            $this->result->add_error( "ERR_API_INTERNAL: Could not load partner include definition from $include_file" );
            $include_definition = array('commands' => array());
          }

          // loop through commands definitions
          foreach ($include_definition['commands'] as $include_command => $idef)
          {
            if ($definition === 'include' || preg_match($definition, $include_command))
            {
              if (isset($this->apiDefinition['commands'][$include_command]))
                $out['errors'][] = "Found included command $include_command more than once";
              else
                $this->apiDefinition['commands'][$include_command] = $idef;
            }
          }
          unset($this->apiDefinition['commands'][$include]);
        }
      }

      // loop through commands definitions
      foreach ($this->apiDefinition['commands'] as $include_command => $definition)
      {
        foreach (array("parameters", "returns") as $type)
        {
          $given = $definition[$type];
          $back = array();
          foreach ($given as $paramDefinition)
          {
            if (is_string($paramDefinition))
            {
              $include_file = "$dir/$paramDefinition";
              $include_file_content = file_get_contents($include_file);
              $include_definition = json_decode_nice($include_file_content, true);

              foreach ($include_definition as $parameter)
                $back[] = $parameter;
            }
            else
              $back[] = $paramDefinition;
          }
          $this->apiDefinition['commands'][$include_command][$type] = $back;
        }
      }
    }

    return $this->apiDefinition;
  }

  /**
   * terminate
   *
   * API result must be previously populated in either one of
   *  -  $this->result
   *  -  $this->result->data_array['output']
   *
   * Terminates API, echoes output
   */
  public function terminate( array $logInput=array(), array $logOutput=array() )
  {
    $user = ( isset( $_SERVER['PHP_AUTH_USER'] ) ) ? $_SERVER['PHP_AUTH_USER'] : '' ;

    // MVNO-2144: do not log if output data is too large
    $dataset = empty($this->result->data_array['output']) ? $this->result->to_array() : $this->result->data_array['output'];

    if ( ! empty($dataset['data']) && ( strlen(print_r($dataset['data'], TRUE)) > self::MAX_LOG_DATA_SET ) )
      $dataset['data'] = 'dataset is too large for logging';

    if ( ! empty( $logOutput ) )
      $dataset = $logOutput;
    if ( ! empty( $logInput ) )
    {
      $inputData =  $logInput;
      // replace logged REQUEST values with logInput values
      $requestData = $_REQUEST;
      foreach ( $logInput as $key=>$val )
        if ( isset( $requestData[$key] ) )
          $requestData[$key] = $val;
    }
    else
    {
      $inputData = $this->apiParameters;
      $requestData = $_REQUEST;
    }

    dlog('', "partner = %s, command = %s, request = %s, input = %s, output = %s, user = %s, runtime = %.2f (sec)",
      $this->partner, $this->command, $requestData, $inputData, $dataset, $user, microtime(TRUE) - $this->start);

    // MVNO-2630 - If no error_code, default to IN0002 (Generic API Error)
    if (!$this->error_code)
      $this->error_code = 'IN0002';

    if ( isset($this->result->data_array['output'] ) )
    {
      echo $this->result->data_array['output'];
    }
    elseif( $this->error_code )
    {
      $errors = $this->result->get_errors();
      $apiExecutionResult = $this->defaultApiExecutionError ( $errors[0] );

      if ($this->format == 'mime' && $this->mime)
          $this->output( $this->apiFormatObject->output( $apiExecutionResult, $this->command, $this->mime ) );
      else

          $this->output( $this->apiFormatObject->output( $apiExecutionResult ) );

      echo $this->result->data_array['output'];
    }
    else
    {
      dlog('', "Uncaught error made it to Api::terminate() with no error code, returning generic API error.");
      $apiExecutionResult = $this->defaultApiExecutionError ( 'Generic api error' );

      if ($this->format == 'mime' && $this->mime)
          $this->output( $this->apiFormatObject->output( $apiExecutionResult, $this->command, $this->mime ) );
      else
          $this->output( $this->apiFormatObject->output( $apiExecutionResult ) );

      echo $this->result->data_array['output'];
    }
  }

  /**
   * decodeToUserError
   *
   * Wrapper to call decodeToUserError function in api namespace
   *
   * @return string
   */
  function decodeToUserError( $error_code , $language='EN' , $redis=NULL )
  {
    if ( ! isset($this->errorCodes) )
      $this->errorCodes = $this->errorCodes( $language );

    $func = $this->apiNameSpace . '\Errors\decodeToUserError';
    return $func( $error_code, $language, $this->errorCodes, $redis );
  }

  /**
   * errorCodes
   *
   * Wrapper to call errorCodes function in api namespace
   *
   * @return array or object
   */
  function errorCodes($language='EN',$redis=NULL)
  {
    $func = $this->apiNameSpace . '\Errors\errorCodes';
    return $func( $language, $redis );
  }
}

