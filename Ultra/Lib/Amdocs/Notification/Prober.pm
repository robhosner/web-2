package Ultra::Lib::Amdocs::Notification::Prober;


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Miscellaneous;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::WSDL11;


use base qw(Ultra::Lib::Amdocs::Notification);


=head1 NAME

Ultra::Lib::Amdocs::Notification::Prober

=head1 Amdocs Notifications Handler

=head1 SYNOPSIS

  use Ultra::Lib::Amdocs::Notification::Prober;

  my $notificationProber = Ultra::Lib::Amdocs::Notification::Prober->new();

  print STDOUT $notificationProber->probeNotification( $command );

=cut


=head4 _initialize

  Specific initialization on object instantiation.

=cut
sub _initialize
{
  my ($this, $data) = @_;

  $this->SUPER::_initialize($data);

  # this has to be executed after the superclass instantiates Ultra::Lib::Util::Config
  $this->{ WSDL } = XML::Compile::WSDL11->new( $data->{ WSDL_FILE } );
}


=head4 probeNotification

  Main method to test an inbound SOAP notification

=cut
sub probeNotification
{
  my ($this,$notification,$probeParams) = @_;

  my $return =
  {
    'outcome'      => '',
    'soapRequest'  => '',
    'soapResponse' => '',
  };

  my $notificationMapper =
  {
    'ThrottlingAlert'      => sub { return $this->notificationThrottlingAlert( @_ );      },
    'PortOutDeactivation'  => sub { return $this->notificationPortOutDeactivation( @_ );  },
    'NotificationReceived' => sub { return $this->notificationNotificationReceived( @_ ); },
    'PortOutRequest'       => sub { return $this->notificationPortOutRequest( @_ );       },
    'ProvisioningStatus'   => sub { return $this->notificationProvisioningStatus( @_ );   },
  };

  if ( defined $notificationMapper->{ $notification } )
  {
    print STDERR "probeParams = ".Dumper($probeParams);

    my $soapCallResult = $notificationMapper->{ $notification }->( $notification , $probeParams );

    if ( $soapCallResult->{ error } )
    {
      $return->{ 'outcome' } = $soapCallResult->{ error };
    }
    else
    {
      # returns SOAP request
      $return->{ 'soapRequest' } = $this->prettyPrintXML( $soapCallResult->{ trace }->request()->content() );

      # returns SOAP response
      $return->{ 'soapResponse' } = $this->prettyPrintXML( $soapCallResult->{ trace }->response()->content() );

      $return->{ 'outcome' } = $soapCallResult->{ trace }->response()->status_line();
    }
  }
  else
  {
    $return->{ 'outcome' } = "Notification prober not implemented for '$notification'\n";
  }

  return $return;
}


=head4 notificationUserAgent

  Returns the LWP::UserAgent object user for SOAP calls.

=cut
sub notificationUserAgent
{
  my ($this,$notification) = @_;

  my $ua = LWP::UserAgent->new;
  $ua->timeout(600);
  $ua->proxy('http', $this->{ WSDL }->endPoint );
  $ua->default_header('soapAction' => $notification);

  return $ua;
}


=head4 notificationCall

  Performs a notification SOAP call.

=cut
sub notificationCall
{
  my ($this,$notification,$params) = @_;

  print STDERR "notificationCall = $notification\n";

  my $result = { error => '', trace => undef, };

  my $ua = $this->notificationUserAgent( $notification );

  my $transport = XML::Compile::Transport::SOAPHTTP->new( timeout => 600 , address => $this->{ WSDL }->endPoint , user_agent => $ua );

  my $call = $this->{ WSDL }->compileClient( $notification , transport => $transport , action => $notification , operation => $notification );

  my ($answer, $trace);

  eval
  {
    # we won't really need $answer in this module
    ($answer, $trace) = $call->( $params );
  };

  if ( $@ )
  {
    $result->{ error } = $@;
  }
  elsif ( ! $trace )
  {
    $result->{ error } = "No trace available after $notification SOAP call";
  }
  else
  {
    $result->{ trace } = $trace;
  }

  return $result;
}


=head4 notificationThrottlingAlert, notificationPortOutDeactivation, notificationNotificationReceived, notificationPortOutRequest, notificationProvisioningStatus

  Methods to test specific inbound SOAP notifications,
  they return SOAP::SOM objects.

=cut

sub notificationThrottlingAlert
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = { 'UserData' => { 'senderId' => 'ULTRA' , 'timeStamp' => $timeStamp } , 'Originator' => 'Prober' };
  }

  return $this->notificationCall($notification,$params);
}


sub notificationPortOutDeactivation
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = { 'UserData' => { 'senderId' => 'ULTRA' , 'timeStamp' => $timeStamp } };
  }

  return $this->notificationCall($notification,$params);
}


sub notificationNotificationReceived
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = { 'UserData' => { 'senderId' => 'ULTRA' , 'timeStamp' => $timeStamp } };
  }

  return $this->notificationCall($notification,$params);
}


sub notificationPortOutRequest
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = { 'UserData' => { 'senderId' => 'ULTRA' , 'timeStamp' => $timeStamp } };
  }

  return $this->notificationCall($notification,$params);
}


sub notificationProvisioningStatus
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = { 'UserData' => { 'senderId' => 'ULTRA' , 'timeStamp' => $timeStamp } };
  }

  return $this->notificationCall($notification,$params);
}


1;


__END__


