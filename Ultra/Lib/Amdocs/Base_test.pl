use strict;
use warnings;

use Data::Dumper;
use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;
use JSON::XS;
use File::Basename;
use FindBin;
use Log::Report mode => 'VERBOSE';
use LWP::Protocol::https;
use LWP::UserAgent;
use Time::HiRes qw( time sleep );
use lib "$FindBin::Bin/";
use Ultra::Lib::Amdocs::Base;

my $wsdl = XML::Compile::WSDL11->new("/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20130913/MVNOWSAPIService.wsdl");
$wsdl->compileCalls;
my $configuration = {
  'QueryMSISDN' => undef,
};
my $display       = 1;

$b = Ultra::Lib::Amdocs::Base->new();

my $x = $b->extractConfiguration( $wsdl , $configuration , $display );

print "\n\n\n\n";

print Dumper($x);

__END__

