use strict;
use warnings;
use JSON::XS;
use Ultra::Lib::Amdocs::Notification;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::MQ::EndPoint;
use Ultra::Lib::Util::Config;
use Data::Dumper;

my $json_coder  = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();
my $config      = Ultra::Lib::Util::Config->new();

my $mq_endpoint = Ultra::Lib::MQ::EndPoint->new(
  CONFIG     => $config,
  JSON_CODER => $json_coder
);

my $db = Ultra::Lib::DB::MSSQL->new(
  DB_NAME => 'ULTRA_ACC',
  DB_HOST => 'sandy.hometowntelecom.com',
  CONFIG  => $config
);

my $operation = 'test';
my $action_uuid = 'test'.time;
my $msisdn = '1001001000';

my $amdocsNotificationObject = Ultra::Lib::Amdocs::Notification->new(
  DB          => $db,
  operation   => $operation,
  mq_endpoint => $mq_endpoint,
  COMMAND     => $operation,
  JSON_CODER  => $json_coder,
  CONFIG      => $mq_endpoint->{ CONFIG },
  ACTION_UUID => $action_uuid,
  SESSION_ID  => $action_uuid,
  MSISDN      => $msisdn,
  ICCID       => undef,
  TAG         => $action_uuid,
);

# test failure

my $outcome = $amdocsNotificationObject->callbackOperation( 'dummy' , {} );

print Dumper( $outcome );

# test ignored

my $params =
{
  'UserData'              => { 'senderId' => 'ACC' , 'timeStamp' => '2015-05-14T00:54:13' },
  'MSISDN'                => '1001001000',
  'TransactionIDAnswered' => 'ATR/86-ULTRA-16600549910',
  'ProvisioningType'      => 'AutoRenew',
  'ProvisioningStatus'    => 'true',
  'StatusDescription'     => undef,
  'CorrelationID'         => undef,
  'PortErrorCode'         => undef,
  'PortErrorDesc'         => undef,
};

$outcome = $amdocsNotificationObject->callbackOperation( 'ProvisioningStatus' , $params );

print Dumper( $outcome );

# test success

$params =
{
  'UserData'              => { 'senderId' => 'ACC' , 'timeStamp' => '2015-05-14T00:54:13' },
  'MSISDN'                => '1001001001',
  'TransactionIDAnswered' => 'ULTRA-16600549910',
  'ProvisioningType'      => 'AutoRenew',
  'ProvisioningStatus'    => 'true',
  'StatusDescription'     => undef,
  'CorrelationID'         => undef,
  'PortErrorCode'         => undef,
  'PortErrorDesc'         => undef,
};

$outcome = $amdocsNotificationObject->callbackOperation( 'ProvisioningStatus' , $params );

print Dumper( $outcome );

exit;

my $result = $amdocsNotificationObject->operationPortOutDeactivation(
  'PortOutDeactivation',
  {
    MSISDN             => '1001001000',
    portOutCarrierID   => 'a',
    portOutCarrierName => 'b',
  }
);

__END__

Test for Ultra::Lib::Amdocs::Notification

Example:

su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev perl Ultra/Lib/Amdocs/Notification_test.pl'

