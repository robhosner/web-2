<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Soap/SendSMSPayload.php'; // Generate SendSMS payload
require_once 'Ultra/Lib/Util/Soap/AmdocsSoapClient.php';
require_once 'Ultra/Lib/Amdocs/Control/ControlSendSMS.php';

$control  = new \Ultra\Lib\Amdocs\Control\ControlSendSMS();
$response = $control->call(array(
  'MSISDN'    => $argv[1],
  'language'  => 'en',
  'smsText'   => $argv[2]
));

print_r( $response );

