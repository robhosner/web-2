<?php

namespace Ultra\Lib\Amdocs\Control;

require_once 'Ultra/Lib/Util/Soap/SendSMSPayload.php';  // SendSMS payload generator
require_once 'Ultra/Lib/Util/Soap/SendSMSResponse.php'; // SendSMSResponse class
require_once 'Ultra/Lib/Util/Soap/AmdocsSoapClient.php';

class ControlSendSMS
{
  protected $client;

  /**
   * Class constructor
   *
   * Initializes members with default values
   */
  public function __construct( $actionUUID=NULL )
  {
    $this->client = new \AmdocsSoapClient( FALSE , $actionUUID );
  }

  /**
   * Handles generating the proper payload for the
   * SOAP call, verifying required params are supplied
   * and dispatching the call.
   *
   * @param class $parameters
   * @return \Result
   * @author bwalters@ultra.me
   */
  public function call($parameters)
  {
    try
    {
      if (empty($parameters['MSISDN']))   return make_error_Result("Missing MSISDN");
      if (empty($parameters['language'])) return make_error_Result("Missing language");
      if (empty($parameters['smsText']))  return make_error_Result("Missing smsText");

      $sms = new \SendSMSPayload($parameters['MSISDN'], $parameters['language'], $parameters['smsText']);
      $payload = $sms->getPayload();

      dlog('', "payload  = %s", $payload);

      $response = $this->client->call('SendSMS', $payload);

      // disabled due to the following error message: json_encode(): type is unsupported, encoded as null
      #dlog('',"client = %s",$this->client);

      $result = $this->verifySendSMSResponse($response);
    }
    catch( \Exception $e )
    {
      dlog('', "%s", $e->getMessage());

      $result = make_error_Result( $e->getMessage() );
    }

    return $result;
  }

  /**
   * Verifies response from the SendSMS command.
   *
   * @param SendSMSResponse $response
   * @return \Result
   * @author bwalters@ultra.me
   */
  private function verifySendSMSResponse($response)
  {
    if ( ! empty($response->isTimeout) && $response->isTimeout )
      return make_timeout_Result('Timeout');

    try
    {
      if (!isset($response->SendSMSResult))        throw new \Exception('SendSMSResult is invalid');
      if (!isset($response->ResultCode))           throw new \Exception('ResultCode is invalid');
      if (!isset($response->ResultMsg))            throw new \Exception('ResultMsg is invalid');
      if (!isset($response->serviceTransactionId)) throw new \Exception('serviceTransactionId is invalid');

      dlog('',"SENDSMSGRESULT       = %s", $response->SendSMSResult);
      dlog('',"RESULTCODE           = %s", $response->ResultCode);
      dlog('',"RESULTMSG            = %s", $response->ResultMsg);
      dlog('',"SERVICETRANSACTIONID = %s", $response->serviceTransactionId);

      $result = make_ok_Result(
        array(
          'SendSMSResult'        => $response->SendSMSResult,
          'ResultCode'           => $response->ResultCode,
          'ResultMsg'            => $response->ResultMsg,
          'serviceTransactionId' => $response->serviceTransactionId
        )
      );
    }
    catch ( \Exception $e )
    {
      dlog('',"%s",$e->getMessage());

      $result = make_error_Result( $e->getMessage() );
    }

    return $result;
  }
}

