<?php

require_once 'classes/PHPUnitBase.php';
require_once 'Ultra/Lib/DB/DealerPortal.php';

class DealerPortalTest extends PHPUnitBase
{
  public function test__verifyUser()
  {
    // successful test: MVNO
    $info = \Ultra\Lib\DB\DealerPortal\validateUser('dschofield', 'dave123');
    $this->assertTrue(is_object($info));
    $this->assertEquals('NVO', $info->BUSINESS_TYPE);
    # print_r($info);

    // successful test: SubDistributor
    $info = \Ultra\Lib\DB\DealerPortal\validateUser('10JoSam', 'jqtevwxb');
    $this->assertTrue(is_object($info));
    $this->assertEquals('SubDistributor', $info->BUSINESS_TYPE);
    # print_r($info);

    // successful test: Master
    $info = \Ultra\Lib\DB\DealerPortal\validateUser('UVMaster', '123456');
    $this->assertTrue(is_object($info));
    $this->assertEquals('Master', $info->BUSINESS_TYPE);
    # print_r($info);

    // successful test: Dealer
    $info = \Ultra\Lib\DB\DealerPortal\validateUser('UNEDEMO2', '5fiqgqvf');
    $this->assertTrue(is_object($info));
    $this->assertEquals('Dealer', $info->BUSINESS_TYPE);
    # print_r($info);
  }

  public function test__createUser()
  {

    // bad role
    $result = \Ultra\Lib\DB\DealerPortal\createUser('Supervisor', 0, 'cat', 'fluffy', 0, 'Pussy', 'Cat', 'cat@feline.org', '5555555555', time(), time() + 1000, 'Pretty Girl');
    # echo "$result \n";
    $this->assertTrue(is_string($result));

    // invalid userID
    $result = \Ultra\Lib\DB\DealerPortal\createUser('Dealer Owner', 0, 'cat', 'fluffy', 0, 'Pussy', 'Cat', 'cat@feline.org', '5555555555', time(), time() + 1000, 'Pretty Girl');
    # echo "$result \n";
    $this->assertTrue(is_string($result));

    // existing username
    $result = \Ultra\Lib\DB\DealerPortal\createUser('Dealer Owner', 25, 'MABC0002', 'fluffy', 0, 'Pussy', 'Cat', 'cat@feline.org', '5555555555', time(), time() + 100000, 'Pretty Girl');
    # echo "$result \n";
    $this->assertTrue(is_string($result));

    // successful with all parameters as MVNO Owner (user ID 25)
    $result = \Ultra\Lib\DB\DealerPortal\createUser(25, 'Manager', 'cat2', 'fluffy', 0, 'Pussy', 'Cat', 'cat@feline.org', '5555555555', time(), time() + 60 * 60 * 24, 'Fluffy Pussy Cat');
    # echo "$result \n";
    $this->assertTrue(is_integer($result));

    // successful without optional parameters as Dealer Manager (user ID 1149)
   $result = \Ultra\Lib\DB\DealerPortal\createUser(1149, 'User', 'cat4', 'fluffy', 0, 'Pussy', 'Cat', 'cat@feline.org', '5555555555', time(), NULL, NULL);
    echo "$result \n";
    $this->assertTrue(is_integer($result));
  }

  public function test__getUsers()
  {
    // orglevelid = 1
    $result = \Ultra\Lib\DB\DealerPortal\getUsers(15266);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue((count($result->data_array) > 0));

    // orglevelid = 2
    $result = \Ultra\Lib\DB\DealerPortal\getUsers(1093);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue((count($result->data_array) > 0));

    // orglevelid = 3
    $result = \Ultra\Lib\DB\DealerPortal\getUsers(9062);
    //print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue((count($result->data_array) > 0));

    // orglevelid = 4
    $result = \Ultra\Lib\DB\DealerPortal\getUsers(39, FALSE);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue((count($result->data_array) > 0));
  }

  // getBusinessRecord
  public function test__getBusinessRecord()
  {
    // invalid business type
    list($business, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord('University', 'MVNO');
    # print_r($business); print_r($error);
    $this->assertEmpty($business);
    $this->assertNotEmpty($error);

    // invalid business code
    list($business, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord('Dealer', 'MVNO');
    # print_r($business); print_r($error);
    $this->assertEmpty($business);
    $this->assertNotEmpty($error);

    // valid dealer
    list($business, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord('Dealer', 'SME123');
    print_r($business); print_r($error);
    $this->assertNotEmpty($business);
    $this->assertEmpty($error);

    // valid master
    list($business, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord('Master', 'SUB7225');
    print_r($business); print_r($error);
    $this->assertNotEmpty($business);
    $this->assertEmpty($error);

    // valid MVNO
    list($business, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord('MVNO', 'UVNVO');
    print_r($business); print_r($error);
    $this->assertNotEmpty($business);
    $this->assertEmpty($error);
  }

  public function test__getUserAccess()
  {
    \Ultra\Lib\DB\dealer_portal_connect();

    $user_id = 10;
    $result = \Ultra\Lib\DB\DealerPortal\getUserAccess($user_id);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertEquals('Owner', $result->data_array['access_role']);

    $user_id = 19;
    $result = \Ultra\Lib\DB\DealerPortal\getUserAccess($user_id);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertEquals('Manager', $result->data_array['access_role']);

    $user_id = 23;
    $result = \Ultra\Lib\DB\DealerPortal\getUserAccess($user_id);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertEquals('User', $result->data_array['access_role']);

    // unknown user_id
    $user_id = 999999;
    $result = \Ultra\Lib\DB\DealerPortal\getUserAccess($user_id);
    print_r($result);
    $this->assertTrue( ! $result->is_success());
  }

  // renamed (.'Info') to not conflict when using --filter
  public function test__getBusinessInfo()
  {
    // Master / Owner / get Master
    $params = array('user_id' => 10, 'business_type' => 'Master', 'business_code' => 'AAMast');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());

    // Master / Owner / get Dealer
    $params = array('user_id' => 10, 'business_type' => 'Dealer', 'business_code' => 'NEW2976');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());

    // NVO / Owner / get Master
    $params = array('user_id' => 9, 'business_type' => 'Master', 'business_code' => 'AAMast');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());

    // NVO / Owner / get Dealer
    $params = array('user_id' => 9, 'business_type' => 'Dealer', 'business_code' => 'NEW2976');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());

    // Dealer / Manager / get Master / does not have permission
    $params = array('user_id' => 19, 'business_type' => 'Master', 'business_code' => 'AAMast');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue( ! $result->is_success());

    // Dealer / Manager / get Dealer
    $params = array('user_id' => 19, 'business_type' => 'Dealer', 'business_code' => 'UNEDEMO1');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());

    // Dealer / User / get Dealer
    $params = array('user_id' => 23, 'business_type' => 'Dealer', 'business_code' => 'UNEDEMO1');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());

    // unknown user_id
    $params['user_id'] = 999999;
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue( ! $result->is_success());
  }

  public function test__getBusinesses()
  {
    // Master / Owner / get Master , cannot
    $params     = array('user_id' => 10, 'business_type' => 'Master');
    $onlyActive = FALSE;
    $result = \Ultra\Lib\DB\DealerPortal\getBusinesses($params, $onlyActive);
    print_r($result);
    $this->assertTrue( ! $result->is_success());

    // Master / Owner / get Dealer
    $params     = array('user_id' => 10, 'business_type' => 'Dealer');
    $onlyActive = FALSE;
    $result = \Ultra\Lib\DB\DealerPortal\getBusinesses($params, $onlyActive);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue(count($result->data_array) > 0);

    // Dealer / Manager / get Dealer
    $params = array('user_id' => 19, 'business_type' => 'Dealer');
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue(count($result->data_array) > 0);

    // NVO / Owner / get Master
    $params = array('user_id' => 9, 'business_type' => 'Master', 'business_name' => 'island');
    $result = \Ultra\Lib\DB\DealerPortal\getBusinesses($params);
    print_r($result);
    $this->assertTrue($result->is_success());
    $this->assertTrue(count($result->data_array) > 0);

    // unknown user_id
    $params['user_id'] = 999999;
    $result = \Ultra\Lib\DB\DealerPortal\getBusiness($params);
    print_r($result);
    $this->assertTrue( ! $result->is_success());
  }
  
  public function test__requestBusinessReport()
  {

   // invalid business code
    $params = array(
      'business_type'   => 'Master',
      'business_code'   => 'ZZZ',
      'request_type'    => 1,
      'user_id'         => 26);
    $error =  \Ultra\Lib\DB\DealerPortal\requestBusinessReport($params);
    print_r($error);
    $this->assertNotEmpty($error);

    // invalid user ID
    $params = array(
      'business_type'   => 'Master',
      'business_code'   => 'Dis0001',
      'request_type'    => 1,
      'user_id'         => 10000);
    $error =  \Ultra\Lib\DB\DealerPortal\requestBusinessReport($params);
    print_r($error);
    $this->assertNotEmpty($error);

    // successful test
    $params = array(
      'business_type'   => 'Master',
      'business_code'   => 'Dis0001',
      'request_type'    => 1,
      'user_id'         => 1000);
    $error =  \Ultra\Lib\DB\DealerPortal\requestBusinessReport($params);
    print_r($error);
    $this->assertNotEmpty($error);
  }

  public function test__listMiscellaneousAdjustments()
  {
    $params = array(
      'business_type' => 'Master',
      'business_code' => 'Master08',
      'period_id'     => 76
    );
    $error =  \Ultra\Lib\DB\DealerPortal\listMiscellaneousAdjustments($params);
    print_r($error);
    $this->assertNotEmpty($error);

    $params = array(
      'business_type' => 'Master',
      'business_code' => 'Master08',
      'period_id'     => 79
    );
    $error =  \Ultra\Lib\DB\DealerPortal\listMiscellaneousAdjustments($params);
    print_r($error);
    $this->assertNotEmpty($error);

    $params = array(
      'business_type' => 'Master',
      'business_code' => 'Master08',
      'period_id'     => 81
    );
    $error =  \Ultra\Lib\DB\DealerPortal\listMiscellaneousAdjustments($params);
    print_r($error);
    $this->assertNotEmpty($error);
  }

  public function test__getProductDescription()
  {
    // unknown flags
    list($description, $error) = \Ultra\Lib\DB\DealerPortal\getProductDescription(24);
    print_r($description);
    $this->assertEmpty($error);
    $this->assertEmpty($description);

    // purple and orange SIMs
    list($description, $error) = \Ultra\Lib\DB\DealerPortal\getProductDescription(6);
    $this->assertEmpty($error);
    $this->assertNotEmpty($description);
    print_r($description);
  }

  public function test_authorizeHierarchyAccess()
  {
    // connect to database
    $dbc = \Ultra\Lib\DB\dealer_portal_connect();
    $this->assertNotEmpty($dbc);

    // test MVNO
    $params = array('user_id' => 9062, 'business_type' => 'Dealer', 'business_code' => 'CCity');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertTrue($access);
    $this->assertEmpty($error);

    // test Master requesting MVNO access
    $params = array('user_id' => 15370, 'business_type' => 'MVNO', 'business_code' => 'UVNVO');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertFalse($access);
    $this->assertNotEmpty($error);

    // test Master requesting another Master access
    $params = array('user_id' => 15370, 'business_type' => 'Master', 'business_code' => 'DEJul27');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertFalse($access);
    $this->assertNotEmpty($error);

    // test Master requesting own SubDistributor access
    $params = array('user_id' => 26, 'business_type' => 'Master', 'business_code' => 'CCal');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertTrue($access);
    $this->assertEmpty($error);

    // test Master requesting another Master's Dealer access
    $params = array('user_id' => 26, 'business_type' => 'Dealer', 'business_code' => 'CCONEX3');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertFalse($access);
    $this->assertNotEmpty($error);

    // test Master requesting own Dealer access
    $params = array('user_id' => 26, 'business_type' => 'Dealer', 'business_code' => 'DEA4');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertTrue($access);
    $this->assertEmpty($error);

    // test Dealer requesting MVNO access
    $params = array('user_id' => 15353, 'business_type' => 'MVNO', 'business_code' => 'UVNVO');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertFalse($access);
    $this->assertNotEmpty($error);

    // test Dealer requesting Master access
    $params = array('user_id' => 15353, 'business_type' => 'Master', 'business_code' => 'CCal');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertFalse($access);
    $this->assertNotEmpty($error);

    // test Dealer requesting another Dealer access
    $params = array('user_id' => 15353, 'business_type' => 'Dealer', 'business_code' => 'DEA4');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertFalse($access);
    $this->assertNotEmpty($error);

    // test Dealer requesting access to self
    $params = array('user_id' => 15353, 'business_type' => 'Dealer', 'business_code' => 'UNE10032');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertTrue($access);
    $this->assertEmpty($error);

    // test Dealer requesting access child dealer
    $params = array('user_id' => 15353, 'business_type' => 'Dealer', 'business_code' => 'TXGREEN1');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertTrue($access);
    $this->assertEmpty($error);

    // test Master requesting access to self
    $params = array('user_id' => 15370, 'business_type' => 'Master', 'business_code' => 'NEW3308');
    list($access, $error) = \Ultra\Lib\DB\DealerPortal\authorizeHierarchyAccess($params, $dbc);
    echo 'access: ' . ($access ? 'Y' : 'N') . ", error: $error";
    $this->assertTrue($access);
    $this->assertEmpty($error);
  }

}

