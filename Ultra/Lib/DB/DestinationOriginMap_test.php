<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/DestinationOriginMap.php';

teldata_change_db();

$customer_id = 10883;

$query    = make_find_ultra_customer_query_from_customer_id($customer_id);
$customer = find_customer( $query );

if ($customer)
{
  // insert into DESTINATION_ORIGIN_MAP
  $check = \Ultra\Lib\DB\DestinationOriginMap\insertAfterActivation($customer);

  if ( $check )
  {
    echo "OK\n";
  }
  else
  {   
    echo "KO\n";
  }
}
else
{
  echo "Cannot find customer id $customer_id\n";
}

?>
