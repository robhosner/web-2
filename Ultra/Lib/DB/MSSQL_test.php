<?php

include_once 'db.php';
include_once 'Ultra/Lib/DB/MSSQL.php';

#echo \Ultra\Lib\DB\user()."\n";
#echo \Ultra\Lib\DB\pwd()."\n";
#echo \Ultra\Lib\DB\host()."\n";

$db   = ( empty($argv[1]) ) ? 'ULTRA_DEVELOP_TEL' : $argv[1];
$host = ( empty($argv[2]) ) ? NULL                : $argv[2];

\Ultra\Lib\DB\connect( $db , $host );

$x = \Ultra\Lib\DB\fetch_first_value('select getutcdate()');

echo "$x\n";

exit;


/*
$sql = "SELECT GETUTCDATE() now";

$data1 = mssql_fetch_all_objects(logged_mssql_query($sql));

print_r($data1);

$data2 = \Ultra\Lib\DB\fetch_objects($sql);

print_r($data2);

$data3 = \Ultra\Lib\DB\fetch_rows($sql);

print_r($data3);
*/
/*
putenv("OVERRIDE_DB_NAME=ULTRA_ACC");
putenv("OVERRIDE_DB_HOST=sandy.hometowntelecom.com");
putenv("OVERRIDE_DB_PORT=1433");

$db_params = array( 'db_name' => 'ULTRA_ACC' );

$db_link = connect_db($db_params);

$sql = "SELECT COUNT(*) FROM SOAP_LOG";

$data2 = \Ultra\Lib\DB\fetch_objects($sql);

print_r($data2);
*/

$stored_procedure = '[ULTRA].[Get_Wholesale_Plan_By_Customer_Id]';
$params = array(
  array(
    'param_name'  => '@CUSTOMER_ID',
    'variable'    => '18950',
    'type'        => 56,
    'is_output'   => false,
    'is_null'     => false,
    'max_length'  => 255
  )
);

$response = Ultra\Lib\DB\run_stored_procedure($stored_procedure, $params);
echo "run_stored_procedure()\n";
print_r($response);
echo "\n\n";
exit(0);

// $stored_procedure = 'ULTRA.ben_test_proc';
// $params = array(
//   array(
//     'param_name'  => '@IMEI',
//     'value'       => '3530240549739610',
//     'data_type'   => PDO::PARAM_STR,
//     'is_output'   => false,
//     'length'      => 30
//   ),
//   array(
//     'param_name'  => '@IMEI8',
//     'value'       => '',
//     'data_type'   => PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,
//     'is_output'   => true,
//     'length'      => 8
//   )
// );

// $response = Ultra\Lib\DB\pdo_run_stored_procedure($stored_procedure, $params);
// echo "pdo_run_stored_procedure()\n";
// print_r($response);
// echo "\n\n";

$stored_procedure = '[ultra].[Get_Dealers_For_Customer]';
$params = array(
  array(
    'param_name'  => '@CustomerId',
    'variable'    => '18745',
    'type'        => SQLINT4,
    'is_output'   => false,
    'is_null'     => false,
    'max_length'  => 255
  )
);

$response = Ultra\Lib\DB\run_stored_procedure($stored_procedure, $params);
echo "run_stored_procedure()\n";
print_r($response);
echo "\n\n";
