<?php

namespace Ultra\Lib\DB\DestinationOriginMap;

include_once('db.php');

/**
 * destination_origin_map_prefixes 
 * @return array
 */
function destinationOriginMapPrefixes()
{
  return array(
    '1' , '' , '011' ,'00111' , '01' , '0' , '001'
  );
}

/**
 * insertAfterActivation
 *
 * Invokes insertQuery()
 *
 * @return boolean
 * @param  object $customer
 */
function insertAfterActivation($customer)
{  
  $destination_origin_map_prefixes = destinationOriginMapPrefixes();

  $check = TRUE;
  
  foreach( $destination_origin_map_prefixes as $prefix )
  {
    //where is run_sql_and_check comming from? do i need to wrap this?
    $check = $check && insertRow(
      array( 'destination' => $prefix . $customer->current_mobile_number )
    );
  }
  return $check;
}

/**
 * insertQuery: Executes stored procedure [ultra].[Destination_Origin_Insert]
 * $params: destination 
 * 
 * @param  array $params
 * @return Boolean
 */
function insertRow($params)
{
  $account     = 'CALL_ME_FREE';
  $origin      = '7001';
  $destination = $params['destination'];

  $params = array(
      array(
      'param_name' => '@ACCOUNT',
      'variable'   => $account,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
    array(
      'param_name' => '@ORIGIN',
      'variable'   => $origin,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
    array(
      'param_name' => '@DESTINATION',
      'variable'   => $destination,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
  );
  return \Ultra\Lib\DB\run_stored_procedure( '[ultra].[Destination_Origin_Insert]', $params );
}
