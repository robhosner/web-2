<?php

namespace Ultra\Lib\DB {

include_once('Ultra/UltraConfig.php');

/**
 * defaultHost
 *
 * Returns the default DB host
 *
 * @return string
 */
function defaultHost()
{
  return "db-a.hometowntelecom.com";
}

/**
 * defaultPort
 *
 * Returns the default DB port
 *
 * @return string
 */
function defaultPort()
{
  return "1433";
}

/**
 * host
 *
 * Returns DB host and port
 *
 * @return string
 */
function host()
{
  $defaultHost = defaultHost();
  $defaultPort = defaultPort();

  $configHost = \Ultra\UltraConfig\find_config('db/host');
  $configPort = \Ultra\UltraConfig\find_config('db/port');

  $configHost = ( $configHost ? $configHost : $defaultHost );
  $configPort = ( $configPort ? $configPort : $defaultPort );

  $OVERRIDE_DB_HOST = getenv('OVERRIDE_DB_HOST');
  $OVERRIDE_DB_PORT = getenv('OVERRIDE_DB_PORT');

  if ( isset( $OVERRIDE_DB_HOST ) && $OVERRIDE_DB_HOST )
    $configHost = $OVERRIDE_DB_HOST;

  if ( isset( $OVERRIDE_DB_PORT ) && $OVERRIDE_DB_PORT )
    $configPort = $OVERRIDE_DB_PORT;

  return $configHost.':'.$configPort;
}

/**
 * user
 *
 * Returns the DB user
 *
 * @return string
 */
function user()
{
  $OVERRIDE_DB_USER = getenv('OVERRIDE_DB_USER');

  if ( isset( $OVERRIDE_DB_USER ) && $OVERRIDE_DB_USER )
    return $OVERRIDE_DB_USER;

  $db_user = find_credential('db/user');

  if ( ! $db_user )
    die("Unable to retrieve database user");

  return $db_user;
}

/**
 * pwd
 *
 * Returns the password to access the DB
 *
 * @return string
 */
function pwd()
{
  $OVERRIDE_DB_PWD = getenv('OVERRIDE_DB_PWD');

  if ( isset( $OVERRIDE_DB_PWD ) && $OVERRIDE_DB_PWD )
    return $OVERRIDE_DB_PWD;

  $password = find_credential('db/pfile__retrieved');

  if (!empty($password)) {
    return $password;
  }

  $pfile = find_credential('db/pfile');
  $teldata_db_pwd = trim(file_get_contents($pfile),"\n\r");

  if ( ! $teldata_db_pwd )
    die("Unable to retrieve database credentials from $pfile!");

  return $teldata_db_pwd;
}

/**
 * connect
 *
 * Opens a DB connection to $db_name
 */
function connect($db_name,$db_host=NULL, $persistent = FALSE, $retries = 3)
{
  $db_user = user();
  $db_pwd  = pwd();
  setDatabaseName($db_name);

  if ( !$db_host )
    $db_host = host();

  $link = $persistent ? mssql_pconnect($db_host, $db_user, $db_pwd) : mssql_connect($db_host, $db_user, $db_pwd);
  if ( ! $link)
  {
    if ($retries > 0) {
      // wait (5 seconds + random interval) and retry
      \logFatal("Retrying database connection (host: $db_host, database: $db_name, user: $db_user)");

      usleep(5000000 + rand(0, 3000000));
      return connect($db_name, $db_host, $persistent, $retries - 1);
    } else {
      \logFatal("Unable to connect to database (host: $db_host, database: $db_name, user: $db_user)");
      throw new \Exception( 'Unable to connect to database' );
    }
  }

  if (! mssql_select_db($db_name, $link))
  {
    $msg = "Unable to select database (host: $db_host, database: $db_name, user: $db_user)";
    \logFatal($msg);
    throw new \Exception( 'Unable to select database' );
  }

  // Return the link so it can be used elsewhere
  return $link;
}

/**
 * mw_db_connect
 *
 * Opens a DB connection to Middleware DB
 * Substitutes ultra_acc_connect()
 */
function mw_db_connect()
{
  $middlewareDBCredentials = \Ultra\UltraConfig\getMiddlewareDBCredentials();

  $db_host = $middlewareDBCredentials['host'];
  $db_user = $middlewareDBCredentials['username'];
  $db_pwd  = $middlewareDBCredentials['password'];
  $db_name = $middlewareDBCredentials['db_name'];
  setDatabaseName($db_name);

  \logDebug("$db_host, $db_user, $db_name");

  $link = mssql_connect($db_host, $db_user, $db_pwd);

  mssql_select_db($db_name, $link);

  if ( ! $link || ! mssql_select_db( $db_name , $link ) )
  {
    $msg = 'Unable to select Middleware DB (host:'.$db_host.' database:'.$db_name.' user:'.$db_user.')';
    \logFatal($msg);
    throw new \Exception( 'Unable to select Middleware DB' );
  }

  // Return the link so it can be used elsewhere
  return $link;
}

/**
 * ultra_acc_connect
 *
 * Now an alias for mw_db_connect
 * Opens a DB connection to ULTRA_ACC
 */
function ultra_acc_connect( $host_override=NULL )
{
  return mw_db_connect();
}

/**
 * dealer_portal_connect
 *
 * open a connection to the dealer portal host and database
 * TODO: multiple connections use, host/db detection and re-use of existing connection
 * @see https://issues.hometowntelecom.com:8443/browse/DEAL
 */
function dealer_portal_connect()
{
  // load host info and credentials
  $host     = \Ultra\UltraConfig\find_config('ultra/celluphone/db/host');
  $database = \Ultra\UltraConfig\find_config('ultra/celluphone/db/name');
  $username = user();
  $password = pwd();
  setDatabaseName($database);

  if (empty($host) || empty($database) || empty($username) || empty($password))
  {
    dlog('', "ERROR: invalid connection info host: $host, DB: $database, username: $username");
    return NULL;
  }

  if ( ! $link = mssql_pconnect($host, $username, $password))
  {
    dlog('', "ERROR: unable to login to host $host as username $username");
    return NULL;
  }

  if ( ! mssql_select_db($database, $link))
  {
    dlog('', "ERROR: unable to select database $database: " . mssql_get_last_message());
    return NULL;
  }

  \logInfo( "using database $database on host $host as username $username" );

  return $link;
}

function line_credits_connect()
{
  $host     =  \Ultra\UltraConfig\find_config('db/host');
  $database = ( ! \Ultra\UltraConfig\isProdEnvironment()) ? 'LineCreditsDev' : 'LineCreditsProd';
  $username = user();
  $password = pwd();
  setDatabaseName($database);

  $link = mssql_pconnect($host, $username, $password);
  mssql_select_db($database);

  \logInfo( "using database $database on host $host as username $username" );

  return $link;
}

/**
 * fetch_objects
 *
 * @return array
 */
function fetch_objects($sql)
{
  $result = array();

  $res = logged_mssql_query( $sql );

  if (is_resource($res))
  {
    do
    {
      while ($row = mssql_fetch_object($res))
      {
        $result[] = $row;
      }
    }
    while ( mssql_next_result($res) );
    // Clean up
    mssql_free_result($res);
  }

  return $result;
}

/**
 * fetch_rows
 *
 * @return array
 */
function fetch_rows($sql,$limit=999999999)
{
  $result = array();

  $res = logged_mssql_query( $sql );

  if (is_resource($res))
  {
    $count = 0;

    do
    {
      while ($row = mssql_fetch_row($res))
      {
        $result[] = $row;
        $count++;
        if ($count >= $limit) break;
      }

      if ($count >= $limit) break;
    }
    while ( mssql_next_result($res) );
    // Clean up
    mssql_free_result($res);
  }

  return $result;
}

/**
 * fetch_first_value($sql,$limit=999999999)
 *
 * @return string
 */
function fetch_first_value($sql,$limit=999999999)
{
  $value = NULL;

  $result = fetch_rows($sql,$limit);

  if ( $result && is_array($result) && count($result) )
    $value = $result[0][0];

  return $value;
}

/**
 * pdo_run_stored_procedure
 *
 * Runs a stored procedure using pdo library.
 * @see http://php.net/manual/en/book.pdo.php
 * @param string $sp_name name of stored procedure
 * @param array $params = array(
 *   array(
 *     'param_name'  => '@IMEI',
 *     'value'       => '3530240549739610',
 *     'data_type'   => PDO::PARAM_STR,
 *     'is_output'   => false,
 *     'length'      => 30
 *   ),
 *   array(
 *     'param_name'  => '@IMEI8',
 *     'value'       => '',
 *     'data_type'   => PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT,
 *     'is_output'   => true,
 *     'length'      => 8
 *   )
 * );
 * @return boolean|array
 * @author bwalters@utra.me
 */
function pdo_run_stored_procedure($sp_name, $params)
{
  try
  {
    $db_host  = \Ultra\Lib\DB\host();
    $db       = "ULTRA_DEVELOP_TEL"; // wtf
    $db_user  = \Ultra\Lib\DB\user();
    $db_pwd   = \Ultra\Lib\DB\pwd();
    $dbh      = new \PDO("dblib:host={$db_host};dbname={$db}", $db_user, $db_pwd);
    $query    = "exec {$sp_name} ";

    // Construct placeholders for statement
    for ($i = 1, $length = count($params); $i <= $length; $i++)
    {
      $query .= "?";
      if ($i < $length)
      {
        $query .= ", ";
      }
    }

    // Prepare statement
    $stmt = $dbh->prepare($query);

    // Bind params to placeholders in statement
    for ($i = 1, $length = count($params); $i <= $length; $i++)
    {
      $index = $i - 1;
      $stmt->bindParam($i, $params[$index]['value'], $params[$index]['data_type'], $params[$index]['length']);
    }

    $result     = $stmt->execute();
    $result_set = $stmt->fetchAll();

    return ((is_array($result_set) && count($result_set) > 0))
      ? $result_set
      : $result;
  }
  catch (\PDOException $e)
  {
    dlog('', 'PDO Error: ' . $e->getMessage());
  }

  return false;
}

/**
 * run_stored_procedure
 *
 * Runs a stored procedure using mssql_execute.
 * @see http://php.net/manual/en/function.mssql-execute.php
 * @param string $sp_name
 * @param array $params = array(
 *    array(
 *      'param_name'  => '@IMEI',
 *      'variable'    => '3530240549739610',
 *      'type'        => SQLVARCHAR,
 *      'is_output'   => false,
 *      'is_null'     => false,
 *      'max_length'  => 255
 *    )
 *  );
 * @param boolean $returnObjects whether to return a flat array or kva
 * @return boolean|array
 * @author bwalters@utra.me
 */
function run_stored_procedure($sp_name, $params, $returnObjects = FALSE)
{
  dlog('', '(%s)', func_get_args());

  $result = NULL;

  $has_output_param = FALSE;

  try
  {
    if (!extension_loaded('mssql') && extension_loaded('sqlsrv')) {
      $paramNames = [];
      $paramValues = [];

      foreach ($params as $param) {
        $paramNames[] = $param['param_name'] . ' = ?';
        $paramValues[] = $param['variable'];
      }

      $sql = "EXEC $sp_name " . implode(", ", $paramNames);
      $stmt = sqlsrv_prepare(getDatabaseConnection(), $sql, $paramValues);
      $result = sqlsrv_execute($stmt);

      if (!$result) {
        throw new \Exception('sqlsrv_execute failed for ' . $sp_name);
      }

      dlog('', "EXECUTING STORED PROCEDURE '$sql' with parameters " . json_encode($paramValues));

      $result_set = [];

      while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)) {
        $result_set[] = $row;
      }

      sqlsrv_free_stmt($stmt);

    } else {
      $stmt = mssql_init( $sp_name );

      foreach ( $params as $param )
      {
        if ($param['is_output']) $has_output_param = TRUE;
        // explanation: mssql_bind($stmt, $param_name, &$variable, $type, $is_output, $is_null, $maxlen);
        // example: mssql_bind($stmt, '@MasterCd', $masteragent, 'NVARCHAR', false, false, 8);
        mssql_bind( $stmt, $param['param_name'], $param['variable'], $param['type'], $param['is_output'], false, $param['max_length'] );
      }

      $result = mssql_execute($stmt);

      mssql_free_statement($stmt);

      if ( empty( $result ) || ! $result )
        throw new \Exception('mssql_execute failed for '.$sp_name);

      $result_set = array();

      if ( is_resource( $result ) )
      {
        if ($returnObjects)
        {
          while ( $arr = mssql_fetch_object( $result ) )
            $result_set[] = $arr;
        }
        else
        {
          while ( $arr = mssql_fetch_row( $result ) )
            $result_set[] = $arr;
        }
      }
    }

    $result = ((is_array($result_set) && count($result_set) > 0)) ? $result_set : NULL ;
  }
  catch( \Exception $e )
  {
    dlog('',$e->getMessage());
    $result = false;
  }

  dlog('', 'result: %s', $result);
  return $result;
}

function setDatabaseName($databaseName)
{
  global $dbName;
  $dbName = $databaseName;
}

function getDatabaseName()
{
  global $dbName;
  return $dbName;
}

function setDatabaseConnection($connection)
{
  global $dbConnection;
  $dbConnection = $connection;
}

function getDatabaseConnection()
{
  global $dbConnection;
  return !empty($dbConnection) ? $dbConnection : null;
}

function closeDatabaseConnection()
{
  $dbConnection = getDatabaseConnection();

  if (is_resource($dbConnection)) {
    mssql_close($dbConnection);
  }
}

function isMssqlExtensionMissing()
{
  return (!extension_loaded('mssql') && extension_loaded('sqlsrv'));
}
}

namespace
{
  if (\Ultra\Lib\DB\isMssqlExtensionMissing()) {
    define('MSSQL_ASSOC', 1);
    define('MSSQL_NUM', 2);
    define('MSSQL_BOTH', 3);
    define('SQLTEXT', 35);
    define('SQLVARCHAR', 39);
    define('SQLCHAR', 47);
    define('SQLINT1', 48);
    define('SQLINT2', 52);
    define('SQLINT4', 56);
    define('SQLBIT', 50);
    define('SQLFLT4', 59);
    define('SQLFLT8', 62);
    define('SQLFLTN', 109);

    function mssql_connect($servername = null, $username = null, $password = null, $new_link = false)
    {
      $dbConnection = sqlsrv_connect($servername, [
        "Database" => Ultra\Lib\DB\getDatabaseName(),
        "UID" => $username,
        "PWD" => $password,
        "MultipleActiveResultSets" => false,
        "ReturnDatesAsStrings" => true,
        'CharacterSet' => 'UTF-8'
      ]);
      Ultra\Lib\DB\setDatabaseConnection($dbConnection);

      return $dbConnection;
    }

    function mssql_pconnect($servername = null, $username = null, $password = null, $new_link = false)
    {
      return mssql_connect($servername, $username, $password);
    }

    function mssql_close($link_identifier = null)
    {
      return sqlsrv_close(Ultra\Lib\DB\getDatabaseConnection());
    }

    // sqlsrv extention does not have an equivalent for this, sqlsrv_connect() will accept the database name.
    function mssql_select_db($database_name, $link_identifier = null)
    {
      return true;
    }

    function mssql_query($query, $link_identifier = null, $batch_size = 0)
    {
      return sqlsrv_query(Ultra\Lib\DB\getDatabaseConnection(), "SET ANSI_WARNINGS OFF; " . $query);
    }

    function mssql_rows_affected($link_identifier)
    {
      return sqlsrv_rows_affected($link_identifier);
    }

    function mssql_free_result($result)
    {
      return sqlsrv_free_stmt($result);
    }

    function mssql_get_last_message()
    {
      $errors = sqlsrv_errors();
      if (!empty($errors[0]['message'])) {
        return $errors[0]['message'];
      } else {
        return null;
      }
    }

    function mssql_num_rows($result)
    {
      return sqlsrv_num_rows($result);
    }

    function mssql_num_fields($result)
    {
      return sqlsrv_num_fields($result);
    }

    function mssql_fetch_field($result, $field_offset = -1)
    {
      return sqlsrv_field_metadata($result);
    }

    function mssql_fetch_row($result)
    {
      return sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC);
    }

    function mssql_fetch_array($result, $result_type = MSSQL_BOTH)
    {
      return sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH);
    }

    function mssql_fetch_assoc($result_id)
    {
      return sqlsrv_fetch_array($result_id, SQLSRV_FETCH_ASSOC);
    }

    function mssql_fetch_object($result)
    {
      return sqlsrv_fetch_object($result);
    }

    function mssql_next_result($result_id)
    {
      return sqlsrv_next_result($result_id);
    }

    function mssql_execute($stmt, $skip_results = false)
    {
      return sqlsrv_execute($stmt);
    }

    function mssql_free_statement($stmt)
    {
      return sqlsrv_free_stmt($stmt);
    }

    // function stubs not used in project //
    function mssql_fetch_batch($result){}

    function mssql_field_length($result, $offset = null){}

    function mssql_field_name($result, $offset = -1){}

    function mssql_field_type($result, $offset = -1){}

    function mssql_data_seek($result_identifier, $row_number){}

    function mssql_field_seek($result, $field_offset){}

    function mssql_result($result, $row, $field){}

    function mssql_min_error_severity($severity){}

    function mssql_min_message_severity($severity){}

    function mssql_guid_string($binary, $short_format = null){}

    function mssql_init($sp_name, $link_identifier = null){}

    function mssql_bind($stmt, $param_name, &$var, $type, $is_output = false, $is_null = false, $maxlen = -1){}
  }
}
