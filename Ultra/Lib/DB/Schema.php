<?php

/*
 * Schema.php
 *
 * static database schema description and query generation functions
 * @todo: add constants for SQL NULL and special time values (past, future, now)
 *
 */

namespace Ultra\Lib\DB;

/**
 * getSchema
 *
 * get schema for all tables (which have been defined here)
 * simple format: type only (.e.g. 'varchar')
 * extended format, required for parameterized queries: CSV 'type, legnth, nullable'
 * where length is required only for vartiable types, such as varchar and nvarchar
 * @return: 3D array(table => array(name => type)) of attributes
 * @author: VYT, 14-01-24
 */
function getSchema()
{
  // WARNING: all schema (except data types) must be in CAPS regardless of the actual DB schema
  return array(
    'ACCOUNT_ALIASES' => array(
      'ACCOUNT_ALIAS_ID'    => 'bigint',
      'DNIS'                => 'varchar',
      'ALIAS'               => 'varchar',
      'ACCOUNT_ID'          => 'bigint',
      'ACCOUNT_ALIAS_TYPE'  => 'bigint',
      'PASSWORD'            => 'varchar',
      'SIP_USER_ID'         => 'varchar'
    ),

    'CUSTOMERS' => array(
      'CUSTOMER_ID'               => 'bigint',
      'CUSTOMER'                  => 'varchar',
      'FIRST_NAME'                => 'varchar',
      'LAST_NAME'                 => 'varchar',
      'COMPANY'                   => 'varchar',
      'ADDRESS1'                  => 'varchar',
      'ADDRESS2'                  => 'varchar',
      'CITY'                      => 'varchar',
      'STATE_REGION'              => 'varchar',
      'POSTAL_CODE'               => 'varchar',
      'COUNTRY'                   => 'varchar',
      'LOCAL_PHONE'               => 'varchar',
      'FAX'                       => 'varchar',
      'E_MAIL'                    => 'varchar',
      'DATE_OF_BIRTH'             => 'smalldatetime',
      'SIGNUP_DATE'               => 'smalldatetime',
      'LOGIN_NAME'                => 'varchar',
      'LOGIN_PASSWORD'            => 'varchar',
      'CC_NUMBER'                 => 'varchar',
      'CC_EXP_DATE'               => 'varchar',
      'CC_NAME'                   => 'varchar',
      'CC_ADDRESS1'               => 'varchar',
      'CC_ADDRESS2'               => 'varchar',
      'CC_CITY'                   => 'varchar',
      'CC_COUNTRY'                => 'varchar',
      'CC_STATE_REGION'           => 'varchar',
      'CC_POSTAL_CODE'            => 'varchar',
      'NOTES'                     => 'varchar',
      'USER_1'                    => 'varchar',
      'USER_2'                    => 'varchar',
      'USER_3'                    => 'varchar',
      'USER_4'                    => 'varchar',
      'USER_5'                    => 'varchar',
      'USER_6'                    => 'varchar',
      'USER_7'                    => 'varchar',
      'USER_8'                    => 'varchar',
      'USER_9'                    => 'varchar',
      'USER_10'                   => 'varchar',
      'CCV'                       => 'varchar',
      'PUBLIC_CUSTOMER'           => 'bit'
    ),

    'ACCOUNTS' => array(
      'ACCOUNT_ID'                    => 'bigint',
      'ACCOUNT'                       => 'bigint',
      'PIN'                           => 'varchar',
      'CUSTOMER_ID'                   => 'bigint',
      'PARENT_ACCOUNT_ID'             => 'bigint',
      'BATCH_ID'                      => 'smallint',
      'SEQUENCE_NUMBER'               => 'int',
      'ACCOUNT_GROUP_ID'              => 'smallint',
      'ACCOUNT_TYPE'                  => 'tinyint',
      'CALLBACK_NUMBER'               => 'varchar',
      'BILLING_TYPE'                  => 'tinyint',
      'CREATION_DATE_TIME'            => 'datetime',
      'FIRST_AUTHENTICATED_DATE_TIME' => 'datetime',
      'ACTIVATION_DATE_TIME'          => 'datetime',
      'STARTING_BALANCE'              => 'decimal',
      'CREDIT_LIMIT'                  => 'decimal',
      'BALANCE'                       => 'decimal', // dollar denominated balance that does not expire and is rolled over from one month to the next. It is used to purchase services provided outside of a monthly plan, i.e "extra international calling to a destination".
      'PACKAGED_BALANCE1'             => 'int', // Call Anywhere Credit ( $ * 60000 ) - TSIP 3.13 => minutes ; TSIP 3.11 and 3.12 => seconds (!)
      'PACKAGED_BALANCE2'             => 'int',
      'PACKAGED_BALANCE3'             => 'int',
      'PACKAGED_BALANCE4'             => 'int',
      'PACKAGED_BALANCE5'             => 'int',
      'STARTING_PACKAGED_BALANCE1'    => 'int',
      'STARTING_PACKAGED_BALANCE2'    => 'int',
      'STARTING_PACKAGED_BALANCE3'    => 'int',
      'STARTING_PACKAGED_BALANCE4'    => 'int',
      'STARTING_PACKAGED_BALANCE5'    => 'int',
      'SERVICE_CHARGE_DATE'           => 'smalldatetime',
      'COS_ID'                        => 'bigint',
      'WRITE_CDR'                     => 'bit',
      'SERVICE_CHARGE_STATUS'         => 'bit',
      'CALLS_TO_DATE'                 => 'int',
      'LAST_CALL_DATE_TIME'           => 'datetime',
      'MINUTES_TO_DATE_BILLED'        => 'decimal',
      'LAST_CALLER'                   => 'varchar',
      'LAST_CALLED_PARTY'             => 'varchar',
      'LAST_CREDIT_DATE_TIME'         => 'datetime',
      'LAST_DEBIT_DATE_TIME'          => 'datetime',
      'LAST_RECHARGE_DATE_TIME'       => 'datetime',
      'LAST_TRANSFER_DATE_TIME'       => 'datetime',
      'LAST_ENABLED_UPDATE_DATE_TIME' => 'datetime',
      'LAST_AUTHENTICATED_DATE_TIME'  => 'datetime',
      'PERIOD_CALLS_TO_DATE'          => 'int',
      'PERIOD_MINUTES_TO_DATE_BILLED' => 'decimal',
      'PERIOD_MINUTES_TO_DATE_ACTUAL' => 'decimal',
      'PERIOD_LAST_RESET_DATE_TIME'   => 'datetime'
    ) +
    // PROD-2332: add either ENABLED or ACCOUNT_STATUS_TYPE depending to TSIP version
    (\Ultra\UltraConfig\getTSIPDBVersion() == '3.12' ?
      array('ACCOUNT_STATUS_TYPE'     => 'int') :
      array('ENABLED'                 => 'bit')),

/*
    'BILLING' => array(
if AMOUNT is 0 => it uses packaged balance 1

example:

PER_MINUTE_CHARGE:                 25
QUANTITY:                         425
 => duration is 7~ minutes

*/

    'MRC_USAGE_LOOKUP'              => array(
      'LOOKUP_ID'                   => 'int',
      'TYPE'                        => 'varchar',
      'NAME'                        => 'varchar',
      'ULTRA_SOC'                   => 'varchar',
      'EFFECTIVE_DATE'              => 'date',
      'DETAIL'                      => 'varchar'
    ),

    'MRC_USAGE' => array(
      'MRC_USAGE_ID'                      => 'BIGINT,   ,     FALSE', // primary key
      'CUSTOMER_ID'                       => 'BIGINT,   ,     FALSE',
      'SAMPLE_DATE'                       => 'DATETIME, ,     FALSE',
      'HTT_PLAN_TRACKER_ID'               => 'BIGINT,   ,     TRUE',
      'SESSION_ID_CHECKBALANCE'           => 'VARCHAR,  200,  TRUE',
      'SESSION_ID_QUERYSUBSCRIBER'        => 'VARCHAR,  200,  TRUE',
      'WHOLESALEPLAN'                     => 'INT,      ,     FALSE'
    ),

    'MRC_USAGE_VALUES'              => array(
      'MRC_USAGE_ID'                => 'bigint',
      'LOOKUP_ID'                   => 'int',
      'VALUE'                       => 'bigint'
    ),

    'PORTIN_QUEUE' => array(
      'PORTIN_QUEUE_ID'           => 'bigint',
      'CUSTOMER_ID'               => 'bigint',
      'MSISDN'                    => 'varchar',
      'ACCOUNT_NUMBER'            => 'varchar',
      'ACCOUNT_PASSWORD'          => 'varchar',
      'ACCOUNT_ZIPCODE'           => 'varchar',
      'START_DATE_TIME'           => 'datetime',
      'UPDATED_DATE_TIME'         => 'datetime',
      'LAST_ACC_API'              => 'varchar',
      'REQUESTED_DATE_TIME'       => 'datetime',
      'PORT_REQUEST_ID'           => 'varchar',
      'DUE_DATE_TIME'             => 'datetime',
      'COMPLETED_DATE_TIME'       => 'datetime',
      'PORT_STATUS'               => 'varchar',
      'PORT_ERROR'                => 'varchar',
      'PROVSTATUS_DATE'           => 'datetime',
      'PROVSTATUS_DESCRIPTION'    => 'varchar',
      'PROVSTATUS_ERROR_CODE'     => 'varchar',
      'PROVSTATUS_ERROR_MSG'      => 'varchar',
      'QUERYSTATUS_PORTSTATUS'    => 'varchar',
      'QUERYSTATUS_ERRORMSG'      => 'varchar',
      'QUERYSTATUS_DATE_TIME'     => 'datetime',
      'OCN'                       => 'int',
      'CARRIER_NAME'              => 'varchar',
      'OCN_REQUEST_DATE'          => 'datetime',
      'OLD_SERVICE_PROVIDER'      => 'varchar'
    ),

    'MAKEITSO_QUEUE' => array(
      'MAKEITSO_QUEUE_ID'         => 'bigint',   // MakeItSo task unique numeric identifier bigint autoincrement - primary key
      'ACTION_UUID'               => 'varchar',  // Unique Identifier of the associated action
      'CUSTOMER_ID'               => 'bigint',   // ultra customer numeric identifier bigint
      'CREATED_DATE_TIME'         => 'datetime', // MakeItSo task creation date and time
      'LAST_ATTEMPT_DATE_TIME'    => 'datetime', // MakeItSo task last attempted date and time
      'NEXT_ATTEMPT_DATE_TIME'    => 'datetime', // MakeItSo task allowed date and time for the next attempt
      'COMPLETED_DATE_TIME'       => 'datetime', // MakeItSo task completion date and time
      'ATTEMPT_COUNT'             => 'tinyint',  // Number of attempts performed
      'ATTEMPT_HISTORY'           => 'text',     // History of attempts
      'MAKEITSO_TYPE'             => 'varchar',  // MakeItSo type
      'MAKEITSO_SUBTYPE'          => 'varchar',  // MakeItSo subtype
      'MAKEITSO_OPTIONS'          => 'text',     // MakeItSo associated data (JSON serialized object)
      'LAST_ERROR_CODE'           => 'varchar',  // Last error code associated with a failed MW call
      'LAST_ACC_API'              => 'varchar',  // Last MW call operation
      'STATUS'                    => 'varchar'   // current MakeItSo status
    ),

    'COMMAND_INVOCATIONS' => array(
      'COMMAND_INVOCATIONS_ID'    => 'bigint',
      'ACTION_UUID'               => 'varchar',
      'CUSTOMER_ID'               => 'bigint',
      'CORRELATION_ID'            => 'varchar',
      'ACC_TRANSITION_ID'         => 'varchar',
      'ACC_API'                   => 'varchar',
      'COMMAND'                   => 'varchar',
      'START_DATE_TIME'           => 'datetime',
      'LAST_STATUS_DATE_TIME'     => 'datetime',
      'LAST_STATUS_UPDATED_BY'    => 'varchar',
      'STATUS'                    => 'varchar',
      'TICKET_DETAILS'            => 'varchar',
      'REPLY_DATE_TIME'           => 'datetime',
      'REPORTED_DATE_TIME'        => 'datetime',
      'ERROR_DATE_TIME'           => 'datetime',
      'COMPLETED_DATE_TIME'       => 'datetime',
      'STATUS_HISTORY'            => 'text',
      'ERROR_CODE_AND_MESSAGE'    => 'varchar'
    ),

    'HTT_ACTIVATION_LOG' => array(
      'ICCID_NUMBER'          => 'char', // primary key
      'ACTIVATED_CUSTOMER_ID' => 'bigint',
      'ACTIVATED_MASTERAGENT' => 'bigint',
      'ACTIVATED_DISTRIBUTOR' => 'bigint',
      'ACTIVATED_DEALER'      => 'bigint',
      'ACTIVATED_USERID'      => 'bigint',
      'ACTIVATED_DATE'        => 'datetime',
      'ACTIVATED_BY'          => 'varchar',  // agent
      'ICCID_FULL'            => 'char',
      'PROMISED_AMOUNT'       => 'decimal',
      'INITIAL_COS_ID'        => 'bigint',
      'INITIAL_BOLTONS'       => 'varchar'
    ),

    'HTT_BILLING_ACTIONS' => array(
      'CUSTOMER_ID'    => 'bigint', // foreign key
      'UUID'           => 'varchar',
      'STATUS'         => 'varchar',
      'CREATED'        => 'datetime',
      'CLOSED'         => 'datetime',
      'NEXT_ATTEMPT'   => 'datetime',
      'LAST_ATTEMPT'   => 'datetime',
      'ATTEMPT_LOG'    => 'text',
      'TRANSACTION_ID' => 'varchar',
      'PRODUCT_ID'     => 'varchar',
      'SUBPRODUCT_ID'  => 'varchar',
      'AMOUNT'         => 'decimal'
    ),

    'HTT_BILLING_HISTORY' => array(
      'TRANSACTION_ID'               => 'bigint', // primary key
      'CUSTOMER_ID'                  => 'bigint',
      'TRANSACTION_DATE'             => 'datetime',
      'COS_ID'                       => 'bigint',
      'ENTRY_TYPE'                   => 'varchar',
      'STORED_VALUE_CHANGE'          => 'decimal',
      'BALANCE_CHANGE'               => 'decimal',
      'PACKAGE_BALANCE_CHANGE'       => 'decimal',
      'CHARGE_AMOUNT'                => 'decimal',
      'REFERENCE'                    => 'varchar',
      'REFERENCE_SOURCE'             => 'tinyint',
      'DETAIL'                       => 'varchar',
      'DESCRIPTION'                  => 'varchar',
      'RESULT'                       => 'varchar',
      'SOURCE'                       => 'varchar',
      'STORE_ZIPCODE'                => 'varchar',
      'STORE_ID'                     => 'varchar',
      'CLERK_ID'                     => 'varchar',
      'TERMINAL_ID'                  => 'varchar',
      'IS_COMMISSIONABLE'            => 'bit',
      'SURCHARGE_AMOUNT'             => 'decimal',
      'COMMISSIONABLE_CHARGE_AMOUNT' => 'decimal'
    ),

    'HTT_CANCELLATION_REASONS' => array(
      'CUSTOMER_ID'               => 'bigint',
      'REASON'                    => 'varchar',
      'TYPE'                      => 'varchar',
      'PORTOUT_OCN'               => 'int',
      'PORTOUT_CARRIER_NAME'      => 'varchar',
      'PORTOUT_OCN_REQUEST_DATE'  => 'datetime',
      'PORTOUT_SPID'              => 'varchar',
      'DEACTIVATION_DATE'         => 'datetime',
      'DEACTIVATION_CARRIER_NAME' => 'varchar',
      'MSISDN'                    => 'varchar',
      'ICCID'                     => 'varchar',
      'COS_ID'                    => 'bigint',
      'STATUS'                    => 'varchar',
      'EVER_ACTIVE'               => 'bit',
      'AGENT'                     => 'varchar',
      'DEACTIVATION_CARRIER_ID'   => 'varchar'
    ),

    'HTT_COVERAGE_INFO' => array(
      'ZIPCODE'         => 'varchar', // primary key
      'RF_FAIR_POP'     => 'decimal',
      'RF_FAIR_AREA'    => 'decimal',
      'RF_GOOD_POP'     => 'decimal',
      'RF_GOOD_AREA'    => 'decimal',
      'RF_GREAT_POP'    => 'decimal',
      'RF_GREAT_AREA'   => 'decimal',
      'ZIP_TYPE'        => 'char',
      'ZIP_STATE'       => 'varchar',
      'ZIP_RATE_CENTER' => 'varchar',
    ),

    'HTT_CUSTOMERS_OVERLAY_ULTRA' => array(
      'CUSTOMER_ID'                       => 'BIGINT,   ,     FALSE', // ultra customer numeric identifier bigint autoincrement
      'NOTES'                             => 'TEXT,     16,   TRUE',  // special notes about this specific ultra customer
      'CURRENT_MOBILE_NUMBER'             => 'VARCHAR,  10,   TRUE',  // phone number currently associated with this ultra customer
      'CURRENT_ICCID'                     => 'VARCHAR,  18,   TRUE',  // SIM identifier (18 digits) currently associated with this ultra customer
      'APPLIED_DATA_SOC'                  => 'VARCHAR,  8000, TRUE',  // obsolete
      'APPLIED_DATA_SOC_DATE'             => 'DATETIME, ,     TRUE',  // obsolete
      'EASYPAY_ACTIVATED'                 => 'BIT,      ,     TRUE', 
      'STORED_VALUE'                      => 'DECIMAL,  ,     FALSE', // dollar amount used for future plan recharge
      'PLAN_STATE'                        => 'VARCHAR,  50,   TRUE',  // current state of the customer (see state machine definition)
      'PREFERRED_LANGUAGE'                => 'CHAR,     2,    FALSE', // customer's preferred language
      'PLAN_STARTED'                      => 'DATETIME, ,     TRUE',  // start date of the current billing cycle
      'PLAN_EXPIRES'                      => 'DATETIME, ,     TRUE',  // end date of the current billing cycle
      'MONTHLY_CC_RENEWAL'                => 'BIT,      ,     TRUE',  // flag (0 or 1) which idicates if plan recharge should automatically charge customer's credit card on file
      'TOS_ACCEPTED'                      => 'DATETIME, ,     TRUE',  // date of the Terms Of Services acceptance
      'MONTHLY_RENEWAL_TARGET'            => 'VARCHAR,  20,   TRUE',  // to which plan the customer requested to change plan to at the end of the current billing cycle
      'MONTHLY_RENEWAL_TARGET_REQUESTED'  => 'DATETIME, ,     TRUE',  // date in which MONTHLY_RENEWAL_TARGET has changed
      'CANCEL_REQUESTED'                  => 'DATETIME, ,     TRUE',  // date in which customer cancellation has been requested
      'CANCELLED'                         => 'DATETIME, ,     TRUE',  // date in which customer has been cancelled
      'ACTIVATION_ICCID'                  => 'VARCHAR,  18,   TRUE',  // SIM identifier (18 digits) of the first activated SIM for this ultra customer
      'DATA_RECHARGES_LEFT'               => 'TINYINT,  ,     FALSE', // obsolete
      'SPEEDY_MBYTES'                     => 'INT,      ,     TRUE',  // obsolete
      'CUSTOMER_SOURCE'                   => 'VARCHAR,  18,   TRUE',  // source of this customer's activation
      'CURRENT_ICCID_FULL'                => 'VARCHAR,  19,   TRUE',  // SIM identifier (19 digits) currently associated with this ultra customer
      'ACTIVATION_ICCID_FULL'             => 'VARCHAR,  19,   TRUE',  // SIM identifier (19 digits) of the first activated SIM for this ultra customer
      'CUSTOMER_TYPE'                     => 'VARCHAR,  15,   FALSE',
      'CUSTOMER_TYPE_VALUE'               => 'VARCHAR,  20,   TRUE',
      'MVNE'                              => 'VARCHAR,  20,   FALSE', // 1 for Aspider (previous MVNE), 2 for Amdocs (MVNE we curretnly use)
      'GROSS_ADD_DATE'                    => 'DATETIME, ,     TRUE',  // date in which customer plan has been activated for the first time
      'BRAND_ID'                          => 'SMALLINT, ,     TRUE',  // foreign key to ULTRA.BRANDS.BRAND_ID
    ),

    'HTT_DATA_EVENT_LOG' => array(
      'HTT_DATA_EVENT_LOG_ID' => 'bigint', //-- primary key
      'ACTION'                => 'varchar',
      'EVENT_DATE'            => 'datetime',
      'CUSTOMER_ID'           => 'bigint',
      'SOC'                   => 'varchar',
      'VALUE'                 => 'int',
      'HTT_PLAN_TRACKER_ID'   => 'bigint'
    ),

    'HTT_INVENTORY_PIN' => array(
      'SERIAL_NUMBER'         => 'char', // primary key
      'PIN_CRYPTED'           => 'char',
      'PIN_VALUE'             => 'decimal',
      'CUSTOMER_USED'         => 'bigint',
      'METHOD_USED'           => 'varchar',
      'INVENTORY_MASTERAGENT' => 'bigint',
      'INVENTORY_DISTRIBUTOR' => 'bigint',
      'INVENTORY_DEALER'      => 'bigint',
      'STATUS'                => 'varchar',
      'LAST_CHANGED_DATE'     => 'datetime',
      'LAST_CHANGED_BY'       => 'varchar',
      'CREATED_BY_DATE'       => 'datetime',
      'CREATED_BY'            => 'varchar',
      'SERIAL_NUMBER_ID'      => 'char', // foreign key
      'EXPIRES_DATE'          => 'datetime'
    ),

    'HTT_INVENTORY_SIM' => array(
      'ICCID_NUMBER'              => 'char', 
      'IMSI'                      => 'char', 
      'SIM_ACTIVATED'             => 'bit', 
      'INVENTORY_MASTERAGENT'     => 'bigint', 
      'INVENTORY_DISTRIBUTOR'     => 'bigint', 
      'INVENTORY_DEALER'          => 'bigint', 
      'INVENTORY_STATUS'          => 'varchar', 
      'LAST_CHANGED_DATE'         => 'datetime', 
      'LAST_CHANGED_BY'           => 'varchar', 
      'CREATED_BY_DATE'           => 'datetime', 
      'CREATED_BY'                => 'varchar', 
      'ICCID_BATCH_ID'            => 'char', 
      'ICCID_FULL'                => 'char', 
      'EXPIRES_DATE'              => 'datetime', 
      'PIN1'                      => 'char', 
      'PUK1'                      => 'char', 
      'PIN2'                      => 'char', 
      'PUK2'                      => 'char', 
      'OTHER'                     => 'char', 
      'LAST_TRANSITION_UUID'      => 'varchar', 
      'CUSTOMER_ID'               => 'bigint', 
      'RESERVATION_TIME'          => 'datetime', 
      'GLOBALLY_USED'             => 'bit', 
      'ACT_CODE'                  => 'char' ,
      'PRODUCT_TYPE'              => 'varchar', 
      'STORED_VALUE'              => 'int', 
      'SIM_HOT'                   => 'bit', 
      'MVNE'                      => 'varchar',
      'OFFER_ID'                  => 'tinyint',
      'BRAND_ID'                  => 'smallint'
    ),

// extended schema: type, length, nullable
    'HTT_INVENTORY_SIMBATCHES' => array(
      'ICCID_BATCH_ID' => 'CHAR, 16, FALSE',
      'SKU'            => 'VARCHAR, 30, FALSE',
      'UPC'            => 'CAR, 12, FALSE',
      'CHAANNEL'       => 'VARCHAR, 15, FALSE',
      'TMOPROFILESKU'  => 'VARCHAR, 25, TRUE',
      'EXPIRES_DATE'   => 'DATETIME'
    ),

    'HTT_LOOKUP_REFERENCE_ZIPLOC' => array(
      'ZIP_CODE'  => 'varchar', // primary key
      'LATITUDE'  => 'decimal',
      'LONGITUDE' => 'decimal',
      'LOCATION'  => 'geography'
    ),

    'HTT_MESSAGING_QUEUE' => array(
      'HTT_MESSAGING_QUEUE_ID'     => 'bigint', // primary key
      'REASON'                     => 'varchar',
      'CREATED_DATETIME'           => 'datetime',
      'EXPECTED_DELIVERY_DATETIME' => 'datetime',
      'NEXT_ATTEMPT_DATETIME'      => 'datetime',
      'TYPE'                       => 'varchar',
      'ATTEMPT_COUNT'              => 'tinyint',
      'CUSTOMER_ID'                => 'bigint',
      'ATTEMPT_LOG'                => 'text',
      'CONDITIONAL'                => 'bit',
      'STATUS'                     => 'varchar'
    ),

    'HTT_MONTHLY_SERVICE_CHARGE_LOG' => array(
      'HTT_MONTHLY_SERVICE_CHARGE_LOG_ID' => 'bigint', // primary key
      'CUSTOMER_ID'                       => 'bigint',
      'CREATED_DATETIME'                  => 'datetime',
      'NEXT_ATTEMPT_DATETIME'             => 'datetime',
      'ATTEMPT_COUNT'                     => 'tinyint',
      'PROCESS_ID'                        => 'int',
      'STATUS'                            => 'varchar',
      'MRC_GROUP_ID'                      => 'int' // FK to ULTRA.MRC_GROUPS.MRC_GROUP_ID
    ),

    'HTT_PLAN_TRACKER' => array(
      'HTT_PLAN_TRACKER_ID' => 'bigint', // primary key
      'CUSTOMER_ID'         => 'bigint',
      'COS_ID'              => 'int',
      'PLAN_STARTED'        => 'datetime',
      'PLAN_EXPIRES'        => 'datetime',
      'PERIOD'              => 'int',
      'PLAN_UPDATED'        => 'datetime'
    ),

    'HTT_TRANSITION_LOG' => array(
      'CUSTOMER_ID'               => 'bigint',
      'TRANSITION_UUID'           => 'VARCHAR,     128,    FALSE',
      'STATUS'                    => 'VARCHAR,     128,    FALSE',
      'CREATED'                   => 'datetime',
      'CLOSED'                    => 'datetime',
      'FROM_COS_ID'               => 'bigint',
      'FROM_PLAN_STATE'           => 'VARCHAR,     50,    FALSE',
      'TO_COS_ID'                 => 'bigint',
      'TO_PLAN_STATE'             => 'VARCHAR,     50,    FALSE',
      'CONTEXT'                   => 'VARCHAR,     1014,    FALSE',
      'HTT_ENVIRONMENT'           => 'VARCHAR,     25,    FALSE',
      'TRANSITION_LABEL'          => 'VARCHAR,     60,    FALSE',
      'PRIORITY'                  => 'tinyint'
    ),

    'HTT_TRANSITION_ARCHIVE' => array(
      'CUSTOMER_ID'               => 'bigint',
      'TRANSITION_UUID'           => 'varchar',
      'STATUS'                    => 'status',
      'CREATED'                   => 'datetime',
      'CLOSED'                    => 'datetime',
      'FROM_COS_ID'               => 'bigint',
      'FROM_PLAN_STATE'           => 'varchar',
      'TO_COS_ID'                 => 'bigint',
      'TO_PLAN_STATE'             => 'varchar',
      'CONTEXT'                   => 'varchar',
      'HTT_ENVIRONMENT'           => 'varchar',
      'TRANSITION_LABEL'          => 'varchar',
      'PRIORITY'                  => 'tinyint'
    ),

    'HTT_ACTION_LOG' => array(
      'ACTION_UUID'               => 'varchar',
      'TRANSITION_UUID'           => 'varchar',
      'STATUS'                    => 'status',
      'CREATED'                   => 'datetime',
      'CLOSED'                    => 'datetime',
      'ACTION_SEQ'                => 'int',
      'ACTION_TYPE'               => 'varchar',
      'ACTION_NAME'               => 'varchar',
      'ACTION_RESULT'             => 'varchar',
      'PENDING_SINCE'             => 'datetime',
      'ACTION_SQL'                => 'varchar'
    ),

    'HTT_ACTION_ARCHIVE' => array(
      'ACTION_UUID'               => 'varchar',
      'TRANSITION_UUID'           => 'varchar',
      'STATUS'                    => 'status',
      'CREATED'                   => 'datetime',
      'CLOSED'                    => 'datetime',
      'ACTION_SEQ'                => 'int',
      'ACTION_TYPE'               => 'varchar',
      'ACTION_NAME'               => 'varchar',
      'ACTION_RESULT'             => 'varchar',
      'PENDING_SINCE'             => 'datetime',
      'ACTION_SQL'                => 'varchar'
    ),

    'HTT_ACTION_PARAMETER_LOG' => array(
      'ACTION_UUID'               => 'varchar',
      'PARAM'                     => 'varchar',
      'VAL'                       => 'varchar'
    ),

    'HTT_ACTION_PARAMETER_ARCHIVE' => array(
      'ACTION_UUID'               => 'varchar',
      'PARAM'                     => 'varchar',
      'VAL'                       => 'varchar'
    ),

    'HTT_ULTRA_MSISDN' => array(
      'MSISDN'                    => 'varchar',
      'MSISDN_ACTIVATED'          => 'bit',
      'LAST_CHANGED_DATE'         => 'datetime',
      'CREATED_BY_DATE'           => 'datetime',
      'REPLACED_BY_MSISDN'        => 'varchar',
      'CUSTOMER_ID'               => 'bigint',
      'EXPIRES_DATE'              => 'datetime',
      'LAST_TRANSITION_UUID'      => 'varchar',
      'MVNE'                      => 'varchar'
    ),

    'HTT_CUSTOMERS_NOTES' => array(
      'HTT_CUSTOMERS_NOTES_ID' => 'bigint',
      'CUSTOMER_ID'            => 'bigint',
      'CREATED'                => 'datetime',
      'TYPE'                   => 'varchar',
      'SOURCE'                 => 'varchar',
      'USER'                   => 'varchar',
      'CONTENTS'               => 'varchar'
    ),

    'ULTRA.ACTIVATION_LOG_OVERRIDE' => array(
      'ACTIVATION_ICCID'          => 'varchar',
      'TYPE'                      => 'tinyint',
      'NEW_MASTERAGENT'           => 'bigint',
      'NEW_DISTRIBUTOR'           => 'bigint',
      'NEW_STORE'                 => 'bigint', // this is actually a dealer ID
      'NEW_USERID'                => 'bigint',
      'CREATED_BY'                => 'varchar',
      'CREATED_DATE_TIME'         => 'datetime',
      'NOTES'                     => 'text'
     ),

    'ULTRA.BRANDS' => array(
      'BRAND_ID'   => 'smallint',
      'BRAND_NAME' => 'varchar',
      'SHORT_NAME' => 'varchar'
    ),

    'ULTRA.CUSTOMER_OPTIONS' => array(
      'CUSTOMER_ID'        => 'bigint', // primary key
      'OPTION_ATTRIBUTE'   => 'varchar', // primary key, foreign key
      'OPTION_VALUE'       => 'varchar',
      'LAST_MOD_TIMESTAMP' => 'datetime',
      'LAST_MOD_USERNAME'  => 'varchar'
    ),

    // @see DEMO-1, http://wiki.hometowntelecom.com:8090/display/SPEC/Automation+of+Demo+Line+Program
    'ULTRA.DEMO_LINE' => array(
      'DEMO_LINE_ID'                  => 'int',       // primary key
      'DEALER_ID'                     => 'int',       // foreign key: tblDealerSite.DealerSiteID
      'DEALER_TYPE'                   => 'varchar',   // "1:1" or "EPP"
      'CUSTOMER_ID'                   => 'bigint',    // foreign key: HTT_CUSTOMERS_OVERLAY_ULTRA.CUSTOMER_ID
      'STATUS'                        => 'varchar',   // "eligible", "active", "warning", "invalid", "unclaimed"
      'ACTIVATIONS_NEEDED'            => 'int',       // monthly activation quota required for meet eligibility
      'CLAIM_BY_DATE'                 => 'datetime',  // eligibility expiration date
      'INVALID_DATE'                  => 'datetime'   // when line was cancelled 
    ),

    // @see http://wiki.hometowntelecom.com:8090/display/SPEC/MRC+support+for+processing+customers+according+to+multiple+rules
    'ULTRA.MRC_GROUPS' => array(
      'MRC_GROUP_ID'                    => 'int',     // PK
      'DESCRIPTION'                     => 'varchar', // group description
      'SQL_STATEMENT'                   => 'text',    // group selection SQL
      'EARLIEST_LOCAL_HOUR'             => 'tinyint'  // earliest subscribers local processing time (UTC)
    ),

    'ULTRA.PROMO_BROADCAST_CAMPAIGN' => array(
      'PROMO_BROADCAST_CAMPAIGN_ID'   => 'bigint',   // primary key
      'NAME'                          => 'varchar',  // unique name
      'STATUS'                        => 'varchar',
      'MESSAGE_EN'                    => 'varchar',  // message in English
      'MESSAGE_ES'                    => 'varchar',  // message in Spanish
      'MESSAGE_ZH'                    => 'varchar',  // message in Mandarin
      'STARTED_DATE'                  => 'datetime', // when should the promotional campaign start? (UTC)
      'CUSTOMER_CLAUSE'               => 'text',
      'PROMO_SHORTCODE'               => 'varchar',
      'PROMO_ENABLED'                 => 'bit',      // is the PROMO enabled?
      'PROMO_DESCRIPTION'             => 'varchar',
      'PROMO_EXPIRES_DATE'            => 'datetime',
      'PROMO_ACTION'                  => 'varchar',
      'PROMO_VALUE'                   => 'varchar',
      'CREATED_DATE'                  => 'datetime', // when was the promotional campaign created? (UTC)
      'EARLIEST_DELIVERY_TIME'        => 'char',     // Examples: 0730 , 0845 , ...
      'LATEST_DELIVERY_TIME'          => 'char',     // Examples: 2100 , 2000 , ...
      'MESSAGES_PER_HOUR'             => 'int',
      'SEND_TO_ACTIVE'                => 'bit',
      'SEND_TO_SUSPENDED'             => 'bit',
      'SEND_TO_MULTI_MONTH'           => 'bit',
      'SEND_TO_OPT_OUT'               => 'bit',
      'SEND_TO_AUTO_RENEWAL'          => 'tinyint',
      'BRAND_ID'                      => 'smallint'
    ),

    'ULTRA.SUMMARY_DEALER_RECHARGE_RATE' => array(
      'DEALER'                    => 'bigint',
      'ACTIVATION_PERIOD_STARTED' => 'datetime',
      'ACTIVATION_PERIOD_ENDED'   => 'datetime',
      'ACTIVATIONS'               => 'int',
      'REMAINING'                 => 'int',
      'RECHARGED'                 => 'int',
      'PERIOD_ONE_ENDED'          => 'bit',
      'SUSPEND_ENDED'             => 'bit',
      'RECHARGE_RATE'             => 'decimal'
    ),

    'ULTRA.HTT_ACTIVATION_HISTORY' => array(
      'CUSTOMER_ID'               => 'bigint',
      'ICCID_FULL'                => 'varchar',
      'MSISDN'                    => 'varchar',
      'COS_ID'                    => 'bigint',
      'MASTERAGENT'               => 'bigint',
      'DISTRIBUTOR'               => 'bigint',
      'DEALER'                    => 'bigint',
      'USERID'                    => 'bigint',
      'CREATION_DATE'             => 'datetime',
      'ACTIVATION_TYPE'           => 'varchar',
      'ACTIVATION_TRANSITION'     => 'varchar',
      'FINAL_STATE'               => 'varchar',
      'PLAN_STARTED_DATE_TIME'    => 'datetime',
      'FUNDING_SOURCE'            => 'varchar',
      'FUNDING_AMOUNT'            => 'decimal',
      'LAST_UPDATED_DATE'         => 'datetime',
      'LAST_ATTEMPTED_TRANSITION' => 'varchar',
      'PROMISED_AMOUNT'           => 'decimal'
    ),

    'ULTRA.WHOLESALE_PLAN_TRACKER' => array(
      'CUSTOMER_ID'               => 'bigint',
      'WHOLESALE_PLAN_ID'         => 'tinyint',
      'LAST_UPDATED_DATE'         => 'datetime'
    ),

    'ULTRA.SURCHARGE_HISTORY' => array(
      'SURCHARGE_HISTORY_ID'      => 'bigint', // primary key
      'SURCHARGE_TYPE'            => 'varchar',
      'TRANSACTION_ID'            => 'bigint',
      'AMOUNT'                    => 'decimal',
      'RULE'                      => 'varchar',
      'BASIS'                     => 'varchar',
      'LOCATION'                  => 'char',
      'STATUS'                    => 'varchar'
    ),

    // extended schema: type, length, nullable
    'ULTRA.USER_ERROR_MESSAGES' => array(
      'ERROR_CODE'                        => 'CHAR,     6,    FALSE',
      'EN_MESSAGE'                        => 'VARCHAR,  200,  FALSE',
      'ES_MESSAGE'                        => 'VARCHAR,  200,  TRUE',
      'ZH_MESSAGE'                        => 'VARCHAR,  200,  TRUE',
      'DETAILS'                           => 'VARCHAR,  200,  TRUE'
    ),

    'DP_RBAC.ACTIVITY' => array(
      'ACTIVITY_ID'               => 'int', // primary key
      'ACTIVITY_NAME'             => 'varchar',
      'ACTIVITY_TYPE_ID'          => 'int',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),

    'DP_RBAC.ACTIVITY_TYPE' => array(
      'ACTIVITY_TYPE_ID'          => 'int', // primary key
      'ACTIVITY_TYPE_NAME'        => 'varchar',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),
      
    'DP_RBAC.API' => array(
      'API_ID'                    => 'int', // primary key
      'API_NAME'                  => 'varchar',
      'API_DESC'                  => 'varchar',
      'API_ACTIVE_FLAG'           => 'bit',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),
      
    'DP_RBAC.API_ACTIVITY' => array(
      'API_ID'                    => 'int',
      'ACTIVITY_ID'               => 'int',
      'ACTIVE_FLAG'               => 'bit',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),
      
    'DP_RBAC.PLAN_STATE' => array(
      'PLAN_STATE_ID'             => 'int',
      'PLAN_STATE_NAME'           => 'varchar',
      'PLAN_STATE_DESC'           => 'varchar',
      'ACTIVE_FLAG'               => 'bit',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),

    'DP_RBAC.PLAN_STATE_ACTIVITY' => array(
      'PLAN_STATE_ID'             => 'int',
      'ACTIVITY_ID'               => 'int',
      'ACTIVE_FLAG'               => 'bit',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),
      
    'DP_RBAC.ROLE' => array(
      'ROLE_ID'                   => 'int',
      'ROLE_NAME'                 => 'varchar',
      'ROLE_DESC'                 => 'varchar',
      'ROLE_ACTIVE_FLAG'          => 'bit',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),

    'DP_RBAC.ROLE_ACTIVITY' => array(
      'ROLE_ID'                   => 'int',
      'ACTIVITY_ID'               => 'int',
      'ACTIVE_FLAG'               => 'bit',
      'LAST_MOD_TIMESTAMP'        => 'datetime',
      'LAST_MOD_USERNAME'         => 'varchar'
    ),

    'SOAP_PAID_EVENT' => array(
      'SOAP_LOG_ID'               => 'bigint', // foreign key to SOAP_LOG
      'MAKEITSO_QUEUE_ID'         => 'bigint', // foreign key to MAKEITSO_QUEUE
      'SOC_NAME'                  => 'varchar'
    ),

    'SOAP_LOG' => array(
      'SOAP_LOG_ID'               => 'bigint',   // primary key
      'SOAP_DATE'                 => 'datetime', // date of soap request/response
      'DATA_XML'                  => 'xml',      // XML contained in SOAP request
      'TYPE_ID'                   => 'tinyint',  // [1,2,3,4] : 1 = synchronous outboud (request) ; 2 = synchronous inbound (response) ; 3 = notification inbound ; 4 = notification outbound (response)
      'MSISDN'                    => 'char',
      'ICCID'                     => 'char',
      'TAG'                       => 'varchar',  // correlationID or UUID
      'SESSION_ID'                => 'varchar',  // serviceTransactionId node if available
      'COMMAND'                   => 'varchar',  // API name
      'ENV'                       => 'varchar',  // environment
      'RESULTCODE'                => 'int'       // ResultCode node if available
    ),
    
    'COMMAND_ERROR_CODES' => array(
      'COMMAND'                => 'varchar', // MW Command or Amdocs API
      'ERROR_CODE'             => 'varchar', // ResultCode returned by Amdocs APIs
      'ERROR_TYPE'             => 'tinyint', // 0 == 'unavailable' ; 1 == 'fatal'
      'UNAVAILABLE_ATTEMPTS'   => 'tinyint'  // number of repeated 'unavailable' errors before giving up
    ),

    'ULTRA.BOLTON_TRACKER' => array(
      'BOLTON_TRACKER_ID'         => 'bigint',  // primary key
      'CUSTOMER_ID'               => 'bigint',
      'SOURCE'                    => 'varchar', // Channel by which the request came to us. ("MRC", "CARE" ... ) - use domain list from HTT_BILLING_HISTORY
      'STATUS'                    => 'varchar', // "DONE", "ISF" (Insufficient Funds), "REFUND"
      'SKU'                       => 'varchar', // SKU name such as "500MB@$5.00"
      'PRODUCT'                   => 'varchar', // "IDDCA" , "DATA"
      'REQUEST_TYPE'              => 'varchar', // "MONTHLY" , "IMMEDIATE"
      'REQUEST_DATE'              => 'datetime',
      'HTT_PLAN_TRACKER_ID'       => 'bigint',  // Foreign Key ( HTT_PLAN_TRACKER.HTT_PLAN_TRACKER_ID )
      'TRANSACTION_ID'            => 'bigint'   // Foreign Key ( HTT_BILLING_HISTORY.TRANSACTION_ID )
    ),

    'ULTRA.CC_HOLDERS' => array(
      'CC_HOLDERS_ID'         => 'bigint',
      'CUSTOMER_ID'           => 'bigint',  // foreign key to CUSTOMERS
      'CREATED_DATE_TIME'     => 'datetime',
      'BIN'                   => 'char',    // first 6 digits
      'LAST_FOUR'             => 'char',    // last 4 digits
      'EXPIRES_DATE'          => 'char',    // CC expiration date
      'CVV_VALIDATION'        => 'tinyint', // CVV response code
      'AVS_VALIDATION'        => 'char',    // AVS response code
      'ENABLED'               => 'tinyint', // 0 if the credit card has been deactivated, customers cannot have more than 1 CC enabled
      'CUSTOMER_IP'           => 'varchar'
    ),

    'ULTRA.CC_HOLDER_TOKENS' => array(
      'CC_HOLDER_TOKENS_ID'   => 'bigint',
      'CC_HOLDERS_ID'         => 'bigint', // foreign key to [ULTRA].[CC_HOLDERS]
      'GATEWAY'               => 'char',
      'MERCHANT_ACCOUNT'      => 'char',
      'TOKEN'                 => 'varchar',
      'ENABLED'               => 'tinyint', // 0 if the credit card has been deactivated, customers cannot have more than 1 CC enabled
      'CUSTOMER_IP'           => 'varchar'
    ),
      'ICCID_BATCH_ID' => 'CHAR, 16, FALSE',

    'ULTRA.CC_TRANSACTIONS' => array(
      'CC_TRANSACTIONS_ID'        => 'BIGINT, , FALSE',     // will be used as 'invoice_number'
      'CC_HOLDER_TOKENS_ID'       => 'BIGINT, , FALSE',     // foreign key to [ULTRA].[CC_HOLDER_TOKENS]
      'TRANSACTION_ID'            => 'BIGINT, , TRUE',      // foreign key to HTT_BILLING_HISTORY
      'MERCHANT_TRANSACTION_ID'   => 'VARCHAR, 128, TRUE',  // Unique transaction Id provided by gateway/merchant
      'CUSTOMER_ID'               => 'BIGINT, , FALSE',     // foreign key to CUSTOMERS
      'CHARGE_AMOUNT'             => 'DECIMAL, , TRUE',
      'STATUS'                    => 'VARCHAR, 20, FALSE',  // INITIATED, SUBMITTED, COMPLETE
      'TYPE'                      => 'VARCHAR, 20, FALSE',  // aka COMMAND : VERIFY, CHARGE, REFUND
      'RESULT'                    => 'VARCHAR, 20, TRUE',   // APPROVED, TIMEOUT, FAILED, ...
      'CREATED_DATE_TIME'         => 'DATETIME, , FALSE',
      'COMPLETED_DATE_TIME'       => 'DATETIME, , TRUE',
      'DESCRIPTION'               => 'VARCHAR, 128, FALSE', // natural language description
      'CC_TRANSACTIONS_ID_PARENT' => 'BIGINT, , FALSE',     // foreign Key to [ULTRA].[CC_TRANSACTIONS] ( reference for VOIDed transaction )
      'ERR_DESCRIPTION'           => 'VARCHAR, 128, TRUE',
      'CUSTOMER_IP'               => 'VARCHAR, 24, TRUE',
      'DETAILS'                   => 'VARCHAR, 512, TRUE'   // transaction details: surchages, destination, description, source
    ),

    'ULTRA.PROMO_BROADCAST_LOG' => array(
      'PROMO_BROADCAST_LOG_ID'      => 'bigint', // primary key
      'PROMO_BROADCAST_CAMPAIGN_ID' => 'bigint', // foreign key to ULTRA.PROMO_BROADCAST_CAMPAIGN
      'CUSTOMER_ID'                 => 'bigint', // foreign key to HTT_CUSTOMERS_OVERLAY_ULTRA
      'MSG_STATUS'                  => 'varchar', // message delivery status for this customer 'SENT', 'ERROR'
      'MSG_DELIVERY_DATE'           => 'datetime', // when was the SMS message delivered successfully (UTC)
      'ATTEMPT_COUNT'               => 'tinyint', // how many times we attempted to deliver the SMS to the customer
      'PROMO_STATUS'                => 'varchar', // 'UNCLAIMED', 'CLAIMED'
      'PROMO_CLAIMED_DATE'          => 'datetime' // when was the promotion claimed (UTC)
    ),

    'ULTRA.PROMO_BROADCAST_ATTEMPT_LOG' => array(
      'PROMO_BROADCAST_ATTEMPT_LOG_ID' => 'bigint', // primary key
      'CUSTOMER_ID'                    => 'bigint', // foreign key to HTT_CUSTOMERS_OVERLAY_ULTRA
      'ATTEMPT_DATE'                   => 'datetime', // when did the customer attempt? (UTC)
      'PROMO_SHORTCODE'                => 'varchar',
      'RESULT'                         => 'varchar'
    ),

    // @see http://wiki.hometowntelecom.com:8090/pages/viewpage.action?pageId=17302412
    'ULTRA.WEBPOS_ACTIONS' => array(
      'WEBPOS_ACTIONS_ID'                 => 'BIGINT,   ,     FALSE',       // PK auto increment
      'CUSTOMER_ID'                       => 'BIGINT,   ,     FALSE',       // unique
      'UUID'                              => 'VARCHAR,  128,  FALSE',   // Ultra request ID
      'STATUS'                            => 'VARCHAR,  32,   FALSE',    // OPEN , DONE , VOID , REFUND , IN PROGRESS
      'CREATED'                           => 'DATETIME, ,     FALSE',     // when was this action created
      'CLOSED'                            => 'DATETIME, ,     TRUE',      // when was this action closed
      'VOIDED'                            => 'DATETIME, ,     TRUE',      // when (if) was this action closed
      'TYPE'                              => 'TINYINT,  ,     FALSE',      // ( 1 = Activation ; 2 = Port-In )
      'TRANSACTION_ID'                    => 'VARCHAR,  160,  FALSE',   // external payment provider transaction ID
      'SUBPRODUCT_ID'                     => 'VARCHAR,  32,   FALSE',    // commissioning indicator: L19, PQ19, L29, PQ29 etc
      'PLAN_NAME'                         => 'VARCHAR,  16,   FALSE',    // NINETEEN, TWENTY_NINE, THIRTY_NINE etc
      'AMOUNT'                            => 'DECIMAL,  ,     TRUE',       // US Dollars (not cents)
      'TMOBILE_CODE'                      => 'VARCHAR,  60,   FALSE',    // activating dealer T-Mobile unique code
      'PROVIDER_NAME'                     => 'VARCHAR,  60,   FALSE',    // external provider name (EPAY)
      'BOLT_ON_DATA'                      => 'VARCHAR,  32,   TRUE',     // data bolt on name requested with activation
      'BOLT_ON_INTL'                      => 'VARCHAR,  32,   TRUE',     // international bolt on name requested with activation
      'ICCID'                             => 'CHAR,     19,   FALSE',       // SIM
      'MSISDN'                            => 'VARCHAR,  10,   TRUE',     // MSISDN if portin
      'ZIPCODE'                           => 'CHAR,     5,    FALSE',        // zip code if not porting
      'LANGUAGE'                          => 'CHAR,     2,    FALSE',        // prefered language
      'PORT_CARRIER'                      => 'VARCHAR,  60,   TRUE',     // carrier code if porting
      'PORT_ACCOUNT_NUMBER'               => 'VARCHAR,  50,   TRUE',     // account if porting
      'PORT_ACCOUNT_PASSWORD'             => 'VARCHAR,  50,   TRUE',     // account password if porting
      'PORT_ACCOUNT_ZIPCODE'              => 'VARCHAR,  50,   TRUE'),    // account zip code if porting

    'REGISTRATION_ORIGIN_MAP' => array(
      'REGISTRATION_ORIGIN_MAP_ID' => 'int', // primary key
      'NODE_NAME'                  => 'varchar',
      'ORIGIN'                     => 'varchar',
      'PREFIX'                     => 'varchar',
      'CALLER_REGISTERED'          => 'bit',
      'TRIM_DIGITS_PREFIX'         => 'tinyint',
      'TRIM_DIGITS_SUFFIX'         => 'tinyint',
      'ADD_PREFIX'                 => 'varchar',
      'ADD_SUFFIX'                 => 'varchar',
      'ORIGIN_REGISTERED'          => 'varchar',
      'ORIGIN_NOT_REGISTERED'      => 'varchar',
      'DESCRIPTION'                => 'varchar',
      'PRIORITY'                   => 'tinyint',
      'REALM'                      => 'varchar'
    ),

    'ULTRA.IMEI_TACDB' => array(
      'IMEI_ABBR'   => 'varchar',
      'MAKE'        => 'varchar',
      'MODEL'       => 'varchar',
      'MODEL_NAME'  => 'varchar',
      'OS'          => 'varchar',
      'DEVICE_TYPE' => 'varchar',
      'BAND'        => 'varchar',
      'PREFIX_LEN'  => 'int'
    ),

    'ULTRA.INCOMM_INVENTORY_SIM' => array(
      'ICCID'           => 'varchar', // primary key
      'TYPE'            => 'varchar',
      'CREATED_DATE'    => 'datetime',
      'CREATED_BY'      => 'varchar',
      'EXPIRES_DATE'    => 'datetime',
      'REDEMPTION_DATE' => 'datetime',
      'CUSTOMER_ID'     => 'bigint',
      'STORED_VALUE'    => 'decimal',
      'START_DATE'      => 'datetime'
    ),

    'ULTRA.INVENTORY_SIM_OFFERS' => array(
      'OFFER_ID'     => 'tinyint',
      'OFFER_DETAIL' => 'varchar',
      'DURATION'     => 'tinyint'
    ),

    'ULTRA.ONLINE_ORDERS' => array(
      'ONLINE_ORDER_ID'    => 'bigint',
      'ICCID_FULL'         => 'char',
      'IMEI'               => 'varchar',
      'SKU'                => 'varchar',
      'FIRST_NAME'         => 'varchar',
      'LAST_NAME'          => 'varchar',
      'EMAIL'              => 'varchar',
      'ADDRESS1'           => 'varchar',
      'ADDRESS2'           => 'varchar',
      'CITY'               => 'varchar',
      'STATE'              => 'char',
      'POSTAL_CODE'        => 'char',
      'TOKEN'              => 'varchar',
      'BIN'                => 'char',
      'LAST_FOUR'          => 'char',
      'EXPIRES_DATE'       => 'char',
      'CVV_VALIDATION'     => 'char',
      'AVS_VALIDATION'     => 'char',
      'GATEWAY'            => 'char',
      'MERCHANT_ACCOUNT'   => 'char',
      'AUTO_ENROLL'        => 'bit',
      'CUSTOMER_IP'        => 'varchar',
      'PREFERRED_LANGUAGE' => 'char',
      'TRACK_TRACE'        => 'varchar'
    ),

    // Project W Port-In Migration DB table
    'ULTRA.UNIVISION_IMPORT' => array(
      'UNIVISION_IMPORT_ID'   => 'bigint',
      'CUSTOMER_ID'           => 'bigint', // foreign key to HTT_CUSTOMERS_OVERLAY_ULTRA
      'STATUS'                => 'varchar',
      'LAST_STATUS_DATE'      => 'datetime',
      'LAST_PROCESS_ID'       => 'int',
      'LAST_ERROR'            => 'varchar',
      'LAST_ERROR_DATE_TIME'  => 'datetime',
      'IMPORT_FILE_NAME'      => 'varchar',
      'IMPORT_FILE_DATE_TIME' => 'datetime',
      'IMPORT_FILE_STATUS'    => 'varchar',
      'COMPLETED_MIGRATION_DATE' => 'datetime',
      'ICCID_FULL'            => 'char',
      'TEMP_ICCID_FULL'       => 'char',
      'MSISDN'                => 'varchar',
      'FEATURE'               => 'varchar',
      'ZIP_CODE'              => 'char',
      'ADDRESS'               => 'varchar',
      'CITY'                  => 'varchar',
      'STATE'                 => 'varchar',
      'FIRST_NAME'            => 'varchar',
      'LAST_NAME'             => 'varchar',
      'ACCOUNT_NUMBER'        => 'varchar',
      'ACCOUNT_PIN'           => 'varchar',
      'PLAN_NAME'             => 'varchar',
      'LANGUAGE'              => 'char',
      'PLAN_STATE'            => 'varchar',
      'MONTHLY_RENEWAL_TARGET'              => 'varchar',
      'MONTHLY_RENEWAL_TARGET_REQUEST'      => 'varchar',
      'DATA_ALLOTTED'         => 'int',
      'DATA_USED'             => 'int',
      'DATA_REMAINING'        => 'int',
      'UNIVISION_CODE'        => 'varchar',
      'IMPORT_HISTORY'        => 'text',
      'DATA_ADJUSTED'         => 'bit'
    ),

    // Celluphone: Address record
    'TBLADDRESS' => array(
      'ADDRESSID'                     => 'int',
      'ADDRESS'                       => 'nvarchar',
      'CITY'                          => 'nvarchar',
      'ZIP'                           => 'nvarchar',
      'STATE'                         => 'nvarchar',
      'LONGITUDE'                     => 'nvarchar',
      'LATITUDE'                      => 'nvarchar',
      'SUITE'                         => 'nvarchar'),

    // Celluphone: Dealer business record
    'TBLDEALERSITE' => array(
      'DEALERSITEID'                  => 'int',
      'BUSINESSNAME'                  => 'nvarchar',
      'DEALERCD'                      => 'nvarchar',
      'ACTIVEFLAG'                    => 'bit',
      'MASTERID'                      => 'int',
      'CREATEDBYUSERID'               => 'int',
      'CREATEDDT'                     => 'smalldatetime',
      'LASTCHANGEBYUSERID'            => 'int',
      'LASTCHANGEDT'                  => 'smalldatetime',
      'NOTE'                          => 'nvarchar',
      'PARENTDEALERCD'                => 'nvarchar',
      'EPAY'                          => 'nvarchar',
      'EMIDA'                         => 'nvarchar',
      'LOCATIONPORTALFLAG'            => 'bit',
      'PARENTDEALERSITEID'            => 'int',
      'TMOBILECODE'                   => 'nvarchar',
      'INACTIVEREASONID'              => 'int',
      'PARENTDEALERSITEASSIGNDT'      => 'smalldatetime',
      'INACTIVEDT'                    => 'smalldatetime',
      'PRODUCTTYPE'                   => 'int',
      'PRODUCTTYPEDESC'               => 'varchar',
      'ONETOONEDEALERFLAG'            => 'bit'),

    // Dealer address reference
    'TBLDEALERSITEADDRESS' => array(
      'DEALERSITEID'                  => 'int',
      'ADDRESSID'                     => 'int',
      'ADDRESSTYPECD'                 => 'nvarchar',
      'EFFECTIVEDT'                   => 'smalldatetime',
      'EXPIRATIONDT'                  => 'smalldatetime'),

    // Celluphone: Master Agent and Sub Distributor business record
    'TBLMASTER' => array(
      'MASTERID'                      => 'int',
      'BUSINESSNAME'                  => 'nvarchar',
      'MASTERCD'                      => 'nvarchar',
      'PARENTMASTERID'                => 'int',
      'EFFECTIVEDT'                   => 'smalldatetime',
      'EXPIRATIONDT'                  => 'smalldatetime',
      'PAYMENTTYPEID'                 => 'int',
      'INSTACHECKAUTHFLAG'            => 'bit',
      'FEDTAXID'                      => 'nvarchar',
      'CREATEDDT'                     => 'smalldatetime',
      'CREATEDBYUSERID'               => 'int',
      'LASTCHANGEBYUSERID'            => 'int',
      'LASTCHANGEDT'                  => 'smalldatetime',
      'AXACCOUNTNUM'                  => 'nvarchar',
      'DATAAREAID'                    => 'nvarchar',
      'NOTE'                          => 'nvarchar'),

    // Master Agent and Sub Distributor address reference
    'TBLMASTERADDRESS' => array(
      'MASTERID'                      => 'int',
      'ADDRESSID'                     => 'int',
      'ADDRESSTYPECD'                 => 'nvarchar',
      'EFFECTIVEDT'                   => 'smalldatetime',
      'EXPIRATIONDT'                  => 'smalldatetime'),

    // Celluphone: MVNO business record
    'TBLNVO' => array(
      'NVOID'                         => 'int',
      'BUSINESSNAME'                  => 'nvarchar',
      'NVOCD'                         => 'nvarchar',
      'CORPID'                        => 'int',
      'CREATEDDT'                     => 'smalldatetime',
      'CREATEDBYUSERID'               => 'int',
      'NOTE'                          => 'nvarchar'),

    // MVNO address
    'TBLNVOADDRESS' => array(
      'NVOID'                         => 'int',
      'ADDRESSID'                     => 'int',
      'ADDRESSTYPECD'                 => 'nvarchar',
      'EFFECTIVEDT'                   => 'smalldatetime',
      'EXPIRATIONDT'                  => 'smalldatetime'),

    // report file base meta data by report type
    'TBLREPORTREQUESTTYPE' => array(
      'REPORTTYPEID'                  => 'int',
      'DESCRIPTION'                   => 'varchar',
      'BASEFILENAME'                  => 'varchar',
      'BASEFILEPATH'                  => 'varchar'),

    'TBLCOMMISSIONMISCPROD' => array(
      'COMMISSIONMISCID'              => 'int',
      'CUSTOMERID'                    => 'bigint',
      'TRANSACTIONID'                 => 'bigint',
      'TRANSACTIONDT'                 => 'smalldatetime',
      'ENTITYID'                      => 'int',
      'ORGLEVELID'                    => 'int',
      'COMMAMOUNT'                    => 'money',
      'TRANSACTIONTYPECD'             => 'varchar',
      'TRANSACTIONTYPEDESCR'          => 'varchar',
      'PAIDDT'                        => 'smalldatetime',
      'COMMISSIONSCHEDULEID'          => 'int',
      'COMMISSIONTYPEID'              => 'tinyint', // foreign key to tblCommissionType
      'COMMENTTEXT'                   => 'varchar',
      'CREATEDDT'                     => 'smalldatetime',
      'CREATEDBYUSERID'               => 'int'),

    // reporting periods
    'TBLCOMMISSIONSCHEDULE' => array(
      'COMMISSIONSCHEDULEID'          => 'int',
      'COMMISSIONTYPEID'              => 'tinyint', // foreign key to tblCommissionType
      'PAYPERIOD'                     => 'smalldatetime',
      'PAIDDT'                        => 'smalldatetime',
      'TRANSACTIONSTARTDT'            => 'smalldatetime',
      'TRANSACTIONENDDT'              => 'smalldatetime',
      'POSTEDDT'                      => 'smalldatetime',
      'SCHEDULETEXT'                  => 'varchar',
      'CREATEDDT'                     => 'smalldatetime',
      'SUBSEQUENTRECHARGEDT'          => 'smalldatetime'),
    'TBLCOMMISSIONSCHEDULEPROD' => array(
      'COMMISSIONSCHEDULEID'          => 'int',
      'COMMISSIONTYPEID'              => 'tinyint', // foreign key to tblCommissionType
      'PAYPERIOD'                     => 'smalldatetime',
      'PAIDDT'                        => 'smalldatetime',
      'TRANSACTIONSTARTDT'            => 'smalldatetime',
      'TRANSACTIONENDDT'              => 'smalldatetime',
      'POSTEDDT'                      => 'smalldatetime',
      'SCHEDULETEXT'                  => 'varchar',
      'CREATEDDT'                     => 'smalldatetime',
      'SUBSEQUENTRECHARGEDT'          => 'smalldatetime'),

    // generated commissions and activations (SIM) report file meta data
    'TBLREPORTREQUESTCOMMISSIONPROD' => array(
      'REPORTREQUESTID'               => 'int',
      'REPORTREQUESTTYPEID'           => 'int',
      'ENTITYID'                      => 'int',
      'ORGLEVELID'                    => 'int',
      'COMMISSIONSCHEDULEID'          => 'int',
      'FILEPATH'                      => 'varchar',
      'FILENAME'                      => 'varchar',
      'REQUESTDT'                     => 'smalldatetime',
      'REQUESTEXPIRATIONDT'           => 'smalldatetime',
      'REPORTREQUESTSTATUS'           => 'int',
      'ERRORMESSAGE'                  => 'varchar',
      'CREATEDDT'                     => 'smalldatetime',
      'USERID'                        => 'int',
      'REPORTGENERATEDDT'             => 'smalldatetime',
      'EMAILNOTIFICATION'             => 'varchar',
      'NONACTIVATEDFLAG'              => 'bit',
      'ACTIVATIONSTARTDT'             => 'smalldatetime',
      'ACTIVATIONENDDT'               => 'smalldatetime'),

    // Celluphone: User record
    'TBL_USER' => array(
      'USER_ID'                       => 'int',
      'USER_NAME'                     => 'varchar',
      'ACCESS_ROLE_ID'                => 'smallint',
      'IS_ACTIVE_FLAG'                => 'tinyint',
      'IS_INTERNAL_USER_FLAG'         => 'tinyint',
      'PASSWORD'                      => 'varchar',
      'FIRST_NAME'                    => 'varchar',
      'LAST_NAME'                     => 'varchar',
      'EMAIL_ADDRESS'                 => 'varchar',
      'CONTACT_NUMBER'                => 'char',
      'USER_EFFECTIVE_DT'             => 'smalldatetime',
      'USER_EXPIRATION_DT'            => 'smalldatetime',
      'USER_NOTE'                     => 'varchar',
      'LAST_CHANGE_BY_USER_ID'        => 'int',
      'LAST_CHANGE_DT'                => 'datetime',
      'PROFILE_LAST_UPDATE'           => 'smalldatetime',
      'ORGLEVELID'                    => 'int'),

    'TBLPERSON' => array(
      'PERSONID'                      => 'int',
      'FIRSTNAME'                     => 'nvarchar',
      'LASTNAME'                      => 'nvarchar',
      'PHONE'                         => 'nvarchar',
      'FAX'                           => 'nvarchar',
      'EMAIL'                         => 'nvarchar'),

    // dealer product type
    'TBLPRODUCTTYPE' => array(
      'ID'                            => 'int',
      'SIMTYPE'                       => 'nvarchar',
      'DESCRIPTION'                   => 'nvarchar',
      'CREATEDDT'                     => 'smalldatetime',
      'ACTIVEFLAG'                    => 'bit',
      'FLAGVALUE'                     => 'bigint'),

    // dealer portal: reasons for deactivation of dealers
    'TBLINACTIVEREASON' => array(
      'REASONID'                      => 'int',
      'REASON'                        => 'nvarchar',
      'ISACTIVE'                      => 'bit'),

    // dealerportal: user access log
    'TBL_USER_ACCESS_LOG' => array(
      'USER_ACCESS_LOG_ID'            => 'int',
      'USER_ID'                       => 'int',
      'DEALER_SITE_CD'                => 'varchar',
      'LOGIN_DATETIME'                => 'datetime',
      'LOGOUT_DATETIME'               => 'datetime',
      'IPADDRESS'                     => 'varchar',
      'HOST_NAME'                     => 'varchar',
      'SERVER_NAME'                   => 'varchar'
    )
  );
}


/**
 * getTableSchema
 *
 * get field attributes of a specific table (optionally only of the specific datatype)
 * @param string table name
 * @param string type of column 
 * @return array of (name => type) attributes
 * @author VYT, 14-01-24
 */
function getTableSchema($table, $datatype = NULL)
{
  $attributes = NULL;
  $schema = getSchema();

  if (isset($schema[$table]))
    $attributes = $schema[$table];
  else
    dlog('', "ERROR: not implemented getAttributes on table $table");

  // if datatype is given then return only columns of this type
  if ($datatype && $attributes)
    foreach ($attributes as $name => $type)
      if ($type != $datatype)
        unset($attributes[$name]);

  return $attributes;
}


/**
 * makeSelectQuery
 *
 * prepare a simple select query for a given table
 * @example: makeSelectQuery('DP_RBAC.ROLE_ACTIVITY', 2, array('ROLE_ID'), array('ACTIVITY_ID' => 3));
 *  -> SELECT TOP (2) ROLE_ID FROM DP_RBAC.ROLE_ACTIVITY WHERE ACTIVITY_ID = 3
 *
 * @param string table: table name
 * @param integer limit: optional number of rows to select
 * @param array attributes: optional array of fields to select
 * @param array clauses: array of field => value for WHERE clause
 * @return string: SQL query or NULL
 * @author: VYT, 14-01-24
 */
function makeSelectQuery($table, $limit=NULL, $attributes=NULL, $clauses = NULL, $having=NULL, $order=NULL, $nolock=FALSE)
{
  # dlog('',"table = $table , limit = $limit , attributes = %s , clauses = %s",$attributes,$clauses);
  
  if ((is_null($clauses) || ! is_array($clauses)) && $limit === NULL)
  {
    dlog('', "ERROR: clauses parameter cannot be NULL when no LIMIT given");
    return NULL;
  }

  // prepare limit
  $top = $limit && is_numeric($limit) ? "($limit)" : NULL;

  // prepare fields, default all
  // @todo: check that fields exist
  $fields = empty($attributes) ? '*' : (is_array($attributes) ? implode(', ', $attributes) : $attributes);

  // get schema
  $table = strtoupper($table);
  $schema = getTableSchema($table);
  if ( !$schema )
  {
    dlog('', "ERROR: no schema for table $table");
    return NULL;
  }

  // prepare and check if at least WHERE or TOP are present
  $where = makeWhereClause($schema, $clauses);
  if (! count($where) && ! $top)
  {
    dlog('', 'ERROR: no conditional clause given (attemp to select all rows)');
    return NULL;
  }

  // $having is TODO
  if ($having)
  {
    dlog('', 'ERROR: HAVING is currently not implemented');
    return NULL;
  }

  // precautional against out of memory errors
  if ( !$top && !count($where) )
    $top = 1000;

  // compose and return query
  $query = ' SELECT ' .
    ($top ? " TOP $top" : '') .
    " $fields FROM $table " .
    ($nolock ? ' WITH (nolock) ' : '' ) .
    (count($where) ? ' WHERE ' . implode(' AND ', $where) : '') .
    ($order ? ' ORDER BY ' . (is_array($order) ? implode(', ', $order) : $order) : '');

  return $query;
}


/**
 * makeUpdateQuery
 *
 * prepare a simple update query for a given table
 * @example: makeUpdateQuery('DP_RBAC.ROLE_ACTIVITY', array('ROLE_ID' => 5), array('ACTIVITY_ID' => 3)) -> UPDATE DP_RBAC.ROLE_ACTIVITY SET ROLE_ID = 5 WHERE ACTIVITY_ID = 3
 *
 * @param string table: table name
 * @param array values: array of field => value to for SET statement
 * @param array clauses: array of field => value for WHERE clause
 * @return string: SQL query or NULL
 * @author: VYT, 14-02-26
 */
function makeUpdateQuery($table, $values, $clauses)
{
  // get schema
  $table = strtoupper($table);
  $schema = getTableSchema($table);
  if ( !$schema )
  {
    dlog('', "ERROR: no schema for table $table");
    return NULL;
  }

  // prepare values
  $values = makeSqlValues($schema, $values);
  if ( ! count($values))
  {
    dlog('', "ERROR: no set values provided");
    return NULL;
  }
  $set = array();
  foreach ($values as $name => $value)
    if (is_integer($name)) // index with literal SQL string
      $set[] = $value;
    else
      $set[] = "$name = $value";

  // prepare and check WHERE clause
  $where = makeWhereClause($schema, $clauses);
  if (! count($where))
  {
    dlog('', "ERROR: no conditional clause given (attemp to select all rows)");
    return NULL;
  }

  // compose and return query
  $query = "UPDATE $table SET " . implode(', ', $set) . ' WHERE ' . implode(' AND ', $where);
  return $query;
}


/**
 * makeDeleteQuery
 * prepare a simple delete query for a given table
 * @example: makeDeleteQuery('DP_RBAC.ROLE_ACTIVITY', array('ACTIVITY_ID' => 3)) -> 'DELETE FROM DP_RBAC.ROLE_ACTIVITY WHERE ACTIVITY_ID = 3'
 * @param String table name
 * @param Array array of field => value WHERE clauses
 * @return String SQL query or NULL on failure
 * @author VYT, 15-03-01
 */
function makeDeleteQuery($table, $clauses)
{
  // get schema
  $table = strtoupper($table);
  $schema = getTableSchema($table);
  if ( !$schema )
  {
    dlog('', "ERROR: no schema for table $table");
    return NULL;
  }

  // prepare and check WHERE clause
  $where = makeWhereClause($schema, $clauses);
  if (! count($where))
  {
    dlog('', "ERROR: no conditional clause given (attemp to delete all rows)");
    return NULL;
  }

  // compose and return query
  $query = "DELETE FROM $table WHERE " . implode(' AND ', $where);
  return $query;
}


/**
 * makeInsertQuery
 *
 * prepare an insert query for a given table
 * @example: makeInsertQuery('DP_RBAC.ROLE_ACTIVITY', array('ROLE_ID' => 5, 'ACTIVITY_ID' => 3)) -> INSERT INTO DP_RBAC.ROLE_ACTIVITY (ROLE_ID, ACTIVITY_ID) VALUES (5, 3)
 *
 * @param string table: table name
 * @param array values: array of field => value for INSERT statement
 * @return string: SQL query or NULL
 * @author: VYT, 2014-12-04
 */
function makeInsertQuery($table, $values)
{
  // get schema
  $table = strtoupper($table);
  $schema = getTableSchema($table);
  if ( !$schema )
  {
    dlog('', "ERROR: no schema for table $table");
    return NULL;
  }

  // prepare values
  $data = makeSqlValues($schema, $values);
  if (! count($data))
  {
    dlog('', "ERROR: insert values provided");
    return NULL;
  }

  // separate NAMES and VALUES
  $names = array();
  $values = array();
  foreach ($data as $name => $value)
  {
    $names[] = $name;
    $values[] = $value;
  }

  // compose and return query
  $query = "INSERT INTO $table (" . implode(', ', $names) . ') VALUES (' . implode(', ', $values) . ')';
  return $query;
}


/**
 * makeWhereClause
 *
 * internal; prepare NAME => VALUE pairs according to table schema; if NAME is not given then use VALUE as is
 * @param array schema: table schema
 * @param array columns: array of field => value for WHERE clause
 * @result array of string NAME => VALUE pairs
 * @author: VYT, 14-02-26
 */
function makeWhereClause($schema, $clauses)
{
  $where = array();

  // process each value according to schema
  if (is_array($clauses) && count($clauses))
  {
    foreach ($clauses as $name => $value)
    {
      $name = strtoupper($name);
      if (isset($schema[$name]) && isset($value))
      {
        // handle NULL for all data types
        if ($value == 'NULL')
          $where[] = "$name IS NULL";
        else
        {
          // parse schema definition and handle each type
          list($type) = parseColumnDefinition($schema[$name]);
          switch(strtolower($type))
          {
            case 'char':
            case 'varchar':
            case 'text':
              if ( is_array($value) )
                $where[] = $name . ' IN (' . implode(',', array_map("mssql_escape_with_zeroes", $value)) . ')';
              elseif ( preg_match("/^gt_(\w+)$/", $value , $matches ) )
                $where[] = sprintf("%s > '%s'", $name, $matches[1]);
              elseif ( preg_match("/^lt_(\w+)$/", $value , $matches ) )
                $where[] = sprintf("%s < '%s'", $name, $matches[1]);
              elseif ( preg_match("/^gteq_(\w+)$/", $value , $matches ) )
                $where[] = sprintf("%s >= '%s'", $name, $matches[1]);
              elseif ( preg_match("/^lteq_(\w+)$/", $value , $matches ) )
                $where[] = sprintf("%s <= '%s'", $name, $matches[1]);
              else
                $where[] = sprintf("%s = %s", $name, mssql_escape_with_zeroes($value));
              break;

            // VYT: for the lack of more elegant solution to avoid SQL injection
            case 'nchar':
            case 'nvarchar':
              $where[] = sprintf('%s = %s', $name, mssql_escape_with_zeroes($value, TRUE));
              break;

            case 'bit':
            case 'bigint':
            case 'decimal':
            case 'int':
            case 'numeric':
            case 'smallint':
            case 'tinyint':
              if (is_array($value))
              {
                foreach ($value as &$item)
                  $item = sprintf('%d', $item);
                $where[] = $name . ' IN (' . implode(',', $value) . ')';
              }
              else if ( preg_match("/^gt_(\d+)$/", $value , $matches ) )
                $where[] = sprintf("%s > %d", $name, $matches[1]);
              else if ( preg_match("/^lt_(\d+)$/", $value , $matches ) )
                $where[] = sprintf("%s < %d", $name, $matches[1]);
              else
                $where[] = sprintf("%s = %d", $name, $value);
              break;

            case 'date':
            case 'datetime':
              if (is_array($value))
                $where[] = "$name BETWEEN '{$value[0]}' AND '{$value[1]}'";
              elseif ( preg_match("/^past_(\d+)_hours$/", $value , $matches ) )
                $where[] = "DATEDIFF( mi, $name , GETUTCDATE() ) >= ".(60*$matches[1])." ";
              elseif ( preg_match("/^past_(\d+)_minutes$/", $value , $matches ) )
                $where[] = "DATEDIFF( mi, $name , GETUTCDATE() ) >= ".$matches[1]." ";
              elseif ( preg_match("/^last_(\d+)_days$/", $value , $matches ) )
                $where[] = "DATEDIFF( dd, $name , GETUTCDATE()) <= ".$matches[1]." ";
              elseif ( preg_match("/^past_(\d+)_days$/", $value , $matches ) )
                $where[] = "DATEDIFF( dd, $name, GETUTCDATE() ) >= " . $matches[1] . " ";
              elseif ( preg_match("/^last_(\d+)_hours$/", $value , $matches ) )
                $where[] = "DATEDIFF( mi, $name, GETUTCDATE() ) <= " . (60*$matches[1]) . " ";
              elseif ( preg_match("/^last_(\d+)_minutes$/", $value , $matches ) )
                $where[] = "DATEDIFF( mi, $name, GETUTCDATE() ) <= " . $matches[1] . " ";
              elseif ( $value == 'past' )
                $where[] = "$name < GETUTCDATE()";
              elseif ( $value == 'future' )
                $where[] = "$name > GETUTCDATE()";
              else
                $where[] = "$name = '$value'";
              break;

            default:
              dlog('', "ERROR: not implemented makeWhereClause on data type $type ({$schema[$name]})");
              return NULL;
          }
        }
      }
      elseif (is_numeric($name)) // no schema name given: literal string
        $where[] = $value;
      else
        dlog('', "ERROR: cannot find column $name");
    }
  }

  return $where;
}


/**
 * makeSqlValues
 *
 * internal; prepare NAME => VALUE pairs according to table schema used for UPDATE or INSERT queries
 * @param array schema: table schema
 * @param array values: array of field => value for SET statement
 * @result array of NAMES => VALUES
 * @author: VYT, 14-02-26
 */
function makeSqlValues($schema, $values)
{
  // process each value according to schema
  $set = array();
  foreach ($values as $name => $value)
  {
    if (is_integer($name) && is_string($value)) // index with literal SQL string
      $set[] = $value;
    else
    {
      $name = strtoupper($name);
      if (isset($schema[$name]))
      {
        // handle NULL for all data types
        if ($value === 'NULL' || $value === NULL)
          $set[$name] = 'NULL';
        else
        {
          list($type) = parseColumnDefinition($schema[$name]);
          switch(strtolower($type))
          {
            case 'char':
            case 'varchar':
              $set[$name] = mssql_escape_with_zeroes($value);
              break;

            // VYT: for the lack of more elegant solution to avoid SQL injection
            case 'nchar':
            case 'nvarchar':
              $set[$name] = mssql_escape_with_zeroes($value, TRUE);
              break;

            case 'text':
              $set[$name] = sprintf("'%s'", $value);
              break;

            case 'bit':
            case 'bigint':
            case 'decimal':
            case 'int':
            case 'numeric':
            case 'smallint':
            case 'tinyint':
              $set[$name] = sprintf('%d', $value);
              break;

            // money is a decimal with 2 trailing digits
            case 'money':
            case 'smallmoney':
              $set[$name] = sprintf('%.2f', $value);
              break;

            case 'smalldatetime':
            case 'datetime':
              if ($value == 'NOW')
                $set[$name] = 'GETUTCDATE()';
              elseif (ctype_digit($value)) // UNIX time
                $set[$name] = "'" . gmdate(MSSQL_DATE_FORMAT, $value) . "'";
              else
                $set[$name] = "'$value'";
              break;

            default:
              dlog('', "ERROR: not implemented makeSetValues on data type $type {$schema[$name]}");
              return NULL;
          }
        }
      }
      else
        dlog('', "WARNING: column $name does not exist");
    }
  }

  return $set;
}


/**
 * makeParameterizedInsertStatement
 * create parametrized INSERT statement string (without actually preparing it against DB)
 * this function required extended schema definition
 * @param String table
 * @param Array of names => values
 * @return String on success, NULL on failure
 * @author VYT, 15-11-05
 */
function makeParameterizedInsertStatement($table, $values)
{
  // get table schema
  $table = strtoupper($table);
  if ( ! $schema = getTableSchema($table))
    return logError("missing schema for table $table");

  // prepare values
  $values = makeSqlValues($schema, $values);
  if ( ! count($values))
    return logError('failed to prepare values');

  // prepare declarations, columns and parameter names
  $declarations = array();
  $columns = array();
  $params = array();
  $index = 0;
  foreach ($values as $column => $value)
  {
    list($type, $length) = parseColumnDefinition($schema[$column]);
    $declarations[] = 'DECLARE @P' . $index . ($length ? " $type($length)" : " $type") . " = $value";
    $columns[] = "[$column]";
    $params[] = '@P' . $index++;
  }

  // compose and return query
  $declarations = implode('; ', $declarations);
  $columns = implode(', ', $columns);
  $params = implode(', ', $params);
  return "$declarations; INSERT INTO $table ($columns) VALUES ($params)";
}


/**
 * makeParameterizedSelectStatement
 * create parametrized SELECT statement string (without actually preparing it against DB)
 * this function required extended schema definition
 * @param String table name
 * @param Integer limit
 * @param Array attributes to select
 * @param Array name => value clauses
 * @param Array order clauses
 * @param Boolean lock
 * @return String query on success, NULL on failure
 * @author VYT, 15-11-05
 */
function makeParameterizedSelectStatement($table, $limit = NULL, $attributes = NULL, $clauses =  NULL, $having = NULL, $order = NULL, $nolock = TRUE)
{
  // get table schema
  $table = strtoupper($table);
  if ( ! $schema = getTableSchema($table))
    return logError("missing schema for table $table");

  // do not allow to select all rows
  if ( ! $limit && empty($clauses))
    return logError('limit and clauses parameter cannot be NULL (attemtp to select all rows)');

  // prepare limit, default is 1k
  $top = $limit && is_numeric($limit) ? " TOP ($limit)" : 'TOP (1000)';

  // prepare values
  $values = makeSqlValues($schema, $clauses);
  if ( ! count($values))
    return logError('failed to prepare values');

  // prepare declarations and parameter names
  $declarations = array();
  $parameters = array();
  $index = 0;
  foreach ($values as $column => $value)
  {
    list($type, $length) = parseColumnDefinition($schema[$column]);
    $parameter = '@P' . $index++;
    $declarations[] = "DECLARE $parameter " . ($length ? "$type($length)" : "$type") . "=$value";
    $parameters[] = "[$column]=$parameter";
  }

  // TODO
  if ( ! empty($having))
    logError('sorry, HAVING is not implemented yet');
  if ( ! empty($order))
    logError('sorry, ORDER BY not implemented yet');

  // prepare attributes
  if ($attributes && $attributes != '*')
  {
    // validate attributes
    if ( ! is_array($attributes)) // single attribute
      $attributes = array($attributes);
    $temp = array();
    foreach ($attributes as $attribute)
      if ( ! isset($schema[$attribute]))
        return logError("invalid attribute $attribute");
      else
        $temp[] = "[$attribute]";
    $attributes = implode(',', $temp);
  }
  else
    $attributes = '*';

  // compose and return query
  $declarations = implode('; ', $declarations);
  $lock = $nolock ? 'WITH (NOLOCK)' : NULL;
  $parameters = implode(' AND ', $parameters);
  return "$declarations; SELECT $top $attributes FROM $table $lock WHERE $parameters";
}


/**
 * parseColumnDefinition
 * split extended column definition CSV of format 'TYPE, LENGTH, NULLABLE' or 'TYPE' into individual components
 * @param String column definition
 * @return Array (TYPE, LENGTH, NULLABLE)
 */
function parseColumnDefinition($definition)
{
  // defaults
  $type = NULL;
  $length = NULL;
  $nullable = NULL;

  // remove all spaces from CSV
  $definition = str_replace(' ', '', strtoupper($definition));

  // depending on schema we might get one (type only) or three (type, length, nullable) parts
  $parts = explode(',', $definition);
  if (count($parts) == 3)
  {
    list($type, $length, $nullable) = $parts;
    $nullable = $nullable == 'TRUE' ? TRUE : FALSE;
  }
  elseif (count($parts) == 1)
    list($type) = $parts;
  else
    logError("invalid column definition $definition");

  return array($type, $length, $nullable);
}

