<?php

namespace Ultra\Lib\DB;

/*

Main class for accessing Solr

See http://wiki.hometowntelecom.com:8090/pages/viewpage.action?pageId=17303027

*/

require_once 'classes/Outcome.php';

class Solr extends \Outcome
{
  private $config;

  private static $solrClient;

  public function __construct()
  {
    if ( empty( self::$solrClient ) )
    {
      try
      {
        $this->config = \Ultra\UltraConfig\solrCredentials();

        if ( ! empty( $this->config['hostname'] ) && is_array( $this->config['hostname'] ) )
          $this->config['hostname'] = $this->config['hostname'][0];

        \logDebug( json_encode( $this->config ) );

        // establish connection
        self::$solrClient = new \SolrClient( $this->config );
      }
      catch ( \Exception $e )
      {
        \logError( 'Error connecting to Solr with config=' . json_encode( $this->config ) );
        $this->add_errors_and_code( $e->getMessage() , 'IN0002' );
      }
    }
  }

  /**
   * query
   *
   * Queries the Solr database and sets the data_array[documents] attribute to the results
   * Term is a query string field:value AND|OR field:value
   *
   * @return boolean
   */
  public function query( $term, $limit=null, $offset=null, array $filter=array() )
  {
    $this->succeed();

    try
    {
      $query = new \SolrQuery( $term );

      // filter
      foreach ( $filter as $f )
      {
        $query->addFilterQuery( $f );
      }

      // limit
      if ( !empty( $limit ) && ctype_digit( (string)$limit ) )
      {
        $query->setRows( $limit );
      }

      // offset
      if ( !empty( $offset ) && ctype_digit( (string)$offset ) )
      {
        $query->setStart( $offset );
      }

      // run query
      $result = self::$solrClient->query( $query );
      $response = $result->getResponse();

      $documents = array();
      if ( is_object( $response )
        && !empty( $response->response )
        && property_exists( $response->response, 'numFound' ) 
      ) {
        if ( $response->response->numFound > 0
          && !empty( $response->response->docs )
          && is_array( $response->response->docs )
          && count( $response->response->docs )
        ) {
          foreach ( $response->response->docs as $doc )
          {
            $docArr = (array)$doc;

            // unset version field
            unset( $docArr['_version_'] );

            $documents[] = $docArr;
          }
        }
        else
        {
          // no documents found
        }
      }
      else
      {
        // error querying Solr
        $this->add_errors_and_code( 'Error querying Solr', 'DB0001' );
      }

      $this->data_array['documents'] = $documents;
    }
    catch ( \Exception $e )
    {
      $this->add_errors_and_code( $e->getMessage() , 'IN0002' );
    }

    return $this->is_success();
  }

  /**
   * addDocument
   *
   * Adds a document to the Solr database
   *
   * @return boolean
   */
  public function addDocument( array $documentArr )
  {
    $this->succeed();

    try
    {
      $doc = new \SolrInputDocument();
      foreach ( $documentArr as $key => $val )
      {
        $doc->addField( $key, $val );
      }
      
      $result = self::$solrClient->addDocument( $doc );
      self::$solrClient->commit();
      $response = $result->getResponse();

      if ( !empty( $response->responseHeader )
        && property_exists( $response->responseHeader, 'status' ) 
        && $response->responseHeader->status == 0
      ) {
        // success
        return true;
      }
      else
      {
        // failed
        $this->add_errors_and_code( 'Error adding document to Solr', 'DB0001' );
      }
    }
    catch ( \Exception $e )
    {
      $this->add_errors_and_code( $e->getMessage() , 'IN0002' );
    }

    return $this->is_success();
  }

  /**
   * deleteById
   *
   * Removes a document from the Solr database by the uniqueKey value
   *
   * @return boolean
   */
  public function deleteById( $id )
  {
    $this->succeed();

    try
    {
      $result = self::$solrClient->deleteById( $id );
      self::$solrClient->commit();
      $response = $result->getResponse();

      if ( !empty( $response->responseHeader )
        && property_exists( $response->responseHeader, 'status' ) 
        && $response->responseHeader->status == 0
      ) {
        // success
        return true;
      }
      else
      {
        // failed
        $this->add_errors_and_code( 'Error removing document to Solr', 'DB0001' );
      }
    }
    catch ( \Exception $e )
    {
      $this->add_errors_and_code( $e->getMessage() , 'IN0002' );
    }

    return $this->is_success();
  }
}

