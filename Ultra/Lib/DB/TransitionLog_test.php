<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/TransitionLog/Archive.php';

teldata_change_db();

$error = \Ultra\Lib\DB\TransitionLog\Archive\un_archive( $argv[1] );

if ( $error )
  echo "$error\n";
else
  echo "success!\n";

?>
