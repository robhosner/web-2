package Ultra::Lib::DB::SoapLog;


use strict;
use warnings;


use base qw(Ultra::Lib::DB::MSSQL);


use Data::Dumper;
use Scalar::Util;


=head1 NAME

Ultra::Lib::DB::SoapLog

=head1 Perl module to access DB table SOAP_LOG

=head1 SYNOPSIS

  use Ultra::Lib::DB::SoapLog;

  my $db = Ultra::Lib::DB::MSSQL->new();

  my $dbSoapLog = Ultra::Lib::DB::SoapLog->new( DB => $db );

  my $success = $dbSoapLog->insertSoapLog($params);

=cut

sub insertSoapPaidEvent
{
  my ($this,$params) = @_;

  if ( ( ! defined $params->{ soap_log_id } ) || ( $params->{ soap_log_id } eq '' ) )
  {
    $this->addError("SOAP_LOG_ID is required");
  }

  if ( ( ! defined $params->{ soc_name } ) || ( $params->{ soc_name } eq '' ) )
  {
    $this->addError("SOC_NAME is required");
  }

  return 0 if $this->hasErrors();

  my $makeitso_queue_id = ( $params->{ makeitso_queue_id } ) ? $params->{ makeitso_queue_id } : '' ;

  my $columns = [qw/
    SOAP_LOG_ID
    SOC_NAME
  /];

  my $placeholders = [
    '?',
    '?',
  ];

  my $values = [
    $params->{ soap_log_id },
    $params->{ soc_name }
  ];

  if ( ( defined $params->{ makeitso_queue_id } ) && $params->{ makeitso_queue_id } )
  {
    push( @$columns , 'MAKEITSO_QUEUE_ID' );
    push( @$values  , $params->{ makeitso_queue_id } );
    push( @$placeholders , '?');
  }

  my $db_table = 'SOAP_PAID_EVENT';

  my $sql = " INSERT INTO $db_table
    ( " . join(',',@$columns) . '
    )
    VALUES
    ( ' . join(',',@$placeholders) . '
    ) ';

  my ($success, $rows) = $this->{ DB }->executeWrite($sql,$values);

  if ( $this->{ DB }->hasErrors() )
  {
    $this->addErrors( $this->{ DB }->getErrors() );

    $this->{ DB }->writeToRedoLog( $this->getRedoLogName() , $sql , $values );
  }

  return $success;
}

=head4 insertSoapLog

  Insert a row into SOAP_LOG

=cut
sub insertSoapLog
{
  my ($this,$params) = @_;

  #$this->log( "insertSoapLog invoked with params = ".$this->{ JSON_CODER }->encode( $params ) );

  if ( ( ! defined $params->{ data_xml } ) || ( $params->{ data_xml } eq '' ) )
  {
    $this->addError("DATA_XML is required");
  }

  if ( ( ! defined $params->{ command } ) || ( $params->{ command } eq '' ) )
  {
    $this->addError("COMMAND is required");
  }

  if ( ( ! defined $params->{ type_id } ) || ( $params->{ type_id } !~ /^\s*[1234]\s*$/ ) )
  {
    $this->addError("TYPE_ID is invalid");
  }

  return 0 if $this->hasErrors();

  my $environment = ( $ENV{ HTT_ENV } ) ? $ENV{ HTT_ENV } : 'NO_ENV_GIVEN' ;

  my $columns = [qw/
    DATA_XML
    COMMAND
    SESSION_ID
    TYPE_ID
    ENV
  /];

  my $placeholders = [
    '?',
    '?',
    '?',
    '?',
    '?',
  ];

  my $values = [
    $params->{ data_xml },
    $params->{ command },
    $params->{ session_id },
    $params->{ type_id },
    $environment,
  ];

  if ( ( defined $params->{ tag } ) && $params->{ tag } )
  {
    push( @$columns , 'TAG' );
    push( @$values  , $params->{ tag } );
    push( @$placeholders , '?');
  }

  if ( ( defined $params->{ msisdn } ) && $params->{ msisdn } )
  {
    push( @$columns , 'MSISDN' );
    push( @$values  , $params->{ msisdn } );
    push( @$placeholders , '?');
  }

  if ( ( defined $params->{ iccid } ) && $params->{ iccid } )
  {
    push( @$columns , 'ICCID' );
    push( @$values  , $params->{ iccid } );
    push( @$placeholders , '?');
  }

  if ( ( defined $params->{ ResultCode } ) && $params->{ ResultCode } )
  {
    push( @$columns , 'RESULTCODE' );
    push( @$values  , $params->{ ResultCode } );
    push( @$placeholders , '?');
  }

  if ( ( defined $params->{ soap_date_add_time_ms } ) && $params->{ soap_date_add_time_ms } && Scalar::Util::looks_like_number( $params->{ soap_date_add_time_ms } ) )
  {
    push( @$columns      , 'SOAP_DATE'    );
    push( @$placeholders , 'DATEADD(ms, '.$params->{ soap_date_add_time_ms }.', GETUTCDATE())' );
  }

  my $db_table = 'SOAP_LOG';

  my $sql = " INSERT INTO $db_table
    ( " . join(',',@$columns) . '
    )
    VALUES
    ( ' . join(',',@$placeholders) . '
    );
    SELECT SCOPE_IDENTITY() as SOAP_LOG_ID;';

  my ($success, $rows) = $this->{ DB }->executeWrite($sql,$values,1);

  if ( $this->{ DB }->hasErrors() )
  {
    $this->addErrors( $this->{ DB }->getErrors() );

    $this->{ DB }->writeToRedoLog( $this->getRedoLogName() , $sql , $values );
  }

  my $soap_log_id = ( defined $rows->[0]->{ SOAP_LOG_ID } ) ? $rows->[0]->{ SOAP_LOG_ID } : '' ;

  return ($success, $soap_log_id);
}


sub getRedoLogName
{
  my ($this) = @_;

  return $this->{ CONFIG }->soapLogRedoLog();
}


1;


__END__

For SELECT use this syntax:
 CAST( DATA_XML AS VARCHAR(1000) )

 SELECT SOAP_LOG_ID, SOAP_DATE, ICCID, MSISDN, CAST( DATA_XML AS VARCHAR(1000) ), COMMAND, SESSION_ID, TYPE_ID, TAG, ENV FROM ULTRA_ACC..SOAP_LOG;

