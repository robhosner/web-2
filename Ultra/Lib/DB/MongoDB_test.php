<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/MongoDB.php';
include_once('test/api/test_api_include.php');

/*
usage
php Ultra/Lib/DB/MongoDB_test.php getMongoDBDocuments key value key value key value 
php Ultra/Lib/DB/MongoDB_test.php getMongoDBDocumentsMatchList
php Ultra/Lib/DB/MongoDB_test.php addMongoDBDocument $COLLECTION 'key,value,key,value'
php Ultra/Lib/DB/MongoDB_test.php removeMongoDBDocument $COLLECTION key value
php Ultra/Lib/DB/MongoDB_test.php updateMongoDBDocument 'needle_key' needle_value 'new_key' 'new_value'
php Ultra/Lib/DB/MongoDB_test.php upsert
php Ultra/Lib/DB/MongoDB_test.php batchInsertMongoDBDocuments (modify array in test method)
php Ultra/Lib/DB/MongoDB_test.php findOneMongoDBDocument (modify arrays $query, $field in test method)
php Ultra/Lib/DB/MongoDB_test.php importJSON $COLLECTION $FILENAME
php Ultra/Lib/DB/MongoDB_test.php exportJSON $COLLECTION
php Ultra/Lib/DB/MongoDB_test.php findAll $COLLECTION $LIMIT $FIELDS
php Ultra/Lib/DB/MongoDB_test.php findSome $COLLECTION key value
php Ultra/Lib/DB/MongoDB_test.php findAndModifyMongoDBDocument (modify arrays $query, $update, $fields in test method)
php Ultra/Lib/DB/MongoDB_test.php groupMongoDBDocuments (modify arrays $keys, $initial, $func ($func takes a JS function for grouping data) in test method)
php Ultra/Lib/DB/MongoDB_test.php group
php Ultra/Lib/DB/MongoDB_test.php createIndexMongoDBDocument
php Ultra/Lib/DB/MongoDB_test.php getIndexInfoMongoDBDocument
php Ultra/Lib/DB/MongoDB_test.php deleteIndexMongoDBDocument key_index_to_delete
php Ultra/Lib/DB/MongoDB_test.php aggregate
php Ultra/Lib/DB/MongoDB_test.php pushMongoDBDocument
php Ultra/Lib/DB/MongoDB_test.php pullFromListInMongoDBDocument
php Ultra/Lib/DB/MongoDB_test.php showAllIndexes
php Ultra/Lib/DB/MongoDB_test.php connectMongoDBEvents
*/

class TestMongoBase
{
  public $dbObject;
  
  public function __construct()
  {
    $this->dbObject = \Ultra\Lib\DB\selectMongoDB( array( 'db_name' => 'primodb' ) );
  }
}

class Test_connectMongoDBEvents extends TestMongoBase
{
  function test()
  {
    global $argv;

    $mongoClientObject = \Ultra\Lib\DB\connectMongoDBEvents( array() );

    print_r( $mongoClientObject );

    if ( ! $mongoClientObject )
    {
      echo "Failed to create Mongo Client Object\n";
      exit;
    }

    $dbObject = $mongoClientObject->selectDB('primodb');

    print_r($dbObject);

    $collection = $dbObject->Testing;

    print_r( $collection );

    $cursor = $collection->find( array() );

    $c = 0;
    // iterate cursor to display title of documents
    foreach ($cursor as $document)
    {
      $c++;
      print_r($document);
    }
    echo "$c documents found\n";
  }
}

class Test_showAllIndexes extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collections = array(
      'UserDirectory',
      'AddressBooks',
      'SynchronizationTasks',
      'LoginTokens',
      'UserErrors',
      'LoginHistory',
      'TransitionLog'
    );

    foreach( $collections as $collection_name )
    {
      $collection = $this->dbObject->$collection_name;

      $outcome = \Ultra\Lib\DB\getIndexInfoMongoDBDocument( $collection );

      echo PHP_EOL . PHP_EOL . $collection_name . PHP_EOL;

      foreach( $outcome->data_array as $index_info )
        if ( $index_info['name'] != '_id_' )
        {
          $index_info['fields'] = implode( ' , ' , array_keys( $index_info['key'] ) );
          unset( $index_info['key'] );
          print_r( $index_info );
        }
    }
  }
}

class Test_group extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $keys      = array( 'os' => 1 , 'de' => 1 );
    $initial   = array( "count" => 0 );
    $reduce    = "function (obj, prev) { prev.count++; }";
    $condition = array( 'un' => 'a1421086852' );

    $results = $collection->group( $keys, $initial, $reduce, $condition );
    
    print_r($results);
  }
}

class Test_pushMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $criteria = array(
      'un' => 'a1426692455'
    );
    $listName = 'ts';
    $element  = 'raftest_'.time();

    $results = \Ultra\Lib\DB\pushMongoDBDocument( $collection , $criteria , $listName , $element );

    $results = \Ultra\Lib\DB\pushMongoDBDocument( $collection , $criteria , $listName , array('a','b'));

    print_r($results);
  }
}

class Test_pullFromListInMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $criteria = array(
      'un' => 'a1426692455'
    );
    $listName = 'ts';

    $element  = 'test_1426804352';

    $results = \Ultra\Lib\DB\pullFromListInMongoDBDocument( $collection , $criteria , $listName , $element );

    print_r($results);
  }
}

class Test_aggregate extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $ops = array(
      array(
        '$group' => array(
          '_id'    => array('os' => '$os', 'de' => '$de' ),
          'count'  => array('$sum' => 1)
        )
      )
    );
    
    $results = $collection->aggregate($ops);

    print_r($results);
  }
}

class Test_getMongoDBDocumentsMatchList extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];
   
    $params = array(
      'ts' => array( '$all' => array('test_1426805916','test_1426805931') )
    );
    
    print_r( \Ultra\Lib\DB\getMongoDBDocuments( $collection , $params ) );
  }
}

class Test_getMongoDBDocuments extends TestMongoBase
{
// Example: %> php Ultra/Lib/DB/MongoDB_test.php getMongoDBDocuments SynchronizationTasks

  function test()
  {
    global $argv;
    
    echo "collection = ".$argv[2]."\n";

    $collection = $this->dbObject->$argv[2];
    $params = array();

    unset($argv[0],$argv[1],$argv[2]);
    
    $args = array_values(array_filter($argv));
    
    for( $i = 0; $i < count( $args ); $i += 2 )
    {
      if ( isset( $args[ $i + 1 ] ) )
        $params[ $args[$i] ] = $args[$i + 1];
      else
        $params[ $args[$i] ] = null;
    }
   
    #$params = array( '$query' => array() , '$orderby' => array( 'ot' => 1 ) );

    echo "params     = ".json_encode( $params )."\n";

    print_r( \Ultra\Lib\DB\getMongoDBDocuments( $collection , $params ) );
  }
}

class Test_addMongoDBDocument extends TestMongoBase
{
/*
php Ultra/Lib/DB/MongoDB_test.php addMongoDBDocument AccessNumbers 'cc,SI,st,0,tp,Geographic,rd,1,ai,1113033077,ad,113033077,ce,1113033077,up,1113033077'
*/
  function test()
  {
    global $argv;
    
    $collection = $this->dbObject->$argv[2];

    $test_array = explode(",",$argv[3]);
    $payload    = [];

    print_r( $test_array );

    $n = count( $test_array );
    $i = 0;

    while( $i < $n )
    {
      $payload[ $test_array[$i] ] = $test_array[ $i + 1 ];
      $i = $i + 2;
    }

    print_r( $payload );

    $outcome = \Ultra\Lib\DB\addMongoDBDocument( $collection , $payload );
    
    print_r($outcome);
  }
}

class Test_removeMongoDBDocument extends TestMongoBase
{
/*
php Ultra/Lib/DB/MongoDB_test.php removeMongoDBDocument SynchronizationTasks st OPEN
php Ultra/Lib/DB/MongoDB_test.php removeMongoDBDocument SynchronizationTasks st CLOSED
php Ultra/Lib/DB/MongoDB_test.php removeMongoDBDocument SynchronizationTasks st ABORTED
*/
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $outcome = \Ultra\Lib\DB\removeMongoDBDocument( $collection , array( $argv[3] => $argv[4] ) );
    
    print_r($outcome);
  }
}

class Test_upsert extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->LoginTokens;

    $criteria = array(
      'login_name' => 'raf123'
    );

    $newData = array(
      'login_name' => 'raf123',
      'salt'       => 's',
      'uid'        => 'u',
      'token_type' => 'refresh'
    );

    $outcome = \Ultra\Lib\DB\updateMongoDBDocument( $collection, $criteria, $newData , array( 'upsert' => TRUE ) );

    print_r($outcome);
  }
}

class Test_updateMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;
    
    $collection = $this->dbObject->$argv[2];

    $outcome = \Ultra\Lib\DB\updateMongoDBDocument( $collection, array( $argv[3] => $argv[4] ), array( $argv[5] => $argv[6] ) );
    
    print_r($outcome);
  }
}

class Test_batchInsertMongoDBDocuments extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];
    $documents = array(
      array( 'user' => 'billy', 'role' => 'golfer' ),
      array( 'user' => 'danny', 'role' => 'footballplayer' ),
    );

    $outcome = \Ultra\Lib\DB\batchInsertMongoDBDocuments( $collection,$documents );
    
    print_r($outcome);
  }
}

class Test_findSome extends TestMongoBase
{
/*
Examples:
php Ultra/Lib/DB/MongoDB_test.php findSome UserDirectory un a1429140441
*/

  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $clause = array();
    $param  = NULL;

    $i = 3;

    while( ! empty( $argv[$i] ) )
    {
      if ( $i % 2 )
        $param = $argv[$i];
      else
        $clause[ $param ] = $argv[$i];

      $i++;
    }

    print_r($clause);

    $cursor = $collection->find( $clause );

    // iterate cursor to display title of documents
    foreach ($cursor as $document)
      print_r($document);
  }
}

class Test_importJSON extends TestMongoBase
{
/*
Examples:
 php Ultra/Lib/DB/MongoDB_test.php importJSON $COLLECTION $FILENAME
*/

  function test()
  {
    global $argv;

    $result_array = array();

    if ( ! file_exists( $argv[3] ) )
      die("Cannot load JSON file ".$argv[3]."\n");

    $json_data = file_get_contents($argv[3]);

    $data = json_decode($json_data);

    if ( empty( $data ) )
      die("Cannot load JSON content from file ".$argv[3]."\n");

    $collection = $this->dbObject->$argv[2];

    print_r( $data );

    foreach( $data as $document )
    {
      $result = $collection->insert( $document );

      print_r( $result );
    }
  }
}

class Test_exportJSON extends TestMongoBase
{
/*
Examples:
 php Ultra/Lib/DB/MongoDB_test.php exportJSON InternationalRates
*/

  function test()
  {
    global $argv;

    $result_array = array();

    $collection = $this->dbObject->$argv[2];

    $top = empty( $argv[3] ) ? 999999 : $argv[3] ;

    $cursor = $collection->find( array() );

    $c = 0;
    // iterate cursor to display title of documents
    foreach ($cursor as $document)
    {
      unset($document["_id"]);

      $result_array[] = $document;

      $c++;

      if ( $top && ( $c == $top ) )
        exit;
    }

    echo json_encode( $result_array ) . PHP_EOL ;
  }
}

class Test_findAll extends TestMongoBase
{
/*
Examples:
 php Ultra/Lib/DB/MongoDB_test.php findAll UserDirectory
 php Ultra/Lib/DB/MongoDB_test.php findAll AddressBooks
*/

  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $top = empty( $argv[3] ) ? 999999 : $argv[3] ;

    $fields = empty( $argv[4] ) ? array() : explode( '|' , $argv[4] ) ;

    $cursor = $collection->find( array() );

    $c = 0;
    // iterate cursor to display title of documents
    foreach ($cursor as $document)
    {
      $c++;

      if ( empty( $fields ) )
        print_r($document);
      else
      {
        foreach( $fields as $field )
          echo "$field => ".$document[ $field ]."\n";

        echo "\n";
      }

      if ( $top && ( $c == $top ) )
        exit;
    }
    echo "$c documents found\n";
  }
}

class Test_findOneMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;
    
    $collection = $this->dbObject->$argv[2];
    $query = array( 'user' => 'chadmarciniak' );
    $field = array( 'user' );

    $outcome = \Ultra\Lib\DB\findOneMongoDBDocument( $collection,$query, $field );
    
    print_r($outcome);
  }
}

class Test_findAndModifyMongoDBDocument extends TestMongoBase
{
  function test()
  { 
    global $argv;
    
    $collection = $this->dbObject->$argv[2];
    $query = array( 'user' => 'chadmarciniak' );
    $update = array( 'role' => 'asdasd' );
    $fields = array( 'user' => true, 'role' => true );

    $outcome = \Ultra\Lib\DB\findAndModifyMongoDBDocument( $collection, $query, $update, $fields );
    
    print_r($outcome);
  }
}

class Test_groupByCountMongoDBDocuments extends TestMongoBase
{
  function test()
  {
    global $argv;
    
    $collection = $this->dbObject->$argv[2];

    $outcome = \Ultra\Lib\DB\groupByCountMongoDBDocuments( $collection , array('os' , 'de') );

    print_r($outcome);
  }
}

class Test_groupMongoDBDocuments extends TestMongoBase
{
  function test()
  {
    global $argv;
    
    $collection = $this->dbObject->$argv[2];
    $keys      = array( 'user' => 1 , 'role' => 1 );
    $initial   = array( "count" => 0 );
    $func      = "function (obj, prev) { prev.count++; }";
    $condition = array( 'user' => 'chadmarciniak' );

    $outcome = \Ultra\Lib\DB\groupMongoDBDocuments( $collection, $keys, $initial, $func, $condition);

    print_r($outcome);
  }
}

class Test_createIndexMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];
    $keys = array($argv[3] => 1);

    $outcome = \Ultra\Lib\DB\createIndexMongoDBDocument( $collection, $keys );
    
    print_r($outcome);
  }
}

class Test_ensureIndexMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];
    $keys = array($argv[3] => 1);

    $outcome = \Ultra\Lib\DB\ensureIndexMongoDBDocument( $collection, $keys );
    
    print_r($outcome);
  }
}

class Test_getIndexInfoMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];

    $outcome = \Ultra\Lib\DB\getIndexInfoMongoDBDocument( $collection );
    
    print_r($outcome);
  }
}

class Test_deleteIndexMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];
    
    $outcome = \Ultra\Lib\DB\deleteIndexMongoDBDocument( $collection, $argv[3] );
   
    print_r($outcome);
  }
}

class Test_saveMongoDBDocument extends TestMongoBase
{
  function test()
  {
    global $argv;

    $collection = $this->dbObject->$argv[2];
    unset($argv[0],$argv[1],$argv[2]);

    $params = array_values(array_filter($argv));
  
    for( $i = 0; $i < count( $params ); $i += 2 )
    {
      if ( isset( $params[ $i + 1 ] ) )
        $obj[ $params[$i] ] = $params[$i + 1];
      else
        $obj[ $params[$i] ] = null;
    }
  
    $outcome = \Ultra\Lib\DB\saveMongoDBDocument( $collection, $obj );
    print_r($outcome);
  }
}

$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$testObject = new $testClass();
$testObject->test();
