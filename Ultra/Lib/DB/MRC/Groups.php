<?php

/**
 * Monthly Runner Change Groups
 * @see API-212
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MRC+support+for+processing+customers+according+to+multiple+rules
 * @author VYT, 2015-10
 */

namespace Ultra\Lib\DB\MRC;

define('RETENTION_EMAIL_KEY', 'runner/monthly_charge/distro_email'); // cfengine key
define('MRC_RESERVATION_KEY', 'ultra/runner/monthly/customer_id/'); // redis reservation key
define('MRC_RESERVATION_TTL', 600); // we reserve customers for processing for 10 min
define('MRC_GROUPS_KEY', 'ultra/runner/monthly/groups'); // redis groups cache key

/**
 * initGroups
 * initailize monthly charge groups
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MRC+support+for+processing+customers+according+to+multiple+rules
 * @param Array runner execution parameters
 */
function initGroups($redis)
{
  // load groups queries
  if ( ! $groups = loadGroups(NULL, $redis))
    return logError('failed to fetch MRC groups');

  // process each group
  foreach ($groups as $group)
  {
    // execute group query: we must verify results carefully since the query can be changed at any time
    logInfo("initializing group {$group->MRC_GROUP_ID}: {$group->DESCRIPTION}");
    $subscribers = mssql_fetch_all_objects(logged_mssql_query($group->SQL_STATEMENT), MSSQL_FLAG_CAPITALIZE);

    if (empty($subscribers))
      logWarn("group {$group->MRC_GROUP_ID} query returned no results");
    else
    {
      logInfo("group {$group->MRC_GROUP_ID} has " . count($subscribers) . ' subscribers');
      foreach ($subscribers as $subscriber)
        initSubscriber($subscriber, $group);
    }
  }

  if ( ! sendStatsEmail($groups))
    logError('failed to send initialization statistics email message');
}


/**
 * initSubscriber
 * initialize group subscriber
 * @param Object subscriber
 * @param Object subscriber group
 */
function initSubscriber($subscriber, $group)
{
  // validate subscriber properties
  if (empty($subscriber->CUSTOMER_ID) || ! isset($subscriber->TZ))
  {
    $error = 'missing customer ID or time zone: ' . jsonEncode($subscriber);
    return logError($error);
  }

  // add default time zone statistics
  if (empty(trim($subscriber->TZ)))
     $subscriber->TZ = 'CST';

  // add subscriber for futher processing with status 'TODO'
  if ( ! add_pending_renewal_subscriber($group, $subscriber))
    logError("failed to add customer ID {$subscriber->CUSTOMER_ID}");
}


/**
 * compute_start_time
 * computes start time in UTC given group local hour and timezone
 * @param  Integer $local_hour earliest local hour
 * @param  String  $timezone
 * @return String  $start
 */
function compute_start_time($local_hour, $timezone)
{
  if ( ! $start = date_to_datetime("tomorrow $local_hour:00", FALSE, TRUE))
  {
    // failover to 7AM Pacific which is possible if time zone is not recognized
    logError('failed to compute subscriber start time: ' . json_encode(func_get_args()));
    if ( ! $start = date_to_datetime('tomorrow 7:00 ' . date('T'), FALSE, TRUE))
    {
      logError('failed to compute default start time');
      return FALSE;
    }
  }

  $toPst  = array('EST' => 3,'CST' => 2,'MST' => 1);
  $offset = (isset($toPst[$timezone])) ? $toPst[$timezone] : 0;
  $start  = date('Y-m-d h:i A', strtotime("$start - $offset hours"));

  return $start;
}


/**
 * sendStatsEmail
 * send initialization statistics email message
 * @param Array of Objects of initialization groups
 * @return Boolean TRUE on success, FALSE otherwise
 */
function sendStatsEmail($groups)
{
  // retrieve initialization results
  $stats = get_monthly_service_charge_log_init_stats();
  if (empty($stats))
    return logError('failed to retrieve initialization statistics');

  // compose HTML message
  $body = '<!DOCTYPE html><html>
    <head><title>MRC Initialization</title></head>
    <body>
      <h3>MRC initialization results</h3>
      <table>
        <tr><th>GROUP</th>
        <th>DESCRIPTION</th>
        <th>TZ</th>
        <th>START (PT)</th>
        <th>COUNT</th></tr>';

  // fill in table rows
  foreach ($stats as $row)
  {
    // prepare row values
    $start = compute_start_time($row->EARLIEST_LOCAL_HOUR, $row->TZ);
    if ( ! $group = findObjectByProperty($groups, array('MRC_GROUP_ID' => $row->MRC_GROUP_ID)))
    {
      logError("unable to locate MRG group $row->MRC_GROUP_ID");
      break;
    }

    $body .= "<tr>
      <td>{$row->MRC_GROUP_ID}</td>
      <td>{$group->DESCRIPTION}</td>
      <td>{$row->TZ}</td>
      <td>$start</td>
      <td>{$row->TOTAL}</td>
      </tr>";
  }

  // finalize email body
  $body .= '</table></body></html>';

  // get distribution list email address
  if ( ! $address = find_credential(RETENTION_EMAIL_KEY))
    return logError('missing credential ' . RETENTION_EMAIL_CRED);

  // send email via postage app
  $params = array(
    'postage_app_key' => POSTAGE_API_KEY_ULTRA,
    'template_name'   => 'generic-email',
    'email'           => $address,
    'subject'         => 'MRC initialization',
    'header'          => array('from' => 'donotreply@ultra.me'),
    'template_params' => array('body' => $body));
  return send_postage_app_mail($params);
}


/**
 * reserveSubscriber
 * reserve subscriber for processing in redis and database
 * @param Object HTT_MONTHLY_SERVICE_CHARGE_LOG row
 * @return Boolean TRUE on success or FALSE on failure
 */
function reserveSubscriber($subscriber, $redis)
{
  // check if not aready reserved
  $key = MRC_RESERVATION_KEY . $subscriber->CUSTOMER_ID;
  if ($reservation = $redis->get($key))
    return logWarn("subscriber {$subscriber->CUSTOMER_ID} is already reserved by $reservation");

  // reserve and validate in redis
  $value = getmypid() . '@' . gethostname();
  if ( ! $redis->setnx($key, $value, MRC_RESERVATION_TTL))
    return logError("failed to make redis reservation for subscriber {$subscriber->CUSTOMER_ID}");
  $reservation = $redis->get($key);
  if ($reservation != $value)
    return logError("failed to confirm redis reservation for subscriber {$subscriber->CUSTOMER_ID}");

  // update DB and check
  if ( ! monthly_service_charge_log_update_status($subscriber->HTT_MONTHLY_SERVICE_CHARGE_LOG, 'RESERVED', FALSE))
  {
    // remove from redis on failure
    $redis->del($key);
    return logError("failed to update DB status of for subscriber {$subscriber->CUSTOMER_ID}");
  }

  logInfo("reserved subscriber {$subscriber->CUSTOMER_ID} for $reservation");
  return TRUE;
}


/**
 * releaseSubscriber
 * un-reserve subscriber in redis and database
 * @param Integer customer ID
 * @param Boolean TRUE if successfully processed, FALSE otherwise
 * @return Boolean TRUE on success, FALSE on failure
 */
function releaseSubscriber($subscriber, $redis, $success)
{
  if ($success)
    if ( ! monthly_service_charge_log_update_status($subscriber->HTT_MONTHLY_SERVICE_CHARGE_LOG, 'COMPLETE', TRUE))
      logError("failed to update HTT_MONTHLY_SERVICE_CHARGE_LOG_ID {$subscriber->HTT_MONTHLY_SERVICE_CHARGE_LOG}");
  $redis->del(MRC_RESERVATION_KEY . $subscriber->CUSTOMER_ID);
}


/**
 * loadGroups
 * load specific (or all) MRC groups from cache or select from DB if needed
 * @param Array of Integers (group IDs) or NULL for all groups
 * @param Object redis instance
 * @return Array of Objects or NULL on failure
 */
function loadGroups($groups, $redis)
{
  $result = array();

  // load from cache or DB if neccessary
  if ( ! $cache = $redis->get(MRC_GROUPS_KEY))
  {
    // prepare SELECT query
    if ( ! $select = \Ultra\Lib\DB\makeSelectQuery('ULTRA.MRC_GROUPS', 1000, NULL, NULL, NULL, array('EARLIEST_LOCAL_HOUR ASC', 'MRC_GROUP_ID ASC')))
      return logError('failed to prepare SELECT statement');

    // execute query
    $data = mssql_fetch_all_objects(logged_mssql_query($select));
    if ( ! count($data))
      return logError('failed to load groups from database');

    // encode and cache groups data
    if ( ! $cache = jsonEncode($data))
      return logError('failed to encode groups data');

    $redis->set(MRC_GROUPS_KEY, $cache, SECONDS_IN_HOUR);
  }
  else
    $data = json_decode($cache);

  // extract needed groups from the sorted dataset
  if ( ! count($groups))
    $result = $data;
  else
    foreach ($data as $group)
      if (in_array($group->MRC_GROUP_ID, $groups))
        $result[] = $group;

  logInfo('found ' . count($result) . ' groups');
  return $result;
}
