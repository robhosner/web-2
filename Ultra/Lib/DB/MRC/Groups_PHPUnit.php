<?php

require_once 'classes/PHPUnitBase.php';
require_once 'lib/util-common.php';
require_once 'db/monthly_service_charge_log.php';
require_once 'Ultra/Lib/DB/MRC/Groups.php';

class DealerPortalTest extends PHPUnitBase
{
  public function setup()
  {
    teldata_change_db();
    $this->redis = new \Ultra\Lib\Util\Redis;
  }

  public function test_initGroups()
  {
    // validate in DB
    \Ultra\Lib\DB\MRC\initGroups($this->redis);
  }

  public function test__loadGroups()
  {
    // all groups are present
    $data = \Ultra\Lib\DB\MRC\loadGroups(array(3, 1), new \Ultra\Lib\Util\Redis);
    print_r($data);
    $this->assertEquals(2, count($data));

    // unknown group
    $data = \Ultra\Lib\DB\MRC\loadGroups(array(12, 333), new \Ultra\Lib\Util\Redis);
    print_r($data);
    $this->assertEquals(0, count($data));
  }

  public function test_reserveSubscriber()
  {
    // double reservation test
    $lock = \Ultra\Lib\DB\MRC\reserveSubscriber(99994, $this->redis);
    $this->assertTrue((boolean)$lock['lock_id']);
    $lock = \Ultra\Lib\DB\MRC\reserveSubscriber(99994, $this->redis);
    $this->assertFalse((boolean)$lock['lock_id']);
  }
}



?>
