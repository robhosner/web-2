<?php

namespace Ultra\Lib\DB\BillingHistory;

function addBillingHistory( $customerId, array $data )
{
  if ( !ctype_digit( (string)$customerId ) )
  {
    \logFatal( 'Invalid customerId in addBillingHistory' );
    return false;
  }

  $requiredFields = array( 'cos_id', 'entry_type', 'result', 'is_commissionable' );
  foreach ( $requiredFields as $field )
  {
    if ( !array_key_exists( $field, $data ) )
    {
      \logFatal( 'Missing ' . $field . ' in addBillingHistory' );
      return false;
    }
  }

  $params = array(
    array(
      'param_name' => '@CUSTOMER_ID',
      'variable'   => $customerId,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255
    ),
    array(
      'param_name' => '@TRANSACTION_DATE',
      'variable'   => !empty( $data['transaction_date'] ) ? $data['transaction_date'] : null,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => empty( $data['transaction_date'] ),
      'max_length' => 128
    ),
    array(
      'param_name' => '@COS_ID',
      'variable'   => $data['cos_id'],
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255
    ),
    array(
      'param_name' => '@ENTRY_TYPE',
      'variable'   => $data['entry_type'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 20
    ),
    array(
      'param_name' => '@STORED_VALUE_CHANGE',
      'variable'   => isset( $data['stored_value_change'] ) ? $data['stored_value_change'] : null,
      'type'       => SQLFLT8,
      'is_output'  => false,
      'is_null'    => !empty( $data['stored_value_change'] ),
      'max_length' => 128
    ),
    array(
      'param_name' => '@BALANCE_CHANGE',
      'variable'   => isset( $data['balance_change'] ) ? $data['balance_change'] : null,
      'type'       => SQLFLT8,
      'is_output'  => false,
      'is_null'    => empty( $data['balance_change'] ),
      'max_length' => 128
    ),
    array(
      'param_name' => '@PACKAGE_BALANCE_CHANGE',
      'variable'   => isset( $data['package_balance_change'] ) ? $data['package_balance_change'] : null,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => empty( $data['package_balance_change'] ),
      'max_length' => 255
    ),
    array(
      'param_name' => '@CHARGE_AMOUNT',
      'variable'   => isset( $data['charge_amount'] ) ? $data['charge_amount'] : null,
      'type'       => SQLFLT8,
      'is_output'  => false,
      'is_null'    => empty( $data['charge_amount'] ),
      'max_length' => 255
    ),
    array(
      'param_name' => '@REFERENCE',
      'variable'   => isset( $data['reference'] ) ? $data['reference'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['reference'] ),
      'max_length' => 40
    ),
    array(
      'param_name' => '@REFERENCE_SOURCE',
      'variable'   => isset( $data['reference_source'] ) ? $data['reference_source'] : null,
      'type'       => SQLINT2,
      'is_output'  => false,
      'is_null'    => empty( $data['reference_source'] ),
      'max_length'  => 40
    ),
    array(
      'param_name' => '@DETAIL',
      'variable'   => isset( $data['detail'] ) ? $data['detail'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['detail'] ),
      'max_length'  => 40
    ),
    array(
      'param_name' => '@DESCRIPTION',
      'variable'   => isset( $data['description'] ) ? $data['description'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['description'] ),
      'max_length'  => 30
    ),
    array(
      'param_name' => '@RESULT',
      'variable'   => $data['result'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['result'] ),
      'max_length' => 15
    ),
    array(
      'param_name' => '@SOURCE',
      'variable'   => $data['source'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 10
    ),
    array(
      'param_name' => '@STORE_ZIPCODE',
      'variable'   => isset( $data['store_zipcode'] ) ? $data['store_zipcode'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['store_zipcode'] ),
      'max_length' => 9
    ),
    array(
      'param_name' => '@STORE_ID',
      'variable'   => isset( $data['store_id'] ) ? $data['store_id'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['store_id'] ),
      'max_length' => 15
    ),
    array(
      'param_name' => '@CLERK_ID',
      'variable'   => isset( $data['clerk_id'] ) ? $data['clerk_id'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['clerk_id'] ),
      'max_length' => 25
    ),
    array(
      'param_name' => '@TERMINAL_ID',
      'variable'   => isset( $data['terminal_id'] ) ? $data['terminal_id'] : null,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => empty( $data['terminal_id'] ),
      'max_length' => 25
    ),
    array(
      'param_name' => '@IS_COMMISSIONABLE',
      'variable'   => $data['is_commissionable'],
      'type'       => SQLINT1,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 2
    ),
    array(
      'param_name' => '@SURCHARGE_AMOUNT',
      'variable'   => isset( $data['surcharge_amount'] ) ? $data['surcharge_amount'] : null,
      'type'       => SQLFLT8,
      'is_output'  => false,
      'is_null'    => empty( $data['surcharge_amount'] ),
      'max_length' => 255
    ),
    array(
      'param_name' => '@COMMISSIONABLE_CHARGE_AMOUNT',
      'variable'   => isset( $data['commissionable_charge_amount'] ) ? $data['commissionable_charge_amount'] : null,
      'type'       => SQLFLT8,
      'is_output'  => false,
      'is_null'    => empty( $data['commissionable_charge_amount'] ),
      'max_length' => 255
    )
  );

  return \Ultra\Lib\DB\run_stored_procedure('[ULTRA].[HTT_BILLING_HISTORY_ADD]', $params);
}