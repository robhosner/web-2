<?php

namespace Ultra\Lib\DB\Merchants;

use \Ultra\Configuration\Configuration;
use \Ultra\Lib\Services\PaymentAPI;

require_once 'Ultra/Lib/DB/Merchants/MerchantInterface.php';
require_once 'Ultra/Lib/MeS.php';
require_once 'Ultra/Configuration/Configuration.php';
require_once 'Ultra/Lib/Services/PaymentAPI.php';

/**
 * MeS Merchant class for tokenization
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Credit Card Processor
 */

class MeS implements MerchantInterface
{
  private $configObj;
  private $api;

  public function __construct(Configuration $configObj=null, PaymentAPI $paymentAPI=null)
  {
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();
    $this->api = !empty($paymentAPI) ? $paymentAPI : new PaymentAPI($this->configObj);
  }

  /**
   * tokenize
   *
   * Creates token and returns it
   *
   * Input:
   *  - 'cc_number'
   *  - 'expires_date'
   *
   * @returns a Result object
   */
  public function tokenize( $params )
  {
    $params = $this->clientReferenceNumberUpdateValidation($params);

    // invoke MeS API to store the CC number and generate the token
    $result = \Ultra\Lib\MeS\store( $params );

    if ( $result->is_failure() )
      return $result;

    $result->data_array = array_merge( $result->data_array , $this->merchantInfo() );

    if ( !isset( $result->data_array['token'] ) || !$result->data_array['token'] )
      return \make_error_Result( 'MeS did not return a token' );

    return $result;
  }

  /**
   * merchantInfo
   *
   * Specific Merchant Data to be stored in ULTRA.CC_HOLDER_TOKENS
   *
   * @return array
   */
  public function merchantInfo()
  {
    return array(
      'gateway'          => 'MeS',
      'merchant_account' => '1',
      'cc_processor_id'  => $this->configObj->getCCProcessorIDByName('mes')
    );
  }

  /**
   * saleRecurring
   *
   * Invoke MeS API for a recurring sale
   *
   * @returns a Result object
   */
  public function saleRecurring( $params )
  {
    return $this->sale($params, true);
  }

  /**
   * sale
   *
   * Invoke MeS API for a sale
   *
   * @returns a Result object
   */
  public function sale( $params, $recurring=false )
  {
    $params = $this->clientReferenceNumberUpdateValidation($params);

    // call payment API
    $data = [
      'token'           => $params['token'],
      'orderID'         => (string)$params['cc_transactions_id'],
      'customerID'      => $params['customer_id'],
      'brandID'         => (int)$params['brand_id'],
      'expDate'         => $params['expires_date'],
      'amount'          => (int)round($params['amount'] * 100),
      'billingAddress1' => !empty($params['address']) ? $params['address'] : null,
      'billingZipCode'  => !empty($params['zipcode']) ? $params['zipcode'] : null,
      'recurring'       => $recurring
    ];

    return $this->api->charge($params['cc_processors_id'], $data);
  }

  /**
   * void
   *
   * Invoke MeS API for voiding a transaction
   *
   * @returns a Result object
   */
  public function void( $params )
  {
    $params = $this->clientReferenceNumberUpdateValidation($params);

    return \Ultra\Lib\MeS\void(
      array(
        'transaction_id'          => $params['transaction_id'],
        'client_reference_number' => $params['client_reference_number']
      )
    );
  }

  /**
   * refund
   *
   * Invoke MeS API for refunding a transaction
   *
   * @returns a Result object
   */
  public function refund( $params )
  {
    $params = $this->clientReferenceNumberUpdateValidation($params);

    return \Ultra\Lib\MeS\refund(
      array(
        'transaction_id'          => $params['transaction_id'],
        'client_reference_number' => $params['client_reference_number']
      )
    );
  }

  /**
   * verify
   *
   * Invoke MeS API for validating a credit card
   *
   * @returns a Result object
   */
  public function verify( $params )
  {
    $params = $this->clientReferenceNumberUpdateValidation($params);
    
    $params['amount'] = '0.00';

    return \Ultra\Lib\MeS\verify( $params );
  }

  /**
   * The is a temporary function to ensure MeS Client Reference Update was successful
   *
   * @returns parameter object
   */
  public function clientReferenceNumberUpdateValidation($params)
  {
    if ( ! isset($params['client_reference_number']))
    {
      $params['client_reference_number'] = '';
      \logWarn('Failed to pass client reference number to MeS : data - ' . json_encode($params));
    }

    return $params;
  }
}

?>
