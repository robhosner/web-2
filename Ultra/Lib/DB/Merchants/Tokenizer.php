<?php

namespace Ultra\Lib\DB\Merchants;

require_once 'Ultra/Lib/DB/Merchants/MeS.php';
require_once 'Ultra/Lib/DB/Merchants/Vantiv.php';
require_once 'Ultra/Lib/DB/Merchants/Test.php';
require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Main class for Tokenization Logic.
 * This class is not merchant specific.
 *
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Credit+Card+Processor+Change+-+High+Level
 * 
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Credit Card Processor
 */

class Tokenizer
{
  protected $merchants;
  protected $customer;
  protected $redis;

  public function __construct()
  {
    // initialize lists of merchants
    $this->merchants = array(
      'MeS'
      #'Test'
    );

    $this->redis = new \Ultra\Lib\Util\Redis;
  }

  public function __destruct()
  {
  }

  /**
   * runRecurringCharge
   *
   * Performs a recurring sale for a merchant/gateway
   * Successful results will provide 'merchant_transaction_id'
   *
   * @returns a Result object
   */
  public function runRecurringCharge( $params )
  {
    $params['recurring'] = TRUE;

    return $this->runCharge( $params );
  }

  /**
   * runCharge
   *
   * Performs a sale for a merchant/gateway
   * Successful results will provide 'merchant_transaction_id'
   *
   * @returns a Result object
   */
  public function runCharge( $params )
  {
    dlog('', "(%s)", func_get_args());

    if ( ! isset($params['token']) || ! $params['token'] )
      return \make_error_Result( 'Missing token' );

    if ( ! isset($params['charge_amount']) )
      return \make_error_Result( 'Missing charge_amount' );

    /*
    $error = $this->validateGateway( $params );

    if ( $error )
      return \make_error_Result( $error );
    */
      
    // merchant class
    $merchantClass = '\Ultra\Lib\DB\Merchants\\'.$params['processor_name'];

    dlog('','merchantClass = %s',$merchantClass);

    // sanity check
    if ( ! class_exists( $merchantClass ) )
      return \make_error_Result( 'Merchant '.$params['processor_name'].' is invalid' );

    $merchantObject = new $merchantClass;

    // load CUSTOMERS info needed for a sale
    $error = $this->loadCustomer( $params['customer_id'] );

    if ( $error )
      return \make_error_Result( $error );

    dlog('',"CC_NAME = ".$this->customer->CC_NAME);
    dlog('',"LAST_NAME = ".$this->customer->LAST_NAME);

    $params['last_name'] = ( ($this->customer->LAST_NAME) ? $this->customer->LAST_NAME : $this->customer->CC_NAME );

    if ( ! empty( $this->customer->CC_NAME ) )
      $params['cc_name'] = $this->customer->CC_NAME;

    dlog('',"last_name = ".$params['last_name']);

    // check for always_succeed credentials
    if ( ( $params['token'] == 'always_succeed' ) && $this->alwaysSucceedAccount( $params ) )
      return \make_ok_Result(
        array(
          'merchant_transaction_id' => 'always_succeed'
        )
      );
    // check for always_fail credentials
    elseif ( ( $params['token'] == 'always_fail' ) && $this->alwaysFailAccount( $params ) )
      return \make_ok_Result(
        array(
          'merchant_transaction_id' => 'always_fail'
        )
      );
    else
    {
      $saleParams = array(
        'brand_id'                => $this->customer->BRAND_ID,
        'address'                 => $this->customer->CC_ADDRESS,
        'zipcode'                 => $this->customer->CC_POSTAL_CODE,
        'amount'                  => $params['charge_amount'],
        'expires_date'            => $params['expires_date'],
        'token'                   => $params['token'],
        'client_reference_number' => $params['client_reference_number'],
        'cc_holder_tokens_id'     => $params['cc_holder_tokens_id'],
        'customer_id'             => $params['customer_id'],
        'cc_transactions_id'      => $params['cc_transactions_id'],
        'cc_processors_id'        => $params['cc_processors_id']
      );

      $result = ( isset($params['recurring']) && $params['recurring'] )
                ?
                $merchantObject->saleRecurring( $saleParams )
                :
                $merchantObject->sale( $saleParams )
                ;

      dlog('','result = %s - %s',$result->data_array,$result->get_errors());

      if ( ! $result->is_success() )
      {
        $result->data_array['merchant_transaction_id'] = ( isset( $result->data_array['transaction_id'] ) ? $result->data_array['transaction_id'] : NULL ) ;

        return $result;
      }
    }

    return \make_ok_Result(
      array(
        'merchant_transaction_id' => $result->data_array['transaction_id']
      )
    );
  }

  /**
   * loadCustomer
   *
   * Load CUSTOMERS info
   *
   * @return string
   */
  public function loadCustomer( $customer_id )
  {
    $sql = \ultra_customer_select_query([
      'select_fields' => [
        'LAST_NAME',
        'CC_NAME',
        'CC_ADDRESS1',
        'CC_ADDRESS2',
        'CC_CITY',
        'CC_COUNTRY',
        'CC_STATE_REGION',
        'CC_POSTAL_CODE',
        'CC_EXP_DATE',
        'BRAND_ID'
      ],
      'WHERE:customer_id' => $customer_id
    ]);
    $data = \mssql_fetch_all_objects( \logged_mssql_query( $sql ) );

    if ( $data && is_array( $data ) && count( $data ) )
    {
      $this->customer = $data[0];

      // adjust address
      if ( ! $this->customer->CC_ADDRESS1 )
        $this->customer->CC_ADDRESS = $this->customer->CC_ADDRESS2;
      elseif ( ! $this->customer->CC_ADDRESS2 )
        $this->customer->CC_ADDRESS = $this->customer->CC_ADDRESS1;
      else
        $this->customer->CC_ADDRESS = $this->customer->CC_ADDRESS1 . ' ' . $this->customer->CC_ADDRESS2 ;

      if ( ! $this->customer->CC_NAME && $this->customer->LAST_NAME )
        $this->customer->CC_NAME = $this->customer->LAST_NAME;
      elseif ( ! $this->customer->LAST_NAME && $this->customer->CC_NAME )
        $this->customer->LAST_NAME = $this->customer->CC_NAME;

      return NULL;
    }
    else
      return 'Customer data not found';
  }

  /**
   * runRefund
   *
   * Performs a refund for a merchant
   *
   * @returns a Result object
   */
  public function runRefund( $params )
  {
    dlog('', "(%s)", func_get_args());

return \make_ok_Result();
  }

  /**
   * runVoid
   *
   * Performs a void for a merchant
   *
   * @returns a Result object
   */
  public function runVoid( $params )
  {
    dlog('', "(%s)", func_get_args());

return \make_ok_Result();
  }

  /**
   * runVerify
   *
   * Performs ``verify`` for the primary merchant
   *
   * @returns a Result object
   */
  public function runVerify( $params )
  {
    dlog('', "(%s)", func_get_args());

    $result = \make_ok_Result();

    $merchant = \Ultra\UltraConfig\getPrimaryCCGateway();

    // merchant class
    $merchantClass = '\Ultra\Lib\DB\Merchants\\'.$merchant;

    dlog('','merchantClass = %s',$merchantClass);

    // sanity check
    if ( class_exists( $merchantClass ) )
    {
      $merchantObject = new $merchantClass;

      if ( $this->alwaysFailAccount( $params ) )
      {
        $result->data_array['cvv_validation'] = '0';
        $result->data_array['avs_validation'] = '0';

        $result->add_error( 'Always Fail Account match' );
      }
      elseif ( $this->alwaysSucceedAccount( $params ) )
      {
        $result->data_array['cvv_validation'] = 'M';
        $result->data_array['avs_validation'] = 'Y';

        $result->add_warning( 'Always Succeed Account match' );
      }
      else
      {
        // invoke verify

        $result = $merchantObject->verify( $params );

        dlog('',$merchantClass.'::verify returned %s - %s',$result->data_array,$result->get_errors());
      }
    }
    else
      $result->add_errors( "Merchant $merchant is invalid" );

    return $result;
  }

  /**
   * alwaysFailAccount
   *
   * Input:
   *  - 'cc_number' or ( 'bin' and 'last_four' )
   *  - 'last_name'
   *  - 'expires_date'
   *
   * @returns boolean
   */
  public function alwaysFailAccount( $params )
  {
    dlog('', "(%s)", func_get_args());

    $alwaysFailAccount = FALSE;

    list($checkLastName,$checkBin,$checkLastFour,$checkExpiresDate) = \Ultra\UltraConfig\ccAlwaysFailTokens();

    // $checkExpiresDate can be part of 'last_name' or 'cc_name'

    $last_name = isset( $params['last_name'] ) ? $params['last_name'] : '' ;

    if ( ! empty($params['cc_name']) )
    {
      $parts = explode(' ', $params['cc_name']);

      if ( $parts[ count($parts) - 1 ] == $checkLastName )
        $last_name = $checkLastName;
    }

    if ( isset( $params['last_name'] ) && ( $params['last_name'] == $checkLastName ) )
    {
      if ( isset( $params['cc_number'] ) )
      {
        $params['bin']       = substr( $params['cc_number'] , 0, 6);
        $params['last_four'] = substr( $params['cc_number'] ,-4);
      }

// TODO: fix octal cfengine values
      $alwaysFailAccount = ! ! ( ( $params['bin'] == $checkBin ) && ( $params['last_four'] == $checkLastFour ) && ( $params['expires_date'] == $checkExpiresDate ) );
    }

    dlog('', "returning ".($alwaysFailAccount?'TRUE':'FALSE'));

    return $alwaysFailAccount;
  }

  /**
   * alwaysSucceedAccount
   *
   * Input:
   *  - 'cc_number' or ( 'bin' and 'last_four' )
   *  - 'last_name'
   *  - 'expires_date'
   *
   * @returns boolean
   */
  public function alwaysSucceedAccount( $params )
  {
    dlog('', "(%s)", func_get_args());

    $alwaysSucceedAccount = FALSE;

    list($checkLastName,$checkBin,$checkLastFour,$checkExpiresDate) = \Ultra\UltraConfig\ccAlwaysSucceedTokens();

    // $checkExpiresDate can be part of 'last_name' or 'cc_name'

    $last_name = isset( $params['last_name'] ) ? $params['last_name'] : '' ;

    if ( ! empty($params['cc_name']) )
    {
      $parts = explode(' ', $params['cc_name']);

      if ( $parts[ count($parts) - 1 ] == $checkLastName )
        $last_name = $checkLastName;
    }

    if ( $last_name )
    {
      $parts = explode(' ', $last_name);

      $last_name = $parts[ count($parts) - 1 ];
    }

    dlog('', "last_name = $last_name");

    if ( $last_name == $checkLastName )
    {
      if ( isset( $params['cc_number'] ) )
      {
        $params['bin']       = substr( $params['cc_number'] , 0, 6);
        $params['last_four'] = substr( $params['cc_number'] ,-4);
      }

      $decimal_exp_date = decoct($checkExpiresDate);

      $alwaysSucceedAccount = ! ! ( ( $params['bin'] == $checkBin ) && ( $params['last_four'] == $checkLastFour ) && ( ( $params['expires_date'] == $checkExpiresDate ) || ( $decimal_exp_date == '0'.$params['expires_date'] ) ) );
    }

    dlog('', "returning ".($alwaysSucceedAccount?'TRUE':'FALSE'));

    return $alwaysSucceedAccount;
  }

  /**
   * runTokenization
   *
   * Performs Tokenization for all our merchants
   * Input:
   *  - 'customer_id'
   *  - 'cc_number'
   *  - 'expires_date'
   *  - 'cvv_validation'
   *  - 'avs_validation'
   *
   * @returns a Result object
   */
  public function runTokenization( $params )
  {
    $result = \make_ok_Result();

    $errors = $this->validateRunInput( $params );

    if ( count( $errors ) )
      return \make_error_Result( $errors );

    // check if being processed on another thread
    if ( ! $this->reserveCustomer($params['customer_id']))
      return \make_error_Result( 'Customer ID (' . $params['customer_id'] . ') already being processed' );

    // data to be stored in the DB
    $data = array();

    // loop through merchants
    $merchants = !empty($params['processor_name']) ? [$params['processor_name']] : $this->merchants;
    foreach( $merchants as $merchant )
    {
      // perform tokenization for a merchant
      $merchantResult = $this->runMerchantTokenization( $merchant , $params );

      if ( $merchantResult->is_failure() )
        $result->add_errors( $merchantResult->get_errors() );
      else
      {
        if ( ! empty($params['cc_number'])
          && ! \Ultra\UltraConfig\allowInfiniteCCCount()
          && ! overridden_cc_dupe_check($params['cvv'], $params['cc_number'], $params['expires_date'], $params['last_name']))
        {
          $maxDuplicateCCCount = \Ultra\UltraConfig\maxDuplicateCCCount();
          $count = \ultra_cc_holders_count_by_token( $merchantResult->data_array['token'] , $params['customer_id'] );

          if ( $count >= $maxDuplicateCCCount )
            $result->add_error("This credit card is already in use by other $count users in our system");
          else
            $data[] = $merchantResult->data_array;
        }
        else
          $data[] = $merchantResult->data_array;
      }
    }

    dlog('','errors = %s',$result->get_errors());

    if ( $result->is_success() )
      return $this->storeTokenizationData( $params , $data );

    return $result;
  }

  /**
   * reserveCustomer
   *
   * Attempts to reserve a Customer Id using Redis
   *
   * @return boolean - TRUE if success , FALSE if failure
   */
  protected function reserveCustomer($customer_id)
  {
    $pid = gethostname() . getmypid();

    $reserved = FALSE;

    dlog('', 'Process Id ' . $pid . ' attempts to reserve customer_id = ' . $customer_id);

    $redisKey = 'ultra/tokenizer/customer_id/' . $customer_id;

    $value = $this->redis->get($redisKey);

    if ( ( ! $value ) || ( $value == $pid ) )
    {
      $this->redis->setnx( $redisKey , $pid , 5 ); # ttl is 5 seconds
      $value = $this->redis->get($redisKey);
      $reserved = ! ! ( $value == $pid );
    }

    return $reserved;
  }

  /**
   * storeTokenizationData
   *
   * Store Tokenization data in ULTRA.CC_HOLDERS and ULTRA.CC_HOLDER_TOKENS .
   * Make sure we only have an ENABLED row in ULTRA.CC_HOLDERS per customer_id .
   * Make sure we only have an ENABLED CC_HOLDERS_ID in ULTRA.CC_HOLDER_TOKENS per customer_id .
   * Input: ( $params )
   *  - customer_id
   *  - expires_date
   *  - cc_number
   *  - cvv_validation
   *  - avs_validation
   * Input: ( $data rows )
   *  - token
   *  - gateway
   *  - merchant_account
   *
   * @returns a Result object
   */
  protected function storeTokenizationData( $params , $data )
  {
    dlog('', "(%s)", func_get_args());

    $params['bin']       = !empty($params['bin']) ? $params['bin'] : substr( $params['cc_number'] , 0, 6);
    $params['last_four'] = !empty($params['last_four']) ? $params['last_four'] : substr( $params['cc_number'] ,-4);

    $sqlList = array(
      $this->sqlInsertUltraCCHolders( $params ),
      $this->sqlDisablePreviousUltraCCHolders( $params )
    );

    foreach( $data as $merchantData )
      $sqlList[] = $this->sqlInsertUltraCCHolderTokens( $params['customer_id'] , $merchantData , $params );

    $sqlList[] = $this->sqlDisablePreviousUltraCCHolderTokens( $params + $merchantData );

    $success = exec_queries_in_transaction( $sqlList );

    if ( $success )
      return \make_ok_Result();
    else
      return \make_error_Result( 'DB Error' );
  }

  /**
   * sqlInsertUltraCCHolders
   *
   * SQL statement to add a new row into ULTRA.CC_HOLDERS, or enable a previously inserted row
   *
   * @return string
   */
  protected function sqlInsertUltraCCHolders( $params )
  {
    return \ultra_cc_holders_insert_query( $params );
  }

  /**
   * sqlDisablePreviousUltraCCHolders
   *
   * SQL statement to update previous ULTRA.CC_HOLDERS rows to ``disabled``
   *
   * @return string
   */
  protected function sqlDisablePreviousUltraCCHolders( $params )
  {
    return \ultra_cc_holders_update_query(
      array(
        'SET_enabled'          => '0',
        'customer_id'      => $params['customer_id'],
        'NOT_bin'          => $params['bin'],
        'NOT_last_four'    => $params['last_four'],
        'NOT_expires_date' => $params['expires_date']
      )
    );
  }

  protected function sqlDisablePreviousUltraCCHolderTokens( $params )
  {
    return \ultra_cc_holder_tokens_disable_query( $params );
  }

  /**
   * sqlInsertUltraCCHolderTokens
   *
   * SQL statement to add a new row into ULTRA.CC_HOLDER_TOKENS
   *
   * @return string
   */
  protected function sqlInsertUltraCCHolderTokens( $customer_id , $merchantData , $params )
  {
    return \ultra_cc_holder_tokens_insert_from_customer_id_query( $customer_id , $merchantData , $params );
  }

  /**
   * validateGateway
   *
   * @return string
   */
  public function validateGateway( $params )
  {
    $error = NULL;

    if ( !isset( $params['gateway'] ) )
      $error = 'gateway missing';
    elseif( ! in_array( $params['gateway'] , $this->merchants ) )
      $error = 'gateway not handled';

    return $error;
  }

  /**
   * validateRunInput
   *
   * Input:
   *  - 'customer_id'
   *  - 'cc_number'
   *  - 'expires_date'
   *
   * @return array
   */
  public function validateRunInput( $params )
  {
    $errors = array();

    if ( !isset($params['customer_id']) )
      $errors[] = 'customer_id is missing';
    elseif ( !is_numeric($params['customer_id']) )
      $errors[] = 'customer_id is invalid';

    if ( empty($params['token']) )
      if ( !isset($params['cc_number']) )
        $errors[] = 'cc_number is missing';
      elseif ( !is_numeric($params['cc_number']) )
        $errors[] = 'cc_number is invalid';
      elseif ( strlen($params['expires_date']) > 16 )
        $errors[] = 'cc_number is invalid';

    if ( !isset($params['expires_date']) )
      $errors[] = 'expires_date is missing';
    elseif ( !is_numeric($params['expires_date']) )
      $errors[] = 'expires_date is invalid';
    elseif ( strlen($params['expires_date']) != 4 )
      $errors[] = 'expires_date is invalid';

    return $errors;
  }

  /**
   * runMerchantTokenization
   *
   * Performs Tokenization for a merchant
   *
   * @returns a Result object
   */
  public function runMerchantTokenization( $merchant , $params )
  {
    $result = \make_ok_Result();

    // merchant class
    $merchantClass = '\Ultra\Lib\DB\Merchants\\'.$merchant;
    dlog('','merchantClass = %s',$merchantClass);

    // set cc_processor_id
    $processorID = \Ultra\UltraConfig\getCCProcessorIDByName($merchant);
    if (empty($processorID)) {
      return \make_error_Result('Missing processor config for ' . $merchant);
    }

    $result->data_array['cc_processor_id'] = $processorID;

    // sanity check
    if ( class_exists( $merchantClass ) )
    {
      $merchantObject = new $merchantClass;

      if ( $this->alwaysSucceedAccount( $params ) )
      {
        $alwaysSucceedData = $merchantObject->merchantInfo();

        $alwaysSucceedData['token'] = 'always_succeed';

        $result->add_data_array( $alwaysSucceedData );
      }
      else
      {
        // tokenize data for the given merchant
        $result = $merchantObject->tokenize( $params );

        dlog('',$merchantClass.'::tokenize returned %s',$result->data_array);
      }
    }
    else
      $result->add_errors( "Merchant $merchant is invalid" );

    return $result;
  }

}

?>
