<?php
namespace Ultra\Lib\DB\Merchants;

interface MerchantInterface
{
  public function merchantInfo();

  public function tokenize($params);

  public function sale($params);

  public function void($params);

  public function refund($params);

  public function verify($params);
}
