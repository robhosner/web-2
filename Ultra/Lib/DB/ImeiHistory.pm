package Ultra::Lib::DB::ImeiHistory;


use strict;
use warnings;


use base qw(Ultra::Lib::DB::MSSQL);


=head1 NAME

Ultra::Lib::DB::ImeiHistory

=head1 Perl module to access DB table ULTRA_ACC..IMEI_HISTORY

=head1 SYNOPSIS

  use Ultra::Lib::DB::ImeiHistory;

  my $db = Ultra::Lib::DB::MSSQL->new();

  my $dbImeiHistory = Ultra::Lib::DB::ImeiHistory->new( DB => $db );
    DB           => $db,
    REDIS_OBJECT => $redis,
  );

  my $success = $dbImeiHistory->insertImeiHistory({
    imei   => '1234567890123456',
    msisdn => '1001001000',
    iccid  => '1234567890123456789',
  });

=cut


=head4 addImeiHistory

  Insert a row into ULTRA_ACC..IMEI_HISTORY only if IMEI has not been inserted in the last $ttl seconds.
  Use Redis to cache.

=cut
sub addImeiHistory
{
  my ($this,$params) = @_;

  if ( ( ! defined $params->{ imei } ) || ( $params->{ imei } eq '' ) )
  {
    $this->addError("IMEI is required");
  }

  if ( ( ! defined $params->{ iccid } ) || ( $params->{ iccid } eq '' ) )
  {
    $this->addError("ICCID is required");
  }

  if ( ( ! defined $params->{ msisdn } ) || ( $params->{ msisdn } eq '' ) )
  {
    $this->addError("MSISDN is required");
  }

  return 0 if $this->hasErrors();

  # check Redis cache
  if ( ! $this->checkImeiHistoryCache( $params->{ imei } ) )
  {
    return $this->insertImeiHistory($params);
  }

  return 1;
}


=head4 checkImeiHistoryCache

  Returns a TRUE value if the given IMEI has been inserted in the last $ttl seconds.

=cut
sub checkImeiHistoryCache
{
  my ($this,$imei) = @_;

  my $redisKey = 'ultra/mvne/imei/'.$imei;

  return $this->{ REDIS_OBJECT }->get( $redisKey );
}


=head4 markImeiHistoryCache

  Sets Redis semaphore for the given IMEI for $ttl seconds.

=cut
sub markImeiHistoryCache
{
  my ($this,$imei) = @_;

  my $redisKey = 'ultra/mvne/imei/'.$imei;

  my $ttl = 30 * 60 ;

  $this->{ REDIS_OBJECT }->set( $redisKey , 1 );
  $this->{ REDIS_OBJECT }->expire( $redisKey , $ttl );
}


=head4 insertImeiHistory

  Insert a row into ULTRA_ACC..IMEI_HISTORY

=cut
sub insertImeiHistory
{
  my ($this,$params) = @_;

  if ( ( ! defined $params->{ imei } ) || ( $params->{ imei } eq '' ) )
  {
    $this->addError("IMEI is required");
  }

  if ( ( ! defined $params->{ iccid } ) || ( $params->{ iccid } eq '' ) )
  {
    $this->addError("ICCID is required");
  }

  if ( ( ! defined $params->{ msisdn } ) || ( $params->{ msisdn } eq '' ) )
  {
    $this->addError("MSISDN is required");
  }

  return 0 if $this->hasErrors();

  my $sql = '
    IF NOT EXISTS (
      SELECT IMEI
      FROM   IMEI_HISTORY
      WHERE  IMEI   = ?
      AND    ICCID  = ?
      AND    MSISDN = ?
    )

    BEGIN

    INSERT INTO IMEI_HISTORY
    ( IMEI , ICCID , MSISDN )
    VALUES
    (    ? ,     ? ,      ? )

    END ';

  my $values = [
    $params->{ imei },
    $params->{ iccid },
    $params->{ msisdn },
    $params->{ imei },
    $params->{ iccid },
    $params->{ msisdn },
  ];

  my ($success, $rows) = $this->{ DB }->executeWrite($sql,$values);

  if ( $this->{ DB }->hasErrors() )
  {
    $this->addErrors( $this->{ DB }->getErrors() );
  }

  if ( $success )
  {
    $this->markImeiHistoryCache( $params->{ imei } );
  }

  return $success;
}

1;

__END__

