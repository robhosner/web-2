<?php

require_once 'Ultra/Lib/DB/Intake.php';

$db_name = 'db_intake_test.db';
$parameters = array(
  array(
    'name'  => ':request_name',
    'value' => "Name",
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':request_description',
    'value' => "Description",
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':start_date',
    'value' => "2015-01-15",
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':due_date',
    'value' => "2015-01-31",
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':priority',
    'value' => '{"must_have": false, "nice_to_have": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':status',
    'value' => '{"idea": false, "approved": false, "on_hold": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':demand_type',
    'value' => '{"sales": false, "marketing": false, "care": false, "tech_environment": false, "tech_new": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':business_values',
    'value' => '{"gross_adds": false, "churn_retention": false, "cost_saving": false, "stability": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':tech_dependency',
    'value' => '{"front_end": false, "api": false, "back_end": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':launch_communications',
    'value' => '{"field": false, "release_notes": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':knowledge_transfer',
    'value' => '{"it": false, "ops": false, "care": false}',
    'type'  => \SQLITE3_TEXT
  ),
  array(
    'name'  => ':training',
    'value' => '{"field": false, "care": false}',
    'type'  => \SQLITE3_TEXT
  )
);

echo "Creating database...\n";
$intake = new Ultra\Lib\DB\Intake($db_name);

echo "Inserting row - ";
if ($result = $intake->addRow($parameters))
{
  echo "SUCCESS" . PHP_EOL;
}
else
{
  echo "FAILURE" . PHP_EOL;
}
print_r($result);
echo PHP_EOL;

echo "Getting rows - ";
if ($result = $intake->getAllRows())
{
  echo "SUCCESS" . PHP_EOL;
}
else
{
  echo "FAILURE" . PHP_EOL;
}
print_r($result);
echo PHP_EOL;

cleanup($db_name);

function cleanup($db_name)
{
  echo "Removing test database... ";
  if (!unlink($db_name)) {
    echo "FAILURE!";
  } else {
    echo "SUCCESS!";
  }

  echo PHP_EOL;
}

echo PHP_EOL;