<?php

require_once 'db.php';
require_once 'db/ultra_acc/portin_queue.php';
require_once 'db/ultra_activation_history.php';
require_once 'lib/util-common.php';
require_once 'db/ultra_session.php';


teldata_change_db();


$CUSTOMER_ID = 745;


$cos_id = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'COS_ID');
echo "CUSTOMER_ID = $CUSTOMER_ID ; COS_ID = $cos_id\n";


$dealer = \Ultra\Lib\DB\Getter\getScalar('ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID', $CUSTOMER_ID, 'DEALER');
echo "CUSTOMER_ID = $CUSTOMER_ID ; dealer = $dealer\n";


$MSISDN = '1000000001';
$mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $MSISDN, 'MVNE');
echo "MSISDN = $MSISDN ; mvne = $mvne\n";


$ICCID = '100000000000000001';
$mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $ICCID, 'MVNE');
echo "ICCID = $ICCID ; mvne = $mvne\n";


$CUSTOMER_ID = 1;
$mvne = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'MVNE');
echo "CUSTOMER_ID = $CUSTOMER_ID ; mvne = $mvne\n\n\n";


$mvnes = array('1','2');

foreach( $mvnes as $mvne )
{
 echo "invoking setScalarMVNE with mvne = $mvne\n";
 $status = \Ultra\Lib\DB\Getter\setScalarMVNE( $mvne , $MSISDN , $ICCID , $CUSTOMER_ID );
 echo "invoked setScalarMVNE ; status = ".($status?1:0)."\n";


 $mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $MSISDN, 'MVNE');
 echo "MSISDN = $MSISDN ; mvne = $mvne\n";

 $mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $ICCID, 'MVNE');
 echo "ICCID = $ICCID ; mvne = $mvne\n";

 $mvne = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'MVNE');
 echo "CUSTOMER_ID = $CUSTOMER_ID ; mvne = $mvne\n\n\n";
}


#$CUSTOMER_ID = 31;
#$WHOLESALE_PLAN_ID = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'WHOLESALE_PLAN_ID');
#echo "WHOLESALE_PLAN_ID = $WHOLESALE_PLAN_ID ; CUSTOMER_ID = $CUSTOMER_ID\n";


$CUSTOMER_ID = 448;
$ICCID = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'CURRENT_ICCID_FULL');
echo "ICCID = $ICCID ; CUSTOMER_ID = $CUSTOMER_ID\n";


$CUSTOMER_ID = 10;
$MSISDN = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'current_mobile_number');
echo "MSISDN = $MSISDN ; CUSTOMER_ID = $CUSTOMER_ID\n";


$MSISDN = '2138202156';
$mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $MSISDN, 'MVNE');
echo "MSISDN = $MSISDN ; mvne = $mvne\n";


$MSISDN = '12138202156';
$mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $MSISDN, 'MVNE');
echo "MSISDN = $MSISDN ; mvne = $mvne\n";


$ICCID = '123456789012345676';
$mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $ICCID, 'MVNE');
echo "ICCID = $ICCID ; mvne = $mvne\n";


$CUSTOMER_ID = 32;
$mvne = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $CUSTOMER_ID, 'MVNE');
echo "CUSTOMER_ID = $CUSTOMER_ID ; mvne = $mvne\n";


// valid MSISDN -> customer_id
$msisdn = '3478373351';
$result = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'customer_id');
echo "valid MSISDN = $msisdn -> customer_id = $result\n";

// invalid MSISDN -> customer_id
$msisdn = '2138202156';
$result = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'customer_id');
echo "invalid MSISDN = $msisdn -> customer_id = $result\n";

// invalid dealer code
$dealer_code = 'ZZZ';
$dealer_id = \Ultra\Lib\DB\Getter\getScalar('DealerCD', $dealer_code, 'DEALERSITEID');
echo "dealer_code: $dealer_code, dealer_id: $dealer_id \n";

// valid dealer code
$dealer_code = 'CCity';
$dealer_id = \Ultra\Lib\DB\Getter\getScalar('DealerCD', $dealer_code, 'DealerSiteID');
echo "dealer_code: $dealer_code, dealer_id: $dealer_id \n";


\Ultra\Lib\DB\ultra_acc_connect();


$msisdn = '2122121122';
$result = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'PORT_REQUEST_ID');
echo "MSISDN = $msisdn ; PORT_REQUEST_ID = $result\n";



?>
