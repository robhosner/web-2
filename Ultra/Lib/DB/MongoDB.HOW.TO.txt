

// Connect to a MongoDB instance / DB

$dbObject = \Ultra\Lib\DB\selectMongoDB( array( 'db_name' => 'primodb' ) );



// Select a Collection

$collection = $dbObject->AddressBooks;



// Add a document to a Collection

$document = array(
 'testkey1' => time(),
 'testkey2' => 'a'.time()
);
$outcome = \Ultra\Lib\DB\addMongoDBDocument( $collection , $document );



// Retrieve the entire Collection ( don't do this in production )

$cursor = $collection->find( array() );
foreach ($cursor as $document) { print_r($document); }



// Retrieve documents which match to all elements in a list
$cursor = $collection->find( array( 'ts' => array( '$all' => array('test_1426805916','test_1426805931') ) ) );
foreach ($cursor as $document) { print_r($document); }



// create an index ( single attribute )

$collection->createIndex(
  array(
    'un' => 1
  ),
  array('unique' => false)
);



// create an index ( multiple attributes )

$collection->createIndex(
  array(
    'fn' => 1 , 'ln' => 1
  ),
  array('unique' => false)
);



// show info about indexes in a Collection

$x = $collection->getIndexInfo();
print_r($x);


// count , group by

$results = $collection->aggregate(
  array(
    '$group' => array(
      '_id'    => array( 'os'   => '$os' , 'de' => '$de' ),
      'count'  => array( '$sum' => 1 )
    )
  )
);



// backup and restore

Mongo Restore
mongorestore -h my_server:27017 -d primodb -c my_collection ~/my_backup_file

Mongo Backup
mongodump -h my_server:27017 -d primodb -c my_collection -o ~/my_backup_file


