<?php

/**
 * Dealer Portal DB Access Layer
 * Database migrated from Celluphone to Ultra in June 2015
 * With the exception of complex JOIN queries most functions in this file use the following methodology:
 * - map API parameter names to DB schema (e.g. business_type -> TBLMASTER or TBLDEALERSITE, business_code -> MASTERCD or DEALERCD)
 * - prepare SQL query using Ultra/Lib/DB functions
 * - remap schema values to API paramaters (e.g. DEALERSITEID -> business_id)
 * @see https://issues.hometowntelecom.com:8443/browse/DEAL-129
 * @author VYT, 2015
 */

namespace Ultra\Lib\DB\DealerPortal;

// supported tblReportRequestType.ReportTypeID
define('COMMISSION_DETAIL_REPORT', 1);
define('COMMISSION_SUMMARY_REPORT', 2);
define('SIM_INVENTORY_REPORT', 3);

// supported tblProductType.FlagValue
define('PRODUCT_FLAG_PURPLE_SIM', 2);
define('PRODUCT_FLAG_ORANGE_SIM', 4);

// sales BCC email address
define('RETAILER_CC_EMAIL', 'agentinfo@ultra.me');

/**
 * validateEmail
 * validate user email and return access role and organization info
 * @param String email
 * @return Array (Object access and organization info or NULL on failure, String error message)
 * adds properties BUSINESS_CODE and BUSINESS_NAME to successfull output
 */
function validateEmail($email)
{
  // validate parameters
  $email = trim($email);
  if (empty($email))
    return array(NULL, 'Invalid parameters');

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // find user by username who is active and within the effective date window and belongs to an active organization
  // retrieve all types of organizations at once: user -> map -> MVNO, user -> map -> master, user -> map -> dealer
  $sql = sprintf(
    'DECLARE @email VARCHAR(50) = %s;
    SELECT u.USER_ID, u.PASSWORD, u.LAST_NAME, u.FIRST_NAME, g.DESCR AS BUSINESS_TYPE, r.ACCESS_ROLE_NAME AS ACCESS_ROLE,
    uo.NVOID, o.NVOCD, o.BUSINESSNAME AS NVOBUSINESSNAME,
    um.MASTERID, m.MASTERCD, m.BUSINESSNAME AS MASTERBUSINESSNAME,
    ud.DEALERSITEID AS DEALERID, d.DEALERCD, d.BUSINESSNAME AS DEALERBUSINESSNAME, dm.PARENTMASTERID,
    d.ACTIVEFLAG AS DEALERACTIVEFLAG
    FROM TBL_USER u WITH (NOLOCK)
    JOIN TBLORGLEVEL g WITH (NOLOCK) ON u.ORGLEVELID = g.ORGLEVELID
    JOIN TBL_ACCESS_ROLE r WITH (NOLOCK) ON u.ACCESS_ROLE_ID = r.ACCESS_ROLE_ID
    LEFT JOIN TBLUSERNVO uo WITH (NOLOCK) ON u.USER_ID = uo.USERID
      LEFT JOIN TBLNVO o WITH (NOLOCK) ON uo.NVOID = o.NVOID
    LEFT JOIN TBLUSERMASTER um WITH (NOLOCK) ON u.USER_ID = um.USERID
      LEFT JOIN TBLMASTER m WITH (NOLOCK) ON um.MASTERID = m.MASTERID
    LEFT JOIN TBLUSERDEALERSITE ud WITH (NOLOCK) ON u.USER_ID = ud.USERID
      LEFT JOIN TBLDEALERSITE d WITH (NOLOCK) ON ud.DEALERSITEID = d.DEALERSITEID
        LEFT JOIN TBLMASTER dm WITH (NOLOCK) ON d.MASTERID = dm.MASTERID
    WHERE u.IS_ACTIVE_FLAG = 1
    AND (m.EFFECTIVEDT IS NULL OR m.EFFECTIVEDT < GETUTCDATE()) AND (m.EXPIRATIONDT IS NULL OR m.EXPIRATIONDT > GETUTCDATE())
    AND u.EMAIL_ADDRESS = @email',
    mssql_escape_with_zeroes($email));

  // validate results
  $info = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  if ( ! count($info))
    return array(NULL, 'Username or password not found');
  if (count($info) > 1)
    return array(NULL, 'Multiple records exist for this email');

  // normalize results
  $result = $info[0];

  if ($result->BUSINESS_TYPE == 'Dealer' && $result->DEALERACTIVEFLAG === 0)
    return array(NULL, 'Your retailer code is inactive. Please contact your Master Agent to get reinstated and begin selling Ultra Mobile again.');

  $type = $result->BUSINESS_TYPE == 'SubDistributor' ? 'MASTER' : strtoupper($result->BUSINESS_TYPE); // VYT @ 2015-06-09: currently we are treating SubDistributor as Master
  if ($result->ACCESS_ROLE == 'User')
    $result->ACCESS_ROLE = 'Employee';
  $result->BUSINESS_CODE = $result->{$type . 'CD'};
  $result->BUSINESS_NAME = $result->{$type . 'BUSINESSNAME'};
  if ($result->BUSINESS_TYPE == 'NVO') // correct misspelled business type
    $result->BUSINESS_TYPE = 'MVNO';

  // set set compatability dealer and master
  $result->DEALER = empty($result->DEALERID) ? NULL : $result->DEALERID;
  $result->MASTER = empty($result->PARENTMASTERID) ? (empty($result->MASTERID) ? NULL : $result->MASTERID) : $result->PARENTMASTERID;

  return array($result, NULL);
}

/**
 * validateUser
 * validate user login credentials and return access role and organization info
 * @param String username
 * @return Array (Object access and organization info or NULL on failure, String error message)
 * adds properties BUSINESS_CODE and BUSINESS_NAME to successfull output
 */
function validateUser($username)
{
  // validate parameters
  $username = trim($username);
  if (empty($username))
    return array(NULL, 'Invalid parameters');

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // find user by username who is active and within the effective date window and belongs to an active organization
  // retrieve all types of organizations at once: user -> map -> MVNO, user -> map -> master, user -> map -> dealer
  $sql = sprintf(
    'DECLARE @username VARCHAR(20) = %s;
    SELECT u.USER_ID, u.PASSWORD, u.LAST_NAME, u.FIRST_NAME, g.DESCR AS BUSINESS_TYPE, r.ACCESS_ROLE_NAME AS ACCESS_ROLE,
    uo.NVOID, o.NVOCD, o.BUSINESSNAME AS NVOBUSINESSNAME,
    um.MASTERID, m.MASTERCD, m.BUSINESSNAME AS MASTERBUSINESSNAME,
    ud.DEALERSITEID AS DEALERID, d.DEALERCD, d.BUSINESSNAME AS DEALERBUSINESSNAME, dm.PARENTMASTERID,
    d.ACTIVEFLAG AS DEALERACTIVEFLAG
    FROM TBL_USER u WITH (NOLOCK)
    JOIN TBLORGLEVEL g WITH (NOLOCK) ON u.ORGLEVELID = g.ORGLEVELID
    JOIN TBL_ACCESS_ROLE r WITH (NOLOCK) ON u.ACCESS_ROLE_ID = r.ACCESS_ROLE_ID
    LEFT JOIN TBLUSERNVO uo WITH (NOLOCK) ON u.USER_ID = uo.USERID
      LEFT JOIN TBLNVO o WITH (NOLOCK) ON uo.NVOID = o.NVOID
    LEFT JOIN TBLUSERMASTER um WITH (NOLOCK) ON u.USER_ID = um.USERID
      LEFT JOIN TBLMASTER m WITH (NOLOCK) ON um.MASTERID = m.MASTERID
    LEFT JOIN TBLUSERDEALERSITE ud WITH (NOLOCK) ON u.USER_ID = ud.USERID
      LEFT JOIN TBLDEALERSITE d WITH (NOLOCK) ON ud.DEALERSITEID = d.DEALERSITEID
        LEFT JOIN TBLMASTER dm WITH (NOLOCK) ON d.MASTERID = dm.MASTERID
    WHERE u.IS_ACTIVE_FLAG = 1
    AND (m.EFFECTIVEDT IS NULL OR m.EFFECTIVEDT < GETUTCDATE()) AND (m.EXPIRATIONDT IS NULL OR m.EXPIRATIONDT > GETUTCDATE())
    AND u.USER_NAME = @username',
    mssql_escape_with_zeroes($username));

  // validate results
  $info = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  if ( ! count($info))
    return array(NULL, 'Username or password not found');
  if (count($info) > 1)
    return array(NULL, 'Multiple records exist, you must change your username');

  // normalize results
  $result = $info[0];

  if ($result->BUSINESS_TYPE == 'Dealer' && $result->DEALERACTIVEFLAG === 0)
    return array(NULL, 'Your retailer code is inactive. Please contact your Master Agent to get reinstated and begin selling Ultra Mobile again.');

  $type = $result->BUSINESS_TYPE == 'SubDistributor' ? 'MASTER' : strtoupper($result->BUSINESS_TYPE); // VYT @ 2015-06-09: currently we are treating SubDistributor as Master
  if ($result->ACCESS_ROLE == 'User')
    $result->ACCESS_ROLE = 'Employee';
  $result->BUSINESS_CODE = $result->{$type . 'CD'};
  $result->BUSINESS_NAME = $result->{$type . 'BUSINESSNAME'};
  if ($result->BUSINESS_TYPE == 'NVO') // correct misspelled business type
    $result->BUSINESS_TYPE = 'MVNO';

  // set set compatability dealer and master
  $result->DEALER = empty($result->DEALERID) ? NULL : $result->DEALERID;
  $result->MASTER = empty($result->PARENTMASTERID) ? (empty($result->MASTERID) ? NULL : $result->MASTERID) : $result->PARENTMASTERID;

  return array($result, NULL);
}


/**
 * getUser
 * retrieves user information for provided unique parameter
 * @param  Array key => value pairs or SQL SELECT WHERE clause
 * @param Resource DB connection
 * @return Array (Array of Arrays or NULL on failure, String NULL or error message on failure)
 */
function getUser($params, $dbc = NULL)
{
  // prepare SQL SELECT statement
  $map = getSchemaApiMap($table = 'TBL_USER');
  $where = mapApiParamsToSchema($map, $params, TRUE);
  $clause = \Ultra\Lib\DB\makeWhereClause(\Ultra\Lib\DB\getTableSchema($table), $where);
  foreach ($clause as $key => $value)
    $clause[$key] = "u.$value";
  $sql = sprintf('SELECT
    u.USER_ID, u.USER_NAME, u.FIRST_NAME, u.LAST_NAME, u.CONTACT_NUMBER, u.EMAIL_ADDRESS,
    u.USER_NOTE, u.IS_ACTIVE_FLAG, u.USER_EFFECTIVE_DT, u.USER_EXPIRATION_DT, r.ACCESS_ROLE_NAME
    FROM %s AS u WITH (NOLOCK)
    LEFT JOIN TBL_ACCESS_ROLE AS r WITH (NOLOCK) ON u.ACCESS_ROLE_ID = r.ACCESS_ROLE_ID
    WHERE %s ORDER BY u.USER_ID DESC',
    $table, implode(' AND ', $clause));

  // connect to DB
  if ( ! $dbc && ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'DB connection failed');

  $users = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TP);
  if ( ! count($users))
    return array(NULL, 'User not found');

  $result = array();
  foreach ($users as $user)
  {
    // convert dates and parameter names
    if ($user->USER_EFFECTIVE_DT)
      $user->USER_EFFECTIVE_DT = date_to_epoch($user->USER_EFFECTIVE_DT);
    if ($user->USER_EXPIRATION_DT)
      $user->USER_EXPIRATION_DT = date_to_epoch($user->USER_EXPIRATION_DT);
    $result[] = mapSchemaToApiParams($map, (array)$user);
  }
  return array($result, NULL);
}


/**
 * appendWhereClauses
 * performs common design pattern of appending where clauses
 * @param  String   $sql to append to
 * @param  String[] $whereClauses array of clauses ex. 'col = val'
 * @return String   $sql
 */
function appendWhereClauses($sql, $whereClauses)
{
  $i = 0;
  foreach($whereClauses as $clause)
  {
    $prefix = ($i) ? ' AND ' : ' WHERE ';
    $sql .= $prefix . $clause;
    $i++;
  }

  return $sql;
}


/**
 * getBusinesses
 * Searches businesses for business_type based on full business_code AND/OR partial business_name
 * verifies user_id has access to queried results
 *
 * @param  String[] $params [user_id,business_type,business_code,business_name]
 * @param  Boolean  $onlyActive if TRUE, only select active businesses
 * @return Result
 */
function getBusinesses($params, $onlyActive = FALSE)
{
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return make_error_Result('DB connection failed');

  // get requesting user access info
  $accessResult = getUserAccess($params['user_id']);
  if ($accessResult->is_failure())
    return $accessResult;

  $user = $accessResult->data_array;

  // restrictions
  // $user['orglevelid'] == 3 has no restrictions
  if ($user['orglevelid'] == 1)
  {
    if ( ! in_array($params['business_type'], array('SubDistributor', 'Dealer') ) )
      return make_error_Result('Requesting user cannot search this business type: ' . $params['business_type']);
  }
  else if ($user['orglevelid'] == 2)
  {
    if ( ! in_array($params['business_type'], array('Dealer') ) )
      return make_error_Result('Requesting user cannot search this business type: ' . $params['business_type']);
  }
  else if ($user['orglevelid'] == 4)
    return make_error_Result('Requesting user cannot search businesses.');

  $sql = '';

  if ( ! empty($params['business_code']))
    $sql .= sprintf('DECLARE @BUSINESSCD NVARCHAR(8) = %s;', mssql_escape_with_zeroes($params['business_code'], TRUE));

  $sql .= 'SELECT

    USER_OWNER.USER_EFFECTIVE_DT  AS effective_date,
    QT.BUSINESSNAME AS business_name,

  ';

  $whereClauses = array();

  $businessTypeTable = NULL;
  $accessRoleId      = NULL;

  // build whereClauses and SELECTs based on business_type
  switch ($params['business_type'])
  {
    case 'MVNO':
      // not supported
      break;

    case 'Master':
    case 'SubDistributor':
      $businessTypeTable = 'MASTER';

      $sql .= ' QT.MASTERCD AS business_code ';

      if ( ! empty($params['business_code']))
        $whereClauses[] = 'QT.MASTERCD = @BUSINESSCD';

      if ( $onlyActive )
        $whereClauses[] = '( QT.EXPIRATIONDT IS NULL OR QT.EXPIRATIONDT > GETUTCDATE() )';

      $accessRoleId = ($params['business_type'] == 'Master') ? 3 : 6;
      break;

    case 'Dealer':
      $businessTypeTable = 'DEALERSITE';

      $sql .= ' QT.DEALERCD AS business_code, QT.LOCATIONPORTALFLAG AS store_locator ';

      if ( ! empty($params['business_code']))
        $whereClauses[] = 'QT.DEALERCD = @BUSINESSCD';

      if ( $onlyActive )
        $whereClauses[] = 'QT.ACTIVEFLAG = 1';

      $accessRoleId = 12;
      break;
  }

  // build join and main table select based on business_type logic
  if ($businessTypeTable && $accessRoleId)
    $sql .= " FROM TBL{$businessTypeTable} AS QT WITH (NOLOCK)
              LEFT JOIN TBLUSER{$businessTypeTable} AS QUT WITH (NOLOCK) ON QUT.{$businessTypeTable}ID = QT.{$businessTypeTable}ID
              LEFT JOIN TBL_USER AS USER_OWNER WITH (NOLOCK) ON USER_OWNER.USER_ID = QUT.USERID AND USER_OWNER.ACCESS_ROLE_ID = $accessRoleId
              ";
  else
    return make_error_Result('Failed to create select statement.');

  $sql .= ' JOIN TBLORGLEVEL g WITH (NOLOCK) ON USER_OWNER.ORGLEVELID = g.ORGLEVELID ';

  // search by business name
  // this LIKE clause could affect performance if invoked too frequently
  if ( ! empty($params['business_name']))
    $whereClauses[] = sprintf(' QT.BUSINESSNAME LIKE %s', mssql_escape_with_zeroes('%' . $params['business_name'] . '%', TRUE));

  // if not NVO
  if ($user['orglevelid'] != 3)
  {
    // verify user has access to business
    $clause = " %d IN (SELECT USERID FROM TBLUSERMASTER WITH (NOLOCK) WHERE MASTERID = QT.MASTERID ";

    if (in_array($params['business_type'], array('Master', 'SubDistributor')))
      $clause .= " OR MASTERID = QT.PARENTMASTERID ";

    $clause .= ')';
    $whereClauses[] = sprintf($clause, $params['user_id']);
  }

  $sql = appendWhereClauses($sql, $whereClauses);

  $queryResult = logged_mssql_query($sql);
  return ($queryResult) ?
    make_ok_Result(mssql_fetch_all_objects($queryResult), MSSQL_FLAGS_TP) :
    make_error_Result('DB Error retrieving business info.');
}


/**
 * getUserAccess
 * returns Result containing flat array with orglevelid and access_role_name
 * @param  Integer $user_id
 * @return Result
 */
function getUserAccess($user_id)
{
  $sql   = sprintf('SELECT TOP 1
      U.ORGLEVELID AS orglevelid,
      AR.access_role_name AS access_role
    FROM TBL_USER AS U WITH (NOLOCK)
    LEFT JOIN TBL_ACCESS_ROLE AS AR WITH (NOLOCK) ON U.access_role_id = AR.access_role_id
    WHERE U.USER_ID = %d', $user_id);

  $user = find_first($sql);

  return ($user) ? make_ok_Result((array)$user) : make_error_Result('Failed retrieving requesting user info.');
}


/**
 * getUsers
 * retrieves users 
 * @param  Integer $userId user_id of user making call
 * @param  Boolean $onlyActive (IF TRUE, select only active users)
 * @return Result Object users
 */
function getUsers($user_id, $onlyActive = TRUE)
{
  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return make_error_Result('DB connection failed');

  // get requesting user access info
  $accessResult = getUserAccess($user_id);
  if ($accessResult->is_failure())
    return $accessResult;

  $user = $accessResult->data_array;

  $sql = '
    DECLARE @USER_ID INT = %d;

    SELECT
      user_id,
      first_name,
      last_name,
      contact_number     AS phone_number,
      email_address      AS email,
      user_name          AS username,
      user_note          AS note,
      is_active_flag     AS active,
      user_effective_dt  AS effective_date,
      user_expiration_dt AS expiration_date,

      CASE WHEN access_role_name = \'User\' THEN \'Employee\' ELSE access_role_name END AS access_role
    FROM tbl_user AS U WITH (NOLOCK)
    LEFT JOIN tbl_access_role AS AR WITH (NOLOCK) ON U.access_role_id = AR.access_role_id ';

  $whereClauses = array();

  // if requesting user has role name "User", only search self
  if ($user['access_role'] == 'User')
  {
    $whereClauses[] = 'U.user_id = @USER_ID';
  }
  else
  {
    // if requesting user has role name "Manager", only search users
    if ($user['access_role'] == 'Manager')
      $whereClauses[] = 'access_role_name = \'User\'';

    // do not search self
    $whereClauses[] = 'U.user_id != @USER_ID';

    $userTable = NULL;

    switch ($user['orglevelid'])
    {
      case '1':
      case '2':
        $userTable = 'MASTER';
        break;
      case '3':
        $userTable = 'NVO';
        break;
      case '4':
        $userTable = 'DEALERSITE';
        break;
    }

    if ($userTable)
      $sql .= " INNER JOIN TBLUSER{$userTable} AS UT WITH (NOLOCK) ON UT.USERID = U.user_id
                AND UT.{$userTable}ID = (SELECT TOP 1 {$userTable}ID FROM TBLUSER{$userTable} WITH (NOLOCK) WHERE TBLUSER{$userTable}.USERID = @USER_ID) ";
  }

  // if onnlyActive, only query active users
  if ($onlyActive)
      $whereClauses[] = 'U.is_active_flag = 1';

  $sql = sprintf(appendWhereClauses($sql, $whereClauses), $user_id);

  return make_ok_Result(mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TP));
}


/**
 * getBusinessInfo
 * return detailed info about a business
 * @params Array (business_type, business_code, user_id)
 * @returns Array (Array of properties or NULL on failure, String NULL or error message on failure)
 */
function getBusinessInfo($params)
{
  // all parameters are required
  if (empty($params['user_id']) || empty($params['business_type']) || empty($params['business_code']))
    return array(NULL, __FUNCTION__ .': missing required parameters');

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // verify access
  list($granted, $error) = authorizeHierarchyAccess($params, $dbc);
  if ( ! $granted)
    return array(NULL, $error);

  // get business record
  list($business, $error) = getBusinessRecord($params['business_type'], $params['business_code'], NULL, $dbc);
  if ($error)
    return array(NULL, $error);

  // remap parameters
  if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $params['business_type']))
    return array(NULL, __FUNCTION__ . ': uknown type of business');
  $map = getSchemaApiMap($table);
  $result = mapSchemaToApiParams($map, (array)$business);
  $result['business_type'] = $params['business_type']; // formality

  // get master info for dealers
  $master = NULL;
  if ( ! empty($result['master_id']))
  {
    list($master, $error) = getBusinessRecord('Master', NULL, $result['master_id'], $dbc);
    if ($error)
      return array(NULL, $error);
  }
  $result['master_business_code'] = $master ? $master->MASTERCD : NULL;
  $result['master_business_name'] = $master ? $master->BUSINESSNAME : NULL;

  // get parent business info
  $parent = NULL;
  if ( ! empty($result['parent_business_id']))
  {
    list($parent, $error) = getBusinessRecord($params['business_type'], NULL, $result['parent_business_id'], $dbc);
    if ($error)
      return array(NULL, $error);
    $parent = mapSchemaToApiParams($map, (array)$parent);
  }
  $result['parent_business_type'] = $parent ? $params['business_type'] : NULL;
  $result['parent_business_code'] = $parent ? $parent['business_code'] : NULL;

  // replace inactive_reason ID with description
  if ( ! empty($result['inactive_reason_id']))
  {
    if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery('TBLINACTIVEREASON', NULL, NULL, array('REASONID' => $result['inactive_reason_id']), NULL, NULL, TRUE))
      return array(NULL, __FUNCTION__ . ': failed to prepare SELECT query');
    $reasons = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
    if ( ! count($reasons))
      return array(NULL, __FUNCTION__ . ': invalid inactive reason ID');
    unset($result['inactive_reason_id']);
    $result['inactive_reason'] = $reasons[0]->REASON;
  }

  // get business owner
  list($owner, $error) = getBusinessOwner($params, $dbc);
  if ($error)
    dlog('', 'ERROR: cannot find business owner');
  else
  {
    $result['first_name'] = $owner->FirstName;
    $result['last_name'] = $owner->LastName;
    $result['phone_number'] = $owner->Phone;
    $result['email'] = $owner->Email;
  }

  // get business address
  list($address, $error) = getBusinessAddress($params);
  if ($error)
    dlog('', 'ERROR: cannot find business address');
  else
  {
    $result['street'] = $address->ADDRESS;
    $result['suite'] = $address->SUITE;
    $result['city'] = $address->CITY;
    $result['state'] = $address->STATE;
    $result['zipcode'] = $address->ZIP;
    $result['longitude'] = $address->LONGITUDE;
    $result['latitude'] = $address->LATITUDE;
    $result['effective_date'] = $address->EFFECTIVEDT;
    $result['expiration_date'] = $address->EXPIRATIONDT;
  }

  return array($result, NULL);
}


/**
 * createUser
 * create new user record within the given organization
 * @return Array (Integer user ID on success or NULL on failure, String NULL on success or error message on failure)
 */
function createUser($userId, $role, $username, $password, $isActive, $firstName, $lastName, $eMail, $telephone, $startDate, $endDate, $note, $orgLevel = NULL, $orgType = NULL)
{
  // validate required parameters
  if (empty($userId) || empty($role) || empty($username) || empty($password) || empty($firstName) || empty($lastName) || empty($eMail) || empty($telephone) || empty($startDate))
    return array(NULL, 'Missing required parameters');
  if ( ! in_array($role, array('Owner', 'Manager', 'Employee')))
    return array(NULL, "Uknown role $role");
  if ($role == 'Employee')
    $role = 'User';

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'DB connection failed');

  // determine the caller's organization if not given
  if ( ! $orgLevel || ! $orgType)
  {
    $sql = sprintf('SELECT o.ORGLEVELID, o.DESCR FROM TBL_USER u WITH (NOLOCK) JOIN TBLORGLEVEL o WITH (NOLOCK) ON u.ORGLEVELID = o.ORGLEVELID WHERE u.USER_ID = %d', $userId);
    $data = mssql_fetch_all_rows(logged_mssql_query($sql));
    if ( ! $data || count($data) != 1)
      return array(NULL, 'Cannot determine user organization type (1)');
    list($orgLevel, $orgType) = $data[0];
    if (empty($orgLevel) || empty($orgType))
      return array(NULL, 'Cannot determine user organization type (2)');
  }

  // prepare INSERT WHERE NOT EXISTS as a signle atomic statement which eliminates the need for preliminary check of existing username
  $sql = 'INSERT INTO TBL_USER
      (ORGLEVELID,
      ACCESS_ROLE_ID,
      IS_ACTIVE_FLAG, IS_INTERNAL_USER_FLAG,
      USER_NAME, PASSWORD,
      FIRST_NAME, LAST_NAME,
      EMAIL_ADDRESS, CONTACT_NUMBER,
      USER_EFFECTIVE_DT, USER_EXPIRATION_DT,
      USER_NOTE,
      LAST_CHANGE_BY_USER_ID, LAST_CHANGE_DT, PROFILE_LAST_UPDATE)
    SELECT
      %d,
      (SELECT ACCESS_ROLE_ID FROM TBL_ACCESS_ROLE WITH (NOLOCK) WHERE ORGLEVELID = %d AND ACCESS_ROLE_NAME = %s),
      %d, 0,
      %s, %s, %s, %s, %s, %s,
      DATEADD(S, %d, \'1970-01-01\'), CASE %s WHEN NULL THEN NULL ELSE DATEADD(S, %d, \'1970-01-01\') END,
      %s,
      %d, GETUTCDATE(), GETUTCDATE()
    WHERE NOT EXISTS (SELECT USER_NAME FROM TBL_USER WITH (NOLOCK) WHERE USER_NAME = %s)';
  $sql = sprintf($sql,
    $orgLevel,
    $orgLevel, mssql_escape_with_zeroes($role),
    $isActive,
    mssql_escape_with_zeroes($username), mssql_escape_with_zeroes($password),
    mssql_escape_with_zeroes($firstName), mssql_escape_with_zeroes($lastName),
    mssql_escape_with_zeroes($eMail), mssql_escape_with_zeroes($telephone),
    $startDate, empty($endDate) ? 'NULL' : $endDate, $endDate,
    empty($note) ? 'NULL' : mssql_escape_with_zeroes($note),
    $userId, mssql_escape_with_zeroes($username));

  // execute INSERT and get newly created user ID
  if ( ! run_sql_and_check($sql))
    return array(NULL, 'Database error (1)');
  if ( ! $newUserId = (integer)get_scope_identity())
    return array(NULL, "Username $username already taken");
  $rollback = "DELETE FROM TBL_USER WHERE USER_ID = $newUserId";

  // prepare INSERT for the corresponding user map table
  if ($orgType == 'NVO')
    $sql = 'INSERT INTO TBLUSERNVO (NVOID, USERID) VALUES ((SELECT NVOID FROM TBLUSERNVO WITH (NOLOCK) WHERE USERID = %d), %d)';
  elseif ($orgType == 'Master' || $orgType == 'SubDistributor')
    $sql = 'INSERT INTO TBLUSERMASTER (MASTERID, USERID) VALUES ((SELECT MASTERID FROM TBLUSERMASTER WITH (NOLOCK) WHERE USERID = %d), %d)';
  elseif ($orgType == 'Dealer')
    $sql = 'INSERT INTO TBLUSERDEALERSITE (DEALERSITEID, USERID) VALUES ((SELECT u.DEALERSITEID FROM TBLUSERDEALERSITE u WITH (NOLOCK)
      JOIN TBLDEALERSITE d WITH (NOLOCK) ON u.DEALERSITEID = d.DEALERSITEID AND d.ACTIVEFLAG = 1 WHERE u.USERID = %d), %d)';
  else
  {
    run_sql_and_check($rollback);
    return array(NULL, "Uknown organization type $orgType");
  }
  $sql = sprintf($sql, $userId, $newUserId);

  // execute INSERT and check result
  if ( ! run_sql_and_check($sql))
  {
    run_sql_and_check($rollback);
    return array(NULL, 'Database error (2)');
  }

  return array($newUserId, NULL);
}


/**
 * updateUser
 * update user record
 * @return String error message NULL on success or message on failure
 */
function updateUser($userId, $params)
{
  // check required parameters
  if (empty($userId) || empty($params->user_id))
    return 'Missing caller or target user ID';

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return 'DB connection failed';

  // usernames should be unique
  $table = 'TBL_USER';
  if ( ! empty($params->username))
  {
    $sql = sprintf('SELECT USER_ID FROM %s WHERE USER_NAME = %s AND USER_ID <> %d', $table, mssql_escape_with_zeroes($params->username), $params->user_id);
    $data = mssql_fetch_all_objects(logged_mssql_query($sql));
    if (count($data))
      return "Username {$params->username} is not available";
  }

  // prepare SQL SELECT
  $sql = 'SELECT r.ACCESS_ROLE_NAME, u.USER_NAME, u.PASSWORD, u.IS_ACTIVE_FLAG, u.FIRST_NAME, u.LAST_NAME, u.EMAIL_ADDRESS, u.CONTACT_NUMBER, u.USER_NOTE,
  DATEDIFF(ss, \'1970-01-01\', u.USER_EFFECTIVE_DT) AS USER_EFFECTIVE_DT, DATEDIFF(ss, \'1970-01-01\', u.USER_EXPIRATION_DT) AS USER_EXPIRATION_DT
    FROM %s u WITH (NOLOCK)
    JOIN TBL_ACCESS_ROLE r WITH (NOLOCK) ON u.ACCESS_ROLE_ID = r.ACCESS_ROLE_ID
    WHERE u.USER_ID = %d';
  $sql = sprintf($sql, $table, $params->user_id);

  $data = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TP);
  if ( ! $data || ! count($data))
    return "No record found for user ID {$params->user_id}";
  if (count($data) > 1)
    return "Database inconsistency for user ID $userId";
  $info = $data[0];
  dlog('', 'user ID %d info: %s', $userId, $info);

  // compare existing values against parameters
  $map = getSchemaApiMap($table);
  $set = array();
  if ($params->access_role && $params->access_role == 'Employee')
    $params->access_role = 'User';

  foreach ($info as $name => $value)
  {
    $value = trim($value); // some columns with fixed CHAR types return extra spaces
    if ($params->{$map[$name]} != '' && $params->{$map[$name]} != $value)
    {
      if ($name == 'ACCESS_ROLE_NAME') // special case
        $set[] = sprintf('ACCESS_ROLE_ID = (SELECT r.ACCESS_ROLE_ID FROM TBL_USER u WITH (NOLOCK)
          JOIN TBL_ACCESS_ROLE r WITH (NOLOCK) ON u.ORGLEVELID = r.ORGLEVELID
          WHERE u.USER_ID = %d AND r.ACCESS_ROLE_NAME = %s)',
          $userId, mssql_escape_with_zeroes($params->access_role));
      else // most parameters are handled here
        $set[$name] = $params->{$map[$name]};
    }
  }
  if ( ! count($set))
    return 'No values to update';

  // prepare UPDATE statement
  if ( ! $sql = \Ultra\Lib\DB\makeUpdateQuery($table, $set, array('USER_ID' => $params->user_id)))
    return 'Failed to create UPDATE statement';

  // execute UPDATE
  if ( ! run_sql_and_check($sql))
    return 'Failed to execute UPDATE statement';

  return NULL;
}


/**
 * getSchemaApiMap
 * returns a map of API paramater names to DB Schema names
 * if key is given then return its value; if value is given then return its key
 * @param String table name
 * @param String specific schema key
 * @param String specific schema value
 * @return Array (String API parameter name => DB schema column name)
 */
function getSchemaApiMap($table, $key = NULL, $value = NULL)
{
  $map = array(
    'TBLNVO' => array(
      'NVOID'                         => 'business_id',
      'BUSINESSNAME'                  => 'business_name',
      'NVOCD'                         => 'business_code',
      'NOTE'                          => 'note'),

    'TBLMASTER' => array(
      'MASTERID'                      => 'business_id',
      'BUSINESSNAME'                  => 'business_name',
      'MASTERCD'                      => 'business_code',
      'PARENTMASTERID'                => 'parent_business_id',
      'EFFECTIVEDT'                   => 'effective_date',
      'EXPIRATIONDT'                  => 'expiration_date',
      'FEDTAXID'                      => 'tax_id',
      'NOTE'                          => 'note',
      'LASTCHANGEBYUSERID'            => 'author_id',
      'LASTCHANGEDT'                  => 'modification_date'),

    'TBLDEALERSITE' => array(
      'DEALERSITEID'                  => 'business_id',
      'BUSINESSNAME'                  => 'business_name',
      'DEALERCD'                      => 'business_code',
      'ACTIVEFLAG'                    => 'active',
      'INACTIVEDT'                    => 'inactive_date',
      'INACTIVEREASONID'              => 'inactive_reason_id',
      'MASTERID'                      => 'master_id',
      'NOTE'                          => 'note',
      'PARENTDEALERSITEASSIGNDT'      => 'parent_assigned_date',
      'EPAY'                          => 'epay_id',
      'EMIDA'                         => 'emida_id',
      'LOCATIONPORTALFLAG'            => 'store_locator',
      'PARENTDEALERSITEID'            => 'parent_business_id',
      'TMOBILECODE'                   => 'tmo_id',
      'LASTCHANGEBYUSERID'            => 'author_id',
      'LASTCHANGEDT'                  => 'modification_date',
      'PRODUCTTYPE'                   => 'product_type',
      'PRODUCTTYPEDESC'               => 'product_description'),

    'TBL_USER' => array(
      'ACCESS_ROLE_NAME'              => 'access_role',
      'USER_ID'                       => 'user_id',
      'USER_NAME'                     => 'username',
      'PASSWORD'                      => 'password',
      'IS_ACTIVE_FLAG'                => 'active',
      'FIRST_NAME'                    => 'first_name',
      'LAST_NAME'                     => 'last_name',
      'EMAIL_ADDRESS'                 => 'email',
      'CONTACT_NUMBER'                => 'phone_number',
      'USER_EFFECTIVE_DT'             => 'effective_date',
      'USER_EXPIRATION_DT'            => 'expiration_date',
      'USER_NOTE'                     => 'note'),

    'TBLPERSON' => array(
      'PERSONID'                      => 'PersonId',
      'FIRSTNAME'                     => 'first_name',
      'LASTNAME'                      => 'last_name',
      'PHONE'                         => 'phone_number',
      'FAX'                           => 'fax',
      'EMAIL'                         => 'email'),

    'TBLADDRESS' => array(
      'ADDRESS'                       => 'street',
      'SUITE'                         => 'suite',
      'CITY'                          => 'city',
      'STATE'                         => 'state',
      'ZIP'                           => 'zipcode',
      'LATITUDE'                      => 'latitude',
      'LONGITUDE'                     => 'longitude'),

    'TBLCOMMISSIONMISCPROD' => array(
      'COMMISSIONMISCID'              => 'adjustment_id',
      'TRANSACTIONTYPECD'             => 'type',
      'COMMAMOUNT'                    => 'amount',
      'COMMENTTEXT'                   => 'comment',
      'COMMISSIONSCHEDULEID'          => 'period_id',
      'CREATEDBYUSERID'               => 'author_id'),

    // mapping of business types to business table names
    'BUSINESS_TYPE' => array(
      'MVNO'                          => 'TBLNVO',
      'Master'                        => 'TBLMASTER',
      'SubDistributor'                => 'TBLMASTER',
      'Dealer'                        => 'TBLDEALERSITE'));

  if ( ! empty($map[$table]))
  {
    $map = $map[$table];

    // return specific value by key if requested
    if ($key)
      return empty($map[$key]) ? NULL : $map[$key];

    // return specific key by value if requested
    if ($value)
      return array_search($value, $map);

    // return entire table map
    return $map;
  }

  dlog('', "ERROR: table $table or key $key or value $value not defined");
  return NULL;
}


/**
 * mapApiParamsToSchema
 * map API parameters to DB schema names
 * given a a corresponding map this function allows us to hide unsightly DB schema names behind good looking API parameter names
 * @param Array of schema => API parameter names
 * @param Array of API paramters
 * @param Boolean only use parameters that have values
 * @return Array of schema paramaters
 */
function mapApiParamsToSchema($map, $params, $exists = FALSE)
{
  // remap access role
  if (isset($params['access_role']) && $params['access_role'] == 'Employee')
    $params['access_role'] = 'User';

  return mapValues($map, $params, TRUE, $exists);
}


/**
 * mapSchemaToApiParams
 * map DB schema names to API return values (reverse of mapApiParamsToSchema)
 * @param Array of schema => API parameter names
 * @param Array of schema paramaters
 * @param Boolean only use params that have values
 * @return Array of API paramters
 */
function mapSchemaToApiParams($map, $params, $exists = FALSE)
{
  // remap access role
  if (isset($params['ACCESS_ROLE_NAME']) && $params['ACCESS_ROLE_NAME'] == 'User')
    $params['ACCESS_ROLE_NAME'] = 'Employee';

  return mapValues($map, $params, FALSE, $exists);
}


/**
 * mapValues
 * given a map and associative array of values construct an array that contains keys from the map
 * @param Array map of parameter names => names
 * @param Array of names => values
 * @param Boolean reverse map keys <=> values
 * @param Boolean only return values present in the input (not empty)
 * @returns Array of mapped names => values
 */
function mapValues($map, $params, $reverse, $exists = FALSE)
{
  $result = array();
  if ($reverse)
    $map = array_flip($map);

  // return only those parameters present in the map
  foreach ($params as $param => $value)
    if (isset($map[$param]))
    {
      if ($exists)
      {
        if (isset($value) && $value !== '') // missing optional API parameters are passed as empty strings
          $result[$map[$param]] = $value;
      }
      else
        $result[$map[$param]] = $value;
    }

  return $result;
}


/**
 * createBusiness
 * create new business entity
 * @param Integer user ID attempting to create new business
 * @param Array of parameters to create business from
 * @returns String NULL on success or error message on failure
 */
function createBusiness($params) // author_id
{
  // initialize
  $rollback = NULL;

  try
  {
    // determine main DB table
    if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $params['business_type']))
      throw new \Exception(__FUNCTION__ . ': uknown type of business');

    // connect to database
    if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
      throw new \Exception(__FUNCTION__ . ': DB connection failed');

    // master_code and product type are required to create a dealer
    $masterId = 0;
    if ($table == 'TBLDEALERSITE')
    {
      if (empty($params['master_code']))
        throw new \Exception(__FUNCTION__ . ': missing master code paramter');

      // validate master code
      list($master, $error) = getBusinessRecord('Master', $params['master_code'], NULL, $dbc);
      if ( ! $master)
        throw new \Exception(__FUNCTION__ . ': invalid master code');
      $masterId = $master->MASTERID;

      // default product type is Purple SIM
      if (empty($params['product_type']))
        $params['product_type'] = PRODUCT_FLAG_PURPLE_SIM;

      // validate product type and add description
      list($description, $error) = \Ultra\Lib\DB\DealerPortal\getProductDescription($params['product_type'], $dbc);
      if ($error)
        throw new \Exception($error);
      $params['product_description'] = $description;
    }

    // validate parent if given
    list($parent, $error) = verifyParentBusiness($params, $dbc);
    if ($error)
      throw new \Exception($error);
    $parentId = $parent ? $parent->{getSchemaApiMap($table, NULL, 'business_id')} : 0;

    // ensure that business code is unique
    list($business, $error) = getBusinessRecord($params['business_type'], $params['business_code'], NULL, $dbc);
    if ($business)
      throw new \Exception(__FUNCTION__ . ': business code is already used');

    // create INSERT statement
    if ($table == 'TBLMASTER') // Master or SubDistributor
      $sql = \Ultra\Lib\DB\makeInsertQuery('TBLMASTER', array(
        'MASTERCD'            => $params['business_code'],
        'BUSINESSNAME'        => $params['business_name'],
        'EFFECTIVEDT'         => 'NOW',
        'CREATEDDT'           => 'NOW',
        'CREATEDBYUSERID'     => $params['author_id'],
        'LASTCHANGEBYUSERID'  => $params['author_id'],
        'LASTCHANGEDT'        => 'NOW',
        'PARENTMASTERID'      => $parentId ? $parentId : 'NULL',
        'NOTE'                => $params['note']));
    else // Dealer
      $sql = \Ultra\Lib\DB\makeInsertQuery('TBLDEALERSITE', array(
        'DEALERCD'            => $params['business_code'],
        'BUSINESSNAME'        => $params['business_name'],
        'ACTIVEFLAG'          => 1,
        'MASTERID'            => $masterId ? $masterId : 'NULL',
        'CREATEDBYUSERID'     => $params['author_id'],
        'CREATEDDT'           => 'NOW',
        'LASTCHANGEBYUSERID'  => $params['author_id'],
        'LASTCHANGEDT'        => 'NOW',
        'NOTE'                => $params['note'],
        'EPAY'                => $params['epay_id'] ? $params['epay_id'] : 'NULL',
        'EMIDA'               => $params['emida_id'] ? $params['emida_id'] : 'NULL',
        'TMOBILECODE'         => $params['tmo_id'] ? $params['tmo_id'] : 'NULL',
        'LOCATIONPORTALFLAG'  => $params['store_locator'],
        'PRODUCTTYPE'         => $params['product_type'],
        'PRODUCTTYPEDESC'     => $params['product_description'],
        'ONETOONEDEALERFLAG'  => computeOneToOneFlag($params['business_code'])));
    if ( ! $sql)
      throw new \Exception(__FUNCTION__ . ': failed to prepare INSERT statement');

    // execute INSERT
    if ( ! run_sql_and_check($sql))
      throw new \Exception(__FUNCTION__ . ': failed to create record');

    // get newly created business record ID
    if ( ! $businessId = (integer)get_scope_identity())
      throw new \Exception(__FUNCTION__ . ': failed to get business ID');

    // prepare rollback statement
    if ($table == 'TBLMASTER')
      $rollback = "DELETE FROM TBLMASTER WHERE MASTERID = $businessId;";
    else
      $rollback = "DELETE FROM TBLDEALERSITE WHERE DEALERSITEID = $businessId;";

    // update parent ID to self if parent is not given
    if ( ! $parentId)
    {
      if ($table == 'TBLMASTER')
        $sql = \Ultra\Lib\DB\makeUpdateQuery($table, array('PARENTMASTERID' => $businessId), array('MASTERID' => $businessId));
      else
        $sql = \Ultra\Lib\DB\makeUpdateQuery($table, array('PARENTDEALERSITEID' => $businessId), array('DEALERSITEID' => $businessId));
      if ( ! $sql)
        throw new \Exception(__FUNCTION__ . 'failed to prepare UPDATE parent statement');

      // update record
      if ( ! run_sql_and_check($sql))
        throw new \Exception(__FUNCTION__ . ': failed to update business parent');
    }

    // create Address record
    $sql = sprintf(
      "DECLARE @street NVARCHAR(100) = %s;
      DECLARE @suite NVARCHAR(10) = %s;
      DECLARE @city NVARCHAR(50) = %s;
      DECLARE @state NVARCHAR(3) = %s;
      DECLARE @zip NVARCHAR(20) = '%s';
      DECLARE @latitude NVARCHAR(100) = %s;
      DECLARE @longitude NVARCHAR(100) = %s;
      INSERT INTO TBLADDRESS
        (ADDRESS,
        SUITE, CITY, STATE, ZIP,
        LATITUDE,
        LONGITUDE)
        VALUES
        (@street,
        CASE @suite WHEN '' THEN NULL ELSE @suite END,
        @city, @state, @zip,
        CASE @latitude WHEN '' THEN NULL ELSE @latitude END,
        CASE @longitude WHEN '' THEN NULL ELSE @longitude END);",
      mssql_escape_with_zeroes($params['street'], TRUE),
      mssql_escape_with_zeroes($params['suite'], TRUE),
      mssql_escape_with_zeroes($params['city'], TRUE),
      mssql_escape_with_zeroes($params['state'], TRUE),
      $params['zipcode'],
      mssql_escape_with_zeroes($params['latitude'], TRUE),
      mssql_escape_with_zeroes($params['longitude'], TRUE));
    if ( ! run_sql_and_check($sql))
      throw new \Exception(__FUNCTION__ . ': failed to create business address');

    // get newly created address ID
    if ( ! $addressId = (integer)get_scope_identity())
      throw new \Exception(__FUNCTION__ . ': failed to get address ID');

    // update rollback
    $rollback .= "DELETE FROM TBLADDRESS WHERE ADDRESSID = $addressId;";

    // associate address to business: address type MA - shipping, IN - invoice (billing)
    if ($table == 'TBLMASTER')
      $sql = sprintf("INSERT INTO TBLMASTERADDRESS (MASTERID, ADDRESSID, ADDRESSTYPECD, EFFECTIVEDT) VALUES (%d, %d, 'MA', GETUTCDATE())", $businessId, $addressId);
    else // dealer
      $sql = sprintf("INSERT INTO TBLDEALERSITEADDRESS (DEALERSITEID, ADDRESSID, ADDRESSTYPECD, EFFECTIVEDT) VALUES (%d, %d, 'MA', GETUTCDATE())", $businessId, $addressId);
    if ( ! run_sql_and_check($sql))
      throw new \Exception(__FUNCTION__ . ': failed to create address reference record');

    // update rollback
    if ($table == 'TBLMASTER')
      $rollback .= "DELETE FROM TBLMASTERADDRESS WHERE MASTERID = $businessId;";
    else
      $rollback .= "DELETE FROM TBLDEALERSITEADDRESS WHERE DEALERSITEID = $businessId;";

    // determine available username: start with business code and append digits till available
    $suffix = 0;
    do
    {
      $username = $params['business_code'] . ($suffix++ ? $suffix : NULL);
      $sql = sprintf("SELECT USER_ID FROM TBL_USER WITH (NOLOCK) WHERE USER_NAME = %s", mssql_escape_with_zeroes($username));
      $data = mssql_fetch_all_objects(logged_mssql_query($sql));
    } while (count($data) && $suffix < 100);
    if ($suffix == 100)
      throw new \Exception(__FUNCTION__ . ': failed to locate available username');
    $params['username'] = $username;

    // create busness owner user
    $params = array_merge($params, array(
      'password'       => substr($params['business_code'] . '-' . rand(100, 999999), 0, 12),
      'access_role'    => 'Owner',
      'active'         => 1,
      'note'           => 'automatically generated business owner record'));
    if ($error = createOrganizationUser($params))
      throw new \Exception($error);

    // PROD-1827: add CC and BCC addresses
    $params['headers'] = array('TO' => $params['email']);
    $params['email'] = array($params['email'], RETAILER_CC_EMAIL); // must add all recipients as array for PostageApp BCC mode

    // specify email template depending on business type
    if ($table == 'TBLMASTER')
      $template = 'dealer-new-master';
    else
    {
      $template = 'dealer-new-retailer';
      $params['master_name'] = $master->BUSINESSNAME;

      // query master owner email address and add it to the list of recipients as CC
      list($owner, $error) = getBusinessOwnerOld(array('business_type' => 'Master', 'business_code' => $master->MASTERCD), $dbc);
      if ($error)
        throw new \Exception($error);
      $owner->email_address = trim($owner->email_address);
      if ( ! empty($owner->email_address))
        $params['email'][] = $owner->email_address;
      $params['headers'] = array_merge($params['headers'], array('CC' => $owner->email_address));
    }

    // send welcome email to the newly created business owner
    if ($error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail($template, $params))
      throw new \Exception($error);
  }
  catch(\Exception $e)
  {
    $error = $e->getMessage();
    dlog('', "EXCEPTION: $error");
    if ($rollback)
      logged_mssql_query($rollback);
    return $error;
  }

  // success
  return NULL;
}


/**
 * updateBusiness
 * update existing business record
 * @param Interger user ID performing update
 * @param Array fields to update
 * @returns String NULL on success or error message on failure
 */
function updateBusiness($params)
{
  // get table name for the business type
  if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $params['business_type']))
    return __FUNCTION__ . ": unknown business type {$params['business_type']}";

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return __FUNCTION__ . ': failed to connect to DB';

  // verify access
  list($granted, $error) = authorizeHierarchyAccess($params, $dbc);
  if ( ! $granted)
    return $error;

  // get parent business id if code is given
  list($parent, $error) = verifyParentBusiness($params, $dbc);
  if ($error)
    return $error;
  if ($parent)
    $params['parent_business_id'] = $parent->{getSchemaApiMap($table, NULL, 'business_id')};

  // validate product type if given
  if ( ! empty($params['product_type']))
  {
    list($description, $error) = \Ultra\Lib\DB\DealerPortal\getProductDescription($params['product_type'], $dbc);
    if ($error)
      return $error;
    $params['product_description'] = $description;
  }

  // update business record
  $key = getSchemaApiMap($table, NULL, 'business_id');

  if ($error = updateTable($table, $params, [$key => $params['business_id']]))
    return $error;

  // fetch and update the address record
  list($address, $error) = getBusinessAddress($params);
  if ($error)
    return $error;
  if ($error = updateTable('TBLADDRESS', $params, array('ADDRESSID' => $address->ADDRESSID)))
    return $error;

  // fetch owner record
  list($owner, $error) = getBusinessOwner($params, $dbc);
  if ($error)
    return $error;

  // remove conflicting paramters which occur in both business and owner records
  foreach (array('note', 'effective_date', 'expiration_date', 'active') as $name)
    unset($params[$name]);

  // update owner record
  if ($error = updateTable('TBLPERSON', $params, array('PersonId' => $owner->PersonId)))
    return $error;

  // success
  return NULL;
}


/**
 * createOrganizationUser
 * create a new user, associate to the given organization and send a welcome email
 * @param Object parameters
 * @returns String NULL when successfull or error messsage on failure
 */
function createOrganizationUser($params)
{
  // rollback statement executed on errors
  $rollback = NULL;

  try
  {
    // make it easier to work with
    $params = (object)$params;

    // check required parameters
    foreach (array('username', 'password',  'access_role', 'first_name', 'last_name', 'email', 'phone_number', 'author_id', 'business_type', 'business_code') as $name)
      if (empty($params->$name))
        return "Missing required parameter $name";

    $sql = sprintf(
      "DECLARE @date datetime = GETUTCDATE();
      DECLARE @effective INT = %d;
      DECLARE @expiration INT = %d;
      DECLARE @organization VARCHAR(50) = %s;
      DECLARE @role VARCHAR(20) = %s;
      DECLARE @username VARCHAR(20) = %s;
      INSERT INTO TBL_USER
        (USER_NAME, PASSWORD, FIRST_NAME, LAST_NAME,
        EMAIL_ADDRESS, CONTACT_NUMBER, IS_ACTIVE_FLAG, IS_INTERNAL_USER_FLAG,
        USER_EFFECTIVE_DT, USER_EXPIRATION_DT,
        ACCESS_ROLE_ID,
        ORGLEVELID,
        USER_NOTE, LAST_CHANGE_BY_USER_ID, LAST_CHANGE_DT)
      SELECT
        @username, %s, %s, %s,
        %s, %s, %d, 0,
        DATEADD(s, @effective, '1970-01-01'), CASE @expiration WHEN 0 THEN NULL ELSE DATEADD(s, @effective, '1970-01-01') END,
        (SELECT r.ACCESS_ROLE_ID FROM TBLORGLEVEL o JOIN TBL_ACCESS_ROLE r ON o.ORGLEVELID = r.ORGLEVELID WHERE o.DESCR = @organization AND r.ACCESS_ROLE_NAME = @role),
        (SELECT o.ORGLEVELID FROM TBLORGLEVEL o JOIN TBL_ACCESS_ROLE r ON o.ORGLEVELID = r.ORGLEVELID WHERE o.DESCR= @organization AND r.ACCESS_ROLE_NAME = @role),
        %s, %d, @date
      WHERE NOT EXISTS (SELECT USER_NAME FROM TBL_USER WITH (NOLOCK) WHERE USER_NAME = @username)",
      $params->effective_date,
      $params->expiration_date,
      mssql_escape_with_zeroes($params->business_type),
      mssql_escape_with_zeroes($params->access_role),
      mssql_escape_with_zeroes($params->username),
      mssql_escape_with_zeroes($params->password), mssql_escape_with_zeroes($params->first_name), mssql_escape_with_zeroes($params->last_name),
      mssql_escape_with_zeroes($params->email), mssql_escape_with_zeroes($params->phone_number), $params->active,
      mssql_escape_with_zeroes($params->note), $params->author_id);

    // execute INSERT and get newly created user ID
    if ( ! run_sql_and_check($sql))
      throw new \Exception("Failed to create user {$params->username}");
    if ( ! $userId = (integer)get_scope_identity())
      throw new \Exception("Username {$params->username} already taken");
    $rollback = "DELETE FROM TBL_USER WHERE USER_ID = $userId";

    // accosiate with the appropriate business table
    if ($params->business_type == 'MVNO')
      $sql = 'INSERT INTO TBLUSERNVO (NVOID, USERID) VALUES ((SELECT NVOID FROM TBLNVO WITH (NOLOCK) WHERE NVOCD = %s), %d)';
    elseif ($params->business_type == 'Master' || $params->business_type == 'SubDistributor')
      $sql = 'INSERT INTO TBLUSERMASTER (MASTERID, USERID) VALUES ((SELECT MASTERID FROM TBLMASTER WITH (NOLOCK) WHERE MASTERCD = %s), %d)';
    elseif ($params->business_type == 'Dealer')
      $sql = 'INSERT INTO TBLUSERDEALERSITE (DEALERSITEID, USERID) VALUES ((SELECT DEALERSITEID FROM TBLDEALERSITE WITH (NOLOCK) WHERE DEALERCD = %s), %d)';
    else
      throw new \Exception("Uknown organization type {$params->business_type}");
    $sql = sprintf($sql, mssql_escape_with_zeroes($params->business_code, TRUE), $userId);

    // execute INSERT and check result
    if ( ! run_sql_and_check($sql))
      throw new \Exception('Failed to associate user with the organization');

    $sql = sprintf("
      DECLARE @firstName   VARCHAR(50) = '%s';
      DECLARE @lastName    VARCHAR(50) = '%s';
      DECLARE @phoneNumber VARCHAR(20) = '%s';
      DECLARE @email       VARCHAR(50) = '%s';
      INSERT INTO TBLPERSON (FirstName, LastName, Phone, Email) VALUES (@firstName, @lastName, @phoneNumber, @email)",
      $params->first_name,
      $params->last_name,
      $params->phone_number,
      $params->email
    );

    // execute INSERT and get newly created person ID
    if ( ! run_sql_and_check($sql))
      throw new \Exception("Failed to insert row in TBLPERSON");

    $personId = (integer)get_scope_identity();

    if ($params->business_type == 'Dealer')
    {
      $sql = sprintf("
        DECLARE @effective INT = %d;
        INSERT INTO TBLDEALERSITEPERSON (DealerSiteID, PersonID, persontypecd, effectivedt)
        VALUES (
          (SELECT DEALERSITEID FROM TBLDEALERSITE WITH (NOLOCK) WHERE DEALERCD = %s),
          %d,
          '%s',
          DATEADD(s, @effective, '1970-01-01')
        )",
        $params->effective_date,
        mssql_escape_with_zeroes($params->business_code, TRUE),
        $personId,
        'OW'
      );

      // execute INSERT
      if ( ! run_sql_and_check($sql))
        throw new \Exception("Failed to insert row in TBLDEALERSITEPERSON");
    }
    elseif ($params->business_type == 'Master')
    {
      $sql = sprintf("
        DECLARE @effective INT = %d;
        INSERT INTO TBLMASTERPERSON (MasterID, PersonID, persontypecd, effectivedt)
        VALUES (
          (SELECT MASTERID FROM TBLMASTER WITH (NOLOCK) WHERE MASTERCD = %s),
          %d,
          '%s',
          DATEADD(s, @effective, '1970-01-01')
        )",
        $params->effective_date,
        mssql_escape_with_zeroes($params->business_code, TRUE),
        $personId,
        'OW'
      );

      // execute INSERT
      if ( ! run_sql_and_check($sql))
        throw new \Exception("Failed to insert row in TBLMASTERPERSON");
    }
  }
  catch(\Exception $e)
  {
    $error = $e->getMessage();
    dlog('', "EXCEPTION: $error");
    if ($rollback)
      logged_mssql_query($rollback);
    return $error;
  }

  // success
  return NULL;
}


/**
 * getBusinessAddress
 * return address record for a given business
 * @param String business_type
 * @param String business_code
 * @returns Array (Object address on success or NULL on failure, String NULL on success or error message on failure)
 */
function getBusinessAddress($params)
{
  // check required parameters
  if (empty($params['business_type']) || empty($params['business_code']))
    return array(NULL, __FUNCTION__ . ': missing required parameters business_type or business_code');

  if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $params['business_type']))
    return array(FALSE, __FUNCTION__ . ': uknown type of business');

  // prepare SELECT statement
  if ($table == 'TBLMASTER')
    $sql = sprintf(
      "DECLARE @code NVARCHAR(8) = %s;
      DECLARE @type NVARCHAR(4) = 'MA';
      SELECT * FROM TBLMASTERADDRESS r WITH (NOLOCK)
      JOIN TBLMASTER m WITH (NOLOCK) ON r.MASTERID = m.MASTERID
      JOIN TBLADDRESS a WITH (NOLOCK) on r.ADDRESSID = a.ADDRESSID
      WHERE r.ADDRESSTYPECD = @type AND m.MASTERCD = @code AND (r.EXPIRATIONDT IS NULL OR r.EXPIRATIONDT > GETUTCDATE())",
      mssql_escape_with_zeroes($params['business_code'], TRUE));
  else // Dealer
    $sql = sprintf(
      "DECLARE @code NVARCHAR(8) = %s;
      DECLARE @type NVARCHAR(4) = 'MA';
      SELECT * FROM TBLDEALERSITEADDRESS r WITH (NOLOCK)
      JOIN TBLDEALERSITE d WITH (NOLOCK) ON r.DEALERSITEID = d.DEALERSITEID
      JOIN TBLADDRESS a WITH (NOLOCK) on r.ADDRESSID = a.ADDRESSID
      WHERE r.ADDRESSTYPECD = @type AND d.DEALERCD = @code AND (r.EXPIRATIONDT IS NULL OR r.EXPIRATIONDT > GETUTCDATE())",
      mssql_escape_with_zeroes($params['business_code'], TRUE));

  // fetch and analyze result
  $data = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  if (count($data) == 1)
  {
    // convert dates
    $result = $data[0];
    if ( ! empty($result->EFFECTIVEDT))
      $result->EFFECTIVEDT = date_to_epoch($result->EFFECTIVEDT);
    if ( ! empty($result->EXPIRATIONDT))
      $result->EXPIRATIONDT = date_to_epoch($result->EXPIRATIONDT);

    return array($result, NULL);
  }

  if (count($data))
    return array(NULL, 'Multile addresses found');
  else
    return array(NULL, 'No data found');
}


/**
 * getBusinessRecord
 * return business record by business_type and business_code
 * @param String business type
 * @param String business code
 * @param Integer business ID
 * @param Resource database connection
 * @returns Array (Object business record or NULL on failure, String NULL on success or error message)
 */
function getBusinessRecord($type, $code, $id = NULL, $dbc = NULL)
{
  // determine main DB table based on busiss type
  if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $type))
    return array(NULL, __FUNCTION__ . ': uknown type of business');

  // get business code or ID name
  if ( ! $key = getSchemaApiMap($table, NULL, $code ? 'business_code' : 'business_id'))
    return array(NULL, __FUNCTION__ . ": undefined parameter map for $table");

  // prepare SELECT statement
  $value = $code ? $code : $id;
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery($table, NULL, NULL, array($key => $value), NULL, NULL, TRUE))
    return array(NULL, __FUNCTION__ . ': failed to prepare SELECT statement');

  // connect to database
  if ( ! $dbc && ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, __FUNCTION__ . ': failed to connect to DB');

  // get data and analyze results
  $records = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  $count = count($records);

  // if a single record is found then clean and return data
  if ($count == 1)
  {
    $business = $records[0];

    // remove MSSQL replication key
    if (isset($business->MSREPL_TRAN_VERSION))
      unset($business->MSREPL_TRAN_VERSION);

    // remove non printable characters from business name
    $business->BUSINESSNAME = preg_replace('/[^[:print:]]/', '', $business->BUSINESSNAME); // ASCII printable only

    // convert dates to UNIX time
    foreach (array('LASTCHANGEDT', 'INACTIVEDT', 'PARENTDEALERSITEASSIGNDT') as $date)
      if ( ! empty($business->$date))
        $business->$date = date_to_epoch($business->$date);

    return array($business, NULL);
  }

  // errors
  if ($count > 1)
    return array(NULL, __FUNCTION__ . ': multiple records found');
  else
    return array(NULL, __FUNCTION__ . ': invalid business code');
}


/**
 * getBusinessOwner
 * return owner person record for a given business
 * @param Array (String business_type, String business_code)
 * @returns Array (Object address on success or NULL on failure, String NULL on success or error message on failure)
 */
function getBusinessOwner($params, $dbc = NULL)
{
  // check required parameters
  if (empty($params['business_type']) || empty($params['business_code']))
    return array(NULL, __FUNCTION__ . ': missing required parameters business_type or business_code');

  // prepare SELECT statement
  $sql = sprintf('DECLARE @code NVARCHAR(8) = %s;', mssql_escape_with_zeroes($params['business_code'], TRUE));
  if ($params['business_type'] == 'Master' || $params['business_type'] == 'SubDistributor')
    $sql .= " select p.PersonId, p.FirstName, p.LastName, p.Phone, p.Email
      from tblmaster m, tblperson p, tblmasterperson mp
      where m.masterid = mp.masterid
      and mp.personid=p.personid
      and mp.persontypecd = 'OW'
      and mp.effectivedt <= GETUTCDATE()
      and (mp.expirationdt is null OR mp.expirationdt > GETUTCDATE())
      and m.mastercd = @code";
  else // Dealer
    $sql .= " select p.PersonId, p.FirstName, p.LastName, p.Phone, p.Email
      from tbldealersite ds, tblperson p, tbldealersiteperson dsp
      where ds.dealersiteid = dsp.dealersiteid
      and dsp.personid=p.personid
      and dsp.persontypecd = 'OW'
      and dsp.effectivedt <= GETUTCDATE()
      and (dsp.expirationdt is null OR dsp.expirationdt > GETUTCDATE())
      and ds.dealerCd = @code";

  // connect to database
  if ( ! $dbc && ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, __FUNCTION__ . ': failed to connect to DB');

  // fetch and analyze result
  $result = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TP);
  if (count($result) == 1)
    return array($result[0], NULL);
  if (count($result))
    return array(NULL, __FUNCTION__ . ': multiple owners found');
  return array(NULL, __FUNCTION__ . ': no data found');
}

/**
 * getBusinessOwnerOld
 * return owner person record for a given business
 * @param Array (String business_type, String business_code)
 * @returns Array (Object address on success or NULL on failure, String NULL on success or error message on failure)
 */
function getBusinessOwnerOld($params, $dbc = NULL)
{
  // check required parameters
  if (empty($params['business_type']) || empty($params['business_code']))
    return array(NULL, __FUNCTION__ . ': missing required parameters business_type or business_code');

  // prepare SELECT statement
  $sql = sprintf('DECLARE @code NVARCHAR(8) = %s;', mssql_escape_with_zeroes($params['business_code'], TRUE));
  if ($params['business_type'] == 'Master' || $params['business_type'] == 'SubDistributor')
    $sql .= "SELECT TOP 2 * FROM TBL_USER u WITH (NOLOCK)
      JOIN TBL_ACCESS_ROLE r WITH (NOLOCK) ON u.ACCESS_ROLE_ID = r.ACCESS_ROLE_ID
      JOIN TBLUSERMASTER um WITH (NOLOCK) ON u.USER_ID = um.USERID
      JOIN TBLMASTER m WITH (NOLOCK) ON um.MASTERID = m.MASTERID
      WHERE r.ACCESS_ROLE_NAME = 'OWNER' AND m.MASTERCD = @code";
  else // Dealer
    $sql .= "SELECT TOP 2 * FROM TBL_USER u WITH (NOLOCK)
      JOIN TBL_ACCESS_ROLE r WITH (NOLOCK) ON u.ACCESS_ROLE_ID = r.ACCESS_ROLE_ID
      JOIN TBLUSERDEALERSITE ud WITH (NOLOCK) ON u.USER_ID = ud.USERID
      JOIN TBLDEALERSITE d WITH (NOLOCK) ON ud.DEALERSITEID = d.DEALERSITEID
      WHERE r.ACCESS_ROLE_NAME = 'OWNER' AND d.DEALERCD = @code";

  // connect to database
  if ( ! $dbc && ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, __FUNCTION__ . ': failed to connect to DB');

  // fetch and analyze result
  $result = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TP);
  if (count($result) == 1)
    return array($result[0], NULL);
  if (count($result))
    return array(NULL, __FUNCTION__ . ': multiple owners found');
  return array(NULL, __FUNCTION__ . ': no data found');
}


/**
 * updateTable
 * update a table by a primary key
 * @param String table name
 * @param Array of update parameters (API paramater names)
 * @param Array primary key => value
 * @returns String NULL on success or error message on failure
 */
function updateTable($table, $params, $primary)
{
  // dlog('', 'PARAMS: %s', print_r(func_get_args(), TRUE));

  // get table schema
  if ( ! $schema = \Ultra\Lib\DB\getTableSchema($table))
    return "Table $table is not defined";

  // map API paramaters to DB schema
  if ( ! $map = getSchemaApiMap($table))
    return "Undefined parameter map for $table";
  $values = mapApiParamsToSchema($map, $params, TRUE);

  // remove primary key from parameters if present
  if (isset($values[$key = key($primary)]))
    unset($values[$key]);

  // check if any updates required
  if ( ! count($values))
    return NULL;

  // TODO: fetch and compare existing values before updating

  // generate UPDATE statement
  if ( ! $sql = \Ultra\Lib\DB\makeUpdateQuery($table, $values, $primary))
    return 'Failed to prepare UPDATE statement';

  // update record
  if ( ! run_sql_and_check($sql))
    return 'Failed to execute UPDATE statement';
}


/**
 * listReportBusinesses
 * get business codes and names eligible for reporting
 * @see DEAL-144
 * @param String business_code or NULL for all businesses
 * @returns Array (Array of business record Objects, String NULL on success or error message)
 */
function listReportBusinesses($business_code = NULL)
{
  // construct SQL query: use LEFT JOIN with post-removal instead of NOT IN
  $sql = "SELECT DISTINCT 'Master' AS business_type, m.MASTERCD AS business_code, m.BUSINESSNAME AS business_name, x.ENTITYID AS exclude
    FROM TBLMASTER m WITH (NOLOCK)
    JOIN TBLCOMMISSIONENTITYSCHEDULEPROD i WITH (NOLOCK) ON m.MASTERID = i.ENTITYID
    LEFT JOIN TBLCOMMISSIONEXCLUDEENTITY x WITH (NOLOCK) ON m.MASTERID = x.ENTITYID AND x.ORGLEVELID = 1";

  // limit to the business_code if provided
  if ($business_code)
    $sql = sprintf('DECLARE @business NVARCHAR(8) = %s; %s WHERE m.MASTERCD = @business', mssql_escape_with_zeroes($business_code, TRUE), $sql);

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // get data
  $data = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TP);

  // remove exclusions
  $result = array();
  foreach ($data as $business)
    if ( ! $business->exclude)
    {
      unset($business->exclude);
      $result[] = $business;
    }

  // succeed no matter how many records returned
  return array($result, NULL);
}


/**
 * listReportPeriods
 * get reporting periods available
 * @see DEAL-143
 * @param Array (business_type, business_code, request_type)
 * @returns Array (Array of report period Objects, String NULL on success or error message)
 */
function listReportPeriods($params)
{
  // construct SQL query for specific master
  if ( ! empty($params['business_code']))
    $sql = sprintf(
      'DECLARE @business_code NVARCHAR(8) = %s;
      DECLARE @request_type INT = %d;
      SELECT
        s.COMMISSIONSCHEDULEID AS period_id,
        s.TRANSACTIONSTARTDT AS transaction_start_date,
        s.TRANSACTIONENDDT AS transaction_end_date,
        s.POSTEDDT AS posted,
        s.SCHEDULETEXT as text,
        x.ENTITYID AS exclude
      FROM TBLMASTER m WITH (NOLOCK)
      JOIN TBLCOMMISSIONENTITYSCHEDULEPROD i WITH (NOLOCK) ON m.MASTERID = i.ENTITYID
      JOIN TBLCOMMISSIONSCHEDULEPROD s WITH (NOLOCK) ON i.COMMISSIONSCHEDULEID = s.COMMISSIONSCHEDULEID
      LEFT JOIN TBLCOMMISSIONEXCLUDEENTITY x WITH (NOLOCK) ON m.MASTERID = x.ENTITYID AND x.ORGLEVELID = 1
      WHERE m.MASTERCD = @business_code and s.COMMISSIONTYPEID = @request_type',
      mssql_escape_with_zeroes($params['business_code'], TRUE), $params['request_type']);
  // construct SQL query without any master
  else
    $sql = sprintf(
      'DECLARE @request_type INT = %d;
      SELECT
        s.COMMISSIONSCHEDULEID AS period_id,
        s.TRANSACTIONSTARTDT AS transaction_start_date,
        s.TRANSACTIONENDDT AS transaction_end_date,
        s.POSTEDDT AS posted,
        s.SCHEDULETEXT as text
      FROM TBLCOMMISSIONSCHEDULEPROD s WITH (NOLOCK)
      WHERE s.COMMISSIONTYPEID = @request_type',
      $params['request_type']);

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // get data
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  // post process: convert dates, remove exclusions
  $result = array();
  foreach ($data as $period)
    if (empty($period->exclude))
    {
      $period->transaction_start_date =  date_to_epoch($period->transaction_start_date);
      $period->transaction_end_date = date_to_epoch($period->transaction_end_date);
      $period->posted = $period->posted ? date_to_epoch($period->posted) : 0;
      if (isset($period->exclude))
        unset($period->exclude);
      $result[] = $period;
    }

  // succeed no matter how many records returned
  return array($result, NULL);
}


/**
 * listBusinessReports
 * list reports available for a given user and business (or all businesses)
 * @see DEAL-141
 * @param Array (business_type, [business_code], request_type, user_id)
 * @returns Array (Array of report objects, String NULL on success or error message)
 */
function listBusinessReports($params)
{
  // check required parameters
  foreach (array('business_type', 'request_type', 'user_id') as $required)
    if ( ! isset($params[$required]))
      return array(NULL, "Missing required parameter $required");

  // construct SQL query for specific business
  $sql = sprintf(
    "DECLARE @user_id INT = %d;
    DECLARE @type INT = %d;
    SELECT
      r.REPORTREQUESTID AS report_id,
      r.REPORTREQUESTTYPEID AS request_type,
      r.RequestDt AS report_date,
      r.RequestExpirationDt AS expiration_date,
      r.FILENAME AS file_name,
      'Master' AS business_type,
      m.MASTERCD AS business_code,
      m.BUSINESSNAME AS business_name,
      s.TRANSACTIONSTARTDT AS transaction_start_date,
      s.TRANSACTIONENDDT AS transaction_end_date,
      r.ACTIVATIONSTARTDT,
      r.ACTIVATIONENDDT,
      d.DESCRIPTION AS description,
      x.ENTITYID AS exclude
    FROM TBLREPORTREQUESTCOMMISSIONPROD r WITH (NOLOCK)
    LEFT JOIN TBLCOMMISSIONSCHEDULEPROD s WITH (NOLOCK) ON r.COMMISSIONSCHEDULEID = s.COMMISSIONSCHEDULEID
    LEFT JOIN TBLMASTER m WITH (NOLOCK) ON r.ENTITYID = m.MASTERID
    JOIN TBLREPORTREQUESTSTATUS d WITH (NOLOCK) ON r.REPORTREQUESTSTATUS = d.STATUS
    LEFT JOIN TBLCOMMISSIONEXCLUDEENTITY x WITH (NOLOCK) ON r.ENTITYID = x.ENTITYID AND x.ORGLEVELID = 1
    WHERE r.USERID = @user_id AND r.REPORTREQUESTTYPEID = @type",
    $params['user_id'],
    $params['request_type']);

  // adjust SQL for specific master if given
  if ( ! empty($params['business_code']))
    $sql = sprintf('DECLARE @business NVARCHAR(8) = %s; %s AND m.MASTERCD = @business', mssql_escape_with_zeroes($params['business_code'], TRUE), $sql);

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // fetch and process data: convert dates, remove exclusions, clean business name
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));
  $result = array();
  foreach ($data as $report)
    if ( ! $report->exclude)
    {
      $report->report_date = date_to_epoch($report->report_date);
      if ($report->expiration_date)
        $report->expiration_date = date_to_epoch($report->expiration_date);
      if ($report->request_type == SIM_INVENTORY_REPORT)
      {
        $report->transaction_start_date = $report->ACTIVATIONSTARTDT;
        $report->transaction_end_date = $report->ACTIVATIONENDDT;
      }
      if ( ! empty($report->transaction_start_date))
        $report->transaction_start_date =  date_to_epoch($report->transaction_start_date);
      if ( ! empty($report->transaction_end_date))
        $report->transaction_end_date = date_to_epoch($report->transaction_end_date);

      $report->business_name = preg_replace('/[^[:print:]]/', '', $report->business_name); // clean ASCII printable only

      unset($report->exclude);
      $result[] = $report;
    }

  // succeed no matter how many records returned
  return array($result, NULL);
}


/**
 * listMiscellaneousAdjustments
 * lists misc adjustments given business_type, business_code, period_id
 * @see DEAL-233
 * @param  Array (business_type, business_code, period_id)
 * @return Array (Array of adjustment objects, String NULL on success or error message)
 */
function listMiscellaneousAdjustments($params)
{
  // check required parameters
  foreach (array('business_type', 'business_code', 'period_id') as $required)
    if (empty($params[$required]))
      return array(NULL, __FUNCTION__ . ": missing required parameter $required");

  // connect to DB
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, __FUNCTION__ . ': failed to connect to DB');

  // get Master record
  list($master, $error) = getBusinessRecord($params['business_type'], $params['business_code'], NULL, $dbc);
  if ($error)
    return array(NULL, $error);

  // prepare and execute SELECT
  if ( ! $map = getSchemaApiMap($table = 'TBLCOMMISSIONMISCPROD'))
    return array(NULL, __FUNCTION__ . ': missing paramater map');
  $where = array(
    'ENTITYID'             => $master->MASTERID,
    'ORGLEVELID'           => getOrganizationLevel($params['business_type']),
    'COMMISSIONSCHEDULEID' => $params['period_id']);
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery($table, NULL, NULL, $where, NULL, NULL, TRUE))
    return array(NULL, __FUNCTION__ . ': failed to repare SELECT query');
  $result = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);

  // map schema to API parameters
  foreach ($result as &$adjustment)
  {  
    $adjustment = mapSchemaToApiParams($map, (array)$adjustment, TRUE);
    $adjustment['amount'] *= 100; // convert dollars to cents
  }

  return array($result, NULL);
}


/**
 * deleteMiscellaneousAdjustment
 * deletes a misc adjustment with adjustment_id from the database
 * @param  Integer adjustment_id
 * @return String  error || NULL
 */
function deleteMiscellaneousAdjustment($adjustment_id)
{
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return 'Failed to connect to DB';

  $sql = \Ultra\Lib\DB\makeDeleteQuery('TBLCOMMISSIONMISCPROD', array('COMMISSIONMISCID' => $adjustment_id));
  $success = is_mssql_successful(logged_mssql_query($sql));

  return ( ! $success) ? 'DB Error: Failed to delete adjustment' : NULL;
}


/**
 * updateMiscellaneousAdjustment
 * updates amount, comment for a misc adjustment with adjustment_id
 * @param  Array  (adjustment_id, amount, comment)
 * @return String error || NULL
 */
function updateMiscellaneousAdjustment($params)
{
  // check required parameters
  if ( ! $params['adjustment_id'])
    return 'Missing required parameter adjustment ID';

   // parameter amount is in cents
  if ($params['amount'])
    $params['amount'] = $params['amount'] / 100;

  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return 'Failed to connect to DB';

  return updateTable('TBLCOMMISSIONMISCPROD', $params, array('COMMISSIONMISCID' => $params['adjustment_id']));
}


/**
 * requestBusinessReport
 * insert a new record into tblReportRequestCommissionProd
 * @see DEAL-142
 * @param Array (business_type, [business_code], request_type, user_id, [period_id])
 * @returns String NULL on success or error message on failure
 */
function requestBusinessReport($params)
{
  // check required parameters
  foreach (array('business_type', 'request_type', 'user_id') as $required)
    if ( ! isset($params[$required]))
      return "Missing required parameter $required";

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return 'Failed to connect to DB';

  // get Master record
  if ($params['business_code'])
  {
    list($master, $error) = getBusinessRecord($params['business_type'], $params['business_code'], NULL, $dbc);
    if ($error)
      return $error;
  }

  // get User record
  list($users, $error) = getUser(array('user_id' => $params['user_id']), $dbc);
  if ($error)
    return $error;
  $user = (object)$users[0];

  // get report file attributes
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery('TBLREPORTREQUESTTYPE', 1, NULL, array('REPORTTYPEID' => $params['request_type']), NULL, NULL, TRUE))
    return 'Invalid report request schema';
  $metas = mssql_fetch_all_objects(logged_mssql_query($sql));
  if ( ! count($metas))
    return 'Invalid report request type';
  $meta = $metas[0];

  // prepare common INSERT values
  $values = array(
    'REPORTREQUESTTYPEID'   => $params['request_type'],
    'ENTITYID'              => $params['business_code'] ? $master->MASTERID : 0,
    'ORGLEVELID'            => 1, // always Master
    'COMMISSIONSCHEDULEID'  => 0,
    'FILEPATH'              => $meta->BaseFilePath,
    'REQUESTDT'             => 'NOW',
    'REQUESTEXPIRATIONDT'   => time() + 2 * SECONDS_IN_DAY,
    'REPORTREQUESTSTATUS'   => 0,
    'USERID'                => $user->user_id);
  if ($params['notify'])
    $values['eMailNotification'] = $user->email;

  // request type 1 (Commission Detail Report) and 2 (Commission Summary Report) require reporting period data
  if ($params['request_type'] == 1 || $params['request_type'] == 2)
  {
    // required parameters commissions reports
    if (empty($params['period_id']))
      return 'Missing required parameter period ID';

    // fetch and verify reporting period
    list($period, $error) = getCommissionPeriod($params['period_id']);
    if ($error)
      return $error;

    // add commissions specific INSERT values
    $values['COMMISSIONSCHEDULEID'] = $params['period_id'];

    // VYT @ 2015-07026: based on Harish @ Celluphone SQL SP
    $values['FILENAME'] = 'Activity' .
      date('Y-m-d', strtotime($period->PayPeriod)) .
      $meta->BaseFileName .
      $params['user_id'] .
      ($params['business_code'] ? trim(substr($master->BUSINESSNAME, 0, 5)) : 'AllMasters') .
      date('YmdHis') .
      '.txt';
  }

  // request type 3 (SIM Inventory Report)
  elseif ($params['request_type'] == 3)
  {
    // required parameters commissions reports
    if ( ! $params['inactive'] && (empty($params['start_date']) || empty($params['end_date'])))
      return 'Missing required parameters start_date or end_date';

    // add SIM report specific INSERT values
    $values['NONACTIVATEDFLAG'] = (int)$params['inactive'];
    if ($params['start_date'])
      $values['ACTIVATIONSTARTDT'] = date(MSSQL_DATE_FORMAT, $params['start_date']);
    if ($params['end_date'])
      $values['ACTIVATIONENDDT'] = date(MSSQL_DATE_FORMAT, $params['end_date']);

    // VYT @ 2015-07026: based on Harish @ Celluphone SQL SP
    $values['FILENAME'] = $meta->BaseFileName .
      ($params['inactive'] ? 'NONActivated-' : 'Activated' . date('Ymd', $params['start_date']) . 'To' . date('Ymd', $params['end_date'])) .
      $params['user_id'] .
      ($params['business_code'] ? trim(substr($master->BUSINESSNAME, 0, 5)) : 'AllMasters') .
      date('YmdHis') .
      '.txt';
  }
  else
    return 'Invalid report request type';

  if (checkDuplicateBusinessReport($values))
    return 'Duplicate business report request';

  // prepare INSERT statement
  if ( ! $sql = \Ultra\Lib\DB\makeInsertQuery('TBLREPORTREQUESTCOMMISSIONPROD', $values))
    return 'Failed to prepare INSERT statement';

  // execute INSERT
  return logged_mssql_query($sql) ? NULL : 'Failed to submit report request';
}


/**
 * getProductDescription
 * validate sum of product flags and get full description of SIM product(s) which is a concatenation of individual product descriptions based on product IDs
 * @param Integer product IDs (sum of product ID bit flags)
 * @returns Array (String product(s) description or NULL on failure, String NULL or error message on failure)
 */
function getProductDescription($flags, $dbc)
{
  if (empty($flags))
    return array(NULL, __FUNCTION__ . ': invalid product ID');

  // extract flags into array
  $ids = array();
  for ($i = 1; $i < 32; $i *= 2)
    if ($i & $flags)
      $ids[] = $i;

  // prepare SELECT statement
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery('TBLPRODUCTTYPE', NULL, NULL, array('FLAGVALUE' => $ids), NULL, NULL, TRUE))
    return array(NULL, __FUNCTION__ . ': failed to prepare product SELECT query');

  // fetch and combine product flag sum and descriptions
  $products = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  $description = '';
  $sum = 0;
  foreach ($products as $product)
  {
    $sum += $product->FLAGVALUE;
    $description .= ($description ? ',' : NULL) . $product->DESCRIPTION;
  }
  
  // validate flag sum
  if ($flags != $sum)
    return array(NULL, __FUNCTION__ . ': invalid product ID');

  // success
  dlog('', 'flags: %s, product IDs: %s, description: %s', $flags, $ids, $description);
  return array($description, NULL);
}


/**
 * createMiscellaneousAdjustment
 * create a single row in tblCommissionMiscProd
 * @param Array parameters
 * @returns String NULL or error message on failure
 */
function createMiscellaneousAdjustment($params)
{
  // check required parameters
  foreach (array('business_type', 'business_code', 'amount', 'period_id', 'author_id') as $required)
    if (empty($params[$required]))
      return "Missing required parameter $required";

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // get bussiness record
  list($master, $error) = getBusinessRecord($params['business_type'], $params['business_code'], NULL, $dbc);
  if ($error)
    return $error;
  // dlog('', 'MASTER: %s', print_r($master, TRUE));

  // get commission schedule record
  list($period, $error) = getCommissionPeriod($params['period_id']);
  if ($error)
    return $error;
  // dlog('', 'PERIOD: %s', print_r($period, TRUE));
  
  // prepare required parameters
  $values = array(
    'CUSTOMERID'            => 0,
    'TRANSACTIONID'         => 0,
    'TRANSACTIONDT'         => 'NOW',
    'ENTITYID'              => $master->MASTERID,
    'ORGLEVELID'            => getOrganizationLevel($params['business_type']),
    'COMMAMOUNT'            => $params['amount'] / 100, // parameter amount is in cents
    'TRANSACTIONTYPECD'     => 'MX',
    'TRANSACTIONTYPEDESCR'  => 'Misc Adjustment',
    'PAIDDT'                => $period->PaidDt,
    'COMMISSIONSCHEDULEID'  => $period->CommissionScheduleID,
    'COMMISSIONTYPEID'      => $period->CommissionTypeID,
    'CREATEDDT'             => 'NOW',
    'CREATEDBYUSERID'       => $params['author_id']);

  // optional parameters
  if ($params['comment'])
    $values['COMMENTTEXT'] = $params['comment'];

  // prepare INSERT statement
  if ( ! $sql = \Ultra\Lib\DB\makeInsertQuery('TBLCOMMISSIONMISCPROD', $values))
    return 'Failed to prepare INSERT statement';

  // execute INSERT
  return logged_mssql_query($sql) ? NULL : 'Failed to create miscellaneous adjustment record';
}


/**
 * getOrganizationLevel
 * return organizational level for the given business type
 * this could be a DB query to tblOrgLevel but since these values are mostly hardcoded there is no need for a network trip
 * @param String business type or NULL on failure
 * @return Integer organization level
 */
function getOrganizationLevel($businessType)
{
  switch (strtoupper($businessType))
  {
    case 'MVNO':
      return 3;
    case 'MASTER':
      return 1;
    case 'SUBDISTRIBUTOR':
      return 2;
    case 'DEALER':
      return 4;
    default:
      dlog('', "ERROR: unknown organization type $businessName");
      return NULL;
  }
}


/**
 * getCommissionPeriod
 * fetch commission report period based on period ID
 * @param Integer period ID
 * @returns Array (Object period record or NULL on failure, String NULL or eror message on failure)
 */
function getCommissionPeriod($periodId)
{
  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return array(NULL, 'Failed to connect to DB');

  // fetch reporting period
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery('TBLCOMMISSIONSCHEDULEPROD', 2, NULL, array('COMMISSIONSCHEDULEID' => $periodId), NULL, NULL, TRUE))
    return array(NULL, 'Invalid report request schema');
  $periods = mssql_fetch_all_objects(logged_mssql_query($sql));

  // return result
  if (count($periods) != 1)
    return array(NULL, "Cannot find period ID $periodId");

  return array(count($periods) ? $periods[0] : NULL, NULL);
}

/**
 * verifyParentBusiness
 * check API parameters to ensure that that 1) parent business is the same type as subject business and b) parent business exists
 * @param Array API parameters (business_type, business_code, parent_business_type, parent_business_code)
 * @param Resource DB connection
 * @returns Array (Object business record or NULL on error, NULL or String error message on error)
 */
function verifyParentBusiness($params, $dbc)
{
  // parent business parameters are optional
  if (empty($params['parent_business_type']) && empty($params['parent_business_code']))
    return array(NULL, NULL);

  // check for missing values
  if (empty($params['parent_business_type']) || empty($params['parent_business_code']))
    return array(NULL, __FUNCTION__ . ': missing parent business parameter');

  // parent must be the same type as subject business
  if (getSchemaApiMap('BUSINESS_TYPE', $params['business_type']) != getSchemaApiMap('BUSINESS_TYPE', $params['parent_business_type']))
    return array(NULL, __FUNCTION__ . ': invalid parent business type');

  // get parent business
  return getBusinessRecord($params['parent_business_type'], $params['parent_business_code'], NULL, $dbc);
}


/**
 * authorizeHierarchyAccess
 * given a user ID verify access to the requested business record
 * MVNO: all access, Master: own record, own subdistributors and own dealers, SubDistributor: own record and own dealers, Dealer: own and children record only
 * @params Array (user_id, business_type, business_code)
 * @param Resource DB connection
 * @returns Array(Boolean TRUE access authorized or FALSE access denied, String NULL on success or error message on failure)
 */
function authorizeHierarchyAccess($params, $dbc)
{
  // author_id overwrites user_id
  if ( ! empty($params['author_id']))
    $params['user_id'] = $params['author_id'];

  // all parameters are required
  if (empty($params['user_id']) || empty($params['business_type']) || empty($params['business_code']))
    return array(FALSE, __FUNCTION__ .': missing required parameters');

  // get target business schema table
  if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $params['business_type']))
    return array(FALSE, __FUNCTION__ . ': uknown type of business');

  // get user organization type and ID
  $sql = sprintf('SELECT a.NVOID, m.MASTERID, d.DEALERSITEID
    FROM TBL_USER u WITH (NOLOCK)
    LEFT JOIN TBLUSERNVO a WITH (NOLOCK) ON u.USER_ID = a.USERID
    LEFT JOIN TBLUSERMASTER m WITH (NOLOCK) ON u.USER_ID = m.USERID
    LEFT JOIN TBLUSERDEALERSITE d WITH (NOLOCK) ON u.USER_ID = d.USERID
    LEFT JOIN TBLDEALERSITE ds ON d.DEALERSITEID = ds.DEALERSITEID AND ds.ACTIVEFLAG = 1
    WHERE u.USER_ID = %d',
    $params['user_id']);
  $data = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  if ( ! count($data))
    return array(FALSE, __FUNCTION__ . ': user not found');
  if (count($data) > 1)
    return array(FALSE, __FUNCTION__ . ': database inconsistency');
  $organization = $data[0];

  // MVNO: allow all access
  if ( ! empty($organization->NVOID))
    return array(TRUE, NULL);

  // master: own, children and own dealer records only
  elseif ( ! empty($organization->MASTERID))
  {
    if ($table == 'TBLNVO')
      return array(FALSE, __FUNCTION__ . ': hierarchy violation');

    // access to Master or SubDistributor
    if ($table == 'TBLMASTER')
    {
      $clause = array(
        'MASTERCD' => $params['business_code'],
         sprintf('(MASTERID = %d OR PARENTMASTERID = %d)', $organization->MASTERID, $organization->MASTERID));
      $sql = \Ultra\Lib\DB\makeSelectQuery($table, 1, NULL, $clause, NULL, NULL, TRUE);
    }
    else // access to Dealer
    {
      // prepare SQL for all dealer and children under this master
      $clause = array(
        'DEALERCD' => $params['business_code'],
        'MASTERID' => $organization->MASTERID);
      $sql = \Ultra\Lib\DB\makeSelectQuery($table, 1, NULL, $clause, NULL, NULL, TRUE);
    }
  }

  else // Dealer: self and children only
  {
    if ($table == 'TBLDEALERSITE')
    {
      $clause = array(
        'DEALERCD' => $params['business_code'],
        sprintf('(DEALERSITEID = %d OR PARENTDEALERSITEID = %d)', $organization->DEALERSITEID, $organization->DEALERSITEID));
      $sql = \Ultra\Lib\DB\makeSelectQuery($table, 1, NULL, $clause, NULL, NULL, TRUE);
    }
    else if ($table == 'TBLMASTER')
    {
      $sql = sprintf("DECLARE @BUSINESSCD NVARCHAR(8) = '%s'; SELECT * FROM TBLMASTER as M
      LEFT JOIN TBLDEALERSITE as D ON D.DEALERSITEID = %d
      WHERE M.MASTERCD = @BUSINESSCD AND M.MASTERID = D.MASTERID",
        $params['business_code'],
        $organization->DEALERSITEID
      );
    }
    else
      return array(FALSE, __FUNCTION__ . ': hierarchy violation');
  }

  if ( ! $sql)
    return array(FALSE, __FUNCTION__ . ': failed to repare SELECT query');

  // get allowed businesses
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));
  if ( ! count($data))
    return array(FALSE, __FUNCTION__ . ': access denied');
  return array(TRUE, NULL);
}


/**
 * logUserAccess
 * log successfull user authentication
 * @see DEAL-312
 * @param Object user
 * @returns String NULL on success or error message on failure
 */
function logUserAccess($user)
{
  // validate parameters
  if (empty($user) || empty($user->USER_ID) || empty($user->BUSINESS_CODE) || empty($user->BUSINESS_TYPE))
    return __FUNCTION__ . ': missing required parameters';

    // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return __FUNCTION__ . ': failed to connect to DB';

  // prepare row values
  $ip = \Session::getClientIp();
  $values = array(
    'USER_ID'                       => $user->USER_ID,
    'DEALER_SITE_CD'                => substr($user->BUSINESS_TYPE == 'Dealer' ? $user->BUSINESS_CODE : $user->BUSINESS_TYPE, 0, 10),
    'LOGIN_DATETIME'                => 'NOW',
    'IPADDRESS'                     => $ip,
    'HOST_NAME'                     => gethostbyaddr ($ip), // VYT @ 2015-07-24: this may cause API to hang on timeout
    'SERVER_NAME'                   => gethostname());

  // prepare INSERT statement
  if ( ! $sql = \Ultra\Lib\DB\makeInsertQuery('TBL_USER_ACCESS_LOG', $values))
    return __FUNCTION__ . ': failed to prepare INSERT statement';

  // execute INSERT
  return logged_mssql_query($sql) ? NULL : __FUNCTION__ . ': failed to log user access';
}


/**
 * getBusinessChildren
 * return a list of parent's children
 * @param Array(business_type, business_code)
 * @returns Array (Array of Objects(children of the parent) or NULL on failure, String NULL or error message on failure)
 */
function getBusinessChildren($params, $dbc = NULL)
{
  // all parameters are required
  if (empty($params['business_type']) || empty($params['parent_business_id']))
    return array(NULL, __FUNCTION__ .': missing required parameters');

  // determine schema
  if ( ! $table = getSchemaApiMap('BUSINESS_TYPE', $params['business_type']))
    return array(NULL, __FUNCTION__ . ': uknown type of business');

  // prepare SQL statement from parameters
  if ( ! $map = getSchemaApiMap($table))
    return array(NULL, __FUNCTION__ . ": undefined parameter map for $table");
  $where = mapApiParamsToSchema($map, $params);

  // exclude parent
  $exclude = mapApiParamsToSchema($map, array('business_id' => NULL));
  $where[] = key($where) . ' <> ' . key($exclude);

  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery($table, NULL, NULL, $where, NULL, NULL, TRUE))
    return array(NULL, __FUNCTION__ . ': failed to prepare SELECT query');

  // connect to database
  if ( ! $dbc)
    if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
      return array(NULL, __FUNCTION__ . ': failed to connect to DB');

  // fetch and check data
  $children = mssql_fetch_all_objects(logged_mssql_query($sql), MSSQL_FLAGS_TPC);
  return array($children, NULL);
}


/**
 * computeOneToOneFlag
 * compare given dealer code to credential 'ultra/dealer_portal/one_to_one_prefixes' and determine its one-to-one value
 * @see DSR-140
 * @param String dealer code
 * @returns Boolean one-to-one value
 */
function computeOneToOneFlag($code)
{
  if ( ! $prefixes = find_credential('ultra/dealer_portal/one_to_one_prefixes'))
  {
    \logError('cannot find one-to-one prefixes');
    return FALSE;
  }

  foreach (explode(' ', $prefixes) as $prefix)
    if ($len = strlen($prefix))
      if (substr($code, 0, $len) == $prefix)
        return TRUE;

  return FALSE;
}


/**
 * checkDuplicateBusinessReport
 * @see PROD-1931
 * @params Array of report values
 * @returns Boolean TRUE if duplicate exists, FALSE otherwise
 */
function checkDuplicateBusinessReport($values)
{
  $sql = sprintf(
    'DECLARE @request INT = %d;
    DECLARE @entity INT = %d;
    DECLARE @schedule INT = %d;
    DECLARE @user INT = %d;
    SELECT REPORTREQUESTID FROM TBLREPORTREQUESTCOMMISSIONPROD WITH (NOLOCK) WHERE
      REPORTREQUESTTYPEID = @request AND
      ENTITYID = @entity AND
      COMMISSIONSCHEDULEID = @schedule AND
      USERID = @user AND
      REPORTREQUESTSTATUS <> 2',
    $values['REPORTREQUESTTYPEID'],
    $values['ENTITYID'],
    $values['COMMISSIONSCHEDULEID'],
    $values['USERID']);

  $duplicates = mssql_fetch_all_objects(logged_mssql_query($sql));
  return (boolean)count($duplicates);

}


/**
 * getDealerByTmoCode
 * retrieve dealer record by unique TMO code
 * for internal processes only since this function does not check access permissions
 * @param String TBLDEALERSITE.TMOBILECODE
 * @return Object dealer record or NULL on failure
 */
function getDealerByTmoCode($code)
{
  // prepare statement: join dealer to master
  $sql = sprintf('DECLARE @tmo VARCHAR(24) = %s;
    SELECT TOP 2 d.DEALERSITEID, d.ACTIVEFLAG, d.MASTERID, m.PARENTMASTERID
    FROM TBLDEALERSITE d WITH (NOLOCK) JOIN TBLMASTER m WITH (NOLOCK) on d.MASTERID = m.MASTERID
    WHERE d.TMOBILECODE = @tmo',
    mssql_escape_with_zeroes($code));

  // connect to database
  if ( ! $dbc = \Ultra\Lib\DB\dealer_portal_connect())
    return \logError('Failed to connect to DB');

  // get data and analyze results
  $dealers = mssql_fetch_all_objects(logged_mssql_query($sql));
  $count = count($dealers);
  if ($count > 1)
    return \logError('Multiple records found');
  elseif ($count < 1)
    return \logError('No data found');

  // success: we found just one dealer
  return $dealers[0];
}

