<?php

/**
 * Dealer Portal Reporting functions
 * statistical analyis of dealer portal activity
 * @author VYT, 2016
 */

namespace Ultra\Lib\DB\DealerPortal;


/**
 * getNewActivationsStatistics
 * query ULTRA_DATA and fetch new STATE_ACTIVE dealer activations counts for the last number of given days grouped by dealer and brand
 * requires open DB connection
 * @param Integer max days into the past
 * @return Array of Objects(DEALER_ID, BRAND_ID, COUNT)
 */
function getNewActivationsStatistics($pastDays)
{
  $sql = sprintf('SELECT l.ACTIVATED_DEALER AS DEALER_ID, o.BRAND_ID, COUNT(*) AS COUNT
    FROM HTT_ACTIVATION_LOG l WITH (NOLOCK)
    JOIN HTT_CUSTOMERS_OVERLAY_ULTRA o WITH (NOLOCK) ON l.ACTIVATED_CUSTOMER_ID = o.CUSTOMER_ID
    WHERE o.PLAN_STATE=\'%s\' AND DATEDIFF(DAY, l.ACTIVATED_DATE, GETUTCDATE()) <= %d
    GROUP BY l.ACTIVATED_DEALER, o.BRAND_ID',
    STATE_ACTIVE, $pastDays);
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


/**
 * saveActivationsStatistics
 * insert a row into ULTRA.HISTORICAL_ACTIVATIONS_BY_BRAND
 * requires open DB connection
 * @param Object (DEALER_ID, BRAND_ID, COUNT) as returned by getNewActivationsStatistics
 * @return Boolean TRUE on success or FALSE on error
 */
function saveActivationsStatistics($activations)
{
  // validate params, prohibit insertion of zero statistics
  if ( ! is_object($activations) || empty($activations->DEALER_ID) || empty($activations->BRAND_ID) || empty($activations->COUNT))
    return logErro('invalid parameters');

  // UPSERT: according to some the most efficient method to UPDATE/INSERT because it avoids an extra scan of EXISTS
  $sql = sprintf(
    'UPDATE ULTRA.HISTORICAL_ACTIVATIONS_BY_BRAND
      SET ACTIVATIONS_COUNT = %d, LAST_COMPUTED_DATE = GETUTCDATE()
      WHERE DEALER_ID = %d AND BRAND_ID = %d;
    IF @@ROWCOUNT = 0
      INSERT INTO ULTRA.HISTORICAL_ACTIVATIONS_BY_BRAND
      (DEALER_ID, BRAND_ID, ACTIVATIONS_COUNT, LAST_COMPUTED_DATE)
      VALUES (%d, %d, %d, GETUTCDATE())',
    $activations->COUNT,
    $activations->DEALER_ID, $activations->BRAND_ID,
    $activations->DEALER_ID, $activations->BRAND_ID, $activations->COUNT);
  return is_mssql_successful(logged_mssql_query($sql));
}


/**
 * removeOldActivationsStatistics
 * remove rows of ULTRA.HISTORICAL_ACTIVATIONS_BY_BRAND older than given time
 * @param Integer UNIX time
 * @return Boolean TRUE on success or FALSE on error
 */
function removeOldActivationsStatistics($time)
{
  if ( ! $time)
    return logError("invalid parameter $time");

  $datetime = gmdate(MSSQL_DATE_FORMAT, $time);
  $sql = sprintf('DELETE FROM ULTRA.HISTORICAL_ACTIVATIONS_BY_BRAND WHERE LAST_COMPUTED_DATE < \'%s\'', $datetime);
  return is_mssql_successful(logged_mssql_query($sql));
}

