<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/Celluphone/functions.php';

/**
 * PHPUnit tests for Ultra\Lib\DB\Celluphone class
 */
class CelluphoneTest extends PHPUnit_Framework_TestCase
{

  /**
   * test getDealerInfo
   */
  public function test__getDealerInfo()
  {
    $values = array('dealerId', 'dealerCode', 'dealerName', 'distributorId', 'distributorName', 'masterId', 'masterName');
    teldata_change_db();

    // get dealer by code
    $info = \Ultra\Lib\DB\Celluphone\getDealerInfo('12345');
    print_r($info);
    foreach ($values as $value)
      $this->assertNotEmpty($info->$value);
    
    // get dealer by ID
    $info = \Ultra\Lib\DB\Celluphone\getDealerInfo(NULL, 5196);
    print_r($info);
    foreach ($values as $value)
      $this->assertNotEmpty($info->$value);

    // invalid dealer code
    $info = \Ultra\Lib\DB\Celluphone\getDealerInfo('ABC');
    print_r($info);
    $this->assertEmpty($info);
  }
}
