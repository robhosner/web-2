<?php

/**
 * functions library to access CELLUPHONE database via caching class
 */

namespace Ultra\Lib\DB\Celluphone;

require_once 'Ultra/Lib/DB/Celluphone/class.php';

/**
 * getDealerInfo
 * given dealer code or ID return dealer record
 * @param String dealer code
 * @param Integer dealer ID
 * @return Object dealer (properties: dealerId, dealerCode, dealerName, distributorId, distributorName, masterId, masterName) or NULL on faiure
 */
function getDealerInfo($code = NULL, $id = NULL)
{
  $cache = new \Ultra\Lib\DB\Celluphone;
  return $cache->getDealerInfoFromCache($code, $id);
}

?>
