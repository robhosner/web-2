<?php

namespace Ultra\Lib\DB\CCTransactions;

/**
 * getUltraCCTransactionSum
 *
 * @param  integer $customer_id
 * @param  integer $days_range
 * @param  string $status
 * @param  string $result
 * @return integer $ultra_cc_transaction_count
 */
function getUltraCCTransactionSum( $customer_id, $days_range, $status = 'COMPLETE', $result = "APPROVED" )
{
  $ultra_cc_transaction_count = 0;
  
  $params = array(
    array(
      'param_name' => '@CUSTOMER_ID',
      'variable'   => $customer_id,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255,
    ),
    array(
      'param_name' => '@DAYS_RANGE',
      'variable'   => $days_range,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255,
    ),
    array(
      'param_name' => '@STATUS',
      'variable'   => $status,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 20,
    ),
    array(
      'param_name' => '@RESULT',
      'variable'   => $result,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 20,
    ),
  );

  $data = \Ultra\Lib\DB\run_stored_procedure( '[ultra].[Get_Ultra_CC_Transaction_Sum]', $params );

  if ( $data && is_array( $data ) && count( $data ) > 0 )
  {
  	$ultra_cc_transaction_count = $data[0][0];
  } 
  return $ultra_cc_transaction_count;
}