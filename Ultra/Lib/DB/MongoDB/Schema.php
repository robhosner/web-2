<?php

namespace Ultra\Lib\DB\MongoDB;

require_once 'Ultra/Lib/DB/MongoDB/Schema/Events.php';

/************************/
/* Ultra MongoDB Schema */
/************************/

/**
 * schema
 *
 * MongoDB schema definition
 *
 * @return array
 */
function schema()
{
  $eventsSchema = \Ultra\Lib\DB\MongoDB\Schema\Events();

  return array_merge(
    $eventsSchema,
    array()
  );
}

