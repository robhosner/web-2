<?php

namespace Ultra\Lib\DB\AccountALiases;

/**
* insertAccountAliases
* 
* Executes stored procedure [ULTRA].[ACCOUNT_ALIASES_INSERT]
* 
* @return string
*/
function insertAccountAliases($payload)
{
  $params = array(
    array(
      'param_name' => '@ALIAS',
      'variable'   => $payload['msisdn'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
    array(
      'param_name' => '@ACCOUNT_ID',
      'variable'   => $payload['account_id'],
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
    array(
      'param_name' => '@DNIS',
      'variable'   => "*",
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 256,
    ),
    array(
      'param_name' => '@ACCOUNT_ALIAS_TYPE',
      'variable'   => 1,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
    array(
      'param_name' => '@PASSWORD',
      'variable'   => 'NULL',
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
    array(
      'param_name' => '@SIP_USER_ID',
      'variable'   => "*",
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ),
  );
  return \Ultra\Lib\DB\run_stored_procedure('[ultra].[account_aliases_insert]', $params);
}
