<?php

namespace Ultra\Lib\DB;

require_once 'Ultra/Lib/Util/SQLite.php';

/**
 * Class used to interface w/Intake table.
 * @see Ultra\Lib\Util\SQLite
 * @author bwalters@ultra.me
 */
class Intake {

  private $database_name  = 'intake_database.sqlite';
  private $table_name     = "INTAKE";
  private $db             = null;

  /**
   * Constructs a new Intake table db connection.
   * @param string $database_name
   */
  public function __construct($database_name = false)
  {
    $this->database_name = ($database_name) ? $database_name : $this->database_name;
    $this->db = new \Ultra\Lib\Util\SQLite($this->database_name);
    $this->createTableIfNotExist();
  }

  /**
   * Creates table if it doesn't exist.
   */
  private function createTableIfNotExist()
  {
    $this->db->query($this->getTableDefinition());
  }

  /**
   * String representation of table definition and column explanations.
   * @return string
   */
  protected function getTableDefinition()
  {
    /*
    request_name          STRING, // "Create a new form"
    request_description   STRING, // "Form should allow people to enter new feature requests."
    start_date            STRING, // "2015-01-15"
    due_date              STRING, // "2015-01-16"
    priority              STRING, // {"must_have": false, "nice_to_have": false}
    status                STRING, // {"idea": false, "approved": false, "on_hold": false}
    demand_type           STRING, // {"sales": false, "marketing": false, "care": false, "tech_environment": false, "tech_new": false}
    business_values       STRING, // {"gross_adds": false, "churn_retention": false, "cost_saving": false, "stability": false}
    tech_dependency       STRING, // {"front_end": false, "api": false, "back_end": false}
    launch_communications STRING, // {"field": false, "release_notes": false}
    knowledge_transfer    STRING, // {"it": false, "ops": false, "care": false}
    training              STRING, // {"field": false, "care": false}
    */
    return
      "CREATE TABLE IF NOT EXISTS {$this->table_name} (\n" .
        "request_name           STRING,\n" .
        "request_description    STRING,\n" .
        "start_date             STRING,\n" .
        "due_date               STRING,\n" .
        "priority               STRING,\n" .
        "status                 STRING,\n" .
        "demand_type            STRING,\n" .
        "business_values        STRING,\n" .
        "tech_dependency        STRING,\n" .
        "launch_communications  STRING,\n" .
        "knowledge_transfer     STRING,\n" .
        "training               STRING \n" .
      ");";
  }

  /**
   * Inserts a row into the intake table.
   * @param array $parameters
   * @return boolean
   * @throws \Exception
   */
  public function addRow($parameters)
  {
    return $this->db->insertRow($this->table_name, $parameters);
  }

  /**
   * Returns all rows from the intake table.
   * @return array
   * @throws \Exception
   */
  public function getAllRows()
  {
    return $this->db->select($this->table_name);
  }
}