package Ultra::Lib::DB::UltraSettings;


use strict;
use warnings;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::DB::UltraSettings

=head1 Perl module to access DB table ULTRA_SETTINGS

=head1 SYNOPSIS

  use Ultra::Lib::DB::UltraSettings;

  my $db = Ultra::Lib::DB::MSSQL->new();

  my $dbUltraSettings = Ultra::Lib::DB::UltraSettings->new( DB => $db );

  my $data = $dbUltraSettings->getUltraSettings( 'test' );

=cut


=head4 getUltraSettings

  Retrieve a row from DB table ULTRA_SETTINGS

=cut
sub getUltraSettings
{
  my ($this,$token) = @_;

  my $row;

  my $query = " SELECT * FROM ULTRA_SETTINGS WHERE NAME = ? ";

  my $sth = $this->{ DB }->executeSelect( $query , $token );

  if ( $this->{ DB }->hasErrors() )
  {
    $this->addErrors( $this->{ DB }->getErrors() );
  }
  else
  {
    $row = $sth->fetchrow_hashref();
  }

  return $row;
}


=head4 assignUltraSettings

  Assign the value to a ULTRA_SETTINGS token

=cut
sub assignUltraSettings
{
  my ($this,$token,$value,$ttlSeconds) = @_;

  my $sql = "
IF EXISTS (SELECT NAME FROM ULTRA_SETTINGS WHERE NAME = ? )
UPDATE  ULTRA_SETTINGS
  SET   VALUE = ? , TTL_SECONDS = ?
  WHERE NAME  = ?
ELSE
  INSERT INTO ULTRA_SETTINGS ( NAME , VALUE , TTL_SECONDS )
  VALUES ( ? , ? , ? )
  ";

  my $values = [ $token , $value , $ttlSeconds , $token , $token , $value , $ttlSeconds ];

  my ($success, $rows) = $this->{ DB }->executeWrite($sql,$values);

  if ( $this->{ DB }->hasErrors() )
  {
    $this->addErrors( $this->{ DB }->getErrors() );
  }

  return $success;
}


1;


__END__

