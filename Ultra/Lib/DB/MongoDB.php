<?php

namespace Ultra\Lib\DB;

include_once('classes/Outcome.php');
include_once('Ultra/UltraConfig.php');

/*****************************/
/* Generic MongoDB functions */
/*****************************/

/**
 * removeMongoDBDocument
 *
 * Removes a document in a MongoCollection
 *
 * @param \MongoCollection $collection
 * @param array $criteria
 * @return \Outcome Object
 */
function removeMongoDBDocument( \MongoCollection $collection , array $criteria, array $options=array() )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  // MongoCollection::remove
  $start = microtime(TRUE);
  $result = $collection->remove( $criteria, $options );
  \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));

  dlog('',"result = %s",$result);

  if ( empty( $result ) || ! is_array($result) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB remove error' , 'DB0003' );
  elseif ( $result['ok'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB remove error ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );

  return $outcome;
}

/**
 * updateMongoDBDocument
 *
 * Updates a document in a MongoCollection
 *
 * @param \MongoCollection $collection
 * @param array $criteria
 * @param array $newData
 * @return \Outcome Object
 */
function updateMongoDBDocument( \MongoCollection $collection , array $criteria , array $newData , array $options=array() )
{
  dlog('', '(%s)', func_get_args());

  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! count( $criteria ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no criteria)' , 'DB0003' );

  if ( ! count( $newData ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no newData)' , 'DB0003' );

  if ( $outcome->is_failure() )
    return $outcome;

  // MongoCollection::update
  $start = microtime(TRUE);
  if ( !empty( $newData['$set'] ) || !empty( $newData['$inc'] ) || !empty( $newData['$unset'] ) )
  {
    $result = $collection->update( $criteria , $newData, $options );
  }
  else
  {
    $result = $collection->update( $criteria , array( '$set' => $newData ), $options );
  }
  \logInfo(sprintf('query time: %.4f (sec)', microtime(TRUE) - $start));

  dlog('',"result = %s",$result);

  if ( empty( $result ) || ! is_array($result) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error' , 'DB0003' );
  elseif ( $result['ok'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );
  elseif ( empty( $options['upsert'] ) && empty( $options['multiple'] ) && ( $result['nModified'] != 1 ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error ' . $result['nModified'] . ' documents modified' , 'DB0004' );
  elseif ( ! empty( $options['upsert'] ) && $options['upsert'] && empty( $result['updatedExisting'] ) && empty( $result['upserted'] ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB upsert failed' , 'DB0005' );

  if ( !empty( $result ) && is_array( $result ) )
  {
    if ( ! empty( $result['nModified'] ) )
      $outcome->data_array['documents_modified'] = $result['nModified'];
    if ( ! empty( $result['updatedExisting'] ) )
      $outcome->data_array['updated_existing'] = $result['updatedExisting'];
    if ( ! empty( $result['upserted'] ) )
      $outcome->data_array['upserted'] = $result['upserted'];
  }

  return $outcome;
}

/**
 * pullFromListInMongoDBDocument
 *
 * Removes $element from the list $listName in all documents matching $criteria
 *
 * @param \MongoCollection $collection
 * @param array $criteria
 * @param string $listName
 * @param string or array $element
 * @return \Outcome Object
 */
function pullFromListInMongoDBDocument( \MongoCollection $collection , array $criteria , $listName , $element )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! count( $criteria ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no criteria)' , 'DB0003' );

  if ( empty( $element ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no element)' , 'DB0003' );

  if ( $outcome->is_failure() )
    return $outcome;

  if ( is_array( $element ) )
    $element = array( '$in' => $element );

  if ( is_array( $element ) )
    $element = \strvalArrayValues( $element );
  else
    $element = strval( $element );

  // MongoCollection::update
  $start = microtime(TRUE);
  $result = $collection->update( $criteria , array( '$pull' => array( $listName => $element ) ) );
  \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));

  dlog('',"result = %s",$result);

  if ( empty( $result ) || ! is_array($result) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error' , 'DB0003' );
  elseif ( $result['ok'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );
  elseif ( $result['nModified'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error ' . $result['nModified'] . ' documents modified' , 'DB0004' );
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * pushMongoDBDocument
 *
 * Adds an element to a list in a MongoCollection
 *
 * @param \MongoCollection $collection
 * @param array $criteria
 * @param string $listName
 * @param string or array $element
 * @return \Outcome Object
 */
function pushMongoDBDocument( \MongoCollection $collection , array $criteria , $listName , $element )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! count( $criteria ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no criteria)' , 'DB0003' );

  if ( ! count( $listName ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no listName)' , 'DB0003' );

  if ( empty( $element ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters (no element)' , 'DB0003' );

  if ( $outcome->is_failure() )
    return $outcome;

  $result = NULL;

  // MongoCollection::update
  if ( is_array( $element ) )
  {
    $element = \strvalArrayValues( $element );

    $start = microtime(TRUE);
    $result  = $collection->update( $criteria , array( '$push' => array( $listName => array( '$each' => $element ) ) ) );
    \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
  }
  else
  {
    $element = strval( $element );

    $start = microtime(TRUE);
    $result  = $collection->update( $criteria , array( '$push' => array( $listName => strval( $element ) ) ) );
    \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
  }

  dlog('',"result = %s",$result);

  if ( empty( $result ) || ! is_array($result) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error' , 'DB0003' );
  elseif ( $result['ok'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );
  elseif ( $result['nModified'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB update error ' . $result['nModified'] . ' documents modified' , 'DB0004' );
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * getMongoDBDocuments
 *
 * Retrieves documents from a MongoCollection
 *
 * @param \MongoCollection $collection
 * @return \Outcome Object
 */
function getMongoDBDocuments( \MongoCollection $collection , array $query , array $sortArr=array(), $limit=NULL, array $fieldsArr=array(), $excludeMongoID=TRUE, $skip=NULL )
{
  $outcome = new \Outcome();
  $outcome->succeed();
  $outcome->data_array['documents'] = array();

  // MongoCollection::find
  $start = microtime(TRUE);
  $cursor = $collection->find($query, $fieldsArr);
  \logInfo(sprintf('conditions: %s query time: %.6f (sec)', json_encode($query), microtime(TRUE) - $start));

  if ( ! empty( $sortArr ) )
    $cursor->sort( $sortArr );

  if ( ! empty( $limit ) )
    $cursor->limit( $limit );

  if ( ! empty( $skip ) )
    $cursor->skip( $skip );

  if ( empty( $cursor ) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB no data found' , '------' );
  else
  {
    foreach ($cursor as $doc)
    {
      if ( $excludeMongoID )
      {
        // remove MongoId object
        unset($doc['_id']);
      }
      else
      {
        // set field to string of MongoId object
        $doc['_id'] = $doc['_id']->{'$id'};
      }

      $outcome->data_array['documents'][] = $doc;
    }

    \logInfo(sprintf('retrieval time: %.6f (sec)', microtime(TRUE) - $start));
  }

  return $outcome;
}

/**
 * getDistinctValues
 *
 * Retrieves unique values for a field in a collection
 *
 * @param \MongoCollection $collection
 * @return \Outcome Object
 */
function getDistinctValues( \MongoCollection $collection, $field, array $query=array() )
{
  $outcome = new \Outcome();
  $outcome->succeed();
  $outcome->data_array['documents'] = array();

  $values = $collection->distinct( $field, $query );
  if ( $values === false )
    $outcome->add_errors_and_code( 'MongoDB distinct query failed', 'IN0002' );
  else
    foreach( $values as $value )
      $outcome->data_array['documents'][] = $value;

  return $outcome;
}

/**
 * addMongoDBDocument
 *
 * Adds a document to a MongoCollection
 *
 * @param \MongoCollection $collection
 * @param array $document
 * @return \Outcome Object
 */
function addMongoDBDocument( \MongoCollection $collection , array $document, array $options=array() )
{
  dlog('', '(%s)', func_get_args());

  $outcome = new \Outcome();
  $outcome->succeed();

  // MongoCollection::insert
  $start = microtime(TRUE);
  $result = $collection->insert( $document, $options );
  \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));

  if ( empty( $result ) || ! is_array($result) )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB insert error' , 'DB0003' );
  elseif ( $result['ok'] != 1 )
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB insert error ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );

  return $outcome;
}

/**
 * batchInsertMongoDBDocuments
 *
 * Adds multiple documents to a MongoCollection
 * @param \MongoCollection $collection
 * @param array $documents 
 * @return \Outcome Object
 */
function batchInsertMongoDBDocuments( \MongoCollection $collection, array $documents )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  //check for valid params
  if ( count( $documents ) > 0 && ! is_null( $collection ) )
  {
    $start = microtime(TRUE);
    $result = $collection->batchInsert( $documents, array( 'w' => 1, 'continueOnError' => true ) );
    \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
    
    dlog( '', 'result =%s', $result);
    
    if ( empty( $result ) || ! is_array( $result ) )
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB batch insert error' , 'DB0003' ); 
    elseif ( $result['ok'] != 1 )
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB batch insert error ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );
    elseif ( $result['lastOp']->inc !== count( $documents ) )
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB batch insert error - not all entires were inserted ' . $result['err'] . ' - ' . $result['errmsg'] , 'DB0003' );
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * findOneMongoDBDocument
 * 
 * Finds a single element in a collection
 *
 * @param \MongoCollection $collection
 * @param array $query
 * @param array $fields (optional)
 * @return \Outcome Object
 * @example $outcome = \Ultra\Lib\DB\findOneMongoDBDocument( $collection , array('un' => 'a1425487725', 'fn' => 'Test Fname'), array('un','fn','ln'));
 */
function findOneMongoDBDocument( \MongoCollection $collection, array $query, array $fields = array())
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! is_null( $collection ) && count( $query ) > 0 )
  {
    $start = microtime(TRUE);
    $result = $collection->findOne($query,$fields);
    \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));

    if ( empty( $result ) )
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB no data found' , '------' );
    else
    {
      unset($result['_id']);
      $outcome->data_array['documents'][] = $result;
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * findAndModifyMongoDBDocument
 * 
 * Find a single docuemnt update the contents and return it
 *
 * @param \MongoCollection $collection
 * @param array $query
 * @param array $update
 * @param array $fields = array('fn' => true, 'ln' => true, 'rc' => true);
 * @param array $options ('new' => true) by default to return updated document
 * @see http://php.net/manual/en/mongocollection.findandmodify.php
 * @return \Outcome Object
 */
function findAndModifyMongoDBDocument( \MongoCollection $collection, array $query, array $update, array $fields = array(), array $options = array() )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! is_null( $collection ) && count( $query ) > 0 && count( $update ) > 0 )
  {
    if ( ! array_key_exists( 'new', $options ) )
      $options['new'] = TRUE;

    $start = microtime(TRUE);
    $result = $collection->findAndModify( $query, array( '$set' => $update ), $fields, $options );
    \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
    
    if ( empty( $result ) )
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB no data found' , '------' );
    else
    {
      unset( $result['_id'] );
      $outcome->data_array['documents'][] = $result;
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * groupByCountMongoDBDocuments
 * 
 * Performs an operation similar to SQL's COUNT(*) GROUP BY using mongos aggregate
 * 
 * @return \Outcome Object
 */
function groupByCountMongoDBDocuments( \MongoCollection $collection, array $fields )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( count( $fields ) )
  {
    $result = NULL;

    // prepare pipeline
    $pipeline = array();
    foreach( $fields as $field )
      $pipeline[ $field ] = '$'.$field ;

    try
    {
      // invoke MongoCollection::aggregate
      $start = microtime(TRUE);
      $result = $collection->aggregate(
        array(
          array(
            '$group' => array(
              '_id'    => $pipeline,
              'count'  => array('$sum' => 1)
            )
          )
        )
      );
      \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
    }
    catch ( \MongoResultException $e )
    {
      $outcome->add_errors_and_code( $e->getMessage() , $e->getCode() );
    }

    if ( ! empty($result) && ! empty($result['result']) )
    {
      foreach( $result['result'] as $row )
      {
        $row['_id']['count'] = $row['count'];
        $outcome->data_array['documents'][] = $row['_id'];
      }
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * groupMongoDBDocuments
 * 
 * Performs an operation similar to SQL's GROUP BY command using mongos reduce method
 *
 * @param \MongoCollection $collection
 * @param array $keys
 * @param array $initial
 * @param string $reduce js function to pass to mongodb to perform the grouping 
 * @param array $condition
 * @see http://php.net/manual/en/mongocollection.group.php
 * @example groupMongoDBDocuments( $collection, array("ch" => 1), array("items" => array()), "function (obj, prev) { prev.items.push(obj.cr); }");
 * @return \Outcome Object
 */
function groupMongoDBDocuments( \MongoCollection $collection, array $keys, array $initial, $reduce, array $condition = array() )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( count( $keys ) && count( $initial ) && ! empty( $reduce ) )
  {
    try
    {
      // make sure condition array is not implicit
      if ( !array_key_exists( 'condition', $condition ) )
        $condition['condition'] = $condition;

      $start = microtime(TRUE);
      $result = $collection->group( $keys, $initial, $reduce, $condition );
      \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
    }
    catch ( \MongoResultException $e )
    {
      $outcome->add_errors_and_code( $e->getMessage() , $e->getCode() );
    }
    
    if ( empty( $result ) || ! is_array( $result ) || empty( $result['retval'] ) )
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB no data found' , '------' );
    else
    {
      unset( $result['_id'] );
      $outcome->data_array['documents'] = $result;
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * createIndexMongoDBDocument
 * 
 * Creates index on specified fields
 *
 * @param \MongoCollection $collection
 * @param array $keys
 * @param array $options
 * @see http://php.net/manual/en/mongocollection.createindex.php
 * @example createIndexMongoDBDocument( $collection, array("un" => 1), array('unique' => true));
 * @return \Outcome Object
 */
function createIndexMongoDBDocument( \MongoCollection $collection, array $keys, array $options = array() )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! empty( $keys ) )
  {
    try
    {
      $start = microtime(TRUE);
      $result = $collection->createIndex( $keys, $options );
      \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
      if ( ! $result )
        $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoCollection::createIndex failed' , 'DB0003' );
    }
    catch( \MongoResultException $e )
    {
      $outcome->add_errors_and_code( $e->getMessage() , $e->getCode() );
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * ensureIndexMongoDBDocument
 * 
 * Ensures an index is created on specified fields
 * @param \MongoCollection $collection
 * @param array $keys
 * @param array $options
 * @see http://php.net/manual/en/mongocollection.ensureindex.php
 * @example ensureIndexMongoDBDocument( $collection, array("un" => 1), array('unique' => true));
 * @return \Outcome Object
 */
function ensureIndexMongoDBDocument( \MongoCollection $collection, array $keys, array $options = array() )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! empty( $keys ) )
  {
    try
    {
      $start = microtime(TRUE);
      $result = $collection->ensureIndex( $keys, $options );
      \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
      if ( ! $result )
        $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoCollection::ensureIndex failed' , 'DB0003' );
    }
    catch( \MongoResultException $e )
    {
      $outcome->add_errors_and_code( $e->getMessage() , $e->getCode() );
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * saveMongoDBDocument
 * 
 * Ensures an index is created on specified fields
 * @param \MongoCollection $collection
 * @param array $object
 * @see http://php.net/manual/en/mongocollection.save.php
 * @example saveMongoDBDocument( $collection, array("first_name" => 'chad', "last_name" => 'smith', "address" => "123 front st") );
 * @return \Outcome Object
 */
function saveMongoDBDocument( \MongoCollection $collection, array $obj )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ! empty( $obj ) )
  {
    try
    {
      $start = microtime(TRUE);
      $result = $collection->save( $obj );
      \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
      
      if ( ! $result )
        $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoCollection::save failed' , 'DB0003' );
    }
    catch( \MongoResultException $e )
    {
      $outcome->add_errors_and_code( $e->getMessage() , $e->getCode() );
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * getIndexInfoMongoDBDocument
 * 
 * Returns information about indexes on collection
 *
 * @param \MongoCollection $collection
 * @see http://php.net/manual/en/mongocollection.createindex.php
 * @example getIndexInfoMongoDBDocument($collection);
 * @return \Outcome Object
 */
function getIndexInfoMongoDBDocument( \MongoCollection $collection )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( $collection )
  {
    $start = microtime(TRUE);
    $result = $collection->getIndexInfo();
    \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));

    if ( $result && is_array( $result ) )
      $outcome->data_array = $result;
    else
      $outcome->add_errors_and_code( 'ERR_API_INTERNAL: invalid result' , 'IN0001' );
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );

  return $outcome;
}

/**
 * deleteIndexMongoDBDocument
 * 
 * Deletes an index from a document
 *
 * @param \MongoCollection $collection
 * @param string|array $keys
 * @see http://php.net/manual/en/mongocollection.deleteindex.php
 * @example deleteIndexMongoDBDocument( $collection, "un" );
 * @return \Outcome Object
 */
function deleteIndexMongoDBDocument( \MongoCollection $collection, $keys )
{
  $outcome = new \Outcome();
  $outcome->succeed();

  if ( ( is_array( $keys ) || is_string( $keys ) ) )
  {
    try
    {
      $start = microtime(TRUE);
      $result = $collection->deleteIndex( $keys );
      \logInfo(sprintf('query time: %.6f (sec)', microtime(TRUE) - $start));
     
      if ( !  $result['ok'] )
      {
        $outcome->add_errors_and_code( $result['errmsg'] , 'DB0003' );
        $outcome->fail();
      }
    }
    catch( \MongoResultException $e )
    {
      $outcome->add_errors_and_code( $e->getMessage() , $e->getCode() );
    }
  }
  else
    $outcome->add_errors_and_code( 'ERR_API_INTERNAL: MongoDB invalid parameters' , 'DB0003' );  

  return $outcome;
}

/**
 * selectMongoDB
 *
 * Instantiates an object of class MongoDB
 * Input:
 *  - 'host'
 *  - 'port'
 *  - 'db_name'
 *  - 'username'
 *  - 'password'
 *
 * @return object of class \MongoDB
 */
function selectMongoDB( array $params )
{
  $dbObject = NULL;

  \logDebug('Connecting to Mongo DB instance');

  try
  {
    $mongoClientObject = connectMongoDB( $params );

    if ( ! $mongoClientObject )
      throw new \Exception('Cannot establish a connection with MongoDB');

    $dbObject = $mongoClientObject->selectDB( $params['db_name'] );
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }

  return $dbObject;
}

/**
 * selectMongoDBEvents
 *
 * Select a MongoLab DB (mongolab.com) for the event tracker
 *
 * @return object of class \MongoDB
 */
function selectMongoDBEvents( array $params )
{
  $dbObject = NULL;

  \logDebug('Connecting to Events Mongo DB instance');

  try
  {
    $defaultMongoDBEventsCredentials = \Ultra\UltraConfig\getDefaultMongoDBEventsCredentials();

    if ( empty( $params['db'] ) )
      $params['db'] = $defaultMongoDBEventsCredentials['db'];

    if ( empty( $params['hosts'] ) )
      $params['hosts'] = $defaultMongoDBEventsCredentials['hosts'];

    if ( empty( $params['username'] ) )
      $params['username'] = $defaultMongoDBEventsCredentials['username'];

    if ( empty( $params['password'] ) )
      $params['password'] = $defaultMongoDBEventsCredentials['password'];

    if ( empty( $params['replicaset'] ) )
      $params['replicaset'] = $defaultMongoDBEventsCredentials['replicaset'];

    $mongoClientObject = connectMongoDBEvents( $params );

    if ( ! $mongoClientObject )
      throw new \Exception('Cannot establish a connection with MongoDB');

    $dbObject = $mongoClientObject->selectDB( $params['db'] );
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }

  return $dbObject;
}

/**
 * connectMongoDBEvents
 *
 * Instantiates an object of class MongoClient to connect to MongoLab (mongolab.com) for the event tracker
 * Input:
 *  - 'db'
 *  - 'hosts'
 *  - 'username'
 *  - 'password'
 *  - 'replicaset'
 *
 * @return object of class \MongoClient
 */
function connectMongoDBEvents( array $params )
{
  $mongoClientObject = NULL;

  if ( empty( $params ) || ! is_array( $params ) )
    $params = array();

  $defaultMongoDBEventsCredentials = \Ultra\UltraConfig\getDefaultMongoDBEventsCredentials();

  if ( empty( $params['db'] ) )
    $params['db'] = $defaultMongoDBEventsCredentials['db'];

  if ( empty( $params['hosts'] ) )
    $params['hosts'] = $defaultMongoDBEventsCredentials['hosts'];

  if ( empty( $params['username'] ) )
    $params['username'] = $defaultMongoDBEventsCredentials['username'];

  if ( empty( $params['password'] ) )
    $params['password'] = $defaultMongoDBEventsCredentials['password'];

  if ( empty( $params['replicaset'] ) )
    $params['replicaset'] = $defaultMongoDBEventsCredentials['replicaset'];

  try
  {
    $mongoClientObject = new \MongoClient(
      'mongodb://' . $params['username'] . ':' . $params['password'] . '@' . implode( ',' , $params['hosts'] ) ,
      array(
        'replicaSet' => $params['replicaset'],
        'db'         => $params['db']
      )
    );
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }

  return $mongoClientObject;
}

/**
 * connectMongoDB
 *
 * Instantiates an object of class MongoClient
 * Input:
 *  - 'host'
 *  - 'port'
 *  - 'db_name'
 *  - 'username'
 *  - 'password'
 *
 * @return object of class \MongoClient
 */
function connectMongoDB( array $params )
{
  $mongoClientObject = NULL;

  if ( empty( $params ) || ! is_array( $params ) )
    $params = array();

  $defaultMongoDBCredentials = \Ultra\UltraConfig\getDefaultMongoDBCredentials();

  if ( empty( $params['host'] ) )
  {
    if ( empty( $defaultMongoDBCredentials['host'] ) )
      $params['host'] = 'localhost';
    else
      $params['host'] = $defaultMongoDBCredentials['host'];
  }

  if ( empty( $params['port'] ) )
  {
    if ( empty( $defaultMongoDBCredentials['port'] ) )
      $params['port'] = '27017';
    else
      $params['port'] = $defaultMongoDBCredentials['port'];
  }

  if ( empty( $params['db_name'] ) )
  {
    if ( empty( $defaultMongoDBCredentials['db_name'] ) && empty( $defaultMongoDBCredentials['connection_string'] ) )
    {
      dlog('',"db_name or connection_string ARE REQUIRED !");
      return $mongoClientObject;
    }
    else
      $params['db_name'] = $defaultMongoDBCredentials['db_name'];
  }

  if ( empty( $params['username'] ) )
  {
    if ( empty( $defaultMongoDBCredentials['username'] ) )
    {
      dlog('',"username IS REQUIRED !");
      return $mongoClientObject;
    }
    else
      $params['username'] = $defaultMongoDBCredentials['username'];
  }

  if ( empty( $params['password'] ) )
  {
    if ( empty( $defaultMongoDBCredentials['password'] ) )
    {
      dlog('',"password IS REQUIRED !");
      return $mongoClientObject;
    }
    else
      $params['password'] = $defaultMongoDBCredentials['password'];
  }

  try
  {
#$defaultMongoDBCredentials['connection_string'] = 'mongodb://db-dev-01:27017,db-dev-02:27017/primodb?replicaSet=primoDevRS&readPreference=secondary';

    if ( empty( $defaultMongoDBCredentials['connection_string'] ) )
      $defaultMongoDBCredentials['connection_string'] = 'mongodb://' . $params['host'] . ':' . $params['port'] . '/' . $params['db_name'];

    \logDebug($defaultMongoDBCredentials['connection_string']);

    $start = microtime(TRUE);
    
    $mongoClientObject = new \MongoClient(
      $defaultMongoDBCredentials['connection_string'],
      array(
        'username' => $params['username'],
        'password' => $params['password']
      )
    );

    \logInfo(sprintf('MongoDB connection time: %.4f (sec)', microtime(TRUE) - $start));

  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }

  return $mongoClientObject;
}

