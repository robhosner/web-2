#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::DB::SoapLog;
use Ultra::Lib::Util::Config;
use XML::LibXML;
use XML::LibXML::PrettyPrint;
use XML::Simple;


my $config = Ultra::Lib::Util::Config->new();

my $db_table = 'SOAP_LOG';

# connect to DB
my $db = Ultra::Lib::DB::MSSQL->new( DB_NAME => 'ULTRA_ACC' , CONFIG => $config , DB_HOST => 'SQL-PRD-01.ultra.local' );

my $dbSoapLog = Ultra::Lib::DB::SoapLog->new( DB => $db );

my $msisdn = $ARGV[ 0 ];
my $iccid  = $ARGV[ 1 ];
my $top    = defined $ARGV[ 2 ] ? $ARGV[ 2 ] : 1 ;

my $where  = [];

if ( $msisdn && ( $msisdn !~ /^\d{10}$/ ) )
{
  die("msisdn $msisdn is not valid");
}
elsif( $msisdn )
{
  push(@$where," MSISDN = '".$msisdn."'");
}

if ( $iccid && ( $iccid !~ /^\d{18,19}$/ ) )
{
  die("iccid $iccid is not valid");
}
else
{
  push(@$where," ICCID = '".$iccid."'");
}

my $where_clause = '';

if ( ! scalar(@$where) )
{
  die("Please provide MSISDN or ICCID");
}
else
{
  $where_clause = "WHERE (".join(" OR ",@$where).")";
}

my $query = " SELECT
 TOP $top
 CONVERT(varchar(max), DATA_XML ) xml,
 SOAP_LOG_ID,
 SOAP_DATE,
 MSISDN,
 ICCID,
 TYPE_ID,
 RESULTCODE,
 ENV,
 COMMAND,
 SESSION_ID,
 TAG
 FROM $db_table
 $where_clause
 ORDER BY SOAP_LOG_ID ASC";

my $sth = $dbSoapLog->{ DB }->executeSelect( $query );

if ( ! $sth )
{
  die "DB error";
}
elsif ( $dbSoapLog->{ DB }->hasErrors() )
{
  die Dumper( $dbSoapLog->{ DB }->getErrors() );
}
else
{
  show_result( $sth );
}

sub show_result
{
  my $sth = shift;

  while( my $row = $sth->fetchrow_hashref() )
  {
    show_result_row( $row );
  }
}

sub show_result_row
{
  my $row = shift;

  my $xmlOutput = '';

  print STDOUT $row->{ SOAP_DATE } .' - ' . $row->{ COMMAND } . "\n\n";

  local $@;

  eval
  {
    my $xmlObject = XML::LibXML->load_xml( string => $row->{ xml } );

    my $prettyPrinter = XML::LibXML::PrettyPrint->new( indent_string => "  " );

    $prettyPrinter->pretty_print( $xmlObject );

    $xmlOutput = $xmlObject->toString;
  };

  if ( $@ )
  {
    print STDOUT $row->{ xml } . " (not well formed)\n";
  }
  else
  {
    print STDOUT $xmlOutput . "\n\n\n";
  }
}

#my $row = $dbSoapLog->getSoapLog( 'test' );

__END__

Example:
%> sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli_acc_dev HTT_CONFIGROOT=/home/ht/config/rgalli_acc_dev ./Ultra/Lib/DB/SoapLog_test_read.pl 2188655554 8901260842107730807'

#my $success = $dbSoapLog->insertSoapLog({
#  data_xml         => '<?xml version="1.0" encoding="UTF-8"?><test>123</test>',
#  method           => 'test',
#  session_id       => 'a',
#  type_id          => 1,
#  command          => 'test_command',
#});

web-dev-01:
sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev ./Ultra/Lib/DB/SoapLog_test_read.pl $MSISDN $ICCID'

web-01:
sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli4_dev HTT_CONFIGROOT=/home/ht/config/rgalli4_dev ./Ultra/Lib/DB/SoapLog_test_read.pl $MSISDN $ICCID'

mw-dev-01:
sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli_acc_dev HTT_CONFIGROOT=/home/ht/config/rgalli_acc_dev ./Ultra/Lib/DB/SoapLog_test_read.pl $MSISDN $ICCID'

