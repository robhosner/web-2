<?php

require_once 'db.php';
require_once 'Ultra/Lib/SureTax.php';

$amount  = 10;
$zipcode = '11222';
$account = time();

echo "computing taxes for $amount \$ , on location $zipcode\n";

$sales_tax_result = \Ultra\Lib\SureTax\perform_call(
  array(
    'amount'            => $amount,
    'zip_code'          => $zipcode,
    'account'           => $account
  )
);

print_r($sales_tax_result);

if ( $sales_tax_result->is_failure() )
{
  echo "call failed\n";
  exit;
}

$sales_tax            = 0;
$sales_tax_percentage = 0;

foreach ( $sales_tax_result->data_array['GroupList'][0]->TaxList as $tax_entry )
{
  $suffix = substr( $tax_entry->TaxTypeCode , -2 );

  if ( in_array( $suffix , array('01','02','03','04','05') ) )
    $sales_tax += ( $tax_entry->TaxAmount * 100 );

  $sales_tax_percentage += ( $tax_entry->TaxRate * $tax_entry->PercentTaxable );
}

echo "computed taxes for $amount \$ , on location $zipcode\n";

echo "sales_tax            = ".($sales_tax/100)." \$\n";
echo "sales_tax_percentage = $sales_tax_percentage\n";

?>
