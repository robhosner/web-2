<?php

namespace Ultra\Lib;

// dependencies
require_once 'Ultra/Lib/DB/Cache.php';
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/CustomerNotification/Context.php';

// workflows (in alphabetical order)
require_once 'Ultra/Lib/CustomerNotification/Workflow/Balance.php';   // check balance
require_once 'Ultra/Lib/CustomerNotification/Workflow/BoltOns.php';   // immediate bolt ons purchases
require_once 'Ultra/Lib/CustomerNotification/Workflow/Campaign.php';  // promotional SMS campaigns
require_once 'Ultra/Lib/CustomerNotification/Workflow/Check.php';     // workflow to test connectivity
require_once 'Ultra/Lib/CustomerNotification/Workflow/Data.php';      // check data usage
require_once 'Ultra/Lib/CustomerNotification/Workflow/Gogo.php';      // orange activation
require_once 'Ultra/Lib/CustomerNotification/Workflow/Language.php';  // check and set preferred language
require_once 'Ultra/Lib/CustomerNotification/Workflow/Menu.php';      // get SMS commands descriptions
require_once 'Ultra/Lib/CustomerNotification/Workflow/Recharge.php';  // recharge account with PIN
require_once 'Ultra/Lib/CustomerNotification/Workflow/Roam.php';      // check roam balance
require_once 'Ultra/Lib/CustomerNotification/Workflow/Store.php';     // store locator
require_once 'Ultra/Lib/CustomerNotification/Workflow/Setup.php';     // setup apn settings


/**
 * inbound SMS notifiations class for 6700 self care
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/3ci+Replacement
 * @see UNI-86
 * @author VYT 2015
 */

class CustomerNotification
{
  // constants
  const CAMPAIGN_KEY = 'ultra/lib/customernotification/campaign/'; // cached active promo campaign ID
  const MAX_CONTEXT_FAILURES = 3; // teminate worklow after this many user input failures

  // variables
  private $redis = NULL;
  private $cache = NULL;
  private $outcome = NULL;


  public function __construct()
  {
    // instantiate required classes
    $this->redis = new \Ultra\Lib\Util\Redis;
    $this->cache = new \Ultra\Lib\DB\Cache($this->redis);
    $this->outcome = new \Outcome(NULL, TRUE);
  }


  /**
   * execute
   * process inbound SMS message
   * the only public method of this class
   * @param Array parameters of NotificationReceived ACC message (shortCode, MSISDN, smsText, serviceGrade, messageType, UserData)
   * @return Object Outcome: success if message was handled (successfully or not is another matter), failure if we encountered a system error
   */
  public function execute($parameters)
  {
    // initialize
    logInfo('parameters: ' . json_encode($parameters));

    if ($this->isNoReplyMessage($parameters['smsText']))
      return $this->outcome;

    // validate and extract subscriber MSISDN from parameters
    if (empty($parameters['MSISDN']) || ! in_array(strlen($parameters['MSISDN']), array(10, 14)))
    {
      logError("invalid MSISDN length for {$parameters['MSISDN']}");
      return $this->handleSystemError('MP0003', 'EN', "invalid MSISDN {$parameters['MSISDN']}");
    }
    $msisdn = $parameters['MSISDN'];

    // load existing or create a new session context
    $context = new \Ultra\Lib\Context($msisdn);
    if ( ! $context->load($this->redis) && ! $context->create($this->cache))
      return $this->handleSystemError('IN0002', 'EN', "invalid MSISDN {$parameters['MSISDN']}");

    // parse inbound SMS message
    if (empty($parameters['smsText']) || ! $message = parseSmsIntoParts($parameters['smsText']))
      return $this->sendSms($context, 'IN0006');

    // remap TMO codes to Ultra for a newly created context
    $context->setMessage($context->action ? $message : $this->remapTmoCommands($message));
    $context->log();

    // check if session should be terminated
    if ($this->sessionTerminated($message, $context))
      return $this->sendSms($context, 'IN0007');

    // identify and execute responsible workflow (this also handles SMS campaigns)
    if ($workflowClass = $this->identifyWorkflow($context))
    {
      // execute workflow
      logInfo("instantiating workflow class $workflowClass");
      $workflow = new $workflowClass;
      $context->setWorkflow($workflowClass);
      if ($error = $workflow->run($context))
      {
        logError("workflow $workflowClass returned error code $error");
        $context->delete($this->redis, $this->cache); // always terminate workflow failure
        return $this->sendSms($context, $error);
      }

      // save or remove context based on workflow completion
      if ($context->action === NULL)
        $context->delete($this->redis, $this->cache);
      else
        $context->save($this->redis);

      return $this->outcome;
    }

    // keyword is unknown
    return $this->sendSms($context, $this->getInvalidCommandErrorCode($context->brand));
  }

  /**
   * isNoReplyMessage
   * checks if CustomerNotification should handle a reply for message
   * @param String message
   * @return Boolean
   */
  private function isNoReplyMessage($msg)
  {
    if (in_array($msg, [
      "This is a Kosher Phone; can't get messages."
    ])) {
      logInfo('Skipping reply for message');
      return TRUE;
    }

    return FALSE;
  }


  /**
   * sendSms
   * send outbound SMS message to subscriber and return default successfull outcome (unless SMS fails)
   * @param Object session context
   * @param String message error code
   * @return Object Outcome
   */
  private function sendSms($context, $code)
  {
    // we cannot send SMS to fake MSISDN
    if (strlen($context->msisdn) != 10)
    {
      logError("unable to send SMS to invalid MSISDN {$context->msisdn}");
      return $this->outcome;
    }

    // load SMS response message
    $message = $this->loadErrorMessage($code, $context->brand, $context->language);
    logInfo("msisdn: {$context->msisdn}, type: $code, brand: {$context->brand}, language: {$context->language}, message: $message");

    // and send it off to network
    $result = mvneBypassListAsynchronousSendSMS($message, $context->msisdn, getNewActionUUID(__CLASS__ . time()));
    if ($result->is_failure() || $result->has_errors())
    {
      $details = "failed to send SMS: " . json_encode($result->get_errors());
      logError($details);
      return $this->handleSystemError('MW0001', $context->language, $details);
    }

    return $this->outcome;
  }


  /**
   * identifyWorkflow
   * attempt to find a workflow by keyword or existing context
   * @param Object Context instance
   * @return String workflow class name or NULL on failure
   */
  private function identifyWorkflow($context)
  {
    // existing workflow, nothing to do
    if ($context->workflow)
      return $context->workflow;

    // look for workflow classes in our namespace only by keyword
    $keyword = $context->getKeyword(0, TRUE);

    if ($keyword && ! $this->isWorkflowRestricted($keyword, $context->brand))
    {
      $workflow = __CLASS__ . '\\' . $keyword;
      if (class_exists($workflow))
        return $workflow;
    }

    // check if a campaign is called
    if ($this->isCampaignKeyword($keyword))
      return __CLASS__ . '\\Campaign';

    logInfo("no workflows or campaigns for keyword $keyword");
    return NULL;
  }


  /**
   * isCampaignKeyword
   * check if given keyword corresponds to an enabled SMS campaign (further API call with validate that it has not expired)
   * @param String keyword
   * @return Boolean TRUE or FALSE
   */
  private function isCampaignKeyword($keyword)
  {
    // check cache for active campaigns
    $keyword = strtoupper($keyword);
    if ($this->redis->get(self::CAMPAIGN_KEY . $keyword))
      return TRUE;

    // attempt to load from DB: most recent by keyword
    teldata_change_db();
    $result = \get_ultra_promo_broadcast_campaign(
      array
      (
        'top'             => 1,
        'promo_shortcode' => $keyword,
        'promo_enabled'   => 1,
        'order_by'        => 'PROMO_BROADCAST_CAMPAIGN_ID DESC'
      ));

    // validate DB result
    $data = $result->get_data_array();
    if ($result->is_failure() || empty($data))
      return FALSE;
    $campaign = $data[0];
    logInfo("found active campaign ID {$campaign->PROMO_BROADCAST_CAMPAIGN_ID}");

    // cache for one hour
    $this->redis->set(self::CAMPAIGN_KEY . $keyword, $campaign->PROMO_BROADCAST_CAMPAIGN_ID, SECONDS_IN_HOUR);
    return TRUE;
  }


  /**
   * handleSystemError
   * called only we encounter a hard system error and cannot handle inbound SMS
   * @param String error code
   * @return Object Outcome to be returned to the class caller
   */
  private function handleSystemError($code, $language, $details = NULL)
  {
    $message = $this->loadErrorMessage($code, 0, $language);
    $this->outcome->add_errors_and_code($message, $code, $details);
    return $this->outcome;
  }


  /**
   * loadErrorMessage
   * return SMS message to be send to subscriber based on brand, language and message error code
   * @param String error code ULTRA.USER_ERROR_MESSAGES.ERROR_CODE
   * @param Integer brand ID (as defined in cfengine), unused, reserved for future
   * @param String language code (EN, ES, ZH)
   * @return String message suitable for sending to subscriber
   */
  private function loadErrorMessage($code, $brand, $language)
  {
    // load error message by code
    if ( ! $row = $this->cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', $code, SECONDS_IN_DAY, FALSE))
    {
      // hard error: replace with a generic error
      logError("missing data for error code $code");
      $message = 'System error. Please try later.'; // hard coded IN0005
    }
    else
    {
      $message = empty($row->{$language . '_MESSAGE'}) ? $row->EN_MESSAGE : $row->{$language . '_MESSAGE'};
      if (is_language_encoded($language))
        $message = convertUnicodeFromJavaEscape($message);
    }

    return $message;
  }


  /**
   * remapTmoCommands
   * a fairly large number of subscribers coming from T-Mobile mistakenly use T-Mobile SMS codes with Ultra
   * this function maps T-Mobile SMS commands to Ultra in order to maintain good customer experience
   * @see https://support.t-mobile.com/docs/DOC-4041
   * @param Array parsed SMS message
   * @param String updated SMS message
   */
  private function remapTmoCommands($message)
  {
    // note that hash signs are removed by SMS parser
    $tmo = array(
      '225'   => 'BALANCE',
      'MIN'   => 'BALANCE',
      '646'   => 'BALANCE',
      'MSG'   => 'BALANCE',
      '674'   => 'BALANCE',
      '999'   => 'BALANCE',
      'WEB'   => 'DATA',
      '932'   => 'DATA');

    $lookup = strtoupper($message[0]);
    if ( ! empty($tmo[$lookup]))
      $message[0] = $tmo[$lookup];
    return $message;
  }


  /**
   * sessionTerminated
   * check and terminate session if requested by subscriber or workflow encouteres too many errors (to prevent getting stuck in a workflow)
   * @param Array message
   * @param Object session context
   * @return Boolean TRUE if session was terminated, FALSE otherwise
   */
  private function sessionTerminated($message, $context)
  {
    $stop = array(
      'STOP', 'CANCEL', 'END', 'EXIT',   // English
      'DETENER', 'CANCELAR', 'TERMINA'); // Spanish

    if (in_array(strtoupper($message[0]), $stop) || $context->failure > self::MAX_CONTEXT_FAILURES)
    {
      $context->delete($this->redis, $this->cache);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * isWorkflowRestricted
   * @param  String keyword
   * @param  Integer brand id
   * @return Boolean
   */
  public function isWorkflowRestricted($keyword, $brandId)
  {
    $restrictions = [
      'ULTRA'     => [],
      'UNIVISION' => [],
      'MINT'      => [ 'LANGUAGE','RECHARGE','STORE','UPINTL' ]
    ];

    $brand = \Ultra\UltraConfig\getShortNameFromBrandId($brandId);
    return in_array($keyword, $restrictions[$brand]);
  }

  /**
   * getInvalidCommandErrorCode
   * @param  String keyword
   * @param  Integer brand id
   * @return String error code
   */
  public function getInvalidCommandErrorCode($brandId)
  {
    $errorCodes = [
      'ULTRA'     => 'IN0010',
      'UNIVISION' => 'IN0010',
      'MINT'      => 'IN0014'
    ];

    $brand = \Ultra\UltraConfig\getShortNameFromBrandId($brandId);
    return (isset($errorCodes[$brand])) ? $errorCodes[$brand] : 'IN0010';
  }
}
