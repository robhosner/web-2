<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/CheckProcess.php';

/**
 * workflow for test SMS connectivity
 * confirm receipt
 * request -> response only, implements a single default method
 */
class Check extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Process');
  protected $workflow = __CLASS__; // due to late static binding
}


// this workflow can also be called by the following additional keywords
class_alias(__NAMESPACE__ . '\\Check', __NAMESPACE__ . '\\test');

