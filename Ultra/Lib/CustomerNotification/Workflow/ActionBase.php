<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionInterface.php';

/**
 * ActionBase
 * skeleton class for all workflow actions
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/3ci+Replacement
 * @author VYT, 2015
 */
class ActionBase implements ActionInterface
{
  protected $action = NULL; // final workflow action class name after inheritance

  /**
   * perform
   * execute a workflow action; this function must be redefined by each workflow action
   * @param Object Context instance
   * @return String CustomerNotifiaction error code or NULL on success
   */
  public function perform($context)
  {
    return NULL;
  }


  /**
   * callUltraApi
   * call an Ultra API, parse and return result
   * @param Array API configuration
   * @param Array API parameters
   * @return Array API result
   */
  protected function callUltraApi($config, $params)
  {
    // gather API environment configuration
    if ( ! $site = $this->getSiteUrl())
      return logError('unable to get API site config');
    if (empty($config['partner']))
      list($partner) = explode('__', $config['command']);
    $options = array(
      'location'  => "{$site['url']}/pr/$partner/{$config['version']}/ultra/api/{$config['command']}.json",
      'username'  => empty($site['username']) ? NULL : $site['username'],
      'password'  => empty($site['password']) ? NULL : $site['password']);

    // call API
    return callJsonApi($config['command'], $params, $options);
  }


  /**
   * getSiteUrl
   * prepare a usable URL for internal API calls
   * @return Array (String url, String username, String password)
   */
  protected function getSiteUrl()
  {
    list($domain, $username, $password) = \Ultra\UltraConfig\ultra_api_internal_info();
    $result['url'] = "https://$domain";
    $result['username'] = $username;
    $result['password'] = $password;
    return $result;
  }


  /**
   * sendSms
   * send outbound SMS message to subscriber
   * @param Object session context
   * @param String message text
   * @return String error code or NULL on success
   */
  protected function sendSms($context, $message)
  {
    $result = mvneBypassListAsynchronousSendSMS($message, $context->msisdn, getNewActionUUID($this->action . time()));
    if ($result->is_failure() || $result->has_errors())
    {
      logError("failed to send SMS: " . json_encode($result->get_errors()));
      return 'SM0002';
    }

    return NULL;
  }


}

