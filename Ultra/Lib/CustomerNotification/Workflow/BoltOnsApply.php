<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * BoltOns Apply workflow action
 */
class BoltOnsApply extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * apply selected bolt on to subscriber's account
   * at this point we expect the final confirmation and have the following session data variables:
   *   boltons: Array of boltons we are selecting from
   *   selection: Integer index of the selected bolt on
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // validate response
    $response = $context->getKeyword(0, TRUE);
    if ( ! in_array($response, array('YES', 'SI', 'Y')))
    {
      // cancel purchase and terminate workflow
      $context->terminate();
      return 'IN0007';
    }

    // prepare API config
     $config = array(
      'command'   => 'customercare__ApplyBoltOn',
      'version'   => 2
    );

    // prepare API parameters
    $boltOn = (object)$context->boltons[$context->selection];
    $params = array(
      'customer_id' => $context->customer_id,
      'bolt_on_id'  => $boltOn->id
    );

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    if ( ! empty($result->error_codes))
      return $result->error_codes[0];

    // terminate workflow
    $context->terminate();

    // reply with confirmation since purchase processing might take a few minutes
    if ( ! $message = \Ultra\Messaging\Templates\SMS_by_language(array('message_type' => 'boltons_finish'), $context->language))
      return 'IN0004';

    return $this->sendSms($context, $message);
  }
}
