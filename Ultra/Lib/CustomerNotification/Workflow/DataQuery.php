<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Data Query workflow action
 */
class DataQuery extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * call Ultra API v1 interactivecare__ShowCustomerInfo and SMS its response to the subscriber
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    // prepare API config
    $config = array(
      'command'   => 'interactivecare__ShowCustomerInfo',
      'version'   => 1);

    // prepare API parameters
    $params = array(
      'msisdn'          => $context->msisdn,
      'language'        => $context->language,
      'get_data_usage'  => TRUE);

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // send response to subscriber (ignore success = FALSE since subscriber may be Neutral)
    if ( ! empty($result->message))
      return $this->sendSms($context, $result->message);
    if ( ! empty($result->error_codes))
      return $result->error_codes[0]; // caller will handle this error

    // contingency
    logError('Unusable API response');
    return 'IN0005';
  }
}

