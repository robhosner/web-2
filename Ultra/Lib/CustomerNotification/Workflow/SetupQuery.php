<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Data Query workflow action
 */
class SetupQuery extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding

  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    // prepare API config
    $config = array(
      'command'   => 'internal__SendApnSettings',
      'version'   => 2);

    // prepare API parameters
    $params = array( 'msisdn' => $context->msisdn );

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // send response to subscriber (ignore success = FALSE since subscriber may be Neutral)
    if ( ! empty($result->message))
      return $this->sendSms($context, $result->message);
    if ( ! empty($result->error_codes))
      return $result->error_codes[0]; // caller will handle this error

    if ($result->success) {
      return null;
    }

    // contingency
    logError('Unusable API response');
    return 'IN0005';
  }
}

