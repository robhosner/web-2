<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/BoltOnsDetermine.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/BoltOnsQuery.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/BoltOnsValidate.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/BoltOnsApply.php';

/**
 * workflow for SMS commands BUYINTL, UPINTL, BUYDATA, UPDATA
 * show list of available bolt ons, allow subscriber to select one and apply it to the account
 */
class BoltOns extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Determine', 'Query', 'Validate', 'Apply');
  protected $workflow = __CLASS__; // due to late static binding
}

// this workflow can also be called by the following additional keywords
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\buy');    // BUYDATA is often sent as "BUY DATA"
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\buydata');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\buymins');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\buyintl');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\up');     // UPDATA is often sent as "UP DATA"
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\updata');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\update'); // UPDATA is frequently corrected by spellcheckers
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\upintl');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\500MB');  // surprisingly common SMS
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\uproam');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\buyroam');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\upglobe');
class_alias(__NAMESPACE__ . '\\BoltOns', __NAMESPACE__ . '\\buyglobe');
