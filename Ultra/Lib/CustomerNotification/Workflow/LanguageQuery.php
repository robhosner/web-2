<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Language Query workflow action
 */
class LanguageQuery extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding

  /**
   * perform
   *
   * interpret workflow keyword, send subscriber SMS with the current preferred language and explainion how to change it
   * Keywords: CHINESE, ENG, ENGLISH, ESP, ESPANOL, SPANISH
   *
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // this workflow allows jumps: check if requested
    $keyword = $context->getKeyword(0, TRUE);
    if (in_array($keyword, array('CHINESE')))
      return $this->jump($context, 'ZH');
    if (in_array($keyword, array('ENG', 'ENGLISH')))
      return $this->jump($context, 'EN');
    if (in_array($keyword, array('ESP', 'ESPANOL', 'SPANISH')))
      return $this->jump($context, 'ES');

    // prepare next action
    $context->setAction('Decide');
    $context->pause();

    // send current language and instructions to change it
    if ( ! $message = \Ultra\Messaging\Templates\SMS_by_language(array('message_type' => 'language_query'), $context->language))
      return 'IN0004';
    return $this->sendSms($context, $message);
  }


  /**
   * jump
   * validate change and prepare paramaters for jump to immediate language change action
   * @param Object Context instance
   * @param String language
   */
  private function jump($context, $language)
  {
    // check if same language requested
    if ($context->language == $language)
    {
      // terminate workflow
      $context->terminate();

      // inform that language is aready set
      if ( ! $message = \Ultra\Messaging\Templates\SMS_by_language(array('message_type' => 'language_notify'), $context->language))
        return 'IN0004';
      return $this->sendSms($context, $message);
    }

    // prepare for jump to Change action
    $context->resume();
    $context->setLanguage($language);
    $context->setAction('Change');
    return NULL;
  }
}
