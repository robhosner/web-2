<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/RechargeQuery.php';

/**
 * workflow for SMS command RECHARGE
 * recharge account by PIN
 * request -> response only, implements a single default method
 */
class Recharge extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Query');
  protected $workflow = __CLASS__; // due to late static binding
}

// this workflow can also be called by the following additional keywords
class_alias(__NAMESPACE__ . '\\Recharge', __NAMESPACE__ . '\\recarga');
class_alias(__NAMESPACE__ . '\\Recharge', __NAMESPACE__ . '\\recargar');
