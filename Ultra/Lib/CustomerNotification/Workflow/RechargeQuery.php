<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';
require_once 'Ultra/Lib/Util/Validator.php';

/**
 * Recharge Query workflow action
 */
class RechargeQuery extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * apply PIN to the account
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    // validate PIN format since API validation responds with non-customer facing message on invalid PINs
    if ($pin = $context->getKeyword(1))
    {
      list($message, $code) = \Ultra\Lib\Util\validatorPin('pin', $pin, array(ULTRA_PIN_LENGTH, INCOMM_PIN_LENGTH));
      if ($code)
        return $code;
    }

    // prepare API config
     $config = array(
      'command'   => 'interactivecare__ApplyPIN',
      'version'   => 1);

    // prepare API parameters
    // we do not validate the PIN since the API will handle it and respond accordingly
    $params = array(
      'msisdn'          => $context->msisdn,
      'language'        => $context->language,
      'pin_number'      => $pin
    );

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // handle response: both error and success are returned in message
    if ( ! empty($result->message))
      return $this->sendSms($context, $result->message);
    if ( ! empty($result->error_codes))
      return $result->error_codes[0]; // caller will handle this error

    // contingency
    logError('Unusable API response');
    return 'IN0005';
  }
}
