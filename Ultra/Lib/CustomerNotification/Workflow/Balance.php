<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/BalanceQuery.php';

/**
 * workflow for SMS command BALANCE
 * query and return subscriber account balance details
 * request -> response only, implements a single default method
 */
class Balance extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Query');
  protected $workflow = __CLASS__; // due to late static binding
}


// this workflow can also be called by the following additional keywords
class_alias(__NAMESPACE__ . '\\Balance', __NAMESPACE__ . '\\bal');

