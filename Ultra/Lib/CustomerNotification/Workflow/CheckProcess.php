<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Check Process workflow action
 */
class CheckProcess extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * reply with acknowledgment
   * @param Object Context instance
   * @return NULL always
   */
  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    // send acknowledgement
    $this->sendSms($context, 'Acknowledged.');

    return NULL;
  }
}

