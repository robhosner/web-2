<?php

namespace Ultra\Lib\CustomerNotification;

/**
 * ActionInterface
 * class interface used by all workflow actions
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/3ci+Replacement
 * @author VYT, 2015
 */

interface ActionInterface
{
  /**
   * perform
   * execute a workflow action
   * if an error code is returned than the caller must take futher action to notify the subscriber
   * @param Object Context instance
   * @return String ULTRA.USER_ERROR_MESSAGES.ERROR_CODE on error or NULL on success
   */
  public function perform($context);
}
