<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Store Query workflow action
 */
class StoreQuery extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * call Ultra API v2 ultrainfo__FindDealersByZip and SMS its response to the subscriber
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    // prepare API config
    $config = array(
      'command'   => 'ultrainfo__FindDealersByZip',
      'version'   => 2);

    // prepare API parameters
    $params = array(
      'msisdn'    => $context->msisdn,
      'language'  => $context->language,
      'shortcode' => $context->getKeyword(0),
      'zipcode'   => $context->getKeyword(1)
    );

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // handle response
    if ( ! empty($result->error_codes))
      return $result->error_codes[0]; // caller will handle this error
    if ( ! empty($result->message))
      return $this->sendSms($context, $result->message);

    // contingency
    logError('Unusable API response');
    return 'IN0005';
  }
}
