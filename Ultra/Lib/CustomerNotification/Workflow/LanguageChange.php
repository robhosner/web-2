<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Language Change workflow action
 */
class LanguageChange extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * set subscriber's preferred language
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // terminate workflow
    $context->terminate();

    // prepare API config
    $config = array(
      'command'   => 'interactivecare__SetLanguagebyMSISDN',
      'version'   => 1);

    // prepare API parameters
    $params = array(
      'msisdn'              => $context->msisdn,
      'preferred_language'  => $context->language,
      'notify'              => TRUE);

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // handle response
    if ( ! empty($result->error_codes))
      return $result->error_codes[0]; // caller will handle this error
    return NULL;
  }
}
