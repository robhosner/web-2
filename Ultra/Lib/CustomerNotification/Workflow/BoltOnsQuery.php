<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';
require_once 'classes/Flex.php';

/**
 * BoltOns Query workflow action
 */
class BoltOnsQuery extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding
  private $unlimited4gPlans = ['100003', '100007'];


  /**
   * perform
   * query and send available bolt ons to the subscriber based on type of purchase requested
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();
    if (\Ultra\Lib\Flex::isFlexPlan($context->cos_id))
      return $this->performFlex($context);

    // query available bolt ons if not already saved
    if (empty($context->boltons))
    {
      $boltOns = $this->getBoltOns($context);
      if ( ! $boltOns)
        return 'IN0005';

      $dataBoltOns = \Ultra\Lib\Util\validateMintBrandId($context->brand)
        ? $boltOns->bolt_ons->MDATA
        : $boltOns->bolt_ons->DATA;

      // save bolt ons and balance for future decision

      $customBoltOns = [];

      switch ($context->purchase)
      {
        case 'DATA':
          $customBoltOns = $dataBoltOns;
          break;
        case 'INTL':
          $customBoltOns = $boltOns->bolt_ons->IDDCA;
          break;
        case 'ROAM':
          $customBoltOns = $boltOns->bolt_ons->ROAM;
          break;
        case 'GLOBE':
          $customBoltOns = $boltOns->bolt_ons->GLOBE;
          break;
      }

      $context->setCustom('boltons', $customBoltOns);
      $context->setCustom('balance', $boltOns->balance);
    }

    // set next action and breakpoint
    $context->setAction('Validate');
    $context->pause();

    // compose and send bolt ons selection message
    if ( ! $message = $this->prepareMessage($context))
      return 'IN0005';

    return $this->sendSms($context, $message);
  }

  private function performFlex($context)
  {
    // query available bolt ons if not already saved
    if (empty($context->boltons))
    {
      $boltOns = $this->getBoltOns($context);
      if ( ! $boltOns)
        return 'IN0005';

      // save bolt ons and balance for future decision

      $customBoltOns = [];

      switch ($context->purchase)
      {
        case 'DATA':
          $customBoltOns = $boltOns->bolt_ons->SHAREDDATA;
          break;
        case 'INTL':
          $customBoltOns = $boltOns->bolt_ons->SHAREDILD;
          break;
        case 'ROAM':
          $customBoltOns = $boltOns->bolt_ons->ROAM;
          break;
        case 'GLOBE':
          $customBoltOns = $boltOns->bolt_ons->GLOBE;
          break;
      }

      $context->setCustom('boltons', $customBoltOns);
      $context->setCustom('balance', $boltOns->balance);
    }

    // set next action and breakpoint
    $context->setAction('Validate');
    $context->pause();

    // compose and send bolt ons selection message
    if ( ! $message = $this->prepareMessageFlex($context))
      return 'IN0005';

    return $this->sendSms($context, $message);
  }

  private function prepareMessageFlex($context)
  {
    // init work data for type of purchase
    $data = $context->purchase == 'DATA';
    $boltOns = $context->boltons;

    $start = 0;
    $end   = count($boltOns);

    // only first 3 available bolt ons are presented: add as costX, valueX
    $params = array('balance' => '$' . $context->balance);
    for ($i = $start; $i < $end; $i++)
    {
      if (isset($boltOns[$i]))
      {
        $boltOn = (object)$boltOns[$i];
        $params["cost$i"] = '$' . $boltOn->cost;

        switch ($context->purchase)
        {
          case 'DATA':
            $params["value$i"] = $boltOn->get_value >= 1024 ? round($boltOn->get_value / 1024) . 'GB'  : $boltOn->get_value . 'MB';
            break;
          case 'INTL':
            $params["value$i"] = '$' . $boltOn->get_value;
            break;
          case 'ROAM':
            $params["value$i"] = '$' . $boltOn->get_value / 100;
            break;
          case 'GLOBE':
            $params["value$i"] = 'Globe Unlimited';
            break;
        }
      }
    }

    $params['message_type'] = 'flex_boltons_select_' . strtolower($context->purchase);

    return $message = \Ultra\Messaging\Templates\SMS_by_language($params, $context->language, $context->brand);
  }

  private function getBoltOns($context)
  {
    // prepare API config
    $config = array(
      'command'   => 'customercare__GetBoltOns',
      'version'   => 2);

    // prepare API parameters
    $params = array('customer_id' => $context->customer_id);
    $result = $this->callUltraApi($config, $params);

    // call API and check result
    if (!$result || !$result->success)
    {
      logError("call to Ultra API {$config['command']} failed");
      return null;
    }

    // validate API response
    if (!in_array($context->cos_id, $this->unlimited4gPlans)) {
      if (((empty($result->bolt_ons->IDDCA) || empty($result->bolt_ons->DATA)) && empty($result->bolt_ons->MDATA) && empty($result->bolt_ons->SHAREDDATA))) {
        return null;
      }
    }

    if ($context->brand == 1 && empty($result->bolt_ons->GLOBE)) {
      return null;
    }

    return $result;
  }

  /**
   * prepareMessage
   * prepare SMS with balance and available bolt ons
   * @param Object session context
   * @return String message or NULL on failure
   */
  private function prepareMessage($context)
  {
    // init work data for type of purchase
    $data = $context->purchase == 'DATA';
    $boltOns = $context->boltons;

    if (\Ultra\Lib\Util\validateMintBrandId($context->brand))
    {
      if ($context->balance < 10)
      {
        $params['message_type'] = 'boltons_select_data_low_balance';
        return $message = \Ultra\Messaging\Templates\SMS_by_language($params, $context->language, $context->brand);
      }
    }

    // only first 3 available bolt ons are presented: add as costX, valueX
    $params = array('balance' => '$' . $context->balance);
    for ($i = 0; $i < 3; $i++)
    {
      if (isset($boltOns[$i]))
      {
        $boltOn = (object)$boltOns[$i];
        $params["cost$i"] = '$' . $boltOn->cost;

        switch ($context->purchase)
        {
          case 'DATA':
            $params["value$i"] = $boltOn->get_value >= 1024 ? round($boltOn->get_value / 1024) . 'GB'  : $boltOn->get_value . 'MB';
            break;
          case 'INTL':
            $params["value$i"] = '$' . $boltOn->get_value;
            break;
          case 'ROAM':
            $params["value$i"] = '$' . $boltOn->get_value / 100;
            break;
          case 'GLOBE':
            $params["value$i"] = 'Globe Unlimited';
            break;
        }
      }
    }

    $params['message_type'] = 'boltons_select_' . strtolower($context->purchase);

    if ($params['message_type'] == 'boltons_select_data' && in_array($context->cos_id, $this->unlimited4gPlans)) {
      return 'UpDATA is not supported on your current plan';
    }

    return $message = \Ultra\Messaging\Templates\SMS_by_language($params, $context->language, $context->brand);
  }

}



