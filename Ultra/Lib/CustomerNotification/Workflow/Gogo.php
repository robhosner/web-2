<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/GogoActivate.php';

/**
 * workflow for SMS command DATA
 * query network and return subscriber data usage details
 * request -> response only, implements a single default method
 */
class Gogo extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Activate');
  protected $workflow = __CLASS__; // due to late static binding
}

