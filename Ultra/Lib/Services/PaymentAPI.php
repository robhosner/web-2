<?php

namespace Ultra\Lib\Services;

require_once 'Ultra/Lib/Services/RestAPI.php';
require_once 'Ultra/Lib/Util/JWT.php';
require_once 'Ultra/Configuration/Configuration.php';
require_once 'Ultra/Messaging/Messenger.php';

use \Ultra\Lib\Util\JWT;
use \Ultra\Configuration\Configuration;
use Ultra\Messaging\Messenger;

class PaymentAPI extends RestAPI
{
	private $configObj;
	private $config;
	private $customerInformation = [];

	public function __construct(Configuration $configObj=null)
	{
		$this->configObj = !empty($configObj) ? $configObj : new Configuration();

		$this->config = $this->configObj->getPaymentAPIConfig();

    $curlConfig = [ CURLOPT_TIMEOUT => 60 ];

		parent::__construct($this->config['host'], $this->config['basepath'], $curlConfig);
	}

	public function charge($processorID, array $data)
	{
		$result = new \Result();

		// check required inputs
		$required = [ 'orderID', 'customerID', 'brandID', 'token', 'expDate', 'amount' ];
		foreach ($required as $field) {
			if (empty($data[$field])) {
				$result->add_error('Missing field ' . $field);
			}
			$this->customerInformation[$field] = $data[$field];
		}

		if ($result->has_errors()) {
			return $result;
		}

    // trim billingZipCode
    if (array_key_exists('billingZipCode', $data)) {
      $data['billingZipCode'] = trim($data['billingZipCode']);
    }

		$data = $this->removeEmptyParameters($data);

		// call API
		$response = $this->post(
			'/' . $processorID . '/charge',
			$data,
			[ 'authorization: Bearer ' . $this->getJWT($processorID) ]
		);
		$responseData = json_decode($response['body'], true);

		// handle response
		if ($response['code'] == 200) {
			if (!empty($responseData['transactionID'])) {
				$result->data_array['transaction_id'] = $responseData['transactionID'];
				$result->succeed();
			} else {
				$result->add_error('Missing transactionID in response');
			}
		} else {
			return $this->getErrorResult($response);
		}

		// remind the customer to update their expired card even if a payment processor succeeds.
    $expirationDate = $this->customerInformation['expDate'];
    $lastTwo = (int) substr($expirationDate, -2);
    $firstSection = (int) substr($expirationDate, 0, (strlen($expirationDate) < 4 ? 1 : 2));
    $currentYear = date('y');
    $currentMonth = date('m');

    if ($lastTwo < $currentYear || ($lastTwo <= $currentYear && $firstSection < $currentMonth)) {
      $this->sendExpiredCardSMS();
    }

		return $result;
	}

	public function verify($processorID, array $data)
	{
		$result = new \Result();

		// check required
		$required = [ 'token', 'expDate', 'orderID', 'brandID' ];
		foreach ($required as $field) {
			if (empty($data[$field])) {
				$result->add_error('Missing field ' . $field);
			}
		}
		if ($result->has_errors()) {
			return $result;
		}

		$data = $this->removeEmptyParameters($data);

		$response = $this->post(
			'/' . $processorID . '/verify',
			$data,
			[ 'authorization: Bearer ' . $this->getJWT($processorID) ]
		);

		if ($response['code'] == 200 && !empty($response['body'])) {
			// parse response
			$responseArr = json_decode($response['body'], true);
			if (!empty($responseArr['token'])) {
				$result->succeed();
				$result->data_array['token'] = $responseArr['token'];
			} else {
				$result->add_error('Missing response token');
			}
		} elseif (empty($response['body'])) {
			$result->add_error('Missing response body');
		} else {
			return $this->getErrorResult($response);
		}

		return $result;
	}

	private function getErrorResult($response)
	{
		$result = new \Result();

		// check for curl errors
		if (!empty($response['curl_errno'])) {
			$result->add_error('(retry) Payment API curl failed - ' . $response['curl_errno']);
			return $result;
		}

		// add errors from API response
		if (!empty($response['body'])) {
			$responseArr = json_decode($response['body'], true);
			if (is_array($responseArr) && !empty($responseArr['errors'])) {
				foreach ($responseArr['errors'] as $error) {
					$result->add_error($error);
				}
			}
		}

    if (in_array('Expired Card', $result->get_errors())) {
		  $this->sendExpiredCardSMS(true);
    }

		if (!$result->has_errors()) {
			$result->add_error('Unknown error');
		}

		return $result;
	}

	private function removeEmptyParameters(array $data)
	{
		// remove empty parameters
		foreach ($data as $key=>$val) {
			if (empty($val)) {
				unset($data[$key]);
			}
		}

		return $data;
	}

	private function getJWT($processorID)
	{
		$currentTime = \timeUTC();

		return JWT::encode(
			[
				'id' 	=> $processorID,
				'aud'	=> 'MintWeb',
				'iss' => 'ULTRA',
				'iat'	=> $currentTime,
				'nbf'	=> $currentTime,
				'exp'	=> $currentTime + ( 60 * 60 * 24 ),

			],
			$this->config['jwtSecret']
		);
	}

  private function sendExpiredCardSMS($didCardProcessFail = false)
  {
    if (!empty($this->customerInformation['customerID'])) {
      $customer = get_ultra_customer_from_customer_id($this->customerInformation['customerID'], ['current_mobile_number']);

      if (!empty($customer) && !empty($customer->current_mobile_number)) {
        if ($didCardProcessFail) {
          $template = 'credit_card_failed_and_expired';
          $parameters = [
            'amount' => number_format($this->customerInformation['amount'] / 100, 2),
            'login_token' => \Session::encryptToken($customer->current_mobile_number),
          ];
        } else {
          $template = 'credit_card_has_expired';
          $parameters = ['login_token' => \Session::encryptToken($customer->current_mobile_number)];
        }

        $smsResult = (new Messenger())->enqueueImmediateSms($this->customerInformation['customerID'], $template, $parameters);

        if ($smsResult->is_failure()) {
          dlog('', 'Failed to send expired credit card sms.');
        }
      } else {
        dlog('', 'Unable to find customer to send expired credit card sms.');
      }
    } else {
      dlog('', 'Unable to find customer to send expired credit card sms.');
    }
  }
}
