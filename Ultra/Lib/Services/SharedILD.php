<?php
namespace Ultra\Lib\Services;

use Ultra\Configuration\Configuration;

class SharedILD extends RestAPI
{
  private $configObj;
  private $config;

  public function __construct(Configuration $configObj=null)
  {
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();

    $this->config = $this->configObj->getSharedILDConfig();
    if (empty($this->config['host']) || empty($this->config['basepath'])) {
      throw new \Exception('Missing SharedILD configuration');
    }

    $curlConfig = [ CURLOPT_TIMEOUT => 30 ];

    parent::__construct($this->config['host'], $this->config['basepath'], $curlConfig, 3);
  }

  public function createCustomerBucket($customerID)
  {
    $response = $this->post('bucket', [ 'customerId' => $customerID ]);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    }

    $result = new \Result();
    $responseArr = json_decode($response['body'], true);

    if (is_array($responseArr) && !empty($responseArr['bucketId'])) {
      $result->data_array['bucket_id'] = $responseArr['bucketId'];
      $result->succeed();
    } else {
      $result->add_error('Failed to parse SharedILD response');
    }

    return $result;
  }

  public function getBucketIDByCustomerID($customerID)
  {
    $response = $this->get('bucket', [ 'customerId' => $customerID ]);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add bucket_id to result data_array
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if (is_array($responseArr) && !empty($responseArr['bucketId'])) {
        $result->data_array['bucket_id'] = $responseArr['bucketId'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse SharedILD response');
      }

      return $result;
    }
  }

  public function creditBucket($bucketID, $amount)
  {
    $response = $this->post(
      'bucket/' . $bucketID . '/credit',
      [ 'amount' => $amount]
    );

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    }

    $result = new \Result();
    $responseArr = json_decode($response['body'], true);

    if (is_array($responseArr) && !empty($responseArr['bucketId'])) {
      $result->data_array['bucket_id'] = $responseArr['bucketId'];
      $result->succeed();
    } else {
      $result->add_error('Failed to parse SharedILD response');
    }

    return $result;
  }

  public function creditBucketByCustomerID($customerID, $amount)
  {
    // find bucket ID by customer ID
    $result = $this->getBucketIDByCustomerID($customerID);
    if ($result->is_success()) {
      if (!empty($result->data_array['bucket_id'])) {
        $bucketID = $result->data_array['bucket_id'];
        return $this->creditBucket($bucketID, $amount);
      } else {
        $result->add_error('Missing bucket_id in result');
        return $result;
      }
    } else {
      return $result;
    }
  }

  private function getErrorResult($response)
  {
    $result = new \Result();

    // check for curl errors
    if (!empty($response['curl_errno'])) {
      $result->add_error('Shared ILD API curl failed - ' . $response['curl_errno']);
      return $result;
    }

    // add errors from API response
    if (!empty($response['body'])) {
      $responseArr = json_decode($response['body'], true);
      if (is_array($responseArr) && !empty($responseArr['errors'])) {
        foreach ($responseArr['errors'] as $error) {
          $result->add_error($error);
        }
      }
    }

    return $result;
  }
}