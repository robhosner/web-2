<?php
namespace Ultra\Lib\Services;

use Ultra\Configuration\Configuration;

class SharedData extends RestAPI
{
  private $configObj;
  private $config;

  public function __construct(Configuration $configObj=null)
  {
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();

    $this->config = $this->configObj->getSharedDataConfig();
    if (empty($this->config['host']) || empty($this->config['basepath'])) {
      throw new \Exception('Missing SharedData configuration');
    }

    $curlConfig = [ CURLOPT_TIMEOUT => 30 ];

    parent::__construct($this->config['host'], $this->config['basepath'], $curlConfig, 3);
  }

  public function getBucketIDByCustomerID($customerID)
  {
    $response = $this->get('bucket', [ 'customerId' => $customerID ]);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add bucket_id to result data_array
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if (is_array($responseArr) && !empty($responseArr['bucketId'])) {
        $result->data_array['bucket_id'] = $responseArr['bucketId'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse SharedData response');
      }

      return $result;
    }
  }

  public function getBucketByBucketID($bucketId)
  {
    $response = $this->get("/bucket/$bucketId");

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add bucket_id to result data_array
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if ( is_array($responseArr)
        && ! empty($responseArr['bucketId'])
        && ! empty($responseArr['ban'])
      ) {
        $result->data_array['bucket_id'] = $responseArr['bucketId'];
        $result->data_array['ban']       = $responseArr['ban'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse SharedData response');
      }

      return $result;
    }
  }

  public function setCustomerDataLimit($customerID, $percentage)
  {
    // find bucketID for customer
    $result = $this->getBucketIDByCustomerID($customerID);
    if (!$result->is_success()) {
      return $result;
    }

    $bucketID = $result->data_array['bucket_id'];

    // set data limit
    $response = $this->put('/bucket/' . $bucketID . '/members/' . $customerID, ['dataLimit' => $percentage]);
    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      $result = new \Result(null, true);
      return $result;
    }
  }

  private function getErrorResult($response)
  {
    $result = new \Result();

    // check for curl errors
    if (!empty($response['curl_errno'])) {
      $result->add_error('SharedData API curl failed - ' . $response['curl_errno']);
      return $result;
    }

    // add errors from API response
    if (!empty($response['body'])) {
      $responseArr = json_decode($response['body'], true);
      if (is_array($responseArr) && !empty($responseArr['errors'])) {
        foreach ($responseArr['errors'] as $error) {
          $result->add_error($error);
        }
      }
    }

    return $result;
  }
}