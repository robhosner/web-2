<?php

namespace Ultra\Lib\Billing;
use Ultra\Taxes\TransactionTypeCodeConfig;

/**
 * addBillingActionData
 *
 * Adds a new row to DB table HTT_BILLING_ACTIONS
 *
 * @return boolean
 */
function addBillingActionData($customer , $request_id , $provider_trans_id , $product_id , $subproduct_id , $load_amount, $sku, $provider)
{
  // RTRIN-44: transaction delay is based on payment source (payment provider's division)
  $delay = \Ultra\UltraConfig\getPaymentProviderDelay(\Ultra\UltraConfig\getPaymentProviderSkuAttribue($provider, $sku, 'SOURCE'));

  $query = htt_billing_actions_insert_query(
    array(
      'customer_id'    => $customer->CUSTOMER_ID,
      'request_id'     => $request_id,
      'status'         => 'OPEN',
      'attempt_log'    => '',
      'transaction_id' => $provider_trans_id,
      'product_id'     => $product_id,
      'subproduct_id'  => $subproduct_id,
      'next_attempt'   => 'dateadd(ss, '.$delay.', getutcdate())', // now + $delay seconds
      'amount'         => $load_amount,
      'provider_sku'   => $sku
    )
  );

  $success = is_mssql_successful(logged_mssql_query($query));

  if ( $success )
  {
    // add to Redis priority queue

    $redisBilling = new \Ultra\Lib\Util\Redis\Billing();

    $redisBilling->addUUID( $request_id , time() + $delay );
  }

  return $success;
}

/**
 * getBillingActionData
 *
 * Load a row from DB table HTT_BILLING_ACTIONS
 *
 * @return array
 */
function getBillingActionData( $uuid )
{
  $billingActionData = mssql_fetch_all_objects(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery(
        'HTT_BILLING_ACTIONS',
        NULL,
        array( '*' ),
        array( 'UUID' => $uuid )
      )
    )
  );

  dlog('',"billingActionData = %s",$billingActionData);

  return $billingActionData;
}

/**
 * updateBillingActionData
 *
 * Updates HTT_BILLING_ACTIONS according to API result
 *
 * @return boolean
 */
function updateBillingActionData( $redisBilling , $uuid , $decodedResponse , $attemptLog )
{
  $success = FALSE;

  if ( $decodedResponse != null && $decodedResponse->success )
    $success = \htt_billing_actions_update_success( $uuid );
  else
  {
    // compute retry count
    $entries = explode(";", $attemptLog);

    $retryCount = count($entries);

    $delay = 5 * 60; // 5 minutes
    if ( $retryCount > 1 )
      $delay *= 6;   // 30 minutes
    if ( $retryCount > 2 )
      $delay *= 2;   // 1 hour

    dlog('',"retryCount = $retryCount ; delay = $delay");

    // max 3 retries
    if ( count($entries) > 3 )
      $success = \htt_billing_actions_update_rejected( $uuid );
    else
    {
      // add to priority queue for the retry
      $redisBilling->addUUID( $uuid , time() + $delay );

      $success = \htt_billing_actions_update_retry( $uuid , $delay );
    }
  }

  return $success;
}

/**
 * billingTask
 *
 * Billing Runner task: process an OPEN row from DB table HTT_BILLING_ACTIONS
 *
 * @return string
 */
function billingTask( $redisBilling , $uuid=NULL )
{
  // fatal error: if set, the whole thread should interrupt the execution
  $error = FALSE;

  if ( ! $uuid )
    $uuid = $redisBilling->getNextUUID();

  if ( $uuid )
  {
    dlog('',"uuid = %s",$uuid);

    // load row from DB table HTT_BILLING_ACTIONS
    $billingActionData = getBillingActionData( $uuid );

    if ( ! $billingActionData || ! is_array($billingActionData) || ! count($billingActionData) )
    {
      dlog('',"No data found for uuid $uuid");
    }
    elseif ( $billingActionData[0]->STATUS == 'OPEN' )
    {
      // invoke internal__SubmitChargeAmount

      list( $internal_domain , $internal_user , $internal_password ) = \Ultra\UltraConfig\ultra_api_epay_info();

      dlog('',"internal_domain = $internal_domain , internal_user = $internal_user");

      $curlOptions = array(
        CURLOPT_USERPWD  => $internal_user.':'.$internal_password,
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      );

      $url = 'https://'.$internal_domain.'/pr/internal/1/ultra/api/internal__SubmitChargeAmount';

      $params = array(
        'customer_id'   => $billingActionData[0]->CUSTOMER_ID,
        'amount'        => $billingActionData[0]->AMOUNT,
        'reason'        => $billingActionData[0]->PRODUCT_ID,
        'reference'     => $billingActionData[0]->TRANSACTION_ID,
        'subproduct_id' => $billingActionData[0]->SUBPRODUCT_ID,
        'partner_tag'   => __FUNCTION__ . '|' . getmypid()
      );

      // example: curl -i 'https://$DOMAIN/pr/internal/1/ultra/api/internal__SubmitChargeAmount' -d 'customer_id=25&amount=1000&reason=a&reference=b'

      dlog('',"url    = $url");
      dlog('',"params = %s",$params);

      $jsonResult = curl_post($url,$params,$curlOptions,NULL,120);

      dlog('',"jsonResult = %s",$jsonResult);

      $decodedResponse = json_decode($jsonResult);

      dlog('',"decodedResponse = %s",$decodedResponse);

      // update HTT_BILLING_ACTIONS according to API result
      $success = updateBillingActionData( $redisBilling , $uuid , $decodedResponse , $billingActionData[0]->ATTEMPT_LOG );

      dlog('',"updateBillingActionData ".( $success ? 'succeeded' : 'failed' ));
    }
    else
    {
      dlog('',"Invalid status for uuid $uuid : ".$billingActionData[0]->STATUS);
    }
  }

  return $error;
}

function processCustomerCharge($customer,$subscriber_owed_amount,$lock_result,$transaction_type = NULL)
{
  /* move subscriber funds to accounts.balance */

  $result = array(
    'transient_errors' => array(), # transient CC errors, we will retry
    'rejection_errors' => array(), # fatal CC errors, the customer will be suspended
    'errors'           => array(), # software or DB or unknown errors, nothing to do
    'success'          => FALSE
  );

  $target_cos_id = ( $customer->MONTHLY_RENEWAL_TARGET ) ? get_cos_id_from_plan( $customer->MONTHLY_RENEWAL_TARGET ) : $customer->COS_ID;

  // Determine what the subscriber owes us if he stays in the current plan - in cents (including recurring bolt ons)
  $current_plan_costs = compute_subscriber_owed_amount_current_plan($customer);

  $owed_amount_current_plan       = $current_plan_costs['subscriber_owed_amount_current_plan'];
  $owed_amount_recurring_bolt_ons = $current_plan_costs['subscriber_owed_amount_recurring_bolt_ons'];

  dlog('',"target_cos_id = $target_cos_id ; subscriber_owed_amount = $subscriber_owed_amount ; %s",$current_plan_costs);

  // edge case : what if $customer->BALANCE < 0 ?
  if ($error = \Ultra\Lib\Billing\handleBalanceLessThanZero($customer))
  {
    $result['errors'][] = $error;
    return $result;
  }

  if ( ( ( $customer->stored_value * 100 ) >= $subscriber_owed_amount ) && ( $customer->BALANCE >= 0 ) )
  {
    dlog('', "Customer stored_value sufficient for a monthly renewal");

    $result['success'] = TRUE;
  }
  elseif ( $customer->monthly_cc_renewal )
  {
    dlog('', "customer about to charge credit card for customer %d", $customer->customer_id);

    // credit accounts.balance, make sure that we generate a LOAD record in htt_billing_history with a description of: "Automatic monthly recharge"
    // include Recurring Bolt Ons cost

    $return = func_add_balance_by_tokenized_cc(
      array(
        'skip_max_balance_check' => TRUE,
        'customer'               => $customer,
        'include_taxes_fees'     => TRUE,
        'reason'                 => 'Automatic monthly recharge',
        'target_cos_id'          => $target_cos_id,
        'session'                => $lock_result['lock_id'],
        'description'            => 'Automatic monthly recharge',
        'detail'                 => basename(__FILE__),
        'charge_amount'          => ( ( $subscriber_owed_amount + $owed_amount_recurring_bolt_ons ) / 100 ), // in $
        'recurring_payment'      => TRUE,
        'transaction_type'       => $transaction_type,
        'product_type'           => strpos($transaction_type, 'FLEX_RECHARGE_') === 0 ? TransactionTypeCodeConfig::LINE_CREDIT : TransactionTypeCodeConfig::PLAN,
      )
    );

    dlog('',"func_add_balance_by_tokenized_cc returned %s",$return);

    if ( $return->is_failure() )
      $result['rejection_errors'] = $return->get_errors();
    else
      $result['success'] = TRUE;
  }
  elseif ( ( ( $customer->BALANCE + $customer->stored_value ) * 100 ) >= $subscriber_owed_amount )
  {
    // the funds are enough to fund the monthly renewal or upgrade

    dlog('', "Customer balance + stored_value sufficient for a monthly renewal");

    $result['success'] = TRUE;
  }
  elseif ( ( ( $customer->BALANCE + $customer->stored_value ) * 100 ) >= $owed_amount_current_plan )
  {
    // the funds are enough to fund the monthly renewal, but not the upgrade to a higher plan

    dlog('', "Customer balance + stored_value only sufficient for a monthly renewal, not for a plan upgrade");

    $sms_enqueue_result = funcSendExemptCustomerSMSRenewalPlanFailChange(
      array(
        'customer' => $customer
      )
    );

    $target_cos_id = $customer->COS_ID;
    $customer->MONTHLY_RENEWAL_TARGET = '';

    $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
      array(
        'customer_id'            => $customer->customer_id,
        'monthly_renewal_target' => 'NULL'
      )
    );

    $check = run_sql_and_check($htt_customers_overlay_ultra_update_query);

    if ( $check )
      $result['success'] = TRUE;
    else
      $result['errors'][] = "ERROR : cannot update htt_customers_overlay_ultra";

  }
  else
  {
    // monthly_cc_renewal = 0 AND balance + stored_value < subscriber_owed_amount

    dlog('', "Not enough funds to be accredited to accounts.balance for customer %d", $customer->customer_id);
  }

  // handle rejection errors
  if ( count($result['rejection_errors']) && ( ( ( $customer->BALANCE + $customer->stored_value ) * 100 ) >= $subscriber_owed_amount ) )
  {
    // CC charge failed, but the customer has enough funds to fund the monthly renewal or upgrade

    dlog('', "Customer funds sufficient for a monthly renewal/upgrade even though CC charge failed");

    monthly_service_charge_log_fatal_errors_list($customer->customer_id,$result['rejection_errors']);

    $result['rejection_errors'] = array();

    $result['success'] = TRUE;
  }
  elseif ( count($result['rejection_errors']) && ( ( ( $customer->BALANCE + $customer->stored_value ) * 100 ) >= $owed_amount_current_plan ) )
  {
    // CC charge failed, but the customer has enough funds to fund only the monthly renewal

    dlog('', "Customer funds sufficient for a monthly renewal only even though CC charge failed");

    monthly_service_charge_log_fatal_errors_list($customer->customer_id,$result['rejection_errors']);

    $result['rejection_errors'] = array();

    $result['success'] = TRUE;
  }
  elseif (count($result['rejection_errors']))
  {
    \logDebug('Processing rejection errors ' . json_encode($result['rejection_errors']));

    $error_code = \Ultra\Lib\Billing\getRejectionErrorCode($return->data_array['cc_transactions_id']);

    if ($error_code && $error_code != 'retry')
      \Ultra\Lib\Billing\handleRejectionErrorSMS($customer, $error_code, $lock_result['attempt_count']);

    // set 051,005 as a transient error, utilized in after process_customer in process_customers
    if (in_array($error_code, \Ultra\UltraConfig\getInsufficientFundsErrorCodes()))
    {
      \logDebug('Processing (insufficient funds) rejection, handling as transient error');
      $result['transient_errors'] = array($error_code);
    }

    // internal retry
    if ($error_code == 'retry') {
      \logDebug('Processing internal retry');
      $result['transient_errors'] = array($error_code);
    }

    $result['success'] = FALSE;
  }

  // Note: if CC charge failed with transient errors, we retry even if the customer has enough funds to fund the monthly renewal

  dlog('', "processCustomerCharge ended with %s - %d", ( $result['success'] ? 'success' : 'failure' ), $customer->customer_id);

  return $result;
}

/**
 * handleBalanceLessThanZero
 * @param  Object $customer
 * @return String || Boolean $error_code
 */
function handleBalanceLessThanZero($customer)
{
  $error = FALSE;

  if ( ( $customer->BALANCE < 0 ) && ( $customer->BALANCE > -10 ) )
  {
    // adjust $customer->BALANCE to 0

    $sql = accounts_update_query(
      array(
        'add_to_balance' => ( - $customer->BALANCE ),
        'customer_id'    => $customer->customer_id
      )
    );

    if ( !is_mssql_successful(logged_mssql_query($sql)) )
      return "ERROR : cannot update accounts";

    $customer = get_customer_from_customer_id($customer->customer_id);

    if ( ! $customer )
      return "ERROR : cannot load customer after accounts update";
  }
  else if ( $customer->BALANCE <= - 10 )
    return "ERROR : cannot process customer id ".$customer->customer_id.", since his wallet is lower than -10";

  return $error;
}

/**
 * getRejectionErrorCode
 * get rejection error description, parse and return error code
 * @param  Int    $transaction_id
 * @return String $error_code
 */
function getRejectionErrorCode($transaction_id)
{
  $error_code = NULL;

  $cc_transaction = get_ultra_cc_transaction(array('CC_TRANSACTIONS_ID' => $transaction_id));

  \logDebug('ULTRA_CC_TRANSACTION data: ' . json_encode($cc_transaction));

  // decoding ERR_DESCRIPTION - i.e. (051) Declined, $matches[1] == 051
  $matches = array();

  if ( ! empty( $cc_transaction ) && ! empty( $cc_transaction->ERR_DESCRIPTION ) )
  {
    if (substr($cc_transaction->ERR_DESCRIPTION, 0, 7) == '(retry)') {
      \logInfo('Detected internal retry error');

      return 'retry';
    }

    preg_match("/\((\d+)\)/", $cc_transaction->ERR_DESCRIPTION, $matches);

    if ( ! empty($matches[1]))
      $error_code = $matches[1];
    else
      \logDebug('Could not match rejection error ID in error string: ' . $cc_transaction->ERR_DESCRIPTION);
  }

  return $error_code;
}

/**
 * handleRejectionErrorSMS
 * attempts to send a friendly SMS error based on error_code and attempt_count
 *
 * @param  Object $customer
 * @param  String $error_code
 * @param  Int    $attempt_count Number of attempts for transaction
 * @return NULL
 */
function handleRejectionErrorSMS($customer, $error_code, $attempt_count)
{
  \logDebug("Handling rejection error SMS with input: error_code => $error_code, attempt_count => $attempt_count");

  $message  = NULL;
  $messages = array();

  // insufficient funds
  if (in_array($error_code, \Ultra\UltraConfig\getInsufficientFundsErrorCodes()))
  {
    $template = NULL;

    if ($attempt_count == 1)
      $template = 'first_decline_day_0';
    elseif ($attempt_count == 2)
      $template = 'second_decline_day_1';
    elseif ($attempt_count == 3)
      $template = 'final_decline_day_2';

    // set 051 specific SMS message
    if ($template)
      $messages[$customer->preferred_language] = \Ultra\Messaging\Templates\getTemplate($template, $customer->preferred_language);
  }
  else
    $messages = \Ultra\Lib\MeS\getMeaningfulApiErrorMessage($error_code);

  // get user preferred language error or try to default to EN
  if (count($messages))
  {
    if ( ! empty($messages[$customer->preferred_language]))
      $message = $messages[$customer->preferred_language];
    elseif ( ! empty($messages['EN']))
      $message = $messages['EN'];
  }

  // if found friendly message, send
  if ($message != NULL)
  {
    \logDebug('Sending CC rejection message, ' . $message . ', to customer_id: ' . $customer->CUSTOMER_ID);

    $sms_result = funcSendCustomerSMS(array(
      'type'     => 'internal',
      'customer' => $customer,
      'message'  => $message
    ));

    if ( ! $sms_result['sent'])
    {
      if (count($sms_result['errors']))
        \logError('ERROR sending friendly MeS error SMS: ' . $sms_result['errors'][0]);
      else
        \logError('ERROR sending friendly MeS error SMS');
    }
  }
  else
  {
    \logError('ERROR finding friendly MeS error SMS message');
  }
}
