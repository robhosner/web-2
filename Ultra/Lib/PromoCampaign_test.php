<?php

// test for \Ultra\Lib\PromoCampaign

/*
php Ultra/Lib/PromoCampaign_test.php triggerShortCode SHORTCODE CUSTOMER_ID
*/

require_once 'db.php';
require_once 'Ultra/Lib/PromoCampaign/functions.php';

abstract class AbstractTestStrategy
{
  abstract function test( $argv  );
}

class TestPromoCampaign_triggerShortCode
{
  function test( $argv )
  {
    $info = \Ultra\Lib\PromoCampaign\triggerShortCode( $argv[2] , get_customer_from_customer_id( $argv[3] ) );

    print_r( $info );
  }
}

class TestPromoCampaign_getPromoEnabledCampaignByShortCode
{
  function test( $argv )
  {
    $info = \Ultra\Lib\PromoCampaign\getPromoEnabledCampaignByShortCode( $argv[2] );

    print_r( $info );
  }
}

class TestPromoCampaign_add4gData
{
  function test( $argv )
  {
    $customer = get_customer_from_customer_id( $argv[2] );
    $value    = $argv[3];

    if ( ! $customer )
    {
      echo "customer not found\n"; exit;
    }

    if ( ! is_numeric($value) )
    {
      echo "value is not numeric\n"; exit;
    }

    $result = \Ultra\Lib\PromoCampaign\add4gData( $customer , $value );

    print_r( $result );
  }
}


# perform test #


$testClass = 'TestPromoCampaign_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

// connect to DB
teldata_change_db();

$testObject->test( $argv );


?>
