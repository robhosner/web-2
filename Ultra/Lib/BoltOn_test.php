<?php

// test for \Ultra\Lib\BoltOn namespace

require_once 'db.php';
require_once 'Ultra/Lib/BoltOn/functions.php';

/*
php Ultra/Lib/BoltOn_test.php addBoltOnImmediate
php Ultra/Lib/BoltOn_test.php addDATABoltOnImmediate  $CUSTOMER_ID
php Ultra/Lib/BoltOn_test.php addIDDCABoltOnImmediate $CUSTOMER_ID
php Ultra/Lib/BoltOn_test.php addVOICEBoltOnImmediate $CUSTOMER_ID $BOLT_ON
php Ultra/Lib/BoltOn_test.php verifyMintDataAddOnThreshold $MSISDN
*/

abstract class AbstractTestStrategy
{
  abstract function test( $argv  );
}

class TestBoltOn_verifyMintDataAddOnThreshold extends AbstractTestStrategy
{
  function test( $argv )
  {
    $msisdn = $argv[2];

    $customer = get_customer_from_msisdn($msisdn);

    if ( empty( $customer ) )
    {
      echo "There is no customer associated with msisdn $msisdn\n";
      exit;
    }

    $error = \Ultra\Lib\BoltOn\verifyMintDataAddOnThreshold( $customer );

    if ( empty( $error ) )
      echo "OK\n";
    else
      echo "$error\n";
  }
}

class TestBoltOn_addDATABoltOnImmediate extends AbstractTestStrategy
{
  function test( $argv )
  {
    $customer_id = $argv[2];

    $info = \Ultra\UltraConfig\getBoltOnInfo('DATA_500_5');

    print_r( $info );

    list($e,$c) = \Ultra\Lib\BoltOn\addBoltOnImmediate( $customer_id , $info , 'test_source' );

    echo "error = $e ; code = $c\n";
  }
}

class TestBoltOn_addIDDCABoltOnImmediate extends AbstractTestStrategy
{
  function test( $argv )
  {
    $customer_id = $argv[2];

    $info = \Ultra\UltraConfig\getBoltOnInfo('IDDCA_12.5_10');

    print_r( $info );

    list($e,$c) = \Ultra\Lib\BoltOn\addIDDCABoltOnImmediate( $customer_id , $info , 'test_source' );

    echo "error = $e ; code = $c\n";
  }
}

class TestBoltOn_addBoltOnImmediate extends AbstractTestStrategy
{
  function test( $argv )
  {
    list($e,$c) = \Ultra\Lib\BoltOn\addBoltOnImmediate( NULL , NULL , 'test_source' );

    echo "error = $e ; code = $c\n";

    list($e,$c) = \Ultra\Lib\BoltOn\addBoltOnImmediate( 1 , array() , 'test_source' );

    echo "error = $e ; code = $c\n";
  }
}


class TestBoltOn_addVOICEBoltOnImmediate extends AbstractTestStrategy
{
  function test( $argv )
  {
    $customer_id = $argv[2];
    $info = \Ultra\UltraConfig\getBoltOnInfo($argv[3]);
    print_r( $info );
    list($e,$c) = \Ultra\Lib\BoltOn\addBoltOnImmediate( $customer_id , $info , 'test_source', 'TEST' );
    echo "error = $e ; code = $c\n";
  }
}


# perform test #


$testClass = 'TestBoltOn_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

// connect to DB
teldata_change_db();

$testObject->test( $argv );


?>
