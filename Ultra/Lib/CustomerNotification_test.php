<?php

require_once 'db.php';
require_once 'Ultra/Lib/CustomerNotification.php';

teldata_change_db();

$cn = new \Ultra\Lib\CustomerNotification();

$method = new ReflectionMethod('\Ultra\Lib\CustomerNotification', 'isCampaignKeyword');

$method->setAccessible(true);

$method->invoke(
  $cn,
  'a'
);

