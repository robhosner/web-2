<?php

/*

MVNE functions at Ultra Adapter level

*/

require_once 'db/htt_cancellation_reasons.php';
require_once 'Ultra/Lib/MQ/ListEndPoint.php';
require_once 'Ultra/Lib/MVNE/MakeItSo.php';
require_once 'Ultra/Lib/DB/Customer.php';
require_once 'classes/RoamCreditPromo.php';

/**
 * mvneBypassListAsynchronousSendSMS
 *
 * Invoke a SendSMS ACC API call asynchronously
 * This function allows UTF-8 symbols in $text
 *
 * @return object of class Result
 */
function mvneBypassListAsynchronousSendSMS( $text , $msisdn , $actionUUID=NULL )
{
  return mvneBypassListSendSMS( $text , $msisdn , $actionUUID , TRUE );
}

/**
 * mvneBypassListSynchronousSendSMS
 *
 * Invoke a SendSMS ACC API call synchronously
 * This function allows UTF-8 symbols in $text
 *
 * @return object of class Result
 */
function mvneBypassListSynchronousSendSMS( $text , $msisdn , $actionUUID=NULL )
{
  return mvneBypassListSendSMS( $text , $msisdn , $actionUUID , FALSE );
}

/**
 * mvneBypassListSendSMS
 *
 * Invoke a SendSMS ACC API call directly from the MW layer for speed reasons ( ListEndPoint implementation )
 * This function allows UTF-8 symbols in $text
 *
 * @return object of class Result
 */
function mvneBypassListSendSMS( $text , $msisdn , $actionUUID=NULL , $asynchronous=FALSE )
{
  $listEndPoint = \Ultra\Lib\MQ\getSMSListEndPoint();

  $listEndPoint->setRequestReplyTimeoutSeconds(60);

  if (empty($actionUUID))
    $actionUUID = getNewActionUUID('mvne ' . time());

  $outboundMessageKey = $listEndPoint->pushMessage(
    $listEndPoint->buildMessage(
      array(
        'actionUUID' => $actionUUID,
        'header'     => 'SendSMS',
        'body'       => array(
          'msisdn'             => $msisdn,
          'preferred_language' => 'en', // do not change this parameter, since it does not really affect language //
          'sms_text'           => $text
        )
      )
    )
  );

  if ( ! $outboundMessageKey )
    return make_error_Result( 'Cannot push SendSMS message to MW' );

  if ( $asynchronous )
    return make_ok_Result();

  $result = $listEndPoint->waitForReplyMessage( $outboundMessageKey );

  dlog('',"waitForReplyMessage result = %s",$result);

  if ( $result->is_success() )
  {
    $data = $listEndPoint->extractFromMessage( $result->data_array['reply'] );

    if ( $data )
    {
      $data = (array) $data;

      dlog('',"data = %s",$data);

      if ( $data['body'] )
      {
        dlog('',"body = %s",$data['body']);

        if ( $data['body']->success )
          $result = make_ok_Result( (array) $data['body'] );
        else
        {
          $result = make_error_Result( $data['body']->errors[0] );
          $result->data_array['ResultCode'] = (empty($data['body']->ResultCode)) ? '' : $data['body']->ResultCode;
        }
      }
      else
        $result = make_error_Result( 'Cannot extract data body from reply message' );
    }
    else
     $result = make_error_Result( 'Cannot extract data from reply message' );
  }

  return $result;
}

/**
 * mvneBypassSendSMS
 *
 * Invoke a SendSMS ACC API call directly from the MW layer for speed reasons
 *
 * @return array
 */
function mvneBypassSendSMS( $text , $msisdn )
{
  $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

  $accResult = $accMiddleware->processControlCommand(
    array(
      'actionUUID' => getNewActionUUID('messaging ' . time()),
      'command'    => 'SendSMS',
      'parameters' => array(
        'msisdn'             => $msisdn,
        'preferred_language' => 'en', // TODO
        'qualityOfService'   => '801',
        'sms_text'           => $text
      )
    )
  );

  $result = array(
    'sent'   => $accResult->is_success(),
    'errors' => $accResult->get_errors()
  );

  if ( $accResult->is_success() )
  {
    // MW success
    if ( isset($accResult->data_array['message']) )
    {
      dlog( '' , "errors = %s ; message = %s" , $accResult->get_errors() , $accResult->data_array['message'] );

      $dataBody = $accMiddleware->controlObject->extractDataBodyFromMessage( $accResult->data_array['message'] );

      dlog('',"resultQuerySubscriber dataBody = %s",$dataBody);

      // SendSMS failure
      if ( $dataBody['ResultCode'] != 100 )
        $result = array(
          'sent'   => FALSE,
          'errors' => array('SendSMS error : '.$dataBody['ResultCode'].' - '.$dataBody['ResultMsg'])
        );
    }
    else
      // MW response invalid
      $result = array(
        'sent'   => FALSE,
        'errors' => array('empty return message from SendSMS')
      );
  }
  else
    dlog('',"SendSMS errors = %s",$accResult->get_errors());

  return $result;
}

/**
 * mvneCustomerActiveCheck
 *
 * Returns 'is_active' set to true if the customer is active on the network.
 *
 * @return object of class \Result
 */
function mvneCustomerActiveCheck( $customer )
{
  $msisdn = ( $customer->current_mobile_number ) ? $customer->current_mobile_number : NULL ;
  $iccid  = ( $customer->CURRENT_ICCID_FULL    ) ? $customer->CURRENT_ICCID_FULL    : NULL ;

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $result = $mwControl->mwQuerySubscriber(
    array(
      'actionUUID' => getNewActionUUID('mvneCustomerActiveCheck ' . time()),
      'msisdn'     => $msisdn,
      'iccid'      => $iccid
    )
  );

  if ( $result->is_success()
    && isset($result->data_array['body'])
    && ( property_exists( $result->data_array['body'] , 'SubscriberStatus' ) )
  )
    $result->data_array['is_active'] = ! ! ( $result->data_array['body']->SubscriberStatus == 'Active' );
  else
    $result->fail();

  return $result;
}

/**
 * mvneRequestPortIn
 *
 * Invokes mwPortIn
 *
 * @return object of class \Result
 */
function mvneRequestPortIn(
  $customer_id,
  $numberToPort,
  $ICCID,
  $portUsername,
  $portPassword,
  $billingZip,
  $cos_id,
  $transition_uuid=NULL,
  $action_uuid=NULL
)
{
  // PROD-1251: abort canceled port-ins
  if ($redis = new \Ultra\Lib\Util\Redis)
    if ($redis->get("ultra/port/status/$numberToPort") === 'ABORT')
      return make_error_Result('Port in cancelled by API');

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID( 'port ' . time() );

  // retrieve customer's current preferred language
  $customer = get_ultra_customer_from_customer_id($customer_id, array('preferred_language'));

  if ( ! $customer )
    return make_error_Result( 'Cannot find customer for customer_id = '.$customer_id );

  // API-377: prohibit activations on grandfathered plans
  $planName = get_monthly_plan_from_cos_id($cos_id);
  if (is_grandfathered_plan($planName))
    return make_error_Result("Plan $planName is no longer available");

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($planName));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $result = $mwControl->mwPortIn(
    array(
      'customer_id'        => $customer_id,
      'msisdn'             => $numberToPort,
      'iccid'              => $ICCID,
      'actionUUID'         => $action_uuid,
      'zipcode'            => $billingZip,
      'ultra_plan'         => $planName,
      'wholesale_plan'     => $planConfiguration['wholesale'],
      'preferred_language' => $customer->preferred_language,
      'port_account'       => $portUsername,
      'port_password'      => $portPassword
    )
  );

  dlog('',"mwPortIn result = %s",$result);

  if ( $result->is_success() ) // the MW call succeeded
    if ( $result->get_data_key('success') )
    {
      $portInQueue = new \PortInQueue();

      $portInQueueResult = $portInQueue->loadByMsisdn( $numberToPort );

      if ( $portInQueueResult->is_success() )
      {
        $portInQueueResult = $portInQueue->updateCarrierInfo();

        if ( $portInQueueResult->is_failure() )
          dlog('',"ERROR ESCALATION ALERT DAILY - we could not update carrier info for MSISDN $numberToPort");
      }
      else
        dlog('',"ERROR ESCALATION ALERT DAILY - we could not load PORTIN_QUEUE for MSISDN $numberToPort");
    }
    else
      $result = make_error_Result( $result->get_data_key('errors') );

  // connect back to the default DB
  teldata_change_db();

  return $result;
}

/**
 * mvneUpdatePortIn
 *
 * Invokes mwUpdatePortIn
 *
 * @return object of class \Result
 */
function mvneUpdatePortIn(
  $customer_id,
  $numberToPort,
  $ICCID,
  $portUsername,
  $portPassword,
  $billingZip,
  $transition_uuid=NULL,
  $action_uuid=NULL
)
{
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $mwResult = $mwControl->mwUpdatePortIn(
    array(
      'actionUUID'         => $action_uuid,
      'msisdn'             => $numberToPort,
      'port_account'       => $portUsername,
      'port_password'      => $portPassword,
      'zipcode'            => $billingZip
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return $mwResult;
}

/**
 * mvneAddRecurringUpData
 *
 * Recurring Bolt On - UpData - Wraps around mvneMakeitsoUpgradePlan
 *
 * @return array
 */
function mvneAddRecurringUpData( $action_uuid=NULL , $customer_id , $option , $sku, $plan )
{
  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $result = mvneMakeitsoUpgradePlan( $action_uuid , $customer_id , $option, NULL, $plan );

  if ( ! $result['success'] )
  {
    // record the failure in ULTRA.BOLTON_TRACKER
    $result = trackBoltOn( $customer_id , 'MONTHLY' , 'ERROR' , $sku , 'DATA' , 'MONTHLY' );

    // send message to the customer: Recurring Bolt On UpData failed
    \funcSendExemptCustomerSMSRecurringBoltOnsError(
      array(
        'customer_id' => $customer_id,
        'customer'    => get_customer_from_customer_id( $customer_id ),
        'bolt_ons'    => 'UpData'
      )
    );
  }

  return $result;
}

/**
 * mvneMakeitsoUpgradePlan
 *
 * Invokes mwMakeitsoUpgradePlan with an appropriate $option
 * Examples:
 *  - A-DATA-BLK|500
 *  - A-DATA-BLK|1024
 *  - A-DATA-BLK|2048
 *  - A-DATA-MINT|1
 *  - A-DATA-MINT|3
 *  - CHANGE|S29
 *  - V-ENGLISH
 *  - V-SPANISH
 *
 * @return array
 */
function mvneMakeitsoUpgradePlan( $action_uuid=NULL , $customer_id , $option , $customer = NULL, $plan = NULL )
{
  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $cos_id = null;
  if ( ! $customer )
  {
    $customer = get_ultra_customer_from_customer_id($customer_id, array(
      'current_mobile_number',
      'CURRENT_ICCID_FULL',
      'preferred_language'
    ));

    $account = get_account_from_customer_id($customer_id, array('COS_ID'));
    $cos_id  = $account->COS_ID;
  }
  else
  {
    $cos_id = $customer->COS_ID;
  }

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  // PROD-1908: override subscribers current wholesale plan if a different target plan is given
  if ($plan)
  {
    if ( ! $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan)))
      return array('success' => FALSE, 'errors' => "unknown plan $plan");
    $wholesalePlan = $planConfiguration['wholesale'];
  }
  else
    $wholesalePlan = \Ultra\Lib\DB\Customer\getWholesalePlan($customer_id);

  $result = $mwControl->mwMakeitsoUpgradePlan(
    array(
      'actionUUID'         => $action_uuid,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'customer_id'        => $customer_id,
      'wholesale_plan'     => $wholesalePlan,
      'ultra_plan'         => get_base_plan_name(get_plan_from_cos_id( $cos_id )), # L[12345]9
      'preferred_language' => $customer->preferred_language,
      'option'             => $option
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}

/**
 * mvneMakeitsoRenewPlan
 *
 * Invokes mwMakeitsoRenewPlan
 *
 * @return array
 */
function mvneMakeitsoRenewPlan($customer_id, $plan, $transition_uuid = NULL, $action_uuid = NULL)
{
  $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language', 'plan_expires'));
  $account  = get_account_from_customer_id($customer_id, array('COS_ID', 'ACTIVATION_DATE_TIME'));
  $subplan  = get_brand_uv_subplan($customer_id);

  if ( ! $customer || ! $account)
    return array( 'success' => FALSE , 'errors' => array('Could not load data for customer id '.$customer_id.' in mvneMakeitsoRenewPlan') );

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $preferred_language = ( ! $customer->preferred_language ) ? 'EN' : $customer->preferred_language ;

  // PROD-2048: retrieve current HTT_PLAN_TRACKER_ID and pass it to MISO
  // missing plan tracker or first period means a new activation, therefore we do not need to log usage
  $plan_tracker = get_last_plan_tracker($customer_id);
  $plan_tracker_id = $plan_tracker && $plan_tracker->period > 1 ? $plan_tracker->HTT_PLAN_TRACKER_ID : NULL;

  $mwMakeitsoRenewPlanParams = [
    'actionUUID'         => $action_uuid,
    'msisdn'             => $customer->current_mobile_number,
    'iccid'              => $customer->CURRENT_ICCID_FULL,
    'ultra_plan'         => get_base_plan_name($plan),
    'wholesale_plan'     => $planConfiguration['wholesale'], // DATAQ-29: wholesale plan may change
    'preferred_language' => $preferred_language,
    'customer_id'        => $customer_id,
    'plan_tracker_id'    => $plan_tracker_id,
    'subplan'            => $subplan
  ];

  // RoamCreditPromo
  $roamCreditPromo = new \Ultra\Lib\RoamCreditPromo();
  $roamCreditPromo->setActivationDate($account->ACTIVATION_DATE_TIME);
  $roamCreditPromo->setRenewalDate($customer->plan_expires);
  $roamCreditPromo->setCosId($account->COS_ID);
  $roamCreditPromo->addMakeitsoParam($mwMakeitsoRenewPlanParams);

  $result = $mwControl->mwMakeitsoRenewPlan($mwMakeitsoRenewPlanParams);

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}

/**
 * mvneChangePlanImmediate
 *
 * Invokes enqueueUpgradePlan ('CHANGE|(L[1245]9)|D39|(UV[2345][05])') // note: UV plans have gaps
 * $plan can be NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE
 * For immediate plan switches ("Change Plan Mid-Cycle $from to $to")
 *
 * @return array
 */
function mvneChangePlanImmediate( $customer_id , $plan , $transition_uuid=NULL , $action_uuid=NULL )
{
  // API-377: prohibit changes to grandfathered plans
  if (is_grandfathered_plan($plan))
    return array('success' => FALSE, 'errors' => array("Plan $plan is no longer available"));

  // get MSISDN from $customer_id
  $MSISDN = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer_id, 'current_mobile_number');

  if ( ! $MSISDN )
    return array( 'success' => FALSE , 'errors' => array('No MSISDN found for customer id '.$customer_id.' in mvneChangePlanImmediate') );

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $customer = get_ultra_customer_from_customer_id($customer_id, array('CURRENT_ICCID_FULL', 'preferred_language'));

  if ( ! $customer )
    return array( 'success' => FALSE , 'errors' => array('Could not load data for customer id '.$customer_id.' in mvneChangePlanImmediate') );

  $option = 'CHANGEIMMEDIATE|'.$plan;
  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  // enqueue a MakeItSo item and return
  $result = $mwControl->mwMakeitsoUpgradePlan(
    array(
      'actionUUID'         => $action_uuid,
      'customer_id'        => $customer_id,
      'msisdn'             => $MSISDN,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => $plan,
      'wholesale_plan'     => $planConfiguration['wholesale'], // DATAQ-22: wholesale plan may change
      'preferred_language' => $customer->preferred_language,
      'option'             => $option
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}

/**
 * mvneChangeMSISDN
 *
 * Invokes mwChangeMSISDN
 * $account_id is $customer->ACCOUNT_ID
 * $source is the API call or whatever triggers the call
 *
 * @return object of class \Result
 */
function mvneChangeMSISDN( $old_msisdn , $iccid , $zipcode , $customer_id , $source , $account_id )
{
  $result = make_ok_Result(NULL);

  $new_msisdn = '';

  try
  {
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwChangeMSISDN(
        array(
          'actionUUID'         => getNewActionUUID('mvne ' . time()),
          'msisdn'             => $old_msisdn,
          'iccid'              => $iccid,
          'zipcode'            => $zipcode,
          'customer_id'        => $customer_id
        )
      );

      dlog('',"mwChangeMSISDN result = %s",$result);

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        if ( count($errors) )
          throw new Exception( $errors[0] );
        else
          throw new Exception("ERR_API_INTERNAL: middleware call failed");
      }

      if ( isset($result->data_array['success']) && $result->data_array['success'] == FALSE )
        throw new Exception( $result->data_array['errors'][0] );

      $dataBody = (array) $result->data_array['body'];

      $new_msisdn = $dataBody['MSISDN'];

      dlog('',"mwChangeMSISDN success : new msisdn = %s",$new_msisdn);
    }

    if ( ! $new_msisdn )
      throw new Exception("ERR_API_INTERNAL: ChangeMSISDN did not return the new MSISDN");

    // update CURRENT_MOBILE_NUMBER in DB table HTT_CUSTOMERS_OVERLAY_ULTRA
    $check = run_sql_and_check(
      htt_customers_overlay_ultra_update_query(
        array(
          'customer_id'           => $customer_id,
          'current_mobile_number' => $new_msisdn
        )
      )
    );

    if ( ! $check )
      throw new Exception("ERR_API_INTERNAL: Customer DB write error");

    // add new msisdn to HTT_ULTRA_MSISDN
    $check = add_to_htt_ultra_msisdn( $customer_id , $new_msisdn, 1, $source , '2' );

    if ( ! $check )
      throw new Exception("ERR_API_INTERNAL: new MSISDN DB write error");

    $check = run_sql_and_check(sprintf("UPDATE HTT_ULTRA_MSISDN
SET msisdn_activated = 0, expires_date = GETUTCDATE(), last_changed_date = GETUTCDATE(), replaced_by_msisdn = %s
WHERE customer_id = %d AND msisdn <> %s",
                        $new_msisdn,
                        $customer_id,
                        $old_msisdn
    ));

    if ( ! $check )
      throw new Exception("ERR_API_INTERNAL: old MSISDN DB write error");

    // update ACCOUNT_ALIASES

    $account_aliases_update_query = account_aliases_update_query(
      array(
        'msisdn'     => $new_msisdn,
        'account_id' => $account_id
      )
    );

    if ( ! run_sql_and_check($account_aliases_update_query) )
      throw new Exception("ERR_API_INTERNAL: account_aliases DB write error");

    // update DESTINATION_ORIGIN_MAP

    $destination_origin_map_update_query1 = destination_origin_map_update_query(
      array(
        'destination_from' => '1' . $old_msisdn,
        'destination_to'   => '1' . $new_msisdn
      )
    );

    $destination_origin_map_update_query2 = destination_origin_map_update_query(
      array(
        'destination_from' => $old_msisdn,
        'destination_to'   => $new_msisdn
      )
    );

    if ( ! run_sql_and_check($destination_origin_map_update_query1) )
      throw new Exception("ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP DB write error");

    if ( ! run_sql_and_check($destination_origin_map_update_query2) )
      throw new Exception("ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP DB write error");

    $result = make_ok_Result(
      array(
        'msisdn' => $new_msisdn
      )
    );
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());

    $result = make_error_Result( $e->getMessage() );
  }

  return $result;
}

/**
 * mvneProvisionSIM
 *
 * voice/sms/data socs are not added
 * Invokes mwActivateSubscriber
 * $plan can be [ STANDBY , NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE ]
 *
 * @return object of class \Result
 */
function mvneProvisionSIM( $ICCID, $zipcode, $language, $plan, $customer_id, $transition_uuid, $action_uuid )
{
  return mvneActivateSIM( $ICCID, $zipcode, $language, $plan, $customer_id, $transition_uuid, $action_uuid , TRUE );
}

/**
 * mvneActivateSIM
 *
 * Invokes mwActivateSubscriber
 * $plan can be [ STANDBY , NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE ]
 *
 * @return object of class \Result
 */
function mvneActivateSIM( $ICCID, $zipcode, $language, $plan, $customer_id, $transition_uuid, $action_uuid , $provisioning=FALSE )
{
  // API-377: prohibit activations on grandfathered plans
  if (is_grandfathered_plan($plan))
    return make_error_Result("Plan $plan is no longer available");

  if ( !$ICCID )
  {
    $sql = htt_customers_overlay_ultra_select_query(
      array(
        'select_fields' => array('customer_id','CURRENT_ICCID_FULL'),
        'customer_id'   => $customer_id
      )
    );

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( $data && is_array($data) && count($data) )
      $ICCID = $data[0]->CURRENT_ICCID_FULL;
  }

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_base_plan_name($plan));

  {
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    reserve_iccid($ICCID);

    $mwResult = $mwControl->mwActivateSubscriber(
      array(
        'iccid'              => $ICCID,
        'zipcode'            => $zipcode,
        'ultra_plan'         => ( $provisioning ? 'STANDBY' : $planConfiguration['aka'] ),
        'wholesale_plan'     => $planConfiguration['wholesale'], // DATAQ-20: activation Wholesale Plan is configured in cfengine
        'preferred_language' => $language,
        'actionUUID'         => $action_uuid,
        'customer_id'        => $customer_id,
        'brand_id'           => $planConfiguration['brand_id']
      )
    );

    if ( $mwResult->is_success() )
    {
      if ( isset( $mwResult->data_array['errors'] ) && count($mwResult->data_array['errors']) )
        $mwResult = make_error_Result( $mwResult->data_array['errors'][0] );
      else
      {
        $dataBody = (array) $mwResult->data_array['body'];

        $record_ret = record_msisdn($customer_id, $dataBody['MSISDN'], $transition_uuid);

        $mwResult->add_result_object($record_ret);
      }
    }

    if ( $mwResult->is_failure() )
      unreserve_iccid($ICCID);

    return $mwResult;
  }
}

/**
 * mvneCancelPortIn
 *
 * Execute mwCancelPortIn only if there is a port in progress for the given $MSISDN
 *
 * @return object of class \Result
 */
function mvneCancelPortIn( $MSISDN , $transition_uuid=NULL , $action_uuid=NULL )
{
  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID(__FUNCTION__ . ' ' . time());

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $result = $mwControl->mwQuerySubscriber(
    array(
      'actionUUID' => $action_uuid,
      'msisdn'     => $MSISDN
    )
  );

  dlog('',"result      = %s",$result);
  dlog('',"result data = %s",$result->data_array);

  if ( ! $result->is_success() )
    return make_ok_Result( array() , NULL , 'QuerySubscriber failed' );

  if ( isset( $result->data_array['ResultCode'] ) )
    dlog('',"ResultCode  = %s",$result->data_array['ResultCode']);

  if ( isset( $result->data_array['ResultMsg'] ) )
    dlog('',"ResultMsg   = %s",$result->data_array['ResultMsg']);

  if ( isset( $result->data_array['body'] )
    && property_exists( $result->data_array['body'] , 'SubscriberStatus' )
    && ( $result->data_array['body']->SubscriberStatus == 'Active' )
  )
  {
    // return failure, but trigger activation ( we failed to acknowledge the successful completion of the port )

    $customer_id = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $MSISDN, 'customer_id');

    if ( $customer_id )
    {
      $result = internal_func_delayed_provisioning_or_activation( $customer_id );

      dlog('',"internal_func_delayed_provisioning_or_activation result = %s",$result);
    }
    else
      dlog('',"Could not find customer_id associated with MSISDN $MSISDN");

    return make_error_Result('Subscriber is Active');
  }
  elseif ( ( $result->data_array['ResultCode'] == '372' )
    && ( $result->data_array['ResultMsg']  == 'Subscriber is being ported in' )
  )
  {
    dlog('',"Subscriber is being ported in, we can proceed with mwCancelPortIn");

    return $mwControl->mwCancelPortIn(
      array(
        'actionUUID' => $action_uuid,
        'msisdn'     => $MSISDN
      )
    );
  }

  return make_ok_Result( array() , NULL , 'Subscriber is not being ported in, we could not proceed with mwCancelPortIn' );
}

/**
 * mvneEnsureCancel
 *
 * Execute appropriate middleware Ensure Cancel depending in customer's MVNE
 * Invokes mwMakeitsoDeactivate
 * NOOP if agent is 'Port Out Import Tool' in HTT_CANCELLATION_REASONS
 *
 * @return object of class \Result
 */
function mvneEnsureCancel( $MSISDN , $ICCID , $transition_uuid=NULL , $action_uuid=NULL , $customer_id=NULL )
{
  if ( ! $customer_id )
    $customer_id = \Ultra\Lib\DB\Getter\getScalar('MSISDN', normalize_msisdn($MSISDN, FALSE), 'customer_id');

  // NOOP if agent is 'Port Out Import Tool' in HTT_CANCELLATION_REASONS
  if ( is_cancellation_port_out( $customer_id ) )
    return make_ok_Result();

  $ICCID = luhnenize( $ICCID );

  {
    $MSISDN = normalize_msisdn($MSISDN, FALSE);

    if ( ! $action_uuid )
      $action_uuid = getNewActionUUID(__FUNCTION__ . ' ' . time());

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    // MVNO-2474: ensure that subscriber is Active
    $result = $mwControl->mwQuerySubscriber(array(
      'actionUUID' => $action_uuid,
      'msisdn'     => $MSISDN,
      'iccid'      => $ICCID));

    if ( $result->is_success()
      && ! empty($result->data_array['body']->SubscriberStatus)
      && in_array( $result->data_array['body']->SubscriberStatus , array( 'Active' , 'Suspended' ) )
    )
      // deactivate
      $result = $mwControl->mwMakeitsoDeactivate(
        array(
          'actionUUID'         => $action_uuid,
          'customer_id'        => $customer_id,
          'msisdn'             => $MSISDN,
          'iccid'              => $ICCID,
          'instantly'          => TRUE // attempt MakeitsoDeactivate immediately
        )
      );
    else
      // subscriber is not Active or Suspended failed to query: return existing result
      dlog('', "skipping deactivate MSISDN $MSISDN: %s", $result->data_array);

    // switch back to default DB
    teldata_change_db();
  }

  return $result;
}

/**
 * mvneEnsureSuspend
 *
 * Execute appropriate middleware Ensure Suspend depending in customer's MVNE
 *
 * @param string action UUID
 * @param integer customer ID
 * @return object of class Result
 * @author VYT, 2014-03-06
 */
function mvneEnsureSuspend( $actionUUID , $customer_id , $msisdn=NULL , $iccid=NULL )
{
  try
  {
    // check parameters
    if (empty($actionUUID))
      $actionUUID = getNewActionUUID('mvne ' . time());

    if (empty($customer_id))
      throw new Exception('ERR_API_INTERNAL: invalid parameters');

    // get customer object
    $customer = get_ultra_customer_from_customer_id($customer_id, array(
      'current_mobile_number',
      'current_iccid',
      'CURRENT_ICCID_FULL',
      'preferred_language',
      'BRAND_ID'
    ));

    $account = get_account_from_customer_id($customer_id, array('COS_ID'));

    if ( ! $customer || ! $account )
      throw new Exception('ERR_API_INTERNAL: cannot find customer ID');

    if (empty($customer->current_mobile_number))
      throw new Exception('ERR_API_INTERNAL: cannot find customer MSISDN');

    if (empty($customer->current_iccid) && empty($customer->CURRENT_ICCID_FULL))
      throw new Exception('ERR_API_INTERNAL: cannot find customer ICCID');

    // PROD-2048: retrieve current HTT_PLAN_TRACKER_ID and pass it to MISO
    // missing plan tracker would be a serious DB inconsistency error but is not fatal
    if ($plan_tracker = get_last_plan_tracker($customer_id))
      $plan_tracker_id = $plan_tracker->HTT_PLAN_TRACKER_ID;
    else
    {
      \logError("missing plan tracker for customer $customer_id");
      $plan_tracker_id = NULL;
    }

    // get customer info common for both MW calls
    if ( ! $msisdn )
      $msisdn = $customer->current_mobile_number;
    if ( ! $iccid )
      $iccid = empty($customer->CURRENT_ICCID_FULL) ? luhnenize($customer->current_iccid) : $customer->CURRENT_ICCID_FULL;

    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $params = [
        'actionUUID'         => $actionUUID,
        'customer_id'        => $customer_id,
        'msisdn'             => normalize_msisdn($msisdn, FALSE), // wants MSISDN-10
        'iccid'              => $iccid,
        'preferred_language' => $customer->preferred_language,
        'ultra_plan'         => get_plan_from_cos_id($account->COS_ID),
        'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer_id),
        'instantly'          => TRUE, // attempt MakeitsoSuspend immediately
        'plan_tracker_id'    => $plan_tracker_id
      ];

      $brandShortName = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
      switch ($brandShortName)
      {
        case 'MINT':
          $result = $mwControl->mwMakeitsoSuspendMintPlan($params);
          break;

        default:
          $result = $mwControl->mwMakeitsoSuspend($params);
          break;
      }

      // switch back to default DB
      teldata_change_db();
    }
  }
  catch(Exception $e)
  {
    $result = make_error_Result($e->getMessage());
  }

  return $result;
}

/**
 * mvneEnsureDeactivate
 *
 * Execute appropriate middleware Ensure Deactivate depending in customer's MVNE
 *
 * @param string action UUID
 * @param integer customer ID
 * @return object of class Result
 * @author VYT, 2014-03-12
 */
function mvneEnsureDeactivate( $actionUUID , $customer_id, $msisdn = NULL , $iccid = NULL )
{
  try
  {
    // check parameters
    if (empty($actionUUID))
      $actionUUID = getNewActionUUID('mvne ' . time());

    if (empty($customer_id))
      throw new Exception('ERR_API_INTERNAL: invalid parameters');

    // get customer object
    $customer = get_ultra_customer_from_customer_id($customer_id, [
      'current_mobile_number',
      'current_iccid',
      'CURRENT_ICCID_FULL'
    ]);

    if (! $customer)
      throw new Exception('ERR_API_INTERNAL: cannot find customer ID');

    if (empty($customer->current_mobile_number))
      throw new Exception('ERR_API_INTERNAL: cannot find customer MSISDN');

    if (empty($customer->current_iccid) && empty($customer->CURRENT_ICCID_FULL))
      throw new Exception('ERR_API_INTERNAL: cannot find customer ICCID');

    // get customer info common for both MW calls
    if ( ! $msisdn )
      $msisdn = $customer->current_mobile_number;
    if ( ! $iccid )
      $iccid = empty($customer->CURRENT_ICCID_FULL) ? luhnenize($customer->current_iccid) : $customer->CURRENT_ICCID_FULL;

    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwMakeitsoDeactivate(array(
        'actionUUID'         => $actionUUID,
        'customer_id'        => $customer_id,
        'msisdn'             => normalize_msisdn($msisdn, FALSE), // wants MSISDN-10
        'iccid'              => $iccid,
        'instantly'          => TRUE // attempt immediately
        )
      );

      // switch back to default DB
      teldata_change_db();
    }
  }
  catch(Exception $e)
  {
    $result = make_error_Result($e->getMessage());
  }

  return $result;
}

/**
 * mvneGet4gLTE
 *
 * Invokes CheckBalance to retrieve 4G LTE Data Usage
 *
 * @return array
 */
function mvneGet4gLTE( $actionUUID , $msisdn , $customer_id , $redis=NULL )
{
  $mvneError  = NULL;
  $remaining  = 0;
  $usage      = 0;
  
  $base        = 0;
  $base_used   = 0;
  $addon       = 0;
  $addon_used  = 0;
  $total3G     = 0;
  $used3G      = 0;
  $remaining3G = 0;
  
  $break_down = [];

  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis();

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  // invoke ACC API CheckBalance
  $mwResult = $mwControl->mwCheckBalance(
    array(
      'actionUUID' => $actionUUID,
      'msisdn'     => $msisdn
    )
  );

  if ( $mwResult->is_success()
    && isset($mwResult->data_array['body'])
    && property_exists( $mwResult->data_array['body'] , 'BalanceValueList' )
    && property_exists( $mwResult->data_array['body']->BalanceValueList , 'BalanceValue' )
    && is_array( $mwResult->data_array['body']->BalanceValueList->BalanceValue )
  )
  {
    dlog('',"BalanceValue = %s",$mwResult->data_array['body']->BalanceValueList->BalanceValue);

    foreach( $mwResult->data_array['body']->BalanceValueList->BalanceValue as $row )
    {
      $break_down[$row->Type][$row->UltraType] = $row->Value;

      if (in_array("{$row->Type}", ['WPRBLK33S','WPRBLK35S']))
      {
        if      ($row->Name == 'Used')  $base_used += $row->Value;
        else if ($row->Name == 'Limit') $base      += $row->Value;
        continue;
      }

      if (in_array("{$row->Type}", ['WPRBLK34S','WPRBLK36S']))
      {
        if      ($row->Name == 'Used')  $addon_used += $row->Value;
        else if ($row->Name == 'Limit') $addon      += $row->Value;
        continue;
      }

      if (in_array($row->UltraName, ['Data Trigger Total', 'Included Data Limit', 'Plan Included Data Total']) || $row->UltraType == 'Base Data Usage Limit')
        $base += $row->Value;

      // Mint Plan Data
      if (in_array($row->UltraType, ['Plan Data Limit']))
        $base += $row->Value;

      if ($row->UltraName == 'Data Add-on Total')
        $addon = $row->Value;

      // Mint Data Add-on
      if (in_array($row->UltraName, ['Mint Data Add-on Total']))
        $addon += $row->Value;

      if (in_array($row->UltraName, ['Data Trigger Included', 'Included Data Used', 'Plan Included Data Used']) || $row->UltraType == 'Base Data Usage Used')
        $base_used += $row->Value;

      // Mint Plan Data
      if (in_array($row->UltraType, ['Plan Data Used']))
        $base_used += $row->Value;

      if (in_array($row->UltraName, ['Data Add-on Used','Mint Data Add-on Used']))
        $addon_used = $row->Value;

      // Mint Data Add-on
      if (in_array($row->UltraName, ['Mint Data Add-on Used']))
        $addon_used = $row->Value;

      if (in_array($row->Type, ['WPRBLK39S'])) {
        if ($row->Name == 'Used') $used3G += $row->Value;
        else if ($row->Name == 'Limit') $total3G += $row->Value;
      }
    }

    //from mrc_throttle_updates
    $plan_used   = 0;
    $plan_limit  = 0;
    $block_used  = 0;
    $block_limit = 0;

    // get latest plan tracker row
    if ($plan_tracker = get_last_plan_tracker($customer_id))
    {
      // get history of throttle updates for cycle
      \Ultra\Lib\DB\ultra_acc_connect();
      $throttle_changes = mrcGetThrottleUpdatesByPlanTrackerId($plan_tracker->HTT_PLAN_TRACKER_ID);

      // add saved used values to _used
      for ($i = 0; $i < count($throttle_changes); $i++)
        if ($feature = \Ultra\MvneConfig\getThrottleSpeedFeatureFromType($throttle_changes[$i]->BUCKET))
        {
          ${$feature . '_used'}  += $throttle_changes[$i]->USED;
          ${$feature . '_limit'} += $throttle_changes[$i]->GRANTED;
        }

      // switch back to core db
      teldata_change_db();
    }

    // if base_used > base then subscriber went into throttle: report usage but discount for remaining
    // note that throttling does not apply to add-on data, the latter simply stops after reaching the limit
    $usage     = ($base_used + $plan_used) + ($addon_used + $block_used);
    $remaining = ($base - $base_used) + ($addon - $addon_used);
    if ($remaining < 0) {
      $remaining = 0;
    }
    
    // cache results
    if ( $usage || $remaining )
    {
      $ttl = 60*60*12; // 12 hours

      $redis->set( 'ultra/data/4g_lte/remaining/'.$customer_id , $remaining , $ttl );
      $redis->set( 'ultra/data/4g_lte/usage/'    .$customer_id , $usage     , $ttl );

      $mint_addon_percent_used = empty($addon) ? 0 : ceil( ( $addon_used * 100 ) / $addon );

      // valid only for Mint
      $redis->set( 'ultra/data/4g_lte/mint/addon/remaining/'   .$customer_id , ( $addon - $addon_used ) , $ttl );
      $redis->set( 'ultra/data/4g_lte/mint/addon/usage/'       .$customer_id , $addon_used              , $ttl );
      $redis->set( 'ultra/data/4g_lte/mint/addon/percent/used/'.$customer_id , $mint_addon_percent_used , $ttl );

      \logDebug("Mint addon remaining = ".( $addon - $addon_used ));
      \logDebug("Mint addon usage     = ".$addon_used);
    }
  }
  elseif( $mwResult->is_failure() )
  {
    // something went wrong at MW level
    $errors = $mwResult->get_errors();
    $mvneError = $errors[0];
  }
  elseif( $mwResult->is_success()
       && ! empty($mwResult->data_array)
       && ! empty($mwResult->data_array['ResultMsg'])
       && ! empty($mwResult->data_array['ResultCode'])
       && ( $mwResult->data_array['ResultCode'] != ACC_SUCCESS_RESULTCODE )
  )
  {
    // something went wrong at ACC/TMO level
    $mvneError = $mwResult->data_array['ResultCode'] . ' - ' . $mwResult->data_array['ResultMsg'] ;
  }

  return array( $remaining , $usage , ( $addon - $addon_used ) , $addon_used , $break_down, $mvneError, ['total3G' => $total3G, 'used3G' => $used3G, 'remaining3G' => $total3G - $used3G]);
}

/**
 * mvneGetVoiceMinutes
 *
 * returns voice minutes from mwCheckBalance
 * checks redis first for cached value
 *
 * @param  Integer $customer_id
 * @return Integer $voice_minutes
 */
function mvneGetVoiceMinutes($customer_id)
{
  $redis = new \Ultra\Lib\Util\Redis;

  $voice_minutes = $redis->get('ultra/middleware/check_voice_minutes/' . $customer_id);
  if ( $voice_minutes === NULL )
  {
    $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number'));

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $mwResult = $mwControl->mwCheckBalance(array(
      'actionUUID' => getNewActionUUID('dealerportal ' . time()),
      'msisdn' => $customer->current_mobile_number)
    );

    if ( $mwResult->is_success()
      && isset($mwResult->data_array['body'])
      && property_exists( $mwResult->data_array['body'] , 'BalanceValueList' )
      && property_exists( $mwResult->data_array['body']->BalanceValueList , 'BalanceValue' )
      && is_array( $mwResult->data_array['body']->BalanceValueList->BalanceValue )
    )
    {
      foreach( $mwResult->data_array['body']->BalanceValueList->BalanceValue as $row )
      {
        if ( $row->Type == 'DA1' )
        {
          $voice_minutes = $row->Value;
          $redis->setnx('ultra/middleware/check_voice_minutes/' . $customer_id, $voice_minutes, 5 * 60);
        }
      }
    }

    teldata_change_db();
  }

  dlog('', 'Voice minutes for customer_id: %d , %d', $customer_id, $voice_minutes);
  return $voice_minutes;
}

/**
 * mvneCanActivate
 * validate that given ICCID can be activated on MVNE
 * @param String ICCID
 * @return Boolean TRUE if can be activated, FALSE otherwise
 */
function mvneCanActivate($iccid)
{
  // check cache first
  $iccid = luhnenize($iccid);
  $redis = new \Ultra\Lib\Util\Redis;
  if ($redis->get("iccid/not_good_to_activate/$iccid"))
    return FALSE;
  if ($redis->get("iccid/good_to_activate/$iccid"))
    return TRUE;

  // development SIMs are always good to 'activate'
  if (override_test_iccid($iccid))
    return TRUE;

  // validate on MVNE (which will automatically cache result)
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  $result = $mwControl->mwCanActivate(array(
    'iccid'=> $iccid,
    'actionUUID' => getNewActionUUID(__FUNCTION__ . time())));

  return $result->is_success() && ! empty($result->data_array['available']) && empty($result->data_array['errors']);
}

/**
 * mvneCanPortIn
 * validate that MSISDN can be ported in
 * @param String MSISDN
 * @return Boolean TRUE if portable, FALSE otherwise
 */
function mvneCanPortIn($msisdn)
{
  $msisdn = normalize_msisdn($msisdn);

  // verify port in eligivility on MVNE
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  $mwResult = $mwControl->mwPortInEligibility(array(
    'actionUUID' => getNewActionUUID(__FUNCTION__ . time()),
    'msisdn'     => $msisdn));

  // check MVNE results
  if ($mwResult->get_errors())
    return logError($mwResult->get_errors());
  if ( ! $mwResult->get_data_key('eligible'))
    return logInfo("MSISDN $msisdn is not eligible to port in - " . $mwResult->get_data_key('failure_reason'));

  // success
  return TRUE;
}

/**
 * mvneSetVoicemailLanguage
 *
 * change VM SOC on ACC
 * WARNING: this function connects to ULTRA_ACC, you may need to restore connection to ULTRA_DATA upon return
 * @param  object  customer
 * @param  string  language EN | ES
 * @return boolean TRUE on success, FALSE on failure
 */
function mvneSetVoicemailLanguage($customer, $language = 'EN')
{
  // check prerequisites
  if (empty($customer) || (empty($customer->CURRENT_ICCID) && empty($customer->CURRENT_ICCID_FULL)) || empty($customer->current_mobile_number) || empty($customer->COS_ID) || empty($customer->preferred_language))
  {
    dlog('', 'ERROR: missing required values in customer object');
    return FALSE;
  }

  // check state
  if (empty($customer->plan_state) || $customer->plan_state != 'Active')
  {
    dlog('', 'ERROR: customer is not Active');
    return FALSE;
  }

  // instantiate MW class
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  if ( ! $languageSOC = \Ultra\UltraConfig\mapLanguageToVoicemailSoc($language))
  {
    dlog('', 'ERROR: failed to determinte voicemail language');
    return FALSE;
  }

  // there is currently no mandarin voicemail option
  if ( is_mandarin( $language ) )
    $language = 'EN';

  // get voicemail values and call MW
  list($accSocsDefinition, $accSocsDefinitionByUltraSoc, $ultraSocsByPlanId) = \Ultra\MvneConfig\getAccSocsDefinitions();
  $result = $mwControl->mwMakeitsoUpgradePlan(
    array(
      'actionUUID'         => getNewActionUUID(__FUNCTION__ . time()),
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL ? $customer->CURRENT_ICCID_FULL : $customer->CURRENT_ICCID,
      'customer_id'        => $customer->CUSTOMER_ID,
      'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
      'ultra_plan'         => get_plan_from_cos_id($customer->COS_ID),
      'preferred_language' => $customer->preferred_language,
      'option'             => $accSocsDefinitionByUltraSoc[$languageSOC]['ultra_service_name']));

  // check results
  if ($result->is_failure())
  {
    dlog('',"ERROR: %s", $result->get_errors());
    return FALSE;
  }

  return TRUE;
}

/**
 * mvneIntraPort
 *
 * queue up MakeItSo MakeitsoIntraPort and return
 * @param Integer customer ID
 * @param String porting MSISDN
 * @param String old ICCID to deactivate
 * @param String new ICCID to activate
 * @param String plan to activate on
 * @param String subscriber's preferred language
 * @param String final plan state
 * @return Array
 */
function mvneIntraPort($customer_id, $msisdn, $old_iccid, $new_iccid, $plan, $preferred_language, $state, $account, $old_brand_id)
{
  // enqueue a MakeItSo item and return
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  $result = $mwControl->mwMakeitsoIntraPort(array(
    'action_uuid'         => getNewActionUUID('mvne ' . time()),
    'customer_id'         => $customer_id,
    'msisdn'              => normalize_msisdn($msisdn),
    'old_iccid'           => luhnenize($old_iccid),
    'new_iccid'           => luhnenize($new_iccid),
    'ultra_plan'          => $plan,
    'preferred_language'  => $preferred_language,
    'wholesale_plan'      => \Ultra\UltraConfig\getUltraPlanConfigurationItem($plan, 'wholesale'),
    'state'               => $state)
  );
  if ($result->is_failure())
    array('success' => FALSE, 'errors' => $result->get_errors());

  // add to PORTIN_QUEUE
  $portInQueue = new \PortInQueue();
  $result = $portInQueue->initiate(array(
    'customer_id'           => $customer_id,
    'msisdn'                => $msisdn,
    'last_acc_api'          => __FUNCTION__,
    'account_number'        => $account,
    'port_request_id'       => $result->data_array['makeitso_queue_id'],
    'old_service_provider'  => \Ultra\UltraConfig\getShortNameFromBrandId($old_brand_id)
  ));

  teldata_change_db();
  return array('success' => $result->is_success(), 'errors' => $result->get_errors());
}

/**
 * mvneMakeitsoRenewEndMintPlan
 *
 * Invokes mwMakeitsoRenewEndMintPlan
 *
 * @return array
 */
function mvneMakeitsoRenewEndMintPlan($customer_id, $plan, $transition_uuid = NULL, $action_uuid = NULL)
{
  $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language'));

  if ( ! $customer )
    return array( 'success' => FALSE , 'errors' => array('Could not load data for customer id '.$customer_id.' in mvneMakeitsoRenewEndMintPlan') );

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $preferred_language = ( ! $customer->preferred_language ) ? 'EN' : $customer->preferred_language ;

  // missing plan tracker or first period means a new activation, therefore we do not need to log usage
  $plan_tracker = get_last_plan_tracker($customer_id);
  $plan_tracker_id = $plan_tracker && $plan_tracker->period > 1 ? $plan_tracker->HTT_PLAN_TRACKER_ID : NULL;

  $result = $mwControl->mwMakeitsoRenewEndMintPlan(
    array(
      'actionUUID'         => $action_uuid,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => get_base_plan_name($plan),
      'wholesale_plan'     => $planConfiguration['wholesale'],
      'preferred_language' => $preferred_language,
      'customer_id'        => $customer_id,
      'plan_tracker_id'    => $plan_tracker_id,
      'subplan'            => NULL
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}

/**
 * mvneMakeitsoRenewMidMintPlan
 *
 * Invokes mwMakeitsoRenewMidMintPlan
 *
 * @return array
 */
function mvneMakeitsoRenewMidMintPlan($customer_id, $plan, $transition_uuid = NULL, $action_uuid = NULL)
{
  $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language'));

  if ( ! $customer )
    return array( 'success' => FALSE , 'errors' => array('Could not load data for customer id '.$customer_id.' in mvneMakeitsoRenewMidMintPlan') );

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $preferred_language = ( ! $customer->preferred_language ) ? 'EN' : $customer->preferred_language ;

  // missing plan tracker or first period means a new activation, therefore we do not need to log usage
  $plan_tracker = get_last_plan_tracker($customer_id);
  $plan_tracker_id = $plan_tracker && $plan_tracker->period > 1 ? $plan_tracker->HTT_PLAN_TRACKER_ID : NULL;

  $result = $mwControl->mwMakeitsoRenewMidMintPlan(
    array(
      'actionUUID'         => $action_uuid,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => get_base_plan_name($plan),
      'wholesale_plan'     => $planConfiguration['wholesale'],
      'preferred_language' => $preferred_language,
      'customer_id'        => $customer_id,
      'plan_tracker_id'    => $plan_tracker_id,
      'subplan'            => NULL
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}

/**
 * mvneMakeitsoRenewMintPlan
 *
 * Invokes mwMakeitsoRenewPlan
 *
 * @return array
 */
function mvneMakeitsoRenewMintPlan($customer_id, $plan, $transition_uuid = NULL, $action_uuid = NULL)
{
  $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language'));

  if ( ! $customer )
    return array( 'success' => FALSE , 'errors' => array('Could not load data for customer id '.$customer_id.' in mvneMakeitsoRenewMintPlan') );

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $preferred_language = ( ! $customer->preferred_language ) ? 'EN' : $customer->preferred_language ;

  // PROD-2048: retrieve current HTT_PLAN_TRACKER_ID and pass it to MISO
  // missing plan tracker or first period means a new activation, therefore we do not need to log usage
  $plan_tracker = get_last_plan_tracker($customer_id);
  $plan_tracker_id = $plan_tracker && $plan_tracker->period > 1 ? $plan_tracker->HTT_PLAN_TRACKER_ID : NULL;

  $result = $mwControl->mwMakeitsoRenewMintPlan(
    array(
      'actionUUID'         => $action_uuid,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => get_base_plan_name($plan),
      'wholesale_plan'     => $planConfiguration['wholesale'], // DATAQ-29: wholesale plan may change
      'preferred_language' => $preferred_language,
      'customer_id'        => $customer_id,
      'plan_tracker_id'    => $plan_tracker_id,
      'subplan'            => NULL
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}

/**
 * mvneMakeitsoUpdateThrottleSpeed
 *
 * Invokes mwMakeitsoUpdateThrottleSpeed
 *
 * @return bool|array
 */
function mvneMakeitsoUpdateThrottleSpeed($customer_id, $plan, $throttle_speed, $lte_select_method, $transition_uuid = NULL, $action_uuid = NULL, $migratingFromMrc = false)
{
  $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language'));

  if ( ! $customer )
    return array( 'success' => FALSE , 'errors' => array('Could not load data for customer id '.$customer_id.' in mvneMakeitsoRenewEndMintPlan') );

  if ( ! $action_uuid )
    $action_uuid = getNewActionUUID('mvne ' . time());

  $planConfiguration = \Ultra\UltraConfig\getUltraPlanConfiguration(get_plan_name_from_short_name($plan));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $preferred_language = ( ! $customer->preferred_language ) ? 'EN' : $customer->preferred_language ;

  // missing plan tracker or first period means a new activation, therefore we do not need to log usage
  $plan_tracker    = get_last_plan_tracker($customer_id);
  $plan_tracker_id = $plan_tracker->HTT_PLAN_TRACKER_ID;

  $balance_values = mvneGet4gLTE($action_uuid, $customer->current_mobile_number, $customer_id);
  $balance_values = json_encode(\Ultra\MvneConfig\throttleValuesFromBalanceValues($balance_values[4]));

  $result = $mwControl->mwMakeitsoUpdateThrottleSpeed(
    array(
      'actionUUID'         => $action_uuid,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => get_base_plan_name($plan),
      'wholesale_plan'     => $planConfiguration['wholesale'],
      'preferred_language' => $preferred_language,
      'customer_id'        => $customer_id,
      'throttle_speed'     => $throttle_speed,
      'lte_select_method'  => $lte_select_method,
      'balance_values'     => $balance_values,
      'plan_tracker_id'    => $plan_tracker_id,
      'subplan'            => NULL,
      'migratingFromMrc'   => $migratingFromMrc
    )
  );

  // connect back to the default DB
  teldata_change_db();

  return array( 'success' => $result->is_success() , 'errors' => $result->get_errors() );
}