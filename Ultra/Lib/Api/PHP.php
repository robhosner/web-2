<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for PHP code stubs
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class PHP extends Base
{
  /**
   * getPresentation
   * 
   * Returns PHP stub.
   * I simply copied Ted's code ( function make_php )
   * 
   * @return string
   */
  public function getPresentation ()
  {
    header("content-type: text/plain");

    $code = '<?php
';

    foreach ($this->apiDefinition['commands'] as $command => $def)
    {
     if ( is_null( $this->command ) || !$this->command || ( $this->command == '' ) || ( $this->command == $command ) )
     {
      $output_params = array();
      $input_params  = array();

      foreach ( $def['parameters'] as $defParam )
      {
        if ( $defParam['name'] != 'partner_tag' )
          $input_params[] = '$'.$defParam['name'];
      }

      foreach ( $def['returns'] as $defReturns )
      {
        if ( ( $defReturns['name'] != 'success' )
          && ( $defReturns['name'] != 'errors' )
          && ( $defReturns['name'] != 'user_errors' )
          && ( $defReturns['name'] != 'error_codes' )
          && ( $defReturns['name'] != 'warnings' )
          && ( $defReturns['name'] != 'partner_tag' )
          && ( $defReturns['name'] != 'ultra_trans_epoch' )
        ) $output_params[] = '$this->addToOutput(\''.$defReturns['name']."','');";
      }

      $class_namespace = ucfirst(strtok($command,"_"));
      $class_name = substr($command, strlen($class_namespace)+2);
      $code = $code . sprintf("
namespace Ultra\Lib\Api\Partner\\{$class_namespace};

require_once 'Ultra/Lib/Api/Partner/{$class_namespace}.php';
  

class {$class_name} extends \Ultra\Lib\Api\Partner\\{$class_namespace}
{
  /**
  * %s
  *
  * %s
  *
  * @return Result object
  */
  public function %s ()
  {
    list ( %s ) = \$this->getInputValues();

    %s

    try
    {

      \$this->succeed ();
    }
    catch( \Exception \$e )
    {
      dlog( '' , \$e->getMessage () );
    }

    return \$this->result;
  }
}

      ",
        $command,
        $def['desc'],
        $command,
        implode(' , ',$input_params),
        implode("\n    ",$output_params)
      );
     }
    }

   

    return $code;
  }
}
