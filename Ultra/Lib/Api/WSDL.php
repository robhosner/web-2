<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for WSDL output
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class WSDL extends Base
{

  public function isPresentation ()
  {
    return TRUE;
  }

  /**
   * getPresentation
   *
   * Returns WSDL definition
   * I simply copied Ted's code ( function make_wsdl )
   *
   * @return string
   */
  public function getPresentation ()
  {
    header("content-type: text/xml");
    #return make_wsdl($site, $partner, $e_config, $p);

    $namespace = "urn:httapi".$this->partner;

    $type_map = array(
      'int[]'    => 'ArrayOf_xsd_int',
      'real[]'    => 'ArrayOf_xsd_float',
      'string[]' => 'ArrayOf_soapenc_string',
      'string'   => 'soapenc:string',
      'epoch'    => 'xsd:int',
      'integer'  => 'xsd:int',
      'real'     => 'xsd:float',
      'boolean'  => 'xsd:boolean',
    );

    $types = sprintf('
<wsdl:types>
  <schema targetNamespace="%s" xmlns="http://www.w3.org/2001/XMLSchema">
   <import namespace="http://schemas.xmlsoap.org/soap/encoding/"/>
   <complexType name="ArrayOf_soapenc_string">
    <complexContent>
     <restriction base="soapenc:Array">
      <attribute ref="soapenc:arrayType" wsdl:arrayType="soapenc:string[]"/>
     </restriction>
    </complexContent>
   </complexType>
   <complexType name="ArrayOf_xsd_int">
    <complexContent>
     <restriction base="soapenc:Array">
      <attribute ref="soapenc:arrayType" wsdl:arrayType="xsd:int[]"/>
     </restriction>
    </complexContent>
   </complexType>
   <complexType name="ArrayOf_xsd_float">
    <complexContent>
     <restriction base="soapenc:Array">
      <attribute ref="soapenc:arrayType" wsdl:arrayType="xsd:float[]"/>
     </restriction>
    </complexContent>
   </complexType>
  </schema>
 </wsdl:types>
', $namespace);

    $messages     = '';
    $bindings_ops = '';
    $port_ops     = '';

    foreach ($this->apiDefinition['commands'] as $command => $definition)
    {
      $parameters = $definition['parameters'];

      $porder  = '';
      $pdetail = '';

      foreach ($parameters as $pdef)
      {
        $pname = $pdef['name'];

        $porder = $porder . " $pname";

        if (isset($type_map[$pdef['type']]))
        {
          $pdetail = $pdetail . sprintf('
<wsdl:part name="%s" type="%s"/>
', $pname, $type_map[$pdef['type']]);
        }
        else
        {
          return "Unknown type for parameter $pname: $pdef[type]";
        }
      }

      $messages = $messages . sprintf('
   <wsdl:message name="%s_Response">
      <wsdl:part name="%s_Return" type="soapenc:string"/>
   </wsdl:message>

   <wsdl:message name="%s_Request">
%s
   </wsdl:message>
', $command, $command, $command, $pdetail);

      $port_ops = $port_ops . sprintf('
      <wsdl:operation name="%s" parameterOrder="%s">
         <wsdl:input message="impl:%s_Request" name="%s_Request"/>
         <wsdl:output message="impl:%s_Response" name="%s_Response"/>
      </wsdl:operation>
', $command, $porder, $command, $command, $command, $command);

      $bindings_ops = $bindings_ops . sprintf('
      <wsdl:operation name="%s">
         <wsdlsoap:operation soapAction=""/>
         <wsdl:input name="%s_Request">
            <wsdlsoap:body encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="%s" use="encoded"/>
         </wsdl:input>
         <wsdl:output name="%s_Response">
            <wsdlsoap:body encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="%s" use="encoded"/>
         </wsdl:output>
      </wsdl:operation>
', $command, $command, $namespace, $command, $namespace);
    }

    $bindings = sprintf('
   <wsdl:binding name="apiSoapBinding" type="impl:%s">
      <wsdlsoap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
%s
   </wsdl:binding>
', $this->partner, $bindings_ops);

    $ports = sprintf('
   <wsdl:portType name="%s">
%s
   </wsdl:portType>', $this->partner, $port_ops);

    return sprintf(
'<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions targetNamespace="urn:httapi%s" xmlns:apachesoap="http://xml.apache.org/xml-soap" xmlns:impl="urn:httapi%s" xmlns:intf="urn:httapi%s" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:wsdlsoap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:xsd="http://www.w3.org/2001/XMLSchema">

%s

%s

%s

%s

   <wsdl:service name="%sService">
      <wsdl:port binding="impl:apiSoapBinding" name="api">
         <wsdlsoap:address location="https://%s/ps/%s/%s/ultra/api"/>
      </wsdl:port>
   </wsdl:service>
</wsdl:definitions>', $this->partner, $this->partner, $this->partner, $types, $messages, $ports, $bindings, $this->partner, $this->site, $this->partner, $this->apiDefinition['meta']['version']);
  }

  public function output($response_array)
  {
    /*
    Array
    (
        [errors] => Array
            (
                [0] => ERR_API_INTERNAL: Version validation failed.
            )

        [warnings] => Array
            (
            )

        [success] => false
        [user_errors] => Array
            (
                [0] => The version specified is incorrect
            )

        [error_codes] => Array
            (
                [0] => FA0003
            )

      )
    */
    $xml = new \SimpleXMLElement('<response/>');
    $errors = $xml->addChild('errors');
    foreach ($response_array['errors'] as $key => $error)
    {
      $errors->addChild('error', $error);
    }

    $warnings = $xml->addChild('warnings');
    foreach ($response_array['warnings'] as $key => $warning)
    {
      $warnings->addChild('warning', $warning);
    }

    $success = $xml->addChild('success', $response_array['success']);

    $user_errors = $xml->addChild('user_errors');
    foreach ($response_array['user_errors'] as $key => $user_error)
    {
      $user_errors->addChild('user_error', $user_error);
    }

    $error_codes = $xml->addChild('error_codes');
    foreach ($response_array['error_codes'] as $key => $error_code)
    {
      $error_codes->addChild('error_code', $error_code);
    }

    return $xml->asXML();
  }
}

?>
