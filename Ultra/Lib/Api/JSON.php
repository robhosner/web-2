<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for JSON output
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class JSON extends Base
{
  /**
   * Disable Presentation mode
   */
  public function isPresentation ()
  {
    return FALSE;
  }

  /**
   * JSON output
   */
  public function output ( $apiExecutionResult )
  {
    return json_encode( $apiExecutionResult );
  }
}

?>
