<?php

namespace Ultra\Lib\Api;

/**
 * Base Ultra API class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Base
{
  protected $site;
  protected $partner;
  protected $e_config;
  protected $apiDefinition;
  protected $jsonpCallback;
  protected $command;
  protected $metaDirectory;

  /**
   * Class constructor
   * 
   * Sets up protected object members
   */
  public function __construct ( $site , $partner , $e_config , $apiDefinition , $jsonpCallback , $command, $metaDirectory )
  {
    $this->site          = $site;
    $this->partner       = $partner;
    $this->e_config      = $e_config;
    $this->apiDefinition = $apiDefinition;
    $this->jsonpCallback = $jsonpCallback;
    $this->command       = $command;
    $this->metaDirectory = $metaDirectory;
  }

  /**
   * isPresentation
   * 
   * Returns FALSE if this is actually not an API call, but we want to display some info about the given partner APIs.
   * Returns TRUE if this is an actual API call we need to execute.
   * 
   * @return boolean
   */
  public function isPresentation ()
  {
    return TRUE;
  }
}

?>
