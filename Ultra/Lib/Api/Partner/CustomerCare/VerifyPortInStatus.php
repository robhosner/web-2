<?php
namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Lib\Api\Partner\CustomerCare;

class VerifyPortInStatus extends CustomerCare
{
  public function customercare__VerifyPortInStatus()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      // connect to the DB
      teldata_change_db();

      $select_fields = array(
        'a.CUSTOMER_ID',
        'a.COS_ID',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'stored_value',
        'BALANCE',
        'a.COS_ID',
        'c.POSTAL_CODE',
        "CASE WHEN CREATION_DATE_TIME IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', CREATION_DATE_TIME ) END created_date",
        "( Datediff(ss, '1970-01-01', GETUTCDATE()) - Datediff(ss, '1970-01-01', a.CREATION_DATE_TIME) ) CREATED_SECONDS_AGO",
        "u.BRAND_ID",
        'c.E_MAIL'
      );

      // get customer data
      $customer = get_customer_from_customer_id( $customer_id , $select_fields );

      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      $redis = new \Ultra\Lib\Util\Redis;
      $request_id = $redis->get( 'ultra/port/request_id/' . $customer_id);

      if (!$request_id) {
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no port-in request id found for customer $customer_id." , 'PR0001', "No port-in request id found for customer $customer_id.");
      }

      // get transition and check result
      $transition_result = provision_check_transition($request_id);
      $errors = append_transition_failure_reason($transition_result['errors'], $request_id);
      if ($errors && is_array($errors) && count($errors))
      {
        $this->addToOutput('port_pending', FALSE);
        $this->addToOutput('port_success', FALSE);
        $this->addToOutput('port_status', 'ERROR');
        $this->addToOutput('port_resolution', $errors);
        $this->errException('ERR_API_INTERNAL: provisioning errors - ' . implode('; ', $errors), 'PR0001');
      }
      else
      {
        // get data from PORTIN_QUEUE
        $portInQueue = new \PortInQueue();
        $loadByCustomerIdResult = $portInQueue->loadByCustomerId( $transition_result['port_attempt_customer_id'] );
        if ( $loadByCustomerIdResult->is_success() )
        {
          $this->addToOutput('phone_number',$portInQueue->msisdn);
          list(
            $port_success,
            $port_pending,
            $port_status,
            $port_resolution
            ) = interpret_port_status( $portInQueue, $transition_result );

          $this->addToOutput('port_success',    $port_success);
          $this->addToOutput('port_pending',    $port_pending);
          $this->addToOutput('port_status',     $port_status);

          if ( $port_resolution )
            $this->addToOutput('port_resolution', array($port_resolution));
        }
        else
        {
          // the port request is not yet recorded in PORTIN_QUEUE
          $this->addToOutput('port_success', FALSE);
          $this->addToOutput('port_pending', TRUE);
          $this->addToOutput('port_status', 'initialized');
        }

        teldata_change_db();
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
