<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class CancelPortIn extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__CancelPortIn
   *
   * Cancels a Port Request. (then does an API QueryPortIn to send the customer to Port-In Denied.).
   *
   * @param integer customer_id
   * @return object Result
   */
  public function customercare__CancelPortIn()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_customer_from_customer_id($customer_id);
      if (!$customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // check if intra-brand port, cannot cancel
      $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);
      if ($sim && $customer->BRAND_ID != $sim->BRAND_ID)
        $this->errException('ERROR_API_INTERNAL: The porting attempt cannot be cancelled', 'PO0004');

      $state = internal_func_get_state_from_customer_id($customer_id);

      if (!in_array($state['state'], array(STATE_NEUTRAL, STATE_PORT_IN_DENIED, STATE_PORT_IN_REQUESTED)))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer must be in Port-In Denied state', 'IN0001');

      if (!$customer->current_mobile_number)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no MSISDN found', 'NS0001');

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwCancelPortIn(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn'     => $customer->current_mobile_number
        )
      );

      dlog('',"mwCancelPortIn result = %s", $result);

      $this->addWarnings($result->get_warnings());

      teldata_change_db();

      if (isset($result->data_array['errors'])
        && is_array($result->data_array['errors'])
        && count($result->data_array['errors']))
      {
        $this->errException('ERR_API_INTERNAL: ' . $result->data_array['errors'][0], 'MW0001');
      }

      if (!$result->is_success())
      {
        if ( isset($result->data_array['result'])
          && ( $result->data_array['result'] == 'ERROR' )
          && isset($result->data_array['message'])
          && $result->data_array['message']
        )
        {
          $this->errException('ERR_API_INTERNAL: ' . $result->data_array['message'], 'MW0001');
        }
      }
      else
      {
        // transition the customer to 'Cancelled'

        $context = array('customer_id' => $customer->customer_id);

        $result_status = cancel_account(
          $customer,
          $context,
          NULL,
          'customercare__CancelPortIn',
          'CancelPortIn called',
          'customercare API'
        );

        if (count($result_status['errors']))
          $this->errException('ERR_API_INTERNAL: state transition error' , 'SM0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
