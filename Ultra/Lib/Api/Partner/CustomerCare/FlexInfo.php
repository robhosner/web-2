<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';
require_once 'classes/Flex.php';
require_once 'Ultra/Lib/Services/SharedData.php';
require_once 'Ultra/Lib/Services/SharedILD.php';

class FlexInfo extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__FlexInfo
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__FlexInfo()
  {
    list ($customer_id) = $this->getInputValues();

      $this->addToOutput('parentCustomerId', null);
      $this->addToOutput('payForFamily', null);
      $this->addToOutput('planCredits', null);
      $this->addToOutput('memberCount', null);
      $this->addToOutput('ban', null);
      $this->addToOutput('shared_intl', null);
      $this->addToOutput('parent_msisdn', null);

    try
    {
      // get basic info
      $info = \Ultra\Lib\Flex::getInfoByCustomerId($customer_id);

      if ( ! $info)
        $this->errException('ERR_API_INTERNAL: error retrieving flex info', 'DB0001');

      if ($info)
        foreach ($info as $key => $val)
          $this->addToOutput($key, $val);

      // get ban
      $sharedData = new \Ultra\Lib\Services\SharedData();
      $result = $sharedData->getBucketIDByCustomerID($customer_id);

      if ($result->is_success() && isset($result->data_array['bucket_id']))
        $result = $sharedData->getBucketByBucketID($result->data_array['bucket_id']);

      if ($result->is_success())
        $this->addToOutput('ban', $result->data_array['ban']);

      // get shared ild
      $sharedILD = new \Ultra\Lib\Services\SharedILD();
      $result = $sharedILD->getBucketIDByCustomerID($info['parentCustomerId']);
      if ($result->is_success())
      {
        $bucketId = $result->data_array['bucket_id'];
        $account  = find_first("SELECT TOP 1 CUSTOMER_ID,PACKAGED_BALANCE1 FROM ACCOUNTS with (nolock) WHERE ACCOUNT_ID = $bucketId");
        if ($account)
          $this->addToOutput('shared_intl', $account->PACKAGED_BALANCE1);
      }

      // get parent msisdn
      $customer = get_ultra_customer_from_customer_id($info['parentCustomerId'], ['current_mobile_number']);
      if ($customer)
        $this->addToOutput('parent_msisdn', $customer->current_mobile_number);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
