<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class GetBoltOnHistoryByCustomerId extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__GetBoltOnHistoryByCustomerId
   * retrive bolt on history of the given customer
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Bolt+Ons+APIs
   * @param integer customer_id
   * @return object Result
   */
  public function customercare__GetBoltOnHistoryByCustomerId()
  {
    // initialize
    list ($customer_id) = $this->getInputValues();

    try
    {
      // get customer record
      teldata_change_db();
      if ( ! $customer = get_ultra_customer_from_customer_id( $customer_id, array('customer_id') ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // get Bolt On history
      $query = \Ultra\Lib\DB\makeSelectQuery(
          'ULTRA.BOLTON_TRACKER',
          NULL,
          NULL,
          array('customer_id' => $customer_id),
          NULL,
          'BOLTON_TRACKER_ID DESC',
          TRUE);
      $history = mssql_fetch_all_objects(logged_mssql_query($query));
      $this->addToOutput('bolt_on_history', $history);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }
}
