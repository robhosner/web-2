<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

use \Ultra\Lib\Api\Partner\CustomerCare;

/**
 * Class UnblockPortalLogin
 * @package Ultra\Lib\Api\Partner\CustomerCare
 */
class UnblockPortalLogin extends CustomerCare
{

  /**
   * customercare__UnblockPortalLogin
   *
   * Returns the history of the SIM inventory and provisioning.
   *
   * @param string|int customer_id
   * @param string agent_name
   * @param int customercare_event_id
   * @return object Result
   */
  public function customercare__UnblockPortalLogin()
  {
    // initialize
    list ($customer_id, $agent_name, $customercare_event_id) = $this->getInputValues();

    try
    {
      $api = 'portal__Login';
      $skipInDev = !\Ultra\UltraConfig\isDevelopmentDB();

      if (checkApiAbuseByIdentifier($api, $customer_id, 6, $this->getRedis(), false, false, $skipInDev))
      {

        if (is_numeric($customer_id))
        {
          $this->removeBlockedUserByMsisdn($customer_id, $api, $agent_name, $customercare_event_id);
        }
        else
        {
          $this->removeBlockedUserByUsername($customer_id, $api, $agent_name, $customercare_event_id);
        }

        $this->removeBlockedUser($customer_id, $api, $agent_name, $customercare_event_id);
      }

      // success
      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
