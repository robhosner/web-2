<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class RunFlexSetup extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__RunFlexSetup
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__RunFlexSetup()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $account = \get_account_from_customer_id($customer_id, ['cos_id']);

      if ( ! $account)
        throw new \Exception('ERR_API_INTERNAL: error pulling account info', 'DB0001');

      if ( ! \Ultra\Lib\Flex::isFlexPlan($account->cos_id))
        throw new \Exception('ERR_API_INTERNAL: not a flex customer', 'IN0002');

      $actionUUID = getNewActionUUID(__FUNCTION__ . ' ' . time());

      $m = new \Ultra\Lib\MVNE\MakeItSo();
      $result = $m->enqueueFlexSetup($actionUUID, $customer_id);
      if ( ! $result->is_success())
      {
        $errors = $result->get_errors();
        $error  = (count($errors)) ? $errors[0] : 'Unknown error';
        throw new \Exception($error);
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
