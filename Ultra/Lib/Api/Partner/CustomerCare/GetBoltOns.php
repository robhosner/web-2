<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class GetBoltOns extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__GetBoltOns
   *
   * Get all allowed Bolt Ons
   *
   * @return Result object
   */
 
  public function customercare__GetBoltOns ()
  {
    // currently ignored
    list($customer_id, $msisdn, $mode) = $this->getInputValues();
    $bolt_on_data = NULL;
    $customer = NULL;
    $balance = NULL;
    $bolt_ons = NULL;

    try
    {
      // BOLT-34: include subscriber balance
      teldata_change_db();
      if ($customer_id)
      {
        if ( ! $customer = get_account_from_customer_id($customer_id, array('BALANCE','COS_ID')))
          $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }
      elseif ($msisdn)
      {
        if ( ! $customer = get_customer_from_msisdn($msisdn, 'a.BALANCE, a.COS_ID'))
          $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      // get customer specific bolt ons
      if ($customer)
      {
        $balance = $customer->BALANCE;
        $plan = get_plan_name_from_short_name(get_plan_from_cos_id($customer->COS_ID));
        $bolt_on_data = \Ultra\UltraConfig\getBoltOnInfoByPlan($plan);
      }
      // no CustomerID or MSISDN: old behavior, get all bolt ons
      else
        $bolt_on_data = \Ultra\UltraConfig\getBoltOnsMappingByType();

      foreach ($bolt_on_data as $type => &$boltons)
      {
        $newData = [];
        for ($i = 0; $i < count($boltons); $i++)
        {
          if ( ! in_array($boltons[$i]['id'], ['SHAREDDATA_250_0']))
            $newData[] = $boltons[$i];
        }
        $boltons = $newData;
      }

      // BOLT-8: 3ci cannot parse indexed arrays, all members must have keys starting with 1
      if ($mode == '3ci')
      {
        if (count($bolt_on_data))
        {
          $data = array();
          foreach ($bolt_on_data as $type => $boltons) // IDDCA => [...], DATA => [...]
          {
            for ($i = 0, $keyed = array(); $i < count($boltons); $i++)
              $keyed["bolt_on_" . ($i + 1)] = $boltons[$i];
            $keyed['count'] = $i;
            $data[$type] = $keyed;
          }
          $bolt_ons = $data;
        }
        // DOP-27: fail API if no bolt ons are found
        else
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105');
      }
      else
        $bolt_ons = $bolt_on_data;

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage ());
    }

    $this->addToOutput('bolt_ons', $bolt_ons);
    $this->addToOutput('balance', $balance);
    $this->addApiErrorNode();
    return $this->result;
  }
}
