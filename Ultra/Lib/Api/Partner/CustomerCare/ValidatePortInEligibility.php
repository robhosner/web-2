<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class ValidatePortInEligibility extends \Ultra\Lib\Api\Partner\CustomerCare
{
  private $brands;
  private $eligibility = [];
  
  public function __construct()
  {
    $this->brands = find_credential('brands');
  }

  /**
   * customercare__ValidatePortInEligibility
   *
   * Validates MVNE 2 port-in eligibility (not MVNE 1)
   *
   * @param string MSISDN
   * @return Result object
   * @author: VYT, 2014-03-23
   */
  public function customercare__ValidatePortInEligibility()
  {
    list ($msisdn, $brand) = $this->getInputValues();
    $msisdn = normalize_msisdn($msisdn);

    try
    {
      // check for intra-brand port
      $customer = get_customer_from_msisdn($msisdn, 'u.BRAND_ID,plan_state');
      if ($customer && $brand)
      {
        // then may be intra-brand port
        $this->validateIntraPort($customer, \Ultra\BrandConfig\getBrandFromShortName($brand));
      }
      else
        $this->validateNormalPort($msisdn);
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }

  /**
   * validateNormalPort
   *
   * checks if brands are different and customer in correct state
   * else deny due to MSISDN existing in system
   * adjusts Result objects for class
   *
   * @param  $customer [plan_state, BRAND_ID]
   * @param  $requestBrand brand configuration for input brand
   * @return void
   */
  private function validateIntraPort($customer, $requestBrand)
  {
    if (!in_array($customer->plan_state, [STATE_ACTIVE, STATE_SUSPENDED]))
    {
      $this->errException('ERR_API_INTERNAL: port-In denied due to customer state', 'EL0001');
    }
    else
    {
      foreach ($this->brands as $key => $brand)
      {
        $brandOutput = new \stdClass();
       
        $brandOutput->brand = $key;
        $brandOutput->is_eligible = $customer->BRAND_ID == $brand['id'] ? false : true;
        $this->eligibility[] = $brandOutput;
      }
      
      $this->addToOutput('eligibility', $this->eligibility);
      $this->succeed();
    }
  }

  /**
   * validateNormalPort
   *
   * normal port in attempt through MW
   * returns void but throws exception
   * adjusts Result objects for class
   *
   * @param  $msisdn to validate
   * @return void
   */
  private function validateNormalPort($msisdn)
  {
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
    $mwResult = $mwControl->mwPortInEligibility(array(
      'actionUUID' => $this->getRequestId(),
      'msisdn'     => $msisdn));

    // warnings are not fatal
    $warnings = $mwResult->get_warnings();
    if (count($warnings))
      $this->addWarnings($warnings);

    // handle errors
    $errors = $mwResult->get_errors();
    if (count($errors))
    {
      $this->addErrors($errors);
      $this->errException('ERR_API_INTERNAL: MW error', 'MW0001');
    }

    // determine eligibility
    $data = $mwResult->get_data_array();
    if ($mwResult->is_success() && isset($data['eligible']) && $data['eligible'])
    {
      foreach ($this->brands as $key => $brand)
      {
        $brandOutput = new \stdClass();

        $brandOutput->brand = $key;
        $brandOutput->is_eligible = true;
        $this->eligibility[] = $brandOutput;
      }

      $this->addToOutput('eligibility', $this->eligibility);
      $this->succeed();
    }
    else
    {
      if (isset($data['failure_reason']))
        $this->addWarning('Reason: ' . $data['failure_reason']);
      $this->errException('ERR_API_INVALID_ARGUMENTS: number is not eligible to port in', 'EL0001');
    }
  }
}
