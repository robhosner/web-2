<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Customers\Customer;
use Ultra\Customers\Options;
use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Lib\Util\Redis;

/**
 * Class CustomerInfo
 * @package Ultra\Lib\Api\Partner\CustomerCare
 */
class CustomerInfo extends CustomerCare
{
  protected $redis;
  private $customer;
  private $plan_name;
  private $customer_info_array = [];

  /**
   * customercare__CustomerInfo
   *
   * Given a phone number or an ICCID, provides customer useful details.
   *
   * @param string $phone_number
   * @param string $mode
   * @param $cache_override
   * @return \Result
   */
  public function customercare__CustomerInfo()
  {
    list ($phone_number, $mode, $cache_override) = $this->getInputValues();

    try
    {
      $this->redis = $this->getRedis();

      if ($this->customer = $this->getCustomerByPhoneNumber($phone_number))
      {
        $this->plan_name = get_plan_from_cos_id($this->customer->cos_id);

        $results = get_customer_state($this->customer);

        if (count($results['errors']) == 0)
        {
          $this->addCustomerInfoOutput($this->customer, $this->redis, $mode, $results);
          $this->addCustomerPukOutput($this->customer);
          $this->addCustomerDataUsageOutput($this->customer, $this->redis, $mode, $this->plan_name, $cache_override, $this->customer_info_array);
        }
        else
        {
          $this->addErrors($results['errors']);
        }
      }
      else
      {
        $this->errException("ERR_API_INVALID_ARGUMENTS: customer not found from input value = $phone_number", 'VV0031');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Get customer by phone number.
   *
   * @param $phoneNumber
   * @return object
   */
  private function getCustomerByPhoneNumber($phoneNumber)
  {
    if (strlen($phoneNumber) == 11)
    {
      // $phoneNumber is an Actcode
      \logit('Getting customer from ACT CODE');
      $customer = get_customer_from_actcode($phoneNumber);
    }
    elseif (strlen($phoneNumber) < 11)
    {
      // $phoneNumber is a MSISDN
      \logit('Getting customer from MSISDN');
      if ( ! $customer = get_customer_from_msisdn($phoneNumber))
      {
        \logit('Getting customer from cancelled MSISDN');
        $customer = get_customer_from_cancelled_msisdn($phoneNumber);
      }
    }
    else
    {
      // $phoneNumber is a ICCID
      if (strlen($phoneNumber) == 19)
      {
        $phoneNumber = substr($phoneNumber, 0, -1);
      }

      \logit('Getting customer from ICCID');
      $customer = get_customer_from_iccid($phoneNumber);
    }

    return $customer;
  }

  /**
   * Add customer info output.
   *
   * @param $customer
   * @param $redis
   * @param $mode
   * @param $results
   */
  private function addCustomerInfoOutput($customer, $redis, $mode, $results)
  {
    $customer_address = $customer->ADDRESS2 ? $customer->ADDRESS1 . $customer->ADDRESS2 : $customer->ADDRESS1;
    $originalsimproduct = (strlen($customer->CURRENT_ICCID_FULL) == 19) ? get_product_type_from_iccid($customer->CURRENT_ICCID_FULL) : '';

    $ultra_customer_options = get_ultra_customer_options_by_customer_id($customer->CUSTOMER_ID);
    $plan_name = get_plan_from_cos_id($customer->cos_id);

    if (\Ultra\Lib\Util\validateMintBrandId($customer->BRAND_ID) || \Ultra\UltraConfig\isBPlan($customer->cos_id))
    {
      $multi_month_overlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);
      $paid_through = strtotime($multi_month_overlay->CYCLE_EXPIRES);
      $duration     = $multi_month_overlay->TOTAL_MONTHS;
    }
    else
    {
      $multi_month_options = multi_month_info($customer->CUSTOMER_ID, $ultra_customer_options);
      $paid_through = $multi_month_options ? strtotime("+{$multi_month_options['months_left']} month", $customer->plan_expires_epoch) : NULL;
      $duration     = $multi_month_options ? $multi_month_options['months_total'] : NULL;
    }

    // check for MVNE mismatch between customer, MSISDN and ICCID records
    $msisdn_mvne = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer->CUSTOMER_ID, 'MVNE', NULL, 'HTT_ULTRA_MSISDN');
    $mvne_sync = empty($msisdn_mvne) || $customer->MVNE == $msisdn_mvne ? NULL : 'msisdn';
    $iccid_mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $customer->CURRENT_ICCID_FULL, 'MVNE');
    $mvne_sync .= empty($iccid_mvne) || $customer->MVNE == $iccid_mvne ? ($mvne_sync ? NULL : 'none') : ($mvne_sync ? '+iccid' : 'iccid');

    $cc_info = get_cc_info_from_customer_id($customer->CUSTOMER_ID);

    $customerObject = new Customer((array) $customer);

    $e911_address = (new Options($customerObject))->getE911Address();

    $this->customer_info_array = [
      'recharge_status'          => $results['recharge_status'],
      'customer_id'              => $customer->CUSTOMER_ID,
      'customer_plan'            => \Ultra\UltraConfig\getUltraPlanConfigurationItem($plan_name, 'name'),
      'customer_plan_start'      => $customer->plan_started_epoch,
      'customer_plan_expires'    => $customer->plan_expires_epoch,
      'paid_through'             => $paid_through,
      'duration'                 => $duration,
      'preferred_language'       => $customer->preferred_language,
      'customer_stored_balance'  => sprintf("%.2f",$customer->stored_value),
      'customer_balance'         => sprintf("%.2f",$customer->BALANCE),
      'customer_minutes'         => sprintf("%.2f",($customer->PACKAGED_BALANCE1 / 60)),
      'customer_status'          => $customer->plan_state,
      'customer_loginname'       => $customer->LOGIN_NAME,
      'current_mobile_number'    => $customer->current_mobile_number,
      'current_iccid'            => $customer->CURRENT_ICCID_FULL,
      'activation_iccid'         => $customer->ACTIVATION_ICCID,
      'brand'                    => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
      'first_name'               => $customer->FIRST_NAME,
      'last_name'                => $customer->LAST_NAME,
      'email'                    => $customer->E_MAIL,
      'address'                  => $customer->ADDRESS1,
      'address2'                 => $customer->ADDRESS2,
      'city'                     => $customer->CITY,
      'state'                    => $customer->STATE_REGION,
      'zip_code'                 => $customer->POSTAL_CODE,
      'country'                  => $customer->COUNTRY,
      'activation_start_date'    => customer_activation_start_date($customer->CUSTOMER_ID),
      'customer_has_credit_card' => customer_has_credit_card($customer),
      'voice_minutes'            => 0,
      'originalsimproduct'       => $originalsimproduct,
      'MVNE'                     => $customer->MVNE,
      'mvne_sync_failures'       => $mvne_sync,
      'puk1'                     => '',
      'puk2'                     => '',
      'cc_bin'                   => empty($cc_info->BIN)       ? '' : $cc_info->BIN,
      'cc_last4'                 => empty($cc_info->LAST_FOUR) ? '' : $cc_info->LAST_FOUR,
      'temp_password'            => func_read_temp_password($customer->CUSTOMER_ID),
      '4g_lte_remaining'         => (($mode == 'DATA_USAGE') ? $redis->get('ultra/data/4g_lte/remaining/' . $customer->CUSTOMER_ID) : ''),
      '4g_lte_usage'             => (($mode == 'DATA_USAGE') ? $redis->get('ultra/data/4g_lte/usage/' . $customer->CUSTOMER_ID) : ''),
      '4g_lte_last_applied'      => 0,
      '4g_lte_bolton_percent_used' => (($mode == 'DATA_USAGE') ? $redis->get('ultra/data/4g_lte/mint/addon/percent/used/' . $customer->CUSTOMER_ID) : ''),
      'ultra_customer_options'   => $ultra_customer_options,
      'zero_minutes'             => int_positive_or_zero(1000 - \Ultra\UltraConfig\zeroUsedMinutes($plan_name) - $customer->PERIOD_MINUTES_TO_DATE_BILLED),
      'bolt_ons'                 => get_bolt_ons_info_from_customer_options($customer->CUSTOMER_ID),
      'intl_dialing'             => ($customer->ACCOUNT_STATUS_TYPE != ACCOUNT_STATUS_TYPE_DEACTIVATED) ? 1 : 0,
      'wholesale_plan'           => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
      'bogo_free_month'          => count(array_intersect(\Ultra\UltraConfig\getValidBOGOTypes(), $ultra_customer_options)) ? 1 : 0,
      'account_number'           => $customer->CUSTOMER,
      'e911_address'             => $e911_address,
      'has_wifi_soc'             => ($e911_address) ? true : false,
      'globe_minutes'            => $customer->PACKAGED_BALANCE3,
    ];

    $this->customer = $customer;
  }

  /**
   * Add customer puk output.
   *
   * @param $customer
   */
  private function addCustomerPukOutput($customer)
  {
    // retrieve PUK1 and PUK2 from HTT_INVENTORY_SIM
    $htt_inventory_sim_result = mssql_fetch_all_objects(
      logged_mssql_query(
        htt_inventory_sim_select_query(
          ["customer_id" => $customer->CUSTOMER_ID]
        )
      )
    );

    if ($htt_inventory_sim_result && is_array($htt_inventory_sim_result) && count($htt_inventory_sim_result))
    {
      $this->customer_info_array['puk1'] = $htt_inventory_sim_result[0]->PUK1;
      $this->customer_info_array['puk2'] = $htt_inventory_sim_result[0]->PUK2;
      $this->customer_info_array['master_agent'] = $htt_inventory_sim_result[0]->INVENTORY_MASTERAGENT;
    }
  }

  /**
   * Add customer data usage output.
   *
   * @param $customer
   * @param $redis
   * @param $mode
   * @param $plan_name
   * @param $cache_override
   * @param $customer_info_array
   */
  private function addCustomerDataUsageOutput($customer, $redis, $mode, $plan_name, $cache_override, $customer_info_array)
  {
    if ($mode == 'DATA_USAGE')
    {
      // get '4g_lte_last_applied' from HTT_DATA_EVENT_LOG
      $latest_data_event_log = get_latest_data_event_log($customer->CUSTOMER_ID);
      $customer_info_array['4g_lte_last_applied'] = $latest_data_event_log ? $latest_data_event_log->event_date_epoch : 0;
    }

    // invoke mwCheckBalance to check 4G LTE data usage for this customer
    if ($customer->current_mobile_number
      && ($mode == 'DATA_USAGE')
      && ((!$customer_info_array['4g_lte_usage'] && !$customer_info_array['4g_lte_remaining']) || $cache_override)
    )
    {
      list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown , $mvneError ) = mvneGet4gLTE($this->getRequestId(), $customer->current_mobile_number, $customer->CUSTOMER_ID, $redis);

      $customer_info_array['4g_lte_remaining'] = $remaining;
      $customer_info_array['4g_lte_usage'] = $usage;

      if ( ! empty( $mintAddOnUsage )
        && ! empty( $mintAddOnUsage + $mintAddOnRemaining )
      )
      {
        $customer_info_array['4g_lte_bolton_percent_used'] = ceil( ( $mintAddOnUsage * 100 ) / ( $mintAddOnUsage + $mintAddOnRemaining ) );
      }
    }

    // If the result is under 1GB, then return as MB rounded up to the nearest MB i.e. 120MB
    // If the result is over 1GB, then show it as the GB number, the decimal, and then the MB amount out to three digits rounded up to the nearest MB i.e. 1.329GB
    $customer_info_array['4g_lte_remaining'] = $customer_info_array['4g_lte_remaining'] ? beautify_4g_lte_string($customer_info_array['4g_lte_remaining'], FALSE) : '';
    $customer_info_array['4g_lte_usage'] = $customer_info_array['4g_lte_usage'] ? beautify_4g_lte_string($customer_info_array['4g_lte_usage'], TRUE) : '';

    // DATAQ-112
    if ($mode == 'OCS_BALANCE' && $plan_name == 'L34')
    {
      $customer_info_array['voice_minutes'] = mvneGetVoiceMinutes($customer->customer_id);
    }

    $this->addArrayToOutput($customer_info_array);
  }
}

