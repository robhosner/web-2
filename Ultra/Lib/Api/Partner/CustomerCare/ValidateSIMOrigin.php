<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class ValidateSIMOrigin extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__ValidateSIMOrigin
   *
   * Returns the history of the SIM inventory and provisioning.
   *
   * @param string iccid
   * @return object Result
   */
  public function customercare__ValidateSIMOrigin()
  {
    list ($iccid) = $this->getInputValues();

    $returnData = array (
      'inventory_master'      => '',
      'inventory_distributor' => '',
      'inventory_dealer'      => '',
      'activated_master'      => '',
      'activated_distributor' => '',
      'activated_dealer'      => '',
      'enabled'               => FALSE,
      'iccid_batch_id'        => '',
      'sku'                   => '',
      'stored_value'          => '',
      'pin_and_puk_info'      => '',
      'product'               => '',
      'mvne'                  => '',
      'brand'                 => '',
      'expires_date'          => ''
    );

    try
    {
      // The ICCID is validated to ensure it is all digits, not empty, and either 18 or 19 digits in length
      if (($iccid != '') && is_numeric($iccid) && (strlen($iccid) == 18 || strlen($iccid) == 19))
      {
        $iccid = luhnenize($iccid); // if 19 in length, it just returns the original value.
      }
      else
      {
        $this->errException('ERR_API_INTERNAL: Invalid ICCID provided, it MUST be 18 or 19 digits, and can not be NULL.', 'VA0001');
      }

      teldata_change_db();

      // Select the Distributor Information
      $query = htt_inventory_sim_join_ultra_celluphone_channel_query ( $iccid );

      // Execute the query
      $rowSet = mssql_fetch_all_objects ( logged_mssql_query ( $query ) );

      if (!$rowSet || !is_array($rowSet) || !count($rowSet))
      {
        $this->errException('ERR_API_INTERNAL: SIM not found in inventory. Check the value of the ICCID and try again.', 'VV0013');
      }

      $row = $rowSet[0];

      $returnData ['mvne']             = $row->MVNE;
      $returnData ['product']          = $row->PRODUCT_TYPE;
      $returnData ['enabled']          = $row->SIM_HOT;
      $returnData ['pin_and_puk_info'] = sprintf("PIN1:%s, PUK1:%s, PIN2:%s, PUK2:%s",$row->PIN1,$row->PUK1,$row->PIN2,$row->PUK2);
      $returnData ['stored_value']     = $row->STORED_VALUE;
      $returnData ['sku']              = $row->SKU;
      $returnData ['iccid_batch_id']   = $row->ICCID_BATCH_ID;
      $returnData ['expires_date']     = $row->SIMBATCH_EXPIRES_DATE;
      $returnData ['brand']            = \Ultra\UltraConfig\getShortNameFromBrandId($row->BRAND_ID);

      dlog('', 'XXX: %s', $row);

      // Process the Master ID
      if (isset ( $row->master_id ) && isset ( $row->master_name ))
        $returnData ['inventory_master'] = $row->master_id . ' ' . $row->master_name;

      // Process the Distributor ID
      if (isset ( $row->dist_id ) && isset ( $row->dist_name ))
        $returnData ['inventory_distributor'] = $row->dist_id . ' ' . $row->dist_name;

      // Process the Dealer ID
      if (isset ( $row->dealer_id ) && isset ( $row->dealer_name ))
        $returnData ['inventory_dealer'] = $row->dealer_id . ' ' . $row->dealer_name;

      // Fetch the Activation Log Record for the respective Full Iccid passed in
      $activationEntry = mssql_fetch_all_objects ( logged_mssql_query ( htt_activation_log_select_by_iccid_full_query ( $iccid ) ) );

      if ( count ( $activationEntry ) == 0 )
      {
        $returnData ['activated_master'] = '';
        $returnData ['activated_distributor'] = '';
        $returnData ['activated_dealer'] = '';
      }
      else
      {
        if (isset ( $activationEntry[0]->master_id ) && $activationEntry[0]->master_id > 0)
          $returnData ['activated_master'] = $activationEntry[0]->master_id . ' ' . $activationEntry[0]->master_name;

        if (isset ( $activationEntry[0]->dist_id) && $activationEntry[0]->dist_id > 0)
          $returnData ['activated_distributor'] = $activationEntry[0]->dist_id . ' ' . $activationEntry[0]->dist_name;

        if (isset ( $activationEntry[0]->dealer_id ) && $activationEntry[0]->dealer_id > 0 )
          $returnData ['activated_dealer'] = $activationEntry[0]->dealer_id . ' ' . $activationEntry[0]->dealer_name;
      }

      $this->addArrayToOutput(array(
        "inventory_master"      => $returnData ['inventory_master'],
        "inventory_distributor" => $returnData ['inventory_distributor'],
        "inventory_dealer"      => $returnData ['inventory_dealer'],
        "activated_master"      => $returnData ['activated_master'],
        "activated_distributor" => $returnData ['activated_distributor'],
        "activated_dealer"      => $returnData ['activated_dealer'],
        "mvne"                  => $returnData ['mvne'],
        "brand"                 => $returnData ['brand'],
        "product"               => $returnData ['product'],
        "enabled"               => $returnData ['enabled'],
        "pin_and_puk_info"      => $returnData ['pin_and_puk_info'],
        "stored_value"          => $returnData ['stored_value'],
        "sku"                   => $returnData ['sku'],
        "iccid_batch_id"        => $returnData ['iccid_batch_id'],
        "expires_date"          => $returnData ['expires_date']
      ));

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
