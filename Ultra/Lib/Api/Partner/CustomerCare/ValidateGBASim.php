<?php
namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Sims\Interfaces\SimRepository;

class ValidateGBASim extends CustomerCare
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * ValidateGBASim constructor.
   * @param CustomerRepository $customerRepository
   * @param SimRepository $simRepository
   */
  public function __construct(CustomerRepository $customerRepository, SimRepository $simRepository)
  {
    $this->customerRepository = $customerRepository;
    $this->simRepository = $simRepository;
  }

  /**
   * customercare__ValidateGBASim
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__ValidateGBASim()
  {
    list ($customer_id) = $this->getInputValues();

    try {
      $customer = $this->customerRepository->getCustomerById($customer_id, ['current_iccid_full']);

      if (!$customer || !$customer->current_iccid_full) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      $sim = $this->simRepository->getSimInventoryAndBatchInfoByIccid($customer->current_iccid_full);

      $this->addToOutput('valid', $sim->allowsWifiSoc());
      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
