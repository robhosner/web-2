<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Messaging\Messenger;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class SetCustomerInfo extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var CreditCardRepository
   */
  private $cardRepository;

  public $customer;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * SetCustomerInfo constructor.
   * @param Messenger $messenger
   * @param CreditCardRepository $cardRepository
   * @param CustomerRepository $customerRepository
   */
  public function __construct(Messenger $messenger, CreditCardRepository $cardRepository, CustomerRepository $customerRepository)
  {
    $this->messenger = $messenger;
    $this->cardRepository = $cardRepository;
    $this->customerRepository = $customerRepository;
  }

  /**
   * customercare__SetCustomerInfo
   *
   * update given customer info
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands+-+CustomerCare#Commands-CustomerCare-CustomerCare::SetCustomerInfo
   * @return object Result
   */
  public function customercare__SetCustomerInfo()
  {
    // initialize
    list ($customer_id, $preferred_language, $auto_recharge, $address, $address2, $city, $state, $zip, $first_name, $last_name) = $this->getInputValues();

    try
    {
      // get customer record
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, array(
        'CUSTOMER_ID',
        'plan_state',
        'preferred_language',
        'monthly_cc_renewal',
        'CURRENT_ICCID',
        'CURRENT_ICCID_FULL',
        'current_mobile_number'
      ));

      $this->customer = $customer;

      $account = get_account_from_customer_id($customer_id, array('COS_ID'));

      if (! $customer || !$account)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      $customer->COS_ID = $account->COS_ID;

      // reject updates on Cancelled
      if ($customer->plan_state == STATE_CANCELLED)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer state', 'IN0001');

      // prepare each update parameter
      $params = array('customer_id' => $customer_id);

      if ( ! empty($preferred_language))
        if ($customer->preferred_language != $preferred_language)
          $params['preferred_language'] = $preferred_language;

      if ($auto_recharge !== '')
      {
        if ($customer->monthly_cc_renewal != $auto_recharge)
        {
          $params['monthly_cc_renewal'] = $auto_recharge;
          if ($auto_recharge && ! customer_has_credit_card($customer)) // enabling auto rechange without CC on file
            $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no credit card on file', 'VV0037');
        }
        // handle when customer successfully turns off auto recharge
        if ($auto_recharge == 0){
          $params['easypay_activated'] = 0;
        }
        else
        {
          $this->sendAutoRechargeSms();
        }
      }

      $customerUpdateParams = [];

      empty($address) ?: $customerUpdateParams['address1'] = $address;
      empty($address2) ?: $customerUpdateParams['address2'] = $address2;
      empty($city) ?: $customerUpdateParams['city'] = $city;
      empty($state) ?: $customerUpdateParams['state_region'] = $state;
      empty($zip) ?: $customerUpdateParams['postal_code'] = $zip;
      empty($first_name) ?: $customerUpdateParams['first_name'] = $first_name;
      empty($last_name) ?: $customerUpdateParams['last_name'] = $last_name;

      if (!empty($customerUpdateParams)) {
        if (!$this->customerRepository->updateCustomerByCustomerId($customer_id, $customerUpdateParams)) {
          return $this->errException('ERR_API_INTERNAL: error updating customer info', 'DB0001', 'ERR_API_INTERNAL: error updating customer info');
        }
      }

      // ensure that something is given to udpate
      if (count($params) == 1 && empty($customerUpdateParams))
        $this->errException('ERR_API_INVALID_ARGUMENTS: nothing to update', 'ND0012');

      if (count($params) > 1) {
        // prepare and execute update statement
        $update_query = htt_customers_overlay_ultra_update_query($params);
        if ( ! run_sql_and_check($update_query))
          $this->errException('ERR_API_INTERNAL: failed to update customer info', 'DB0001');
      }

      // update voicemail language if requested
      if ($preferred_language)
      {
        if ( ! mvneSetVoicemailLanguage($customer, $preferred_language))
          $this->addWarning('Failed to set voicemail language');
        teldata_change_db();
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' , 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }

  private function sendAutoRechargeSms()
  {
    $this->customer->customer_id = $this->customer->CUSTOMER_ID;
    $creditCardInfo = $this->cardRepository->getCcInfoFromCustomerId($this->customer->customer_id);
    $lastFour = empty($creditCardInfo->LAST_FOUR) ? '' : $creditCardInfo->LAST_FOUR;

    $result = $this->messenger->enqueueImmediateSms($this->customer->customer_id, 'auto_recharge_active', ['last_four' => $lastFour]);

    if ($result->is_failure())
    {
      return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'SM0002');
    }
  }
}
