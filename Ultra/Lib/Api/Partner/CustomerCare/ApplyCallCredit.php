<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class ApplyCallCredit extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__ApplyCallCredit
   *
   * Debit the wallet and add money to Call Credit.
   *
   * @return Result object
   */
  public function customercare__ApplyCallCredit ()
  {
    list ( $msisdn , $amount ) = $this->getInputValues();

    $msisdn = normalize_msisdn_10( $msisdn );

    try
    {
      // get customer record
      teldata_change_db();

      $customer = get_customer_from_msisdn($msisdn);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      $call_anywhere_additional_credit_options = \Ultra\UltraConfig\call_anywhere_additional_credit_options();

      if ( ! $call_anywhere_additional_credit_options || ! is_array( $call_anywhere_additional_credit_options ) )
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );

      dlog('',"call_anywhere_additional_credit_options = %s",$call_anywhere_additional_credit_options);

      $credit_option = NULL;

      foreach( $call_anywhere_additional_credit_options as $option )
        if ( $option->cost == ( $amount / 100 ) )
          $credit_option = $option;

      // check if the amount matches with the allowed credit options
      if ( ! $credit_option )
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid amount', 'VV0115');

      // check if the customer has enough money in his BALANCE
      if ( $customer->BALANCE < ( $amount / 100 ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

      // execute the following DB operations in a DB transaction
      if ( ! start_mssql_transaction() )
        $this->errException('ERR_API_INTERNAL: DB write error (0)', 'DB0001');

      $result = func_add_ild_minutes(
        array(
          'customer'  => $customer,
          'amount'    => ( $amount / 100 ),
          'reason'    => __FUNCTION__,
          'source'    => 'PORTAL',
          'reference' => $this->getRequestId(),
          'balance_change' => ( $amount / 100 ),
          'bonus'     => $credit_option->bonus_percent
        )
      );

      dlog('',"func_add_ild_minutes result = %s",$result);

      if ( ! $result['success'] )
      {
        rollback_mssql_transaction();

        $this->errException('ERR_API_INTERNAL: DB write error (1)', 'DB0001');
      }

      if ( ! commit_mssql_transaction() )
        $this->errException('ERR_API_INTERNAL: DB write error (2)', 'DB0001');

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
