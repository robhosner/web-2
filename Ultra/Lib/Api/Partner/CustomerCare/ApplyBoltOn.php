<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class ApplyBoltOn extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__ApplyBoltOn
   * apply the given Immediate Bolt On to the customer account
   * WARNING: care must be taken to return user friendly messages in user_errors since those are shown to subscribers (3ci)
   * @see BOLT-4
   * @param integer customer_id
   * @param string subscriber mobile number
   * @param string bolt_on_id
   * @return object Result
   */
  
  public function customercare__ApplyBoltOn()
  {
    // initialize
    list ($customer_id, $msisdn, $bolt_on_id) = $this->getInputValues();

    try
    {
      // get customer record
      teldata_change_db();
      if ($customer_id)
      {
        $account  = get_account_from_customer_id( $customer_id, array('CUSTOMER_ID','COS_ID','BALANCE') );
        $customer = get_ultra_customer_from_customer_id( $customer_id, array('BRAND_ID','current_mobile_number') );
        $customer = (object)array_merge((array)$account, (array)$customer);
      }
      elseif ($msisdn)
        $customer = get_customer_from_msisdn($msisdn);

      if (empty($customer))
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      $customer_id = $customer->CUSTOMER_ID;

      // verify that the customer is Active
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);
      if ( $state['state'] != STATE_ACTIVE )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      // get plan from COS_ID
      $plan = get_plan_from_cos_id($customer->COS_ID);
      if( empty($plan) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: plan cannot be found.', 'IN0001');

      // get Bolt On info
      $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id );
      if ( ! $boltOnInfo )
        $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0101');
      dlog('', "boltOnInfo = %s", $boltOnInfo);

      //get plans that are allowed
      $bolt_on_data = \Ultra\UltraConfig\getBoltOnInfoByPlan(get_plan_name_from_short_name($plan));
      $bolts = array();
      foreach ($bolt_on_data as $bolt)
      {
        for ($i = 0; $i < count($bolt); $i++)
        {
          $bolts[] = $bolt[$i]['product'].'_'.$bolt[$i]['get_value'].'_'.$bolt[$i]['cost'];
        }
      }
      //if the current bolt on being asked for is not in the bolts array throw exception
      if ( ! in_array($bolt_on_id, $bolts))
        $this->errException('ERR_API_INTERNAL: bolt on not allowed for plan level', 'VV0103');

      // check redis semaphore
      $redis = new \Ultra\Lib\Util\Redis;
      if ( get_bolt_on_semaphore( $redis , $customer_id, $boltOnInfo['product'] ) )
        $this->errException("ERR_API_INTERNAL: a bolt on has been processed less than 15 minutes ago.", 'VV0102', 'another purchase has been processed recently, please rety again later');

      // check for sufficient wallet balance
      if ( $boltOnInfo['cost'] > $customer->BALANCE )
        $this->errException('ERR_API_INTERNAL: not enough money to perform this operation', 'VV0103', 'not enough money in your Ultra Wallet');

      if (\Ultra\MvneConfig\isMintDataSOC($boltOnInfo['upgrade_plan_soc']))
      {
        $error = \Ultra\Lib\BoltOn\verifyMintDataAddOnThreshold($customer, $boltOnInfo['type']);
        if ($error)
          $this->errException('ERR_API_INTERNAL: Too much remaining add on data.', 'VV0259', 'Too much remaining add on data. Please try again after using more add on data.');
      }

      // action for adding a Bolt On on demand
      list( $error , $error_code ) = \Ultra\Lib\BoltOn\addBoltOnImmediate($customer_id, $boltOnInfo, 'SPEND', __FUNCTION__, NULL);
      if ( $error )
        $this->errException( $error , $error_code, $error );

      // set Redis semaphore to block another dealerportal__ApplyBoltOn by the same customer for 15 minutes
      set_bolt_on_semaphore($redis, $customer_id, $boltOnInfo['product']);

      // set Redis semaphore to block SMS from notification__DataNotificationHandler for 5 minutes
      if ( $boltOnInfo['product'] == 'DATA' )
        set_data_recharge_notification_delay($redis, $customer_id);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    $this->addApiErrorNode();
    return $this->result;
  }
}
