<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class CheckCallCreditOptions extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__CheckCallCreditOptions
   *
   * Returns possible purchase options for buying Additional Call Anywhere Credit.
   *
   * @return Result object
   */
  public function customercare__CheckCallCreditOptions ()
  {
    list ( $customer_id ) = $this->getInputValues();

    $this->addToOutput('call_credit_options','');
    $this->addToOutput('days_plan_expires',  '');

    try
    {
      // get customer record
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, array('days_before_expire'));
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      $call_anywhere_additional_credit_options = \Ultra\UltraConfig\call_anywhere_additional_credit_options();

      if ( ! $call_anywhere_additional_credit_options || ! is_array( $call_anywhere_additional_credit_options ) )
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );

      $this->addToOutput( 'call_credit_options' , $call_anywhere_additional_credit_options );
      $this->addToOutput( 'days_plan_expires',    $customer->days_before_expire );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
