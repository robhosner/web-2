<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class CustomerDirectSMS extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__CustomerDirectSMS
   *
   * Send an SMS to the phone number of the customer.
   *
   * @param int customer_id
   * @param string message
   * @param int agent agent making request
   * @return object Result
   */
  public function customercare__CustomerDirectSMS()
  {
    list ($customer_id, $message, $agent) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID'));

      if ( ! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      else
      {
        $messaging_queue_params = array();

        $message = 'From Ultra Customer Svc: ' . $message;

        if ( strlen($message) > 100 )
        {
          $chunks = str_split($message, 100);

          $messaging_queue_params['message1'] = $chunks[0];
          $messaging_queue_params['message2'] = $chunks[1];
        }
        else
        {
          $messaging_queue_params['message'] = $message;
        }

        $params = array(
          'customer_id'                 => $customer_id,
          'reason'                      => 'customercare__CustomerDirectSMS',
          'expected_delivery_datetime'  => 'getutcdate()',
          'next_attempt_datetime'       => 'getutcdate()',
          'type'                        => 'SMS_INTERNAL',
          'messaging_queue_params'      => $messaging_queue_params
        );

        $result = htt_messaging_queue_add($params);

        $errors = $result ->get_errors();
        if (count($errors))
          $this->errException($errors[0], 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
