<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';
require_once 'classes/Flex.php';

class ApplyBalanceToStoredValue extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__ApplyBalanceToStoredValue
   *
   * transfer amount from BALANCE to STORED_VALUE
   * @see MVNO-2534
   * @param string subscriber MSISDN
   * @return object Result
   */
  public function customercare__ApplyBalanceToStoredValue()
  {
    // initialize
    list ($msisdn, $amount) = $this->getInputValues();

    try
    {
      // get customer record
      teldata_change_db();
      if (! $customer = get_customer_from_msisdn($msisdn)) // already normalized
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // we must not update Cancelled accounts
      if ($customer->plan_state == 'Cancelled')
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer account has been cancelled', 'IN0001');

      if (\Ultra\Lib\Flex::isFlexPlan($customer->cos_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: command disabled for flex customers', 'IN0001');

      // confirm funds
      $amount = $amount / 100;
      if ($customer->BALANCE < $amount)
        $this->errException('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

      // perform transaction
      $params = array(
        'customer'    => $customer,
        'amount'      => $amount,
        'source'      => 'IVR',
        'detail'      => __FUNCTION__,
        'reason'      => 'move funds to monthly recharge',
        'reference'   => NULL);
      $result = func_apply_balance_to_stored_value($params);
      if ( ! $result['success'])
      {
        foreach ($result['errors'] as $error)
          $this->addError($error, 'VV0001');
        $this->errException('ERR_API_INTERNAL: failed to transfer amount to stored value', 'DB0001');
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' ,'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}
