<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class CreateUser extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CreateUser
   * create a new user in the organization of the currently logged in user and return the newly created user ID
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__CreateUser()
  {
    // initialize
    list($securityToken, $username, $password, $firstName, $lastName, $phoneNumber, $eMail, $active, $accessRole, $effectiveDate, $expirationDate, $note) = $this->getInputValues();
    $this->addToOutput('user_id', NULL);

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($securityToken, __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      // create new user
      list($userId, $errorMessage) = \Ultra\Lib\DB\DealerPortal\createUser($session['user_id'], $accessRole, $username, $password, $active, $firstName, $lastName, $eMail, $phoneNumber, $effectiveDate, $expirationDate, $note);
      if ( ! $userId)
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0003', $errorMessage);

      $this->addToOutput('user_id', $userId);
      $this->succeed();
    } 
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
