<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class VerifyPortInStatus extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__VerifyPortInStatus
   *
   * This is the API to be invoked after dealerPortal__RequestProvisionPortedCustomer in order to get the complete response.
   * The non-dealerportal version is provisioning__verifyProvisionPortedCustomerAsync
   *
   * @return Result object
   */
  public function dealerportal__VerifyPortInStatus ()
  {
    list ( $security_token , $request_id ) = $this->getInputValues();

    $this->addToOutput('port_pending','');
    $this->addToOutput('phone_number','');
    $this->addToOutput('port_success','');
    $this->addToOutput('port_status','');
    $this->addToOutput('port_resolution',array());

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // check transition
      $transition_result = provision_check_transition($request_id);

      dlog('',"transition_result = %s",$transition_result);

      $errors = append_transition_failure_reason($transition_result['errors'],$request_id); //TODO

      dlog('',"errors = %s",$errors);

      $porting_status = 'unknown';
      $porting_success = FALSE;
      $porting_final = FALSE;
      $r_codes = array();

        if ( $errors && count($errors) )
        {
          dlog('',"port errors : %s",$errors);

          $this->addToOutput('port_success',   FALSE);
          $this->addToOutput('port_pending',   FALSE);
          $this->addToOutput('port_resolution',$errors);
          $this->addToOutput('port_status',    'ERROR');
        }
        else
        {
          dlog('',"port initialized successfully");

          // get data from DB table PORTIN_QUEUE
          $portInQueue = new \PortInQueue();

          $loadByCustomerIdResult = $portInQueue->loadByCustomerId( $transition_result['port_attempt_customer_id'] );

          if ( $loadByCustomerIdResult->is_success() )
          {
            $this->addToOutput('phone_number',$portInQueue->msisdn);

            list(
              $port_success,
              $port_pending,
              $port_status,
              $port_resolution
            ) = interpret_port_status( $portInQueue, $transition_result );

            $this->addToOutput('port_success',    $port_success);
            $this->addToOutput('port_pending',    $port_pending);
            $this->addToOutput('port_status',     $port_status);

            if ( $port_resolution )
              $this->addToOutput('port_resolution', array($port_resolution));
          }
          else
          {
            // In this case the Port request is not yet recorded in PORTIN_QUEUE

            $this->addToOutput('port_success',   FALSE);
            $this->addToOutput('port_pending',   TRUE);
            $this->addToOutput('port_status',    'initialized');
          }

          teldata_change_db();
        }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}