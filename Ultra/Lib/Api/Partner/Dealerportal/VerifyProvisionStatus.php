<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class VerifyProvisionStatus extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__VerifyProvisionStatus
   *
   * Verify the status of the customer and suggests next actions (if any).
   *
   * @return Result object
   */
  public function dealerportal__VerifyProvisionStatus ()
  {
    list ( $security_token , $customer_id ) = $this->getInputValues();

    $this->addArrayToOutput(array(
      'created_date' => '',
      'customer_id' => '',
      'zipcode' => '',
      'iccid' => '',
      'plan_amount' => '',
      'plan_state' => '',
      'pending_transition' => '',
      'port_resolution' => array(),
      'msisdn' => '',
      'port_account_number' => '',
      'port_account_password' => '',
      'port_status' => '',
      'next_action' => '',
      'amount_needed' => '',
      'activations_enabled' => '',
      'history_last_updated' => '',
      'port_request_id' => '',
      'bolt_ons' => '',
      'email' => '',
      'contact_phone' => ''
    ));

    $amount_needed = NULL;

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $select_fields = array(
        'a.CUSTOMER_ID',
        'a.COS_ID',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'stored_value',
        'BALANCE',
        'a.COS_ID',
        'c.POSTAL_CODE',
        "CASE WHEN CREATION_DATE_TIME IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', CREATION_DATE_TIME ) END created_date",
        "( Datediff(ss, '1970-01-01', GETUTCDATE()) - Datediff(ss, '1970-01-01', a.CREATION_DATE_TIME) ) CREATED_SECONDS_AGO",
        "u.BRAND_ID",
        'c.E_MAIL'
      );

      // get customer data
      $customer = get_customer_from_customer_id( $customer_id , $select_fields );

      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      $this->addToOutput('plan_state',$customer->plan_state);

      // BOLT-43: return $0 for amount_needed if sub is already Active
      if ($customer->plan_state == STATE_ACTIVE)
        $amount_needed = 0;

      $msisdn = '';
      if ( $customer->current_mobile_number )
        $msisdn = $customer->current_mobile_number;

      // get data from HTT_TRANSITION_LOG
      $result = get_all_customer_state_transitions( $customer->CUSTOMER_ID );

      $pending_transition = '';

      if ( count($result['errors']) )
        $this->addWarnings( $result['errors'] );
      elseif( count($result['state_transitions']) )
      {
        // check if there is an OPEN transition

        foreach( $result['state_transitions'] as $transition )
          if ( $transition->STATUS == 'OPEN' )
          {
            $pending_transition = $this->addToOutput('pending_transition',$transition->TRANSITION_UUID);

            // BOLT-43: return $0 for amount_needed if sub has an OPEN  transition to Active
            if ($transition->TO_PLAN_STATE == STATE_ACTIVE)
              $amount_needed = 0;
          }
      }

      $redis = new \Ultra\Lib\Util\Redis;

      $port_status              = $redis->get( 'ultra/port/status/'               . $msisdn );
      $port_resolution          = $redis->get( 'ultra/port/provstatus_error_msg/' . $msisdn );
      $port_updated_seconds_ago = $redis->get( 'ultra/port/update_timestamp/'     . $msisdn );

      if ( !$port_status && !$port_resolution && !$port_updated_seconds_ago )
      { // get Portin info from DB

        $portInQueue = new \PortInQueue();

        $loadPortInQueueResult = $portInQueue->loadByCustomerId( $customer->CUSTOMER_ID );

        if ( $loadPortInQueueResult->is_success() && $portInQueue->portin_queue_id )
        {
          list(
            $port_success,
            $port_pending,
            $port_status,
            $port_resolution
          ) = interpret_port_status( $portInQueue );

          $port_updated_seconds_ago = $portInQueue->updated_seconds_ago;

          $port_resolution = ( $port_resolution ) ? array( $port_resolution ) : array() ;
        }

        teldata_change_db();

      } // get Portin info from DB

      if ($amount_needed === NULL)
        $amount_needed = get_amount_needed_from_customer( $customer );
      $next_action   = get_next_action_from_plan_state( $customer->plan_state , $pending_transition , $port_status , $customer->CREATED_SECONDS_AGO , $port_updated_seconds_ago );

      $plan_amount = get_plan_from_cos_id($customer->COS_ID);
      $plan_amount = substr($plan_amount, -2); // in dollars

      $redis_port = new \Ultra\Lib\Util\Redis\Port();

      $this->addArrayToOutput(array(
        'created_date'  => $customer->created_date,
        'customer_id'   => $customer->CUSTOMER_ID,
        'zipcode'       => $customer->POSTAL_CODE,
        'iccid'         => $customer->CURRENT_ICCID_FULL,
        'plan_amount'   => $plan_amount,
        'bolt_ons'      => get_bolt_ons_info_from_customer_options($customer_id),
        'brand'         => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
        'email'         => trim($customer->E_MAIL),
        'contact_phone' => get_customer_contact_phone($customer->CUSTOMER_ID)
      ));

      if ( $port_resolution )
      {
        if ( is_array($port_resolution) )
          $this->addToOutput('port_resolution',$port_resolution);
        else
          $this->addToOutput('port_resolution',array($port_resolution));
      }

      $this->addArrayToOutput(array(
        'port_status'         => $port_status,
        'next_action'         => $next_action,
        'amount_needed'       => $amount_needed,
        'pending_transition'  => $pending_transition,
        'activations_enabled' => activations_enabled(TRUE),
        'msisdn'              => $msisdn,
        'port_request_id'     => $redis_port->getRequestId( $customer->CUSTOMER_ID , $customer->plan_state ),
      ));

      if ( $msisdn )
      {
        $port_account = retrieve_port_account( $msisdn );

        $this->addToOutput('port_account_number',  $port_account['port_account_number']  );
        $this->addToOutput('port_account_password',$port_account['port_account_password']);
      }

      $activation_history = get_ultra_activation_history( $customer->CUSTOMER_ID );

      if ( $activation_history )
        $this->addToOutput('history_last_updated',$activation_history->history_last_updated);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}