<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Lib\Flex;
use Ultra\Lib\Services\FamilyAPI;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class RequestProvisionNewCustomer extends \Ultra\Lib\Api\Partner\Dealerportal
{
  use FlexHandler;
  
  private $familyAPI;

  public function __construct(FamilyAPI $familyAPI)
  {
    parent::__construct();

    $this->familyAPI = $familyAPI;
  }

  /**
   * dealerportal__RequestProvisionNewCustomer
   *
   * This API call is used to create a customer and 'activate' his SIM.
   * Behaves like provisioning__requestProvisionNewCustomerAsync invoked with loadPayment = 'NONE'
   *
   * @return Result object
   */
  public function dealerportal__RequestProvisionNewCustomer ()
  {
    list ( $security_token , $ICCID , $zipcode , $target_plan , $preferred_language , $bolt_ons, $invite_code ) = $this->getInputValues();

    $this->addToOutput('request_id','');
    $this->addToOutput('customer_id','');

    try
    {
      // connect to the DB
      teldata_change_db();

      $isFlexPlan = Flex::isFlexPlan($target_plan);

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // validate ICCID
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$ICCID,'VALID');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // verify that activations are enabled
      if ( ! activations_enabled(TRUE) )
        $this->errException( 'ERR_API_INTERNAL: ' . \Ultra\UltraConfig\dealer_portal_activations_disabled_msg() , 'AP0001' , \Ultra\UltraConfig\dealer_portal_activations_disabled_msg() );

      // check invite code, if given
      if ($isFlexPlan && $invite_code) {
        $this->validateInviteCode($invite_code);
      }

      // get Bolt Ons info
      $bolt_ons = $bolt_ons ? (is_array($bolt_ons) ? $bolt_ons : array($bolt_ons)) : array();

      $new_bolt_on_configuration = array();
      $bolt_ons_cost = 0;
      foreach( $bolt_ons as $bolt_on_id )
        if ($bolt_on_id = trim($bolt_on_id))
        {
          $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id );

          if ( ! $boltOnInfo )
            $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0101');

          dlog('',"%s : boltOnInfo = %s",$bolt_on_id,$boltOnInfo);

          $new_bolt_on_configuration[ $boltOnInfo['option_attribute'] ] = $boltOnInfo['cost'];
          $bolt_ons_cost += $boltOnInfo['cost'];
        }

      $activation_agent       = $session_data['dealer'];
      $activation_store       = $session_data['dealer'];
      $activation_masteragent = $session_data['masteragent'];
      $activation_distributor = $session_data['distributor'];
      $activation_user_id     = $session_data['user_id'];

      // get SIM info
      $sim = get_htt_inventory_sim_from_iccid($ICCID);
      if (! $sim)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      // validate $ICCID
      if ( ! validate_ICCID($sim, 1) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used" , 'VV0065' );

      if ( ! \Ultra\UltraConfig\isBrandIdAllowed($sim->BRAND_ID))
        $this->errException('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid', 'IC0003');

      // check brand of target_plan
      $targetPlanBrandID = get_brand_id_from_cos_id(get_cos_id_from_plan($target_plan));
      if ($targetPlanBrandID != $sim->BRAND_ID) {
        $this->errException('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid', 'IC0003');
      }

      $redis = new \Ultra\Lib\Util\Redis;

      // check if the $ICCID is good to activate

      if ( $redis->get( 'iccid/good_to_activate/' . luhnenize( $ICCID ) ) )
        $redis->del( 'iccid/good_to_activate/' . luhnenize( $ICCID ) );
      else
      {
        // Activate/Can

        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        $result = $mwControl->mwCanActivate(
          array(
            'iccid'      => $ICCID,
            'actionUUID' => getNewActionUUID('dealerportal ' . time())
          )
        );

        if ( $result->is_failure() )
          $this->errException( "ERR_API_INVALID_ARGUMENTS: the given ICCID cannot be activated" , 'VV0066' );
      }

      // create ULTRA user in DB

      $ICCID_18 = substr($ICCID,0,-1);

      // English is the default preferred language
      if ( is_null($preferred_language) || ! $preferred_language )
        $preferred_language = 'EN';

      $params = array(
        "preferred_language"    => $preferred_language,
        "cos_id"                => get_cos_id_from_plan('STANDBY'),
        "activation_cos_id"     => get_cos_id_from_plan( $target_plan ),
        "postal_code"           => $zipcode,
        "country"               => 'USA',
        "plan_state"            => 'Neutral',
        "plan_started"          => 'NULL',
        "plan_expires"          => 'NULL',
        "customer_source"       => 'DEALER',
        "current_iccid"         => $ICCID_18, # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
        "current_iccid_full"    => $ICCID,     # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
        'userid'                => $activation_user_id,
        'promised_amount'       => get_plan_cost_by_cos_id(get_cos_id_from_plan($target_plan)) + $bolt_ons_cost);

      if ( isset( $activation_masteragent ) && $activation_masteragent )
        $params['masteragent'] = $sim->INVENTORY_MASTERAGENT;

      if ( isset( $activation_distributor ) && $activation_distributor )
        $params['distributor'] = $sim->INVENTORY_DISTRIBUTOR;

      if ( isset( $activation_store ) && $activation_store )
        $params['dealer'] = $activation_store;

      $result = create_ultra_customer_db_transaction($params);

      $customer = $result['customer'];

      if ( ! $customer )
        $this->errException( "ERR_API_INTERNAL: DB error - could not create customer" , 'DB0001' );

      customer_reset_first_last_name($customer->CUSTOMER_ID);

      $this->addToOutput('customer_id',$customer->CUSTOMER_ID);

      // handle Flex plan
      if ($isFlexPlan && !empty($invite_code)) {
        $this->joinFamily($customer->CUSTOMER_ID, $invite_code);
      }

      // configure Recurring Bolt Ons

      dlog('',"new_bolt_on_configuration = %s",$new_bolt_on_configuration);

      // modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
      $result_bolt_ons = \update_bolt_ons_values_to_customer_options( $customer->CUSTOMER_ID , array() , $new_bolt_on_configuration , 'DPORTAL' );

      // $result_bolt_ons failures will be ignored

      // save transition info
      set_redis_provisioning_values($customer->CUSTOMER_ID, $activation_masteragent, $activation_agent, $activation_distributor, $activation_store, $activation_user_id, $redis);

      $context          = array('customer_id' => $customer->CUSTOMER_ID );
      $resolve_now      = FALSE;
      $dry_run          = TRUE;
      $plan             = get_plan_name_from_short_name($target_plan);
      $transition_name  = 'Provision '.$plan;

      // test State Transition

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);

      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException( "ERR_API_INTERNAL: state transition error (1)" , 'SM0001' );
      }

      // initiate State Transition, but don't wait for it to resolve

      $dry_run = FALSE;

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);

      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException( "ERR_API_INTERNAL: state transition error (2)" , 'SM0001' );
      }

      reserve_iccid($ICCID);

      // update ULTRA.HTT_ACTIVATION_HISTORY
      $sql = ultra_activation_history_update_query(
        array(
          'customer_id'     => $customer->CUSTOMER_ID,
          'activation_type' => 'NEW',
        )
      );

      if ( !is_mssql_successful(logged_mssql_query($sql)) )
        dlog('',"ULTRA.HTT_ACTIVATION_HISTORY update failed");

      flush_ultra_activation_history_cache_by_customer_id( $customer->CUSTOMER_ID );

      $this->addToOutput('request_id', $result_status['transitions'][0]);

      // handle Flex plan
      if ($isFlexPlan && empty($invite_code)) {
        $this->createFamily($customer->CUSTOMER_ID);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $fraudStatus = $this->isSuccess() ? 'success' : 'error';
    $data = array('source' => json_encode($this->getInputValues()));
    $customer = (isset($customer) && is_object($customer)) ? $customer : null;

    fraud_event($customer, 'dealerportal', 'RequestProvisionNewCustomer', $fraudStatus, $data);

    return $this->result;
  }
}

