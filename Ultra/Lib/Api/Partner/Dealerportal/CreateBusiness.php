<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class CreateBusiness extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CreateBusiness
   * create a new business entity
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__CreateBusiness()
  {
    // initialization
    $parameters = $this->getNamedInputValues();
    $addresses = NULL;

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($parameters['security_token'], __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      // always validate address
      list($addresses, $error) = \Ultra\Lib\Util\validateStreetAddress($parameters);

      // validation was requested
      if ($parameters['validate'])
      {
        // validation failed
        if ($error)
          $this->errException('ERR_API_INVALID_ARGUMENTS: Address (1) is not valid', 'VV0047', $error);

        // validation succeeded: copy corrected address
        foreach ($addresses[0] as $name => $value)
          if (isset($parameters[$name]))
            $parameters[$name] = $value;
        dlog('', 'updated parameters: %s', $parameters);
      }

      // create business
      $parameters['author_id'] = $session['user_id'];
      if ($error = \Ultra\Lib\DB\DealerPortal\createBusiness($parameters))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('address_matches', $addresses);
    return $this->result;
  }
}
