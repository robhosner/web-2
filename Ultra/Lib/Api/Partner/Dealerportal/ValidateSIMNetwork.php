<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidateSIMNetwork extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ValidateSIMNetwork
   *
   * Check the ICCID validity on the MVNE Network.
   *
   * @return Result object
   */
  public function dealerportal__ValidateSIMNetwork ()
  {
    list ( $security_token , $ICCID ) = $this->getInputValues();

    $this->addToOutput('valid_network','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // check ICCID according to its MVNE
      $params = array(
        'actionUUID' => getNewActionUUID('action ' . time()),
        'iccid'  => $ICCID);

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $mwControl->mwCanActivate($params);

      if ( $result->is_failure() )
      {
        dlog('',"MW errors : %s", $result->get_errors());

        if ( $result->is_timeout() )
          $this->errException( "ERR_API_INTERNAL: MW timeout" , 'MW0002' );
        else
          $this->errException( "ERR_API_INTERNAL: MW error - " . implode(' ; ', $result->get_errors()), 'MW0001');
      }
      else
      {
        if ( ! isset( $result->data_array['available'] ) )
          $this->errException( "ERR_API_INTERNAL: MW did not return SIM availability" , 'MW0001' );

        $this->addToOutput( 'valid_network' , ! ! $result->data_array['available'] );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}