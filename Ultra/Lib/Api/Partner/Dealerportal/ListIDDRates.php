<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ListIDDRates extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListIDDRates
   *
   * Provides the calling card rates for the specified product
   *
   * @return Result object
   */
  public function dealerportal__ListIDDRates ()
  {
    list ( $security_token ) = $this->getInputValues();

    $this->addToOutput('rates_list','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $rates_data = htt_card_calling_rates_get_rates_data('ULTRAMOBILE', TRUE);

      if ( $rates_data && is_array($rates_data) && count($rates_data) )
      {
        // convert data to UFT-8
        foreach($rates_data as &$row)
        {
          $row->country = utf8_encode($row->country);
          $row->destination = utf8_encode($row->destination);
        }

        $this->addToOutput('rates_list',$rates_data);

        $this->succeed ();
      }
      else
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}