<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ListSMSCountries extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListSMSCountries
   *
   * Provides a list of all the international locations an Ultra user can SMS.
   *
   * @return Result object
   */
  public function dealerportal__ListSMSCountries ()
  {
    list ( $security_token ) = $this->getInputValues();

    $this->addToOutput('sms_list','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $sms_carriers_data = htt_country_sms_carriers_get_country_list();

      if ( ( ! $sms_carriers_data ) || ( ! count($sms_carriers_data) ) )
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );

      $this->addToOutput('sms_list',$sms_carriers_data);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}