<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class UpdateBusinessParent extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__UpdateBusinessParent
   * update an existing business parent
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__UpdateBusinessParent()
  {
    // initialization
    $parameters = $this->getNamedInputValues(); // business_type, business_code, parent_business_type, parent_dealer_code

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($parameters['security_token'], __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      // DEAL-173: Dealer -> Dealer assignment (also enforced by validation in JSON)
      if ($parameters['business_type'] == 'Dealer' && $parameters['parent_business_type'] == 'Dealer')
      {
        // verify business record
        list($child, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord($parameters['business_type'], $parameters['business_code']);
        if ($error)
          $this->errException('ERR_API_INVALID_ARGUMENTS: An unexpected database error has occurred', 'DB0001', $error);
        # dlog('', 'CHILD: %s', print_r($child, 1));

        // verify parent record
        list($parent, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord($parameters['parent_business_type'], $parameters['parent_business_code']);
        if ($error)
          $this->errException('ERR_API_INVALID_ARGUMENTS: An unexpected database error has occurred', 'DB0003', $error);
        # dlog('', 'PARENT: %s', print_r($parent, 1));

        // parent dealer cannot be a child of another dealer
        if ( ! empty($parent->PARENTDEALERSITEID) && $parent->DEALERSITEID != $parent->PARENTDEALERSITEID)
          $this->errException('ERR_API_INVALID_ARGUMENTS: Hierarchy violation', 'DP0001');

        // update child dealer
        $params = array(
          'parent_business_code'    => $parent->DEALERCD,
          'parent_business_id'      => $parent->DEALERSITEID,
          'active'                  => 0,
          'parent_assigned_date'    => 'NOW',
          'inactive_date'           => 'NOW',
          'inactive_reason_id'      => 8, // 'Merged into 1:1 dealer'
          'author_id'               => $session['user_id'],
          'modification_date'       => 'NOW');
        $primary = array('DealerSiteID' => $child->DEALERSITEID);
        if ($error = \Ultra\Lib\DB\DealerPortal\updateTable('TBLDEALERSITE', $params, $primary))
          $this->errException('ERR_API_INTERNAL: No data has been modified', 'DB0004', $error);

        // update child dealer's children
        $primary = array('ParentDealerSiteID' => $child->DEALERSITEID);
        if ($error = \Ultra\Lib\DB\DealerPortal\updateTable('TBLDEALERSITE', $params, $primary))
          $this->errException('ERR_API_INTERNAL: No data has been modified', 'DB0004', $error);

        // PROD-1743: move demo lines from all children to parent
        list($error, $warning) = \Ultra\Lib\DemoLine\transferDemoLines($parent);
        if ($error)
          $this->errException('ERR_API_INTERNAL: Failed to transfer existing demo line', 'DP0003', $error);
        if ($warning)
          $this->addWarning($warning);

      }
      else
        $this->errExpception('ERR_API_INVALID_ARGUMENTS: Generic internal error', 'IN0002', 'Reserved for future use');

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
