<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ApplyVoiceRecharge extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ApplyVoiceRecharge
   *
   * debit the wallet and add Voice Balances
   *
   * @param: customer_id
   * @param: voice_soc_id
   * @return Result object (plan_state)
   * @author: VYT, 14-01-22, adopted from v1
   */
  public function dealerportal__ApplyVoiceRecharge()
  {
    // init
    list ( $security_token , $customer_id, $voice_soc_id ) = $this->getInputValues();
    $this->addToOutput('plan_state', '');

    try
    {
      // retrieve and validate session
      teldata_change_db(); // connect to the DB
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer info
      teldata_change_db(); // connect to DB

      $customer = get_ultra_customer_from_customer_id($customer_id, array(
        'CUSTOMER_ID',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'preferred_language',
        'plan_state'
      ));

      $account = get_account_from_customer_id( $customer_id, array('COS_ID','BALANCE'));

      if ( ! $customer || ! $account )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found' , 'VV0031');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // verify that the customer is Active
      if ( $customer->plan_state != STATE_ACTIVE )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active', 'IN0001');

      // get the customer plan
      $plan = get_plan_from_cos_id( $account->COS_ID );
      if( empty($plan) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: plan cannot be found.', 'IN0001');

      // get the voice recharge data
      $voice_recharge = find_voice_recharge($plan, $voice_soc_id);
      if (! $voice_recharge)
        $this->errException('ERR_API_INVALID_ARGUMENTS: voice_soc_id not valid', 'VV0101');

      // check redis semaphore
      $redis = new \Ultra\Lib\Util\Redis;
      if ( get_voice_recharge_semaphore( $redis , $customer->CUSTOMER_ID ))
        $this->errException("ERR_API_INTERNAL: a voice recharge has been processed less than 15 minutes ago", 'VV0106');

      // check for sufficient wallet balance for the given voice recharge
      if (( $voice_recharge['cost'] / 100 ) > $account->BALANCE )
        $this->errException("ERR_API_INTERNAL: not enough money to perform this operation", 'VV0103');

        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        $result = $mwControl->mwMakeitsoUpgradePlan(
          array(
            'actionUUID'         => getNewActionUUID('dealerportal ' . time()),
            'msisdn'             => $customer->current_mobile_number,
            'iccid'              => $customer->CURRENT_ICCID_FULL,
            'customer_id'        => $customer->CUSTOMER_ID,
            'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
            'ultra_plan'         => get_plan_from_cos_id($account->COS_ID),
            'preferred_language' => $customer->preferred_language,
            'option'             => 'B-VOICE|' . $voice_recharge['voice_minutes']));

        teldata_change_db(); // connect back to the default DB

      // debit the wallet accordingly
      $result = func_spend_from_balance(
        array(
         'customer_id' => $customer->CUSTOMER_ID,
         'amount'      => ( $voice_recharge['cost'] / 100 ), # in $
         'detail'      => __FUNCTION__,
         'reason'      => 'VOICE Purchase',
         'reference'   => 'VOICE_PAYMENT',
         'source'      => 'SPEND',
         'commissionable'   => 1,
         'reference_source' => create_guid('PHPAPI')));
      if ( count($result['errors']) )
        $this->errException("ERR_API_INTERNAL: could not complete voice recharge process (3)", 'VO0001');

      // record event
      $event_status = log_bucket_event(
        array(
          'action'      => 'Recharge ' . $voice_recharge['voice_minutes'] . ' minutes',
          'customer_id' => $customer->CUSTOMER_ID,
          'soc'         => $voice_soc_id));

      // set Redis semaphore to block another ApplyVoiceRecharge by the same customer for 15 minutes
      set_voice_recharge_semaphore( $redis , $customer->CUSTOMER_ID );

      // send confirmation via SMS
      funcSendExemptCustomerSMSVoiceRecharge(
        array(
          "customer"       => $customer,
          "cost_amount"    => ( $voice_recharge['cost'] / 100 ),
          "wallet_balance" => ( $account->BALANCE - ( $voice_recharge['cost'] / 100 ) ),
          "minutes"        => $voice_recharge['voice_minutes']));

      // done
      $this->addToOutput('plan_state', $customer->plan_state);
      $this->succeed();

    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
