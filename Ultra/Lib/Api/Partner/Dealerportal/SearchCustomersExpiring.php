<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class SearchCustomersExpiring extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SearchCustomersExpiring
   * return a list of expiring subscribers for the authenticated dealer and its children
   * @see SMR-15
   */
  public function dealerportal__SearchCustomersExpiring()
  {
    // init
    list ($security_token, $days_since_suspend, $days_until_expire) = $this->getInputValues();
    $customers = array();
    $record_count = 0;

    try
    {
      // retrieve and validate session from $security_token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);
      $dealer = $session_data['dealer'];

      // get expiring customers
      $customers = get_expiring_subscribers_from_dealer($dealer, $days_since_suspend, $days_until_expire);
      if ( ! count($customers))
        $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');

      $record_count = count($customers);
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('customers', $customers);
    $this->addToOutput('record_count', $record_count);
    return $this->result;
  }
}