<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ChangePlanSuspended extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ChangePlanSuspended
   *
   * change the plan of a Suspended customer to allow re-activation under a different plan
   *
   * @param: customer_id
   * @param: target_plaln
   * @return Result object
   * @author: VYT, 14-01-24, adopted from v1
   */
  public function dealerportal__ChangePlanSuspended()
  {
    // init
    list ( $security_token , $customer_id, $target_plan ) = $this->getInputValues();

    try
    {
      // retrieve and validate session
      teldata_change_db(); // connect to the DB

      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // patch as per PROD-2506
      if ( $target_plan == 'L39' )
        $target_plan = 'D39';

      // get customer info
      $customer = get_customer_from_customer_id($customer_id);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      if ( ! isSameBrand(get_plan_from_cos_id($customer->cos_id), $target_plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous and future plan brands do not match', 'MP0011');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // check customer plan COS_ID
      if ( ! $customer->COS_ID )
        $this->errException('ERR_API_INTERNAL: customer plan could not be determined (1)', 'UN0002');

      // verify that the customer is Suspended
      $state = internal_func_get_state_from_customer_id($customer_id);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: customer state could not be determined', 'IN0002');

      if ( $state['state'] != 'Suspended' )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Suspended', 'IN0001');

      // check customer plan
      $plan = get_plan_from_cos_id( $customer->COS_ID ); # L[12345]9
      if ( ! $plan )
        $this->errException('ERR_API_INTERNAL: customer plan could not be determined (2)', 'UN0001');

      if ( $target_plan == $plan )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer already in plan $plan', 'VV0001');

      // change state and check the result
      $context = array('customer_id' => $customer_id);
      $result = change_suspended_plan($state, $customer, $context, $target_plan);
      if ($result['success'])
        $this->succeed();
      elseif (! empty($result['errors']))
        foreach ($result['errors'] as $error)
          $this->addError($error, 'VV0001');
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
