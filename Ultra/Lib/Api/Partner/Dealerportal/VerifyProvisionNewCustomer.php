<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class VerifyProvisionNewCustomer extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__VerifyProvisionNewCustomer
   *
   * This is the API to be invoked after dealerportal__RequestProvisionNewCustomer in order to get the complete response.
   * Behaves like provisioning__verifyProvisionNewCustomerAsync
   *
   * @return Result object
   */
  public function dealerportal__VerifyProvisionNewCustomer ()
  {
    list ( $security_token , $request_id ) = $this->getInputValues();

    $this->addToOutput('provision_pending','');
    $this->addToOutput('phone_number','');
    $this->addToOutput('provision_status','');

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $transition_result = provision_check_transition($request_id);

      $errors = append_transition_failure_reason($transition_result['errors'],$request_id);

      $this->addToOutput('provision_pending',$transition_result['pending']);
      $this->addToOutput('phone_number',$transition_result['phone_number']);
      $this->addToOutput('provision_status',$transition_result['status']);

      if ( $errors && is_array( $errors ) && count( $errors ) )
        $this->errException( "ERR_API_INTERNAL: provisioning errors - ".implode(" ; ", $errors) , 'PR0001' );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}