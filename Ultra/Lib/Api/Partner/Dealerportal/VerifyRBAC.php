<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class VerifyRBAC extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__VerifyRBAC
   *
   * development API to test RBAC
   *
   * @author VYT, 14-03-03
   */
  public function dealerportal__VerifyRBAC()
  {
    // init
    list ($security_token, $customer_id, $api_name, $plan_state) = $this->getInputValues();

    try
    {
      // retrieve and validate security token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // get customer
      $customer = get_ultra_customer_from_customer_id($customer_id, array('customer_id'));
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // get result
      if ($this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $plan_state, $api_name))
        $this->succeed();
      else
        $this->errException('checkUltraSessionAllowedAPICustomer failed', 'SM0001');
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage ());
    }

    return $this->result;
  }
}