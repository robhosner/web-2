<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Flex;
use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Lib\Services\FamilyAPI;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

require_once 'classes/Provisioning.php';
require_once 'classes/IntraBrand.php';

class RequestProvisionPortedCustomer extends \Ultra\Lib\Api\Partner\Dealerportal
{
  use FlexHandler;
  
  private $familyAPI;

  public function __construct(FamilyAPI $familyAPI)
  {
    parent::__construct();

    $this->familyAPI = $familyAPI;
  }

  /**
   * recordFraudEvent
   * records a fraud event
   *
   * @param  $customer Object [CUSTOMER_ID]
   * @return void
   */
  private function recordFraudEvent($customer)
  {
    $fraudStatus = $this->isSuccess() ? 'success' : 'error';
    $data = array('source' => json_encode($this->getInputValues()));

    fraud_event($customer, 'dealerportal', 'RequestProvisionPortedCustomer', $fraudStatus, $data);
  }

  /**
   * updateBoltOns
   * modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
   * @param  Integer $customer_id
   * @param  Array   $bolt_on_configuration returned from \Provisioning::configureBoltons
   * @return void
   */
  private function updateBoltOns($customer_id, $bolt_on_configuration)
  {
    // $update_bolt_ons_result failures will be ignored
    $update_bolt_ons_result = \update_bolt_ons_values_to_customer_options(
      $customer_id,
      array(),
      $bolt_on_configuration,
      'DPORTAL'
    );
  }

  /**
   * dealerportal__RequestProvisionPortedCustomer
   *
   * This API call is used to create a customer and 'activate' their SIM. This call is ONLY for ported in customers.
   * [ Neutral ] => [ Port-In Requested ]
   * The non-dealerportal version is provisioning__requestProvisionPortedCustomerAsync.
   *
   * @return Result object
   */
  public function dealerportal__RequestProvisionPortedCustomer()
  {
    list (
      $security_token, $ICCID, $zipcode, $target_plan, $preferred_language, $number_to_port,
      $port_account_number , $port_account_password , $bolt_ons, $email, $contact_phone, $invite_code
    ) = $this->getInputValues();

    $this->addToOutput('customer_id', '');
    $this->addToOutput('request_id',  '');

    # trim spaces
    $port_account_number   = trim($port_account_number);
    $port_account_password = trim($port_account_password);

    $customer   = null;
    $request_id = '';
    $context = array(); // port transition context

    $changeStateResult = array();

    // patch as per PROD-2506
    if ( $target_plan == 'L39' )
      $target_plan = 'D39';

    // English is the default preferred language
    if ( ! $preferred_language )
      $preferred_language = 'EN';

    try
    {
      teldata_change_db();

      $isFlexPlan = Flex::isFlexPlan($target_plan);

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // verify that activations are enabled
      if ( ! activations_enabled(TRUE) )
        $this->errException(
          'ERR_API_INTERNAL: ' . \Ultra\UltraConfig\dealer_portal_activations_disabled_msg(),
          'AP0001',
          \Ultra\UltraConfig\dealer_portal_activations_disabled_msg()
        );

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode', $zipcode, 'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // validate ICCID
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID', $ICCID, 'VALID');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // get SIM info
      $sim = get_htt_inventory_sim_from_iccid($ICCID);
      if (! $sim)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      if ( ! \Ultra\UltraConfig\isBrandIdAllowed($sim->BRAND_ID))
        $this->errException('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid', 'IC0003');

      // validate ICCID
      if ( ! validate_ICCID($sim, 1) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used', 'VV0065');

      // check invite code, if given
      if ($isFlexPlan && $invite_code) {
        $this->validateInviteCode($invite_code);
      }

      $activation_agent       = $session_data['dealer'];
      $activation_store       = $session_data['dealer'];
      $activation_masteragent = $session_data['masteragent'];
      $activation_distributor = $session_data['distributor'];
      $activation_user_id     = $session_data['user_id'];

      $redis = new \Ultra\Lib\Util\Redis;

      // verify iccid over network
      $result = \Provisioning::verifyICCIDGoodToActivate($sim->ICCID_FULL, $redis);
      if (count($errors = $result->get_errors()))
      {
        list ($error, $code) = explode('|', $errors[0]);
        $this->errException($error, $code);
      }

      $configureBoltonsResult = \Provisioning::configureBoltOns($bolt_ons);
      if (count($errors = $configureBoltonsResult->get_errors()))
      {
        list ($error, $code) = explode('|', $errors[0]);
        $this->errException($error, $code);
      }

      // store port account credentials for 2 weeks in Redis
      save_port_account( $number_to_port , $port_account_number , $port_account_password );

      $transition_name = '';

      $customer = get_customer_from_msisdn($number_to_port, 'u.CUSTOMER_ID, u.BRAND_ID, plan_state, u.CURRENT_ICCID, a.COS_ID, a.ACCOUNT');

      if (\IntraBrand::isIntraBrandPortByMSISDNAndSIM($number_to_port, $sim)) {
        // regular intraport
        $doIntraport = true;
      } else if ($customer && (Flex::isFlexPlan(get_plan_from_cos_id($customer->COS_ID)) && !Flex::isFlexPlan($target_plan))) {
        // flex -> non flex plan
        $doIntraport = true;
      } else if ($customer && (!Flex::isFlexPlan(get_plan_from_cos_id($customer->COS_ID)) && Flex::isFlexPlan($target_plan))) {
        // non flex -> flex plan
        $doIntraport = true;
      } else {
        // no intraport
        $doIntraport = false;
      }

      // check for intra-brand port or flex plan or if already on flex
      if ($doIntraport) {
        \logit("MSISDN: $number_to_port is intra-brand port");

        if ( ! $customer)
          $this->errException('ERR_API_INTERNAL: Could not find customer by MSISDN for intra-port', 'VV0031');
        $context['old_iccid'] = luhnenize($customer->CURRENT_ICCID);
        $context['old_brand_id'] = $customer->BRAND_ID;

        if ( ! in_array($customer->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED)))
          $this->errException('ERR_API_INTERNAL: port-In denied due to customer state', 'IN0001');

        if ($customer->ACCOUNT != $port_account_number) {
          $this->errException('ERR_API_INTERNAL: port-In denied due to invalid account number', 'PO0009');
        }

        // intra port operations, clone and soft cancel customer
        $result = \Provisioning::intraBrandPortOperations($customer->CUSTOMER_ID, $sim);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        $customer = $result->get_data_key('customer');

        // assign dealer to newly created customer
        assign_ultra_customer_dealer(array(
          'activation_type'       => 'INTRA-PORT',
          'customer_id'           => $customer->CUSTOMER_ID,
          'current_iccid_full'    => $customer->CURRENT_ICCID_FULL,
          'current_mobile_number' => $customer->current_mobile_number,
          'cos_id'                => $customer->COS_ID,
          'dealer'                => $activation_store,
          'masteragent'           => $activation_masteragent,
          'distributor'           => $activation_distributor
        ));

        $transition_name = 'Provision Intra Port ' . get_plan_name_from_short_name($target_plan);

        if (!empty($contact_phone)) {
          if (!(new CustomerRepository())->addCustomerContactPhone($customer->CUSTOMER_ID, $contact_phone)) {
            $this->errException('ERR_API_INTERNAL: Cannot add customer contact_phone', 'DB0001');
          }
        }
      }
      // end intra brand port
      else
      {
        // not currently used
        // $customer_source = get_customer_source($activation_masteragent, $activation_store, $activation_user_id);

        // verify number over network
        $result = \Provisioning::verifyPortInEligibility($number_to_port);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        // verify number not in system
        $result = \Provisioning::verifyNumberNotInSystem($number_to_port);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        $promised_amount = get_plan_cost_by_cos_id(get_cos_id_from_plan($target_plan)) + $configureBoltonsResult->get_data_key('bolt_ons_cost');

        $params = array(
          'activation_type'       => 'PORT',
          'dealer'                => $activation_store,
          'preferred_language'    => $preferred_language,
          'cos_id'                => get_cos_id_from_plan('STANDBY'),
          'activation_cos_id'     => get_cos_id_from_plan( $target_plan ),
          'postal_code'           => $zipcode,
          'country'               => 'USA',
          'plan_state'            => 'Neutral',
          'plan_started'          => 'NULL',
          'plan_expires'          => 'NULL',
          'customer_source'       => 'DEALER',
          'current_iccid'         => substr($ICCID, 0, -1), // Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
          'current_iccid_full'    => $ICCID,                // Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
          'current_mobile_number' => $number_to_port,
          'userid'                => $activation_user_id,
          'promised_amount'       => $promised_amount
        );

        // create new customer
        if ( ! empty($sim->INVENTORY_MASTERAGENT))
          $params['masteragent'] = $sim->INVENTORY_MASTERAGENT;

        if ( ! empty($sim->INVENTORY_DISTRIBUTOR))
          $params['distributor'] = $sim->INVENTORY_DISTRIBUTOR;

        if ( isset( $activation_store ) && $activation_store )
          $params['dealer'] = $activation_store;

        if (!empty($email)) {
          $params['e_mail'] = $email;
        }

        if (!empty($contact_phone)) {
          $params['contact_phone'] = $contact_phone;
        }

        $result   = create_ultra_customer_db_transaction($params);
        $customer = $result['customer'];

        if ( ! $customer )
          $this->errException('ERR_API_INTERNAL: DB error - could not create customer', 'DB0001');

        customer_reset_first_last_name($customer->CUSTOMER_ID);

        $transition_name = 'Request Port ' . get_plan_name_from_short_name($target_plan);
      }

      // handle Flex plan
      if ($isFlexPlan && !empty($invite_code)) {
        $this->joinFamily($customer->CUSTOMER_ID, $invite_code);
      }

      $this->updateBoltOns($customer->CUSTOMER_ID, $configureBoltonsResult->get_data_key('bolt_on_configuration'));

      // save activation attribution info in redis before attempting state transition
      set_redis_provisioning_values(
        $customer->CUSTOMER_ID,
        $activation_masteragent,
        $activation_agent,
        $activation_distributor,
        $activation_store,
        $activation_user_id
      );

      // setup context for state transition
      $context['customer_id']              = $customer->CUSTOMER_ID;
      $context['port_in_msisdn']           = $number_to_port;
      $context['port_in_iccid']            = $ICCID;
      $context['port_in_account_number']   = $port_account_number;
      $context['port_in_account_password'] = $port_account_password;
      $context['port_in_zipcode']          = $zipcode;
      $context['masteragent']              = $activation_masteragent;
      $context['agent']                    = $activation_agent;
      $context['distributor']              = $activation_distributor;
      $context['store']                    = $activation_store;
      $context['user_id']                  = $activation_user_id;

      // performs State Transition [ Neutral ] => [ Port-In Requested ]
      $changeStateResult = change_state($context, FALSE, $transition_name, 'take transition', FALSE, 1);
      if ( $changeStateResult['success'] != 1 )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException( 'ERR_API_INTERNAL: state transition error (2)', 'SM0001' );
      }

      reserve_iccid($ICCID);

      // record MSISDN into ULTRA.HTT_ACTIVATION_HISTORY
      \Provisioning::updateActivationHistoryForMsisdn($customer->CUSTOMER_ID, $number_to_port);

      // add transition UUID to result
      $array_values = array_values($changeStateResult['transitions']);
      $request_id   = end($array_values);

      // memorize $request_id in Redis by customer_id for dealerportal__SearchActivationHistoryDealer
      $redis_port = new \Ultra\Lib\Util\Redis\Port();
      $redis_port->setRequestId( $customer->CUSTOMER_ID , $request_id );

      // handle Flex plan
      if ($isFlexPlan && empty($invite_code)) {
        $this->createFamily($customer->CUSTOMER_ID);
      }

      $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
      $this->addToOutput('request_id',  $request_id);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    $customer = (isset($customer) && is_object($customer)) ? $customer : null;
    $this->recordFraudEvent($customer);

    return $this->result;
  }
}

