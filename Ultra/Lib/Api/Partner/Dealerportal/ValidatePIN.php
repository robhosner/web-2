<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidatePIN extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ValidatePIN
   *
   * Verify that the PIN card is in our system, it has not been used and not disabled.
   * TODO: implement rate limiting.
   *
   * @return Result object
   */
  public function dealerportal__ValidatePIN ()
  {
    list ( $security_token , $pin ) = $this->getInputValues();

    $this->addToOutput('used_date','');
    $this->addToOutput('used_msisdn','');
    $this->addToOutput('pin_value','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $pin_validation = func_validate_pin_cards(
        array(
          'pin_list' => array($pin)
        )
      );

      if ( $pin_validation['at_least_one_not_found'] )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: the PIN is invalid."    , 'PI0001' );

      if ( $pin_validation['at_least_one_at_foundry'] )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: the PIN is not active." , 'PI0002' );

      if ( count($pin_validation['errors']) )
      {
        dlog('',"%s",$pin_validation['errors']);

        $this->errException( "ERR_API_INVALID_ARGUMENTS: validation failed."     , 'PI0003' );
      }

      if ( $pin_validation['at_least_one_customer_used'] )
      {
        // fill 'used_date' and 'used_msisdn'

        if ( $pin_validation['result'][ $pin ]['customer_used'] )
        {
          $customer = get_ultra_customer_from_customer_id( $pin_validation['result'][ $pin ]['customer_used'] , array('current_mobile_number') );

          if ( $customer )
            $this->addToOutput('used_msisdn',$customer->current_mobile_number);
        }

        $this->addToOutput('used_date', get_date_from_full_date( $pin_validation['result'][ $pin ]['last_changed_date'] ) );

        $this->errException( "ERR_API_INVALID_ARGUMENTS: the PIN is already used." , 'PI0004' );
      }
      else
      {
        // fill 'pin_value'

        $this->addToOutput('pin_value', ( $pin_validation['result'][$pin]['pin_value'] * 100 ) );

        $this->succeed ();
      }
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}