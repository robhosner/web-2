<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class CheckZipCode extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckZipCode
   *
   * Check to see if the ZIP Code is in the Activation Footprint.
   *
   * @return Result object
   */
  public function dealerportal__CheckZipCode ()
  {
    list ( $security_token , $zipcode ) = $this->getInputValues();

    $this->addToOutput('zipcode_valid','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $htt_coverage_info_select_query = htt_coverage_info_select_query( array('zip_code' => $zipcode) );

      $coverage_data = \Ultra\Lib\DB\fetch_objects($htt_coverage_info_select_query);

      if ( $coverage_data && is_array($coverage_data) && count($coverage_data) )
        $this->addToOutput('zipcode_valid',TRUE);
      else
        $this->addToOutput('zipcode_valid',FALSE);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}