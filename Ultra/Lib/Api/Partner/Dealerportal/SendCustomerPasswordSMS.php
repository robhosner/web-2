<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class SendCustomerPasswordSMS extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SendCustomerPasswordSMS
   *
   * send a temporary password to a customer via SMS for dealer authorization
   *
   * @param int customer_id
   * @return Result object
   * @author VYT, 14-02-13, adopted from v1
   */
  public function dealerportal__SendCustomerPasswordSMS()
  {
    // init
    list ( $security_token , $customer_id ) = $this->getInputValues();

    try
    {
      // retrieve and validate session
      teldata_change_db();
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer
      $customer = get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number'));
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // get customer state
      $state = internal_func_get_state_from_customer_id($customer_id);
      if ( ! $state )
        $this->errException( 'ERR_API_INTERNAL: customer state could not be determined' , 'UN0001' );

      // reject Cancelled customers
      if ( $state['state'] == 'Cancelled' )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer account has been cancelled', 'IN0001');

      // reject customers without MSISDN (Neutral or Pre-Funded)
      if (! trim($customer->current_mobile_number))
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer does not have a mobile number', 'IN0001');

      // generate a 6 digit password, store in Redis with 15 minute TTL
      $password = $this->createTempPassword($customer_id);

      // set password via SMS
      $result = funcSendExemptCustomerSMSTempPasswordDealer(array('customer_id' => $customer_id, 'temp_password' => $password));
      if (! $result['sent'] || count($result['errors']))
        $this->errException('ERR_API_INTERNAL: SMS delivery error', 'SM0002');

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

  /**
   * Stores temporary 6 digit password for customer_id in redis
   *
   * @param  integer $customer_id
   * @return integer $password
   */
  private function createTempPassword($customer_id)
  {
    $password = mt_rand(100000, 999999);
    $temp_password_key = 'temp_password/' . $customer_id;
    $redis = new \Ultra\Lib\Util\Redis;
    $redis->set($temp_password_key, $password, 900);

    return $password;
  }
}