<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class CheckSession extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckSession
   * validate existing session and refresh its expiration
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__CheckSession()
  {
    // initialization
    list ($security_token) = $this->getInputValues();

    try
    {
      // retrieve existing session
      $session = $this->getUltraSessionData($security_token);
      if ( ! $session || ! $session['is_valid'])
      {
        dlog('', 'WARNING: invalid security token %s', $security_token);
        $this->errException('ERR_API_INTERNAL: no login session found for the given security_token', 'SE0002');
      }
      list($businessType, $accessRole) = explode(' ', $session['role']);

      $this->addToOutput('reset_password', $session['reset_password']);
      $this->addToOutput('access_role', $accessRole);
      $this->addToOutput('user_id', $session['user_id']);
      $this->addToOutput('business_type', $businessType);
      $this->addToOutput('business_code', $session['business_code']);
      $this->addToOutput('business_name', $session['business_name']);

      // reset session expiration
      $this->resetSessionExpiration($security_token, $session);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
