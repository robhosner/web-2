<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ReplaceSIMCard extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ReplaceSIMCard
   *
   * activate customer's MSISDN on a different SIM
   *
   * @param int customer_id
   * @param string ICCID: new SIM card
   * @return Result object
   * @author VYT, 14-02-25
   */
  public function dealerportal__ReplaceSIMCard()
  {
    // init
    list ($security_token, $customer_id, $iccid) = $this->getInputValues();

    try
    {
      // retrieve and validate security token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // check if activations are available (per MVNO-2296)
      if ( ! activations_enabled(TRUE) )
        $this->errException( 'ERR_API_INTERNAL: ' . \Ultra\UltraConfig\dealer_portal_activations_disabled_msg() , 'AP0001' , \Ultra\UltraConfig\dealer_portal_activations_disabled_msg() );

      // get customer
      $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_state', 'current_mobile_number', 'CUSTOMER_ID', 'CURRENT_ICCID_FULL'));
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      //get customer cos_id
      $customer_plan = get_customer_plan($customer_id);
      if( ! is_array($customer_plan) && is_null($customer_plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer plan not found', 'VV0031');
      
       $customer->COS_ID = $customer_plan['cos_id'];

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // do not allow SIM replacement during MISO tasks
      if ( \Ultra\Lib\MVNE\exists_open_task( $customer_id ) )
        $this->errException(
          'ERR_API_INTERNAL: The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.',
          'MM0001',
          'The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.'
        );

      // get and validate customer state
      $state = internal_func_get_state_from_customer_id($customer_id);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: customer state could not be determined' , 'UN0001' );

      // PROD-511: prohibit modifications to in-Active subscribers
      if ($state['state'] !== 'Active')
        $this->errException('ERR_API_INVALID_ARGUMENTS: Subscriber must be active to change SIM or phone number.', 'IN0001');

      // validate Ultra SIM
      $newSim = get_htt_inventory_sim_from_iccid($iccid);
      if ( !$newSim || !validate_ICCID($newSim, TRUE) || is_multi_month_sim($iccid) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid ICCID', 'VV0013');

      // validate that old and new ICCIDs belong to same BRAND
      if ( ! isSameBrandByICCID($customer->CURRENT_ICCID_FULL, $iccid))
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given new_ICCID is of an INVALID BRAND type', 'IC0003');

      // block replacement ORANGE sim
      if (strtoupper($newSim->PRODUCT_TYPE) == 'ORANGE')
        $this->errException('Cannot replace with Orange SIM', 'IN0015');

        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        // confirm that new SIM is good
        $result = $mwControl->mwCanActivate(
          array(
            'actionUUID' => getNewActionUUID('dealerportal ' . time()),
            'iccid'      => $iccid));

        if ($result->is_failure() || ! isset($result->data_array['available']))
          $this->errException('ERR_API_INVALID_ARGUMENTS: cannot activate this ICCID (2)', 'VV0016');

        // invoke MW command
        $result = $mwControl->mwChangeSIM(
          array(
            'actionUUID'         => getNewActionUUID('dealerportal ' . time()),
            'msisdn'             => normalize_msisdn($customer->current_mobile_number, TRUE),
            'old_iccid'          => $customer->CURRENT_ICCID_FULL,
            'new_iccid'          => $iccid,
            'customer_id'        => $customer_id));

        dlog('',"mwChangeSIM result data = %s",$result->data_array);

        // the MW call failed
        if ( ! $result->is_success() )
        {
          $errors = $result->get_errors();

          dlog('',"mwChangeSIM result errors = %s",$errors);

          $this->errException($errors[0], 'MW0003');
        }

        // the MW ChangeSIM command failed
        if ( ! $result->data_array['success'] )
        {
          $errors = $result->data_array['errors'];

          dlog('',"mwChangeSIM result data errors = %s",$errors);

          $this->errException($errors[0], 'MW0003');
        }

        teldata_change_db();

      // side effects: HTT_CUSTOMERS_OVERLAY_ULTRA and HTT_INVENTORY_SIM
      $errors = callbackChangeSIM($iccid, $customer->CUSTOMER_ID);
      if (count($errors))
        $this->errException($errors[0], 'MW0001');

      $description = 'Replace SIM';
      $entry_type  = 'SIM_REPLACEMENT';

      // save into HTT_BILLING_HISTORY
      $history_params = array(
        'customer_id'            => $customer->CUSTOMER_ID,
        'date'                   => 'now',
        'cos_id'                 => $customer->COS_ID,
        'entry_type'             => $entry_type,
        'stored_value_change'    => 0,
        'balance_change'         => 0,
        'package_balance_change' => 0,
        'charge_amount'          => 0,
        'reference'              => 'dealerportal__ReplaceSIMCard',
        'reference_source'       => 'DEALERPORTAL',
        'detail'                 => 'dealerportal__ReplaceSIMCard',
        'description'            => $description,
        'result'                 => 'COMPLETE',
        'source'                 => $entry_type,
        'is_commissionable'      => 0,
        'terminal_id'            => '0'
      );

      $query = htt_billing_history_insert_query($history_params);
      if (! is_mssql_successful(logged_mssql_query($query)))
        $this->errException('ERR_API_INTERNAL: failed to update customer info', 'DB0001');

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage ());
    }

    return $this->result;
  }
}