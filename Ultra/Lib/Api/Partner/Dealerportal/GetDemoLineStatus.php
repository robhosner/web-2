<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';
require_once 'Ultra/Lib/DemoLine.php';

class GetDemoLineStatus extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetDemoLineStatus
   * return status of demo lines for a dealer
   * @see: DEMO-1
   * @return Result object
   */
  public function dealerportal__GetDemoLineStatus()
  {
    list($security_token) = $this->getInputValues();
    $demo_lines = array();
    $participating = 0;

    try
    {
      // retrieve and validate session
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // try getting cached data
      $redis = new \Ultra\Lib\Util\Redis;
      $key = DEMO_LINE_CACHE_KEY . $session_data['dealer'];
      if ($cache = $redis->get($key))
        $demo_lines = json_decode($cache);

      // get lines from DB
      else
      {
        // get demo line status from ULTRA.DEMO_LINE
        $query = \Ultra\Lib\DB\makeSelectQuery(
          'ULTRA.DEMO_LINE',
          NULL,
          array('DEMO_LINE_ID', 'CUSTOMER_ID', 'STATUS', 'ACTIVATIONS_NEEDED', 'CLAIM_BY_DATE', 'INVALID_DATE'),
          array('DEALER_ID' => $session_data['dealer'], 'STATUS' => array(DEMO_STATUS_ELIGIBLE, DEMO_STATUS_ACTIVE, DEMO_STATUS_WARNING)),
          NULL,
          NULL,
          TRUE);
        $lines = mssql_fetch_all_objects(logged_mssql_query($query));

        // DEMO-10: if less then 2 active or eligible lines found then add unclaimed
        if (count($lines) < 2)
        {
          $query = \Ultra\Lib\DB\makeSelectQuery(
            'ULTRA.DEMO_LINE',
            2 - count($lines),
            array('DEMO_LINE_ID', 'CUSTOMER_ID', 'STATUS', 'ACTIVATIONS_NEEDED', 'CLAIM_BY_DATE', 'INVALID_DATE'),
            array('DEALER_ID' => $session_data['dealer'], 'STATUS' => DEMO_STATUS_UNCLAIMED, 'CLAIM_BY_DATE' => 'last_14_days'),
            NULL,
            'DEMO_LINE_ID DESC',
            TRUE);
          $extra = mssql_fetch_all_objects(logged_mssql_query($query));
          foreach ($extra as $line)
            $lines[] = $line;
        }

        // DEMO-10: if still less than 2 lines then also include invalid
        if (count($lines) < 2)
        {
          $query = \Ultra\Lib\DB\makeSelectQuery(
            'ULTRA.DEMO_LINE',
            2 - count($lines),
            array('DEMO_LINE_ID', 'CUSTOMER_ID', 'STATUS', 'ACTIVATIONS_NEEDED', 'CLAIM_BY_DATE', 'INVALID_DATE'),
            array('DEALER_ID' => $session_data['dealer'], 'STATUS' => DEMO_STATUS_INVALID, 'INVALID_DATE' => 'last_14_days'),
            NULL,
            'DEMO_LINE_ID DESC',
            TRUE);
          $extra = mssql_fetch_all_objects(logged_mssql_query($query));
          foreach ($extra as $line)
            $lines[] = $line;
        }

        // get MSISDN and ICCID for each line
        foreach ($lines as $line)
        {
          // default details
          $line->MSISDN = NULL;
          $line->ICCID = NULL;
  
          // only 'active' and 'warning' lines have MSISDN and ICCID
          if ($line->STATUS == DEMO_STATUS_ACTIVE || $line->STATUS == DEMO_STATUS_WARNING)
            if ($details = get_ultra_customer_from_customer_id($line->CUSTOMER_ID, array('current_mobile_number', 'CURRENT_ICCID_FULL')))
            {
              $line->MSISDN = $details->current_mobile_number;
              $line->ICCID = $details->CURRENT_ICCID_FULL;
            }
            else // database inconsistency
              $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

          $demo_lines[] = $line;
        }

        // cache result in Redis
        $redis->set($key, json_encode($demo_lines), DEMO_LINE_CACHE_TTL);
      }

      //check dealer list and compare against current dealer_code from
      $dealer_list = \Ultra\UltraConfig\dealer_portal_demo_line_dealers();
      if (empty($session_data['dealer_code']))
      {
        if ( ! $info = \Ultra\Lib\DB\Celluphone\getDealerInfo(NULL, $session_data['dealer']))
          $this->errException('ERR_API_INTERNAL: No celluphone dealer info found', 'ND0003');
        $session_data['dealer_code'] = $info->dealerCode;
      }

      if (empty($dealer_list) || in_array($session_data['dealer_code'], $dealer_list))
        $participating = 1;

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('demo_line', $demo_lines);
    $this->addToOutput('participating', $participating);
    return $this->result;
  }
}
