<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class CheckDealerAccess extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckDealerAccess
   * check if dealer has access to customer
   * @return Result object
   */
  public function dealerportal__CheckDealerAccess()
  {
    list ($security_token, $customer_id) = $this->getInputValues();

    $has_access = NULL;

    try {
      // validate dealer portal session
      list ($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code) $this->errException($error, $error_code);

      $has_access = $this->checkDealerAccessToCustomer($session_data, $customer_id);

      $this->addToOutput('has_access', $has_access);
      $this->succeed();
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
