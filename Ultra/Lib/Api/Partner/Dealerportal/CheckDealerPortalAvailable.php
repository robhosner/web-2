<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class CheckDealerPortalAvailable extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckDealerPortalAvailable
   *
   * Returns Dealer Portal settings.
   *
   * @return Result object
   */
  public function dealerportal__CheckDealerPortalAvailable ()
  {
    $error_code = '';

    $this->addToOutput('enabled_uat','');
    $this->addToOutput('enabled_stage','');
    $this->addToOutput('enabled_prod','');
    $this->addToOutput('disabled_message','');
    $this->addToOutput('enabled_tsp', '');

    try
    {
      // connect to the DB
      teldata_change_db();

      $this->addToOutput('enabled_uat',      \Ultra\UltraConfig\dealer_portal_enabled_uat() );
      $this->addToOutput('enabled_stage',    \Ultra\UltraConfig\dealer_portal_enabled_stage() );
      $this->addToOutput('enabled_prod',     \Ultra\UltraConfig\dealer_portal_enabled_prod() );
      $this->addToOutput('disabled_message', \Ultra\UltraConfig\dealer_portal_disabled_msg() );
      $this->addToOutput('enabled_tsp', \Ultra\UltraConfig\dealer_portal_enabled_tsp());

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}