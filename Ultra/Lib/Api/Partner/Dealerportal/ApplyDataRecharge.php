<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ApplyDataRecharge extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ApplyDataRecharge
   *
   * applies the given Data Recharge to the customer account
   *
   * @param: customer_id
   * @param: data_soc_id
   * @return Result object
   * @author: VYT, 14-01-21, adopted from portal__ApplyDataRecharge
   */
  public function dealerportal__ApplyDataRecharge()
  {
    // init API
    list ( $security_token , $customer_id, $data_soc_id ) = $this->getInputValues();

    $this->addToOutput('plan','');

    try
    {
      // retrieve and validate session
      teldata_change_db(); // connect to the DB
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer info
      $account = get_account_from_customer_id($customer_id, array('CUSTOMER_ID','COS_ID','BALANCE'));
      if ( ! $account )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found' , 'VV0031');

      $state = internal_func_get_state_from_customer_id($account->CUSTOMER_ID);

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $state['state'], __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // verify that the customer is Active
      if ( $state['state'] != STATE_ACTIVE )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      // get recharge data for this plan
      $plan = get_plan_from_cos_id($account->COS_ID);
      if(empty($plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: plan cannot be found.', 'IN0001');

      $data_recharge = find_data_recharge($plan, $data_soc_id);
      if ( ! $data_recharge )
        $this->errException('ERR_API_INVALID_ARGUMENTS: data_soc_id not valid', 'VV0101');

      // check redis semaphore
      $redis = new \Ultra\Lib\Util\Redis;
      if ( get_data_recharge_semaphore( $redis , $account->CUSTOMER_ID ) )
        $this->errException("ERR_API_INTERNAL: a data recharge has been processed less than 15 minutes ago.", 'VV0102');

      // check for sufficient wallet balance for the given data SOC
      if ( $data_recharge['cost'] > $account->BALANCE )
        $this->errException('ERR_API_INTERNAL: not enough money to perform this operation', 'VV0103');

      // initialize the context for the sequence of actions we will generate
      $context = array('customer_id' => $account->CUSTOMER_ID);
      $action_seq = 0;
      $action_transaction = NULL; # no TRANSITION_UUID yet

      // add action to validate balance
      $result_status = append_make_funcall_action(
        'assert_balance',
        $context,
        $action_transaction,
        $action_seq,
        ( $data_recharge['cost'] * 100 ),
        NULL,
        NULL,
        NULL,
        'portal__ApplyDataRecharge');

      if ( ! $result_status['success'] )
        $this->errException('ERR_API_INTERNAL: unexpected error (1)', 'SM0001');

      // enqueue to the TRANSITION_UUID created with the previous append_make_funcall_action
      $context['transition_id'] = $result_status['transitions'][0];

      $action_seq++;

/*
A-DATA-BLK         MB

L19 , L29       => 50,250,500
L39 , L49 , L59 => 500
*/
        $result_status = append_make_funcall_action(
          'mvneMakeitsoUpgradePlan',
          $context,
          $action_transaction,
          $action_seq,
          DEFAULT_ACC_DATA_ADD_ON . '|' . $data_recharge['MB'] // example: 'A-DATA-BLK|250'
        );

        if ( ! $result_status['success'] )
          $this->errException('ERR_API_INTERNAL: unexpected error (4)', 'SM0001');

      $action_seq++;

      // debit the wallet accordingly, including the name of the data recharge in the HTT_BILLING_LOG entry.
      $result_status = append_make_funcall_action(
        'spend_from_balance',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge['cost'], # amount in dollars
        __FUNCTION__,           # detail
        'DATA Purchase',        # reason
        create_guid('PHPAPI')); # reference source

      if ( ! $result_status['success'] )
        $this->errException('ERR_API_INTERNAL: unexpected error (4)', 'SM0001');
      $action_seq++;

      // send SMS to customer
      $result_status = append_make_funcall_action(
        'send_sms_data_recharge',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge["MB"]);
      if ( ! $result_status['success'] )
        $this->errException('ERR_API_INTERNAL: unexpected error (5)', 'SM0001');
      $action_seq++;

      // track Data Socs to htt_customers_overlay_ultra
      $result_status = append_make_funcall_action(
        'record_customer_soc',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge['data_soc']);
      if ( ! $result_status['success'] )
        $this->Exception('ERR_API_INTERNAL: unexpected error (6)', 'SM0001');

      unreserve_transition_uuid_by_pid($context['transition_id']);

      // record event
      $event_status = log_data_event(
        array(
          'action'      => 'Recharge ' . $data_recharge['MB'] . 'MB',
          'customer_id' => $account->CUSTOMER_ID,
          'soc'         => $data_recharge['data_soc']));

      // set Redis semaphore to block another ApplyDataRecharge by the same customer for 15 minutes
      set_data_recharge_semaphore($redis, $account->CUSTOMER_ID);

      // set Redis semaphore to block SMS from notification__DataNotificationHandler for 5 minutes
      set_data_recharge_notification_delay($redis, $account->CUSTOMER_ID);

      // success
      $this->addToOutput('plan', $plan);
      $this->succeed();

    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}