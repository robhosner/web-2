<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class RequestProvisionPortedCustomerCancel extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__RequestProvisionPortedCustomerCancel
   *
   * Cancels a Port Request
   *
   * @return Result object
   */
  public function dealerportal__RequestProvisionPortedCustomerCancel()
  {
    list ( $security_token , $ICCID , $number_to_port ) = $this->getInputValues();

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // retrieve the customer associated with $ICCID
      $customer = get_customer_from_iccid( $ICCID );

      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      // check if intra-brand port, cannot cancel
      $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);
      if ($sim && $customer->BRAND_ID != $sim->BRAND_ID)
        $this->errException('ERROR_API_INTERNAL: The porting attempt cannot be cancelled', 'PO0004');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer( $session_data , $customer->CUSTOMER_ID, $customer->plan_state, __FUNCTION__ ) )
        $this->errException( "ERR_API_INTERNAL: API execution not allowed for the given caller" , 'SE0005' );

      // check port status and mark for ABORT if not started
      if ( ! $redis = new \Ultra\Lib\Util\Redis)
        $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002');
      $key = "ultra/port/status/$number_to_port";
      if ( ! $portStatus = $redis->get($key))
      {
        $redis->set($key, 'ABORT', 60 * 60);
      }
      // otherwise attemtp to cancel a pending port
      else
      {
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
        $result = $mwControl->mwCancelPortIn(
          array(
            'actionUUID'         => $this->getRequestId(),
            'msisdn'             => $number_to_port));

        if ( $result->is_failure() )
        {
          dlog('',"MW errors : %s", $result->get_errors());

          if ( $result->is_timeout() )
            $this->errException( "ERR_API_INTERNAL: MW timeout" , 'MW0002' );
          else
            $this->errException( "ERR_API_INTERNAL: MW error - " . implode(' ; ', $result->get_errors()), 'MW0001');
        }
        dlog('',"mwCancelPortIn returned %s",$result->data_array);

        if ( empty( $result->data_array['success'] ) || ! $result->data_array['success'] )
        {
          if ( isset( $result->data_array['errors'] ) && is_array( $result->data_array['errors'] ) )
            $this->errException( "ERR_API_INTERNAL: MW error - " . implode(' ; ', $result->data_array['errors']) , 'MW0001');
          else
            $this->errException( "ERR_API_INTERNAL: MW error" , 'MW0001');
        }

        // transition the customer to 'Cancelled'
        $context = array('customer_id' => $customer->customer_id);

        $result_status = cancel_account($customer,$context,NULL,'dealerportal__RequestProvisionPortedCustomerCancel','dealerportal__RequestProvisionPortedCustomerCancel','dealerportal API');

        if ( count($result_status['errors']) )
          $this->errException( "ERR_API_INTERNAL: state transition error" , 'SM0001' );

        // update ULTRA.HTT_ACTIVATION_HISTORY.FINAL_STATE to 'Port-In Cancelled'
        $sql = ultra_activation_history_update_query(
          array(
            'customer_id'    => $customer->customer_id,
            'final_state'    => 'Port-In Cancelled'
          )
        );

        if ( ! is_mssql_successful(logged_mssql_query($sql)) )
          $this->addWarning( "Error while trying to update ULTRA.HTT_ACTIVATION_HISTORY" );

        flush_ultra_activation_history_cache_by_customer_id( $customer->CUSTOMER_ID );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
