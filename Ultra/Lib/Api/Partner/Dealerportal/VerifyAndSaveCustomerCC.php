<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class VerifyAndSaveCustomerCC extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__VerifyAndSaveCustomerCC
   *
   * Set credit card fields for a customer after appropriate tokenization and validation.
   *
   * @return Result object
   */
  public function dealerportal__VerifyAndSaveCustomerCC ()
  {
    list (
      $security_token , $customer_id , $cc_name , $cc_address1 , $cc_address2 , $cc_city , $cc_country ,$cc_state_or_region ,
      $cc_postal_code , $account_cc_exp , $account_cc_cvv , $account_cc_number , $token , $processor
    ) = $this->getInputValues();

    $error_code = '';

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer data
      $customer = get_customer_from_customer_id( $customer_id );

      if ( ! $customer )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer( $session_data , $customer->CUSTOMER_ID , $customer->plan_state , __FUNCTION__ ) )
        $this->errException( "ERR_API_INTERNAL: API execution not allowed for the given caller" , 'SE0005' );

      // validate and store credit card info - this includes tokenization
      if (!empty($processor)) {
        // use name from config
        $processorConfig = \Ultra\UltraConfig\getCCProcessorByName($processor);
        if (!$processorConfig) {
          throw new \Exception('Missing processor config');
        } else {
          $processor = $processorConfig['name'];
        }
      }
      
      $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
        array(
          'customer_id'    => $customer->CUSTOMER_ID,
          'cc_number'      => $account_cc_number,
          'cvv'            => $account_cc_cvv,
          'expires_date'   => $account_cc_exp,
          'cc_address1'    => $cc_address1,
          'cc_address2'    => $cc_address2,
          'cc_postal_code' => $cc_postal_code,
          'cc_name'        => $cc_name,
          'cc_city'        => $cc_city,
          'cc_country'     => $cc_country,
          'cc_state_region' => $cc_state_or_region,
          'last_name'       => $customer->LAST_NAME,
          'token'           => $token,
          'processor'       => $processor
        )
      );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $userErrors = isset($result->data_array['user_errors'][$customer->preferred_language]) ? $result->data_array['user_errors'][$customer->preferred_language] : null;

        $this->errException($errors[0], $result->data_array['error_codes'][0], $userErrors);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}