<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Lib\Api\Traits\CCHandler;
use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCardValidator;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\SaveMethodFailedException;
use Ultra\Exceptions\UnhandledCCProcessorException;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Utilities\Common;

/**
 * Class SaveCreditCard
 * @package Ultra\Lib\Api\Partner\Dealerportal
 */
class SaveCreditCard extends Dealerportal
{
  use CCHandler;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var CreditCardRepository
   */
  private $cardRepository;

  /**
   * @var CreditCardValidator
   */
  private $creditCardValidator;
  
  /**
   * @var Common
   */
  private $utilities;

  /**
   * SaveCreditCard constructor.
   * @param Common $utilities
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param CreditCardRepository $cardRepository
   * @param CreditCardValidator $creditCardValidator
   * @internal param Session $session
   */
  public function __construct(
    Common $utilities,
    CustomerRepository $customerRepository,
    Configuration $configuration,
    CreditCardRepository $cardRepository
  )
  {
    $this->utilities = $utilities;
    $this->customerRepository = $customerRepository;
    $this->cardRepository = $cardRepository;
    $this->configuration = $configuration;

    parent::__construct();
  }

  /**
   * @return NULL
   */
  public function dealerportal__SaveCreditCard()
  {
    $params = $this->getNamedInputValues();

    try {
      // connect to the DB
      $this->utilities->teldataChangeDb();

      // retrieve and validate session from $security_token
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($error_code) {
        return $this->errException($error, $error_code);
      }

      $this->validateAndSaveToken(
        $this->configuration,
        $this->customerRepository,
        $this->cardRepository,
        $this->getNamedInputValues()
      );

      $this->succeed();

    } catch (InvalidObjectCreationException $e) {
      if (is_object($this->customer) && !empty($e->getCustomUserErrors()[$this->customer->preferred_language])) {
        $this->addError($e->getMessage(), $e->code(), $e->getCustomUserErrors()[$this->customer->preferred_language]);
      } elseif (!empty($e->getCustomUserErrors()[0])) {
        $this->addError($e->getMessage(), $e->code(), $e->getCustomUserErrors()[0]);
      } else {
        $this->addError($e->getMessage(), $e->code());
      }
    } catch (SaveMethodFailedException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), 'DB0001');
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
