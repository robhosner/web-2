<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class GetRechargeSummaryMaster extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetRechargeSummaryMaster
   * return dealer and children recharge periods
   * @see SMR-8
   */
  public function dealerportal__GetRechargeSummaryMaster()
  {
    // init
    list ($security_token) = $this->getInputValues();
    $this->addToOutput('recharge_periods', array());
    $this->addToOutput('record_count', 0);

    try
    {
      teldata_change_db();

      // retrieve and validate security token
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // get cached results if available
      $master = $session_data['masteragent'];
      $redis_key = __FUNCTION__ . "/$master";
      $redis = new \Ultra\Lib\Util\Redis;
      if ($cached_result = $redis->get($redis_key))
      {
        $recharge_periods = json_decode($cached_result);
      }
      else // retrieve from DB
      {
        $dealers = get_dealers_from_master($master);
        if ($dealers)
          $recharge_periods = get_summary_dealer_recharge_rate_by_dealers($dealers);

        // cache result in Redis for 30 minutes
        if (count($recharge_periods))
          $redis->set($redis_key, json_encode($recharge_periods), 30 * 60);
      }

      $this->addToOutput('recharge_periods', $recharge_periods);
      $this->addToOutput('record_count', count($recharge_periods));

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}