<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ChangePlanFuture extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ChangePlanFuture
   *
   * change Future plan while Active
   *
   * @param: target_plan
   * @return Result object
   * @author: VYT, 14-01-21
   */
  public function dealerportal__ChangePlanFuture()
  {
    list ( $security_token , $customer_id, $target_plan ) = $this->getInputValues();

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, NULL, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      if (!is_eligible_for_plan_change($customer_id, array('BILLING.MRC_PROMO_PLAN', 'BILLING.MRC_7_11', BILLING_OPTION_MULTI_MONTH)))
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer is not eligible to change their plan', 'BI0001');

      $account = get_account_from_customer_id($customer_id, array('cos_id'));
      if ( ! $account || ! $account->cos_id)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      if ( ! isSameBrand(get_plan_from_cos_id($account->cos_id), $target_plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous and future plan brands do not match', 'MP0011');

      // prepare query
      $params = array(
        'monthly_renewal_target' => $target_plan,
        'customer_id'            => $customer_id
      );

      $query = htt_customers_overlay_ultra_update_query($params);

      // update plan
      if (! is_mssql_successful(logged_mssql_query($query)))
        $this->errException('ERR_API_INTERNAL: failed to update target plan', 'DB0001');

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}