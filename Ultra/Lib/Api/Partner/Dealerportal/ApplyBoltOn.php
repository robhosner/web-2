<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

use Ultra\FeatureFlags\FeatureFlagsClientWrapper;

class ApplyBoltOn extends \Ultra\Lib\Api\Partner\Dealerportal
{
  private $flagsObj;

  public function __construct(FeatureFlagsClientWrapper $flagsObj)
  {
    parent::__construct();
    $this->flagsObj = $flagsObj;
  }

  /**
   * dealerportal__ApplyBoltOn
   *
   * Applies the given Bolt On to the customer account.
   *
   * @return Result object
   */
  
  public function dealerportal__ApplyBoltOn ()
  {
    list ( $security_token , $customer_id , $bolt_on_id ) = $this->getInputValues();

    $this->addToOutput('plan','');

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer info
      $account  = get_account_from_customer_id( $customer_id, array('CUSTOMER_ID','COS_ID','BALANCE') );
      if ( ! $account )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found' , 'VV0031');

      $state = internal_func_get_state_from_customer_id($account->CUSTOMER_ID);

      // verify that the customer is Active
      if ( $state['state'] != STATE_ACTIVE )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      $plan = get_plan_from_cos_id($account->COS_ID);
      if ( empty($plan) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: plan cannot be found.', 'IN0001');

      if ( ! \Ultra\Lib\BoltOn\validateImmediateBoltOn($account->COS_ID, $bolt_on_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan.', 'VV0105');

      // get Bolt On info
      $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id );
      if ( ! $boltOnInfo )
        $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0101');

      dlog('',"boltOnInfo = %s",$boltOnInfo);

      $redis = new \Ultra\Lib\Util\Redis;

      // check redis semaphore
      if (!$this->flagsObj->disableRateLimits($account->CUSTOMER_ID)) {
        if ( get_bolt_on_semaphore( $redis , $account->CUSTOMER_ID, $boltOnInfo['product'] ) )
          $this->errException("ERR_API_INTERNAL: a bolt on has been processed less than 15 minutes ago.", 'VV0102');
      }

      // check for sufficient wallet balance
      if ( $boltOnInfo['cost'] > $account->BALANCE )
        $this->errException('ERR_API_INTERNAL: not enough money to perform this operation', 'VV0103');

      // action for adding a Bolt On on demand
      list( $error , $error_code ) = \Ultra\Lib\BoltOn\addBoltOnImmediate( $account->CUSTOMER_ID , $boltOnInfo, 'SPEND' , __FUNCTION__, NULL);

      if ( $error )
        $this->errException( $error , $error_code );

      // set Redis semaphore to block another immediate Bolt On addition by the same customer for 15 minutes
      set_bolt_on_semaphore($redis, $account->CUSTOMER_ID, $boltOnInfo['product']);

      // set Redis semaphore to block SMS from notification__DataNotificationHandler for 5 minutes
      if ( $boltOnInfo['product'] == 'DATA' )
        set_data_recharge_notification_delay($redis, $account->CUSTOMER_ID);

      // success
      $this->addToOutput('plan', $plan);
      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}