<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class GetCustomerActions extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetCustomerActions
   *
   * Provides information regarding the Dealer's level of access to a Customer.
   *
   * @return Result object
   */
  public function dealerportal__GetCustomerActions()
  {
    list ( $security_token , $customer_id ) = $this->getInputValues();

    $error_code = '';

    $this->addToOutput('ui_actions', array());

    try
    {
      // retrieve and validate session
      teldata_change_db();
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer
      $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_state'));
      $account  = get_account_from_customer_id($customer_id , array('COS_ID'));

      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // get customer state
      $plan_state = $customer->plan_state;
      if (! $plan_state)
        $this->errException('ERR_API_INTERNAL: customer state could not be determined', 'UN0001');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data , $customer_id , $plan_state , __FUNCTION__ ) )
        $this->errException( 'ERR_API_INTERNAL: API execution not allowed for the given caller' , 'SE0005' );

      // get possible activities
      $activities = get_activities_from_plan_state($plan_state);

      // special case: L19 is the only plan that can have Buy Minutes activity
      // therefore we must remove this action from other plans
      $plan = get_plan_from_cos_id($account->COS_ID);
      if ($plan == 'L19')
        $this->addToOutput('ui_actions', $activities);
      else
      {
        $result = array();
        foreach($activities as $activity)
          if ($activity != 'Buy Minutes')
            $result[] = $activity;
        $this->addToOutput('ui_actions', $result);
      }

      // all done
      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}