<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Exception;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Plans\Interfaces\PlansRepository;
use Ultra\Lib\Api\Traits\PlansHandler;

/**
 * Class GetPlans
 * @package Ultra\Lib\Api\Partner\Dealerportal
 */
class GetPlans extends Dealerportal
{
  use PlansHandler;

  /**
   * @var PlansRepository
   */
  private $plansRepository;

  /**
   * GetPlans constructor.
   * @param PlansRepository $plansRepository
   */
  public function __construct(PlansRepository $plansRepository)
  {
    parent::__construct();

    $this->plansRepository = $plansRepository;
  }

  /**
   * @return mixed
   */
  public function dealerportal__GetPlans()
  {
    list ($brand, $plan_id) = $this->getInputValues();

    try
    {
      $this->getPlans($this->plansRepository, $brand, $plan_id);

      $this->succeed();
    }
    catch (Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
