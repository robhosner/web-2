<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class CheckVoiceRechargeByCustomerId extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckVoiceRechargeByCustomerId
   *
   * get Voice Recharge Options for a customer
   *
   * @param int customer_id
   * @return Result object
   * @author VYT, 14-02-10, adopted from v1
   */
  public function dealerportal__CheckVoiceRechargeByCustomerId()
  {
    // init
    list ( $security_token , $customer_id ) = $this->getInputValues();
    $this->addToOutput('voice_recharge', array());

    try
    {
      // retrieve and validate session
      teldata_change_db();
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer info
      $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_state'));
      $account  = get_account_from_customer_id($customer_id, array('COS_ID'));

      if (! $customer || ! $account->COS_ID)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // get recharge options: could be none which is not an error
      $voice_recharge = get_voice_recharge_by_plan(get_plan_from_cos_id($account->COS_ID));
      if (count($voice_recharge))
        $this->addToOutput('voice_recharge', $voice_recharge);

      // all done
      $this->succeed();

    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}