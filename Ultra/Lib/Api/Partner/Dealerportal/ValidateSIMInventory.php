<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidateSIMInventory extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ValidateSIMInventory
   *
   * Check the ICCID validity on the Ultra DB.
   *
   * @return Result object
   */
  public function dealerportal__ValidateSIMInventory ()
  {
    list ( $security_token , $ICCID ) = $this->getInputValues();

    $this->addToOutput('valid_ext','');
    $this->addToOutput('valid','');
    $this->addToOutput('brand','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // check for 19 digits ICCID
      $htt_inventory_sim_select_query = htt_inventory_sim_select_query( array( "iccid_full" => $ICCID ) );

      $htt_inventory_sim_data = \Ultra\Lib\DB\fetch_objects($htt_inventory_sim_select_query);

      if ( ! ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: ICCID not found. Enter new ICCID." , 'IC0001' );

      $iccid_user_status = get_ICCID_user_status($ICCID);

      $simbatch = htt_inventory_simbatches_get_by_batch_id($htt_inventory_sim_data[0]->ICCID_BATCH_ID, array('EXPIRES_DATE'));
      if ( ! $simbatch )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: ICCID has unknown SIMBATCH." , 'IC0001' );

      $brand = \Ultra\UltraConfig\getShortNameFromBrandId($htt_inventory_sim_data[0]->BRAND_ID);

      if ( \Ultra\Lib\Util\validateUnivisionBrandId($htt_inventory_sim_data[0]->BRAND_ID)
        || \Ultra\Lib\Util\validateMintBrandId($htt_inventory_sim_data[0]->BRAND_ID)
      ) {
        $this->errException('ERR_API_INVALID_ARGUMENTS: ICCID not valid. Enter new ICCID.', 'VV0013');
      }

      if ($htt_inventory_sim_data[0]->PRODUCT_TYPE == 'ORANGE')
        $this->errException('ERR_API_INVALID_ARGUMENTS: SIM is Prefunded. Use the Pre-Funded Activation link.', 'VV0013');

      $this->addToOutput('valid_ext',    get_ICCID_user_status($ICCID, 'allow reserved') );
      $this->addToOutput('valid',        $iccid_user_status );
      $this->addToOutput('brand',        $brand);
      $this->addToOutput('expires_date', $simbatch->EXPIRES_DATE);

      if ( $iccid_user_status != 'VALID' )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: ICCID has been previously activated. Enter new ICCID." , 'IC0002' );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}