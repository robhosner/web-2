<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Dealers\Interfaces\DealersRepository;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Lib\Util\Redis;

class GetRechargeDetailByPlanMaster extends Dealerportal
{
  /**
   * @var DealersRepository
   */
  private $dealersRepository;

  /**
   * @var Redis
   */
  public $redis;

  /**
   * GetRechargeDetailByPlanMaster constructor.
   *
   * @param DealersRepository $dealersRepository
   * @param Redis $redis
   */
  public function __construct(DealersRepository $dealersRepository, Redis $redis)
  {
    $this->dealersRepository = $dealersRepository;
    $this->redis = $redis;

    parent::__construct();
  }
  
  /**
   * dealerportal__GetRechargeDetailByPlanMaster
   * report aggregated period recharge stats by plan for dealers of the given master agent
   * @see SMR-14
   */
  public function dealerportal__GetRechargeDetailByPlanMaster()
  {
    // init
    list ($security_token, $recharge_period) = $this->getInputValues();
    $details_by_plan = [];
    $record_count = 0;

    try
    {
      // retrieve and validate session from $security_token
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      
      if ($error_code)
      {
        return $this->errException($error, $error_code);
      }

      // get master (or subdistributor)
      list($organization) = explode(' ', $session_data['role']);
      
      $master = $organization == 'SubDistributor' ? $session_data['distributor'] : $master = $session_data['masteragent'];

      // get cached results if available
      $recharge_period = sprintf('%d/1/%d', substr($recharge_period, 0, 2), 2000 + substr($recharge_period, -2));
      $redis_key = __FUNCTION__ . "/$master/$recharge_period";
      
      if ($cache = $this->redis->get($redis_key))
      {
        $details_by_plan = json_decode($cache);
      }
      else // retrieve from DB
      {
        // get all dealer groups for this master
        $dealers = $this->dealersRepository->getDealersFromMaster($master);
        $details_by_plan = $this->getRechargeDetailsByDealer($this->redis, $redis_key, $dealers, $recharge_period);
      }
      
      $record_count = count($details_by_plan);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('details_by_plan', $details_by_plan);
    $this->addToOutput('record_count', $record_count);
    
    return $this->result;
  }
}
