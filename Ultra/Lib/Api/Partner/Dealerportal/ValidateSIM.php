<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidateSIM extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ValidateSIM
   *
   * Check if the ICCID is unused for activation.
   *
   * @return Result object
   */
  public function dealerportal__ValidateSIM ()
  {
    list ( $security_token , $ICCID ,$mode ) = $this->getInputValues();

    $this->addToOutput('sim_ready_activate','');
    $this->addToOutput('valid_ext','');
    $this->addToOutput('valid','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $result = htt_inventory_sim_get_iccid_user_status($ICCID);
      $errors = $result->get_errors();
      if (count($errors))
        $this->errException( "ERR_API_INTERNAL: " . $errors[0] , 'IC0001' );

      $this->addToOutput('valid_ext', get_ICCID_user_status($ICCID, 'allow reserved') );
      $this->addToOutput('valid'    , $result->data_array['status'] );

      if ( $result->data_array['status'] != 'VALID' )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: ICCID not valid or already used." , 'IC0002' );

      if ( isset($mode) && ( $mode == 'PREACTIVATION' ) )
      {
        $this->addToOutput('sim_ready_activate',TRUE);

        // if the SIM comes back as ready to activate; a flag should be set in Redis with ttl = 5m noting that sim is 'good to activate.'
        if ( $success )
        {
          $redis = new \Ultra\Lib\Util\Redis;

          $redis->set( 'iccid/good_to_activate/' . $ICCID , 1 , 60 * 20 );
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}