<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidateCelluphoneSession extends \Ultra\Lib\Api\Partner\Dealerportal
{
    /**
   * dealerportal__ValidateCelluphoneSession
   *
   * Verifies celluphone_session_id against tblPHPUserLog. Returns a valid security token to be used for dealer portal APIs.
   *
   * @return Result object
   */
  public function dealerportal__ValidateCelluphoneSession ()
  {
    list ( $celluphone_session_id ) = $this->getInputValues();

    $this->addArrayToOutput(array(
      'security_token' => '',
      'role' => '',
      'store_name' => '',
      'dealer_code' => '',
      'master_id' => '',
      'parent_master_id' => ''
    ));


    try
    {
      teldata_change_db(); // connect to the DB

      // get data from tblPHPUserLog
      $php_user_log = get_php_user_log( $celluphone_session_id );

      if ( ! $php_user_log )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no celluphone session found." , 'ND0002' );

      dlog('',"php_user_log SessionID = %s ; UserID = %s" , $php_user_log->SessionID , $php_user_log->UserID );

      // get Role and other info from Celluphone DB
      list( $user_info , $error_code , $error ) = $this->getCelluphoneSessionData( $php_user_log->UserID );

      if ( $error_code )
        $this->errException( $error , $error_code );

      dlog('',"user_info = %s",$user_info);

      $security_token = $this->ultrasession->addObject(
        array(
          'donor_id'    => $celluphone_session_id,
          'user_id'     => $php_user_log->UserID,
          'role'        => $user_info['role'],
          'dealer_code' => $user_info['dealer_code'],
          'store_name'  => $user_info['store_name'],
          'masteragent' => $user_info['masteragent'],
          'distributor' => $user_info['distributor'],
          'dealer'      => $user_info['dealer']
        )
      );

      if ( ! $security_token )
        $this->errException( "ERR_API_INTERNAL: Could not generate a security token." , 'SE0001' );

      $this->addArrayToOutput(array(
        'security_token'   => $security_token,
        'role'             => $user_info['role'],
        'dealer_code'      => $user_info['dealer_code'],
        'store_name'       => $user_info['store_name'],
        'master_id'        => $user_info['master_id'],
        'parent_master_id' => $user_info['parent_master_id']
      ));

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}