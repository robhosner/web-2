<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class GetBoltOns extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetBoltOns
   *
   * Get all allowed Bolt Ons
   *
   * @return Result object
   */
  
  public function dealerportal__GetBoltOns ()
  {
    list ( $security_token, $customer_id ) = $this->getInputValues();

    $this->addToOutput('bolt_ons',array());

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      if ( ! empty($customer_id))
        $bolt_ons = \Ultra\UltraConfig\getBoltOnInfoByPlan(get_plan_name_from_short_name(get_plan_from_cos_id(\Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer_id, 'COS_ID'))));
      else
        $bolt_ons = \Ultra\UltraConfig\getBoltOnsMappingByType();

      if ( ! $bolt_ons || ! is_array( $bolt_ons ) || ! count( $bolt_ons ) )
        $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');

      $this->addToOutput('bolt_ons',$bolt_ons);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}