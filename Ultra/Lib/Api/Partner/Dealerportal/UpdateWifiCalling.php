<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use CommandInvocation;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Address;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\DatabaseErrorException;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\InvalidSimStateException;
use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Mvne\MakeItSo\Interfaces\MakeItSoRepositoryInterface;
use Ultra\Mvne\UpdateWifiCallingService;
use Ultra\Sims\Interfaces\SimRepository;
use Ultra\Utilities\Common;

/**
 * Class UpdateWifiCalling
 * @package Ultra\Lib\Api\Partner\Dealerportal
 */
class UpdateWifiCalling extends Dealerportal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var UpdateWifiCallingService
   */
  public $updateWifiCallingService;

  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var MakeItSoRepositoryInterface
   */
  private $makeItSoRepository;

  /**
   * @var CommandInvocation
   */
  private $commandInvocation;

  /**
   * UpdateWifiCalling constructor.
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param SimRepository $simRepository
   * @param Common $utilities
   * @param CommandInvocation $commandInvocation
   * @param MakeItSoRepositoryInterface $makeItSoRepository
   */
  public function __construct(
    CustomerRepository $customerRepository,
    Configuration $configuration,
    SimRepository $simRepository,
    Common $utilities,
    CommandInvocation $commandInvocation,
    MakeItSoRepositoryInterface $makeItSoRepository
  )
  {
    $this->customerRepository = $customerRepository;
    $this->configuration = $configuration;
    $this->simRepository = $simRepository;
    $this->utilities = $utilities;
    $this->commandInvocation = $commandInvocation;
    $this->makeItSoRepository = $makeItSoRepository;

    parent::__construct();
  }

  public function dealerportal__UpdateWifiCalling()
  {
    list ($security_token, $customer_id, $address1, $address2, $city, $state, $zipcode, $enable_wifi_calling) = $this->getInputValues();

    try {
      teldata_change_db();
      
      // retrieve and validate session from $security_token
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);

      if ($error_code) {
        return $this->errException($error, $error_code);
      }

      $this->updateWifiCallingService = $this->setWifiCallingService(
        $customer_id,
        $address1,
        $address2,
        $city,
        $state,
        $zipcode,
        $enable_wifi_calling
      );

      $this->updateWifiCallingService->execute();
      $this->succeed();

    } catch(InvalidObjectCreationException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(InvalidSimStateException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(MiddleWareException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(DatabaseErrorException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  public function setWifiCallingService($customer_id, $address1, $address2, $city, $state, $zipcode, $enable_wifi_calling)
  {
    if (empty($this->updateWifiCallingService)) {
      return new UpdateWifiCallingService(
        new Address([
          'address1' => $enable_wifi_calling ? $address1 : '>__<',
          'address2' => $address2,
          'city' => $city,
          'state_region' => $state,
          'postal_code' => $zipcode,
        ]),
        $this->customerRepository,
        $this->configuration,
        $this->simRepository,
        $this->utilities,
        $customer_id,
        $enable_wifi_calling,
        $this->getRequestId(),
        $this->commandInvocation,
        $this->makeItSoRepository,
        'WIFI_CALLING.DEALER_PORTAL'
      );
    } else {
      return $this->updateWifiCallingService;
    }
  }
}
