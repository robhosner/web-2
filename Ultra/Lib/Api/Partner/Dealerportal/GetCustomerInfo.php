<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Customers\Customer;
use Ultra\Customers\Options;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use function Ultra\UltraConfig\isBPlan;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class GetCustomerInfo extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetCustomerInfo
   *
   * Retrieves basic account info for subscriber
   *
   * @param: customer_id
   * @return Result object
   * @author: VYT, 14-02-10, adapted from v1
   */
  public function dealerportal__GetCustomerInfo()
  {
    // input
    list ( $security_token , $customer_id , $get_voice_minutes , $get_data_usage ) = $this->getInputValues();

    $results = [
      'first_name'             => NULL,
      'last_name'              => NULL,
      'email'                  => NULL,
      'msisdn'                 => NULL,
      'iccid'                  => NULL,
      'plan'                   => NULL,
      'plan_state'             => NULL,
      'created_date'           => NULL,
      'service_expires_date'   => NULL,
      'paid_through'           => NULL,
      'duration'               => NULL,
      'balance'                => NULL,
      'stored_value'           => NULL,
      'packaged_balance'       => NULL,
      'auto_recharge'          => NULL,
      'preferred_language'     => NULL,
      'has_billing_info_saved' => NULL,
      'marketing_settings'     => array(),
      'monthly_renewal_target' => NULL,
      'zero_minutes'           => NULL,
      'voice_minutes'          => NULL,
      'bolt_ons'               => array(),
      '4g_lte_remaining'       => NULL,
      '4g_lte_usage'           => NULL,
      '4g_lte_last_applied'    => 0,
      '4g_lte_bolton_percent_used' => '',
      'has_access'          => false
    ];

    foreach($results as $key => $value)
      $this->addToOutput($key, $value);

    try
    {
      $redis = $this->getRedis();

      // retrieve and validate session
      teldata_change_db(); // connect to the DB
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer info
      $customer = get_customer_from_customer_id($customer_id);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      $plan_name = cos_id_plan_description($customer->cos_id);

      // check if the caller is allowed to execute this API
      if ( $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $results['has_access'] = true;

      // get customer state
      $state = get_customer_state( $customer );
      if (count($state['errors']))
        $this->errException('ERR_API_INTERNAL: customer state could not be determined', 'IN0002');

      // DB stores dates in UTC
      if (! date_default_timezone_set('UTC'))
        $this->errException('ERR_API_INTERNAL: failed to set UTC time zone' , 'AP0001');

      // remove default white spaces on CC fields
      $customer->CC_NUMBER = trim($customer->CC_NUMBER);
      $customer->CC_EXP_DATE = trim($customer->CC_EXP_DATE);

      $customerObject = new Customer((array) $customer);

      // return customer personal info only if dealer has permissions
      if ($results['has_access']) {
        $results['first_name']             = $customer->FIRST_NAME;
        $results['last_name']              = $customer->LAST_NAME;
        $results['email']                  = $customer->E_MAIL;
      }

      // fill in return values
      $results['msisdn']                 = $customer->current_mobile_number;
      $results['iccid']                  = empty($customer->CURRENT_ICCID_FULL) ? $customer->current_iccid : $customer->CURRENT_ICCID_FULL;
      $results['brand']                  = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
      $results['plan']                   = $plan_name;
      $results['plan_id']                = get_plan_from_cos_id($customer->cos_id);
      $results['plan_state']             = $customer->plan_state;
      $results['created_date']           = empty($customer->CREATION_DATE_TIME) ? '' : strtotime($customer->CREATION_DATE_TIME);
      $results['service_expires_date']   = strtotime($customer->plan_expires);
      $results['balance']                = $customer->BALANCE;
      $results['stored_value']           = $customer->stored_value;
      $results['packaged_balance']       = $customer->PACKAGED_BALANCE1;
      $results['auto_recharge']          = $customer->monthly_cc_renewal ? TRUE : FALSE;
      $results['preferred_language']     = $customer->preferred_language;
      $results['monthly_renewal_target'] = $customer->MONTHLY_RENEWAL_TARGET;
      $results['bolt_ons']               = get_bolt_ons_info_from_customer_options( $customer_id );
      $results['throttle_speed']         = $customerObject->getThrottleSpeed();

      $e911_address = (new Options($customerObject))->getE911Address();
      $results['e911_address']        = $e911_address;
      $results['has_wifi_soc']        = ($e911_address) ? true : false;

      $results['globe_minutes']          = $customer->PACKAGED_BALANCE3;
      $results['has_gba_sim']            = (new SimRepository())->getSimInventoryAndBatchInfoByIccid($customer->CURRENT_ICCID_FULL)->allowsWifiSoc();

      // CCCP-17: get credit card info
      $cc_info = get_cc_info_from_customer_id($customer_id);
      $results['has_billing_info_saved'] = empty($cc_info->LAST_FOUR) || empty($cc_info->EXPIRES_DATE) ? FALSE : TRUE;

      $results['zero_minutes'] = ( in_array( get_plan_from_cos_id( $customer->COS_ID), array('L19','L24','L34','L44') )
        || $results['plan_state'] != STATE_ACTIVE ) ? 0 : int_positive_or_zero(1000 - $customer->PERIOD_MINUTES_TO_DATE_BILLED);

      // DATAQ-112
      if ( $get_voice_minutes && get_plan_from_cos_id($customer->COS_ID) == 'L34' )
        $results['voice_minutes'] = mvneGetVoiceMinutes($customer->customer_id);

      // get marketing preferences
      $preferences = get_marketing_settings(
        array(
          'customer_id' => $customer_id
        )
      );

      if (count($preferences['errors'])) // non-fatal
        $this->addErrors($preferences['errors']);
      else
        $results['marketing_settings'] = $preferences['marketing_settings'];

      $voice_option = get_voice_preference($customer_id);
      $results['marketing_settings']['marketing_voice_option'] = ($voice_option) ? 0 : 1;

      if ( $get_data_usage )
      {
        // get '4g_lte_last_applied' from HTT_DATA_EVENT_LOG

        $latest_data_event_log = get_latest_data_event_log( $customer_id );

        if ( $latest_data_event_log )
          $results['4g_lte_last_applied'] = $latest_data_event_log->event_date_epoch;

        // invoke mwCheckBalance to check 4G LTE data usage for this customer
        if ( $customer->current_mobile_number )
        {
          list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown , $mvneError ) = mvneGet4gLTE($this->getRequestId(), $customer->current_mobile_number, $customer_id, $redis);

          $results['4g_lte_remaining'] = $remaining;
          $results['4g_lte_usage']     = $usage;
        }
      }

      if ( ! empty( $mintAddOnUsage )
        && ! empty( $mintAddOnUsage + $mintAddOnRemaining )
      )
      {
        $results['4g_lte_bolton_percent_used'] = ceil( ( $mintAddOnUsage * 100 ) / ( $mintAddOnUsage + $mintAddOnRemaining ) );
      }

      // MVNO-2503: format regardless of value
      $results['4g_lte_remaining'] = beautify_4g_lte_string( $results['4g_lte_remaining'], FALSE );
      $results['4g_lte_usage']     = beautify_4g_lte_string( $results['4g_lte_usage'], TRUE );

      // MRP-98
      if ($multi_month_options = multi_month_info($customer_id))
      {
        $results['paid_through'] = strtotime("+{$multi_month_options['months_left']} month", $customer->plan_expires_epoch);
        $results['duration'] = $multi_month_options['months_total'];
      }

      if (isBPlan($customer->COS_ID)) {
        $overlayRow = ultra_multi_month_overlay_from_customer_id($customer_id);
        $results['paid_through'] = strtotime($overlayRow->CYCLE_EXPIRES);
        $results['duration'] = $overlayRow->TOTAL_MONTHS;
      }

      // all done
      foreach($results as $key => $value)
        $this->addToOutput($key, $value);

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

