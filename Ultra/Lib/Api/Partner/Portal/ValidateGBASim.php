<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Billing\Interfaces\BillingHistoryRepository;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Sims\Interfaces\SimRepository;

class ValidateGBASim extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var BillingHistoryRepository
   */
  private $billingHistoryRepository;

  /**
   * ValidateGBASim constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param SimRepository $simRepository
   * @param BillingHistoryRepository $billingHistoryRepository
   */
  public function __construct(
    Session $session,
    CustomerRepository $customerRepository,
    SimRepository $simRepository,
    BillingHistoryRepository $billingHistoryRepository
  )
  {
    $this->session = $session;
    $this->customerRepository = $customerRepository;
    $this->simRepository = $simRepository;
    $this->billingHistoryRepository = $billingHistoryRepository;
  }

  /**
   * portal__ValidateGBASim
   *
   * @param int customer_id
   * @return object Result
   */
  public function portal__ValidateGBASim()
  {
    try {
      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');
      }
      $eligibleForReplacementSim = false;

      $customer = $this->customerRepository->getCustomerById($this->session->customer_id, ['current_iccid_full', 'brand_id'], true);

      if (!$customer || !$customer->current_iccid_full) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      $sim = $this->simRepository->getSimInventoryAndBatchInfoByIccid($customer->current_iccid_full);
      $allowsWifiSoc = $sim->allowsWifiSoc();

      if (!$allowsWifiSoc && $customer->brand_id == 1) {
        $billingHistory = $this->billingHistoryRepository->getBillingTransactionsByDescription($customer->customer_id, [], 'SIM Replacement - Free GBA SIM');

        if (empty($billingHistory)) {
          $eligibleForReplacementSim = true;
        }
      }

      $this->addToOutput('valid', $allowsWifiSoc);
      $this->addToOutput('eligible', $eligibleForReplacementSim);
      $this->succeed();
    } catch(InvalidObjectCreationException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
