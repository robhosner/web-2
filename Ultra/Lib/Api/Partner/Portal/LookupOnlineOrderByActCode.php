<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Lib\Api\Partner\Portal;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use Ultra\Orders\Repositories\Mssql\OrderRepository;

/**
 * Class LookupOnlineOrderByActCode
 * @package Ultra\Lib\Api\Partner\Portal
 */
class LookupOnlineOrderByActCode extends Portal
{
  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var OrderRepository
   */
  private $orderRepository;

  /**
   * LookupOnlineOrderByActCode constructor.
   * @param SimRepository $simRepository
   * @param OrderRepository $orderRepository
   */
  public function __construct(SimRepository $simRepository, OrderRepository $orderRepository)
  {
    $this->simRepository   = $simRepository;
    $this->orderRepository = $orderRepository;
  }

  /**
   * portal__LookupOnlineOrderByActCode
   * @param string actcode
   * @return Result object
   */
  public function portal__LookupOnlineOrderByActCode()
  {
    list ($actcode) = $this->getInputValues();
    
    try
    {
      if (empty($actcode))
      {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: missing actcode', 'MP0001');
      }
      
      $sim = $this->simRepository->getSimFromActCode($actcode);
      
      if (!$sim)
      {
        return $this->errException('ERR_API_INTERNAL: no sim associated with actcode ' . $actcode, 'ND0001');
      }

      $order = $this->orderRepository->getOrderByIccid(
        $sim->ICCID_FULL,
        [
          'FIRST_NAME',
          'LAST_NAME',
          'EMAIL',
          'BIN',
          'LAST_FOUR',
          'AUTO_ENROLL'
        ]
      );

      if (!$order)
      {
        return $this->errException('ERR_API_INTERNAL: no order found for iccid ' . $sim->ICCID_FULL, 'ND0001');
      }

      $this->addArrayToOutput([
        'first_name' => $order->FIRST_NAME,
        'last_name' => $order->LAST_NAME,
        'email' => $order->EMAIL,
        'bin' => $order->BIN,
        'last_four' => $order->LAST_FOUR,
        'auto_enroll' => $order->AUTO_ENROLL
      ]);
      
      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}