<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class VerifyProvisionStatus extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__VerifyProvisionStatus
   * provide necessary info to the UI so that it can resume the activation flow
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Self-Activation+APIs#Self-ActivationAPIs-portal__VerifyProvisionStatus
   * @return object Result
   */
  public function portal__VerifyProvisionStatus()
  {
    // init
    $zipcode = NULL;
    $port_account_number = NULL;
    $port_account_password = NULL;
    $iccid = NULL;
    $plan_amount = NULL;
    $plan_state = NULL;
    $activation_request_id = NULL;
    $port_request_id = NULL;
    $port_resolution = NULL;
    $pending_transition = NULL;
    $msisdn = NULL;
    $port_status = NULL;
    $next_action = NULL;
    $amount_needed = NULL;
    $history_last_updated = NULL;
    $activations_enabled = NULL;

    try
    {
      teldata_change_db();

      // get subscriber from zsession cookie
      if (empty($_COOKIE['zsession']))
        $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
      $zsession = $_COOKIE['zsession'];
      if (! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $this->setOutputLanguage($customer->preferred_language);

      // get subscriber state
      $state = get_customer_state($customer);
      if (empty($state['state']['state']))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer state', 'IN0001');
      $plan_state = $state['state']['state'];

      // BOLT-43: return $0 for amount_needed if sub is already Active
      if ($plan_state == STATE_ACTIVE)
        $amount_needed = 0;

      // fill in return values
      $zipcode = $customer->POSTAL_CODE;
      $iccid = $customer->CURRENT_ICCID_FULL;
      $msisdn = empty($customer->current_mobile_number) ? '' : $customer->current_mobile_number;
      $plan_amount = \get_plan_cost_from_cos_id($customer->COS_ID) / 100;

      // get activation request
      $htt_transition_log_query = htt_transition_log_activation_request_id($customer->CUSTOMER_ID);
      $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_query));
      if ((is_array($query_result)) && count($query_result))
        $activation_request_id = $query_result[0]->TRANSITION_UUID;
      $activations_enabled = activations_enabled(TRUE);

      // get transitions
      $transitions = get_all_customer_state_transitions($customer->CUSTOMER_ID);
      if (count($transitions['errors']))
        $this->addWarnings($transitions['errors']);
      elseif (count($transitions['state_transitions']))
      {
        // check if there is an OPEN transition
        foreach($transitions['state_transitions'] as $transition)
          if ($transition->STATUS == 'OPEN')
          {
            $pending_transition = $transition->TRANSITION_UUID;

            // BOLT-43: return $0 for amount_needed if sub has an OPEN  transition to Active
            if ($transition->TO_PLAN_STATE == STATE_ACTIVE)
              $amount_needed = 0;
          }
      }

      // get port info from redis
      if (! empty($msisdn))
      {
        $port_account = retrieve_port_account($msisdn);
        $port_account_number = $port_account['port_account_number'];
        $port_account_password = $port_account['port_account_password'];

        $redis = new \Ultra\Lib\Util\Redis;
        $port_status  = $redis->get("ultra/port/status/$msisdn");
        $port_resolution = $redis->get("ultra/port/provstatus_error_msg/$msisdn");
        $port_updated_seconds_ago = $redis->get("ultra/port/update_timestamp/$msisdn");
      }

      // otherwise get port info from DB
      if ( ! $port_status && ! $port_resolution)
      {
        $portInQueue = new \PortInQueue();
        $loadPortInQueueResult = $portInQueue->loadByCustomerId($customer->CUSTOMER_ID);
        if ($loadPortInQueueResult->is_success() && $portInQueue->portin_queue_id)
        {
          list($port_success, $port_pending, $port_status, $port_resolution) = interpret_port_status($portInQueue);
          $port_updated_seconds_ago = $portInQueue->updated_seconds_ago;
        }
        teldata_change_db();
      }

      date_default_timezone_set('UTC');
      $created_seconds_ago = time() - strtotime($customer->CREATION_DATE_TIME);
      $next_action = get_next_action_from_plan_state($customer->plan_state, $pending_transition, $port_status, $created_seconds_ago, $port_updated_seconds_ago);

      $redis_port = new \Ultra\Lib\Util\Redis\Port();
      $port_request_id = $redis_port->getRequestId($customer->CUSTOMER_ID, $customer->plan_state);

      // get activation history
      if ($history = get_ultra_activation_history($customer->CUSTOMER_ID))
        $history_last_updated = $history->history_last_updated;

      if ($amount_needed === NULL)
        $amount_needed = get_amount_needed_from_customer($customer);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    $this->addArrayToOutput(array(
      'zipcode'               => $zipcode,
      'port_account_number'   => $port_account_number,
      'port_account_password' => $port_account_password,
      'ICCID'                 => $iccid,
      'plan_amount'           => $plan_amount,
      'plan_state'            => $plan_state,
      'activation_request_id' => $activation_request_id,
      'port_request_id'       => $port_request_id,
      'port_resolution'       => array($port_resolution),
      'pending_transition'    => $pending_transition,
      'MSISDN'                => $msisdn,
      'port_status'           => $port_status,
      'next_action'           => $next_action,
      'amount_needed'         => $amount_needed,
      'history_last_updated'  => $history_last_updated,
      'activations_enabled'   => $activations_enabled
    ));

    return $this->result;
  }
}

?>