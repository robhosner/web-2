<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;
use Ultra\Mvne\Adapter;

class RemoveFromFamily extends Portal
{
  use FlexHandler;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var FamilyAPI
   */
  private $familyAPI;

  /**
   * @var Adapter
   */
  private $adapter;

  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var SharedData
   */
  private $sharedData;

  /**
   * RemoveFromFamily constructor.
   * @param Session $session
   * @param FamilyAPI $familyAPI
   * @param Adapter $adapter
   * @param AccountsRepository $accountsRepository
   * @param Configuration $configuration
   * @param SharedData $sharedData
   */
  public function __construct(
    Session $session,
    FamilyAPI $familyAPI,
    Adapter $adapter,
    AccountsRepository $accountsRepository,
    Configuration $configuration,
    SharedData $sharedData
  ) {
    $this->session = $session;
    $this->familyAPI = $familyAPI;
    $this->adapter = $adapter;
    $this->accountsRepository = $accountsRepository;
    $this->configuration = $configuration;
    $this->sharedData = $sharedData;
  }

  public function portal__RemoveFromFamily()
  {
    try {
      teldata_change_db();

      list ($customer_id) = $this->getInputValues();

      if ( ! $sessionCustomerId = $this->session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');

      $this->removeFromFamily($customer_id, $this->accountsRepository, $this->familyAPI, $this->sharedData, $this->adapter, $this->configuration);
      $this->succeed();
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
