<?php

namespace Ultra\Lib\Api\Partner\Portal;

class ReplaceSIMCard extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__ReplaceSIMCard
   *
   * @return Result object
   */
  public function portal__ReplaceSIMCard()
  {
    list( $old_ICCID, $new_ICCID ) = $this->getInputValues();
  
    try
    {
      teldata_change_db(); // connect to the DB

      // verify session
      $session = new \Session();
      if ( ! $customer_id = $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');

      // get customer
      $customer = get_ultra_customer_from_customer_id($session->customer_id, array('plan_state', 'current_mobile_number', 'CUSTOMER_ID', 'CURRENT_ICCID_FULL'));
      if ( ! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // rohibit modifications to in-Active subscribers
      if ($customer->plan_state !== STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Subscriber must be active to change SIM or phone number.', 'IN0001');

      // get customer cos_id
      $customer_plan = get_customer_plan($customer_id);
      if( ! is_array($customer_plan) && is_null($customer_plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer plan not found', 'VV0031');

      $customer->COS_ID = $customer_plan['cos_id'];

      $sim = get_htt_inventory_sim_from_iccid($old_ICCID);
      if ( ! $sim || $sim->CUSTOMER_ID != $customer->CUSTOMER_ID)
        $this->errException('ERR_API_INVALID_ARGUMENTS: the old ICCID is invalid', 'VV0013');

      // check against HTT_CUSTOMERS_OVERLAY_ULTRA
      if ( $customer->CURRENT_ICCID_FULL != luhnenize($old_ICCID) )
        $this->errException("ERR_API_INVALID_ARGUMENTS: the given old_ICCID does not belong to customer_id $customer_id", 'VV0013');

      // validate iccid isn't invalid or already used
      $newSim = get_htt_inventory_sim_from_iccid($new_ICCID);
      if ( !$newSim || !validate_ICCID($newSim, 1) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given new_ICCID is invalid or already used', 'VV0065');

      // block replacement ORANGE sim
      if (strtoupper($newSim->PRODUCT_TYPE) == 'ORANGE')
        $this->errException('Cannot replace with Orange SIM', 'IN0015');

      // brand types must match
      if ( ! isSameBrandByICCID($old_ICCID, $new_ICCID))
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given new_ICCID is of an INVALID BRAND type', 'IC0003');

      // do not allow SIM replacement during MISO tasks
      if ( \Ultra\Lib\MVNE\exists_open_task( $customer->CUSTOMER_ID ) )
        $this->errException("ERR_API_INTERNAL: The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.", 'MM0001');

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      // verify new ICCID
      $result = $mwControl->mwCanActivate(
        array(
          'actionUUID' => getNewActionUUID('dealerportal ' . time()),
          'iccid'  => $new_ICCID
        )
      );

      if ($result->is_failure() || ! isset($result->data_array['available']) || $result->data_array['available'] != 'true')
        $this->errException('ERR_API_INVALID_ARGUMENTS: cannot activate this ICCID (2)', 'VV0016');

      // replace ICCID
      $result = $mwControl->mwChangeSIM(
        array(
          'actionUUID'         => getNewActionUUID('dealerportal ' . time()),
          'msisdn'             => normalize_msisdn($customer->current_mobile_number, TRUE),
          'old_iccid'          => $old_ICCID,
          'new_iccid'          => $new_ICCID,
          'customer_id'        => $customer->CUSTOMER_ID
        )
      );

      dlog('',"mwChangeSIM result data = %s",$result->data_array);

      // the MW call failed
      if ( ! $result->is_success() )
      {
        $errors = $result->get_errors();

        dlog('',"mwChangeSIM result errors = %s",$errors);

        $this->errException($errors[0], 'MW0003');
      }
      // the MW ChangeSIM command failed
      elseif ( ! $result->data_array['success'] )
      {
        $errors = $result->data_array['errors'];

        dlog('',"mwChangeSIM result data errors = %s",$errors);

        $this->errException($errors[0], 'MW0003');
      }

      // change customers sim in DB
      teldata_change_db();

      // side effects: HTT_CUSTOMERS_OVERLAY_ULTRA and HTT_INVENTORY_SIM
      $errors = callbackChangeSIM( $new_ICCID , $customer->CUSTOMER_ID , '' );
      if (count($errors))
        $this->errException($errors[0], 'MW0001');

      $description = 'Replace SIM';
      $entry_type  = 'SIM_REPLACEMENT';

      // add billing history row
      $history_params = array(
        "customer_id"            => $customer->CUSTOMER_ID,
        "date"                   => 'now',
        "cos_id"                 => $customer->COS_ID,
        "entry_type"             => $entry_type,
        "stored_value_change"    => 0,
        "balance_change"         => 0,
        "package_balance_change" => 0,
        "charge_amount"          => 0,
        "reference"              => 'portal__ReplaceSIMCard',
        "reference_source"       => 'PORTAL',
        "detail"                 => 'portal__ReplaceSIMCard',
        "description"            => $description,
        "result"                 => 'COMPLETE',
        "source"                 => $entry_type,
        "is_commissionable"      => 0,
        "terminal_id"            => '0'
      );

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );
      if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
        $this->errException('ERR_API_INTERNAL: failed to update customer info', 'DB0001');

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
