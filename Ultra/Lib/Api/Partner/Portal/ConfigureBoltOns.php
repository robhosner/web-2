<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class ConfigureBoltOns extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__ConfigureBoltOns
   *
   * Overwrite Customer's Recurring Bolt Ons Configuration.
   * Example: curl -i 'https://$DOMAIN/pr/portal/2/ultra/api/portal__ConfigureBoltOns' -d 'bolt_ons[]=DATA_2048_20&bolt_ons[]=IDDCA_6.25_5'
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/Bolt+Ons+APIs
   *
   * @return Result object
   */
  
  public function portal__ConfigureBoltOns ()
  {
    list ( $bolt_ons ) = $this->getInputValues();

    try
    {
      // connect to the DB
      teldata_change_db();

      // MOB-45: try Session first
      $session = new \Session();
      if ($session->customer_id)
        $customer = get_ultra_customer_from_customer_id($session->customer_id, array('preferred_language', 'CUSTOMER_ID', 'MONTHLY_RENEWAL_TARGET'));
      else // legacy zsession
      {
        // get subscriber from zsession cookie
        if (empty($_COOKIE['zsession']))
          $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
        $customer = getCustomerFromSession($_COOKIE['zsession']);
      }

      if (empty($customer))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');
      $this->setOutputLanguage($customer->preferred_language);

      $account = get_account_from_customer_id($customer->CUSTOMER_ID, array('COS_ID'));
      if ( ! $account ||  ! $account->COS_ID )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no account for customer', 'VV0031');
      $cos_id = ($customer->MONTHLY_RENEWAL_TARGET ? get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET) : $account->COS_ID);

      // get Bolt Ons info
      $total_cost = 0;
      $new_bolt_on_configuration = array();
      $new_bolt_on_products = array();

      foreach ((array)$bolt_ons as $bolt_on_id)
        if ( ! empty($bolt_on_id))
        {
          if ( ! \Ultra\Lib\BoltOn\validateRecurringBoltOn($cos_id, $bolt_on_id))
            $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan.', 'VV0105');

          $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id );

          if ( ! $boltOnInfo )
            $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0235');

          dlog('',"%s : boltOnInfo = %s",$bolt_on_id,$boltOnInfo);

          $total_cost += $boltOnInfo['cost'];

          $new_bolt_on_configuration[ $boltOnInfo['option_attribute'] ] = $boltOnInfo['cost'];

          $new_bolt_on_products[] = $boltOnInfo['product'];
        }

      $underscoreObject = new \__ ;

      // verify that there are no duplicate bolt on products
      if ( count( $underscoreObject->uniq($new_bolt_on_products) ) != count( $new_bolt_on_products ) )
        $this->errException("ERR_API_INTERNAL: invalid selection", 'VV0234');

      dlog('',"total bolt on cost = %s",$total_cost);

      // configure bolt-ons
      $current_bolt_ons = \get_bolt_ons_values_from_customer_options( $customer->CUSTOMER_ID );

      dlog('',"current_bolt_ons          = %s",$current_bolt_ons);
      dlog('',"new_bolt_on_configuration = %s",$new_bolt_on_configuration);

      // modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
      $result = \update_bolt_ons_values_to_customer_options( $customer->CUSTOMER_ID , $current_bolt_ons , $new_bolt_on_configuration , 'PORTAL' );

      if ( $result->is_failure() )
        $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

