<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Accounts\Interfaces\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Api\Partner\Portal;

class GetBoltOns extends Portal
{
  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * GetBoltOns constructor.
   * @param AccountsRepository $accountsRepository
   * @param Configuration $configuration
   */
  public function __construct(AccountsRepository $accountsRepository, Configuration $configuration)
  {
    $this->accountsRepository = $accountsRepository;
    $this->configuration = $configuration;
  }


  /**
   * portal__GetBoltOns
   *
   * Get all allowed Bolt Ons
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/Bolt+Ons+APIs
   *
   * @return Result object
   */
  
  public function portal__GetBoltOns()
  {
    list ($customer_id) = $this->getInputValues();

    $bolt_ons = '';
    $this->addToOutput('bolt_ons', $bolt_ons);

    try {
      // connect to the DB
      teldata_change_db();
    
      if ($customer_id) {
        $customer = $this->accountsRepository->getAccountFromCustomerId($customer_id, ['COS_ID']);
        if (!is_null($customer) && !empty($customer->cos_id)) {
          $plan = $this->configuration->getPlanNameFromShortName($this->configuration->getPlanFromCosId($customer->cos_id));
          $bolt_ons = $this->configuration->getBoltOnInfoByPlan($plan);
        }
      } else {
        $bolt_ons = $this->configuration->getBoltOnsMappingByType();

        if (!$bolt_ons || !is_array($bolt_ons) || !count($bolt_ons)) {
          return $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');
        }
      }

      $this->addToOutput('bolt_ons', $bolt_ons);
      $this->succeed();
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
