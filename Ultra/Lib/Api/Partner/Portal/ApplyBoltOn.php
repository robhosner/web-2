<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

use Ultra\FeatureFlags\FeatureFlagsClientWrapper;

class ApplyBoltOn extends \Ultra\Lib\Api\Partner\Portal
{
  private $flagsObj;

  public function __construct(FeatureFlagsClientWrapper $flagsObj)
  {
    $this->flagsObj = $flagsObj;
  }

  /**
   * portal__ApplyBoltOn
   * apply the given Bolt On to the customer account
   * @see BOLT-6
   */
  
  public function portal__ApplyBoltOn()
  {
    // initialize
    list ($bolt_on_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      // MOB-46: try Session first
      $session = new \Session();
      if ($session->customer_id)
      {
        $customer = get_ultra_customer_from_customer_id( $session->customer_id, array('CUSTOMER_ID', 'preferred_language' ,'BRAND_ID', 'current_mobile_number') );
      }
      else // legacy zsession
      {
        // get subscriber from zsession cookie
        if (empty($_COOKIE['zsession']))
          $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
        $customer = getCustomerFromSession($_COOKIE['zsession']);
      }

      if (empty($customer))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $account  = get_account_from_customer_id( $customer->CUSTOMER_ID, array('COS_ID', 'BALANCE') );
      if ( ! $account )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found' , 'VV0031');

      $customer_id = $customer->CUSTOMER_ID;
      $this->setOutputLanguage($customer->preferred_language);

      // verify that the customer is Active
      $state = internal_func_get_state_from_customer_id($customer_id);
      if (empty($state['state']) || $state['state'] != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      $plan = get_plan_from_cos_id($account->COS_ID);
      if (empty($plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: plan cannot be found.', 'IN0001');

      if ( ! \Ultra\Lib\BoltOn\validateImmediateBoltOn($account->COS_ID, $bolt_on_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan.', 'VV0105');

      // get Bolt On info
      $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo($bolt_on_id);
      if ( ! $boltOnInfo )
        $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0101');
      dlog('', "boltOnInfo = %s", $boltOnInfo);

      if (\Ultra\MvneConfig\isMintDataSOC($boltOnInfo['upgrade_plan_soc']))
      {
        $error = \Ultra\Lib\BoltOn\verifyMintDataAddOnThreshold($customer, $boltOnInfo['type']);
        if ($error)
          $this->errException('ERR_API_INTERNAL: Too much remaining add on data.', 'VV0259', 'Too much remaining add on data. Please try again after using more add on data.');
      }

      // check redis semaphore
      if (!$this->flagsObj->disableRateLimits($customer->CUSTOMER_ID)) {
        $redis = new \Ultra\Lib\Util\Redis;
        if ( get_bolt_on_semaphore( $redis , $customer_id, $boltOnInfo['product'] ) )
          $this->errException('ERR_API_INTERNAL: a bolt on has been processed less than 15 minutes ago.', $boltOnInfo['product'] == 'IDDCA' ? 'VV0106' : 'VV0102');
      }

      // check for sufficient wallet balance
      if ( $boltOnInfo['cost'] > $account->BALANCE )
        $this->errException('ERR_API_INTERNAL: not enough money to perform this operation', 'VV0103');

      // action for adding a Bolt On on demand
      list( $error , $error_code ) = \Ultra\Lib\BoltOn\addBoltOnImmediate($customer_id, $boltOnInfo, 'SPEND', __FUNCTION__, NULL);
      if ( $error )
        $this->errException( $error , $error_code );

      // set Redis semaphore to block another portal__ApplyBoltOn by the same customer for 15 minutes
      set_bolt_on_semaphore($redis, $customer_id, $boltOnInfo['product']);

      // set Redis semaphore to block SMS from notification__DataNotificationHandler for 5 minutes
      if ( $boltOnInfo['product'] == 'DATA' )
        set_data_recharge_notification_delay($redis, $customer_id);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }
}

?>