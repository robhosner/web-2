<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class ChargeCustomerPINs extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__ChargeCustomerPINs
   *
   * tests and charges up to 6 pin cards; puts the balance into the account; then tries to activate the customer if necessary
   * fails if there are more than 5 calls in an hour
   * fails if customer status is not one of the following: Active, Provisioned, Suspended, Port-In Requested, Neutral
   * fails if (stored_value + balance + new_load) is greater than $200
   * if there are no transitions in progress for this customer, attempts to transition from {Provisioned|Suspended} to Active
   *
   * @param array of strings: PINs
   * @param string: destination
   * @return Result object
   */
  public function portal__ChargeCustomerPINs()
  {
    // init
    list ($pin_list, $destination) = $this->getInputValues();
    $destination = empty($destination) ? 'WALLET' : $destination;

    $this->addToOutput('pins_total_amount', NULL);
    $this->addToOutput('customer_activated', NULL);

    try
    {
      // get subscriber from zsession cookie
      teldata_change_db();

      if (empty($_COOKIE['zsession']))
        $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');

      $zsession = $_COOKIE['zsession'];

      if ( ! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $this->setOutputLanguage($customer->preferred_language);

      // block if customer called this API more than 5 times in an hour
      if (checkApiAbuseByIdentifier(__FUNCTION__, $customer->CUSTOMER_ID, 5))
        $this->errException('ERR_API_INTERNAL: command disabled; please try again later', 'AP0001');

      // check if customer state is allowed
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: failed to load current customer state', 'UN0001');
      if ( ! in_array($state['state'], array('Active', 'Neutral', 'Port-In Requested', 'Provisioned', 'Suspended')))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

      // check PIN count
      if ( ! count( $pin_list ) || (count( $pin_list ) > 6))
        $this->errException('ERR_API_INVALID_ARGUMENTS: the command requires from one to six PINs', 'PI0010');

      // validate PINs
      $validation = func_validate_pin_cards(array('pin_list' => $pin_list));
      dlog('', 'validation = %s', $validation);

      // check validation results
      if ( $validation['at_least_one_customer_used'])
        $this->errException('ERR_API_INVALID_ARGUMENTS: one or more PINs are already used', 'PI0007');
      if ( $validation['at_least_one_not_found'])
        $this->errException('ERR_API_INVALID_ARGUMENTS: could not find one or more PINs', 'PI0008');

      // PIN status should be AT_MASTER
      if ( $validation['at_least_one_at_foundry'])
        $this->errException('ERR_API_INVALID_ARGUMENTS: One or more PINs are cannot be used', 'PI0009');

      // compute total value
      $pins_total_amount = 0;
      foreach($validation['values'] as $value)
        $pins_total_amount += $value;
      $this->addToOutput('pins_total_amount', $pins_total_amount);

      // stored_value + Balance + new_load must not be greater than $200
      dlog('', 'total_value = ' . ($pins_total_amount + $customer->BALANCE + $customer->stored_value));
      if (($pins_total_amount + $customer->BALANCE + $customer->stored_value) > 200)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance', 'CH0001');

      // apply charge
      $return = func_apply_pin_cards(
        array(
          'destination' => $destination,
          'pin_list'    => $pin_list,
          'customer'    => $customer,
          'source'      => 'WEBPIN',
          'reference'   => create_guid('portal'),
          'reason'      => __FUNCTION__,
          'entry_type'  => 'LOAD',
          'store_id'    => 0,
          'validated'   => $validation));

      // check results
      if (count($return['errors']))
        $this->errException('ERR_API_INTERNAL: An unexpected database error occurred while applying PIN cards', 'DB0001');
      if ($return['activated'])
        $this->addToOutput('customer_activated', TRUE);

      // update ULTRA.HTT_ACTIVATION_HISTORY if necessary
      $activation_history = get_ultra_activation_history( $customer->CUSTOMER_ID );
      if ( $activation_history
        && ( $activation_history->FINAL_STATE != FINAL_STATE_COMPLETE )
        && ( $activation_history->FINAL_STATE != 'Suspended' )
        && ( $activation_history->FINAL_STATE != 'Cancelled' )
      )
        log_funding_in_activation_history($customer->CUSTOMER_ID, 'PIN', $pins_total_amount);

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>