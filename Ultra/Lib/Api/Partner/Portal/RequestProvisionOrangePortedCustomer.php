<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Flex;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

require_once 'classes/Provisioning.php';
require_once 'classes/IntraBrand.php';

class RequestProvisionOrangePortedCustomer extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * recordFraudEvent
   * records a fraud event
   *
   * @param  $customer Object [CUSTOMER_ID]
   * @return void
   */
  private function recordFraudEvent($customer)
  {
    $fraudStatus = $this->isSuccess() ? 'success' : 'error';
    $data = array('source' => json_encode($this->getInputValues()));

    fraud_event($customer, 'portal', 'RequestProvisionOrangePortedCustomer', $fraudStatus, $data);
  }

  /**
   * portal__RequestProvisionOrangePortedCustomer
   *
   * create a customer with an Orange SIM; this call is ONLY for port in customers
   * [ Neutral ] => [ Port-In Requested ]
   *
   * @return Result object
   */
  public function portal__RequestProvisionOrangePortedCustomer()
  {
    list ($actcode, $number_to_port, $zipcode, $preferred_language, $port_account_number, $port_account_password, $email, $contact_phone ) = $this->getInputValues();

    $this->addToOutput('zsession',   NULL);
    $this->addToOutput('request_id', NULL);

    $port_account_number   = trim($port_account_number);
    $port_account_password = trim($port_account_password);

    $masteragent = self::DEFAULT_ACTIVATION_MASTERAGENT;
    $distributor = self::DEFAULT_ACTIVATION_DISTRIBUTOR;
    $dealer      = self::DEFAULT_ACTIVATION_DEALER;
    $userid      = self::DEFAULT_ACTIVATION_USER_ID;

    $customer = null;
    $context = array(); // port transition context

    $changeStateResult = array();
    $changeStateResult['success'] = false;

    try
    {
      teldata_change_db();

      // verify that activations are enabled
      if ( ! activations_enabled())
        $this->errException('ERR_API_INTERNAL: activations disabled', 'AP0001', 'Activations are unavailable, please try again later');

      // block if the API was called by the IP more than 5 times in an hour
      if (checkApiAbuseByIdentifier(__FUNCTION__, getip(), 20))
        $this->errException('ERR_API_INTERNAL: command disabled; please try again later', 'AP0001');

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode', $zipcode, 'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // retrieve SIM data from actcode
      $sim_data = get_htt_inventory_sim_from_actcode($actcode);
      if ( ! $sim_data || ! is_array($sim_data) || ! count($sim_data))
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given actcode is invalid', 'VV0065');

      $sim = $sim_data[0];

      // validate SIM brand
      \logit( "API partner = {$this->partner} ; BRAND_ID = {$sim->BRAND_ID}" );

      if ( ! \Ultra\UltraConfig\isBrandAllowedByAPIPartner( $sim->BRAND_ID , $this->partner ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid', 'IC0003');

      // validate SIM
      if ($sim->PRODUCT_TYPE != 'ORANGE' || ! $sim->SIM_HOT || $sim->MVNE != parent::MVNE2)
        $this->errException('ERR_API_INTERNAL: the SIM cannot be activated', 'VV0016');

      if ($sim->SIM_ACTIVATED || $sim->CUSTOMER_ID || empty($sim->STORED_VALUE))
        $this->errException('ERR_API_INTERNAL: the SIM has been already activated', 'VV0014');

      if (! validate_ICCID($sim, TRUE))
        $this->errException('ERR_API_INTERNAL: the SIM is already being activated', 'VV0015');

      // new redis
      $redis = new \Ultra\Lib\Util\Redis;

      // verify iccid over network
      $result = \Provisioning::verifyICCIDGoodToActivate($sim->ICCID_FULL, $redis);
      if (count($errors = $result->get_errors()))
      {
        list ($error, $code) = explode('|', $errors[0]);
        $this->errException($error, $code);
      }

      if ( ! $dealer_info = \Ultra\Lib\DB\Celluphone\getDealerInfo(NULL, $dealer))
        $this->errException('ERR_API_INTERNAL: No celluphone dealer info found', 'ND0003');

      // save port account credentials in redis for 2 weeks
      save_port_account($number_to_port, $port_account_number, $port_account_password);

      $target_plan =  func_get_orange_plan_from_sim_data($sim);

      $transition_name = '';

      $customer = get_customer_from_msisdn($number_to_port, 'u.CUSTOMER_ID, u.BRAND_ID, plan_state, u.CURRENT_ICCID, a.COS_ID, a.ACCOUNT');

      if (\IntraBrand::isIntraBrandPortByMSISDNAndSIM($number_to_port, $sim)) {
        // regular intraport
        $doIntraport = true;
      } else if ($customer && (Flex::isFlexPlan(get_plan_from_cos_id($customer->COS_ID)) && !Flex::isFlexPlan($target_plan))) {
        // flex -> non flex plan
        $doIntraport = true;
      } else if ($customer && (!Flex::isFlexPlan(get_plan_from_cos_id($customer->COS_ID)) && Flex::isFlexPlan($target_plan))) {
        // non flex -> flex plan
        $doIntraport = true;
      } else {
        // no intraport
        $doIntraport = false;
      }

      // check for intra-brand port or flex plan or if already on flex
      if ($doIntraport) {
        \logit("MSISDN: $number_to_port is intra-brand port");

        if ( ! $customer)
          $this->errException('ERR_API_INTERNAL: Could not find customer by MSISDN for intra-port', 'VV0031');
        $context['old_iccid'] = luhnenize($customer->CURRENT_ICCID);
        $context['old_brand_id'] = $customer->BRAND_ID;

        if ( ! in_array($customer->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED)))
          $this->errException('ERR_API_INTERNAL: port-In denied due to customer state', 'IN0001');

        if ($customer->ACCOUNT != $port_account_number) {
          $this->errException('ERR_API_INTERNAL: port-In denied due to invalid account number', 'PO0009');
        }

        // intra port operations, clone and soft cancel customer
        $result = \Provisioning::intraBrandPortOperations($customer->CUSTOMER_ID, $sim);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        // cloned customer
        $customer = $result->get_data_key('customer');

        // transition name for intra port
        $transition_name = 'Activate Intra Port ' . get_plan_name_from_short_name($target_plan);

        if (!empty($contact_phone)) {
          if (!(new CustomerRepository())->addCustomerContactPhone($customer->CUSTOMER_ID, $contact_phone)) {
            $this->errException('ERR_API_INTERNAL: Cannot add customer contact_phone', 'DB0001');
          }
        }
      }
      else
      {
        // verify number over network
        $result = \Provisioning::verifyPortInEligibility($number_to_port);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        // verify number not in system
        $result = \Provisioning::verifyNumberNotInSystem($number_to_port);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        // create ULTRA customer in DB
        $params = array(
          'masteragent'           => $sim->INVENTORY_MASTERAGENT ? $sim->INVENTORY_MASTERAGENT : $masteragent,
          'distributor'           => $sim->INVENTORY_DISTRIBUTOR ? $sim->INVENTORY_DISTRIBUTOR : $distributor,
          'dealer'                => $dealer,
          'userid'                => $userid,
          'preferred_language'    => $preferred_language,
          'cos_id'                => get_cos_id_from_plan('STANDBY'),
          'activation_cos_id'     => get_cos_id_from_plan( $target_plan ),
          'postal_code'           => $zipcode,
          'country'               => 'USA',
          'plan_state'            => 'Neutral',
          'plan_started'          => 'NULL',
          'plan_expires'          => 'NULL',
          'customer_source'       => 'PORTAL',
          'current_iccid'         => $sim->ICCID_NUMBER,
          'current_iccid_full'    => $sim->ICCID_FULL,
          'current_mobile_number' => $number_to_port
        );

        if (!empty($email)) {
          $params['e_mail'] = $email;
        }

        if (!empty($contact_phone)) {
          $params['contact_phone'] = $contact_phone;
        }

        $result  = create_ultra_customer_db_transaction($params);

        // new customer
        $customer = $result['customer'];

        if ( ! $customer )
          $this->errException('ERR_API_INTERNAL: failed to create customer', 'DB0001');

        customer_reset_first_last_name($customer->CUSTOMER_ID);

        // transition name for normal port
        $transition_name = 'Request Port ' . get_plan_name_from_short_name($target_plan);
      }

      // fund customer with the orange SIM
      $addStoredValueResult = func_add_stored_value_from_orange_sim(array(
        'amount'   => $sim->STORED_VALUE,
        'customer' => $customer,
        'session'  => $actcode,
        'cos_id'   => get_cos_id_from_plan($target_plan),
        'detail'   => __FUNCTION__
      ));
        
      if ( ! $addStoredValueResult->is_success())
        $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');

      log_funding_in_activation_history($customer->CUSTOMER_ID , 'PREPAID' , $sim->STORED_VALUE);

      // save activation attribution info in redis before attempting state transition
      set_redis_provisioning_values(
        $customer->CUSTOMER_ID,
        $dealer_info->masterId,
        $dealer,
        $dealer_info->distributorId,
        $dealer,
        $userid,
        $redis
      );

      // prepare transition context
      $context['customer_id']              = $customer->CUSTOMER_ID;
      $context['port_in_msisdn']           = $number_to_port;
      $context['port_in_iccid']            = $sim->ICCID_FULL;
      $context['port_in_account_number']   = $port_account_number;
      $context['port_in_account_password'] = $port_account_password;
      $context['port_in_zipcode']          = $zipcode;

      // initiate state transition Neutral -> Port-In Requested but do not resolve it now
      $changeStateResult = change_state($context, FALSE, $transition_name, 'take transition', FALSE, 1);
      if ( $changeStateResult['success'] != 1 )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException( 'ERR_API_INTERNAL: state transition error (2)', 'SM0001' );
      }

      reserve_iccid($sim->ICCID_FULL);

      // record MSISDN into ULTRA.HTT_ACTIVATION_HISTORY
      \Provisioning::updateActivationHistoryForMsisdn($customer->CUSTOMER_ID, $number_to_port);

      // add transition UUID to result
      $array_values = array_values($changeStateResult['transitions']);
      $request_id   = end($array_values);
      $this->addToOutput('request_id', $request_id);

      // memorize $request_id in Redis by customer_id for portal__SearchActivationHistoryDealer
      $redis_port = new \Ultra\Lib\Util\Redis\Port();
      $redis_port->setRequestId($customer->CUSTOMER_ID, $request_id);

      // create zsession
      $_REQUEST['zsession'] = session_save($customer, NULL, $redis);
      $verified = verify_session($customer->CUSTOMER);
      if (! $verified)
        $this->errException('ERR_API_INTERNAL: failed to create user session', 'SE0001');
      $this->addToOutput('zsession', $verified[2]);
      setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER['SERVER_NAME']);

      $session = new \Session();

      if (!$session->confirm($customer->CUSTOMER_ID))
      {
        throw new \Exception('ERR_API_INTERNAL: A problem occurred when trying to create a session.');
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' , $e->getMessage());
    }

    $customer = (isset($customer) && is_object($customer)) ? $customer : null;
    $this->recordFraudEvent($customer);

    return $this->result;
  }
}

