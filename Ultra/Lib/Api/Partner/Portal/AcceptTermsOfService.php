<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Utilities\Common;

/**
 * Class AcceptTermsOfService
 * @package Ultra\Lib\Api\Partner\Portal
 */
class AcceptTermsOfService extends Portal
{
  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * AcceptTermsOfService constructor.
   * @param Common $utilities
   * @param Session $session
   * @param CustomerRepository $customerRepository
   */
  public function __construct(Common $utilities, Session $session, CustomerRepository $customerRepository)
  {
    $this->utilities = $utilities;
    $this->session = $session;
    $this->customerRepository = $customerRepository;
  }

  /**
   * portal__AcceptTermsOfService
   * Record when the customer accepts the Terms Of Service
   * @return Result object
   */
  public function portal__AcceptTermsOfService()
  {
    try
    {
      $this->utilities->teldataChangeDb();

      // MOB-46: try Session first
      if (!$this->session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');
      $customer_id = $this->session->customer_id;

      if (!$this->customer = $this->customerRepository->getCustomerById($customer_id, ['preferred_language'], true))
      {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }

      $this->setOutputLanguage($this->customer->preferred_language);

      if (!$this->customer->acceptTermsOfService())
      {
        return $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
