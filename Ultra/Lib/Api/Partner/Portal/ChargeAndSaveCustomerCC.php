<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Billing\Repositories\Mssql\BillingHistoryRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\DB\Customer as Customer;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use Ultra\Configuration\Configuration;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;

class ChargeAndSaveCustomerCC extends Portal
{
  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var BillingHistoryRepository
   */
  private $billingHistoryRepository;

  private $freeSimReplacement = false;

  /**
   * ChargeAndSaveCustomerCC constructor.
   * @param SimRepository $simRepository
   * @param BillingHistoryRepository $billingHistoryRepository
   */
  public function __construct(
    SimRepository $simRepository,
    BillingHistoryRepository $billingHistoryRepository,
    Configuration $configuration,
    FeatureFlagsClientWrapper $flagsObj
  ) {
    $this->simRepository = $simRepository;
    $this->billingHistoryRepository = $billingHistoryRepository;
    $this->configuration = $configuration;
    $this->flagsObj = $flagsObj;
  }

  /**
   * portal__ChargeAndSaveCustomerCC
   *
   * Tokenizes a card so that it can be used for a charge. Attempts charge. Removes CC info from DB if customer doesn't wish to save info.
   *
   * @return Result object
   */
  public function portal__ChargeAndSaveCustomerCC()
  {
    list ($remove_card, $charge_amount, $target_plan, $description, $reason, $cc_name, $cc_address1, $cc_address2, $cc_city, $cc_country, $cc_state_or_region, $cc_postal_code, $account_cc_number, $account_cc_exp, $account_cc_cvv, $enroll_auto_recharge, $free_sim_replacement, $token, $processor) = $this->getInputValues();

    // default value for $description
    if ( !$description )
      $description = $reason;

    try
    {
      // connect to the DB
      teldata_change_db();

      // default to MeS processor
      $processor = !empty($processor) ? strtolower($processor) : 'mes';
      $processorConfig = $this->configuration->getCCProcessorByName($processor);
      if (empty($processorConfig)) {
        throw new UnhandledCCProcessorException('Missing processor configuration', 'IN0002');
      }

      $this->freeSimReplacement = !empty($free_sim_replacement) ? (bool) $free_sim_replacement : $this->freeSimReplacement;

      // validate $target_plan for PREPAY_PLAN and ORDER_SIM
      if ( ( ( $reason == 'ORDER_SIM' ) || ( $reason == 'PREPAY_PLAN' ) )
        && ( ! $target_plan )
      )
        $this->errException('ERR_API_INVALID_ARGUMENTS: "target_plan" is required for the given "reason"','MP0011');

      // validate credit card info dependency
      if ( $account_cc_number
        && ( ! $account_cc_exp || ! $account_cc_cvv || ! $cc_postal_code )
      )
        $this->errException('ERR_API_INVALID_ARGUMENTS: some credit card info are missing','VV0037');

      // API-424: check sesison first then legasy zsession
      $session = new \Session();
      if ($session->customer_id)
        $customer = get_customer_from_customer_id($session->customer_id);
      else // legacy zsession
      {
        // get subscriber from zsession cookie
        if (empty($_COOKIE['zsession']))
          $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
        $customer = getCustomerFromSession($_COOKIE['zsession']);
      }

      $this->setOutputLanguage($customer->preferred_language);

      // RECHARGE needs plan_state = 'Active'
      if ( ( $reason == 'RECHARGE' )
        && ( $customer->plan_state != 'Active' )
      )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state not valid for the given "reason"','VV0105');

      // sanity check for REPLACE_SIM required funds
      if (($reason == 'REPLACE_SIM') && !$this->freeSimReplacement && (self::REPLACE_SIM_COST > (($customer->BALANCE * 100) + $charge_amount))) {
        $this->errException('ERR_API_INVALID_ARGUMENTS: insufficient "charge_amount" for SIM replacement', 'VV0103');
      }

      // validation for PREPAY_PLAN
      if ( $reason == 'PREPAY_PLAN' )
      {
        $plan_cost = get_plan_cost_from_cos_id(get_cos_id_from_plan($plan));

        // validate $target_plan vs $charge_amount
        if ( $plan_cost != $charge_amount )
          $this->errException('ERR_API_INVALID_ARGUMENTS: "target_plan" does not match "charge_amount"','VV0089');

        // plan_state must be 'Neutral'
        if ( $customer->plan_state != 'Neutral' )
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer state not valid for the given "reason"','VV0105');
      }

      // only if $account_cc_number is provided, validate and store credit card info - this includes tokenization
      if ( ($account_cc_number || $token) && $charge_amount )
      {
        $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
          array(
            'customer_id'     => $customer->CUSTOMER_ID,
            'cc_number'       => $account_cc_number,
            'cvv'             => $account_cc_cvv,
            'expires_date'    => $account_cc_exp,
            'cc_address1'     => $cc_address1,
            'cc_address2'     => $cc_address2,
            'cc_postal_code'  => $cc_postal_code,
            'cc_name'         => $cc_name,
            'cc_city'         => $cc_city,
            'cc_country'      => $cc_country,
            'cc_state_region' => $cc_state_or_region,
            'token'           => $token,
            'processor'       => $processor
          )
        );

        if ( $result->is_failure() )
        {
          $errors = $result->get_errors();

          $this->errException( $errors[0] , $result->data_array['error_codes'][0] );
        }
      }

      $include_taxes_fees = ! ( ( $reason == 'ORDER_SIM' ) || ( $reason == 'REPLACE_SIM' ) );

      $cos_id = ( $target_plan ) ? get_cos_id_from_plan( $target_plan ) : $customer->COS_ID ;

      // used only for 'RECHARGE'
      $amount_needed = 0;

      // 'RECHARGE' special logic
      if ( $reason == 'RECHARGE' )
      {
        // determine the customers's $plan_amount
        $plan =
          ( ( ! is_null($customer->MONTHLY_RENEWAL_TARGET) ) && ( $customer->MONTHLY_RENEWAL_TARGET != '' ) )
          ?
          $customer->MONTHLY_RENEWAL_TARGET
          :
          get_plan_from_cos_id( $customer->COS_ID )
          ;

        $plan_amount = get_plan_cost_from_cos_id(get_cos_id_from_plan($plan));

        dlog('',"customer applicable plan = $plan ; customer applicable cost = $plan_amount");

        $amount_needed = $plan_amount - ( $customer->stored_value * 100 ) ; // in cents

        dlog('',"BALANCE = ".( $customer->BALANCE * 100 )." ; charge_amount = $charge_amount ; amount_needed = $amount_needed ; plan_amount = $plan_amount");

        if ( ( ( $customer->BALANCE * 100 ) + $charge_amount ) < $amount_needed )
          $this->errException('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');
      }

      if ( ( $reason == 'REPLACE_SIM' ) || ( $reason == 'RECHARGE' ) )
        // refresh customer object ( TODO: maybe a lighter query? )
        $customer = get_customer_from_customer_id( $customer->CUSTOMER_ID );

      // REPLACE_SIM: check for free replacement sim condition
      if ( $reason == 'REPLACE_SIM' && $this->freeSimReplacement ) {
        // Dont charge if ultra customer has a non GBA sim and hasn't ordered a replacement
        $sim = $this->simRepository->getSimInventoryAndBatchInfoByIccid($customer->CURRENT_ICCID_FULL);
        if (!$sim->allowsWifiSoc() && $customer->BRAND_ID == 1) {
          $billingHistory = $this->billingHistoryRepository->getBillingTransactionsByDescription($customer->CUSTOMER_ID, [], 'SIM Replacement - Free GBA SIM');
          if (empty($billingHistory)) {
            $charge_amount = 0;
            \logInfo('Free SIM replacement condition met');
          }
        }
      }

      if ( $charge_amount && !$this->freeSimReplacement)
      {
        // check if would exceed maximum allowed amounts
        if ($reason == 'RECHARGE')
        {
          // set the customer's maximum allowed stored_value to be the greater of their current plan amount and future plan amount
          // https://issues.hometowntelecom.com:8443/browse/API-1168

          // plan cost returns in cents
          $plan_cost     = get_plan_cost_from_cos_id($customer->cos_id);
          $bolt_ons_cost = get_bolt_ons_costs_by_customer_id( $customer->CUSTOMER_ID );

          // target cost in cents
          $target_cost = ($customer->MONTHLY_RENEWAL_TARGET)
            ? get_plan_cost_from_cos_id(get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET))
            : 0;

          // use greater of two costs
          $plan_cost = ($plan_cost > $target_cost) ? $plan_cost : $target_cost;

          if ( (($charge_amount / 100) + $customer->stored_value) > ($plan_cost / 100) + $bolt_ons_cost )
            $this->errException('ERR_API_INVALID_ARGUMENTS: Charge would exceed maximum allowed stored value amount', 'VV0115');
        }
        else
        {
          // maximum allowed wallet balance is 200
          // https://issues.hometowntelecom.com:8443/browse/API-1168
          if ( (($charge_amount / 100) + $customer->BALANCE) > 200 )
            $this->errException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet', 'CH0001');
        }

        // charge customer's credit card ( attempt transition to Active if Suspended or Provisioned )
        $disableFraudCheck = $this->flagsObj->disableRateLimits($customer->CUSTOMER_ID);
        $result = func_add_funds_by_tokenized_cc(
          array(
            'customer'           => $customer,
            'charge_amount'      => ( $charge_amount / 100 ),
            'description'        => $description,
            'session'            => $this->getRequestId(),
            'reason'             => $reason,
            'detail'             => __FUNCTION__,
            'fund_destination'   => ( ( $reason == 'RECHARGE' ) ? 'stored_value' : 'balance' ),
            'include_taxes_fees' => $include_taxes_fees,
            'target_cos_id'      => $cos_id
          ),
          $disableFraudCheck
        );

        if ( $result->is_failure() )
        {
          // error messages at this level can be fine tuned
          $errors = $result->get_errors();

          $friendly_error = null;

          preg_match("/\((\d+)\)/", $errors[0], $matches);

          if ( ! empty($matches[1]))
          {
            $messages = \Ultra\Lib\MeS\getMeaningfulApiErrorMessage($matches[1]);
            if ( isset($messages[$customer->preferred_language]) )
              $friendly_error = $messages[$customer->preferred_language];
          }

          if ($friendly_error)
            $this->errException( $errors[0] , 'CC0001', $friendly_error );
          else
            $this->errException( $errors[0] , 'CC0001' );
        }
      }

      if ($reason == 'REPLACE_SIM') {
        // send off another SIM
        $result = inventory_individual_ship_sim($customer->CUSTOMER_ID, __FUNCTION__);
        if (!$result['success']) {
          $this->errException("ERR_API_INTERNAL: {$result['error']}", $result['code']);
        }

        // handle charge from balance
        $chargeParams = [
          'customer_id'      => $customer->CUSTOMER_ID,
          'amount'           => (self::REPLACE_SIM_COST / 100), // in $
          'detail'           => __FUNCTION__,
          'reason'           => 'SIM Replacement',
          'reference'        => $this->getRequestId(),
          'source'           => 'SHIP',
          'reference_source' => 'WEBPORTAL'
        ];
        if ($this->freeSimReplacement && $charge_amount == 0) {
          $chargeParams['amount'] = 0;
          $chargeParams['reason'] = 'SIM Replacement - Free GBA SIM';
        }

        $result = func_spend_from_balance($chargeParams);
        if (count($result['errors'])) {
          $this->errException($result['errors'][0], 'DB0001');
        }
      }

      // ORDER_SIM: fund SIM shipment
      if ( $reason == 'ORDER_SIM' )
      {
        // minus the sim card cost and the shipping cost from customer's balance

        $result1 = func_spend_from_balance(
          array(
            'customer_id' => $customer->CUSTOMER_ID,
            'amount'      => ( \sim_cost() / 100 ),
            'detail'      => __FUNCTION__,
            'reason'      => 'SIM Purchase',
            'reference'   => 'SIM_PAYMENT',
            'source'      => 'SPEND',
            'commissionable'   => 1,
            'reference_source' => $this->getRequestId(),
            'target_cos_id'    => $cos_id
          )
        );

        $result2 = func_spend_from_balance(
          array(
            'customer_id' => $customer->CUSTOMER_ID,
            'amount'      => ( \shipping_cost() / 100 ),
            'detail'      => __FUNCTION__,
            'reason'      => 'SIM Shipment',
            'reference'   => 'SHIPPING_PAYMENT',
            'source'      => 'SPEND',
            'commissionable'   => 0,
            'reference_source' => $this->getRequestId(),
            'target_cos_id'    => $cos_id
          )
        );

        $errors = array_merge( $result1['errors'] , $result2['errors'] );

        if ( count($errors) )
        {
          dlog('',"func_spend_from_balance returned errors = %s",$errors);

          $this->errException('ERR_API_INTERNAL: DB error during func_spend_from_balance', 'DB0001');
        }
      }

      // PREPAY_PLAN: spend balance for SIM cost
      if ( $reason == 'PREPAY_PLAN' )
      {
        $result = func_spend_from_balance(
          array(
           'customer_id' => $customer->CUSTOMER_ID,
           'amount'      => ( \sim_cost() / 100 ),
           'detail'      => __FUNCTION__,
           'reason'      => 'SIM Purchase',
           'reference'   => 'SIM_PAYMENT',
           'source'      => 'SPEND',
           'commissionable'   => 1,
           'reference_source' => $this->getRequestId(),
           'target_cos_id'    => $cos_id
          )
        );

        if ( count($result['errors']) )
        {
          dlog('',"func_spend_from_balance returned errors = %s",$result['errors']);

          $this->errException('ERR_API_INTERNAL: DB error during func_spend_from_balance', 'DB0001');
        }
      }

      // RECHARGE: transfer plan cost to stored_value if insufficient
      if ( ( $reason == 'RECHARGE' )
        && ( $amount_needed > ( $customer->stored_value * 100 ) )
      )
      {
        dlog('',"invoking func_sweep_balance_to_stored_value with value = ".( ( $amount_needed / 100 ) - $customer->stored_value ));

        // move amount from balance to stored value
        $result = func_sweep_balance_to_stored_value(
          array(
            'customer'      => $customer,
            'sweep_value'   => ( ( $amount_needed / 100 ) - $customer->stored_value ), // in $
            'reference'     => 'ChargeAndSaveCustomerCC',
            'source'        => 'ChargeAndSaveCustomerCC',
            'store_zipcode' => NULL,
            'store_id'      => NULL,
            'clerk_id'      => NULL,
            'terminal_id'   => NULL
          )
        );

        if ( ! $result['success'] )
        {
          dlog('',"func_sweep_balance_to_stored_value returned errors = %s",$result['errors']);

          $this->errException('ERR_API_INTERNAL: DB error during func_sweep_balance_to_stored_value', 'DB0001');
        }
      }

      // ORDER_SIM or PREPAY_PLAN: transition to Pre-Funded
      if ( ( $reason == 'ORDER_SIM' ) || ( $reason == 'PREPAY_PLAN' ) )
      {
        // transition to state [ Pre-Funded ]

        $max_path_depth  = 1;
        $dry_run         = FALSE;
        $resolve_now     = TRUE;
        $context         = array( 'customer_id' => $customer->CUSTOMER_ID );

        $transition_name = ( ( $reason == 'ORDER_SIM' ) ? 'Ship Ordered SIM ' : 'Pre-Fund SIM ' ) . get_plan_name_from_short_name($target_plan) ;

        $result = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

        if ( ! $result['success'] )
        {
          dlog('',"change_state returned errors = %s",$result['errors']);

          $this->errException('ERR_API_INTERNAL: state transition error', 'SM0001');
        }
      }

      // ORDER_SIM: send email about shipping data
      if ( $reason == 'ORDER_SIM' )
      {
        $tracking_number      = '';
        $shipping_data_output = \get_shipping_data( $customer->CUSTOMER_ID );

        dlog('',"shipping_data_output = %s",$shipping_data_output);

        if ( isset($shipping_data_output['shipping_estimate']) )
        {
          $shippingEstimate = $shipping_data_output['shipping_estimate'];
          $tracking_number  = $shipping_data_output['tracking_number'];
        }

        $email_sent = \funcSendExemptCustomerEmail_ultra_sim_shipped(
          array(
            'customer'        => $customer,
            'tracking_number' => $tracking_number
          )
        );

        if ( ! $email_sent )
          $this->addWarning( 'Could not send email to customer '.$customer->CUSTOMER_ID);
      }

      // removes CC info from DB if customer doesn't wish to save it.

      if ( $remove_card )
      {
        $result = \Ultra\Lib\DB\Setter\Customer\disable_cc_info(
          array(
            'customer_id'    => $customer->CUSTOMER_ID,
            'cc_number'      => $account_cc_number,
            'expires_date'   => $account_cc_exp
          )
        );

        if ( $result->is_failure() )
        {
          $errors = $result->get_errors();

          $this->errException( $errors[0] , 'DB0001' );
        }
      }

      if ($enroll_auto_recharge || ($customer->monthly_cc_renewal == 1 && $enroll_auto_recharge == "0"))
      {
        Customer\updateAutoRecharge($customer->CUSTOMER_ID, $enroll_auto_recharge);
      }

      $this->addToOutput('enroll_auto_recharge', $enroll_auto_recharge);

      $this->succeed ();
    } catch(InvalidObjectCreationException $e) {
      $this->addError('Failed to find customers SIM. ' . $e->getMessage(), $e->code());
    } catch(\Exception $e) {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
