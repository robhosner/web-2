<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';
require_once 'classes/CustomerOptions.php';

class ChangePlan extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__ChangePlan
   * @return Result object
   */
  public function portal__ChangePlan()
  {
    list($targetPlan, $immediate) = $this->getInputValues();

    try
    {
      if ($targetPlan == 'UF15')
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid target plan', 'MP0001');

      $session = new \Session();
      if ( ! $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');

      teldata_change_db();

      // get customer
      if ( ! $customer = get_customer_from_customer_id($session->customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      // check if target plan is part of customers current brand
      if ( ! isSameBrand(get_plan_from_cos_id($customer->COS_ID), $targetPlan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous and future plan brands do not match', 'MP0011');

      $current_plan = get_plan_from_cos_id( $customer->COS_ID );

      // changePlanImmediate
      if ($immediate)
      {
        // make sure target plan is not customers current plan
        if ( $current_plan == $targetPlan )
          $this->errException("ERR_API_INVALID_ARGUMENTS: Customer is already in plan $current_plan", 'IN0001');

        // customer should be in ACTIVE or SUSPENDED states
        if ( ! in_array($customer->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED)))
        {
          $this->errException('ERR_API_INTERNAL: The customer is not eligible to change their plan', 'DB0001');
        }
        else
        {
          if ($customer->plan_state == STATE_ACTIVE)
          {
            // check for ineligible options
            $customerOptions = new \CustomerOptions($customer->CUSTOMER_ID);

            if ($customerOptions->hasOption(BILLING_OPTION_MULTI_MONTH))
              $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105');

            // check if target plan is not lower than current plan
            $dollar_current_plan = get_plan_cost_from_cos_id($customer->COS_ID);
            $dollar_targetPlan = get_plan_cost_from_cos_id(get_cos_id_from_plan($targetPlan));

            dlog('',"current_plan = $current_plan ($dollar_current_plan)");
            dlog('',"targetPlan   = $targetPlan ($dollar_targetPlan)");

            if ( $dollar_current_plan > $dollar_targetPlan )
              $this->errException('ERR_API_INVALID_ARGUMENTS: Cannot change plan to a lower one.', 'IN0001');

            // transition to new plan
            $result = transition_customer_plan($customer, $current_plan, $targetPlan, 'Mid-Cycle');

            if (count($result['errors']))
              $this->errException($result['errors'][0], 'VV0001');
          }
          // plan_state is Suspended
          else
          {
            // change state
            $context = array('customer_id' => $customer->CUSTOMER_ID);
            $result  = change_suspended_plan($customer->plan_state, $customer, $context, $targetPlan);

            if (count($result['errors']))
              $this->errException($result['errors'][0], 'VV0001');
          }
        }
      }
      // changePlanFuture
      else
      {
        // check for ineligible options
        $customerOptions = new \CustomerOptions($customer->CUSTOMER_ID);

        if ($customerOptions->hasOption(array('BILLING.MRC_7_11', BILLING_OPTION_MULTI_MONTH)))
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105');

        // update MONTHLY_RENEWAL_TARGET
        $result = func_reset_monthly_renewal_target($customer->CUSTOMER_ID, $targetPlan);

        if ( ! $result['success'] )
          $this->errException('ERR_API_INTERNAL: Error updating table', 'DB0001');
      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage());
    }

    return $this->result;
  }
}
