<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Services\OnlineSalesAPI;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Payments\Payment;

class PayForOrder extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var CustomerRepository
   */
  private $customerRepo;

  /**
   * @var OnlineSalesAPI
   */
  private $ordersAPI;

  /**
   * PayForOrder constructor.
   * @param Session $session
   * @param Configuration $configuration
   * @param CustomerRepository $customerRepo
   * @param OnlineSalesAPI $ordersAPI
   */
  public function __construct(
    Session $session,
    Configuration $configuration,
    CustomerRepository $customerRepo,
    OnlineSalesAPI $ordersAPI,
    Payment $paymentObj
  ) {
    $this->session = $session;
    $this->configuration = $configuration;
    $this->customerRepo = $customerRepo;
    $this->ordersAPI = $ordersAPI;
    $this->paymentObj = $paymentObj;
  }

  public function portal__PayForOrder()
  {
    list ($order_id, $funds_source, $zip_code) = $this->getInputValues();

    $funds_source = strtoupper($funds_source);

    try {
      // verify session
      if (!$customer_id = $this->session->customer_id) {
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }
      
      // get customer
      $customer = $this->customerRepo->getCombinedCustomerByCustomerId($customer_id);
      if (!$customer) {
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
      }

      // get order
      if (empty($zip_code)) {
        $zip_code = '92626';
      }
      $order = $this->getOrder($order_id, $customer_id, $zip_code);

      // set amount
      if ($funds_source == 'CC') {
        $amount = $order['cost']['creditCard']['totalAmount'];
      } else {
        $amount = $order['cost']['wallet']['totalAmount'];
      }

      // add funds to wallet via CC payment
      if ($funds_source == 'CC') {
        $this->addFromCC($this->paymentObj, $customer, $amount);
      }

      // use funds to pay for order
      $this->spendFromWallet($this->paymentObj, $customer->customer_id, $amount, $order['orderId']);
      
      // mark order as paid
      $paidResult = $this->ordersAPI->markOrderPaid($order_id, $zip_code, $amount);
      if (!$paidResult->is_success()) {
        throw new CustomErrorCodeException('Failed to mark order as paid', 'IN0002');
      }

      $this->succeed();
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Adds funds to the user's wallet via their credit card
   * @param int order_id
   * @param int customer_id
   * @param string zipCode
   * @return array order information
   */
  private function getOrder($order_id, $customer_id, $zipCode)
  {
    $orderResult = $this->ordersAPI->getOrderByOrderID($order_id, $zipCode);
    if (!$orderResult->is_success()) {
      $this->result->add_result_object($orderResult);
      throw new \Exception('Failed to retrieve order');
    }

    $order = $orderResult->data_array;

    // verify status
    if (empty($order['status'])) {
      throw new CustomErrorCodeException('Missing order status', 'IN0002');
    } else if ($order['status'] != 'created') {
      throw new CustomErrorCodeException('Invalid order status of ' . $order['status'], 'IN0002');
    }

    // verify order is for customer
    foreach ($order['items'] as $item) {
      if (!empty($item['customerId']) && $item['customerId'] != $customer_id) {
        throw new CustomErrorCodeException('Invalid order for customer', 'IN0002');
      }
    }

    return $order;
  }

  /**
   * Adds funds to the user's wallet via their credit card
   * @param Payment $paymentObj
   * @param stdClass $customer
   * @param integer $amount
   * @return null
   */
  private function addFromCC(Payment $paymentObj, $customer, $amount)
  {
    $result = $paymentObj->funcAddFundsByTokenizedCC([
      'customer'           => $customer,
      'charge_amount'      => $amount / 100,
      'description'        => 'Order payment',
      'session'            => $this->getRequestId(),
      'reason'             => 'Order payment',
      'detail'             => __FUNCTION__,
      'fund_destination'   => 'balance',
      'include_taxes_fees' => false
    ]);

    if ( $result->is_failure() ) {
      $errors = $result->get_errors();
      $this->errException( $errors[0] , 'CC0001' );
    }
  }

  /**
   * Spends from a user's wallet to pay for an order
   * @param customer_id $customer_id
   * @param integer $amount
   * @param integer $orderId
   * @return null
   */
  private function spendFromWallet(Payment $paymentObj, $customerID, $amount, $orderId)
  {
    $result = $paymentObj->funcSpendFromBalance([
      'customer_id'      => $customerID,
      'amount'           => ($amount / 100),
      'detail'           => 'Pay for order',
      'reason'           => 'Order payment',
      'reference'        => 'ORDER #' . $orderId,
      'source'           => 'SPEND',
      'commissionable'   => 1,
      'reference_source' => create_guid('PHPAPI')
    ]);
    if (count($result['errors'])) {
      throw new CustomErrorCodeException($result['errors'][0], 'DB0001');
    }
  }
}
