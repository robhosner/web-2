<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Payments\Payment;
use Ultra\Utilities\Common as CommonUtilities;
use Ultra\Utilities\SessionUtilities;

class ValidatePIN extends Portal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var SessionUtilities
   */
  private $sessionUtilities;

  /**
   * @var Payment
   */
  private $payment;

  /**
   * ValidatePIN constructor.
   * @param CustomerRepository $customerRepository
   * @param SessionUtilities $sessionUtilities
   * @param Payment $payment
   */
  public function __construct(CustomerRepository $customerRepository, SessionUtilities $sessionUtilities, Payment $payment)
  {
    $this->customerRepository = $customerRepository;
    $this->sessionUtilities = $sessionUtilities;
    $this->payment = $payment;
  }

  /**
   * portal__ValidatePIN
   *
   * verify that the PIN card is in our system, it has not been used and not disabled
   * @param string PIN
   * @return Result object
   */
  public function portal__ValidatePIN()
  {
    // init
    list ($pin) = $this->getInputValues();
    $this->addToOutput('pin_value', NULL);
    $this->addToOutput('used_date', NULL);
    $this->addToOutput('used_msisdn', NULL);

    try {
      // get subscriber from zsession cookie
      teldata_change_db();
      if (empty($_COOKIE['zsession'])) {
        return $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
      }

      $zsession = $_COOKIE['zsession'];
      if (!$customer = $this->customerRepository->getCustomerFromSession($zsession)) {
        return $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');
      }

      // block if customer called this API more than 5 times in an hour
      if ($this->sessionUtilities->checkApiAbuseByIdentifier(__FUNCTION__, $customer->customer_id, 10)) {
        return $this->errException('ERR_API_INTERNAL: command disabled; please try again later', 'AP0001');
      }

      // validate PIN
      $pin_validation = $this->payment->funcValidatePinCards(['pin_list' => [$pin]]);

      if ($pin_validation['at_least_one_not_found']) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: the PIN is invalid', 'PI0001');
      }

      if ($pin_validation['at_least_one_at_foundry']) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: the PIN is not active', 'PI0002');
      }

      if (count($pin_validation['errors'])) {
        dlog('', '%s', $pin_validation['errors']);
        return $this->errException('ERR_API_INVALID_ARGUMENTS: validation failed', 'PI0003');
      }

      if ($pin_validation['at_least_one_customer_used']) {
        // fill 'used_date' and 'used_msisdn'
        if ($pin_validation['result'][$pin]['customer_used']) {
          $customer = $this->customerRepository->getCustomerById($pin_validation['result'][$pin]['customer_used'], ['current_mobile_number']);
          if ($customer) {
            $this->addToOutput('used_msisdn', $customer->current_mobile_number);
          }
        }

        $this->addToOutput('used_date', (new CommonUtilities())->getDateFromFullDate($pin_validation['result'][ $pin ]['last_changed_date']));
        return $this->errException('ERR_API_INVALID_ARGUMENTS: the PIN is already used', 'PI0004');
      }

      // fill in 'pin_value'
      $this->addToOutput('pin_value', $pin_validation['result'][$pin]['pin_value'] * 100);
      $this->succeed();
    } catch(MissingRequiredParametersException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
