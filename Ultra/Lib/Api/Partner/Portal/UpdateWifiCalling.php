<?php
namespace Ultra\Lib\Api\Partner\Portal;

use CommandInvocation;
use Session;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Address;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\DatabaseErrorException;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\InvalidSimStateException;
use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Mvne\MakeItSo\Interfaces\MakeItSoRepositoryInterface;
use Ultra\Mvne\UpdateWifiCallingService;
use Ultra\Sims\Interfaces\SimRepository;
use Ultra\Utilities\Common;

/**
 * Class UpdateWifiCalling
 * @package Ultra\Lib\Api\Partner\Portal
 */
class UpdateWifiCalling extends Portal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var UpdateWifiCallingService
   */
  public $updateWifiCallingService;

  /**
   * @var MakeItSoRepositoryInterface
   */
  private $makeItSoRepository;

  /**
   * @var CommandInvocation
   */
  private $commandInvocation;

  /**
   * UpdateWifiCalling constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param SimRepository $simRepository
   * @param Common $utilities
   * @param CommandInvocation $commandInvocation
   * @param MakeItSoRepositoryInterface $makeItSoRepository
   */
  public function __construct(
    Session $session,
    CustomerRepository $customerRepository,
    Configuration $configuration,
    SimRepository $simRepository,
    Common $utilities,
    CommandInvocation $commandInvocation,
    MakeItSoRepositoryInterface $makeItSoRepository
  )
  {
    $this->customerRepository = $customerRepository;
    $this->configuration = $configuration;
    $this->simRepository = $simRepository;
    $this->session = $session;
    $this->utilities = $utilities;
    $this->commandInvocation = $commandInvocation;
    $this->makeItSoRepository = $makeItSoRepository;
  }

  public function portal__UpdateWifiCalling()
  {
    list ($address1, $address2, $city, $state, $zipcode, $enable_wifi_calling) = $this->getInputValues();

    try {
      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }

      $this->updateWifiCallingService = $this->setWifiCallingService(
        $address1,
        $address2,
        $city,
        $state,
        $zipcode,
        $enable_wifi_calling
      );

      $this->updateWifiCallingService->execute();
      $this->succeed();

    } catch(InvalidObjectCreationException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(InvalidSimStateException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(MiddleWareException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(DatabaseErrorException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  public function setWifiCallingService($address1, $address2, $city, $state, $zipcode, $enable_wifi_calling)
  {
    if (empty($this->updateWifiCallingService)) {
      return new UpdateWifiCallingService(
        new Address([
          'address1' => $enable_wifi_calling ? $address1 : '>__<',
          'address2' => $address2,
          'city' => $city,
          'state_region' => $state,
          'postal_code' => $zipcode,
        ]),
        $this->customerRepository,
        $this->configuration,
        $this->simRepository,
        $this->utilities,
        $this->session->customer_id,
        $enable_wifi_calling,
        $this->getRequestId(),
        $this->commandInvocation,
        $this->makeItSoRepository,
        'WIFI_CALLING.PORTAL'
      );
    } else {
      return $this->updateWifiCallingService;
    }
  }
}
