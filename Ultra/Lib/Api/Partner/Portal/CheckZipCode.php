<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Lib\Api\Partner\Portal;
use Ultra\Utilities\Validator;

class CheckZipCode extends Portal
{
  /**
   * @var Validator
   */
  private $validator;

  /**
   * CheckZipCode constructor.
   * @param Validator $validator
   */
  public function __construct(Validator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * portal__CheckZipCode
   * check if the given zip code is in the activation footprint
   * @return Result object
   */
  public function portal__CheckZipCode()
  {
    // init
    list ($zipcode) = $this->getInputValues();
    $valid = false;

    try {
      // get ZIP coverage
      teldata_change_db();
      $valid = $this->validator->validateZipCodeIsInCoverage($zipcode);
      $this->succeed();
    } catch(\Exception $e) {
      dlog('' , 'EXCEPTION: ' . $e->getMessage());
    }

    $this->addToOutput('zipcode_valid', $valid);
    return $this->result;
  }
}
