<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Accounts\Interfaces\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Customers\SendFamilyReplenishmentSms;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\Common;

/**
 * Class SetCustomerPreferences
 * @package Ultra\Lib\Api\Partner\Portal
 */
class SetCustomerPreferences extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Control
   */
  private $mwControl;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * @var CreditCardRepository
   */
  private $cardRepository;
  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var SendFamilyReplenishmentSms
   */
  private $sendFamilyReplenishmentSms;

  /**
   * SetCustomerPreferences constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param Control $control
   * @param Configuration $configuration
   * @param Common $utilities
   * @param AccountsRepository $accountsRepository
   * @param CreditCardRepository $cardRepository
   * @param Messenger $messenger
   * @param SendFamilyReplenishmentSms $sendFamilyReplenishmentSms
   */
  public function __construct
  (
    Session $session,
    CustomerRepository $customerRepository,
    Control $control,
    Configuration $configuration,
    Common $utilities,
    AccountsRepository $accountsRepository,
    CreditCardRepository $cardRepository,
    Messenger $messenger,
    SendFamilyReplenishmentSms $sendFamilyReplenishmentSms
  )
  {
    $this->session = $session;
    $this->customerRepository = $customerRepository;
    $this->mwControl = $control;
    $this->configuration = $configuration;
    $this->utilities = $utilities;
    $this->accountsRepository = $accountsRepository;
    $this->cardRepository = $cardRepository;
    $this->messenger = $messenger;
    $this->sendFamilyReplenishmentSms = $sendFamilyReplenishmentSms;
  }

  /**
   * portal__SetCustomerPreferences
   * update customer preferences which subscriber may change frequently
   * @see MOBIII-10
   * @return Result object
   */
  public function portal__SetCustomerPreferences()
  {
    list (
      $preferred_language,
      $auto_recharge,
      $marketing_sms,
      $marketing_email,
      $marketing_voice,
      $party_sms,
      $party_email,
      $party_voice,
      $tag
    ) = $this->getInputValues();

    try
    {
      // verify session
      if (!$customer_id = $this->session->customer_id)
      {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }

      // get subscriber object
      $customer = $this->customerRepository->getCustomerById($customer_id, [
        'preferred_language',
        'plan_state',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'CUSTOMER_ID'
      ]);

      if (!$customer)
      {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
      }

      if (!$account = $this->accountsRepository->getAccountFromCustomerId($customer_id, ['COS_ID']))
      {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: account not found.', 'VV0031');
      }

      $this->setCustomer($customer);
      $this->setOutputLanguage($this->customer->preferred_language);

      // cannot modify Cancelled subscriber
      if ($this->customer->plan_state == STATE_CANCELLED)
      {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
      }

      // at least one parameter is required (missing optional prameters are passed as empty strings)
      if ( $preferred_language === ''
        && $auto_recharge      === ''
        && $marketing_sms      === ''
        && $marketing_email    === ''
        && $marketing_voice    === ''
        && $party_sms          === ''
        && $party_email        === ''
        && $party_voice        === ''
      ) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing', 'MP0001');
      }

      // handle preferred language
      $update = [];
      if ($preferred_language)
      {
        // change voicemail language
        $languageSOC = $this->configuration->mapLanguageToVoicemailSoc($preferred_language);
        list($accSocsDefinition, $accSocsDefinitionByUltraSoc, $ultraSocsByPlanId) = $this->configuration->getAccSocsDefinitions();
        $result = $this->mwControl->mwMakeitsoUpgradePlan([
          'actionUUID'         => $this->utilities->getNewActionUUID(__FUNCTION__ . time()),
          'msisdn'             => $this->customer->current_mobile_number,
          'iccid'              => $this->customer->current_iccid_full,
          'customer_id'        => $this->customer->customer_id,
          'wholesale_plan'     => $this->customer->getWholesalePlan(),
          'ultra_plan'         => $this->configuration->getPlanFromCosId($account->cos_id),
          'preferred_language' => $this->customer->preferred_language,
          'option'             => $accSocsDefinitionByUltraSoc[$languageSOC]['ultra_service_name']
        ]);

        $this->utilities->teldataChangeDb();

        // log errors but continue execution
        if ($result->is_failure())
        {
          dlog('', 'errors = %s', $result->get_errors());
          $this->addWarning('Failed to set voicemail language');
        }

        // add DB update
        $update['preferred_language'] = $preferred_language;
      }

      // handle auto recharge
      if ($auto_recharge !== '')
      {
        // check if subscriber has credit card on file
        if (!$this->customer->hasCreditCard())
        {
          return $this->errException('ERR_API_INVALID_ARGUMENTS: No credit card on file', 'CC0005');
        }

        // handle when customer successfully turns off auto recharge
        if ($auto_recharge == 0)
        {
          $update['easypay_activated'] = 0;
        }
        else
        {
          $this->sendAutoRechargeSms();
        }

        $this->sendFamilyReplenishmentSms->sendSms($customer_id);

        $update['monthly_cc_renewal'] = $auto_recharge;
      }

      // submit updates for language and auto recharge and check result
      if (count($update))
      {
        $update['customer_id'] = $this->customer->customer_id;
        $result = $this->customer->updateUltraInfo($update);

        if ($result->is_failure())
        {
          $errors = $result->get_errors();
          $this->addWarning($errors[0]);

          return $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred (1)', 'DB0001');
        }
      }

      // handle marketing SMS and email
      $update = [];

      if ($tag)
        $tag .= ".";

      $options = [
        "$tag" . "OPT_OUT.3RDPARTY_VOICE" => $party_voice,
        "$tag" . "OPT_OUT.3RDPARTY_EMAIL" => $party_email,
        "$tag" . "OPT_OUT.3RDPARTY_SMS"   => $party_sms,
        "PREFERENCES.OPT_OUT.VOICE"   => $marketing_voice
      ];

      // input is opt-in value, so flip the value
      $pairs = [];
      foreach ($options as $attr => $val)
        if ($val !== '')
          $pairs[$attr] = !$val ? '1' : '0';

      if ($pairs)
        $result = $this->customer->setUltraCustomerOptionsByArray($customer_id, $pairs, [
          'OPT_OUT.3RDPARTY_VOICE',
          'OPT_OUT.3RDPARTY_EMAIL',
          'OPT_OUT.3RDPARTY_SMS'
        ]);

      if ($marketing_sms !== '')
      {
        $update['marketing_sms_option'] = $marketing_sms;
      }

      if ($marketing_email !== '')
      {
        $update['marketing_email_option'] = $marketing_email;
      }

      if (count($update))
      {
        $update['customer_id'] = $this->customer->customer_id;
        $errors = $this->customer->setMarketingSettings($update);

        if (count($errors))
        {
          dlog('', '%s', $errors);
          $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred (3)', 'DB0001');
        }
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  private function setCustomer($customer)
  {
    $this->customer = empty($this->customer) ? new Customer((array) $customer) : $this->customer;
  }

  private function sendAutoRechargeSms()
  {
    $creditCardInfo = $this->cardRepository->getCcInfoFromCustomerId($this->customer->customer_id);
    $lastFour = empty($creditCardInfo->LAST_FOUR) ? '' : $creditCardInfo->LAST_FOUR;

    $result = $this->messenger->enqueueImmediateSms($this->customer->customer_id, 'auto_recharge_active', ['last_four' => $lastFour]);

    if ($result->is_failure())
    {
      return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'SM0002');
    }
  }
}
