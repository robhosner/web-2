<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Lib\Api\Partner\Portal;

class KillSession extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * KillSession constructor.
   * @param Session $session
   */
  public function __construct(Session $session)
  {
    $this->session = $session;
  }

  /**
   * portal__KillSession
   * expire current cookie-based session
   * @return Result object
   */
  public function portal__KillSession()
  {
    try {
      if (!$this->session->kill()) {
        return $this->errException('ERR_API_INTERNAL: Login session expired.', 'SE0003', 'Failed to log out, please close your browser.');
      }

      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
