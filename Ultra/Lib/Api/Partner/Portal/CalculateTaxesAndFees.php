<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\Api\Traits\TaxesAndFees;
use Ultra\Taxes\TaxesAndFeesCalculator;

class CalculateTaxesAndFees extends Portal
{
  /**
   * @var TaxesAndFeesCalculator
   */
  private $taxesAndFeesCalculator;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var Session
   */
  private $session;

  use TaxesAndFees;

  /**
   * CalculateTaxesFees constructor.
   * @param TaxesAndFeesCalculator $taxesAndFeesCalculator
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param Session $session
   */
  public function __construct(TaxesAndFeesCalculator $taxesAndFeesCalculator, CustomerRepository $customerRepository, Configuration $configuration, Session $session)
  {
    $this->taxesAndFeesCalculator = $taxesAndFeesCalculator;
    $this->customerRepository = $customerRepository;
    $this->configuration = $configuration;
    $this->session = $session;
  }

  public function portal__CalculateTaxesAndFees()
  {
    list ($charge_amount, $zip, $product_type) = $this->getInputValues();

    try
    {
      // get customer from cookie Session
      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');
      }

      teldata_change_db();

      $result = $this->calculateTaxesAndFees(
        $this->customerRepository,
        $this->taxesAndFeesCalculator,
        $this->configuration,
        $this->session->customer_id,
        $zip,
        $charge_amount,
        $product_type
      );

      $this->addToOutput('sales_tax', $result->data_array['sales_tax'] + $result->data_array['mts_tax']);
      $this->addToOutput('recovery_fee', $result->data_array['recovery_fee']);

      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}