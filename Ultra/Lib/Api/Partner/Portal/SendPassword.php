<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\Common;

class SendPassword extends Portal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var Common
   */
  private $utilities;

  /**
   * SendPassword constructor.
   * @param CustomerRepository $customerRepository
   * @param Messenger $messenger
   * @param Common $utilities
   */
  public function __construct(CustomerRepository $customerRepository, Messenger $messenger, Common $utilities)
  {
    $this->customerRepository = $customerRepository;
    $this->messenger = $messenger;
    $this->utilities = $utilities;
  }

  /**
   * portal__SendPassword
   */
  public function portal__SendPassword()
  {
    list ($msisdn, $preferred_language) = $this->getInputValues();

    try {
      $customer = $this->customerRepository->getCustomerFromMsisdn($msisdn, ['u.CUSTOMER_ID'], true);
      if (!$customer) {
        return $this->errException("ERR_API_INVALID_ARGUMENTS: customer not found from input value = $msisdn", 'VV0031');
      }
        // create temporary password
      $password = $customer->createTemporaryPassword();

      // SMS temporary password
      $result = $this->messenger->enqueueImmediateSms($customer->customer_id, 'temp_password', ['temp_password' => $password]);

      if ($result->is_failure()) {
        return $this->errException('ERR_API_INTERNAL: failure sending temporary password', 'SM0002');
      }

      $passwordParam = ['login_password' => $this->utilities->encryptPasswordHS($password)];

      if (!$this->customerRepository->updateCustomerByCustomerId($customer->customer_id, $passwordParam)) {
        return $this->errException('ERR_API_INTERNAL: error updating password', 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
