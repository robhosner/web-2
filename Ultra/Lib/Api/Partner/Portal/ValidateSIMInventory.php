<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Configuration\Configuration;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Sims\Interfaces\SimRepository;

class ValidateSIMInventory extends Portal
{
  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var Configuration
   */
  private $config;

  /**
   * ValidateSIMInventory constructor.
   * @param SimRepository $simRepository
   * @param Configuration $config
   */
  public function __construct(SimRepository $simRepository, Configuration $config)
  {
    $this->simRepository = $simRepository;
    $this->config = $config;
  }

  /**
   * portal__ValidateSIMInventory
   *
   * check the ICCID validity on the Ultra DB
   *
   * @param string ICCID
   * @return object Result
   */
  public function portal__ValidateSIMInventory()
  {
    // init
    list($ICCID, $brand) = $this->getInputValues();
    $this->addToOutput('valid_ext', null);
    $this->addToOutput('valid', null);

    try {
      // get SIM data
      teldata_change_db();
      $sim = $this->simRepository->getSimInventoryAndBatchInfoByIccid($ICCID);

      // check if input brand matches ICCID brand
      if ($this->config->getShortNameFromBrandId($sim->brand_id) != $brand) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid brand for ICCID', 'IC0003');
      }

      // validate SIM
      $this->addToOutput('valid', $sim->sim_activated || $sim->reservation_time ? 'USED' : 'VALID');
      $this->addToOutput('valid_ext', $sim->sim_activated ? 'USED' : ($sim->reservation_time ? 'RESERVED' : 'VALID'));
      $this->addToOutput('expires_date', $sim->expires_date);

      if ($sim->sim_activated != 0 || $sim->reservation_time) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: SIM cannot be used', 'VV0065');
      }

      $this->succeed();
    } catch(InvalidObjectCreationException $e) {
      $this->addError('ERR_API_INVALID_ARGUMENTS: ICCID not found or has unknown SIMBATCH.', 'IC0001');
    } catch(\Exception $e) {
      dlog('' , 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }
}
