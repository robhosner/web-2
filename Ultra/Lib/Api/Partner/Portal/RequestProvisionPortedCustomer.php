<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Flex;
use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Lib\Services\FamilyAPI;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

require_once 'classes/Provisioning.php';
require_once 'classes/IntraBrand.php';

class RequestProvisionPortedCustomer extends \Ultra\Lib\Api\Partner\Portal
{
  use FlexHandler;

  private $familyAPI;

  public function __construct(FamilyAPI $familyAPI)
  {
    $this->familyAPI = $familyAPI;
  }

  /**
   * recordFraudEvent
   * records a fraud event
   *
   * @param  $customer Object [CUSTOMER_ID]
   * @return void
   */
  private function recordFraudEvent($customer)
  {
    $fraudStatus = $this->isSuccess() ? 'success' : 'error';
    $data = array('source' => json_encode($this->getInputValues()));

    fraud_event($customer, 'portal', 'RequestProvisionPortedCustomer', $fraudStatus, $data);
  }

  /**
   * updateBoltOns
   * modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
   * @param  Integer $customer_id
   * @param  Array   $bolt_on_configuration returned from \Provisioning::configureBoltons
   * @return void
   */
  private function updateBoltOns($customer_id, $bolt_on_configuration)
  {
    // $update_bolt_ons_result failures will be ignored
    $update_bolt_ons_result = \update_bolt_ons_values_to_customer_options(
      $customer_id,
      array(),
      $bolt_on_configuration,
      'PORTAL'
    );
  }

  /**
   * portal__RequestProvisionPortedCustomer
   *
   * create a customer and 'activate' their SIM; this call is ONLY for port in customers
   * [ Neutral ] => [ Port-In Requested ]
   *
   * @return Result object
   */
  public function portal__RequestProvisionPortedCustomer()
  {
    list ( $ICCID , $number_to_port , $target_plan , $zipcode , $preferred_language , $port_account_number , $port_account_password ,
      $bolt_ons, $email, $contact_phone, $invite_code ) = $this->getInputValues();
    
    $bolt_ons = empty($bolt_ons) ? array() : (is_array($bolt_ons) ? $bolt_ons : array($bolt_ons));

    $this->addToOutput('zsession',   NULL);
    $this->addToOutput('request_id', NULL);
    $this->addToOutput('customer_id', null);

    $port_account_number   = trim($port_account_number);
    $port_account_password = trim($port_account_password);

    $masteragent = self::DEFAULT_ACTIVATION_MASTERAGENT;
    $distributor = self::DEFAULT_ACTIVATION_DISTRIBUTOR;
    $dealer      = self::DEFAULT_ACTIVATION_DEALER;
    $userid      = self::DEFAULT_ACTIVATION_USER_ID;

    $changeStateResult = array();

    $customer = null;
    $context = array(); // port transition context

    try
    {
      // connect to the DB
      teldata_change_db();

      $isFlexPlan = Flex::isFlexPlan($target_plan);

      // verify that activations are enabled
      if ( ! activations_enabled())
        $this->errException('ERR_API_INTERNAL: activations disabled', 'AP0001', 'Activations are unavailable, please try again later');

      // validate ICCID
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$ICCID,'VALID');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // get SIM info and validate it
      $sim = get_htt_inventory_sim_from_iccid($ICCID);
      if ( ! $sim)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      if ( ! \Ultra\UltraConfig\isBrandAllowedByAPIPartner( $sim->BRAND_ID , $this->partner ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid', 'IC0003');

      if ( ! validate_ICCID($sim, 1))
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used', 'VV0065');

      // validate SIM brand
      \logit( "API partner = {$this->partner} ; BRAND_ID = {$sim->BRAND_ID}" );

      $redis = new \Ultra\Lib\Util\Redis;

      // verify iccid over network
      $result = \Provisioning::verifyICCIDGoodToActivate($sim->ICCID_FULL, $redis);
      if ($error = $result->get_first_error())
      {
        list ($error, $code) = explode('|', $error);
        $this->errException($error, $code);
      }

      // check invite code, if given
      if ($isFlexPlan && $invite_code) {
        $this->validateInviteCode($invite_code);
      }

      // configure Recurring Bolt Ons
      $configureBoltonsResult = \Provisioning::configureBoltOns($bolt_ons);
      if ($error = $configureBoltonsResult->get_first_error())
      {
        list ($error, $code) = explode('|', $error);
        $this->errException($error, $code);
      }

      if ( ! $dealer_info = \Ultra\Lib\DB\Celluphone\getDealerInfo(NULL, $dealer))
        $this->errException('ERR_API_INTERNAL: No celluphone dealer info found', 'ND0003');

      // save port account credentials in redis for 2 weeks
      save_port_account($number_to_port, $port_account_number, $port_account_password);

      $transition_name = '';

      $customer = get_customer_from_msisdn($number_to_port, 'u.CUSTOMER_ID, u.BRAND_ID, plan_state, u.CURRENT_ICCID, a.COS_ID, a.ACCOUNT');

      if (\IntraBrand::isIntraBrandPortByMSISDNAndSIM($number_to_port, $sim)) {
        // regular intraport
        $doIntraport = true;
      } else if ($customer && (Flex::isFlexPlan(get_plan_from_cos_id($customer->COS_ID)) && !Flex::isFlexPlan($target_plan))) {
        // flex -> non flex plan
        $doIntraport = true;
      } else if ($customer && (!Flex::isFlexPlan(get_plan_from_cos_id($customer->COS_ID)) && Flex::isFlexPlan($target_plan))) {
        // non flex -> flex plan
        $doIntraport = true;
      } else {
        // no intraport
        $doIntraport = false;
      }

      // check for intra-brand port or flex plan or if already on flex
      if ($doIntraport) {
        \logit("MSISDN: $number_to_port is intra-brand port");

        if ( ! $customer)
          $this->errException('ERR_API_INTERNAL: Could not find customer by MSISDN for intra-port', 'VV0031');
        $context['old_iccid'] = luhnenize($customer->CURRENT_ICCID);
        $context['old_brand_id'] = $customer->BRAND_ID;

        if ( ! in_array($customer->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED)))
          $this->errException('ERR_API_INTERNAL: port-In denied due to customer state', 'IN0001');

        if ($customer->ACCOUNT != $port_account_number) {
          $this->errException('ERR_API_INTERNAL: port-In denied due to invalid account number', 'PO0009');
        }

        // intra port operations, clone and soft cancel customer
        $result = \Provisioning::intraBrandPortOperations($customer->CUSTOMER_ID, $sim);
        if ($error = $result->get_first_error())
        {
          list ($error, $code) = explode('|', $error);
          $this->errException($error, $code);
        }

        $customer = $result->get_data_key('customer');

        // initiate state transition Neutral -> Port-In Requested but do not resolve it now
        $transition_name = 'Provision Intra Port ' . get_plan_name_from_short_name($target_plan);

        if (!empty($contact_phone)) {
          if (!(new CustomerRepository())->addCustomerContactPhone($customer->CUSTOMER_ID, $contact_phone)) {
            $this->errException('ERR_API_INTERNAL: Cannot add customer contact_phone', 'DB0001');
          }
        }
      }
      else
      {
        // verify number over network
        $result = \Provisioning::verifyPortInEligibility($number_to_port);
        if ($error = $result->get_first_error())
        {
          list ($error, $code) = explode('|', $error);
          $this->errException($error, $code);
        }

        // verify number not in system
        $result = \Provisioning::verifyNumberNotInSystem($number_to_port);
        if (count($errors = $result->get_errors()))
        {
          list ($error, $code) = explode('|', $errors[0]);
          $this->errException($error, $code);
        }

        $promised_amount = get_plan_cost_by_cos_id(get_cos_id_from_plan($target_plan)) + $configureBoltonsResult->get_data_key('bolt_ons_cost');

        // create ULTRA customer in DB
        $params = array(
          'masteragent'           => $sim->INVENTORY_MASTERAGENT ? $sim->INVENTORY_MASTERAGENT : $masteragent,
          'distributor'           => $sim->INVENTORY_DISTRIBUTOR ? $sim->INVENTORY_DISTRIBUTOR : $distributor,
          'dealer'                => $dealer,
          'userid'                => $userid,
          'preferred_language'    => $preferred_language,
          'cos_id'                => get_cos_id_from_plan('STANDBY'),
          'activation_cos_id'     => get_cos_id_from_plan( $target_plan ),
          'postal_code'           => $zipcode,
          'country'               => DEFAULT_ULTRA_COUNTRY,
          'plan_state'            => STATE_NEUTRAL,
          'plan_started'          => 'NULL',
          'plan_expires'          => 'NULL',
          'customer_source'       => 'PORTAL',
          'current_iccid'         => substr($ICCID, 0, -1),
          'current_iccid_full'    => $ICCID,
          'current_mobile_number' => $number_to_port,
          'promised_amount'       => $promised_amount
        );

        if (!empty($email)) {
          $params['e_mail'] = $email;
        }

        if (!empty($contact_phone)) {
          $params['contact_phone'] = $contact_phone;
        }

        $result   = create_ultra_customer_db_transaction($params);
        $customer = $result['customer'];

        if ( ! $customer )
          $this->errException('ERR_API_INTERNAL: failed to create customer', 'DB0001');

        customer_reset_first_last_name($customer->CUSTOMER_ID);

        // initiate state transition Neutral -> Port-In Requested but do not resolve it now
        $transition_name = 'Request Port ' . get_plan_name_from_short_name($target_plan);
      }

      // handle Flex plan
      if ($isFlexPlan && !empty($invite_code)) {
        $this->joinFamily($customer->CUSTOMER_ID, $invite_code);
      }

      $this->updateBoltOns($customer->CUSTOMER_ID, $configureBoltonsResult->get_data_key('bolt_on_configuration'));

      // save activation attribution info in redis before attempting state transition
      set_redis_provisioning_values(
        $customer->CUSTOMER_ID,
        $dealer_info->masterId,
        $dealer,
        $dealer_info->distributorId,
        $dealer,
        $userid,
        $redis
      );

      // prepare transition
      $context['customer_id']              = $customer->CUSTOMER_ID;
      $context['port_in_msisdn']           = $number_to_port;
      $context['port_in_iccid']            = $ICCID;
      $context['port_in_account_number']   = $port_account_number;
      $context['port_in_account_password'] = $port_account_password;
      $context['port_in_zipcode']          = $zipcode;

      $changeStateResult = change_state($context, FALSE, $transition_name, 'take transition', FALSE, 1);
      if ( $changeStateResult['success'] != 1 )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException( 'ERR_API_INTERNAL: state transition error (2)', 'SM0001' );
      }

      reserve_iccid($ICCID);

      // record MSISDN into ULTRA.HTT_ACTIVATION_HISTORY
      \Provisioning::updateActivationHistoryForMsisdn($customer->CUSTOMER_ID, $number_to_port);

      // add transition UUID to result
      $transition_values = array_values($changeStateResult['transitions']);
      $request_id = end($transition_values);
      $this->addToOutput('request_id', $request_id);

      // memorize $request_id in Redis by customer_id for portal__SearchActivationHistoryDealer
      $redis_port = new \Ultra\Lib\Util\Redis\Port();
      $redis_port->setRequestId($customer->CUSTOMER_ID, $request_id);

      // handle Flex plan
      if ($isFlexPlan && empty($invite_code)) {
        $this->createFamily($customer->CUSTOMER_ID);
      }

      // create zsession
      $_REQUEST['zsession'] = session_save($customer, NULL, $redis);
      $verified = verify_session($customer->CUSTOMER);
      if (! $verified)
        $this->errException('ERR_API_INTERNAL: failed to create user session', 'SE0001');
      $this->addToOutput('zsession', $verified[2]);
      setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER['SERVER_NAME']);

      $session = new \Session();

      if (!$session->confirm($customer->CUSTOMER_ID))
      {
        throw new \Exception('ERR_API_INTERNAL: A problem occurred when trying to create a session.');
      }

      $this->addToOutput('customer_id', $customer->CUSTOMER_ID);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      \logError($e->getMessage());
    }

    $customer = (isset($customer) && is_object($customer)) ? $customer : null;
    $this->recordFraudEvent($customer);

    return $this->result;
  }
}

