<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class ExtendSession extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__ExtendSession
   *
   * Extend the lifespan of current zsession by 30 minutes.
   *
   * @param string zsession
   * @return Result object
   */
  public function portal__ExtendSession()
  {
    list ($zsession) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $data_zsession = get_customer_from_zsession($zsession);

      dlog('',"data_zsession = %s",$data_zsession);

      if ( count($data_zsession['errors']) )
      {
        $this->errException($data_zsession['errors'][0], 'SE0002');
      }
      else
      {
        session_save($data_zsession['customer'], $zsession);

        $verified = verify_session( $data_zsession['customer']->CUSTOMER );

        dlog('',"verified = %s",$verified);

        setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER['SERVER_NAME']);
        setcookielive('zsessionC', $data_zsession['customer']->CUSTOMER, $verified[1], '/', $_SERVER['SERVER_NAME']);
      }

      $this->succeed();
    }
    catch (\Excetion $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>