<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class CheckCallCreditOptions extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__CheckCallCreditOptions
   *
   * Returns possible purchase options for buying Additional Call Anywhere Credit.
   * ``bonus_percent`` is fixed at 25%
   * ``value``         is computed from ``cost`` and ``bonus_percent``
   *
   * @return Result object
   */
  public function portal__CheckCallCreditOptions ()
  {
    $error_code = '';

    $this->addToOutput('call_credit_options','');

    try
    {
      // get subscriber from zsession cookie
      teldata_change_db();

      if (empty($_COOKIE['zsession']))
        $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');

      $zsession = $_COOKIE['zsession'];

      if ( ! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $this->setOutputLanguage($customer->preferred_language);

      $call_anywhere_additional_credit_options = \Ultra\UltraConfig\call_anywhere_additional_credit_options();

      if ( ! $call_anywhere_additional_credit_options || ! is_array( $call_anywhere_additional_credit_options ) )
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );

      $this->addToOutput( 'call_credit_options' , $call_anywhere_additional_credit_options );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

?>