<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Utilities\Common;

/**
 * Class GetE911SubscriberAddress
 * @package Ultra\Lib\Api\Partner\Portal
 */
class GetE911SubscriberAddress extends Portal
{
  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * GetE911SubscriberAddress constructor.
   * @param Common $utilities
   * @param Session $session
   * @param CustomerRepository $customerRepository
   */
  public function __construct(Common $utilities, Session $session, CustomerRepository $customerRepository)
  {
    $this->utilities = $utilities;
    $this->session = $session;
    $this->customerRepository = $customerRepository;
  }

/**
   * portal__GetE911SubscriberAddress
   *
   * Retrieve customer E911 Subscriber Address
   *
   * @param  Integer $customer_id
   * @return Result object
   */
  public function portal__GetE911SubscriberAddress()
  {
    try {
      $this->utilities->teldataChangeDb();

      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }

      $this->customer = $this->customerRepository->getCustomerById($this->session->customer_id, [], true);

      $this->addToOutput('address', $this->customer->getAccAddressE911());

      // success
      $this->succeed();
    } catch (MissingRequiredParametersException $e){
      $this->addError($e->getMessage(), 'VV0031');
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
