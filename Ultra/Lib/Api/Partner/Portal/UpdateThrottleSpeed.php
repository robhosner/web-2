<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Accounts\Account;
use Ultra\Accounts\Interfaces\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Mvne\Adapter;

class UpdateThrottleSpeed extends Portal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * @var Adapter
   */
  private $adapter;

  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var Account
   */
  private $account;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * UpdateThrottleSpeed constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param Adapter $adapter
   * @param AccountsRepository $accountsRepository
   * @param Configuration $configuration
   */
  public function __construct(
    Session $session,
    CustomerRepository $customerRepository,
    Adapter $adapter,
    AccountsRepository $accountsRepository,
    Configuration $configuration
  )
  {
    $this->customerRepository = $customerRepository;
    $this->session = $session;
    $this->adapter = $adapter;
    $this->accountsRepository = $accountsRepository;
    $this->configuration = $configuration;
  }

  public function portal__UpdateThrottleSpeed()
  {
    list($targetSpeed) = $this->getInputValues();

    try
    {
      // verify session
      if (!$customer_id = $this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }

      if (!$this->customer = $this->customerRepository->getCustomerById($customer_id, ['BRAND_ID'], true)) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }

      if (!$this->account = $this->accountsRepository->getAccountFromCustomerId($customer_id, ['COS_ID'])) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }

      try {
        if ($this->customer->canUpdateToThrottleSpeed($targetSpeed)) {
          $selectionMethod = [
            1 => 'LTE_SELECT.ULTRA_MOBILE_CUSTOMER_PORTAL',
            2 => 'LTE_SELECT.UNIVISION_CUSTOMER_PORTAL',
          ];

          $result = $this->adapter->mvneMakeitsoUpdateThrottleSpeed(
            $customer_id,
            $this->configuration->getPlanFromCosId($this->account->cos_id),
            $targetSpeed,
            $selectionMethod[$this->customer->brand_id]
          );

          if (!$result['success']) {
            return $this->errException('The mvneMakeitsoUpdateThrottleSpeed result failed.', 'IN0002', 'The mvneMakeitsoUpdateThrottleSpeed result failed.');
          }
        } else {
          return $this->errException('unable to update to throttle speed ' . $targetSpeed, 'IN0002', 'unable to update to throttle speed ' . $targetSpeed);
        }
      } catch (\Exception $e) {
        return $this->errException('ERR_API_INTERNAL: ' . $e->getMessage(), 'IN0002', $e->getMessage());
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
