<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Payments\CreditCardCharger;

class ChargeCC extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var CreditCardCharger
   */
  private $creditCardCharger;

  public function __construct(CreditCardCharger $creditCardCharger, Session $session)
  {
    $this->creditCardCharger = $creditCardCharger;
    $this->session = $session;
  }

  /**
   * portal__ChargeCreditCard
   * charge customers credit card on file previously saved with portal__VerifyAndSaveCreditCard
   * @return Result object
   */
  public function portal__ChargeCC()
  {
    // init
    list ($charge_amount, $destination, $product_type) = $this->getInputValues();

    try {
      if (!$customer_id = $this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');
      }

      teldata_change_db();

      $this->creditCardCharger->chargeCreditCard($customer_id, $charge_amount, $destination, $product_type, __FUNCTION__, $this->getRequestId());
      $this->succeed();
    } catch (CustomErrorCodeException $e) {
      dlog('' , $e->getMessage());
      $this->addError($e->getMessage(), $e->code());
    } catch(\Exception $e) {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }
}
