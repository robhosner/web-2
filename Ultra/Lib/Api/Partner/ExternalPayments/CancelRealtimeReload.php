<?php

namespace Ultra\Lib\Api\Partner\ExternalPayments;

require_once 'Ultra/Lib/Api/Partner/ExternalPayments.php';

class CancelRealtimeReload extends \Ultra\Lib\Api\Partner\ExternalPayments
{

  /**
   * externalpayments__CancelRealtimeReload
   * allows external payment processor to void a balance application; must be submitted within 120 seconds of the original request
   * @return object Result
   */
  public function externalpayments__CancelRealtimeReload()
  {
    // initialize
    list($request_epoch, $phone_number, $cancel_type, $load_amount, $provider_trans_id, $provider_name) = $this->getInputValues();
    foreach (array('phone_number', 'customer_balance', 'ultra_payment_trans_id', 'provider_trans_id') as $value)
      $this->addToOutput($value, NULL);
    $fraud = NULL;

    try
    {
      // parameter checking and initialization
      $this->checkTimeSync($request_epoch);
      teldata_change_db();
      $subscriber = $this->verifySubscriber($phone_number);

      // get values of the prevously posted transaction from redis
      if ( ! $redis = new \Ultra\Lib\Util\Redis())
        $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002');
      $key = "externalpayments/$provider_trans_id";
      $fraud = array(
        'provider'      => $provider_name,
        'store_id'      => $redis->get("$key/store_id"),
        'clerk_id'      => $redis->get("$key/clerk_id"),
        'terminal_id'   => $redis->get("$key/terminal_id"),
        'load_amount'   => $load_amount,
        'subproduct_id' => NULL);

      $transaction = $this->cancelTransaction($subscriber, $provider_trans_id, $load_amount, $cancel_type);

      // fill in return values
      $this->addToOutput('phone_number', $phone_number);
      $this->addToOutput('customer_balance', ($subscriber->stored_value + $subscriber->BALANCE) * 100);
      $this->addToOutput('ultra_payment_trans_id', $transaction->UUID);
      $this->addToOutput('provider_trans_id', $provider_trans_id);

      fraud_event($subscriber, 'externalpayments', 'CancelRealtimeReload', 'success', $fraud);
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
      if ($fraud)
        fraud_event($subscriber, 'externalpayments', 'CancelRealtimeReload', 'error', $fraud);
    }

    return $this->result;
  }

}

?>
