<?php

require_once 'classes/PHPUnitBase.php';


/**
 * tests for 'externalpayments' partner APIs v2
 */
class ExternalPaymentsTest extends PHPUnitBase
{
  /**
   * test__externalpayments__SubmitRealtimeReload
   */
  public function test__externalpayments__SubmitRealtimeReload()
  {
    // API setup
    list($test, $partner, $command) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$command}",
      'bath'      => 'rest',
      'version'   => 2,
      'partner'   => $partner));

    // API-385: Active D39 reload
    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '6463592475',
      'store_zipcode'     => '99999',
      'dealer_code'       => 'B20150',
      'clerk_id'          => 100,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'UV30',
      'sku'               => '6600',
      'load_amount'       => '3000',
      'provider_trans_id' => rand(11111111, 99999999),
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);

    // API-385: Active D39 reload
    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '7144176594',
      'store_zipcode'     => '99999',
      'dealer_code'       => 'B20150',
      'clerk_id'          => 100,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'D39',
      'sku'               => '1564',
      'load_amount'       => '3900',
      'provider_trans_id' => rand(11111111, 99999999),
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);

    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '6573215227',
      'store_zipcode'     => '99999',
      'dealer_code'       => 'B20150',
      'clerk_id'          => 100,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'S29',
      'sku'               => '1559',
      'load_amount'       => '2900',
      'provider_trans_id' => rand(11111111, 99999999),
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);

    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '6573215225',
      'store_zipcode'     => '99999',
      'dealer_code'       => 'B20150',
      'clerk_id'          => 100,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'S44',
      'sku'               => '4759',
      'load_amount'       => '4400',
      'provider_trans_id' => rand(11111111, 99999999),
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
  }
}
