<?php

namespace Ultra\Lib\Api\Partner\ExternalPayments;

require_once 'Ultra/Lib/Api/Partner/ExternalPayments.php';

class CheckMobileBalance extends \Ultra\Lib\Api\Partner\ExternalPayments
{

  /**
   * externalpayments__CheckMobileBalance
   * allows external payment processor to to check subscriber's balance and status
   * @return object Result
   */
  public function externalpayments__CheckMobileBalance()
  {
    // initialize
    list($request_epoch, $phone_number, $dealer_code, $store_zipcode, $store_zipcode_extra4, $clerk_id, $terminal_id) = $this->getInputValues();
    foreach (array('phone_number', 'customer_balance', 'plan_state', 'plan_name', 'created_date', 'plan_cost', 'full_plan_cost', 'iccid', 'dealer_code') as $value)
      $this->addToOutput($value, NULL);

    try
    {
      // parameter checking and initialization
      $this->checkTimeSync($request_epoch);
      teldata_change_db();
      $dealer = $this->verifyDealer($dealer_code);
      $subscriber = $this->verifySubscriber($phone_number);

      // subscriber info
      $this->addToOutput('phone_number', $phone_number);
      $this->addToOutput('customer_balance', ($subscriber->stored_value + $subscriber->BALANCE) * 100);
      $this->addToOutput('brand', \Ultra\UltraConfig\getShortNameFromBrandId($subscriber->BRAND_ID));
      $this->addToOutput('plan_state', strtoupper($subscriber->plan_state));
      $this->addToOutput('plan_name', get_plan_name_from_short_name(get_plan_from_cos_id($subscriber->COS_ID)));
      $this->addToOutput('created_date', date_to_epoch($subscriber->CREATION_DATE_TIME));

      // plan costs
      $cos_id = $subscriber->MONTHLY_RENEWAL_TARGET ? get_cos_id_from_plan($subscriber->MONTHLY_RENEWAL_TARGET) : $subscriber->COS_ID;
      $costs = get_plan_costs_by_customer_id($subscriber->customer_id, $cos_id);
      $this->addToOutput('plan_cost', $costs['plan'] * 100);
      $this->addToOutput('full_plan_cost', ($costs['plan'] + $costs['bolt_ons']) * 100);

      // current ICCID
      $this->addToOutput('iccid', $subscriber->CURRENT_ICCID_FULL ? $subscriber->CURRENT_ICCID_FULL : luhnenize($subscriber->current_iccid));

      // MVNO-2595: also return activating dealer
      if ($dealer = get_dealer_from_customer_id($subscriber->customer_id))
        $this->addToOutput('dealer_code', $dealer->Dealercd);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}

?>
