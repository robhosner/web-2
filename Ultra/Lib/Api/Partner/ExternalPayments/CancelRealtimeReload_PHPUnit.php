<?php

require_once 'classes/PHPUnitBase.php';


/**
 * tests for 'externalpayments' partner APIs v2
 */
class ExternalPaymentsTest extends PHPUnitBase
{

  /**
   * test__externalpayments__CancelRealtimeReload
   */
  public function test__externalpayments__CancelRealtimeReload()
  {
    // API setup
    list($test, $partner, $api) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'version'   => 2,
      'partner'   => $partner,
      'debug'     => TRUE));

    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    # print_r($result);
    $this->assertTrue( ! $result->success);

    // test invalid epoch
    $params = array(
      'request_epoch'     => time() - 181,
      'phone_number'      => '5555555555',
      'load_amount'       => 1900,
      'provider_trans_id' => 'abc123',
      'cancel_type'       => 'VOID',
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid UNIX Epoch value", $result->errors);

    // test invalid MSISDN
    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '5555555555',
      'load_amount'       => 1900,
      'provider_trans_id' => 'abc123',
      'cancel_type'       => 'VOID',
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: The customer does not exist", $result->errors);
    $this->assertTrue( ! $result->success);

    // test invalid transaction
    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '5189866600',
      'load_amount'       => 1900,
      'provider_trans_id' => 'abc123',
      'cancel_type'       => 'VOID',
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Transaction does not exist", $result->errors);
    $this->assertTrue( ! $result->success);

    // test expired transaction
    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '5189866528',
      'load_amount'       => 1900,
      'provider_trans_id' => '55518712',
      'cancel_type'       => 'VOID',
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INTERNAL: Cannot void transaction", $result->errors);
    $this->assertTrue( ! $result->success);

    // successfull test
    $params = array(
      'request_epoch'     => time(),
      'phone_number'      => '9499034028',
      'load_amount'       => 1900,
      'provider_trans_id' => '1000',
      'cancel_type'       => 'VOID',
      'provider_name'     => 'TCETRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
  }
}

