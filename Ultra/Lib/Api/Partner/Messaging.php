<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';
require_once 'Ultra/Lib/MVNE/Adapter.php';

/**
 * Messaging Partner class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Messaging extends \Ultra\Lib\Api\PartnerBase
{
  /**
   * messaging__SendSMS
   *
   * Errors:
   * MP0003 : MSISDN not provided
   * MP0001 : SMS text not provided
   * MW0001 : malformed MSISDN
   * VV0007 : MSISDN too short
   * VV0008 : MSISDN too long
   *
   * send SMS text to given MSISDN
   * @param string sms_text: message text
   * @param string MSISDN: customer mobile number
   * @return object Result
   * @author VYT, 14-02-04
   */
  public function messaging__SendSMS ()
  {
    // init
    list ($access_token, $sms_text, $msisdn) = $this->getInputValues();
    $msisdn = normalize_msisdn($msisdn);

    try
    {
      // verify 3ci access token
      if ( empty($access_token)
        || ( $access_token != \Ultra\UltraConfig\accessToken3ci() && $access_token != \Ultra\UltraConfig\accessTokenExacaster() )
      )
      {
        dlog('', 'token ' . $access_token . ' vs ' . \Ultra\UltraConfig\accessToken3ci() . ' or vs ' . \Ultra\UltraConfig\accessTokenExacaster() );
        $this->errException( "ERR_API_INVALID_ARGUMENTS: invalid access token" , 'SE0006' );
      }

      // verify customer
      // VYT @ 2014-02-26: taken out on Rizwan's request
/*      teldata_change_db();
      $customer = get_customer_from_msisdn($msisdn);
      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      $preferred_language = $customer->preferred_language;
      if ( ! $preferred_language )
        $preferred_language = 'EN';
*/

      $sms_text = preg_replace( '/[\r\n]+/' , " " , $sms_text );

      $result = mvneBypassListAsynchronousSendSMS( $sms_text , $msisdn , getNewActionUUID('messaging ' . time() ) );

      // check for high level errors
      if ($result->is_failure() || $result->has_errors())
      {
        $errors = $result->get_errors();

        if ( count($errors) )
          $this->errException( $errors[0] , 'MW0001' );
        else
          $this->errException( "ERR_API_INTERNAL: MW call failed (1)", 'MW0001' );
      }

      // check for MW errors
      if ( isset($result->data_array['errors']) && count($result->data_array['errors']) )
        $this->errException( $result->data_array['errors'][0] , 'MW0001' );

      // done
      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

