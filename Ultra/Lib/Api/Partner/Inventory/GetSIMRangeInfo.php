<?php

namespace Ultra\Lib\Api\Partner\Inventory;

require_once 'Ultra/Lib/Api/Partner/Inventory.php';

class GetSIMRangeInfo extends \Ultra\Lib\Api\Partner\Inventory
{
  /**
   * inventory__GetSIMRangeInfo
   *
   * @param string startICCID
   * @param string endICCID
   * @return object Result
   */
  public function inventory__GetSIMRangeInfo()
  {
    list ($startICCID, $endICCID) = $this->getInputValues();

    $data = array();

    try
    {
      teldata_change_db();

      $nolock = TRUE;
      $result = get_htt_inventory_sims_by_range($startICCID, $endICCID, $nolock);

      if (count($result) > 1000)
        $this->errException('ERR_API_INTERNAL: SIM range cannot return more than 1000 records.', 'VV0236');

      foreach ($result as $row)
      {
        $data[] = array(
          'iccid_full' => $row->ICCID_FULL,
          'sim_hot' => $row->SIM_HOT,
          'product_type' => $row->PRODUCT_TYPE,
          'sim_activated' => $row->SIM_ACTIVATED,
          'customer_id' => $row->CUSTOMER_ID
        );
      }

      $this->addToOutput('sim_data', $data);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
