<?php

namespace Ultra\Lib\Api\Partner\Inventory;

require_once 'Ultra/Lib/Api/Partner/Inventory.php';

class AssignPINRangeToHot extends \Ultra\Lib\Api\Partner\Inventory
{
  /**
   * inventory__AssignPINRangeToHot
   *
   * Set PIN cards to HOT so that customers can use them.
   *  'AT_MASTER' = Hot
   *  'AT_FOUNDRY = Cold
   *
   * @param string startPinSerialNumber
   * @param string endPinSerialNumber
   * @param integer masteragent
   * @param string user User performing the operation
   * @return object Result
   */
  public function inventory__AssignPINRangeToHot()
  {
    list ($startPinSerialNumber, $endPinSerialNumber, $masteragent, $user) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $result = htt_inventory_pin_set_range_hot($masteragent, $startPinSerialNumber, $endPinSerialNumber, $user);
      if (!$result)
        $this->errException('ERR_API_INTERNAL: ' . mssql_get_last_message(), 'DB0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}