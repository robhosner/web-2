<?php

namespace Ultra\Lib\Api\Partner\Exacaster;

require_once 'Ultra/Lib/Api/Partner/Exacaster.php';

class RedeemPromoCampaign extends \Ultra\Lib\Api\Partner\Exacaster
{
  /**
   * exacaster__RedeemPromoCampaign
   *
   * Triggers actions associated to a Promo Campaign Shortcode
   * WARNING: care must be taken to return end-user friendly error messages which are displayed to subscribers
   * @see https://issues.hometowntelecom.com:8443/browse/API-298
   * @return Result object
   */
  public function exacaster__RedeemPromoCampaign()
  {
    // initialize
    list ($access_token, $customer_id, $promo_keyword, $promo_action, $promo_value, $promo_unit) = $this->getInputValues();

    $promo_action = trim(strtoupper($promo_action), ". \t\n\r\0\x0B"); // aka keyword

    try
    {
      if ($access_token != \Ultra\UltraConfig\accessTokenExacaster())
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter a valid security token', 'MP0002');

      teldata_change_db();

      // get subscriber object
      if ( ! $customer = get_customer_from_customer_id($customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031', 'This customer_id does not belong to a subscriber.');

      // verify subscriber status
      if ( ! in_array($customer->plan_state, array(STATE_ACTIVE, STATE_PROVISIONED, STATE_SUSPENDED)))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001', 'Subscriber status is invalid.');

      $result = \Ultra\Lib\PromoCampaign\triggerPromoAction($promo_keyword , $promo_action, $promo_value, $customer);

      if ( $result->is_failure() )
        $this->errException(
          $result->data_array['error'],
          $result->data_array['error_code'],
          $result->data_array['user_error']
        );

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', $e->getMessage());
    }

    $this->addApiErrorNode();
    return $this->result;
  }
}
