<?php

namespace Ultra\Lib\Api\Partner\Exacaster;

require_once 'Ultra/Lib/Api/Partner/Exacaster.php';

class GetCustomerInfo extends \Ultra\Lib\Api\Partner\Exacaster
{
   /**
   * exacaster__GetCustomerInfo
   *
   * Returns customer information
   *
   * @return Result object
   */
  public function exacaster__GetCustomerInfo()
  {
    list($access_token, $customer_id) = $this->getInputValues();

    $results = [
      'email'                  => NULL,
      'msisdn'                 => NULL,
      'iccid'                  => NULL,
      'plan'                   => NULL,
      'plan_state'             => NULL,
      'paid_through'           => NULL,
      'duration'               => NULL,
      'monthly_renewal_target' => NULL,
      'balance'                => NULL,
      'stored_value'           => NULL,
      'packaged_balance'       => NULL,
      'auto_recharge'          => NULL,
      'preferred_language'     => NULL,
      'has_billing_info_saved' => NULL,
      'marketing_settings'     => array(),
      'bolt_ons'               => array(),
      'voice_minutes'          => NULL,
      '4g_lte_remaining'       => NULL,
      '4g_lte_usage'           => NULL,
      '4g_lte_last_applied'    => 0,
      '4g_lte_bolton_percent_used' => 0,
      'brand'                  => NULL  
    ];

    try
    {
      if ($access_token != \Ultra\UltraConfig\accessTokenExacaster())
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter a valid security token', 'MP0002');

      // DB stores dates in UTC
      if (! date_default_timezone_set('UTC'))
        $this->errException('ERR_API_INTERNAL: failed to set UTC time zone' , 'AP0001');

      teldata_change_db();

      $overlay  = get_ultra_customer_from_customer_id($customer_id, array(
        'BRAND_ID',
        'CURRENT_MOBILE_NUMBER',
        'CURRENT_ICCID_FULL',
        'CURRENT_ICCID',
        'MONTHLY_CC_RENEWAL',
        'MONTHLY_RENEWAL_TARGET',
        'PLAN_STATE',
        'PREFERRED_LANGUAGE',
        'STORED_VALUE'
      ));
      $account  = get_account_from_customer_id($customer_id, array(
        'BALANCE',
        'COS_ID',
        'CREATION_DATE_TIME',
        'PACKAGED_BALANCE1',
      ));
      $customer = customers_get_customer_by_customer_id($customer_id, array(
        'FIRST_NAME',
        'LAST_NAME',
        'E_MAIL',
      ));

      \logit('OVERLAY : '  . json_encode($overlay));
      \logit('ACCOUNT : '  . json_encode($account));
      \logit('CUSTOMER : ' . json_encode($customer));

      if ( !($overlay  && is_object($overlay))
        || !($account  && is_object($account))
        || !($customer && is_object($customer)) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      else
        $customer = (object)array_merge((array)$customer, (array)$overlay, (array)$account);

      $results['email']                  = $customer->E_MAIL;
      $results['msisdn']                 = $customer->CURRENT_MOBILE_NUMBER;
      $results['iccid']                  = empty($customer->CURRENT_ICCID_FULL) ? $customer->current_iccid : $customer->CURRENT_ICCID_FULL;
      $results['plan']                   = cos_id_plan_description($customer->COS_ID);
      $results['plan_state']             = $customer->PLAN_STATE;
      $results['monthly_renewal_target'] = $customer->MONTHLY_RENEWAL_TARGET;
      $results['balance']                = $customer->BALANCE;
      $results['stored_value']           = $customer->STORED_VALUE;
      $results['packaged_balance']       = $customer->PACKAGED_BALANCE1;
      $results['auto_recharge']          = $customer->MONTHLY_CC_RENEWAL ? TRUE : FALSE;
      $results['preferred_language']     = $customer->PREFERRED_LANGUAGE;
      $results['bolt_ons']               = get_bolt_ons_info_from_customer_options( $customer_id );
      $results['brand']                  = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);

      // MRP-98
      if ($multi_month_options = multi_month_info($customer_id))
      {
        $results['paid_through'] = strtotime("+{$multi_month_options['months_left']} month", $customer->plan_expires_epoch);
        $results['duration']     = $multi_month_options['months_total'];
      }

      // CCCP-17: get credit card info
      $cc_info = get_cc_info_from_customer_id($customer_id);
      $results['has_billing_info_saved'] = empty($cc_info->LAST_FOUR) || empty($cc_info->EXPIRES_DATE) ? FALSE : TRUE;

      // get marketing preferences
      $preferences = get_marketing_settings(
        array(
          'customer_id' => $customer_id
        )
      );

      if (count($preferences['errors'])) // non-fatal
        $this->addErrors($preferences['errors']);
      else
        $results['marketing_settings'] = $preferences['marketing_settings'];

      $voice_option = get_voice_preference($customer_id);
      $results['marketing_settings']['marketing_voice_option'] = ($voice_option) ? 0 : 1;

      // get data usage

      // get '4g_lte_last_applied' from HTT_DATA_EVENT_LOG
      $latest_data_event_log = get_latest_data_event_log( $customer_id );

      if ( $latest_data_event_log )
        $results['4g_lte_last_applied'] = $latest_data_event_log->event_date_epoch;

      // invoke mwCheckBalance to check 4G LTE data usage for this customer
      if ( $customer->CURRENT_MOBILE_NUMBER )
      {
        $redis = new \Ultra\Lib\Util\Redis();

        list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown , $mvneError ) = mvneGet4gLTE($this->getRequestId(), $customer->CURRENT_MOBILE_NUMBER, $customer_id, $redis);

        $results['4g_lte_remaining'] = $remaining;
        $results['4g_lte_usage']     = $usage;
      }

      if ( ! empty( $mintAddOnUsage )
        && ! empty( $mintAddOnUsage + $mintAddOnRemaining )
      )
      {
        $results['4g_lte_bolton_percent_used'] = ceil( ( $mintAddOnUsage * 100 ) / ( $mintAddOnUsage + $mintAddOnRemaining ) );
      }

      // MVNO-2503: format regardless of value
      $results['4g_lte_remaining'] = beautify_4g_lte_string( $results['4g_lte_remaining'], FALSE );
      $results['4g_lte_usage']     = beautify_4g_lte_string( $results['4g_lte_usage'], TRUE );

      foreach($results as $key => $value)
        $this->addToOutput($key, $value);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

