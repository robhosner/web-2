<?php

namespace Ultra\Lib\Api\Partner;

if (!getenv("UNIT_TESTING"))
{
  require_once 'Ultra/Lib/Api/PartnerBase.php';
  include_once 'db/customers.php';
  include_once 'db/htt_billing_history.php';
  include_once 'db/htt_data_event_log.php';
  include_once 'db/htt_transition_log.php';
  include_once 'db/ultra_cc_holders.php';
  include_once 'db/ultra_customer_options.php';
  require_once 'classes/Session.php';
  require_once 'Ultra/Lib/DB/Celluphone/functions.php';
}

/**
 * Portal partner class
 *
 * @author VYT 2014-05
 * @project Ultra Self Activations
 */
class Portal extends \Ultra\Lib\Api\PartnerBase
{
  const REPLACE_SIM_COST = 995; // cents
  const DEFAULT_ACTIVATION_DEALER = 34; // MVNO-2694: redefine existing parent constant
  const MAX_FAILED_LOGINS = 6; // max number of failed logins from the same IP address per hour
  const MAX_FAILED_CHECKS = 30; // max number of failed portal__CheckSession from the same IP address per hour; hard to crack since cookie is long
}

