<?php

namespace Ultra\Lib\Api\Partner;

require_once 'classes/ProjectW.php';
require_once 'classes/ProjectW/Parser.php';
require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * ProjectW partner class
 */
class ProjectW extends \Ultra\Lib\Api\PartnerBase
{
  const PROJECTW_IMPORT_FILE_PATH = '/var/tmp/Ultra/ProjectW/';
}
