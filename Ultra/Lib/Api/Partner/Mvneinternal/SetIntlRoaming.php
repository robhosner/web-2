<?php

namespace Ultra\Lib\Api\Partner\Mvneinternal;

require_once 'Ultra/Lib/Api/Partner/Mvneinternal.php';

class SetIntlRoaming extends \Ultra\Lib\Api\Partner\Mvneinternal
{

  /**
   * mvneinternal__SetIntlRoaming
   *
   * @param  $customer_id
   * @param  $amount
   * @return Result object
   */
  public function mvneinternal__SetIntlRoaming()
  {
    // initialization
    list ($customer_id, $a_voicesmswallet_ir) = $this->getInputValues();
    teldata_change_db();

    try
    {
      // validate subscriber
      if ( ! $customer = get_customer_from_customer_id($customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter the customer unique identifier', 'MP0020', 'Invalid subscriber ID');

      if ( ! $customer->current_mobile_number)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter a valid phone number', 'MP0003', 'Subscriber has no phone number');
      if ( ! $customer->CURRENT_ICCID_FULL)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter a valid ICCID', 'MP0004', 'Subscriber has no ICCID');
      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001', "Subsriber is {$customer->plan_state}");

      // set intl roaming to a_voicesmswallet_ir
      $acc = new \Ultra\Lib\MiddleWare\ACC\Control;
      $result = $acc->processControlCommand(
        array(
          'command'    => 'UpdatePlanAndFeatures',
          'actionUUID' => getNewActionUUID(__FUNCTION__ . time()),
          'parameters' => array(
            'msisdn'            => $customer->current_mobile_number,
            'iccid'             => $customer->CURRENT_ICCID_FULL,
            'customer_id'       => $customer_id,
            'wholesale_plan'    => \Ultra\Lib\DB\Customer\getWholesalePlan($customer_id),
            'ultra_plan'        => get_monthly_plan_from_cos_id($customer->COS_ID),
            'preferred_language'=> $customer->preferred_language,
            'option'            => 'RESET',
            'reset_config'      => array('a_voicesmswallet_ir' => round($a_voicesmswallet_ir * 100)),
            'keepDataSocs'      => TRUE
          )
        )
      );

      // check result
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: Middleware error', 'MW0001', implode(', ', $result->get_errors()));
      if ( ! empty($result->data_array['message']))
      {
        $message = json_decode($result->data_array['message']);
        if ( ! empty($message->body->errors))
          $this->errException('ERR_API_INTERNAL: Middleware error', 'MW0001', $message->body->errors[0]);
      }

      // no errors
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

}
