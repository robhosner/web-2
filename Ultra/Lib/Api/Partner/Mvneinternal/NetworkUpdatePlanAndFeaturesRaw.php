<?php

namespace Ultra\Lib\Api\Partner\Mvneinternal;

require_once 'Ultra/Lib/Api/Partner/Mvneinternal.php';

class NetworkUpdatePlanAndFeaturesRaw extends \Ultra\Lib\Api\Partner\Mvneinternal
{

  /**
   * mvneinternal__NetworkUpdatePlanAndFeaturesRaw
   * invokes mwUpdatePlanAndFeaturesRaw
   * @return Result object
   */
  public function mvneinternal__NetworkUpdatePlanAndFeaturesRaw()
  {
    list ($customer_id, $b_voice_value, $b_sms_value, $voicemail) = $this->getInputValues();

    try
    {
      if ( ! $b_voice_value && ! $b_sms_value )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: requires b_voice_value or b_sms_value', 'IN0002' );
        
      teldata_change_db();
    
      $customer = get_customer_from_customer_id( $customer_id );

      if ( ! $customer )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: invalid customer_id' , 'MP0020' );

      if ( ! $customer->current_mobile_number)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no phone number', 'MP0003');
        
      if ( ! $customer->CURRENT_ICCID_FULL )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer has no ICCID' , 'MP0004' );
        
      $middleware = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $middleware->mwUpdatePlanAndFeaturesRaw(
        array(
          'msisdn'             => $customer->current_mobile_number,
          'iccid'              => $customer->CURRENT_ICCID_FULL,
          'customer_id'        => $customer_id,
          'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer_id),
          'ultra_plan'         => get_plan_from_cos_id($customer->COS_ID),
          'preferred_language' => $customer->preferred_language,
          'raw_b_voice'        => $b_voice_value,
          'raw_b_sms'          => $b_sms_value,
          'raw_voicemail'      => $voicemail
        )
      );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $this->errException('ERR_API_INTERNAL: '.$errors[0],'MW0001');
      }

      if ( isset($result->data_array['success']) && ! $result->data_array['success'] )
        $this->errException('ERR_API_INTERNAL: '.$result->data_array['errors'][0],'MW0001');

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

}
