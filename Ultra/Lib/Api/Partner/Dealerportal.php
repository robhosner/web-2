<?php

namespace Ultra\Lib\Api\Partner;

if (!getenv("UNIT_TESTING"))
{
  require_once 'partner-face/provisioning/provision_check_transition.php';
  require_once 'Ultra/Lib/Api/Partner/DealerportalBase.php';
  require_once 'Ultra/Lib/DB/DealerPortal.php';
  require_once 'Ultra/Messaging/Email/DealerPortal.php';
}


/**
 * Dealerportal Partner classes
 *
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Dealer+Portal+Technical+Specs
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Dealer+Portal+-+Search+Spec
 *
 * Example:
 * curl -i 'https://$DOMAIN/ultra_api.php?bath=rest&partner=dealerportal&version=2&command=dealerportal__ValidatePIN&format=jsonp&callback=testc' -d 'pin=123451234512345123&security_token=xyz'
 * curl -i 'https://$DOMAIN/pr/dealerportal/2/ultra/api/dealerportal__SetCustomerInfo' -u dougmeli:Flora -d 'security_token=123&customer_id=31'
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Dealerportal extends \Ultra\Lib\Api\Partner\DealerportalBase
{
  const MAX_FAILED_LOGINS = 6; // max number of failed logins for the same user name

  /**
   * dealerportal__ChargeCustomerProvidedCC
   *
   * Charge up an arbitrary amount to the customer's credit card and add it to the customer's balance.
   * 1 - store CC data - fails if customer had already CC data beforehand
   * 2 - charge CC
   * 3 - delete CC data if $store_cc is not "1"
   * 4 - activate customer if possible
   *
   * @return Result object
   */
/*
  public function dealerportal__ChargeCustomerProvidedCC ()
  {
    list ( $security_token , $account_cc_exp , $account_cc_cvv , $account_cc_number , $cc_name , $cc_postal_code , $charge_amount , $customer_id , $reason , $destination , $store_cc ) = $this->getInputValues();

    if ( ( ! $destination ) || ( $destination == '' ) )
      $destination="WALLET";

    $this->addToOutput('cc_errors',array());
    $this->addToOutput('customer_activated',FALSE);

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $redis = new \Ultra\Lib\Util\Redis;

      // block if there are more than 2500 calls in an hour
      redis_increment_command_count_by_hour( $redis , "dealerportal__ChargeCustomerProvidedCC" );

      // block if there are more than 2500 calls in an hour
      if ( redis_get_command_count_by_hour( $redis , "dealerportal__ChargeCustomerProvidedCC" ) > 2500 )
        $this->errException( "ERR_API_INTERNAL: this command has been currently disabled. Please try again later." , 'AP0001' );

      // get customer data
      $customer = get_customer_from_customer_id( $customer_id );

      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      // fail if customer has already credit card info in the DB
      if ( customer_has_credit_card($customer) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: customer has already credit card info in the DB." , 'CC0002' );

      // get current customer state
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( ! $state )
        $this->errException( "ERR_API_INTERNAL: customer (".$customer->CUSTOMER_ID.") state could not be determined" , 'UN0001' );

      if ( !isset($state['state']) || ( $state['state'] == 'Cancelled' ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command" , 'IN0001' );

      // stored_value+Balance+new_load must not be greater than $200
      dlog('',"total_value = ".( ($charge_amount/100) + $customer->BALANCE + $customer->stored_value ) );

      if ( ( ($charge_amount/100) + $customer->BALANCE + $customer->stored_value ) > 200 )
        $this->errException(
          "ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet. Please use up some of your current balance before adding more.",
          'CH0001'
        );

      // store CC data
      $result = func_update_stored_credit_card(
        array(
          'customer'            => $customer,
          'cc_name'             => $cc_name,
          'account_postal_code' => $cc_postal_code,
          'account_cc_exp'      => $account_cc_exp,
          'account_cc_cvv'      => $account_cc_cvv,
          'account_cc_number'   => $account_cc_number
        )
      );

      if ( count($result['errors']) )
      {
        dlog('',"func_update_stored_credit_card result = %s",$result);

        if ( count($result['error_codes']) )
          $this->errException( $result['errors'][0] , $result['error_codes'][0] );
        else
          $this->errException( "ERR_API_INTERNAL: DB write error. (1)" , 'DB0001' );
      }

      sleep(2); // give the IPCOMMAND a chance to catch up

      $result['success'] = 1;

      // reload customer data
      list($result,$customer) = verify_cc_number_propagation($result,$customer);

      if ( $result['success'] != 1 )
        $this->errException( "ERR_API_INTERNAL: DB write error. (2)" , 'DB0001' );

      // charge CC

      $session = create_guid('dealerportal');
      $return  = array();

      if ( $destination == "WALLET" )
      {
        $return = func_add_balance_by_credit_card(
          array(
            'customer'             => $customer,
            'amount'               => ($charge_amount/100), # in $
            'reason'               => $reason,
            'session'              => $session,
            //'terminal_id'          => '', // HTT_BILLING_HISTORY.TERMINAL_ID
            //'store_zipcode'        => '', // HTT_BILLING_HISTORY.STORE_ZIPCODE
            'store_id'             => $session_data['dealer'],  // HTT_BILLING_HISTORY.STORE_ID
            'clerk_id'             => $session_data['user_id'], // HTT_BILLING_HISTORY.CLERK_ID
            'upgrade_seconds_gain' => 0,
            'detail'               => __FUNCTION__
          )
        );
      }
      else
      {
        $return = func_add_stored_value_by_credit_card(
          array(
            'customer'             => $customer,
            'amount'               => ($charge_amount/100), # in $
            'description'          => $reason,
            'detail'               => __FUNCTION__,
            'session'              => $session,
            //'terminal_id'          => '', // HTT_BILLING_HISTORY.TERMINAL_ID
            //'store_zipcode'        => '', // HTT_BILLING_HISTORY.STORE_ZIPCODE
            'store_id'             => $session_data['dealer'],  // HTT_BILLING_HISTORY.STORE_ID
            'clerk_id'             => $session_data['user_id'], // HTT_BILLING_HISTORY.CLERK_ID
            'upgrade_seconds_gain' => 0
          )
        );
      }

      if ( isset($return['activated']) && $return['activated'] )
        $this->addToOutput('customer_activated',TRUE);

      if ( count($return['errors']) )
      {
        if ( isset( $result['rejection_errors'] ) )
          $this->addToOutput('cc_errors',$result['rejection_errors']);

        // cleanup CC info

        if ( ! reset_credit_card( $customer->CUSTOMER_ID ) )
        {
          dlog('',"reset_credit_card failed");
          $this->addWarning( "Error while trying to update accounts" );
        }

        $this->errException( "ERR_API_INTERNAL: credit card transaction not successful" , 'CC0001' );
      }

      // cleanup CC info

      if ( $store_cc != '1' )
        if ( ! reset_credit_card( $customer->CUSTOMER_ID ) )
        {
          dlog('',"reset_credit_card failed");
          $this->addWarning( "Error while trying to update accounts" );
        }

      // update ULTRA.HTT_ACTIVATION_HISTORY if necessary

      $activation_history = get_ultra_activation_history( $customer->CUSTOMER_ID );

      if ( $activation_history
        && ( $activation_history->FINAL_STATE != FINAL_STATE_COMPLETE )
        && ( $activation_history->FINAL_STATE != 'Suspended' )
        && ( $activation_history->FINAL_STATE != 'Cancelled' )
      )
        log_funding_in_activation_history( $customer->CUSTOMER_ID , 'CCARD' , ($charge_amount/100) );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
*/

  protected function validate_search_sort_by( $sort_by )
  {
    $invalid = 0;

    foreach( $sort_by as $sort_clause )
    {
      $strings = explode(' ',$sort_clause);

      if ( ! preg_match( "/^(created_date|service_expires_date|customer_id|msisdn|iccid|email|first_name|last_name|cos_id|plan_state|balance|stored_value|monthly_cc_renewal|port_status|port_query_status|port_status_date)$/" , $strings[0] ) )
        $invalid = 1;

      if ( isset($strings[1]) && ( $strings[0] != '' ) && ( ! preg_match( "/^(a|de)sc$/" , $strings[1] ) ) )
        $invalid = 1;
    }

    return $invalid;
  }

  protected function search_api_select_attributes()
  {
    return array(
      customer_plan_amount_attribute(),
      customer_plan_name_attribute(),
      "CASE WHEN DEACTIVATION_DATE  IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', DEACTIVATION_DATE  ) END cancelled_date",
      "CASE WHEN CREATION_DATE_TIME IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', CREATION_DATE_TIME ) END created_date",
      "CASE WHEN PLAN_EXPIRES       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', PLAN_EXPIRES       ) END service_expires_date",
      "CASE WHEN port_status_date   IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', port_status_date   ) END port_status_date",
      "CASE WHEN plan_started       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', plan_started       ) END plan_started",
      'u.CUSTOMER_ID         customer_id',
      'CURRENT_MOBILE_NUMBER msisdn',
      'CURRENT_ICCID_FULL    iccid',
      'E_MAIL                email',
      'first_name',
      'last_name',
      'a.cos_id',
      'plan_state',
      'balance',
      'stored_value',
      'monthly_cc_renewal',
      'port_status',
      'port_query_status',
    );
  }

  /**
   * getCustomerTransactionHistory
   *
   * Query HTT_BILLING_HISTORY
   *
   * @return array
   */
  protected function getCustomerTransactionHistory($customer_id = false, $start_epoch = false, $end_epoch = false)
  {
    if (!$customer_id)  throw new \Exception('Missing customer_id');
    if (!$start_epoch)  throw new \Exception('Missing start_epoch');
    if (!$end_epoch)    throw new \Exception('Missing end_epoch');

    $transaction_history = array();
    $result = get_billing_transaction_history(array(
      'customer_id' => $customer_id,
      'start_epoch' => $start_epoch,
      'end_epoch'   => $end_epoch)
    );

    // flatten the result into an array
    if (count($result['errors']) > 0)
    {
      $this->errException('ERR_API_INTERNAL: ' . $results['errors'][0], 'DB0001');
    }

    foreach( $result['billing_transaction_history'] as $id => $data )
    {
      $transaction_history[] = $data->history_epoch; // date returned in PST
      $transaction_history[] = $data->order_id;
      $transaction_history[] = $data->type;
      $transaction_history[] = $data->history_source;
      $transaction_history[] = $data->AMOUNT;
      $transaction_history[] = $data->description;
    }

    return $transaction_history;
  }
}

