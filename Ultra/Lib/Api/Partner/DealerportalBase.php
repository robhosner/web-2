<?php

namespace Ultra\Lib\Api\Partner;

/**
 * Base class for Dealerportal APIs
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Dealer Portal
 */
abstract class DealerportalBase extends \Ultra\Lib\Api\PartnerBase
{
  protected $ultrasession;

  // access roles
  const ROLE_NAME_ADMIN           = 'Administrator All Access';
  const ROLE_NAME_NVO             = 'MVNO Owner';
  const ROLE_NAME_NVO_MANAGER     = 'MVNO Manager';
  const ROLE_NAME_NVO_EMPLOYEE    = 'MVNO Employee';
  const ROLE_NAME_MASTER_ALL      = 'Master Owner';
  const ROLE_NAME_MASTER_LIMITED  = 'Master Manager';
  const ROLE_NAME_MASTER_EMPLOYEE = 'Master Employee';
  const ROLE_NAME_SUB_ALL         = 'SubDistributor Owner';
  const ROLE_NAME_SUB_LIMITED     = 'SubDistributor Manager';
  const ROLE_NAME_SUB_EMPLOYEE    = 'SubDistributor Employee';
  const ROLE_NAME_DEALER_OWNER    = 'Dealer Owner';
  const ROLE_NAME_DEALER_MANAGER  = 'Dealer Manager';
  const ROLE_NAME_DEALER_EMPLOYEE = 'Dealer Employee';

  const CRT_DEV  = 13408;
  const CRT_PROD = 39798;

  // customer session cookie prefix and TTL
  const SUBSCRIBER_COOKIE_NAME = 'ultrasub_';
  const SUBSCRIBER_COOKIE_TTL  = 14400; // 4 hours

  // dashboard redis keys and TTL
  const DASHBOARD_CUSTOMERS_KEY   = 'ultra/dashboard/customers/';
  const DASHBOARD_CUSTOMERS_TTL   = 1200; // 20 min
  const DASHBOARD_ACTIVATIONS_KEY = 'ultra/dashboard/activations/';
  const DASHBOARD_ACTIVATIONS_TTL = 60; // 1 min
  const DASHBOARD_RECHARGE_TTL    = 3600; // 1 hour

  // other redis keys
  const TEMP_PASSWORD_KEY         = 'temp_password/dealer'; // temporary password

  // PROD-1068: max time that counts as easypay activated
  const EASYPAY_ACTIVATION_KEY    = 'ultra/activation/customer/';
  const EASYPAY_ACTIVATION_PERIOD = 600; // 10 min

  // allowed output values when dealer has no access to customer
  private static $unrestricted = array(
    'balance',
    'customer_id',
    'msisdn',
    'plan_name',
    'plan_state',
    'created_date',
    'service_expires_date',
    'has_access',
    'brand',
  );

  /**
   * Class constructor
   */
  public function __construct ()
  {
    $this->ultrasession = new \Ultra\Lib\Util\Redis\UltraSession;
  }


  /**
   * checkUltraSessionAllowedRole
   *
   * Verifies that $roleName can execute $apiName
   *
   * @return boolean
   */
  private function checkUltraSessionAllowedRole( $roleName , $apiName )
  {
    if ( $roleName == self::ROLE_NAME_ADMIN )
      return TRUE;

    // load data from DB
    $data = get_api_by_role( NULL , $roleName , $apiName);

    dlog('',"roleName = $roleName ; apiName = $apiName ; data = %s",$data);

    return ! ! count( $data );
  }

  /**
   * checkDealerAccessToCustomer
   *
   * Verifies that the user has access to customer
   *
   * @return boolean
   */
  protected function checkDealerAccessToCustomer( $sessionData , $customer_id )
  {
    if ( $sessionData['role'] == self::ROLE_NAME_ADMIN )
      return TRUE;

    $crtDealerSiteId = \Ultra\UltraConfig\isProdEnvironment() ? self::CRT_PROD : self::CRT_DEV;
    if ( $sessionData['dealer'] == $crtDealerSiteId )
      return TRUE;

    // TODO: move to cfengine
    // API-2042: check super users
    if (\Ultra\UltraConfig\isProdEnvironment()) {
      $suArr = [39026, 39027, 39028];
    } else {
      $suArr = [13405, 13406, 13407];
    }
    if (in_array($sessionData['dealer'], $suArr)) {
      return true;
    }

    if (in_array($sessionData['role'], array(self::ROLE_NAME_DEALER_OWNER, self::ROLE_NAME_DEALER_MANAGER, self::ROLE_NAME_DEALER_EMPLOYEE)) && can_map_dealer_to_customer( $sessionData['dealer'] , $customer_id )) {
      // dealer tied to customer
      return TRUE;
    }

    // the following roles are currently not handled
    if ( $sessionData['role'] == self::ROLE_NAME_MASTER_ALL )
      return FALSE;

    if ( $sessionData['role'] == self::ROLE_NAME_MASTER_LIMITED )
      return FALSE;

    if ( $sessionData['role'] == self::ROLE_NAME_SUB_ALL )
      return FALSE;

    if ( $sessionData['role'] == self::ROLE_NAME_SUB_LIMITED )
      return FALSE;

    // at this point the only possibility is that a customer session has been set for this not-activating dealer by dealerportal__AuthorizeCustomerAccess
    $ultraCustomerSessionData = $this->ultrasession->getCustomerObject( $customer_id );

    dlog('',"ultraCustomerSessionData = %s",$ultraCustomerSessionData);


    if ( $ultraCustomerSessionData && is_object($ultraCustomerSessionData) ) {
      return FALSE;
    }

    // verifies that the tracking attributes are matching
    if ( ( $sessionData['dealer']      != $ultraCustomerSessionData->dealer )
      || ( $sessionData['distributor'] != $ultraCustomerSessionData->distributor )
      || ( $sessionData['masteragent'] != $ultraCustomerSessionData->masteragent )
    ) {
      return FALSE;
    }

    // dealer has requested and been granted access to customer
    return TRUE;
  }

  /**
   * checkUltraSessionAllowedAPICustomer
   *
   * Verifies that the user can invoke the given API for the given customer, which is in the given plan_state.
   *
   * @return boolean
   */
  protected function checkUltraSessionAllowedAPICustomer( $sessionData , $customer_id , $plan_state=NULL , $api_name )
  {
    if ( $sessionData['role'] == self::ROLE_NAME_ADMIN )
      return TRUE;

    $crtDealerSiteId = \Ultra\UltraConfig\isProdEnvironment() ? self::CRT_PROD : self::CRT_DEV;
    if ( $sessionData['dealer'] == $crtDealerSiteId )
      return TRUE;

    // TODO: move to cfengine
    // API-2042: check super users
    if (\Ultra\UltraConfig\isProdEnvironment()) {
      $suArr = [39026, 39027, 39028];
    } else {
      $suArr = [13405, 13406, 13407];
    }
    if (in_array($sessionData['dealer'], $suArr)) {
      return true;
    }

    if (in_array($sessionData['role'], array(self::ROLE_NAME_DEALER_OWNER, self::ROLE_NAME_DEALER_MANAGER, self::ROLE_NAME_DEALER_EMPLOYEE)))
    {
      $check = can_map_dealer_to_customer( $sessionData['dealer'] , $customer_id );

      if ( !$check )
      {
        // at this point the only possibility is that a customer session has been set for this not-activating dealer by dealerportal__AuthorizeCustomerAccess
        $ultraCustomerSessionData = $this->ultrasession->getCustomerObject( $customer_id );

        dlog('',"ultraCustomerSessionData = %s",$ultraCustomerSessionData);

        // verifies that the tracking attributes are matching
        if ( $ultraCustomerSessionData && is_object($ultraCustomerSessionData) )
        {
          if ( ( $sessionData['dealer']      != $ultraCustomerSessionData->dealer )
            || ( $sessionData['distributor'] != $ultraCustomerSessionData->distributor )
            || ( $sessionData['masteragent'] != $ultraCustomerSessionData->masteragent )
          )
            return FALSE;
        }
        else
          return FALSE;
      }
    }

    // get actual plan_state without caching
    if ( is_null($plan_state) )
      $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer_id, 'plan_state', 0);

    // the following roles are currently not handled

    if ( $sessionData['role'] == self::ROLE_NAME_MASTER_ALL )
      return FALSE;

    if ( $sessionData['role'] == self::ROLE_NAME_MASTER_LIMITED )
      return FALSE;

    if ( $sessionData['role'] == self::ROLE_NAME_SUB_ALL )
      return FALSE;

    if ( $sessionData['role'] == self::ROLE_NAME_SUB_LIMITED )
      return FALSE;

    // check if the API belongs to at least an allowable UI action based on the customer's plan_state
    return can_map_plan_state_to_api( $plan_state , $api_name );
  }

  /**
   * getValidUltraSessionData
   *
   * Retrieve and validate session from $securityToken
   *
   * @return array
   */
  protected function getValidUltraSessionData( $securityToken , $apiName )
  {
    // get session data
    $sessionData = $this->getUltraSessionData( $securityToken );

    if ( ! $sessionData || ! $sessionData['is_valid'] )
      return array(
        $sessionData,
        'SE0002',
        'ERR_API_INTERNAL: no login session found for the given security_token'
      );

    if ( ! $this->checkUltraSessionAllowedRole( $sessionData['role'] , $apiName ) )
      return array(
        $sessionData,
        'SE0004',
        'ERR_API_INTERNAL: the current user is not allowed to execute this command'
      );

// TODO: subscriber_dependent or subscriber_independent ?

    return array(
      $sessionData,
      '',
      ''
    );
  }

  /**
   * getUltraSessionData
   *
   * Retrieve Session Data from Redis.
   * Session Data includes:
   *  - masteragent
   *  - distributor
   *  - dealer
   *  - dealer_code
   *  - store_name
   *  - user_id
   *  - role
   *  - donor_id
   *
   * @return array
   */
  protected function getUltraSessionData( $securityToken )
  {
    $sessionData = array(
      'is_valid' => FALSE
    );

    // retrieve Session Data from Redis
    $ultraSessionData = $this->ultrasession->getObject( $securityToken );

    if ( $ultraSessionData )
    {
      $sessionData = (array) $ultraSessionData;

      $sessionData['is_valid'] = TRUE;
    }
    else
    {
      // global override
      $securityTokenOverride = \Ultra\UltraConfig\celluphoneUltrasessionOverride();

      if ( $securityTokenOverride == $securityToken )
        $sessionData = array(
          'timestamp_lastcheck' => time(),
          'is_valid'         => TRUE,
          'donor_id'         => '999999999',
          'user_id'          => '999999999',
          'role'             => self::ROLE_NAME_ADMIN, // VYT: change to ROLE_NAME_DEALER_OWNER for limited testing
          'dealer_code'      => '',
          'store_name'       => '',
          'masteragent'      => '999999999',
          'businessname'     => '999999999',
          'distributor'      => '999999999',
          'dealer'           => '999999999',
          'master_id'        => '',
          'parent_master_id' => ''
        );
    }

    dlog('',"sessionData = %s",$sessionData);

    return $sessionData;
  }

  /**
   * getCelluphoneSubDistributorData
   *
   * Given a Celluphone user ID, we retrieve SubDistributor info from Celluphone DB
   *
   * @return array
   */
  private function getCelluphoneSubDistributorData( $user_id )
  {
    $distributorInfo = \get_celluphone_distributor( $user_id );

    if ( ! $distributorInfo )
      return array(
        array(),
        'ND0007',
        'ERR_API_INVALID_ARGUMENTS: no celluphone SubDistributor info found.'
      );

    return array(
      array(
        'role'             => self::ROLE_NAME_SUB_ALL, // We currently do not distinguish between ROLE_NAME_SUB_ALL and ROLE_NAME_SUB_LIMITED
        'dealer_code'      => $distributorInfo->master_dealer_code,
        'store_name'       => $distributorInfo->master_store_name,
        'masteragent'      => $distributorInfo->masteragent,
        'businessname'     => '', # TODO:
        'distributor'      => $distributorInfo->distributor,
        'dealer'           => 0,
        'master_id'        => $distributorInfo->master_id,
        'parent_master_id' => $distributorInfo->parent_master_id
      ),
      '',
      ''
    );
  }

  /**
   * getCelluphoneMasterData
   *
   * Given a Celluphone user ID, we retrieve Master info from Celluphone DB
   *
   * @return array
   */
  private function getCelluphoneMasterData( $user_id )
  {
    $masterInfo = \get_celluphone_masteragent( $user_id );

    if ( ! $masterInfo )
      return array(
        array(),
        'ND0008',
        'ERR_API_INVALID_ARGUMENTS: no celluphone Master info found.'
      );

    return array(
      array(
        'role'             => self::ROLE_NAME_MASTER_ALL, // We currently do not distinguish between ROLE_NAME_MASTER_ALL and ROLE_NAME_MASTER_LIMITED
        'dealer_code'      => $masterInfo->master_dealer_code,
        'store_name'       => $masterInfo->master_store_name,
        'masteragent'      => $masterInfo->masteragent,
        'businessname'     => '', # TODO:
        'distributor'      => 0,
        'dealer'           => 0,
        'master_id'        => $masterInfo->master_id,
        'parent_master_id' => $masterInfo->parent_master_id
      ),
      '',
      ''
    );
  }

  /**
   * getCelluphoneSessionData
   *
   * Given a Celluphone user ID, we retrieve Role and other info from Celluphone DB
   *
   * @return array
   */
  protected function getCelluphoneSessionData( $user_id )
  {
    $userInfo = \get_session_info_from_user_id( $user_id );

    if ( ! $userInfo )
      return array(
        array(),
        'ND0005',
        'ERR_API_INVALID_ARGUMENTS: no celluphone user info found.'
      );

    if ( $userInfo->ROLE_DESCRIPTION == 'Dealer' )
      return array(
        array(
          'role'             => self::ROLE_NAME_DEALER_OWNER, // We currently do not distinguish between ROLE_NAME_DEALER_OWNER and ROLE_NAME_DEALER_EMPLOYEE
          'dealer_code'      => $userInfo->DEALER_CODE,
          'store_name'       => $userInfo->STORE_NAME,
          'masteragent'      => $userInfo->MASTERAGENT,
          'businessname'     => $userInfo->BUSINESSNAME,
          'distributor'      => $userInfo->DISTRIBUTOR,
          'dealer'           => $userInfo->DEALER,
          'master_id'        => $userInfo->DEALER_MASTER_ID,
          'parent_master_id' => ''
        ),
        '',
        ''
      );

    if ( $userInfo->ROLE_DESCRIPTION == 'Master' )
      return $this->getCelluphoneMasterData( $user_id );

    if ( $userInfo->ROLE_DESCRIPTION == 'SubDistributor' )
      return $this->getCelluphoneSubDistributorData( $user_id );

    // MVNO-2282 - Celluphone actually mispelled MVNO
    if ( $userInfo->ROLE_DESCRIPTION == 'NVO' )
      return array(
        array(
          'role'             => self::ROLE_NAME_NVO,
          'dealer_code'      => '',
          'store_name'       => '',
          'masteragent'      => '999919999',
          'businessname'     => '999919999',
          'distributor'      => '999919999',
          'dealer'           => '999919999',
          'master_id'        => '',
          'parent_master_id' => ''
        ),
        '',
        ''
      );

    return array(
      array(),
      'ND0006',
      'ERR_API_INTERNAL: celluphone role not handled ('.$userInfo->ROLE_DESCRIPTION.').'
    );
  }


  /**
   * removeRestrictedOutput
   *
   * remove restricted output values from API output when dealer does not have full access to customer
   *
   * @author: VYT 2014-03-04
   *
   */
  protected function removeRestrictedOutput()
  {
    // process all data and remove fields missing from $unrestricted
    foreach($this->result->data_array as $name => $value)
      if (! in_array($name, self::$unrestricted))
        $this->result->data_array[$name] = NULL;
  }


  /**
   * resetSessionExpiration
   * refresh session in redis and renew it expiration time
   */
  public function resetSessionExpiration($token, $session)
  {
     $this->ultrasession->refreshObject($token, $session);
  }

  /**
   * overwriteIdentityParameter
   * replace business_type and business_code parameters with the caller's identity as required by the logic of some APIs
   * since such logic deals with access permissions we must throw API exception on failures
   * @param Array API parameters
   * @param Array user session
   * @param String minimum business type that does not trigger overwrite (Administrator, Master, SubDistributor)
   * @returns Array API parameters
   */
  protected function overwriteIdentityParameters($params, $session, $type)
  {
    if ( ! is_array($params))
      $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002', 'Invalid function parameters');

    // get caller organization type
    list($caller) = explode(' ', $session['role']); // discard role
    if (empty($caller) || empty($session['business_code']))
      $this->errException('ERR_API_INTERNAL: Role permission error', 'SE0009', 'Failed to determine user organization or role');

    // overwrite identity if the caller has lower organizational level lower than specified
    if (($params['business_type'] != $caller || $params['business_code'] != $session['business_code']) &&
      (($type == 'Administrator' && $caller != $type) ||
      ($type == 'MVNO' && $caller != 'Administrator' && $caller != $type) ||
      ($type == 'Master' && ($caller = 'SubDistributor' || $caller == 'Dealer')) ||
      ($type == 'SubDistributor' && $caller == 'Dealer')))
    {
      dlog('', "overwritten business identity type: {$params['business_type']} -> $caller, code: {$params['business_code']} -> {$session['business_code']}");
      $params['business_type'] = $caller;
      $params['business_code'] = $session['business_code'];
    }

    return $params;
  }
}

