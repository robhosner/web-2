<?php

namespace Ultra\Lib\Api\Partner\ApiPublic;

require_once 'Ultra/Lib/Api/Partner/ApiPublic.php';

class ZIPCoverageInfo extends \Ultra\Lib\Api\Partner\ApiPublic
{
  /**
   * Provides the coverage levels of Ultra Service in the specified Zipcode
   *
   * @param string $zipcode
   * @return object Result
   */
  public function ultrainfo__ZIPCoverageInfo()
  {
    list ($zipcode) = $this->getInputValues();

    $quality = 0;

    try
    {
      if (!verify_request_source())
      {
        $this->errException('ERR_API_ACCESS_DENIED: request source rejected', 'SE0004');
      }

      $redis = new \Ultra\Lib\Util\Redis;

      $quality_key = "coverage_info_quality/" . $zipcode;

      $quality = $redis->get($quality_key);

      if (!isset($quality) || $quality == '')
      {
        # Redis does not contain a value for the given zip code, we must query the DB

        teldata_change_db();

        $coverage_data = htt_coverage_info_get_coverage_data($zipcode);

        if ( $coverage_data && is_array($coverage_data) && count($coverage_data) )
        {
          $quality = $coverage_data[0]->quality;
        }
        else
        {
          $quality = 0;
        }

        # save the result in Redis.
        $redis->set($quality_key, $quality, 3*60*60*24);
      }

      $this->addToOutput('quality', $quality);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}