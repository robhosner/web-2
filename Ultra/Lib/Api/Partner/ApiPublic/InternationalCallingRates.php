<?php

namespace Ultra\Lib\Api\Partner\ApiPublic;

require_once 'Ultra/Lib/Api/Partner/ApiPublic.php';

class InternationalCallingRates extends \Ultra\Lib\Api\Partner\ApiPublic
{
  /**
   * ultrainfo__InternationalCallingRates
   *
   * Provides the calling card rates for the specified product
   *
   * @param string $product_name
   * @return object Result
   */
  public function ultrainfo__InternationalCallingRates()
  {
    list ($product_name) = $this->getInputValues();

    $rates_list = array();

    try
    {
      teldata_change_db();

      $rates_data = htt_card_calling_rates_get_rates_data($product_name, TRUE);

      if ($rates_data && is_array($rates_data) && count($rates_data))
      {
        $rates_list = $rates_data;
      }
      else
        $this->errException('ERR_API_INTERNAL: data not found', 'ND0001');

      $this->addToOutput('rates_list', $rates_list);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}