<?php

namespace Ultra\Lib\Api\Partner\Publicprojw;

require_once 'classes/ProjectW/DB.php';

class ExportErrors extends \Ultra\Lib\Api\Partner\Publicprojw
{
  const MAX_IMPORT_ROWS = 2250; // maximum number of failed import rows to return
  const MAX_SOAP_ROWS   = 50;   // maximun number of SOAP_LOG rows to retrieve per import row

  /**
   * publicprojw__ExportErrors
   *
   * export all errors from ULTRA.UNIVISION_IMPORT along with corresponding rows of SOAP_LOG
   * @see API-419
   * @return Result object
   */
  public function publicprojw__ExportErrors()
  {
    // initialization
    list($batch) = $this->getInputValues();
    $data = array(); // output data

    try
    {
      // get ULTRA.UNIVISION_IMPORT records with errors
      teldata_change_db();

      $import = \ProjectW\getFailedImportRows($batch, self::MAX_IMPORT_ROWS);
      if (empty($import))
        $this->errException('ERR_API_INVALID_ARGUMENTS: No data found', 'ND0001');
      logInfo('found ' . count($import) . ' rows');

      // get corresponding SOAP_LOG rows
      \Ultra\Lib\DB\ultra_acc_connect();
      foreach ($import as $importRow)
      {
        $soap = get_soap_log([
          'sql_limit'   => self::MAX_SOAP_ROWS,
          'msisdn'      => array("1{$importRow->MSISDN}", $importRow->MSISDN),
          'order'       => 'SOAP_LOG_ID DESC'
        ]);

        // copy import row for each SOAP_LOG row
        if (count($soap))
          foreach ($soap as $soapRow)
          {
            unset($soapRow->ENV);
            $data[] = array_merge((array)$importRow, (array)$soapRow);
          }
        else
          logError("no SOAP_LOG rows found for UNIVISION_IMPORT_ID {$importRow->UNIVISION_IMPORT_ID} and MSISDN {$importRow->MSISDN}");
      }

      logInfo('result contains ' . count($data) . ' rows');
      $this->succeed();
    }
    catch (\Exception $e)
    {
      logError('EXCEPTION: ' . $e->getMessage());
    }

    $this->addToOutput('import_errors', $data);
    return $this->result;
  }
}

