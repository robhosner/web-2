<?php

namespace Ultra\Lib\Api\Partner\Publicprojw;

require_once 'Ultra/Lib/Api/Partner/Publicprojw.php';
require_once 'classes/ProjectW.php';

class GetTodaysFiles extends \Ultra\Lib\Api\Partner\Publicprojw
{
  /**
   * publicprojw__GetTodaysFiles
   *
   * Retrieve filenames of UNIVISION_IMPORT records for current day
   *
   * @return Result object
   */
  public function publicprojw__GetTodaysFiles()
  {
    $error_code = '';

    $this->addToOutput( 'files', '' );

    try
    {
      teldata_change_db();

      $now = time();
      // if it is before 10am, lookup files from yesterday
      if ( $now < strtotime( 'today 10am' ) )
      {
        $start = date('Y-m-d H:i:s',  strtotime( 'yesterday 00:00:00' ) );
        $end = date('Y-m-d H:i:s',  strtotime( 'yesterday 23:59:59' ) );
      }
      else
      {
        $start = date('Y-m-d H:i:s',  strtotime( 'today 00:00:00' ) );
        $end = date('Y-m-d H:i:s',  strtotime( 'today 23:59:59' ) );
      }

      // lookup UNIVISION_IMPORT records
      $q = \select_univision_import_by_date_query( $start, $end );
      $r = mssql_fetch_all_objects( logged_mssql_query( $q ) );

      dlog('',"query result = %s",$r);

      $result = array();
      foreach ( $r as $univisionImport )
      {
        $result[$univisionImport->IMPORT_FILE_NAME] = (array)$univisionImport;
      }

      $this->addToOutput( 'files', $result );

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}
