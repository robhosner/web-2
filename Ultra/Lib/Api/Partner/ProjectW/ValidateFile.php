<?php

namespace Ultra\Lib\Api\Partner\ProjectW;

require_once 'Ultra/Lib/Api/Partner/ProjectW.php';

class ValidateFile extends \Ultra\Lib\Api\Partner\ProjectW
{
  /**
   * projectw__ValidateFile
   *
   * Validates UNIVISION_IMPORT records for a given filename
   *
   * @return Result object
   */
  public function projectw__ValidateFile()
  {
    list( $filename ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      // check file exists
      $fullPath = self::PROJECTW_IMPORT_FILE_PATH . $filename;
      \logInfo( 'Full path to file: ' . $fullPath );
      if ( ! file_exists( $fullPath ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: filename $filename does not exist", 'PW0001' );

      // check status of file
      $status = \select_univision_import_file_status_by_filename( $filename );
      if ( !$status || empty( $status->IMPORT_FILE_STATUS ) || ( $status->IMPORT_FILE_STATUS != 'INITIALIZED' && $status->IMPORT_FILE_STATUS != 'VALIDATED' ) )
        $this->errException( 'Invalid IMPORT_FILE_STATUS for file', 'IN0002' );

      // validate
      $projectWObj = new \ProjectW();
      $result = $projectWObj->validateImportFile( $filename );
      
      // write validation log
      $validationFilename = str_replace( '.csv', '.validation.txt', $filename );
      $validationFileHandle = fopen( self::PROJECTW_IMPORT_FILE_PATH . $validationFilename, 'w' );
      if ( $validationFileHandle )
      {
        foreach ( $result as $r )
        {
          if ( !empty( $r['LAST_ERROR'] ) )
          {
            $str = implode( ' - ', $r );
            fwrite( $validationFileHandle, $str . PHP_EOL );
          }
        }

        fclose( $validationFileHandle );

        $this->succeed();
      }
      else
        $this->errException( 'Failed to open validation log file for writing', 'IN0002', 'Failed to open validation log file for writing' );
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

