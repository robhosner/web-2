<?php

namespace Ultra\Lib\Api\Partner\ProjectW;

require_once 'Ultra/Lib/Api/Partner/ProjectW.php';

class RetryPort extends \Ultra\Lib\Api\Partner\ProjectW
{
  /**
   * projectw__RetryPort
   *
   * Sets a UNIVISION_IMPORT record for retry
   *
   * @param 
   * @return Result object
   */
  public function projectw__RetryPort()
  {
    list( $msisdn, $univision_import_id ) = $this->getInputValues();

    try
    {
      if ( empty( $msisdn ) && empty( $univision_import_id ) )
        $this->errException( 'Either msisdn or univision_import_id is required', 'MP0001' );

      // find univision_import record
      $projectWObj = new \ProjectW();
      $success = !empty( $msisdn ) ? $projectWObj->getUVImportByMSISDN( $msisdn ) :  $projectWObj->getUVImportByID( $univision_import_id );
      if ( !$success )
        $this->errException( 'Record not found', 'ND0001' );

      $customerData = $projectWObj->getCustomerData();
      if ( !$projectWObj->checkUVImportPortRetryStatus( $customerData ) )
        $this->errException( 'Invalid status for retry', 'IN0002' );

      // set to retry
      if ( !$projectWObj->setUVImportPortRetry( $customerData ) )
        $this->errException( 'Failed to set retry status', 'IN0002' );

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
