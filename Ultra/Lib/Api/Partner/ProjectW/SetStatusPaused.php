<?php

namespace Ultra\Lib\Api\Partner\ProjectW;

require_once 'Ultra/Lib/Api/Partner/ProjectW.php';
require_once 'classes/ProjectW.php';

class SetStatusPaused extends \Ultra\Lib\Api\Partner\ProjectW
{
  /**
   * projectw__SetStatusPaused
   *
   *
   *
   * @param 
   * @return Result object
   */
  public function projectw__SetStatusPaused()
  {
    list ($filename) = $this->getInputValues();

    try
    {
      \logit("FULL PATH TO FILE: " . self::PROJECTW_IMPORT_FILE_PATH . "$filename");

      if ( ! file_exists(self::PROJECTW_IMPORT_FILE_PATH . "$filename"))
        $this->errException("ERR_API_INVALID_ARGUMENTS: filename $filename does not exist", 'PW0001');

      teldata_change_db();

      $projectw = new \ProjectW();

      if ( ! $projectw->validateImportFileStatusIn($filename, array(
        UNIVISION_FILE_STATUS_ONGOING
      )))
        $this->errException('ERR_API_INVALID_ARGUMENTS: IMPORT_FILE_STATUS invalid for command', 'PW0002');

      \logit("Current IMPORT_FILE_STATUS for file $filename is OK");
        
      $newStatus = UNIVISION_FILE_STATUS_PAUSED;

      \logit("Setting IMPORT_FILE_STATUS to $newStatus for file $filename");

      if ( ! $projectw->updateImportFileStatusByFilename($filename, $newStatus))
        $this->errException('ERR_API_INTERNAL: ERROR updating table ULTRA.UNIVISION_IMPORT', 'DB0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
