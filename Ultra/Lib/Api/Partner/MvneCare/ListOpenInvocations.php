<?php

namespace Ultra\Lib\Api\Partner\MvneCare;

require_once 'Ultra/Lib/Api/Partner/MvneCare.php';

class ListOpenInvocations extends \Ultra\Lib\Api\Partner\MvneCare
{
  const MAX_INVOCATION_RECORDS = 1000; // max number of records to return from COMMAND_INVOCATIONS
  
  /**
   * mvnecare__ListOpenInvocations
   *
   * return invocation records with specific status
   * http://wiki.hometowntelecom.com:8090/display/SPEC/MVNEcare+APIs
   *
   * @return Result object
   */
  public function mvnecare__ListOpenInvocations()
  {
    // init
    list ($status) = $this->getInputValues();
    $this->addToOutput('records', array());
    $this->addToOutput('record_count', 0);

    try
    {
      // connect to DB
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');

      // AMDOCS-147: always filter out COMPLETED
      $params = array("STATUS <> 'COMPLETED'");

      // AMDOCS-363: filter out OPEN and FAILED
      $params[] = "STATUS <> 'FAILED'";

      // AMDOCS-394: filter RESOLVED before today's midnight PT
      $dateTime = new \DateTime(date('m/d/Y'), new \DateTimeZone('America/Los_Angeles'));
      $dateTime->setTimezone(new \DateTimeZone('UTC'));
      $cutoff = $dateTime->format('M d Y h:i:s A');
      $params[] = "(STATUS <> 'RESOLVED' OR (STATUS = 'RESOLVED' AND LAST_STATUS_DATE_TIME > '$cutoff'))";

      // AMDOCS-440: filter INITIATED less than 5 minutes ago
      $params[] = "(STATUS <> 'INITIATED' OR (STATUS = 'INITIATED' AND LAST_STATUS_DATE_TIME < DATEADD(mi, -5, GETUTCDATE())))";

      if (! empty($status))
          $params['STATUS'] = $status;

      // get data
      $data = get_ultra_acc_command_invocations($params, self::MAX_INVOCATION_RECORDS, 'COMMAND_INVOCATIONS_ID desc', TRUE);
      $count = count($data);

      // check results and add to response
      if ($count)
      {
        // AMDOCS-363: add ICCID and MSISDN
        teldata_change_db();
        foreach ($data as &$row)
        {
          if (empty($row->CUSTOMER_ID))
          {
            $row->ICCID = NULL;
            $row->MSISDN = NULL;
          }
          else
          {
            $row->ICCID = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $row->CUSTOMER_ID, 'CURRENT_ICCID_FULL');
            $row->MSISDN = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $row->CUSTOMER_ID, 'CURRENT_MOBILE_NUMBER');
          }

          // PROD-312: attempt tp retrieve MSISDN and ICCID from HTT_CANCELLATION_REASONS
          if (empty($row->MSISDN) && ! empty($row->CUSTOMER_ID))
          {
            $cancellation = get_cancellation_by_customer_id($row->CUSTOMER_ID);
            if (! empty($cancellation->MSISDN))
              $row->MSISDN = $cancellation->MSISDN;
            if (empty($row->ICCID) && ! empty($cancellation->ICCID))
              $row->ICCID = $cancellation->ICCID;
          }
        }

        // AMDOCS-366: post-process dates
        $schema = \Ultra\Lib\DB\getTableSchema('COMMAND_INVOCATIONS', 'datetime');
        foreach ($data as &$row)
          foreach ($schema as $column => $type)
            $row->$column = date_sql_to_usa($row->$column, TRUE);

        // also process STATUS_HISTORY
        if (! empty($row->STATUS_HISTORY))
        {
          $pairs = explode(';', $row->STATUS_HISTORY); // split into 'ACTION|DATE' pairs
          foreach ($pairs as &$pair)
          {
            $elements = explode('|', $pair);
            if (count($elements) == 2)
            {
              list($command, $date) = $elements;
              $date = date_sql_to_usa($date, TRUE);
              $pair = "$command|$date";
            }
            else
              dlog('', "WARNING: invalid data '$pair'");
          }
          $row->STATUS_HISTORY = implode(';', $pairs); // re-compose into original format
        }

        $this->addToOutput('records', $data);
        $this->addToOutput('record_count', $count);
      }

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

