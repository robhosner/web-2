<?php

namespace Ultra\Lib\Api\Partner\MvneCare;

require_once 'Ultra/Lib/Api/Partner/MvneCare.php';

class ListCustomerInvocations extends \Ultra\Lib\Api\Partner\MvneCare
{
  /**
   * mvnecare__ListCustomerInvocations
   *
   * return invocation records for a specific customer within given date rage
   * http://wiki.hometowntelecom.com:8090/display/SPEC/MVNEcare+APIs
   *
   * @return Result object
   */
  public function mvnecare__ListCustomerInvocations()
  {
    // init
    list ($customer_id, $date_from, $date_to) = $this->getInputValues();
    $this->addToOutput('records', array());
    $this->addToOutput('record_count', 0);

    try
    {
      // verify that date range does not exceed 30 days
      $range = abs(strtotime($date_to) - strtotime($date_from));
      if ($range / 60 / 60 / 24 > 90) // change to 30 when AMDOCS-267 is fixed
        $this->errException('ERR_API_INVALID_ARGUMENTS: date range is greater than 30 days', 'VV0033');

      // prepare dates
      $date_from = date_to_datetime($date_from);
      $date_to = date_to_datetime($date_to, TRUE);

      // connect to DB
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');

      // get data
      $params = array('CUSTOMER_ID' => $customer_id, 'START_DATE_TIME' => array($date_from, $date_to));
      $data = get_ultra_acc_command_invocations($params, NULL, 'START_DATE_TIME DESC'); // AMDOCS-374
      $count = count($data);

      // check results and add to response
      if ($count)
      {
        // AMDOCS-374: post-process all dates
        $schema = \Ultra\Lib\DB\getTableSchema('COMMAND_INVOCATIONS', 'datetime');
        foreach ($data as &$row)
        {
          // process all schema datetime data types
          foreach ($schema as $column => $type)
            $row->$column = date_sql_to_usa($row->$column, TRUE);
          
          // also process STATUS_HISTORY
          if (! empty($row->STATUS_HISTORY))
          {
            $pairs = explode(';', $row->STATUS_HISTORY); // split into 'ACTION|DATE' pairs
            foreach ($pairs as &$pair)
            {
              $elements = explode('|', $pair);
              if (count($elements) >= 2)
              {
                list($command, $date) = $elements;
                $date = date_sql_to_usa($date, TRUE);
                $pair = "$command|$date";
              }
              else
                dlog('', "WARNING: invalid data '$pair'");
            }
            $row->STATUS_HISTORY = implode(';', $pairs); // re-compose into original format
          }
        }

        $this->addToOutput('records', $data);
        $this->addToOutput('record_count', $count);
      }

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
