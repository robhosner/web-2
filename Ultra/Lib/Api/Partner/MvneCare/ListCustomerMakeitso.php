<?php

namespace Ultra\Lib\Api\Partner\MvneCare;

require_once 'Ultra/Lib/Api/Partner/MvneCare.php';

class ListCustomerMakeitso extends \Ultra\Lib\Api\Partner\MvneCare
{
  const MAX_MAKEITSO_RECORDS = 1000; // max mumber of records to return from MAKEITSO_QUEUE
  
  /**
   * mvnecare__ListCustomerMakeitso
   *
   * return Makeitso records for a specific customer within given date rage
   *
   * @return Result object
   */
  public function mvnecare__ListCustomerMakeitso()
  {
    // init
    list ($customer_id, $date_from, $date_to) = $this->getInputValues();
    $this->addToOutput('records', array());
    $this->addToOutput('record_count', 0);

    try
    {
      // prepare dates
      $date_from = date_to_datetime($date_from);
      $date_to = date_to_datetime($date_to, TRUE);

      // connect to DB
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');

      // get data
      $params = array('CUSTOMER_ID' => $customer_id, 'CREATED_DATE_TIME' => array($date_from, $date_to));
      $params['order_by'] = 'CREATED_DATE_TIME DESC'; // AMDOCS-374
      $data = get_makeitso($params, self::MAX_MAKEITSO_RECORDS);
      $count = count($data);

      // check results and add to response
      if ($count)
      {
        // AMDOCS-374: post-process dates
        $schema = \Ultra\Lib\DB\getTableSchema('MAKEITSO_QUEUE', 'datetime');
        foreach ($data as &$row)
        {
          foreach ($schema as $column => $type)
            $row->$column = date_sql_to_usa($row->$column, TRUE);

          // also process ATTEMPT_HISTORY
          if (! empty($row->ATTEMPT_HISTORY))
          {
            $pairs = explode(';', $row->ATTEMPT_HISTORY); // split into 'ACTION|DATE' pairs
            foreach ($pairs as &$pair)
            {
              $elements = explode('|', $pair);
              if (count($elements) == 2)
              {
                list($command, $date) = $elements;
                $date = date_sql_to_usa($date, TRUE);
                $pair = "$command|$date";
              }
              else
                dlog('', "WARNING: invalid data '$pair'");
            }
            $row->ATTEMPT_HISTORY = implode(';', $pairs); // re-compose into original format
          }
        }

        $this->addToOutput('records', $data);
        $this->addToOutput('record_count', $count);
      }

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
