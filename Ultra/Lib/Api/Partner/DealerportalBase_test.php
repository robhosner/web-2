<?php

include_once 'Ultra/Lib/Api.php';
require_once('Ultra/tests/API/TestConstants.php');

class DealerportalBaseTest extends \Ultra\Lib\Api\Partner\DealerportalBase
{
  public function test()
  {
    // Dealer Owner
    $celluphoneSessionData = $this->getCelluphoneSessionData( 39 );
    print_r($celluphoneSessionData);

    // Master Owner
    $celluphoneSessionData = $this->getCelluphoneSessionData( 10 );
    print_r($celluphoneSessionData);

    // Sub-distributor All Access
    $celluphoneSessionData = $this->getCelluphoneSessionData( 27 );
    print_r($celluphoneSessionData);
  }
}

function main()
{
  teldata_change_db();

  $dealerportalBaseTest = new DealerportalBaseTest( 'dummy' );

  $dealerportalBaseTest->test();
}

main();

?>
