<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class Crone extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__Crone
   *
   * Cron runner API
   *
   * @param string mode
   * @return Result object
   */
  public function internal__Crone()
  {
    list ($mode) = $this->getInputValues();

    try
    {
      $result = crone_run($mode);

      if (count($result['errors']) > 0)
      {
        $this->errException('ERR_API_INTERNAL: ' . $result['errors'][0], 'IN0002');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
