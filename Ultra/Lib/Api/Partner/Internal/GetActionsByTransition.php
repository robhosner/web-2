<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetActionsByTransition extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetActionsByTransition
   *
   * Gets all actions belonging to a transition
   *
   * @param string transition_uuid
   * @return Result object
   */
  public function internal__GetActionsByTransition()
  {
    list ($transition_uuid) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $htt_action_log_select_query = htt_action_log_select_query(
        array( 'transition_uuid' => $transition_uuid )
      );

      $htt_action_log_data = mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));

      if ( ! ( $htt_action_log_data && is_array($htt_action_log_data) && count($htt_action_log_data) ) )
      {
      // try to query HTT_ACTION_ARCHIVE instead

        $htt_action_log_select_query = htt_action_log_select_query(
          array( 'transition_uuid' => $transition_uuid , 'archive' => TRUE )
        );

        $htt_action_log_data = mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));
      }

      if ( $htt_action_log_data && is_array($htt_action_log_data) && count($htt_action_log_data) )
      {
        foreach( $htt_action_log_data as $id => $data )
        {
          $actions[] = $data->ACTION_UUID;
          $actions[] = $data->ACTION_SEQ;
          $actions[] = $data->STATUS;
          $actions[] = $data->ACTION_TYPE;
          $actions[] = $data->ACTION_NAME;
          $actions[] = $data->ACTION_RESULT;
        }
      }
      else
      {
        $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');;
      }

      $this->addToOutput('actions', $actions);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>