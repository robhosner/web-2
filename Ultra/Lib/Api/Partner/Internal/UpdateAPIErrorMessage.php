<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class UpdateAPIErrorMessage extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__UpdateAPIErrorMessage
   *
   * Modify a single ULTRA.USER_ERROR_MESSAGES row
   *
   * @param string error_code
   * @param string en_message
   * @param string es_message
   * @return object Result
   */
  public function internal__UpdateAPIErrorMessage()
  {
    list ($error_code, $es_message, $zh_message) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $messages = array();

      if (!$es_message && !$zh_message)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Parameter es_message or zh_message must be provided', 'IN0002');

      if (!is_null($es_message) && $es_message)
        $messages['es_message'] = $es_message;

      if (!is_null($zh_message) && $zh_message)
        $messages['zh_message'] = $zh_message;

      $result = update_ultra_user_error_message($error_code, $messages);

      if (!$result['success'] && count($result['errors']))
        $this->errException($result['errors'][0], 'DB0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
