<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RestoreCancelledPortedCustomer extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RestoreCancelledPortedCustomer
   *
   * Returns rows from ULTRA.USER_ERROR_MESSAGES
   *
   * @param string error_code
   * @param string error_string
   * @return object Result
   */
  public function internal__RestoreCancelledPortedCustomer()
  {
    list ($customer_id, $msisdn, $iccid, $dry_run) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id( $customer_id, array('BRAND_ID') );
      if (!$customer)
          $this->errException('no customer found', 'VV0031', 'no customer found');

      $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer_id, 'plan_state', 0);

      if (!$plan_state)
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: Customer does not exist', 'VV0031');
      }

      if ($plan_state != STATE_CANCELLED)
      {
        $this->errException('ERR_API_INTERNAL: Customer is in an invalid state for this command. (1)', 'IN0001');
      }

      $customers = get_ultra_customers_from_msisdn($msisdn, array('CUSTOMER_ID', 'BRAND_ID'));
      foreach ($customers as $customerToCheck)
      {
        if ($customerToCheck->CUSTOMER_ID != $customer_id)
        {
          $this->errException('ERR_API_INVALID_ARGUMENTS: MSISDN already assigned to another customer', 'IN0002');
        }
      }

      $customers = get_ultra_customers_from_iccid($iccid, array('CUSTOMER_ID', 'BRAND_ID'));
      foreach ($customers as $customerToCheck)
      {
        if ($customerToCheck->CUSTOMER_ID != $customer_id)
        {
          $this->errException('ERR_API_INVALID_ARGUMENTS: ICCID already assigned to another customer', 'IC0002');
        }
      }

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwQuerySubscriber(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn' => $msisdn,
          'iccid'  => $iccid
        )
      );

      $errors = $result->data_array['errors'];
      if (count($errors))
      {
        $this->errException('ERR_API_INTERNAL: MW error ' . $errors[0], 'MW0001');
      }
      else
      {
        if ( ! in_array($result->data_array['body']->SubscriberStatus, array(STATE_ACTIVE, STATE_SUSPENDED)))
        {
          $this->errException('ERR_API_INTERNAL: Customer is in an invalid state for this command.', 'IN0001');
        }

        $result = htt_transition_log_to_cos_id_from_customer_id($customer_id);

        if ($result)
        {
          if ( !!$dry_run && !is_null($dry_run) )
          {
            $this->succeed();
            return $this->result;
          }

          $sql = accounts_update_query(array(
            'cos_id'      => $result->to_cos_id,
            'customer_id' => $customer_id
          ));

          if ( ! is_mssql_successful(logged_mssql_query($sql)) )
          {
            $this->errException('ERR_API_INTERNAL: Error updating table : accounts', 'DB0001');
          }

          $sql = htt_customers_overlay_ultra_update_query(array(
            'iccid_current'         => $iccid,
            'current_mobile_number' => $msisdn,
            'plan_state'            => STATE_PORT_IN_REQUESTED,
            'customer_id'           => $customer_id
          ));

          if ( ! is_mssql_successful(logged_mssql_query($sql)) )
          {
            $this->errException('ERR_API_INTERNAL: Error updating table : htt_customers_overlay_ultra', 'DB0001');
          }

          // verify changes
          $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer_id, 'plan_state', 0);

          if ($plan_state != STATE_PORT_IN_REQUESTED)
          {
            $this->errException('ERR_API_INVALID_ARGUMENTS: Customer is in an invalid state for this command. (2)', 'IN0001');
          }
          else
          {
            $changeResult = change_state(array('customer_id' => $customer_id), TRUE, 'Port Provisioned', 'take transition', FALSE, 1);
            if (count($changeResult['errors']))
            {
              $this->errException("ERR_API_INTERNAL: {$changeResult['errors'][0]}", 'IN0002');
            }
          }
        }
        else
          $this->errException("ERR_API_INVALID_ARGUMENTS: cos_id not found for customer_id, $customer_id, transition log", 'IN0002');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
