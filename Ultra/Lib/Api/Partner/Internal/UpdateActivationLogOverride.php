<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class UpdateActivationLogOverride extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__UpdateActivationLogOverride
   * update relevant values that belong to the given ICCID
   * @see MVNO-2932
   * @author VYT, 2014-12-04
   */
  public function internal__UpdateActivationLogOverride()
  {
    // initialize
    list ($iccid, $override_dealer, $override_notes) = $this->getInputValues();
    $iccid = luhnenize($iccid);

    try
    {
      $override_notes = mssql_escape_raw($override_notes);

      // verify ICCID in SIM inventory record
      teldata_change_db();
      if ( ! $sql = htt_inventory_sim_join_ultra_celluphone_channel_query($iccid))
        $this->errException('ERR_API_INTERNAL: an unexpected database error has occurred (1)', 'DB0001');
      $inventory = mssql_fetch_all_objects(logged_mssql_query($sql));
      if (count($inventory) != 1)
        $this->errException('ERR_API_INTERNAL: the given ICCID does not exist in our database', 'DB0001');

      // update ULTRA.ACTIVATION_LOG_OVERRIDE
      if ($override_dealer)
      {
        if ( ! $info = \Ultra\Lib\DB\Celluphone\getDealerInfo($override_dealer))
          $this->errException('ERR_API_INVALID_ARGUMENTS: No celluphone dealer info found', 'ND0003');

        if (! $user = empty($_SERVER['PHP_AUTH_USER']) ? NULL : $_SERVER['PHP_AUTH_USER'])
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid login information', 'SE0007');

        if ( ! save_activation_log_override($iccid, 1, $info->masterId, $info->distributorId, $info->dealerId, NULL, $user, get_utc_date(), $override_notes))
          $this->errException('An unexpected database error has occurred', 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
