<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class TestOutput extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__TestOutput
   *
   * Test Ultra API Validation.
   *
   * @return Result object
   */
  public function internal__TestOutput ()
  {
    $this->succeed ();

    $this->addToOutput('test_integer',    1234567890);
    $this->addToOutput('test_string',     '1234567890');
    $this->addToOutput('test_truncate1',  '1234567890');
    $this->addToOutput('test_truncate2',  '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890');

    return $this->result;
  }
}

