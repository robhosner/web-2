<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetPromoBroadcastInfo extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__GetPromoBroadcastInfo
   * 
   * returns promo broadcast campaign info for given broadcast campaign id
   * 
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+APIs
   * @return Result object
   */
  public function internal__GetPromoBroadcastInfo()
  {
    // initialize
    list ($promo_broadcast_campaign_id) = $this->getInputValues();
    $campaign = NULL;

    try
    {
      // get campaign list
      teldata_change_db();
      $broadcast = new \PromoBroadcast();
      $result = $broadcast->loadCampaigns(array('promo_broadcast_campaign_id' => array($promo_broadcast_campaign_id)));

      if ( $result->is_failure() || ! count($result->data_array))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid campaign', 'ND0001');

      $campaign = $result->data_array[0];

      $message_count = get_ultra_promo_broadcast_log_success_count($promo_broadcast_campaign_id);

      $campaign->SENT       = ( ! empty($message_count['SENT']))  ? $message_count['SENT']  : 0;
      $campaign->SENT_ERROR = ( ! empty($message_count['ERROR'])) ? $message_count['ERROR'] : 0;

      $campaign->REDEMPTION_SUCCESS = 0;
      $campaign->REDEMPTION_ERROR   = 0;

      if ( ! empty($campaign->PROMO_SHORTCODE))
      {
        $redemption_count = get_ultra_promo_broadcast_attempt_log_success_count($campaign->PROMO_SHORTCODE);

        $campaign->REDEMPTION_SUCCESS = ( ! empty($redemption_count['SUCCESS'])) ? $redemption_count['SUCCESS'] : 0;
        $campaign->REDEMPTION_ERROR   = ( ! empty($redemption_count['ERROR']))   ? $redemption_count['ERROR']   : 0;
      }

      $ouptut = array();
      foreach ($campaign as $key => $val)
        $output[strtolower($key)] = $val;

      $this->addArrayToOutput($output);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
