<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetCustomerAggregatedSoapLog extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetCustomerAggregatedSoapLog
   *
   * query ULTRA_ACC..SOAP_LOG and return an aggregated record set
   *
   * @return Result object
   */
  public function internal__GetCustomerAggregatedSoapLog()
  {
    // collect input
    list ($date_from, $date_to, $iccid, $msisdn) = $this->getInputValues();

    // initialize output
    $this->addToOutput('records', array());

    try
    {
      // fail if both ICCID and MSISDN are not given
      if (! $iccid && ! $msisdn)
        $this->errException('ERR_API_INTERNAL: missing ICCID or MSISDN parameter', 'NS0001');

      // fail is dates are more than MAX_SOAP_LOG_RANGE apart
      if (abs(strtotime($date_to) - strtotime($date_from)) > self::MAX_SOAP_LOG_RANGE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: date range is too large', 'VV0033');

      // convert 'DD-MM-YYYY' to 'Mon DD YYYY'
      $date_from = date_to_datetime($date_from);
      $date_to = date_to_datetime($date_to, TRUE);
      if (empty($date_from) || empty($date_to))
        $this->errException('ERR_API_INVALID_ARGUMENTS: failed to convert input dates', 'VV0033');

      // get records
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $records = get_soap_log_aggregated($iccid, $msisdn, array($date_from, $date_to));

      // add to output
      if ($records && is_array($records) && count($records))
      {
        // AMDOCS-343: remove all GetNetworkDetails CheckBalance QuerySubscriber commands EXCEPT for the most recent ones
        $commands = array(
          'GetNetworkDetails'   =>  NULL,
          'CheckBalance'        =>  NULL,
          'QuerySubscriber'     =>  NULL);

        // loop through all commands (in reverse for efficiency since records are return in SOAP_LOG ascending order) and drop old $commands
        for ($i = count($records) - 1; $i >= 0; $i--)
        {
          if (array_key_exists($records[$i]->command, $commands)) // found command in question
          {
            if ($commands[$records[$i]->command]) // already saved this command
            {
              if (strtotime($records[$commands[$records[$i]->command]]->began) < strtotime($records[$i]->began)) // saved command is older
              {
                // remove saved command from array and save the index of the current one
                unset($records[$commands[$records[$i]->command]]);
                $commands[$records[$i]->command] = $i;
              } 
              else // current command is older
                unset($records[$i]);
            }
            else // no previously saved command: save the current one
              $commands[$records[$i]->command] = $i;
          }
        }

        // AMDOCS-392: convert dates
        $schema = array('began', 'sync_request_time', 'sync_response_time', 'async_request_time');
        foreach ($records as &$record)
          foreach($schema as $name)
            $record->$name = date_sql_to_usa($record->$name, TRUE);

        // reindex array and add to output
        $this->addToOutput('records', array_values($records));
      }

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }
}

?>
