<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class QuerySubscriber extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__QuerySubscriber
   *
   * ACC adapter command for QuerySubscriber; used by BARK tool
   *
   * @return Result object
   */
  public function internal__QuerySubscriber()
  {
    list ($iccid, $msisdn) = $this->getInputValues();
    $this->addToOutput('customer_id', NULL);
    $this->addToOutput('ICCID', NULL);
    $this->addToOutput('MSISDN', NULL);
    $this->addToOutput('XML', NULL);
    $customer = NULL;

    try
    {
      // check input and init
      if (! $iccid && !$msisdn)
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN', 'NS0001');
      $iccid = $iccid ? luhnenize($iccid) : NULL;
      $msisdn = $msisdn ? normalize_msisdn($msisdn, FALSE) : NULL;

      // get customer record and check result
      teldata_change_db();
      $clause = ($iccid ? array('CURRENT_ICCID_FULL' => $iccid) : array('CURRENT_MOBILE_NUMBER' => $msisdn));
      $query = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 2,
        array('CUSTOMER_ID', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'MVNE'), $clause);
      $customers = mssql_fetch_all_objects(logged_mssql_query($query));
      if (count($customers) == 1)
      {
        $customer = $customers[0];
        $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
      }
      if (count($customers) < 1)
        $this->AddWarning('ERR_API_INVALID_ARGUMENTS: the customer does not exist');
      if (count($customers) > 1)
        $this->addWarning('ERR_API_INVALID_ARGUMENTS: found multiple customers');

      // verify MVNE
      if ($customer && $customer->MVNE != self::MVNE2)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer MVNE', 'MV0001');

      // call middleware
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $params = array('actionUUID' => $this->getRequestId());
      if ($iccid)
        $params['iccid'] = $iccid;
      else
        $params['msisdn'] = $msisdn;
      $result = $mwControl->mwQuerySubscriber($params);

      // check results
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');
      if (! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100')
        $this->errException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');
      if ( ! $result->data_array['success'] || empty($result->data_array['body']))
        $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');

      // extract values if present: some may be missing depending on subscriber state
      $properties = array('ICCID' => 'SIM', 'MSISDN', 'wholesalePlan', 'AddOnFeatureInfoList', 'PlanEffectiveDate', 'AutoRenewalIndicator',
        'PlanId', 'SubscriberStatus', 'PlanExpirationDate');
      foreach ($properties as $name => $property)
        $this->addToOutput(is_numeric($name) ? $property : $name,
          empty($result->data_array['body']->$property) ? NULL : $result->data_array['body']->$property);

      // get XML
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $log = get_soap_log(array('session_id' => $result->data_array['body']->serviceTransactionId));
      if (count($log))
        $this->addToOutput('XML', $log[0]->xml);

      // we are done
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }

}

?>
