<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ListPromoBroadcast extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__ListPromoBroadcast
   * return a list of broadcast SMS campaigns
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+Framework
   * @return Result object
   * @author: VYT, 2014-09-22
   */
  public function internal__ListPromoBroadcast()
  {
    // initialize
    list ($status, $created_days_ago) = $this->getInputValues();
    $campaigns = array();

    try
    {
      // get campaign list
      teldata_change_db();
      $broadcast = new \PromoBroadcast();
      $result = $broadcast->loadCampaigns(array(
        'status' => $status,
        'created_days_ago' => $created_days_ago
      ));
      if ($result->is_success())
        $campaigns = $result->data_array;
        
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('campaigns', $campaigns);
    $this->addToOutput('count', count($campaigns));
    return $this->result;
  }
}

?>
