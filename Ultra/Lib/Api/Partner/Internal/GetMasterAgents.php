<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetMasterAgents extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetMasterAgents
   * return a list of parent master agents without children
   * @see MVNO-2497
   * @return Result object
   * @author: VYT, 2014-08-05
   */
  public function internal__GetMasterAgents()
  {
    $master_agents = array();
    $count = 0;

    try
    {
      teldata_change_db();
      $master_agents = get_all_celluphone_masters();
      $count = count($master_agents);
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('master_agents', $master_agents);
    $this->addToOutput('count', $count);
    return $this->result;
  }
}

?>
