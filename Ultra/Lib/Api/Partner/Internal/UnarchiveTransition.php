<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class UnarchiveTransition extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__UnarchiveTransition
   *
   * Unarchive a Transition.
   *
   * @return Result object
   */
  public function internal__UnarchiveTransition ()
  {
    list ( $transition_uuid ) = $this->getInputValues();

    $error_code = '';

    try
    {
      require_once 'Ultra/Lib/DB/TransitionLog/Archive.php';

      $transition_uuid = fix_transition_uuid_format( $transition_uuid );

      $error = \Ultra\Lib\DB\TransitionLog\Archive\un_archive( $transition_uuid ) ;

      if ( $error )
        $this->errException( $error , 'IN0001' , $error );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>
