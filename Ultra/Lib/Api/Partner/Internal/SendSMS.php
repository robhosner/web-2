<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Messaging\Messenger;

class SendSMS extends Internal
{
  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * SendApnSettings constructor.
   * @param Messenger $messenger
   * @param CustomerRepository $customerRepository
   */
  public function __construct(Messenger $messenger, CustomerRepository $customerRepository)
  {
    $this->messenger = $messenger;
    $this->customerRepository = $customerRepository;
  }

  // template parameters not supported
  public function internal__SendSMS()
  {
    list($customer_id, $template_name, $msisdn) = $this->getInputValues();

    try {
      $customer = false;

      if (empty($customer_id) && empty($msisdn)) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: missing customer_id or msisdn', 'MP0001', 'Missing customer_id or msisdn.');
      }

      if (!empty($customer_id)) {
        $customer = $this->customerRepository->getCustomerById($customer_id, []);
      }

      if (empty($customer_id) && !empty($msisdn)) {
        $customer = $this->customerRepository->getCustomerFromMsisdn($msisdn, ['c.CUSTOMER_ID']);
      }

      if (!$customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031');
      }

      $smsResult = $this->messenger->enqueueImmediateSms($customer->CUSTOMER_ID, $template_name, []);

      if ($smsResult->is_failure()) {
        return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'SM0002');
      }

      $this->succeed();
    }
    catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}