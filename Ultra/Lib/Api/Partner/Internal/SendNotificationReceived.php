<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

class SendNotificationReceived extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__SendNotificationReceived
   *
   * sends a NotificationReceived message to requested MW test environment
   *
   * @return Result object
   */
  
  public function internal__SendNotificationReceived()
  {
    list($env, $msisdn, $service_grade, $short_code, $message_type, $sms_text) = $this->getInputValues();

    try
    {
      // hard default
      // $service_grade = 'SG801';
      $message_type  = 'ASCII';

      // get correct host to send to
      $endpoint = null;
      switch ($env)
      {
        case 'DEV':
          $endpoint = \Ultra\UltraConfig\getDevMiddlewareEndpoint();
          break;
        case 'QA':
          $endpoint = \Ultra\UltraConfig\getQAMiddlewareEndpoint();
          break;
      }

      if ( ! $endpoint)
        $this->errException('ERR_API_INVALID_ARGUMENTS: env parameter is invalid', 'IN0002');

      // get correct WSDL to use
      $wsdl = \Ultra\UltraConfig\acc_notification_wsdl();
      $wsdl = ($wsdl) ? 'runners/amdocs/' . $wsdl : NULL;

      // format notification data
      $timeStamp = date('Y-m-d') . 'T' . date('H:i:s');
      $data = array(
        'UserData'     => array("senderId" => "MVNEACC", "timeStamp" => $timeStamp),
        'MSISDN'       => $msisdn,
        'serviceGrade' => $service_grade,
        'shortCode'    => $short_code,
        'messageType'  => $message_type,
        'smsText'      => $sms_text
      );

      \logDebug('DATA TO SEND: ' . json_encode($data));
      \logDebug('USING WSDL: ' . $wsdl . ' / and ENDPOINT: ' . $endpoint);

      // make soap request
      $result = \Ultra\Lib\MiddleWare\ACC\Notification::divertNotification(
        $wsdl,
        $endpoint,
        'NotificationReceived',
        $data
      );

      \logDebug('RESULT: ' . $result);

      if ($result->is_success())
        $this->succeed();
      else
        $this->errException('ERR_API_INTERNAL: error sending notification to MW endpoint', 'MW0001');
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
