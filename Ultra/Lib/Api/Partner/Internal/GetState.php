<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetState extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetState
   *
   * Get customer state
   *
   * @param int customer_id
   * @return Result object
   */
  public function internal__GetState()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_state'));
      $account  = get_account_from_customer_id($customer_id, array('cos_id'));

      if ( ! empty($customer->plan_state) && ! empty($account->cos_id))
      {
        $plan_name       = get_plan_name_from_short_name(get_monthly_plan_from_cos_id($account->cos_id));
        $plan_desc_name  = cos_id_plan_description($account->cos_id);
        $state_name      = $customer->plan_state;
        $state_desc_name = $plan_desc_name . '/' . $state_name;

        if (empty($plan_name))
          $this->errException('ERR_API_INTERNAL: plan_name is empty', 'IN0001');

        if (empty($plan_desc_name))
          $this->errException('ERR_API_INTERNAL: plan_desc_name is empty', 'IN0001');

        if ($plan_name == PLAN_STANDBY && $state_name == STATE_ACTIVE)
          $this->errException('ERR_API_INTERNAL: plan_name/state_name is STANDBY/ACTIVE', 'IN0001');
      }
      else
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');
      }

      $this->addToOutput('state_valid',   ! empty($state_desc_name));
      $this->addToOutput('plan_name',       empty($plan_name)       ? NULL : $plan_name);
      $this->addToOutput('plan_desc_name',  empty($plan_desc_name)  ? NULL : $plan_desc_name);
      $this->addToOutput('state_name',      empty($state_name)      ? NULL : $state_name);
      $this->addToOutput('state_desc_name', empty($state_desc_name) ? NULL : $state_desc_name);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
