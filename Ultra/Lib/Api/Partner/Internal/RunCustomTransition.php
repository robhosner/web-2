<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'db.php';
require_once 'Ultra/Lib/StateMachine.php';
#require_once 'Primo/Lib/StateMachine/DataEngineMongoDB.php';
require_once 'Ultra/Lib/StateMachine/DataEngineMSSQL.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRAM.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRedis.php';

# Ultra specific actions and requirements
require_once 'Ultra/Lib/StateMachine/Action/functions.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';

class RunCustomTransition extends \Ultra\Lib\Api\Partner\Internal
{
  public function internal__RunCustomTransition()
  {
    list($transition, $label, $context) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $context    = json_decode($context);
      $transition = json_decode($transition);

      // check for open transitions
      $active = get_by_column('htt_transition_log',
        array('customer_id' => $context->customer_id),
        " AND status <> 'ABORTED' AND status <> 'CLOSED'
          AND to_cos_id IS NOT NULL
          AND to_plan_state IS NOT NULL");

      if ($active != NULL)
      {
        $error = 'Customer has active state change' . $active->TRANSITION_UUID;
        $this->errException($error, 'IN0002', $error);
      }

      $redis = new \Ultra\Lib\Util\Redis();
      $dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;

      $sm = new \Ultra\Lib\StateMachine( $dataEngineMSSQLObject , NULL , NULL , $redis );

      $sm->injectConfiguration($transition);

      $customer = (array)get_customer_from_customer_id($context->customer_id);
      $sm->setCustomerData($customer);

      // set transition configuration
      $result = $sm->selectTransitionByLabel(
        $label,
        $customer['plan_state'],
        get_plan_name_from_short_name(get_plan_from_cos_id($customer['cos_id']))
      );

      if ( ! $result )
        $this->errException('Failed to match transition', 'IN0002', 'Failed to match transition');

      $genTransitionResult   = $sm->generateTransitionData((array)$context);
      $currentTransitionData = $sm->getCurrentTransitionData();

      $runTransitionResult = $sm->runTransition($reserve = TRUE);
      if ( ! $runTransitionResult)
      {
        $error = 'Failed running transition' . $currentTransitionData['transition_uuid'];
        $this->errException($error, 'IN0002', $error);
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
