<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetEnvironmentStage extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetEnvironmentStage
   *
   * outputs stage of environment
   *
   * @return Result object
   */
  public function internal__GetEnvironmentStage ()
  {
    $this->addToOutput('stage',   \Ultra\UltraConfig\getEnvironment());

    $this->succeed();

    return $this->result;
  }
}
