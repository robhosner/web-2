<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class UndoActivationLogOverride extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__UndoActivationLogOverride
   * removes a record from ULTRA.ACTIVATION_LOG_OVERRIDE record by ICCID
   * @see PROD-1485
   */
  public function internal__UndoActivationLogOverride()
  {
    // initialize
    list ($iccid) = $this->getInputValues();
    $iccid = luhnenize($iccid);

    try
    {
      teldata_change_db();

      if ( ! undo_activation_log_override($iccid))
        $this->errException('An unexpected database error has occurred', 'DB0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
