<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RenamePromoBroadcast extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RenamePromoBroadcast
   *
   * Rename a promo broadcast campaign
   *
   * @param  int    $campaign_id
   * @param  string $new_name
   * @return Result object
   */
  public function internal__RenamePromoBroadcast()
  {
    list ($campaign_id, $new_name) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $result = update_ultra_promo_broadcast_campaign(array(
        'promo_broadcast_campaign_id' => $campaign_id,
        'name' => $new_name
      ));

      $errors = $result->get_errors();

      if (count($errors) > 0)
      {
        $this->errException('ERR_API_INTERNAL: ' . $errors[0], 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
