<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetCancellationReasons extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetCancellationReasons
   *
   * Get cancellation reasons
   *
   * @param  integer $customer_id
   * @return Result  object
   */
  public function internal__GetCancellationReasons()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $result = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID'));
      if ( ! $result )
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');

      $this->addToOutput('cancellation_reasons', get_cancellations_by_customer_id($customer_id));
      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
