<?php

namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Lib\Util\Redis;
use Ultra\Lib\Api\Partner\Internal;

/**
 * Class CleanMiddlewareRedisQueues
 * @package Ultra\Lib\Api\Partner\Internal
 */
class CleanMiddlewareRedisQueues extends Internal
{
  /**
   * @var array
   */
  private $allowedOperationModes = array('read', 'delete');
  /**
   * @var string
   */
  private $cachedFileName = '/tmp/tempRedisQueueData.php';
  /**
   * @var string
   */
  private $operationMode;
  /**
   * @var Redis
   */
  protected $redis;
  /**
   * @var array
   */
  private $redisQueues = array(
    'SMS_MW/OUTBOUND/SYNCH',
    'ULTRA_MW/INBOUND/SYNCH',
    'ULTRA_MW/OUTBOUND/SYNCH',
    'ACC_MW/INBOUND/SYNCH',
    'ACC_MW/OUTBOUND/SYNCH',
    'ULTRA_MW/INBOUND/SYNCH',
    'ACCMW/INBOUND/NOTIFICATION',
    'ULTRAMW/INBOUND/NOTIFICATION'
  );

  /**
   * internal__CleanMiddlewareQueues
   *
   * Clean the middleware queues in redis.
   *
   * @param string mode
   * @return Result object
   */
  public function internal__CleanMiddlewareRedisQueues()
  {
    list ($mode) = $this->getInputValues();

    $this->addToOutput('timeRemaining', null);
    $this->addToOutput('timeRemainingAllowsToDelete', null);

    try
    {
      $this->init($mode);

      switch ($this->operationMode)
      {
        case 'read':
          $this->readRedisQueueData();
          break;
        case 'delete':
          $this->deleteRedisQueueData();
          break;
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Init class values.
   *
   * @param $mode
   * @throws \Exception
   */
  private function init($mode)
  {
    $mode = strtolower($mode);

    if (!in_array($mode, $this->allowedOperationModes))
    {
      $this->errException('ERR_API_INTERNAL: THIS IS NOT A VALID OPERATION', 'IN0002');
    }

    $this->operationMode = $mode;
    $this->redis = $this->getRedis();
  }

  /**
   * Get Redis queue data for each queue.
   *
   * @return array
   */
  private function readRedisQueueData()
  {
    return $this->getRedisQueueData();
  }


  /**
   * Delete redis queue data based on a list intersection after 90s.
   *
   * @return bool
   */
  private function deleteRedisQueueData()
  {
    $tempStoredData = $this->getTempStoredQueueData();
    $timeToWait = 90;

    if (!is_array($tempStoredData))
    {
      $this->addWarning('There are no queues to be cleaned up.');

      return false;
    }

    $this->addToOutput('timeRemaining', ($tempStoredData['date'] + $timeToWait) - time());

    if (time() > ($tempStoredData['date'] + $timeToWait))
    {
      $this->deleteQueueValuesBasedOnUnderscoreIntersection($tempStoredData);
      $this->addToOutput('timeRemainingAllowsToDelete', 'true');

      return true;
    }
    else
    {
      $this->addToOutput('timeRemainingAllowsToDelete', 'false');

      return false;
    }
  }

  /**
   * Temporarily store queue data to file.
   *
   * @param $data
   * @throws \Exception
   */
  private function storeTempQueueData($data)
  {
    $storedFile = file_put_contents($this->cachedFileName, json_encode(array('date' => time(), 'queueData' => $data)));

    if (!$storedFile)
    {
      throw new \Exception('An issue occurred while trying to save to the file' . $this->cachedFileName);
    }
  }

  /**
   * Get queue data from redis.
   *
   * @return array
   */
  private function getRedisQueueData()
  {
    $queueData = array();

    foreach ($this->redisQueues as $queue)
    {
      $queueData[$queue] = $this->redis->smembers($queue);
    }

    $this->storeTempQueueData($queueData);

    return $queueData;
  }

  /**
   * Get queued data from file.
   *
   * @return mixed
   */
  private function getTempStoredQueueData()
  {
    if (file_exists($this->cachedFileName))
    {
      return json_decode(file_get_contents($this->cachedFileName), true);
    }

    return false;
  }

  /**
   * Delete queue values from redis that intersect with the temporary stored data.
   *
   * @param array $tempStoredData
   */
  private function deleteQueueValuesBasedOnUnderscoreIntersection(array $tempStoredData)
  {
    $underscoreObject = new \__;
    $queueData = $this->getRedisQueueData();

    foreach ($this->redisQueues as $queue)
    {
      $intersection = $underscoreObject->intersection($tempStoredData['queueData'][$queue], $queueData[$queue]);

      foreach($intersection as $value)
      {
        $this->redis->srem($queue, $value);
      }
    }

    unlink($this->cachedFileName);
  }
}
