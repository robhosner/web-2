<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class CancelDeviceLocation extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__CancelDeviceLocation
   *
   * Performs a CancelDeviceLocation to the network.
   *
   * @return Result object
   */
  public function internal__CancelDeviceLocation()
  {
    list ( $ICCID , $MSISDN ) = $this->getInputValues();

    return $this->internal__CancelDeviceLocationMVNE2( $ICCID , $MSISDN );
  }

  /**
   * internal__CancelDeviceLocationMVNE2
   *
   * Performs a CancelDeviceLocation to the MVNE2 network.
   *
   * @return Result object
   */
  private function internal__CancelDeviceLocationMVNE2( $ICCID , $MSISDN )
  {
    $error_code = '';
  
    try
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwCancelDeviceLocation(
        array(
          'actionUUID'         => getNewActionUUID('internal ' . time()),
          'msisdn'             => $MSISDN,
          'iccid'              => $ICCID
        )
      );

      dlog('',"mwCancelDeviceLocation result = %s",$result);

      if ( $result->has_errors() )
      {
        dlog('',"mwCancelDeviceLocation errors : %s",$result->get_errors());
        $error_code = 'MW0001';
        throw new \Exception("mwCancelDeviceLocation middleware error - ".implode(' ; ',$result->get_errors()));
      }
      else
      {
        if ( isset( $result->data_array['errors'] ) && is_array($result->data_array['errors']) && count( $result->data_array['errors'] ) )
        {
          dlog('',"mwCancelDeviceLocation errors : %s",$result->data_array['errors']);
          $error_code = 'MW0001';
          throw new \Exception("mwCancelDeviceLocation - ".implode(' ; ',$result->data_array['errors']));
        }
        elseif ( isset( $result->data_array['errors'] ) && !is_array($result->data_array['errors']) )
        {
          dlog('',"mwCancelDeviceLocation errors : %s",$result->data_array['errors']);
          $error_code = 'MW0001';
          throw new \Exception("mwCancelDeviceLocation error - ".$result->data_array['errors']);
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>
