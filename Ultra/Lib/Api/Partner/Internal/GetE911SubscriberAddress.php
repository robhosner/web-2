<?php

namespace Ultra\Lib\Api\Partner\Internal;

class GetE911SubscriberAddress extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__GetE911SubscriberAddress
   *
   * Retrieve customer E911 Subscriber Address
   *
   * @param  Integer $customer_id
   * @return Result object
   */
  
  public function internal__GetE911SubscriberAddress()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      $address = get_acc_address_e911($customer_id);

      $this->addToOutput('address', $address);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $this->addToOutput('address', $address);

    return $this->result;
  }
}
