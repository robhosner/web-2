<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetAPIErrorMessages extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetAPIErrorMessages
   *
   * Returns rows from ULTRA.USER_ERROR_MESSAGES
   *
   * @param string error_code
   * @param string error_string
   * @return object Result
   */
  public function internal__GetAPIErrorMessages()
  {
    list ($error_code, $error_string) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $params = array();

      if (!is_null($error_code) && $error_code)
        $params['error_code'] = $error_code;

      if (!is_null($error_string) && $error_string)
        $params['en_message'] = $error_string;

      $result = search_ultra_user_error_messages($params, array(
        'ERROR_CODE',
        'EN_MESSAGE',
        'ES_MESSAGE',
        'ZH_MESSAGE',
        'DETAILS'
      ));

      $this->addToOutput('messages', $result);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
