<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Messaging/Templates.php';

class GetAllMessagingTemplates extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__GetAllMessagingTemplates
   *
   * Retrieves all messaging templates
   *
   * @return Result object
   */
  
  public function internal__GetAllMessagingTemplates()
  {
    list ($brand) = $this->getInputValues();

    \logit('Messaging Templates BRAND: ' . $brand);

    try
    {
      $templates = \Ultra\Messaging\Templates\getAllTemplates($brand);
      ksort($templates);
      $this->addToOutput('templates', $templates);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
