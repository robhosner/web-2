<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetRolesActionsApis extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetRolesActionsApis
   *
   * Display the RBAC setup.
   *
   * @return Result object
   */
  public function internal__GetRolesActionsApis ()
  {
    list ( $role_name ) = $this->getInputValues();

    $error_code = '';

    $this->addToOutput('roles_actions_apis','');

    try
    {
      $api_by_role = get_api_by_role( NULL , $role_name, NULL, NULL, TRUE);

      if ( ! $api_by_role )
      {
        $error_code = 'ND0001';
        throw new \Exception("ERR_API_INTERNAL: data not found.");
      }

      // process output to remove duplicate rows and pack plan_state_name into an array
      $result = array();
      $current = -1; // current row index
      foreach($api_by_role as $row)
      {
        if (! empty($result[$current]) && $row->ACTIVITY_ID == $result[$current]->ACTIVITY_ID && $row->API_ID == $result[$current]->API_ID)
        {
          // append to array
          $result[$current]->PLAN_STATE_NAME[] = $row->PLAN_STATE_NAME;
        }
        else
        {
          // create new row
          $result[++$current] = $row;
          $result[$current]->PLAN_STATE_NAME = array(empty($row->PLAN_STATE_NAME) ? 'ANY' : $row->PLAN_STATE_NAME);
        }
      }

      $this->addToOutput('roles_actions_apis', $result);
      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }

}

?>
