<?php

namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Accounts\Interfaces\AccountsRepository;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class SetAccountValues extends \Ultra\Lib\Api\Partner\Internal
{
  private $config;
  private $customerRepository;
  private $accountsRepo;

  public function __construct(
    Configuration $configuration,
    CustomerRepository $customerRepository,
    AccountsRepository $accountsRepo
  ) {
    $this->config = $configuration;
    $this->customerRepository = $customerRepository;
    $this->accountsRepo = $accountsRepo;
  }

  /**
   * internal__SetAccountValues
   *
   * Sets values in the ACCOUNTS table for a user
   *
   * @return Result object
   */
  public function internal__SetAccountValues()
  {
    list ($customer_id, $packaged_balance3) = $this->getInputValues();

    try
    {
      teldata_change_db();

      // get customer data
      $customer = $this->customerRepository->getCustomerById(
        $customer_id,
        [ 'plan_state', 'brand_id' ]
      );
      if (empty($customer))
        $this->errException('Customer not found', 'VV0031');

      // validate that the customer is Active
      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('Customer state is not Active', 'IN0001');

      // validate brand
      if ($customer->brand_id != $this->config->getBrandIdFromBrand('ULTRA'))
        $this->errException('Customer must be Ultra brand', 'FA0004');

      $result = $this->accountsRepo->updateAccount(
        $customer_id,
        [ 'packaged_balance3' => $packaged_balance3 ]
      );
      if ( !$result )
        $this->errException('Failed to update ACCOUNTS table', 'DB0009');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
