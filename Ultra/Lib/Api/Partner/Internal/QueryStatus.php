<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class QueryStatus extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__QueryStatus
   *
   * ACC adapter command for QueryStatus; used by BARK tool
   *
   * @return Result object
   */
  public function internal__QueryStatus()
  {
    list ($msisdn) = $this->getInputValues();

    try
    {
      $msisdn = normalize_msisdn($msisdn);

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwQueryStatus(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn' => $msisdn
        )
      );

      // check results
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');

      if (! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100')
        $this->errException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');

      if (! $result->data_array['success'] || empty($result->data_array['body']))
        $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');

      // extract values if present
      $properties = array(
        'MSISDN',
        'QueryStatusResult',
        'portRequestedTime',
        'portCompletedTime',
        'serviceTransactionId',
        'portDueTime',
        'portSubmittedTime',
        'portStatus',
        'portStatusReason'
      );

      foreach ($properties as $name => $property)
        $this->addToOutput(is_numeric($name) ? $property : $name,
          empty($result->data_array['body']->$property) ? NULL : $result->data_array['body']->$property);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
