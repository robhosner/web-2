<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Customers\SendFamilyReplenishmentSms as FamilyReplenishmentSms;
use Ultra\Lib\Api\Partner\Internal;

class SendFamilyReplenishmentSms extends Internal
{
  /**
   * @var FamilyReplenishmentSms
   */
  private $familyReplenishmentSms;

  /**
   * SendApnSettings constructor.
   * @param FamilyReplenishmentSms $familyReplenishmentSms
   */
  public function __construct(FamilyReplenishmentSms $familyReplenishmentSms)
  {
    $this->familyReplenishmentSms = $familyReplenishmentSms;
  }

  public function internal__SendFamilyReplenishmentSms()
  {
    list($customer_id) = $this->getInputValues();

    try {
      $sentSms = $this->familyReplenishmentSms->sendSms($customer_id);

      if (!$sentSms) {
        return $this->result;
      }

      $this->succeed();
    }
    catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}