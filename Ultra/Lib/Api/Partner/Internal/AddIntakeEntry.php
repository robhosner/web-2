<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class AddIntakeEntry extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__AddIntakeEntry
   *
   * Add a new intake record to a JSON plain text file
   * @see MVNO-3191
   * @author bwalters@ultra.me
   */
  public function internal__AddIntakeEntry()
  {
    try
    {
      // initialize
      list ($submitter, $request_name, $request_description, $start_date, $due_date, $priority, $status, $demand_type, $business_values, $tech_dependency, $launch_communications, $knowlege_transfer, $training) = $this->getInputValues();

      $parameters = array(
        array(
          'name'  => ':submitter',
          'value' => $submitter,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':request_name',
          'value' => $request_name,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':request_description',
          'value' => $request_description,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':start_date',
          'value' => $start_date,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':due_date',
          'value' => $due_date,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':priority',
          'value' => $priority,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':status',
          'value' => $status,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':demand_type',
          'value' => $demand_type,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':business_values',
          'value' => $business_values,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':tech_dependency',
          'value' => $tech_dependency,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':launch_communications',
          'value' => $launch_communications,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':knowlege_transfer',
          'value' => $knowlege_transfer,
          'type'  => \SQLITE3_TEXT
        ),
        array(
          'name'  => ':training',
          'value' => $training,
          'type'  => \SQLITE3_TEXT
        )
      );

      $this->storeJSONIntakeData($parameters);

      // send mail from host
      $message = "
        Request Name: $request_name
        Submitter: $submitter\n
        Start Date: $start_date
        Due Date: $due_date\n
        Request Description:
        $request_description\n
        Status: $status\n
        Demand Type: $demand_type\n
        Business Values: $business_values\n
        Tech Dependency: $tech_dependency\n
        Launch Communications: $launch_communications\n
        Knowledge Transfer: $knowlege_transfer\n
        Training: $training
      ";

      mail(\Ultra\UltraConfig\find_config('intake/email'), 'New Intake Form Entry', $message, "From: Intake\r\n");

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

