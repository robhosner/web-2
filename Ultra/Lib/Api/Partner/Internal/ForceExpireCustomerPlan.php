<?php

namespace Ultra\Lib\Api\Partner\Internal;

use \DateTime;
use \Exception;

/**
 * Class ForceExpireCustomerPlan
 * @package Ultra\Lib\Api\Partner\Internal
 */
class ForceExpireCustomerPlan extends \Ultra\Lib\Api\Partner\Internal
{

  const DATE_FORMAT = 'Y-m-d H:i:s';

  /**
   * internal__ForceExpireCustomerPlan
   *
   * Sets plan_expires for customer_id to 2 days ago
   *
   * @return Result object
   */
  public function internal__ForceExpireCustomerPlan()
  {
    list ($customer_id, $date) = $this->getInputValues();

    try
    {
      teldata_change_db();

      if ( ! \Ultra\UltraConfig\isDevelopmentDB())
        $this->errException('ERR_API_INTERNAL: command disabled', 'AP0001');

      $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_expires'));
      if ( ! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031');

      $this->addToOutput('previous_expiration_date', $customer->plan_expires);

      // set expiration date to 2 days in the past if empty otherwise validate and format date/time
      if (empty($date))
      {
        $formatDate = new DateTime('-2 days');
        $formatDate = $formatDate->format(self::DATE_FORMAT);
      }
      else
      {
        $date = $this->validateDate($date);
        $formatDate = $date instanceof DateTime ? $date->format(self::DATE_FORMAT) : '';
      }

      // set customer expiration date to two days in the past
      $sql = sprintf('UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET PLAN_EXPIRES = "%s" WHERE CUSTOMER_ID = %d', $formatDate, $customer_id);

      if ( ! is_mssql_successful(logged_mssql_query($sql)))
        $this->errException('ERR_API_INTERNAL: DB ERROR updating customer', 'DB0001');

      $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_expires'));

      $this->addToOutput('new_expiration_date', $customer->plan_expires);

      // success
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Validate date from input.
   *
   * @param string $date
   * @throws Exception
   * @return mixed
   */
  private function validateDate($date)
  {
    try
    {
      $date = new DateTime($date);

      // Sometimes mssql returning "Arithmetic overflow occurred" when submitting garbage data, this fixes the issue.
      if ($date->format('Y') < '1970')
      {
        throw new \Exception();
      }

      return $date;
    }
    catch(\Exception $e)
    {
      $this->errException('ERR_API_INVALID_ARGUMENTS: date is not in a proper format e.g., yyyy-mm-dd', 'VV0035');
    }

    return false;
  }
}
