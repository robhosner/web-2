<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RecoverFailedTransition extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RecoverFailedTransition
   *
   * Attempts to recover a Failed Transition.
   *
   * @param string transition_uuid
   * @return Result object
   */
  public function internal__RecoverFailedTransition()
  {
    list ($transition_uuid) = $this->getInputValues();

    try
    {
      teldata_change_db();

      if (strlen($transition_uuid) == 36)
      {
        $transition_uuid = fix_transition_uuid_format($transition_uuid);
      }

      $result = recover_failed_transition($transition_uuid);

      if (isset($result['warnings']) && count($result['errors']) > 0) 
      {
        $this->errException($errors[0], 'IN0002'); 
      }


      if (isset($result['warnings']) && count($result['warnings']) > 0)
      {
        $this->addWarnings($result['warnings']);
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
