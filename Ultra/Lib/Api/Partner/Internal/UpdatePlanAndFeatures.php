<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class UpdatePlanAndFeatures extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__UpdatePlanAndFeatures
   *
   * Calls mwUpdatePlanAndFeatures if necessary
   *
   * @param string mode
   * @return Result object
   */
  public function internal__UpdatePlanAndFeatures()
  {
    list ($msisdn, $iccid) = $this->getInputValues();

    try
    {
      dlog('', 'authenticated user: %s', empty($_SERVER['PHP_AUTH_USER']) ? 'NULL' : $_SERVER['PHP_AUTH_USER']);

      // confirm customer exists
      teldata_change_db();
      $select_fields = array('customer_id', 'current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language', 'plan_state', 'BRAND_ID');
      if ($msisdn)
        $customers = get_ultra_customers_from_msisdn($msisdn, $select_fields);
      elseif ($iccid)
        $customers = get_ultra_customers_from_iccid($iccid, $select_fields);
      else
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: requires msisdn or iccid', 'IN0002' );

      if (empty($customers[0]))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');
      $customer = $customers[0];

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      $account = get_account_from_customer_id($customer->customer_id, array('COS_ID'));
      if ( ! $account || ! $account->COS_ID )
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer account does not exist', 'VV0031');

      if ( ! $customer->current_mobile_number )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no phone number', 'MP0003');

      if ( ! $customer->CURRENT_ICCID_FULL )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer has no ICCID' , 'MP0004' );

      // allow only Active since subscriber must be active on MVNE
      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001', "Subsriber is {$customer->plan_state}");

      // retrieve correct short plan name
      if ( ! $plan_short = get_monthly_plan_from_cos_id($account->COS_ID))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105', 'Invalid Ultra Plan', 'Invalid Ultra plan');

      // retrieve correct plan configuration and customer configuration from mwCheckBalance
      $accSocsPlanConfigNewPlan = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($plan_short);

      // get current balance from MVNE
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $params = array(
        'actionUUID' => $this->getRequestId(),
        'msisdn'     => $customer->current_mobile_number);
      $result = $mwControl->mwCheckBalance($params);

      // check result
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');
      if ( ! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100')
        $this->errException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');
      if ( ! $result->data_array['success'] || empty($result->data_array['body']))
        $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');
      $socs = $result->data_array['body'];

      // historical map of Ultra Plan refreshes: only update features that changed
      $planRefreshMap = array(
        'L34' => array('DA1'                    => array('b_voice', 'B-VOICE')),
        'L44' => array('Base Data Usage Limit'  => array('b_data_thr128', 'B-DATA-THR128')));
      $socFeatureMap = $planRefreshMap[$plan_short];

      foreach ($socs->BalanceValueList->BalanceValue as $balance_value)
      {
        // if SOC exists both on MVNE and Ultra Plan and Ultra Plan value is higher than MVNE
        if ( ! empty($socFeatureMap[$balance_value->UltraType])
          && ! empty($accSocsPlanConfigNewPlan[$socFeatureMap[$balance_value->UltraType][0]])
          && $accSocsPlanConfigNewPlan[$socFeatureMap[$balance_value->UltraType][0]] > $balance_value->Value)
        {
          $delta = round($accSocsPlanConfigNewPlan[$socFeatureMap[$balance_value->UltraType][0]] - $balance_value->Value);
          dlog('', 'updating %s value from %s to %s by %s',
            $socFeatureMap[$balance_value->UltraType][0],
            $balance_value->Value,
            $accSocsPlanConfigNewPlan[$socFeatureMap[$balance_value->UltraType][0]],
            $delta);
          $this->performUpdatePlanAndFeaturesCall($mwControl, $customer, $plan_short, "{$socFeatureMap[$balance_value->UltraType][1]}|$delta");
        }
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  private function performUpdatePlanAndFeaturesCall($mwControl, $customer, $plan_short, $option)
  {
    $result = $mwControl->mwUpdatePlanAndFeatures(
      array(
        'msisdn'             => $customer->current_mobile_number,
        'iccid'              => $customer->CURRENT_ICCID_FULL,
        'customer_id'        => $customer->customer_id,
        'ultra_plan'         => $plan_short,
        'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->customer_id),
        'preferred_language' => $customer->preferred_language,
        'option'             => $option,
        'keepDataSocs'       => TRUE));

    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();
      $this->errException('ERR_API_INTERNAL: ' . $errors[0], 'MW0001');
    }

    if ( isset($result->data_array['success']) && ! $result->data_array['success'] )
      $this->errException('ERR_API_INTERNAL: ' . $result->data_array['errors'][0], 'MW0001');
  }

}

?>
