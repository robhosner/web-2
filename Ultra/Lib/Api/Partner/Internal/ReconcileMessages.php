<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'classes/Messenger.php';

class ReconcileMessages extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__ReconcileMessages
   *
   *  invokes method reconcile_messages of class Messenger
   *
   * @return Result object
   */
  
  public function internal__ReconcileMessages()
  {
    try
    {
      teldata_change_db();

      $messenger = new \Messenger();
      $messenger->reconcile_messages();

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
