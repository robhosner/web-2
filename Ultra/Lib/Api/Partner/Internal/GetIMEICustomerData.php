<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetIMEICustomerData extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetIMEICustomerData
   *
   * Get IMEI customer data
   *
   * @param  String ICCID
   * @param  String MSISDN
   * @return Result object
   */
  public function internal__GetIMEICustomerData()
  {
    list ($ICCID) = $this->getInputValues();

    $imei_list        = array();
    $make_list        = array();
    $model_list       = array();
    $model_name_list  = array();
    $os_list          = array();
    $device_type_list = array();

    try
    {
      $imei_history = get_imei_history(array('iccid' => $ICCID));

      if ( empty($imei_history->IMEI))
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: no IMEI history data found for ICCID/MSISDN', 'IN0001');
      }
  
      $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.IMEI_TACDB', NULL, NULL, array( 'IMEI_ABBR' => substr($imei_history->IMEI, 0, 8) ));
      $devices = mssql_fetch_all_objects(logged_mssql_query($sql));

      if ( ! count($devices))
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: no IMEI data found for ICCID/MSISDN', 'IN0001');
      }
      else
      {
        foreach ($devices as $device)
        {
          $imei_list[]        = $imei_history->IMEI;
          $make_list[]        = $device->make;
          $model_list[]       = $device->model;
          $model_name_list[]  = $device->model_name;
          $os_list[]          = $device->os;
          $device_type_list[] = $device->device_type;
        }
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('imei_list',        $imei_list);
    $this->addToOutput('make_list',        $make_list);
    $this->addToOutput('model_list',       $model_list);
    $this->addToOutput('model_name_list',  $model_name_list);
    $this->addToOutput('os_list',          $os_list);
    $this->addToOutput('device_type_list', $device_type_list);

    return $this->result;
  }
}

?>
