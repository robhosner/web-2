<?php
namespace Ultra\Lib\Api\Partner\Ultrainfo;

use Ultra\Configuration\Configuration;
use Ultra\Dealers\Repositories\Mssql\DealersRepository;

/**
 * Class GetLocations
 * @package Ultra\Lib\Api\Partner\Ultrainfo
 */
class GetLocations extends \Ultra\Lib\Api\Partner\Ultrainfo
{
  /**
   * @var
   */
  private $thresholdActivations;

  /**
   * @var
   */
  private $thresholdMile;

  /**
   * @var DealersRepository
   */
  private $dealersRepository;

  /**
   * @var Configuration
   */
  private $config;

  /**
   * GetLocations constructor.
   * @param DealersRepository $dealersRepository
   * @param Configuration $config
   */
  public function __construct(DealersRepository $dealersRepository, Configuration $config)
  {
    $this->dealersRepository = $dealersRepository;
    $this->config = $config;
    $thresholds = $this->config->getDealerLocatorThresholds();
    $this->thresholdActivations = 1;
    $this->thresholdMile = $thresholds['LOCATOR_MILE_THRESHOLD'];
  }
  
  /**
   * Provides a list of the topup-only, and dealer locations,
   * registered with Ultra.
   *
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+UltraInfo#Commands:UltraInfo-ultrainfo::GetLocations
   */
  public function ultrainfo__GetLocations()
  {
    list($latitude, $longitude, $radius, $max_entries, $location_type, $brand) = $this->getInputValues();

    try
    {
      $locationList = [];
      $locations = $this->dealersRepository->getLocations(
        [
          'latitude'      => $latitude,
          'longitude'     => $longitude,
          'radius'        => $radius,
          'max_entries'   => $max_entries,
          'location_type' => $location_type,
          'brand_id'      => $this->config->getBrandFromShortName($brand)['id']
        ]
      );

      if (!empty($locations))
      {
        $locationList = $this->formatLocations(
          $this->parseResults(
            $locations,
            $location_type
          )
        );
      }
      else
      {
        $this->errException('ERR_API_INTERNAL: No Locations found.', 'ND0001');
      }

      $this->addToOutput('location_list', $locationList);
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * @param $results
   * @param string $location_type
   * @return array
   */
  protected function parseResults($results, $location_type = 'DEALER')
  {
    switch ($location_type)
    {
      case 'DEALER':
        $parsedResults = $this->parseResultsIntoPriorityList($results);
        break;
      default:
        $parsedResults = $this->parseResultsTopup($results);
        break;
    }

    return $parsedResults;
  }

  /**
   * @param $results
   * @return mixed
   */
  protected function parseResultsTopup($results)
  {
    foreach ($results as &$result)
    {
      if ($result->ACTIVATIONS_COUNT >= $this->thresholdActivations)
      {
        $result->LOCATION_TYPE = 'DEALER';
      }
      else if ($result->ACTIVATIONS_COUNT < $this->thresholdActivations)
      {
        $result->LOCATION_TYPE = 'TOPUP';
      }
    }

    return $results;
  }

  /**
   * Create an ordered list of dealers based on priorities.
   *
   * @param $results
   * @return mixed
   */
  protected function parseResultsIntoPriorityList($results)
  {
    // Ordered List of 4 groups
    $groups = [[], [], [], []];

    foreach ($results as $result)
    {
      $group = null;

      if ($result->LOCATION_DISTANCE_MILES <= $this->thresholdMile && $result->ACTIVATIONS_COUNT >= $this->thresholdActivations)
        $group = 0;
      else if ($result->LOCATION_DISTANCE_MILES <= $this->thresholdMile && $result->ACTIVATIONS_COUNT < $this->thresholdActivations)
        $group = 1;
      else if ($result->LOCATION_DISTANCE_MILES > $this->thresholdMile && $result->ACTIVATIONS_COUNT >= $this->thresholdActivations)
        $group = 2;
      else if ($result->LOCATION_DISTANCE_MILES > $this->thresholdMile && $result->ACTIVATIONS_COUNT < $this->thresholdActivations)
        $group = 3;

      if (!is_null($group))
      {
        $groups[$group][] = $result;
      }
    }

    return array_merge($groups[0], $groups[1], $groups[2], $groups[3]);
  }
}
