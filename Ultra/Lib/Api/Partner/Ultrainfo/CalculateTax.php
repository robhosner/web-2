<?php
namespace Ultra\Lib\Api\Partner\Ultrainfo;

use SalesTaxCalculator;
use Ultra\Lib\Api\Partner\Ultrainfo;

class CalculateTax extends Ultrainfo
{
  /**
   * @var SalesTaxCalculator
   */
  private $salesTaxCalculator;

  /**
   * @var string
   */
  private $transactionTypeCode;

  public function __construct(SalesTaxCalculator $salesTaxCalculator, $transactionTypeCode = SalesTaxCalculator::TRANSACTION_TYPE_CODE_RECHARGE)
  {
    $this->salesTaxCalculator = $salesTaxCalculator;
    $this->transactionTypeCode = $transactionTypeCode;
  }

  public function ultrainfo__CalculateTax()
  {
    list($amount, $zipCode, $fees) = $this->getInputValues();

    try {
      $this->salesTaxCalculator->setAmount($amount + $fees);
      $this->salesTaxCalculator->setBillingZipCode($zipCode);
      $this->salesTaxCalculator->setAccount(' ');
      $this->salesTaxCalculator->activationZipCode = $zipCode;

      $result = $this->salesTaxCalculator->getSalesTax($this->transactionTypeCode);

      if ($result->is_success() && $result->data_array['Successful'] === 'Y') {
        $this->addToOutput('tax', (int) ($result->data_array['TotalTax'] * 100));
        $this->succeed();
      } else {
        if (!empty($result->get_errors()) && strpos($result->get_errors()[0], 'Zip Code not found') > -1) {
          throw new \Exception('No zip code found for ' . $zipCode);
        }

        throw new \Exception('Error calculating taxes');
      }
    } catch(\Exception $e) {
      dlog('' , $e->getMessage());
      $this->addError($e->getMessage(), 'IN0002', $e->getMessage());
    }

    return $this->result;
  }
}
