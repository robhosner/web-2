<?php

namespace Ultra\Lib\Api\Partner\Ultrainfo;

require_once 'db/htt_lookup_reference_ziploc.php';

class FindDealersByZip extends \Ultra\Lib\Api\Partner\Ultrainfo
{
  /**
   * Given a MSISDN and a zip code get a list of dealers in that area
   * we cache all dealers for specific zip code as well as customer's previous query - with each API call we return the next dealer in the list
   * @see PROD-1402
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Tech+-+Miscellaneous
   */
  public function ultrainfo__FindDealersByZip()
  {
    // initialization
    list($msisdn, $zipcode, $shortcode, $language, $brand) = $this->getInputValues();

    if (empty($shortcode))
      $shortcode = 'the keyword';

    if (empty($brand))
      $brand = 'ULTRA';

    $msisdn = normalize_msisdn($msisdn);
    $message = NULL;
    $redis = new \Ultra\Lib\Util\Redis;
    $customerKey = __FUNCTION__ . '/' . $msisdn; // redis key to cache previos customer query parameters: zipcode and index result returned
    $index = NULL; // previolsy returned store index

    try
    {
      // check cache for existing query
      if ($query = $redis->get($customerKey))
      {
        list($lastZipcode, $lastIndex) = json_decode($query);
        dlog('', 'found previous query: zipcode %s, index: %d', $lastZipcode, $lastIndex);

        // use last zip code if not given
        if ( ! $zipcode)
          $zipcode = $lastZipcode;

        // use last index if zipcode matches
        if ($zipcode == $lastZipcode)
          $index = $lastIndex;
      }

      // zipcode is required to proceed
      if (empty($zipcode))
      {
        $message = $this->findStringByLang($language, array(
          'EN' => "5 digit zip code expected after $shortcode. Please try again.",
          'ES' => "Codigo postal de 5 digitos necesario despues del $shortcode. Por favor intenta nuevamente."
        ));

        $this->errException('ERR_API_INVALID_ARGUMENTS: ' . $message, 'VV0023');
      }
      $locationKey = __FUNCTION__ . '/' . $zipcode; // key to cache dealers by zipcode

      // try getting cached data, otherwise retrieve from DB
      if ($locations = $redis->get($locationKey))
      {
        $locations = json_decode($locations);
        dlog('', 'using %d cached locations', count($locations));
      }
      else
      {
        // get zip code coordinates
        teldata_change_db();
        $sql = htt_lookup_reference_ziploc_select_query_by_zip_code($zipcode);
        $coordinates = mssql_fetch_all_objects(logged_mssql_query($sql));
        if ( ! count($coordinates))
        {
          $message = $this->findStringByLang($language, array(
            'EN' => 'Unknown zip code. Call 611 for Customer Support.',
            'ES' => 'Codigo postal desconocido. Marca 611 para comunicarte con Servicio al Cliente.'
          ));

          $this->errException('ERR_API_INTERNAL: No data found', 'ND0001');
        }
        $coordinate = $coordinates[0];

        // prepare and query table htt_ultra_locations; distance returned is in meters
        $params = array(
          'latitude'        => $coordinate->LATITUDE,
          'longitude'       => $coordinate->LONGITUDE,
          'max_entries'     => 50,
          'location_type'   => 'DEALER',
          'radius'          => 5000); # TODO: if I increase this, I don't get data
        $query = htt_ultra_locations_select_query($params);
        $locations = mssql_fetch_all_objects(logged_mssql_query($query));
        if ( ! count($locations))
        {
          $message = $this->findStringByLang($language, array(
            'EN' => 'No stores found in 50 mile radius. Call 611 for Customer Support.',
            'ES' => 'No hay puntos de venta en 50 millas. Marca 611 para comunicarte con Servicio al Cliente.'
          ));

          $this->errException('ERR_API_INVALID_ARGUMENTS: ' . $message, 'VV0021');
        }

        // clean data for caching and SMS
        $properties = array('RETAILER_NAME', 'ADDRESS1', 'ADDRESS2', 'CITY', 'STATE', 'RETAILER_PHONE_NUMBER');
        foreach ($locations as &$location)
        {
          foreach ($properties as $property)
            $location->$property = preg_replace('/[^[:print:]]/', '', $location->$property); // ASCII printable only
          $phone = normalize_msisdn($location->RETAILER_PHONE_NUMBER);
          $location->RETAILER_PHONE_NUMBER = substr($phone, 0, 3) . '-' . substr($phone, 3, 3) . '-' . substr($phone, -4);
        }

        // cache these locations
        $redis->set($locationKey, json_encode($locations), SECONDS_IN_DAY);
      }

      // PROD-1880: determine the last member of $locations within 1 mile distance
      for ($last = 0; $last < count($locations); $last++)
        if ($locations[$last]->location_distance_miles > 1)
          break;

      // if the 1 mile group is large enough then return a random location from it
      if ($last > 3)
        $index = rand(0, $last);
      // otherwise determine next location in sequence
      else 
      {
        $index = $index === NULL ? 0 : ++$index;
        if (empty($locations[$index]))
          $index = 0;
      }

      $location = $locations[$index];
      dlog('', 'using location index %d = %s, ', $index, $location);

      $message_tel = $this->findStringByLang($language, array('EN' => 'Tel', 'ES' => 'Telefono'));
      $message_distance = $this->findStringByLang($language, array('EN' => 'Distance', 'ES' => 'Distancia'));

      $message_footer = '';
      switch ($brand)
      {
        case 'ULTRA':
          $message_footer = $this->findStringByLang($language, array(
            'EN' => 'Visit http://ultra.me/locator or reply with STORE for more results', 
            'ES' => 'Visita http://ultra.me/locator o contesta a este mensaje con STORE por mas resultados.'
          ));
          break;
        case 'UNIVISION':
          $message_footer = $this->findStringByLang($language, array(
            'EN' => 'Visit http://univision.com/tiendas or reply with STORE for more results', 
            'ES' => 'Visita http://univision.com/tiendas o contesta a este mensaje con STORE por mas resultados.'
          ));
          break;
      }

      // compose SMS
      $message = implode(' ', array(
        beautify_address($location->RETAILER_NAME),
        beautify_address($location->ADDRESS1 . (empty($location->ADDRESS2) ? NULL : " {$location->ADDRESS2}")),
        beautify_address($location->CITY),
        $location->STATE,
        $location->ZIPCODE,
        "$message_tel: {$location->RETAILER_PHONE_NUMBER}",
        "$message_distance: " . ceil($location->location_distance_miles) . ' mi'));

      // append footer if we have enough space left
      if (strlen($message) + strlen($message_footer) + 1 <= 160)
        $message .= " $message_footer";

      // cache customer query parameters
      $redis->set($customerKey, json_encode(array($zipcode, $index)), SECONDS_IN_DAY);

      $this->succeed();
    }

    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
    }

    $this->addToOutput('message', str_replace(',', '', $message));
    return $this->result;
  }

  protected function findStringByLang($lang, $messages)
  {
    return (isset($messages[$lang])) ? $messages[$lang] : $messages['EN'];
  }
}

