<?php

namespace Ultra\Lib\Api\Partner;

require_once 'db/htt_ultra_locations.php';
require_once 'Ultra/Lib/Api/PartnerBase.php';
include_once 'db/htt_card_calling_rates.php';

/**
 * Promotional partner class
 *
 * @project ultra.me
 * @author bwalters@ultra.me
 */
class Promotional extends \Ultra\Lib\Api\PartnerBase
{

  /**
   * promotional__ActivatePromotionalCustomer
   *
   * Kicksoff the state transition from Promo Used to Active.
   *
   * @return Result object
   */
  public function promotional__ActivatePromotionalCustomer()
  {
    list ( $customer_id ) = $this->getInputValues();

    $error_code = '';

    try
    {
      $this->connectToDatabase();
      $customer = $this->getCustomerByCustomerId($customer_id);
      $state    = $this->getStateFromCustomerId($customer->CUSTOMER_ID);

      if ($state['state'] != 'Promo Unused') {
        throw new Exception("ERR_API_INVALID_ARGUMENTS: customer is not in state 'Promo Unused'");
      }

      $context  = array('customer_id' => $customer->CUSTOMER_ID);
      $result   = $this->activatePromoAccount($state, $customer, $context);

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }

  /**
   * promotional__PreparePromoCustomers
   *
   * Prepares promotional SIMs.
   *
   * @return Result object
   */
  public function promotional__PreparePromoCustomers()
  {
    list ($promo_id, $prepare_count) = $this->getInputValues();

    $error_code     = '';
    $prepare_count  = isset($prepare_count) ? $prepare_count : 15;

    try
    {
      $semaphore_key  = 'promotional/PreparePromoCustomers/' . $promo_id;
      $redis          = $this->getRedisInstance();
      $semaphore      = $redis->get($semaphore_key);

      if ($semaphore)
      {
        $redis->del($semaphore_key);
        throw new \Exception('ERR_API_INTERNAL: Another process is running for promo_id $promo_id, please try later');
      }

      $redis->set($semaphore_key, 1, (15 * 60));
      $this->connectToDatabase();
      $open_transactions = $this->getOpenTransitionLogCount();

      if ($open_transactions > 50)
      {
        throw new \Exception('ERR_API_INTERNAL: too many OPEN transitions ($htt_transition_log_open_count), please retry later.');
      }

      $promo_data = $this->getPromotionalPlanById($promo_id);
      $zipcodes   = $this->getZipCodesFromString($promo_data->ZIPRANGE);
      $this->validateZipCodesCoverage($zipcodes);
      $sims = $this->getSimsWithRange($promo_data->ICCID_START, $promo_data->ICCID_END);
      if ($promo_data->PROMO_STATUS == 'VIRGIN')
      {
        $used_count   = 0;
        $start_exists = FALSE;
        $end_exists   = FALSE;
        $error        = NULL;

        // loop though SIMS in the promo range
        foreach($sim_data as $data)
        {
          if (($data->SIM_ACTIVATED) || ($data->CUSTOMER_ID))
          {
            $used_count++;
          }

          if ($promo_data->ICCID_START == $data->ICCID_FULL)
          {
            $start_exists = TRUE;
          }

          if ($promo_data->ICCID_END == $data->ICCID_FULL)
          {
            $end_exists = TRUE;
          }

          // ensure that all SIMs are Hot
          if (!$data->SIM_HOT)
          {
            $error = "ERR_API_INTERNAL: SIM ".$data->ICCID_FULL." is not Hot";
            dlog('', $error);
            throw new \Exception($error);
          }

          // ensure that all SIMs are not expired
          if ($data->is_expired)
          {
            $error = "ERR_API_INTERNAL: SIM " . $data->ICCID_FULL . " is expired";
            dlog('', $error);
            throw new \Exception($error);
          }
        }

        // ensure that all SIMs are unused
        if ($used_count)
        {
          throw new \Exception("ERR_API_INTERNAL: promo status is 'VIRGIN', but {$used_count} SIMS in range are already used.");
        }

        // make sure that the SIM start and end exist
        if (!$start_exists)
        {
          throw new \Exception("ERR_API_INTERNAL: SIM range start does not exist.");
        }

        if (!$end_exists)
        {
          throw new \Exception("ERR_API_INTERNAL: SIM range end does not exist.");
        }

        $this->validateZipCodeDistribution($promo_data->ICCID_START, $promo_data->ICCID_END, $zipcodes);
        $this->setPromotionalPlanStatus($promo_id, 'PREPARATION'); // set ULTRA.PROMOTIONAL_PLANS.PROMO_STATUS to 'PREPARATION'
      }
      else
      {
        // Promo SIMs with SIM_ACTIVATED = 0 and no CUSTOMER_ID
        $unused_sims = array();

        foreach($sim_data as $data)
        {
          if ((!$data->SIM_ACTIVATED) && (!$data->CUSTOMER_ID))
          {
            $unused_sims[] = $data;
          }
        }

        if (count($unused_sims))
        {
          $sims_to_be_processed = array();

          // If the status is PREPARATION, find all ICCIDs still to be processed:
          //   - exclude if in htt_customers_overlay_ultra.current_iccid
          //   - exclude if in htt_customers_overlay_ultra.ACTIVATION_ICCID
          //   - exclude if htt_inventory_sim.SIM_ACTIVATED is 1
          // If this list is empty, throw an error that some sims are in an errored state or still processing.

          foreach ($unused_sims as $data)
          {
            $customer = $this->getCustomerMatchBySim($data->ICCID_FULL);

            if ($customer)
            {
              dlog('', "ICCID %s already assigned to a customer", $data->ICCID_FULL);
            }
            else
            {
              $sims_to_be_processed[] = $data;
              if (count($sims_to_be_processed) > $prepare_count) // collect one more than needed.
              {
                break;
              }
            }
          }

          if (count($sims_to_be_processed))
          {
            // activate $prepare_count SIMS
            $count_processed = 0;

            foreach ($sims_to_be_processed as $data)
            {
              // we want to process maximum $prepare_count SIMs
              if ($count_processed < $prepare_count)
              {
                $postal_code_id = array_rand($zipcodes, 1);
              }


              // creates a new customer, associate it with the SIM, transition to 'Promo Unused'
              $result = $this->initializePromotionalSim($promo_data, $zipcodes[$postal_code_id], $data->ICCID_FULL);
              if ($result->has_errors())
              {
                $errors = array_merge($errors, $result->get_errors());
              }

              $count_processed++;
            }

            dlog('', "count_processed = $count_processed");
          }
          else
          {
            throw new Exception("ERR_API_INTERNAL: some sims are in an errored state or still processing");
          }
        }
        else
        {
          // all the SIMs are used
          // If all the SIMs are used from htt_inventory_sim, set ULTRA.PROMOTIONAL_PLANS.PROMO_STATUS to 'READY'
          $this->setPromotionalPlanStatus($promo_id, 'READY');

          // If ULTRA.PROMOTIONAL_PLANS.ACTIVATION_MASTERAGENT = 54 or NULL
          // set  HTT_INVENTORY_SIM.INVENTORY_MASTERAGENT to ULTRA.PROMOTIONAL_PLANS.ACTIVATION_MASTERAGENT
          // set  HTT_INVENTORY_SIM.INVENTORY_DISTRIBUTOR to $promo_data->ICCID_START
          // set  HTT_INVENTORY_SIM.INVENTORY_DEALER      to $promo_data->ICCID_END
          if ((!$promo_data->ACTIVATION_MASTERAGENT) || ($promo_data->ACTIVATION_MASTERAGENT == 54))
          {
            $this->updateSimsByRange($promo_data->ACTIVATION_MASTERAGENT, $promo_data->ICCID_START, $promo_data->ICCID_END, $promo_data->ICCID_START, $promo_data->ICCID_END);
          }
        }
      }

      if (count($errors))
      {
        foreach ($errors as $error)
        {
          $this->addError($error, $error_code);
        }

        $this->fail();
      }
      else
        $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' , $e->getMessage());
      $this->addError($e->getMessage(), $error_code);
    }

    $redis->del($semaphore_key);

    return $this->result;
  }

  /**
   * promotional__ListPromotionsForPreparation
   *
   * Lists the state of current promotions with regards to preparation.
   *
   * @return Result object
   */
  public function promotional__ListPromotionsForPreparation()
  {
    try
    {
      $this->connectToDatabase();
      $error_code         = '';
      $promotional_plans  = $this->getAllPromotionalPlans();

      if (count($promotional_plans))
      {
        foreach ($promotional_plans as $plan)
        {
          dlog("", "%s", $plan);
          $total_sims = $this->getSimCountByRange($plan->ICCID_START, $plan->ICCID_END);
          // number of non-Neutral customers associated with this promotion
          $ready_customers = $this->getSimCountByReadyCustomerPromotions($plan->ULTRA_PROMOTIONAL_PLANS_ID);
          // number of active iccids in the sim range associated with this promotion
          $ready_sims = $this->getReadySimsByRange($plan->ICCID_START, $plan->ICCID_END, TRUE);
          // number of Active customers associated with this promo id
          $active_customers = $this->getSimCountByActiveCustomersPromotional($plan->ULTRA_PROMOTIONAL_PLANS_ID);

          $promotion = array(
            'id'                => $plan->ULTRA_PROMOTIONAL_PLANS_ID,
            'promo_status'      => $plan->PROMO_STATUS,
            'project_name'      => $plan->PROJECT_NAME,
            'channel'           => $plan->CHANNEL,
            'subchannel'        => $plan->SUBCHANNEL,
            'microchannel'      => $plan->MICROCHANNEL,
            'iccid_start'       => $plan->ICCID_START,
            'iccid_end'         => $plan->ICCID_END,
            'total_sims'        => $total_sims,
            'ready_customers'   => $ready_customers,
            'ready_sims'        => $ready_sims,
            'active_customers'  => $active_customers
          );

          $promotions[] = $promotion;
        }

        $this->addToOutput('promotions', $promotions);
      } else {
        $this->addWarning("ERR_API_INTERNAL: No data found");
        $this->addToOutput('promotions', array());
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $error_code);
    }

    return $this->result;
  }

  protected function getCustomerByCustomerId($customer_id = false)
  {
    if (!$customer_id)
    {
      throw new \Exception("Customer id is required");
    }

    $customer = get_customer_from_customer_id($customer_id);
    if (!$customer) {
      throw new \Exception("ERR_API_INVALID_ARGUMENTS: customer not found.");
    }

    return $customer;
  }

  protected function getStateFromCustomerId($customer_id = false)
  {
    if (!$customer_id)
    {
      throw new \Exception("Customer id is required");
    }

    $state = internal_func_get_state_from_customer_id( $customer->CUSTOMER_ID );
    if (!$state) {
      throw new \Exception("ERR_API_INTERNAL: customer state could not be determined.");
    }

    return $state;
  }

  protected function activatePromoAccount($state = false, $customer = false, $context = false)
  {
    if (!$state)    throw new \Exception("State is required");
    if (!$customer) throw new \Exception("Customer is required");
    if (!$context)  throw new \Exception("Context is required");

     $result = activate_promo_account($state, $customer, $context);
     if (count($result['errors']) > 0) {
      throw new \Exception($result['errors'][0]);
     }

     return $result;
  }

  protected function getRedisInstance()
  {
    return new \Ultra\Lib\Util\Redis();
  }

  protected function getOpenTransitionLogCount()
  {
    return htt_transition_log_open_count();
  }

  protected function getPromotionalPlanById($promo_id = false)
  {
    if (!$promo_id) throw new Exception('Promo id is required');

    $promo_data = get_ultra_promotional_plans_by_id($promo_id);

    if ((!$promo_data) || (!is_array($promo_data)) || (!count($promo_data)))
    {
      throw new Exception("ERR_API_INVALID_ARGUMENTS: promotional plan not found");
    }

    dlog('', "ULTRA.PROMOTIONAL_PLANS data = %s", $promo_data);

    $promo_data = $promo_data[0];

    return $promo_data;
  }

  protected function getZipCodesFromString($zip_string = false)
  {
    if (!$zip_string) throw new \Exception('Zip string is required');

    list($zipcodes, $error) = extractZipcodesFromString($zip_string);
    if ($error) throw new \Exception($error);

    dlog('', "zipcodes = %s", $zipcodes);

    return $zipcodes;
  }

  protected function validateZipCodesCoverage(array $zipcodes)
  {
    if (!validateZipCodesCoverage($zipcodes)) throw new \Exception("ERR_API_INTERNAL: one or more zip codes is not in coverage");

    return true;
  }

  protected function getSimsWithRange($start_iccid = false, $end_iccid = false)
  {
    if (!$start_iccid || !$end_iccid) throw new \Exception('Start and End ICCID are required');

    $sim_data = get_htt_inventory_sims_by_range($start_iccid, $end_iccid);
    if ((!$sim_data) || (!is_array($sim_data)) || (!count($sim_data))) {
      throw new \Exception("ERR_API_INTERNAL: No SIMs found in range ({$start_iccid}, {$end_iccid})");
    }

    return $sim_data;
  }

  protected function validateZipCodeDistribution($start_iccid = false, $end_iccid = false, $zip_codes)
  {
    if (!$start_iccid)  throw new \Exception('Start iccid is required');
    if (!$end_iccid)    throw new \Exception('End iccid is required');
    if (!$zipcodes)     throw new \Exception('Zipcodes is required');

    // ensure sparse zip codes distribution
    $zipValidationResult = validate_sim_zip_code_distribution($start_iccid, $end_iccid, $zipcodes);

    if ($zipValidationResult->is_failure())
    {
      $errors = $zipValidationResult->get_errors();

      if (isset($errors[0]) && $errors[0])
      {
        throw new \Exception($errors[0]);
      }
      else
      {
        throw new Exception("ERR_API_INTERNAL: Zip Codes distribution is invalid");
      }
    }
  }

  protected function setPromotionalPlanStatus($promo_id = false, $status = false)
  {
    if (!$promo_id) throw new \Exception('Promo id is required');
    if (!$status)   throw new \Exception('Status is required');

    if (!set_ultra_promotional_plan_status($promo_id, $status)) {
      throw new Exception("ERR_API_INTERNAL: DB error, could not update ULTRA.PROMOTIONAL_PLANS");
    }

    return true;
  }

  protected function getCustomerMatchBySim($iccid = false)
  {
    if (!$iccid) throw new \Exception('ICCID is required');

    return ultra_customer_match_by_sim($iccid);
  }

  protected function initializePromotionalSim($promo_data = false, $zipcode = false, $iccid_full = false)
  {
    if (!$promo_data) throw new \Exception('Promo data is required');
    if (!$zipcode)    throw new \Exception('Zipcode is required');
    if (!$iccid_full) throw new \Exception('ICCID is required');

    return internal_func_promotional_sim_init(
      array(
        'ultra_promotional_data' => $promo_data,
        'postal_code'            => $zipcode,
        'iccid'                  => $iccid_full
      )
    );
  }

  protected function updateSimsByRange($master_agent = false, $inventory_distributor = false, $inventory_dealer = false, $iccid_full_from = false, $iccid_full_to = false)
  {
    if (!$master_agent)           throw new \Exception('Master agent is required');
    if (!$inventory_distributor)  throw new \Exception('Inventory distributor is required');
    if (!$inventory_dealer)       throw new \Exception('Inventory dealer is required');
    if (!$iccid_full_from)        throw new \Exception('ICCID full from is required');
    if (!$iccid_full_to)          throw new \Exception('ICCID full to is required');

    $result = htt_inventory_sim_update_range(
      array(
        'inventory_masteragent' => $master_agent,
        'inventory_distributor' => $inventory_distributor,
        'inventory_dealer'      => $inventory_dealer,
        'iccid_full_from'       => $iccid_full_from,
        'iccid_full_to'         => $iccid_full_to
      )
    );

    if (!$result) throw new Exception("ERR_API_INTERNAL: DB error, could not update HTT_INVENTORY_SIM");

    return $result;
  }

  protected function getAllPromotionalPlans()
  {
    $sql      = ultra_promotional_plans_select_query(array());
    $results  = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ((!$results) || (!is_array($results)))
    {
      throw new Exception("ERR_API_INTERNAL: DB Error");
    }

    return $results;
  }

  protected function getSimCountByRange($start_iccid = false, $end_iccid = false)
  {
    if (!$start_iccid)  throw new \Exception('Start iccid is required');
    if (!$end_iccid)    throw new \Exception('End iccid is required');

    list($total_sims, $warning) = htt_inventory_sim_count_by_range($start_iccid, $end_iccid);
    if ($warning) $this->addWarning($warning);

    return $total_sims;
  }

  protected function getSimCountByReadyCustomerPromotions($promotion_plan_ids = false)
  {
    if (!$promotion_plan_ids)  throw new \Exception('Promotional plan id is required');

    list($ready_customers, $warning) = get_ready_customers_promotional_sim_count($promotion_plan_ids);
    if ($warning) $this->addWarning($warning);

    return $ready_customers;
  }

  protected function getReadySimsByRange($start_iccid = false, $end_iccid = false, $active_switch = null)
  {
    if (!$start_iccid)            throw new Exception('Start iccid is required');
    if (!$end_iccid)              throw new Exception('End iccid is required');
    if (is_null($active_switch))  throw new Exception('Active switch is required');

    list($ready_sims, $warning) = htt_inventory_sim_count_by_range($start_iccid, $end_iccid, $active_switch);
    if ($warning) $this->addWarning($warning);

    return $ready_sims;
  }

  protected function getSimCountByActiveCustomersPromotional($promotion_plan_ids = false)
  {
    if (!$promotion_plan_ids) throw new \Exception('Promotional plan id is required');

    list($active_customers, $warning) = get_active_customers_promotional_sim_count($promotion_plan_ids);
    if ($warning) $this->addWarning($warning);

    return $active_customers;
  }
}

