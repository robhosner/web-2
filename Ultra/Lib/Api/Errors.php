<?php

namespace Ultra\Lib\Api\Errors;

require_once('db/ultra_user_error_messages.php');
require_once('Ultra/Lib/Util/Redis.php');

/**
 * decodeToUserError
 *
 * Decode error codes to user friendly errors
 *
 * @return string
 */
function decodeToUserError( $error_code , $language='EN' , $errorCodes=NULL , $redis=NULL )
{
  // default to EN
  if ( ! in_array( $language , array('EN','ES','ZH') ) )
  {
    if ( $language )
      dlog('',"Language %s not supported, using EN",$language);

    $language = 'EN';
  }

  if ( ! $errorCodes)
    $errorCodes = errorCodes( $language , $redis );

  $userError = "ESCALATION ERROR CODES - Error Code $error_code missing message";

  if ( is_array( $errorCodes ) && ( isset($errorCodes[ $error_code ]) ) )
    $userError = $errorCodes[ $error_code ];

  if ( is_object( $errorCodes ) && ( property_exists( $errorCodes , $error_code ) ) )
    $userError = $errorCodes->$error_code;

  return $userError;
}

/**
 * errorCodes
 *
 * Map error codes to user friendly errors
 *
 * @return array or object
 */
function errorCodes($language='EN',$redis=NULL)
{
  // default to EN
  if ( ! in_array( $language , array('EN','ES','ZH') ) )
  {
    dlog('',"Language %s not supported, using EN",$language);

    $language = 'EN';
  }

  $errorCodes = array();

  // check if the errors are cached in Redis
  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis;

  $cachedErrorCodes = $redis->get( 'ultra/api/user_errors/'.$language );

  if ( $cachedErrorCodes )
    $errorCodes = json_decode( $cachedErrorCodes );
  else
  {
    // load errors from DB

    teldata_change_db();

    $errorCodes = get_ultra_user_error_messages($language);

    if ( count($errorCodes) )
    {
      // save errors in Redis - ``A String value can be at max 512 Megabytes in length.``

      $cachedErrorCodes = json_encode( $errorCodes );

      // TTL is 24 hours
      $redis->set( 'ultra/api/user_errors/'.$language , $cachedErrorCodes , 24 * 60 * 60 );
    }
    else
      dlog('',"ERROR ESCALATION ALERT DAILY - get_ultra_user_error_messages did not return data");
  }

  return $errorCodes;
}

/**
 * errorCodesMissingParams
 *
 * Map input fields with error codes related to ``missing parameter`` messages
 *
 * @return array
 */
function errorCodesMissingParams()
{
  return
  array(
    'security_token'        => 'MP0002',
    'msisdn'                => 'MP0003',
    'ICCID'                 => 'MP0004',
    'iccid'                 => 'MP0004',
    'date_from'             => 'MP0005',
    'date_to'               => 'MP0006',
    'zipcode'               => 'MP0007',
    'celluphone_session_id' => 'MP0008',
    'number_to_port'        => 'MP0009',
    'request_id'            => 'MP0010',
    'target_plan'           => 'MP0011',
    'port_account_number'   => 'MP0012',
    'price_plan'            => 'MP0013',
    'account_cc_exp'        => 'MP0014',
    'account_cc_cvv'        => 'MP0015',
    'account_cc_number'     => 'MP0016',
    'cc_name'               => 'MP0017',
    'cc_postal_code'        => 'MP0018',
    'charge_amount'         => 'MP0019',
    'customer_id'           => 'MP0020',
    'reason'                => 'MP0021',
    'plan_state'            => 'MP0022',
    'epoch_from'            => 'MP0023',
    'epoch_to'              => 'MP0024',
    'pin'                   => 'MP0025',
    'pin_list'              => 'MP0026',
    'password'              => 'MP0029',
    'username'              => 'MP0030',
    'login_token'           => 'MP0031',
    'refresh_token'         => 'MP0032',
    'tos_accepted'          => 'MP0034',
    'contact_login_name'    => 'MP0035',
    'country'               => 'MP0036',
    'network_type'          => 'MP0037',
    'device_type'           => 'MP0038',
    'contacts'              => 'MP0039',
    'message'               => 'MP0040',
    'apiToken'              => 'MP0041',
    'messageSid'            => 'MP0042',
    'accountSid'            => 'MP0043',
    'direction'             => 'MP0044',
    'dateCreated'           => 'MP0045',
    'dateSent'              => 'MP0046',
    'dateReceived'          => 'MP0047',
    'fromDID'               => 'MP0048',
    'toDID'                 => 'MP0049',
    'status'                => 'MP0050',
    'SKU'                   => 'MP0051',
    'postal_code'           => 'MP0052',
    'token'                 => 'MP0053',
    'bin'                   => 'MP0054',
    'last_four'             => 'MP0055',
    'expires_date'          => 'MP0056',
    'gateway'               => 'MP0057',
    'merchant_account'      => 'MP0058',
    'customer_ip'           => 'MP0059',
    'preferred_language'    => 'MP0060',
    'auto_enroll'           => 'MP0061',
    'track_trace'           => 'MP0062',
  );
}

function missingParamCode( $field )
{
  $errorCodesMissingParams = errorCodesMissingParams();

  return ( isset( $errorCodesMissingParams[ $field ] ) )
         ?
         $errorCodesMissingParams[ $field ]
         :
         'MP0001'
         ;
}

