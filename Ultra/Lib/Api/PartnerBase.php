<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Errors.php';
require_once 'Ultra/Commander/CommanderTrait.php';

use Ultra\Commander\CommanderTrait;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\Util as Utils;
use Ultra\Lib\Util\Redis;

/**
 * Base class for Api Partner classes
 *
 * Api functions must return $this->result or a \Result object
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
abstract class PartnerBase
{
  private $requestId;

  protected $partner;
  public $result;
  protected $apiDefinition;
  protected $inputValues;
  public $defaultValues;
  protected $language = 'EN';
  protected $redis;
  
  /**
   * @var ApiErrors
   */
  public $apiErrorHandler;

  // supported MVNE values
  const MVNE1 = 1;
  const MVNE2 = 2;

  // activation defaults
  const DEFAULT_ACTIVATION_MASTERAGENT  = 54;
  const DEFAULT_ACTIVATION_DISTRIBUTOR  = 28;
  const DEFAULT_ACTIVATION_DEALER       = 28;
  const DEFAULT_ACTIVATION_USER_ID      = 67;

  use CommanderTrait;

  public function setOutputLanguage( $language='EN' )
  {
    $this->language = $language;
  }

  public function getOutputLanguage()
  {
    return $this->language;
  }
  
  public function setUp($apiDefinition, $requestId, $partner = null)
  {
    $this->partner       = $partner;
    $this->requestId     = $requestId;
    $this->apiDefinition = $apiDefinition;
    $this->defaultValues = $this->getDefaultValuesFromApiDefinition($apiDefinition);

    // default result is error
    $this->result        = new \Result(NULL, FALSE);
    $this->result->append_to_array_data( 'error_codes' , 'IN0002' );
    $this->result->append_to_array_data( 'user_errors' , 'ERR_API_INTERNAL: Generic internal error' );

    $this->apiErrorHandler = new ApiErrors();
  }

  /**
   * getRequestId
   */
  protected function getRequestId ()
  {
    return $this->requestId;
  }

  /**
   * getInputValues
   * returns non-associative array of input values
   */
  protected function getInputValues ()
  {
    return array_values($this->inputValues);
  }

  /**
   * getNamesInputValues
   * return associative array of input values
   */
  protected function getNamedInputValues()
  {
    return $this->inputValues;
  }

  /**
   * getDefaultValuesFromApiDefinition
   */
  protected function getDefaultValuesFromApiDefinition (array $apiDefinition)
  {
    $defaultValues = array();
    $index = 0;

    foreach ($apiDefinition as $definitionKey => $definition)
    {
      if ($definitionKey == 'parameters')
      {
        foreach ($definition as $parameterList)
        {
          $name = $parameterList['name'];

          foreach ($parameterList as $parameterKey => $parameter)
          {
            if ($parameterKey == 'default')
            {
              $defaultValues[$name] = array(
                'index' => $index,
                'value' => $parameter
              );
            }
          }

          if ($name != 'partner_tag') $index++;
        }

        break;
      }
    }

    return $defaultValues;
  }

  /**
   * setInputValues
   *
   * @return NULL
   */
  public function setInputValues ( $inputValues )
  {
    // API-228: one cannot access associative array by indexes, only by keys (named or indexed)
    $inputKeys = array_keys($inputValues);
    foreach ($this->defaultValues as $defaultValue)
    {
      $index = $defaultValue['index'];
      $value = $inputValues[$inputKeys[$index]];
      if (empty($value) && $value !== 0 && $value !== '0')
        $inputValues[$inputKeys[$index]] = $defaultValue['value'];
    }

    $this->inputValues = $inputValues;
  }

  /**
   * addToOutput
   *
   * @return NULL
   */
  protected function addToOutput ( $field , $value )
  {
    $this->result->data_array[ $field ] = $value;
  }

  /**
   * addArrayToOutput
   *
   * @return NULL
   */
  protected function addArrayToOutput ( $input )
  {
    foreach ($input as $field => $value)
      $this->result->data_array[ $field ] = $value;
  }

  /**
   * clearErrors
   *
   * @return NULL
   */
  protected function clearErrors()
  {
    // wipe out default errors
    $this->result->del_errors();
    $this->result->data_array['error_codes'] = array();
    $this->result->data_array['user_errors'] = array();
  }

  /**
   * errException
   *
   * @return NULL
   */
  protected function errException ( $error , $error_code , $user_error=FALSE )
  {
    // wipe out default errors
    $this->result->del_errors();
    $this->result->data_array['error_codes'] = array();
    $this->result->data_array['user_errors'] = array();

    $this->addError( $error , $error_code , $user_error );

    throw new \Exception( $error );
  }

  /**
   * addError
   *
   * @return NULL
   */
  public function addError ( $error , $error_code , $user_error=FALSE )
  {
    if ( ! $user_error )
      $user_error = $this->apiErrorHandler->decodeToUserError($error_code, $this->language);

    $this->result->append_to_array_data( 'error_codes' , $error_code );
    $this->result->append_to_array_data( 'user_errors' , $user_error );

    $this->result->add_error ( $error );

    $errorCodes = $this->result->data_array['error_codes'];
    $userErrors = $this->result->data_array['user_errors'];
    $defaultErrorIndex = array_search('IN0002', $errorCodes);
    $defaultUserErrorIndex = array_search('ERR_API_INTERNAL: Generic internal error', $userErrors);

    if (count($errorCodes) > 1 && !empty($errorCodes[$defaultErrorIndex])) {
      unset($this->result->data_array['error_codes'][$defaultErrorIndex]);
      $this->result->data_array['error_codes'] = array_values($this->result->data_array['error_codes']);

      if (count($userErrors) > 1 && !empty($userErrors[$defaultUserErrorIndex])) {
        unset($this->result->data_array['user_errors'][$defaultUserErrorIndex]);
        $this->result->data_array['user_errors'] = array_values($this->result->data_array['user_errors']);
      }
    }
  }

  /**
   * addErrors
   *
   * @return NULL
   */
  protected function addErrors ( $errors )
  {
    $this->result->add_errors ( $errors );
  }

  /**
   * addWarning
   *
   * @return NULL
   */
  protected function addWarning ( $warning )
  {
    $this->result->add_warning ( $warning );
  }

  /**
   * addWarnings
   *
   * @return NULL
   */
  protected function addWarnings ( $warnings )
  {
    $this->result->add_warnings ( $warnings );
  }

  /**
   * succeed
   *
   * @return NULL
   */
  protected function succeed ()
  {
    // wipe out errors
    $this->result->del_errors();
    $this->result->data_array['error_codes'] = array();
    $this->result->data_array['user_errors'] = array();

    $this->result->succeed ();
  }

  /**
   * is_success
   * return $this->success
   *
   * @return boolean $this->success
   */
  public function isSuccess()
  {
    return $this->result->is_success();
  }

  /**
   * addApiErrorNode
   *
   * add first API error as an object node
   * 3ci platform cannot (despite their claims) read values from indexed arrays; in order to overcome this limitation we must return objects
   */
  protected function addApiErrorNode()
  {
    // add user_errors[0] if present
    if ( ! empty($this->result->data_array['user_errors'][0]))
      $this->addToOutput('api_error', $this->result->data_array['user_errors'][0]);
    // othewise add errors[0]
    elseif ( ! empty($this->result->data_array['errors'][0]))
      $this->addToOutput('api_error', $this->result->data_array['user_errors'][0]);
  }

  /**
   * Given a customer_id, will return a customer.
   *
   **************
   * DEPRECATED *
   **************
   *
   * @param integer $customer_id
   * @return array
   * @author bwalters@ultra.me
   */
  protected function getCustomerById($customer_id = false)
  {
    if (!$customer_id) $this->errException("ERR_API_INTERNAL: Customer_id is required", "MP0020", "Customer id was not provided but is required");

    $customer = get_customer_from_customer_id($customer_id);

    if (!$customer) $this->errException("ERR_API_INTERNAL: Customer not found with id {$customer_id}", "VV0031", "Customer not found");

    return $customer;
  }

  /**
   * accountAliasesAreOK
   *
   * Given a customer_id, and their current_mobile_number,
   * will verify that their account aliases are correct.
   * @param integer $customer_id
   * @param integer $current_mobile_number
   * @return boolean
   * @author bwalters@ultra.me
   */
  protected function accountAliasesAreOK($customer_id, $current_mobile_number)
  {
    if (!$customer_id) $this->errException("ERR_API_INTERNAL: customer_id is required", "MP0020", "Customer id was not provided but is required");
    if (!$current_mobile_number) $this->errException("ERR_API_INTERNAL: current_mobile_number is required", "MP0003", "current_mobile_number was not provided but is required");

    if (!verify_account_aliases($customer_id, $current_mobile_number)) {
      return false;
    }

    return true;
  }

  /**
   * destinationOriginMapIsOk
   *
   * Given a customer_id, and their current_mobile_number,
   * will verify that their destination origin map is correct.
   * @param integer $customer_id
   * @param integer $current_mobile_number
   * @return boolean
   * @author bwalters@ultra.me
   */
  protected function destinationOriginMapIsOk($customer_id, $current_mobile_number)
  {
    if (!$customer_id) $this->errException("ERR_API_INTERNAL: customer_id is required", "MP0020", "Customer id was not provided but is required");
    if (!$current_mobile_number) $this->errException("ERR_API_INTERNAL: current_mobile_number is required", "MP0001", "current_mobile_number was not provided but is required");

    if (!verify_destination_origin_map($customer_id, $current_mobile_number)) {
      return false;
    }

    return true;
  }

  /**
   * Give a redis object, key, dealer info and recharge period,
   * will return the combined recharge details.
   * @param \Redis $redis
   * @param string $redis_key
   * @param $dealers
   * @param string $recharge_period
   * @return array(object)
   * @author bwalters@ultra.me
   */
  protected function getRechargeDetailsByDealer($redis, $redis_key, $dealers, $recharge_period)
  {
    $details_by_plan = get_summary_dealer_recharge_rate_by_month($dealers, $recharge_period);
    if ( ! count($details_by_plan))
      $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');

    // create Combined summary
    $combined = clone $details_by_plan[0];
    $combined->activations = 0;
    $combined->recharged = 0;
    $combined->remaining = 0;
    $combined->plan_name = 'Combined';

    // sum up all plans into Combined
    for ($i = 0, $count = count($details_by_plan); $i < $count; $i++)
    {
      $combined->activations += $details_by_plan[$i]->activations;
      $combined->recharged += $details_by_plan[$i]->recharged;
      $combined->remaining += $details_by_plan[$i]->remaining;
      $combined->suspended += $details_by_plan[$i]->suspended;
    }

    if ($combined->activations - $combined->remaining)
    {
      $combined->recharge_rate = round(($combined->recharged / ($combined->activations - $combined->remaining)), 2);
    }

    $details_by_plan[] = $combined;

    // cache result for 30 minutes
    if (count($details_by_plan))
    {
      $redis->set($redis_key, json_encode($details_by_plan), 30 * 60);
    }

    return $details_by_plan;
  }

  /**
   * Connect to the database.
   *
   * @author bwalters@ultra.me
   */
  protected function connectToDatabase()
  {
    teldata_change_db();
  }

  /**
   * Given a customer_id and desired fields, will return a customer
   * from htt_customers_overlay_ultra.
   * @param integer $customer_id
   * @param array(string) $select_fields
   * @return stdclass
   * @throws \Exception
   * @author bwalters@ultra.me
   */
  protected function getCustomerByIdWithFields($customer_id = false, array $select_fields)
  {
    if (!$customer_id) $this->errException("ERR_API_INTERNAL: Customer_id is required", "MP0020");

    $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
      array(
        'select_fields'    => $select_fields,
        'customer_id'      => $customer_id
      )
    );

    $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

    if ( !$ultra_customer_data && !is_array($ultra_customer_data) && count($ultra_customer_data) == 0 )
    {
      $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
    }

    return $ultra_customer_data[0];;
  }

  /**
   * Return a customer by $msisdn
   *
   * @param $msisdn
   * @param array $fields
   * @return mixed
   */
  public function getCustomerFromMsisdn($msisdn, array $fields = array())
  {
    $customer = null;
    $customers = $this->getUltraCustomerFromMsisdn($msisdn);

    if (count($customers))
    {
      $customer = customers_select_customer(array(
        'customer_id'   => $customers->CUSTOMER_ID,
        'select_fields' => $fields
      ));
    }

    return $customer;
  }

  /**
   * Attempt to unblock a previously blocked user from api login abuse by msisdn.
   *
   * @param $customerId
   * @param $api
   * @param $agentName
   * @param $customerCareEventId
   * @return bool
   */
  public function removeBlockedUserByMsisdn($customerId, $api, $agentName = null, $customerCareEventId = null)
  {
    $customer = $this->getCustomerFromMsisdn($customerId, array('LOGIN_NAME'));

    if (!empty($customer))
    {
      $key = $api . "/" . $customer->LOGIN_NAME;
      $result = Utils\Redis\redis_reset_command_count($this->getRedis(), $key);

      if ($result)
      {
        $message = $this->composeRemovedBlockedUserMessage($agentName, $customerCareEventId);
        $message .= !empty($message) ? "Successfully unblocked $customer->LOGIN_NAME with msisdn $customerId from previously blocked api: $api" : '';

        dlog('', $message);
        return true;
      }
    }

    return false;
  }

  /**
   * Attempt to unblock a previously blocked user from api login abuse by username.
   *
   * @param $customerId
   * @param $api
   * @param $agentName
   * @param $customerCareEventId
   * @return bool
   */
  public function removeBlockedUserByUsername($customerId, $api, $agentName = null, $customerCareEventId = null)
  {
    $customer = find_customer(make_find_ultra_customer_query_anycosid(-1, $customerId));

    if (!empty($customer))
    {
      $key = $api . "/" . $customer->current_mobile_number;
      $result = Utils\Redis\redis_reset_command_count($this->getRedis(), $key);

      if ($result)
      {
        $message = $this->composeRemovedBlockedUserMessage($agentName, $customerCareEventId);
        $message .= !empty($message) ? "Successfully unblocked $customer->current_mobile_number with username $customerId from previously blocked api: $api" : '';

        dlog('', $message);
        return true;
      }
    }

    return false;
  }

  /**
   * Attempt to unblock a previously blocked user from api login abuse.
   *
   * @param $customerId
   * @param $api
   * @param $agentName
   * @param $customerCareEventId
   * @return bool
   */
  public function removeBlockedUser($customerId, $api, $agentName = null, $customerCareEventId = null)
  {
    $result = Utils\Redis\redis_reset_command_count($this->getRedis(), $api . "/" . $customerId);

    if ($result)
    {
      $message = $this->composeRemovedBlockedUserMessage($agentName, $customerCareEventId);
      $message .= !empty($message) ? "Successfully unblocked $customerId from previously blocked api: $api" : '';

      dlog('', $message);
      return true;
    }

    return false;
  }

  /**
   * Compose a debug message header for removing blocked users.
   *
   * @param $agentName
   * @param $customerCareEventId
   * @return string
   */
  private function composeRemovedBlockedUserMessage($agentName = null, $customerCareEventId = null)
  {
    $agentMessage = !empty($agentName) ? "Agent: $agentName, " : "";
    $eventIdMessage = !empty($customerCareEventId) ? "Customer Care EventID: $customerCareEventId, " : '';

    return $agentMessage . $eventIdMessage;
  }

  /**
   * @return Redis
   */
  protected function getRedis()
  {
    return is_null($this->redis) ? new Redis() : $this->redis;
  }

  /**
   * Get Ultra customer by their msisdn.
   *
   * @param $msisdn
   * @return mixed
   */
  protected function getUltraCustomerFromMsisdn($msisdn)
  {
    $customer = null;

    $customers = get_ultra_customers_from_msisdn($msisdn, array('CUSTOMER_ID', 'BRAND_ID'));
    if (count($customers)) $customer = $customers[0];

    return $customer;
  }

  /**
   * validateAPIPartner
   *
   * Returns true if the API partner matches with $string
   *
   * @return boolean
   */
  public function validateAPIPartner( $string )
  {
    return ! ! ( $string == $this->partner ) ;
  }

  public function dlog($options, $message)
  {
    return dlog($options, $message);
  }
}

