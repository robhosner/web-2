<?php

namespace Ultra\Lib\Api\Traits;

use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;
use Ultra\Mvne\Adapter;

trait FlexHandler
{
  protected abstract function errException($error, $error_code, $user_error=false);
  protected abstract function addToOutput($field, $value);

  /**
   * Validates a given invite code belongs to an existing family
   * @param string
   * @return null
   */
  public function validateInviteCode($inviteCode)
  {
    $result = $this->familyAPI->getFamilyIDByInviteCode($inviteCode);
    if (!$result->is_success()) {
      $errors = $result->get_errors();
      if (count($errors)) {
        return $this->errException($errors[0], 'FX0001', $errors[0]);
      } else {
        return $this->errException('Invalid invite code', 'FX0001', 'Invalid invite code');
      }
    } else if (empty($result->data_array['family_id'])) {
      return $this->errException('Invalid invite code', 'IN0002', 'Missing family_id');
    }
  }

  /**
   * Handles a family invite code during activation
   * Adds customer to the family by invite code
   * @param int
   * @param string
   * @return null
   */
  public function joinFamily($customerID, $inviteCode)
  {
    // joining family
    $result = $this->familyAPI->joinFamily($customerID, $inviteCode);
    if (!$result->is_success()) {
      $errors = $result->get_errors();
      if (count($errors)) {
        return $this->errException($errors[0], 'IN0002', $errors[0]);
      } else {
        return $this->errException('Failed to join family', 'IN0002', 'Failed to join family');
      }
    }
  }

  /**
   * Creates a new family for a customer
   * @param int
   * @return null
   */
  public function createFamily($customerID)
  {
    // creating family
    $result = $this->familyAPI->createCustomerFamily($customerID);
    if (!$result->is_success()) {
      $errors = $result->get_errors();
      if (count($errors)) {
        return $this->errException($errors[0], 'IN0002', $errors[0]);
      } else {
        return $this->errException('Failed to create family for customer', 'IN0002', 'Failed to create family');
      }
    }

    $this->addToOutput('invite_code', $result->data_array['invite_code']);
  }

  /**
   * Sets the data limit for a customer in a family
   * @param object
   * @param int
   * @param int
   * @return null
   */
  public function setDataLimit(SharedData $sharedData, $customerId, $percent)
  {
    // clamp percent
    if ($percent > 100) $percent = 100;
    if ($percent < 0)   $percent = 0;

    // if percent > 100, remove share cap
    $customer = \get_ultra_customer_from_customer_id($customerId, ['current_mobile_number']);
    if ( ! $customer) {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
    }

    // check for pending command invocation
    \Ultra\Lib\DB\ultra_acc_connect();

    $commandInvocation = new \CommandInvocation([ 'customer_id' => $customerId ]);

    if ( $commandInvocation->isOccupied() )
      return $this->errException('ERR_API_INVALID_ARGUMENTS: already a pending request for customer', 'CI0005');

    teldata_change_db();

    $actionUUID = getNewActionUUID('dealerportal ' . time());
    $control = new \Ultra\Lib\MiddleWare\ACC\ControlCommandManageCap($actionUUID);
    $params = [
      'customer_id' => $customerId,
      'MSISDN'      => $customer->current_mobile_number,
      'command'     => ($percent >= 100) ? 'remove' : 'add',
      'percent'     => $percent
    ];

    $result = $control->processCommand($params);
    $data   = json_decode($result->data_array['message']);

    if ( ! $data->body->success) {
      $errors = $result->get_errors();
      $error  = (count($errors)) ? $errors[0] : 'Unexpected error setting share cap limit';
      return $this->errException("ERR_API_INVALID_ARGUMENTS: $error", 'IN0001');
    }

    if ($result->is_failure()) {
      $errors = $result->get_errors();
      $error  = (count($errors)) ? $errors[0] : 'Unexpected error setting share cap limit';
      return $this->errException("ERR_API_INVALID_ARGUMENTS: $error", 'IN0001');
    }

    $result = $sharedData->setCustomerDataLimit($customerId, $percent);
    if ($result->is_failure()) {
      return $this->errException('ERR_API_INTERNAL: ' . $result->get_first_error(), 'IN0002', $result->get_errors());
    }
  }

  public function removeFromFamily($customerId, AccountsRepository $accountsRepository, FamilyAPI $familyAPI, SharedData $sharedData, Adapter $adapter, Configuration $configuration)
  {
    $account = $accountsRepository->getAccountFromCustomerId($customerId, ['COS_ID']);

    if (empty($account)) {
      return $this->errException('ERR_API_INTERNAL: Unable to find customers account.', 'ND0001');
    }

    $result = $familyAPI->getFamilyByCustomerID($customerId);

    if ($result->is_failure()) {
      return $this->errException('ERR_API_INTERNAL: ' . $result->get_first_error(), 'ND0001', $result->get_errors());
    }

    if (empty($result->data_array['id'])) {
      return $this->errException("ERR_API_INTERNAL: Unable to remove customer $customerId from family.", 'ND0001');
    }

    $familyId = $result->data_array['id'];
    $getBucketIdResult = $sharedData->getBucketIDByCustomerID($customerId);

    if ($getBucketIdResult->is_success() && !empty($getBucketIdResult->data_array['bucket_id'])) {
      $getBucketResult = $sharedData->getBucketByBucketID($getBucketIdResult->data_array['bucket_id']);

      if ($getBucketResult->is_success() && !empty($getBucketResult->data_array['ban'])) {
        $result = $adapter->mvneMakeitsoRemoveFromBan(
          $customerId,
          $configuration->getPlanFromCosId($account->cos_id),
          null,
          $getBucketResult->data_array['ban']
        );

        if (!$result['success']) {
          return $this->errException('ERR_API_INTERNAL: ' . implode(", ", $result['errors']), 'IN0002', $result['errors']);
        }
      } else {
        $this->addWarning("Customer $customerId does not have a ban.");
        dlog('', "Customer $customerId does not have a ban.");
      }
    } else {
      dlog('', "Customer $customerId is not part of a shared data bucket.");
    }

    $result = $familyAPI->removeFamilyMember($familyId, $customerId);

    if ($result->is_failure()) {
      $error = empty($result->get_first_error()) ? "Failed removing customerId $customerId from familyId $familyId." : $result->get_first_error();
      return $this->errException('ERR_API_INTERNAL: ' . $error, 'IN0002', $result->get_errors());
    }

    // create family for removed user
    $result = $familyAPI->createCustomerFamily($customerId);
    if ($result->is_failure()) {
      $error = empty($result->get_first_error()) ? "Failed creating family for customerId $customerId." : $result->get_first_error();
      return $this->errException('ERR_API_INTERNAL: ' . $error, 'IN0002', $result->get_errors());
    }

    return null;
  }
}
