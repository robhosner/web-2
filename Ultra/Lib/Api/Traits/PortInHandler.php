<?php

namespace Ultra\Lib\Api\Traits;

use Ultra\Messaging\Messenger;

trait PortInHandler
{
  public function sendPortInErrorMessage(Messenger $messagengerObj, $customerID, $phone, $contactPhone, $email)
  {
    if (!empty($phone)) {
      $messagengerObj->enqueueImmediateExternalSms($customerID, 'port_in_error', ['_to_msisdn' => $phone]);
      \logInfo('Sent error SMS to phone ' . $phone);
    }

    if (!empty($contactPhone)) {
      $messagengerObj->enqueueImmediateExternalSms($customerID, 'port_in_error', ['_to_msisdn' => $contactPhone]);
      \logInfo('Sent error SMS to contact phone ' . $contactPhone);
    }

    if (!empty($email)) {
      \logInfo('Sent error email to ' . $email);
    }
  }
}
