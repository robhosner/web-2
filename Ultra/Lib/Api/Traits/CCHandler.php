<?php

namespace Ultra\Lib\Api\Traits;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Exceptions\UnhandledCCProcessorException;
use Ultra\Exceptions\SaveMethodFailedException;

trait CCHandler
{
  public function validateAndSaveToken(
    Configuration $configuration,
    CustomerRepository $customerRepo,
    CreditCardRepository $creditCardRepo,
    array $params
  ) {
    // default to MeS
    $params['processor'] = !empty($params['processor']) ? $params['processor'] : 'mes';
    $params = $this->validateAndMapParameters($configuration, $params);

    // get customer
    $customer = $customerRepo->getCustomerById($params['customer_id'], ['brand_id', 'preferred_language'], true);
    if (!$customer) {
      throw new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
    }
    $this->customer = $customer;

    $params['brand_id'] = $customer->brand_id;
    $params['client_reference_number'] = $configuration->getShortNameFromBrandId($customer->brand_id);

    // credit card validator based on merchant
    $processorConfig = $configuration->getCCProcessorByName($params['processor']);
    if (empty($processorConfig)) {
      throw new UnhandledCCProcessorException('Missing processor configuration', 'IN0002');
    }

    // get credit card validator based on processor
    $ccValidator = $this->getProcessorValidator($configuration, $processorConfig);

    // validate credit card
    $creditCard = $ccValidator->validate($params);

    // save credit card
    $creditCardRepo->saveCreditCardInformation($creditCard);

    if (!$customerRepo->updateCustomerByCustomerId($creditCard->customer_id, (array) $creditCard)) {
      throw new SaveMethodFailedException('Failed to save the customer\'s information');
    }
  }

  public function validateAndMapParameters(Configuration $configuration, array $params)
  {
    // validate parameters
    $required = [
      'customer_id', 'account_cc_exp',
      'processor', 'bin', 'last_four', 'token'
    ];
    $optional = [
      'account_cc_cvv', 'cc_name', 'cc_address1', 'cc_address2', 
      'cc_city', 'cc_country', 'cc_state_region', 'cc_postal_code'
    ];

    // check required parameters
    $missing = [];
    foreach ($required as $field) {
      if (empty($params[$field])) {
        $missing[] = $field;
      }
    }
    if (!empty($missing)) {
      throw new MissingRequiredParametersException(__CLASS__, $missing);
    }

    // set optional fields, if needed
    foreach ($optional as $field) {
      if (!array_key_exists($field, $params) || trim($params[$field]) == '') {
        $params[$field] = null;
      }
    }

    // map API parameters
    $params['expires_date'] = $params['account_cc_exp'];
    unset($params['account_cc_exp']);
    $params['cvv'] = $params['account_cc_cvv'];
    unset($params['account_cc_cvv']);

    return $params;
  }

  public function getProcessorValidator(Configuration $configuration, array $processorConfig)
  {
    $merchantClass = '\\Ultra\\Lib\\DB\\Merchants\\' . $processorConfig['name'];
    if (!class_exists($merchantClass)) {
      throw new UnhandledCCProcessorException('Missing processor implementation', 'IN0002');
    }
    $merchantObj = new $merchantClass();

    $container = new \Ultra\Container\AppContainer();
    return new \Ultra\CreditCards\CreditCardValidator(
      $merchantObj,
      $container->make('Ultra\Utilities\Validator'),
      $configuration,
      $container->make('Ultra\Lib\DB\Merchants\Tokenizer')
    );
  }
}