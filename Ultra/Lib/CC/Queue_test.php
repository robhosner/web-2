<?php

require_once 'db.php';
require_once 'Ultra/Lib/CC/Queue.php';

$q = new \Ultra\Lib\CC\Queue();

abstract class AbstractTestStrategy
{
  abstract function test( $argv , $q );
}

class Test_CCQueue_getNext extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    return $q->getNext(empty($argv[2]) ? NULL : $argv[2]);
  }
}

class Test_CCQueue_processNext extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    $r = $q->processNext(empty($argv[2]) ? NULL : $argv[2]);

    if ( isset( $r->data_array['cc_transactions_id'] ) )
    {
      list( $status , $result ) = $q->peekTransaction( $r->data_array['cc_transactions_id'] );

      echo 'cc_transactions_id = '.$r->data_array['cc_transactions_id']."\n";
      echo 'status             = '.$status."\n";
      echo 'result             = '.$result."\n";
    }

    return $r;
  }
}

class Test_CCQueue_enqueue extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    $result = $q->enqueue(
      array(
        'cc_holder_tokens_id' => 14,
        'customer_id'         => 222,
        'charge_amount'       => 59,
        'type'                => 'type_test',
        'description'         => 'description_test'
      )
    );

    return $result;
  }
}

class Test_CCQueue_enqueueRecurringcharge extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    $r = $q->enqueueRecurringcharge(
      array(
        'customer_id'         => 32,
        'charge_amount'       => 59,
        'description'         => 'description_test'
      )
    );

    list( $status , $result ) = $q->peekTransaction( $r->data_array['cc_transactions_id'] );

    echo 'cc_transactions_id = '.$r->data_array['cc_transactions_id']."\n";
    echo 'status             = '.$status."\n";
    echo 'result             = '.$result."\n";

    return $result;
  }
}

class Test_CCQueue_enqueueCharge_and_wait extends AbstractTestStrategy
{
// DANGEROUS!
// THIS TEST ACTUALLY CHARGES A CUSTOMER'S CREDIT CARD
  function test( $argv , $q )
  {
    sleep(5);

    $sql = sprintf(
"SELECT TOP 1 t.CC_HOLDER_TOKENS_ID
 FROM ULTRA.CC_HOLDER_TOKENS t
 JOIN ULTRA.CC_HOLDERS h
 ON t.CC_HOLDERS_ID = h.CC_HOLDERS_ID
 WHERE h.CUSTOMER_ID = %d
 AND t.ENABLED = 1
 AND h.ENABLED = 1
 ORDER BY t.CC_HOLDER_TOKENS_ID DESC",
      $argv[2]
    );

    $row = find_first($sql);

    if ( empty( $row ) || ! is_object( $row ) || empty( $row->CC_HOLDER_TOKENS_ID ) )
    {
      echo "No credit card active on file for customer id ".$argv[2]."\n";
      exit;
    }

    echo "CC_HOLDER_TOKENS_ID = ".$row->CC_HOLDER_TOKENS_ID."\n";

    $r = $q->enqueueCharge(
      array(
        'customer_id'         => $argv[2],
        'charge_amount'       => $argv[3],
        'description'         => $argv[4],
        'gateway'             => \Ultra\UltraConfig\getPrimaryCCGateway()
      )
    );

    if ( $r->is_failure() )
    {
      echo "ccQueue::enqueue failed\n";
      exit;
    }

    print_r("ccQueue returned ".json_encode($r->data_array));

    echo "cc_transactions_id = ".$r->data_array['cc_transactions_id']."\n";

    list( $success , $timeout , $status , $result , $error ) = \Ultra\Lib\CC\waitForTransaction( $q , $r->data_array['cc_transactions_id'] );

    if ( $timeout )
    {
      echo "timeout\n";
      exit;
    }

    if( $success )
    {
      echo "success\n";
      exit;
    }

    if( ! $success )
    {
      echo "failure\n";
      exit;
    }
  }
}

class Test_CCQueue_enqueueCharge extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    $r = $q->enqueueCharge(
      array(
        #'cc_holder_tokens_id' => 7,
        'customer_id'         => 31,
        'charge_amount'       => 59,
        'description'         => 'description_test'
      )
    );

    list( $status , $result ) = $q->peekTransaction( $r->data_array['cc_transactions_id'] );

    echo 'cc_transactions_id = '.$r->data_array['cc_transactions_id']."\n";
    echo 'status             = '.$status."\n";
    echo 'result             = '.$result."\n";

    return $result;
  }
}

class Test_CCQueue_enqueueRefund extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    return $q->enqueueRefund(
      array(
        'cc_holder_tokens_id' => 14,
        'customer_id'         => 222,
        'charge_amount'       => 59,
        'description'         => 'description_test'
      )
    );
  }
}

class Test_CCQueue_enqueueVoid extends AbstractTestStrategy
{
  function test( $argv , $q )
  {
    return $q->enqueueVoid(
      array(
        'cc_holder_tokens_id' => 14,
        'customer_id'         => 222,
        'charge_amount'       => 59,
        'description'         => 'description_test'
      )
    );
  }
}


# perform test #


teldata_change_db();

$testClass = 'Test_CCQueue_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$r = $testObject->test( $argv , $q );

print_r($r);


/*
php Ultra/Lib/CC/Queue_test.php enqueue
php Ultra/Lib/CC/Queue_test.php enqueueCharge
php Ultra/Lib/CC/Queue_test.php enqueueCharge_and_wait $CUSTOMER_ID $AMOUNT $DESCRIPTION
php Ultra/Lib/CC/Queue_test.php enqueueRecurringcharge
php Ultra/Lib/CC/Queue_test.php enqueueRefund
php Ultra/Lib/CC/Queue_test.php enqueueVoid
php Ultra/Lib/CC/Queue_test.php getNext [CC_TRANSACTIONS_ID]
php Ultra/Lib/CC/Queue_test.php processNext [CC_TRANSACTIONS_ID]
*/

