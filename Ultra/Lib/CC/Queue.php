<?php

namespace Ultra\Lib\CC;

require_once 'db.php';
require_once 'Ultra/Lib/DB/Merchants/Tokenizer.php';
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/Util/Redis/CCQueue.php';
require_once 'Ultra/UltraConfig.php';

/**
 * Main class for CC Queue.
 *
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Phase+1+-+Credit+Card+State+Machine
 * 
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Credit Card Processor
 */

class Queue
{
  private $redis;
  private $redis_ccqueue;
  private $transaction;
  private $details; // destination, surcharges, description et al
  private $tokenizer;

  const TRANSACTION_TIMEOUT_SECONDS = 45;

  public function __construct( $params=array() )
  {
    // initialize private variables

    $this->redis = ( isset($params['redis']) )
                   ?
                   $params['redis']
                   :
                   new \Ultra\Lib\Util\Redis
                   ;

    $this->redis_ccqueue = new \Ultra\Lib\Util\Redis\CCQueue( $this->redis );
  }

  public function __destruct()
  {
  }

  /**
   * enqueueCharge
   *
   * @return Result object
   */
  public function enqueueCharge ( $params )
  {
    $params['type'] = 'CHARGE';

    return $this->enqueue ( $params );
  }

  /**
   * enqueueRecurringcharge
   *
   * @return Result object
   */
  public function enqueueRecurringcharge ( $params )
  {
    $params['type'] = 'RECURRINGCHARGE';

    return $this->enqueue ( $params );
  }

  /**
   * enqueueRefund
   *
   * @return Result object
   */
  public function enqueueRefund ( $params )
  {
    $params['type'] = 'REFUND';

    return $this->enqueue ( $params );
  }

  /**
   * enqueueVoid
   *
   * @return Result object
   */
  public function enqueueVoid ( $params )
  {
    $params['type'] = 'VOID';

    return $this->enqueue ( $params );
  }

  /**
   * enqueue
   *
   * Input:
   *  - 'customer_id'
   *  - other parameters are depending on ULTRA.CC_TRANSACTIONS.TYPE
   * Output:
   *  - 'unavailable' ( TRUE if the error is temporary and the transaction can be attempted again )
   *
   * @return Result object
   */
  public function enqueue ( $params )
  {
    dlog('', "(%s)", func_get_args());

    $error = NULL;

    // the opposite of being a fatal error
    $unavailable = FALSE;

    try
    {
      // we do not allow to process more than one CC queue element for the same customer at the same time
      if ( ! $this->redis_ccqueue->reserve_customer_id( $params['customer_id'] ) )
      {
        $unavailable = TRUE;
        throw new \Exception( 'ERR_API_INTERNAL: there is already a CC transaction in progress for the same customer');
      }

      // assign default gateway if not specified
      if ( ! isset($params['gateway']) )
        $params['gateway'] = \Ultra\UltraConfig\getPrimaryCCGateway();

      // add to ULTRA.CC_TRANSACTIONS
      $success = \add_to_ultra_cc_transactions( $params );

      if ( ! $success )
        throw new \Exception( 'ERR_API_INTERNAL: a DB error occurred' );

      // load from ULTRA.CC_TRANSACTIONS
      $this->transaction = \get_initated_cc_transaction( $params );

      if ( ! $this->transaction )
        throw new \Exception( 'ERR_API_INTERNAL: a DB error occurred' );

      dlog('',"transaction = %s",$this->transaction);

      // add to Redis queue
      $this->redis_ccqueue->setTransitionStatusInitiated( $this->transaction->CC_TRANSACTIONS_ID );
      $this->redis_ccqueue->add( time() , $this->transaction->CC_TRANSACTIONS_ID );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      $error = $e->getMessage();
    }

    return $error
           ?
           \make_error_Result( $error , array( 'unavailable' => $unavailable ) )
           :
           \make_ok_Result( array( 'cc_transactions_id' => $this->transaction->CC_TRANSACTIONS_ID ) )
           ;
  }

  /**
   * peekTransaction
   *
   * Gets STATUS and RESULT from Redis
   *
   * @return array
   */
  public function peekTransaction( $cc_transactions_id )
  {
    return array(
      $this->redis_ccqueue->getTransitionStatus( $cc_transactions_id ),
      $this->redis_ccqueue->getTransitionResult( $cc_transactions_id )
    );
  }

  /**
   * processNext
   *
   * Gets the next item to be processed, process it if found.
   *
   * @return Result object
   */
  public function processNext ( $cc_transactions_id=NULL )
  {
    $success = $this->getNext ( $cc_transactions_id );

    return ( $success )
           ?
           $this->process()
           :
           \make_ok_Result()
           ;
  }

  /**
   * getNext
   *
   * Gets the next item to be processed ( status = 'INITIATED' ), but does not process it yet.
   *
   * @return boolean
   */
  public function getNext ( $cc_transactions_id=NULL )
  {
    $success = TRUE;

    try
    {

      $cc_transactions_id = $this->redis_ccqueue->popNext( $cc_transactions_id );

      // there are no items to be processed at this time
      if ( !$cc_transactions_id )
        throw new \Exception( 'no items to be processed at this time' );

      // get transaction data from ULTRA.CC_TRANSACTIONS
      $this->transaction = \get_ultra_cc_transaction_and_token( $cc_transactions_id );

      if ( ! $this->transaction )
      {
        dlog('','ERR_API_INTERNAL: CC transaction not found');

        throw new \Exception( 'ERR_API_INTERNAL: CC transaction not found' );
      }

      // decode transaction details
      $this->details = json_decode($this->transaction->DETAILS, TRUE);
      unset($this->transaction->DETAILS);

      dlog('', 'transaction = %s, details = %s', $this->transaction, $this->details);
    }
    catch(\Exception $e)
    {
      #dlog('', $e->getMessage());

      $success = FALSE;
    }

    return $success;
  }

  /**
   * process
   *
   * Process item we just got from getNext.
   *
   * @return Result object
   */
  public function process()
  {
    $result = NULL;

    try
    {
      if ( empty($this->transaction) || ! is_object($this->transaction) )
        throw new \Exception( 'Transaction data not valid' );

      dlog('',"processing cc_transactions_id %s of type %s",$this->transaction->CC_TRANSACTIONS_ID,$this->transaction->TYPE);

      // clean up CHAR fields which may contain spaces
      $this->transaction->MERCHANT_ACCOUNT = trim( $this->transaction->MERCHANT_ACCOUNT );
      $this->transaction->GATEWAY          = trim( $this->transaction->GATEWAY          );

      // verify that the token is enabled
      if ( ! $this->transaction->ENABLED )
        throw new \Exception( 'Token is not enabled ('.$this->transaction->CC_HOLDER_TOKENS_ID.')' );

      $customer = get_ultra_customer_from_customer_id($this->transaction->CUSTOMER_ID, array('BRAND_ID'));

      // verify that the token is enabled and is assigned a brand
      if ( ! $customer || ! $customer->BRAND_ID)
        throw new \Exception( 'Cannot find BRAND ID for transaction customer ' . $this->transaction->CUSTOMER_ID );

      // client reference number [ULTRA,UNIVSION,...]
      $this->transaction->client_reference_number = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);

      $processMethod = 'process'.ucfirst(strtolower($this->transaction->TYPE));

      dlog('',"processMethod = $processMethod");

      // verify that the type is handled
      if ( !method_exists( $this , $processMethod ) )
        throw new \Exception( 'Type not handled' );

      $this->tokenizer = new \Ultra\Lib\DB\Merchants\Tokenizer();

      $this->redis_ccqueue->setTransitionStatusProcessing( $this->transaction->CC_TRANSACTIONS_ID );

      $result = $this->$processMethod();

      dlog('',"$processMethod returned %s - %s",$result->data_array,$result->get_errors());

      $result->data_array['cc_transactions_id'] = $this->transaction->CC_TRANSACTIONS_ID;
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      $result = \make_error_Result( $e->getMessage() );
    }

    return $result;
  }

  /**
   * processRecurringcharge
   *
   * @return Result object
   */
  private function processRecurringcharge()
  {
    $transactionData = $this->getTransactionData();

    $transactionData['recurring'] = TRUE;

    $result = $this->tokenizer->runCharge( $transactionData );

    $recordCreditTxnResult = $this->recordCreditTransaction($result);

    $result = $this->endProcess( $result );

    return $recordCreditTxnResult;
  }

  /**
   * processCharge
   *
   * @return Result object
   */
  private function processCharge()
  {
    $result = $this->tokenizer->runCharge( $this->getTransactionData() );

    $recordCreditTxnResult = $this->recordCreditTransaction($result);

    $result = $this->endProcess( $result );

    return $recordCreditTxnResult;
  }

  /**
   * processRefund
   *
   * @return Result object
   */
  private function processRefund ()
  {
    $result = $this->tokenizer->runRefund( $this->getTransactionData() );

    return $this->endProcess( $result );
  }

  /**
   * processVoid
   *
   * @return Result object
   */
  private function processVoid ()
  {
    $result = $this->tokenizer->runVoid( $this->getTransactionData() );

    return $this->endProcess( $result );
  }

  /**
   * endProcess
   *
   * Finalize transaction, update CC_TRANSACTIONS
   *
   * @return Result object
   */
  private function endProcess( $result )
  {
    $endResult = NULL;

    if ( $result->is_success() )
      $endResult = $this->completeAsApproved( $result->data_array );
    elseif( $result->is_timeout() )
      $endResult = $this->completeAsTimeout( $result->data_array , $result->get_errors() );
    else
      $endResult = $this->completeAsFailed( $result->data_array , $result->get_errors() );

    $this->redis_ccqueue->unreserve_customer_id( $this->transaction->CUSTOMER_ID );

    // $endResult->add_data_array( $result->data_array );

    return $result;
  }

  /**
   * getTransactionData
   *
   * Prepare data to be passed to $this->tokenizer
   *
   * @return array
   */
  private function getTransactionData()
  {
    $transactionData = array();

    foreach( (array) $this->transaction as $param => $value )
      $transactionData[ strtolower( $param ) ] = $value;

    return $transactionData;
  }

  public function getTransitionError()
  {
    return $this->redis_ccqueue->getTransitionError( $this->transaction->CC_TRANSACTIONS_ID );
  }

  /**
   * completeAsApproved
   *
   * Closes transaction with result = 'APPROVED'
   *
   * @return Result object
   */
  private function completeAsApproved( $params )
  {
    dlog('', "(%s)", func_get_args());

    $this->redis_ccqueue->setTransitionResultApproved( $this->transaction->CC_TRANSACTIONS_ID );

    $success = \update_cc_transactions(
      array(
        'cc_transactions_id'      => $this->transaction->CC_TRANSACTIONS_ID,
        'merchant_transaction_id' => $params['merchant_transaction_id'],
        'status'                  => 'COMPLETE',
        'result'                  => 'APPROVED'
      )
    );

    return $success ? \make_ok_Result() : \make_error_Result( 'ERR_API_INTERNAL: a DB error occurred' ) ;
  }

  /**
   * completeAsTimeout
   *
   * Closes transaction with result = 'TIMEOUT'
   *
   * @return Result object
   */
  private function completeAsTimeout( $params , $errors )
  {
    dlog('', "(%s)", func_get_args());

    $this->redis_ccqueue->setTransitionResultTimeout( $this->transaction->CC_TRANSACTIONS_ID );

    if ( count($errors) )
      $this->redis_ccqueue->setTransitionError( $this->transaction->CC_TRANSACTIONS_ID , $errors[0] );

    $success = \update_cc_transactions(
      array(
        'cc_transactions_id'      => $this->transaction->CC_TRANSACTIONS_ID,
        'merchant_transaction_id' => $params['merchant_transaction_id'],
        'status'                  => 'COMPLETE',
        'result'                  => 'TIMEOUT',
        'err_description'         => ( count($errors) ? $errors[0] : NULL )
      )
    );

    return $success ? \make_ok_Result() : \make_error_Result( 'ERR_API_INTERNAL: a DB error occurred' ) ;
  }

  /**
   * completeAsFailed
   *
   * Closes transaction with result = 'FAILED'
   *
   * @return Result object
   */
  private function completeAsFailed( $params , $errors )
  {
    dlog('', "(%s)", func_get_args());

    $this->redis_ccqueue->setTransitionResultFailed( $this->transaction->CC_TRANSACTIONS_ID );

    if ( count($errors) )
      $this->redis_ccqueue->setTransitionError( $this->transaction->CC_TRANSACTIONS_ID , $errors[0] );

    $success = \update_cc_transactions(
      array(
        'cc_transactions_id'      => $this->transaction->CC_TRANSACTIONS_ID,
        'merchant_transaction_id' => $params['merchant_transaction_id'],
        'status'                  => 'COMPLETE',
        'result'                  => 'FAILED',
        'err_description'         => ( count($errors) ? $errors[0] : NULL )
      )
    );

    return $success ? \make_ok_Result() : \make_error_Result( 'ERR_API_INTERNAL: a DB error occurred' ) ;
  }

  /**
   * transactionTimeoutSeconds
   */
  public function transactionTimeoutSeconds()
  {
    return self::TRANSACTION_TIMEOUT_SECONDS;
  }

  /**
   * setTransactionId
   *
   * Update CC_TRANSACTIONS.TRANSACTION_ID
   *
   * @return boolean
   */
  public function setTransactionId($transaction_id)
  {
    return \update_cc_transactions_from_htt_billing_history(
      array(
        'cc_transactions_id'      => $this->transaction->CC_TRANSACTIONS_ID,
        'billing_transaction_id'  => $transaction_id
      )
    );
  }


  /**
   * recordCreditTransaction
   *
   * credit account with the charge amount, record transaction and surcharges
   * @param object Result
   * @return object Result
   */
  private function recordCreditTransaction($result)
  {
    // do not record if previous operation failed
    if ($result->is_failure())
      return $result;

    // extract successfull transaction reference
    if (empty($result->data_array['merchant_transaction_id']))
      return \make_error_Result('missing merchange transaction ID');
    $reference = $result->data_array['merchant_transaction_id'];

    // calcualte credit and surcharge amounts
    $surcharge = 0;
    foreach (array('mts_tax', 'sales_tax', 'recovery_fee') as $type) // these are known surcharges
      if ( ! empty($this->details['surcharges'][$type]))
        $surcharge += $this->details['surcharges'][$type] / 100; // surcharges are in cents
    $amount = $this->transaction->CHARGE_AMOUNT - $surcharge;

    // add funds to subscriber's account for the amount charged less surcharges
    if ($error = $this->applyAccountCredit($amount))
      return \make_error_Result($error);

    // record transaction in HTT_BILLING_HISTORY
    if ( ! $transaction_id = $this->recordBillingHistory($amount, $surcharge, $reference))
      return \make_error_Result('failed to record transaction in HTT_BILLING_HISTORY');

    // update CC_TRANSACTIONS.TRANSACTION_ID
    if ( ! $this->setTransactionId($transaction_id))
      return \make_error_Result('failed to cross reference CC_TRANSACTIONS to HTT_BILLING_HISTORY');

    // record surcharges in ULTRA.SURCHARGE_HISTORY
    if ($error = $this->recordSurchargeHistory($transaction_id))
      return \make_error_Result($error);

    return \make_ok_Result($result->data_array);
  }


  /**
   * applyAccountCredit
   *
   * apply funds to subscriber's account
   * @param float amount to apply, in dollars
   * @return string NULL on success or error message on failure
   */
  private function applyAccountCredit($amount)
  {
    // compose appropriate query based on destination
    if ($this->details['destination'] == MONTHLY)
      $query = htt_customers_overlay_ultra_update_query(array(
        'customer_id'             => $this->transaction->CUSTOMER_ID,
        'balance_to_add'          => $amount)
      );
    elseif ($this->details['destination'] == WALLET)
      $query = accounts_update_query(array(
        'customer_id'             => $this->transaction->CUSTOMER_ID,
        'add_to_balance'          => $amount,
        'last_recharge_date_time' => 'now')
      );
    else
      return "Invalid destination {$this->details['destination']}";

    return is_mssql_successful(logged_mssql_query($query)) ? NULL : 'DB error while applying funds to customer account';
  }


  /**
   * recordBillingHistory
   *
   * record transaction in HTT_BILLING_HISTORY
   * @param float credit amount to apply, in dollars
   * @param float surchage amount, in dollars
   * @param string CC transaction reference as received from CC processor
   * @return integer HTT_BILLING_HISTORY.TRANSACTION_ID on success or NULL on failure
   */
  private function recordBillingHistory($amount, $surcharge, $reference)
  {
    // required parmaetes
    $params = array(
    'customer_id'                   => $this->transaction->CUSTOMER_ID,
    'date'                          => 'now',
    'cos_id'                        => $this->details['cos_id'],
    'entry_type'                    => 'LOAD',
    'stored_value_change'           => $this->details['destination'] == MONTHLY ? $amount : '0',
    'balance_change'                => $this->details['destination'] == WALLET  ? $amount : '0',
    'package_balance_change'        => '0',
    'charge_amount'                 => $amount + $surcharge,
    'reference'                     => $reference,
    'reference_source'              => get_reference_source('WEBCC'),
    'detail'                        => $this->details['detail'],
    'description'                   => $this->transaction->DESCRIPTION,
    'result'                        => 'COMPLETE',
    'source'                        => 'WEBCC',
    'store_zipcode'                 => $this->details['zipcode'],
    'is_commissionable'             => $this->details['commissionable'],
    'output_identity'               => TRUE,
    'commissionable_charge_amount'  => $amount,
    'surcharge_amount'              => $surcharge);

    // add optional parameters
    foreach (array('store_id', 'clerk_id', 'terminal_id') as $name)
      if (isset($this->details[$name]))
        $params[$name] = $this->details[$name];

    $result = mssql_fetch_all_objects(logged_mssql_query(htt_billing_history_insert_query($params)));
    return empty($result[0]->TRANSACTION_ID) ? NULL : $result[0]->TRANSACTION_ID;
  }


  /**
   * recordSurchargeHistory
   *
   * save surchages in ULTRA.SURCHARGE_HISTORY
   * @param integer HTT_BILLING_HISTORY ID
   * @return string NULL on success or error message on failure
   */
  private function recordSurchargeHistory($transaction_id)
  {
    if (empty($this->details['surcharges']))
      return NULL;

    $params = array(
      'customer_id'     => $this->transaction->CUSTOMER_ID,
      'entry_type'      => 'LOAD',
      'transaction_id'  => $transaction_id,
      'location'        => $this->details['zipcode']
    );

    $success = add_rows_to_ultra_surcharge_history($this->details['surcharges'], $params);

    return $success ? NULL : 'failed to record surchage history';
  }
}


/**
 * waitForTransaction
 *
 * @return array
 */
function waitForTransaction( $ccQueue , $cc_transactions_id )
{
  list( $status , $result ) = $ccQueue->peekTransaction( $cc_transactions_id );

  dlog('',"cc_transactions_id = %s ; status = %s ; result = %s",$cc_transactions_id,$status,$result);

  $timeout    = FALSE;
  $success    = FALSE;
  $error      = '';
  $start_time = time();
  $max_time   = time() + $ccQueue->transactionTimeoutSeconds();

  // wait for the transaction to be completed
  while( ( time() < $max_time )
      && ( $status != 'COMPLETE' )
  )
  {
    sleep(1);

    list( $status , $result ) = $ccQueue->peekTransaction( $cc_transactions_id );
  }

  if ( $status == 'COMPLETE' )
  {
    if ( $result == 'APPROVED' )
      $success = TRUE;
    elseif ( $result == 'TIMEOUT' )
    {
      $timeout = TRUE;
      $error   = 'TIMEOUT';
    }
    else
      $error = $ccQueue->getTransitionError();
  }
  else
  {
    $timeout = TRUE;
    $error   = 'TIMEOUT';
    $result  = 'TIMEOUT';
  }

  dlog('',"cc_transactions_id = %s ; status = %s ; result = %s ; elapsed time = %s",$cc_transactions_id,$status,$result,( time() - $start_time ));

  return array( $success , $timeout , $status , $result , $error );
}

