<?php

include_once 'Ultra/Lib/MQ/CommandChannel.php';

$commandChannel = new \Ultra\Lib\MQ\CommandChannel;

echo \Ultra\Lib\MQ\CommandChannel::commandChannelTypeASPMW."\n\n";
echo \Ultra\Lib\MQ\CommandChannel::commandChannelTypeASPSOAP."\n\n";
echo \Ultra\Lib\MQ\CommandChannel::callbackChannelPrefix."\n\n";
echo \Ultra\Lib\MQ\CommandChannel::commandChannelPrefix."\n\n";

echo $commandChannel->outboundASPMWCommandAsynchChannel()."\n";
echo $commandChannel->outboundASPMWCommandSynchChannel()."\n";
echo $commandChannel->inboundASPMWCommandAsynchChannel()."\n";
echo $commandChannel->inboundASPMWCommandSynchChannel()."\n";
echo $commandChannel->outboundASPSOAPCommandSynchChannel()."\n";
echo $commandChannel->outboundASPSOAPCommandAsynchChannel()."\n";
echo $commandChannel->inboundASPSOAPCommandSynchChannel()."\n";
echo $commandChannel->inboundASPSOAPCommandAsynchChannel()."\n";

echo $commandChannel->inboundASPCallbackResponseChannelNotBlocking()."\n";
echo $commandChannel->inboundASPCallbackResponseChannelBlocking()."\n";

list( $requestChannelName , $replyChannelName ) = $commandChannel->createNewASPMWCommandSynchChannels();

echo "createNewASPMWCommandSynchChannels : ( $requestChannelName , $replyChannelName )\n\n";

$requestChannelName = $commandChannel->createNewOutboundASPMWCommandAsynchChannel();

echo "createNewOutboundASPMWCommandAsynchChannel : $requestChannelName\n\n";

$replyChannelName = $commandChannel->createNewInboundASPMWCommandAsynchChannel();

echo "createNewInboundASPMWCommandAsynchChannel : $replyChannelName\n\n";

list( $requestChannelName , $replyChannelName ) = $commandChannel->createNewASPSOAPCommandSynchChannels();

echo "createNewASPSOAPCommandSynchChannels : ( $requestChannelName , $replyChannelName )\n\n";

$requestChannelName = $commandChannel->createNewOutboundASPSOAPCommandAsynchChannel();

echo "createNewOutboundASPSOAPCommandAsynchChannel : $requestChannelName\n\n";

$replyChannelName = $commandChannel->createNewInboundASPSOAPCommandAsynchChannel();

echo "createNewInboundASPSOAPCommandAsynchChannel : $replyChannelName\n\n";

?>
