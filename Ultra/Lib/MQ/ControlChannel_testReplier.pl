use strict;
use warnings;


use Ultra::Lib::MQ::ControlChannel;


# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/ControlChannel_testReplier.log';


# instantiate Ultra::Lib::MQ::ControlChannel
my $mq_cc = Ultra::Lib::MQ::ControlChannel->new();


# select an outbound request channel + message
my $getNextOutboundMessageResult = $mq_cc->getNextOutboundMessage();

unless ( @$getNextOutboundMessageResult ) { die "No outbound request channel found.\n"; }

my ( $nextOutboundControlChannel , $message ) = @$getNextOutboundMessageResult;

$mq_cc->log("outbound channel = $nextOutboundControlChannel");
$mq_cc->log("outbound message = $message");

my $anInboundRequestMessage = '[' . time . '] 456 def !!!';
my $nextInboundControlChannel = $mq_cc->toInbound( $nextOutboundControlChannel );

$mq_cc->log("inbound channel = $nextInboundControlChannel");
$mq_cc->log("inbound message = $anInboundRequestMessage");

my $controlUUID = $mq_cc->sendToControlChannel( $nextInboundControlChannel , $anInboundRequestMessage );

$mq_cc->log("controlUUID = $controlUUID");


__END__


