package Ultra::Lib::MQ::ControlChannel;


use strict;
use warnings;


use Time::HiRes qw( usleep );


use base qw(Ultra::Lib::MQ::EndPoint);


=head1

==== Control Channel methods ====

=cut


=head4 controlChannelTypeACCMW , controlChannelTypeULTRAMW , outboundControlSynchChannel , inboundControlSynchChannel, inboundACCMWControlChannel, outboundACCMWControlChannel, inboundUltraMWControlChannel, outboundUltraMWControlChannel

  Channel Set Names.
  The methods controlChannelTypeUltraMW, inboundUltraMWControlChannel, outboundUltraMWControlChannel are not supposed to be used by the Perl layer.

=cut
sub controlChannelTypeACCMW   { return 'ACC_MW';   }
sub controlChannelTypeUltraMW { return 'ULTRA_MW'; }
sub controlChannelTypeSMSMW   { return 'SMS_MW';   }

sub outboundControlSynchChannel
{
  my ($this,$type) = @_;

  return $type.'/OUTBOUND/SYNCH';
}

sub inboundControlSynchChannel
{
  my ($this,$type) = @_;

  return $type.'/INBOUND/SYNCH';
}

sub inboundSMSMWControlChannel
{
  my ($this) = @_;

  return $this->inboundControlSynchChannel( $this->controlChannelTypeSMSMW() );
}

sub outboundSMSMWControlChannel
{
  my ($this) = @_;

  return $this->outboundControlSynchChannel( $this->controlChannelTypeSMSMW() );
}

sub inboundACCMWControlChannel
{
  my ($this) = @_;

  return $this->inboundControlSynchChannel( $this->controlChannelTypeACCMW() );
}

sub outboundACCMWControlChannel
{
  my ($this) = @_;

  return $this->outboundControlSynchChannel( $this->controlChannelTypeACCMW() );
}

sub inboundUltraMWControlChannel
{
  my ($this) = @_;

  return $this->inboundControlSynchChannel( $this->controlChannelTypeUltraMW() );
}

sub outboundUltraMWControlChannel
{
  my ($this) = @_;

  return $this->outboundControlSynchChannel( $this->controlChannelTypeUltraMW() );
}


sub createNewSMSMWControlChannels
{
  my ($this) = @_;

  return $this->createNewControlSynchChannels( $this->controlChannelTypeSMSMW() );
}


=head4 createNewACCMWControlChannels

  Creates new ACC MW Control Channels

=cut
sub createNewACCMWControlChannels
{
  my ($this) = @_;

  return $this->createNewControlSynchChannels( $this->controlChannelTypeACCMW() );
}


=head4 createNewUltraMWControlSynchChannels

  Creates new Ultra MW Control Channels

=cut
sub createNewUltraMWControlSynchChannels
{
  my ($this) = @_;

  return $this->createNewControlSynchChannels( $this->controlChannelTypeUltraMW() );
}


=head4 createNewControlSynchChannels

  Creates new Control Channels.
  Used in Request-Reply pattern to create both Request channel and Reply channel.
  Returns 0 for failure, the channel names in case of success.

=cut
sub createNewControlSynchChannels
{
  my ($this, $messageQueue) = @_;

  my $newChannelName = $this->_getNewChannelName();

  my $requestChannelName = 'out_'.$newChannelName;
  my $replyChannelName   = 'in_'.$newChannelName;

  $requestChannelName = $this->createControlChannel( $requestChannelName , $this->outboundControlSynchChannel( $messageQueue ) );
  $replyChannelName   = $this->createControlChannel( $replyChannelName   , $this->inboundControlSynchChannel( $messageQueue ) );

  return ( $requestChannelName , $replyChannelName );
}


=head4 getICCIDByCorrelationID

  Gets $iccid from $correlationID

=cut
sub getICCIDByCorrelationID
{
  my ($this,$correlationID) = @_;

  return $this->_redis()->get( 'acc/correlationID/'.$correlationID );
}


=head4 setICCIDByCorrelationID

  Associated $correlationID with $iccid

=cut
sub setICCIDByCorrelationID
{
  my ($this,$iccid,$correlationID) = @_;

  #$this->_redis()->set(    'acc/correlationID/'.$correlationID , $iccid );
  #$this->_redis()->expire( 'acc/correlationID/'.$correlationID , 60*60 );
  $this->_redis()->set( 'acc/correlationID/'.$correlationID , $iccid , 60*60 );
}


=head4 existsControlChannel

  Returns a true value if the channel exists.
  Invokes Redis primitive exists.

=cut
sub existsControlChannel
{
  my ($this,$channelName) = @_;

  return $this->_redis()->exists( 'CN_' . $channelName );
}


=head4 getChannelTypeMembers

  Invoke Redis primitive smembers which returns all the members of the set value stored at $messageQueue (layer).

=cut
sub getChannelTypeMembers
{
  my ($this,$messageQueue) = @_;

  return $this->_redis()->smembers( $messageQueue );
}


=head4 createControlChannel

  Attempts to create a Control Channel, given a channel name.
  Returns 0 for failure, the Control Channel name in case of success.
  $channelName (required) = name of the Control Channel we want to create
  $messageQueue  (required) = name of the Control Channel type (layer) we want this channel to be part of.
  After this call:
    The string 'CN_' . $channelName will be the ID of a new Control Channel
    The status of this Control Channel will be stored with key 'CS_' . $channelName
    Control Channel status can be 'created', 'filled', 'used', 'reserved_$PID'.
    'used' Control Channels are eligible for GC.

=cut
sub createControlChannel
{
  my ($this,$channelName,$messageQueue) = @_;

  #TODO: do we want to validate $messageQueue ?

  my $return = 0;

  $this->log( 'createControlChannel: ' . $channelName);

  if ( ! $this->existsControlChannel($channelName) )
  {
    # the channel does not exists, we create it

    # allocates channel name
    #$this->_redis()->set( 'CN_' . $channelName , 'channel' );
    #$this->_redis()->expire( 'CN_' . $channelName , $this->getMessageTTLSeconds() );
    $this->_redis()->set( 'CN_' . $channelName , 'channel' , $this->getMessageTTLSeconds() );

    if ( $this->reserveControlChannel( $channelName ) )
    {
      #$this->log("Process $$ reserved channel $channelName");
      #$this->log("Setting CN_".$channelName." to channel");

      # add this channel to the $messageQueue set (layer)
      $this->_redis()->sadd( $messageQueue , $channelName );
      $this->log("Adding $channelName to set $messageQueue");

      # release channel
      $this->releaseControlChannel( $channelName , 'created' );

      $return = $channelName;
    }
    else
    {
      #$this->log("Process $$ could not reserve channel $channelName");

      $return = 0;
    }
  }
  else
  {
    #$this->log("channel $channelName already exists");
  }

  return $return;
}


=head4 peekControlChannel

  Attempts to read the message sent through the given channel.
  The message will NOT be removed from the channel.
  Returns an empty string for failure, the message in case of success.

=cut
sub peekControlChannel
{
  my ($this,$channelName) = @_;

  my $return = '';

  if ( $this->_redis()->exists( 'CN_' . $channelName ) # channel name
    && $this->_redis()->exists( 'CS_' . $channelName ) # channel status
    && $this->_redis()->exists( 'CM_' . $channelName ) # channel message
    && ( $this->_redis()->get( 'CS_' . $channelName ) eq 'filled' )
  )
  {
    # read the CONTROL_UUID contained in the channel
    my $controlUUID = $this->_redis()->get( 'CM_' . $channelName );

    if ( $controlUUID )
    {
      # get the message pointed by the CONTROL_UUID
      # we assume that a message is a ``true value``
      $return = $this->_redis()->get( $controlUUID );
    }
  }

  return $return;
}


=head4 assignControlChannelStatus

  Modify status of a Control Channel

=cut
sub assignControlChannelStatus
{
  my ($this,$channelName,$status) = @_;

  #$this->log("Setting CS_".$channelName." to $status");

  #$this->_redis()->set( 'CS_' . $channelName , $status );
  #$this->_redis()->expire( 'CS_' . $channelName , $this->getMessageTTLSeconds() );
  $this->_redis()->set( 'CS_' . $channelName , $status , $this->getMessageTTLSeconds() );
}


=head4 peekControlChannelStatus

  Returns the status of a channel.
  Returns an empty string for non-existing channels.

=cut
sub peekControlChannelStatus
{
  my ($this,$channelName) = @_;

  my $return = '';

  if ( $this->_redis()->exists( 'CN_' . $channelName )
    && $this->_redis()->exists( 'CS_' . $channelName )
  )
  {
    $return = $this->_redis()->get( 'CS_' . $channelName );
  }

  return $return;
}


sub sendToSMSControlChannel
{
  my ($this,$channelName,$message) = @_;

  return $this->sendToControlChannel($channelName,$message);
}


=head4 sendToControlChannel

  Sends a new message through the given channel.
  Returns '' for failure, the generated CONTROL_UUID in case of success.
  a. create a unique identifier for the control message (CONTROL_UUID)
  b. associate the CONTROL_UUID with $message in Redis (using the primitive set)
  c. populate the channel with CONTROL_UUID
  d. change the channel state to 'filled'

=cut
sub sendToControlChannel
{
  my ($this,$channelName,$message) = @_;

  # we do not accept empty messages
  if ( ! $message )
  {
    $this->log("sendToControlChannel : message cannot be empty");

    return '';
  }

  my $return = '';

  if ( $this->_redis()->exists( 'CN_' . $channelName ) )
  {
    if ( $this->reserveControlChannel( $channelName ) )
    {
      #$this->log("Process $$ reserved channel $channelName");

      # create a unique identifier for the control message
      my $controlUUID = $this->_getNewControlUUID();

      # associate the CONTROL_UUID with $message
      #$this->_redis()->set($controlUUID,$message);
      #$this->_redis()->expire($controlUUID,$this->getMessageTTLSeconds());
      $this->_redis()->set($controlUUID,$message,$this->getMessageTTLSeconds());

      #$this->log("Setting $controlUUID to $message");

      # place the CONTROL_UUID in the appropriate container
      #$this->_redis()->set( 'CM_' . $channelName , $controlUUID );
      #$this->_redis()->expire( 'CM_' . $channelName , $this->getMessageTTLSeconds() );
      $this->_redis()->set( 'CM_' . $channelName , $controlUUID , $this->getMessageTTLSeconds() );
      #$this->log("Setting CM_".$channelName." to $controlUUID");

      # release channel
      $this->releaseControlChannel( $channelName , 'filled' );

      $return = $controlUUID;
    }
    else
    {
      $this->log("Process $$ could not reserve channel $channelName");
    }
  }
  else
  {
    $this->log("Channel $channelName does not exist");
  }

  return $return;
}


=head4 isControlChannelFilled

  Returns a true value if the Control Channel exists and there is a message waiting to be extracted.

=cut
sub isControlChannelFilled
{
  my ($this, $controlChannel ) = @_;

  return ( $this->peekControlChannelStatus( $controlChannel ) eq 'filled' );
}


sub getNextOutboundSMSMessage
{
  my ($this,$ratio) = @_;

  $ratio ||= 0;

  my $return = [];

  # outer layer (ACCMW - SMS)
  my $messageQueue = $this->outboundControlSynchChannel( $this->controlChannelTypeSMSMW() );

  my $outboundControlSynchChannels = $this->_redis()->smembers( $messageQueue );

  my $exitLoop = 0;

  # check all filled channels
  while( ! $exitLoop )
  {
    my $nextOutboundControlChannel = '';

    if ( $outboundControlSynchChannels )
    {
      #use Data::Dumper; $this->log("channels = ".Dumper($outboundControlSynchChannels));

      # get the first
      #$nextOutboundControlChannel = shift @$outboundControlSynchChannels;

      # get the next channel, given a $ratio
      $nextOutboundControlChannel = splice(@$outboundControlSynchChannels,$ratio,1);
    }

    if ( $nextOutboundControlChannel )
    {
      # there is a message in this outbound ACC MW Control Channel
      if ( $this->isControlChannelFilled( $nextOutboundControlChannel ) )
      {
        $this->log("ratio = $ratio ; messageQueue = $messageQueue ; nextOutboundControlChannel = $nextOutboundControlChannel ; outboundSMSMWControlChannel = ".$this->outboundSMSMWControlChannel() );

        # we extract the message
        my $message = $this->popControlChannel( $nextOutboundControlChannel , $this->outboundSMSMWControlChannel() );

        if ( $message )
        {
          # success: we will return the channel name and the message
          $return = [ $nextOutboundControlChannel , $message ];

          $exitLoop = 1;
        }
      }
    }
    else
    {
      # there are no more outbound ACC MW Control Channels, we exit
      $exitLoop = 1;
    }
  }

  return $return;
}

=head4 getNextOutboundMessage

  Extracts a message and the channel name from a filled outbound ACC MW Control Channel.
  Returns an empty array in case of failure.

=cut
sub getNextOutboundMessage
{
  my ($this) = @_;

  my $return = [];

  # outer layer (ACCMW)
  my $messageQueue = $this->outboundControlSynchChannel( $this->controlChannelTypeACCMW() );

  my $outboundControlSynchChannels = $this->_redis()->smembers( $messageQueue );

  my $exitLoop = 0;

  # check all filled channels
  while( ! $exitLoop )
  {
    my $nextOutboundControlChannel = '';

    if ( $outboundControlSynchChannels )
    {
      $nextOutboundControlChannel = shift @$outboundControlSynchChannels;
    }

    if ( $nextOutboundControlChannel )
    {
      # there is a message in this outbound ACC MW Control Channel
      if ( $this->isControlChannelFilled( $nextOutboundControlChannel ) )
      {
        $this->log("messageQueue = $messageQueue");

        # we extract the message
        my $message = $this->popControlChannel( $nextOutboundControlChannel , $this->outboundACCMWControlChannel() );

        if ( $message )
        {
          # success: we will return the channel name and the message
          $return = [ $nextOutboundControlChannel , $message ];

          $exitLoop = 1;
        }
      }
    }
    else
    {
      # there are no more outbound ACC MW Control Channels, we exit
      $exitLoop = 1;
    }
  }

  return $return;
}


=head4 waitForControlMessage

  Used in synchronous Request-Reply pattern.
  To be used after sending an outbound message on the Request Channel.
  Waits for an inbound message on the Reply Channel for the specified $messageQueue (layer).
  Returns an empty string for failure, the message in case of success.
  Errors are handled by using Ultra::Lib::Util::Error::Setter

=cut
sub waitForControlMessage
{
  my ($this,$channelName,$messageQueue) = @_;

  $this->log("waitForControlMessage - $channelName,$messageQueue");

  my $return = '';

  my $exitLoop = 0;

  my $timeoutTime = time + $this->getRequestReplyTimeoutSeconds();

  my $channelPingFrequencySeconds = $this->getChannelPingFrequencySeconds();

  while( ! $exitLoop )
  {
    $exitLoop = $this->isControlChannelFilled( $channelName );

    if ( $exitLoop )
    {
      # we got a reply on the Reply Channel
      $return = $this->popControlChannel( $channelName , $messageQueue );
    }
    else
    {
      $this->log( "Waiting on channel $channelName..." );

      # check for timeout
      if ( $timeoutTime < time )
      {
        $exitLoop = 1;
        $this->addError("waitForControlMessage timed out after ".$this->getRequestReplyTimeoutSeconds()." seconds");
      }
    }

    sleep $channelPingFrequencySeconds unless $exitLoop;
  }

  return $return;
}


=head4 releaseControlChannel

  Release Control Channel we previously reserved using $$.

=cut
sub releaseControlChannel
{
  my ($this,$channelName,$status) = @_;

  $this->assignControlChannelStatus( $channelName , $status );
}


=head4 reserveControlChannel

  Reserve Control Channel using $$

=cut
sub reserveControlChannel
{
  my ($this,$controlChannel) = @_;

  my $success = 0;

  my $currentStatus = $this->peekControlChannelStatus( $controlChannel );

  if ( ( $currentStatus !~ /^reserved / ) && ( $currentStatus ne 'used' ) )
  {
    $this->assignControlChannelStatus( $controlChannel , "reserved $$" );

    usleep(1000);

    my $currentStatus = $this->peekControlChannelStatus( $controlChannel );

    $success = ( $currentStatus eq "reserved $$" );
  }

  return $success;
}


=head4 popControlChannel

  Attempts to read the message sent through the given channel.
  The message will be removed from the channel.
  Returns an empty string for failure, the message in case of success.
  $messageQueue must be given so that we remove the channel from the channel set (layer).

  'CS_' . $channelName : {created} -->> {filled} -->> {reserved $$} -->> {used}

=cut
sub popControlChannel
{
  my ($this,$channelName,$messageQueue) = @_;

  $this->log("popControlChannel - $channelName,$messageQueue");

  my $return = '';

  if ( $this->_redis()->exists( 'CN_' . $channelName )
    && $this->_redis()->exists( 'CS_' . $channelName )
    && $this->_redis()->exists( 'CM_' . $channelName )
    && ( $this->_redis()->get( 'CS_' . $channelName ) eq 'filled' )
  )
  {
    # read the CONTROL_UUID contained in the channel
    my $controlUUID = $this->_redis()->get( 'CM_' . $channelName );

    if ( $controlUUID )
    {
      # get the message pointed by the CONTROL_UUID
      # we assume that every message is a ``true value``
      $return = $this->_redis()->get( $controlUUID );

      if ( $this->reserveControlChannel( $channelName ) )
      {
        #$this->log("Process $$ reserved channel $channelName");

        # remove the message from the channel
        $this->_redis()->del( 'CM_' . $channelName );

        # remove channel from channel set
        $this->_redis()->srem( $messageQueue , $channelName );
        #$this->log("Removing $channelName from $messageQueue");

        # release channel
        $this->releaseControlChannel( $channelName , 'used' );
      }
      else
      {
        #$this->log("Process $$ could not reserve channel $channelName");

        $return = '';
      }
    }
  }

  #unless( $return ) { $this->log( "popControlChannel failed" ); }

  return $return;
}


1;


__END__


