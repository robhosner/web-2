<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ControlChannel.php");


$mq_sms = new \Ultra\Lib\MQ\ControlChannel;

echo $mq_sms->outboundSMSMWControlChannel()."\n";
echo $mq_sms->inboundSMSMWControlChannel()."\n";

list($c_outbound,$c_inbound) = $mq_sms->createNewSMSMWControlChannels();

print "($c_outbound,$c_inbound)\n";

print $mq_sms->peekControlChannelStatus( $c_outbound )."\n"; # 'created'

$anOutboundRequestMessage = 'hello from ControlChannel_SMS_test.php';                                                                                                                                            
                                                                                                                                                                                                                   
# send outbound Request                                                                                                                                                                                            
$controlUUID = $mq_sms->sendToSMSControlChannel( $c_outbound , $anOutboundRequestMessage );                                                                                                                      
                                                                                                                                                                                                                   
print "$controlUUID\n";                                                                                                                                                                                            
                                                                                                                                                                                                                   
$peekControlChannelResult = $mq_sms->peekControlChannel( $c_outbound );                                                                                                                                          
                                                                                                                                                                                                                   
print "$peekControlChannelResult\n"; # $anOutboundRequestMessage

print $mq_sms->peekControlChannelStatus( $c_outbound )."\n"; # 'filled'

# extract SMS MW outbound message
$popControlChannelResult = $mq_sms->popControlChannel( $c_outbound , $mq_sms->outboundSMSMWControlChannel() );

print "$popControlChannelResult\n"; # $anOutboundRequestMessage

$anInboundReplyMessage = 'how do you do ControlChannel_SMS_test.php';

# send inbound Request
$controlUUID = $mq_sms->sendToSMSControlChannel( $c_inbound , $anInboundReplyMessage );

print "$controlUUID\n";

$peekControlChannelResult = $mq_sms->peekControlChannel( $c_inbound );

print "$peekControlChannelResult\n"; # $anInboundReplyMessage

print $mq_sms->peekControlChannelStatus( $c_inbound )."\n"; # 'filled'

$popControlChannelResult = $mq_sms->popControlChannel( $c_inbound , $mq_sms->inboundSMSMWControlChannel() );

print "$popControlChannelResult\n"; # $anInboundReplyMessage

?>
