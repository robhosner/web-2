<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ListEndPoint.php");

$msisdn = ( empty($argv[1]) ) ? '1001001000' : $argv[1] ;

$listEndPoint = \Ultra\Lib\MQ\getSMSListEndPoint();

$listEndPoint->setRequestReplyTimeoutSeconds(60);

echo time()."\n";

$sms_text = 'Здравствуйте!';

$data = array(
  'actionUUID' => 'a'.time(),
  'header'     => 'SendSMS',
  'body'       => array(
    'msisdn'             => $msisdn, #'6572218070',
    'preferred_language' => 'en',
    'sms_text'           => $sms_text
  )
);

$message = $listEndPoint->buildMessage( $data );

#echo "] message = $message\n";

$outboundMessageKey = $listEndPoint->pushMessage( $message );

echo "] outboundMessageKey = $outboundMessageKey\n";

// stop here if you need the SendSMS to be asynchronous

$result = $listEndPoint->waitForReplyMessage( $outboundMessageKey );

echo time()."\n";

#print_r( $result );

if ( $result->is_failure() )
  print_r( $result->get_errors() );
else
  print_r( $result->data_array );

