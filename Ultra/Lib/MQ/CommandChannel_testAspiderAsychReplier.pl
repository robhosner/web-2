use strict;
use warnings;


use Data::Dumper;
use JSON::XS;
use Ultra::Lib::MQ::CommandChannel;


my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

my $mqCommandChannelObject = Ultra::Lib::MQ::CommandChannel->new( JSON_CODER => $json_coder );

# select an outbound request channel + message
my $getNextOutboundMessageResult = $mqCommandChannelObject->getNextOutboundASPSOAPAsynchMessage();

print Dumper($getNextOutboundMessageResult);

__END__

Used to test Outbound ASP SOAP Asynchronous Messages (fire-and-forget)

