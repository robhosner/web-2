Ultra Message Queue implemented with Redis.

For the Message Queue layer and End Points, messages are simply strings.
Redis limits string values to 512 MB, therefore our message should fit. 
We do store messages indirectly: we associate a unique ID to them.
A NOTIFICATION_UUID is associated to messages which should be sent through the Notification Channel,
a CONTROL_UUID is associated to messages which should be sent through a Control Channel.
The Control Channel follows the Request-Reply pattern, the Notification Channel is basically a FIFO list.
Control Channels and Notification Channel behave in very different way.
Every Control Channels is used only once, the Notification Channel is unique and is constatntly in use.
Control Channels are bidirectional, the Notification Channel is monodirectional.
Control Channels are to be used only once.

The synchronous communication through Control Channels must begin with the
creation of the Request Channel and the Reply Channel.

The identifier of the Request Channel and the identifier of the Reply Channel
must be distinct, but we must be able to obtain seamlessly one from the other one.
The requestor initiate the generation of those string/channels.


Synchronous Control Channels communication steps:

0. decide the channel type T ( ACC MW or ULTRA MW )
1. create identifier of the Request Channel (outbound) (ReqCCID) and verify that it's not already used.
2. create identifier of the Reply Channel   (inbound)  (RepCCID) and verify that it's not already used.
3. create Request Channel and Reply Channel:
  3a. allocate the channel name 'CN_' . $ReqCCID ( Redis: set ( 'CN_'.$ReqCCID , 'channel' ) )
  3b. allocate the channel name 'CN_' . $RepCCID ( Redis: set ( 'CN_'.$RepCCID , 'channel' ) )
  3c. allocate the channel initial status to 'created' ( Redis: set ( 'CS_'.$ReqCCID , 'created' ) )
  3d. allocate the channel initial status to 'created' ( Redis: set ( 'CS_'.$RepCCID , 'created' ) )
  3e. record the Request Channel in the set ( T.outbound )
  3f. record the Reply Channel   in the set ( T.inbound  ) 
4. insert a new message into the Request Channel (outbound):
  4a. we associate a $CONTROL_UUID to the message ( Redis: set ( $CONTROL_UUID , $message ) )
  4b. we associate the message to the Request Channel ( Redis: set ( 'CM_'.$ReqCCID , $CONTROL_UUID ) )
  4c. we change the Request Channel status to 'filled' ( Redis: set ( 'CS_'.$ReqCCID , 'filled' ) )
5. This thread waits synchronously for the Reply Channel to have 'filled' status. ( Redis: get ( 'CS_'.$RepCCID ) )
6. (a different thread) read the message from the Request Channel (outbound):
  6a. we extract a random member from the Request Channel set of type T ( ACC MW or ULTRA MW ) ( Redis: srandmember )
  6b. we verify that the status is 'filled' ( Redis: get ( 'CS_'.$ReqCCID ) )
  6c. we reserve the Request Channel with the current PID ( Redis: set ( 'CS_'.$ReqCCID , 'reserved_'.PID ) )
  6d. we verify that the Request Channel has been reserved by this PID ( Redis: get ( 'CS_'.$ReqCCID ) )
  6e. we retrieve the $CONTROL_UUID contained in the channel. Redis: get ( 'CM_'.$ReqCCID )
  6f. we retrieve the message associated with the $CONTROL_UUID ( Redis: get ( $CONTROL_UUID ) )
  6g. we remove the message from the Request Channel ( Redis: del( 'CM_'.$ReqCCID ) )
  6h. we change the Request Channel status to 'used' ( Redis: set ( 'CS_'.$ReqCCID , 'used' ) )
7. insert a new message into the Reply Channel (inbound):
  7a. we associate a $CONTROL_UUID to the reply message ( Redis: set ( $CONTROL_UUID , $message ) )
  7b. we associate the message to the Reply Channel ( Redis: set ( 'CM_'.$RepCCID , $CONTROL_UUID ) )
  7c. we change the Reply Channel status to 'filled' ( Redis: set ( 'CS_'.$RepCCID , 'filled' ) )
8. This thread completes. Continue with the thread on step 5. ( Redis: get ( 'CS_'.$RepCCID ) )
9. read the message from the Reply Channel (inbound):
  9a. we verify that the status is 'filled' ( Redis: get ( 'CS_'.$RepCCID ) )
  9b. we reserve the Reply Channel with the current PID ( Redis: set ( 'CS_'.$RepCCID , 'reserved_'.PID ) )
  9c. we verify that the Reply Channel has been reserved by this PID ( Redis: get ( 'CS_'.$RepCCID ) )
  9d. we retrieve the $CONTROL_UUID contained in the channel. Redis: get ( 'CM_'.$RepCCID )
  9e. we retrieve the message associated with the $CONTROL_UUID ( Redis: get ( $CONTROL_UUID ) )
  9f. we remove the message from the Reply Channel ( Redis: del( 'CM_'.$RepCCID ) )
  9g. we change the Reply Channel status to 'used' ( Redis: set ( 'CS_'.$RepCCID , 'used' ) )


Asynchronous Control Channels communication steps:

The step number 4 concludes the first thread.
The step number 8 is performed by a brand new thread
8. extract a random member from the Reply Channel set of type T ( ACC MW or ULTRA MW ) ( Redis: srandmember )


Examples:

 two ACC MW Request-Reply channels:
 =================================

redis> smembers ACC_MW
1)  "in_1369335860.18270.9775"
2) "out_1369335860.18270.9775"

 just created:
 ============

redis> get CN_out_1369335860.18270.9775
"channel"
redis> get CS_out_1369335860.18270.9775
"created"

redis> get CN_in_1369335860.18270.9775
"channel"
redis> get CS_in_1369335860.18270.9775
"created"

  a message sent through an outbound channel:
  ==========================================

redis> get CS_out_1369337271.18701.710
"filled"
redis> get CM_out_1369337271.18701.710
"{CB-184BDFD13D98D522-96D7163BDFA60227}"
redis> get "{CB-184BDFD13D98D522-96D7163BDFA60227}"
"hello from ControlChannel_test.pl"

  


