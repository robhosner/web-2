<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ControlChannel.php");

$controlChannel = new \Ultra\Lib\MQ\ControlChannel;

echo $controlChannel->inboundACCMWControlChannel()."\n";
echo $controlChannel->outboundACCMWControlChannel()."\n";
echo $controlChannel->inboundUltraMWControlChannel()."\n";
echo $controlChannel->outboundUltraMWControlChannel()."\n";

echo $controlChannel->notificationChannelACCMW()."\n";
echo $controlChannel->notificationChannelUltraMW()."\n";

$m = $controlChannel->buildMessage(
                array(
                  'header'     => 'testcommand',
                  'body'       => array(
                    'success'       => TRUE,
                    'errors'        => array(),
                    'customer_id'   => 12
                  ),
                  'actionUUID' => 'thisactionUUID'.time()
                )
              );

echo "$m\n";

# enqueue $m in the ULTRA MW Notification Channel

$notificationChannel = $controlChannel->notificationChannelUltraMW();

echo "$notificationChannel\n";

/*

$notificationUUID = $controlChannel->enqueueNotificationChannel( $m );

$testPeek = $controlChannel->peekNotificationChannelUltraMW();

echo "$testPeek\n";

$output = $controlChannel->dequeueNotificationChannelUltraMW();

print_r( $output );

*/

?>
