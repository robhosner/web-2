package Ultra::Lib::MQ::EndPoint;

use strict;
use warnings;


use Data::UUID;
use Time::HiRes qw( usleep gettimeofday );
use Ultra::Lib::Util::Config;
use Ultra::Lib::Util::Redis::Cluster;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::MQ::EndPoint

=head1 Message Queue End Point

  End Point class to be used between ACC MW and SOAP scripts.
  This class contains methods which implement the Request-Reply pattern ( Control Channel )
  and methods which implement a FIFO message queue for the Notification Channel.
  Control Channels and Notification Channel behave in very different way.
  Every Control Channels is used only once, the Notification Channel is unique and is constatntly in use.
  Control Channels are bidirectional, the Notification Channel is monodirectional.

=head1 SYNOPSIS

  use Ultra::Lib::MQ::EndPoint;
  my $mq_endpoint = Ultra::Lib::MQ::EndPoint->new();

  # Notification Channels
  $mq_endpoint->enqueueNotificationChannel($aMessage);
  $aMessage = $mq_endpoint->dequeueNotificationChannel();

=cut

=head1 METHODS

=head4 new

  Object constructor.

=cut
sub new
{
  my ($class, %data) = @_;

  my $this = {};

  bless $this, $class;

  $this->log("new called");

  # object members
  $data{ LOG_ENABLED  } = 1;
  $data{ CONFIG }       ||= Ultra::Lib::Util::Config->new();

  $data{ REDIS_OBJECT } = Ultra::Lib::Util::Redis::Cluster->new( CONFIG => $data{ CONFIG } );

  $this->_initialize(\%data);

  return $this;
}


=head4 DESTROY

  Method invoked on object deallocation

=cut
sub DESTROY
{
  my ($this) = @_;
}


=head1

==== Generic Redis wrappers ====

=cut


=head4 _redis

  Private Redis object accessor.

=cut
sub _redis
{
  my ($this) = @_;

  return $this->{ REDIS_OBJECT };
}

=head1

==== Notification Queue methods ====

=cut


=head4 _notificationChannel

  Redis identifier (key) of the notification channel (implemented as a Redis list utilized in a FIFO fashion).

=cut
sub _notificationChannel
{
  my ($this, $mq) = @_;

  return $mq.'/INBOUND/NOTIFICATION';
}


=head4 notificationChannelACCMW

  ACC MW Notification channel identifier string
  Note: this should be the only Notification channel accessible from the Perl layer.

=cut
sub notificationChannelACCMW
{
  my ($this) = @_;

  return $this->_notificationChannel( 'ACCMW' );
}


=head4 notificationChannelUltraMW

  Ultra MW Notification channel identifier string
  Note: this should NOT be accessible from the Perl layer. Therefore this method is deprecated.

=cut
sub notificationChannelUltraMW
{
  my ($this) = @_;

  return $this->_notificationChannel( 'ULTRAMW' );
}


=head4 enqueueNotificationChannel

  Add a message at the bottom (right) of the ACC MW notification channel. (The ULTRA MW notification channel must not be available here).
  a. create a unique identifier for the notification message (NOTIFICATION_UUID)
  b. associate the NOTIFICATION_UUID with $message in Redis (using the primitive set)
  c. enqueue the NOTIFICATION_UUID in the Notification Channel
  Returns the NOTIFICATION_UUID for success, an empty string for failure.

=cut
sub enqueueNotificationChannel
{
  my ($this,$message) = @_;

  my $notificationChannel = $this->notificationChannelACCMW();

  $this->log("enqueueNotificationChannel is about to enqueue the message '$message' into $notificationChannel");

  # create a unique identifier for the notification message
  my $notificationUUID = $this->_getNewNotificationUUID();

  # associate the NOTIFICATION_UUID with $message
  #$this->_redis()->set($notificationUUID,$message);
  #$this->_redis()->expire($notificationUUID,$this->getMessageTTLSeconds());
  $this->_redis()->set($notificationUUID,$message,$this->getMessageTTLSeconds());

  $this->log("enqueueNotificationChannel rpush $notificationChannel $notificationUUID");

  # enqueue the NOTIFICATION_UUID in the Notification Channel
  $this->_redis()->rpush($notificationChannel,$notificationUUID);

  return $notificationUUID;
}


=head4 dequeueNotificationChannel

  POP Notification Channel.
  The message is removed from the ACC MW notification channel.
  Note: this implementation is not atomic.
  Note: this method should not be invoked by the Perl layer, since it's only supposed to write notifications to the ACC MW, not extracting them.
  Therefore this method is deprecated.

=cut
sub dequeueNotificationChannel
{
  my ($this) = @_;

  my $return = '';

  my $notificationChannel = $this->notificationChannelACCMW();

  $this->log("dequeueNotificationChannel is about to extract a notificationUUID from channel $notificationChannel");

  # FIFO operation: we pop from left
  my $notificationUUID = $this->_redis()->lpop( $notificationChannel );

  if ( $notificationUUID )
  {
    $this->log("dequeueNotificationChannel is about to get the message associated with the notificationUUID $notificationUUID");

    # finally we get the message
    $return = $this->_redis()->get( $notificationUUID );
  }

  $this->log("dequeueNotificationChannel returns $return");

  return $return;
}


=head4 peekNotificationChannel

  Read the message at the top of the ACC MW notification channel
  The message will NOT be removed from the channel.
  Note: this method should not be invoked by the Perl layer, since it's only supposed to write notifications to the ACC MW, not extracting them.
  Therefore this method is deprecated.

=cut
sub peekNotificationChannel
{
  my ($this) = @_;

  my $return = '';

  my $notificationChannel = $this->notificationChannelACCMW();

  $this->log("peekNotificationChannel is about to check a notificationUUID from channel $notificationChannel");

  # check the top (left) of the queue
  my $notificationUUID = $this->_redis()->lrange( $notificationChannel , 0 , 0 );

  if ( $notificationUUID && ( ref $notificationUUID eq 'ARRAY' ) )
  {
    $return = $this->_redis()->get( $notificationUUID->[0] );
  }

  return $return;
}


=head1

==== Generic Channel methods ====

=cut


=head4 _getNewChannelName

  Returns a string which will be used to create a new channel.
  Therefore, it's better be unique.

=cut
sub _getNewChannelName
{
  my ($this) = @_;

  return join('.',(time,$$,int(rand(10000))));
}


=head4 _getNewControlUUID

  ControlUUID with prefix CX
  'C' is for ControlUUID
  'X' is left open for future use

=cut
sub _getNewControlUUID
{
  my ($this) = @_;

  return $this->_getNewUUID('CX');
}


=head4 _getNewNotificationUUID

  NotificationUUID with prefix NX
  'N' is for NotificationUUID
  'X' is left open for future use

=cut
sub _getNewNotificationUUID
{
  my ($this) = @_;

  return $this->_getNewUUID('NX');
}


=head4 _getNewNotificationActionUUID

  Action UUID for a Notification message

=cut
sub _getNewNotificationActionUUID
{
  my ($this) = @_;

  return $this->_getNewUUID('AN');
}


=head4 _getMessageUUID

  SessionUUID for a Control message

=cut
sub _getMessageUUID
{
  my ($this) = @_;

  return $this->_getNewUUID('MM');
}


=head4 _getNewUUID

  Format:
  {pp-****************-****************}
  'pp' is the given prefix
  '*' belongs to [01234567890ABCDEF]
  See ticket AMDOCS-11

=cut
sub _getNewUUID
{
  my ($this,$prefix) = @_;

  my ($seconds, $microseconds) = gettimeofday();

  my $namespace  = 'Ultra';
  my $name       = 'Ultra'.$$.$seconds.$microseconds;

  my $uuidObject = new Data::UUID;

  $uuidObject->create_str();
  my $uuid = $uuidObject->create_from_name_str( $namespace , $name );
  $uuid =~ s/\-//;
  $uuid =~ s/\-//;
  $uuid =~ s/\-([^-]+)$/$1/;

  return "{". $prefix . "-" . $uuid . "}";
}


=head1

==== Utility methods ====

=cut


=head4 getMessageTTLSeconds

  Messages and volatile channels TTL in seconds

=cut
sub getMessageTTLSeconds
{
  my ($this) = @_;

  return ( $this->{ CONFIG }->find_credential('redis/messages/ttl_hours') * 60 * 60 );
}


=head4 getChannelPingFrequencySeconds

  How often we should check for a reply on a channel.
  Default is 10.

=cut
sub getChannelPingFrequencySeconds
{
  my ($this) = @_;

  return $this->{ CHANNEL_PING_FREQUENCY } || 1;
}


=head4 setChannelPingFrequencySeconds

  Sets how often we should check for a reply on a channel.

=cut
sub setChannelPingFrequencySeconds
{
  my ($this,$seconds) = @_;

  $this->{ CHANNEL_PING_FREQUENCY } = $seconds;

  # allow chaining
  return $this;
}


=head4 getRequestReplyTimeoutSeconds

  After how many seconds Request-Reply message exchange will timeout.
  Default is 1 minute.

=cut
sub getRequestReplyTimeoutSeconds
{
  my ($this) = @_;

  return $this->{ REQUEST_REPLY_TIMEOUT } || 60;
}


=head4 setRequestReplyTimeoutSeconds

  Sets how many seconds Request-Reply message exchange will timeout.

=cut
sub setRequestReplyTimeoutSeconds
{
  my ($this,$seconds) = @_;

  $this->{ REQUEST_REPLY_TIMEOUT } = $seconds;

  # allow chaining
  return $this;
}


=head1

==== Message methods ====

=cut


=head4 _getCreatedDateTime

  Method to get a formatted Date + Time string

=cut
sub _getCreatedDateTime
{
  my ($this) = @_;

  my ($sec,$min,$hr,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

  sprintf "%04d-%02d-%02d %02d:%02d:%02d", ($year + 1900, ++$mon, $mday, $hr, $min, $sec);
}


=head4 buildMessage

  Method to build a message out of header (string) and body (data structure).
  The inverse process is extractFromMessage.
  Expects:
    header     (string)
    body       (hash ref)
    actionUUID (string)
=cut
sub buildMessage
{
  my ($this,$params) = @_;

  my $message = $this->{ JSON_CODER }->encode(
    {
    header           => $params->{ header     },
    body             => $params->{ body       },
    _actionUUID      => $params->{ actionUUID },
    _uuid            => $this->_getMessageUUID(),
    _timestamp       => time,
    }
  );

  return $message;
}


=head4 extractFromMessage

  Extract header (string) and body (data structure) from a message.

=cut
sub extractFromMessage
{
  my ($this,$message) = @_;

  my $data;

  eval
  {
    $data = $this->{ JSON_CODER }->decode($message);
  };

  if ( $@ )
  {
    $this->log( "extractFromMessage error : $@" );
  }

  return $data;
}


=head4 popEndPointChannel

  Pops a message from the $prefix . '_' . $channelName channel in the $messageQueue message queue

=cut
sub popEndPointChannel
{
  my ($this,$channelName,$messageQueue,$prefix) = @_;

  $this->log( "popEndPointChannel : $channelName,$messageQueue,$prefix" );

  my $return = '';

  if ( $this->_redis->exists( $prefix . '_' . $channelName ) # channel name
    && $this->_redis->exists( 'CS_' . $channelName ) # channel status
    && $this->_redis->exists( 'CM_' . $channelName ) # channel message
    && ( $this->_redis->get( 'CS_' . $channelName ) eq 'filled' )
  )
  {
    # read the UUID contained in the channel
    my $uuid = $this->_redis->get( 'CM_' . $channelName );

    if ( $uuid )
    {
      if ( $this->reserveEndPointChannel( $channelName , $prefix ) )
      {
        #$this->log( "Process $$ reserved channel $channelName" );

        # get the message pointed by the UUID
        # we assume that every message is a ``true value``
        $return = $this->_redis->get( $uuid );

        # remove the message from the channel
        $this->_redis->del( 'CM_' . $channelName );

        # remove channel from channel set
        $this->_redis->srem( $messageQueue , $channelName );
        $this->log( "Removing $channelName from $messageQueue" );

        # release channel
        $this->releaseEndPointChannel( $channelName , 'used' );
      }
      else
      {
        #$this->log( "Process $$ could not reserve channel $channelName" );
      }
    }
  }

  #if ( ! $return ) { $this->log( "popEndPointChannel failure" ); }

  return $return;
}


sub sendToEndPointChannel
{
  my ($this,$channelName,$message,$prefix) = @_;

  if ( ! $message )
  {
    $this->log( "sendToControlChannel : message cannot be empty" );

    return '';
  }

  my $return = '';

  if ( $this->_redis()->exists( $prefix .  '_' . $channelName ) )
  {
    if ( $this->reserveEndPointChannel( $channelName , $prefix ) )
    {
      #$this->log( "Process $$ reserved channel $channelName" );

      # create a unique identifier for the control message
      my $uuid = $this->_getNewControlUUID(); #TODO: EndPointUUID

      # associate the UUID with $message
      #$this->_redis()->set( $uuid , $message );
      #$this->_redis()->expire( $uuid, $this->getMessageTTLSeconds() );
      $this->_redis()->set( $uuid , $message , $this->getMessageTTLSeconds() );
      $this->log( "Setting $uuid to $message" );

      # place the UUID in the appropriate container
      #$this->_redis()->set( 'CM_' . $channelName , $uuid );
      #$this->_redis()->expire( 'CM_' . $channelName , $this->getMessageTTLSeconds() );
      $this->_redis()->set( 'CM_' . $channelName , $uuid , $this->getMessageTTLSeconds() );
      #$this->log( "Setting CM_".$channelName." to $uuid" );

      # release channel
      $this->releaseEndPointChannel( $channelName , 'filled' );

      $return = $uuid;
    }
    else
    {
      #$this->log( "Process $$ could not reserve channel $channelName" );
    }
  }
  else
  {
    #$this->log( "Channel $channelName does not exist" );
  }

  return $return;
}


=head4

  Creates a channel named $prefix . '_' . $channelName in $messageQueue
  $prefix denotes the Channel Type.

=cut
sub createEndPointChannel
{
  my ($this,$channelName,$messageQueue,$prefix) = @_;

  my $return = 0;

  if ( ! $this->existsEndPointChannel($channelName,$prefix) )
  {
    # allocates channel name
    #$this->_redis->set(    $prefix . '_' . $channelName , 'channel' );
    #$this->_redis->expire( $prefix . '_' . $channelName , $this->getMessageTTLSeconds() );
    $this->_redis->set( $prefix . '_' . $channelName , 'channel' , $this->getMessageTTLSeconds() );

    if ( $this->reserveEndPointChannel( $channelName , $prefix ) )
    {
      $this->log("Process $$ reserved channel $channelName");

      # add this channel to the $messageQueue set
      $this->_redis->sadd( $messageQueue , $channelName );
      $this->log("Adding $channelName to set $messageQueue");

      # release channel
      $this->releaseEndPointChannel( $channelName , 'created' );

      $return = $channelName;
    }
    else
    {
      $this->log("Process $$ could not reserve channel $channelName");
    }
  }
  else
  {
    $this->log("channel $channelName already exists");
  }

  return $return;
}


# Reserve Control Channel using $$
sub reserveEndPointChannel
{
  my ($this,$channelName,$prefix) = @_;

  my $success = 0;

  my $currentStatus = $this->peekChannelStatus( $channelName , $prefix );

  if ( $currentStatus !~ /^reserved / )
  {
    $this->assignChannelStatus( $channelName , "reserved $$" );

    usleep(1000);

    my $currentStatus = $this->peekChannelStatus( $channelName , $prefix );

    $success = ( $currentStatus eq "reserved $$" );
  }

  return $success;
}


sub assignChannelStatus
{
  my ($this,$channelName,$status) = @_;

  #$this->log("Setting CS_".$channelName." to $status");

  #$this->_redis->set(    'CS_' . $channelName , $status );
  #$this->_redis->expire( 'CS_' . $channelName , $this->getMessageTTLSeconds() );
  $this->_redis->set( 'CS_' . $channelName , $status , $this->getMessageTTLSeconds() );
}


sub releaseEndPointChannel
{
  my ($this,$channelName,$status) = @_;

  $this->assignChannelStatus( $channelName , $status );
}


sub existsEndPointChannel
{
  my ($this,$channelName,$prefix) = @_;

  return $this->_redis->exists( $prefix . '_' . $channelName );
}


sub peekChannelStatus
{
  my ($this,$channelName,$prefix) = @_;

  my $return = '';

  if ( $this->_redis->exists( $prefix . '_' . $channelName )
    && $this->_redis->exists( 'CS_' . $channelName )
  )
  {
    $return = $this->_redis->get( 'CS_' . $channelName );
  }

  return $return;
}


=head4 toInbound

  Needed by Repliers to obtain Replier channels

=cut
sub toInbound
{
  my ($this, $requestChannelName) = @_;

  $requestChannelName =~ s/^out_/in_/;

  return $requestChannelName;
}


=head4 toOutbound

  Needed by Requestors to obtain Replier channels

=cut
sub toOutbound
{
  my ($this, $replyChannelName) = @_;

  $replyChannelName =~ s/^in_/out_/;

  return $replyChannelName;
}


1;


__END__


