use strict;
use warnings;


use JSON::XS;
use Ultra::Lib::MQ::CommandChannel;


# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/CommandChannel_testRequestor.log';


my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();


# instantiate Ultra::Lib::MQ::CommandChannel
my $mq_cc = Ultra::Lib::MQ::CommandChannel->new( JSON_CODER => $json_coder );

# create Request-Reply ASP SOAP Control Channels
my ($c_outbound,$c_inbound) = $mq_cc->createNewASPSOAPCommandSynchChannels();

# I am a Requestor, I want to send an *outbound* message through $c_outbound,
# then I will synchronously wait for a reply the Replier will send through $c_inbound

$mq_cc->log("c_outbound = $c_outbound");
$mq_cc->log("c_inbound  = $c_inbound");

my $params =
{
  msisdn => '11001001000',
  tag    => 'CommandChannel_testRequestor-'.time,
};

my $anOutboundRequestMessage = $mq_cc->buildMessage({
  'header'     => 'QueryMSISDN',
  'body'       => $params,
  'uuid'       => 'uuid',
  'actionUUID' => 'actionUUID'
});

my $controlUUID = $mq_cc->sendToCommandChannel( $c_outbound , $anOutboundRequestMessage );

$mq_cc->log("controlUUID = $controlUUID");
$mq_cc->log("message     = $anOutboundRequestMessage");

my $anInboundReplyMessage = $mq_cc->waitForInboundASPSOAPCommandMessage( $c_inbound );

if ( $anInboundReplyMessage )
{
  $mq_cc->log("Success: message = $anInboundReplyMessage");
}
else
{
  $mq_cc->log("Failure");
}


__END__


