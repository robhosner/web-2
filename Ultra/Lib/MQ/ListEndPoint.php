<?php

namespace Ultra\Lib\MQ;

include_once('Ultra/Lib/MQ/EndPoint.php');

/**
 * Main Class which implements Endpoint functionalities using a Redis list.
 * 
 * Redis list length:
 * %> llen SMS/LIST/OUTBOUND/SYNCH
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra Middleware
 */
class ListEndPoint extends EndPoint
{
  protected $list;

  // outbound FIFO lists - there is no need for an inbound list
  const SMS_OUTBOUND_SYNCH_LIST  = 'SMS/LIST/OUTBOUND/SYNCH';

  const SMS_OUTBOUND_ASYNCH_LIST = 'SMS/LIST/OUTBOUND/ASYNCH';

  /**
   * The EndPoint constructor instantiates a Redis object and connects to Redis.
   * It needs a list name.
   */
  public function __construct( $list )
  {
    if ( ! $list )
    {
      dlog('',"ERROR: list name is needed");

      return NULL;
    }

    parent::__construct();

    $this->list = $list;

    #dlog('',"list  = %s",$this->list);
  }

  /**
   * buildReplyErrorMessage
   *
   * @returns string
   */
  public function buildReplyErrorMessage( $header='ERROR' , $error , $actionUUID=NULL )
  {
    if ( ! $actionUUID )
      $actionUUID = getNewActionUUID('middleware ' . time());

    return $this->buildMessage(
      array(
        'actionUUID' => $actionUUID,
        'header'     => $header,
        'body'       => array(
          'success'    => FALSE,
          'errors'     => array( $error )
        )
      )
    );
  }

  /**
   * popMessageKey
   *
   * Retrieves an outbound message key from the list
   * Uses RPOP
   *
   * @returns string or NULL
   */
  public function popMessageKey()
  {
    $outboundMessageKey = NULL;

    try
    {
      if ( ! $this->redis )
        throw new \Exception('Redis object not available');

      // pop from the right
      $outboundMessageKey = $this->redis->rpop( $this->list );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $outboundMessageKey;
  }

  /**
   * pushMessageKey
   *
   * Adds an outbound message key to the list
   * Uses LPUSH
   *
   * @returns integer
   */
  public function pushMessageKey( $outboundMessageKey )
  {
    $status = NULL;

    try
    {
      if ( ! $this->redis )
        throw new \Exception('Redis object not available');

      // push from the left
      $status = $this->redis->lpush( $this->list , $outboundMessageKey );

      if ( ! $status )
        throw new \Exception('lpush failed');
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $status;
  }

  /**
   * popMessage
   *
   * Retrieves a message from the list
   *
   * @returns array
   */
  public function popMessage()
  {
    $message            = NULL;
    $outboundMessageKey = NULL;

    try
    {
      if ( ! $this->redis )
        throw new \Exception('Redis object not available');

      // retrieve an outbound message key from the list
      $outboundMessageKey = $this->popMessageKey();

      if ( $outboundMessageKey )
        // retrieve the outbound message associated with $outboundMessageKey
        $message = $this->redis->get( $outboundMessageKey );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return array( $outboundMessageKey , $message );
  }

  /**
   * pushMessage
   *
   * Adds a message to the list
   *
   * @returns string
   */
  public function pushMessage( $message )
  {
    $outboundMessageKey = NULL;

    try
    {
      if ( ! $this->redis )
        throw new \Exception('Redis object not available');

      // generate a new outbound message key
      $outboundMessageKey = $this->getNewOutboundMessageKey();

      // associate $outboundMessageKey with $message
      $status = $this->redis->set( $outboundMessageKey , $message );

      if ( ! $status )
        throw new \Exception('set failed');

      // set TTL for $outboundMessageKey
      $this->redis->expire( $outboundMessageKey , $this->getMessageTTLSeconds() );

      // adds the outbound message key to the list
      $status = $this->pushMessageKey( $outboundMessageKey );

      if ( ! $status )
        throw new \Exception('pushMessageKey failed');
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      $outboundMessageKey = NULL;
    }

    dlog('',"outboundMessageKey = $outboundMessageKey");

    return $outboundMessageKey;
  }

  /**
   * replyMessage
   *
   * @returns integer
   */
  public function replyMessage( $outboundMessageKey , $replyMessage )
  {
    dlog('', '(%s)', func_get_args());

    $status = NULL;

    try
    {
      if ( ! $this->redis )
        throw new \Exception('Redis object not available');

      // outbound key => inbound key
      $inboundMessageKey = $this->toInbound( $outboundMessageKey );

      // associate $inboundMessageKey with $replyMessage
      $status = $this->redis->set( $inboundMessageKey , $replyMessage );

      if ( ! $status )
        throw new \Exception('set failed');

      // set TTL for $inboundMessageKey
      $this->redis->expire( $inboundMessageKey , $this->getMessageTTLSeconds() );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      $status = NULL;
    }

    return $status;
  }

  /**
   * waitForReplyMessage
   *
   * @return object of class \Result
   */
  public function waitForReplyMessage( $outboundMessageKey , $delays=array() )
  {
    $result = new \Result();

    try
    {
      if ( ! $this->redis )
        throw new \Exception('Redis object not available');

      // outbound key => inbound key
      $inboundMessageKey = $this->toInbound( $outboundMessageKey );

      $exitLoop    = FALSE;
      $startTime   = time();
      $timeoutTime = time() + $this->getRequestReplyTimeoutSeconds();

      dlog('',"Start waiting on inbound channel $inboundMessageKey...");

      $waitHistory = array();

      while( ! $exitLoop )
      {
        $replyMessage = $this->redis->get( $inboundMessageKey );

        if ( $replyMessage )
        {
          // we got a reply

          $exitLoop = TRUE;

          $result->data_array['reply'] = $replyMessage;

          $result->succeed();
        }
        else
        {
          // check for timeout
          if ( $timeoutTime < time() )
          {
            $exitLoop = TRUE;

            dlog('', __CLASS__ . " Timeout after ".$this->getRequestReplyTimeoutSeconds()." seconds" );

            $result = make_timeout_Result( "waitForReplyMessage timed out after ".$this->getRequestReplyTimeoutSeconds()." seconds" );
          }
        }

        // decide how much time should we sleep
        if ( is_array( $delays ) && count( $delays ) )
          $channelPingFrequencySeconds = array_shift( $delays );
        else
          $channelPingFrequencySeconds = $this->getChannelPingFrequencySeconds();

        $waitHistory[] = $channelPingFrequencySeconds;

        if ( ! $exitLoop )
        {
          // fragment of a second
          usleep( $channelPingFrequencySeconds*1000000 );
        }
      }

      dlog('',"ended after %s seconds - seq : %s",array_sum( $waitHistory ),$waitHistory);
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      $result->add_error( $e->getMessage() );
    }

    return $result;
  }

}

// factory function
function getSMSListEndPoint( $asynch=FALSE )
{
  return $asynch ? new ListEndPoint( \Ultra\Lib\MQ\ListEndPoint::SMS_OUTBOUND_ASYNCH_LIST )
                 : new ListEndPoint( \Ultra\Lib\MQ\ListEndPoint::SMS_OUTBOUND_SYNCH_LIST  )
                 ;
}

