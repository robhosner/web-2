#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Config;


# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/Config_test.log';


my $config = Ultra::Lib::Util::Config->new();


print $config->envConfigValue('db/ccv')."\n";


my ( $domain , $user , $pwd ) = $config->ultraApiInternalInfo();

print "domain = $domain\n";
print "user   = $user\n";
print "pwd    = $pwd\n";

my $url  = 'https://'.$user.':'.$pwd.'@'.$domain.'/pr/internal/2/ultra/api/internal__PortOutDeactivation';

print "url    = $url\n";

print "soapLogRedoLog = ".$config->soapLogRedoLog()."\n";

my ($d,$u,$p) = $config->ultraApiInternalInfo();

print "ultraApiInternalInfo = ($d,$u,$p)\n";

foreach my $key ('credentials','db/TSIP','mongodb/connection_string','partner/api/allow')
{
  print $config->envConfigValue( $key )."\n";
}

__END__

Example:
%> sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev Ultra/Lib/Util/Config_test.pl'

