<?php

// functions concerning customer's language, country and any special variant preferences

/**
 * is_english
 *
 * Returns TRUE if $language is an abbreviation for English Language
 * We currently associate 'zh' (mandarin) with English Language back-end logic as well.
 */
function is_english( $language )
{
  return ! ! ( ( $language == 'en-us' ) || ( $language == 'en-US' ) || ( $language == 'en' ) || ( $language == 'zh' ) || ( $language == 'ZH' ) );
}

/**
 * is_mandarin
 *
 * Returns TRUE if $language is an abbreviation for Mandarin Language
 */
function is_mandarin( $language )
{
  return ! ! ( ( $language == 'zh' ) || ( $language == 'ZH' ) );
}

/**
 * convertUnicodeFromJavaEscape
 *
 * Returns input string with java escape converted to unicode
 */
function convertUnicodeFromJavaEscape($str)
{
  return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UTF-16BE');
  }, $str);
}

/**
 * convertUnicodeToJavaEscape
 *
 * Returns input string with unicode converted to java escape
 */
function convertUnicodeToJavaEscape($str)
{
  $str = preg_replace('/[[:cntrl:]]+/', '', $str);
  $str = trim(json_encode($str), '"');
  $str = preg_replace_callback('/\\\\([\\"\\/\\\]{1})/', function ($match) {
    return $match[1];
  }, $str);

  return $str;
}

/**
 * Will return an array of all alpha 2 country codes.
 *
 * @return array
 * @author bwalters@ultra.me
 */
function getAlpha2CountryCodes()
{
  return array(
    'AF',
    'AX',
    'AL',
    'DZ',
    'AS',
    'AD',
    'AO',
    'AI',
    'AQ',
    'AG',
    'AR',
    'AM',
    'AW',
    'AU',
    'AT',
    'AZ',
    'BS',
    'BH',
    'BD',
    'BB',
    'BY',
    'BE',
    'BZ',
    'BJ',
    'BM',
    'BT',
    'BO',
    'BA',
    'BW',
    'BV',
    'BR',
    'IO',
    'BN',
    'BG',
    'BF',
    'BI',
    'KH',
    'CM',
    'CA',
    'CV',
    'KY',
    'CF',
    'TD',
    'CL',
    'CN',
    'CX',
    'CC',
    'CO',
    'KM',
    'CG',
    'CD',
    'CK',
    'CR',
    'CI',
    'HR',
    'CU',
    'CY',
    'CZ',
    'DK',
    'DJ',
    'DM',
    'DO',
    'EC',
    'EG',
    'SV',
    'GQ',
    'ER',
    'EE',
    'ET',
    'FK',
    'FO',
    'FJ',
    'FI',
    'FR',
    'GF',
    'PF',
    'TF',
    'GA',
    'GM',
    'GE',
    'DE',
    'GH',
    'GI',
    'GR',
    'GL',
    'GD',
    'GP',
    'GU',
    'GT',
    'GG',
    'GN',
    'GW',
    'GY',
    'HT',
    'HM',
    'VA',
    'HN',
    'HK',
    'HU',
    'IS',
    'IN',
    'ID',
    'IR',
    'IQ',
    'IE',
    'IM',
    'IL',
    'IT',
    'JM',
    'JP',
    'JE',
    'JO',
    'KZ',
    'KE',
    'KI',
    'KR',
    'KW',
    'KG',
    'LA',
    'LV',
    'LB',
    'LS',
    'LR',
    'LY',
    'LI',
    'LT',
    'LU',
    'MO',
    'MK',
    'MG',
    'MW',
    'MY',
    'MV',
    'ML',
    'MT',
    'MH',
    'MQ',
    'MR',
    'MU',
    'YT',
    'MX',
    'FM',
    'MD',
    'MC',
    'MN',
    'ME',
    'MS',
    'MA',
    'MZ',
    'MM',
    'NA',
    'NR',
    'NP',
    'NL',
    'AN',
    'NC',
    'NZ',
    'NI',
    'NE',
    'NG',
    'NU',
    'NF',
    'MP',
    'NO',
    'OM',
    'PK',
    'PW',
    'PS',
    'PA',
    'PG',
    'PY',
    'PE',
    'PH',
    'PN',
    'PL',
    'PT',
    'PR',
    'QA',
    'RE',
    'RO',
    'RU',
    'RW',
    'BL',
    'SH',
    'KN',
    'LC',
    'MF',
    'PM',
    'VC',
    'WS',
    'SM',
    'ST',
    'SA',
    'SN',
    'RS',
    'SC',
    'SL',
    'SG',
    'SK',
    'SI',
    'SB',
    'SO',
    'ZA',
    'GS',
    'ES',
    'LK',
    'SD',
    'SR',
    'SJ',
    'SZ',
    'SE',
    'CH',
    'SY',
    'TW',
    'TJ',
    'TZ',
    'TH',
    'TL',
    'TG',
    'TK',
    'TO',
    'TT',
    'TN',
    'TR',
    'TM',
    'TC',
    'TV',
    'UG',
    'UA',
    'AE',
    'GB',
    'US',
    'UM',
    'UY',
    'UZ',
    'VU',
    'VE',
    'VN',
    'VG',
    'VI',
    'WF',
    'EH',
    'YE',
    'ZM',
    'ZW'
  );
}

/**
 * Will return an array of all alpha 2 country codes
 * mapped to the full name of the country.
 *
 * @return array
 * @author bwalters@ultra.me
 */
function getAlpha2CountryCodesMap()
{
  return array(
    'AF' => 'Afghanistan',
    'AX' => 'Aland Islands',
    'AL' => 'Albania',
    'DZ' => 'Algeria',
    'AS' => 'American Samoa',
    'AD' => 'Andorra',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AQ' => 'Antarctica',
    'AG' => 'Antigua And Barbuda',
    'AR' => 'Argentina',
    'AM' => 'Armenia',
    'AW' => 'Aruba',
    'AU' => 'Australia',
    'AT' => 'Austria',
    'AZ' => 'Azerbaijan',
    'BS' => 'Bahamas',
    'BH' => 'Bahrain',
    'BD' => 'Bangladesh',
    'BB' => 'Barbados',
    'BY' => 'Belarus',
    'BE' => 'Belgium',
    'BZ' => 'Belize',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BT' => 'Bhutan',
    'BO' => 'Bolivia',
    'BA' => 'Bosnia And Herzegovina',
    'BW' => 'Botswana',
    'BV' => 'Bouvet Island',
    'BR' => 'Brazil',
    'IO' => 'British Indian Ocean Territory',
    'BN' => 'Brunei Darussalam',
    'BG' => 'Bulgaria',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodia',
    'CM' => 'Cameroon',
    'CA' => 'Canada',
    'CV' => 'Cape Verde',
    'KY' => 'Cayman Islands',
    'CF' => 'Central African Republic',
    'TD' => 'Chad',
    'CL' => 'Chile',
    'CN' => 'China',
    'CX' => 'Christmas Island',
    'CC' => 'Cocos (Keeling) Islands',
    'CO' => 'Colombia',
    'KM' => 'Comoros',
    'CG' => 'Congo',
    'CD' => 'Congo, Democratic Republic',
    'CK' => 'Cook Islands',
    'CR' => 'Costa Rica',
    'CI' => 'Cote D\'Ivoire',
    'HR' => 'Croatia',
    'CU' => 'Cuba',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DK' => 'Denmark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'EC' => 'Ecuador',
    'EG' => 'Egypt',
    'SV' => 'El Salvador',
    'GQ' => 'Equatorial Guinea',
    'ER' => 'Eritrea',
    'EE' => 'Estonia',
    'ET' => 'Ethiopia',
    'FK' => 'Falkland Islands (Malvinas)',
    'FO' => 'Faroe Islands',
    'FJ' => 'Fiji',
    'FI' => 'Finland',
    'FR' => 'France',
    'GF' => 'French Guiana',
    'PF' => 'French Polynesia',
    'TF' => 'French Southern Territories',
    'GA' => 'Gabon',
    'GM' => 'Gambia',
    'GE' => 'Georgia',
    'DE' => 'Germany',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Greece',
    'GL' => 'Greenland',
    'GD' => 'Grenada',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GG' => 'Guernsey',
    'GN' => 'Guinea',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HT' => 'Haiti',
    'HM' => 'Heard Island & Mcdonald Islands',
    'VA' => 'Holy See (Vatican City State)',
    'HN' => 'Honduras',
    'HK' => 'Hong Kong',
    'HU' => 'Hungary',
    'IS' => 'Iceland',
    'IN' => 'India',
    'ID' => 'Indonesia',
    'IR' => 'Iran, Islamic Republic Of',
    'IQ' => 'Iraq',
    'IE' => 'Ireland',
    'IM' => 'Isle Of Man',
    'IL' => 'Israel',
    'IT' => 'Italy',
    'JM' => 'Jamaica',
    'JP' => 'Japan',
    'JE' => 'Jersey',
    'JO' => 'Jordan',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KI' => 'Kiribati',
    'KR' => 'Korea',
    'KW' => 'Kuwait',
    'KG' => 'Kyrgyzstan',
    'LA' => 'Lao People\'s Democratic Republic',
    'LV' => 'Latvia',
    'LB' => 'Lebanon',
    'LS' => 'Lesotho',
    'LR' => 'Liberia',
    'LY' => 'Libyan Arab Jamahiriya',
    'LI' => 'Liechtenstein',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'MO' => 'Macao',
    'MK' => 'Macedonia',
    'MG' => 'Madagascar',
    'MW' => 'Malawi',
    'MY' => 'Malaysia',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malta',
    'MH' => 'Marshall Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MU' => 'Mauritius',
    'YT' => 'Mayotte',
    'MX' => 'Mexico',
    'FM' => 'Micronesia, Federated States Of',
    'MD' => 'Moldova',
    'MC' => 'Monaco',
    'MN' => 'Mongolia',
    'ME' => 'Montenegro',
    'MS' => 'Montserrat',
    'MA' => 'Morocco',
    'MZ' => 'Mozambique',
    'MM' => 'Myanmar',
    'NA' => 'Namibia',
    'NR' => 'Nauru',
    'NP' => 'Nepal',
    'NL' => 'Netherlands',
    'AN' => 'Netherlands Antilles',
    'NC' => 'New Caledonia',
    'NZ' => 'New Zealand',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NU' => 'Niue',
    'NF' => 'Norfolk Island',
    'MP' => 'Northern Mariana Islands',
    'NO' => 'Norway',
    'OM' => 'Oman',
    'PK' => 'Pakistan',
    'PW' => 'Palau',
    'PS' => 'Palestinian Territory, Occupied',
    'PA' => 'Panama',
    'PG' => 'Papua New Guinea',
    'PY' => 'Paraguay',
    'PE' => 'Peru',
    'PH' => 'Philippines',
    'PN' => 'Pitcairn',
    'PL' => 'Poland',
    'PT' => 'Portugal',
    'PR' => 'Puerto Rico',
    'QA' => 'Qatar',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RU' => 'Russian Federation',
    'RW' => 'Rwanda',
    'BL' => 'Saint Barthelemy',
    'SH' => 'Saint Helena',
    'KN' => 'Saint Kitts And Nevis',
    'LC' => 'Saint Lucia',
    'MF' => 'Saint Martin',
    'PM' => 'Saint Pierre And Miquelon',
    'VC' => 'Saint Vincent And Grenadines',
    'WS' => 'Samoa',
    'SM' => 'San Marino',
    'ST' => 'Sao Tome And Principe',
    'SA' => 'Saudi Arabia',
    'SN' => 'Senegal',
    'RS' => 'Serbia',
    'SC' => 'Seychelles',
    'SL' => 'Sierra Leone',
    'SG' => 'Singapore',
    'SK' => 'Slovakia',
    'SI' => 'Slovenia',
    'SB' => 'Solomon Islands',
    'SO' => 'Somalia',
    'ZA' => 'South Africa',
    'GS' => 'South Georgia And Sandwich Isl.',
    'ES' => 'Spain',
    'LK' => 'Sri Lanka',
    'SD' => 'Sudan',
    'SR' => 'Suriname',
    'SJ' => 'Svalbard And Jan Mayen',
    'SZ' => 'Swaziland',
    'SE' => 'Sweden',
    'CH' => 'Switzerland',
    'SY' => 'Syrian Arab Republic',
    'TW' => 'Taiwan',
    'TJ' => 'Tajikistan',
    'TZ' => 'Tanzania',
    'TH' => 'Thailand',
    'TL' => 'Timor-Leste',
    'TG' => 'Togo',
    'TK' => 'Tokelau',
    'TO' => 'Tonga',
    'TT' => 'Trinidad And Tobago',
    'TN' => 'Tunisia',
    'TR' => 'Turkey',
    'TM' => 'Turkmenistan',
    'TC' => 'Turks And Caicos Islands',
    'TV' => 'Tuvalu',
    'UG' => 'Uganda',
    'UA' => 'Ukraine',
    'AE' => 'United Arab Emirates',
    'GB' => 'United Kingdom',
    'US' => 'United States',
    'UM' => 'United States Outlying Islands',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VU' => 'Vanuatu',
    'VE' => 'Venezuela',
    'VN' => 'Viet Nam',
    'VG' => 'Virgin Islands, British',
    'VI' => 'Virgin Islands, U.S.',
    'WF' => 'Wallis And Futuna',
    'EH' => 'Western Sahara',
    'YE' => 'Yemen',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe'
  );
}

/**
 * Returns array of alpha 2 country calling codes
 *
 * @return array
 */
function getCallingCodesMap()
{
  return array (
      'BD' => '880',
      'BE' => '32',
      'BF' => '226',
      'BG' => '359',
      'BA' => '387',
      'BB' => '1246',
      'WF' => '681',
      'BL' => '590',
      'BM' => '1441',
      'BN' => '673',
      'BO' => '591',
      'BH' => '973',
      'BI' => '257',
      'BJ' => '229',
      'BT' => '975',
      'JM' => '1876',
      'BW' => '267',
      'WS' => '685',
      'BQ' => '599',
      'BR' => '55',
      'BS' => '1242',
      'JE' => '441534',
      'BY' => '375',
      'BZ' => '501',
      'RU' => '7',
      'RW' => '250',
      'RS' => '381',
      'TL' => '670',
      'RE' => '262',
      'TM' => '993',
      'TJ' => '992',
      'RO' => '40',
      'TK' => '690',
      'GW' => '245',
      'GU' => '1671',
      'GT' => '502',
      'GR' => '30',
      'GQ' => '240',
      'GP' => '590',
      'JP' => '81',
      'GY' => '592',
      'GG' => '441481',
      'GF' => '594',
      'GE' => '995',
      'GD' => '1473',
      'GB' => '44',
      'GA' => '241',
      'SV' => '503',
      'GN' => '224',
      'GM' => '220',
      'GL' => '299',
      'GI' => '350',
      'GH' => '233',
      'OM' => '968',
      'TN' => '216',
      'JO' => '962',
      'HR' => '385',
      'HT' => '509',
      'HU' => '36',
      'HK' => '852',
      'HN' => '504',
      'VE' => '58',
      'PR' => '1787',  // and 1-939
      'PS' => '970',
      'PW' => '680',
      'PT' => '351',
      'SJ' => '47',
      'PY' => '595',
      'IQ' => '964',
      'PA' => '507',
      'PF' => '689',
      'PG' => '675',
      'PE' => '51',
      'PK' => '92',
      'PH' => '63',
      'PN' => '870',
      'PL' => '48',
      'PM' => '508',
      'ZM' => '260',
      'EH' => '212',
      'EE' => '372',
      'EG' => '20',
      'ZA' => '27',
      'EC' => '593',
      'IT' => '39',
      'VN' => '84',
      'SB' => '677',
      'ET' => '251',
      'SO' => '252',
      'ZW' => '263',
      'SA' => '966',
      'ES' => '34',
      'ER' => '291',
      'ME' => '382',
      'MD' => '373',
      'MG' => '261',
      'MF' => '590',
      'MA' => '212',
      'MC' => '377',
      'UZ' => '998',
      'MM' => '95',
      'ML' => '223',
      'MO' => '853',
      'MN' => '976',
      'MH' => '692',
      'MK' => '389',
      'MU' => '230',
      'MT' => '356',
      'MW' => '265',
      'MV' => '960',
      'MQ' => '596',
      'MP' => '1670',
      'MS' => '1664',
      'MR' => '222',
      'IM' => '441624',
      'UG' => '256',
      'TZ' => '255',
      'MY' => '60',
      'MX' => '52',
      'IL' => '972',
      'FR' => '33',
      'IO' => '246',
      'SH' => '290',
      'FI' => '358',
      'FJ' => '679',
      'FK' => '500',
      'FM' => '691',
      'FO' => '298',
      'NI' => '505',
      'NL' => '31',
      'NO' => '47',
      'NA' => '264',
      'VU' => '678',
      'NC' => '687',
      'NE' => '227',
      'NF' => '672',
      'NG' => '234',
      'NZ' => '64',
      'NP' => '977',
      'NR' => '674',
      'NU' => '683',
      'CK' => '682',
      'CI' => '225',
      'CH' => '41',
      'CO' => '57',
      'CN' => '86',
      'CM' => '237',
      'CL' => '56',
      'CC' => '61',
      'CA' => '1',
      'CG' => '242',
      'CF' => '236',
      'CD' => '243',
      'CZ' => '420',
      'CY' => '357',
      'CX' => '61',
      'CR' => '506',
      'CW' => '599',
      'CV' => '238',
      'CU' => '53',
      'SZ' => '268',
      'SY' => '963',
      'SX' => '599',
      'KG' => '996',
      'KE' => '254',
      'SS' => '211',
      'SR' => '597',
      'KI' => '686',
      'KH' => '855',
      'KN' => '1869',
      'KM' => '269',
      'ST' => '239',
      'SK' => '421',
      'KR' => '82',
      'SI' => '386',
      'KP' => '850',
      'KW' => '965',
      'SN' => '221',
      'SM' => '378',
      'SL' => '232',
      'SC' => '248',
      'KZ' => '7',
      'KY' => '1345',
      'SG' => '65',
      'SE' => '46',
      'SD' => '249',
      'DO' => '1809',  // and 1-829
      'DM' => '1767',
      'DJ' => '253',
      'DK' => '45',
      'VG' => '1284',
      'DE' => '49',
      'YE' => '967',
      'DZ' => '213',
      'US' => '1',
      'UY' => '598',
      'YT' => '262',
      'UM' => '1',
      'LB' => '961',
      'LC' => '1758',
      'LA' => '856',
      'TV' => '688',
      'TW' => '886',
      'TT' => '1868',
      'TR' => '90',
      'LK' => '94',
      'LI' => '423',
      'LV' => '371',
      'TO' => '676',
      'LT' => '370',
      'LU' => '352',
      'LR' => '231',
      'LS' => '266',
      'TH' => '66',
      'TG' => '228',
      'TD' => '235',
      'TC' => '1649',
      'LY' => '218',
      'VA' => '379',
      'VC' => '1784',
      'AE' => '971',
      'AD' => '376',
      'AG' => '1268',
      'AF' => '93',
      'AI' => '1264',
      'VI' => '1340',
      'IS' => '354',
      'IR' => '98',
      'AM' => '374',
      'AL' => '355',
      'AO' => '244',
      'AS' => '1684',
      'AR' => '54',
      'AU' => '61',
      'AT' => '43',
      'AW' => '297',
      'IN' => '91',
      'AX' => '35818',
      'AZ' => '994',
      'IE' => '353',
      'ID' => '62',
      'UA' => '380',
      'QA' => '974',
      'MZ' => '258',
  );
}

/**
 * Returns a country code given a calling prefix, false if not found
 *
 * @return string
 */
function getCountryCodeFromCallingPrefix($callingPrefix)
{
    $codes = getCallingCodesMap();
    return array_search( strtoupper( $callingPrefix ), $codes );
}

/**
 * Returns a calling prefix given a country code, null if not found
 *
 * @return string
 */
function getCallingPrefixFromCountryCode($countryCode)
{
    $codes = getCallingCodesMap();
    return isset( $codes[$countryCode] ) ? $codes[$countryCode] : null;
}

/**
 * Returns map of NANP codes to country codes
 *
 * @return array
 */
function getNANPMap()
{
  return array(
    201 => 'US',
    202 => 'US',
    203 => 'US',
    204 => 'US',
    205 => 'US',
    206 => 'US',
    207 => 'US',
    208 => 'US',
    209 => 'US',
    210 => 'US',
    212 => 'US',
    213 => 'US',
    214 => 'US',
    215 => 'US',
    216 => 'US',
    217 => 'US',
    218 => 'US',
    219 => 'US',
    224 => 'US',
    225 => 'US',
    226 => 'US',
    228 => 'US',
    229 => 'US',
    231 => 'US',
    234 => 'US',
    239 => 'US',
    240 => 'US',
    242 => 'US',
    246 => 'US',
    248 => 'US',
    250 => 'US',
    251 => 'US',
    252 => 'US',
    253 => 'US',
    254 => 'US',
    256 => 'US',
    260 => 'US',
    262 => 'US',
    264 => 'US',
    267 => 'US',
    268 => 'US',
    269 => 'US',
    270 => 'US',
    276 => 'US',
    281 => 'US',
    284 => 'US',
    289 => 'US',
    301 => 'US',
    302 => 'US',
    303 => 'US',
    304 => 'US',
    305 => 'US',
    306 => 'US',
    307 => 'US',
    308 => 'US',
    309 => 'US',
    310 => 'US',
    312 => 'US',
    313 => 'US',
    314 => 'US',
    315 => 'US',
    316 => 'US',
    317 => 'US',
    318 => 'US',
    319 => 'US',
    320 => 'US',
    321 => 'US',
    323 => 'US',
    325 => 'US',
    330 => 'US',
    334 => 'US',
    336 => 'US',
    337 => 'US',
    339 => 'US',
    340 => 'US',
    345 => 'US',
    347 => 'US',
    351 => 'US',
    352 => 'US',
    360 => 'US',
    361 => 'US',
    386 => 'US',
    401 => 'US',
    402 => 'US',
    403 => 'US',
    404 => 'US',
    405 => 'US',
    406 => 'US',
    407 => 'US',
    408 => 'US',
    409 => 'US',
    410 => 'US',
    412 => 'US',
    413 => 'US',
    414 => 'US',
    415 => 'US',
    416 => 'US',
    417 => 'US',
    418 => 'US',
    419 => 'US',
    423 => 'US',
    424 => 'US',
    425 => 'US',
    430 => 'US',
    432 => 'US',
    434 => 'US',
    435 => 'US',
    438 => 'US',
    440 => 'US',
    441 => 'US',
    443 => 'US',
    450 => 'US',
    456 => 'US',
    469 => 'US',
    473 => 'US',
    478 => 'US',
    479 => 'US',
    480 => 'US',
    484 => 'US',
    500 => 'US',
    501 => 'US',
    502 => 'US',
    503 => 'US',
    504 => 'US',
    505 => 'US',
    506 => 'US',
    507 => 'US',
    508 => 'US',
    509 => 'US',
    510 => 'US',
    512 => 'US',
    513 => 'US',
    514 => 'US',
    515 => 'US',
    516 => 'US',
    517 => 'US',
    518 => 'US',
    519 => 'US',
    520 => 'US',
    530 => 'US',
    540 => 'US',
    541 => 'US',
    551 => 'US',
    559 => 'US',
    561 => 'US',
    562 => 'US',
    563 => 'US',
    567 => 'US',
    570 => 'US',
    571 => 'US',
    573 => 'US',
    574 => 'US',
    580 => 'US',
    585 => 'US',
    586 => 'US',
    600 => 'US',
    601 => 'US',
    602 => 'US',
    603 => 'US',
    604 => 'US',
    605 => 'US',
    606 => 'US',
    607 => 'US',
    608 => 'US',
    609 => 'US',
    610 => 'US',
    612 => 'US',
    613 => 'US',
    614 => 'US',
    615 => 'US',
    616 => 'US',
    617 => 'US',
    618 => 'US',
    619 => 'US',
    620 => 'US',
    623 => 'US',
    626 => 'US',
    630 => 'US',
    631 => 'US',
    636 => 'US',
    641 => 'US',
    646 => 'US',
    647 => 'US',
    650 => 'US',
    651 => 'US',
    660 => 'US',
    661 => 'US',
    662 => 'US',
    664 => 'US',
    670 => 'US',
    671 => 'US',
    678 => 'US',
    682 => 'US',
    684 => 'US',
    700 => 'US',
    701 => 'US',
    702 => 'US',
    703 => 'US',
    704 => 'US',
    705 => 'US',
    706 => 'US',
    707 => 'US',
    708 => 'US',
    709 => 'US',
    710 => 'US',
    712 => 'US',
    713 => 'US',
    714 => 'US',
    715 => 'US',
    716 => 'US',
    717 => 'US',
    718 => 'US',
    719 => 'US',
    720 => 'US',
    724 => 'US',
    727 => 'US',
    731 => 'US',
    732 => 'US',
    734 => 'US',
    740 => 'US',
    754 => 'US',
    757 => 'US',
    758 => 'US',
    760 => 'US',
    762 => 'US',
    763 => 'US',
    765 => 'US',
    767 => 'US',
    769 => 'US',
    770 => 'US',
    772 => 'US',
    773 => 'US',
    774 => 'US',
    775 => 'US',
    778 => 'US',
    780 => 'US',
    781 => 'US',
    784 => 'US',
    785 => 'US',
    786 => 'US',
    787 => 'US',
    800 => 'US',
    801 => 'US',
    802 => 'US',
    803 => 'US',
    804 => 'US',
    805 => 'US',
    806 => 'US',
    807 => 'US',
    808 => 'US',
    809 => 'US',
    810 => 'US',
    812 => 'US',
    813 => 'US',
    814 => 'US',
    815 => 'US',
    816 => 'US',
    817 => 'US',
    818 => 'US',
    819 => 'US',
    828 => 'US',
    829 => 'US',
    830 => 'US',
    831 => 'US',
    832 => 'US',
    843 => 'US',
    845 => 'US',
    847 => 'US',
    848 => 'US',
    850 => 'US',
    856 => 'US',
    857 => 'US',
    858 => 'US',
    859 => 'US',
    860 => 'US',
    862 => 'US',
    863 => 'US',
    864 => 'US',
    865 => 'US',
    866 => 'US',
    867 => 'US',
    869 => 'US',
    870 => 'US',
    876 => 'US',
    877 => 'US',
    878 => 'US',
    888 => 'US',
    900 => 'US',
    901 => 'US',
    902 => 'US',
    903 => 'US',
    904 => 'US',
    905 => 'US',
    906 => 'US',
    907 => 'US',
    908 => 'US',
    909 => 'US',
    910 => 'US',
    912 => 'US',
    913 => 'US',
    914 => 'US',
    915 => 'US',
    916 => 'US',
    917 => 'US',
    918 => 'US',
    919 => 'US',
    920 => 'US',
    925 => 'US',
    928 => 'US',
    931 => 'US',
    936 => 'US',
    937 => 'US',
    939 => 'US',
    940 => 'US',
    941 => 'US',
    947 => 'US',
    949 => 'US',
    951 => 'US',
    952 => 'US',
    954 => 'US',
    956 => 'US',
    970 => 'US',
    971 => 'US',
    972 => 'US',
    973 => 'US',
    978 => 'US',
    979 => 'US',
    980 => 'US',
    985 => 'US',
    989 => 'US',
    403 => 'CA',
    587 => 'CA',
    780 => 'CA',
    250 => 'CA',
    778 => 'CA',
    236 => 'CA',
    604 => 'CA',
    204 => 'CA',
    431 => 'CA',
    506 => 'CA',
    709 => 'CA',
    902 => 'CA',
    762 => 'CA',
    416 => 'CA',
    647 => 'CA',
    437 => 'CA',
    519 => 'CA',
    226 => 'CA',
    613 => 'CA',
    343 => 'CA',
    705 => 'CA',
    249 => 'CA',
    807 => 'CA',
    905 => 'CA',
    289 => 'CA',
    365 => 'CA',
    289 => 'CA',
    365 => 'CA',
    902 => 'CA',
    762 => 'CA',
    418 => 'CA',
    581 => 'CA',
    450 => 'CA',
    579 => 'CA',
    514 => 'CA',
    438 => 'CA',
    819 => 'CA',
    873 => 'CA',
    306 => 'CA',
    639 => 'CA',
    867 => 'CA',
    684 => 'AS',
    264 => 'AI',
    268 => 'AG',
    242 => 'BS',
    246 => 'BB',
    441 => 'BM',
    284 => 'IO',
    345 => 'KY',
    767 => 'DM',
    809 => 'DO',
    829 => 'DO',
    849 => 'DO',
    473 => 'GD',
    671 => 'GU',
    876 => 'JM',
    664 => 'MS',
    670 => 'MP',
    787 => 'PR',
    939 => 'PR',
    869 => 'KN',
    758 => 'LC',
    784 => 'VC',
    721 => 'MF',
    868 => 'TT',
    649 => 'TC',
    340 => 'UM',
  );
}

/**
 * Checks whether a given country is an NANP country
 *
 * @return boolean
 */
function isNANPCountry( $country )
{
  $codes = getNANPMap();
  return array_search( strtoupper( $country ), $codes ) !== false;
}


/**
 * is_language_encoded
 * return TRUE if language strings are Java Escape encoded
 * @param String language code (EN, ES, ZH)
 * @return Boolean TRUE if encoded, FALSE otherwise
 */
function is_language_encoded($lang)
{
  $encoded = array('ZH');
  return in_array(strtoupper($lang), $encoded);
}
