<?php

require_once 'Ultra/tests/API/DefaultUltraAPITest.php';
require_once 'Ultra/tests/API/TestConstants.php';
require_once 'lib/util-common.php';

/**
 * PHPUnit tests for Ultra/Lib/Util/Locale.php
 */
class CustomerCareTest extends PHPUnit_Framework_TestCase
{
  public function setUp()
  {

  }

  /**
   * test_is_english
   */
  public function test_is_english()
  {
    $l = 'en';
    $this->assertTrue(is_english($l));

    $l = 'zh';
    $this->assertTrue(is_english($l));

    $l = 'ZH';
    $this->assertTrue(is_english($l));
    
    $l = 'en-us';
    $this->assertTrue(is_english($l));

    $l = 'en-US';
    $this->assertTrue(is_english($l));
    
    $l = 'es';
    $this->assertFalse(is_english($l));
  }

  /**
   * test_is_mandarin
   */
  public function test_is_mandarin()
  {
    $l = 'en';
    $this->assertFalse(is_mandarin($l));

    $l = 'zh';
    $this->assertTrue(is_mandarin($l));

    $l = 'ZH';
    $this->assertTrue(is_mandarin($l));
    
    $l = 'en-us';
    $this->assertFalse(is_mandarin($l));
    
    $l = 'es';
    $this->assertFalse(is_mandarin($l));
  }

  /**
   * test_convertUnicodeFromJavaEscape
   */
  public function test_convertUnicodeFromJavaEscape()
  {
    $str = 'I\'m feeling \u2620 fine.';

    echo PHP_EOL . $str . PHP_EOL;

    $uni = convertUnicodeFromJavaEscape($str);

    echo $uni . PHP_EOL;
    $this->assertFalse(($str == $uni));

    $uni = convertUnicodeToJavaEscape($uni);

    echo $uni . PHP_EOL;
    $this->assertTrue(($str == $uni));
  }

  /**
   * test_convertUnicodeFromJavaEscape
   */
  public function test_convertUnicodeToJavaEscape()
  {
    $str = 'I\'m feeling ☠ fine.';

    echo PHP_EOL . $str . PHP_EOL;

    $esc = convertUnicodeToJavaEscape($str);

    echo $esc . PHP_EOL;
    $this->assertFalse(($str == $esc));

    $esc = convertUnicodeFromJavaEscape($esc);

    echo $esc . PHP_EOL;
    $this->assertTrue(($str == $esc));
  }
}
