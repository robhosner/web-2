#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::Util::Settings;


# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/Settings_test.log';


my $db = Ultra::Lib::DB::MSSQL->new();

my $settings = Ultra::Lib::Util::Settings->new( DB => $db , CONFIG => $db->{ CONFIG } );

my $value = $settings->checkUltraSettings('test');

print STDOUT "value = $value\n";

$value = $settings->checkUltraSettings('test_failure');

print STDOUT "value = $value\n";

__END__

sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev ./Ultra/Lib/Util/Settings_test.pl'

