<?php

namespace Ultra\Lib\Util;

/**
 * Class used to interface w/SQLite DB.
 * @see http://php.net/manual/en/book.sqlite3.php
 * @author bwalters@ultra.me
 */
class SQLite {

  // Public

  // Protected
  protected $lastQuery = '';
  protected $result = null;
  protected $resultCount = 0;

  // Private
  // private $working_directory = '/var/tmp/Ultra/Intake/';
  private $working_directory = '/tmp/';
  private $filename = null;
  private $instance = null;

  /**
   * Creates a new instance of SQLite class.
   * @see http://php.net/manual/en/sqlite3.construct.php
   * @param string $filename Location of database file
   * @param integer $flags SQLITE3_OPEN_READONLY, SQLITE3_OPEN_READWRITE, or SQLITE3_OPEN_CREATE. Defaults to SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE
   * @return SQLite
   * @throws \Exception
   */
  public function __construct($filename = false, $flags = false, $encryption_key = null)
  {
    if (!$filename) throw new \Exception("Missing SQLite database location");
    $flags = ($flags) ? $flags : (SQLITE3_OPEN_READWRITE|SQLITE3_OPEN_CREATE);
    $this->filename = $this->working_directory . $filename;
    $this->instance = new \SQLite3($filename, $flags, $encryption_key);
    $this->connected = true;
  }

  /**
   * Returns the last error code.
   * @return integer
   */
  public function getLastErrorCode()
  {
    return $this->instance->lastErrorCode();
  }

  /**
   * Returns the last error in a human readable format.
   * @return string
   */
  public function getLastErrorMessage()
  {
    return $this->instance->lastErrorMsg();
  }

  /**
   * Returns the last executed query.
   * @return string
   */
  public function getLastQuery()
  {
    return $this->lastQuery;
  }

  /**
   * Opens a new db.
   * @see http://php.net/manual/en/sqlite3.construct.php
   * @param string $filename Location of database file
   * @param integer $flags SQLITE3_OPEN_READONLY, SQLITE3_OPEN_READWRITE, or SQLITE3_OPEN_CREATE. Defaults to SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE
   * @throws \Exception
   */
  public function open($filename = false, $flags = false, $encryption_key = null)
  {
    if (!$filename) throw new \Exception("Missing SQLite database location");
    $flags = ($flags) ? $flags : SQLITE3_OPEN_READWRITE|SQLITE3_OPEN_CREATE;
    $this->instance = new \SQLite3($filename, $flags, $encryption_key);
    $this->connected = true;

    return true;
  }

  /**
   * Closes the db.
   */
  public function close()
  {
    $this->connected = false;
    $this->instance->close();
  }

  /**
   * Will return the number of results.
   * @return integer
   */
  public function getResultCount()
  {
    return $this->resultCount;
  }

  /**
   * Some of us like to use objects vs arrays.
   * @return \ArrayObject
   */
  public function getFormattedResults()
  {
    return new \ArrayObject($this->result, \ArrayObject::ARRAY_AS_PROPS);
  }

  /**
   * Gets the last result from a previous query.
   * @return array
   */
  public function getLastResult()
  {
    return $this->result;
  }

  /**
   * Escapes a string
   */
  public function escapeString($string)
  {
    return $this->instance->escapeString($string);
  }

  /**
   * Create a table given a specific definition.
   * @param string $table_name
   * @param array $column_definitions
   * @return boolean
   * @throws \Exception
   */
  public function createTable($table_name, $column_definitions)
  {
    if (!$this->connected)    throw new \Exception("Not connected to a database");
    if (!$table_name)         throw new \Exception("Missing table name");
    if (!$column_definitions) throw new \Exception("Missing column definitions");

    $this->initializeResults();

    $column_definition_statement = '';
    for ($i = 0, $l = count($column_definitions); $i < $l; $i++)
    {
      $name = $column_definitions[$i]['name'];
      $type = $column_definitions[$i]['type'];

      $column_definition_statement .= "{$name} {$type}";
      if ($i != ($l - 1))
      {
        $column_definition_statement .= ', ';
      }
    }

    $query = "CREATE TABLE {$table_name} ({$column_definition_statement});";
    $this->generateLastQuery($query);

    return $this->instance->exec($query);
  }

  /**
   * Given a table name, will delete it.
   * @param string $table_name
   * @return boolean
   * @throws \Exception
   */
  public function dropTable($table_name)
  {
    if (!$this->connected)    throw new \Exception("Not connected to a database");
    if (!$table_name)         throw new \Exception("Missing table name");

    $this->initializeResults();

    $query = "DROP TABLE {$table_name};";
    $this->generateLastQuery($query);

    return $this->instance->exec($query);
  }

  /**
   * Truncates a table and reclaims unused space (defrag?)
   * @param string $table_name
   * @return boolean
   * @throws \Exception
   */
  public function truncateTable($table_name)
  {
    if (!$this->connected)    throw new \Exception("Not connected to a database");
    if (!$table_name)         throw new \Exception("Missing table name");

    $this->initializeResults();

    $query = "DELETE FROM {$table_name}; VACUUM;";
    $this->generateLastQuery($query);

    return $this->instance->exec($query);
  }

  /**
   * Will insert a row into the desired table.
   * @param string $table_name
   * @param array $data
   * @param string|array $where
   * @return boolean
   * @throws \Exception
   */
  public function insertRow($table_name, $data, $where = false)
  {
    if (!$this->connected)  throw new \Exception("Not connected to a database");
    if (!$table_name)       throw new \Exception("Missing table name");
    if (!$data)             throw new \Exception("Missing columns and values");

    $this->initializeResults();

    $columns      = '';
    $placeholders = '';

    // Prepare columns & value portion of insert statement
    for ($i = 0, $l = count($data); $i < $l; $i++)
    {
      $columns .= str_replace(":", "", $data[$i]['name']);
      $placeholders .= $data[$i]['name'];
      if ($i != ($l - 1))
      {
        $columns .= ', ';
        $placeholders .= ', ';
      }
    }

    // Construct the insert statement w/o WHERE clause
    $query = "INSERT INTO {$table_name} ({$columns}) VALUES ({$placeholders});";

    $this->generateLastQuery($query, $data, 'name');
    if (!$statement = $this->instance->prepare($query)) throw new \Exception($this->getLastErrorMessage(), $this->getLastErrorCode());
    $statement = $this->bindParametersToStatement($statement, $data);

    if (!$result = $statement->execute()) throw new \Exception($this->getLastErrorMessage(), $this->getLastErrorCode());
    // DANGER: If this is not here, we will get duplicate insert values inserted into the table; If it's in
    // processResultsFromStatement() we will get the error "The SQLite3Result object has not been correctly initialised".
    $statement->close();

    //return $this->processResultsFromStatement($result);
    return true;
  }

  /**
   * Will select a specified set of data from a table.
   * @param string $table_name
   * @param array $fields
  * @param string|array $where
   * @return \SQLite3Result
   */
  public function select($table_name, $fields = false, $where = false)
  {
    if (!$this->connected) throw new \Exception("Not connected to a database");

    $this->initializeResults();

    // Construct select query

    $fields = (is_array($fields)) ? join(",", $fields) : '*';
    $query = "SELECT {$fields} FROM {$table_name}";

    // Construct where clause if applicable
    $where_clause = $this->constructWhereClause($where);

    // Construct the whole query w/proper termination
    $query = $query . $where_clause . ";";

    $this->generateLastQuery($query);
    if (!$statement = $this->instance->prepare($query)) throw new \Exception($this->getLastErrorMessage(), $this->getLastErrorCode());
    // @TODO: Implement parameterized statements later
    // $statement = $this->bindParametersToStatement($statement, $parameters);

    if (!$result = $statement->execute()) throw new \Exception($this->getLastErrorMessage(), $this->getLastErrorCode());

    return $this->processResultsFromStatement($result, $statement);
  }

  /**
   * Will update n rows in the desired table.
   * @param string $table_name
   * @param array $fields
   * @param string|array $where
   * @return boolean
   * @throws \Exception
   */
  public function update($table_name, $fields, $where = false)
  {
    if (!$this->connected)  throw new \Exception("Not connected to a database");
    if (!$table_name)       throw new \Exception("Missing table name");
    if (!$fields)           throw new \Exception("Missing columns and values");

    $this->initializeResults();

    $update_clause = '';

    // Prepare columns & value portion of insert statement
    for ($i = 0, $l = count($fields); $i < $l; $i++)
    {
      $field  = $fields[$i]['field'];
      $value  = $fields[$i]['value'];
      $update_clause .= "{$field} = '{$value}'";
      if ($i != ($l - 1))
      {
        $columns .= ', ';
      }
    }

    $query = "UPDATE {$table_name} SET {$update_clause}";

    // Construct where clause if applicable
    $where_clause = $this->constructWhereClause($where);

    // Construct the whole query w/proper termination
    $query = $query . $where_clause . ";";
    $this->generateLastQuery($query);

    return $this->instance->exec($query);
  }

   /**
   * Will delete n rows in the desired table.
   * @param string $table_name
   * @param string|array $where
   * @return boolean
   * @throws \Exception
   */
  public function delete($table_name, $where = false)
  {
    if (!$this->connected)  throw new \Exception("Not connected to a database");
    if (!$table_name)       throw new \Exception("Missing table name");
    if (!$where)            throw new \Exception("Missing where clause");

    $this->initializeResults();

    $query = "DELETE FROM {$table_name}";

    // Construct where clause if applicable
    $where_clause = $this->constructWhereClause($where);

    // Construct the whole query w/proper termination
    $query = $query . $where_clause . ";";
    $this->generateLastQuery($query);

    return $this->instance->exec($query);
  }

  /**
   * Executes a query with parameters if given.
   * @see http://php.net/manual/en/sqlite3.constants.php for datatypes
   * @param string $query
   * @param array $parameters
   * @return boolean|array
   * @throws \Exception
   */
  public function query($query = false, array $parameters = array())
  {
    if (!$this->connected) throw new \Exception("Not connected to a database");
    if (!$query) throw new \Exception('Query is required');

    $this->initializeResults();

    $this->generateLastQuery($query, $parameters);
    if (!$statement = $this->instance->prepare($query)) throw new \Exception($this->getLastErrorMessage(), $this->getLastErrorCode());
    $statement = $this->bindParametersToStatement($statement, $parameters);

    if (!$result = $statement->execute()) throw new \Exception($this->getLastErrorMessage(), $this->getLastErrorCode());


    return $this->processResultsFromStatement($result);
  }

  /**
   * Initializes the results array.
   * @return boolean
   */
  private function initializeResults()
  {
    $this->resultCount = 0;
    $this->result = array();

    return true;
  }

  /**
   * Will bind given parameters to a statement.
   * @param \SQLite3Stmt $statement
   * @param array $parameters
   * @return \SQLite3Stmt
   * @throws \Exception
   */
  private function bindParametersToStatement(\SQLite3Stmt $statement, array $parameters)
  {
    foreach ($parameters as $parameter)
    {
      if (!isset($parameter['name']))   throw new \Exception("Missing name in parameter list!");
      if (!isset($parameter['value']))  throw new \Exception("Missing value in parameter list!");
      if (!isset($parameter['type']))   throw new \Exception("Missing type in parameter list!");

      $statement->bindValue($parameter['name'], $parameter['value'], $parameter['type']);
    }

    return $statement;
  }

  /**
   * Constructs the WHERE a b c (AND x y z) clause for a SQL query.
   * @param string|array $where
   * @return string
   * @throws \Exception
   */
  private function constructWhereClause($where = false)
  {
    $where_clause = (($where !== false) ? ' WHERE ' : '');

    if (is_array($where) && count($where) > 0)
    {
      for ($i = 0, $l = count($where); $i < $l; $i++)
      {
        // Error checking
        if (!isset($where[$i]['field']))      throw new \Exception("Missing field parameter in where clause!");
        if (!isset($where[$i]['operator']))   throw new \Exception("Missing operator parameter in where clause!");
        if (!isset($where[$i]['comparison'])) throw new \Exception("Missing comparison parameter in where clause!");

        $field      = $where[$i]['field'];
        $operator   = $where[$i]['operator'];
        $comparison = $where[$i]['comparison'];

        // i/e: first_name = Ben
        // i/e: id >= 28394
        $where_clause .= "{$field} {$operator} '{$comparison}'";

        if ($i != ($l - 1))
        {
          $columns .= ' AND ';
        }
      }
    } else if (strlen($where) > 0) {
      $where_clause = $where;
    }

    return $where_clause;
  }

  /**
   * Replaces the placeholders in a query with actual values
   * @param string $query
   * @param array|boolean $column_definitions
   * @return boolean
   */
  private function generateLastQuery($query, $column_definitions = false)
  {
    if ($column_definitions)
    {
      foreach ($column_definitions as $definition)
      {
        if (!isset($definition['name']))  throw new \Exception('Missing name in parameter list');
        if (!isset($definition['value'])) throw new \Exception('Missing value in parameter list');
        $query = str_replace($definition['name'], $definition['value'], $query);
      }
    }

    $this->lastQuery = $query;

    return true;
  }

  /**
   * Parses response from SQLite3Stmt returning formatted results.
   * @param string|array $where
   * @param \SQLite3Stmt $statement
   * @return array|boolean
   * @throws \Exception
   */
  private function processResultsFromStatement($result, $statement = false)
  {
    $this->initializeResults();

    if ($result instanceof \SQLite3Result)
    {
      while ($row = $result->fetchArray())
      {
        $this->resultCount++;
        $this->result[] = $row;
      }

      if ($statement) $statement->close();

      // We do this so we can return the expected results
      if ($result === false)
      {
        return false;
      }
      else if ($this->getResultCount() > 0)
      {
        return $this->result;
      }
      else
      {
        return true;
      }
    }

    return $result;
  }
}