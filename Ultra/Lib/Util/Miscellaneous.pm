package Ultra::Lib::Util::Miscellaneous;

use strict;
use warnings;


use Data::Dumper;
use LWP::UserAgent 6;


=head1 NAME

Ultra::Lib::Util::Miscellaneous

Miscellaneous static methods, not specific to any particular application.

=cut


=head1 Methods

=head4 cleanUpDataStructure

  Given a complex data structure, removes undef values, empty array references and empty hash references.

=cut
sub cleanUpDataStructure
{
  my ($dataStructure) = @_;

  my $cleanedDataStructure;

  if ( ref $dataStructure eq 'ARRAY' )
  {
    $cleanedDataStructure = [];

    if ( @$dataStructure )
    {
      for my $element( @$dataStructure )
      {
        my $element = cleanUpDataStructure($element);

        if ( $element )
        {
          push(@$cleanedDataStructure,$element);
        }
      }
    }
    else
    {
      $cleanedDataStructure = undef;
    }
  }
  elsif ( ref $dataStructure eq 'HASH' )
  {
    $cleanedDataStructure = {};

    if ( keys %$dataStructure )
    {
      $cleanedDataStructure = $dataStructure;
    }
    else
    {
      $cleanedDataStructure = undef;
    }
  }
  else
  {
    $cleanedDataStructure = $dataStructure;
  }

  return $cleanedDataStructure;
}


=head4 getTimeStamp

  Returns a timestamp in the format YYYY-MM-DDThh:mm:ss

=cut
sub getTimeStamp
{
  my ($sec,$min,$hr,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

  sprintf "%04d-%02d-%02dT%02d:%02d:%02d", ($year + 1900, ++$mon, $mday, $hr, $min, $sec);
}


=head4 userAgentRequest

  Performs an HTTP request using LWP::UserAgent

=cut
sub userAgentRequest
{
  my ($url,$data,$seconds_timeout,$ua) = @_;

  if ( !$ua )
  {
    $ua = LWP::UserAgent->new;
  }

  $seconds_timeout ||= 600;

  my $timeout  = 0;
  my $response = undef;

  eval
  {
    local $SIG{ALRM} = sub { die "timeout\n"; };
    alarm $seconds_timeout;

    $response = $ua->post($url,$data);

    alarm 0;
  };

  if ($@)
  {
    print STDERR Dumper($@);
    $timeout = 1;
  }

  return ($response,$timeout);
}


1;


__END__

Miscellaneous static methods

