package Ultra::Lib::Util::XML;


use strict;
use warnings;


use XML::LibXML;
use XML::LibXML::PrettyPrint;
use XML::Simple;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::Util::XML

=head1 Perl module for generic XML utility methods

=head1 SYNOPSIS

  use Ultra::Lib::Util::XML;

=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ LOG_ENABLED } = 1;

  $this->SUPER::_initialize($data);
}


=head4 extractResponseData

  Extract data from SOAP response

=cut
sub extractResponseData
{
  my ($this,$soapXMLResponse) = @_;

  my $responseData = {};

  my $data;

  eval
  {
    my $xmlSimpleObject = XML::Simple->new();

    $data = $xmlSimpleObject->XMLin( $soapXMLResponse );
  };

  if ( $@ || ( ! $data ) )
  {
    $this->addError("cannot extract data from XML ($@)");

    return 0;
  }

  for my $key ( keys %$data )
  {
    if ( $key =~ /^xmlns\:/ )
    {
      delete $data->{ $key };
    }
  }

  $data = $data->{ ( %$data )[0] };
  $data = $data->{ ( %$data )[0] };

  for my $key ( keys %$data )
  {
    my $cleanedKey = $key;

    if ( $cleanedKey =~ /\:(.+)$/ ) { $cleanedKey = $1; }

    $responseData->{ $cleanedKey } = $data->{ $key };

    if ( ref $responseData->{ $cleanedKey } eq 'HASH' )
    {
      for my $innerKey ( keys % { $responseData->{ $cleanedKey } } )
      {
        my $cleanedInnerKey = $innerKey;

        chomp $responseData->{ $cleanedKey }->{ $innerKey };
        $responseData->{ $cleanedKey }->{ $innerKey } =~ s/^\s+//g;
        $responseData->{ $cleanedKey }->{ $innerKey } =~ s/\s+$//g;

        if ( $cleanedInnerKey =~ /\:(.+)$/ ) { $cleanedInnerKey = $1; }

        if ( $cleanedInnerKey ne $innerKey )
        {
          $responseData->{ $cleanedKey }->{ $cleanedInnerKey } = $responseData->{ $cleanedKey }->{ $innerKey };

          delete $responseData->{ $cleanedKey }->{ $innerKey };
        }
      }
    }
    else
    {
      chomp $responseData->{ $cleanedKey };
      $responseData->{ $cleanedKey } =~ s/^\s+//g;
      $responseData->{ $cleanedKey } =~ s/\s+$//g;
    }

    if ( ( ( ref $responseData->{ $cleanedKey } eq 'HASH' ) && ( ! keys % { $responseData->{ $cleanedKey } } ) )
      || ( $cleanedKey eq 'xmlns' )
      || ( $cleanedKey eq 'tns' )
      || ( $cleanedKey eq 'xsi' ) )
    {
      delete $responseData->{ $cleanedKey };
    }
  }

  return $responseData;
}


=head4 prettyPrintXML

  Make XML human-readable.

=cut
sub prettyPrintXML
{
  my ($this,$xml) = @_;

  my $xmlOutput = $xml;

  local $@;

  eval
  {
    my $xmlObject = XML::LibXML->load_xml( string => $xml );

    my $prettyPrinter = XML::LibXML::PrettyPrint->new( indent_string => "  " );

    $prettyPrinter->pretty_print( $xmlObject );

    $xmlOutput = $xmlObject->toString;
  };

  if ( $@ )
  {
    $this->log( "prettyPrintXML failed : $@" );

    # attach XML parsing error to output
    $xmlOutput .= "\n$@";
  }

  return $xmlOutput;
}


=head4 extractSOAPXMLEnvelope

  Removes SOAP Envelope from an XML string.

=cut
sub extractSOAPXMLEnvelope
{
  my ($this,$soapXML) = @_;

  # remove <?xml version="1.0" encoding="UTF-8"?>
  $soapXML =~ s/^[^\<]*\<\?xml version[^\>]+\>//i;

  # remove <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  $soapXML =~ s/^\s*\<s:Envelope\s*[^\>]*\>\s*//i;

  # remove <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  $soapXML =~ s/^\s*\<SOAP-ENV:Envelope\s*[^\>]*\>\s*//i;

  # remove <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  $soapXML =~ s/^\s*\<SOAP:Envelope\s*[^\>]*\>\s*//i;

  # remove <SOAP-ENV:Body>
  $soapXML =~ s/^\s*\<SOAP-ENV:Body\s*[^\>]*\>\s*//i;

  # remove </SOAP-ENV:Envelope>
  $soapXML =~ s/\s*\<\/SOAP-ENV:Envelope\>\s*$//i;

  # remove </SOAP-ENV:Body>
  $soapXML =~ s/\s*\<\/SOAP-ENV:Body\>\s*$//i;

  # remove </s:Envelope>
  $soapXML =~ s/\s*\<\/s:Envelope\>\s*$//i;

  # remove </soap:Envelope>
  $soapXML =~ s/\s*\<\/soap:Envelope\>\s*$//i;

  # remove </s:Body> and <s:Body>
  $soapXML =~ s/\s*\<\/?s:Body\>\s*//ig;

  # remove </soap:Body> and <soap:Body>
  $soapXML =~ s/\s*\<\/?soap:Body\>\s*//ig;

  # remove xsi:nil="true"
  $soapXML =~ s/ xsi\:nil\=\"true\"//ig;

  # remove xsi:nil="false"
  $soapXML =~ s/ xsi\:nil\=\"false\"//ig;

  # patch: DB placeholders don't like ``<ns1:``
  $soapXML =~ s/<ns1\:/</ig;
  $soapXML =~ s/<\/ns1\:/<\//ig;

  return $soapXML;
}


sub xmlify
{
  my ( $node , $data ) = @_;

  my $xml = '<'.$node.'>';

  while ( my ($key, $value) = each % { $data } )
  {
    $xml .= '<'.$key.'>'.$value.'</'.$key.'>';
  }

  $xml .= '</'.$node.'>';

  return $xml;
}


1;


__END__


