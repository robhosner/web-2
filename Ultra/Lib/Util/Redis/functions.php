<?php

namespace Ultra\Lib\Util\Redis;

define('REDIS_COVERAGE_INFO_QUALITY', 'coverage_info_quality');
define('REDIS_TRANSITION_RECOVERY',   'transition/recovery');
define('REDIS_TRANSACTION_VOIDED',    'transaction/voided');
define('REDIS_COMMAND_COUNT',         'command/count');

function get_coverage_info_quality($zipcode, $redis = NULL)
{
  $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis : $redis;

  $quality_key = REDIS_COVERAGE_INFO_QUALITY . '/' . $zipcode;

  return $redis->get($quality_key);
}

function set_coverage_info_quality($zipcode, $quality, $ttl, $redis = NULL)
{
  $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis : $redis;

  $redis->set(REDIS_COVERAGE_INFO_QUALITY . '/' . $zipcode, $quality, $ttl);
}

function set_transition_recover_attempted($transition_uuid, $redis = NULL)
{
  $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis : $redis;

  $redis->set(REDIS_TRANSITION_RECOVERY . '/' . $transition_uuid, 1, 60 * 60 * 4);
}

function get_transition_recover_attempted($transition_uuid, $redis = NULL)
{
  $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis : $redis;

  return !($redis->get(REDIS_TRANSITION_RECOVERY . '/' . $transition_uuid));
}

function get_transaction_voided_attempt($transaction_id, $redis = NULL)
{
  $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis : $redis;

  return $redis->get(REDIS_TRANSACTION_VOIDED . '/' . $transaction_id);
}

function set_transaction_voided_attempt($transaction_id, $redis = NULL)
{
  $redis = (is_null($redis)) ? new \Ultra\Lib\Util\Redis : $redis;

  $redis->set(REDIS_TRANSACTION_VOIDED . '/' . $transaction_id, 1, 60 * 60 * 24 * 2);
}


/**
 * redis_get_command_count_by_hour
 *
 * @return integer
 */
function redis_get_command_count_by_hour($redis=NULL,$command)
{
  $key_command_count = REDIS_COMMAND_COUNT . '/' . $command;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  return $redis->get( $key_command_count );
}

/**
 * redis_increment_command_count_by_hour
 *
 * @return NULL
 */
function redis_increment_command_count_by_hour($redis=NULL, $command)
{
  // Increments Redis count for commands invocation by hour
  $key_command_count = REDIS_COMMAND_COUNT . '/' . $command;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $count = $redis->get($key_command_count);

  if ($count)
    $redis->incr($key_command_count);
  else
    $redis->set($key_command_count, 1, 60 * 60);
}

/**
 * redis_increment_command_count_with_max
 *
 * Increment the redis count by one for the specified command.
 *
 * @param $redis
 * @param $command
 * @param int $max
 * @param int $ttl
 * @return null
 */
function redis_increment_command_count_with_max($redis=NULL, $command, $max = 6, $ttl = 3600)
{
  // Increments Redis count for commands invocation by hour
  $key_command_count = REDIS_COMMAND_COUNT . '/' . $command;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $count = $redis->get($key_command_count);

  if ($count)
  {
    $count = $redis->incr($key_command_count);

    if ($count > $max)
    {
      $redis->set($key_command_count, $max + 1, $ttl);
    }
  }
  else
  {
    $redis->set($key_command_count, 1, $ttl);
  }
}

/**
 * redis_reset_command_count_by_hour
 *
 * Reset values accumulated in redis_increment_command_count_by_hour
 *
 * @retuns NULL
 */
function redis_reset_command_count($redis=NULL, $command)
{
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $key_command_count = REDIS_COMMAND_COUNT . '/' . $command;
  \logDebug('ATTEMPTING TO DELETE KEY: ' . $key_command_count);
  return $redis->del($key_command_count);
}

