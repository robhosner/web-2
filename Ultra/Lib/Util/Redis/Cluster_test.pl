use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Config;
use Ultra::Lib::Util::Redis::Cluster;


my $config = Ultra::Lib::Util::Config->new();

my $ultraRedis = Ultra::Lib::Util::Redis::Cluster->new( CONFIG => $config , VERBOSE => 1 );

#my $info = $ultraRedis->monitorInfo();
#print Dumper($info);

## rpush

my $c = 0;

for my $k ( 'sal','sbl','zal','zbl','ool','iil' )
{
  print STDOUT "\n\n";

  my $success = $ultraRedis->rpush($k,'bbbb'.$c++);

  if ( $success )
  { print STDOUT "OK $k\n"; }
  else
  { print STDOUT "KO $k\n"; }
}

## lrange

for my $k ( 'sal','sbl','zal','zbl','ool','iil' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->lrange($k,0,0);

  print STDOUT "value = ".Dumper($value)."\n";
}

## lpop

for my $k ( 'sal','sbl','zal','zbl','ool','iil' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->lpop($k);

  print STDOUT "value = $value\n";
}

for my $k ( 'sal','sbl','zal','zbl','ool','iil' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->del($k);

  print STDOUT "value = $value\n";
}


## sadd

for my $k ( 'sas','sbs','zas','zbs','oos','iis' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->del($k);

  print STDOUT "value = $value\n";
}

for my $k ( 'sa','sb','za','zb','oo','ii' )
{
  print STDOUT "\n\n";

  my $success = $ultraRedis->sadd($k,'bbbb'.$c++);

  if ( $success )
  { print STDOUT "OK $k\n"; }
  else
  { print STDOUT "KO $k\n"; }
}


## smembers

for my $k ( 'sas','sbs','zas','zbs','oos','iis' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->smembers($k);

  print STDOUT "value = ".Dumper($value)."\n";
}


## srem

$c = 0;

for my $k ( 'sas','sbs','zas','zbs','oos','iis' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->srem($k,'bbbb'.$c++);

  print STDOUT "value = $value\n";
}


## set

for my $k ( 'a','aaaa','b','bbbb','test','x','z' )
{
  print STDOUT "\n\n";

  my $success = $ultraRedis->set($k,'bbbb',60);

  if ( $success )
  { print STDOUT "OK $k\n"; }
  else
  { print STDOUT "KO $k\n"; }
}


## expire

for my $k ( 'a','aaaa','b','bbbb','test','x','z' )
{
  print STDOUT "\n\n";

  my $success = $ultraRedis->expire($k,60);

  if ( $success )
  { print STDOUT "OK $k\n"; }
  else
  { print STDOUT "KO $k\n"; }
}


## exists

for my $k ( 'sa','sb','za','zb','oo','ii' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->exists($k);

  print STDOUT "value = $value\n";
}


## get

for my $k ( 'a','aaaa','b','bbbb','test','x','z' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->get($k);

  print STDOUT "value = $value\n";
}


## del

for my $k ( 'a','aaaa','b','bbbb','test','x','z' )
{
  print STDOUT "\n\n";

  my $value = $ultraRedis->del($k);

  print STDOUT "value = $value\n";
}


