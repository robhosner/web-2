<?php

namespace Ultra\Lib\Util\Redis;

require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Redis Port class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Port
{
  public $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * getRequestId
   *
   * Retrieve the request id relative to a port in attempt
   *
   * @return string
   */
  public function getRequestId( $customer_id , $plan_state )
  {
    $request_id = '';

    if ( $plan_state == 'Port-In Requested' )
      $request_id = $this->redis->get( 'ultra/port/request_id/'.$customer_id );

    return $request_id;
  }

  /**
   * setRequestId
   *
   * Memorize the request id relative to a port in attempt
   */
  public function setRequestId( $customer_id , $request_id )
  {
    return $this->redis->set( 'ultra/port/request_id/'.$customer_id , $request_id , 2 * 24 * 60 * 60 );
  }
}
