<?php

namespace Ultra\Lib\Util\Redis;

require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Redis CCQueue class
 *
 * Reis keys:
 * "ultra/sortedset/ccqueue" : CC_TRANSACTIONS sorted set
 * "ultra/ccqueue/$CC_TRANSACTIONS_ID" : CC_TRANSACTIONS_ID semaphore
 * "ultra/cc/transactions/result/$CC_TRANSACTIONS_ID" : ULTRA.CC_TRANSACTIONS.RESULT
 * "ultra/cc/transactions/status/$CC_TRANSACTIONS_ID" : ULTRA.CC_TRANSACTIONS.STATUS
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Credit Card Processor
 */
class CCQueue
{
  // ZRANGEBYSCORE ultra/sortedset/ccqueue -inf +inf
  const SORTED_SET                    = 'ultra/sortedset/ccqueue';
  const CC_TRANSACTIONS_TTL           = 120;
  const CC_TRANSACTIONS_ID_PREFIX     = 'ultra/ccqueue/';
  const CC_TRANSACTIONS_ERROR_PREFIX  = 'ultra/cc/transactions/error/';
  const CC_CUSTOMER_ID_PREFIX         = 'ultra/cc/customer_id/';
  const CC_TRANSACTIONS_STATUS_PREFIX = 'ultra/cc/transactions/status/';
  const CC_TRANSACTIONS_RESULT_PREFIX = 'ultra/cc/transactions/result/';

  private $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * popNext
   *
   * Retrieves and reserves a $cc_transactions_id
   *
   * @returns integer or NULL
   */
  public function popNext( $cc_transactions_id=NULL )
  {
    if ( ! $cc_transactions_id )
      $cc_transactions_id = $this->redis->zhead( self::SORTED_SET );

    if ( $cc_transactions_id )
    {
      dlog('',"cc_transactions_id = $cc_transactions_id");

      $pid = gethostname() . getmypid();

      // timestamp PST (server time)
      $timestamp = $this->redis->zscore( self::SORTED_SET , $cc_transactions_id );

      dlog('', "zhead returned $cc_transactions_id , with timestamp = $timestamp (".timestamp_to_date( $timestamp ).")");

      try
      {
        if ( ! $timestamp )
        {
          // removes $cc_transactions_id from the priority queue
          $this->redis->zrem( self::SORTED_SET , $cc_transactions_id );

          throw new \Exception("empty timestamp associated with CC_TRANSACTIONS_ID $cc_transactions_id");
        }

        if ( $this->redis->get( self::CC_TRANSACTIONS_ID_PREFIX . $cc_transactions_id ) )
          throw new \Exception("CC_TRANSACTIONS_ID $cc_transactions_id already reserved by another process");

        // attempt to reserve
        $return = $this->redis->setnx( self::CC_TRANSACTIONS_ID_PREFIX . $cc_transactions_id , $pid , 600 ); // 10 minutes

        if ( ! $return )
          throw new \Exception("CC_TRANSACTIONS_ID $cc_transactions_id already reserved");

        usleep(250000);

        $pid = $this->redis->get( self::CC_TRANSACTIONS_ID_PREFIX . $cc_transactions_id );

        if ( $pid != gethostname() . getmypid() )
          throw new \Exception("CC_TRANSACTIONS_ID $cc_transactions_id already reserved by process $pid");

        // removes $cc_transactions_id from the priority queue
        $this->redis->zrem( self::SORTED_SET , $cc_transactions_id );
      }
      catch(\Exception $e)
      {
        dlog('', $e->getMessage());
        $cc_transactions_id = NULL;
      }
    }

    return $cc_transactions_id;
  }

  /**
   * free
   *
   * Unreserve $cc_transactions_id
   */
  public function free( $cc_transactions_id )
  {
    $this->redis->del( self::CC_TRANSACTIONS_ID_PREFIX . $cc_transactions_id );
  }

  /**
   * add
   *
   * Add $cc_transactions_id to the sorted set
   */
  public function add( $timestamp , $cc_transactions_id )
  {
    if ( !$timestamp || !$cc_transactions_id )
      return NULL;

    $this->redis->zadd( self::SORTED_SET , $timestamp , $cc_transactions_id );
  }

  /**
   * setTransitionStatusInitiated
   *
   * STATUS = 'INITIATED' means that the transaction has been added to the DB, but not yet handled by a process
   */
  public function setTransitionStatusInitiated( $cc_transactions_id )
  {
    $this->redis->set( self::CC_TRANSACTIONS_STATUS_PREFIX . $cc_transactions_id , 'INITIATED' , self::CC_TRANSACTIONS_TTL );
  }

  /**
   * setTransitionStatusProcessing
   *
   * STATUS = 'PROCESSING' means that the transaction is being processed
   */
  public function setTransitionStatusProcessing( $cc_transactions_id )
  {
    $this->redis->set( self::CC_TRANSACTIONS_STATUS_PREFIX . $cc_transactions_id , 'PROCESSING' , self::CC_TRANSACTIONS_TTL );
  }

  /**
   * setTransitionResultApproved
   */
  public function setTransitionResultApproved( $cc_transactions_id )
  {
    $this->redis->set( self::CC_TRANSACTIONS_RESULT_PREFIX . $cc_transactions_id , 'APPROVED' , self::CC_TRANSACTIONS_TTL );
    $this->redis->set( self::CC_TRANSACTIONS_STATUS_PREFIX . $cc_transactions_id , 'COMPLETE' , self::CC_TRANSACTIONS_TTL );
  }

  /**
   * setTransitionResultTimeout
   */
  public function setTransitionResultTimeout( $cc_transactions_id )
  {
    $this->redis->set( self::CC_TRANSACTIONS_RESULT_PREFIX . $cc_transactions_id , 'TIMEOUT'  , self::CC_TRANSACTIONS_TTL );
    $this->redis->set( self::CC_TRANSACTIONS_STATUS_PREFIX . $cc_transactions_id , 'COMPLETE' , self::CC_TRANSACTIONS_TTL );
  }

  /**
   * setTransitionResultFailed
   */
  public function setTransitionResultFailed( $cc_transactions_id )
  {
    $this->redis->set( self::CC_TRANSACTIONS_RESULT_PREFIX . $cc_transactions_id , 'FAILED'   , self::CC_TRANSACTIONS_TTL );
    $this->redis->set( self::CC_TRANSACTIONS_STATUS_PREFIX . $cc_transactions_id , 'COMPLETE' , self::CC_TRANSACTIONS_TTL );
  }

  /**
   * setTransitionError
   */
  public function setTransitionError( $cc_transactions_id , $error )
  {
    $this->redis->set( self::CC_TRANSACTIONS_ERROR_PREFIX . $cc_transactions_id , $error , self::CC_TRANSACTIONS_TTL );
  }

  /**
   * getTransitionError
   */
  public function getTransitionError( $cc_transactions_id )
  {
    return $this->redis->get( self::CC_TRANSACTIONS_ERROR_PREFIX . $cc_transactions_id );
  }

  /**
   * getTransitionResult
   */
  public function getTransitionResult( $cc_transactions_id )
  {
    return $this->redis->get( self::CC_TRANSACTIONS_RESULT_PREFIX . $cc_transactions_id );
  }

  /**
   * getTransitionStatus
   */
  public function getTransitionStatus( $cc_transactions_id )
  {
    return $this->redis->get( self::CC_TRANSACTIONS_STATUS_PREFIX . $cc_transactions_id );
  }

  /**
   * reserve_customer_id
   */
  public function reserve_customer_id( $customer_id )
  {
    if ( ! $customer_id )
    {
      dlog('',"customer_id not given");

      return FALSE;
    }

    if ( $value = $this->redis->get( self::CC_CUSTOMER_ID_PREFIX . $customer_id ) )
    {
      dlog('',"redis key already assigned to $value");

      return FALSE;
    }

    $return = $this->redis->setnx( self::CC_CUSTOMER_ID_PREFIX . $customer_id , getmypid() , 360 ); // 6 minutes

    if ( ! $return )
    {
      dlog('',"setnx failed");
      
      return NULL;
    }

    usleep(25000);

    $value = $this->redis->get( self::CC_CUSTOMER_ID_PREFIX . $customer_id );

    if ( $value == getmypid() )
      return TRUE;

    dlog('',"[%s] customer already reserved by process $value",getmypid());

    return FALSE;
  }

  /**
   * unreserve_customer_id
   */
  public function unreserve_customer_id( $customer_id )
  {
    if ( ! $customer_id )
    {
      dlog('',"customer_id not given");
      
      return FALSE;
    }

    return $this->redis->del( self::CC_CUSTOMER_ID_PREFIX . $customer_id );
  }

}

