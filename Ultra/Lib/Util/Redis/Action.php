<?php

namespace Ultra\Lib\Util\Redis;

require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Redis Action class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Action
{
  private $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * addObject
   *
   * Adds a new Action Object to the Redis Hash 'ultra/action/'.$a_uuid
   *
   * @return boolean - FALSE in case of issues ; TRUE otherwise
   */
  public function addObject( $a_uuid , $t_uuid , $action_type , $action_name , $status , $action_seq=NULL )
  {
    $a_uuid = preg_replace( '/[\{\}]/' , '' , $a_uuid );
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    if ( is_null($action_seq) )
      $action_seq = $this->redis->zcard( 'ultra/sortedset/transitions/actions/'.$t_uuid ) + 1 ; # we assume those are provided incrementally

    $hmsetResult = $this->redis->hmset( 'ultra/action/'.$a_uuid ,
      array(
        'transition_uuid' => '{'.$t_uuid.'}',
        'action_seq'      => $action_seq,
        'action_type'     => $action_type,
        'action_name'     => $action_name,
        'status'          => $status
      )
    );

    // add to sorted set of action UUIDs ( score = $action_seq )
    $zaddResult = $this->redis->zadd( 'ultra/sortedset/transitions/actions/'.$t_uuid , $action_seq , $a_uuid );

    return ( ! ! ( $zaddResult && $hmsetResult ) );
  }

  /**
   * getListByTransition
   *
   * Returns the sorted set (by action_seq) identified by $t_uuid
   *
   * @return array
   */
  public function getListByTransition( $t_uuid )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    $listByTransition = $this->redis->zrevrangebyscore( 'ultra/sortedset/transitions/actions/'.$t_uuid , '+inf' , '-inf' , TRUE );

    return ( $listByTransition )
           ?
           array_reverse( $listByTransition )
           :
           array()
           ;
  }

  /**
   * deleteObjectsByTransition
   *
   * Delete all Action Objects from Redis connected to $t_uuid
   */
  public function deleteObjectsByTransition( $t_uuid )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    $listByTransition = $this->getListByTransition( $t_uuid );

    foreach( $listByTransition as $a_uuid )
    {
      $this->deleteObject( $a_uuid );
    }
  }

  /**
   * deleteObject
   *
   * Delete the Action Object from Redis
   */
  public function deleteObject( $a_uuid )
  {
    $a_uuid = preg_replace( '/[\{\}]/' , '' , $a_uuid );

    return $this->redis->del( 'ultra/action/'.$a_uuid );
  }

  /**
   * setObjectAttribute
   *
   * Sets or Updates an Action Object attribute to a value
   */
  public function setObjectAttribute( $a_uuid , $field , $value )
  {
    $a_uuid = preg_replace( '/[\{\}]/' , '' , $a_uuid );

    return $this->redis->hset( 'ultra/action/'.$a_uuid , $field , $value );
  }

  /**
   * getObjectAttribute
   *
   * Returns the value of an Action Object attribute
   */
  public function getObjectAttribute( $a_uuid , $field )
  {
    $a_uuid = preg_replace( '/[\{\}]/' , '' , $a_uuid );

    return $this->redis->hget( 'ultra/action/'.$a_uuid , $field );
  }

}

?>
