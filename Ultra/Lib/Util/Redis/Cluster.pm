package Ultra::Lib::Util::Redis::Cluster;


use strict;
use warnings;


use Redis;
use Ultra::Lib::Util::Config;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::Util::Redis::Cluster

Ultra Redis Cluster utilities

=head1 SYNOPSIS

  my $config = Ultra::Lib::Util::Config->new();

  my $object = Ultra::Lib::Util::Redis::Cluster->new( CONFIG => $config );

  my $success = $object->set($k,$v,$ttl);

  my $v = $object->get($k);

  my $success = $object->del($k);

  my $success = $object->sadd($set,$element);

=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ LOG_ENABLED } = 1;

  $this->SUPER::_initialize($data);

  $this->_initializeRedisCluster();
}

=head4 _initializeRedisCluster

  Object initialization

=cut
sub _initializeRedisCluster
{
  my ($this) = @_;

  ( $this->{ SERVER } , $this->{ PASSWORD } ) = $this->get_server();

  if ( ! $this->{ REDIS_OBJECT } )
  {
    my $redis_params =
    {
      server    => $this->{ SERVER },
      reconnect => 0,
      name      => 'Ultra::Lib::Util::Redis::Cluster-'.time,
    };

    $this->{ REDIS_OBJECT } = Redis->new( %$redis_params );

    my $cluster_hosts = $this->{ CONFIG }->find_credential('redis/cluster/hosts');

    if ( ! $cluster_hosts )
    {
      # Redis authentication
      $this->{ REDIS_OBJECT }->auth( $this->{ PASSWORD } );
    }
  }
}

=head4 get_server

  Returns server info

=cut
sub get_server
{
  my ($this) = @_;

  my $cluster_hosts = $this->{ CONFIG }->find_credential('redis/cluster/hosts');

  if ( $cluster_hosts )
  {
    # we have a cluster
    my @servers = split(/\s/,$cluster_hosts);

    # remove 'tcp://' string
    $servers[0] =~ /^tcp\:\/\/(.+)$/;

    return
    (
      $1.':6379',
      $this->{ CONFIG }->find_credential('redis/cluster/password')
    );
  }
  else
  {
    # we have a single server
    return
    (
      $this->{ CONFIG }->find_credential('redis/host'),
      $this->{ CONFIG }->find_credential('redis/password')
    );
  }
}

=head4 monitorInfo

  Returns info about the current redis server

=cut
sub monitorInfo
{
  my ($this) = @_;

  my $monitorInfo = $this->{ REDIS_OBJECT }->info;

  $monitorInfo->{ dbsize } = $this->{ REDIS_OBJECT }->dbsize;

  return $monitorInfo;
}

=head4 log_info

  Log info if the apprioriate configuration is set

=cut
sub log_info
{
  my ($this, $info) = @_;

  $this->log( $info ) if $this->{ VERBOSE };
}

=head4 move_connection

  When a command returns the server to which we should point,
  destroy the current connection and create a new one.

=cut
sub move_connection
{
  my ($this, $error) = @_;

  if ( $error =~ /MOVED (\d+) ([\d\.]+)\:(\d+)/ )
  {
    undef $this->{ REDIS_OBJECT };

    $this->log_info("moved to server ".$2.':'.$3);

    my $redis_params =
    {
      server    => $2.':'.$3,
      reconnect => 0,
      name      => 'Ultra::Lib::Util::Redis::Cluster-'.time,
    };

    $this->{ REDIS_OBJECT } = Redis->new( %$redis_params );

    #$this->{ REDIS_OBJECT }->auth( $this->{ PASSWORD } );

    return 1;
  }

  return 0;
}

=head4 get

  http://redis.io/commands/get

=cut
sub get
{
  my ($this, $key) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return the actual value or MOVED
    $result = $this->{ REDIS_OBJECT }->get( $key );
  };

  $this->log_info("get result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->get( $key );

      $this->log_info("get result = $result");
    }
  }

  return $result;
}

=head4 del

  http://redis.io/commands/del

=cut
sub del
{
  my ($this, $key) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return the actual value or MOVED
    $result = $this->{ REDIS_OBJECT }->del( $key );
  };

  $this->log_info("del result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->del( $key );

      $this->log_info("del result = $result");
    }
  }

  return $result;
}

=head4 expire

  http://redis.io/commands/expire

=cut
sub expire
{
  my ($this, $key, $ttl) = @_;

  my $result  = '';
  $ttl ||= 60*60*24*7; # 1 week default

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->expire( $key, $ttl );
  };

  $this->log_info("expire result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->set( $key, $ttl );

      $this->log_info("expire result = $result");
    }
  }

  return $result;
}

=head4 set

  http://redis.io/commands/set

=cut
sub set
{
  my ($this, $key, $value, $ttl) = @_;

  my $success = 0;
  my $result  = '';
  $ttl ||= 60*60*24*7; # 1 week default

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->set( $key, $value );
  };

  $this->log_info("set result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->set( $key, $value );

      $this->log_info("set result = $result");
    }
  }

  if ( $result eq 'OK' )
  {
    # this should return a true value
    $result = $this->{ REDIS_OBJECT }->expire( $key, $ttl );

    $this->log_info("expire result = $result");

    if ( $result )
    {
      $success = 1;
    }
  }

  return $success;
}

=head4 srem

  http://redis.io/commands/srem

=cut
sub srem
{
  my ($this, $set, $element) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->srem( $set, $element );
  };

  $this->log_info("srem result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->srem( $set, $element );

      $this->log_info("srem result = $result");
    }
  }

  return $result;
}

=head4 smembers

  http://redis.io/commands/smembers

=cut
sub smembers
{
  my ($this, $set) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->smembers($set);
  };

  $this->log_info("smembers result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->smembers($set);

      $this->log_info("smembers result = $result");
    }
  }

  return $result;
}

=head4 sadd

  http://redis.io/commands/sadd
  Returns
  1 if inserted
  0 if not inserted (duplicate)
  '' if error

=cut
sub sadd
{
  my ($this, $set, $element) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->sadd($set,$element);
  };

  $this->log_info("sadd result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->sadd($set,$element);

      $this->log_info("sadd result = $result");
    }
  }

  return $result;
}

=head4 rpush

  http://redis.io/commands/rpush

=cut
sub rpush
{
  my ($this, $set, $element) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->rpush( $set, $element );
  };

  $this->log_info("rpush result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->rpush($set,$element);

      $this->log_info("rpush result = $result");
    }
  }

  return $result;
}

=head4 lrange

  http://redis.io/commands/lrange

=cut
sub lrange
{
  my ($this, $list, $start, $end) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->lrange( $list, $start, $end );
  };

  $this->log_info("lrange result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->lrange( $list, $start, $end );

      $this->log_info("lrange result = $result");
    }
  }

  return $result;
}

=head4 lpop

  http://redis.io/commands/lpop

=cut
sub lpop
{ 
  my ($this, $list) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->lpop( $list );
  };

  $this->log_info("lpop result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->lpop( $list );

      $this->log_info("lpop result = $result");
    }
  }

  return $result;
}

=head4 exists

  http://redis.io/commands/exists
  Returns
  1 if the key exists
  0 if the key does not exist
  '' if error

=cut
sub exists
{ 
  my ($this, $key) = @_;

  my $result  = '';

  # the command may ``die``, providing the info about the server we should connect to
  eval
  {
    # this may return OK or MOVED
    $result = $this->{ REDIS_OBJECT }->exists( $key );
  };

  $this->log_info("exists result = $result");

  if ( ! $result && $@ )
  {
    if ( $this->move_connection( $@ ) )
    {
      $result = $this->{ REDIS_OBJECT }->exists( $key );

      $this->log_info("exists result = $result");
    }
  }

  return $result;
}

1;

__END__

