<?php

namespace Ultra\Lib\Util\Redis;

include_once('Ultra/Lib/Util/Redis.php');

/**
 * Redis Messaging class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Messaging
{
  private $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis(FALSE) // silent
                   :
                   $redis
                   ;
  }

  /**
   * addMessageId
   *
   * Adds a new message ID to the appropriate sorted set
   *
   * @returns NULL
   */
  public function addMessageId( $htt_messaging_queue_id , $priority , $timestamp )
  {
    $this->redis->zadd( "ultra/sortedset/messages/" . $priority , $timestamp , $htt_messaging_queue_id );
  }

  /**
   * freeMessageId
   *
   * Removed a message ID from the appropriate sorted set; unreserve message ID.
   *
   * @returns NULL
   */
  public function freeMessageId( $htt_messaging_queue_id , $priority )
  {
    if ( ! $this->redis->zrem( "ultra/sortedset/messages/" . $priority , $htt_messaging_queue_id )) // failed to remove: wrong queue
      for ($i = 1; $i < 4; $i++) // attempt to remove from other queues
        if ($i != $priority) // except the original priority
          if ($this->redis->zrem("ultra/sortedset/messages/$i", $htt_messaging_queue_id)) // success
            break;

    $this->redis->del( 'ultra/message/' . $htt_messaging_queue_id );
  }

  /**
   * getNextMessageId
   *
   * Retrieves and reserves a message id waiting in one of the 3 sorted sets
   *
   * @returns integer or NULL
   */
  public function getNextMessageId()
  {
    $next_htt_messaging_queue_id = NULL;

    for( $priority = 1 ; $priority < 4 ; $priority++ )
    {
      if ( ! $next_htt_messaging_queue_id )
        $next_htt_messaging_queue_id = $this->getNextMessageIdByPriority( $priority );
    }

    return $next_htt_messaging_queue_id;
  }

  /**
   * getNextMessageIdByPriority
   *
   * Retrieves and reserves a message id waiting in the sorted set for priority $priority
   *
   * @returns integer or NULL
   */
  public function getNextMessageIdByPriority( $priority )
  {
    // get the next HTT_MESSAGING_QUEUE_ID from the Redis sorted set
    # $next_htt_messaging_queue_id = $this->redis->zhead( 'ultra/sortedset/messages/' . $priority );

    $messaging_queue_id_list = $this->redis->zhead_list( 'ultra/sortedset/messages/' . $priority , 15 );

    #dlog('',"messaging_queue_id_list = %s",$messaging_queue_id_list);

    $next_htt_messaging_queue_id = NULL;

    if ( is_array($messaging_queue_id_list) )
      foreach( $messaging_queue_id_list as $id )
        if ( !$next_htt_messaging_queue_id && !$this->redis->get( 'ultra/message/' . $id )  )
        {
          $timestamp = $this->redis->zscore( 'ultra/sortedset/messages/' . $priority , $id );

          if ( $timestamp && ( $timestamp <= time() ) )
            $next_htt_messaging_queue_id = $id;
        }

    if ( $next_htt_messaging_queue_id )
    {
      // timestamp PST (server time)
      $timestamp = $this->redis->zscore( 'ultra/sortedset/messages/' . $priority , $next_htt_messaging_queue_id );

      dlog('', "zhead_list returned $next_htt_messaging_queue_id , with timestamp = $timestamp (".timestamp_to_date( $timestamp ).")");

      try
      {
        if ( ! $timestamp )
          throw new \Exception("empty timestamp associated with message id $next_htt_messaging_queue_id");

        if ( $timestamp > time() )
          throw new \Exception("timestamp $timestamp is in the future, message skipped for now");

        // reserve next_htt_messaging_queue_id in Redis
        $pid = $this->redis->get( 'ultra/message/' . $next_htt_messaging_queue_id );

        if ( $pid )
          throw new \Exception("message id $next_htt_messaging_queue_id already reserved by process $pid");

        // reserve for 10 minutes
        $this->redis->set( 'ultra/message/' . $next_htt_messaging_queue_id , gethostname() . getmypid() , 10 * 60 );

        usleep(250000);

        $pid = $this->redis->get( 'ultra/message/' . $next_htt_messaging_queue_id );

        if ( $pid != gethostname() . getmypid() )
          throw new \Exception("message id $next_htt_messaging_queue_id already reserved by process $pid");
      }
      catch(\Exception $e)
      {
        dlog('', $e->getMessage());
        $next_htt_messaging_queue_id = NULL;
      }
    }

    return $next_htt_messaging_queue_id;
  }

  /**
   * getMessageTimestamp
   * retrieve message timestamp
   * @param int message ID
   * @param int priority queue
   * @return Unix epoch or NULL
   */
   public function getMessageTimestamp($message, $priority)
   {
      $timestamp = $this->redis->zscore("ultra/sortedset/messages/$priority", $message);
      return empty($timestamp) ? NULL : $timestamp;
    }
}

?>
