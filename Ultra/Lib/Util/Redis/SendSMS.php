<?php

/**
 * AMDOCS-275
 *
 * Redis priority queue used by the runner to retry SendSMS
 */


namespace Ultra\Lib\Util\Redis\SendSMS;


require_once 'Ultra/Lib/Util/Redis.php';


const SENDSMS_SORTED_SET = 'ultra/sortedset/sendsms';


/**
 * enqueue
 *
 * Add some data to the sorted set
 *
 * @return integer
 */
function enqueue( $params , $timestamp , $redis=NULL )
{
  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis;

  $data = json_encode( $params );

  // add to sorted set with priority = $timestamp
  return $redis->zadd( SENDSMS_SORTED_SET , $timestamp , $data );
}

/**
 * dequeueNext
 *
 * returns the element with the highest priority, which is also removed from the sorted set
 *
 * @return array
 */
function dequeueNext( $redis=NULL )
{
  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis;

  list( $timestamp , $next_data ) = peekNext( $redis );

  // remove the element from the sorted set
  if ( ! is_null($timestamp) )
    $redis->zrem( SENDSMS_SORTED_SET , $next_data );

  return array( $timestamp , $next_data );
}

/**
 * peekNext
 *
 * returns the element with the highest priority
 *
 * @return array
 */
function peekNext( $redis=NULL )
{
  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis;

  $timestamp = NULL;

  $next_data = $redis->zhead( SENDSMS_SORTED_SET );

  // there is data
  if ( $next_data )
    $timestamp = $redis->zscore( SENDSMS_SORTED_SET , $next_data );

  return array( $timestamp , $next_data );
}

?>
