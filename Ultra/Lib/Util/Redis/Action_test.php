<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/Action.php';

$redis_action = new \Ultra\Lib\Util\Redis\Action();

echo $redis_action->addObject(
  '{AX-0000000000000000-1111111111111111}',
  '{TX-2222222222222222-3333333333333333}',
  1,
  'funcall',
  'test_function',
  'OPEN'
)."\n";

echo $redis_action->getObjectAttribute( '{AX-0000000000000000-1111111111111111}' , 'transition_uuid' )."\n";

echo $redis_action->setObjectAttribute( '{AX-0000000000000000-1111111111111111}' , 'transition_uuid' , '{TX-3333333333333333-2222222222222222}' )."\n";

echo $redis_action->getObjectAttribute( '{AX-0000000000000000-1111111111111111}' , 'transition_uuid' )."\n";

echo $redis_action->deleteObject( '{AX-0000000000000000-1111111111111111}' )."\n";

echo $redis_action->getObjectAttribute( '{AX-0000000000000000-1111111111111111}' , 'transition_uuid' )."\n";

?>
