<?php

namespace Ultra\Lib\Util\Redis;

require_once 'db/ultra_session.php';
require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Redis Celluphone Class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Dealer Portal
 */
class Celluphone
{
  const TTL                         = 1800; // 30 minutes before the Redis key expires
  const DEALER_DATA_KEY_PREFIX      = 'celluphone/dealer/';
  const MASTER_DATA_KEY_PREFIX      = 'celluphone/master/';

  const DEALER_SITE_TTL = 43200; // 60 * 60 * 12
  const DEALER_SITE_ID_DATA_KEY_PREFIX = 'celluphone/site/dealerid/';
  const DEALER_SITE_CD_DATA_KEY_PREFIX = 'celluphone/site/dealercd/';

  private $redis;
  private $dealer_ids_checked=array();

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * getDealerCDById
   *
   * Get tblDealerSite.Dealercd from blDealerSite.DealerSiteID
   *
   * @return string
   */
  public function getDealerCDById( $dealer_id )
  {
    if ( ! $dealer_id )
      return '';

    // is tblDealerSite.Dealercd cached in Redis?
    $dealerCD = $this->redis->get( $this::DEALER_DATA_KEY_PREFIX . $dealer_id );

    if ( ! $dealerCD && ! in_array( $dealer_id , $this->dealer_ids_checked ) )
    {
      // get tblDealerSite from DB
      $celluphone_dealer_site = get_celluphone_dealer_site( $dealer_id );

      // add to $dealer_ids_checked so that we don't repeat the same query
      $this->dealer_ids_checked[] = $dealer_id;

      if ( $celluphone_dealer_site && is_object( $celluphone_dealer_site ) && $celluphone_dealer_site->Dealercd )
      {
        $dealerCD = $celluphone_dealer_site->Dealercd;

        $this->redis->set( $this::DEALER_DATA_KEY_PREFIX . $dealer_id , $dealerCD , $this::TTL );
      }
    }

    return $dealerCD;
  }

  /**
   * getMasterName
   *
   * Get from
   *
   * @return string
   */
  public function getMasterName( $id )
  {
    if ( ! $id )
      return '';

    // is cached in Redis?
    $masterName = $this->redis->get( $this::MASTER_DATA_KEY_PREFIX . $id );

    if ( ! $masterName )
    {
      // get tblMaster from DB
      $celluphone_master = get_celluphone_master( $id );

      if ( $celluphone_master && is_object( $celluphone_master ) && $celluphone_master->BusinessName )
      {
        $masterName = $celluphone_master->BusinessName;

        $this->redis->set( $this::MASTER_DATA_KEY_PREFIX . $id , $masterName , $this::TTL );
      }
    }

    return $masterName;
  }

  public function getDealerSiteByDealerId($dealer_id)
  {
    return $this->redis->get($this::DEALER_SITE_ID_DATA_KEY_PREFIX . $dealer_id);
  }

  public function getDealerSiteByDealerCD($dealercd)
  {
    return $this->redis->get($this::DEALER_SITE_CD_DATA_KEY_PREFIX . $dealercd);
  }

  public function setDealerSiteByDealerId($dealer_id, $dealer_site)
  {
    return $this->redis->set($this::DEALER_SITE_ID_DATA_KEY_PREFIX . $dealer_id, $dealer_site, $this::DEALER_SITE_TTL);
  }

  public function setDealerSiteByDealerCD($dealercd, $dealer_site)
  {
    return $this->redis->set($this::DEALER_SITE_CD_DATA_KEY_PREFIX . $dealercd, $dealer_site, $this::DEALER_SITE_TTL);
  }
}

?>
