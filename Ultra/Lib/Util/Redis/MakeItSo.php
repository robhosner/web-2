<?php

namespace Ultra\Lib\Util\Redis;

require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Redis MakeItSo class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class MakeItSo
{
  // ZRANGEBYSCORE ultra/sortedset/makeitso -inf +inf
  const SORTED_SET               = 'ultra/sortedset/makeitso';
  const MAKEITSO_QUEUE_ID_PREFIX = 'ultra/makeitso/';

  private $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * popNext
   *
   * Retrieves and reserves a $makeitso_queue_id
   *
   * @returns integer or NULL
   */
  public function popNext( $makeitso_queue_id=NULL )
  {
    if ( ! $makeitso_queue_id )
      $makeitso_queue_id = $this->redis->zhead( self::SORTED_SET );

    if ( $makeitso_queue_id )
    {
      dlog('',"makeitso_queue_id = $makeitso_queue_id");

      $pid = getmypid() . gethostname();

      // timestamp PST (server time)
      $timestamp = $this->redis->zscore( self::SORTED_SET , $makeitso_queue_id );

      dlog('', "zhead returned $makeitso_queue_id , with timestamp = $timestamp (".timestamp_to_date( $timestamp ).")");

      try
      {
        if ( ! $timestamp )
        {
          // removes $makeitso_queue_id from the priority queue
          $this->redis->zrem( self::SORTED_SET , $makeitso_queue_id );

          throw new \Exception("empty timestamp associated with MAKEITSO_QUEUE_ID $makeitso_queue_id");
        }

        if ( $timestamp > time() )
          throw new \Exception("timestamp $timestamp is in the future, MAKEITSO_QUEUE_ID $makeitso_queue_id skipped for now");

        if ( $this->redis->get( self::MAKEITSO_QUEUE_ID_PREFIX . $makeitso_queue_id ) )
          throw new \Exception("MAKEITSO_QUEUE_ID $makeitso_queue_id already reserved by another process");

        // attempt to reserve $makeitso_queue
        $this->redis->set( self::MAKEITSO_QUEUE_ID_PREFIX . $makeitso_queue_id , $pid , 60 ); // 5 minutes

        usleep(1000);

        $pid = $this->redis->get( self::MAKEITSO_QUEUE_ID_PREFIX . $makeitso_queue_id );

        if ( $pid != getmypid() . gethostname() )
          throw new \Exception("MAKEITSO_QUEUE_ID $makeitso_queue_id already reserved by process $pid");

        // removes $makeitso_queue_id from the priority queue
        $this->redis->zrem( self::SORTED_SET , $makeitso_queue_id );
      }
      catch(\Exception $e)
      {
        dlog('', $e->getMessage());
        $makeitso_queue_id = NULL;
      }
    }

    return $makeitso_queue_id;
  }

  /**
   * free
   *
   * Unreserve $makeitso_queue_id
   */
  public function free( $makeitso_queue_id )
  {
    $this->redis->del( self::MAKEITSO_QUEUE_ID_PREFIX . $makeitso_queue_id );
  }

  /**
   * add
   *
   * Add $makeitso_queue_id to the sorted set
   */
  public function add( $timestamp , $makeitso_queue_id )
  {
    if ( !$timestamp || !$makeitso_queue_id )
      return NULL;

    $this->redis->zadd( self::SORTED_SET , $timestamp , $makeitso_queue_id );
  }
}

/**
 * restart_missing_queue_ids
 *
 * Query MAKEITSO_QUEUE and enqueue ``forgotten`` queue ids
 */
function restart_missing_queue_ids()
{
  \Ultra\Lib\DB\ultra_acc_connect();

  $sql = 'SELECT MAKEITSO_QUEUE_ID
          FROM   MAKEITSO_QUEUE
          WHERE  STATUS = "PENDING"
          AND    NEXT_ATTEMPT_DATE_TIME < DATEADD( ss , - 60*60 , GETUTCDATE() )
          AND    NEXT_ATTEMPT_DATE_TIME > DATEADD( d  , - 2     , GETUTCDATE() )
  ';

  $data = mssql_fetch_all_rows(logged_mssql_query($sql));

  dlog('',"data = %s",$data);

  if ( $data && is_array($data) && count($data) )
  {
    $redis_makeitso = new \Ultra\Lib\Util\Redis\MakeItSo();

    foreach( $data as $row )
      $redis_makeitso->add( time() , $row[0] );
  }
}

?>
