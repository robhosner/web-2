<?php

namespace Ultra\Lib\Util\Redis;

include_once 'Ultra/Lib/Util/Redis/Billing.php';

/**
 * Redis WebPos class
 *
 * Redis support data for ULTRA.WEBPOS_ACTIONS
 * $uuid is ULTRA.WEBPOS_ACTIONS.UUID
 * Implements a priority queue for UUIDs (strings) which are primary keys for the DB table ULTRA.WEBPOS_ACTIONS .
 * Whenever we will add a new row in ULTRA.WEBPOS_ACTIONS, we will add its UUID in the priority queue using addUUID .
 * The billing runner will subsequently extract the UUID using getNextUUID .
 * The queue can be viewed in redis-cli with
 *   zrangebyscore uultra/runner/webpos/queue -1 9999999999
 *
 * @author James Steinmetz <jsteinmetz@ultra.me>
 * @project WebPos Runner
 */
class WebPos extends Billing
{
  const REDIS_PRIORITY_QUEUE = 'ultra/runner/webpos/queue';
  const REDIS_UUID_PREFIX    = 'ultra/runner/webpos/transaction/';
  const REDIS_CUSTOMER_ROW   = 'ultra/runner/webpos/customer/';
  const REDIS_ICCID_LOCK     = 'ultra/runner/webpos/iccid_lock/';


  /**
   * cache customer record for later use by transition resolver
   * @param Object ULTRA.WEBPOS_ACTIONS row
   */
  public function setCustomer($row)
  {
    if ( ! empty($row) && is_object($row) && ! empty($row->CUSTOMER_ID))
      $this->redis->set(self::REDIS_CUSTOMER_ROW . $row->CUSTOMER_ID, json_encode($row), SECONDS_IN_MINUTE);
  }


  /**
   * get customer record from cache
   * @param Integer customer ID
   * @return Object ULTRA.WEBPOS_ACTIONS row or NULL
   */
  public function getCustomer($customerId)
  {
    if ( ! empty($customerId))
      if ($row = $this->redis->get(self::REDIS_CUSTOMER_ROW . $customerId))
        return json_decode($row);
    return NULL;
  }


  /**
   * lock ICCID in redis for $ttl seconds
   * @param  String  $iccid
   * @param  Integer $ttl seconds
   * @return Object ULTRA.WEBPOS_ACTIONS row or NULL
   */
  public function lockICCID($iccid, $ttl = 60)
  {
    $iccid = luhnenize($iccid);

    if ( ! $this->redis->get(self::REDIS_ICCID_LOCK . $iccid))
      return $this->redis->setnx(self::REDIS_ICCID_LOCK . $iccid, 1, $ttl);

    return FALSE;
  }

  /**
   * unlocks ICCID in redis
   * @param  String $iccid
   * @return NULL
   */
  public function unlockICCID($iccid)
  {
    $this->redis->del(self::REDIS_ICCID_LOCK . $iccid);
  }
}
