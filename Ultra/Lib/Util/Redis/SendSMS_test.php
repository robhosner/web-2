<?php


require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/SendSMS.php';


abstract class AbstractTestStrategy
{
  abstract function test( $argv );
}


class Test_SendSMS_enqueue extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'MSISDN'            => $argv[2],
      'preferredLanguage' => 'en',
      'qualityOfService'  => '801',
      'smsText'           => 'test_'.time()
    );

    return \Ultra\Lib\Util\Redis\SendSMS\enqueue( $params , time() );
  }
}


class Test_SendSMS_dequeueNext extends AbstractTestStrategy
{
  function test( $argv )
  {
    list( $timestamp , $next_data ) = \Ultra\Lib\Util\Redis\SendSMS\dequeueNext();

    return array(
      'timestamp' => $timestamp,
      'next_data' => $next_data
    );
  }
}


class Test_SendSMS_peekNext extends AbstractTestStrategy
{
  function test( $argv )
  {
    list( $timestamp , $next_data ) = \Ultra\Lib\Util\Redis\SendSMS\peekNext();

    return array(
      'timestamp' => $timestamp,
      'next_data' => $next_data
    );
  }
}


# perform test #


$testClass = 'Test_SendSMS_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$result = $testObject->test( $argv );

print_r($result);


/*
php Ultra/Lib/Util/Redis/SendSMS_test.php enqueue     $MSISDN
php Ultra/Lib/Util/Redis/SendSMS_test.php dequeueNext
php Ultra/Lib/Util/Redis/SendSMS_test.php peekNext
*/


?>
