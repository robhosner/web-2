<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/UltraSession.php';

$ultrasession = new Ultra\Lib\Util\Redis\UltraSession;

/*
$securityToken = $ultrasession->addObject(
  array(
    'donor_id'           => 'donor_id1',
    'user_id'            => 'user_id1',
    'role'               => 'role1',
    'masteragent'        => 'masteragent1',
    'distributor'        => 'distributor1',
    'dealer'             => 'dealer1'
  )
);

$x = $ultrasession->getObject( $securityToken );
*/

$customer_id = 31;

$customer_token = $ultrasession->addObject(
  array(
    'customer_id' => $customer_id,
    'account'     => 'ACCOUNT',
    'role'        => 'Customer',
    'masteragent' => 'masteragent',
    'distributor' => 'distributor',
    'dealer'      => 'dealer'
  )
);

echo "customer_token = $customer_token\n";

$x = $ultrasession->getCustomerObject( $customer_id );

print_r($x);

?>
