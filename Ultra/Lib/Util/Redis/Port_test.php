<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/Port.php';

$redis_port = new \Ultra\Lib\Util\Redis\Port();

$redis_port->setRequestId( 1 , 'a' );

echo "- ".$redis_port->getRequestId( 1 , 'Port-In Requested' );
echo "\n";
echo "- ".$redis_port->getRequestId( 1 , 'Active' );
echo "\n";
echo "- ".$redis_port->getRequestId( 2 , 'Port-In Requested' );
echo "\n";

?>
