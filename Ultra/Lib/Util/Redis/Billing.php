<?php

namespace Ultra\Lib\Util\Redis;

include_once('Ultra/Lib/Util/Redis.php');

/**
 * Redis Billing class
 *
 * Redis support data for HTT_BILLING_ACTIONS
 * $uuid is HTT_BILLING_ACTIONS.UUID
 * Implements a priority queue for UUIDs (strings) which are primary keys for the DB table HTT_BILLING_ACTIONS .
 * Whenever we will add a new row in HTT_BILLING_ACTIONS, we will add its UUID in the priority queue using addUUID .
 * The billing runner will subsequently extract the UUID using getNextUUID .
 * The queue can be viewed in redis-cli with
 *   zrangebyscore ultra/sortedset/billing -1 9999999999
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Billing Runner
 */
class Billing
{
  const REDIS_PRIORITY_QUEUE = 'ultra/sortedset/billing';
  const REDIS_UUID_PREFIX    = 'ultra/billing/uuid/';

  protected $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis(FALSE) // silent
                   :
                   $redis
                   ;
  }

  /**
   * addUUID
   *
   * Adds a new UUID to the sorted set
   *
   * @param string UUID
   * @param Unix epoch timestamp
   * @returns NULL
   */
  public function addUUID( $uuid , $timestamp )
  {
    $this->redis->zadd( static::REDIS_PRIORITY_QUEUE , $timestamp , $uuid );
  }

  /**
   * getUUIDTimestamp
   *
   * Retrieve UUID timestamp
   *
   * @param string UUID
   * @return Unix epoch or NULL
   */
  public function getUUIDTimestamp( $uuid )
  {
    $timestamp = $this->redis->zscore( static::REDIS_PRIORITY_QUEUE , $uuid );

    return empty($timestamp) ? NULL : $timestamp;
  }

  /**
   * getNextUUID
   *
   * Retrieves and reserves a UUID
   *
   * @returns integer or NULL
   */
  public function getNextUUID()
  {
    $nextUUID = NULL;

    try
    {
      $nextUUID = $this->redis->zhead( static::REDIS_PRIORITY_QUEUE );

      if ( $nextUUID )
      {
        $timestamp = $this->redis->zscore( static::REDIS_PRIORITY_QUEUE , $nextUUID );

        if ( ! $timestamp )
          throw new \Exception("empty timestamp associated with $nextUUID");

        if ( $timestamp > time() )
          throw new \Exception("timestamp $timestamp is in the future, message skipped for now");

        // reserve $nextUUID in Redis
        $pid = $this->redis->get( static::REDIS_UUID_PREFIX . $nextUUID );

        if ( $pid )
          throw new \Exception("$nextUUID already reserved by process $pid");

        // reserve for 8 minutes
        $return = $this->redis->setnx( static::REDIS_UUID_PREFIX . $nextUUID , gethostname() . getmypid() , 8 * 60 );

        if ( ! $return )
          throw new \Exception("$nextUUID already reserved");

        usleep(200000);

        // last sanity check
        $pid = $this->redis->get( static::REDIS_UUID_PREFIX . $nextUUID );

        if ( $pid != gethostname() . getmypid() )
          throw new \Exception("$nextUUID already reserved by process $pid");

        // remove from queue
        $this->redis->zrem( static::REDIS_PRIORITY_QUEUE , $nextUUID );
      }
      else
        $nextUUID = NULL;
    }
    catch(\Exception $e)
    {
      #dlog('', $e->getMessage());
      $nextUUID = NULL;
    }

    return $nextUUID;
  }

  /**
   * delUUID
   *
   * removes a UUID from the priority queue
   *
   * @returns integer or NULL
   */
  public function delUUID($uuid)
  {
    // remove from queue
    $this->redis->zrem( static::REDIS_PRIORITY_QUEUE , $uuid );
  }
}

