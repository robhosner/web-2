package Ultra::Lib::Util::Error::Setter;

use strict;
use warnings;

use Sys::Hostname;


sub new
{
  my ($class, %data) = @_;

  my $this = {};

  bless $this, $class;

  $this->_initialize(\%data);

  return $this;
}


sub _initialize
{
  my ($this, $data) = @_;

  $data ||= {};

  for my $key (keys (%$data))
  {
    $this->{$key} = $data->{$key};
  }
}


sub getErrors
{
  my ($this) = @_;

  my $errors = [];

  if(
    $this->{'errors'}
    && 
    ref($this->{'errors'}) eq "ARRAY"
    )
  {
    $errors = $this->{'errors'};
  }

  return $errors;
}


sub errors
{
  my ($this) = @_;

  return( $this->getErrors() );
}


sub hasErrors
{
  my ($this) = @_;

  my $errorStatus = 0;

  if(
    $this->{'errors'}
    &&
    ( ( ref $this->{'errors'} ) eq "ARRAY" )
    &&
    @ { $this->{'errors'} }
  )
  {
    $errorStatus  = 1;
  }

  return $errorStatus;
}


sub hasNoErrors
{
  my ($this) = @_;

  my $noErrorStatus = 1;

  if ( $this->hasErrors() )
  {
    $noErrorStatus  = 0;
  }

  return $noErrorStatus;
}


sub setErrors
{
  my ($this, $error) = @_;

  undef $this->{'errors'};

  $this->addErrors($error);
}


sub addErrors
{
  my ($this, $error) = @_;

  if(ref($error) eq "ARRAY")
  {
    for my $e( @$error )
    {
      push(@{$this->{'errors'}}, "[$$ ".hostname."] ".$e);
    }
  }
  else
  {
    push(@{$this->{'errors'}}, "[$$ ".hostname."] ".$error);
  }
}


sub addError
{
  my ($this, $error) = @_;

  $this->addErrors($error);
}


sub resetErrors
{
  my ($this) = @_;

  $this->{'errors'} = [];
}


sub printErrors
{
  my ($this) = @_;

  return join( " ; " , @ { $this->{'errors'} } );
}


sub cleanDebugString
{
  my ($this, $query) = @_;

  $query =~ s/[\t\r\n\s]+/ /mg;

  return $query;
}


1;


__END__

Module to store errors.

use base qw(Ultra::Lib::Util::Error::Setter);

...

$this->addError('an error');

...

if ( $this->hasErrors() )

...

