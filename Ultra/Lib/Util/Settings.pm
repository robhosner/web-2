package Ultra::Lib::Util::Settings;


use strict;
use warnings;


use Redis;
use Ultra::Lib::DB::UltraSettings;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::Util::Settings

Ultra Configuration Settings

=head1 SYNOPSIS

  my $db = Ultra::Lib::DB::MSSQL->new();

  my $settings = Ultra::Lib::Util::Settings->new( DB => $db , CONFIG => $db->{ CONFIG } );

  my $value = $settings->checkUltraSettings('some/setting/token');

=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ LOG_ENABLED } = 1;

  $this->SUPER::_initialize($data);
}


sub _initializeRedis
{
  my ($this) = @_;

  if ( ! $this->{ REDIS_OBJECT } )
  {
    my $redis_params =
    {
      server    => $this->{ CONFIG }->find_credential('redis/host'),
      reconnect => 0,
      name      => 'Ultra::Lib::Util::Settings-'.$$,
    };

    $this->{ REDIS_OBJECT } = Redis->new( %$redis_params );

    # Redis authentication
    $this->{ REDIS_OBJECT }->auth( $this->{ CONFIG }->find_credential('redis/password') );
  }
}


=head4 forceDBRefreshSettings

  We want to use the value currently in DB, overwriting the REDIS one.
  It's sufficient to delete the current REDIS value.

=cut
sub forceDBRefreshSettings
{
  my ($this,$token) = @_;

  $this->_initializeRedis();

  if ( $this->{ REDIS_OBJECT } )
  {
    return $this->{ REDIS_OBJECT }->del( $token );
  }
  else
  {
    $this->log("REDIS instance not available!");

    return 0;
  }
}


=head4 checkUltraSettings

  Check token on Redis, if not available check DB table ULTRA_SETTINGS and update Redis with the content of ULTRA_SETTINGS.
  If DB table ULTRA_SETTINGS not available, use default from ht.cf .

=cut
sub checkUltraSettings
{
  my ($this,$token) = @_;

  my $value = "";
  my $ttl   = "";

  $this->_initializeRedis();

  if ( $this->{ REDIS_OBJECT } )
  {
    $value = $this->checkUltraSettingsRedis($token);
  }
  else
  {
    $this->log("REDIS instance not available!");
  }

  if ( ! $value )
  {
    ($value,$ttl) = $this->checkUltraSettingsFromDB($token);

    if ( ( defined $value ) && ( $value ne '' ) )
    {
      $this->updateUltraSettingsRedis($token,$value,$ttl);
    }
    else
    {
      $value = $this->getDefaultUltraSettings($token);

      $this->log("checkUltraSettings could not find a value for $token , using default $value");
    }
  }

  return $value;
}


=head4 getDefaultUltraSettings

  Retrieve default settings

=cut
sub getDefaultUltraSettings
{
  my ($this,$token) = @_;

  # TODO: decide if this is correct
  return $this->{ CONFIG }->find_credential($token);
}


=head4 checkUltraSettingsRedis

  Check token on Redis.

=cut
sub checkUltraSettingsRedis
{
  my ($this,$token) = @_;

  return $this->{ REDIS_OBJECT }->get( $token );
}


=head4 updateUltraSettingsRedis

  Update token on Redis.

=cut
sub updateUltraSettingsRedis
{
  my ($this,$token,$value,$ttl) = @_;

  $this->{ REDIS_OBJECT }->set( $token , $value );
  $this->{ REDIS_OBJECT }->expire( $token , $ttl );
}


=head4 dbConnectionUltraSettings

  Instantiate Ultra::Lib::DB::UltraSettings object.

=cut
sub dbConnectionUltraSettings
{
  my ($this) = @_;

  if ( ( ! $this->{ DB_ULTRA_SETTINGS } ) && $this->{ DB } )
  {
    $this->{ DB_ULTRA_SETTINGS } = Ultra::Lib::DB::UltraSettings->new( DB => $this->{ DB } );
  }
}


=head4 checkUltraSettingsFromDB

  Check token on DB table ULTRA_SETTINGS.

=cut
sub checkUltraSettingsFromDB
{
  my ($this,$token) = @_;

  my $value = "";
  my $ttl   = "";

  $this->dbConnectionUltraSettings();

  if ( $this->{ DB_ULTRA_SETTINGS } )
  {
    my $row = $this->{ DB_ULTRA_SETTINGS }->getUltraSettings( $token );

    if ( $this->{ DB_ULTRA_SETTINGS }->hasErrors() )
    {
      $this->addErrors( $this->{ DB_ULTRA_SETTINGS }->getErrors() );
    }
    elsif ( $row && ref $row )
    {
      $value = $row->{ VALUE };
      $ttl   = $row->{ TTL_SECONDS };
    }
    else
    {
      $this->addError("No value found in DB for token $token");
    }
  }
  else
  {
    $this->addError('No DB connection available');
  }

  return ($value,$ttl);
}


1;


__END__


