<?php

require_once 'lib/util-common.php';

$str = "\u6587\u5b57'\u9a6c\u91cc\u5965' 6700\u8d4e\u56de\u4e0d\u53ef\u601d\u8bae\u4e00\u751f\u4e00\u6b21\u201c 2014\u5e74\u65e0\u9650\u6570\u636e\u63d0\u4f9b\u201d\u9002\u7528\u4e8e\u6700\u65b0\u53ca\u6d3b\u8dc3\u7528\u6237\u8d85\u8ba1\u5212\u4e8e29\u65e5\u548c39\u8ba1\u5212\u3002";

$str = convertUnicodeFromJavaEscape($str);

echo "$str\n";

exit;

$testXML = '<?xml version="1.0" encoding="UTF-8"?>
<note>
<to> Tove</to>
<from>Jani</from>
<heading>Reminder uFEED</heading>
<body>Ж您好</body>
</note>';

print_r(convertUnicodeToJavaEscape($testXML));

exit;

$l = 'en';

if ( is_english($l) ) { echo "en\n"; } else { echo "--\n"; }

$l = 'zh';

if ( is_english($l) ) { echo "en\n"; } else { echo "--\n"; }

$l = 'en-us';

if ( is_english($l) ) { echo "en\n"; } else { echo "--\n"; }

$l = 'es';

if ( is_english($l) ) { echo "en\n"; } else { echo "--\n"; }

$strs = array('I\'m feeling \u0417 fine.', '\u0439', '\u0416');

echo PHP_EOL . 'UNICODE CONVERSION INPUT:' . PHP_EOL;
print_r($strs);

foreach ($strs as &$str)
{
  $str = convertUnicodeFromJavaEscape($str);
}
print_r($strs);

foreach ($strs as &$str)
{
  $str = convertUnicodeToJavaEscape($str);
}
print_r($strs);
