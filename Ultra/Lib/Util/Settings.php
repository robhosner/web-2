<?php

namespace Ultra\Lib\Util;

include_once('db/ultra_settings.php');
include_once('Ultra/UltraConfig.php');
require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Settings class
 *
 * Configurable switches
 * See AMDOCS-40
 * Settings rely on
 *  1) Redis cached values
 *  2) DB values ( ULTRA_SETTINGS table )
 *  3) Environment tokens (cfengine level)
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project ULTRA API
 */
class Settings
{
  # Redis object
  protected $redis;

  public function __construct()
  {
    $this->redis = new \Ultra\Lib\Util\Redis();
  }

  /**
   * forceDBRefreshSettings
   *
   * Removes token from Redis so that the next access will be done at DB level
   */
  public function forceDBRefreshSettings($token)
  {
    return $this->redis->del( $token );
  }

  /**
   * checkUltraSettings
   *
   * @return string
   */
  public function checkUltraSettings($token)
  {
    $value = $this->checkUltraSettingsRedis($token);

    if ( ! $value )
    {
      // retrieve from MSSQL
      list($value,$ttl) = $this->checkUltraSettingsFromDB($token);

      if ( isset($value) && ( $value != '' ) )
      {
        $this->updateUltraSettingsRedis($token,$value,$ttl);
      }
      else
      {
        // default: retrieve from environment configuration
        $value = $this->getDefaultUltraSettings($token);

        dlog('',"checkUltraSettings could not find a value for $token , using default $value");
      }
    }

    return $value;
  }

  /**
   * getDefaultUltraSettings
   *
   * Gets environment token (cfengine level)
   *
   * @return string
   */
  public function getDefaultUltraSettings($token)
  {
    return find_credential( $token );
  }

  /**
   * checkUltraSettingsRedis
   *
   * Gets value from Redis
   *
   * @return string
   */
  public function checkUltraSettingsRedis($token)
  {
    return $this->redis->get( $token , TRUE );
  }

  /**
   * updateUltraSettingsRedis
   *
   * Updates value in Redis
   *
   * @return NULL
   */
  public function updateUltraSettingsRedis($token,$value,$ttl)
  {
    $this->redis->set( $token , $value );
    $this->redis->expire( $token , $ttl );
  }

  /**
   * checkUltraSettingsFromDB
   *
   * Gets value from DB table ULTRA_SETTINGS
   *
   * @return string
   */
  public function checkUltraSettingsFromDB($token)
  {
    $value = '';
    $ttl   = '';

    teldata_change_db();

    $ultraSettings = getUltraSettings($token);

    if ( $ultraSettings )
    {
      $value = $ultraSettings->VALUE;
      $ttl   = $ultraSettings->TTL_SECONDS;
    }

    return array($value,$ttl);
  }
}

