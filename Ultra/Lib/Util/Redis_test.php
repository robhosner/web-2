<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis.php';

#$ultra_redis = new \Ultra\Lib\Util\Redis(FALSE);
$ultra_redis = new \Ultra\Lib\Util\Redis();

echo $ultra_redis->llen('ACC_MW/INBOUND/SYNCH')."\n";
echo $ultra_redis->llen('ULTRA_MW/INBOUND/SYNCH')."\n";
echo $ultra_redis->llen('ACCMW/INBOUND/NOTIFICATION')."\n";

exit;

$lists = array( 'list_test','rr','yy','ii','qwerqqwt','sklfjghasjkg','cvxcvbn' );
foreach( $lists as $list )
{
  $ultra_redis->del( $list );
  echo "\n\n";
}

#exit;

echo $ultra_redis->llen('list_test')."\n";
$ultra_redis->rpush('list_test','4567');
echo $ultra_redis->llen('list_test')."\n";
$ultra_redis->rpush('list_test','1234');
echo $ultra_redis->llen('list_test')."\n";
$ultra_redis->rpush('list_test','7890');
echo $ultra_redis->llen('list_test')."\n";
$ultra_redis->rpush('list_test','4567');
echo $ultra_redis->llen('list_test')."\n";
$ultra_redis->rpush('list_test','1');
echo $ultra_redis->llen('list_test')."\n";
$ultra_redis->rpush('list_test','1');
echo $ultra_redis->llen('list_test')."\n";

$ultra_redis->lpush('list_test','8');

echo $ultra_redis->llen('list_test')."\n";

echo $ultra_redis->lpop('list_test')."\n";

# get the first 2 elements
$x = $ultra_redis->lrange('list_test',0,1);
print_r($x);

# get the 2nd element
$x = $ultra_redis->lrange('list_test',1,1);
print_r($x);

# get the 3rd element
$x = $ultra_redis->lrange('list_test',2,2);
print_r($x);

$ultra_redis->lrem('list_test','4567',-1);

# get all elements
$x = $ultra_redis->lrange('list_test',-100,100);
print_r($x);

$ultra_redis->lrem('list_test','4567',1);

# get the first 2 elements
$x = $ultra_redis->lrange('list_test',0,1);
print_r($x);

# get all elements
$x = $ultra_redis->lrange('list_test',-100,100);
print_r($x);

#exit;

# sets

echo $ultra_redis->sadd('test_set_1','a');
echo $ultra_redis->sadd('test_set_1','a1');
echo $ultra_redis->sadd('test_set_1','a2');
echo $ultra_redis->sadd('test_set_1','a');

$x = $ultra_redis->smembers('test_set_1');

print_r($x);

echo $ultra_redis->expire('test_set_1',1);

sleep(2);

$x = $ultra_redis->smembers('test_set_1');

print_r($x);

echo $ultra_redis->del('test_set_1');

$x = $ultra_redis->smembers('test_set_1');

print_r($x);

$ultra_redis = new \Ultra\Lib\Util\Redis();
$ultra_redis = new \Ultra\Lib\Util\Redis();

$key = 'incrorset_test_'.time();

echo $ultra_redis->get($key) . "\n";
$ultra_redis->incr_or_set($key);
echo $ultra_redis->get($key) . "\n";
$ultra_redis->incr_or_set($key);
echo $ultra_redis->get($key) . "\n";

# hashes

echo $ultra_redis->hmset('test_raf_1',array('a'=>time(),'b'=>'test','q'=>'z','f'=>'t'));
$x = $ultra_redis->hmget('test_raf_1',array('a','b','c','f'));
print_r($x);

echo $ultra_redis->del('test_raf_1');
$x = $ultra_redis->hmget('test_raf_1',array('a','b','c'));
print_r($x);


$data = array('123 abc');
print_r($data);
$encoded = json_encode($data);
$ultra_redis->set('raf_test_123',$encoded,1000);
$extracted = $ultra_redis->get('raf_test_123')."\n";
$decoded = json_decode($extracted);
print_r($decoded);
 

$data = array('被暂停。请充值');
print_r($data);
$encoded = json_encode($data);
$ultra_redis->set('raf_test_123',$encoded,1000);
$extracted = $ultra_redis->get('raf_test_123')."\n";
$decoded = json_decode($extracted);
print_r($decoded);


echo $ultra_redis->get($key) . "\n";
$ultra_redis->incr_or_set($key);
echo $ultra_redis->get($key) . "\n";
$ultra_redis->incr_or_set($key);
echo $ultra_redis->get($key) . "\n";

$key = 'test_raf';

$ultra_redis->setnx( $key , time() , 1000 );

echo $ultra_redis->get( $key )."\n";

$queue = "ultra/sortedset/messages/1";

$x = $ultra_redis->zhead_list( $queue , 5 );

print_r($x);

echo 'get : '.$ultra_redis->get('setnx_test')."\n";
$ultra_redis->setnx('setnx_test',200,200);
echo 'get : '.$ultra_redis->get('setnx_test')."\n";
$ultra_redis->setnx('setnx_test',300,300);
echo 'get : '.$ultra_redis->get('setnx_test')."\n";

echo \Ultra\Lib\Util\redis_read('a')."\n";
echo \Ultra\Lib\Util\redis_write('a',1,60)."\n";
echo \Ultra\Lib\Util\redis_read('a')."\n";

$ultra_redis->set('a',1234);
echo $ultra_redis->get('a')."\n";

$ultra_redis->hmset('hm_test' , array( 'x' => 90 , 'y' => 45 ) );
$test = $ultra_redis->hgetall('hm_test');
print_r($test);

$ultra_redis->zadd( 'z_test' , 1 , "one" );
$ultra_redis->zadd( 'z_test' , 2 , "two" );
$ultra_redis->zadd( 'z_test' , 2 , "ttwwoo" );
$ultra_redis->zadd( 'z_test' , 4 , "FFFF");
$ultra_redis->zadd( 'z_test' , 4 , "ffff");
$ultra_redis->zadd( 'z_test' , 3 , "tre" );
$ultra_redis->zadd( 'z_test' , 3 , "ttrree" );

echo $ultra_redis->zcard('z_test')."\n";

$x = $ultra_redis->zrevrangebyscore('z_test','+inf','-inf');
print_r($x);
if ( is_array($x))
  print_r(array_reverse($x));

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$rr = $ultra_redis->zrangebyscore('z_test','-1','999999');
print_r($rr);

$ultra_redis->zrem('z_test',$rr[0]);

$ultra_redis->zadd( 'z_test2' , 1 , "one" );
$ultra_redis->zadd( 'z_test2' , 2 , "two" );
$ultra_redis->zadd( 'z_test2' , 2 , "ttwwoo" );
$ultra_redis->zadd( 'z_test2' , 4 , "FFFF");
$ultra_redis->zadd( 'z_test2' , 4 , "ffff");
$ultra_redis->zadd( 'z_test2' , 3 , "tre" );
$ultra_redis->zadd( 'z_test2' , 3 , "ttrree" );

$rr = $ultra_redis->zrangebyscore('z_test2','-1','999999');
print_r($rr);

$x = $ultra_redis->zhead('z_test2');
echo "$x\n";
$x = $ultra_redis->zpop('z_test2');
echo "$x\n";
$ultra_redis->zrem('z_test2',$x);

$rr = $ultra_redis->zrangebyscore('z_test2','-1','999999');
print_r($rr);

$x = $ultra_redis->zhead('z_test2');
echo "$x\n";
$x = $ultra_redis->zpop('z_test2');
echo "$x\n";
$ultra_redis->zrem('z_test2',$x);

$rr = $ultra_redis->zrangebyscore('z_test2','-1','999999');
print_r($rr);

$x = $ultra_redis->zhead('z_test2');
echo "$x\n";
$x = $ultra_redis->zpop('z_test2');
echo "$x\n";
$ultra_redis->zrem('z_test2',$x);

$rr = $ultra_redis->zrangebyscore('z_test2','-1','999999');
print_r($rr);

