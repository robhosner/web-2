<?php

require_once 'Ultra/Lib/Util/Validator.php';
require_once 'Ultra/tests/API/DefaultUltraAPITest.php';
require_once 'Ultra/tests/API/TestConstants.php';
require_once 'lib/util-common.php';

use \Ultra\Lib\Util as UltraUtil;
use \Ultra\Lib\Util\Validator;


/**
 * PHPUnit tests for Ultra/Lib/Util/Validator.php
 */
class ValidatorTest extends PHPUnit_Framework_TestCase
{
  /**
   * test_validate_main
   */
  public function test_validate_main()
  {
    $result = Validator::validate(null, null, null, null);
    $result = array_filter($result);
    $this->assertEquals('TODO', $result[1][0]);

    list( $errors , $error_codes ) = Validator::validate('test', 'string', array('FakeValidation' => 200), 202);

    $this->assertEquals('VV0099', $error_codes[0]);
    $this->assertEquals('VV0001', $error_codes[1]);

    list( $errors , $error_codes ) = Validator::validate('test', 'string', array('min_strlen' => 3), '1234');

    $this->assertEmpty( $errors );
    $this->assertEmpty( $error_codes );

    list( $errors , $error_codes ) = Validator::validate('test', 'string', array('min_strlen' => 3), '1');

    $this->assertNotEmpty( $errors );
    $this->assertNotEmpty( $error_codes );
  }

  /**
   * test_validateByConfiguration
   */
  public function test_validateByConfiguration()
  {
    $result = Validator::validateByConfiguration(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = Validator::validateByConfiguration('', array('FakeValidation' => 200), 202);
    $this->assertEquals('VV0099', $result[1][0]);
  }

  /**
   * test_validateByType
   */
  public function test_validateByType()
  {
    $result = Validator::validateByType(null, null, null);
    $this->assertEquals('TODO', $result[1]);

    $result = Validator::validateByType('', 'fake', 'test');
    $this->assertEquals('TODO', $result[1]);
  }

  /**
   * test_validateTypeStringArray
   */
  public function test_validateTypeStringArray()
  {
    $result = UltraUtil\validateTypeStringArray(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validateTypeStringArray('', new stdClass());
    $this->assertEquals('VV0035', $result[1]);

    $result = UltraUtil\validateTypeStringArray('', true);
    $this->assertEquals('VV0035', $result[1]);

    $result = UltraUtil\validateTypeStringArray('', 01234567890);
    $this->assertEquals('VV0035', $result[1]);

    $result = UltraUtil\validateTypeStringArray('', 10.50);
    $this->assertEquals('VV0035', $result[1]);

    $result = UltraUtil\validateTypeStringArray('', '10ASDAS');
    $this->assertEquals('VV0035', $result[1]);

    $result = UltraUtil\validateTypeStringArray('', array());
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validateTypeString
   */
  public function test_validateTypeString()
  {
    $result = UltraUtil\validateTypeString(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validateTypeString('', new stdClass());
    $this->assertEquals('VV0001', $result[1]);

    $result = UltraUtil\validateTypeString('', true);
    $this->assertEquals('VV0001', $result[1]);

    $result = UltraUtil\validateTypeString('', array('test'));
    $this->assertEquals('VV0001', $result[1]);

    $result = UltraUtil\validateTypeString('', 01234567890);
    $this->assertEquals('VV0001', $result[1]);

    $result = UltraUtil\validateTypeString('', 10.50);
    $this->assertEquals('VV0001', $result[1]);

    $result = UltraUtil\validateTypeString('', '10ASDAS');
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validateTypeInteger
   */
  public function test_validateTypeInteger()
  {
    $result = UltraUtil\validateTypeInteger(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validateTypeInteger('', '10ASDAS');
    $this->assertEquals('VV0002', $result[1]);

    $result = UltraUtil\validateTypeInteger('', 10.50);
    $this->assertEquals('VV0002', $result[1]);

    $result = UltraUtil\validateTypeInteger('', 01234567890);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validateTypeReal
   */
  public function test_validateTypeReal()
  {
    $result = UltraUtil\validateTypeReal(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validateTypeReal('', '0.10ASDAS');
    $this->assertEquals('VV0003', $result[1]);

    $result = UltraUtil\validateTypeReal('', 0.0111);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validateTypeEpoch
   */
  public function test_validateTypeEpoch()
  {
    $result = UltraUtil\validateTypeEpoch(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validateTypeEpoch('', '10ASDAS');
    $this->assertEquals('VV0004', $result[1]);

    $result = UltraUtil\validateTypeEpoch('', 10.50);
    $this->assertEquals('VV0004', $result[1]);

    $result = UltraUtil\validateTypeEpoch('', 01234567890);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validateTypeBoolean
   */
  public function test_validateTypeBoolean()
  {
    $result = UltraUtil\validateTypeBoolean(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validateTypeBoolean('', '10');
    $this->assertEquals('VV0005', $result[1]);

    $result = UltraUtil\validateTypeBoolean('', 10);
    $this->assertEquals('VV0005', $result[1]);

    $result = UltraUtil\validateTypeBoolean('', 0);
    $this->assertEquals('', $result[1]);

    $result = UltraUtil\validateTypeBoolean('', 1);
    $this->assertEquals('', $result[1]);
  }

  /**
    * test_validatorActivePlan
    */
  public function test_validatorActivePlan()
  {
    $result = UltraUtil\validatorActivePlan(null, null);
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorActivePlan('', 'L29');
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorActivePlan('', 'S29');
    $this->assertEquals('', $result[1]);

    $result = UltraUtil\validatorActivePlan('', 'NINETEEN');
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorActivePlan('', 'NINETEEN', 2);
    $this->assertEquals('', $result[1]);

    $result = UltraUtil\validatorActivePlan('', 'TWENTY_THREE', 2);
    $this->assertEquals('VV0006', $result[1]);
  }

  public function test_validatorPlanExists()
  {
    $result = UltraUtil\validatorPlanExists(null, null);
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorPlanExists('', 'L23');
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorPlanExists('', 'L19');
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMatches
   */
  public function test_validatorMatches()
  {
    $result = UltraUtil\validatorMatches(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMatches('', 'TEST', array('NOT'));
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorMatches('', 'NOT', array('NOT'));
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMatchesLanguage
   */
  public function test_validatorMatchesLanguage()
  {
    $result = UltraUtil\validatorMatchesLanguage(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMatchesLanguage('TEST', 'TEST');
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorMatchesLanguage('EN', 'EN');
    $this->assertEquals('', $result[1]);

    $result = UltraUtil\validatorMatchesLanguage('ES', 'ES');
    $this->assertEquals('', $result[1]);

    $result = UltraUtil\validatorMatchesLanguage('ZH', 'ZH');
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMatchesPlanState
   */
  public function test_validatorMatchesPlanState()
  {
    $result = UltraUtil\validatorMatchesPlanState(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMatchesPlanState('TEST', 'TEST');
    $this->assertEquals('VV0006', $result[1]);

    $result = UltraUtil\validatorMatchesPlanState('TEST', 'Cancelled');
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMinstrlen
   */
  public function test_validatorMinstrlen()
  {
    $result = UltraUtil\validatorMinstrlen(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMinstrlen('', 'Hi', 5);
    $this->assertEquals('VV0007', $result[1]);

    $result = UltraUtil\validatorMinstrlen('', 'Hello there', 5);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMaxstrlen
   */
  public function test_validatorMaxstrlen()
  {
    $result = UltraUtil\validatorMaxstrlen(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMaxstrlen('', 'Hello there', 5);
    $this->assertEquals('VV0008', $result[1]);

    $result = UltraUtil\validatorMaxstrlen('', 'Hi', 5);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMinvalue
   */
  public function test_validatorMinvalue()
  {
    $result = UltraUtil\validatorMinvalue(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMinvalue('', 10, 2000);
    $this->assertEquals('VV0009', $result[1]);

    $result = UltraUtil\validatorMinvalue('', 2020, 2000);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMaxvalue
   */
  public function test_validatorMaxvalue()
  {
    $result = UltraUtil\validatorMaxvalue(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMaxvalue('', 2020, 2000);
    $this->assertEquals('VV0010', $result[1]);

    $result = UltraUtil\validatorMaxvalue('', 10, 2000);
    $this->assertEquals('', $result[1]);
  }

  /**
   * test_validatorMsisdn
   */
  public function test_validatorMsisdn()
  {
    $result = UltraUtil\validatorMsisdn(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorMsisdn('', 1234, '');
    $this->assertEquals('MV0001', $result[1]);

    $result = UltraUtil\validatorMsisdn('', 1234567890);
    $result = array_filter($result);
    $this->assertTrue(empty($result));
  }

  /**
   * test_validatorIccidFormat
   */
  public function test_validatorIccidFormat()
  {
    $result = UltraUtil\validatorIccidFormat(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorIccidFormat('', 1234, '');
    $this->assertEquals('MP0004', $result[1]);

    $result = UltraUtil\validatorIccidFormat('', 1234567890123456789);
    $result = array_filter($result);
    $this->assertTrue(empty($result));
  }

  /**
   * test_validatorIccid
   */
  public function test_validatorIccid()
  {
    // connect to TSIP DB
    teldata_change_db();

    // invalid ICCID
    list( $error , $error_code ) = UltraUtil\validatorIccid('test_iccid','1','brand_id_1');
    $this->assertEquals('IC0003',$error_code);

    // load a brand 1 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 1 and CUSTOMER_ID is not null');
    list( $error , $error_code ) = UltraUtil\validatorIccid('test_iccid',$res->ICCID_FULL,'brand_id_1');
    $this->assertEmpty( $error );
    $this->assertEmpty( $error_code );

    // load a brand 2 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 2 and CUSTOMER_ID is not null');
    list( $error , $error_code ) = UltraUtil\validatorIccid('test_iccid',$res->ICCID_FULL,'brand_id_2');
    $this->assertEmpty( $error );
    $this->assertEmpty( $error_code );

    // invalid brand
    list( $error , $error_code ) = UltraUtil\validatorIccid('test_iccid',$res->ICCID_FULL,'brand_id_3');
    $this->assertEquals('IC0003',$error_code);
  }

  /**
   * test_brand_ids
   */
  public function test_brand_ids()
  {
     $this->assertFalse( UltraUtil\validateMintBrandId(1) );
     $this->assertFalse( UltraUtil\validateMintBrandId(2) );
     $this->assertTrue(  UltraUtil\validateMintBrandId(3) );

     $this->assertTrue(  UltraUtil\validateUltraBrandId(1) );
     $this->assertFalse( UltraUtil\validateUltraBrandId(2) );
     $this->assertFalse( UltraUtil\validateUltraBrandId(3) );

     $this->assertFalse( UltraUtil\validateUnivisionBrandId(1) );
     $this->assertTrue(  UltraUtil\validateUnivisionBrandId(2) );
     $this->assertFalse( UltraUtil\validateUnivisionBrandId(3) );
  }

  /**
   * test_msisdn_and_brand
   */
  public function test_msisdn_and_brand()
  {
    // connect to TSIP DB
    teldata_change_db();

    // invalid MSISDN
    $this->assertFalse( UltraUtil\validateMSISDNBrandId( '1' , 1 ) );

    // load a brand 1 MSISDN
    $res = find_first('select top 1 current_mobile_number from htt_customers_overlay_ultra where BRAND_ID = 1 and current_mobile_number is not null order by CUSTOMER_ID desc');
    $this->assertTrue( UltraUtil\validateMSISDNBrandId( $res->current_mobile_number , 1 ) );

    // load a brand 2 MSISDN
    $res = find_first('select top 1 current_mobile_number from htt_customers_overlay_ultra where BRAND_ID = 2 and current_mobile_number is not null and plan_state = "Active" order by CUSTOMER_ID desc');
    $this->assertTrue( UltraUtil\validateMSISDNBrandId( $res->current_mobile_number , 2 ) );

    // load a brand 3 MSISDN
    $res = find_first('select top 1 current_mobile_number from htt_customers_overlay_ultra where BRAND_ID = 3 and current_mobile_number is not null and current_mobile_number not like "1%" and plan_state = "Active" order by CUSTOMER_ID desc');


    // invalid brand
    $this->assertFalse( UltraUtil\validateMSISDNBrandId( $res->current_mobile_number , 1 ) );


    // invalid MSISDN
    $this->assertFalse( UltraUtil\validateMSISDNBrand( '1' , 'ULTRA' ) );

    // load a brand 1 MSISDN
    $res = find_first('select top 1 current_mobile_number from htt_customers_overlay_ultra where BRAND_ID = 1 and current_mobile_number is not null order by CUSTOMER_ID desc');
    $this->assertTrue( UltraUtil\validateMSISDNBrand( $res->current_mobile_number , 'ULTRA' ) );

    // load a brand 2 MSISDN
    $res = find_first('select top 1 current_mobile_number from htt_customers_overlay_ultra where BRAND_ID = 2 and current_mobile_number is not null and plan_state = "Active" order by CUSTOMER_ID desc');
    $this->assertTrue( UltraUtil\validateMSISDNBrand( $res->current_mobile_number , 'UNIVISION' ) );
  }

  /**
   * test_iccid_and_brand
   */
  public function test_iccid_and_brand()
  {
    // connect to TSIP DB
    teldata_change_db();

    // invalid ICCID
    $this->assertFalse( UltraUtil\validateICCIDBrandId( '1' , 1 ) );

    // load a brand 1 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 1 and CUSTOMER_ID is not null');
    $this->assertTrue( UltraUtil\validateICCIDBrandId( $res->ICCID_FULL , 1 ) );

    // load a brand 2 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 2 and CUSTOMER_ID is not null');
    $this->assertTrue( UltraUtil\validateICCIDBrandId( $res->ICCID_FULL , 2 ) );

    // invalid brand
    $this->assertFalse( UltraUtil\validateICCIDBrandId( $res->ICCID_FULL , 3 ) );


    // invalid ICCID
    $this->assertFalse( UltraUtil\validateICCIDBrand( '1' , 'ULTRA' ) );

    // load a brand 1 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 1 and CUSTOMER_ID is not null');
    $this->assertTrue( UltraUtil\validateICCIDBrand( $res->ICCID_FULL , 'ULTRA' ) );

    // load a brand 2 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 2 and CUSTOMER_ID is not null');
    $this->assertTrue( UltraUtil\validateICCIDBrand( $res->ICCID_FULL , 'UNIVISION' ) );

    // invalid brand
    $this->assertFalse( UltraUtil\validateICCIDBrand( $res->ICCID_FULL , 'MINT' ) );


    // invalid ICCID
    $this->assertFalse( UltraUtil\validateICCIDBrandName( '1' , 'Mint' ) );

    // load a brand 1 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 1 and CUSTOMER_ID is not null');
    $this->assertTrue( UltraUtil\validateICCIDBrandName( $res->ICCID_FULL , 'Ultra_Mobile' ) );

    // load a brand 2 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 2 and CUSTOMER_ID is not null');
    $this->assertTrue( UltraUtil\validateICCIDBrandName( $res->ICCID_FULL , 'Univision' ) );

    // invalid brand
    $this->assertFalse( UltraUtil\validateICCIDBrandName( $res->ICCID_FULL , 'Mint' ) );
  }

  /**
   * test_validatorRegexp
   */
/*
  public function test_validatorRegexp()
  {
    $result = UltraUtil\validatorRegexp(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorRegexp('', '!!!!!!', '^[A-Za-z0-9]+$');
    $this->assertEquals('VV', $result[1]);
  }
*/
  /**
   * test_validatorDateformat
   */
  public function test_validatorDateformat()
  {
    $result = UltraUtil\validatorDateformat(null, null, null);
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'ERR_API_INVALID_ARGUMENTS: Unknown format'));

    $result = UltraUtil\validatorDateformat('', '', 'mmyy');
    $this->assertEquals('VV0070', $result[1]);

    $result = UltraUtil\validatorDateformat('', '9999', 'mmyy');
    $this->assertEquals('VV0033', $result[1]);

    $result = UltraUtil\validatorDateformat('', '', 'dd-mm-yyyy');
    $this->assertEquals('VV0034', $result[1]);

    $result = UltraUtil\validatorDateformat('', '99-99-9999', 'dd-mm-yyyy');
    $this->assertEquals('VV0033', $result[1]);

    $result = UltraUtil\validatorDateformat('', '', 'mm-dd-yyyy');
    $this->assertEquals('VV0035', $result[1]);

    $result = UltraUtil\validatorDateformat('', '99-99-9999', 'mm-dd-yyyy');
    $this->assertEquals('VV0033', $result[1]);
  }

  /**
   * test_validatorStringformat
   */
  public function test_validatorStringformat()
  {
    $result = UltraUtil\validatorStringformat(null, null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorStringformat('', 'FAKE_EMAIL', 'email');
    $this->assertEquals('EM0001', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'numbers', 'numeric');
    $this->assertEquals('ST0001', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'alphanumeric');
    $this->assertEquals('ST0002', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'alphanumeric_and_spaces');
    $this->assertEquals('ST0003', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'letters_spaces_apostrophe');
    $this->assertEquals('ST0004', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'alphanumeric_spaces_punctuation');
    $this->assertEquals('ST0005', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'street_address');
    $this->assertEquals('ST0006', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'alphanumeric_punctuation');
    $this->assertEquals('ST0007', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'alphanumeric_spaces_punctuation_octothorpe');
    $this->assertEquals('ST0008', $result[1]);

    $result = UltraUtil\validatorStringformat('', '123456', 'not_all_numeric');
    $this->assertEquals('ST0009', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!123', 'no_digits');
    $this->assertEquals('ST0010', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'no_symbols');
    $this->assertEquals('ST0011', $result[1]);

    $result = UltraUtil\validatorStringformat('', '\u1000', 'alpha_with_unicode');
    $this->assertEquals('ST0013', $result[1]);

    $result = UltraUtil\validatorStringformat('', 'Break it ***&^$!!', 'profanity_exclude');
    $this->assertEquals('ST0014', $result[1]);
  }

  /**
   * test_validatorActcode
   */
  public function test_validatorActcode()
  {
    $result = UltraUtil\validatorActcode(null, null, null);
    $this->assertEquals('VV0019', $result[1]);

    $result = UltraUtil\validatorActcode('', '', 'valid_actcode');
    $this->assertEquals('VV0020', $result[1]);

    $result = UltraUtil\validatorActcode('', '23456789123', 'valid_actcode');
    $this->assertEquals('VV0018', $result[1]);
  }

  /**
   * test_validatorAddress
   */
  public function test_validatorAddress()
  {
    $result = UltraUtil\validatorAddress(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorAddress('', '***$$#@!!!!!');
    $this->assertEquals('VV0047', $result[1]);
  }

  /**
   * test_validatorCC
   */
  public function test_validatorCC()
  {
    $result = UltraUtil\validatorCC(null, null);
    $this->assertEquals('VV0063', $result[1]);

    $result = UltraUtil\validatorCC('', '4111-1111-1111-1111');
    $result = array_filter($result);
    $this->assertTrue(empty($result));
  }

  /**
   * test_validatorCity
   */
  public function test_validatorCity()
  {
    $result = UltraUtil\validatorCity(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorCity('', '!@#asd123');
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: invalid city', $result[0]);
    $this->assertEquals('VV0049', $result[1]);
  }

  /**
   * test_validatorCountry
   */
  public function test_validatorCountry()
  {
    $result = UltraUtil\validatorCountry(null, null);
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: invalid country', $result[0]);
    $this->assertEquals('VV0058', $result[1]);

    $result = UltraUtil\validatorCountry('', 'US');
    $result = array_filter($result);
    $this->assertTrue(empty($result));
  }

  /**
   * test_validatorCVV
   */
  public function test_validatorCVV()
  {
    $result = UltraUtil\validatorCVV(null, null);
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: the given cvv is invalid', $result[0]);
    $this->assertEquals('VV0062', $result[1]);

    $result = UltraUtil\validatorCVV('', 1234);
    $result = array_filter($result);
    $this->assertTrue(empty($result));
  }

  /**
   * test_validatorZipcode
   */
  public function test_validatorZipcode()
  {
    $result = UltraUtil\validatorZipcode(null, null, null);
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'ERR_API_INVALID_ARGUMENTS:'));
    $this->assertEquals('VV0019', $result[1]);

    $result = UltraUtil\validatorZipcode('', '', 'US');
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'ERR_API_INVALID_ZIP:'));
    $this->assertEquals('VV0023', $result[1]);

    $result = UltraUtil\validatorZipcode('', '12321asd', 'in_coverage');
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'ERR_API_INVALID_ZIP:'));
    $this->assertEquals('VV0023', $result[1]);
  }

  /**
   * test_validatorPin
   */
  public function test_validatorPin()
  {
    $result = UltraUtil\validatorPin(null, null, null);
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: invalid PIN (1)', $result[0]);
    $this->assertEquals('PI0001', $result[1]);

    $result = UltraUtil\validatorPin('', '12345', array(1, 2, 3));
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: invalid PIN (2)', $result[0]);
    $this->assertEquals('PI0001', $result[1]);

    $result = UltraUtil\validatorPin('', '12345', 3);
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: invalid PIN (3)', $result[0]);
    $this->assertEquals('PI0001', $result[1]);
  }

  /**
   * test_validatorName
   */
  public function test_validatorName()
  {
    $result = UltraUtil\validatorName(null, null);
    $result = array_filter($result);
    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorName('', 'Invalid NAME!!!***');
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: invalid name', $result[0]);
    $this->assertEquals('VV0038', $result[1]);
  }

  /**
   * test_validatorNumeric
   */
  public function test_validatorNumeric()
  {
    $result = UltraUtil\validatorNumeric(null, null, null);
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'ERR_API_INVALID_ARGUMENTS: Unknown validation'));

    $result = UltraUtil\validatorNumeric('', '', 'integer');
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'value fails integer format validation'));

    #$result = UltraUtil\validatorNumeric('', 3, 'integer');
    #$this->assertTrue(empty($result));

    $result = UltraUtil\validatorNumeric('', '12345.123', 'float');
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'value fails float format validation'));

    #$result = UltraUtil\validatorNumeric('', '', 'currency_US');
    #$this->assertTrue(empty($result));

    $result = UltraUtil\validatorNumeric('', 'asdfasd', 'currency_US');
    $this->assertGreaterThanOrEqual(0, strpos($result[0], 'value fails USA Currency format validation'));
  }

  /**
   * test_validatorCountry2chars
   */
  public function test_validatorCountry2chars()
  {
    $result = UltraUtil\validatorCountry2chars('', '', 'ISO_3166-1');
    $result = array_filter($result);

    $this->assertTrue(empty($result));

    $result = UltraUtil\validatorCountry2chars(null, null, null);

    $this->assertEquals('', $result[1]);

    $result = UltraUtil\validatorCountry2chars('Name', 'USP', 'ISO_3166-1');

    $this->assertEquals('VV0058', $result[1]);
  }

  /**
   * test_validatorCountryCode
   */
  public function test_validatorCountryCode()
  {
    $this->assertFalse(UltraUtil\validatorCountryCode(''));

    $this->assertFalse(UltraUtil\validatorCountryCode('A Fake United States'));

    $this->assertTrue(UltraUtil\validatorCountryCode('US'));

    $this->assertFalse(UltraUtil\validatorCountryCode('U'));
  }

  /**
   * test_validatorCountryName
   */
  public function test_validatorCountryName()
  {
    $this->assertFalse(UltraUtil\validatorCountryName(''));

    $this->assertFalse(UltraUtil\validatorCountryName('UN'));

    $validCountryName = UltraUtil\validatorCountryName('United States');
    $this->assertTrue($validCountryName);

    $validCountryName = UltraUtil\validatorCountryName('A Fake United States');
    $this->assertFalse($validCountryName);
  }

  /**
   * test_validatorPassword
   */
  public function test_validatorPassword()
  {
    $result = UltraUtil\validatorPassword('', 'RandompassWORD', '');

    $this->assertTrue(is_array($result));

    $this->assertEquals('ERR_API_INTERNAL: No password validation rule provided', $result[0]);
    $this->assertEquals('VV0246', $result[1]);

    $result = UltraUtil\validatorPassword('', '11111111', 'blocklist');
    $this->assertEquals('ERR_API_INVALID_ARGUMENTS: password fails blocklist validation', $result[0]);
    $this->assertEquals('VV0245', $result[1]);
  }

  /**
   * test_validateAgainstBlocklist
   */
  public function test_validateAgainstBlocklist()
  {
    $this->assertFalse(UltraUtil\validateAgainstBlocklist('barney'));
    $this->assertTrue(UltraUtil\validateAgainstBlocklist('This Should work'));
  }

  /**
   * test_validateProfanity
   */
  public function test_validateProfanity()
  {
    $profanity = 'anal';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'anus';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'ass';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'bastard';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'bitch';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'boob';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'cock';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'cum';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'cunt';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'dick';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'dildo';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'dyke';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'fag';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'faggot';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'fuck';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'fuk';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'handjob';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'homo';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'jizz';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'kike';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'kunt';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'muff';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'nigger';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'penis';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'piss';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'poop';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'pussy';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'queer';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'rape';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'semen';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'sex';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'shit';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'slut';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'titties';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'twat';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'vagina';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'vulva';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));

    $profanity = 'wank';
    $this->assertTrue(UltraUtil\validateProfanity($profanity));
  }

  // validateStreetAddress
  public function test_validateStreetAddress()
  {
    $parameters = array(
      'street'    => '4166 Buford Hwy NE # M2',
      'city'      => 'Altanta',
      'state'     => 'GA',
      'zipcode'   => '30345');
    $parameters = array(
      'street'    => '812 N Thompson St',
#      'suite'     => 'Ste 9',
      'city'      => 'Springdale',
      'state'     => 'AR',
      'zipcode'   => '72764');

    $result = \Ultra\Lib\Util\validateStreetAddress($parameters);
    print_r($result);
  }
}
