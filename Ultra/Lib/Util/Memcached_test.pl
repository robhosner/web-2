use strict;
use warnings;


use Ultra::Lib::Util::Memcached;


my $memcached = Ultra::Lib::Util::Memcached->new();


print $memcached->mcGet('bb');

print "\n";

print $memcached->mcSet('bb',42);

print "\n";

print $memcached->mcGet('bb');

print "\n";

print $memcached->mcIncr('bb');

print "\n";

print $memcached->mcGet('bb');

print "\n";

__END__

