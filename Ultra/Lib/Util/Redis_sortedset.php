<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis.php';

$ultra_redis = new \Ultra\Lib\Util\Redis;

$ultra_redis->zadd( 'test_sorted_set' , 1 , "1a" );
$ultra_redis->zadd( 'test_sorted_set' , 2 , "2bb" );
$ultra_redis->zadd( 'test_sorted_set' , 2 , "2cc" );
$ultra_redis->zadd( 'test_sorted_set' , 4 , "4dddd");
$ultra_redis->zadd( 'test_sorted_set' , 4 , "4ffff");
$ultra_redis->zadd( 'test_sorted_set' , 3 , "3eee" );
$ultra_redis->zadd( 'test_sorted_set' , 3 , "3ggg" );

$x = $ultra_redis->zhead('test_sorted_set',2);
echo "$x\n";

$y = $ultra_redis->zrangebyscore('test_sorted_set',2,3);
print_r($y);

?>
