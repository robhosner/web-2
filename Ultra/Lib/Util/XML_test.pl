use strict;
use warnings;

use Data::Dumper;
use Ultra::Lib::Util::XML;

my $x = Ultra::Lib::Util::XML::xmlify('a',{x=>1});

print Dumper( $x );

exit;

my $o = Ultra::Lib::Util::XML->new();

my $xml = 'garbage<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<ProvisioningStatus xmlns="http://www.sigvalue.com/acc">
<UserData>
<senderId>ACC</senderId>
<timeStamp>2013-11-11T09:35:38</timeStamp>
</UserData>
<MSISDN>6199060986</MSISDN>
<TransactionIDAnswered>API-ULTRA-20400002204</TransactionIDAnswered>
<ProvisioningType>ChangeMSISDN</ProvisioningType>
<ProvisioningStatus>true</ProvisioningStatus>
<StatusDescription></StatusDescription>
</ProvisioningStatus>
</soap:Body>
</soap:Envelope>';

$xml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><SendThrottlingNotification xmlns="http://tempuri.org/"><msisdn>19738140693</msisdn><notificationType>DATA_USAGE_BLOCKED</notificationType><Message>Subscriber has reached the data threshold limit and is being BLOCKED</Message></SendThrottlingNotification>';

$xml = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <QuerySubscriberResponse xmlns="http://www.sigvalue.com/acc">
      <QuerySubscriberResult>1</QuerySubscriberResult>
      <ResultCode>200</ResultCode>
      <ResultMsg>Invalid ICCID</ResultMsg>
      <MSISDN></MSISDN>
 <serviceTransactionId>API-ULTRA-168362156540</serviceTransactionId>
     <BAN></BAN>
      <SIM></SIM>
      <IMSI></IMSI>
      <IMEI></IMEI>
       <SubscriberStatus></SubscriberStatus>
<wholesalePlan xsi:nil="true"/>
      <PlanId xsi:nil="true"/>
      <PlanEffectiveDate xsi:nil="true"/>
<PlanExpirationDate xsi:nil="true"/>
      <AutoRenewalIndicator xsi:nil="true"/>
      <CurrentAsyncService></CurrentAsyncService>
      <AddOnFeatureInfoList>
         
      </AddOnFeatureInfoList>
     <E911Address>
       <addressLine1></addressLine1>
       <addressLine2></addressLine2>
       <City></City>
       <State></State>
       <Zip></Zip>
     </E911Address>
    </QuerySubscriberResponse>
  </soap:Body>
</soap:Envelope>';


print $o->extractSOAPXMLEnvelope( $xml );
print "\n";

__END__

