use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Miscellaneous;


my ($url,$data,$xml,$seconds_timeout,$ua) = 
(
  # DEV
  #'https://dougmeli:Flora@rgalli3-dev.uvnv.com/pr/internal/2/ultra/api/internal__PortOutDeactivation',
  # PROD
  'https://ultra_mw_prod:f879d47151f35dec!@mw-aspider-prod.ultra.me/pr/internal/2/ultra/api/internal__PortOutDeactivation',
  { MSISDN => '2000500035' , portOutCarrierID => 321 , portOutCarrierName => 'test' },
  120,
  undef
);

my ($response,$timeout) = Ultra::Lib::Util::Miscellaneous::userAgentRequest($url,$data,$seconds_timeout,$ua);

print Dumper($response);
print Dumper($timeout);

__END__

