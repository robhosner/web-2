<?php

/**
 * Class for setting up payload for SendSMS command.
 *
 * Usage:
 *  $sms = new SendSMSPayload('9499032013', 'en', 'Testing');
 *  $payload = $sms->getPayload();
 */
class SendSMSPayload
{
  public $sendSMS; // Payload
  public $UserData;
  public $qualityOfService;
  public $preferredLanguage;
  public $smsText;
  public $SubscriberSMSList;

  /**
   * Constructor for setting up the payload
   *
   * @param string $MSISDN
   * @param string $preferredLanguage
   * @param string $smsText
   * @throws \Exception
   * @author bwalters@ultra.me
   */
  public function __construct($MSISDN = false, $preferredLanguage = 'en', $smsText = false)
  {
    if (!$MSISDN)  throw new \Exception('Missing MSISDN');
    if (!$smsText) throw new \Exception('Missing smsText');

    $now                              = time();
    $this->sendSMS                    = new SendSMS();
    $this->sendSMS->qualityOfService  = getSMSQualityOfService($smsText);
    $this->sendSMS->preferredLanguage = $preferredLanguage;
    $this->sendSMS->smsText           = $smsText;

    // Setup UserData
    $UserData = new UserData();
    $UserData->senderId       = 'MVNEACC';
    $UserData->channelId      = 'ULTRA';
    $UserData->timeStamp      = date('Y-m-d', $now) . 'T' . date('H:s:i', $now);
    $this->sendSMS->UserData  = $UserData;

    $SubscriberSMSList  = new SubscriberSMSList();
    $SubscriberSMS      = new SubscriberSMS();

    $SubscriberSMS->MSISDN    = $MSISDN;
    $SubscriberSMS->Language  = $this->sendSMS->preferredLanguage;

    $SubscriberSMSList->SubscriberSMS = $SubscriberSMS;
    $this->sendSMS->SubscriberSMSList = $SubscriberSMSList;
  }

  /**
   * Returns the actual payload that should
   * be passed to the SoapClient for sending
   * to the SoapServer.
   *
   * @return class
   * @author bwalters@ultra.me
   */
  public function getPayload()
  {
    return $this->sendSMS;
  }
}

/**
 * Structure for SendSMS
 */
class SendSMS
{
  public $UserData;
  public $qualityOfService;
  public $preferredLanguage;
  public $smsText;
  public $SubscriberSMSList;
}

/**
 * Structure for SendSMS->UserData
 */
class UserData
{
  public $senderId;
  public $channelId;
  public $timeStamp;
}

/**
 * Structure for SendSMS->SubscriberSMSList
 */
class SubscriberSMSList
{
  public $SubscriberSMS;
}

/**
 * Structure for SendSMS->SubscriberSMSList->SubscriberSMS
 */
class SubscriberSMS
{
  public $MSISDN;
  public $Language;
  public $Text;
}

