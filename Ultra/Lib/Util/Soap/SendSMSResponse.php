<?php

/**
 * Used by AmdocsSoapClient for mapping responses
 * to properties of a class for easier everything.
 */
class SendSMSResponse
{
  public $isTimeout = FALSE;

  public $SendSMSResult;
  public $ResultCode;
  public $ResultMsg;
  public $serviceTransactionId;
}
