<?php

namespace Ultra\Lib\Util;

define( 'TOKEN_VERIFICATION_TTL' , 604800 );  // 1 week

/**
 * Per MVNO-3135: Start encrypting passwords in our DB w/hash stretching
 * @see See https://nakedsecurity.sophos.com/2013/11/20/serious-security-how-to-store-your-users-passwords-safely/
 * @see http://php.net/manual/en/function.hash-hmac.php
 * For usage, see Ultra/Lib/Util/Hash_test.php
 */
class Hash
{
  /*********************************************
   * DANGER: Please note that changing these will make
   * passwords in the database impossible to retrieve
   * unless we store information such as what algorithm,
   * key size and iteration count we used to generate it.
   ********************************************/
  private $algorithm = 'sha256';
  private $key_length = 32;
  private $iterations = 1000;

  public function __constructor()
  {

  }

  /**
   * get_iterations
   *
   * Will return the number of iterations we ran to generate the hash.
   * @return integer
   * @author bwalters@ultra.me
   */
  public function get_iterations()
  {
    return $this->iterations;
  }

  /**
   * generate_salt
   *
   * Will generate a salt from /dev/urandom
   *
   * @return string
   * @author bwalters@ultra.me
   */
  public function generate_salt()
  {
    return hash($this->algorithm, mcrypt_create_iv($this->key_length, MCRYPT_DEV_URANDOM));
  }

  /**
   * generate_password
   *
   * Given the clear_text password and a salt, will
   * generate the hashed password for storage in a database.
   * @see https://nakedsecurity.sophos.com/2013/11/20/serious-security-how-to-store-your-users-passwords-safely/
   * @param string $user_submitted_password
   * @param string $salt
   * @return string
   * @author bwalters@ultra.me
   */
  public function generate_password($user_submitted_password=FALSE, $salt=FALSE)
  {
    if (!$user_submitted_password)  throw new \Exception("missing required parameter user_submitted_password");
    if (!$salt)                     throw new \Exception("missing required parameter salt");

    $hash = base64_encode($this->str_hash_pbkdf2($user_submitted_password, $salt, $this->iterations, $this->key_length, $this->algorithm));

    return $hash;
  }

  /**
   * check_password
   *
   * Given the user_submitted_password, the hashed_password, and the salt,
   * will attempt to hash & compare the results to see if the user's password is correct.
   * @see https://nakedsecurity.sophos.com/2013/11/20/serious-security-how-to-store-your-users-passwords-safely/
   * @param string $user_submitted_password
   * @param string $hashed_password
   * @param string $salt
   * @return string
   * @author bwalters@ultra.me
   */
  function check_password($user_submitted_password=FALSE, $hashed_password=FALSE, $salt=FALSE)
  {
    if (!$user_submitted_password)  throw new \Exception("missing required parameter user_submitted_password");
    if (!$hashed_password)          throw new \Exception("missing required parameter hashed_password");
    if (!$salt)                     throw new \Exception("missing required parameter salt");

    // Hash user_submitted password to compare against the hashed_password from database
    $password = $this->str_hash_pbkdf2($user_submitted_password, $salt, $this->iterations, $this->key_length, $this->algorithm);
    $is_valid = ($password == $hashed_password) ? true : false;

    return $is_valid;
  }

  /**
   * str_hash_pbkdf2
   *
   * PBKDF2 Implementation (described in RFC 2898)
   * @see http://php.net/manual/en/function.hash-hmac.php
   * @param   string  $password
   * @param   string  $salt
   * @param   int     $iteration_count
   * @param   int     $key_length
   * @param   string  $algorithm
   * @param   int     $start
   * @return  string  derived key
   */
  public function str_hash_pbkdf2($password, $salt, $iteration_count, $key_length, $algorithm = 'sha256', $start = 0)
  {
    $key_block = $start + $key_length; // Key blocks to compute
    $derived_key = '';

    // Create key
    for ($block = 1; $block <= $key_block; $block++)
    {
      // Initial hash for this block
      $ib = $h = hash_hmac($algorithm, $salt . pack('N', $block), $password, true);

      // Perform block iterations
      for ($i = 1; $i < $iteration_count; $i++)
      {
        // XOR each iterate
        $ib ^= ($h = hash_hmac($algorithm, $h, $password, true));
      }

      $derived_key .= $ib; // Append iterated block
    }

    // Return derived key of correct length
    return substr($derived_key, $start, $key_length);
  }
}

/**
 * encryptPasswordHS
 *
 * Returns a 108 characters long string which contains the salt and the hashed password
 *
 * @return string
 */
function encryptPasswordHS( $password )
{
  $hasher          = new Hash();
  $salt            = $hasher->generate_salt();
  $hashed_password = $hasher->generate_password( $password , $salt );

  return $salt . $hashed_password ;
}

/**
 * authenticatePasswordHS
 *
 * Returns true if $givenPassword matches with $encryptedPassword
 *
 * @return boolean (TRUE if verified)
 */
function authenticatePasswordHS( $encryptedPassword , $givenPassword )
{
  // function will not work if less than 65
  if (strlen($encryptedPassword) < 65)
    return FALSE;

  $hasher          = new Hash();
  $salt            = substr($encryptedPassword, 0, 64);
  $hashed_password = substr($encryptedPassword, 64);

  return $hasher->check_password( $givenPassword , base64_decode($hashed_password) , $salt );
}

/**
 * getTokenForRefresh
 *
 * Primo token generator
 * There is no decryption, we use one way hash and we keep the salt.
 * We rehash when we receive the token with $login_name, apply the salt kept in mongo and make sure that both salted hash are equal
 *
 * @param $login_name    is the subscriber's LOGIN_NAME
 * @param $access_number is the two stage dialing access number
 * @return array         ( $salt , $uid , $token )
 */
function getTokenForRefresh( $login_name , $access_number )
{
  $login_name = strtolower($login_name);

  $salt = base64_encode( mcrypt_create_iv(22, MCRYPT_DEV_URANDOM) );
  $salt = str_replace('+', '.', $salt);
  $hash = crypt( $login_name , '$2y$10$'.$salt.'$');
  $uid  = uniqid('', true);

  $token = (string) time() . '|' . $hash . '|' . $uid . '|' . base64_encode( $access_number );

  \logtrace("login_name    = $login_name");
  \logtrace("access_number = $access_number");
  \logtrace("salt          = $salt");
  \logtrace("uid           = $uid");
  \logtrace("token         = $token");

  return array(
    $salt,
    $uid,
    $token
  );
}

/**
 * verifyTokenForRefresh
 *
 * Primo token verification
 *
 * @param $login_name    is the subscriber's LOGIN_NAME
 * @param $access_number is the two stage dialing access number
 * @param $salt          is the previously stored $salt
 * @param $uid           is the previously stored $uid
 * @param $token         is the token to be tested
 * @param $ttl           is the ttl of token
 * @return $error_code   NULL if verified
 */
function verifyTokenForRefresh( $login_name , $access_number , $salt , $uid , $token , $ttl=null )
{
  $login_name = strtolower($login_name);
  
  \logtrace("login_name    = $login_name");
  \logtrace("access_number = $access_number");
  \logtrace("salt          = $salt");
  \logtrace("uid           = $uid");
  \logtrace("token         = $token");

  $components = explode( '|' , $token );

  // sanity checks
  if ( empty( $login_name ) || empty( $access_number ) || empty( $salt ) || empty( $uid ) || empty( $token ) )
  {
    \logit('missing parameter(s)');
    return 'MP0001';
  }

  // analyze token
  if ( count( $components ) != 4 )
  {
    \logit('token is invalid');
    return 'SE0014';
  }

  // analyze time
  $ttl = !empty( $ttl ) ? $ttl : TOKEN_VERIFICATION_TTL;
  if ( time() > $components[0] + $ttl )
  {
    \logit('token is expired');
    return 'SE0015';
  }

  // analyze $hash
  if ( crypt( $login_name , '$2y$10$'.$salt.'$') != $components[1] )
  { 
    \logit('hash mismatch');
    return 'SE0016';
  }

  // analyze $uid
  if ( $uid != $components[2] )
  {
    \logit('uid mismatch');
    return 'SE0017';
  }

  // analyze $access_number
  if ( base64_encode( $access_number ) != $components[3] )
  {
    \logit('access_number mismatch');
    return 'SE0018';
  }

  return NULL;
}

