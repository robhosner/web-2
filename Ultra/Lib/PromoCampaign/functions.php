<?php

namespace Ultra\Lib\PromoCampaign;

require_once 'Ultra/Messaging/Templates.php';
require_once 'lib/state_machine/functions.php';

/**
 * triggerPromoAction
 *
 * Trigger the appropriate action associated with $promoAction
 *
 * @param string $promoKeyword
 * @param string $promoAction
 * @param string $promoValue
 * @param Object $customer
 * @return object of class \Result
 */
function triggerPromoAction($promoKeyword, $promoAction, $promoValue, $customer)
{
  dlog('', '(%s)', func_get_args());

  $promoKeyword = strtoupper( $promoKeyword );
  $promoAction  = ucfirst( strtolower( $promoAction ) );

  if ( ! $customer )
  {
    return make_error_Result(
      'ERR_API_INTERNAL: invalid customer',
      array(
        'error'      => 'ERR_API_INTERNAL: invalid customer',
        'error_code' => 'DB0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }

  $promoActionFunction = '\Ultra\Lib\PromoCampaign\triggerShortCode'.$promoAction;

  dlog('',"promoAction = $promoAction ; promoActionFunction = $promoActionFunction");

  if ( is_callable( $promoActionFunction ) )
  {
    // invoke specific triggerShortCode function
    $result = $promoActionFunction( $customer , $promoValue, $promoKeyword, $promoKeyword );

    $result_error = 'ERROR';
    if ( $result->is_failure() && ! empty($result->data_array['result_error']) )
      $result_error = $result->data_array['result_error'];

    return $result;
  }
  else
  {
    return make_error_Result(
      'ERR_API_INTERNAL: promotion not available',
      array(
        'error'      => 'ERR_API_INTERNAL: promotion not available',
        'error_code' => 'VV0017',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_available' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
}

// See: http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+Framework

/**
 * triggerShortCode
 *
 * Trigger the appropriate action associated with $shortcode
 *  - The campaign should be enabled and not expired
 *  - ULTRA.PROMO_BROADCAST_LOG should contain a row for this customer with PROMO_STATUS = 'UNCLAIMED'
 *
 * @param string $shortcode Shortcode (aka. keyword)
 * @param object $customer Customer object
 * @return object of class \Result
 */
function triggerShortCode( $shortcode , $customer )
{
  dlog('', '(%s)', func_get_args());

  $shortcode = strtoupper( $shortcode );

  if ( ! $customer )
    return make_error_Result(
      'ERR_API_INTERNAL: invalid customer',
      array(
        'error'      => 'ERR_API_INTERNAL: invalid customer',
        'error_code' => 'DB0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  // get the PROMO_ENABLED campaign associated with $shortcode
  $result = getPromoEnabledCampaignByShortCode( $shortcode , $customer );

  if ( $result->is_failure() )
  {
    // record failure in ULTRA.PROMO_BROADCAST_ATTEMPT_LOG
    logTriggerShortCodeResult( $shortcode , $customer->CUSTOMER_ID , 'NOT_ENABLED' );

    return $result;
  }

  $promo_broadcast_campaign_id = $result->data_array['PROMO_BROADCAST_CAMPAIGN_ID'];
  $promo_description           = $result->data_array['PROMO_DESCRIPTION'];

  // get data from ULTRA.PROMO_BROADCAST_LOG
  $logResult = \get_ultra_promo_broadcast_log(
    array(
      'top'                         => 1,
      'customer_id'                 => $customer->CUSTOMER_ID,
      'promo_broadcast_campaign_id' => $promo_broadcast_campaign_id
    )
  );

  if ( $logResult->is_failure() )
    return make_error_Result(
      'ERR_API_INTERNAL: DB error (0)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (0)',
        'error_code' => 'DB0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  dlog('', "get_ultra_promo_broadcast_log returned %s", $logResult->data_array);

  // verify that subscriber has received this offer
  if ( ! count($logResult->data_array) )
    return make_error_Result(
      'ERR_API_INVALID_ARGUMENTS: You do not qualify for this promotion.',
      array(
        'error'      => 'ERR_API_INVALID_ARGUMENTS: You do not qualify for this promotion.',
        'error_code' => 'EL0002',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_qualify' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  // verify that the offer has not been already redeemed
  if ( $logResult->data_array[0]->PROMO_STATUS != 'UNCLAIMED' )
    return make_error_Result(
      'ERR_API_INVALID_ARGUMENTS: You have already redeemed this promotion.',
      array(
        'error'      => 'ERR_API_INVALID_ARGUMENTS: You have already redeemed this promotion.',
        'error_code' => 'EL0003',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_already_redeemed' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  $promoAction = ucfirst( strtolower( $result->data_array['PROMO_ACTION'] ) );

  // construct specific triggerShortCode function name
  $promoActionFunction = '\Ultra\Lib\PromoCampaign\triggerShortCode'.$promoAction;

  dlog('',"promoAction = $promoAction ; promoActionFunction = $promoActionFunction");

  if ( is_callable( $promoActionFunction ) )
  {
    // invoke specific triggerShortCode function
    $result = $promoActionFunction( $customer , $result->data_array['PROMO_VALUE'], $shortcode, $result->data_array['PROMO_DESCRIPTION'] );

    if ( $result->is_success() )
    {
      // redeemed offer: update ULTRA.PROMO_BROADCAST_LOG
      $success = is_mssql_successful(
        logged_mssql_query(
          \Ultra\Lib\DB\makeUpdateQuery(
            'ULTRA.PROMO_BROADCAST_LOG',
            array(
              'PROMO_STATUS'       => 'CLAIMED',
              'PROMO_CLAIMED_DATE' => timestamp_to_date( time() , MSSQL_DATE_FORMAT , 'UTC' )
            ),
            array(
              'PROMO_BROADCAST_CAMPAIGN_ID' => $promo_broadcast_campaign_id,
              'CUSTOMER_ID'                 => $customer->CUSTOMER_ID
            )
          )
        )
      );

      if ( ! $success )
        // non-fatal error
        dlog('',"We could not update ULTRA.PROMO_BROADCAST_LOG");
    }

    $result_error = 'ERROR';
    if ( $result->is_failure() && ! empty($result->data_array['result_error']) )
      $result_error = $result->data_array['result_error'];

    // record result in ULTRA.PROMO_BROADCAST_ATTEMPT_LOG
    logTriggerShortCodeResult(
      $shortcode,
      $customer->CUSTOMER_ID,
      ( $result->is_failure() ? $result_error : 'SUCCESS' )
    );

    return $result;
  }
  else
  {
    // record failure in ULTRA.PROMO_BROADCAST_ATTEMPT_LOG
    logTriggerShortCodeResult( $shortcode , $customer->CUSTOMER_ID , 'NOT_HANDLED' );

    return make_error_Result(
      'ERR_API_INTERNAL: promotion configuration error. (3)',
      array(
        'error'      => 'ERR_API_INTERNAL: promotion configuration error. (3)',
        'error_code' => 'VV0017',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_available' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
}

/**
 * triggerShortCodeAddbalance
 *
 * PROMO_ACTION = ADDBALANCE
 * Add BALANCE to the customer's account
 *
 * @param object $customer Customer object
 * @param integer $promoValue Value to add to the customer's BALANCE in cents
 * @return object of class \Result
 */
function triggerShortCodeAddbalance( $customer , $promoValue, $shortcode, $description )
{
  if ($error = addWalletBalance($customer, $promoValue))
    return $error;

  // get customer's plan_state without caching
  $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer->CUSTOMER_ID, 'plan_state', 0);

  // customer must be Active
  if ( $plan_state != STATE_ACTIVE )
  {
    // if Suspended or Provisioned, transition to Active
    $result_status = \activate_customer($customer);

    dlog('',"activate_customer result = %s",$result_status);
  }

  logBillingHistory($customer, 0, $promoValue, $shortcode, __FUNCTION__, $description);

  return make_ok_Result();
}

/**
 * triggerShortCodeAddstoredvalue
 *
 * PROMO_ACTION = ADDSTOREDVALUE
 * Add STORED_VALUE to the customer's account
 *
 * @param object $customer Customer object
 * @param integer $promoValue Value to add to the customer's STORED_VALUE in cents
 * @return object of class \Result
 */
function triggerShortCodeAddstoredvalue( $customer , $promoValue, $shortcode, $description )
{
  if ($error = addStoredValue($customer, $promoValue))
    return $error;

  // get customer's plan_state without caching
  $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer->CUSTOMER_ID, 'plan_state', 0);

  // customer must be Active
  if ( $plan_state != STATE_ACTIVE )
  {
    // if Suspended or Provisioned, transition to Active
    $result_status = \activate_customer($customer);

    dlog('',"activate_customer result = %s",$result_status);
  }

  logBillingHistory($customer, $promoValue, 0, $shortcode, __FUNCTION__, $description);

  return make_ok_Result();
}

/**
 * triggerShortCodeAddpackagebalance1
 *
 * PROMO_ACTION = ADDPACKAGEBALANCE1
 * Add PACKAGED_BALANCE_1 to the customer's account
 * The customer must be Active
 *
 * @param object $customer Customer object
 * @param integer $promoValue Value to add to the customer's PACKAGED_BALANCE_1 in cents
 * @return object of class \Result
 */
function triggerShortCodeAddpackagebalance1( $customer , $promoValue, $shortcode, $description )
{
  if ($error = ensureSubscriberActive($customer))
    return $error;

  $success = is_mssql_successful(
    logged_mssql_query(
      accounts_update_query(
        array(
          'add_to_package_balance1' => ( $promoValue / 10 ) ,
          'customer_id'             => $customer->CUSTOMER_ID
        )
      )
    )
  );

  if ( $success )
    return make_ok_Result();
  else
    return make_error_Result(
      'ERR_API_INTERNAL: DB error (0)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (0)',
        'error_code' => 'DB0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
}

/**
 * triggerShortCodeAdd4gdata
 *
 * PROMO_ACTION = ADD4GDATA
 * Add Data to the user's account
 * The customer must be Active
 *
 * @param object $customer Customer object
 * @param integer $promoValue Value to add to the customer's in MB
 * @return object of class \Result
 */
function triggerShortCodeAdd4gdata( $customer , $promoValue, $shortcode, $description )
{
  if ($error = ensureSubscriberActive($customer))
    return $error;

  if ($promoValue)
    if ($error = add4gData($customer, $promoValue))
      return $error;

  return make_ok_Result();
}

/**
 * triggerShortCodeRoam
 *
 * PROMO_ACTION = ADDROAM
 * Add Roam credit to the user's account
 * The customer must be Active
 *
 * @param object $customer Customer object
 * @param integer $promoValue Value to add
 * @return object of class \Result
 */
function triggerShortCodeAddRoam( $customer , $promoValue, $shortcode, $description )
{
  if ($error = ensureSubscriberActive($customer))
    return $error;

  if ($promoValue)
    if ($error = addRoam($customer, $promoValue))
      return $error;

  return make_ok_Result();
}

/**
 * triggerShortCodeAutoRechargeWallet
 *
 * PROMO_ACTION = AUTORECHARGEWALLET
 * enable subscriber monthly auto renewal using the credit card on file and optionally add some WALLET balance
 *
 * @param Object Customer object
 * @param Integer value (in cents) to add to the customer's WALLET
 * @return Object of class \Result
 */
function triggerShortCodeAutoRechargeWallet($customer, $promoValue, $shortcode, $description)
{
  if ($error = ensureSubscriberActive($customer))
    return $error;

  if ($error = ensureNotAutoRenewed($customer))
    return $error;

  if ($error = ensureHasCreditCard($customer))
    return $error;

  if ($error = enableAutoRecharge($customer))
    return $error;

  if ($promoValue)
  {
    if ($error = addWalletBalance($customer, $promoValue))
      return $error;
    logBillingHistory($customer, 0, $promoValue, $shortcode, __FUNCTION__, $description);
  }

  return make_ok_Result();
}

/**
 * triggerShortCodeAutoRechargeData
 *
 * PROMO_ACTION = AUTORECHARGEDATA
 * enable subscriber monthly auto renewal using the credit card on file and optionally add some data to the account
 *
 * @param Object Customer object
 * @param Integer value (in MB) of data to add
 * @return Object of class \Result
 */
function triggerShortCodeAutoRechargeData($customer, $promoValue, $shortcode, $description)
{
  if ($error = ensureSubscriberActive($customer))
    return $error;

  if ($error = ensureNotAutoRenewed($customer))
    return $error;

  if ($error = ensureHasCreditCard($customer))
    return $error;

  if ($error = enableAutoRecharge($customer))
    return $error;

  if ($promoValue)
    if ($error = add4gData($customer, $promoValue))
      return $error;

  return make_ok_Result();
}

/**
 * triggerShortCodeCCReactivateWallet
 * PROMO_ACTION = CCREACTIVATEWALLET
 * re-activate suspended subscriber using the credit card on file and optionally add funds to wallet
 *
 * @param Object Customer object
 * @param Integer value (in cents) of wallet credit
 * @return Object of class \Result
 */
function triggerShortCodeCCReactivateWallet($customer, $promoValue, $shortcode, $description)
{
  if ($error = ensureSubscriberSuspended($customer))
    return $error;

  if ($error = ensureHasCreditCard($customer))
    return $error;

  if ($error = chargeCreditCardReactivation($customer))
    return $error;

  if ($promoValue)
  {
    if ($error = addWalletBalance($customer, $promoValue))
      return $error;
    logBillingHistory($customer, 0, $promoValue, $shortcode, __FUNCTION__, $description);
  }

  return make_ok_Result();
}

/**
 * triggerShortCodeCCReactivateData
 * PROMO_ACTION = CCREACTIVATEDATA
 * re-activate suspended subscriber using the credit card on file and optionally add cellular data
 *
 * @param Object Customer object
 * @param Integer value (in cents) of wallet credit
 * @return Object of class \Result
 */
function triggerShortCodeCCReactivateData($customer, $promoValue, $shortcode, $description)
{
  if ($error = ensureSubscriberSuspended($customer))
    return $error;

  if ($error = ensureHasCreditCard($customer))
    return $error;

  if ($error = chargeCreditCardReactivation($customer))
    return $error;

  if ($promoValue)
    if ($error = add4gData($customer, $promoValue))
      return $error;

  return make_ok_Result();
}

/**
 * getPromoEnabledCampaignByShortCode
 *
 * Get the most recent PROMO_ENABLED campaign associated with $shortcode
 *
 * @param string $shortcode Shortcode (aka. keyword)
 * @return object of class \Result
 */
function getPromoEnabledCampaignByShortCode( $shortcode , $customer )
{
  $result = \get_ultra_promo_broadcast_campaign(
    array(
      'top'             => 1,
      'promo_shortcode' => $shortcode,
      'order_by'        => ' PROMO_BROADCAST_CAMPAIGN_ID DESC '
    )
  );

  // did the query fail?
  if ( $result->is_failure() )
    return make_error_Result(
      'ERR_API_INTERNAL: DB error (1)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (1)',
        'error_code' => 'DB0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  dlog( '',"%s",$result->data_array );

  // did we found data?
  if ( ! count( $result->data_array ) )
    return make_error_Result(
      'ERR_API_INTERNAL: data not found',
      array(
        'error'      => 'ERR_API_INTERNAL: data not found',
        'error_code' => 'ND0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_exist' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  // is the promotion enabled?
  if ( ! $result->data_array[0]->PROMO_ENABLED )
    return make_error_Result(
      'ERR_API_INTERNAL: This promotion is disabled.',
      array(
        'error'      => 'ERR_API_INTERNAL: This promotion is disabled.',
        'error_code' => 'VV0017',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_available' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  // is the promotion expired?
  if ( $result->data_array[0]->is_expired )
    return make_error_Result(
      'ERR_API_INTERNAL: This promotion is expired.',
      array(
        'error'      => 'ERR_API_INTERNAL: This promotion is expired.',
        'error_code' => 'VV0017',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_available' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  // is the promotion configured correctly?
  if ( ! $result->data_array[0]->PROMO_ACTION )
    return make_error_Result(
      'ERR_API_INTERNAL: promotion configuration error. (2)',
      array(
        'error'      => 'ERR_API_INTERNAL: promotion configuration error. (2)',
        'error_code' => 'VV0017',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'promo_not_available' ),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  return make_ok_Result( (array) $result->data_array[0] );
}

/**
 * logTriggerShortCodeResult
 *
 * Adds a new row to ULTRA.PROMO_BROADCAST_ATTEMPT_LOG
 *
 * @param string $shortcode Shortcode (aka. keyword)
 * @param integer $customer_id Customer's identifier
 * @param string $result Attempt outcome
 * @return NULL
 */
function logTriggerShortCodeResult( $shortcode , $customer_id , $result )
{
  $addResult = \add_to_ultra_promo_broadcast_attempt_log(
    array(
      'customer_id'     => $customer_id,
      'promo_shortcode' => $shortcode,
      'result'          => $result
    )
  );

  dlog('',"insert into ULTRA.PROMO_BROADCAST_ATTEMPT_LOG ".( $addResult->is_failure() ? 'failed' : 'succeeded' ));
}

/**
 * ensureSubscriberActive
 *
 * get customer's plan_state without caching and ensure that plan is active
 */
function ensureSubscriberActive($customer)
{
  $result = NULL;
  $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer->CUSTOMER_ID, 'plan_state', 0);
  if ($plan_state != STATE_ACTIVE)
  {
    dlog('', 'ERROR: subscriber %d state is %s, expected %s', $customer->CUSTOMER_ID, $plan_state, STATE_ACTIVE);
    $result = make_error_Result(
      'ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command',
      array(
        'error'        => 'ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command',
        'error_code'   => 'IN0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'promo_redeem_inactive'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * ensureSubscriberSuspended
 *
 * get customer's plan_state without caching and ensure that plan is suspended
 */
function ensureSubscriberSuspended($customer)
{
  $result = NULL;
  $plan_state = \Ultra\Lib\DB\Getter\getScalar('customer_id', $customer->CUSTOMER_ID, 'plan_state', 0);
  if ($plan_state != STATE_SUSPENDED)
  {
    dlog('', 'ERROR: subscriber %d state is %s, expected %s', $customer->CUSTOMER_ID, $plan_state, STATE_SUSPENDED);
    $result = make_error_Result(
      'ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command',
      array(
        'error'        => 'ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command',
        'error_code'   => 'IN0001',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'promo_not_qualify'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * ensureNotAutoRenewed
 *
 * check that subscriber does not have auto renewal already
 */
function ensureNotAutoRenewed($customer)
{
  $result = NULL;
  if ($customer->monthly_cc_renewal)
  {
    dlog('', 'ERROR: subscriber %d already has monthly_cc_renewal enabled', $customer->CUSTOMER_ID);
    $result = make_error_Result(
      'ERR_API_INTERNAL: You do not qualify for this promotion',
      array(
        'error'        => 'You do not qualify for this promotion',
        'error_code'   => 'EL0002',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'promo_not_qualify'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * ensureHasCreditCard
 *
 * check that subscriber has credit card is on file
 */
function ensureHasCreditCard($customer)
{
  $result = NULL;
  if ( ! customer_has_credit_card($customer))
  {
    dlog('', 'ERROR: subscriber %d does not have CC on file', $customer->CUSTOMER_ID);
    $result = make_error_Result(
      'ERR_API_INVALID_ARGUMENTS: No credit card on file',
      array(
        'error'        => 'No credit card on file',
        'error_code'   => 'CC0005',
        'user_error'   => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'promo_missing_cc'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * enableAutoRecharge
 *
 * compose and execute monthly renewal UPDATE query
 */
function enableAutoRecharge($customer)
{
  $result = NULL;
  $update_query = htt_customers_overlay_ultra_update_query(array('customer_id' => $customer->CUSTOMER_ID, 'monthly_cc_renewal' => 1));
  if ( ! run_sql_and_check($update_query))
  {
    dlog('', 'ERROR: failed DB update');
    $result = make_error_Result(
      'ERR_API_INTERNAL: DB error (0)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (0)',
        'error_code' => 'DB0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'system_error'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * addWalletBalance
 *
 * add ACCOUNTS.BALANCE
 */
function addWalletBalance($customer, $value)
{
  $result = NULL;
  if ( ! is_mssql_successful(logged_mssql_query(accounts_update_query(
    array(
      'customer_id' => $customer->customer_id,
      'add_to_balance' => $value / 100)))))
  {
    dlog('', 'ERROR: failed DB update');
    $result = make_error_Result(
      'ERR_API_INTERNAL: DB error (1)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (1)',
        'error_code' => 'DB0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'system_error'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * addStoredValue
 *
 * add HTT_CUSTOMERS_OVERLAY_ULTRA.STORED_VALUE
 */
function addStoredValue($customer, $value)
{
  $result = NULL;
  if ( ! is_mssql_successful(logged_mssql_query(htt_customers_overlay_ultra_update_query(
    array(
      'balance_to_add' => ( $value / 100 ),
      'customer_id'    => $customer->CUSTOMER_ID)))))
  {
    dlog('', 'ERROR: failed DB update');
    $result = make_error_Result(
      'ERR_API_INTERNAL: DB error (2)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (2)',
        'error_code' => 'DB0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'system_error'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  return $result;
}

/**
 * add4gData
 *
 * add data on MVNE to subscriber's account
 */
function add4gData($customer, $value)
{
  $result = NULL;

  $arrayResult = mvneMakeitsoUpgradePlan(
    create_guid('PROMOCAMPAIGN'),
    $customer->CUSTOMER_ID,
    \Ultra\MvneConfig\getDataAddOnForMakeitsoUpgradePlan( $customer , [ 'MB' => $value ] ),
    $customer
  );

  dlog('',"mvneMakeitsoUpgradePlan arrayResult = %s",$arrayResult);

  if ( ! $arrayResult['success'])
    $result = make_error_Result(
      $arrayResult['errors'][0],
      array(
        'error'      => $arrayResult['errors'][0],
        'error_code' => 'MW0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ) ,
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  return $result;
}

/**
 * addRoam
 *
 * add ROAM on MVNE to subscriber's account
 */
function addRoam($customer, $value)
{
  $result = NULL;

  $arrayResult = mvneMakeitsoUpgradePlan(
    create_guid('PROMOCAMPAIGN'),
    $customer->CUSTOMER_ID,
    "ROAM|$value",
    $customer
  );

  dlog('',"mvneMakeitsoUpgradePlan arrayResult = %s",$arrayResult);

  if ( ! $arrayResult['success'])
    $result = make_error_Result(
      $arrayResult['errors'][0],
      array(
        'error'      => $arrayResult['errors'][0],
        'error_code' => 'MW0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array( 'message_type' => 'system_error' ) ,
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );

  return $result;
}

/**
 * chargeCreditCardReactivation
 *
 * compute amount need to reactivate the subscriber and charge credit card on file (which will also attempt to reactivate)
 */
function chargeCreditCardReactivation($customer)
{
  // compute plan costs including bolt ons
  $costs = get_plan_costs_by_customer_id($customer->CUSTOMER_ID);
  if ( ! $costs['plan'])
  {
    dlog('', 'ERROR: failed to get subscriber %d plan costs: %s', $customer->CUSTOMER_ID, $costs);
    return make_error_Result(
      'ERR_API_INTERNAL: DB error (3)',
      array(
        'error'      => 'ERR_API_INTERNAL: DB error (3)',
        'error_code' => 'DB0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'system_error'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }
  $amount = $costs['plan'] + $costs['bolt_ons']; // in dollars

  // charge customer's credit card and attempt to re-activate
  $trans = func_add_funds_by_tokenized_cc(
    array(
      'customer'            => $customer,
      'charge_amount'       => $amount,
      'detail'              => __FUNCTION__,
      'include_taxes_fees'  => TRUE,
      'description'         => 'Wallet Balance Added Using Credit Card',
      'reason'              => 'ADD_BALANCE',
      'session'             => create_guid('PROMOCAMPAIGN'),
      'fund_destination'    => 'stored_value'));
  if ($trans->is_failure())
  {
    dlog('', 'ERROR: failed CC transation with %s', $trans->get_errors());
    return make_error_Result(
      'ERR_API_INTERNAL: credit card transaction not successful',
      array(
        'error'      => 'ERR_API_INTERNAL: credit card transaction not successful',
        'error_code' => 'CC0001',
        'user_error' => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'system_error'),
          $customer->preferred_language,
          $customer->BRAND_ID
        )
      )
    );
  }

  // successfull
  return NULL;
}

/**
 * logBillingHistory
 *
 * add entry in HTT_BILLING_HISTORY on all balance changes not already logged
 */
function logBillingHistory($customer, $stored_value, $wallet_value, $reference, $detail, $description)
{
  if ( ! is_mssql_successful(
    logged_mssql_query(
      htt_billing_history_insert_query(
        array(
          'customer_id'            => $customer->CUSTOMER_ID,
          'date'                   => 'now',
          'cos_id'                 => $customer->COS_ID,
          'entry_type'             => 'PROMO',
          'stored_value_change'    => $stored_value / 100,
          'balance_change'         => $wallet_value / 100,
          'package_balance_change' => 0,
          'charge_amount'          => 0,
          'reference'              => $reference . '_' . $customer->CUSTOMER_ID . '_' . time(),
          'reference_source'       => get_reference_source('PROMO'),
          'detail'                 => substr($detail, strrpos($detail, '\\') + 1), // trim off namespace
          'description'            => $description,
          'result'                 => 'COMPLETE',
          'source'                 => 'PROMO SMS',
          'is_commissionable'      => 0,
          'terminal_id'            => '0')))))
    dlog('', 'ERROR: failed to log into HTT_BILLING_HISTORY');
}

