<?php

/*

All functions must take 2 array parameters.
The first one contains customer data, the second one is optional.
All functions must return an \Outcome Object.

*/

namespace Ultra\Lib\StateMachine\Requirement;

include_once('lib/partner-validate.php');
include_once('Ultra/Lib/DB/Setter/Customer.php');

/**
 * test
 *
 * Does nothing, used for testing
 *
 * @return \Outcome Object
 */
function test( array $customerData , $params=NULL )
{
  dlog('',"params = %s",$params);

  $outcome = new \Outcome;

  $outcome->succeed();

  return $outcome;
}

/**
 * validateCustomerId
 *
 * this function will validate the customer id in the $customerData array
 *
 * @param array $customerData 
 * @param array $params
 * @return /Outcome object
 */
function validateCustomerId( array $customerData, $params=NULL )
{
  dlog('',"params = %s",$params);

  $outcome = new \Outcome;

  if ( ! isset( $customerData['customer_id'] ) )
    $outcome->add_errors_and_code('ERR: No customer ID available ', 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkWholeValueMin
 *
 * this function will check the whole value minimum by evaluating if the balance + stored_value is less than the value passed in the $params array
 *
 * @param array $customerData balance, stored_value
 * @param array $params 
 * @return /Outcome object
 */
function checkWholeValueMin( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  // check for line credits if FLEX plan
  if (isset($params['to_plan']) && \Ultra\UltraConfig\isFlexPlan($params['to_plan']))
  {
    // $lineCredits = new \LineCredits();
    // $hasLineCredit = $lineCredits->hasLineCredit($customerData['customer_id']);

    // teldata_change_db();

    // if ($hasLineCredit)
    // {
      $outcome->succeed();
      return $outcome;
    // }
  }

  if ( $customerData['balance'] + $customerData['stored_value'] < $params['value'] )
    $outcome->add_errors_and_code('PREREQUISITE stop: whole_value_min ('.$customerData['balance'].' + '.$customerData['stored_value'].','.$params['value'].')', 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkMintChangePlanDue
 *
 * this function will check the whole value minimum by evaluating if the balance + stored_value is less than the value passed in the $params array
 *
 * @param array $customerData balance, stored_value
 * @param array $params
 * @return /Outcome object
 */
function checkMintPlanDeltaDue( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  $result = \calculateMintPlanDeltaCost($customerData['customer_id'], $params['from_plan'], $params['to_plan']);

  if (count($errors = $result->get_errors()))
  {
    \logError($errors[0]);
    $outcome->add_error($errors[0]);
    return $outcome;
  }

  $amountOwed = $result->get_data_key('amount_owed');

  if ( $customerData['balance'] + $customerData['stored_value'] < $amountOwed / 100 )
  {
    dlog('', 'PREREQUISITE (checkMintPlanDeltaDue) failed: not enough money to change plan (Balance: ' . $customerData['balance'] . ' + Stored Value: ' . $customerData['stored_value'] . ' is less than ' . $amountOwed . ')');
    $outcome->add_errors_and_code('PREREQUISITE stop: mint_plan_delta ('.$customerData['balance'].' + '.$customerData['stored_value'].','.$amountOwed.')', 'ERROR001');
  }
  else
  {
    $outcome->succeed();
  }

  return $outcome;
}

/**
 * checkWholeValueUnder
 *
 * this function will check the whole value by evaluating if the balance + stored_value is greater than or equal to the value passed in the $params array
 *
 * @param array $customerData balance, stored_value
 * @param array $params 
 * @return /Outcome object
 */
function checkWholeValueUnder( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  if ( $customerData['balance'] + $customerData['stored_value'] >= $params['value'] )
    $outcome->add_errors_and_code('PREREQUISITE stop: whole_value_under ('.$customerData['balance'].' + '.$customerData['stored_value'].','.$params['value'].')', 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkBalanceMin
 *
 * this function will check the balance minimum by evaluating if the balance is greater or equal to the value passed in the $params array
 *
 * @param array $customerData balance, stored_value
 * @param array $params 
 * @return /Outcome object
 */
function checkBalanceMin( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  if ( $customerData['balance'] >= $params['value'] )
    $outcome->add_errors_and_code('PREREQUISITE stop: balance_min ('.$customerData['balance'].','.$params['value'].')', 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkPlanRenewalDue
 *
 * this function will check if plan_expired is set in $customerData
 *
 * @param array $customerData plan_expired
 * @param array $params 
 * @return /Outcome object
 */
function checkPlanRenewalDue( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  // if ( ! $customerData['plan_expired'] )
  //   $outcome->add_errors_and_code('PREREQUISITE stop: plan_renewal_due ('.$customerData['plan_expired'].')', 'ERROR001');
  if ( strtotime(date('Y-m-d', strtotime($customerData['plan_expires']))) > time() )
    $outcome->add_errors_and_code('PREREQUISITE stop: plan_renewal_due ('.$customerData['plan_expires'].')', 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkShippingAddress
 *
 * this function will check if if customer has a shipping address
 *
 * @param array $customerData 
 * @param array $params 
 * @return /Outcome object
 */
function checkShippingAddress( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  $address_array = array(
    'customer_id'  => $customerData['customer_id'],
    'address1'     => $customerData['address1'],
    'address2'     => $customerData['address2'],
    'city'         => $customerData['city'],
    'state_region' => $customerData['state_region'],
    'postal_code'  => $customerData['postal_code'],
    'country'      => $customerData['country'],
  );

  $validate_result = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $address_array );
  list($errors, $error_codes, $cc_val, $avs_val) = $validate_result;
  list( $first_name_errors , $first_name_error_codes ) = \Ultra\Lib\Util\validatorName( 'first_name', $customerData['first_name'] );
  list( $last_name_errors  , $last_name_error_codes  ) = \Ultra\Lib\Util\validatorName( 'last_name',  $customerData['last_name']) ;
  
  if ( isset( $first_name_errors ) )
  {
    $errors[] = $first_name_errors;
    $error_codes[] = $first_name_error_codes;
  }
  
  if ( isset( $last_name_errors ) )
  {  
    $errors[] = $last_name_errors;
    $error_codes[] = $last_name_error_codes;
  }

  if ( count( $errors ) > 0 && count( $error_codes > 0 ) )
  {
    for ( $i = 0; $i < count( $errors ); $i++ )
    {
       $outcome->add_errors_and_code( $errors[$i], $error_codes[$i] );
    }
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkShippingAddress
 *
 * this function will check validate_last_shipped_days_ago_min by passing customer_id and $para['last_shipped_days_ago_min']
 *
 * @param array $customerData 
 * @param array $params 
 * @return /Outcome object
 */
function validateLastShippedDaysAgoMin( array $customerData, $params=NULL )
{  
  $outcome = new \Outcome;
  
  if ( ! validate_last_shipped_days_ago_min( $customerData['customer_id'] , $params['value'] ) )
    $outcome->add_errors_and_code('PREREQUISITE stop: last_shipped_days_ago_min', 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * validateSimShipmentsMax
 *
 * this function will check validate_sim_shipments_max by passing customer_id and $para['sim_shipments_max']
 *
 * @param array $customerData 
 * @param array $params 
 * @return /Outcome object
 */
function validateSimShipmentsMax( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  if ( ! validate_sim_shipments_max( $customerData['customer_id'] , $params['value'] ) )
    $outcome->add_errors_and_code("PREREQUISITE stop: sim_shipments_max", 'ERROR001');
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checkCredentialFalse
 *
 * this function will check if specified credential does not exist
 *
 * @param array $customerData 
 * @param array $params 
 * @return /Outcome object
 */
function checkCredentialFalse( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  if ( ! find_credential($params['value']) )
    $outcome->succeed();
  else
    $outcome->add_errors_and_code('Checking if credential is false: Failed credential '.$params['value'].' exists', 'ERROR001');

  return $outcome;
}

/**
 * checkCredentialTrue
 *
 * this function will check if specified credential exists
 *
 * @param array $customerData 
 * @param array $params 
 * @return /Outcome object
 */
function checkCredentialTrue( array $customerData, $params=NULL )
{
  $outcome = new \Outcome;

  if ( find_credential($customerData[$params['value']]) )
    $outcome->succeed();
  else
    $outcome->add_errors_and_code('Checking if credential is true: credential '.$params['value'].' does not exists', 'ERROR001');

  return $outcome;
}

