<?php

require_once 'db.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';
include_once('classes/Outcome.php'); 

class TestStateMachineRequirement
{
  public $customerData = array(
  'customer_id'                      => '104',
  'cos_id'                           => '98274',
  'plan_state'                       => 'ACTIVE',
  'activation_iccid_full'            => '1001110101010110005',
  'current_iccid_full'               => '1001110101010110005',
  'current_mobile_number'            => '3107948239',
  'stored_value'                     => '50',
  'balance'                          => '50',
  'packaged_balance1'                => '50',
  'packaged_balance2'                => '50',
  'mvne'                             => '21234',
  'customer_source'                  => '',
  'preferred_language'               => 'EN',
  'plan_started'                     => '2015-03-18',
  'plan_expires'                     => '2015-03-18',
  'plan_expired'                     => '0',
  'monthly_cc_renewal'               => '1',
  'tos_accepted'                     => '1',
  'monthly_renewal_target'           => '2015-03-18',
  'monthly_renewal_target_requested' => '1',
  'cancel_requested'                 => '0',
  'easypay_activated'                => '1',
  'account'                          => '1348785762',
  'login_name'                       => '1348785762',
  'address1'                         => '14222 Barber st',
  'address2'                         => '',
  'city'                             => 'Westminster',
  'state_or_region'                  => 'CA',
  'postal_code'                      => 'abc',
  'country'                          => 'USA',
  'account_first_name'               => 'chad',
  'account_last_name'                => '123'

  );
}

class Test_validateCustomerId extends TestStateMachineRequirement
{
  function test()
  {
     $result =  \Ultra\Lib\StateMachine\Requirement\validateCustomerId( $this->customerData );
     print_r($result);
  }
}

class Test_checkWholeValueMin extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\checkWholeValueMin( $this->customerData, $params );
     print_r($result);
  }
}

class Test_checkWholeValueUnder extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\checkWholeValueUnder( $this->customerData, $params );
     print_r($result);
  }
}

class Test_checkBalanceMin extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\checkBalanceMin( $this->customerData, $params );
     print_r($result);
  }
}

class Test_checkPlanRenewalDue extends TestStateMachineRequirement
{
  function test()
  {
     $result =  \Ultra\Lib\StateMachine\Requirement\checkPlanRenewalDue( $this->customerData );
     print_r($result);
  }
}

class Test_checkShippingAddress extends TestStateMachineRequirement
{
  function test()
  {
     $result =  \Ultra\Lib\StateMachine\Requirement\checkShippingAddress( $this->customerData );
     print_r($result);
  }
}

class Test_validateLastShippedDaysAgoMin extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\validateLastShippedDaysAgoMin( $this->customerData, $params );
     print_r($result);
  }
}

class Test_validateSimShipmentsMax extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\validateSimShipmentsMax( $this->customerData, $params );
     print_r($result);
  }
}

class Test_checkCredentialFalse extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\checkCredentialFalse( $this->customerData, $params );
     print_r($result);
  }
}

class Test_checkCredentialTrue extends TestStateMachineRequirement
{
  function test()
  {
     global $argv;
     $params['value'] = $argv[2];
     $result =  \Ultra\Lib\StateMachine\Requirement\checkCredentialTrue( $this->customerData, $params );
     print_r($result);
  }
}



$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$testObject = new $testClass();
$result = $testObject->test();