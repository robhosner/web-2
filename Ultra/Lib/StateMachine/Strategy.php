<?php

namespace Ultra\Lib\StateMachine;

interface Strategy
{
  public function getCOSIDByPlanName( $planName );
  public function getRequirementFunction( $function );
  public function getActionFunction( $function );
  public function getBrandConfig( $brandID );
}
