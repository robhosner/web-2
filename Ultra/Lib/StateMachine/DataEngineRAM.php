<?php

namespace Ultra\Lib\StateMachine;

require_once 'Ultra/Lib/StateMachine/DataEngineInterface.php';

class DataEngineRAM implements DataEngineInterface
{
  public $currentTransitionData;
  private $currentActionData;

  /**
   * storeTransitionAndActions
   *
   * Store Transition and Actions in RAM
   *
   * @return \Outcome Object
   */
  public function storeTransitionAndActions()
  {
    $outcome = new \Outcome;

    // nothing to do: $this->currentTransitionData is in RAM

    $this->storeActions();

    return $outcome;
  }

  /**
   * storeActions
   *
   * Store Actions in RAM
   *
   * @return \Outcome Object
   */
  private function storeActions()
  {
    $outcome = new \Outcome;

    $actions = array();

    if ( empty( $this->currentTransitionData ) )
      return make_error_Outcome( 'No transition data available' , '------' );

    if ( empty( $this->currentTransitionData['actions'] ) )
      return make_error_Outcome( 'No action data available' , '------' );

    $n = count( $this->currentTransitionData['actions'] );

    // loop though all actions
    for ( $i = 0 ; $i < $n ; $i++ )
      $actions[] = array(
        'transition_uuid' => $this->currentTransitionData['transition_uuid'],
        'action_uuid'     => \getNewActionUUID('action ' . time()),
        'status'          => STATUS_OPEN,
        'action_seq'      => ( $i + 1 ),
        'action_type'     => 'DEFAULT',
        'action_name'     => $this->currentTransitionData['actions'][ $i ],
        'action_result'   => NULL,
        'created'         => time(),
        'closed'          => NULL,
        'pending_since'   => NULL // TODO: when do we update this?
      );

    $this->currentTransitionData['actions'] = $actions;

    dlog('',"actions    = %s",$actions);
    dlog('',"transition = %s",$this->currentTransitionData);

    return $outcome;
  }

  /**
   * loadTransitionAndActionsByUUID
   *
   * Return Transition and Actions from RAM
   *
   * @return \Outcome Object
   */
  public function loadTransitionAndActionsByUUID( $transition_uuid )
  {
    $outcome = new \Outcome();

    // nothing to do: $this->currentTransitionData is in RAM

    $outcome->succeed();

    return $outcome;
  }

  /**
   * loadNextTransitionAndActions
   *
   * Return Transition and Actions from RAM
   *
   * @return \Outcome Object
   */
  public function loadNextTransitionAndActions()
  {
    $outcome = new \Outcome();

    // nothing to do: $this->currentTransitionData is in RAM

    $outcome->succeed();

    return $outcome;
  }

  public function recordActionOutcome( $outcome )
  {
// TODO

    return NULL;
  }

  public function abortTransitionAndActions()
  {
    $this->currentActionData['status']     = STATUS_ABORTED;
    $this->currentTransitionData['status'] = STATUS_ABORTED;

// TODO

    return ''; // no errors
  }

  /**
   * recordActionPending
   *
   * Take note of timestamp at the beginning of the current action
   *
   * @return NULL
   */
  public function recordActionPending()
  {
    $this->currentActionData['pending_since'] = time();
    $this->currentTransitionData['actions'][ $this->currentActionData['action_seq'] - 1 ]['pending_since'] = time();

    return NULL;
  }

  /**
   * closeAction
   *
   * Close an action, given a specific $action_uuid
   *
   * @return NULL
   */
  public function closeAction( $action_uuid )
  {
    // loop though all actions
    for ( $i = 0 ; $i < $n ; $i++ )
      if ( $this->currentTransitionData['actions'][ $i ]['action_uuid'] === $action_uuid )
      {
        $this->currentTransitionData['actions'][ $i ]['status'] = STATUS_CLOSED;
        $this->currentTransitionData['actions'][ $i ]['closed'] = time();
      }

    return NULL;
  }

  /**
   * closeTransition
   *
   * @return \Outcome Object
   */
  public function closeTransition()
  {

// TODO

    return ''; // no errors
  }

  /**
   * updatePlanState
   *
   * Update plan_state value
   * 
   * @return string
   */
  public function updatePlanState( $plan_state , $customer_id )
  {
    $error = '';

//TODO:

    return $error;
  }

  /**
   * getNextActionData
   *
   * Selects data associated with the next Action
   *
   * @return array
   */
  public function getNextActionData()
  {
    $nextActionSeq = ( empty( $this->currentActionData ) ) ? 0 : $this->currentActionData['action_seq'] ;

    dlog('',"nextActionSeq = %s",$nextActionSeq);

    // are there still actions left?
    if ( empty( $this->currentTransitionData['actions'][ $nextActionSeq ] ) )
      return NULL;

    dlog('',"status = %s",$this->currentTransitionData['actions'][ $nextActionSeq ]['status']);

    $this->currentActionData = $this->currentTransitionData['actions'][ $nextActionSeq ];

    // sanity check
    if ( $this->currentActionData['status'] == STATUS_CLOSED )
    {
      dlog('',"Action ".$this->currentActionData['action_uuid']." already closed");
      return $this->getNextActionData();
    }

    // sanity check
    if ( $this->currentActionData['status'] != STATUS_OPEN )
    {
      dlog('',"Cannot continue: Action ".$this->currentActionData['action_uuid']." is ".$this->currentActionData['status']);
      return NULL;
    }

    return $this->currentActionData;
  }

  /**
   * resetCurrentActionData
   *
   * Clears the current action data
   *
   * @return null
   */
  public function resetCurrentActionData()
  {
    $this->currentActionData = array();
  }
}

