<?php


namespace Ultra\Lib\StateMachine;

function array_change_key_case_recursive($arr)
{
    return array_map(function($item){
        if(is_array($item))
            $item = array_change_key_case_recursive($item);
        return $item;
    },array_change_key_case($arr));
}

require_once 'Ultra/Lib/StateMachine/DataEngineInterface.php';

class DataEngineMSSQL implements DataEngineInterface
{
  public $currentTransitionData;
  private $currentActionData;

  /**
   * storeTransitionAndActions
   *
   * Store Transition and Actions in MSSQL
   *
   * @return \Outcome Object
   */
  public function storeTransitionAndActions($context = NULL)
  {
    $outcome = new \Outcome;

    $actions = array();

    if ( empty( $this->currentTransitionData ) )
      return make_error_Outcome( 'No transition data available', '------' );

    if ( empty( $this->currentTransitionData['actions'] ) )
      return make_error_Outcome( 'No action data available', '------' );

    $temp = $this->currentTransitionData;

    if ( ! $context)
      $context = array('customer_id' => $temp['customer_id']);

    // log transition to DB
    $logTransitionSuccess = \log_transition(
      $temp['transition_uuid'],
      $context,
      $temp['from_cos_id'],
      $temp['from_plan_state'],
      $temp['to_cos_id'],
      $temp['to_plan_state'],
      $temp['transition_label'],
      $temp['priority'],
      $temp['htt_environment']
    );

    // log_transition reserves transition and customer_id
    if ($logTransitionSuccess)
      unreserve_transition_uuid_by_pid( $temp['transition_uuid'], NULL, $temp['customer_id'] );

    $this->applyActionDetails();

    dlog('', "transition = %s", $this->currentTransitionData);

    return $outcome;
  }

  public function applyActionDetails()
  {
    $baseActionData = array(
      'transition_uuid' => $this->currentTransitionData['transition_uuid'],
      'status'          => STATUS_OPEN,
      'action_result'   => NULL,
      'created'         => time(),
      'closed'          => NULL,
      'pending_since'   => NULL
    );

    for ( $i = 0 ; $i < count( $this->currentTransitionData['actions'] ); $i++ )
    {
      $actions[] = array_merge($baseActionData, array(
        'action_uuid' => \getNewActionUUID('action ' . time()),
        'action_seq'  => ( $i + 1 ),
        'action_type' => 'DEFAULT',
        'action_name' => $this->currentTransitionData['actions'][ $i ]
      ));

    }

    $this->currentTransitionData['actions'] = $actions;

  }

  /**
   * loadTransitionAndActionsByUUID
   *
   * Load Transition and Actions from MSSQL using the unique identifier
   *
   * @return \Outcome Object
   */
  public function loadTransitionAndActionsByUUID( $transition_uuid )
  {
    $this->currentTransitionData = NULL;

    $outcome = new \Outcome(NULL, TRUE);

    $transition = \get_transition_by_uuid($transition_uuid);

    if ($transition && ! empty($transition->TRANSITION_UUID))
    {
      $this->currentTransitionData = \array_change_key_case_recursive((array)$transition);
      $this->currentTransitionData['context'] = disclose_JSON($this->currentTransitionData['context']);
    }
    else
      $outcome->add_errors_and_code('ERR_API_INTERNAL: no data found', 'ND0001');

    return $outcome;
  }

  /**
   * loadNextTransitionAndActions
   *
   * Load data for the next transition which should be resolved
   *
   * @return \Outcome Object
   */
  public function loadNextTransitionAndActions()
  {
    $this->currentTransitionData = NULL;

    $outcome = new \Outcome(NULL, TRUE);

    $transition = get_next_transition(NULL, \Ultra\UltraConfig\getEnvironment());
    
    if ($transition && ! empty($transition->TRANSITION_UUID))
    {
      $this->currentTransitionData = \array_change_key_case_recursive((array)$transition);
      $this->currentTransitionData['context'] = disclose_JSON($this->currentTransitionData['context']);
    }
    else
      $outcome->add_errors_and_code('ERR_API_INTERNAL: no data found', 'ND0001');

    return $outcome;
  }

  /**
   * recordActionPending
   *
   * Take note of timestamp at the beginning of the current action
   *
   * @return NULL
   */
  public function recordActionPending()
  {
    $this->currentActionData['pending_since'] = time();
    $this->currentTransitionData['actions'][ $this->currentActionData['action_seq'] - 1 ]['pending_since'] = time();

    return NULL;
  }

  /**
   * recordActionOutcome 
   *
   * records the $outcome of action to private member: currentActionData
   * 
   * @param  array $outcome array
   * @return NULL
   */
  public function recordActionOutcome( $outcome )
  {
    $this->currentActionData['action_result'] = json_encode($outcome->data_array);

    return NULL;
  }


  /**
   * updatePlanState
   *
   * Update plan_state value
   * 
   * @return string
   */
  public function updatePlanState( $plan_state , $customer_id )
  {
    $error = '';

    $success = save_state(
      $this->currentTransitionData['customer_id'],
      $this->currentTransitionData['to_cos_id'],
      $this->currentTransitionData['to_plan_state'],
      $this->currentTransitionData['from_cos_id'],
      $this->currentTransitionData['from_plan_state'],
      $this->currentTransitionData['transition_label'],
      $this->currentTransitionData['transition_uuid']
    );

    if ( ! $success )
      $error = 'Error updating plan state';


    return $error;
  }

  /**
   * abortTransitionAndActions
   *
   * aborts transition and all actions related
   * 
   * @return string
   */
  public function abortTransitionAndActions()
  {
    $error = '';

    $this->currentTransitionData['status'] = STATUS_ABORTED;

    if ( !\abort_transition($this->currentTransitionData['transition_uuid']) )
    {
      $error = 'Failed to abort transition UUID = ' . $this->currentTransitionData['transition_uuid'];
      \logError($error);
    }
    
    $logActionResult = \log_action(
      $this->currentTransitionData['transition_uuid'],
      $this->currentActionData['action_uuid'],
      array(
        'seq'  => $this->currentActionData['action_seq'],
        'type' => $this->currentActionData['action_type'],
        'name' => $this->currentActionData['action_name']
      ),
      NULL,
      STATUS_ABORTED
    );

      if ( ! $logActionResult)
      {
        $error = 'Failed to abort action UUID = ' . $action['action_uuid'];
        \logError($error);
      }

    return $error;
  }

  /**
   * closeAction 
   *
   * closes current action in private member: currentActionData
   * or closes action with uuid $action_uuid
   * 
   * @param  string $action_uuid optional
   * @return NULL
   */
  public function closeAction( $action_uuid = NULL )
  {
    if ($action_uuid !== NULL)
      \close_action($action_uuid, STATUS_CLOSED);
    else
    {
      $this->currentActionData['status'] = STATUS_CLOSED;
      \close_action(
        $this->currentActionData['action_uuid'],
        $this->currentActionData['status'],
        $this->currentActionData['action_result']
      );
    }

    return NULL;
  }

  /**
   * closeTransition 
   *
   * closes the transition stored in public member: currentTransitionData
   * 
   * @return string
   */
  public function closeTransition()
  {
    $error = '';

    $this->currentTransitionData['status'] = STATUS_CLOSED;

    if ( !\close_transition($this->currentTransitionData['transition_uuid']) )
    {
      $error = 'Failed to close transition UUID = ' . $this->currentTransitionData['transition_uuid'];
      \logError($error);
    }

    return $error;
  }

  /**
   * getNextActionData 
   *
   * returns the next action for transition in public member: currentTransitionData
   * set private member, currentActionData, to next action
   * 
   * @return array $actionData
   */
  public function getNextActionData()
  {
    $actionData = NULL;

    if (empty($this->currentActionData) && ! empty($this->currentTransitionData['actions']))
    {
      $actionData = $this->currentTransitionData['actions'][0];
    }
    else
    {
      $seqIndex = $this->currentActionData['action_seq'];
      if ($seqIndex < count($this->currentTransitionData['actions']))
        $actionData = $this->currentTransitionData['actions'][$seqIndex];
    }

    $this->currentActionData = ($actionData) ? (array)$actionData : NULL;

    return $actionData;
  }

  /**
   * getCurrentActionData 
   * @return [type] [description]
   */
  public function getCurrentActionData()
  {
    return $this->currentActionData;
  }

  /**
   * resetCurrentActionData
   *
   * Clears the current action data
   *
   * @return null
   */
  public function resetCurrentActionData()
  {
    $this->currentActionData = array();
  }
}
