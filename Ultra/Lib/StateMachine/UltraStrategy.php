<?php

namespace Ultra\Lib\StateMachine;

require_once 'Ultra/Lib/StateMachine/Strategy.php';

class UltraStrategy implements \Ultra\Lib\StateMachine\Strategy
{
  /**
   * getCOSIDByPlanName
   *
   * Returns the cos_id for a plan
   *
   * @return string
   */
  public function getCOSIDByPlanName( $planName )
  {
    return \get_cos_id_from_plan( $planName );
  }

  /**
   * getRequirementFunction
   *
   * Returns the namespaced function for a transition requirement
   *
   * @return string
   */
  public function getRequirementFunction( $function )
  {
    return '\Ultra\Lib\StateMachine\Requirement\\' . $function;
  }

  /**
   * getActionFunction
   *
   * Returns the namespaced function for a transition action
   *
   * @return string
   */
  public function getActionFunction( $function )
  {
    return '\Ultra\Lib\StateMachine\Action\\' . $function;
  }

  /**
   * getBrandConfig
   *
   * Returns state machine config file for a given brand ID
   *
   * @return string
   */
  public function getBrandConfig( $brandID )
  {
    $brandConfig = \Ultra\UltraConfig\getBrandFromBrandId( $brandID );
    if ( empty( $brandConfig ) || empty( $brandConfig['short_name'] ) )
    {
      \logError( 'Failed to find brand config where brand_id = ' . $brandID );
      return null;
    }
    
    $brandShort  = strtolower( $brandConfig['short_name'] );
    return "$brandShort.json";
  }
}
