package Ultra::Lib::Aspider::Prober;


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Config;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::WSDL11;


use base qw(Ultra::Lib::Amdocs::Base);


=head1 NAME

Ultra::Lib::Aspider::Prober

=head1 SYNOPSIS

  my $aspiderProber = Ultra::Lib::Aspider::Prober->new();

  $requestResult = $aspiderProber->probeCommand( $command , $parameters );

=cut


=head4 _initialize

  Specific initialization on object instantiation.

=cut
sub _initialize
{
  my ($this, $data) = @_;

  $this->SUPER::_initialize($data);

  $data->{ LOG_ENABLED } = 1;
  $data->{ CONFIG }      ||= Ultra::Lib::Util::Config->new();

#  $data->{ WSDL } = XML::Compile::WSDL11->new( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20121012/MVNOWSAPIService.wsdl" );
#  $data->{ WSDL }->importDefinitions( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20121012/xsd0.xsd" );
#  $data->{ WSDL }->importDefinitions( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20121012/xsd1.xsd" );
#  $data->{ WSDL }->importDefinitions( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20121012/xsd2.xsd" );
#  $data->{ WSDL }->importDefinitions( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20121012/xsd3.xsd" );
  #$data->{ WSDL } = XML::Compile::WSDL11->new( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20130913/MVNOWSAPIService.wsdl" );
  #$data->{ WSDL } = XML::Compile::WSDL11->new( "/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPI-20130913/MVNOWSAPIServiceLocal.wsdl" );

  #$data->{ WSDL } = XML::Compile::WSDL11->new( "../runners/soapy/MVNOWSAPI-20130913/MVNOWSAPIService.wsdl" );
  #$data->{ WSDL } = XML::Compile::WSDL11->new( '../runners/soapy/'.$data->{ CONFIG }->find_credential('aspider/command/soap/wsdl') );

  my $wsdlFile = "../runners/soapy/MVNOWSAPI-20140113/MVNOWSAPIService.wsdl";

  print STDERR " _initialize wsdlFile = $wsdlFile\n";

  $data->{ WSDL } = XML::Compile::WSDL11->new( $wsdlFile );

  $data->{ WSDL }->compileCalls;

  $this->SUPER::_initialize($data);
}


=head4 probeCommand

  Performs a direct SOAP call to Aspider

=cut
sub probeCommand
{
  my ($this, $command , $params) = @_;

  my $return =
  {
    'outcome'      => '',
    'soapRequest'  => '',
    'soapResponse' => '',
  };

  my ($answer,$trace);

  eval
  {
    # we won't really need $answer in this module
    ($answer,$trace) = $this->{ WSDL }->call( $command => { parameters => $params } );
  };

  if ( $@ )
  {
    $return->{ 'outcome' } = $@;
  }
  elsif ( ! $trace )
  {
    $return->{ 'outcome' } = "Error: no trace available after $command SOAP call";
  }
  else
  {
    $return->{ 'soapRequest' }  = $this->prettyPrintXML( $trace->request()->content() );
    $return->{ 'soapResponse' } = $this->prettyPrintXML( $trace->response()->content() );
    $return->{ 'outcome' }      = $trace->response()->status_line();
  }

  return $return;
}


1;


__END__

