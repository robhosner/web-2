<?php

# Usage:
# php Ultra/Lib/MiddleWare/ACC/Control_test.php ChangeIMEI      $MSISDN $ICCID $IMEI $NEW_IMEI
# php Ultra/Lib/MiddleWare/ACC/Control_test.php CheckBalance    $MSISDN
# php Ultra/Lib/MiddleWare/ACC/Control_test.php QuerySubscriber $MSISDN
# php Ultra/Lib/MiddleWare/ACC/Control_test.php GetMVNEDetails  $MSISDN $ICCID
# php Ultra/Lib/MiddleWare/ACC/Control_test.php SendSMS         $MSISDN $TEXT
# php Ultra/Lib/MiddleWare/ACC/Control_test.php DeactivateSubscriber $MSISDN $ICCID $CUSTOMER_ID $IMEI
# php Ultra/Lib/MiddleWare/ACC/Control_test.php RenewPlan       
# php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures    $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $OPTION
# php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures    $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG RESET JSON_ENCODED_ARRAY
# php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeaturesRaw $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $B_VOICE $B_SMS $VOICEMAIL
# php Ultra/Lib/MiddleWare/ACC/Control_test.php PortIn $CUSTOMER_ID $MSISDN $ICCID_FULL $ZIPCODE $PLAN
# php Ultra/Lib/MiddleWare/ACC/Control_test.php PortInEligibility $MSISDN
# php Ultra/Lib/MiddleWare/ACC/Control_test.php CancelPortIn $MSISDN
# php Ultra/Lib/MiddleWare/ACC/Control_test.php cleanup_acc_sms_text
# php Ultra/Lib/MiddleWare/ACC/Control_test.php testcommand
# php Ultra/Lib/MiddleWare/ACC/Control_test.php CanActivate $ICCID
# php Ultra/Lib/MiddleWare/ACC/Control_test.php SuspendSubscriber $CUSTOMER_ID $ICCID $MSISDN

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';
require_once 'lib/util-runners.php';


/*
    $mwControlCommandBase = new \Ultra\Lib\MiddleWare\ACC\ControlCommandBase( 'test'.time() );

    $x = $mwControlCommandBase->addPlanDataOnActivation(
      array(
        'wholesalePlan'     => 'W-PRIMARY',
        'ultra_plan_name'   => 'TWENTY_NINE',
        'preferredLanguage' => 'en'
      )
    );

    print_r($x);

exit;
*/


$accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;


class Test_cleanup_acc_sms_text
{
  function test( $argv , $accMiddleware )
  {
    $testdata = array(
'a"b,c&d&ddddddddddd&aaaaaaaaaaaaaaa',
"''aaa,,",
'a.a',
'a&a',
'a<a',
'a>a',
'a	x
b
a',
'a""aa""a'
    );

    foreach ( $testdata as $txt )
      echo "TEXT : $txt\n".\Ultra\Lib\MiddleWare\ACC\cleanup_acc_sms_text( $txt )."\n\n";
  }
}


class Test_SendSMS
{
  function test( $argv , $accMiddleware )
  {
    $sms_text = 'Отличные новости от Ultra Mobile!';

$sms_text = 'test';

    $inboundReply = $accMiddleware->processControlCommand(
      array(
        'actionUUID' => time(),
        'command'    => 'SendSMS',
        'parameters' => array(
          'msisdn'             => '3103097855', // Vitaly's test phone @ NY
          'preferred_language' => 'en',
          'qualityOfService'   => '801',
          'sms_text'           => $sms_text
#'another test '.time()
        )
      )
    );

    echo 'inboundReply: ';
    print_r( $inboundReply );
  }
}


class Test_ChangeIMEI
{
  function test( $argv , $accMiddleware )
  {
    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'ChangeIMEI',
        'parameters' => array(
          'msisdn'    => $argv[2],
          'iccid'     => $argv[3],
          'imei'      => $argv[4],
          'new_imei'  => $argv[5]
        ),
        'uuid'       => 'test'.time()
      )
    );
    
    print_r( $inboundReply );
  }
}


class Test_DeactivateSubscriber
{
  function test( $argv , $accMiddleware )
  {
    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'DeactivateSubscriber',
        'parameters' => array(
          'msisdn'                => $argv[2],
          'iccid'                 => $argv[3],
          'customer_id'           => $argv[4],
          'override_default_imei' => $argv[5]
        ),
        'uuid'       => 'test'.time()
      )
    );

    print_r( $inboundReply );
  }
}


class Test_GetMVNEDetails
{
  function test( $argv , $accMiddleware )
  {
    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'GetMVNEDetails',
        'parameters' => array(
          'msisdn' => $argv[2],
          'iccid'  => $argv[3]
        ),
        'uuid'       => 'test'.time()
      )
    );
    
    print_r( $inboundReply );
  }
}


class Test_testcommand
{
  function test( $argv , $accMiddleware )
  {
    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'Test',
        'parameters' => array(
          'option'          => $argv[2],
          'json_parameters' => $argv[3]
        ),
        'actionUUID' => 'test'.time()
      )
    );

    print_r( $inboundReply );
  }
}


// php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeaturesRaw PROD-356_1 1001001000 1001001000100100100 123456 W-PRIMARY L39 EN 333 444 v_english
class Test_UpdatePlanAndFeaturesRaw
{
  function test( $argv , $accMiddleware )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    $r = $accMiddleware->processControlCommand(
      array(
        'command'    => 'UpdatePlanAndFeaturesRaw', // this is the ACC Command
        'actionUUID' => $actionUUID,
        'parameters' => array(
          'msisdn'             => $msisdn,
          'iccid'              => $iccid,
          'customer_id'        => $argv[5],
          'wholesale_plan'     => $argv[6],
          'ultra_plan'         => $argv[7],
          'preferred_language' => $argv[8],
          'keepDataSocs'       => TRUE,
          'raw_b_voice'        => $argv[9],
          'raw_b_sms'          => $argv[10],
          'raw_voicemail'      => $argv[11]
        )
      )
    );

    print_r( $r );
  }
}


// php Ultra/Lib/MiddleWare/ACC/Control_test.php RenewPlan test123 1001001000 1001001000100100100 123456 W-PRIMARY L29 EN [SUBPLAN]
class Test_RenewPlan
{
  function test( $argv , $accMiddleware )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;

    $r = $accMiddleware->processControlCommand(
      array(
        'command'    => 'RenewPlan', // this is the ACC Command
        'actionUUID' => $actionUUID,
        'parameters' => array(
          'msisdn'             => $msisdn,
          'iccid'              => $iccid,
          'customer_id'        => $argv[5],
          'wholesale_plan'     => $argv[6],
          'ultra_plan'         => $argv[7],
          'preferred_language' => $argv[8],
          'subplan'            => empty($argv[9]) ? NULL : $argv[9]
        )
      )
    );

    print_r( $r );
  }
}


// php Ultra/Lib/MiddleWare/ACC/Control_test.php RenewPlan test123 1001001000 1001001000100100100 123456 W-PRIMARY L29 EN
// php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $OPTION
// php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures VYT_TEST_003 7202431368 8901260962131284348 19204 W-SECONDARY L29 EN 'A-DATA-BLK|125'
// php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures VYT_TEST_004 3137754060 8901260963155428506 19863 W-SECONDARY UV30 ES 'A-DATA-BLK-PLAN|500'
// php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures VYT_TEST_100 7144174760 890126084211606175 19915 W-SECONDARY L19 EN RESET '{"b_sms":9010}'


class Test_UpdatePlanAndFeatures
{
  function test( $argv , $accMiddleware )
  {
    // read input parameters
    $required = array('command', 'actionUUID', 'msisdn', 'iccid', 'customer_id', 'wholesale_plan', 'ultra_plan', 'preferred_language');
    $optional = array('option', 'reset_config');
    if ( ! $params = readOrderedCommandLineArguments($required, 1, TRUE))
    { echo "missing one or more required parameter (see log)\n"; return; }
    $params = array_merge($params, readOrderedCommandLineArguments($optional, count($required) + 1, FALSE));

    $params['actionUUID'] = $params['actionUUID'] ? getNewActionUUID('test ' . time()) : $params['actionUUID'];
    $params['msisdn'] = normalize_msisdn($params['msisdn']);
    $params['iccid'] = luhnenize($params['iccid']);
    if ($params['option'] == 'RESET')
    {
      if ( ! $params['reset_config'] = json_decode($params['reset_config'], TRUE))
      { echo 'invalid or missing reset_config (must be json encoded array string)'; return; }
    }
    echo 'input parameters: ' . print_r($params, TRUE);

    $params['keepDataSocs'] = TRUE;
    $result = $accMiddleware->processControlCommand(
      array(
        'command'    => $params['command'], // this is the ACC Command
        'actionUUID' => $params['actionUUID'],
        'parameters' => $params
      )
    );

    print_r($result);
  }
}


class Test_CheckBalance
{
  function test( $argv , $accMiddleware )
  {
    $actionUUID = ( isset($argv[3]) && $argv[3] ) ? $argv[3]                   : getNewActionUUID('test ' . time());

    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'CheckBalance',
        'parameters' => array(
          'msisdn' => $argv[2]
        ),
        'actionUUID' => $actionUUID
      )
    );
    
    print_r( $inboundReply );

    print_r( json_decode($inboundReply->data_array['message']) );
  }
}


class Test_QuerySubscriber
{
  function test( $argv , $accMiddleware )
  {
    $actionUUID = ( isset($argv[3]) && $argv[3] ) ? $argv[3]                   : getNewActionUUID('test ' . time());

    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'QuerySubscriber',
        'parameters' => array( 
          'msisdn' => $argv[2]
        ),
        'actionUUID' => $actionUUID
      )
    );

    print_r( $inboundReply );

    print_r( json_decode($inboundReply->data_array['message']) );
  }
}

class Test_SuspendSubscriber
{
  function test($argv , $accMiddleware)
  {
    $inboundReply = $accMiddleware->processControlCommand([
      'command'    => 'SuspendSubscriber', // this is the ACC Command
      'actionUUID' => time(),
      'parameters' => array(
        'customer_id' => "{$argv[2]}",
        'iccid'       => "{$argv[3]}",
        'msisdn'      => "{$argv[4]}",
        'paidEvent'  => 1,
      )
    ]);

    print_r( $inboundReply );
  }
}

class Test_PortIn
{
  function test($argv , $accMiddleware)
  {

    $inboundReply = $accMiddleware->processControlCommand(
      array(
        'command'    => 'PortIn',
        'actionUUID' => time(),
        'parameters' => array(
          'customer_id'             => "{$argv[2]}",
          'msisdn'                  => "{$argv[3]}",
          'iccid'                   => "{$argv[4]}",
          'zipcode'                 => "{$argv[5]}",
          "ultra_plan"              => "{$argv[6]}",
          "wholesale_plan"          => "W-PRIMARY",
          "preferred_language"      => "EN",
          "port_account"            => "",
          "port_password"           => "")));

    echo 'inboundReply: ';
    print_r( $inboundReply );
  }
}


class Test_PortInEligibility
{
  function test($argv, $accMiddleware)
  {

    $inboundReply = $accMiddleware->processControlCommand(
      array(
        'command'    => 'PortInEligibility',
        'actionUUID' => time(),
        'parameters' => array('msisdn' => $argv[2])));

    echo 'inboundReply: ';
    print_r( $inboundReply );
  }
}


class Test_CancelPortIn
{
  function test($argv, $accMiddleware)
  {

    $inboundReply = $accMiddleware->processControlCommand(
      array(
        'command'    => 'CancelPortIn',
        'actionUUID' => time(),
        'parameters' => array('msisdn' => $argv[2])));

    echo 'inboundReply: ';
    print_r( $inboundReply );
  }
}


class Test_CanActivate
{
  function test( $argv , $accMiddleware )
  {
    $inboundReply  = $accMiddleware->processControlCommand(
      array(
        'command'    => 'CanActivate',
        'parameters' => array('iccid'  => $argv[2]),
        'actionUUID' => time(),
        'uuid'       => 'test'.time()));

    print_r( $inboundReply );
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test( $argv , $accMiddleware );

echo "\n";

/*
Examples:

php Ultra/Lib/MiddleWare/ACC/Control_test.php UpdatePlanAndFeatures actest223 9496104072 8901260962103787435 580418 W-PRIMARY L29 ES 'CHANGE|L29'

*/

