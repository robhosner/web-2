<?php

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';

/*
Test for class \Ultra\Lib\MiddleWare\ACC\ControlCommandBase
*/

class test_addRetailPlanIDFromUltraPlan
{
  function test( $object , $argv )
  {
    $method = new ReflectionMethod('\Ultra\Lib\MiddleWare\ACC\ControlCommandBase' , 'addRetailPlanIDFromUltraPlan' );
    $method->setAccessible(true);

    $plans = [
      'STANDBY' ,
      'NINETEEN' , 'TWENTY_FOUR' , 'TWENTY_NINE' , 'TWENTY_NINE' , 'THIRTY_FOUR' , 'THIRTY_NINE' , 'DTHIRTY_NINE' , 'FORTY_FOUR' , 'FORTY_NINE' , 'FIFTY_NINE' ,
      'UVTWENTY' , 'UVTHIRTY' , 'UVTHIRTYFIVE' , 'UVFORTYFIVE' , 'UVFIFTY' , 'UVFIFTYFIVE'
    ];

    foreach( $plans as $plan )
    {
      $parameters = [
        'ultra_plan_name' => $plan
      ];

      $parameters = $method->invoke(
        $object,
        $parameters
      );

      echo "plan = $plan ; PlanData = ".json_encode($parameters['PlanData'])."\n";
    }
  }
}

$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$actionUUID = \create_guid( 'test' );

$testObject = new $testClass();
$testObject->test( new \Ultra\Lib\MiddleWare\ACC\ControlCommandBase( $actionUUID ) , $argv );

