<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * ControlCommandManageCap control command implementation
 */
class ControlCommandManageCap extends ControlCommandBiphasic implements ControlCommand
{
  public $addOnFeatureInfo;
  public $balance;

  protected $command = 'ManageCap';

  private $accSocsDefinition;
  private $accSocsDefinitionByUltraSoc;
  private $ultraSocsByPlanId;

  public function __construct($actionUUID)
  {
    parent::__construct($actionUUID);

    // global SOCs configuration and mapping between Ultra and Amdocs
    list(
      $accSocsDefinition,
      $accSocsDefinitionByUltraSoc,
      $ultraSocsByPlanId
    ) = \Ultra\MvneConfig\getAccSocsDefinitions();

    $this->accSocsDefinition           = $accSocsDefinition;
    $this->accSocsDefinitionByUltraSoc = $accSocsDefinitionByUltraSoc;
    $this->ultraSocsByPlanId           = $ultraSocsByPlanId;
  }

  /**
   * addDefaultParameters
   *
   * Add parameters needed for all Control Commands
   *
   * @return array
   */
  private function addDefaultParameters($params)
  {
    $parameters['UserData']['channelId'] = 'ULTRA';
    $parameters['UserData']['senderId']  = 'MVNEACC';
    $parameters['UserData']['timeStamp'] = getTimeStamp();

    // remove the Z at the end of 'timeStamp'
    $parameters['UserData']['timeStamp'] = preg_replace( "/Z$/" , "" , $parameters['UserData']['timeStamp'] );

    return $parameters;
  }

  //
  public function processCommand($params)
  {
    \dlog('', '%s', $params);

    try
    {
      $params = array_merge($params, $this->addDefaultParameters($params));

      $command = strtolower($params['command']);
      $params['percent'] = isset($params['percent']) ? $params['percent'] : 0;

      // may be injected for testing
      if ( ! $this->addOnFeatureInfo)
        $this->addOnFeatureInfo = $this->getAddOnFeatureInfo($params);

      // check if has a data soc
      $hasSoc = $this->hasSoc(
        'a_data_pool_1',
        $this->addOnFeatureInfo
      );

      if ( ! $hasSoc)
        throw new \Exception('Customer cannot manage data limit - misconfiguration');

      //
      $params['BAN'] = $this->getCustomerBanByCustomerId($params['customer_id']);
      if (empty($params['BAN']))
        throw new \Exception('Customer cannot manage data limit - empty ban');

      // error out if method is not handled
      if ( ! method_exists($this, "{$params['command']}CapShare"))
        throw new Exception('command not handled');

      $params = $this->addCorrelationParameter($params, $this->command);

      // There are no pending ACC asynchronous API call, initiate a new Command Invocation
      $result = $this->initiate( $this->command , $this->command , $params );

      if ($result->is_failure())
        return $this->initiateErrorResult($this->command, $params['customer_id'] , $result);

      //
      $fxn = "{$params['command']}CapShare";
      $params = $this->$fxn($params);

      //
      unset($params['customer_id']);
      unset($params['command']);
      unset($params['percent']);

      $outboundControlMessage = $this->buildOutboundControlMessage($this->command, $params);

      return $this->interactWithChannel($this->command, $outboundControlMessage);
    }
    catch (\Exception $e)
    {
      \dlog('', 'XYZ ' . $e->getMessage());
      $result = make_error_Result($e->getMessage());
      return $this->initiateErrorResult($this->command, $params['customer_id'], $result);
    }
  }

  /**
   * gets ban from customer_id
   * if the customer does not have a BAN at this point
   * the UpdatePlanAndFeatures call will fail
   */
  public function getCustomerBanByCustomerId($customerId)
  {
    $ban = 0;

    $sharedData = new \Ultra\Lib\Services\SharedData();
    $result = $sharedData->getBucketIDByCustomerID($customerId);
    if ($result->is_success() && isset($result->data_array['bucket_id']))
    {
      $result = $sharedData->getBucketByBucketID($result->data_array['bucket_id']);
      if ($result->is_success() && isset($result->data_array['ban']))
        $ban = $result->data_array['ban'];
    }

    return $ban;
  }

  /**
   * addResourceData to public for testing
   */
  public function addResourceData($params)
  {
    return parent::addResourceData($params);
  }

  /**
   * inject addOnFeatureInfo from QuerySubscriber
   */
  public function setAddOnFeatureInfo($addOnFeatureInfo)
  {
    $this->addOnFeatureInfo = $addOnFeatureInfo;
  }

  /**
   * inject balance info from CheckBalance
   */
  public function setBalance($balance)
  {
    $this->balance = $balance;
  }

  /**
   */
  protected function hasSoc($soc, $addOnFeatureInfo)
  {
    $hasSoc = FALSE;

    foreach ($addOnFeatureInfo as $addOnFeature)
      if ( isset($this->ultraSocsByPlanId[$addOnFeature->FeatureID])
        && $this->ultraSocsByPlanId[$addOnFeature->FeatureID] == $soc
      ) $hasSoc = TRUE;

    return $hasSoc;
  }

  /**
   * make a QuerySubscriber calls to acquire addOnFeatureInfo
   */
  public function getAddOnFeatureInfo($params)
  {
    $resultQS = $this->getDataFromQuerySubscriber($params);
    if ($resultQS->is_failure())
      return $resultQS;

    $dataQS = $this->extractDataBodyFromMessage($resultQS->data_array['message']);
    \dlog('','dataQuerySubscriber = %s', $dataQS);

    $addOnFeatureInfo = ( isset($dataQS['AddOnFeatureInfoList']) )
      ? $dataQS['AddOnFeatureInfoList']->AddOnFeatureInfo
      : [];

    \dlog('','resultQuerySubscriber addOnFeatureInfo = %s', $addOnFeatureInfo);

    return $addOnFeatureInfo;
  }

  public function removeCapShare($params)
  {
    /*
    <acc:CapBucketList>
      <!--Zero or more repetitions:-->
      <acc:CapBucketInfo>
        <acc:CapBucket>12924</acc:CapBucket>
        <acc:CapAction>REMOVECAPSHARE</acc:CapAction>
        <!--Optional:-->
        <acc:CapAmount xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
      </acc:CapBucketInfo>
    </acc:CapBucketList>
    */

    $capBucketInfo = [
      'CapBucket' => 12924,
      'CapAction' => 'REMOVECAPSHARE'
    ];

    $params['CapBucketList'] = [ 'CapBucketInfo' => $capBucketInfo ];

    return $params;
  }

  public function addCapShare($params)
  {
    /*
    <acc:CapBucketList>
      <!--Zero or more repetitions:-->
      <acc:CapBucketInfo>
        <acc:CapBucket>12924</acc:CapBucket>
        <acc:CapAction>SETCAPSHARE</acc:CapAction>
        <acc:CapAmount>500</acc:CapAmount>
      </acc:CapBucketInfo>
    </acc:CapBucketList>
    */

    \dlog('', 'params on input = %s', $params);

    // check balance if has data soc
    if ( ! $this->balance)
    {
      $cb = new \Ultra\Lib\CheckBalance();
      $this->balance = $cb->byMSISDN($params['MSISDN']);
    }

    \dlog('','dataCheckBalance = %s', $this->balance);

    // get previous limit of soc
    $limit = 0;
    if ($this->balance)
      if (isset($this->balance['a_data_pool_1_limit']))
        $limit = $this->balance['a_data_pool_1_limit'];

    \dlog('','limit = %d', $limit);

    $cap = round($limit * ($params['percent'] / 100));

    \dlog('', 'cap = %d', $cap);

    $capBucketInfo = [
      'CapBucket' => 12924,
      'CapAction' => 'SETCAPSHARE',
      'CapAmount' => $cap
    ];

    $params['CapBucketList'] = [ 'CapBucketInfo' => $capBucketInfo ];

    \dlog('', 'params on output = %s', $params);

    return $params;
  }
}
