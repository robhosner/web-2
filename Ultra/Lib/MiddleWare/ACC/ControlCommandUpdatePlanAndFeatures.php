<?php

namespace Ultra\Lib\MiddleWare\ACC;

require_once 'classes/CheckBalance.php';
require_once 'Ultra/Lib/Services/SharedData.php';
require_once 'classes/Throttling.php';
require_once 'Ultra/Lib/Util/Redis.php';

use Ultra\Lib\Util\Redis;

/**
 * UpdatePlanAndFeatures control command implementation
 */
class ControlCommandUpdatePlanAndFeatures extends ControlCommandBiphasic implements ControlCommand
{
  protected $command = 'UpdatePlanAndFeatures';

  protected $availableCommands = [
    'SharedDataAdd'          => ['adatapool1'],
    'WFC'                    => ['nwfc'],
    'ChangePlan'             => ['change','changeimmediate'],
    'MintDataAddOn'          => ['adatamint'],
    'MintDataRemove'         => ['removedatamint'],
    'MintSuspend'            => ['mintsuspend'],
    'UpdateThrottleSpeed'    => ['throttlespeed'],
    'UpdateThrottleSpeedBan' => ['throttlespeedban'],
    'ResetSharedDataLimit'   => ['resetshareddatalimit'],
    'Reset'                  => ['reset'],
    'AddToBan'               => ['addtoban'],
    'RemoveFromBan'          => ['removefromban'],
    'Voicemail'           => [
      'venglish',
      'vspanish'
    ],
    'DataAdd'             => [
      'adatablk',
      'adatablkdr',
      'adatathr128',
      'adatablkplan'
    ],
    'NonDataAdd'          => [
      'bvoice',
      'bsms',
      'roam',
      'avoicedr',
      'asmsdr',
      'avoicesmswalletir'
    ]
  ];

  public $customer_id  = 0;
  public $keepDataSocs = 1;

  public $addOnFeatureInfo = null;
  public $balance          = null;
  public $brandId          = null;

  public $accSocsDefinition;
  public $accSocsDefinitionByUltraSoc;
  public $ultraSocsByPlanId;

  const BAN_DATA_USAGE_KEY = 'acc/a_data_pool_1_usage/';
  const BAN_DATA_USAGE_TTL = 900; // 15 minutes

  /**
   * Class constructor
   */
  public function __construct ( $actionUUID )
  {
    parent::__construct( $actionUUID );

    // global SOCs configuration and mapping between Ultra and Amdocs
    list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

    $this->accSocsDefinition           = $accSocsDefinition;
    $this->accSocsDefinitionByUltraSoc = $accSocsDefinitionByUltraSoc;
    $this->ultraSocsByPlanId           = $ultraSocsByPlanId;
  }

  /**
   * gets ban from customer_id
   * if the customer does not have a BAN at this point
   * the UpdatePlanAndFeatures call will fail
   */
  function getCustomerBanByCustomerId($customerId)
  {
    $ban = 0;

    $sharedData = new \Ultra\Lib\Services\SharedData();
    $result = $sharedData->getBucketIDByCustomerID($customerId);
    if ($result->is_success() && isset($result->data_array['bucket_id']))
    {
      $result = $sharedData->getBucketByBucketID($result->data_array['bucket_id']);
      if ($result->is_success() && isset($result->data_array['ban']))
        $ban = $result->data_array['ban'];
    }

    return $ban;
  }

  /**
   * addResourceData to public for testing
   */
  public function addResourceData($params)
  {
    return parent::addResourceData($params);
  }

  /**
   * return available commands
   */
  public function getAvailableCommands()
  {
    $ret = [];

    foreach ($this->availableCommands as $command => $aliases)
      foreach ($aliases as $alias)
        $ret[] = $alias;

    return $ret;
  }

  /**
   * inject addOnFeatureInfo from QuerySubscriber
   */
  public function setAddOnFeatureInfo($addOnFeatureInfo)
  {
    $this->addOnFeatureInfo = $addOnFeatureInfo;
  }

  /**
   * inject balance info from CheckBalance
   */
  public function setBalance($balance)
  {
    $this->balance = $balance;
  }

  /**
   * make a QuerySubscriber calls to acquire addOnFeatureInfo
   */
  public function getAddOnFeatureInfo($params)
  {
    $resultQS = $this->getDataFromQuerySubscriber($params);
    if ($resultQS->is_failure())
      return $resultQS;

    $dataQS = $this->extractDataBodyFromMessage($resultQS->data_array['message']);
    \dlog('','dataQuerySubscriber = %s', $dataQS);

    $addOnFeatureInfo = ( isset($dataQS['AddOnFeatureInfoList']) )
      ? $dataQS['AddOnFeatureInfoList']->AddOnFeatureInfo
      : [];

    \dlog('','resultQuerySubscriber addOnFeatureInfo = %s', $addOnFeatureInfo);

    return $addOnFeatureInfo;
  }

  /**
   * add current throttle speed to parameters
   */
  public function addThrottleInfo($params)
  {
    $params['current_throttle'] = \Ultra\MvneConfig\detectThrottleSpeed($this->addOnFeatureInfo);

    if ( ! $params['current_throttle']
      && ! \Ultra\Lib\Util\validateMintBrandId($params['brand_id'])
    ) $params['current_throttle'] = \Ultra\UltraConfig\getDefaultThrottleSpeed();

    return $params;
  }

  /**
   * bool whether call is a "Paid Event"
   */
  public function isPaidEvent($params)
  {
    $paidEvent = (
         $this->mintRetailSocChanged($params, $this->addOnFeatureInfo) // check Mint retailPlanID
      || $this->mintDataSocAdded($params) // check Mint Data Add On SOC
    ) ? 1 : 0;

    $retailPlanId = $params['PlanData']['retailPlanID'];
    \logDebug("retailPlanID = $retailPlanId ; isPaidEvent = $paidEvent");

    return $paidEvent;
  }

  /**
   *
   */
  public function getMintSocName($params, $paidEvent)
  {
    $socName = NULL;

    if ( $paidEvent
      && ! empty($params['PlanData'])
      && ! empty($params['PlanData']['AddOnFeatureList'])
      && ! empty($params['PlanData']['AddOnFeatureList']['AddOnFeature'])
    )
    {
      foreach( $params['PlanData']['AddOnFeatureList']['AddOnFeature'] as $feature )
        if (
          $feature['Action'] == 'ADD'
          && \Ultra\MvneConfig\isMintDataSOC( $feature['FeatureID'] )
        ) $socName = $feature['FeatureID'];

      if ( ! $socName
        && ! empty($params['PlanData']['retailPlanID'])
      ) $socName = $this->ultraSocsByPlanId[ $params['PlanData']['retailPlanID'] ];
    }

    return $socName;
  }

  /**
   */
  protected function hasDataSoc($dataSocToAdd, $addOnFeatureInfo)
  {
    $hasDataSoc = FALSE;

    foreach ($addOnFeatureInfo as $addOnFeature)
      if ( isset($this->ultraSocsByPlanId[$addOnFeature->FeatureID])
        && $this->ultraSocsByPlanId[$addOnFeature->FeatureID] == $dataSocToAdd
      ) $hasDataSoc = TRUE;

    return $hasDataSoc;
  }

  public function addParametersForOption($params)
  {
    $parts  = explode('|', $params['option']);
    $option = preg_replace("/[^A-Za-z0-9]/", '', strtolower($parts[0]));
    $value  = isset($parts[1]) ? $parts[1] : null;

    $fxn = null;
    foreach ($this->availableCommands as $command => $aliases)
      foreach ($aliases as $alias)
        if ($alias == $option)
          $fxn = $command;

    if ($fxn == 'MintDataRemove')
    {
      $parts[0] = "{$parts[1]}-{$parts[2]}";
      $value = null;
    }

    if ($fxn == 'UpdateThrottleSpeed' && (!empty($params['migratingFromMrc'] && $params['migratingFromMrc'] == true))) {
      return $this->parametersCommandUpdateThrottleSpeed($params, $parts[0], $value, true);
    }

    if ( ! $fxn) return null;
    return $this->{"parametersCommand$fxn"}($params, $parts[0], $value);
  }

  protected function parametersCommandResetSharedDataLimit($params, $action, $percent)
  {
    $dataSocToAdd = 'a_data_pool_1';
    $dataSocToAdd = str_replace('-', '_', strtolower($dataSocToAdd));

    // check if has a data soc
    $hasDataSoc = $this->hasDataSoc(
      'a_data_pool_1',
      $this->addOnFeatureInfo
    );

    // check balance if has data soc
    if ($hasDataSoc && ! $this->balance)
    {
      $cb = new \Ultra\Lib\CheckBalance();
      $this->balance = $cb->byMSISDN($params['MSISDN']);
    }

    // SOCs in the current customer plan
    $subplan =  empty($params['subplan']) ? NULL : $params['subplan'];
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );

    // determine the voicemail SOCs

    //english
    if ( is_english( $params['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    // spanish
    if ( $params['preferredLanguage'] == 'es-US' 
      || $params['preferredLanguage'] == 'es'
    ) $accSocsPlanConfig[ 'v_spanish' ] = '';

    // get previous limit of soc
    $dataSet = [];
    if ($this->balance)
    {
      if (isset($this->balance[$dataSocToAdd . '_limit']))
      {
        $dataSet[$dataSocToAdd] = [
          'used'  => $this->balance[$dataSocToAdd . '_used'],
          'limit' => $this->balance[$dataSocToAdd . '_limit']
        ];
      }
    }

    \dlog('',"dataCheckBalance = %s", $this->balance);
    \dlog('',"dataSet = %s", $dataSet);

    //
    $checked = FALSE;

    // iterate through current features
    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      //
      if ( isset($dataSet[ $ultraSocName ]) )
      {
        $action       = 'SETCAPSHARE';
        $featureValue = round($dataSet[$ultraSocName]['limit'] * ($percent / 100));

        \dlog('', 'soc: %s, action: %s, value: %d', $ultraSocName, $action, $featureValue);
      }

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => $action,
            'autoRenew'    => 'false',
            'FeatureID'    => $addOnFeature->FeatureID,
            'FeatureValue' => $featureValue
          );
    }

    // $params['PlanData']['BAN'] = $this->getCustomerBanByCustomerId($params['customer_id']);

    return $params;
  }

  /**
   * parametersCommandWFC
   *
   * @return array
   */
  protected function parametersCommandWFC($params, $wfc, $value)
  {
    $action = 'ADD';

    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      // set to eventually remove wifi soc if exists
      if ( $ultraSocName == 'n_wfc')
        $action = 'REMOVE';
      else
      {
        $featureValue = '';
        if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
          $featureValue = $accSocsPlanConfig[ $ultraSocName ];

        if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
          && ! \Ultra\MvneConfig\isRetailPlanSOC( $ultraSocName )
        ) // ignore retailPlanID and Wholesale in this context
        {
          $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
            'Action'       => 'RETAIN',
            'autoRenew'    => 'false',
            'FeatureID'    => $addOnFeature->FeatureID,
            'FeatureValue' => $featureValue
          ];
        }
      }
    }

    if ( ($action == 'ADD'    && $value == 'ADD')
      || ($action == 'REMOVE' && $value == 'REMOVE')
    )
    {
      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( 'n_wfc' , 'plan_id' ),
          'FeatureValue' => ''
        );
    }

    return $params;
  }

  public function isLatamSoc($soc)
  {
    return in_array($soc, [
      'n_roam_latam',
      12811
    ]);
  }

  /**
   * parametersCommandChangePlan
   *
   * Adjust $params['PlanData']['AddOnFeatureList'] for a plan change
   * $addOnFeatureInfo contains the SOCs currently active for the customer
   * On immediate plan change we should RETAIN all optional Data Socs.
   * On plan renewal we should REMOVE all optional Data Socs.
   *
   * @return array
   */
  protected function parametersCommandChangePlan($params, $option, $newPlan)
  {
    $newPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $newPlan,
      $params['customer_id'],
      empty($params['subplan']) ? NULL : $params['subplan']
    );
    \Ultra\MvneConfig\adjustPlanConfigForThrottle($newPlanConfig, $params['current_throttle']);

    \dlog('',"accSocsPlanConfigNewPlan = %s", $newPlanConfig);

    $retailPlans = \Ultra\MvneConfig\getRetailPlans();
    foreach( $retailPlans as $retailPlan )
      if ( isset($newPlanConfig[ $retailPlan ]) )
        $params['PlanData']['retailPlanID'] = \Ultra\MvneConfig\getAccSocDefinitionField( $retailPlan , 'plan_id' );

    // check balance if has data soc
    if (! $this->balance)
    {
      $cb = new \Ultra\Lib\CheckBalance();
      $this->balance = $cb->byMSISDN($params['MSISDN']);
    }

    $retained = array();

    // loop through SOCs already set
    foreach ( $this->addOnFeatureInfo as $addOnFeature )
    {
      // b_voice n_wfc n_roam_intl etc
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $newPlanConfig[ $ultraSocName ] ) )
        $featureValue = $newPlanConfig[ $ultraSocName ];

      // is $ultraSocName in the new plan ? retain : remove
      if (( ! isset( $newPlanConfig[ $ultraSocName ] )
        && ! \Ultra\UltraConfig\isVoicemailSoc     ( $ultraSocName )
        && ! \Ultra\UltraConfig\isWifiCallingSoc   ( $ultraSocName )
        && ! \Ultra\UltraConfig\isRoamingSoc       ( $ultraSocName )
        && ! \Ultra\UltraConfig\isRoamingWalletSoc ( $ultraSocName )
        && ( ! $this->keepDataSocs || ! \Ultra\UltraConfig\isAddOnDataSoc( $ultraSocName ) ))
        || $this->isLatamSoc( $ultraSocName )
      )
      {
        $featureValue = '';
        $action       = 'REMOVE';
      }
      else
        $retained[] = $ultraSocName;

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
      {
        if ( isset($newPlanConfig [ $ultraSocName ])
          && $newPlanConfig       [ $ultraSocName ]
          && ! \Ultra\UltraConfig\isRoamingSoc       ( $ultraSocName ) // retain ROAMING credit
          && ! \Ultra\UltraConfig\isRoamingWalletSoc ( $ultraSocName )
        )
        {
          $featureValue = $newPlanConfig[ $ultraSocName ];
          $action = 'RESET';
        }

        if ($option == 'CHANGEIMMEDIATE')
        {
          if ($addOnFeature->FeatureID == B_VOICE_FEATURE_ID)
            $featureValue = $featureValue - ($featureValue - $this->balance['voice_minutes']);

          if ($addOnFeature->FeatureID == B_SMS_FEATURE_ID)
            $featureValue = $featureValue - ($featureValue - $this->balance['sms_messages']);
        }

        // rollover roaming credit for Mint
        if ($params['brand_id'] == 3
          && \Ultra\UltraConfig\isRoamingWalletSoc($ultraSocName)
          && !empty($this->balance['romaing_balance'])
        ) {
          $featureValue = $featureValue + $this->balance['romaing_balance'];
        }

        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
      }
    }

    // loop through SOCs to be added
    if ( $newPlan != 'STANDBY' )
    {
      // loop through SOCs to be added
      foreach( $newPlanConfig as $ultraSocName => $featureValue )
        if ( ! in_array( $ultraSocName , $retained )
          && ! \Ultra\MvneConfig\isWholesalePlanSOC ( $ultraSocName )
          && ! \Ultra\MvneConfig\isRetailPlanSOC    ( $ultraSocName )
        ) // ignore retailPlanID and Wholesale in this context
        {
          $currRoamingSoc = \Ultra\MvneConfig\detectRoamingSoc($this->addOnFeatureInfo);
          if ( \Ultra\UltraConfig\isRoamingSoc( $ultraSocName )
            && $currRoamingSoc
            && ! $this->isLatamSoc( $currRoamingSoc ))
            continue;

          if ( \Ultra\UltraConfig\isRoamingWalletSoc( $ultraSocName )
            && \Ultra\MvneConfig\detectRoamingWalletSoc($this->addOnFeatureInfo))
            continue;

          $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
            'Action'       => 'ADD',
            'autoRenew'    => 'false',
            'FeatureID'    => $this->accSocsDefinitionByUltraSoc[ $ultraSocName ]['plan_id'],
            'FeatureValue' => $featureValue
          ];
        }
    }

    if ( ! in_array( 'v_english' , $retained )
      && ! in_array( 'v_spanish' , $retained )
      && ( $newPlan != 'STANDBY' ) // do not add $languageSOC for 'STANDBY' plan
    )
    {
      // language SOC
      $languageSOC = \Ultra\UltraConfig\mapLanguageToVoicemailSoc( $params['preferredLanguage'] );
      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
        'Action'       => 'ADD',
        'autoRenew'    => 'false',
        'FeatureID'    => $this->accSocsDefinitionByUltraSoc[ $languageSOC ]['plan_id'],
        'FeatureValue' => ''
      ];
    }

    \dlog('',"returning %s", $params);

    return $params;
  }

/**
   * parametersCommandDataAdd
   *
   * Adjust $params['PlanData']['AddOnFeatureList'] for A-DATA-BLK-DR and A-DATA-BLK
   *
   * @return array
   */
  protected function parametersCommandDataAdd($params, $dataSocToAdd, $value)
  {
    // A-DATA-BLK -> a_data_blk
    $dataSocToAdd = str_replace('-', '_', strtolower($dataSocToAdd));

    // check if has a data soc
    $hasDataSoc = $this->hasDataSoc(
      \Ultra\MvneConfig\adjustDataSocForThrottle('a_data_blk', $params['current_throttle']),
      $this->addOnFeatureInfo
    );

    // check balance if has data soc
    if ($hasDataSoc && ! $this->balance)
    {
      $cb = new \Ultra\Lib\CheckBalance();
      $this->balance = $cb->byMSISDN($params['MSISDN']);
    }

    // adjust data soc to add based on current throttle speed
    $dataSocToAdd = \Ultra\MvneConfig\adjustDataSocForThrottle(
      $dataSocToAdd,
      $params['current_throttle']
    );

    // SOCs in the current customer plan
    $subplan =  empty($params['subplan']) ? NULL : $params['subplan'];
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );
    \Ultra\MvneConfig\adjustPlanConfigForThrottle($accSocsPlanConfig, $params['current_throttle']);

    // determine the voicemail SOCs

    //english
    if ( is_english( $params['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    // spanish
    if ( $params['preferredLanguage'] == 'es-US' 
      || $params['preferredLanguage'] == 'es'
    ) $accSocsPlanConfig[ 'v_spanish' ] = '';

    // get previous limit of soc to add
    $dataSet = [];
    if ($this->balance)
    {
      if (isset($this->balance[$dataSocToAdd . '_limit']))
      {
        $dataSet[$dataSocToAdd] = [
          'used'  => $this->balance[$dataSocToAdd . '_used'],
          'limit' => $this->balance[$dataSocToAdd . '_limit']
        ];
      }
    }

    \dlog('',"dataCheckBalance = %s", $this->balance);
    \dlog('',"dataSet = %s", $dataSet);

    //
    $checked = FALSE;

    // iterate through current features
    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      //
      if ( isset($dataSet[ $ultraSocName ]) )
      {
        //
        if (in_array($ultraSocName, \Ultra\MvneConfig\getAccDataSocsByType('plan')))
        {
          $action       = 'RESET';
          $featureValue = $value;
        }
        else
        {
          $action       = 'RESETLIMIT';
          $featureValue = $dataSet[$ultraSocName]['limit'] + $value;
        }

        \dlog('', 'soc: %s, action: %s, value: %d', $ultraSocName, $action, $featureValue);
      }

      if ( $dataSocToAdd == $ultraSocName )
        $checked = TRUE;

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => $action,
            'autoRenew'    => 'false',
            'FeatureID'    => $addOnFeature->FeatureID,
            'FeatureValue' => $featureValue
          );
    }

    // add new data SOC
    if ( ! $checked )
      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $dataSocToAdd , 'plan_id' ),
          'FeatureValue' => $value
        );

    return $params;
  }

  /**
   * parametersCommandSharedDataAdd
   *
   * @return array
   */
  protected function parametersCommandSharedDataAdd($params, $dataSocToAdd, $value)
  {
    $ban = $this->getCustomerBanByCustomerId($params['customer_id']);

    // A-DATA-BLK -> a_data_blk
    $dataSocToAdd = str_replace('-', '_', strtolower($dataSocToAdd));

    // check if has a data soc
    $hasDataSoc = $this->hasDataSoc(
      'a_data_pool_1',
      $this->addOnFeatureInfo
    );

    // check balance if has data soc
    if ($hasDataSoc && ! $this->balance)
    {
      $cb = new \Ultra\Lib\CheckBalance();
      $this->balance = $cb->byMSISDN($params['MSISDN']);
    }

    // SOCs in the current customer plan
    $subplan =  empty($params['subplan']) ? NULL : $params['subplan'];
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );

    // determine the voicemail SOCs

    //english
    if ( is_english( $params['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    // spanish
    if ( $params['preferredLanguage'] == 'es-US' 
      || $params['preferredLanguage'] == 'es'
    ) $accSocsPlanConfig[ 'v_spanish' ] = '';

    // get previous limit of soc to add
    $dataSet = [];
    if ($this->balance)
    {
      if (isset($this->balance[$dataSocToAdd . '_limit']))
      {
        $dataSet[$dataSocToAdd] = [
          'used'  => $this->balance[$dataSocToAdd . '_used'],
          'limit' => $this->balance[$dataSocToAdd . '_limit']
        ];

        // check redis for BAN data usage value
        $redis = new Redis();
        $banUsage = $redis->get(self::BAN_DATA_USAGE_KEY . $ban);
        if ($banUsage) {
          $dataSet[$dataSocToAdd]['used'] = $banUsage;
        } else if (!empty($dataSet[$dataSocToAdd]['used'])) {
          $redis->set(
            self::BAN_DATA_USAGE_KEY . $ban,
            $dataSet[$dataSocToAdd]['used'],
            self::BAN_DATA_USAGE_TTL
          );
        }
      }
    }

    \dlog('',"dataCheckBalance = %s", $this->balance);
    \dlog('',"dataSet = %s", $dataSet);

    //
    $checked = FALSE;

    // iterate through current features
    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      //
      if ( isset($dataSet[ $ultraSocName ]) )
      {
        $action       = 'RESETSHARE';
        $featureValue = ($dataSet[$ultraSocName]['limit'] - $dataSet[$ultraSocName]['used']) + $value;
        $featureValue = round($featureValue);

        \dlog('', 'soc: %s, action: %s, value: %d', $ultraSocName, $action, $featureValue);
      }

      if ( $dataSocToAdd == $ultraSocName )
        $checked = TRUE;

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => $action,
            'autoRenew'    => 'false',
            'FeatureID'    => $addOnFeature->FeatureID,
            'FeatureValue' => $featureValue
          );
    }

    // add new data SOC
    if ( ! $checked )
    {
      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => 'ADDSHARE',
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $dataSocToAdd , 'plan_id' ),
          'FeatureValue' => $value
        );

      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( 'n_pool_thr1536' , 'plan_id' ),
          'FeatureValue' => $value
        );
    }

    $params['PlanData']['BAN'] = $ban;

    return $params;
  }

  protected function parametersCommandAddToBan($params, $action, $value)
  {
    return $this->parametersCommandSharedDataAdd($params, 'a_data_pool_1', $value);
  }

  protected function parametersCommandRemoveFromBan($params, $action, $value)
  {
    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];
      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
        && $ultraSocName != 'a_data_pool_1'
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => 'RETAIN',
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
    }

    $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
      [
        'Action'       => 'REMOVESHARE',
        'autoRenew'    => 'false',
        'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( 'a_data_pool_1' , 'plan_id' ),
        'FeatureValue' => ''
      ];

    $params['PlanData']['BAN'] = $value;

    return $params;
  }

  /**
   * parametersCommandMintDataAddOn
   *
   * Adjust $params['PlanData']['AddOnFeatureList'] for Mint Data Add On
   *
   * @param array  $params
   * @param string $tier
   * @return array
   */
  protected function parametersCommandMintDataAddOn($params, $dataSocToAdd, $tier)
  {
    // SOCs in the current customer plan
    $subplan = empty($params['subplan'])
      ? null 
      : $params['subplan'];

    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );

    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
    }

    // Mint Data Add On
    $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
      'Action'       => 'ADD',
      'autoRenew'    => 'false',
      'FeatureID'    => \Ultra\MvneConfig\getMintDataSocFeatureID( $tier ),
      'FeatureValue' => ''
    ];

    return $params;
  }

  /**
   * parametersMintDataRemove
   *
   * Adjust $parameters['PlanData']['AddOnFeatureList'] for Mint Data SOC removal
   *
   * @param array $parameters
   * @param array $addOnFeatureInfo
   * @return array
   */
  protected function parametersCommandMintDataRemove($params, $addOnDataSoc )
  {
    // SOCs in the current customer plan
    $subplan = empty($params['subplan'])
      ? null
      : $params['subplan'];

    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );

    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      if ( $addOnFeature->UltraServiceName == $addOnDataSoc )
        $action = 'REMOVE';

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
    }

    return $params;
  }

  /**
   * parametersCommandMintSuspend
   *
   * Used when suspending a MINT plan
   * Removes all features except [ wholesale, retail, voicemail ]
   *
   * @return array
   */
  protected function parametersCommandMintSuspend($params, $action, $value)
  {
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      null
    );

    foreach ($this->addOnFeatureInfo as $addOnFeature)
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $featureValue = '';
      if (isset($accSocsPlanConfig[ $ultraSocName ]))
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC(  $ultraSocName )
        && ! \Ultra\MvneConfig\isMintRetailPlanSOC( $addOnFeature->FeatureID )
      ) // ignore retailPlanID and Wholesale in this context
      {
        $action = (
             \Ultra\UltraConfig\isVoicemailSoc     ($ultraSocName)
          || \Ultra\UltraConfig\isWifiCallingSoc   ($ultraSocName)
          || \Ultra\UltraConfig\isRoamingSoc       ($ultraSocName)
          || \Ultra\UltraConfig\isRoamingWalletSoc ($ultraSocName)
        ) ? 'RETAIN' : 'REMOVE';


        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
      }
    }

    return $params;
  }

  protected function parametersCommandUpdateThrottleSpeed($params, $option, $throttleSpeed, $migratingFromMrc = false)
  {
    // SOCs in the current customer plan
    $subplan =  empty($params['subplan']) ? NULL : $params['subplan'];

    // get the plan configuration and modify it for current throttling speed
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'], $params['customer_id'], $subplan);
    \Ultra\MvneConfig\adjustPlanConfigForThrottle($accSocsPlanConfig, $params['current_throttle']);

    if (!$migratingFromMrc) {
      // check the dang balance
      if (!$this->balance) {
        $cb = new \Ultra\Lib\CheckBalance();
        $this->balance = $cb->byMSISDN($params['MSISDN']);
        if (empty($this->balance)) {
          // CheckBalance error
          return null;
        }
      }
    }

    // iterate current features
    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      // retain feature by default
      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      // if a throttling feature
      // and we are not attempting to change into same throttle speed
      // set old throttle speed to be removed and new to be added
      $planOrBlock = \Ultra\MvneConfig\getThrottleSpeedFeatureFromSocId($addOnFeature->FeatureID);
      if ( $planOrBlock
        && $addOnFeature->FeatureID != \Ultra\MvneConfig\getThrottleSpeedSocId($planOrBlock, $throttleSpeed)
      )
      {
        $currSocId = \Ultra\MvneConfig\getThrottleSpeedSocId($planOrBlock, $params['current_throttle']);
        $newSocId  = \Ultra\MvneConfig\getThrottleSpeedSocId($planOrBlock, $throttleSpeed);

        // set old feature id to be removed
        $action = 'REMOVE';

        $used  = 0;
        $limit = 0;

        if ($addOnFeature->FeatureID == $currSocId)
            if (array_key_exists($ultraSocName . '_used', $this->balance))
            {
              $used  = $this->balance[$ultraSocName . '_used'];
              $limit = $this->balance[$ultraSocName . '_limit'];
            }

        if ($migratingFromMrc) {
          $account = get_account_from_customer_id($params['customer_id'], ['COS_ID']);
          $plan_short = get_monthly_plan_from_cos_id($account->COS_ID);
          $used  = 0;
          $limit = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($plan_short)['a_data_blk_plan'];
        }

        // add the feature id of our new speed with the new data limit
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => $newSocId,
          'FeatureValue' => (int)($limit - $used)
        ];
      }

      // use $action for features
      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
    }

    return $params;
  }

  /**
   *
   */
  protected function parametersCommandUpdateThrottleSpeedBan($params, $option, $throttleSpeed)
  {
    $throttling = new \Throttling($params['ultra_plan_name']);
    $params['current_throttle'] = $throttling->detectThrottleSpeedFromAddOnFeatureInfo($this->addOnFeatureInfo);

    $currThrottleSocId = $throttling->getThrottleSpeedSocId('plan', $params['current_throttle']);
    $newThrottleSocId  = $throttling->getThrottleSpeedSocId('plan', $throttleSpeed);

    // SOCs in the current customer plan
    $subplan = empty($params['subplan']) ? NULL : $params['subplan'];

    // iterate current features
    foreach( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      // retain feature by default
      $action = 'RETAIN';

      $featureValue = '';
      if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
        $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      // if a throttling feature
      // and we are not attempting to change into same throttle speed
      // set old throttle speed to be removed and new to be added

      if ( $currThrottleSocId == $addOnFeature->FeatureID
        && $addOnFeature->FeatureID != $newThrottleSocId
      )
      {
        // set old feature id to be removed
        if ($addOnFeature->FeatureID != 12924)
        {
          $action = 'REMOVE';
        }

        if ($newThrottleSocId != 12924)
        {
          // add the feature id of our new speed with the new data limit
          $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
            'Action'       => 'ADD',
            'autoRenew'    => 'false',
            'FeatureID'    => $newThrottleSocId,
            'FeatureValue' => 0
          ];
        }
      }

      // use $action for features
      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC   ( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = [
          'Action'       => $action,
          'autoRenew'    => 'false',
          'FeatureID'    => $addOnFeature->FeatureID,
          'FeatureValue' => $featureValue
        ];
    }

    return $params;
  }

  /**
   * parametersCommandVoiceMail
   *
   * Adjust $params['PlanData']['AddOnFeatureList'] for $languageSocToAdd
   * Tested successfully with:
   * %> php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP005 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN V-SPANISH
   * %> php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP006 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN V-ENGLISH
   *
   * @return array
   */
  protected function parametersCommandVoicemail($params, $languageSocToAdd, $value = null)
  {
    $languageSocToAdd = str_replace('-', '_', strtolower($languageSocToAdd));

    // SOCs in the current customer plan
    $subplan =  empty($params['subplan']) ? NULL : $params['subplan'];
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );
    \Ultra\MvneConfig\adjustPlanConfigForThrottle($accSocsPlanConfig, $params['current_throttle']);

    // determine the voicemail SOCs

    if ( is_english( $params['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    if ( $params['preferredLanguage'] == 'es-US'
      || $params['preferredLanguage'] == 'es'
    ) $accSocsPlanConfig[ 'v_spanish' ] = '';

    // set which language soc to remove
    $languageSocToRemove = ( $languageSocToAdd == 'v_english' ) ? 'v_spanish' : 'v_english';

    $retained  = FALSE;
    $removeOld = FALSE;

    foreach ( $this->addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      if ( $ultraSocName == $languageSocToRemove )
        $removeOld = TRUE;
      else
      {
        $action = 'RETAIN';

        if ( $ultraSocName == $languageSocToAdd )
        {
          // retain $languageSocToAdd
          $retained = TRUE;
          $action   = 'RETAIN';
        }

        $featureValue = '';
        if ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )
          $featureValue = $accSocsPlanConfig[ $ultraSocName ];

        if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
          && ! \Ultra\MvneConfig\isRetailPlanSOC( $ultraSocName )
        ) // ignore retailPlanID and Wholesale in this context
          $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
            array(
              'Action'       => $action,
              'autoRenew'    => 'false',
              'FeatureID'    => $addOnFeature->FeatureID,
              'FeatureValue' => $featureValue
            );
      }
    }

    if ( ! $retained )
    {
      // add $languageSocToAdd
      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $languageSocToAdd , 'plan_id' ),
          'FeatureValue' => ''
        );

      // remove $languageSocToRemove only if present
      if ( $removeOld )
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => 'REMOVE',
            'autoRenew'    => 'false',
            'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $languageSocToRemove , 'plan_id' ),
            'FeatureValue' => ''
          );
    }

    return $params;
  }

  /**
   * resetSpecificFeatures
   *
   * generate AddOnFeatureList parameters to reset provided SOCs values while retaining all others
   * SOCs not already present will generate an error, therefore this method is only for SOCs on the same plan
   * @param Array instance parameters
   * @param Array new SOC configuration
   * @param Array existing features
   * @return Array adjusted instance parameters or NULL on failure
   */
  private function parametersCommandReset($params, $option, $value)
  {
    $config = $params['reset_config'];
    unset($params['reset_config']);

    // RESET each SOC value given in $config otherwise RETAIN
    foreach ($this->addOnFeatureInfo as $addOnFeature)
    {
      if ($ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ])
      {
        // skip retail and wholesale plan IDs: they are not actual ACC features
        if ( ! \Ultra\MvneConfig\isWholesalePlanSOC($ultraSocName)
          && ! \Ultra\MvneConfig\isRetailPlanSOC($ultraSocName)
        )
        {
          $reset = array_key_exists($ultraSocName, $config) ? TRUE : FALSE;
          $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] = array(
            'FeatureID'    => $addOnFeature->FeatureID,
            'autoRenew'    => 'false',
            'Action'       => $reset ? 'RESET' : 'RETAIN',
            'FeatureValue' => $reset ? $config[$ultraSocName] : '');

        // remove applied SOC
        if ($reset)
          unset($config[$ultraSocName]);
        }
      }
      else
        logError("failed to identify feature ID {$addOnFeature->FeatureID}");
    }

    // check that all given SOCs have been applied
    if (count($config))
    {
      // check if we should add IR socs
      if (array_key_exists('a_voicesmswallet_ir', $config))
      {
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => 'ADD',
            'autoRenew'    => 'false',
            'FeatureID'    => \Ultra\UltraConfig\roamingSocFeatureIdByBrandId($params['brand_id']),
            'FeatureValue' => ''
          );

        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => 'ADD',
            'autoRenew'    => 'false',
            'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( 'a_voicesmswallet_ir' , 'plan_id' ),
            'FeatureValue' => $config['a_voicesmswallet_ir']
          );

        unset($config['a_voicesmswallet_ir']);
      }

      if (count($config))
        return logError('unable to reset missing SOCs: ' . json_encode($config));
    }

    return $params;
  }

  /**
   * parametersCommandNonDataAdd
   *
   * Adjust $params['PlanData']['AddOnFeatureList'] for $socToAddOrIncrement and $value
   * Tested successfully with:
   * %> php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP31 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'A-SMS-DR|456'
   * %> php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP33 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'A-VOICE-DR|123'
   *
   * @return array
   */
  protected function parametersCommandNonDataAdd($params,$socToAddOrIncrement,$value)
  {
    $socToAddOrIncrement = str_replace('-', '_', strtolower($socToAddOrIncrement));

    // SOCs in the current customer plan
    $subplan =  empty($params['subplan']) ? NULL : $params['subplan'];

    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $params['ultra_plan_name'],
      $params['customer_id'],
      $subplan
    );
    \Ultra\MvneConfig\adjustPlanConfigForThrottle($accSocsPlanConfig, $params['current_throttle']);

    // some brands have different roaming SOCs
    $socToAddOrIncrement = ($socToAddOrIncrement == 'roam')
      ? 'a_voicesmswallet_ir'
      : $socToAddOrIncrement;

    // determine the voicemail SOCs
    if ( is_english( $params['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    if ( $params['preferredLanguage'] == 'es-US'
      || $params['preferredLanguage'] == 'es'
    ) $accSocsPlanConfig[ 'v_spanish' ] = '';

    // increment or decrement
    $increment = FALSE;

    foreach ($this->addOnFeatureInfo as $addOnFeature)
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      $action = 'RETAIN';

      if ( $ultraSocName == $socToAddOrIncrement )
      {
        // set action to DECREMENT if value is < 0
        $action = ($value >= 0) ? 'INCREMENT' : 'DECREMENT';
        $increment = TRUE;
      }

      $featureValue = '';
      if     ( $action == 'INCREMENT' || $action == 'DECREMENT') $featureValue = abs($value);
      elseif ( isset( $accSocsPlanConfig[ $ultraSocName ] ) )    $featureValue = $accSocsPlanConfig[ $ultraSocName ];

      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
        && ! \Ultra\MvneConfig\isRetailPlanSOC( $ultraSocName )
      ) // ignore retailPlanID and Wholesale in this context
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => $action,
            'autoRenew'    => 'false',
            'FeatureID'    => $addOnFeature->FeatureID,
            'FeatureValue' => $featureValue
          );
    }

    // if not INCREMENT or DECREMENT
    if ( ! $increment )
    {
      // if we are adding roaming credit, and roaming soc does not exist
      if ($socToAddOrIncrement == 'a_voicesmswallet_ir' && ! \Ultra\MvneConfig\detectRoamingSoc($this->addOnFeatureInfo))
      {
        $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => 'ADD',
            'autoRenew'    => 'false',
            'FeatureID'    => \Ultra\UltraConfig\roamingSocFeatureIdByBrandId($params['brand_id']),
            'FeatureValue' => ''
          );
      }

      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $socToAddOrIncrement , 'plan_id' ),
          'FeatureValue' => $value
        );
    }

    return $params;
  }

  /**
   * processCommand
   */
  public function processCommand($params)
  {
    \dlog('', '%s', $params);

    $result = new \Result;

    $option = $params['option'];

    $this->customer_id  = $params['customer_id'];
    $this->keepDataSocs = ! empty($params['keepDataSocs']);

    // inject
    if ( ! $this->addOnFeatureInfo)
      $this->addOnFeatureInfo = $this->getAddOnFeatureInfo($params);

    // inject
    $params['brand_id'] = ( ! $this->brandId)
      ? self::getBrandIdFromICCID($params['ICCID'])
      : $this->brandId;

    $params = $this->addThrottleInfo($params);
    $params = $this->addRetailPlanIDFromUltraPlan($params);

    $params = $this->addParametersForOption($params);
    if ( ! $params)
    // option is not handled
    {
      $result->add_data_array(
        [ 'message' => $this->buildInboundErrorMessage(
          $this->command,
          ["option $option not handled yet."],
          ['customer_id' => $this->customer_id]
        ) ] );

      $result->succeed();
      return $result;
    }

    $params = $this->addCorrelationParameter($params, $this->command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $this->command , $this->command , $params );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $this->command , $this->customer_id , $result );

    $params = $this->addAutoRenew($params);

    $params['PlanData']['wholesalePlan'] = $params['wholesalePlan'];

    $params    = $this->addResourceData($params);
    $paidEvent = $this->isPaidEvent($params);
    $socName   = $this->getMintSocName($params, $paidEvent);

    unset( $params['ICCID'] );
    unset( $params['customer_id'] );
    unset( $params['option'] );
    unset( $params['preferredLanguage'] );
    unset( $params['ultra_plan_name'] );
    unset( $params['wholesalePlan'] );

    // failure condition for duplicate paid event for same makeitsoQueueId
    if ( $paidEvent
      && ( isset($params['makeitsoQueueId']) && $params['makeitsoQueueId'] > 0 )
      && \exists_successful_paid_event_by_makeitso_queue_id( $params['makeitsoQueueId'] )
    )
    {
      return make_error_Result( 'Duplicate Paid Event for makeitsoQueueId '.$params['makeitsoQueueId'] );
    }

    $outboundControlMessage = $this->buildOutboundControlMessage( $this->command , $params , $paidEvent , $socName );

    return $this->interactWithChannel( $this->command , $outboundControlMessage );
  }
}
