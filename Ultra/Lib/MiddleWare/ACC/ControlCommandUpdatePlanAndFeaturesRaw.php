<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * UpdatePlanAndFeaturesRaw command implementation
 *
 * Extends Ultra\Lib\MiddleWare\ACC\ControlCommandUpdatePlanAndFeatures and override processCommand
 *
 * Allows to set
 *  - Voice Minutes ( B-VOICE )
 *  - SMS Messages  ( B-SMS   )
 *
 * See PROD-356
 */
class ControlCommandUpdatePlanAndFeaturesRaw extends ControlCommandUpdatePlanAndFeatures
{
  /**
   * processCommand
   *
   * Input values:
   * - 'raw_b_voice'
   * - 'raw_b_sms'
   * - 'raw_voicemail'
   */
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $result = new \Result;

    $customer_id = $parameters['customer_id'];

    $command = 'UpdatePlanAndFeatures';

    // invoke QuerySubscriber to get the SOCs currently set for this customer
    $resultQuerySubscriber = $this->getDataFromQuerySubscriber($parameters);

    if ( $resultQuerySubscriber->is_failure() )
      return $resultQuerySubscriber;

    $dataQuerySubscriber = $this->extractDataBodyFromMessage( $resultQuerySubscriber->data_array['message'] );

    dlog('',"dataQuerySubscriber = %s",$dataQuerySubscriber);

    $addOnFeatureInfo = ( isset($dataQuerySubscriber['AddOnFeatureInfoList']) )
                        ?
                        $dataQuerySubscriber['AddOnFeatureInfoList']->AddOnFeatureInfo
                        :
                        array() // STANDBY
                        ;

    dlog('',"resultQuerySubscriber addOnFeatureInfo = %s",$addOnFeatureInfo);

    $keepDataSocs = ! ! ( isset($parameters['keepDataSocs']) && $parameters['keepDataSocs'] );

    $parameters = $this->addRetailPlanIDFromUltraPlan( $parameters );

    // logic similar to parametersCommandNonDataAdd

    // SOCs in the current customer plan
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($parameters['ultra_plan_name'], $customer_id);
    $throttleSpeed = \Ultra\MvneConfig\detectThrottleSpeed($addOnFeatureInfo);
    \Ultra\MvneConfig\adjustPlanConfigForThrottle($accSocsPlanConfig, $throttleSpeed);

    // determine the voicemail SOCs

    if ( is_english( $parameters['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    if ( ( $parameters['preferredLanguage'] == 'es-US' ) || ( $parameters['preferredLanguage'] == 'es' ) )
      $accSocsPlanConfig[ 'v_spanish' ] = '';

    $languageSocs = array('v_english','v_spanish');
    $socToAdd     = NULL;
    $socsToRemove = array();

    if ( ! empty( $parameters['raw_voicemail'] ) )
    {
      if ( $parameters['raw_voicemail'] == 'remove' )
        $socsToRemove = $languageSocs;
      elseif( in_array( $parameters['raw_voicemail'] , $languageSocs ) )
      {
        if ( $parameters['raw_voicemail'] == 'v_english' )
        {
          $socToAdd     = 'v_english';
          $socsToRemove = array('v_spanish');
        }
        else
        {
          $socToAdd     = 'v_spanish';
          $socsToRemove = array('v_english');
        }
      }
    }

    dlog('',"accSocsPlanConfig = %s",$accSocsPlanConfig);

    // configure SOCS for UpdatePlanAndFeatures request

    foreach( $addOnFeatureInfo as $addOnFeature )
    {
      $ultraSocName = $this->ultraSocsByPlanId[ $addOnFeature->FeatureID ];

      dlog('',"ultraSocName = %s",$ultraSocName);
      dlog('',"addOnFeature = %s",$addOnFeature);

      // we wanted to added $socToAdd, but we already have it
      if ( $socToAdd == $ultraSocName )
        $socToAdd = NULL;

      // we want to remove $ultraSocName
      if ( in_array( $ultraSocName , $socsToRemove ) )
        $parameters['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => 'REMOVE',
            'autoRenew'    => 'false',
            'FeatureID'    => $addOnFeature->FeatureID,
            'FeatureValue' => ''
          );
      else
      {
        $action = 'RETAIN';
        $value  = '';

        // check 'raw_b_voice' and 'raw_b_sms' if provided

        $rawSocs = array('b_voice','b_sms');

        foreach( $rawSocs as $rawSoc ) 
        {
          if ( ( $ultraSocName == $rawSoc )
            && ! empty($parameters['raw_'.$rawSoc])
            && is_numeric($parameters['raw_'.$rawSoc])
            && ( $parameters['raw_'.$rawSoc] > 0 ) 
          )
          {
            $action = 'RESET';
            $value  = $parameters['raw_'.$rawSoc];
          }
        }

        if ( ! $value && ! empty($accSocsPlanConfig[ $ultraSocName ]) )
          $value = $accSocsPlanConfig[ $ultraSocName ];

        if ( ! \Ultra\MvneConfig\isWholesalePlanSOC( $ultraSocName )
          && ! \Ultra\MvneConfig\isRetailPlanSOC( $ultraSocName )
        ) // ignore retailPlanID and Wholesale in this context
          $parameters['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
            array(
              'Action'       => $action,
              'autoRenew'    => 'false',
              'FeatureID'    => $addOnFeature->FeatureID,
              'FeatureValue' => $value
            );
      }
    }

    if ( $socToAdd )
    {
      $parameters['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        array(
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $socToAdd , 'plan_id' ),
          'FeatureValue' => ''
        );
    }

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addAutoRenew($parameters);

    $parameters['PlanData']['wholesalePlan'] = $parameters['wholesalePlan'];

    $parameters = $this->addResourceData($parameters);

    unset( $parameters['ICCID'] );
    unset( $parameters['customer_id'] );
    unset( $parameters['option'] );
    unset( $parameters['preferredLanguage'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['wholesalePlan'] );
    unset( $parameters['raw_b_voice'] );
    unset( $parameters['raw_b_sms'] );
    unset( $parameters['raw_voicemail'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}

?>
