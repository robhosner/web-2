<?php

require_once 'core.php';
require_once 'cosid_constants.php';
require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';
require_once 'classes/RoamCreditPromo.php';

class ControlCommandUpdatePlanAndFeaturesTest
{
  public $params  = null;
  public $control = null;
  public $balance = null;

  public $errors = [];

  public function __construct()
  {
    $actionUUID    = 'TEST' . time();
    $this->control = new \Ultra\Lib\MiddleWare\ACC\ControlCommandUpdatePlanAndFeatures( $actionUUID );

    $this->balance = (array)json_decode('{"voice_minutes":8250,"sms_messages":9000,"roaming_balance":"1150","a_data_blk1536_plan_used":"0","a_data_blk1536_plan_limit":"100","a_data_blk1536_used":"0","a_data_blk1536_limit":"500"}');

    $this->flexBalance = (array)json_decode('{"voice_minutes":8250,"sms_messages":9000,"roaming_balance":"1150","a_data_pool_1_used": "0","a_data_pool_1_limit":"500"}');
  }

  public function userData()
  {
    return [
      'channelId' => 'ULTRA',
      'senderId'  => 'MVNEACC',
      'timeStamp' => date('c')
    ];
  }

  public function runTests()
  {
    try
    {
      if (1)
      {
        $this->test_getAvailableCommands();
        $this->test_addThrottleInfo();
        $this->test_isLatamSoc();
        $this->test_setBalance();
        $this->test_setAddOnFeatureInfo();
        $this->test_parametersCommandWFC();
        $this->test_parametersCommandChangePlan();
        $this->test_parametersCommandDataAdd();
        $this->test_parametersCommandSharedDataAdd();
        $this->test_parametersCommandMintDataAddOn();
        $this->test_parametersCommandMintDataRemove();
        $this->test_parametersCommandMintSuspend();
        $this->test_parametersCommandUpdateThrottleSpeedBan();
        $this->test_parametersCommandNonDataAdd();
        $this->test_parametersCommandVoicemail();
        $this->test_parametersCommandReset();
      }

      if (0)
      {
        $this->test_getCustomerBanByCustomerId();
        $this->test_getAddOnFeatureInfo();
      }
    }
    catch (\Exception $e)
    {
      $this->errors[] = $e->getMessage();
    }

    echo "------\nERRORS\n------\n";
    print_r($this->errors);
  }

  public function test_getCustomerBanByCustomerId()
  {
    $ban = $this->control->getCustomerBanByCustomerId(33159);
    if ($ban != '10000000')
      throw new \Exception('trouble getting ban from shared data service');
  }

  public function test_addResourceData()
  {
    //
  }

  public function test_getAvailableCommands()
  {
    $cmds = $this->control->getAvailableCommands();
    if ( ! is_array($cmds) || ! in_array('throttlespeedban', $cmds))
      throw new \Exception('error using getAvailableCommands');
  }

  public function test_setAddOnFeatureInfo()
  {
    $this->control->setAddOnFeatureInfo('X');
    if ($this->control->addOnFeatureInfo != 'X')
      throw new \Exception('error setting addOnFeatureInfo member variable');
  }

  public function test_setBalance()
  {
    $this->control->setBalance('X');
    if ($this->control->balance != 'X')
      throw new \Exception('error setting balance member variable');
  }

  public function test_getAddOnFeatureInfo()
  {
    $addOnFeatureInfo = $this->control->getAddOnFeatureInfo([
      'MSISDN'   => '6573219415',
      'UserData' => $this->userData()
    ]);

    if ( ! is_array($addOnFeatureInfo) || ! isset($addOnFeatureInfo[0]->FeatureID))
      throw new \Exception('error getting add on feature info from ACC');
  }

  public function test_addThrottleInfo()
  {
    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $params = $this->control->addThrottleInfo([]);
    if ( ! isset($params['current_throttle']) || $params['current_throttle'] != '1536')
      throw new \Exception('adding throttle info');
  }

  public function test_isPaidEvent()
  {
    //
  }

  public function test_getMintSocName()
  {
    //
  }

  public function test_hasDataSoc()
  {
    //
  }

  public function test_addParametersForOption()
  {
    //
  }

  public function test_isLatamSoc()
  {
    $x = $this->control->isLatamSoc('n_roam_latam');
    $y = $this->control->isLatamSoc('not_a_soc');

    if ( ! $x) throw new \Exception('detecting latam roaming soc (1)');
    if (!! $y) throw new \Exception('detecting latam roaming soc (2)');
  }

  public function test_parametersCommandWFC()
  {
    $params = [
      'brand_id'        => 1,
      'option'          => 'N-WFC|ADD',
      'ultra_plan_name' => 'STWENTY_NINE',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);
    $params = $this->control->addParametersForOption($params);

    $success  = false;

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error adding WFC soc (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    foreach ($features as $feature)
      if ($feature['FeatureID'] == '12809' && $feature['Action'] == 'ADD')
        $success = true;

    if ( ! $success)
      throw new Exception('error adding WFC soc (2)');
  }

  public function test_parametersCommandChangePlan()
  {
    $params = [
      'brand_id'        => 1,
      'customer_id'     => 33481,
      'option'          => 'CHANGE|S44',
      'ultra_plan_name' => 'STWENTY_NINE',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error change plan (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success1 = false;
    $success2 = false;

    foreach ($features as $feature)
    {
      if ($feature['FeatureID'] == '12911' && $feature['Action'] == 'ADD')
        $success1 = true;

      if ($feature['FeatureID'] == '12917' && $feature['Action'] == 'RESET')
        $success2 = true;
    }

    if ( ! $success1 || ! $success2)
      throw new Exception('error change plan (2)');
  }

  public function test_parametersCommandDataAdd()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33481,
      'option'            => 'A-DATA-BLK|500',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'STWENTY_NINE',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $this->control->setBalance($this->balance);

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error data add (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12918' 
        && $feature['Action']       == 'ADD'
        && $feature['FeatureValue'] == 500
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error data add (2)');
  }

  public function test_parametersCommandSharedDataAdd()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 1,
      'option'            => 'A-DATA-POOL-1|100',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'ULTRA_FLEX_FIFTEEN',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->flexAddOnFeatureInfo());
    $this->control->setBalance($this->flexBalance);

    $params = $this->control->addRetailPlanIDFromUltraPlan($params);
    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error shared data add (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12924' 
        && $feature['Action']       == 'RESETSHARE'
        && $feature['FeatureValue'] == 600
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error shared data add (2)');
  }

  public function test_parametersCommandMintDataRemove()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 1,
      'option'            => 'REMOVE-DATA-MINT|A-DATA-MINT|1',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'MINT_ONE_MONTH_S',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->mintAddOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error mint data remove (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12913' 
        && $feature['Action']       == 'REMOVE'
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error mint data remove (2)');
  }

  public function test_parametersCommandMintDataAddOn()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 1,
      'option'            => 'A-DATA-MINT|1',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'MINT_ONE_MONTH_S',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error mint data add (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12913' 
        && $feature['Action']       == 'ADD'
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error mint data add (2)');
  }

  public function test_parametersCommandMintSuspend()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 1,
      'option'            => 'MINT-SUSPEND',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'MINT_ONE_MONTH_S',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->mintAddOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);
    $params = $this->control->addParametersForOption($params);

    print_r($params);
  }

  public function test_parametersCommandUpdateThrottleSpeed()
  {
    //
  }

  public function test_parametersCommandUpdateThrottleSpeedBan()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 1,
      'option'            => 'THROTTLE-SPEED-BAN|1536',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'ULTRA_FLEX_FIFTEEN',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->flexAddOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);
    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error update throttle speed for ban (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12931' 
        && $feature['Action']       == 'ADD'
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error update throttle speed for ban (2)');
  }

  public function test_parametersCommandNonDataAdd()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33481,
      'option'            => 'ROAM|123',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'STWENTY_NINE',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error non data add of roam credit (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12707' 
        && $feature['Action']       == 'INCREMENT'
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error non data add of roam credit (2)');
  }

  public function test_parametersCommandVoicemail()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33481,
      'option'            => 'V-SPANISH',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'STWENTY_NINE',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error change voicemail soc (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success1 = false;
    $success2 = false;

    foreach ($features as $feature)
    {
      if ($feature['FeatureID'] == '12803' && $feature['Action'] == 'ADD')
        $success1 = true;

      if ($feature['FeatureID'] == '12802' && $feature['Action'] == 'REMOVE')
        $success2 = true;
    }

    if ( ! $success1 || ! $success2)
      throw new Exception('error change voicemail soc (2)');
  }

  public function test_parametersCommandReset()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33481,
      'option'            => 'RESET',
      'preferredLanguage' => 'EN',
      'reset_config'      => ['a_voicesmswallet_ir' => 135],
      'ultra_plan_name'   => 'STWENTY_NINE',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());

    $params = $this->control->addThrottleInfo($params);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);

    $params = $this->control->addParametersForOption($params);

    if ( ! $params || ! isset($params['PlanData']['AddOnFeatureList']['AddOnFeature']))
      throw new Exception('error using RESET command (1)');

    $features = $params['PlanData']['AddOnFeatureList']['AddOnFeature'];

    $success  = false;

    foreach ($features as $feature)
    {
      if ( $feature['FeatureID']    == '12707' 
        && $feature['Action']       == 'RESET'
        && $feature['FeatureValue'] == '135'
      ) {
        $success = true;
      }
    }

    if ( ! $success)
      throw new Exception('error using RESET command (2)');
  }

  public function flexBalanceValues()
  {
    $flexBalanceValues = '[{"Value":8250,"Type":"DA1","Unit":"Unit","Name":"Voice Base bucket","UltraType":"DA1","UltraName":"Voice Minutes"},{"Value":9000,"Type":"DA2","Unit":"Unit","Name":"SMS Base bucket","UltraType":"DA2","UltraName":"SMS Messages"},{"Value":"0","Type":"Total Data Usage","Unit":"KB","Name":"Used","UltraType":"Total Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Used","UltraType":"Base Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Limit","UltraType":"Base Data Usage Limit","UltraName":""},{"Value":"0","Type":"IN Rated Data Usage","Unit":"KB","Name":"Used","UltraType":"IN Rated Data Usage Used","UltraName":""},{"Value":"0","Type":"WHPADJB1","Unit":"KB","Name":"Used","UltraType":"WHPADJB1 Used","UltraName":""},{"Value":"0","Type":"WHPADJB1-BAN","Unit":"KB","Name":"Used","UltraType":"WHPADJB1-BAN Used","UltraName":""},{"Value":7000,"Type":"WHPADJB1-BAN","Unit":"MB","Name":"Limit","UltraType":"WHPADJB1-BAN Limit","UltraName":""}]';
    return json_decode($flexBalanceValues);
  }

  public function balanceValues()
  {
    $balancesValues = '[{"Value":8250,"Type":"DA1","Unit":"Unit","Name":"Voice Base bucket","UltraType":"DA1","UltraName":"Voice Minutes"},{"Value":9000,"Type":"DA2","Unit":"Unit","Name":"SMS Base bucket","UltraType":"DA2","UltraName":"SMS Messages"},{"Value":"500","Type":"DA7","Unit":"Unit","Name":"Voice - Add on","UltraType":"DA7","UltraName":""},{"Value":"0","Type":"Total Data Usage","Unit":"KB","Name":"Used","UltraType":"Total Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Used","UltraType":"Base Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Limit","UltraType":"Base Data Usage Limit","UltraName":""},{"Value":"0","Type":"IN Rated Data Usage","Unit":"KB","Name":"Used","UltraType":"IN Rated Data Usage Used","UltraName":""},{"Value":"0","Type":"IN Rated Data Usage","Unit":"KB","Name":"Used","UltraType":"IN Rated Data Usage Used","UltraName":""},{"Value":"0","Type":"WPRBLK33S","Unit":"KB","Name":"Used","UltraType":"WPRBLK33S Used","UltraName":""},{"Value":"100","Type":"WPRBLK33S","Unit":"MB","Name":"Limit","UltraType":"WPRBLK33S Limit","UltraName":""}]';

    return json_decode($balancesValues);
  }

  public function addOnFeatureInfo()
  {
    $addOnFeatureInfo = '[{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12802","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"Voicemail English","UltraServiceName":"V-ENGLISH"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12701","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"Voice Minutes","UltraServiceName":"B-VOICE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12702","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"SMS Messages","UltraServiceName":"B-SMS"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12902","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"4G data, then block","UltraServiceName":"B-DATA-BLK"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12808","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"LTE Access","UltraServiceName":"N-LTE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12707","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"INTL Roaming Voice + SMS Wallet","UltraServiceName":"A-VOICESMSWALLET-IR"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12917","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"4G Data, block after n Mb","UltraServiceName":"A-DATA-BLK1536-PLAN"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12814","ExpirationDate":"2020-08-24T00:00:00","UltraDescription":"Simple Global International Roaming (No Domestic Roaming)","UltraServiceName":"N-ROAM-SMPGLB"},{"FeatureID":"12006","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Retail Plan #1","UltraServiceName":"R-UNLIMITED"},{"FeatureID":"111","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Secondary Wholesale","UltraServiceName":"W-SECONDARY"}]';

    return json_decode($addOnFeatureInfo);
  }

  public function mintAddOnFeatureInfo()
  {
    $addOnFeatureInfo = '[{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12802","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"Voicemail English","UltraServiceName":"V-ENGLISH"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12701","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"Voice Minutes","UltraServiceName":"B-VOICE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12702","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"SMS Messages","UltraServiceName":"B-SMS"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12902","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"4G data, then block","UltraServiceName":"B-DATA-BLK"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12808","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"LTE Access","UltraServiceName":"N-LTE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12707","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"INTL Roaming Voice + SMS Wallet","UltraServiceName":"A-VOICESMSWALLET-IR"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12917","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"4G Data, block after n Mb","UltraServiceName":"A-DATA-BLK1536-PLAN"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12814","ExpirationDate":"2020-08-24T00:00:00","UltraDescription":"Simple Global International Roaming (No Domestic Roaming)","UltraServiceName":"N-ROAM-SMPGLB"},{"FeatureID":"12913","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"4G Data. 1Gb. Mint Only. (WPRBLK25) BILLABLE","UltraServiceName":"A-DATA-MINT-1"},{"FeatureID":"111","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Secondary Wholesale","UltraServiceName":"W-SECONDARY"}]';

    return json_decode($addOnFeatureInfo);
  }

  public function flexAddOnFeatureInfo()
  {
    $flexAddOnFeatureInfo = '[{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12802","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"Voicemail English","UltraServiceName":"V-ENGLISH"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12701","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"Voice Minutes","UltraServiceName":"B-VOICE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12702","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"SMS Messages","UltraServiceName":"B-SMS"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12902","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"4G data, then block","UltraServiceName":"B-DATA-BLK"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12707","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"INTL Roaming Voice + SMS Wallet","UltraServiceName":"A-VOICESMSWALLET-IR"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12814","ExpirationDate":"2020-08-24T00:00:00","UltraDescription":"Simple Global International Roaming (No Domestic Roaming)","UltraServiceName":"N-ROAM-SMPGLB"},{"FeatureID":"12006","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Retail Plan #1","UltraServiceName":"R-UNLIMITED"},{"FeatureID":"111","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Secondary Wholesale","UltraServiceName":"W-SECONDARY"},{"FeatureID":"12924","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"4G data, then cutoff data","UltraServiceName":"A-DATA-POOL-1"}]';

    return json_decode($flexAddOnFeatureInfo);
  }
}

$test = new ControlCommandUpdatePlanAndFeaturesTest();
$test->runTests();
