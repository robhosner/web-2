<?php

require_once 'core.php';
require_once 'cosid_constants.php';
require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';

class ControlCommandManageCap_test
{
  public $params  = null;
  public $control = null;
  public $balance = null;

  public function __construct()
  {
    $actionUUID    = 'TEST' . time();

    $this->control = new \Ultra\Lib\MiddleWare\ACC\ControlCommandManageCap($actionUUID);

    $this->balance = (array)json_decode('{
      "voice_minutes":8250,
      "sms_messages":9000,
      "roaming_balance":"1150",
      "a_data_pool_1_used": "0",
      "a_data_pool_1_limit":"500"}
    ');

    print_r($this->balance);
  }

  public function addOnFeatureInfo()
  {
    $addOnFeatureInfo = '[{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12802","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"Voicemail English","UltraServiceName":"V-ENGLISH"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12701","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"Voice Minutes","UltraServiceName":"B-VOICE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12702","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"SMS Messages","UltraServiceName":"B-SMS"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12902","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"4G data, then block","UltraServiceName":"B-DATA-BLK"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12707","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"INTL Roaming Voice + SMS Wallet","UltraServiceName":"A-VOICESMSWALLET-IR"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12814","ExpirationDate":"2020-08-24T00:00:00","UltraDescription":"Simple Global International Roaming (No Domestic Roaming)","UltraServiceName":"N-ROAM-SMPGLB"},{"FeatureID":"12006","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Retail Plan #1","UltraServiceName":"R-UNLIMITED"},{"FeatureID":"111","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Secondary Wholesale","UltraServiceName":"W-SECONDARY"},{"FeatureID":"12924","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"4G data, then cutoff data","UltraServiceName":"A-DATA-POOL-1"}]';

    return json_decode($addOnFeatureInfo);
  }

  public function userData()
  {
    return [
      'channelId' => 'ULTRA',
      'senderId'  => 'MVNEACC',
      'timeStamp' => date('c')
    ];
  }

  public function execute()
  {
    // $this->test__addCapShare();
    // $this->test__removeCapShare();
    $this->test__processCommand();
  }

  public function test__removeCapShare()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33413,
      'MSISDN'            => '9495015751',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'ULTRA_FLEX_FIFTEEN',
      'command'  => 'add',
      'percent'  => '50',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $this->control->setBalance($this->balance);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);
    $params = $this->control->removeCapShare($params);

    print_r($params);
  }

  public function test__addCapShare()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33413,
      'MSISDN'            => '9495015751',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'ULTRA_FLEX_FIFTEEN',
      'command'  => 'add',
      'percent'  => '50',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $this->control->setBalance($this->balance);
    $params = $this->control->addRetailPlanIDFromUltraPlan($params);
    $params = $this->control->addCapShare($params);

    print_r($params);
  }

  public function test__processCommand()
  {
    $params = [
      'brand_id'          => 1,
      'customer_id'       => 33413,
      'MSISDN'            => '9495015751',
      'preferredLanguage' => 'EN',
      'ultra_plan_name'   => 'ULTRA_FLEX_FIFTEEN',
      'command'  => 'add',
      'percent'  => '50',
      'UserData' => $this->userData()
    ];

    $this->control->setAddOnFeatureInfo($this->addOnFeatureInfo());
    $this->control->setBalance($this->balance);
    $this->control->processCommand($params);
  }
}

$manageCapTests = new ControlCommandManageCap_test();
$manageCapTests->execute();
