<?php


namespace Ultra\Lib\MiddleWare\ACC;


include_once('classes/CommandInvocation.php');
include_once('classes/Result.php');
require_once('db/ultra_acc/command_error_codes.php');
include_once('lib/provisioning/functions.php');
include_once('lib/util-common.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandManageCap.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandPortIn.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandPortInMvne1To2.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandQuerySubscriber.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandRenewPlan.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandRenewPlanRaw.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandUpdatePlanAndFeatures.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandUpdatePlanAndFeaturesRaw.php');
include_once('Ultra/Lib/MiddleWare/ACC/ControlCommandUpdateSubscriberAddress.php');
include_once('Ultra/Lib/MQ/ControlChannel.php');
require_once('Ultra/Lib/Util/Redis/SendSMS.php');
require_once('Ultra/Lib/MVNE/MakeItSo/Validator.php');
require_once 'classes/Throttling.php';


/*
PHP classes which implement ACC middleware logic for control commands.

ControlCommand% classes contain ACC specific logic.
*/


class Control
{
  private $actionUUID;
  private $command;
  public  $controlObject;
  public  $redis;

  function __destruct()
  {
  }

  /**
   * \Ultra\Lib\MiddleWare\ACC\Control constructor
   */
  public function __construct()
  {
  }

  /**
   * checkTimeout
   *
   * Returns TRUE if the message did not timeout.
   * Returns FALSE if the message did timeout.
   * "a message's contents have a practical limit for how long they're useful."
   *
   * @return boolean
   */
  public function checkTimeout($params)
  {
    dlog('',"params = %s",$params);

    $secondsDiff = time() - $params['timestamp'] ;

    \logDebug("secondsDiff = $secondsDiff");

    $requestReplyTimeoutSeconds = \Ultra\UltraConfig\channelsDefaultReplyTimeoutSeconds();

    if ( $requestReplyTimeoutSeconds <= $secondsDiff )
      return \logError("Message expired after $secondsDiff seconds");

    return TRUE;
  }

  /**
   * processControlCommand
   *
   * Main method for processing a Control Command
   *
   * @return object of class \Result
   */
  public function processControlCommand($params)
  {
    dlog('',"params = %s",$params);

    if ( !isset($params['parameters']) )
      return make_error_Result('No parameters given!');

    if ( !isset($params['timestamp']) )
      $params['timestamp'] = time();

    $this->actionUUID      = empty( $params['actionUUID'] ) ? gethostname() . getmypid() . time() : $params['actionUUID'] ;
    $this->command         = $params['command'];
    $parameters            = $params['parameters'];

    if ( ! $this->checkTimeout($params) )
    {
      return make_timeout_Result('MW Message timeout');
    }

    // this validation is not command-specific
    $result = $this->validateUltraParameters($parameters);

    if ( $result->is_success() )
    {
      // map adapter parameters to middleware parameters
      $parameters = $this->mapParameters($parameters);

      // add parameters common for all commands
      $parameters = $this->addDefaultParameters($parameters);

      // Class which will execute the command
      $controlClass = '\Ultra\Lib\MiddleWare\ACC\ControlCommand'.$this->command;

      if ( class_exists( $controlClass ) )
      {
        dlog('',"processControlCommand class = $controlClass");

        // checks if the command is blocked by previous pending commands
        if ( $this->blockOccupied( $parameters ) )
        {
          // There is a pending ACC asynchronous API call

          $controlChannel = new \Ultra\Lib\MQ\ControlChannel;

          $result->add_data_array(
            array(
              'message' => $controlChannel->buildMessage(
                array(
                  'header'     => $this->command,
                  'body'       => array(
                    'success'       => FALSE,
                    'errors'        => array( 'There is a pending ACC asynchronous API call for customer_id '.$parameters['customer_id'] ),
                    'customer_id'   => $parameters['customer_id']
                  ),
                  'actionUUID' => $this->actionUUID
                )
              )
            )
          );

          $result->succeed();
        }
        else
        {
          if ( $this->blockBiphasic( $controlClass , $parameters ) )
          {
            // There is a pending Biphasic command

            $controlChannel = new \Ultra\Lib\MQ\ControlChannel;

            $result->add_data_array(
              array(
                'message' => $controlChannel->buildMessage(
                  array(
                    'header'     => $this->command,
                    'body'       => array(
                      'success'       => FALSE,
                      'errors'        => array( 'There is a pending ACC asynchronous API call for customer_id '.$parameters['customer_id'] ),
                      'customer_id'   => $parameters['customer_id']
                    ),
                    'actionUUID' => $this->actionUUID
                  )
               )
              )
            );

            $result->succeed();
          }
          else
          {
            // Factory
            $this->controlObject = new $controlClass( $this->actionUUID );

            $makeitsoQueueId = empty( $params['makeitsoQueueId'] ) ? 0 : $params['makeitsoQueueId'];
            $this->controlObject->setMakeitsoQueueId($makeitsoQueueId);

            $result = $this->controlObject->processCommand($parameters);

            $this->releaseBiphasic( $controlClass , $parameters );
          }
        }
      }
      else
        $result->add_error( __CLASS__ . " control command ".$params['command']." not handled" );
    }

    // this is successful unless something extremely unusual happens (data returned in bad formats, control command does not exist, etc...)
    return $result;
  }

  /**
   * releaseBiphasic
   *
   * Free a Biphasic command
   *
   * @return NULL
   */
  protected function releaseBiphasic( $controlClass , $parameters )
  {
    $shouldBlockBiphasic = ( isset($parameters['customer_id'])
                          && $parameters['customer_id']
                          && is_subclass_of( $controlClass , 'Ultra\Lib\MiddleWare\ACC\ControlCommandBiphasic' )
    );

    if ( $shouldBlockBiphasic )
    {
      if ( !isset($this->redis) || !$this->redis )
        $this->redis = new \Ultra\Lib\Util\Redis;

      $key = 'ultra/command/biphasic/'.$controlClass.'/customer_id/'.$parameters['customer_id'];

      $key = str_replace( '\\' , '/' , $key );

      $key = preg_replace('/\/+/' , '/' , $key );

      $this->redis->del( $key );
    }
  }

  /**
   * blockBiphasic
   *
   * Returns TRUE if the command is blocked by previous pending Biphasic commands for the same customer id
   *
   * @return boolean
   */
  protected function blockBiphasic( $controlClass , $parameters )
  {
    $shouldBlockBiphasic = ( isset($parameters['customer_id'])
                          && $parameters['customer_id']
                          && is_subclass_of( $controlClass , 'Ultra\Lib\MiddleWare\ACC\ControlCommandBiphasic' )
    );

    if ( $shouldBlockBiphasic )
    {
      if ( !isset($this->redis) || !$this->redis )
        $this->redis = new \Ultra\Lib\Util\Redis;

      $key = 'ultra/command/biphasic/'.$controlClass.'/customer_id/'.$parameters['customer_id'];

      $key = str_replace( '\\' , '/' , $key );

      $key = preg_replace('/\/+/' , '/' , $key );

      // there is currently a command invocation (using $controlClass) for this customer
      if ( $this->redis->get( $key ) )
        return TRUE;

      $this->redis->set( $key , 1 , 60*10 ); // 10 minutes
    }

    return FALSE;
  }

  /**
   * blockOccupied
   *
   * Returns TRUE if the command is blocked by previous pending commands
   *
   * @return boolean
   */
  private function blockOccupied( $parameters )
  {
    if ( ! $this->shouldCheckOccupied() )
      return FALSE;

    if ( ! $parameters['customer_id'] )
    {
      dlog('','PROBLEM: blockOccupied invoked with no customer_id');

      return FALSE;
    }

    // Check if already being locked (A2)

    $commandInvocationObject = new \CommandInvocation(
      array(
       'customer_id' => $parameters['customer_id']
      )
    );

    return $commandInvocationObject->isOccupied();
  }

  /**
   * shouldCheckOccupied
   *
   * Returns TRUE if the command can be blocked by previous pending commands
   * Those Amdocs commands are the ones for which we expect late Asynchronous Notifications
   *
   * @return boolean
   */
  private function shouldCheckOccupied()
  {
    $biphasicCommands = $this->biphasicCommands();

    $shouldCheckOccupied = in_array( $this->command , $biphasicCommands );

    dlog('',"shouldCheckOccupied = $shouldCheckOccupied");

    return $shouldCheckOccupied;
  }

  /**
   * biphasicCommands
   *
   * Those Amdocs commands are the ones for which we expect late Asynchronous Notifications
   *
   * @return array
   */
  public function biphasicCommands()
  {
    return array(
      'ActivateSubscriber',
      'DeactivateSubscriber', // revert with ReactivateSubscriber
      'ReactivateSubscriber', // revert with DeactivateSubscriber
      'RenewPlan',
      'RenewPlanRaw',
      'RestoreSubscriber',    // revert with SuspendSubscriber
      'SuspendSubscriber',    // revert with RestoreSubscriber
      'UpdatePlanAndFeatures',
      'UpdatePlanAndFeaturesRaw',
      'UpdateSubscriberFeature', // (not a native Amdocs API)
      'ChangeMSISDN',
      'ChangeSIM',
      'PortIn',
      'ResendOTA',
      'ResetVoiceMailPassword',
      'UpdateSubscriberAddress',
      'ManageCap'
    );
  }

  /**
   * validateUltraParameters
   *
   * Validation layer
   *
   * @return object of class \Result
   */
  private function validateUltraParameters($parameters)
  {
    $result = new \Result();

    if ( ( ! $parameters ) || ( ! is_array($parameters) ) || ( ! count($parameters) ) )
    {
      $result->add_error( __CLASS__ . " : No parameters given" );
    }
    else
    {
      // there should be no need to perform further validation here,
      // the right place is \Ultra\Lib\MiddleWare\Adapter\Control::validateMWCall()

      $result->succeed();
    }

    return $result;
  }

  /**
   * mapParameters
   *
   * Mapping / translation layer
   *
   * @return array
   */
  private function mapParameters($parameters)
  {
    #dlog('',"mapParameters input = %s",$parameters);

    $mappedParameters = array();

    if ( isset($parameters['customer_id']) )
      $mappedParameters['customer_id']          = $parameters['customer_id'];

    if ( isset($parameters['msisdn']) )
      $mappedParameters['MSISDN']               = $parameters['msisdn'];

    if ( isset($parameters['iccid']) )
      $mappedParameters['ICCID']                = \luhnenize( $parameters['iccid'] );

    if ( isset($parameters['old_iccid']) )
      $mappedParameters['OLD_ICCID']            = \luhnenize( $parameters['old_iccid'] );

    if ( isset($parameters['new_iccid']) )
      $mappedParameters['NEW_ICCID']            = \luhnenize( $parameters['new_iccid'] );

    if ( isset($parameters['imei']) )
      $mappedParameters['IMEI']                 = $parameters['imei'];

    if ( isset($parameters['new_imei']) )
      $mappedParameters['NEW_IMEI']             = $parameters['new_imei'];

    if ( isset($parameters['zipcode']) )
      $mappedParameters['zipCode']              = $parameters['zipcode'];

    if ( isset($parameters['full_name']) )
      $mappedParameters['fullName']             = $parameters['full_name'];

    if ( isset($parameters['first_name']) )
      $mappedParameters['firstName']            = $parameters['first_name'];

    if ( isset($parameters['last_name']) )
      $mappedParameters['lastName']             = $parameters['last_name'];

    if ( isset($parameters['address_1']) )
      $mappedParameters['Address']              = $parameters['address_1'];

    if ( isset($parameters['address_2']) )
      $mappedParameters['Address2']             = $parameters['address_2'];

    if ( isset($parameters['state']) )
      $mappedParameters['stateCode']            = $parameters['state'];

    if ( isset($parameters['preferred_language']) )
    {
      $mappedParameters['preferredLanguage']    = strtolower( $parameters['preferred_language'] );

      // ACC only supports 'en' and 'es'
      if ( $mappedParameters['preferredLanguage'] == 'zh' )
        $mappedParameters['preferredLanguage'] = 'en';
    }

    if ( isset($parameters['port_account']) )
      $mappedParameters['donorAccountNumber']   = $parameters['port_account'];

    if ( isset($parameters['port_password']) )
      $mappedParameters['donorAccountPassword'] = $parameters['port_password'];

    if ( isset($parameters['city']) )
      $mappedParameters['cityName']             = $parameters['city'];

    if ( isset($parameters['wholesale_plan']) )
      $mappedParameters['wholesalePlan']        = $this->mapWholesalePlan( $parameters['wholesale_plan'] );

    if ( isset($parameters['qualityOfService']) )
      $mappedParameters['qualityOfService']     = $parameters['qualityOfService'];

    if ( isset($parameters['sms_text']) )
      $mappedParameters['smsText']              = cleanup_acc_sms_text($parameters['sms_text']);

    if ( isset($parameters['option']) )
      $mappedParameters['option']               = $parameters['option'];

    if ( isset($parameters['json_parameters']) )
      $mappedParameters['json_parameters']      = $parameters['json_parameters'];

    if ( isset($parameters['keepDataSocs']) )
      $mappedParameters['keepDataSocs']         = $parameters['keepDataSocs'];

    if ( isset($parameters['_attempt_count']) )
      $mappedParameters['_attempt_count']       = $parameters['_attempt_count'];

    if ( isset($parameters['_attempt_timestamp']) )
      $mappedParameters['_attempt_timestamp']   = $parameters['_attempt_timestamp'];

    if ( isset($parameters['addOnFeatureListRaw']) )
      $mappedParameters['addOnFeatureListRaw']  = $parameters['addOnFeatureListRaw'];

    if ( isset($parameters['raw_b_voice']) )
      $mappedParameters['raw_b_voice']          = $parameters['raw_b_voice'];

    if ( isset($parameters['raw_b_sms']) )
      $mappedParameters['raw_b_sms']            = $parameters['raw_b_sms'];

    if ( isset($parameters['raw_voicemail']) )
      $mappedParameters['raw_voicemail']        = $parameters['raw_voicemail'];

    if ( isset($parameters['E911Address']) )
      $mappedParameters['E911Address']          = $parameters['E911Address'];

    if ( isset($parameters['override_default_imei']) )
      $mappedParameters['override_default_imei'] = $parameters['override_default_imei'];

    // Example: L19 => NINETEEN
    // 'ultra_plan' and 'subplan' are used to derive other fields
    if ( isset($parameters['ultra_plan']) )
      $mappedParameters['ultra_plan_name'] = ( $parameters['ultra_plan'] == 'STANDBY' )
                                             ?
                                             'STANDBY'
                                             :
                                             get_plan_name_from_short_name( $parameters['ultra_plan'] );

    if ( isset($parameters['subplan']) )
      $mappedParameters['subplan']              = $parameters['subplan'];

    if ( isset($parameters['brand_id']) )
      $mappedParameters['brand_id']             = $parameters['brand_id'];

    // API-451: forced plan configuration
    if (isset($parameters['reset_config']))
      $mappedParameters['reset_config']         = $parameters['reset_config'];

    if (isset($parameters['throttle_speed']))
      $mappedParameters['throttle_speed'] = $parameters['throttle_speed'];

    // maps the roam credit promo param
    $roamCreditPromoParam = \Ultra\Lib\RoamCreditPromo::getParam();
    if (isset($parameters[$roamCreditPromoParam]))
      $mappedParameters[$roamCreditPromoParam] = $parameters[$roamCreditPromoParam];

    dlog('',"mapParameters output = %s",$mappedParameters);

    return $mappedParameters;
  }

  /**
   * mapWholesalePlan
   *
   * Map wholesalePlan value
   *
   * @return string
   */
  private function mapWholesalePlan($wholesalePlan)
  {
    $wholesaleSOC = \Ultra\UltraConfig\getAccSocByName( $wholesalePlan );

    return $wholesaleSOC['plan_id'];
  }

  /**
   * addDefaultParameters
   *
   * Add parameters needed for all Control Commands
   *
   * @return array
   */
  private function addDefaultParameters($parameters)
  {
    $parameters['UserData']['channelId'] = 'ULTRA';
    $parameters['UserData']['senderId']  = 'MVNEACC';
    $parameters['UserData']['timeStamp'] = getTimeStamp();

    // remove the Z at the end of 'timeStamp'
    $parameters['UserData']['timeStamp'] = preg_replace( "/Z$/" , "" , $parameters['UserData']['timeStamp'] );

    return $parameters;
  }

}


interface ControlCommand
{
  public function processCommand($parameters);
}


class ControlCommandBase
{
  protected $controlChannel;
  protected $actionUUID;
  protected $makeitsoQueueId;
  protected $MSISDN;
  public    $dataArray;

  function __destruct()
  {
  }

  public function setMakeitsoQueueId($makeitsoQueueId)
  {
    $this->makeitsoQueueId = $makeitsoQueueId;
  }

  /**
   * \Ultra\Lib\MiddleWare\ACC\ControlCommand constructor instantiates a \Ultra\Lib\MQ\ControlChannel object
   */
  public function __construct($actionUUID)
  {
    $this->controlChannel = new \Ultra\Lib\MQ\ControlChannel;
    $this->actionUUID     = $actionUUID;
    $this->dataArray      = array();
  }

  /**
   * buildOutboundControlMessage
   *
   * Composes a message to be sent (outbound) to the SOAP client
   *
   * @return string - json-encoded message (an array)
   */
  protected function buildOutboundControlMessage( $command , $parameters , $paidEvent=0 , $socName=NULL )
  {
    $messageParams = [
      'actionUUID'      => $this->actionUUID,
      'header'          => $command,
      'body'            => $parameters,
      'makeitsoQueueId' => $this->makeitsoQueueId
    ];

    if ( ! empty($paidEvent) )
      $messageParams['paidEvent'] = $paidEvent;

    if ( ! empty($socName) )
      $messageParams['socName']   = $socName;

    return $this->controlChannel->buildMessage( $messageParams );
  }

  /**
   * buildInboundErrorMessage
   *
   * Composes a message to be sent back (inbound) with errors
   *
   * @return string - json-encoded message (an array)
   */
  protected function buildInboundErrorMessage( $command , $errors , $data )
  {
    return $this->controlChannel->buildMessage(
      array(
        'actionUUID' => $this->actionUUID,
        'header'     => $command,
        'body'       => array_merge(
          $data,
          array(
            'success'   => FALSE,
            'errors'    => $errors
          )
        )
      )
    );
  }

  /**
   * getPortRequestIdFromMSISDN
   *
   * Retrieve portRequestId from PORTIN_QUEUE
   *
   * @return string
   */
  protected function getPortRequestIdFromMSISDN( $portInQueue , $MSISDN )
  {
    $portRequestId = NULL;

    $result = $portInQueue->loadByMsisdn( $MSISDN );

    if ( $result->is_success()
      && property_exists( $portInQueue , 'port_request_id' )
      && $portInQueue->port_request_id
    )
      $portRequestId = $portInQueue->port_request_id;

    return array( $portInQueue , $portRequestId );
  }

  /**
   * translateAddOnsToUltraSocs
   *
   * Translates AddOnFeatureInfoList data to Ultra SOCs
   * Example:
   *   {
   *     "AutoRenewalIndicator":"false",
   *     "FeatureDate":"2014-02-27T12:26:21",
   *     "FeatureID":"12802",
   *     "ExpirationDate":"2020-03-22T00:00:00"
   *   },
   * =>
   *   {
   *     "AutoRenewalIndicator":"false",
   *     "FeatureDate":"2014-02-27T12:26:21",
   *     "FeatureID":"12802",
   *     "ExpirationDate":"2020-03-22T00:00:00",
   *     "UltraDescription":"Voicemail English",
   *     "UltraServiceName":"V-ENGLISH"
   *   }
   *
   * @return array
   */
  protected function translateAddOnsToUltraSocs( $addOnFeatureInfoList , $planId , $wholesalePlan )
  {
    if ( is_object($addOnFeatureInfoList->AddOnFeatureInfo) )
      $addOnFeatureInfoList->AddOnFeatureInfo = array($addOnFeatureInfoList->AddOnFeatureInfo);

    // include $planId in SOCs
    if ( $planId && is_numeric($planId) )
    {
      $plan = new \stdClass;
      $plan->FeatureID = $planId;
      $plan->FeatureDate = '';
      $plan->AutoRenewalIndicator = '';
      $plan->ExpirationDate = '';
      $addOnFeatureInfoList->AddOnFeatureInfo[] = $plan;
    }

    // include $wholesalePlan in SOCs
    if ( $wholesalePlan && is_numeric($wholesalePlan) )
    {
      $wholesale = new \stdClass;
      $wholesale->FeatureID = $wholesalePlan;
      $wholesale->FeatureDate = '';
      $wholesale->AutoRenewalIndicator = '';
      $wholesale->ExpirationDate = '';
      $addOnFeatureInfoList->AddOnFeatureInfo[] = $wholesale;
    }

    // loop through SOCs to set UltraDescription and UltraServiceName
    $n = count( $addOnFeatureInfoList->AddOnFeatureInfo );
    for ( $i = 0; $i < $n ; $i++ )
    {
      $ultraSoc = \Ultra\UltraConfig\getAccSocByPlanId( $addOnFeatureInfoList->AddOnFeatureInfo[ $i ]->FeatureID );

      $addOnFeatureInfoList->AddOnFeatureInfo[ $i ]->UltraDescription = $ultraSoc['description'];
      $addOnFeatureInfoList->AddOnFeatureInfo[ $i ]->UltraServiceName = $ultraSoc['ultra_service_name'];
    }

    return $addOnFeatureInfoList;
  }

  /**
   * isMintBrandMSISDN
   *
   * Returns TRUE if the given $MSISDN is associated to a MINT account
   *
   * @return boolean
   */
  public function isMintBrandMSISDN( $MSISDN )
  {
    teldata_change_db();

    $isMintBrandMSISDN = \Ultra\Lib\Util\validateMSISDNBrand( $MSISDN , 'MINT' );

    \logTrace("MSISDN = $MSISDN ; isMintBrandMSISDN = $isMintBrandMSISDN");

    \Ultra\Lib\DB\ultra_acc_connect();

    return $isMintBrandMSISDN;
  }

  /**
   * adjustBalanceValuesByMintBrand
   *
   * Mint specific logic for naming Data SOCs
   *
   * @return object
   */
  public function adjustBalanceValuesByMintBrand($newRow)
  {
    // A-DATA-MINT-3
    if ( $newRow->UltraType == 'WPRBLK20 Used' )
    {
      $newRow->UltraName = 'Mint Data Add-on Used';
      $newRow->UltraType = 'Mint WPRBLK20 Used';
    }

    // A-DATA-MINT-3
    if ( $newRow->UltraType == 'WPRBLK20 Limit' )
    {
      $newRow->UltraName = 'Mint Data Add-on Total';
      $newRow->UltraType = 'Mint WPRBLK20 Total';
    }

    // A-DATA-MINT-1
    if ( $newRow->UltraType == 'WPRBLK25 Used' )
    {
      $newRow->UltraName = 'Mint Data Add-on Used';
      $newRow->UltraType = 'Mint WPRBLK25 Used';
    }

    // A-DATA-MINT-1
    if ( $newRow->UltraType == 'WPRBLK25 Limit' )
    {
      $newRow->UltraName = 'Mint Data Add-on Total';
      $newRow->UltraType = 'Mint WPRBLK25 Total';
    }

    if ( $newRow->UltraType == '7007 Limit' )
    {
      $newRow->UltraType = 'Plan Data Limit';
      $newRow->UltraName = 'Total';
    }

    if ( $newRow->UltraType == '7007 Used' )
    {
      $newRow->UltraType = 'Plan Data Used';
      $newRow->UltraName = 'Used';
    }

    return $newRow;
  }

  /**
   * adjustBalanceValueList
   *
   * Strip out DA* unless it's DA1, DA2, DA14, DA15, or has a non-zero balance.
   * Adds UltraType and UltraName.
   * Do not include if 'NOT USED'.
   *
   * @return array
   */
  private function adjustBalanceValueList( $balanceValue )
  {
    $adjustedBalanceValue = array();

    // boolean flag to determine if MSISDN belongs to a MINT subscriber
    $isMintBrandMSISDN = $this->isMintBrandMSISDN( $this->MSISDN );

    foreach ( $balanceValue as $balanceValueRow )
    {
      $newRow = NULL;

      if ( $balanceValueRow->Type == 'WPRBLK30' )
        $balanceValueRow->Type = '7007';

      // DA\d+
      if ( preg_match("/^DA(.+)$/",$balanceValueRow->Type,$matches) )
      {
        if ( ( $matches[1] == '1' )
          || ( $matches[1] == '2' )
          #|| ( $matches[1] == '14' )
          #|| ( $matches[1] == '15' )
          || ( ( $balanceValueRow->Value != '' ) && ( $balanceValueRow->Value != '0' ) && ( $balanceValueRow->Value != '0.0' ) )
        )
        {
          $newRow = $balanceValueRow;

          // populate UltraType
          $newRow->UltraType = $balanceValueRow->Type;
        }

        if ( ( $matches[1] == '1' )
          || ( $matches[1] == '2' )
          || ( $matches[1] == '14' )
          || ( $matches[1] == '15' )
        )
          // truncate value to full integers
          $balanceValueRow->Value = floor( $balanceValueRow->Value );
      }
      elseif ( $balanceValueRow->Unit != 'DOLLARS' )
      {
        // patch for Name
        if ( is_object($balanceValueRow->Name) )
          $balanceValueRow->Name = '';

        $newRow = $balanceValueRow;

        // populate UltraType
        if ( $balanceValueRow->Name )
          $newRow->UltraType = $balanceValueRow->Type . ' ' . $balanceValueRow->Name;
        else
          $newRow->UltraType = $balanceValueRow->Type;
      }

      if ( $newRow )
      {
        // Normalize from GB and KB to MB
        $newRow = $this->adjustBalanceValueListMB($newRow);

        if ( $balanceValueRow->Type )
        {
          // populate UltraName
          $newRow->UltraName = \Ultra\UltraConfig\getAccBalanceConfig( $balanceValueRow->Type , $balanceValueRow->Name );

          // round data SOCs to 2 decimal points
          if ( ( $balanceValueRow->Type == '1001' ) || ($balanceValueRow->Type == '7001' ) || ($balanceValueRow->Type == '7002' ) )
            $balanceValueRow->Value = round( $balanceValueRow->Value , 2 );
        }

        // do not include if 'NOT USED'
        if ( $newRow->UltraName != 'NOT USED' )
          $adjustedBalanceValue[] = $newRow;

        if ( $isMintBrandMSISDN )
          $newRow = $this->adjustBalanceValuesByMintBrand($newRow);

        \logTrace(json_encode($newRow));
      }
    }

    return $adjustedBalanceValue;
  }

  /**
   * adjustBalanceValueListMB
   *
   * Normalize from GB and KB to MB
   *
   * @return array
   */
  protected function adjustBalanceValueListMB($newRow)
  {
    if ( !$newRow->Value )
      return $newRow;

    if ( $newRow->Unit == 'GB' )
    {
      $newRow->Unit  = 'MB';
      $newRow->Value = ( $newRow->Value * 1024 );
    }

    if ( $newRow->Unit == 'KB' )
    {
      $newRow->Unit  = 'MB';
      $newRow->Value = ( $newRow->Value / 1024 );
    }

    return $newRow;
  }

  /**
   * addDataArray
   *
   * Appends $data to $this->dataArray
   *
   * @return NULL
   */
  protected function addDataArray($data)
  {
    $this->dataArray = array_merge( $this->dataArray , $data );
  }

  /**
   * generateCorrelationId
   *
   * Generates a unique CorrelationId needed for some Control Commands
   *
   * @return string
   */
  protected function generateCorrelationId($command)
  {
    // we won't be using the same command for the same action in the same timestamp
    $correlationId = time() . '-' . $command . '-' . $this->actionUUID;

    if (\Ultra\UltraConfig\isDevEnvironment()) return 'DEV-' . $correlationId;
    if (\Ultra\UltraConfig\isQaEnvironment())  return 'QA-'  . $correlationId;

    return $correlationId;
  }

  /**
   * addPlanDataOnUpdateSubscriberFeature
   *
   * Add PlanData needed for UpdateSubscriberFeature
   *
   * @return array
   */
  protected function addPlanDataOnUpdateSubscriberFeature($parameters,$currentPlanId)
  {
    $parameters['PlanData']['wholesalePlan'] = $parameters['wholesalePlan'];
    $parameters['PlanData']['retailPlanId']  = $currentPlanId;

/*
a) Feature
12715	Addon Voice  Domestic Roaming Voice Minutes	Number of Minutes	A-VOICE-DR
12714	Addon SMS    Domestic Roaming SMS Messages	Number of Messages	A-SMS-DR

b) Feature
Voicemail  V-ENGLISH <-> V-SPANISH 

c) Feature
12906   Addon Data   4G data domestic roaming, then cutoff data (WPRBLK05DR)   			Mbytes of 4G Data  A-DATA-BLK-DR
12907   Addon Data   Block of 4G data, applied only on top of a "WBAS" base soc. (WPRBLK30)	Mbytes of 4G Data  A-DATA-BLK
*/

    return $parameters;
  }

  /**
   * addRetailPlanIDFromUltraPlan
   *
   * Add PlanData::retailPlanID according to 'ultra_plan_name'
   * r_unlimited/r_limited/r_univision SOC
   * [ 12006 , 12007 , 12020 ] <-> [ R-UNLIMITED , R-LIMITED , R-UNIVISION ]
   *
   * @return array
   */
  public function addRetailPlanIDFromUltraPlan($params)
  {
    if ( empty( $params['ultra_plan_name'] ) )
    {
      \logError('ultra_plan_name is missing');

      return $params;
    }

    // STANDBY / plan_state is Neutral
    if ( $params['ultra_plan_name'] == 'STANDBY' )
    {
      $brand_id = empty($params['brand_id']) ? NULL : $params['brand_id'];
      $retailPlanName = \Ultra\MvneConfig\getDefaultRetailPlanName($brand_id);
    }
    else
    {
      $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($params['ultra_plan_name'] );
      $retailPlans       = \Ultra\MvneConfig\getRetailPlans();

      foreach( $retailPlans as $retailPlan )
        if ( isset($accSocsPlanConfig[ $retailPlan ]) )
          $retailPlanName = $retailPlan;
    }

    // retrieve retailPlanID
    $params['PlanData']['retailPlanID'] = \Ultra\MvneConfig\getAccSocDefinitionField(
      $retailPlanName,
      'plan_id'
    );

    return $params;
  }

  /**
   * addPlanDataOnActivation
   *
   * Add PlanData node data needed for ActivateSubscriber
   *
   * @return array
   */
  protected function addPlanDataOnActivation($parameters)
  {
    if ( empty( $parameters['wholesalePlan'] ) )
      \logError('wholesalePlan is missing');
    else
      $parameters['PlanData']['wholesalePlan'] = $parameters['wholesalePlan'];

    $parameters['throttle_speed'] = \Ultra\UltraConfig\getDefaultThrottleSpeed();

    $parameters = $this->addRetailPlanIDFromUltraPlan( $parameters );
    $parameters = $this->addPlanDataAddOnFeatureList( $parameters , 'ADD' );

    return $parameters;
  }

  /**
   * addPlanDataOnRenew
   *
   * Add PlanData node data needed for RenewPlan
   *
   * @return array
   */
  protected function addPlanDataOnRenew($parameters)
  {
    $parameters['PlanData']['wholesalePlan'] = $parameters['wholesalePlan'];

    $parameters = $this->addRetailPlanIDFromUltraPlan( $parameters );

    // SOCs to be RESET
    $parameters = $this->addPlanDataAddOnFeatureList( $parameters , 'RESET' );

    return $parameters;
  }

  /**
   * addPlanDataAddOnFeatureList
   *
   * Add AddOnFeatureList needed for some Control Commands
   *
   * @return array
   */
  protected function addPlanDataAddOnFeatureList( $parameters , $action )
  {
    // populate PlanData::AddOnFeatureList from 'ultra_plan_name'

    if ( empty( $parameters['ultra_plan_name'] ) || ( $parameters['ultra_plan_name'] == 'STANDBY' ) )
      return $parameters;

    $subplan =  empty($parameters['subplan']) ? NULL : $parameters['subplan'];
    $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(
      $parameters['ultra_plan_name'],
      $parameters['customer_id'],
      $subplan
    );

    // adjust throttle configuration to default
    // if (\Throttling::planCanUpdateThrottleSpeed($parameters['ultra_plan_name']))
    // {
      $throttling = new \Throttling($parameters['ultra_plan_name']);
      $throttling->setCurrentThrottleSpeed($parameters['throttle_speed']);
      $throttling->adjustPlanConfigForThrottle($accSocsPlanConfig);
    // }

    // determine the voicemail SOCs

    if ( is_english( $parameters['preferredLanguage'] ) )
      $accSocsPlanConfig[ 'v_english' ] = '';

    if ( ( $parameters['preferredLanguage'] == 'es-US' ) || ( $parameters['preferredLanguage'] == 'es' ) )
      $accSocsPlanConfig[ 'v_spanish' ] = '';

    // determine other SOCs

    foreach( $accSocsPlanConfig as $soc => $featureValue )
      if ( ! \Ultra\MvneConfig\isRetailPlanSOC($soc) )
      {
        $socAction = $action;

        if ( ( $action == 'RESET' )
          && (    \Ultra\UltraConfig\isVoicemailSoc($soc)
               || \Ultra\MvneConfig\isNetworkSoc($soc)
               || \Ultra\UltraConfig\isRoamingSoc($soc)
               || \Ultra\UltraConfig\isRoamingWalletSoc($soc)
               || \Ultra\UltraConfig\isWifiCallingSoc($soc)
          )
        )
        {
          // PROD-549: RETAIN all Voicemail SOCs and all Network SOCs
          $socAction    = 'RETAIN';
          $featureValue = '';

          if ($soc == 'a_voicesmswallet_ir')
          {
            $socAction = 'INCREMENT';
            $featureValue = \Ultra\Lib\RoamCreditPromo::promoAmountFromParams($parameters);
          }
        }

        $parameters['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
          array(
            'Action'       => $socAction,
            'autoRenew'    => 'false',
            'FeatureID'    => \Ultra\MvneConfig\getAccSocDefinitionField( $soc , 'plan_id' ),
            'FeatureValue' => $featureValue
          );
      }

    return $parameters;
  }

  /**
   * addContactData
   *
   * Add ContactData needed for some Control Commands
   * Warning: unsets preferredLanguage and zipCode
   *
   * @return array
   */
  protected function addContactData($parameters)
  {
    if ( isset( $parameters['preferredLanguage'] ) )
    {
      $parameters['ContactData']['preferredLanguage'] = languageIETF( $parameters['preferredLanguage'] );
      unset( $parameters['preferredLanguage'] );
    }

    if ( isset( $parameters['zipCode'] ) )
    {
      $parameters['ContactData']['zipCode'] = $parameters['zipCode'];
      unset( $parameters['zipCode'] );
    }

    return $parameters;
  }

  /**
   * addAutoRenew
   *
   * Add autoRenew needed for some Control Commands
   *
   * @return array
   */
  protected function addAutoRenew($parameters)
  {
    $parameters['autoRenew'] = 'false';

    return $parameters;
  }

  /**
   * addResourceData
   *
   * Add ResourceData needed for some Control Commands
   *
   * @return array
   */
  protected function addResourceData($parameters)
  {
/*
    if ( isset( $parameters['MSISDN'] ) )
      $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
        'productCategory' => 'MSISDN',
        'serialNumber'    => $parameters['MSISDN']
      );
*/

    if ( isset( $parameters['ICCID'] ) )
      $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
        'productCategory' => 'SIM',
        'serialNumber'    => $parameters['ICCID']
      );

    // dummy device
    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'DEVICE',
      'serialNumber'    => $this->getDeviceSerialNumber($parameters)
    );

    return $parameters;
  }

  /**
   * getDeviceSerialNumber
   *
   * IMEI needed for some Control Commands
   *
   * @return string
   */
  protected function getDeviceSerialNumber($parameters)
  {
    if ( isset($parameters['override_default_imei']) )
      return $parameters['override_default_imei'];

    // default
    return '199998587285872';
  }

  /**
   * addPoolAccInd
   *
   * Add poolAccInd needed for some Control Commands
   *
   * @return array
   */
  protected function addPoolAccInd($parameters)
  {
    $parameters['poolAccInd'] = 'false';

    return $parameters;
  }

  /**
   * addCorrelationParameter
   *
   * Add CorrelationId needed for some Control Commands
   *
   * @return array
   */
  protected function addCorrelationParameter($parameters,$command)
  {
    $parameters['CorrelationID'] = $this->generateCorrelationId($command);

    return $parameters;
  }

  /**
   * addOriginatorDataParameters
   *
   * Add OriginatorData parameters needed for some Control Commands
   *
   * @return array
   */
  protected function addOriginatorDataParameters($parameters)
  {
    $parameters['OriginatorData']['accountType']    = 'MVNE';
    $parameters['OriginatorData']['accountSubType'] = 'ULTRA';

    return $parameters;
  }

  /**
   * postProcessInboundMessageData
   *
   * Interpret data and errors from inbound message
   *
   * @return array
   */
  protected function postProcessInboundMessageData( $data , $command )
  {
    dlog('',"postProcessInboundMessageData data = %s",$data);

    $data = (array) $data;

    $data['errors'] = array();

    // interpret command result
    if ( isset( $data['body'] ) )
    {
      $data['body'] = (array) $data['body'];

      if ( isset( $data['body']['MSISDN'] ) )
      {
        $this->MSISDN = $data['body']['MSISDN'];
      }

      if ( isset( $data['body']['ResultCode'] ) )
      {
        // all ACC commands must return ResultCode as 100 for success
        $data['success'] = ! ! ( $data['body']['ResultCode'] == '100' );

        if ( $data['success'] )
        {
          // command-specific postProcess

          $postProcessByCommand = 'postProcessInbound'.$command;

          if ( method_exists( $this , $postProcessByCommand ) )
            $data['body'] = $this->$postProcessByCommand( $data['body'] );
        }
        else
        {
          if ( isset( $data['body']['ResultMsg'] ) )
          {
            // compose error message out of ResultCode and ResultMsg

            $data['errors'][] = $data['body']['ResultCode'] . " : " . $data['body']['ResultMsg'];

            $postProcessErrorsByCommand = 'postProcessInboundErrors'.$command;

            // allow specific error handling by command
            if ( method_exists( $this , $postProcessErrorsByCommand ) )
              $data['body'] = $this->$postProcessErrorsByCommand( $data['body'] );

            // unset( $data['body']['ResultMsg'] );
          }
          else
            $data['errors'][] = $data['body']['ResultCode'];

          // unset( $data['body']['ResultCode'] );
        }
      }
      else
      {
        $data['success']  = FALSE;

        if ( isset($data['body']['errors']) && is_array($data['body']['errors']) )
          $data['errors'] = $data['body']['errors'];
        elseif ( isset($data['body']['errors']) && $data['body']['errors'] )
        {
          $json_errors = json_decode( $data['body']['errors'] );

          if ( is_array($json_errors) )
            $data['errors'] = $json_errors;
          else
            $data['errors'] = array( $data['body']['errors'] );
        }
        else
          $data['errors'][] = "ResultCode missing in response";
      }
    }

    return $data;
  }

  protected function explainCommandErrorCodes( $command , $resultCode )
  {
    $fatal       = 0;
    $unavailable = 0;
    $missing     = 1;

    $errorCodesData = \get_command_error_codes_by_command( $command );

    if ( $errorCodesData && is_array($errorCodesData) && count($errorCodesData) )
      foreach( $errorCodesData as $errorCodesInfo )
        if ( $errorCodesInfo->ERROR_CODE == $resultCode )
        {
          $fatal       = ( $errorCodesInfo->ERROR_TYPE == 1 )?1:0;
          $unavailable = ( $errorCodesInfo->ERROR_TYPE == 0 )?1:0;
          $missing     = 0;
        }

    return array($fatal,$unavailable,$missing);
  }

  private function postProcessInboundErrorsQueryStatus( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'QueryStatus' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  private function postProcessInboundErrorsCheckBalance( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'CheckBalance' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  private function postProcessInboundErrorsSendSMS( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'SendSMS' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  private function postProcessInboundErrorsUpdatePlanAndFeaturesRaw( $body )
  {
    return $this->postProcessInboundErrorsUpdatePlanAndFeatures( $body );
  }

  /**
   * postProcessInboundErrorsUpdatePlanAndFeatures
   *
   * UpdatePlanAndFeatures error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsUpdatePlanAndFeatures( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'UpdatePlanAndFeatures' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  /**
   * postProcessInboundErrorsQuerySubscriber
   *
   * QuerySubscriber error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsQuerySubscriber( $body )
  {
    $desyncList      = array( 257 , 374 );

    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      if ( in_array( $body['ResultCode'] , $desyncList ) )
        dlog('','Signal DESYNC : '.$body['ResultCode'] );

      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'QuerySubscriber' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  private function postProcessInboundErrorsRenewPlanRaw( $body )
  {
    return $this->postProcessInboundErrorsRenewPlan( $body );
  }

  /**
   * postProcessInboundErrorsRenewPlan
   *
   * RenewPlan error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsRenewPlan( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'RenewPlan' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  /**
   * postProcessInboundErrorsSuspendSubscriber
   *
   * SuspendSubscriber error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsSuspendSubscriber( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'SuspendSubscriber' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  /**
   * postProcessInboundErrorsDeactivateSubscriber
   *
   * DeactivateSubscriber error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsDeactivateSubscriber( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'DeactivateSubscriber' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  /**
   * postProcessInboundErrorsReactivateSubscriber
   *
   * ReactivateSubscriber error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsReactivateSubscriber( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'ReactivateSubscriber' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  /**
   * postProcessInboundErrorsRestoreSubscriber
   *
   * RestoreSubscriber error interpretation
   *
   * @return array
   */
  private function postProcessInboundErrorsRestoreSubscriber( $body )
  {
    $body['fatal']       = 0;
    $body['unavailable'] = 0;

    if ( isset($body['ResultCode']) && $body['ResultCode'] )
    {
      list ($fatal,$unavailable,$missing) = $this->explainCommandErrorCodes( 'RestoreSubscriber' ,  $body['ResultCode'] );

      $body['fatal']       = $fatal;
      $body['unavailable'] = $unavailable;

      if ( $missing )
      {
        dlog('',"MISSING ERROR ".$body['ResultCode']); // TODO:
        $body['fatal'] = 1;
      }
    }

    return $body;
  }

  /**
   * postProcessInboundCheckBalance
   *
   * Adjust CheckBalance inbound data
   *
   * @return array
   */
  private function postProcessInboundCheckBalance( $body )
  {
    $body['BalanceValueList']->BalanceValue = $this->adjustBalanceValueList( $body['BalanceValueList']->BalanceValue );

    return $body;
  }

  /**
   * postProcessInboundGetNetworkDetails
   *
   * Adjust GetNetworkDetails inbound data
   *
   * @return array
   */
  private function postProcessInboundGetNetworkDetails( $body )
  {
    if ( !isset($body['IMEI']) || !$body['IMEI'] )
      return $body;

    $body['IMEI_RAW'] = $body['IMEI'];
    $body['IMEI']     = compute_imei($body['IMEI']);

    return $body;
  }

  /**
   * postProcessInboundQuerySubscriber
   *
   * Adjust QuerySubscriber inbound data
   *
   * @return array
   */
  private function postProcessInboundQuerySubscriber( $body )
  {
    if ( isset( $body['AddOnFeatureInfoList'] ) )
      $body['AddOnFeatureInfoList'] = $this->translateAddOnsToUltraSocs( $body['AddOnFeatureInfoList'] , $body['PlanId'] , $body['wholesalePlan'] );
    else
      // STANDBY plan has no SOCs
      $body['AddOnFeatureInfoList'] = NULL;

    return $body;
  }

  /**
   * interactWithMultipleChannels
   *
   * Similar to interactWithChannel, but performed for multiple commands/messages
   *
   * @return array
   */
  protected function interactWithMultipleChannels( $commands , $outboundControlMessages )
  {
    // data which will be returned
    $data = array();

    // data necessary for Request-Reply interaction
    $channelInteraction = array();

    $messageCount = count( $outboundControlMessages );

    // create Request-Reply ACC MW Control Channels
    for ( $i = 0; $i < $messageCount; $i++ )
      list( $channelInteraction[ $i ]['outboundChannel'] , $channelInteraction[ $i ]['inboundChannel'] ) = $this->controlChannel->createNewACCMWControlChannels();

    for ( $i = 0; $i < $messageCount; $i++ )
    {
      if ( ! $channelInteraction[ $i ]['outboundChannel'] )
        throw new \Exception("Could not create an outbound ACC MW Control Channel for ".$commands[ $i ]);

      if ( ! $channelInteraction[ $i ]['inboundChannel'] )
        throw new \Exception("Could not create an inbound ACC MW Control Channel for ".$commands[ $i ]);
    }

    // Request-Reply
    for ( $i = 0; $i < $messageCount; $i++ )
      $channelInteraction[ $i ]['controlUUID'] = $this->controlChannel->sendToControlChannel( $channelInteraction[ $i ]['outboundChannel'] , $outboundControlMessages[ $i ] );

    for ( $i = 0; $i < $messageCount; $i++ )
      if ( ! $channelInteraction[ $i ]['controlUUID'] )
        throw new \Exception("sendToControlChannel failed (no controlUUID) for ".$commands[ $i ]);

    // wait for Reply
    for ( $i = 0; $i < $messageCount; $i++ )
      $channelInteraction[ $i ]['inboundReplyResult'] = $this->controlChannel->waitForACCMWControlMessage( $channelInteraction[ $i ]['inboundChannel'] );

    for ( $i = 0; $i < $messageCount; $i++ )
      if ( ! $channelInteraction[ $i ]['inboundReplyResult'] )
        throw new \Exception("waitForACCMWControlMessage failed for ".$commands[ $i ]);

    for ( $i = 0; $i < $messageCount; $i++ )
      dlog('',"inboundReplyResult for ".$commands[ $i ]." = %s",$channelInteraction[ $i ]['inboundReplyResult']);

    for ( $i = 0; $i < $messageCount; $i++ )
      if ( ! isset($channelInteraction[ $i ]['inboundReplyResult']->data_array['message']) )
        throw new \Exception("No message from ".$commands[ $i ]." Result");

    for ( $i = 0; $i < $messageCount; $i++ )
      dlog('',"inboundReplyResult message for ".$commands[ $i ]." = %s",$channelInteraction[ $i ]['inboundReplyResult']->data_array['message']);

    // data we obtained from the response
    for ( $i = 0; $i < $messageCount; $i++ )
      $channelInteraction[ $i ]['data'] = $this->controlChannel->extractFromMessage( $channelInteraction[ $i ]['inboundReplyResult']->data_array['message'] );

    for ( $i = 0; $i < $messageCount; $i++ )
      if ( ! $channelInteraction[ $i ]['data'] )
        throw new \Exception("No data found in inbound ACC MW Control Message for ".$commands[ $i ]);

    // post-process data
    for ( $i = 0; $i < $messageCount; $i++ )
    {
      $channelInteraction[ $i ]['data'] = $this->postProcessInboundMessageData( $channelInteraction[ $i ]['data'] , $commands[ $i ] );

      $data[ $commands[ $i ] ] = $channelInteraction[ $i ]['data'];

      dlog('',$commands[ $i ]." data = %s",$data[ $commands[ $i ] ]);
    }

    return $data;
  }

  /**
   * extractDataBodyFromMessage
   *
   * @return array
   */
  public function extractDataBodyFromMessage( $message )
  {
    $data = $this->controlChannel->extractFromMessage( $message );

    $data = (array) $data;

    dlog('',"body = %s",$data['body']->body);

    return (array) $data['body']->body;
  }

  /**
   * interactWithChannel
   *
   * Request-Reply interaction
   *
   * @return object of class \Result
   */
  protected function interactWithChannel( $command , $outboundControlMessage , $sms=FALSE , $requestReplyTimeoutSeconds=NULL )
  {
    dlog('',"outboundControlMessage = $outboundControlMessage");

    $result = new \Result();

    try
    {
      # create Request-Reply ( ACC or SMS ) MW Control Channels
      list($outboundACCMWControlChannel,$inboundACCMWControlChannel) =
        $sms
        ?
        $this->controlChannel->createNewSMSMWControlChannels()
        :
        $this->controlChannel->createNewACCMWControlChannels();

      if ( ! $outboundACCMWControlChannel )
        throw new \Exception('Could not create an outbound '.($sms?'SMS':'ACC').' MW Control Channel');

      if ( ! $inboundACCMWControlChannel )
        throw new \Exception('Could not create an inbound '.($sms?'SMS':'ACC').' MW Control Channel');

      # I am a Requestor, I want to send an *outbound* message through $outboundACCMWControlChannel,
      # then I will synchronously wait for a reply the Replier will send through $inboundACCMWControlChannel

      $controlUUID = $this->controlChannel->sendToControlChannel( $outboundACCMWControlChannel , $outboundControlMessage );

      if ( ! $controlUUID )
        throw new \Exception("sendToControlChannel failed (no controlUUID)");

      if ( ! $requestReplyTimeoutSeconds )
        $requestReplyTimeoutSeconds = \Ultra\UltraConfig\channelsDefaultReplyTimeoutSeconds();

      $this->controlChannel->setRequestReplyTimeoutSeconds( $requestReplyTimeoutSeconds );

      $inboundReplyResult =
        $sms
        ?
        $this->controlChannel->waitForSMSMWControlMessage( $inboundACCMWControlChannel )
        :
        $this->controlChannel->waitForACCMWControlMessage( $inboundACCMWControlChannel );

      if ( ! $inboundReplyResult )
        throw new \Exception("wait on $inboundACCMWControlChannel failed (no inboundReplyResult)");

      dlog('',"inboundReplyResult = %s",$inboundReplyResult);

      if ( $inboundReplyResult->is_timeout() )
      {
        $result = $inboundReplyResult;

        throw new \Exception("Middleware Timeout");
      }

      if ( ! isset($inboundReplyResult->data_array['message']) )
        throw new \Exception("wait on $inboundACCMWControlChannel failed (no message)");

      $inboundReplyMessage = $inboundReplyResult->data_array['message'];

      #dlog('',"inboundReplyMessage = %s",$inboundReplyMessage);

      # data we obtained from the response
      $data = $this->controlChannel->extractFromMessage( $inboundReplyMessage );

      if ( ! $data )
        throw new \Exception("No data found in inbound ACC MW Control Message");

      dlog('',"data extracted from message = %s",$data);

      $data = $this->postProcessInboundMessageData( $data , $command );

      dlog('',"data after postProcessInboundMessageData = %s",$data);

      if ( isset( $data['body'] ) )
        $this->addDataArray( $data['body'] );

      if ( isset( $data['success'] ) )
        $this->dataArray['success'] = $data['success'];

      if ( isset( $data['errors'] ) && is_array( $data['errors'] ) && count( $data['errors'] ) )
      {
        // propagate data even in case of errors
        if ( isset( $data['body'] ) && is_array($data['body']) )
          $result->add_data_array( $data['body'] );

        $this->dataArray['errors'] = $data['errors'];

        $this->dataArray['error']  = $data['errors'][0];

        throw new \Exception( $command . ' - ' . $data['errors'][0] );
      }

      $result->add_data_array(
        array(
          'message' => $this->controlChannel->buildMessage(
            array(
              'header'     => $command,
              'body'       => $data,
              'actionUUID' => $this->actionUUID
            )
          )
        )
      );

      $result->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      $result->add_error( $e->getMessage() );
    }

    return $result;
  }

  /**
   * mintRetailSocChanged
   *
   * determine if mint retail soc has changed
   *
   * @param Array $parameters outbound parameter
   * @param Array $addOnFeatureInfo features info from query subscriber
   * @return integer
   */
  protected function mintRetailSocChanged($parameters, $addOnFeatureInfo)
  {
    // collect all current feature ids, output of query subscriber
    $addOnFeatureIds = [];
    foreach ($addOnFeatureInfo as $addOnFeature)
      $addOnFeatureIds[] = $addOnFeature->FeatureID;

    // if new retail plan id is mint and it is new
    return (
         \Ultra\MvneConfig\isMintRetailPlanSOC( $parameters['PlanData']['retailPlanID'] )
      && ! in_array($parameters['PlanData']['retailPlanID'], $addOnFeatureIds)
    );
  }

  /**
   * mintDataSocAdded
   *
   * determine if mint data soc has been added
   *
   * @param Array $parameters outbound parameters
   * @return integer
   */
  protected function mintDataSocAdded($parameters)
  {
    // if a feature id is mint data soc and action is add or reset
    foreach ($parameters['PlanData']['AddOnFeatureList']['AddOnFeature'] as $addOnFeature)
    {
      $isPaidEvent = (
           \Ultra\MvneConfig\isMintDataSOC($addOnFeature['FeatureID'])
        && in_array($addOnFeature['Action'], ['ADD'])
      );

      // if found paid event, stop searching
      if ($isPaidEvent)
        break;
    }

    return $isPaidEvent;
  }
}


/**
 * Biphasic control commands: those need callback notifications
 */
class ControlCommandBiphasic extends ControlCommandBase
{
  protected $commandInvocationObject;

  /**
   * initiate
   *
   * Initiates a Biphasic Command Invocation
   *
   * @return object of class \Result
   */
  protected function initiate( $command , $acc_api , $parameters )
  {
    $initiateParams = array(
      'customer_id'       => $parameters['customer_id'],
      'action_uuid'       => $this->actionUUID,
      'command'           => $command, // class ControlCommand<*>
      'acc_api'           => $acc_api
    );

    if ( isset($parameters['CorrelationID']) )
      $initiateParams['correlation_id'] = $parameters['CorrelationID'];

    $this->commandInvocationObject = new \CommandInvocation( $initiateParams );

    return $this->commandInvocationObject->initiate();
  }

  /**
   * initiateErrorResult
   *
   * Add default message after Command Invocation 'initiate' failure
   *
   * @return object of class \Result
   */
  protected function initiateErrorResult( $command , $customer_id , $result )
  {
    $result->add_data_array(
      array(
        'message' => $this->buildInboundErrorMessage(
          $command,
          array( 'Command Invocation \'initiate\' failed for customer_id '.$customer_id ),
          array( 'customer_id' => $customer_id )
        )
      )
    );

    $result->succeed();

    return $result;
  }

  /**
   * hasMintRetailPlanID
   *
   * Returns true if the request parameters contain a Mint Retail Plan ID
   *
   * @return boolean
   */
  public function hasMintRetailPlanID( $parameters )
  {
    return ! ! 
      ( is_array( $parameters )
      && ! empty( $parameters['PlanData'] )
      && is_array( $parameters['PlanData'] )
      && ! empty( $parameters['PlanData']['retailPlanID'] )
      && \Ultra\MvneConfig\isMintRetailPlanSOC( $parameters['PlanData']['retailPlanID'] )
    );
  }

  /**
   * getBrandIdFromICCID
   *
   * Returns brand id associated with the given ICCID
   *
   * @return integer
   */
  public static function getBrandIdFromICCID($ICCID)
  {
    // connect to TSIP DB
    teldata_change_db();

    // get brand id associated with ICCID
    $brand_id = get_brand_id_from_iccid($ICCID);

    // connect back to MW DB
    \Ultra\Lib\DB\ultra_acc_connect();

    return $brand_id;
  }

  /**
   * interactWithChannel
   *
   * Override parent::interactWithChannel and handle initiated Command Invocation
   *
   * @return object of class \Result
   */
  protected function interactWithChannel( $command , $outboundControlMessage , $sms=FALSE , $requestReplyTimeoutSeconds=NULL )
  {
    $result = parent::interactWithChannel( $command , $outboundControlMessage , $sms , $requestReplyTimeoutSeconds );

    dlog('',"interactWithChannel result = %s",$result);

    if ( $result->is_success() )
    {
      dlog('',"result data = %s",$result->data_array);

      $dataBody = $this->extractDataBodyFromMessage( $result->data_array['message'] );

      if ( count($dataBody)
        && ( $dataBody['ResultCode'] == '100' ) 
        && ( $dataBody['ResultMsg']  == 'Success' )
      )
      {
        // Update Command Invocation to 'OPEN' (M0)

        $confirmOpenResult = $this->commandInvocationObject->confirmOpen(
          array(
            'acc_transition_id' => $dataBody['serviceTransactionId']
          )
        );

        if ( $confirmOpenResult->is_failure() )
        {
          $errors = $confirmOpenResult->get_errors();

          $warning = 'We could not confirm an OPEN Command Invocation - '.implode(' ; ',$errors);
          $result->add_warning($warning);
          dlog('',$warning);
        }
      }
      else
        dlog('','We could not store an OPEN Command Invocation due to missing/invalid data in result');
    }
    else
    {
      // Update Command Invocation to 'FAILED' (C4) or 'ERROR' (M4)

      $status = 'FAILED'; // (C4)
      $error  = 'ERROR';

      if ( isset($this->dataArray['error']) )
        $error = $this->dataArray['error'];

      if ( $error == '261 : This API clashes with other processes running for this subscriber' )
        $status = 'ERROR'; // (M4)

      dlog('',"invoking failOnSync with status = $status and error = $error");

      $failData = array(
        'error_code_and_message' => $error
      );

      if ( ! empty( $result->data_array['serviceTransactionId'] ) )
        $failData['acc_transition_id'] = $result->data_array['serviceTransactionId'];

      $failOnSyncResult = $this->commandInvocationObject->failOnSync(
        $failData,
        $status
      );

      if ( $failOnSyncResult->is_failure() )
      {
        $errors = $failOnSyncResult->get_errors();

        $warning = 'We could not record a failed Command Invocation - '.implode(' ; ',$errors);
        $result->add_warning($warning);
        dlog('',$warning);
      }
    }

    return $result;
  }

  /**
   * getDataFromQuerySubscriber
   *
   * Invoke QuerySubscriber from within another Control Command class
   *
   * @return object of class \Result
   */
  protected function getDataFromQuerySubscriber($parameters)
  {
    $controlClass = '\Ultra\Lib\MiddleWare\ACC\ControlCommandQuerySubscriber';

    $controlObjectQuerySubscriber = new $controlClass( $this->actionUUID );

    unset( $parameters['ICCID'] );
    unset( $parameters['customer_id'] );
    unset( $parameters['option'] );
    unset( $parameters['preferredLanguage'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['wholesalePlan'] );

    return $controlObjectQuerySubscriber->processCommand($parameters);
  }

  /**
   * getDataFromCheckBalance
   *
   * Invoke CheckBalance from within another Control Command class
   *
   * @return object of class \Result
   */
  protected function getDataFromCheckBalance($parameters)
  {
    $controlClass = '\Ultra\Lib\MiddleWare\ACC\ControlCommandCheckBalance';

    $controlObjectCheckBalance = new $controlClass( $this->actionUUID );

    unset( $parameters['ICCID'] );
    unset( $parameters['PlanData'] );
    unset( $parameters['customer_id'] );
    unset( $parameters['keepDataSocs'] );
    unset( $parameters['option'] );
    unset( $parameters['preferredLanguage'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['wholesalePlan'] );

    return $controlObjectCheckBalance->processCommand($parameters);
  }
}


/**
 * ActivateSubscriber control command implementation
 */
class ControlCommandActivateSubscriber extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'ActivateSubscriber';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addActivateSubscriberParameters($parameters);
    $paidEvent  = $this->isPaidEvent($parameters);
    $socName    = $this->getMintSocName($parameters);

    unset( $parameters['ICCID'] );
    unset( $parameters['wholesalePlan'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['customer_id'] );

    \logDebug("retailPlanID = ".$parameters['PlanData']['retailPlanID']." ; socName = $socName");

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters , $paidEvent , $socName );

    return $this->interactWithChannel( $command , $outboundControlMessage , FALSE , 120 );
  }

  // return Mint SOC name if applicable
  protected function getMintSocName( $parameters )
  {
    // MINT Retail plan ID
    if ( $this->hasMintRetailPlanID( $parameters )
      && ! empty( $parameters['PlanData'] )
      && ! empty( $parameters['PlanData']['retailPlanID'] )
    )
    {
      list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

      return $ultraSocsByPlanId[ $parameters['PlanData']['retailPlanID'] ];
    }

    return NULL;
  }

  // Determine if it's a MINT paid event (based on Retail Plan)
  protected function isPaidEvent($parameters)
  {
    $isPaidEvent = \Ultra\MvneConfig\isMintRetailPlanSOC( $parameters['PlanData']['retailPlanID'] ) ? 1 : 0 ;

    \logDebug("ICCID = {$parameters['ICCID']} ; retailPlanID = {$parameters['PlanData']['retailPlanID']} ; isPaidEvent = $isPaidEvent");

    return $isPaidEvent;
  }

  // Populate parameters for ActivateSubscriber SOAP call
  private function addActivateSubscriberParameters($parameters)
  {
    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addPoolAccInd($parameters);
    $parameters = $this->addResourceData($parameters);
    $parameters = $this->addAutoRenew($parameters);
    $parameters = $this->addPlanDataOnActivation($parameters);
    $parameters = $this->addContactData($parameters);

    $parameters['PortInData']['specificationValueName'] = 'PORTTYPE';
    $parameters['PortInData']['specificationValue']     = 'RE'; # regular, i.e. no porting. Otherwise 'PI'

    return $parameters;
  }
}


/**
 * CanActivate control command synchronous implementation
 */
class ControlCommandCanActivate extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $result = new \Result();

    try
    {
      // Invoke QuerySubscriber and GetNetworkDetails in parallel, synchronously

      $outboundControlMessageQuerySubscriber   = $this->buildOutboundControlMessage( 'QuerySubscriber'   , $parameters );
      $outboundControlMessageGetNetworkDetails = $this->buildOutboundControlMessage( 'GetNetworkDetails' , $parameters );

      $commandsCanActivate = array(
        'QuerySubscriber',
        'GetNetworkDetails'
      );

      $messagesCanActivate = array(
        $outboundControlMessageQuerySubscriber,
        $outboundControlMessageGetNetworkDetails
      );

      // perform QuerySubscriber and GetNetworkDetails

      $data = $this->interactWithMultipleChannels( $commandsCanActivate , $messagesCanActivate );

      // interpret interactWithMultipleChannels outcome

      $dataQuerySubscriber   = $data['QuerySubscriber'];
      $dataGetNetworkDetails = $data['GetNetworkDetails'];

      dlog('',"dataQuerySubscriber   = %s",$dataQuerySubscriber);
      dlog('',"dataGetNetworkDetails = %s",$dataGetNetworkDetails);

      $errorsQuerySubscriber   = $data['QuerySubscriber']['errors'];
      $errorsGetNetworkDetails = $data['GetNetworkDetails']['errors'];

      // conditions for ICCID to be available
      $available = ! !
        ( ! $data['QuerySubscriber']['success']
          && ! $data['GetNetworkDetails']['success']
          && count( $errorsQuerySubscriber )
          && count( $errorsGetNetworkDetails )
          && (
            ($errorsQuerySubscriber[0] == "200 : Invalid ICCID" && in_array($errorsGetNetworkDetails[0], \Ultra\UltraConfig\getAccCanActivateMessages($data['QuerySubscriber']['body']['ResultCode'])))
            ||
            ($errorsQuerySubscriber[0] == "202 : The MSISDN does not exist" && in_array($errorsGetNetworkDetails[0], \Ultra\UltraConfig\getAccCanActivateMessages($data['QuerySubscriber']['body']['ResultCode'])))
          )
       );

      $success = TRUE;
      $errors  = array();

      if ( ! $available )
      {
        // handle special errors from QuerySubscriber and GetNetworkDetails

        if ( count( $errorsGetNetworkDetails ) && in_array( "200 : SIM (ICCID) is not Numeric or Not 19 Digits long" , $errorsGetNetworkDetails ) )
          $errors = $errorsGetNetworkDetails;

        if ( isset($data['QuerySubscriber']['body']['MSISDN']) && $data['QuerySubscriber']['body']['MSISDN'] )
          $errors[] = "The SIM is already associated with MSISDN ".$data['QuerySubscriber']['body']['MSISDN'];
      }

      $message = $this->controlChannel->buildMessage(
        array(
          'actionUUID' => $this->actionUUID,
          'header'     => 'CanActivate',
          'body'       => array(
            'success'    => $available,
            'available'  => $available,
            'errors'     => $errors
          )
        )
      );

      $result->add_data_array(
        array(
          'message' => $message
        )
      );

      $result->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      return make_error_Result( $e->getMessage() );
    }

    return $result;
  }
}


/**
 * CancelDeviceLocation control command implementation
 */
class ControlCommandCancelDeviceLocation extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'CancelDeviceLocation';

    $parameters['SIM'] = (isset($parameters['ICCID'])) ? $parameters['ICCID'] : null;

    unset( $parameters['ICCID'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * CancelPortIn control command implementation
 */
class ControlCommandCancelPortIn extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'CancelPortIn';

    $portInQueue = new \PortInQueue();

    list ( $portInQueue , $parameters['portRequestId'] ) = $this->getPortRequestIdFromMSISDN( $portInQueue , $parameters['MSISDN'] );

    if ( ! is_numeric($parameters['portRequestId']) )
      unset( $parameters['portRequestId'] );

/*
    // problem: No portRequestId found
    if ( ! $parameters['portRequestId'] )
    {
      $result = new \Result();

      $result->add_data_array(
        array(
          'message' => $this->buildInboundErrorMessage(
            $command,
            array('No portRequestId found for MSISDN '.$parameters['MSISDN']),
            array()
          )
        )
      );

      $result->succeed();

      return $result;
    }
*/

    // problem: the current Port cannot be cancelled
    if ( $portInQueue->isClosed() )
    {
      $result = new \Result();

      $result->add_data_array(
        array(
          'message' => $this->buildInboundErrorMessage(
            $command,
            array('The Port cannot be cancelled ( status is '.$portInQueue->port_status.' ) - MSISDN '.$parameters['MSISDN']),
            array()
          )
        )
      );

      $result->succeed();

      return $result;
    }

    // update PORTIN_QUEUE
    $updateResult = $portInQueue->updateOnCancelPortIn();

    if ( $updateResult->is_failure() )
      dlog('','ERROR ESCALATION ALERT DAILY - we could not record a CancelPortIn in PORTIN_QUEUE for MSISDN '.$parameters['MSISDN']);

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    $result = $this->interactWithChannel( $command , $outboundControlMessage );

    if ( $result->is_success() )
    {
      // update PORTIN_QUEUE
      $updateResult = $portInQueue->endAsCancelled();

      if ( $updateResult->is_failure() )
        dlog('','ERROR ESCALATION ALERT DAILY - we could not record a successful CancelPortIn in PORTIN_QUEUE for MSISDN '.$parameters['MSISDN']);
    }
    else
    {
      // update PORTIN_QUEUE
      $updateResult = $portInQueue->updateFailedToCancel();

      if ( $updateResult->is_failure() )
        dlog('','ERROR ESCALATION ALERT DAILY - we could not record a failed CancelPortIn in PORTIN_QUEUE for MSISDN '.$parameters['MSISDN']);
    }

    return $result;
  }
}


/**
 * ChangeIMEI control command implementation
 */
class ControlCommandChangeIMEI extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'ChangeIMEI';

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'MSISDN',
      'serialNumber'    => $parameters['MSISDN']
    );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'SIM',
      'serialNumber'    => $parameters['ICCID']
    );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'DEVICE',
      #'serialNumber'    => '111111111111111'
      'serialNumber'    => $parameters['IMEI']
    );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'NEW_DEVICE',
      'serialNumber'    => $parameters['NEW_IMEI']
      #'serialNumber'    => $parameters['IMEI']
    );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * ChangeMSISDN control command implementation
 */
class ControlCommandChangeMSISDN extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'ChangeMSISDN';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'MSISDN',
      'serialNumber'    => $parameters['MSISDN']
    );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'SIM',
      'serialNumber'    => $parameters['ICCID']
    );

    // dummy device
    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'DEVICE',
      'serialNumber'    => $this->getDeviceSerialNumber($parameters)
    );

    unset( $parameters['ICCID'] );
    unset( $parameters['MSISDN'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * ChangeSIM control command implementation
 */
class ControlCommandChangeSIM extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    if ( empty( $parameters['MSISDN'] ) )
      return make_error_result('MSISDN is missing');

    if ( empty( $parameters['OLD_ICCID'] ) )
      return make_error_result('OLD_ICCID is missing');

    if ( empty( $parameters['NEW_ICCID'] ) )
      return make_error_result('NEW_ICCID is missing');

    $command = 'ChangeSIM';

    $customer_id = $parameters['customer_id'];

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'MSISDN',
      'serialNumber'    => $parameters['MSISDN']
    );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'SIM',
      'serialNumber'    => $parameters['OLD_ICCID']
    );

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'NEW_SIM',
      'serialNumber'    => $parameters['NEW_ICCID']
    );

    // dummy device
    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'DEVICE',
      'serialNumber'    => $this->getDeviceSerialNumber($parameters)
    );

    $parameters = $this->addCorrelationParameter($parameters,$command);
    $parameters = $this->addOriginatorDataParameters($parameters);

    unset( $parameters['OLD_ICCID'] );
    unset( $parameters['NEW_ICCID'] );
    unset( $parameters['MSISDN'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * CheckBalance control command implementation
 */
class ControlCommandCheckBalance extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'CheckBalance';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * DeactivateSubscriber control command implementation
 */
class ControlCommandDeactivateSubscriber extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'DeactivateSubscriber';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addResourceData($parameters);

    unset( $parameters['ICCID'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * GetMVNEDetails control command implementation
 * 
 * 1 - If a MSISDN only is provided, execute QuerySubscriber(MSISDN) to get the correct ICCID.
 * 2 - If a ICCID  only is provided, execute QuerySubscriber(ICCID)  to get the correct MSISDN.
 * 3 - Execute CheckBalance, QuerySubscriber and GetNetworkDetails.
 * 4 - Translate AddOnFeatureInfoList to Ultra SOCs
 */
class ControlCommandGetMVNEDetails extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $result = new \Result();

    $resultQuerySubscriber = NULL;

    // we'll start by invoking QuerySubscriber, therefore we need a ControlCommandQuerySubscriber object

    $controlClassQuerySubscriber = '\Ultra\Lib\MiddleWare\ACC\ControlCommandQuerySubscriber';

    $controlObjectQuerySubscriber = new $controlClassQuerySubscriber( $this->actionUUID );

    $iccid  = FALSE;
    $msisdn = FALSE;

    if ( isset($parameters['ICCID']) && ( $parameters['ICCID'] != '' ) )
      $iccid = $parameters['ICCID'];

    if ( isset($parameters['MSISDN']) && ( $parameters['MSISDN'] != '' ) )
      $msisdn = $parameters['MSISDN'];

    // ICCID only is provided, execute QuerySubscriber(ICCID) to get the correct MSISDN
    if ( $iccid && !$msisdn )
      $resultQuerySubscriber = $controlObjectQuerySubscriber->processCommand($parameters);

    // MSISDN only is provided, execute QuerySubscriber(MSISDN) to get the correct ICCID
    if ( $msisdn && !$iccid )
      $resultQuerySubscriber = $controlObjectQuerySubscriber->processCommand($parameters);

    if ( is_object( $resultQuerySubscriber ) && $resultQuerySubscriber->is_failure() )
      return $resultQuerySubscriber;

    // retrieve $iccid or $msisdn from QuerySubscriber result
    if ( is_object( $resultQuerySubscriber ) )
    {
      dlog('',"resultQuerySubscriber = %s",$resultQuerySubscriber);

      dlog('',"controlObjectQuerySubscriber dataArray = %s",$controlObjectQuerySubscriber->dataArray);

      if ( !$iccid && $msisdn )
      {
        if ( ! isset($controlObjectQuerySubscriber->dataArray['SIM']) || ( $controlObjectQuerySubscriber->dataArray['SIM'] == '' ) )
          return make_error_Result( 'No SIM found for MSISDN '.$msisdn );
        else
          $iccid = $controlObjectQuerySubscriber->dataArray['SIM'];
      }
      elseif ( $iccid && !$msisdn )
      {
        if ( ! isset($controlObjectQuerySubscriber->dataArray['MSISDN']) || ( $controlObjectQuerySubscriber->dataArray['MSISDN'] == '' ) )
          return make_error_Result( 'No MSISDN found for SIM '.$iccid );
        else
          $msisdn = $controlObjectQuerySubscriber->dataArray['MSISDN'];
      }
    }

    // at this point we have both
    dlog('',"msisdn = $msisdn ; iccid = $iccid");

    // Invoke CheckBalance, QuerySubscriber and GetNetworkDetails in parallel, synchronously

    $queryData = array(
      'MSISDN'   => $msisdn,
      'ICCID'    => $iccid,
      'UserData' => $parameters['UserData']
    );

    dlog('',"queryData = %s",$queryData);

    try
    {
      $outboundControlMessageCheckBalance      = $this->buildOutboundControlMessage( 'CheckBalance'      , $queryData );
      $outboundControlMessageGetNetworkDetails = $this->buildOutboundControlMessage( 'GetNetworkDetails' , $queryData );

      $commandsGetMVNEDetails = array(
        'CheckBalance',
        'GetNetworkDetails'
      );

      $messagesGetMVNEDetails = array(
        $outboundControlMessageCheckBalance,
        $outboundControlMessageGetNetworkDetails
      );

      if ( ! is_object( $resultQuerySubscriber ) )
      {
        $commandsGetMVNEDetails[] = 'QuerySubscriber';

        $messagesGetMVNEDetails[] = $this->buildOutboundControlMessage( 'QuerySubscriber' , $queryData );
      }

      // perform CheckBalance, GetNetworkDetails and QuerySubscriber (if not already performed)

      $data = $this->interactWithMultipleChannels( $commandsGetMVNEDetails , $messagesGetMVNEDetails );

      // interpret interactWithMultipleChannels outcome

      $dataCheckBalance      = $data['CheckBalance'];
      $dataGetNetworkDetails = $data['GetNetworkDetails'];

      $this->dataArray['success'] = ! !
         ( isset( $dataCheckBalance['success'] )
        && $dataCheckBalance['success']
        && isset( $dataGetNetworkDetails['success'] )
        && $dataGetNetworkDetails['success']
      );

      $this->dataArray['errors'] = array();

      if ( isset( $dataCheckBalance['errors'] ) && is_array( $dataCheckBalance['errors'] ) && count( $dataCheckBalance['errors'] ) )
      {
        $dataCheckBalance['errors'][0] = 'CheckBalance - '.$dataCheckBalance['errors'][0];

        $this->dataArray['errors'] = $dataCheckBalance['errors'];
      }

      if ( isset( $dataGetNetworkDetails['errors'] ) && is_array( $dataGetNetworkDetails['errors'] ) && count( $dataGetNetworkDetails['errors'] ) )
      {
        $dataGetNetworkDetails['errors'][0] = 'GetNetworkDetails - '.$dataGetNetworkDetails['errors'][0];

        $this->dataArray['errors'] = array_merge( $this->dataArray['errors'] , $dataGetNetworkDetails['errors'] );
      }

      if ( !is_object( $resultQuerySubscriber ) )
      {
        $this->dataArray['success'] = ! ! ( $this->dataArray['success'] && $data['QuerySubscriber']['success'] );

        if ( isset( $data['QuerySubscriber']['errors'] ) && is_array( $data['QuerySubscriber']['errors'] ) && count( $data['QuerySubscriber']['errors'] ) )
        {
          $data['QuerySubscriber']['errors'][0] = 'QuerySubscriber - '.$data['QuerySubscriber']['errors'][0];

          $this->dataArray['errors'] = array_merge( $this->dataArray['errors'] , $data['QuerySubscriber']['errors'] );
        }
      }

      if ( ! $this->dataArray['success'] )
        throw new \Exception( 'GetMVNEDetails - ' . $this->dataArray['errors'][0] );

      $dataQuerySubscriber = array();

      if ( is_object( $resultQuerySubscriber ) )
        $dataQuerySubscriber = $controlObjectQuerySubscriber->dataArray;
      else
        $dataQuerySubscriber = $data['QuerySubscriber']['body'];

      dlog('',"dataQuerySubscriber = %s",$dataQuerySubscriber);

      // collect output in a message to be sent back

      if ( isset($dataCheckBalance['body'])
        && is_array($dataCheckBalance['body'])
        && isset($dataCheckBalance['body']['BalanceValueList'])
        && is_object($dataCheckBalance['body']['BalanceValueList'])
        && is_object($dataQuerySubscriber['AddOnFeatureInfoList'])
      )
        $dataCheckBalance['body']['BalanceValueList']->BalanceValue = $this->addDataTriggerAction( $dataCheckBalance['body']['BalanceValueList']->BalanceValue , $dataQuerySubscriber['AddOnFeatureInfoList']->AddOnFeatureInfo );

      $result->add_data_array(
        array(
          'message' => $this->controlChannel->buildMessage(
            array(
              'header'     => 'GetMVNEDetails',
              'body'       => array(
                'success'           => TRUE,
                'errors'            => array(),
                'CheckBalance'      => $dataCheckBalance['body'],
                'GetNetworkDetails' => $dataGetNetworkDetails['body'],
                'QuerySubscriber'   => $dataQuerySubscriber
              ),
              'actionUUID' => $this->actionUUID
            )
          )
        )
      );

      $result->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      return make_error_Result( $e->getMessage() );
    }

    return $result;
  }

  /**
   * addDataTriggerAction
   *
   * Interpret Base Data types as "Data Trigger Action" Ultra Balance Type
   *
   * @return array
   */
  private function addDataTriggerAction( $balanceValue , $addOnFeatureInfo )
  {
    $dataTriggerAction = NULL;

    // If "B-DATA-THR64", "B-DATA-THR128", "B-DATA-THR256" is present than "Throttle"
    // If "B-DATA-BLK" then "Block"
    foreach( $addOnFeatureInfo as $addOn )
    {
      if ( $addOn->UltraServiceName == 'B-DATA-BLK' )
        $dataTriggerAction = 'Block';

      if ( ( $addOn->UltraServiceName == 'B-DATA-THR64' ) || ( $addOn->UltraServiceName == 'B-DATA-THR128' ) || ( $addOn->UltraServiceName == 'B-DATA-THR256' ) )
        $dataTriggerAction = 'Throttle';
    }

    if ( $dataTriggerAction )
      $balanceValue[] = array(
        "Value"     => "",
        "Type"      => "",
        "Unit"      => "",
        "Name"      => "",
        "UltraType" => $dataTriggerAction,
        "UltraName" => "Data Trigger Action"
      );

    return $balanceValue;
  }
}


/**
 * GetNGPList control command implementation
 */
class ControlCommandGetNGPList extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'GetNGPList';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * GetNetworkDetails control command implementation
 */
class ControlCommandGetNetworkDetails extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'GetNetworkDetails';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * PortInEligibility control command implementation
 */
class ControlCommandPortInEligibility extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'PortInEligibility';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * QueryStatus control command implementation
 */
class ControlCommandQueryStatus extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'QueryStatus';

    $parameters = $this->addQueryStatusParameters($parameters);

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    $result = $this->interactWithChannel( $command , $outboundControlMessage );

    dlog('',"interactWithChannel result = %s",$result);

    if ( $result->is_success() )
    {
      $message = $result->data_array['message'];

      $data = $this->controlChannel->extractFromMessage( $message );

      dlog('',"interactWithChannel result message data body = %s",$data->body);

      $this->updatePortIn($parameters,$data->body);
    }

    return $result;
  }

  private function updatePortIn($parameters,$queryStatusData)
  {
    dlog('',"%s",$parameters);

    $portInQueue = new \PortInQueue();

    $result = $portInQueue->loadByMsisdn( $parameters['MSISDN'] );

    $port_error = 'NULL';
    if ( property_exists( $queryStatusData->body , 'PortErrorReason' ) && $queryStatusData->body->PortErrorReason )
      $port_error = $queryStatusData->body->PortErrorReason;

    if ( $result->is_success() )
      $result = $portInQueue->updateOnQueryStatus(
        array(
          'port_status'   => $queryStatusData->body->PortStatus, // <PortStatus> tag from QueryStatusResponse
          'port_error'    => $port_error,
          'due_date_time' => \date_to_datetime( $queryStatusData->body->portDueTime , FALSE , TRUE ) // <portDueTime> tag from QueryStatusResponse
        )
      );

    // ignore $result

    return NULL;
  }

  private function addQueryStatusParameters($parameters)
  {
    $parameters['RequestFlag'] = 'PORTINREQUEST';

    return $parameters;
  }
}


/**
 * ReactivateSubscriber control command implementation
 */
class ControlCommandReactivateSubscriber extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'ReactivateSubscriber';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addPoolAccInd($parameters);
    $parameters = $this->addPlanDataOnActivation($parameters);
    $parameters = $this->addResourceData($parameters);
    $parameters = $this->addAutoRenew($parameters);
    $parameters = $this->addContactData($parameters);

    $paidEvent = $this->isPaidEvent($parameters);

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters , $paidEvent );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }

  // Determine if it's a MINT paid event
  protected function isPaidEvent($parameters)
  {
    $isPaidEvent = \Ultra\MvneConfig\isMintRetailPlanSOC( $parameters['PlanData']['retailPlanID'] ) ? 1 : 0 ;

    \logDebug("ICCID = {$parameters['ICCID']} ; retailPlanID = {$parameters['PlanData']['retailPlanID']} ; isPaidEvent = $isPaidEvent");

    return $isPaidEvent;
  }
}


/**
 * ResendOTA control command implementation
 */
class ControlCommandResendOTA extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'ResendOTA';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * ResetVoiceMailPassword control command implementation
 */
class ControlCommandResetVoiceMailPassword extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'ResetVoiceMailPassword';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addRetailPlanIDFromUltraPlan( $parameters );
    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * RestoreSubscriber control command implementation
 */
class ControlCommandRestoreSubscriber extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'RestoreSubscriber';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters['RestoreReason'] = 'INVOLUNTARY';

    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addResourceData($parameters);

    // this is useful to test inbound notifications to a specific listener
    // $parameters['ReturnURL'] = 'https://162.222.66.68:443/notifications';

    unset( $parameters['ICCID'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * SendSMS control command implementation
 */
class ControlCommandSendSMS extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    return make_error_Result('"ERROR ESCALATION ALERT DAILY - This functionality has been disabled');

    if ( empty($parameters['smsText']) )
      return make_error_Result('No smsText given!');

    $command = 'SendSMS';

    $_attempt_count = 0;
    if ( isset($parameters['_attempt_count']) && $parameters['_attempt_count'] )
      $_attempt_count = $parameters['_attempt_count'];

    $_attempt_timestamp = time();
    if ( isset($parameters['_attempt_timestamp']) && $parameters['_attempt_timestamp'] )
      $_attempt_timestamp = $parameters['_attempt_timestamp'];

    if ( isset($parameters['_attempt_count'])     ) unset( $parameters['_attempt_count'] );
    if ( isset($parameters['_attempt_timestamp']) ) unset( $parameters['_attempt_timestamp'] );

    $parameters['SubscriberSMSList']['SubscriberSMS'][] = array(
      'Language' => $parameters['preferredLanguage'],
      'MSISDN'   => $parameters['MSISDN'],
      'Text'     => ''
    );

    #unset( $parameters['MSISDN'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    $result = $this->interactWithChannel( $command , $outboundControlMessage , 'SMS' );

    dlog('',"interactWithChannel result = %s",$result);

    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();

      // if transient error, enqueue in Redis to be retried later
      if ( $this->isTransientError( $errors[0] ) )
        $this->enqueueForRetry( $parameters , $_attempt_timestamp , $_attempt_count );
    }

    return $result;
  }

  // enqueue in Redis to be retried later
  public function enqueueForRetry( $parameters , $_attempt_timestamp , $_attempt_count )
  {
    dlog('',"parameters = %s",$parameters);

    if ( empty($parameters['smsText']) )
    {
      dlog('',"Won't retry because 'smsText' is empty");
      return 0;
    }

    if ( empty($parameters['MSISDN']) )
    {
      dlog('',"Won't retry because 'MSISDN' is empty");
      return 0;
    }

    $enqueueParams = array(
      '_attempt_count'    => ( $_attempt_count + 1 ),
      'MSISDN'            => $parameters['MSISDN'],
      'preferredLanguage' => $parameters['preferredLanguage'],
      'qualityOfService'  => $parameters['qualityOfService'],
      'smsText'           => $parameters['smsText'],
      // UserData is fixed
    );

    \Ultra\Lib\Util\Redis\SendSMS\enqueue( $enqueueParams , $_attempt_timestamp );
  }

  // is $error a transient error?
  private function isTransientError( $error )
  {
    // we will verify this case by case
    $isTransientError = ( $error != 'SendSMS - 202 : The MSISDN does not exist' && $error != 'SendSMS - 200 : Request Structure Invalid');

    dlog('',"error = %s , isTransientError = %s",$error,($isTransientError?'TRUE':'FALSE'));

    return $isTransientError;
  }
}


/**
 * SuspendSubscriber control command implementation
 */
class ControlCommandSuspendSubscriber extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'SuspendSubscriber';

    $paidEvent = $this->isPaidEvent($parameters);

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters['SuspendReason'] = 'INVOLUNTARY';

    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addResourceData($parameters);
    $socName    = $this->getMintSocName($parameters, $paidEvent, $command);

    unset( $parameters['ICCID'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters , $paidEvent , $socName );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }

  // return Mint SOC name if applicable
  protected function getMintSocName($parameters, $paidEvent, $command)
  {
    return $paidEvent ? $command : NULL ;
  }

  // SuspendSubscriber commands are paid events if brand is MINT (3)
  protected function isPaidEvent($parameters)
  {
    // get brand id associated with ICCID
    $brand_id = self::getBrandIdFromICCID($parameters['ICCID']);

    if ( empty( $brand_id ) )
    {
      \logFatal('Cannot determine brand id for ICCID '.$parameters['ICCID']);

      return 0;
    }

    // get brand name
    $brand_name = \Ultra\UltraConfig\getNameFromBrandId( $brand_id );

    if ( empty( $brand_name ) )
    {
      \logFatal("Cannot determine brand short name for brand_id {$brand_id} , ICCID ".$parameters['ICCID']);

      return 0;
    }

    $isPaidEvent = ( $brand_name == 'Mint' ) ? 1 : 0 ;

    \logDebug("ICCID = {$parameters['ICCID']} ; BRAND_ID = $brand_id ; BRAND = $brand_name ; isPaidEvent = $isPaidEvent");

    return $isPaidEvent;
  }

}


/**
 * UpdateBalance control command implementation
 */
class ControlCommandUpdateBalance extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'UpdateBalance';

    $parameters = $this->addUpdateBalanceParameters($parameters);

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }

  private function addUpdateBalanceParameters($parameters)
  {
#TODO: transactionID ?

#TODO:
/*
      <tns:WalletBucketList>
        <tns:WalletBucket>
          <tns:chargeWallet> MAIN     </tns:chargeWallet>
          <tns:chargeType>   ADDFUNDS </tns:chargeType>
          <tns:chargeAmount> 1        </tns:chargeAmount>
        </tns:WalletBucket>
      </tns:WalletBucketList>
*/

    return $parameters;
  }
}


/**
 * UpdatePortIn control command implementation
 */
class ControlCommandUpdatePortIn extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'UpdatePortIn';

    $portInQueue = new \PortInQueue();

    list ( $portInQueue , $parameters['portRequestId'] ) = $this->getPortRequestIdFromMSISDN( $portInQueue , $parameters['MSISDN'] );

    if ( ! $parameters['portRequestId'] )
    {
      $result = new \Result();

      $result->add_data_array(
        array(
          'message' => $this->buildInboundErrorMessage(
            $command,
            array('No portRequestId found for MSISDN '.$parameters['MSISDN']),
            array()
          )
        )
      );

      $result->succeed();

      return $result;
    }

    if ( empty($parameters['Address']) )
    {
      $parameters['firstName'] = 'Prepaid';
      $parameters['lastName']  = 'Customer';
      $parameters['fullName']  = '';
      $parameters['Address']   = '123 Main Street';
      $parameters['Address2']  = '';
      $parameters['cityName']  = 'Atlanta';
      $parameters['stateCode'] = 'GA';
      $parameters['zipCode']   = empty($parameters['zipCode']) ? '30319' : $parameters['zipCode'];
      $parameters['Country']   = 'USA';
    }
    else
    {
      $parameters['firstName'] = empty($parameters['firstName']) ? '' : $parameters['firstName'];
      $parameters['lastName']  = empty($parameters['lastName'])  ? '' : $parameters['lastName'];
      $parameters['fullName']  = empty($parameters['fullName'])  ? '' : $parameters['fullName'];
      $parameters['Address']   = empty($parameters['Address'])   ? '' : $parameters['Address'];
      $parameters['Address2']  = empty($parameters['Address2'])  ? '' : $parameters['Address2'];
      $parameters['cityName']  = empty($parameters['cityName'])  ? '' : $parameters['cityName'];
      $parameters['stateCode'] = empty($parameters['stateCode']) ? '' : $parameters['stateCode'];
      $parameters['zipCode']   = empty($parameters['zipCode'])   ? '' : $parameters['zipCode'];
      $parameters['Country']   = 'USA';
    }

    if ( !isset($parameters['donorAccountNumber']) || ( $parameters['donorAccountNumber'] == '' ) )
      $parameters['donorAccountNumber']   = '12345';

    if ( !isset($parameters['donorAccountPassword']) || ( $parameters['donorAccountPassword'] == '' ) )
      $parameters['donorAccountPassword'] = '1234';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    $result = $this->interactWithChannel( $command , $outboundControlMessage );

    if ( $result->is_success() )
    {
      // update PORTIN_QUEUE

      $updateResult = $portInQueue->updateOnUpdatePortIn(
        array(
          'account_number'   => $parameters['donorAccountNumber'],
          'account_password' => $parameters['donorAccountPassword'],
          'account_zipcode'  => $parameters['zipCode']
        )
      );

      if ( $updateResult->is_failure() )
        dlog('','ERROR ESCALATION ALERT DAILY - we could not update port credentials in PORTIN_QUEUE for MSISDN '.$parameters['MSISDN']);

      \save_port_account( $parameters['MSISDN'] , $parameters['donorAccountNumber'] , $parameters['donorAccountPassword'] );
    }

    return $result;
  }
}


/**
 * UpdateWholesalePlan control command implementation
 */
class ControlCommandUpdateWholesalePlan extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'UpdateWholesalePlan';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}


/**
 * Test control command
 * Allows to perform any call for test purposes.
 */
class ControlCommandTest extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    return $this->interactWithChannel( $parameters['option'] , $parameters['json_parameters'] );
  }
}


function cleanup_acc_sms_text( $sms_text )
{
  #$sms_text = implode( '&amp;' , explode('&',$sms_text) );
  #$sms_text = implode( '&' , explode('&',$sms_text) );
  #$sms_text = str_replace(',',  '.',      $sms_text);
  #$sms_text = str_replace('‘',  '&apos;', $sms_text);
  #$sms_text = str_replace('`',  '&apos;', $sms_text);
  #$sms_text = str_replace('\'', '&apos;', $sms_text);
  #$sms_text = str_replace('<',  '&lt;',   $sms_text);
  #$sms_text = str_replace('>',  '&gt;',   $sms_text);
  #$sms_text = str_replace('"',  '&quot;', $sms_text);

  $sms_text = preg_replace("/[\s\t\n\r\,]/",' ', $sms_text);

  return $sms_text;
}

