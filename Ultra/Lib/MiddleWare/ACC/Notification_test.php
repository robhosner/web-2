<?php

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';
require_once 'Ultra/Lib/MQ/EndPoint.php';

$actionUUID = time();

$notification = new \Ultra\Lib\MiddleWare\ACC\Notification( $actionUUID );

$method = new ReflectionMethod('\Ultra\Lib\MiddleWare\ACC\Notification','mapParameters');

$method->setAccessible(true);

    $r = $method->invoke(
      $notification,
      array('MSISDN' => '1234567890' )
    );

print_r($r);

$o = new \stdClass();

$o->MSISDN = '1234567890';

    $r = $method->invoke(
      $notification,
      $o
    );

print_r($r);

exit;

$params = array(
);

/*
print_r( $notificationProvisioningStatus );

$result = $notificationProvisioningStatus->processNotification($params);

print_r( $result );

$params = array(
  'MSISDN'                => '1001001000',
  'TransactionIDAnswered' => time(),
  'ProvisioningType'      => time(),
  'ProvisioningStatus'    => time()
);

$result = $notificationProvisioningStatus->processNotification($params);

print_r( $result );
*/

$params = array(
  'MSISDN'                => '1001001000',
  'TransactionIDAnswered' => time(),
  'ProvisioningType'      => 'PortStatusUpdate',
  'ProvisioningStatus'    => '1',
  'StatusDescription'     => 'PORT_RESOLUTION_REQUIRED'
);

#$result = $notificationProvisioningStatus->processNotification($params);

#print_r( $result );

$params['MSISDN'] = '2672655151';

$result = $notificationProvisioningStatus->processNotification($params);

print_r( $result );


exit;

$notificationThrottlingAlert = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert( $actionUUID );

print_r( $notificationThrottlingAlert );

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['Event'] = 123;

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['Event'] = 50;

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['MSISDN'] = 'a';

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['MSISDN'] = '9992229999';

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['MSISDN'] = '6176521251';

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['CounterName'] = 'a';

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

$params['CounterName'] = 'Base Data Usage';
$params['CounterName'] = 'WPRBLK30';
$params['Event'] = 80;
$params['Event'] = 50;

$result = $notificationThrottlingAlert->processNotification($params);
print_r( $result );

exit;

$endPoint = new \Ultra\Lib\MQ\EndPoint;

$accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Notification;

$message = $endPoint->buildMessage(
  array(
    'actionUUID' => time(),
    'header'     => 'MigrationNotification',
    'body'       => array(
      'success'       => TRUE,
      'errors'        => array(),
      'customer_id'   => 31,
      'msisdn'        => '1001001000'
    )
  )
);

// send up to Ultra/Adapter layer
$notificationUUID = $endPoint->enqueueNotificationChannel($message);

print_r( $notificationUUID );

?>
