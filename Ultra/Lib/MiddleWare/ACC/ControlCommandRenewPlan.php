<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * RenewPlan control command implementation
 */
class ControlCommandRenewPlan extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'RenewPlan';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addResourceData($parameters);
    $parameters = $this->addPlanDataOnRenew($parameters);

    unset( $parameters['ICCID'] );
    unset( $parameters['customer_id'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['wholesalePlan'] );
    unset( $parameters['preferredLanguage'] );
    unset( $parameters['throttle_speed'] );
    unset( $parameters['roam_credit_promo'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}
