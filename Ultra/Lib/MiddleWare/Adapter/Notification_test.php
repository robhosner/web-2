<?php

require_once 'db.php';
require_once('Ultra/Lib/MiddleWare/Adapter/Notification.php');

/*
  usage:
    php Ultra/Lib/MiddleWare/Adapter/Notification_test.php NOTIFIACTION_TYPE CUSTOMER_ID

  e.g.
    php Ultra/Lib/MiddleWare/Adapter/Notification_test.php PortStatusUpdate 19598
*/

if ($argc < 3)
  die("missing parameters: NOTIFICATION_TYPE CUSTOMER_ID\n");

// validate notification type
list($script, $test, $customer_id) = $argv;
$test = "test$test";
if ( ! function_exists($test))
  die("notification type $test does not exist");

// load subscriber
teldata_change_db();
$customer = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID, CURRENT_MOBILE_NUMBER, MVNE'));

// execute test
$test($customer);


function testPortStatusUpdate($customer)
{
    $params = array(
      'uuid'            => getNewUUID('TEST'),
      'actionUUID'      => getNewUUID('TEST'),
      'command'         => 'PortStatusUpdate',
        'parameters'    => array(
          'success'     => TRUE,
          'customer_id' => $customer->CUSTOMER_ID,
          'errors'      => array(),
          'msisdn'      => $customer->CURRENT_MOBILE_NUMBER));

    $notification = new Ultra\Lib\MiddleWare\Adapter\Notification;
    $result = $notification->processNotification($params);
    print_r($result);
}


function testMigrationNotification($customer)
{

  $uuid = time();
  $action_id = time();

  $c = new Ultra\Lib\MiddleWare\Adapter\NotificationMigrationNotification( $uuid , $action_id );
  print_r($c);

  if ( $customer->MVNE != '1' )
    die('customer mvne is not 1 : '.$customer->MVNE);

  $params = array(
    'customer_id' => $customer->customer_id,
    'msisdn'      => $customer->CURRENT_MOBILE_NUMBER
  );

  $r = $c->processCommand($params);
  print_r($r);
}

