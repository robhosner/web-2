<?php


namespace Ultra\Lib\MiddleWare\Adapter;


include_once('classes/Result.php');
include_once('lib/messaging/functions.php');
include_once('lib/transitions.php');
require_once('Ultra/Lib/MQ/EndPoint.php');


class Notification
{
  private $uuid;
  private $actionUUID;
  private $command;

  function __destruct()
  {
  }

  public function __construct()
  {
  }

  # process an inbound notification at Ultra Level
  public function processNotification($params)
  {
    dlog('',"params = %s",$params);

    $this->uuid       = ( isset($params['uuid'])       ) ? $params['uuid']       : '' ;
    $this->actionUUID = ( isset($params['actionUUID']) ) ? $params['actionUUID'] : '' ;
    $this->command = $params['command'];
    $parameters    = $params['parameters'];

    $notificationClass = '\Ultra\Lib\MiddleWare\Adapter\Notification'.$this->command;

    $result = new \Result();

    if ( class_exists( $notificationClass ) )
    {
      dlog('',"processNotification class = $notificationClass");

      # Factory
      $notificationObject = new $notificationClass( $this->uuid , $this->actionUUID );

      $result = $notificationObject->processCommand( (array) $parameters );
    }
    else
    {
      \logWarn( __CLASS__ . " notification ".$params['command']." not handled" );

      $result->add_error( __CLASS__ . " notification ".$params['command']." not handled" );
    }

    return $result;
  }
}

interface NotificationInterface
{
  public function processCommand($parameters);
}

class AdapterNotificationBase
{
  protected $uuid;
  protected $actionUUID;

  function __destruct()
  {
  }

  public function __construct( $uuid , $actionUUID )
  {
    $this->uuid       = $uuid;
    $this->actionUUID = $actionUUID;
  }
}

/**
 * MigrationNotification notification class
 *
 * Triggered when we get a notification concerning a migrated customer - AMDOCS-417
 */
class NotificationMigrationNotification extends AdapterNotificationBase implements NotificationInterface
{
  public function processCommand($parameters)
  {
    dlog('',"params = %s",$parameters);

    try
    {
      if ( !isset($parameters['customer_id']) || !$parameters['customer_id'] )
        throw new \Exception( 'No customer_id provided' );

      if ( !isset($parameters['msisdn']) || !$parameters['msisdn'] )
        throw new \Exception( 'No msisdn provided' );

      teldata_change_db();

      $customer = get_customer_from_customer_id( $parameters['customer_id'] );

      // error handling and sanity checks

      if ( !$customer )
        throw new \Exception( 'No customer found associated with customer_id '.$parameters['customer_id'] );

      if ( !$customer->current_mobile_number )
        throw new \Exception( 'The customer has no phone number' );

      if ( !$customer->CURRENT_ICCID_FULL )
        throw new \Exception( 'The customer has no ICCID' );

      if ( $customer->current_mobile_number != $parameters['msisdn'] )
        throw new \Exception( 'Customer msisdn mismatch '.$parameters['customer_id'].' - '.$customer->current_mobile_number.' - '.$parameters['msisdn'] );

      if ( $customer->MVNE == '2' )
        dlog('',"ESCALATION IMMEDIATE MIGRATION - customer id ".$parameters['customer_id']." - msisdn ".$parameters['msisdn']);

      // update ULTRA.ACC_MIGRATION_TRACKER status
      $update_success = update_migration_tracker_to_complete( $parameters['customer_id'] , $parameters['msisdn'] );

      if ( !$update_success )
        throw new \Exception('We could not update ULTRA.ACC_MIGRATION_TRACKER');

      // change sim to MVNE2
      $sql = htt_inventory_sim_update_query(
        array(
          "batch_sim_set" => array( $customer->CURRENT_ICCID_FULL ),
          "mvne"          => 2
        )
      );

      if ( ! is_mssql_successful(logged_mssql_query($sql)) )
        throw new \Exception('We could not update HTT_INVENTORY_SIM');

      // change msisdn to MVNE2
      $add_success = add_to_htt_ultra_msisdn( $customer->CUSTOMER_ID , $customer->current_mobile_number , 1 , $this->actionUUID , '2' );

      if ( !$add_success )
        throw new \Exception('We could not add a record to HTT_ULTRA_MSISDN');

      // change customer to MVNE2
      $sql = htt_customers_overlay_ultra_update_query(
        array(
          'customer_id' => $customer->CUSTOMER_ID,
          'mvne'        => '2'
        )
      );

      if ( ! is_mssql_successful(logged_mssql_query($sql)) )
        throw new \Exception('We could not update HTT_CUSTOMERS_OVERLAY_ULTRA');

      // fix redis for MVNE cache
      \Ultra\Lib\DB\Getter\setScalarMVNE( '2' , $customer->current_mobile_number , $customer->CURRENT_ICCID_FULL , $customer->CUSTOMER_ID );

      // store row in HTT_BILLING_HISTORY
      $history_params = array(
        "customer_id"            => $customer->CUSTOMER_ID,
        "date"                   => 'now',
        "cos_id"                 => $customer->COS_ID,
        "entry_type"             => 'MIGRATION',
        "stored_value_change"    => 0,
        "balance_change"         => 0,
        "package_balance_change" => 0,
        "charge_amount"          => 0,
        "reference"              => 'MIGRATION',
        "reference_source"       => 'NOTIFICATION',
        "detail"                 => 'MIGRATION',
        "description"            => 'MIGRATION',
        "result"                 => 'COMPLETE',
        "source"                 => 'MIGRATION',
        "is_commissionable"      => 0,
        "terminal_id"            => '0'
      );

      $sql = htt_billing_history_insert_query( $history_params );

      if ( !is_mssql_successful( logged_mssql_query( $sql ) ) )
        throw new \Exception('We could not add a record to HTT_BILLING_HISTORY');
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      return \make_error_Result( $e->getMessage() );
    }

    return \make_ok_Result();
  }
}

/**
 * PortStatusUpdate notification class
 *
 * Triggered when we get an update concerning a port in attempt
 */
class NotificationPortStatusUpdate implements NotificationInterface
{
  // this is for recording mint paid events after successful portin
  public function recordPaidEvent($customer_id, $soap_log_id)
  {
    $success = TRUE;

    try
    {
      $customer = \get_ultra_customer_from_customer_id($customer_id, ['brand_id']);
      if ( ! $customer || ! $customer->brand_id)
        throw new \Exception("ERROR getting customer ($customer_id) from database");

      // check if mint customer
      if ( ! \Ultra\Lib\Util\validateMintBrandId($customer->brand_id))
        return TRUE; // was not a MINT customer, do not record anything

      $account = \get_account_from_customer_id($customer_id, ['cos_id']);
      if ( ! $account || ! $account->cos_id)
        throw new \Exception("ERROR getting account ($customer_id) from database");

      $soc_name = \get_mint_soc_name_from_cos_id($account->cos_id);
      if ( ! $soc_name)
        throw new Exception("ERROR determining soc_name for mint account cos_id ({$account->cos_id})");

      // connect to acc DB
      \Ultra\Lib\DB\ultra_acc_connect();

      $check = \insert_soap_paid_event($soap_log_id, $soc_name);

      if ( ! $check)
        throw new \Exception("ERROR adding paid event to database ($soap_log_id)");
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
      $success = FALSE;
    }

    teldata_change_db();

    return $success;
  }

  public function processCommand($parameters)
  {
    dlog('',"params = %s",$parameters);

    $result = new \Result();

    if ( ! isset($parameters['customer_id']) || ! $parameters['customer_id'] )
      return make_error_Result('NotificationPortStatusUpdate::processCommand invoked with no customer id');

    $context = array('customer_id' => $parameters['customer_id']);

    if ( $parameters['success'] )
    {
      // attempt recording paid event
      // dont drop out of activation flow if error
      if ( ! $this->recordPaidEvent($parameters['customer_id'], $parameters['soap_log_id']))
        \logError('ERROR writing paid event for MINT customer on port-in success');

      // transition [ Port-In Requested ] => [ Active | Provisioned ]
      logInfo("Attempting transition to Active {$parameters['customer_id']}");
      $change = change_state($context, TRUE, 'Port Activated', 'take transition', FALSE, 1); # [ Port-In Requested ] => [ Active ]

      if ( ! $change['success'] )
      {
        // API-249: transitioned Web POS subscribers to Port-In Denied so that port can be re-initiated or refunded
        $transition = get_webpos_customer($parameters['customer_id']) ? 'Can Not Port' : 'Port Provisioned';

        // transition immediately
        logInfo("Attempting transition to $transition for customer {$parameters['customer_id']}");
        $change = change_state($context, TRUE, $transition, 'take transition', FALSE, 1);
        if ( $change['success'] )
          logInfo("Successfully transitioned customer id {$parameters['customer_id']} to $transition");
        else
          logInfo("Failed to transition customer id {$parameters['customer_id']} to $transition");
      }
      else
        logInfo("Customer id {$parameters['customer_id']} should now be Active");
    }
    else
    {
      // transition [ Port-In Requested ] => [ Port-In Denied ]

      $change = change_state($context, TRUE, 'Can Not Port', 'take transition', FALSE, 1); # [ Port-In Requested ] => [ Port-In Denied ]

      if ( $change['success'] )
        dlog('', 'customer id '.$parameters['customer_id'].' should now be in Port-In Denied');
      else
        dlog('', 'We could not transition customer id '.$parameters['customer_id'].' to Port-In Denied');
    }

    $result->succeed();

    return $result;
  }
}

/**
 * ThrottlingAlert notification class
 *
 * Triggered when we get a ThrottlingAlert notification from Amdocs
 */
class NotificationThrottlingAlert implements NotificationInterface
{
  public function processCommand($parameters)
  {
    dlog('',"params = %s",$parameters);

    $result = new \Result();

    #TODO:

    $result->succeed();

    return $result;
  }
}

/**
 * NotificationReceived notification class
 *
 * Triggered when we get a NotificationReceived notification from Amdocs
 */
class NotificationNotificationReceived implements NotificationInterface
{
  public function processCommand($parameters)
  {
    dlog('',"params = %s",$parameters);

    $result = new \Result();

    if ( $parameters['shortCode'] && ( $parameters['shortCode'] == '6700' ) )
    {
      // AMDOCS-276 - [6700] reply with "Customer Self Care is currently unavailable. Please call 611 for any questions."

      if ( $parameters['msisdn'] )
      {
        $return = funcSendExemptCustomerSMSSelfCareUnavailable(
          array(
            'msisdn' => $parameters['msisdn']
          )
        );

        dlog('',"funcSendExemptCustomerSMSSelfCareUnavailable return = %s",$return);

        $result->succeed();

        if ( ! $return['success'] )
          $result->add_error('We could not enqueue SMS for msisdn '.$parameters['msisdn']);
      }
      else
        $result->add_error('Missing msisdn for shortCode 6700');
    }
    else
    {
      dlog('','Nothing to do');

      $result->add_warning( 'Nothing to do' );
      $result->succeed();
    }

    return $result;
  }
}

?>
