<?php

namespace Ultra\Lib\MeS;

// connection parameters to improve code robustness
define('MES_CONNECT_TIMEOUT'  , 5 ); // give up if cannot connect, seconds
define('MES_RESPONSE_TIMEOUT' , 30); // give up if cannot receive response after connecting, seconds

/**
 * verify
 *
 * Invokes MeS API for Verification
 * Used during saving of Credit Card to check for a valid card and check CVV2 validity.
 * Input:
 *  - 'address'
 *  - 'zipcode'
 *  - 'amount'
 *  - 'cc_number'
 *  - 'expires_date'
 *  - 'cvv'
 *
 * @returns a Result object
 */
function verify( $input )
{
  $params = array(
    'transaction_type' => 'A',
    'profile_id'       => get_profile_id(),
    'profile_key'      => get_profile_key(),
    'cardholder_street_address' => $input['address'],
    'cardholder_zip'            => $input['zipcode'],
    'transaction_amount'        => $input['amount'],
    'card_exp_date'             => $input['expires_date'],
    'cvv2'                      => $input['cvv'],
    'client_reference_number'   => $input['client_reference_number']
  );

  if (!empty($input['cc_number'])) {
    unset($params['card_number']);
    $params['card_number'] = $input['cc_number'];
  }

  if (!empty($input['token'])) {
    unset($params['card_number']);
    $params['card_id'] = $input['token'];
  }

  $result = perform_call( $params );

  if ( $result->is_failure() )
    return $result;

  // we expect '000' or '085'
  if ( !in_array( $result->data_array['error_code'] , array('000','085') ) )
    return \make_error_Result( $result->data_array['errors'] );

  if (!empty($input['cvv'])) {
    if ( !isset($result->data_array['cvv2_result']) )
      return \make_error_Result( 'cvv2_result not provided after MeS Verification' );

    // if cvv2_result is returned, it must be M or P. Otherwise fail CVV2 check.
    if ( !in_array( $result->data_array['cvv2_result'] , array('M','P') ) )
      return \make_error_Result( 'CVV2 check failed : '.$result->data_array['cvv2_result'] );
  }

  if (!empty($input['address']) && !empty($input['zipcode'])) {
    if ( !isset($result->data_array['avs_result']) )
      return \make_error_Result( 'avs_result not provided after MeS Verification' );
  }

  $transaction_id = isset($result->data_array['transaction_id']) ? $result->data_array['transaction_id'] : NULL ;

  return make_ok_Result(
    array(
      'transaction_id' => $transaction_id,
      'cvv_validation' => $result->data_array['cvv2_result'],
      'avs_validation' => $result->data_array['avs_result']
    )
  );
}

/**
 * sale
 *
 * Invokes MeS API for charging a CC .
 * For customer-initiated purchases . ( $recurring=FALSE )
 * Input:
 *  - 'address'
 *  - 'zipcode'
 *  - 'amount'
 *  - 'token'
 *
 * @returns a Result object
 */
function sale( $input , $recurring=FALSE )
{
  $invoice_number = ( isset($input['invoice_number']) && $input['invoice_number'] )
                    ?
                    $input['invoice_number']
                    :
                    get_invoice_number()
                    ;

  $params = array(
    'transaction_type' => 'D',
    'profile_id'       => get_profile_id(),
    'profile_key'      => get_profile_key(),
    'cardholder_street_address' => $input['address'],
    'cardholder_zip'            => $input['zipcode'],
    'transaction_amount'        => $input['amount'],
    'card_id'                   => $input['token'],
    'card_exp_date'             => $input['expires_date'],
    'invoice_number'            => $invoice_number,
    'client_reference_number'   => $input['client_reference_number']
  );

  if ( $recurring )
    $params['moto_ecommerce_ind'] = '2';

  $result = perform_call( $params );

  $transaction_id = isset($result->data_array['transaction_id']) ? $result->data_array['transaction_id'] : NULL ;

  if ( $result->is_failure() )
    return $result;

  if ( $result->data_array['error_code'] != '000' )
    return \make_error_Result(
      '(' . $result->data_array['error_code'] . ') - ' . $result->data_array['auth_response_text'] ,
      array( 'transaction_id' => $transaction_id )
    );

  return make_ok_Result(
    array(
      'transaction_id' => $transaction_id,
      'invoice_number' => $invoice_number
    )
  );
}

/**
 * sale_recurring
 *
 * Used for recurring charges
 * Input:
 *  - 'address'
 *  - 'zipcode'
 *  - 'amount'
 *  - 'token'
 *
 * @returns a Result object
 */
function sale_recurring( $input )
{
  return sale( $input , TRUE );
}

/**
 * refund
 *
 * Invokes MeS API for a Refund after a successful Sale, which did not meet our CVV or AVS criteria.
 * Input:
 *  - 'transaction_id'
 *
 * @returns a Result object
 */
function refund( $input )
{
  return refund_or_void( $input );
}

/**
 * void
 *
 * Invokes MeS API for voiding a sale.
 * Input:
 *  - 'transaction_id'
 *
 * @returns a Result object
 */
function void( $input )
{
  return refund_or_void( $input );
}

/**
 * refund_or_void
 *
 * 'transaction_type' U is used for both Voids and Refunds.
 * There is no distinction between void and refund in this context.
 * Input:
 *  - 'transaction_id'
 *
 * @returns a Result object
 */
function refund_or_void( $input )
{
  $params = array(
    'transaction_type'        => 'U',
    'profile_id'              => get_profile_id(),
    'profile_key'             => get_profile_key(),
    'transaction_id'          => $input['transaction_id'],
    'client_reference_number' => $input['client_reference_number']
  );

  $result = perform_call( $params );

  if ( $result->is_failure() )
    return $result;

  if ( $result->data_array['error_code'] != '000' )
    return \make_error_Result( 'ERR_API_INTERNAL: (' . $result->data_array['error_code'] . ') - ' . $result->data_array['auth_response_text'] );

  return make_ok_Result(
    array(
      'token' => $result->data_array['transaction_id']
    )
  );
}

/**
 * store
 *
 * Invokes MeS API for storing a card
 * Tokenizes the customer's CC. Each CC num + expiration date combo results in a unique token.
 * Input:
 *  - 'cc_number'
 *  - 'expires_date'
 * Output:
 *  - 'token'
 *
 * @returns a Result object
 */
function store( $input )
{
  $params = array(
    'transaction_type'        => 'T',
    'profile_id'              => get_profile_id(),
    'profile_key'             => get_profile_key(),
    'card_number'             => $input['cc_number'],
    'card_exp_date'           => $input['expires_date'],
    'client_reference_number' => $input['client_reference_number']
  );

  $result = perform_call( $params );

  if ( $result->is_failure() )
    return $result;

  if ( $result->data_array['error_code'] != '000' )
    return \make_error_Result( 'ERR_API_INTERNAL: (' . $result->data_array['error_code'] . ') - ' . $result->data_array['auth_response_text'] );

  if ( !isset( $result->data_array['transaction_id'] ) || !$result->data_array['transaction_id'] )
    return \make_error_Result( 'ERR_API_INTERNAL: no transaction_id in gateway response' );

  return make_ok_Result(
    array(
      'token' => $result->data_array['transaction_id']
    )
  );
}

/**
 * perform_call
 *
 * Invokes MeS API
 *
 * @returns a Result object
 */
function perform_call( $params , $url=NULL )
{
  dlog('', "(%s)", func_get_args());

  $result = new \Result;

  try
  {
    // init HTTP session
    $ch = curl_init();

    if ( !$ch )
      throw new \Exception('Failed session initialization');

    $post_fields = '';

    foreach( $params as $param => $value )
      $post_fields .= $param . '=' . $value . '&' ;

    if ( ! $url )
      $url = get_url();

    dlog('',"url = $url");

    // configure session options
    $options = array(
      CURLOPT_POST            => TRUE,
      CURLOPT_POSTFIELDS      => $post_fields,
      CURLOPT_RETURNTRANSFER  => TRUE,
      CURLOPT_SSL_VERIFYPEER  => TRUE,
      CURLOPT_URL             => $url,
      CURLOPT_CONNECTTIMEOUT  => MES_CONNECT_TIMEOUT,
      CURLOPT_TIMEOUT         => MES_RESPONSE_TIMEOUT
    );

    if ( !curl_setopt_array($ch, $options) )
      throw new \Exception('Failed session configuration');

    $response = curl_exec( $ch );

    if ( !$response )
      throw new \Exception('Invalid response from MeS API call: ' . curl_error($ch), curl_errno($ch));

    $chunks = explode('&',$response);

    dlog('', 'MeS API Response: ' . json_encode($response));

    foreach ( $chunks as $chunk )
    {
      $couple = explode('=',$chunk);
      $result->data_array[ $couple[0] ] = $couple[1];
    }

    if (!empty($result->data_array['error_code']) && $result->data_array['error_code'] != '000' && $result->data_array['error_code'] != '085')
    {
      $result->data_array['errors'] = array();
      $result->data_array['errors']['user_errors'] = getMeaningfulApiErrorMessage($result->data_array['error_code']);
      dlog('', 'Add Helpful Error:' . json_encode($result));
    }

    $result->succeed();
  }
  catch(\Exception $e)
  {
    $result->add_error( $e->getMessage() );
    dlog('', $e->getMessage());
  }

  dlog('', 'data = %s', $result->data_array);

  return $result;
}

function get_url()
{
  #return 'https://cert.merchante-solutions.com/mes-api/tridentApi'; // dev
  return \Ultra\UltraConfig\getCCCredentialsURL( 'MeS' );
}

function get_profile_id()
{
  #return '94100011811900000074'; // dev
  return \Ultra\UltraConfig\getCCCredentialsProfile( 'MeS' );
}

function get_profile_key()
{
  #return 'blGMSGehNOZCHESLZCFtZKvqJPUzVhbE'; // dev
  return \Ultra\UltraConfig\getCCCredentialsKey( 'MeS' );
}

/**
 * MeS specifically requires that we pass an 17 character or less alphanumeric invoice number when we do a Sale.
 * The invoice number should link back to the purchase.
 */
function get_invoice_number()
{
  list($usec, $sec) = explode(" ", microtime());

//TODO: verify this

  return substr( time() , -6).getmypid().substr( $usec , 2 , 6);
}

function getMeaningfulApiErrorMessage($mesResponseCode)
{
  $meaningfulMessagesByMesError = array(
    '001' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 001',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 001',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002001"
    ),
    '002' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 002',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 002',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002002"
    ),
    '003' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 003',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 003',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002003"
    ),
    '004' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 004',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 004',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002004"
    ),
    '005' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 005',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 005',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002005"
    ),
    '006' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 006',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 006',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002006"
    ),
    '007' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 007',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 007',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002007"
    ),
    '012' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 012',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 012',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002012"
    ),
    '013' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 013',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 013',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002013"
    ),
    '014' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 014',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 014',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002014"
    ),
    '015' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 015',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 015',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002015"
    ),
    '019' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 019',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 019',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002019"
    ),
    '021' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 021',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 021',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002021"
    ),
    '025' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 025',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 025',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002025"
    ),
    '030' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 030',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 030',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002030"
    ),
    '039' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 039',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 039',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002039"
    ),
    '041' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 041',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 041',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002041"
    ),
    '043' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 043',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 043',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002043"
    ),
    '051' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 051',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 051',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002051"
    ),
    '052' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 052',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 052',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002052"
    ),
    // 054 Expired card error is handled in
//    '054' => array(
//      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 054',
//      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 054',
//      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002054"
//    ),
    '055' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 055',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 055',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002055"
    ),
    '057' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 057',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 057',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002057"
    ),
    '058' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 058',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 058',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002058"
    ),
    '059' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 059',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 059',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002059"
    ),
    '061' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 061',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 061',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002061"
    ),
    '062' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 062',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 062',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002062"
    ),
    '065' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 065',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 065',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002065"
    ),
    '075' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 075',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 075',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002075"
    ),
    '078' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 078',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 078',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002078"
    ),
    '082' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 082',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 082',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002082"
    ),
    '091' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 091',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 091',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002091"
    ),
    '092' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 092',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 092',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002092"
    ),
    '093' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 093',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 093',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002093"
    ),
    '094' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 094',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 094',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002094"
    ),
    '096' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 096',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 096',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002096"
    ),
    '202' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 202',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 202',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002202"
    ),
    '106' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 106',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 106',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002106"
    ),
    '999' => array(
      'EN' => 'We are unable to properly process your Ultra payment. Please retry or use another card. Pay your bill at https://m.ultra.me - call 222 or visit retailer. 999',
      'ES' => 'No pudimos procesar tu pago con exito. Intenta otra vez o utiliza otra tarjeta. Paga tu factura en https://m.ultra.me - marca 222 o visita una minorista. 999',
      'ZH' => "\\u6211\\u4eec\\u65e0\\u6cd5\\u5904\\u7406\\u60a8\\u7684\\u4ed8\\u6b3e\\u3002\\u8bf7\\u91cd\\u8bd5\\u6216\\u4f7f\\u7528\\u5176\\u5b83\\u5361\\u3002\\u767b\\u5f55https://m.ultra.me -\\u62e8\\u6253222\\u6216\\u53bb\\u96f6\\u552e\\u5546\\u5904\\u4ed8\\u6b3e\\u3002999"
    )
  );

  if (isset($meaningfulMessagesByMesError[$mesResponseCode]))
  {
    return $meaningfulMessagesByMesError[$mesResponseCode];
  }

  return [
    'EN' => 'We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer',
    'ES' => 'No podemos procesar su Ultra Mobile transacción debido a un error del sistema. Por favor, vuelva a intentarlo - visita http://m.ultra.me/#/update - llamar 222 o ir a la tienda',
    'ZH' => "\\u6211\\u5011\\u7121\\u6cd5\\u8655\\u7406\\u60a8\\u7684\\u8d85\\u79fb\\u52d5\\u7684\\u4ea4\\u6613\\uff0c\\u7531\\u65bc\\u7cfb\\u7d71\\u932f\\u8aa4\\u3002\\u8acb\\u91cd\\u8a66 - \\u8a2ahttp://m.ultra.me/#/update - \\u81f4\\u96fb222\\u6216\\u5230\\u96f6\\u552e\\u5546"
  ];
}
?>
