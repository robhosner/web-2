<?php

/*

usage:

php Ultra/Lib/inComm/Incomm_test.php generateSrcRefNum
php Ultra/Lib/inComm/Incomm_test.php statusInquiry PIN
php Ultra/Lib/inComm/Incomm_test.php pinRedemption PIN CUSTOMER_ID
php Ultra/Lib/inComm/Incomm_test.php pinReversal PIN TRANSACTION CUSTOMER_ID
php Ultra/Lib/inComm/Incomm_test.php pinRedeemAndReverse PIN CUSTOMER_ID
php Ultra/Lib/inComm/Incomm_test.php encodeAccountNumber CUSTOMER_ID
php Ultra/Lib/inComm/Incomm_test.php decodeAccountNumber INCOMM_ACCOUNT

*/

require_once 'db.php';
require_once 'Ultra/UltraConfig.php';
require_once 'Ultra/Lib/inComm/Incomm.php';


abstract class AbstractTestStrategy
{
  abstract function test( $argv );
}


class Test_Incomm_pinRedemption extends AbstractTestStrategy
{
  function test( $argv )
  {
    teldata_change_db();
    $subscriber = get_customer_from_customer_id($argv[3]);
    $r = \Ultra\Lib\Incomm\pinRedemption($argv[2], $subscriber);

    print_r($r);
  }
}

class Test_Incomm_pinReversal extends AbstractTestStrategy
{
  function test( $argv )
  {
    teldata_change_db();
    $subscriber = get_customer_from_customer_id($argv[4]);
    $r = \Ultra\Lib\Incomm\pinReversal(
      array(
        'pin' => $argv[2],
        'SrcRefNum' => $argv[3]),
        $subscriber
    );

    print_r($r);
  }
}

class Test_Incomm_statusInquiry extends AbstractTestStrategy
{
  function test( $argv )
  {
    $r = \Ultra\Lib\Incomm\statusInquiry($argv[2]);

    print_r($r);
  }
}


class Test_Incomm_generateSrcRefNum extends AbstractTestStrategy
{
  function test( $argv )
  {
    $r = \Ultra\Lib\Incomm\generateSrcRefNum();

    print_r($r);
  }
}


class Test_Incomm_pinRedeemAndReverse extends AbstractTestStrategy
{
  function test($argv)
  {
    teldata_change_db();
    $subscriber = get_customer_from_customer_id($argv[3]);
    $result = \Ultra\Lib\Incomm\pinRedemption($argv[2], $subscriber);
    print_r($result);

    if ($result->is_success())
      $result = \Ultra\Lib\Incomm\pinReversal(array('pin' => $argv[2], 'SrcRefNum' => $result->get_data_key('SrcRefNum')), $subscriber);
    print_r($result);
  }
}


class Test_Incomm_encodeAccountNumber extends AbstractTestStrategy
{
  function test($argv)
  {
    teldata_change_db();
    $subscriber = get_customer_from_customer_id($argv[2]);
    if ( ! $subscriber)
      echo "FAILED to get subscriber from customer_id " . $argv[2];
    else
    {
      $account = \Ultra\Lib\Incomm\encodeAccountNumber($subscriber->ACCOUNT, $subscriber->current_mobile_number);
      echo "account: '$account'\n";
    }
  }
}

class Test_Incomm_decodeAccountNumber extends AbstractTestStrategy
{
  function test($argv)
  {
    teldata_change_db();
    $subscriber = \Ultra\Lib\Incomm\decodeAccountNumber($argv[2]);
    print_r($subscriber);
  }

}


# perform test #

$testClass = 'Test_Incomm_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test( $argv );




?>
