<?php
namespace Ultra\Utilities;

class Common
{
  /**
   * get_date_from_full_date
   *
   * @param String $date
   *          The date is in the format of "Oct 2 2012 08:39:38:233PM" returned by SQL Server
   * @return String Oct 2 2012
   */
  public function getDateFromFullDate($date)
  {
    // Oct 2 2012 08:39:38:233PM => Oct 2 2012
    if (preg_match("/^(\w+)\s+(\d+)\s+(2\d\d\d)/", $date, $matches)) {
      return $matches[1] . " " . $matches[2] . " " . $matches[3];
    } else if (preg_match("/^(\d{4}+)[-](\d{2}+)[-](\d{2})/", $date, $matches)) {
      return (new \DateTime($date))->format('M d Y');
    } else {
      dlog('', 'No matches found.');
      return '';
    }
  }

  public function luhnenize($iccid)
  {
    return \luhnenize($iccid);
  }

  public function getNewActionUUID($namespace)
  {
    return \getNewActionUUID($namespace);
  }

  public function teldataChangeDb()
  {
    return \teldata_change_db();
  }

  public function encryptPasswordHS($password)
  {
    return \Ultra\Lib\Util\encryptPasswordHS($password);
  }

  public function authenticatePasswordHS($encryptedPassword, $givenPassword)
  {
    return \Ultra\Lib\Util\authenticatePasswordHS($encryptedPassword, $givenPassword);
  }

  public function computeDeliveryDaytime($timestamp, $utc_offset, $priority)
  {
    return \compute_delivery_daytime($timestamp, $utc_offset, $priority);
  }
}