<?php
namespace Ultra\Utilities;

class Validator
{
  public function validatorCC($name, $value)
  {
    return \Ultra\Lib\Util\validatorCC($name, $value);
  }

  public function validatorCVV($name, $value)
  {
    return \Ultra\Lib\Util\validatorCVV($name, $value);
  }

  public function validatorDateformat($name, $value, $format)
  {
    return \Ultra\Lib\Util\validatorDateformat($name, $value, $format);
  }

  public function validatorAddress($name, $value)
  {
    return \Ultra\Lib\Util\validatorAddress($name, $value);
  }

  public function validatorZipcode($name, $value, $option)
  {
    return \Ultra\Lib\Util\validatorZipcode($name, $value, $option);
  }

  public function validatorName($name, $value)
  {
    return \Ultra\Lib\Util\validatorName($name, $value);
  }

  public function validatorCity($name, $value)
  {
    return \Ultra\Lib\Util\validatorCity($name, $value);
  }

  public function validatorCountry($name, $value)
  {
    return \Ultra\Lib\Util\validatorCountry($name, $value);
  }

  public function validatorStringformat($name, $value, $field)
  {
    return \Ultra\Lib\Util\validatorStringformat($name, $value, $field);
  }

  public function validatorMaxstrlen($name, $value, $length)
  {
    return \Ultra\Lib\Util\validatorMaxstrlen($name, $value, $length);
  }

  public function validatorMinstrlen($name, $value, $length)
  {
    return \Ultra\Lib\Util\validatorMinstrlen($name, $value, $length);
  }

  public function validateZipCodeIsInCoverage($zipcode)
  {
    return \validateZipCodesCoverage([$zipcode]);
  }
}
