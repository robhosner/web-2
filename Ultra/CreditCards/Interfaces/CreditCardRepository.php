<?php
namespace Ultra\CreditCards\Interfaces;

use Ultra\CreditCards\CreditCard;
use Ultra\Exceptions\SaveMethodFailedException;

/**
 * Interface CreditCardRepository
 * @package Ultra\CreditCards\Interfaces
 */
interface CreditCardRepository
{
  /**
   * @param array $params
   * @return mixed
   */
  public function addToUltraCCHolderTokens(array $params);

  /**
   * @param array $params
   * @return mixed
   */
  public function addToUltraCCHolders(array $params);

  /**
   * Get credit card info from customer id.
   *
   * @param $customerId
   * @return object
   */
  public function getCcInfoFromCustomerId($customerId);

  /**
   * Save credit card information.
   *
   * @param CreditCard $creditCard
   * @return bool
   * @throws SaveMethodFailedException
   */
  public function saveCreditCardInformation(CreditCard $creditCard);

  /**
   * Get customer's credit card information including token.
   *
   * @param $customerId
   * @return CreditCard
   */
  public function getCCInfoAndTokenByCustomerId($customerId);
}