<?php

require 'db.php';

use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;

teldata_change_db();

$creditCardRepo = new CreditCardRepository;

$customer = $creditCardRepo->getUltraCCHolderByDetails(
  [
    'customer_id'   => 31,
    'bin'           => '545487',
    'last_four'     => '0964',
    'expires_date'  => '0815'
  ]
);

$cc_holders_id = $creditCardRepo->addToUltraCCHolders(
  [
    'customer_id'   => 40,
    'bin'           => '123456',
    'last_four'     => '1234',
    'expires_date'  => '1216'
  ]
);

echo PHP_EOL . "addToUltraCCHolders SCOPE IDENTITY: " . $cc_holders_id . PHP_EOL;

$cc_holder_tokens_id = $creditCardRepo->addToUltraCCHolderTokens(
  [
    'cc_holders_id'    => $cc_holders_id,
    'gateway'          => 'test_gateway',
    'merchant_account' => 'test_merchant_account',
    'token'            => 'test_token'
  ]
);

echo PHP_EOL . "addToUltraCCHolderTokens SCOPE IDENTITY: " . $cc_holder_tokens_id . PHP_EOL;
