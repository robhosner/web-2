<?php
namespace Ultra\Configuration;

use Exception;

class Configuration
{
  public function getShortNameFromBrandId($brandId)
  {
    return \Ultra\UltraConfig\getShortNameFromBrandId($brandId);
  }

  public function getDealerLocatorThresholds()
  {
    return \Ultra\UltraConfig\getDealerLocatorThresholds();
  }

  public function getBrandFromShortName($shortName)
  {
    return \Ultra\BrandConfig\getBrandFromShortName($shortName);
  }

  public function getPlanNameFromShortName($planShortName)
  {
    return \get_plan_name_from_short_name($planShortName);
  }

  public function mapLanguageToVoicemailSoc($preferredLanguage)
  {
    return \Ultra\UltraConfig\mapLanguageToVoicemailSoc($preferredLanguage);
  }

  public function getAccSocsDefinitions()
  {
    return \Ultra\MvneConfig\getAccSocsDefinitions();
  }

  public function getPlanFromCosId($cosId)
  {
    return \get_plan_from_cos_id($cosId);
  }

  public function getPlanCostByCosId($cosId)
  {
    return \get_plan_cost_by_cos_id($cosId);
  }

  public function getCosIdFromPlan($plan)
  {
    return \get_cos_id_from_plan($plan);
  }

  public function getPlanCostFromCosId($cosId)
  {
    return \get_plan_cost_from_cos_id($cosId);
  }

  public function getBoltOnsCostsByCustomerId($customerId)
  {
    return \get_bolt_ons_costs_by_customer_id($customerId);
  }

  public function getThrottlingSpeedMap()
  {
    return [
      'plan'  => [
        'full' => ['feature_id' => 12909, 'type' => 'WPRBLK20' ],
        '1536' => ['feature_id' => 12917, 'type' => 'WPRBLK33S'],
        '1024' => ['feature_id' => 12919, 'type' => 'WPRBLK35S'],
      ],
      'block' => [
        'full' => ['feature_id' => 12907, 'type' => 'WPRBLK30' ],
        '1536' => ['feature_id' => 12918, 'type' => 'WPRBLK34S'],
        '1024' => ['feature_id' => 12920, 'type' => 'WPRBLK36S'],
      ]
    ];
  }

  public function getThrottleSpeedFromKey($key)
  {
    $key = (string) $key;
    $map = [
      '2.0' => 'full',
      '1.5' => '1536',
      '1.0' => '1024',
    ];

    if (in_array($key, array_keys($map))) {
      return $map[$key];
    } else {
      return false;
    }
  }

  public function getThrottleSpeedNameByFeatureId($featureId)
  {
    foreach ($this->getThrottlingSpeedMap() as $throttleType) {
      foreach ($throttleType as $throttleOption => $throttleOptionValues) {
        if (in_array($featureId, $throttleOptionValues)) {
          return $throttleOption;
        }
      }
    }

    throw new Exception('No throttle option found for feature_id = ' . $featureId);
  }

  public function ccTransactionsEnabled()
  {
    return \Ultra\UltraConfig\cc_transactions_enabled();
  }

  public function getBrandIdFromBrand($brand)
  {
    return \Ultra\UltraConfig\getBrandIdFromBrand($brand);
  }

  public function getCCProcessorByName($processor)
  {
    return \Ultra\UltraConfig\getCCProcessorByName($processor);
  }

  public function getCCProcessorIDByName($name)
  {
    return \Ultra\UltraConfig\getCCProcessorIDByName($name);
  }

  public function getPaymentAPIConfig()
  {
    return \Ultra\UltraConfig\getPaymentAPIConfig();
  }

  public function getOnlineSalesAPIConfig()
  {
    return \Ultra\UltraConfig\getOnlineSalesAPIConfig();
  }

  public function getFamilyAPIConfig()
  {
    return \Ultra\UltraConfig\getFamilyAPIConfig();
  }

  public function getSharedILDConfig()
  {
    return \Ultra\UltraConfig\getSharedILDConfig();
  }

  public function getSharedDataConfig()
  {
    return \Ultra\UltraConfig\getSharedDataConfig();
  }

  public function getUserDeviceAPIConfig()
  {
    return \Ultra\UltraConfig\getUserDeviceAPIConfig();
  }

  public function isMintPlan($cosId)
  {
    return \Ultra\UltraConfig\isMintPlan($cosId);
  }

  public function mintMonthsByCosId($cosId)
  {
    return \Ultra\UltraConfig\planMonthsByCosId($cosId);
  }

  public function getCrmIpAddresses()
  {
    return \Ultra\UltraConfig\getCrmIpAddresses();
  }

  public function isBrandAllowedByAPIPartner($id, $partner)
  {
    return \Ultra\UltraConfig\isBrandAllowedByAPIPartner($id, $partner);
  }

  public function getBoltOnsMappingByType()
  {
    return \Ultra\UltraConfig\getBoltOnsMappingByType();
  }

  public function getBoltOnInfoByPlan($plan)
  {
    return \Ultra\UltraConfig\getBoltOnInfoByPlan($plan);
  }

  public function getLaunchDarklySdkKey()
  {
    return \Ultra\UltraConfig\getLaunchDarklySdkKey();
  }

  public function getBasePlanName($plan)
  {
    // full plan name map: 'MULTI_TWENTY_NINE'
    $map = array(PLAN_M29  => PLAN_L29);
    if ( ! empty($map[$plan]))
      return $map[$plan];

    // short plan name map
    $map = array('M29' => 'L29');
    if ( ! empty($map[$plan]))
      return $map[$plan];

    // not found: return as is
    return $plan;
  }

  public function getPlanConfiguration($plan)
  {
    $planConfig = \PlanConfig::Instance();
    return $planConfig->getPlanConfig($this->getPlanNameFromShortName($plan));
  }

  public function getTwilioConfig()
  {
    return \Ultra\UltraConfig\getTwilioCredentials();
  }

  public function getBrandIDFromCOSID($cosID)
  {
    return \get_brand_id_from_cos_id($cosID);
  }

  public function planMonthsByCosID($cosID)
  {
    return \Ultra\UltraConfig\planMonthsByCosId($cosID);
  }

  public function isBPlan($cosID)
  {
    return \Ultra\UltraConfig\isBPlan($cosID);
  }
}
