<?php
namespace Ultra\Configuration\Interfaces;

/**
 * Interface ConfigurationRepository
 * @package Ultra\Configuration\Interfaces
 */
interface ConfigurationRepository
{
  /**
   * Find the configuration by key.
   * 
   * @param $key
   * @return mixed
   */
  public function findConfigByKey($key);
}