<?php

require_once 'classes/PHPUnitBase.php';

class UltraConfigTest extends PHPUnitBase
{

  public function setUp()
  {
    // teldata_change_db();
  }


  // getAccSocsUltraPlanConfig
  public function test_getAccSocsUltraPlanConfig()
  {
    // valid Ultra plans
    $testData = array('NINETEEN', 'L24', 'TWENTY_NINE', 'L34', 'L39', 'FORTY_FOUR', 'FORTY_NINE', 'L59');
    foreach ($testData as $plan)
    {
      $config = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($plan);
      echo "PLAN $plan: " . print_r($config, TRUE) . "\n";
      $this->assertGreaterThan(3, count($config)); // most plans have at least 3 SOCs
    }

    // invalid or empty Ultra plan
    $testData = array('STANDBY', 'ELEVEN');
    foreach ($testData as $plan)
    {
      $config = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($plan);
      echo "PLAN $plan: " . print_r($config, TRUE) . "\n";
      $this->assertEquals(0, count($config));
    }

    // test with customer ID
    $config = \Ultra\MvneConfig\getAccSocsUltraPlanConfig('L39', 12345);
    echo "PLAN $plan: " . print_r($config, TRUE) . "\n";
    $this->assertEquals(123456, $config['a_data_blk128']);
  }


  // isLimitedDataPlan
  public function test_isLimitedDataPlan()
  {
    $plans = array(
      'L19' => TRUE,
      'L24' => TRUE,
      'L29' => TRUE,
      'L34' => FALSE,
      'L39' => FALSE,
      'L44' => FALSE,
      'L49' => FALSE,
      'L59' => FALSE);

    foreach ($plans as $plan => $limited)
    {
      $result = \Ultra\MvneConfig\isLimitedDataPlan($plan);
      echo "plan: $plan, result: " . ($result ? 'TRUE' : 'FALSE') . ', expected: ' . ($limited ? 'TRUE' : 'FALSE') . "\n";
      $this->assertEquals($result, $limited);
    }
  }


  // getAccDataSocsByType
  public function test_getAccDataSocsByType()
  {
    $result = \Ultra\MvneConfig\getAccDataSocsByType('uknown');
    echo 'RESULT for unknown: ' . print_r($result, TRUE) . "\n";
    $this->assertEmpty($result);

    $result = \Ultra\MvneConfig\getAccDataSocsByType('base');
    echo 'RESULT for base: ' . print_r($result, TRUE) . "\n";
    $this->assertNotEmpty($result);
    $this->assertContains('b_data_blk', $result);

    $result = \Ultra\MvneConfig\getAccDataSocsByType('throttle');
    echo 'RESULT for throttle: ' . print_r($result, TRUE) . "\n";
    $this->assertNotEmpty($result);
    $this->assertContains('b_data_thr128', $result);

    $result = \Ultra\MvneConfig\getAccDataSocsByType('plan');
    echo 'RESULT for plan: ' . print_r($result, TRUE) . "\n";
    $this->assertNotEmpty($result);
    $this->assertContains('b_data_thr128', $result);
  }


  // getPlanBaseDataSoc
  public function test_getPlanBaseDataSoc()
  {
    $plans = array('L19', 'L24', 'L29', 'L34', 'L39', 'L44', 'L49', 'L59');
    foreach ($plans as $plan)
    {
      $result = \Ultra\MvneConfig\getPlanBaseDataSoc($plan);
      echo "PLAN: $plan -> BASE SOC: $result \n";
      $this->assertNotEmpty($result);
    }
  }


  // getAccSocDefinition
  public function test_getAccSocDefinition()
  {
    // valid SOCs
    $socs = array("n_lte", "r_unlimited", "b_voice", "b_sms", "b_data_thr128", "a_data_blk_plan", "a_data_blk128", "b_data_thr64");
    foreach ($socs as $soc)
    {
      $details = \Ultra\MvneConfig\getAccSocDefinition($soc);
      print_r($details);
      $this->assertNotEmpty($details);
    }
  }


  // getAccSocDefinitionField
  public function test_getAccSocDefinitionField()
  {
    $data = array(
      // SOC          field               value
      'n_lte          plan_id             12808',
      'n_noroam       tmo_service_name    NOROAM',
      'b_data_thr128  ultra_service_name  B-DATA-THR128',
      'a_data_blk_dr  parameter           mbytes',
      'b_data_thr64   description         4G data, then throttle to 64kbps',
      'r_limited      plan_id             12007');

    foreach ($data as $soc)
    {
      $values = explode(' ', preg_replace('/\s+/', ' ', $soc), 3);
      $value = \Ultra\MvneConfig\getAccSocDefinitionField($values[0], $values[1]);
      echo "SOC: {$values[0]}, field: {$values[1]}, expected: {$values[2]}, actual: $value\n";
      $this->assertEquals($value, $values[2]);
    }
  }
  
  // getAccSocsDefinitions
  public function test_getAccSocsDefinitions()
  {
    $socs = \Ultra\MvneConfig\getAccSocsDefinitions();
    print_r($socs);
  }

}
