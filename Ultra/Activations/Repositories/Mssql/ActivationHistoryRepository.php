<?php
namespace Ultra\Activations\Repositories\Mssql;

require_once __DIR__ . '/../../Interfaces/ActivationHistoryRepository.php';

class ActivationHistoryRepository implements \Ultra\Activations\Interfaces\ActivationHistoryRepository
{
  /**
   * @param $customer_id
   */
  public function getHistory($customer_id)
  {
    return get_ultra_activation_history($customer_id);
  }

  /**
   * @param $customer_id
   * @param $provider_name
   * @param $load_amount in dollars
   */
  public function logFunding($customer_id, $provider_name, $load_amount)
  {
    return log_funding_in_activation_history($customer_id, $provider_name, $load_amount);
  }
}