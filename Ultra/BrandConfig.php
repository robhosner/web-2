<?php

/**
 * Brand Configuration File
 *
 */

namespace Ultra\BrandConfig;


/**
 * getBrandFromShortName
 *
 * return full brand information
 * @param  String brand short name (e.g. 'ULTRA')
 * @return Array  of brand info
 */
function getBrandFromShortName($short_name)
{
  // get brand definitions
  $short_name = strtoupper(trim($short_name));
  $brands = find_credential('brands');
  return empty($brands[$short_name]) ? NULL : $brands[$short_name];
}
