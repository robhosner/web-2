<?php
namespace Ultra\Taxes;

use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Exceptions\MissingRequiredParametersException;

class TransactionTypeCodeConfig
{
  const PLAN = 'PLAN';
  const DATA = 'DATA';
  const IDDCA = 'IDDCA';
  const ROAM = 'ROAM';
  const GLOBE = 'GLOBE';
  const ADD_BALANCE = 'ADD_BALANCE';
  const LINE_CREDIT = 'LINE_CREDIT';
  const REPLACE_SIM = 'REPLACE_SIM';

  private $brandConf = [
    'ULTRA' => [
      self::PLAN        => null,
      self::DATA        => null,
      self::IDDCA       => null,
      self::ROAM        => null,
      self::GLOBE       => null,
      self::ADD_BALANCE => null,
      self::LINE_CREDIT => null,
      self::REPLACE_SIM => null
    ],
    'MINT' => [
      self::PLAN        => null,
      self::DATA        => null,
      self::ROAM        => null,
      self::ADD_BALANCE => null,
      self::REPLACE_SIM => null
    ]
  ];

  private $brandPrefix = [
    'ULTRA' => 'sureTax/TransactionTypeCode/ULTRA/',
    'MINT' => 'sureTax/TransactionTypeCode/MINT/',
  ];

  public function __construct(ConfigurationRepository $configurationRepository)
  {
    foreach ($this->brandConf as $brand => $items) {
      foreach ($items as $brandItem => $value) {
        $this->brandConf[$brand][$brandItem] = $configurationRepository->findConfigByKey($this->brandPrefix[$brand] . $brandItem);

        if (empty($this->brandConf[$brand][$brandItem])) {
          throw new MissingRequiredParametersException(__CLASS__, array_keys($this->brandConf[$brand]));
        }
      }
    }
  }

  public function toArray()
  {
    return $this->brandConf;
  }
}
