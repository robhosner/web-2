<?php
namespace Ultra\Sims\Repositories\Mssql;

use Ultra\Sims\Sim;

require_once __DIR__ . '/../../Interfaces/SimRepository.php';

/**
 * Class SimRepository
 * @package Ultra\Sims\Repositories\Mssql
 */
class SimRepository implements \Ultra\Sims\Interfaces\SimRepository
{
  /**
   * Get Sim from actcode.
   * 
   * @param $actcode
   * @return mixed
   * @throws \Exception
   */
  public function getSimFromActCode($actcode)
  {
    $sim = get_htt_inventory_sim_from_actcode($actcode);

    if (empty($sim) || !is_array($sim))
    {
      return false;
    }
    
    return $sim[0];
  }

  public function getSimFromIccid($iccid)
  {
    return get_htt_inventory_sim_from_iccid($iccid);
  }

  /**
   * Get sim inventory and batch information.
   *
   * @param $iccid
   * @return Sim
   */
  public function getSimInventoryAndBatchInfoByIccid($iccid)
  {
    $query = sprintf(
      "SELECT
        LTRIM(RTRIM(s.ICCID_NUMBER)) ICCID_NUMBER,
        IMSI,
        SIM_ACTIVATED,
        INVENTORY_MASTERAGENT,
        INVENTORY_DISTRIBUTOR,
        INVENTORY_DEALER,
        CUSTOMER_ID,
        INVENTORY_STATUS,
        LAST_CHANGED_DATE,
        LAST_CHANGED_BY,
        CREATED_BY_DATE,
        CREATED_BY,
        s.ICCID_BATCH_ID,
        LTRIM(RTRIM(ICCID_FULL)) ICCID_FULL,
        s.EXPIRES_DATE,
        LAST_TRANSITION_UUID,
        PIN1,
        PUK1,
        PIN2,
        PUK2,
        OTHER,
        RESERVATION_TIME,
        GLOBALLY_USED,
        ACT_CODE,
        PRODUCT_TYPE,
        STORED_VALUE,
        SIM_HOT,
        MVNE,
        CASE WHEN ( s.EXPIRES_DATE IS NOT NULL AND s.EXPIRES_DATE <= GETUTCDATE() )
             THEN 1
             ELSE 0
        END is_expired,
        OFFER_ID,
        BRAND_ID,
        s.ICCID_BATCH_ID,
        TMOPROFILESKU
      FROM HTT_INVENTORY_SIM s with (nolock)
      INNER JOIN HTT_INVENTORY_SIMBATCHES sb ON s.ICCID_BATCH_ID = sb.ICCID_BATCH_ID
      where ICCID_FULL = %s",
      mssql_escape_with_zeroes($iccid));

    return new Sim((array) find_first($query));
  }
}
