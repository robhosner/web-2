<?php
namespace Ultra\Sims\Interfaces;

use Ultra\Sims\Sim;

/**
 * Interface SimRepository
 * @package Ultra\Sims\Interfaces
 */
interface SimRepository
{
  /**
   * Get Sim from actcode.
   *
   * @param $actcode
   * @return mixed
   * @throws \Exception
   */
  public function getSimFromActCode($actcode);

  public function getSimFromIccid($iccid);

  /**
   * Get sim inventory and batch information.
   *
   * @param $iccid
   * @return Sim
   */
  public function getSimInventoryAndBatchInfoByIccid($iccid);
}