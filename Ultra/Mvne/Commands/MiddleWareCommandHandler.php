<?php
namespace Ultra\Mvne\Commands;

use Result;
use Ultra\Exceptions\MiddleWareException;

/**
 * Class MiddleWareCommandHandler
 * @package Ultra\Mvne\Commands
 */
class MiddleWareCommandHandler
{
  /**
   * @param $result
   * @throws MiddleWareException
   */
  public function handleMiddleWareResultCheck(Result $result)
  {
    if ($result->is_failure()) {
      throw new MiddleWareException('ERR_API_INTERNAL: MW error (1)', 'MW0001');
    }

    if (!empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100') {
      throw new MiddleWareException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');
    }

    if (!$result->data_array['success'] || empty($result->data_array['body'])) {
      throw new MiddleWareException('ERR_API_INTERNAL: MW error (2)', 'MW0001');
    }
  }
}
