<?php
namespace Ultra\Mvne\Commands;

use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\MiddleWare\Adapter\Control;

class AddWIFISocCommandHandler extends MiddleWareCommandHandler
{
  /**
   * @var Control
   */
  private $middleWareAdapter;

  /**
   * AddWIFISocCommandHandler constructor.
   * @param Control $middleWareAdapter
   */
  public function __construct(Control $middleWareAdapter)
  {
    $this->middleWareAdapter = $middleWareAdapter;
  }

  /**
   * @param AddWIFISocCommand $command
   * @throws MiddleWareException
   */
  public function handle(AddWIFISocCommand $command)
  {
    $mwResult = $this->middleWareAdapter->mwQuerySubscriber([
      'actionUUID' => $command->actionUUID,
      'iccid'      => $command->iccid,
      'msisdn'     => $command->msisdn
    ]);

    $this->handleMiddleWareResultCheck($mwResult);

    $commandArr = (array)$command;
    // set extra_options field
    if (!empty($commandArr['channel'])) {
      $commandArr['extra_options'] = [ 'channel' => $commandArr['channel'] ];
      unset($commandArr['channel']);
    }

    $result = $this->middleWareAdapter->mwMakeitsoUpgradePlan($commandArr);

    if ($errors = $result->get_errors()) {
      throw new MiddleWareException('ERR_API_INTERNAL: ' . $errors[0]);
    }
  }
}
