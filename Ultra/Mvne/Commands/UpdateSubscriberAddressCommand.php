<?php
namespace Ultra\Mvne\Commands;

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Hydrator;

/**
 * Class UpdateSubscriberAddressCommand
 * @package Ultra\Mvne\Commands
 */
class UpdateSubscriberAddressCommand
{
  /**
   * @var
   */
  public $customer_id;

  /**
   * @var
   */
  public $actionUUID;

  /**
   * @var
   */
  public $msisdn;

  /**
   * @var
   */
  public $iccid;

  /**
   * @var
   */
  public $E911Address = [
    'addressLine1',
    'addressLine2',
    'City',
    'State',
    'Zip',
  ];

  use Hydrator;

  public function __construct(array $input)
  {
    $this->hydrate($input);

    if (empty($this->customer_id) || empty($this->E911Address['addressLine1'])) {
      throw new InvalidObjectCreationException(__CLASS__ . ' Missing required parameters.', 'MP0001');
    }
  }
}
