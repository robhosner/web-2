<?php
namespace Ultra\Mvne\Commands;

use Ultra\Hydrator;
use Ultra\Mvne\UpgradePlanBase;

class AddWIFISocCommand extends UpgradePlanBase
{
  public $option;
  public $channel;

  use Hydrator;

  public function __construct(array $input)
  {
    $input['option'] = 'N-WFC|ADD';
    $this->channel = !empty($input['channel']) ? $input['channel'] : 'WIFI_CALLING.UNKNOWN';

    $this->hyrdateAllItems($input);
  }
}
