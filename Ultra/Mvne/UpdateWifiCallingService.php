<?php
namespace Ultra\Mvne;

use CommandInvocation;
use Ultra\Commander\CommanderTrait;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Address;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\DatabaseErrorException;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\InvalidSimStateException;
use Ultra\Exceptions\MiddleWareException;
use Ultra\Mvne\Commands\AddWIFISocCommand;
use Ultra\Mvne\Commands\RemoveWIFISocCommand;
use Ultra\Mvne\Commands\UpdateSubscriberAddressCommand;
use Ultra\Mvne\MakeItSo\Interfaces\MakeItSoRepositoryInterface;
use Ultra\Sims\Interfaces\SimRepository;
use Ultra\Sims\Sim;
use Ultra\Utilities\Common;

/**
 * Class UpdateWifiCallingService
 * @package Ultra\Mvne
 */
class UpdateWifiCallingService
{
  /**
   * @var Address
   */
  private $address;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var
   */
  private $customer_id;

  /**
   * @var
   */
  private $enable_wifi_calling;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * @var Sim
   */
  private $sim;

  /**
   * @var
   */
  private $requestId;

  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var CommandInvocation
   */

  private $commandInvocation;

  /**
   * @var MakeItSoRepositoryInterface
   */
  private $makeItSoRepository;

  /**
   * @var
   */
  private $channel;

  use CommanderTrait;

  /**
   * UpdateWifiCallingService constructor.
   * @param Address $address
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param SimRepository $simRepository
   * @param Common $utilities
   * @param $customer_id
   * @param $enable_wifi_calling
   * @param $requestId
   * @param CommandInvocation $commandInvocation
   * @param MakeItSoRepositoryInterface $makeItSoRepository
   */
  public function __construct(
    Address $address,
    CustomerRepository $customerRepository,
    Configuration $configuration,
    SimRepository $simRepository,
    Common $utilities,
    $customer_id,
    $enable_wifi_calling,
    $requestId,
    CommandInvocation $commandInvocation,
    MakeItSoRepositoryInterface $makeItSoRepository,
    $channel=null
  )
  {
    $this->address = $address;
    $this->customerRepository = $customerRepository;
    $this->configuration = $configuration;
    $this->simRepository = $simRepository;
    $this->utilities = $utilities;
    $this->customer_id = $customer_id;
    $this->enable_wifi_calling = $enable_wifi_calling;
    $this->requestId = $requestId;
    $this->commandInvocation = $commandInvocation;
    $this->makeItSoRepository = $makeItSoRepository;
    $this->channel = $channel;

    $this->validate();
  }

  /**
   * @throws InvalidSimStateException
   */
  public function execute()
  {
    $this->sim = $this->simRepository->getSimInventoryAndBatchInfoByIccid($this->customer->current_iccid_full);

    if (!$this->sim->allowsWifiSoc()) {
      throw new InvalidSimStateException('ERR_API_INVALID_ARGUMENTS: this SIM does not allow adding the Wifi SOC', 'VV0016');
    }

    if ($this->commandInvocation->isOccupied() || $this->makeItSoRepository->getPendingMakeItSoByCustomerId($this->customer->customer_id)) {
      throw new MiddleWareException('The customer has a pending Command Invocation or MISO', 'CI0005');
    }

    $wifiSocData = [
      'input' => [
        'actionUUID'         => $this->requestId,
        'msisdn'             => $this->customer->current_mobile_number,
        'iccid'              => $this->customer->current_iccid_full,
        'customer_id'        => $this->customer->customer_id,
        'wholesale_plan'     => $this->customer->getWholesalePlan(),
        'ultra_plan'         => $this->configuration->getPlanFromCosId($this->customer->cos_id),
        'preferred_language' => $this->customer->preferred_language,
        'channel'            => $this->channel
      ]
    ];

    if ($this->enable_wifi_calling && $this->address->address1) {
      $this->executeCommand(UpdateSubscriberAddressCommand::class, [
        'input' => [
          'customer_id' => $this->customer->customer_id,
          'actionUUID'  => $this->requestId,
          'msisdn'      => $this->customer->current_mobile_number,
          'iccid'       => $this->customer->current_iccid_full,
          'E911Address' => [
            'addressLine1' => (!empty($this->address->address1)) ? $this->address->address1 : '',
            'addressLine2' => (!empty($this->address->address2)) ? $this->address->address2 : '',
            'City'         => (!empty($this->address->city)) ? $this->address->city : '',
            'State'        => (!empty($this->address->state_region)) ? $this->address->state_region : '',
            'Zip'          => (!empty($this->address->postal_code)) ? $this->address->postal_code : ''
          ]
        ]
      ]);

      \sendSMSE911AddressUpdated($this->customer->customer_id);

      $this->executeCommand(AddWIFISocCommand::class, $wifiSocData);
    } else {
      $this->executeCommand(RemoveWIFISocCommand::class, $wifiSocData);

      $this->utilities->teldataChangeDb();
      $result = $this->customer->removeAccAddressE911();

      if ($result->is_failure()) {
        throw new DatabaseErrorException('Failed to remove customer E911 address.', 'DB0001');
      }
    }
  }

  /**
   * @return bool
   * @throws InvalidObjectCreationException
   */
  private function validate()
  {
    $this->customer = $this->customerRepository->getCombinedCustomerByCustomerId(
      $this->customer_id,
      [
        'u.CUSTOMER_ID',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'preferred_language',
        'plan_state',
        'u.BRAND_ID',
        'a.COS_ID'
      ]
    );

    if (!$this->customer) {
      throw new InvalidObjectCreationException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
    }

    if ($this->customer->plan_state != STATE_ACTIVE) {
      throw new InvalidObjectCreationException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');
    }

    $this->commandInvocation->customer_id = $this->customer_id;

    return true;
  }
}
