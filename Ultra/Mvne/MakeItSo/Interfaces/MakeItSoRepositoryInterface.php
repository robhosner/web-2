<?php
namespace Ultra\Mvne\MakeItSo\Interfaces;

interface MakeItSoRepositoryInterface
{
  /**
   * @param $customerId
   * @return \object[]
   */
  public function getPendingMakeItSoByCustomerId($customerId);
}