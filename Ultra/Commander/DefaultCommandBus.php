<?php
namespace Ultra\Commander;

use InvalidArgumentException;
use Ultra\Commander\Exceptions\HandlerClassMissingHandleMethodException;
use Ultra\Commander\Exceptions\HandlerNotRegisteredException;
use Ultra\Commander\Interfaces\CommandBus;
use Ultra\Commander\Interfaces\CommandTranslator;
use Ultra\Container\AppContainer;

/**
 * Class DefaultCommandBus
 * @package Ultra\Commander
 */
class DefaultCommandBus implements CommandBus, CommandTranslator
{
  /**
   * List of optional decorators for command bus.
   *
   * @var array
   */
  protected $decorators = [];

  /**
   * @var AppContainer
   */
  private $container;

  /**
   * DefaultCommandBus constructor.
   * @param AppContainer $container
   */
  public function __construct(AppContainer $container)
  {
    $this->container = $container;
  }

  /**
   * Decorate the command bus with any executable actions.
   *
   * @param  string $className
   * @return mixed
   */
  public function decorate($className)
  {
    $this->decorators[] = $className;

    return $this;
  }

  /**
   * Execute the command
   *
   * @param $command
   * @return mixed
   */
  public function execute($command)
  {
    $this->executeDecorators($command);

    $handler = $this->toCommandHandler($command);

    return $this->container->make($handler)->handle($command);
  }

  /**
   * Execute all registered decorators
   *
   * @param  object $command
   */
  protected function executeDecorators($command)
  {
    foreach ($this->decorators as $className) {
      $instance = $this->container->make($className);

      if (!$instance instanceof CommandBus) {
        $message = 'The class to decorate must be an implementation of ' . CommandBus::class;

        throw new InvalidArgumentException($message);
      }

      $instance->execute($command);
    }
  }

  /**
   * Translate a command to its handler counterpart
   *
   * @param $command
   * @return mixed
   * @throws HandlerClassMissingHandleMethodException
   * @throws HandlerNotRegisteredException
   */
  public function toCommandHandler($command)
  {
    $commandClass = get_class($command);
    $handler      = substr_replace($commandClass, 'CommandHandler', strrpos($commandClass, 'Command'));

    if (!class_exists($handler)) {
      throw new HandlerNotRegisteredException("Command handler [$handler] does not exist.");
    }

    if (!is_callable([$handler, 'handle'])) {
      throw new HandlerClassMissingHandleMethodException("The Command handler [$handler] does not contain the handle method.");
    }

    return $handler;
  }
}
