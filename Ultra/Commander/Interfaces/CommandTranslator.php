<?php
namespace Ultra\Commander\Interfaces;

use Exception;

interface CommandTranslator
{
  /**
   * Translate a command to its handler counterpart.
   *
   * @param $command
   * @return mixed
   * @throws Exception
   */
  public function toCommandHandler($command);
}
