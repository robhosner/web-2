<?php
namespace Ultra\Customers\Interfaces;

use Ultra\Customers\Customer;

/**
 * Interface CustomerRepository
 * @package Ultra\Customers\Interfaces
 */
interface CustomerRepository
{
  /**
   * Get customer by customer id.
   *
   * @param $customerId
   * @param array $selectFields
   * @param bool $transform
   * @return object
   */
  public function getCustomerById($customerId, array $selectFields, $transform = false);

  /**
   * Get customer from msisdn
   *
   * @param $msisdn
   * @param array $selectFields
   * @param bool $transform
   * @return object
   */
  public function getCustomerFromMsisdn($msisdn, array $selectFields = [], $transform = false);

  /**
   * Get a list of customers from msisdns
   *
   * @param array $msisdns
   * @param array $selectFields
   * @return \Array|bool
   */
  public function getCustomersFromMsisdns(array $msisdns, array $selectFields);

  /**
   * Get customer from iccid
   *
   * @param $iccid
   * @param array $selectFields
   * @return object
   */
  public function getCustomerFromIccid($iccid, array $selectFields);

  /**
   * Get a list of customers from iccids
   *
   * @param array $iccids
   * @param array $selectFields
   * @return \Array|bool
   */
  public function getCustomersFromIccids(array $iccids, array $selectFields);

  /**
   * Update customers table with update fields by customer id
   *
   * @param  int   $customer_id
   * @param  array $fieldsToUpdate key/value
   * @return bool  was sql successful?
   */
  public function updateCustomerByCustomerId($customer_id, array $fieldsToUpdate);

  /**
   * Cancel customer
   *
   * @param $customer
   * @param $customerId
   * @return array
   */
  public function cancelCustomer($customer, $customerId);

  /**
   * Add cancellation reason
   * 
   * @param array $fields
   * @return object
   */
  public function addCancellationReason(array $fields);

  /**
   * Ensure to cancel customer.
   * 
   * @param $msisdn
   * @param $iccid
   * @param null $transitionUuid
   * @param null $actionUuid
   * @param null $customerId
   * @return object
   */
  public function ensureCancelCustomer($msisdn, $iccid, $transitionUuid = null, $actionUuid = null, $customerId = null);

  /**
   * Update auto-recharge
   *
   * @param $customerId
   * @param $enrollAutoRecharge
   */
  public function updateAutoRecharge($customerId, $enrollAutoRecharge);

  /**
   * @param $customerId
   * @param array $selectFields
   * @return Customer
   */
  public function getCombinedCustomerByCustomerId($customerId, array $selectFields = []);

  /**
   * @param $customerId
   * @param array $selectFields
   * @return Customer
   */
  public function getCustomerFromCustomersTable($customerId, array $selectFields = []);

  /**
   * @param $zsession
   * @param string $method
   * @return Customer
   */
  public function getCustomerFromSession($zsession, $method = 'get_customer_from_customer');

  /**
   * @param $username
   * @return Customer
   */
  public function getCustomerFromUsername($username);
}