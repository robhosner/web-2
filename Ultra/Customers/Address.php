<?php
namespace Ultra\Customers;

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Hydrator;

/**
 * Class Address
 * @package Ultra\Customers
 */
class Address
{
  /**
   * @var
   */
  public $address1;

  /**
   * @var
   */
  public $address2;

  /**
   * @var
   */
  public $city;

  /**
   * @var
   */
  public $state_region;

  /**
   * @var
   */
  public $postal_code;

  /**
   * @var
   */
  public $country;

  use Hydrator;

  /**
   * Address constructor.
   * @param array $input
   * @throws InvalidObjectCreationException
   */
  public function __construct(array $input)
  {
    $this->hydrate($input);

    if (empty($this->address1)) {
      throw new InvalidObjectCreationException(__CLASS__ . ' Missing required parameters.', 'MP0001');
    }
  }
}
