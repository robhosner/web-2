<?php
namespace Ultra\Customers;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Messaging\Messenger;

class SendFamilyReplenishmentSms
{
  /**
   * @var FamilyAPI
   */
  private $familyAPI;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * SendFamilyReplenishmentSms constructor.
   * @param FamilyAPI $familyAPI
   * @param CustomerRepository $customerRepository
   * @param Messenger $messenger
   */
  public function __construct(FamilyAPI $familyAPI, CustomerRepository $customerRepository, Messenger $messenger)
  {
    $this->familyAPI = $familyAPI;
    $this->customerRepository = $customerRepository;
    $this->messenger = $messenger;
  }

  public function sendSms($customerId)
  {
    $familyResult = $this->familyAPI->getFamilyByCustomerID($customerId);

    if ($familyResult->is_failure()) {
      return false;
    }

    $parentId = $familyResult->data_array['parentCustomerId'];
    $parent = $this->customerRepository->getCustomerById($parentId, ['monthly_cc_renewal']);
    $memberIds = [];

    foreach ($familyResult->data_array['members'] as $member) {
      $memberIds[] = $member['customerId'];
    }

    if (!$parent) {
      return false;
    }

    if ($parent->monthly_cc_renewal && $familyResult->data_array['payForFamily']) {
      $this->messenger->enqueueImmediateSms($parentId, 'flex_pay_for_family_auto_recharge_on', []);
      return true;
    } elseif (!$parent->monthly_cc_renewal && $familyResult->data_array['payForFamily']) {
      $this->messenger->enqueueImmediateSms($parentId, 'flex_parent_auto_recharge_off_pay_for_family_on', []);
      return true;
    } elseif (!$parent->monthly_cc_renewal && !$familyResult->data_array['payForFamily']) {
      $this->messenger->enqueueImmediateSms($parentId, 'flex_parent_auto_recharge_off_pay_for_family_off', []);
      return true;
    } elseif ($customerId != $parentId && in_array($customerId, $memberIds)) {
      $this->messenger->enqueueImmediateSms($customerId, 'flex_child_auto_recharge_off', []);
      return true;
    }

    return false;
  }
}
