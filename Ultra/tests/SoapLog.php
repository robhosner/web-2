<?php

include_once 'db.php';
include_once 'db/ultra_acc/soap_log.php';

/*
TODO: currently $actionUUID is only stored in the XML

function displaySoapRequestsByActionUUID( $actionUUID )
{
  \Ultra\Lib\DB\ultra_acc_connect();

  $data = get_soap_log(
    array(
    )
  );

  print_r($data);
}
*/

function displaySoapRequests( $params )
{
  \Ultra\Lib\DB\ultra_acc_connect();

  $data = get_soap_log( $params );

  if ( $data && is_array( $data ) && count( $data ) )
  {
    echo "\n";

    foreach( $data as $row )
      echo $row->COMMAND."  type = ".$row->TYPE_ID."\n".$row->xml."\n\n";
  }
  else
  {
    echo "No SOAP_LOG data found\n";
  }
}

?>
