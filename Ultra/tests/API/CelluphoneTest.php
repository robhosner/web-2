<?php

require_once 'PHPUnit/Autoload.php';
require_once((getenv('HTT_CONFIGROOT') . '/e.php'));
require_once('web.php');
require_once('test/api/test_api_include.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

/**
 * Tests for 'celluphone' partner APIs
 */
class CelluphoneTest extends PHPUnit_Framework_TestCase
{
  // a valid orange SIM ICCID
  const TEST_ORANGE_ICCID_19 = '8901260842116671844'; # SELECT TOP 1 ICCID_FULL FROM HTT_INVENTORY_SIM WHERE PRODUCT_TYPE = 'ORANGE' AND ACT_CODE IS NOT NULL AND CUSTOMER_ID IS NULL;

  // a valid purple SIM ICCID
  const TEST_PURPLE_ICCID_19 = '8901260842102000305'; # SELECT TOP 1 ICCID_FULL FROM HTT_INVENTORY_SIM WHERE PRODUCT_TYPE = 'PURPLE' AND ACT_CODE IS NULL AND CUSTOMER_ID IS NULL AND CREATED_BY != 'Raf testing';


  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';


  /**
   * The setUp() and tearDown() template methods are run once for each test method (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/celluphone/1/ultra/api/provisioning__';

    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /**
   ***************************************
   * Test for provisioning__checkSIMCard *
   ***************************************
   */
  public function test__provisioning__checkSIMCard ()
  {
    $url = $this->base_url . 'checkSIMCard';

    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    // positive test
    ////////////////
    $params = array(
      'ICCID'    => '2000008000008000010'
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue(  ! ! $decoded_response->success );
    $this->assertFalse( ! ! $decoded_response->sim_ready_activate );
    $this->assertEquals(   $decoded_response->valid_ext , 'VALID' );
    $this->assertEquals(   $decoded_response->valid     , 'VALID' );
  }


  /**
   *****************************************************************
   * Test for provisioning__requestActivateShippedNewCustomerAsync *
   *****************************************************************
   */
  public function test__provisioning__requestActivateShippedNewCustomerAsync ()
  {
    $url = $this->base_url . 'requestActivateShippedNewCustomerAsync';

    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    $defaultParamsRequestActivateShippedNewCustomerAsync = array(
      "zsession" => "fake_zsession",
      'ICCID'    => self::TEST_PURPLE_ICCID_19
    );

    $requiredParams = array(
      "zsession",
      "ICCID"
    );

    $defaultTester->runMissingParameterTests(
      $defaultParamsRequestActivateShippedNewCustomerAsync,
      $requiredParams
    );


    // test non-purple ICCID
    ////////////////////////
    $params = array_merge(
      array(),
      $defaultParamsRequestActivateShippedNewCustomerAsync
    );

    $params['ICCID'] = self::TEST_ORANGE_ICCID_19;

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertTrue( in_array( "ERR_API_PRODUCT: This SIM cannot be activated by this process." , $decoded_response->errors ) );
  }

}

?>
