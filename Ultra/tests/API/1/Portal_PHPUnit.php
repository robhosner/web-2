<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');
include_once('test/api/test_api_include.php');

/**
 * Tests for 'portal' partner APIs v1
 */
class PortalTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/portal/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    teldata_change_db();

    opt_out_voice_preference(3627);
    $params = array(
        'customer_id' => 3627,
        'marketing_email_option' => 0,
        'marketing_sms_option' => 0
    );
    set_marketing_settings($params);
  }


  /*
   * test_portal__ClearCreditCard
   */
  public function test_portal__ClearCreditCard()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // successful test
    $params['zsession'] = createCustomerSession('Rickey', 'Jugal12345');
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->assertTrue($response->success);
  }

  /*
   * test_portal__CustomerDetail
   */
  public function test_portal__CustomerDetail()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    $zsession = createCustomerSession('jsdfgjsd1', 'jsdfgjsd1');

    // successful test
    $params = [
      'zsession' => $zsession,
      'mode'     => 'DATA_USAGE'
    ];
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->assertTrue($response->success);
  }

  /**
   * MVNO-2449
   */
  public function test_MVNO_2449()
  {
    // create customer session
    $zsession = createCustomerSession('elway10', 'ultra123'); // 3363
    echo "zsession: $zsession \n";
    $this->assertNotEmpty($zsession);

    // validate session
    $url = $this->base_url . 'portal__CustomerInfo';
    $this->curl_options[CURLOPT_COOKIE] = "zsession=$zsession"; 
    $result = curl_post($url, array('zsession' => $zsession), $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->assertTrue($response->success);

    // kill session
    $url = $this->base_url . 'portal__KillSession';
    $this->curl_options[CURLOPT_COOKIE] = "zsession=$zsession"; 
    $result = curl_post($url, array('zsession' => $zsession), $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->assertTrue($response->success);

    sleep(1);

    // test API
    $url = $this->base_url . 'portal__CustomerInfo';
    $this->curl_options[CURLOPT_COOKIE] = "zsession=$zsession"; 
    $result = curl_post($url, array('zsession' => $zsession), $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->assertTrue( ! $response->success);
 }

  /**
   * test_portal__SetMarketingSettings
   */
  public function test_portal__SetMarketingSettings()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // successful test
    $params = array(
      'zsession' => make_zsession_from_customer_id(3627),
      'marketing_settings' => array(
        'marketing_email_option', 0,
        'marketing_sms_option', 1,
        'marketing_voice_option', 1
    ));

    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    print_r($response);
    $this->assertTrue($response->success);

    $url = $this->base_url . 'portal__GetMarketingSettings';

    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    print_r($response);
    $this->assertTrue($response->success);
    $this->assertEquals(0, $response->marketing_settings[1]);
    $this->assertEquals(1, $response->marketing_settings[3]);
    $this->assertEquals(1, $response->marketing_settings[5]);
  }

  /*
   * test_portal__UpdateAutoRecharge
   */
  public function test_portal__UpdateAutoRecharge()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // successful test
    $params = array('zsession' => createCustomerSession('elway10', 'ultra123'), 'auto_recharge' => 0);
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    print_r($response);
    $this->assertTrue($response->success);
  }
}

