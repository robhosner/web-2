<?php

require_once 'classes/PHPUnitBase.php';

class ApplyDataRechargeTest extends PHPUnitBase
{
  function setUp()
  {
    parent::setUp();

    teldata_change_db();

    // prepare test for username update
    run_sql_and_check( "UPDATE ACCOUNTS SET BALANCE = 100 WHERE CUSTOMER_ID = 21133" );

    $this->partner = 'customercare';
    $this->api     = 'ApplyDataRecharge';
  }

  public function testMissingParams()
  {
    $this->setOptions([
      'api'       => $this->partner . '__' . $this->api,
      'bath'      => 'rest',
      'version'   => 1,
      'partner'   => $this->partner
    ]);


    // missing msisdn
    $params = [
      'data_soc_id' => 'PayGo_500_500'
    ];
    $result = $this->callApi($params);

    $this->assertFalse( ! ! $result->success );
    $this->assertNotEmpty( $result->errors );


    // missing data_soc_id
    $params = [
      'msisdn' => '1001001000'
    ];
    $result = $this->callApi($params);

    $this->assertFalse( ! ! $result->success );
    $this->assertNotEmpty( $result->errors );


    // non-exisitng msisdn
    $params = [
      'data_soc_id' => 'PayGo_500_500',
      'msisdn'      => '1000000001'
    ];
    $result = $this->callApi($params);

    $this->assertFalse( ! ! $result->success );
    $this->assertNotEmpty( $result->errors );


    // invalid data soc
    $params = [
      'data_soc_id' => 'TEST_INVALID',
      'msisdn'      => '7203979544'
    ];
    $result = $this->callApi($params);

    $this->assertFalse( ! ! $result->success );
    $this->assertNotEmpty( $result->errors );


    // success
    $params = [
      'data_soc_id' => 'PayGo_500_500',
      'msisdn'      => '7203979544'
    ];
    $result = $this->callApi($params);

    $this->assertTrue( ! ! $result->success );
    $this->assertEmpty( $result->errors );
  }

}

