<?php

require_once 'web.php';
require_once 'Ultra/tests/API/TestConstants.php';
require_once 'Ultra/tests/API/DefaultUltraAPITest.php';
require_once 'test/api/test_api_include.php';

/**
 * Tests for 'inventory' partner APIs v1
 */
class InventoryTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/inventory/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    // teldata_change_db();
  }

  /**
   * test_inventory__UpdateSIMInventory
   */
  public function test_inventory__UpdateSIMInventory()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // failure due to missing parameters
    $params = array();
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // failure: startICCID does not exist
    $params = array(
      'startICCID'         => '765432198765432198',
      'endICCID'           => '765432198765432199',
      'request_epoch'      => time(),
      'startICCIDchecksum' => 1,
      'updated_by'         => 'TEST TEST',
      'action'             => 'ARRIVED_AGENT'
    );
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // failure: endICCID does not exist
    $params = array(
      'startICCID'         => '890126084211665507',
      'endICCID'           => '765432198765432199',
      'request_epoch'      => time(),
      'startICCIDchecksum' => 8,
      'updated_by'         => 'TEST TEST',
      'action'             => 'ARRIVED_AGENT'
    );
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
  }

  /**
   * test_inventory__StockSIMCelluphone
   */
  public function test_inventory__StockSIMCelluphone()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // failure due to missing parameters
    $params = array();
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // failure: startICCID does not exist
    $params = array(
      'startICCID'         => '765432198765432198',
      'endICCID'           => '765432198765432199',
      'request_epoch'      => time(),
      'updated_by'         => 'TEST TEST'
    );
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // failure: endICCID does not exist
    $params = array(
      'startICCID'         => '890126084211665507',
      'endICCID'           => '765432198765432199',
      'request_epoch'      => time(),
      'updated_by'         => 'TEST TEST'
    );
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
  }

  /**
   * test_inventory__ShipSIMMaster
   */
  public function test_inventory__ShipSIMMaster()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // failure due to missing parameters
    $params = array();
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // failure: startICCID does not exist
    $params = array(
      'startICCID'         => '765432198765432198',
      'endICCID'           => '765432198765432199',
      'request_epoch'      => time(),
      'updated_by'         => 'TEST TEST',
      'masteragent'        => '12345678'
    );
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // failure: endICCID does not exist
    $params = array(
      'startICCID'         => '890126084211665507',
      'endICCID'           => '765432198765432199',
      'request_epoch'      => time(),
      'updated_by'         => 'TEST TEST',
      'masteragent'        => '12345678'
    );
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
  }

}

