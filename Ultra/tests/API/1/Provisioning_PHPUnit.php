<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');

/**
 * Tests for 'provisioning' partner APIs v1
 */
class ProvisioningTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  const TEST_VALID_ACT_CODE  = '88817069144';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // all APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url() . '/pr/provisioning/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /**
   * test_provisioning__ProvisionBasicOrangeCustomerAsync
   */
  public function test_provisioning__ProvisionBasicOrangeCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // failure test (missing actcode)
    $params = array(
      'customerZip' => '12345'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // failure test (missing customerZip)
    $params = array(
      'actcode' => 'dummy'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // failure test ( ActCodes are 11 digits, with the 11th digit being a checkdigit - the usual LUHN algorithm check )
    $params = array(
      'customerZip' => '12345',
      'actcode'     => '100000000'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // failure test ( invalid customerZip )
    $params = array(
      'customerZip' => '00001',
      'actcode'     => self::TEST_VALID_ACT_CODE
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);
  }


  /**
   * test_provisioning__ProvisionBasicNewCustomerAsync
   */
  public function test_provisioning__ProvisionBasicNewCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // zip coverage error
    $params = array(
      'ICCID'       => TEST_ICCID_MVNE_2,
      'targetPlan'  => 'L19',
      'customerZip' => '00001'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // invalid iccid
    $params = array(
      'ICCID'       => '9901966665444431234',
      'targetPlan'  => 'L19',
      'customerZip' => '12345'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // already used iccid
    $params = array(
      'ICCID'       => '8901260842107552433',
      'targetPlan'  => 'L19',
      'customerZip' => '12345'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // MVNE tests: we cannot run positive test because API changes DB
    $params = array(
      'ICCID' => TEST_ICCID_MVNE_2,
      'targetPlan' => 'L19',
      'customerZip' => '10001'
    );
    $mvneTester = new MvneTest($url, $this);
    $mvneTester->invalidIccidTest($params);
  }


  /**
   * test_provisioning__requestProvisionPortedCustomerAsync
   */
  public function test_provisioning__requestProvisionPortedCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // zip coverage error
    $params = array(
      'ICCID'                  => '8111008000008000118',
      'loadPayment'            => 'NONE',
      'targetPlan'             => 'L19',
      'numberToPort'           => '2188655554',
      'portAccountNumber'      => '2405',
      'customerZip'            => '00001',
      'activation_masteragent' => '10',
      'activation_agent'       => '11',
      'activation_userid'      => '2405',
      'dealer_code'            => 'CCity'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // invalid iccid
    $params = array(
      'ICCID'                  => '9901966665444431234',
      'loadPayment'            => 'NONE',
      'targetPlan'             => 'L19',
      'numberToPort'           => '2188655554',
      'portAccountNumber'      => '2405',
      'customerZip'            => '12345',
      'activation_masteragent' => '10',
      'activation_agent'       => '11',
      'activation_userid'      => '2405',
      'dealer_code'            => 'CCity'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // already used iccid
    $params = array(
      'ICCID'       => '8901260842107552433',
      'loadPayment'            => 'NONE',
      'targetPlan'             => 'L19',
      'numberToPort'           => '2188655554',
      'portAccountNumber'      => '2405',
      'customerZip'            => '12345',
      'activation_masteragent' => '10',
      'activation_agent'       => '11',
      'activation_userid'      => '2405',
      'dealer_code'            => 'CCity'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // invalid dealer code test
    $params = array(
      'ICCID' => '8901260842107736283',
      'loadPayment' => 'NONE',
      'targetPlan' => 'L19',
      'numberToPort' => '2188655554',
      'portAccountNumber' => '2405',
      'customerZip' => '10001',
      'activation_masteragent' => '10',
      'activation_agent' => '11',
      'activation_userid' => '2405',
      'dealer_code' => 'ZZZ');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: unknown dealer code', $decoded_response->errors);

    // successful test
    $params = array(
      'ICCID' => '1010101010101010010',
      'loadPayment' => 'NONE',
      'targetPlan' => 'L19',
      'numberToPort' => '2188655655',
      'portAccountNumber' => '2405',
      'customerZip' => '10001',
      'activation_masteragent' => '10',
      'activation_agent' => '11',
      'activation_userid' => '2405',
      'activation_store' => '10',
      'dealer_code' => 'CCity');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

     // DOP-43 test insure L24 is not accepted
    $params = array(
      'ICCID' => '8901260842107734361',
      'loadPayment' => 'NONE',
      'targetPlan' => 'L24',
      'numberToPort' => '7144029993',
      'portAccountNumber' => '2405',
      'customerZip' => '10001',
      'activation_masteragent' => '10',
      'activation_agent' => '11',
      'activation_userid' => '2405',
      'dealer_code' => 'CCity');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: targetPlan value has no matches', $decoded_response->errors);
  }


  /**
   * test_provisioning__requestProvisionNewCustomerAsync
   */
  public function test_provisioning__requestProvisionNewCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // zip coverage error
    $params = array(
      'ICCID'                  => '8111008000008000118',
      'loadPayment'            => 'NONE',
      'targetPlan'             => 'L19',
      'customerZip'            => '00001',
      'activation_masteragent' => '111',
      'activation_agent'       => '222',
      'activation_store'       => '333',
      'activation_userid'      => '444',
      'dealer_code'            => 'CCity'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // invalid iccid
    $params = array(
      'ICCID'                  => '9901966665444431234',
      'loadPayment'            => 'NONE',
      'targetPlan'             => 'L19',
      'customerZip'            => '12345',
      'activation_masteragent' => '111',
      'activation_agent'       => '222',
      'activation_store'       => '333',
      'activation_userid'      => '444',
      'dealer_code'            => 'CCity'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // already used iccid
    $params = array(
      'ICCID'       => '8901260842107552433',
      'loadPayment'            => 'NONE',
      'targetPlan'             => 'L19',
      'customerZip'            => '12345',
      'activation_masteragent' => '111',
      'activation_agent'       => '222',
      'activation_store'       => '333',
      'activation_userid'      => '444',
      'dealer_code'            => 'CCity'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // invalid dealer code test
    $params = array(
      'ICCID' => '8901260842107739303',
      'loadPayment' => 'NONE',
      'targetPlan' => 'L19',
      'customerZip' => '10001',
      'activation_masteragent' => '111',
      'activation_agent' => '222',
      'activation_store' => '333',
      'activation_userid' => '444',
      'dealer_code' => 'ZZZ');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: unknown dealer code', $decoded_response->errors);

    // successful test
    $params = array(
      'ICCID' => '1010101010101010366',
      'loadPayment' => 'NONE',
      'targetPlan' => 'L19',
      'customerZip' => '10001',
      'activation_masteragent' => '111',
      'activation_agent' => '222',
      'activation_store' => '333',
      'activation_userid' => '444',
      'dealer_code' => 'CCity');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
  }
}

