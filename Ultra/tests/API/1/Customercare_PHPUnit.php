<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');

/**
 * Tests for 'customercare' partner APIs v1
 */
class CustomercareTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/customercare/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }

  /**
   * customercare__ChangePhoneNumber
   */
  public function test_customercare__ChangePhoneNumber()
  {
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test failure ( missing reason )
    $params = array(
      'agent_name'            => 'James Bond',
      'customercare_event_id' => time(),
      'customer_id'           => 1,
      'old_phone_number'      => '1001001000'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( missing agent_name )
    $params = array(
      'reason'                => 'test',
      'customercare_event_id' => time(),
      'customer_id'           => 1,
      'old_phone_number'      => '1001001000'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( missing customercare_event_id )
    $params = array(
      'reason'                => 'test',
      'agent_name'            => 'James Bond',
      'customer_id'           => 1,
      'old_phone_number'      => '1001001000'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( missing customer_id )
    $params = array(
      'reason'                => 'test',
      'agent_name'            => 'James Bond',
      'customercare_event_id' => time(),
      'old_phone_number'      => '1001001000'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( missing old_phone_number )
    $params = array(
      'reason'                => 'test',
      'agent_name'            => 'James Bond',
      'customercare_event_id' => time(),
      'customer_id'           => 1
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( missing zipcode )
    $params = array(
      'reason'                => 'test',
      'agent_name'            => 'James Bond',
      'customercare_event_id' => time(),
      'customer_id'           => 1,
      'old_phone_number'      => '1001001000'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( customer not found )
    $params = array(
      'reason'                => 'test',
      'agent_name'            => 'James Bond',
      'customercare_event_id' => time(),
      'customer_id'           => 1,
      'old_phone_number'      => '1001001000',
      'zipcode'               => '12345'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    // test failure ( msisdn / customer id mismatch )
    $params = array(
      'reason'                => 'test',
      'agent_name'            => 'James Bond',
      'customercare_event_id' => time(),
      'customer_id'           => 18684,
      'old_phone_number'      => '9496104720',
      'zipcode'               => '12345'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
  }

  /**
   * customercare__AddCourtesyCash
   */
  public function test_customercare__AddCourtesyCash()
  {
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test add courtesy cash on provisioned account
    $params = array(
      'reason'                => 'testing',
      'agent_name'            => '77777',
      'customercare_event_id' => '88888',
      'customer_id' => 18658,
      'amount' => 500
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    $this->assertTrue($decoded_response->success);

    // test add courtesy cash on suspended account
    $params = array(
      'reason'                => 'testing',
      'agent_name'            => '77777',
      'customercare_event_id' => '88888',
      'customer_id' => 3379,
      'amount' => 500
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    $this->assertTrue($decoded_response->success);
  }

  /**
   * customercare__ChargePinCard
   */
  public function test_customercare__ChargePinCard()
  {
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test OK
    $params = array(
      'msisdn' => 3474255162,
      'pin' => 153617423604905390,
      'pin2' => 178446202269035373,
      'pin3' => 125552666347162529,
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);

    // test OK
    $params = array(
      'msisdn' => 3474255162,
      'pin' => 153617423604905111
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);
  }

  /**
   * customercare__ChargeCreditCard
   */
  public function test_customercare__ChargeCreditCard()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test OK
    $params = array(
      'customer_id'  => 32,
      'amount'       => 1234,
      'reason'       => __FUNCTION__
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! ! $decoded_response->success );

  }

  /**
   * customercare__SetCustomerCreditCard
   */
  public function test_customercare__SetCustomerCreditCard()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test customer_id
    $params = array(
      'customer_id'       => 'x',
      'account_cc_exp'    => '1115',
      'account_cc_cvv'    => '123',
      'account_cc_number' => '1234123412341234',
      'account_zipcode'   => '12345'
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains( 'ERR_API_INVALID_ARGUMENTS: customer_id is not a integer' , $decoded_response->errors );

    // test account_zipcode
    $params = array(
      'customer_id'       => 123,
      'account_cc_exp'    => '1115',
      'account_cc_cvv'    => '123',
      'account_cc_number' => '1234123412341234',
      'account_zipcode'   => ''
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains( 'ERR_API_INVALID_ARGUMENTS: missing required account_zipcode' , $decoded_response->errors );

    // test account_cc_exp
    $params = array(
      'customer_id'       => 123,
      'account_cc_exp'    => 'x',
      'account_cc_cvv'    => '123',
      'account_zipcode'   => '12345',
      'account_cc_number' => '1234123412341234'
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains( 'ERR_API_INVALID_ARGUMENTS: account_cc_exp value is shorter than min_strlen' , $decoded_response->errors );

    // test account_cc_cvv
    $params = array(
      'customer_id'       => 123,
      'account_cc_exp'    => '1115',
      'account_cc_cvv'    => 'x',
      'account_zipcode'   => '12345',
      'account_cc_number' => '1234123412341234'
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains( 'ERR_API_INVALID_ARGUMENTS: account_cc_cvv value is shorter than min_strlen' , $decoded_response->errors );

    // test account_cc_number
    $params = array(
      'customer_id'       => 123,
      'account_cc_exp'    => '0815',
      'account_cc_cvv'    => '234',
      'account_zipcode'   => '12345',
      'account_cc_number' => 'zxxxzxxxzxxxzxxx'
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains( 'ERR_API_INVALID_ARGUMENTS: invalid credit card number' , $decoded_response->errors );

    // test Credit Card Gateway Validation error
    $params['account_cc_number'] = '349999999999991';

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains( 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed' , $decoded_response->errors );

  }

  /**
   * test_customercare__ReplaceSIMCard
   */
  public function test_customercare__ReplaceSIMCard()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();
  }


   /**
   ***************************************************************
   * Test for customercare__CheckInternationalDirectDialSettings *
   ***************************************************************
   */
  public function test_customercare__CheckInternationalDirectDialSettings ()
  {
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();
  }


  /**
   * customercare__CalculateTaxesFees
   */
  public function test__customercare__CalculateTaxesFees()
  {
    // API setup
    $api = substr(__FUNCTION__, strlen('test__'));
    $url = $this->base_url . $api;

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test with default customer zip
    $params = array(
      'msisdn'        => '9499031488',
      'charge_amount' => 500,
      'check_taxes'   => TRUE,
      'check_recovery_fee' => TRUE);
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! ! $decoded_response->success );

    // test with different zip
    $params = array(
      'msisdn'        => '9499031488',
      'charge_amount' => 500,
      'check_taxes'   => TRUE,
      'check_recovery_fee' => TRUE,
      'zip'           => '11211');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertNotEmpty($decoded_response->sales_tax);
    $this->assertTrue( ! ! $decoded_response->success );

  }

  public function test__customercare__mvneQueryPortIn()
  {
    // API setup
    $api = substr(__FUNCTION__, strlen('test__'));
    $url = $this->base_url . $api;

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests(); 

    //test with valid customer_id and complete status
    $params = array(
      'customer_id' => 18746
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! ! $decoded_response->success );

    //test with inalid customer_id
    $params = array(
      'customer_id' => 9999999
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse( ! ! $decoded_response->success );

    //test customer_id that isnt present in portin queue
    $params = array(
      'customer_id' => 15523
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse( ! ! $decoded_response->success );


    //test customer_id in portin queue that has errors
    $params = array(
      'customer_id' => 18932
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( $decoded_response->success );

  }

}

?>
