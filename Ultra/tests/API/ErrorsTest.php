<?php

require_once 'PHPUnit/Autoload.php';
require_once((getenv('HTT_CONFIGROOT') . '/e.php'));
require_once('web.php');
require_once('test/api/test_api_include.php');

/**
 * Tests for generic API errors
 */
class ErrorsTest extends PHPUnit_Framework_TestCase
{
  /**
   * The setUp() and tearDown() template methods are run once for each test method (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/ultra_api.php';

    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';

    // options for our curl commands
    $this->curl_options = array(
        CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }

  /**
   *************************
   * Test for generic errors
   *************************************
   */
  public function test__errors ()
  {
    $url = $this->base_url;

    $params = array();


    // test no partner
    //////////////////
    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue( $decoded_response->success == "false" );


    // test no version
    //////////////////
    $url .= "?partner=dealerportal";

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue( $decoded_response->success == "false" );


    // test no command
    //////////////////
    $url .= "&version=2";

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue( $decoded_response->success == "false" );


    // test bad command
    ///////////////////
    $url .= "&command=bad";

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue( ! $decoded_response->success );
  }
}

?>
