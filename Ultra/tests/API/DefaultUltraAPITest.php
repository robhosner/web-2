<?php

/**
 * DefaultUltraAPITest tests default ULTRA API features ( http://wiki.hometowntelecom.com:8090/display/SPEC/API+Standard+Guide )
 */
class DefaultUltraAPITest
{
  private $url         = '';
  private $tester      = '';
  private $partner_tag = '';

  public function __construct( $url , $tester )
  {
    $this->url         = $url;
    $this->tester      = $tester;
    $this->partner_tag = __CLASS__ . time();
  }


  public function runMissingParameterTests( $defaultParams , $requiredParams )
  {
    foreach ( $requiredParams as $requiredParam )
    {
      $params = array_merge(
        array(),
        $defaultParams
      );

      unset($params[ $requiredParam ]);

      $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);

      echo "\n$json_result\n";

      $decoded_response = json_decode($json_result);

      $this->tester->assertNotEmpty( $json_result );
      $this->tester->assertNotEmpty( $decoded_response );
      $this->tester->assertTrue( ! $decoded_response->success );
      $this->tester->assertRegExp( '/API_ERR_PARAMETER/', $decoded_response->errors[0] );
    }
  }


  /**
   * Tests default ULTRA API features
   */
  public function runDefaultTests()
  {
    // test the always=fail case
    ////////////////////////////
    $params = array(
      'always' => 'fail',
    );

    $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);
    echo "\nURL: {$this->url}";
    echo "\nJSON: $json_result: \n";

    $decoded_response = json_decode($json_result);

    $this->tester->assertNotEmpty( $json_result );
    $this->tester->assertNotEmpty( $decoded_response );
    $this->tester->assertTrue( ( ! $decoded_response->success ) || ( $decoded_response->success == "false" ) );
    $this->tester->assertEquals( $decoded_response->errors[0] , 'ERR_API_INTERNAL: always fail' );


    // test the always=succeed case
    ///////////////////////////////
    $params = array(
      'always'      => 'succeed',
      'partner_tag' => $this->partner_tag
    );

    $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);

    echo "\nURL: {$this->url}";
    echo "\nJSON: $json_result\n";

    $decoded_response = json_decode($json_result);

    $this->tester->assertNotEmpty( $json_result );
    $this->tester->assertNotEmpty( $decoded_response );
    $this->tester->assertTrue( $decoded_response->success );
    $this->tester->assertEquals( $decoded_response->warnings[0] , 'ERR_API_INTERNAL: always_succeed' );
    $this->tester->assertEquals( $decoded_response->partner_tag , $this->partner_tag );
  }

  public function runAdditionalDefaultTests()
  {
    $this->testWrongApiVersion();
    $this->testWrongFormat();
    $this->testMissingPartner();
    $this->testNonExistentPartner();
  }

  protected function testWrongApiVersion()
  {
    $params = array(
      'version' => 777
    );

    $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);
    $decoded_response = json_decode($json_result);
    $this->tester->assertTrue( ( ! $decoded_response->success ) || ( $decoded_response->success == "false" ) );
  }

  protected function testWrongFormat()
  {
    $params = array(
      'format' => 'INVALID'
    );

    $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);
    $decoded_response = json_decode($json_result);
    $this->tester->assertTrue( ( ! $decoded_response->success ) || ( $decoded_response->success == "false" ) );
  }

  protected function testMissingPartner()
  {
    $params = array(
      'command' => 'FUNCTION'
    );

    $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);
    $decoded_response = json_decode($json_result);
    $this->tester->assertTrue( ( ! $decoded_response->success ) || ( $decoded_response->success == "false" ) );
  }

  protected function testNonExistentPartner()
  {
    $params = array(
      'command' => 'INVALID__FUNCTION'
    );

    $json_result = curl_post($this->url,$params,$this->tester->curl_options,NULL,240);
    $decoded_response = json_decode($json_result);
    $this->tester->assertTrue( ( ! $decoded_response->success ) || ( $decoded_response->success == "false" ) );
  }
}

?>
