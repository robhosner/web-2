<?php

/*
 * TransitionActionTest.php
 *
 * phpunit test suite for testing individual transition actions on state machine
 * for the purpose of new billing logic:
 * http://wiki.hometowntelecom.com:8090/display/SPEC/API+Billing+Cycle
 *
 * note 1: state machine uses GETUTCDATE() for action 'active->plan_started=X' to set plan_started;
 * we have to replace this parameter with our own values to be able to run this tests at any time
 *
 * @author: VYT, 14-01-05
 *
 */

require_once('db.php');

/*
 * TransitionActionTest
 *
 * class to test individual transitions with corresponding actions
 *
 */
class TransitionActionTest extends PHPUnit_Framework_TestCase
{
  // these times are PST (however DB stores them in UTC, so conversion is required)
  const START_MIDNIGHT = 'Mar 17 2014 12:00:00AM';
  const START_NIGHT    = 'Mar 17 2014 12:01:00AM';
  const START_MORNING  = 'Mar 17 2014 01:59:00PM'; // before cutoff time
  const START_CUTOFF   = 'Mar 17 2014 02:00:00PM';
  const START_EVENING  = 'Mar 17 2014 02:01:00PM'; // after cutoff time
  const ONE_DAY        = 86400; // seconds in a day


  protected function setUp()
  {
    teldata_change_db();
  }


  protected function tearDown()
  {
    parent::tearDown();
  }

  
  /*
   * createTransition
   *
   * create and queue a single transition and its actions
   * @param int customerID
   * @param array of arrays: actions with parameters
   * @return string transition_uuid
   *
   */
  private function createTransition($customerID, $actions)
  {
    // validate customer
    $customer = get_customer_from_customer_id($customerID);
    $this->assertInstanceOf('stdClass', $customer);
    $this->assertGreaterThan(1, $customer->cos_id);

    // prepare transition
    $label = __METHOD__;
    $priority = 1;
    $transition_uuid = getNewTransitionUUID('test ' . time());
    echo "\ntransition $transition_uuid\n"; 
    $this->assertStringStartsWith('{TX-', $transition_uuid); // check we have a valid transition_uuid
    $context = array('customer_id' => $customerID);

    // create transition and check results: for the purpose of a single action we do not care about cos_id and plan_state
    $result = log_transition($transition_uuid,
                            $context,
                            $customer->cos_id,
                            $customer->plan_state,
                            $customer->cos_id,
                            $customer->plan_state,
                            $label,
                            $priority);
    $this->assertTrue($result, 'call to log_transition failed');

    // create actions for this transition and check results
    $seq = 1;
    foreach($actions as $action)
    {
      // prepare action
      $action_uuid = getNewActionUUID('test ' . time());
      $this->assertStringStartsWith('{AX-', $action_uuid);
      echo "action $action_uuid: {$action['sql']}\n";
      $params = array(
        'seq'  => $seq++,
        'type' => 'sql',
        'name' => $action['sql']);

      // create action
      $result = log_action($transition_uuid, $action_uuid, $params, $action['fparams'], 'OPEN');
      $this->assertTrue($result, 'call to log_action failed');
    }

    // update redis transition info
    $error = reset_redis_transition( $transition_uuid );
    $this->assertEmpty($error, 'redis returned error');

    return $transition_uuid;
  }
  
  /*
   * resolveTransition
   *
   * resolve previously created transition
   * @param string transition_uuid
   *
   */
  private function resolveTransition($transition_uuid)
  {
    // resolve all pending transitions in our domain
    $curl_options = array(
      CURLOPT_USERPWD  => "dougmeli:Flora",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC);
    $json_result = curl_post(find_site_url() . '/pr/internal/1/ultra/api/internal__ResolvePendingTransitions.json',
      array(), $curl_options, NULL, 240);
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);

    // confirm that our transition was indeed resolved
    $this->assertContains($transition_uuid, $decoded_response->closed_transitions, "transition $transition_uuid did not resolve");
  }


  /*
   * datePST2UTC
   *
   * convert PST date into UTC
   * @param string date/time
   * @return string date/time
   *
   */
  private function datePST2UTC($date)
  {
    $dateTime = new DateTime($date, new DateTimeZone('America/Los_Angeles'));
    $dateTime->setTimezone(new DateTimeZone('UTC'));
    $result = $dateTime->format('M d Y h:i:sA');
    return $result;
  }


  /*
   * getDifference
   *
   * return difference in seconds betweeen two dates (must be same time zone)
   *
   * @param string date/time
   * @return int seconds
   */
  private function getDifference($first, $second)
  {
    $fistTime = strtotime($first);
    $secondTime = strtotime($second);
    $difference = abs($fistTime - $secondTime);
    echo "getDifference($first, $second) -> $difference sec (" . ($difference / 60 / 60 / 24) . " days)\n";
    return $difference;
  }


  /*
   * runTransition
   *
   * execute single transition with given actions and return updated customer dates
   * @param int custmerID
   * @param array or arrays actions
   * @param boolean dryRun: do not queue transition
   * @retun array of strings: plan_started, plan_expires
   */
  private function runTransition($customerID, $actions, $dryRun = FALSE)
  {
    // create and resolve this transition
    if (! $dryRun)
    {
      $transition_uuid = $this->createTransition($customerID, $actions);
      $this->resolveTransition($transition_uuid);
    }

    // get and validate updated customer info
    $customer = get_customer_from_customer_id($customerID);
    $this->assertInstanceOf('stdClass', $customer);
    $this->assertObjectHasAttribute('plan_started', $customer);
    $this->assertObjectHasAttribute('plan_expires', $customer);

    // return new values
    echo "runTransition() -> plan_started: {$customer->plan_started}, plan_expires: {$customer->plan_expires}\n";
    return array($customer->plan_started, $customer->plan_expires);
  }


  /*
   * testActiveToActive
   *
   * confirm dates for Active->Active transition:
   *  a) 30 days from start date if started at midnight
   *  b) 30+ days if started any other time (first day is free)
   *
   */
  public function testActiveToActive()
  {
    $customerID = 2510;

    // PART 1: plan_start = midnight
    $startTime = self::START_MIDNIGHT;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('renewal->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: exactly 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertEquals(self::ONE_DAY * 30, $diff, 'plan_expired out of range');


    // PART 2: test with starting time in the afternoon
    $startTime = self::START_CUTOFF;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST (technically incorrect)";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('renewal->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: more than 30 but less than 31 days
    $diff = $this->getDifference($started, $expires);
    $this->assertLessThan(self::ONE_DAY * 30, $diff, 'plan_expired is not less than 30 days');
  }


  /*
   * testSuspendedToActive
   *
   * confirm dates for Suspended->Active transition:
   *  a) 30 days from start date if started at midnight
   *  b) 29+ days if started any other time (entire first day counts)
   *
   */
  public function testSuspendedToActive()
  {
    $customerID = 2698;

    // PART 1: test with starting time at midnight
    $startTime = self::START_MIDNIGHT;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('renewal->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: exactly 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertEquals(self::ONE_DAY * 30, $diff, 'plan_expired out of range');


    // PART 2: test with starting time in the afternoon
    $startTime = self::START_CUTOFF;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('renewal->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: greater than 29 but less than 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertGreaterThan(self::ONE_DAY * 29, $diff, 'plan_expired is not greater than 29 days');
    $this->assertLessThan(self::ONE_DAY * 30, $diff, 'plan_expired is not less than 30 days');


    // PART 3: test with starting time at night
    $startTime = self::START_NIGHT;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('renewal->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: greater than 29 but less than 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertGreaterThan(self::ONE_DAY * 29, $diff, 'plan_expired is not greater than 29 days');
    $this->assertLessThan(self::ONE_DAY * 30, $diff, 'plan_expired is not less than 30 days');
  }


  /*
   * testNeutralToActive
   *
   * confirm dates for Neutral->Active transition (most complicated):
   *  a) exactly 30 days if started at midnight
   *  b) 29+ days if started before cutoff time
   *  c) 30+ days if started on or after cutoff time (half day free)
   *
   */
  public function testNeutralToActive()
  {
    $customerID = 2857;

    // PART 1: test with starting time at midnight
    $startTime = self::START_MIDNIGHT;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('initial->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: exactly 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertEquals(self::ONE_DAY * 30, $diff, 'plan_expired out of range');


    // PART 2: test with starting time before cutoff
    $startTime = self::START_MORNING;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('initial->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: greater than 29 but less than 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertGreaterThan(self::ONE_DAY * 29, $diff, 'plan_expired is not greater than 29 days');
    $this->assertLessThan(self::ONE_DAY * 30, $diff, 'plan_expired is not less than 30 days');


    // PART 3: test with starting time at cutoff
    $startTime = self::START_CUTOFF;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('initial->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: greater than 30 but less than 31 days
    $diff = $this->getDifference($started, $expires);
    $this->assertGreaterThan(self::ONE_DAY * 30, $diff, 'plan_expired is not greater than 30 days');
    $this->assertLessThan(self::ONE_DAY * 31, $diff, 'plan_expired is not less than 31 days');


    // PART 4: test with starting time afte cutoff
    $startTime = self::START_EVENING;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('initial->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: greater than 30 but less than 31 days
    $diff = $this->getDifference($started, $expires);
    $this->assertGreaterThan(self::ONE_DAY * 30, $diff, 'plan_expired is not greater than 30 days');
    $this->assertLessThan(self::ONE_DAY * 31, $diff, 'plan_expired is not less than 31 days');


    // PART 5: test with starting time at night
    $startTime = self::START_NIGHT;
    echo "\n" . __FUNCTION__ . ": plan starts at $startTime PST";
    $actions = array(
      make_action('active->plan_started=X', "dbo.PT_to_UTC('$startTime')"), // see note 1
      make_action('initial->active.plan_expires', 'plan_started', 30));

    list($started, $expires) = $this->runTransition($customerID, $actions, FALSE);

    // confirm plan_started is set correctly
    $diff = $this->getDifference($started, $this->datePST2UTC($startTime));
    $this->assertEquals(0, $diff, 'plan_started out of range');

    // confirm plan_expires is set correctly: greater than 29 but less than 30 days
    $diff = $this->getDifference($started, $expires);
    $this->assertGreaterThan(self::ONE_DAY * 29, $diff, 'plan_expired is not greater than 29 days');
    $this->assertLessThan(self::ONE_DAY * 30, $diff, 'plan_expired is not less than 30 days');
  }

}


?>
