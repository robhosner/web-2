<?php
require_once 'PHPUnit/Framework/TestSuite.php';

require_once 'Ultra/tests/API_SOAP/provisioning__checkSIMCardTest.php';

require_once 'Ultra/tests/API_SOAP/provisioning__checkZipCodeTest.php';

require_once 'Ultra/tests/API_SOAP/provisioning__GetPortingStateTest.php';

require_once 'Ultra/tests/API_SOAP/customercare__CustomerInfoByIDTest.php';

require_once 'Ultra/tests/API_SOAP/customercare__ValidateMSISDNTest.php';

/**
 * SOAP Test Suite
 */
class API_SOAPSuite extends PHPUnit_Framework_TestSuite
{

  /**
   * Constructs the test suite handler.
   */
  public function __construct()
  {
    $this->setName('API_SOAPSuite');
    
    $this->addTestSuite('provisioning__checkSIMCardTest');
    
    $this->addTestSuite('provisioning__checkZipCodeTest');
    
    $this->addTestSuite('provisioning__GetPortingStateTest');
    
    $this->addTestSuite('customercare__CustomerInfoByIDTest');
    
    $this->addTestSuite('customercare__ValidateMSISDNTest');
  }

  /**
   * Creates the suite for testing all SOAP tests.
   */
  public static function suite()
  {
    return new self();
  }
}

