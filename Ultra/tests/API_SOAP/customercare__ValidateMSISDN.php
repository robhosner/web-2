<?php
include_once 'SoapModule.php';
/** 
 * @author john
 * 
 */
class customercare__ValidateMSISDN extends SoapModule
{
  function __construct($test_wsdl)
  {
      parent::__construct($test_wsdl, get_class($this));
  }
  
  function params()
  {
      return array(
              'partner_tag' => "internal-test-default",
              'msisdn' => '3238675309'
                   );
  }
  
  function execute(array $args=NULL)
  {
      // Execute and print out results
      if( $args !== NULL )
      {
          echo parent::pretty(parent::getClient()->__soapCall(get_class($this), $args ));
          return parent::getClient()->__soapCall(get_class($this), $args );
      }
      else
      {
          echo parent::pretty(parent::getClient()->__soapCall(get_class($this), $this->params()));
          return parent::getClient()->__soapCall(get_class($this), $this->params());
      }
  }
}