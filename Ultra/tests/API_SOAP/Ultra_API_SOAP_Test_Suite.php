<?php
require_once 'PHPUnit/Framework/TestSuite.php';

require_once 'Ultra/tests/API_SOAP/customercare__CustomerInfoByIDTest.php';

require_once 'Ultra/tests/API_SOAP/customercare__ValidateMSISDNTest.php';

require_once 'Ultra/tests/API_SOAP/provisioning__checkSIMCardTest.php';

require_once 'Ultra/tests/API_SOAP/provisioning__checkZipCodeTest.php';

require_once 'Ultra/tests/API_SOAP/provisioning__GetPortingStateTest.php';


class Ultra_API_SOAP_Test_Suite extends PHPUnit_Framework_TestSuite
{


  public function __construct()
  {
    $this->setName('Ultra_API_SOAP_Test_Suite');
    
    $this->addTestSuite('customercare__CustomerInfoByIDTest');
    
    $this->addTestSuite('customercare__ValidateMSISDNTest');
    
    $this->addTestSuite('provisioning__checkSIMCardTest');
    
    $this->addTestSuite('provisioning__checkZipCodeTest');
    
    $this->addTestSuite('provisioning__GetPortingStateTest');
  }


  public static function suite()
  {
    return new self();
  }
}

