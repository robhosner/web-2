<?php

class SOAPTestRunner
{

  private $configData;

  private $modules = array();

  public function __construct($classToTest = NULL, $userParams = NULL)
  {
    if ($classToTest === NULL)
    {
      $classList = get_declared_classes();
      foreach ($classList as $myClass)
      {
        if (is_subclass_of($myClass, 'SoapModule'))
        {
          $value = array(
            'execute' => array()
          );
          
          $this->configData[$myClass] = $value;
        }
      }
    }
    else
    {
      $value = array(
        'execute' => array(),
        'args' => $userParams
      );
      $this->configData[$classToTest] = $value;
      $this->configData['args'] = $userParams;
    }
  }

  function runTest($wsdl)
  {
    $interface = new ReflectionClass('SoapModule');
   
    foreach ($this->configData as $modulename => $params)
    {
      $module_class = new ReflectionClass($modulename);
     
      if (! $module_class->isSubclassOf($interface))
      {
        throw new Exception("unknown module type: $modulename");
      }
      $module = $module_class->newInstance($wsdl);
      foreach ($module_class->getMethods() as $method)
      {
        if ($method->getName() == "execute")
        {
          return $this->handleMethod($module, $method, $params);
        }
        else
        {
         
          $this->handleMethod($module, $method, $params);
        }
      }
      array_push($this->modules, $module);
    }
  }

  function handleMethod(SoapModule $module, ReflectionMethod $method, $params)
  {
    $name = $method->getName();
    $args = $method->getParameters();
    if ($name != "execute")
    {
      return false;
    }
    
    if ($params['args'] == NULL)
    {
      return $method->invoke($module);
    }
    else
    {
      return $method->invoke($module, $params['args'] );
    }
  }
}
