<?php
require_once 'Ultra/tests/API_SOAP/provisioning__checkSIMCard.php';
require_once 'PHPUnit/Framework/TestCase.php';
include_once 'SOAPTestRunner.php';
/**
 * provisioning__checkSIMCard test case.
 */
class provisioning__checkSIMCardTest extends PHPUnit_Framework_TestCase
{
  private $testRunner;
  
  private $wsdl;
  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
      parent::setUp();
  
      include ((getenv('HTT_CONFIGROOT') . '/e.php'));
  
      $this->testRunner = new SOAPTestRunner("provisioning__checkSIMCard", array(
                'partner_tag' => "internal-test-default",'ICCID' => 123456789012345678,'mode' => '' ));
      $this->wsdl       = "https://" . $e_config['www/sites'] . "/ps/celluphone/1/ultra/api.wsdl";
  }
  
  /**
   * Cleans up the environment after running a test.
   */
  protected function tearDown()
  {
    // TODO Auto-generated provisioning__checkSIMCardTest::tearDown()
    $this->provisioning__checkSIMCard = null;
    
    parent::tearDown();
  }

  public function testExecute()
  {
      $xmlData = $this->testRunner->runTest($this->wsdl);
  
      $client = simplexml_load_string($xmlData);
      $this->assertNotEmpty($client);
      $this->assertTrue( ! $client->data->success );
  
      if( $client->data->success == 'false'   )
      {
          $message = (string )$client->errors->n0;
          echo $message . PHP_EOL;
          $this->assertRegExp( '/ERR_API/', $message );
      }
  }
}

