<?php
include_once 'SoapModule.php';
require_once 'PHPUnit/Framework/TestCase.php';
include_once 'Ultra/tests/API_SOAP/customercare__ValidateMSISDN.php';
include_once 'SOAPTestRunner.php';

/**
 * customercare__ValidateMSISDN test case.
 */
class customercare__ValidateMSISDNTest extends PHPUnit_Framework_TestCase
{

  private $testRunner;
  
  private $wsdl;

  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
    parent::setUp();
    
    include ((getenv('HTT_CONFIGROOT') . '/e.php'));
    
    $this->testRunner = new SOAPTestRunner("customercare__ValidateMSISDN", array(
      'partner_tag' => "internal-test-default",'msisdn' => '9513107133'
    ));
    $this->wsdl = "https://" . $e_config['www/sites'] . "/ps/celluphone/1/ultra/api.wsdl";
  }

  /**
   * Cleans up the environment after running a test.
   */
  protected function tearDown()
  {
    
    parent::tearDown();
  }

  /**
   * Run the SOAP Test
   */
  public function testExecute()
  {
    $xmlData = $this->testRunner->runTest($this->wsdl);

    $client = simplexml_load_string($xmlData);
    $this->assertNotEmpty($client);
    $this->assertTrue( ! $client->data->success );
    
    if( $client->data->success == 'false'   )
    {
       $message = (string )$client->errors->n0;
       echo $message . PHP_EOL;
       $this->assertRegExp( '/ERR_API/', $message );
    }
  }
}

