<?php
// Test Driver
include_once 'SOAPTestRunner.php';

// Test
include_once 'customercare__CustomerInfoByID.php';

// ** WSDL Interfaces that are supported
$target = 'ePay';
$target = 'threeci';
$target = 'celluphone-develop';
$target = 'ePay';
$target = 'interactivecare';
$target = 'celluphone'; // Last one, this is used

try
{
  class SomeOtherTest extends SoapModule
  {

    function __construct($test_wsdl)
    {
      parent::__construct($test_wsdl, get_class($this));
    }

    function params()
    {
      return array(
        'p1' => 1,'p2' => 2
      );
    }

    function execute()
    {
      $bar = $this->params();
      print "SomeOtherTest::execute, p2=" . $bar['p2'] . "\n";
    }
  }

  
  include ((getenv('HTT_CONFIGROOT') . '/e.php'));

  $test = new SOAPTestRunner("customercare__CustomerInfoByID");
  $wsdlTest = "https://" . $e_config['www/sites'] . "/ps/" . $target . "/1/ultra/api.wsdl";
  print_r ( $test->runTest($wsdlTest) );
}
catch (Exception $e)
{}
