<?php

require_once 'PHPUnit/Framework/TestCase.php';
require_once 'db.php';
require_once 'lib/util-common.php';

/**
 * test class for lib/util-common.php
 */
class LibUtilCommonTest extends PHPUnit_Framework_TestCase
{
  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
    parent::setUp ();

    teldata_change_db();
  }

  /**
   * Cleans up the environment after running a test.
   */
  protected function tearDown()
  {
    parent::tearDown ();
  }

  /**
   * Test for function validateUSZipcode
   */
  public function test_validateUSZipcode()
  {
    // invalid zip codes
    $this->assertFalse( ! ! validateUSZipcode('') );
    $this->assertFalse( ! ! validateUSZipcode('abcde') );
    $this->assertFalse( ! ! validateUSZipcode('123456') );
    $this->assertFalse( ! ! validateUSZipcode('1234') );

    // a valid zip code
    $this->assertTrue(  ! ! validateUSZipcode('12345') );
  }

  /**
   * Test for function extractZipcodesFromString
   */
  public function test_extractZipcodesFromString()
  {
    $invalidString = "a,b";

    // an invalid string
    list( $zipcodes , $error ) = extractZipcodesFromString( $invalidString );

    $this->assertTrue(  ! ! $error );
    $this->assertTrue(  ! ! is_array($zipcodes) );
    $this->assertFalse( ! ! count( $zipcodes ) );

    // a valid list of zip codes
    $validString = "12345, 98101 ";

    list( $zipcodes , $error ) = extractZipcodesFromString( $validString );

    $this->assertFalse( ! ! $error );
    $this->assertTrue(  ! ! is_array($zipcodes) );
    $this->assertEquals( count( $zipcodes ) , 2 );
    $this->assertContains( "12345" , $zipcodes );
    $this->assertContains( "98101" , $zipcodes );
  }

}
