<?php

require_once 'PHPUnit/Framework/TestCase.php';
require_once 'db.php';
require_once 'db/htt_inventory_sim.php';

/**
 * test class for db/htt_inventory_sim.php
 */
class TestInventorySim extends PHPUnit_Framework_TestCase
{
  const VALID_PRODUCT = 'PURPLE';
  const VALID_ICCID   = '890126084210224057';
  
  const BOGUS_PRODUCT = 'PINK';
  const SHORT_ICCID   = '89012608421022405';
  
  
  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
    parent::setUp ();
    
    teldata_change_db();
  }
  
  /**
   * Cleans up the environment after running a test.
   */
  protected function tearDown()
  {
    parent::tearDown ();
  }
  
  /**
   * Constructs the test case.
   */
  public function __construct()
  {
  }
  
  public function test_good_sim_and_product() 
  {
    $res = validate_sim_product_type( self::VALID_PRODUCT, self::VALID_ICCID);
    $this->assertTrue(  $res->is_success() == 1 );
    
  }
  
  public function test_good_sim_and_bogus_product()
  {
    $res = validate_sim_product_type( self::VALID_PRODUCT, self::BOGUS_PRODUCT);
    $this->assertFalse( $res->is_failure() == 0);
  }
  
  public function test_short_sim_and_product()
  { 
    $res = validate_sim_product_type( self::SHORT_ICCID, self::VALID_ICCID);
    $this->assertFalse( $res->is_failure() == 0);
  }
  
  public function totally_bogus_data()
  {
    $res = validate_sim_product_type( self::SHORT_ICCID, self::BOGUS_PRODUCT);
    $this->assertFalse( $res->is_failure() == 0 );
  }
}

