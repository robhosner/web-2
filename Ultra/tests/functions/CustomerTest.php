<?php

/*
 * Usage : phpunit Ultra/tests/functions/CustomerTest.php
 */

class CustomerTest extends PHPUnit_Framework_TestCase
{
  const TEST_CUSTOMER    = '706447128491';
  const TEST_CUSTOMER_ID = 1983;
  const TEST_ACT_CODE    = '98765432101';
  const TEST_ICCID       = '890126084210224392';
  const TEST_LOGIN_NAME  = 'RHZHQGFTABFMFZELNLLX';
  const TEST_MSISDN      = '5189865584';
  const TEST_EMAIL       = 'jhgsfkjhsgkjg@ultra.me';
  const TEST_NAME        = 'RHZHQGFTABFMFZELNLLX';

  public function setUp ()
  {
    require_once 'db.php';

    teldata_change_db ();
  }

  /*
   * Test for get_customer_from_customer
   */
  public function testCustomerQueriesFromCustomer ()
  {
    $customer = get_customer_from_customer( self::TEST_CUSTOMER );

    // there must be only a customer with the given CUSTOMER
    $this->assertNotEmpty( $customer );

    $this->assertTrue( is_object($customer) );

    $this->assertEquals( $customer->CUSTOMER , self::TEST_CUSTOMER );
  }

  /*
   * Test for get_customer_from_customer_id
   */
  public function testCustomerQueriesFromCustomerId ()
  {
    $customer = get_customer_from_customer_id( self::TEST_CUSTOMER_ID );

    // there must be only a customer with the given CUSTOMER_ID
    $this->assertNotEmpty( $customer );

    $this->assertTrue( is_object($customer) );

    $this->assertEquals( $customer->CUSTOMER_ID , self::TEST_CUSTOMER_ID );
  }

  /*
   * Test for get_customer_from_actcode
   */
  public function testCustomerQueriesFromActCode ()
  {
    $customer = get_customer_from_actcode( self::TEST_ACT_CODE );

    // there must be only a customer with the given ACT_CODE
    $this->assertNotEmpty( $customer );

    $this->assertTrue( is_object($customer) );
  }

  /*
   * Test for get_customer_from_iccid
   */
  public function testCustomerQueriesFromICCID ()
  {
    $customer = get_customer_from_iccid( self::TEST_ICCID );

    // there must be only a customer with the given ICCID
    $this->assertNotEmpty( $customer );
    
    $this->assertTrue( is_object($customer) );

    $this->assertEquals( $customer->current_iccid , self::TEST_ICCID );
  }

  /*
   * Test for testCustomerQueriesFromLogin
   */
  public function testCustomerQueriesFromLogin ()
  {
    $customer = get_customer_from_login( self::TEST_LOGIN_NAME );

    // there must be only a customer with the given LOGIN
    $this->assertNotEmpty( $customer );
    
    $this->assertTrue( is_object($customer) );
    
    $this->assertEquals( $customer->LOGIN_NAME , self::TEST_LOGIN_NAME );
  }

  /*
   * Test for testCustomerQueriesFromMSISDN
   */
  public function testCustomerQueriesFromMSISDN ()
  {
    $customer = get_customer_from_msisdn( self::TEST_MSISDN );

    // there must be a customer with the given MSISDN
    $this->assertNotEmpty( $customer );
    
    $this->assertTrue( is_object($customer) );

    $this->assertEquals( $customer->current_mobile_number , self::TEST_MSISDN );
  }

  /*
   * Test for get_customers_from_email
   */
  public function testCustomerQueriesFromEmail ()
  {
    $customers = get_customers_from_email( self::TEST_EMAIL );

    // there must be a customer with the given EMAIL
    $this->assertNotEmpty( $customers[0] );
    
    $this->assertTrue( is_object($customers[0]) );

    $this->assertEquals( $customers[0]->E_MAIL , self::TEST_EMAIL );
  }

  /*
   * Test for get_customers_from_name
   */
  public function testCustomerQueriesFromName ()
  {
    $customers = get_customers_from_name( self::TEST_NAME );

    // there must be a customer with the given NAME
    $this->assertNotEmpty( $customers[0] );
    
    $this->assertTrue( is_object($customers[0]) );

    $this->assertEquals( $customers[0]->FIRST_NAME , self::TEST_NAME );
    $this->assertEquals( $customers[0]->LAST_NAME  , self::TEST_NAME );
  }
}

?>
