<?php

// we are testing this module :
require_once 'Ultra/Lib/DB/Setter/Customer.php';

require_once 'db.php';

class LibDBSetterCustomer_Test extends PHPUnit_Framework_TestCase
{
  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
    parent::setUp ();

    teldata_change_db();
  }

  /**
   * Cleans up the environment after running a test.
   */
  protected function tearDown()
  {
    parent::tearDown ();
  }

  /**
   * test for \Ultra\Lib\DB\Setter\Customer\cc_info
   */
  public function test_cc_info_()
  {
    $params = array(
      'customer_id'  => 31,
      'cc_number'    => '5454871000080964',
      'cvv'          => '1234',
      'expires_date' => '0815',
      'cc_address1'  => '123 main',
      'last_name'    => 'test'
    );

    $result = \Ultra\Lib\DB\Setter\Customer\cc_info( $params );

    print_r( $result );
  }

  /**
   * test for \Ultra\Lib\DB\Setter\Customer\ultra_info
   */
  public function test_ultra_info_()
  {
    $params = array(
      'customer_id'  => 31
    );

    $result = \Ultra\Lib\DB\Setter\Customer\ultra_info( $params );

    print_r( $result );
  }

  /**
   * test for \Ultra\Lib\DB\Setter\Customer\validate_ultra_info
   */
  public function test_validate_ultra_info()
  {
    // customer_id

    $params = array();
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params = array( 'customer_id' => 'a' );
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params = array( 'customer_id' => 31 );
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // notes

    $params['notes'] = '+-*=":';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['notes'] = 'test 123';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // current_mobile_number

    $params['current_mobile_number'] = '+-*=":';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['current_mobile_number'] = '10010010001';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['current_mobile_number'] = '100100100';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['current_mobile_number'] = '1001001000';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // current_iccid

    $params['current_iccid'] = 'a';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['current_iccid'] = '10010010001001001000';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['current_iccid'] = '10010010001001001';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['current_iccid'] = '1001001000100100100';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // easypay_activated
  
    $params['easypay_activated'] = 'a';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['easypay_activated'] = '1';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // stored_value
  
    $params['stored_value'] = 'a';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['stored_value'] = '12.34';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // plan_state

    $params['plan_state'] = 'state';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $states = array( 'Active', 'Provisioned', 'Suspended', 'Port-In Requested', 'Port-In Denied', 'Pre-Funded', 'Promo Unused', 'Cancelled');
    foreach ( $states as $state )
    {
      $params['plan_state'] = $state;
      list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
      $this->assertEmpty( $errors );
    }


    // preferred_language

    $params['preferred_language'] = 'IT';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['preferred_language'] = 'EN';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // monthly_cc_renewal

    $params['monthly_cc_renewal'] = 'a';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['monthly_cc_renewal'] = '1';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );


    // tos_accepted

    #$params['tos_accepted'] = 'a';
    #list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    #print_r( $errors );
    #$this->assertNotEmpty( $errors );

    #$params['tos_accepted'] = '1';
    #list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    #$this->assertEmpty( $errors );


    // activation_iccid

    $params['activation_iccid'] = 'a';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['activation_iccid'] = '10010010001001001000';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['activation_iccid'] = '10010010001001001';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['activation_iccid'] = '1001001000100100100';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );

  
    // customer_source
  
    $params['customer_source'] = '+-*:"';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['customer_source'] = 'test';
    list( $errors , $error_codes ) = \Ultra\Lib\DB\Setter\Customer\validate_ultra_info( $params );
    $this->assertEmpty( $errors );
  }

  /**
   * test for \Ultra\Lib\DB\Setter\Customer\validate_cc_info
   */
  public function test_validate_cc_info()
  {
    // customer_id

    $params = array();
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params = array( 'customer_id' => 'a' );
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params = array( 'customer_id' => 31 );
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // local_phone
    $params['local_phone'] = 'q +"-* z';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['local_phone'] = '+1 (888) 999-1111';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // e_mail
    $params['e_mail'] = '@+-*@';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['e_mail'] = 'user@test.com';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // state_region

    $params['state_region'] = '+-*';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['state_region'] = 'WA';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // postal_code

    $params['postal_code'] = 'a';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['postal_code'] = '123456';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['postal_code'] = '1234';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['postal_code'] = '54321';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // country

    $params['country'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['country'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['country'] = 'US';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // city

    $params['city'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['city'] = 'testCITY';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // address1

    $params['address1'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['address1'] = 'test 3 2 1';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );

    // address2

    $params['address2'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['address2'] = 'test 1 2 3';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_name

    $params['cc_name'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_name'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_address1

    $params['cc_address1'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_address1'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_address2

    $params['cc_address2'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_address2'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_city

    $params['cc_city'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_city'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_country

    $params['cc_country'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_country'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_country'] = 'US';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_state_region

    $params['cc_state_region'] = '+-';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_state_region'] = 'test';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_postal_code

    $params['cc_postal_code'] = 'a';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_postal_code'] = '123456';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_postal_code'] = '1234';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_postal_code'] = '12345';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cvv
    $params['cvv'] = '40404';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cvv'] = '11';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cvv'] = '111';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // expires_date

    $params['expires_date'] = '4040';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['expires_date'] = '1118';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertEmpty( $errors );


    // cc_number


    $params['cc_number'] = '123456';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    $params['cc_number'] = '123456123456123456';
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    print_r( $errors );
    $this->assertNotEmpty( $errors );

    // customer does not exist
    $params = array(
      'customer_id'        => 33,
      'cvv'                => '234',
      'expires_date'       => '0815',
      'cc_address1'        => '123 main',
      'cc_name'            => 'test',
      'cc_postal_code'     => '12345',
      'cc_number'          => '349999999999991'
    );

    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertNotEmpty( $errors );

    // invalid credit card
    $params['customer_id'] = 19493;
    list( $errors , $error_codes , $cvv_validation , $avs_validation ) = \Ultra\Lib\DB\Setter\Customer\validate_cc_info( $params );
    $this->assertNotEmpty( $errors );
  }
}

?>
