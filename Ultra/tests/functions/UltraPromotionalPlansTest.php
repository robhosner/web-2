<?php

require_once 'PHPUnit/Framework/TestCase.php';
require_once 'db.php';
require_once 'db/ultra_promotional_plans.php';

/**
 * test class for db/ultra_promotional_plans.php
 */
class LibUtilCommonTest extends PHPUnit_Framework_TestCase
{
  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
    parent::setUp ();

    teldata_change_db();
  }

  /**
   * Cleans up the environment after running a test.
   */
  protected function tearDown()
  {
    parent::tearDown ();
  }

  /**
   * Test for function ultra_promotional_plans_post_activation
   */
  public function test_ultra_promotional_plans_post_activation()
  {
    $return = ultra_promotional_plans_post_activation( 31 );

    $this->assertTrue(  ! ! is_array($return) );
    $this->assertTrue(  ! ! is_array($return['errors']) );
    $this->assertTrue(  ! ! $return['success'] );
    $this->assertTrue(  ! count( $return['errors'] ) );
  }

}

?>
