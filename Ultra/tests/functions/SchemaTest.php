<?php

require_once 'db.php';

class Schema extends PHPUnit_Framework_TestCase
{
  public function setUp() 
  {
    teldata_change_db();
  }

  public function test_makeSelectQuery()
  {
    // past_x_days
    $sql = \Ultra\Lib\DB\makeSelectQuery(
      'ULTRA.PROMO_BROADCAST_CAMPAIGN',
      NULL,
      array('NAME'),
      array('CREATED_DATE' => 'past_5_days')
    );

    echo "$sql" . PHP_EOL;

    $result = mssql_fetch_all_rows(logged_mssql_query($sql));
    print_r($result);
    $this->assertNotEmpty($result);

    // last_x_hours
    $sql = \Ultra\Lib\DB\makeSelectQuery(
      'ULTRA.PROMO_BROADCAST_CAMPAIGN',
      NULL,
      array('NAME'),
      array('CREATED_DATE' => 'last_' . (24 * 5) . '_hours')
    );

    echo "$sql" . PHP_EOL;

    $result = mssql_fetch_all_rows(logged_mssql_query($sql));
    print_r($result);
    $this->assertNotEmpty($result);

    $lastHoursResult = $result;

    // last_x_minutes
    $sql = \Ultra\Lib\DB\makeSelectQuery(
      'ULTRA.PROMO_BROADCAST_CAMPAIGN',
      NULL,
      array('NAME'),
      array('CREATED_DATE' => 'last_' . (24 * 5 * 60) . '_minutes')
    );

    echo "$sql" . PHP_EOL;

    $result = mssql_fetch_all_rows(logged_mssql_query($sql));
    print_r($result);
    $this->assertNotEmpty($result);
    $this->assertEquals($result, $lastHoursResult);

    // makeWhereClause int greater than (gt_x)
    $sql = \Ultra\Lib\DB\makeSelectQuery(
      'ULTRA.SUMMARY_DEALER_RECHARGE_RATE',
      10,
      array('DEALER', 'ACTIVATIONS'),
      array(
        'DEALER' => 9996,
        'ACTIVATIONS' => 'gt_1'
      )
    );

    echo "$sql" . PHP_EOL;

    $result = mssql_fetch_all_rows(logged_mssql_query($sql));
    $this->assertNotEmpty($result);

    // makeWhereClause int less than (lt_x)
    $sql = \Ultra\Lib\DB\makeSelectQuery(
      'ULTRA.SUMMARY_DEALER_RECHARGE_RATE',
      10,
      array('DEALER', 'ACTIVATIONS'),
      array(
        'DEALER' => 9996,
        'ACTIVATIONS' => 'lt_2'
      )
    );

    echo "$sql" . PHP_EOL;

    $result = mssql_fetch_all_rows(logged_mssql_query($sql));
    $this->assertNotEmpty($result);
  }

  public function test_makeUpdateQuery()
  {
    $sql = \Ultra\Lib\DB\makeUpdateQuery(
      'ULTRA.PROMO_BROADCAST_LOG',
      array(
        'PROMO_STATUS'       => 'CLAIMED',
        'PROMO_CLAIMED_DATE' => timestamp_to_date( time() , MSSQL_DATE_FORMAT , 'UTC' )
      ),
      array(
        'PROMO_BROADCAST_CAMPAIGN_ID' => 1,
        'CUSTOMER_ID'                 => 31
      )
    );

    echo "$sql" . PHP_EOL;
  }
}

?>