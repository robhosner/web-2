<?php
require_once 'PHPUnit/Framework/TestCase.php';
require_once 'db.php';
include_once 'lib/inventory/functions.php';

/**
 * test case.
 */
class ZipCodeDistributionTest extends PHPUnit_Framework_TestCase
{
    const VALID_RANGE_START = '800000000000000001';
    const VALID_RANGE_END   = '890126084211563959';

    const VALID_336_START   = '8901260842102000750';
    const VALID_336_END     = '8901260842102004111';

    const VALID_7_START     = '8901260842102012128';
    const VALID_7_END       = '8901260842102012185';
    
    const NARROW_RANGE_START = '800000000000000001';
    const NARROW_RANGE_END   = '890126084211563959';
    
    const BOGUS_RANGE_START = '600000000000000001';
    const BOGUS_RANGE_END   = '700000000000000001';

    private $shortList = array(90210, 90215);
    
    private $sevenZips = array( 92881, 92882, 92883, 92884, 92885, 92886, 92887 );

    private $longList = array( 92881, 92882, 92883 );
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp ()
    {
        parent::setUp();

        teldata_change_db();
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown ()
    {
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct ()
    {    
    }
    
    public function test_valid_range()
    {
        $res = validate_sim_zip_code_distribution( self::VALID_RANGE_START, self::VALID_RANGE_END, $this->shortList );
        dlog('','%s',$res->get_errors());
        $this->assertTrue( ! ! $res->is_failure() );
    }
    
    public function test_valid336_range()
    {
        $res = validate_sim_zip_code_distribution( self::VALID_336_START, self::VALID_336_END, $this->sevenZips );
        dlog('','%s',$res->get_errors());
        $this->assertTrue( ! ! $res->is_success() );
    }
    
    public function test_valid7_range()
    {
        $res = validate_sim_zip_code_distribution( self::VALID_7_START, self::VALID_7_END, $this->shortList );
        dlog('','%s',$res->get_errors());
        $this->assertTrue( ! ! $res->is_success() );
    }
    
    
    public function test_no_sims_in_range()
    {
        $res = validate_sim_zip_code_distribution( self::BOGUS_RANGE_START, self::BOGUS_RANGE_END, $this->longList  );
        dlog('','%s',$res->get_errors());
        $this->assertFalse( ! $res->is_failure() );
    }

    // No good way to simulate -- except code modification to test logic path leaving path commented out.
//     public function test_no_database()
//     {
//         $res = validate_sim_zip_code_distribution( self::BOGUS_RANGE_START, self::BOGUS_RANGE_END );
//         $this->assertFalse( $res->is_failure() == 0);
//     }
}

