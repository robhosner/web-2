<?php

require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

/**
 * PHPunit test class for NotificationNotificationReceived
 */
class NotificationTest extends PHPUnit_Framework_TestCase
{
  public function test_processNotification()
  {
    // test NotificationReceived with a known keyword with regular MSISDN
    $message = array(
      'MSISDN'    => '99126849110730622',
      'shortCode' => '6700',
      'smsText'   => 'START 11211');
    $parameters = array(
      'actionUUID' => getNewActionUUID(__FUNCTION__ . time()),
      'uuid'       => create_guid(),
      'command'    => 'NotificationReceived',
      'parameters' => $message);
    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification;
    $result = $notification->processNotification($parameters);
    echo $result;

    // test NotificationReceived with a known keyword and fake MSISDN
    $message = array(
      'MSISDN'    => '26849110730622',
      'shortCode' => '6700',
      'smsText'   => 'START11211');
    $parameters = array(
      'actionUUID' => getNewActionUUID(__FUNCTION__ . time()),
      'uuid'       => create_guid(),
      'command'    => 'NotificationReceived',
      'parameters' => $message);
    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification;
    $result = $notification->processNotification($parameters);
    echo $result;

    // NotificationReceived test with a known keyword that contains gargage
    $message = array(
      'MSISDN'    => '26849110730622',
      'shortCode' => '6700',
      'smsText'   => 'START.11211.');
    $parameters = array(
      'actionUUID' => getNewActionUUID(__FUNCTION__ . time()),
      'uuid'       => create_guid(),
      'command'    => 'NotificationReceived',
      'parameters' => $message);
    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification;
    $result = $notification->processNotification($parameters);
    echo $result;

    // test NotificationReceived with an unknown keyword
    $message = array(
      'MSISDN'    => '26849110730622',
      'shortCode' => '6700',
      'smsText'   => 'BALANCE');
    $parameters = array(
      'actionUUID' => getNewActionUUID(__FUNCTION__ . time()),
      'uuid'       => create_guid(),
      'command'    => 'NotificationReceived',
      'parameters' => $message);

    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification;
    $result = $notification->processNotification($parameters);
    echo $result;
  }
}
