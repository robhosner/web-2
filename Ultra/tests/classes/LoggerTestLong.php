<?php

// SRV-497: log4php can be installed in /opt/Logger (incorrect) or /usr/share/pear/log4php (correct)
require_once (file_exists('/opt/Logger') ? '' : 'log4php/') . 'Logger.php';

class TestLogger
{
    /** Holds the Logger. */
    private $log;

    /** Logger is instantiated in the constructor. */
    public function __construct()
    {
        // The __CLASS__ constant holds the class name, in our case "TestLogger".
        // Therefore this creates a logger named "TestLogger" (which we configured in the config file)
        $this->log = Logger::getLogger(__CLASS__);
    }

    /** Logger can be used from any member method. */
    public function go()
    {
        $this->log->info("Class-Method: -- Should not report in email" . __CLASS__ . ' - ' . __METHOD__);
        $this->log->fatal( "(FATAL ERROR -- (Email Report) Class-Method: " . __CLASS__ . ' - ' . __METHOD__ );
    }
}


function testLogFunction() {
    $log = Logger::getLogger('myLoggerf');
    LoggerNDC::push("testfoo context");
    $log->info("First Function message.");   // Not logged because INFO < WARN  
    $log->error("Logged from testfoo()."); // This is logged
}
// Tell log4php to use our configuration file.
Logger::configure('config.xml');

// Fetch a logger, it will inherit settings from the root logger
$log = Logger::getLogger('myLogger');

// Start logging
$log->trace("My first message.");   // Not logged because TRACE < WARN
$log->debug("My second message."); 
LoggerNDC::push("Some Context");
$log->info("My third message.");    // Not logged because INFO < WARN   
$log->warn("My fourth message.");   // Logged because WARN >= WARN
$log->error("My fifth message.");   // Logged because ERROR >= WARN
testLogFunction();
$log->fatal("My sixth message.");   // Logged because FATAL >= WARN

$foo = new TestLogger();
//$foo->go();



