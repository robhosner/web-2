<?php

global $credentials;

require_once 'cosid_constants.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

/**
 * PHPunit test class for NotificationNotificationReceived
 */
class NotificationThrottlingTest extends PHPUnit_Framework_TestCase
{

  protected function setUp()
  {
  }

  public function testProcessNotificationNoParameters()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      // 'Event'       => 30,
      // 'CounterName' => 'test',
      // 'MSISDN'      => '9992229999'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);
    $errors       = $result->get_errors();

    // Then
    $this->assertInstanceOf('Result', $result);
    $this->assertEquals(3, count($errors));
    $this->assertContains('Missing Event in ThrottlingAlert callback', $errors);
    $this->assertContains('Missing CounterName in ThrottlingAlert callback', $errors);
    $this->assertContains('Missing MSISDN in ThrottlingAlert callback', $errors);
  }

  public function testProcessNotificationMissingEventParam()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      // 'Event'       => 30,
      'CounterName' => 'test',
      'MSISDN'      => '9992229999'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);
    $errors       = $result->get_errors();

    // Then
    $this->assertInstanceOf('Result', $result);
    $this->assertEquals(1, count($errors));
    $this->assertContains('Missing Event in ThrottlingAlert callback', $errors);
  }

  public function testProcessNotificationInvalidEventParam()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      'Event'       => 1, // invalid
      'CounterName' => 'test',
      'MSISDN'      => '9992229999'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);
    $errors       = $result->get_errors();

    // Then
    $this->assertInstanceOf('Result', $result);
    $this->assertEquals(1, count($errors));
    $this->assertContains('Event 1 not handled', $errors);
  }

  public function testMissingCounterNameParam()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      'Event'       => 30,
      // 'CounterName' => 'test',
      'MSISDN'      => '9992229999'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);
    $errors       = $result->get_errors();

    // Then
    $this->assertInstanceOf('Result', $result);
    $this->assertEquals(1, count($errors));
    $this->assertContains('Missing CounterName in ThrottlingAlert callback', $errors);
  }

  public function testMissingMSISDNParam()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      'Event'       => 30,
      'CounterName' => 'test',
      // 'MSISDN'      => '9992229999'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);
    $errors       = $result->get_errors();

    // Then
    $this->assertInstanceOf('Result', $result);
    $this->assertEquals(1, count($errors));
    $this->assertContains('Missing MSISDN in ThrottlingAlert callback', $errors);
  }

  public function testInvalidMSISDNParam()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      'Event'       => 30,
      'CounterName' => 'test',
      'MSISDN'      => 'aaaaaaa'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);
    $errors       = $result->get_errors();

    // Then
    $this->assertInstanceOf('Result', $result);
    $this->assertEquals(1, count($errors));
    $this->assertContains('Invalid MSISDN in ThrottlingAlert callback', $errors);
  }

  public function testSuccessfulThrottlingNotification()
  {
    // Given
    $actionUUID   = time();
    $params       = array(
      'Event'       => 50,
      'CounterName' => 'WPRBLK30',
      'MSISDN'      => '7146752571'
    );

    // If
    $notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert($actionUUID);
    $result       = $notification->processNotification($params);

    // Then
    $this->assertEquals(TRUE, $result->is_success());
  }
}
