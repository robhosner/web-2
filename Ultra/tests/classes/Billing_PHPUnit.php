<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/Billing.php';

class Billing extends PHPUnit_Framework_TestCase
{
  public function setUp()
  {
  }

  public function test_getNextUUID()
  {
    echo 'HOSTNAME: ' . gethostname() . PHP_EOL;

    $redis = new \Ultra\Lib\Util\Redis();
    $billing = new \Ultra\Lib\Util\Redis\Billing($redis);

    $uuid = create_guid();
    $timestamp = time() - 11;
    $billing->addUUID($uuid, $timestamp);

    $nextUUID = $billing->getNextUUID();
    $this->assertEquals($uuid, $nextUUID);
  }

  public function test_addUUID()
  {
    $billing = new \Ultra\Lib\Util\Redis\Billing();
    $redis = new \Ultra\Lib\Util\Redis();

    echo PHP_EOL;

    $uuid = create_guid();
    $timestamp = time() - 11;
    $billing->addUUID($uuid, $timestamp);

    echo "UUID: $uuid" . PHP_EOL;
    echo "TIMESTAMP: $timestamp" . PHP_EOL;

    $returnedTimestamp = $billing->getUUIDTimestamp($uuid);
    echo "RETURNED TIMESTAMP: " . $returnedTimestamp . PHP_EOL;

    $this->assertEquals($timestamp, $returnedTimestamp);
    $this->assertEquals($uuid, $billing->getNextUUID($uuid));

    echo "PRIORITY QUEUE" . PHP_EOL;
    print_r($redis->zrangebyscore('ultra/sortedset/billing', -1,9999999999));

    echo PHP_EOL;

    // future timestamp
    $uuid = create_guid();
    $timestamp = time() + 11;
    $billing->addUUID($uuid, $timestamp);

    $this->assertEquals(NULL, $billing->getNextUUID());

    echo "PRIORITY QUEUE AFTER FUTURE TIMESTAMP and getNextUUID" . PHP_EOL;
    print_r($redis->zrangebyscore('ultra/sortedset/billing', -1,9999999999));

    sleep(14);

    $this->assertEquals($uuid, $billing->getNextUUID());

    echo "PRIORITY QUEUE AFTER SLEEP(14) and getNextUUID" . PHP_EOL;
    print_r($redis->zrangebyscore('ultra/sortedset/billing', -1,9999999999));
  }
}

?>