<?php
namespace Ultra\Dealers\Interfaces;

/**
 * Interface DealersRepository
 * @package Ultra\Dealers\Interfaces
 */
interface DealersRepository
{
  /**
   * get dealer record tblDealerSite by dealer code (Dealercd)
   *
   * @param  string $code
   * @return array dealer data
   */
  public function getDealerFromCode($code);

  /**
   * Get dealer locations.
   *
   * @param array $parameters
   * @return mixed
   */
  public function getLocations(array $parameters);
  
  /**
   * Get all relevant dealer_ids from the parent-child relationship.
   *
   * @param $dealer
   * @return mixed
   */
  public function getParentDealerChildren($dealer);

  /**
   * Get all relevant dealer_ids from the parent-child relationship.
   *
   * @param $master
   * @return mixed
   */
  public function getDealersFromMaster($master);
}