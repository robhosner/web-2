<?php

namespace Ultra\MvneConfig;

/**
 * Main configuration file that relates Ultra plans to MVNE SOCs
 *
 * See
 *  http://wiki.hometowntelecom.com:8090/display/SPEC/Ultra2+Plan+Mappings
 *  http://wiki.hometowntelecom.com:8090/display/ProjectW/Project+W+Plan+Configurations+-+New+Pricing
 */


/**
 * getAccSocsUltraPlanConfig
 *
 * a wrapper and error checker/logger for getAccSocsPlanConfig: given a short Ultra plan name return its ACC SOC configuration
 * @param String plan name (either short 'L19', 'L44' etc or 'NINETEEN', 'FORTY_FOUR' etc)
 * @param Integer customer ID
 * @param String subplan BRAND_OPTION_SUBPLAN
 * @returns Array or NULL on error
 */
function getAccSocsUltraPlanConfig($plan, $customer_id = NULL, $subplan = NULL)
{
  // validate plan name
  $plans = cos_id_plan_map();
  if (empty($plans[$plan]))
    return \logError("invalid plan name $plan");

  // convert short plan name to long if necessary
  $longName = get_plan_name_from_short_name($plan);
  $planName = $longName ? $longName : $plan;

  // get plan configuration
  $config = getAccSocsPlanConfig($planName);
  if (empty($config))
  {
    \logError("Cannot find plan $planName configuration");
    return NULL;
  }

  // future use: calculate individual subscriber medium throttle value
  // see http://wiki.hometowntelecom.com:8090/display/SPEC/Arch+Specs+for+Stair-Step+Throttle+and+RESETLIMIT
  # if ($customer_id && isset($config['a_data_blk128']))
  #  $config['a_data_blk128'] = getMediumThrottleLimit($customer_id);

  return $subplan ? adjustSubplanSocsValues($config, $planName, $subplan) : $config;
}

/**
 * getAccSocsPlanConfig
 *
 * return SOC configuration for a given plan
 * note: 'STANDBY' is the empty plan (Provisioned)
 * @param String long plan name ('NINETEEN', 'TWENTY_NINE', 'THIRTY_NINE', 'FORTY_NINE', 'FIFTY_NINE' etc)
 * @return Array of SOC names and values (e.g [b_voice] => 8000, [b_sms] => 9000, [a_data_blk_plan] => 750...)
 */
function getAccSocsPlanConfig($plan)
{
  $accSocsPlanConfig = array();
  // $accSocsPlanConfigObjects = json_decode(\Ultra\UltraConfig\find_config("amdocs/plans/Ultra/$plan"));

  $planConfig = \PlanConfig::Instance();
  $accSocsPlanConfigObjects = json_decode($planConfig->find_config($plan));

  if ($accSocsPlanConfigObjects && is_array($accSocsPlanConfigObjects))
    foreach($accSocsPlanConfigObjects as $config)
      foreach($config as $member => $data)
        $accSocsPlanConfig[$member] = $data;

  return $accSocsPlanConfig;
}

/**
 * isLimitedDataPlan
 *
 * determine if the given Ultra plan has limited (blocked) or unlimited (throttled) data
 * @param String short plan name (e.g. 'L24')
 * @return FALSE if unlimited data plan or TRUE otherwise
 */
function isLimitedDataPlan($plan)
{
  if ( ! $config = getAccSocsUltraPlanConfig($plan))
    return TRUE;

  // scan all plan SOCs: assume default block and stop at the first throttle SOC found
  $throttle = getAccDataSocsByType('throttle');
  foreach ($config as $soc => $value)
    if (in_array($soc, $throttle))
      return FALSE;
  return TRUE;
}

/**
 * getPlanBaseDataSoc
 *
 * given a short Ultra plan name return its base data SOC which corresponds to 'Base Data Usage'
 * @param String (L19, L34 etc)
 * @returns String base SOC name or NULL if not found
 */
function getPlanBaseDataSoc($plan)
{
  // get plan configuration and known base SOCs
  if ( ! $config = getAccSocsUltraPlanConfig($plan))
    return NULL;
  $base = getAccDataSocsByType('base');

  // compare plan to SOCs: stop at the first found since we are allowed to have only one base SOC in plan
  foreach ($config as $soc => $value)
    if (in_array($soc, $base))
    {
      $tmo = getAccSocDefinitionField($soc, 'tmo_service_name');
      return empty($tmo) ? $soc : $tmo;
    }

  // not found: this should happen (SOCs are mis-configured)
  \logError("Faild to locate base SOC for plan $plan");
  return NULL;
}

/**
 * getAccDataSocsByType
 *
 * returns a list of ACC data SOCs with the specific property
 * @param String SOC type ('base', 'throttle')
 * @returns Array of Ultra SOC names or NULL on error
 */
function getAccDataSocsByType($type)
{
  // SOC properties map
  $socs = array(
    // flags:                      base   throttle  plan
    'a_data_blk1536'      => array(FALSE,   FALSE,  FALSE),
    'a_data_blk1024'      => array(FALSE,   FALSE,  FALSE),
    'a_data_blk'          => array(FALSE,   FALSE,  FALSE),
    'a_data_blk1536_plan' => array(FALSE,   FALSE,  TRUE),
    'a_data_blk1024_plan' => array(FALSE,   FALSE,  TRUE),
    'a_data_blk_plan'     => array(FALSE,   FALSE,  TRUE),
    'a_data_blk128'       => array(FALSE,   TRUE,   TRUE),
    'a_data_blk256'       => array(FALSE,   TRUE,   TRUE),
    'a_data_blk256_v2'    => array(FALSE,   TRUE,   TRUE),
    'b_data_blk'          => array(TRUE,    FALSE,  TRUE),
    'b_data_thr64'        => array(TRUE,    TRUE,   TRUE),
    'b_data_thr128'       => array(TRUE,    TRUE,   TRUE),
    'b_data_thr256'       => array(TRUE,    TRUE,   TRUE)
  );

  // properties index map
  $map = array(
    'base'              => 0,   // is this a required single plan base SOC?
    'throttle'          => 1,   // is this a throttle SOC (vs block)?
    'plan'              => 2    // is SOC included in plan (vs bolt on)?
  );

  // identify index for given type
  if ( ! isset($map[$type]))
  {
    \logError("Uknown SOC type $type");
    return NULL;
  }
  $index = $map[$type];

  // gather all SOCs of given type
  $result = array();
  foreach ($socs as $soc => $flags)
    if ($flags[$index])
      $result[] = $soc;

  return $result;
}

/**
 * getACCWholesaleValue
 *
 * Input: wholesale plan
 * Output: Plan Id
 *
 * @retuns integer
 */
function getACCWholesaleValue( $wholesalePlan )
{
  $wholesalePlans = getWholesalePlans();

  foreach( $wholesalePlans as $plan )
  {
    $configWholesalePlan = getAccSocDefinition( $plan );

    if ( in_array( $wholesalePlan , $configWholesalePlan ) )
      return $configWholesalePlan[0];
  }

  return NULL;
}

/**
 * getACCWholesaleValueByPlanName
 *
 * Input: Ultra Plan Name
 * Output: 32 or 111
 *
 * @retuns integer
 */
function getACCWholesaleValueByPlanName( $planName )
{
  $wholesalePlan = \Ultra\UltraConfig\getUltraPlanConfigurationItem( $planName , 'wholesale' );

  if ( empty( $wholesalePlan ) )
    return NULL;

  return getACCWholesaleValue( $wholesalePlan );
}

/**
 * getAccSocDefinition
 *
 * return ACC/TMO SOC info
 * @param String Ultra service name (e.g. 'b_data_blk64')
 * @retuns Array SOC details
 */
function getAccSocDefinition($plan_soc_name)
{
  $planConfig = \PlanConfig::Instance();
  return $planConfig->getAccSocDefinition($plan_soc_name);

  // old
  $definition = trim(\Ultra\UltraConfig\find_config('amdocs/definition/plan/'.$plan_soc_name));
  $result = explode(' ', preg_replace('/\s+/', ' ', $definition), count(getAccSocsDescriptorList()));
  return $result;
}

/**
 * a wrapper for getAccSocDefinition: return a requested field value from a network SOC definition
 * @param String Ultra service name (e.g. 'b_data_blk64')
 * @param String field name (e.g. 'plan_id')
 * @returns String SOC field value
 */
function getAccSocDefinitionField($plan_soc_name, $field)
{
  $value = '';
  $definition = \Ultra\MvneConfig\getAccSocDefinition($plan_soc_name);

  $descriptors = getAccSocsDescriptorList();
  for ($j = 0, $k = count($descriptors); $j < $k; $j++)
    if ($descriptors[$j] == $field)
      $value = $definition[$j];

  return $value;
}

/**
 * getAccSocsDescriptorList
 *
 * retuns a list of ACC SOC descriptors to use with SOC definitions
 * @returns Array of strings
 */
function getAccSocsDescriptorList()
{
  $accSocsDescriptorList = preg_replace('/\s+/', ' ', \Ultra\UltraConfig\find_config('amdocs/definition/plandescriptor'));
  return explode(' ', $accSocsDescriptorList);
}

/**
 * getAccSocsDefinitionsList
 *
 * return a list of ACC SOCs
 * @returns Array
 */
function getAccSocsDefinitionsList()
{
  $planConfig = \PlanConfig::Instance();
  return $planConfig->getAccSocsDefinitionsList();

  // old
  return explode(' ', \Ultra\UltraConfig\find_config('amdocs/definition/planlist'));
}

/**
 * getAccSocsDefinitions
 *
 * returns info for all know ACC SOCs
 * @returns Array of Arrays
 */
function getAccSocsDefinitions()
{
  $accSocsDefinition           = array();
  $accSocsDefinitionByUltraSoc = array();
  $ultraSocsByPlanId           = array();

  $descriptorList = getAccSocsDescriptorList();
  $definitionList = getAccSocsDefinitionsList();

  for ($i = 0, $n = count($definitionList); $i < $n; $i++)
  {
    $definition = getAccSocDefinition( $definitionList[ $i ] );
    for ($j = 0, $k = count($descriptorList); $j < $k; $j++) 
      $accSocsDefinition[ $i ][ $descriptorList[ $j ] ] = $definition[ $j ];

    $accSocsDefinitionByUltraSoc[ $definitionList[ $i ] ] = $accSocsDefinition[ $i ];
    $ultraSocsByPlanId[ $accSocsDefinition[ $i ]['plan_id'] ] = $definitionList[ $i ];
  }

  return array($accSocsDefinition, $accSocsDefinitionByUltraSoc, $ultraSocsByPlanId);
}

/**
 * getMediumThrottleLimit
 *
 * return value for a_data_blk128 SOC
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Arch+Specs+for+Stair-Step+Throttle+and+RESETLIMIT
 * @param Integer customer ID: unused, for future extension
 * @returns SOC value in MB
 */
function getMediumThrottleLimit($customer)
{
  $value = 100000;
  dlog('', "value: $value");
  return $value;
}

/**
 * parseEmuSwitchAddOnFeatureInfoList
 *
 * modifies add on feature info list for emu switch
 * @see https://issues.hometowntelecom.com:8443/browse/API-40
 * @param  Array  $addOnFeatureInfoList data to modify
 * @return Array  $return modified $addOnFeatureInfoList input
 */
function parseEmuSwitchAddOnFeatureInfoList($addOnFeatureInfoList)
{
  if (is_array($addOnFeatureInfoList))
  {
    // hide these SOCs for this API
    $hiddenSOCs = array('12911', '12905');
    $return     = array();

    foreach ($addOnFeatureInfoList as $addOnFeature)
    {
      if ( ! empty($addOnFeature->FeatureID) && ! in_array($addOnFeature->FeatureID, $hiddenSOCs))
        $return[] = $addOnFeature;
    }

    return $return;
  }

  return $addOnFeatureInfoList;
}

/**
 * parseEmuSwitchBalanceValueList
 *
 * modifies balance value list for emu switch based on partner namepsace invoking
 * @see https://issues.hometowntelecom.com:8443/browse/API-40
 * @param  Array  $balanceValueList data to modify
 * @param  String $partnerType which partner namespace is invoking
 * @return Array  $return modified $balanceValueList input
 */
function parseEmuSwitchBalanceValueList($balanceValueList, $partnerType)
{
  if (is_array($balanceValueList) && $partnerType)
  {
    $return    = array();
    $usedTypes = array();

    foreach ($balanceValueList as $value)
    {
      // pass over duplicate Base Data Usage
      if ( ! empty($value->UltraType) && $value->Type != 'Base Data Usage' && ! in_array($value->UltraType, $usedTypes))
      {
        $usedTypes[] = $value->UltraType;

        if ($partnerType == 'customercare')
        {
          // new name mapping
          switch ($value->UltraType)
          {
            case 'WPRBLK40 Used':
              $value->UltraType = 'Base Data Usage Used';
              break;
            case 'WPRBLK40 Limit':
              $value->UltraType = 'Base Data Usage Limit';
              break;
            case 'WPRBLK20 Used':
              $value->UltraType = 'Data Add-on Used';
              break;
            case 'WPRBLK20 Limit':
              $value->UltraType = 'Data Add-on Total';
              break;
          }
        }
        elseif ($partnerType == 'internal')
        {
          // new name mapping
          switch ($value->UltraType)
          {
            case 'WPRBLK40 Used':
              $value->UltraType = 'Base Data Usage Used';
              break;
            case 'WPRBLK40 Limit':
              $value->UltraType = 'Base Data Usage Limit';
              break;
            case 'WPRBLK20 Used':
              $value->UltraType = '7007 Used';
              $value->Name = 'Data Add-on Used';
              break;
            case 'WPRBLK20 Limit':
              $value->UltraType = '7007 Limit';
              $value->Name = 'Data Add-on Total';
              break;
          }
        }

        $return[] = $value;
      }
    }

    return $return;
  }

  return $balanceValueList;
}

/**
 * getNetworkSocs
 *
 * Returns list of all Network SOCs
 *
 * @return array
 */
function getNetworkSocs()
{
  return ['n_lte','n_barccf','n_noroam','n_droam','n_boic','n_roam_latam','n_roam_intl', 'n-roam-smpglb'];
}

/**
 * isNetworkSoc
 *
 * Checks if the given SOC is a Network SOC
 *
 * @return boolean
 */
function isNetworkSoc( $soc )
{
  return ! ! in_array( $soc , getNetworkSocs() );
}

/**
 * getWholesalePlans
 *
 * Returns list of all Wholesale ID SOCs
 *
 * @return array
 */
function getWholesalePlans()
{
  return ['w_primary','w_secondary','w_uv_primary','w_mint','w_mrc'];
}

/**
 * isWholesalePlanSOC
 *
 * Checks if the given SOC is a Wholesale ID SOC
 *
 * @return boolean
 */
function isWholesalePlanSOC( $soc )
{
  return ! ! in_array( $soc , getWholesalePlans() );
}

/**
 * getRetailPlans
 *
 * Returns list of all Retail ID SOCs
 *
 * @return array
 */
function getRetailPlans()
{
  return [
    'r_unlimited',
    'r_limited',
    'r_univision',
    'r_mint_s_01',
    'r_mint_m_01',
    'r_mint_l_01',
    'r_mint_s_03',
    'r_mint_m_03',
    'r_mint_l_03',
    'r_mint_s_06',
    'r_mint_m_06',
    'r_mint_l_06',
    'r_mint_s_12',
    'r_mint_m_12',
    'r_mint_l_12',
    'r_mrc'
  ];
}

/**
 * getDefaultRetailPlanName
 *
 * return default retail plan name for a given brand ID
 * @param Integer brand ID
 * @return String retain plan name
 */
function getDefaultRetailPlanName($id)
{
  $brandName = \Ultra\UltraConfig\getShortNameFromBrandId($id);

  switch ($brandName)
  {
    case 'MINT':
      return 'r_limited';

    case 'UNIVISION':
      return 'r_univision';

    default:
      return 'r_limited';
  }
}

/**
 * isRetailPlanSOC
 *
 * Checks if the given SOC is a Retail ID SOC
 *
 * @return boolean
 */
function isRetailPlanSOC( $soc )
{
  return ! ! in_array( $soc , getRetailPlans() );
}

/**
 * isVoicemailSoc
 *
 * Checks if the given SOC is a Voicemail SOC
 *
 * @return boolean
 */
function isVoicemailSOC( $soc )
{
  return in_array($soc, [
    'v_english',
    'v_english_vvm',
    'v_spanish',
    'v_spanish_vvm'
  ]);
}

/**
 * isMintRetailPlanSOC
 *
 * Checks if the given SOC is a Mint Retail Plan ID
 *
 * @return boolean
 */
function isMintRetailPlanSOC( $retailPlanId )
{
  return in_array($retailPlanId, [
    12008,
    12011,
    12014,
    12017,
    12009,
    12012,
    12015,
    12018,
    12010,
    12013,
    12016,
    12019
  ]);
}

/**
 * isMintDataSOC
 *
 * Checks if the given SOC is a Mint Data Soc ID
 *
 * @return boolean
 */
function isMintDataSOC( $dataSocId )
{
  return in_array($dataSocId, [
    12913,
    12914,
    'A-DATA-MINT-1',
    'A-DATA-MINT-3',
    'A-DATA-MINT|1',
    'A-DATA-MINT|3'
  ]);
}

/**
 * given a feature type and speed, returns proper feature id
 *
 * @param string $feature ['plan','block']
 * @param string $speed
 *
 * @return int ex 12909
 */
function getThrottleSpeedSocId($feature, $speed)
{
  $map  = throttlingSpeedMap();
  return (isset($map[$feature][$speed])) ? $map[$feature][$speed]['feature_id'] : null;
}

/**
 * given a feature type and speed, returns feature name
 *
 * @param string $feature ['plan','block']
 * @param string $speed
 *
 * @return string ex WPRBLK20
 */
function getThrottleSpeedSocType($feature, $speed)
{
  $map = throttlingSpeedMap();
  return (isset($map[$feature][$speed])) ? $map[$feature][$speed]['name'] : null;
}

/**
 * given a feature id, returns feature type
 *
 * @param int $socId
 *
 * @return string ['plan','block']
 */
function getThrottleSpeedFeatureFromSocId($socId)
{
  $map  = throttlingSpeedMap();
  foreach ($map as $feature => $featureItem)
  {
    foreach ($featureItem as $speedItem)
    {
      if ($speedItem['feature_id'] == $socId)
        return $feature;
    }
  }

  return null;
}

/**
 * given a feature name, returns feature type
 *
 * @param int $type ex. WPRBLK20
 *
 * @return string ['plan','block']
 */
function getThrottleSpeedFeatureFromType($type)
{
  $map  = throttlingSpeedMap();
  foreach ($map as $feature => $featureItem)
  {
    foreach ($featureItem as $speedItem)
    {
      if ($speedItem['name'] == $type)
        return $feature;
    }
  }

  return null;
}

/**
 * given feature id, return feature name
 *
 * @param int $socId
 *
 * @return string
 */
function getThrottleSpeedTypeFromSocId($socId)
{
  $map  = throttlingSpeedMap();
  foreach ($map as $feature)
  {
    foreach ($feature as $speed => $item)
    {
      if ($item['feature_id'] == $socId)
        return $item['name'];
    }
  }

  return null;
}

/**
 * check if a plan should be able to update their throttle speed
 *
 * @param  string plan short name
 *
 * @return bool
 */
function planCanUpdateThrottleSpeed($plan)
{
  return ! (in_array($plan, ['UV30','UVTHIRTY']));
}


/**
 * a map of throttling features
 *
 * @return array
 */
function throttlingSpeedMap()
{
  return [
    'plan' => [
      'full'  => ['feature_id' => 12909, 'name' => 'WPRBLK20',  'config' => 'a_data_blk_plan'],
      '1536'  => ['feature_id' => 12917, 'name' => 'WPRBLK33S', 'config' => 'a_data_blk1536_plan'],
      '1024'  => ['feature_id' => 12919, 'name' => 'WPRBLK35S', 'config' => 'a_data_blk1024_plan']
    ],
    'block' => [
      'full'  => ['feature_id' => 12907, 'name' => '7007',      'config' => 'a_data_blk'],
      '1536'  => ['feature_id' => 12918, 'name' => 'WPRBLK34S', 'config' => 'a_data_blk1536'],
      '1024'  => ['feature_id' => 12920, 'name' => 'WPRBLK36S', 'config' => 'a_data_blk1024']
    ]
  ];
}

/**
 * given a customers add on features, detects their throttle speed
 *
 * @param array
 * @param string throttle speed
 */
function detectThrottleSpeed($addOnFeatureInfo)
{
  $map = throttlingSpeedMap();

  foreach ($addOnFeatureInfo as $addOnFeature)
  {
    if (!empty($addOnFeature->FeatureID))
    {
      // just check by plan
      foreach ($map['plan'] as $speed => $item)
        if ($addOnFeature->FeatureID == $item['feature_id'])
          return $speed;
    }
  }

  return null;
}

function detectRoamingSoc($addOnFeatureInfo)
{
  foreach ($addOnFeatureInfo as $addOnFeature)
  {
    if (!empty($addOnFeature->FeatureID))
    {
      if (\Ultra\UltraConfig\isRoamingSoc($addOnFeature->FeatureID))
        return $addOnFeature->FeatureID;
    }
  }

  return null;
}

function detectRoamingWalletSoc($addOnFeatureInfo)
{
  foreach ($addOnFeatureInfo as $addOnFeature)
  {
    if (!empty($addOnFeature->FeatureID))
    {
      if (\Ultra\UltraConfig\isRoamingWalletSoc($addOnFeature->FeatureID))
        return $addOnFeature->FeatureID;
    }
  }

  return null;
}

function detectThrottleSpeedFromBalanceValues($balanceValues)
{
  $map = throttlingSpeedMap();

  foreach ($balanceValues as $balanceValue)
  {
    if (!empty($balanceValue->Type))
    {
      // just check by plan
      foreach ($map['plan'] as $speed => $item)
        if ($balanceValue->Type == $item['name'])
          return $speed;
    }
  }

  return null;
}

function throttleValuesFromBalanceValues($balanceValues)
{
  $values = null;

  $map = throttlingSpeedMap();

  foreach ($balanceValues as $key => $val)
  {
    foreach ($map as $type)
    {
      foreach ($type as $speed => $item)
        if ($key == $item['name'])
        {
          $values[$key] = [];
          $values[$key]['Used']  = $val["$key Used"];
          $values[$key]['Limit'] = $val["$key Limit"];
        }
    }
  }

  return $values;
}

/**
 * adjust a plan configuration array for throttle speed
 *
 * @param array by reference
 * @param string throttle speed
 */
function adjustPlanConfigForThrottle(&$accSocsPlanConfig, $throttleSpeed)
{
  if (!$throttleSpeed)
    return;

  $map = throttlingSpeedMap();

  foreach ($accSocsPlanConfig as $feature => $val)
  {
    if (in_array($feature, ['a_data_blk_plan','a_data_blk1536_plan','a_data_blk1024_plan']))
    {
      unset($accSocsPlanConfig[$feature]);
      $accSocsPlanConfig[$map['plan'][$throttleSpeed]['config']] = $val;
    }
  }
}

/**
 * given a data soc, finds equivalent soc for given speed
 *
 * @param string
 * @param string throttle speed
 */
function adjustDataSocForThrottle($dataSoc, $throttleSpeed)
{
  if (!$throttleSpeed)
    return;

  $map = throttlingSpeedMap();

  foreach ($map as $feature => $featureItem)
  {
    foreach ($featureItem as $speedItem)
    {
      if ($dataSoc == $speedItem['config'])
        return $map[$feature][$throttleSpeed]['config'];
    }
  }

  return $dataSoc;
}

/**
 * adjustSubplanSocsValues
 *
 * cahnge SOC values based on specific subplan requirements
 * @see http://wiki.hometowntelecom.com:8090/display/ProjectW/Project+W+Plan+Configurations+-+New+Pricing
 * @param Array plan config
 * @return Array adjusted plan config
 */
function adjustSubplanSocsValues($config, $plan, $subplan)
{
  // re-configure SOCs according to plan/subplan and the following replacement data structure:
  // PLAN => array(SUBPLAN_1 => array(original SOC => array(replacement SOC => replacement value), original SOC => array(replacement SOC => replacement value)), SUBPLAN_2 => ...)
  $replacements = array(
    'UVTHIRTY'      => array('A' => array('b_voice' => array('b_voice' => 10000), 'a_voicesmswallet_ir' => array('a_voicesmswallet_ir' => 1100))),
    'UVTHIRTYFIVE'  => array('A' => array('b_voice' => array('b_voice' => 10000), 'a_voicesmswallet_ir' => array('a_voicesmswallet_ir' => 1350))),
    'UVFORTYFIVE'   => array('A' => array('b_voice' => array('b_voice' => 10000), 'a_voicesmswallet_ir' => array('a_voicesmswallet_ir' =>  750))),
    'UVFIFTYFIVE'   => array('A' => array('b_voice' => array('b_voice' => 10000), 'a_voicesmswallet_ir' => array('a_voicesmswallet_ir' => 1275), 'b_data_thr64' => array('b_data_thr128' => 0)))
  );

  if ( ! empty($replacements[$plan][$subplan]))
  {
    foreach ($replacements[$plan][$subplan] as $original => $replacement)
    {
      unset($config[$original]);
      $config[key($replacement)] = $replacement[key($replacement)];
    }
    logInfo(sprintf('adjusted plan %s subplan %s config: %s', $plan, $subplan, json_encode($config)));
  }

  return $config;
}

/**
 * getRetailPlanNameFromShortPlanName
 *
 * given short plan name (L19,L29) returns retail plan name
 * @param String $shortPlanName short plan name
 * @return String retail plan name
 */
function getRetailPlanNameFromShortPlanName($shortPlanName)
{
  $accSocsPlanConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig( $shortPlanName );

  $retailPlans = \Ultra\MvneConfig\getRetailPlans();

  $retailPlanName = NULL;

  foreach ($retailPlans as $retailPlan)
    if (isset($accSocsPlanConfig[$retailPlan]))
      $retailPlanName = $retailPlan;

  return $retailPlanName;
}

/**
 * getDataAddOnForMakeitsoUpgradePlan
 *
 * Examples:
 *  - A-DATA-BLK|250
 *  - A-DATA-MINT|1
 *  - A-DATA-MINT|3
 *
 * @return String
 */
function getDataAddOnForMakeitsoUpgradePlan( $customer , $data_recharge )
{
  if ( $customer->BRAND_ID == 3 )
  {
    // MINT
    return DEFAULT_ACC_MINT_DATA_ADD_ON . '|' . substr($data_recharge['MB'], 0, 1) ;
  }
  else
    return DEFAULT_ACC_DATA_ADD_ON . '|' . $data_recharge['MB'];
}

/**
 * getMintDataSocFeatureID
 *
 * Maps to 'a_data_mint_\d'
 *
 * @return String
 */
function getMintDataSocFeatureID( $tier )
{
  $tierMap = [
    1024 => 1,
    3072 => 3
  ];

  if ( ! empty( $tierMap[$tier] ) )
    $tier = $tierMap[$tier];

  return \Ultra\MvneConfig\getAccSocDefinitionField( 'a_data_mint_'.$tier , 'plan_id' );
}

