<?php
namespace Ultra\Plans;

require_once 'classes/Flex.php';

use Ultra\Lib\Flex as untestableFlex;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Mvne\Adapter;

class Flex
{
  private $config;
  private $familyAPI;
  private $accountsRepo;
  private $customerRepo;
  private $mwAdapter;

  public function __construct(
    Configuration $config,
    FamilyAPI $familyAPI,
    AccountsRepository $accountsRepo,
    CustomerRepository $customerRepo,
    Adapter $mwAdapter
  ) {
    $this->config = $config;
    $this->familyAPI = $familyAPI;
    $this->accountsRepo = $accountsRepo;
    $this->customerRepo = $customerRepo;
    $this->mwAdapter = $mwAdapter;
  }

  public function queueAddFamilyMembertoBanMISO($customerId, $dataAmount, $skipActiveCheck=false)
  {
    // make sure they are active
    if (!$skipActiveCheck) {
      $customer = $this->customerRepo->getCustomerById($customerId, ['plan_state']);
      if (!$customer) {
        throw new \Exception('Failed to lookup CUSTOMER record where customer_id = ' . $customerId);
      }

      if ($customer->plan_state != STATE_ACTIVE) {
        \logDebug('Skipping inactive customer ' . $customerId);
        return;
      }
    }

    // lookup ACCOUNTS record for cos_id
    $account = $this->accountsRepo->getAccountFromCustomerId($customerId, ['cos_id']);
    if (!$account) {
      throw new \Exception('Failed to lookup ACCOUNTS record where customer_id = ' . $customerId);
    }

    // queue MISO
    $mvneResult = $this->mwAdapter->mvneMakeitsoAddToBan(
      $customerId,
      $this->config->getPlanFromCosId($account->cos_id),
      $dataAmount
    );

    if (!$mvneResult['success']) {
      // connect back to the default DB
      teldata_change_db();

      throw new \Exception($mvneResult['errors'][0]);
    }

    // connect back to the default DB
    teldata_change_db();
  }

  /**
   * Adds other family members to the BAN, when a member purchases a data bolt-on for the first time
   *
   * @param  int
   * @param  int
   * @return \Result
   */
  public function queueFamilyAddToBanMISOs($customerId, $dataAmount, $skipActiveCheck=false)
  {
    $result = new \Result();

    try {
      $familyResult = $this->familyAPI->getFamilyByCustomerID($customerId);
      if (!$familyResult->is_success()) {
        throw new \Exception('Get family API call failed');
      }

      $members = $familyResult->data_array['members'];
      foreach ($members as $member) {
        // skip customer that bought bolt on
        if ($member['customerId'] == $customerId) {
          continue;
        }

        $this->queueAddFamilyMembertoBanMISO($member['customerId'], $dataAmount, $skipActiveCheck);
      }

      $result->succeed();

    } catch (\Exception $e) {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  public function applyCustomerOrder($customerId, $withBoltOns=true)
  {
    return untestableFlex::applyCustomerOrder($customerId, $withBoltOns);
  }
}
