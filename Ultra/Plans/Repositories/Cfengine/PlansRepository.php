<?php
namespace Ultra\Plans\Repositories\Cfengine;

use PlanConfig;
use Ultra\Configuration\Configuration;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Plans\Plan;
use Ultra\Plans\Interfaces\PlansRepository as PlansRepositoryInterface;

require_once 'classes/PlanConfig.php';
require_once 'vendor/autoload.php';

/**
 * Class PlansRepository
 * @package Ultra\Plans\Repositories\Cfengine
 */
class PlansRepository implements PlansRepositoryInterface
{
  /**
   * @var array
   */
  private $plans = [];

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var array
   */
  private $mintPlans = [
    'MINT_ONE_MONTH_L',
    'MINT_ONE_MONTH_M',
    'MINT_ONE_MONTH_S',
    'MINT_THREE_MONTHS_L',
    'MINT_THREE_MONTHS_M',
    'MINT_THREE_MONTHS_S',
    'MINT_SIX_MONTHS_L',
    'MINT_SIX_MONTHS_M',
    'MINT_SIX_MONTHS_S',
    'MINT_TWELVE_MONTHS_L',
    'MINT_TWELVE_MONTHS_M',
    'MINT_TWELVE_MONTHS_S',
  ];

  /**
   * @var array
   */
  private $univisionPlans = [
    'UVSNINETEEN',
    'UVSTWENTYFOUR',
    'UVSTWENTYNINE',
    'UVSTHIRTYFOUR',
    'UVSTHIRTYNINE',
    'UVSFORTYFOUR',
    'UVSFORTYFIVE',
    'UVSFIFTYFOUR',
  ];

  /**
   * @var array
   */
  private $ultraPlans = [
    'NINETEEN',
    'TWENTY_FOUR',
    'STWENTY_NINE',
    'MULTI_TWENTY_NINE',
    'THIRTY_FOUR',
    'STHIRTY_NINE',
    'SFORTY_FOUR',
    'SFIFTY_FOUR',
  ];

  /**
   * @var array
   */
  private $planItems = [
    'planId'               => 'aka',
    'boltOnTypes'          => 'bolt_on_types',
    'boltOns'              => 'bolt_ons',
    'brandId'              => 'brand_id',
    'cosId'                => 'cos_id',
    'cost'                 => 'cost',
    'cycleData'            => 'cycle_data',
    'intlMinutes'          => 'intl_minutes',
    'months'               => 'months',
    'name'                 => 'name',
    'offerId'              => 'offer_id',
    'talkMinutes'          => 'talk_minutes',
    'unlimitedIntlMinutes' => 'unlimited_intl_minutes',
    'wholesale'            => 'wholesale',
    'zeroUsedMinutes'      => 'zero_used_minutes',
    'dataRecharge'         => 'data_recharge',
    'monthly'              => 'monthly',
    'active'               => 'active'
  ];

  /**
   * @var array
   */
  private $brandIds = [
    'ULTRA'     => 1,
    'UNIVISION' => 2,
    'MINT'      => 3
  ];

  /**
   * @var array
   */
  private $planInfo = [];

  /**
   * @var PlanConfig
   */
  private $planConfig;

  /**
   * @var bool
   */
  private $showRefreshPlans;

  /**
   * PlansRepository constructor.
   * @param Configuration $configuration
   * @param FeatureFlagsClientWrapper $featureFlagsClientWrapper
   */
  public function __construct(Configuration $configuration, FeatureFlagsClientWrapper $featureFlagsClientWrapper)
  {
    $this->configuration = $configuration;
    $this->planConfig = PlanConfig::Instance();
    $this->showRefreshPlans = $featureFlagsClientWrapper->showRefreshPlans();
    $this->mintPlans      = $this->planConfig->getLongNames(3);
    $this->univisionPlans = $this->planConfig->getLongNames(2);
    $this->ultraPlans     = $this->filterRefreshPlans($this->planConfig->getLongNames(1));
  }

  /**
   * Get brand plans from cfengine.
   *
   * @param $brand
   * @return array|bool
   */
  public function getAllPlansByBrand($brand)
  {
    $brand = $this->configuration->getShortNameFromBrandId($this->brandIds[$brand]);

    if (empty($brand))
    {
      return false;
    }

    if ($brand == 'MINT')
    {
      return $this->getPlans($this->mintPlans);
    }
    else if ($brand == 'UNIVISION')
    {
      return $this->getPlans($this->univisionPlans);
    }
    else
    {
      return $this->getPlans($this->ultraPlans);
    }
  }

  /**
   * Get brand plan by planId from cfengine.
   *
   * @param $brand
   * @param $planId
   * @return bool|Plan
   * @throws \Exception
   */
  public function getPlanByBrandAndPlanId($brand, $planId)
  {
    $brand = $this->configuration->getShortNameFromBrandId($this->brandIds[$brand]);
    $planKey = $this->configuration->getPlanNameFromShortName($planId);

    $thisPlanConfig = $this->planConfig->getPlanConfig($planKey);

    if (empty($brand) && empty($planKey))
    {
      return false;
    }

    if (empty($thisPlanConfig['brand_id'])) {
      $thisPlanConfig['brand_id'] = null;
    }

    if ($this->brandIds[$brand] != $thisPlanConfig['brand_id'])
    {
      throw new \Exception("The brand $brand does not have a $planId plan.");
    }

    foreach ($this->planItems as $key => $planItem)
    {
      if (isset($thisPlanConfig[$planItem])) {
        $this->planInfo[$key] = $thisPlanConfig[$planItem];
      }
    }

    $this->getVariousPlanData($planKey);

    return new Plan($this->planInfo);
  }

  /**
   * Get list of brands plans.
   *
   * @param array $brandPlans
   * @return array
   */
  private function getPlans(array $brandPlans)
  {
    foreach ($brandPlans as $plan)
    {
      $this->planInfo = [];
      $thisPlanConfig = $this->planConfig->getPlanConfig($plan);

      foreach ($this->planItems as $key => $planItem)
      {
        if (isset($thisPlanConfig[$planItem])) {
          $this->planInfo[$key] = $thisPlanConfig[$planItem];
        }
      }

      $this->getVariousPlanData($plan);

      $this->plans[] = new Plan($this->planInfo);
    }

    return $this->plans;
  }

  private function getVariousPlanData($planKey)
  {
    $dataInfo = [];
    $unlimitedAmount = 30000;
    $notUnlimitedPlans = [
      'L19',
      'UVS19',
      'UV20',
      'L24',
      'UVS24',
      'L29',
      'M29',
    ];
    $bonusMonthPlans = [
      'B49' => 1,
    ];

    if ($planKey != 'MULTI_TWENTY_NINE') {
      $info = json_decode($this->planConfig->find_config($planKey));
      foreach ($info as $item) {
        foreach ($item as $key => $value) {
          $dataInfo[$key] = $value;
        }
      }
    }

    $this->planInfo['unlimited'] = in_array($this->planInfo['planId'], $notUnlimitedPlans) ? false : true;
    $this->planInfo['bonusMonths'] = array_key_exists($this->planInfo['planId'], $bonusMonthPlans) ? $bonusMonthPlans[$this->planInfo['planId']] : 0;

    if (isset($dataInfo['a_data_blk_plan']))
      $this->planInfo['dataLte'] = $dataInfo['a_data_blk_plan'];
    else
      $this->planInfo['dataLte'] = (!empty($this->planInfo['cycleData'])) ? $this->planInfo['cycleData'] * 1024 : 0;

    $this->planInfo['dataLte'] = $this->planInfo['planId'] == 'M29' ? 1024 : $this->planInfo['dataLte'];
    $this->planInfo['dataLte'] = $this->planInfo['planId'] == 'M01T' ? $this->planInfo['cycleData'] : $this->planInfo['dataLte'];
    $this->planInfo['unlimitedLTE'] = false;
    $this->planInfo['unlimited3G'] = false;

    if (isset($dataInfo['a_data_blk_plan']) && $dataInfo['a_data_blk_plan'] >= $unlimitedAmount) {
      $this->planInfo['unlimitedLTE'] = true;
    } else if (isset($dataInfo['a_data_blk256_v2']) && $dataInfo['a_data_blk256_v2'] >= $unlimitedAmount) {
      $this->planInfo['unlimited3G'] = true;
    }

    $this->planInfo['data3G'] = isset($dataInfo['a_data_blk256_v2']) ? $dataInfo['a_data_blk256_v2'] : 0;
    $this->planInfo['roamingCredit'] = isset($dataInfo['a_voicesmswallet_ir']) ? $dataInfo['a_voicesmswallet_ir'] : 0;

    return $this->planInfo;
  }

  /**
   * Remove refresh plans from the plan configuration if the show reflex plan feature flag is off.
   *
   * @param array $plans
   * @return array
   */
  private function filterRefreshPlans(array $plans)
  {
    $refreshPlans = [
      'NINETEEN',
      'ANINETEEN',
      'ATWENTY_NINE',
      'ATHIRTY_NINE',
      'AFORTY_NINE',
      'BNINETEEN',
      'BTWENTY_NINE',
      'BTHIRTY_NINE',
      'BFORTY_NINE',
      'STWENTY_THREE'
    ];

    if (!$this->showRefreshPlans) {
      $count = count($plans);
      for ($i = 0; $i < $count; $i++) {
        if ($plans[$i] != 'STWENTY_THREE' && $plans[$i] != 'NINETEEN' && in_array($plans[$i], $refreshPlans)) {
          unset($plans[$i]);
        }
      }
    } else {
      $count = count($plans);
      for ($i = 0; $i < $count; $i++) {
        if ($plans[$i] == 'ANINETEEN' || !in_array($plans[$i], $refreshPlans)) {
          unset($plans[$i]);
        }
      }
    }

    return $plans;
  }
}
