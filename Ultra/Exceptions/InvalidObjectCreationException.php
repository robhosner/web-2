<?php
namespace Ultra\Exceptions;

require_once 'Ultra/Exceptions/CustomErrorCodeException.php';

class InvalidObjectCreationException extends CustomErrorCodeException
{

}
