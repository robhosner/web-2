<?php
namespace Ultra\Exceptions;

use Exception;

class CustomErrorCodeException extends Exception
{
  public $errorCode;

  /**
   * @var array
   */
  private $customUserErrors;

  public function __construct($message, $code = '', Exception $previous = null, array $customUserErrors = [])
  {
    $this->errorCode = $code;
    parent::__construct($message, 0, $previous);
    $this->customUserErrors = $customUserErrors;

    if (!empty($code)) {
      $class = explode('\\', get_called_class());
      $message = array_pop($class) .  " -> " . $message . " (Error Code: $code)";
    }

    dlog('', $message);
  }

  public function code() {
    return $this->errorCode;
  }

  public function getCustomUserErrors()
  {
    return $this->customUserErrors;
  }
}
