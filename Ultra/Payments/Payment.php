<?php
namespace Ultra\Payments;

class Payment
{
  public function validateFactoredCommission($masterAgent)
  {
    return \validateFactoredCommission($masterAgent);
  }

  public function verifyProviderSku($providerName, $upc, $subscriber)
  {
    return \verifyProviderSku($providerName, $upc, $subscriber);
  }

  public function verifyProviderSkuAmount($providerName, $upc, $loadAmount)
  {
    return \verifyProviderSkuAmount($providerName, $upc, $loadAmount);
  }

  public function verifyAmountDestination($subscriber, $destination, $loadAmount)
  {
    return \verifyAmountDestination($subscriber, $destination, $loadAmount);
  }

  public function checkExcessBalance($customer, $amount)
  {
    return \checkExcessBalance($customer, $amount);
  }

  public function onlineAutoEnroll($customerId, $iccid, $repos = null)
  {
    return \online_auto_enroll($customerId, $iccid, $repos);
  }

  public function funcGetOrangePlanFromSimData($simData)
  {
    return \func_get_orange_plan_from_sim_data($simData);
  }

  public function funcSweepBalanceToStoredValue(array $params)
  {
    return \func_sweep_balance_to_stored_value($params);
  }

  public function funcValidatePinCards(array $params)
  {
    return \func_validate_pin_cards($params);
  }

  public function funcAddFundsByTokenizedCC(array $params, $disableFraudCheck = false, $isCommissionable = 1)
  {
    return \func_add_funds_by_tokenized_cc($params, $disableFraudCheck, $isCommissionable);
  }

  public function funcSpendFromBalance(array $params)
  {
    return \func_spend_from_balance($params);
  }
}