<?php
namespace Ultra\Payments\TaxCalculator;

class SureTax
{
  public function performCall(array $params)
  {
    return \Ultra\Lib\SureTax\perform_call($params);
  }
}
