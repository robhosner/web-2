<?php

namespace Ultra\Payments;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Lib\Util\Redis;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\SessionUtilities;

class CreditCardCharger
{
  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var FeatureFlagsClientWrapper
   */
  private $flagsObj;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var SessionUtilities
   */
  private $sessionUtilities;

  /**
   * @var StateActions
   */
  private $stateActions;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var Payment
   */
  private $payment;

  /**
   * @var Messenger
   */
  private $messenger;

  public function __construct(
    FeatureFlagsClientWrapper $flagsObj,
    CustomerRepository $customerRepository,
    SessionUtilities $sessionUtilities,
    Redis $redis,
    StateActions $stateActions,
    Configuration $configuration,
    Payment $payment,
    Messenger $messenger
  ) {
    $this->flagsObj = $flagsObj;
    $this->customerRepository = $customerRepository;
    $this->sessionUtilities = $sessionUtilities;
    $this->redis = $redis;
    $this->stateActions = $stateActions;
    $this->configuration = $configuration;
    $this->payment = $payment;
    $this->messenger = $messenger;
  }

  public function chargeCreditCard($customerId, $chargeAmount, $destination, $productType, $caller, $requestId)
  {
    if (!$customer = $this->customerRepository->getCombinedCustomerByCustomerId($customerId, ['*'])) {
      throw new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
    }

    // block if customer called this API more than 5 times in an hour
    if ($this->sessionUtilities->checkApiAbuseByIdentifier($caller, $customer->customer_id, 5)) {
      throw new CustomErrorCodeException('ERR_API_INTERNAL: command disabled; please try again later', 'AP0001');
    }

    if (!$this->flagsObj->disableRateLimits($customer->customer_id)) {
      if ($this->redis->get("creditcardcharge/$customerId/$chargeAmount")) {
        throw new CustomErrorCodeException('ERR_API_INTERNAL: duplicate charge; please try again later', 'AP0001');
      }
    }

    // fail if customer does not have credit card info in the DB
    if (!$customer->hasCreditCard()) {
      throw new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: No credit card on file.', 'CC0005');
    }

    // get current customer state
    $state = $this->stateActions->getStateFromCustomerId($customer->customer_id);
    if (!$state) {
      throw new CustomErrorCodeException('ERR_API_INTERNAL: customer state could not be determined', 'UN0001');
    }

    if (!isset($state['state']) || $state['state'] == STATE_CANCELLED) {
      throw new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
    }

    if ($destination == 'WALLET') {
      // maximum allowed wallet balance is 200
      // https://issues.hometowntelecom.com:8443/browse/API-1168
      if ((($chargeAmount / 100) + $customer->balance) > 200) {
        throw new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet', 'CH0001');
      }
    } else {
      // set the customer's maximum allowed stored_value to be the greater of their current plan amount and future plan amount
      // https://issues.hometowntelecom.com:8443/browse/API-1168

      // plan cost returns in cents
      $plan_cost = $this->configuration->getPlanCostFromCosId($customer->cos_id);
      $bolt_ons_cost = $this->configuration->getBoltOnsCostsByCustomerId($customer->customer_id);

      // target cost in cents
      $target_cost = ($customer->monthly_renewal_target)
        ? $this->configuration->getPlanCostFromCosId($this->configuration->getCosIdFromPlan($customer->monthly_renewal_target))
        : 0;

      // use greater of two costs
      $plan_cost = ($plan_cost > $target_cost) ? $plan_cost : $target_cost;

      if ((($chargeAmount / 100) + $customer->stored_value) > ($plan_cost / 100) + $bolt_ons_cost) {
        throw new CustomErrorCodeException('ERR_API_INVALID_ARGUMENTS: Charge would exceed maximum allowed stored value amount', 'VV0115');
      }
    }

    // charge customer's credit card
    $disableFraudCheck = $this->flagsObj->disableRateLimits($customer->customer_id);
    $result = $this->payment->funcAddFundsByTokenizedCC(
      [
        'customer'            => $customer,
        'charge_amount'       => $chargeAmount / 100,
        'detail'              => $caller,
        'include_taxes_fees'  => true,
        'description'         => $destination == 'WALLET' ? 'Wallet Balance Added Using Credit Card' : 'Plan Recharge Using Credit Card',
        'reason'              => $destination == 'WALLET' ? 'ADD_BALANCE' : 'RECHARGE',
        'session'             => $requestId,
        'fund_destination'    => $destination == 'WALLET' ? 'balance' : 'stored_value',
        'product_type'        => $productType,
      ],
      $disableFraudCheck
    );

    if ($result->is_failure()) {
      dlog('', 'ERRORS: %s', $result->get_errors());
      throw new CustomErrorCodeException('ERR_API_INTERNAL: credit card transaction not successful', 'CC0001');
    }

    $this->redis->set("creditcardcharge/$customerId/$chargeAmount", 1, 5 * 60);

    // send SMS for stored value load
    if ($destination != 'WALLET') {
      $result_object = $this->messenger->enqueueImmediateSms(
        $customerId,
        'balanceadd_recharge',
        [
          'reload_amount' => '$' . ($chargeAmount/100),
          'renewal_date'  => date('M d Y', strtotime($customer->plan_expires))
        ]
      );

      if ($result_object->get_errors()) {
        $result->add_warning('Failed to send SMS');
      }
    }

    return $result;
  }
}
