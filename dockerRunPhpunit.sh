#!/bin/bash
rm -rf reports/coverage/
rm -f reports/unitreport.xml

/home/ht/www/api/web/vendor/phpunit/phpunit/phpunit --log-junit=reports/unitreport.xml --coverage-html=reports/coverage --coverage-clover=reports/coverage/coverage.xml