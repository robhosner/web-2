<?php

/**
 * Call Detail Record Handler
 * PHP application that moves large CDR files from MVNE to Ultra & Exacaster
 *
 * workflow:
 *    initialize
 *    loop for each directory pair of source:download -> destination:upload
 *      list all files and sizes on source
 *      list all files and sizes on destination
 *      loop for each file in source
 *        if missing or different on destination
 *          download source into temporary local file
 *          upload temporary local file into destination
 *
 * @author VYT, 14-08-04
 * @todo
    - replace stream_copy_to_stream due to crashing on signal
    - add non-blocking reading and connection checking when reading off SFTP
    - add empty FTP directory detection
    - check DAT file ending for ';'
    - better structure for deflated file names
    - temp files cleanup on premature exit
 */

// SFTP source
define ('CDR_SOURCE_HOST', '12.232.86.97');
define ('CDR_SOURCE_PORT', 22);
define ('CDR_SOURCE_USERNAME', 'ultra');
define ('CDR_PUBLIC_KEY', $_SERVER['HOME'] . '/.ssh/id_rsa.pub'); // path to our SSH public key
define ('CDR_PRIVATE_KEY', $_SERVER['HOME'] . '/.ssh/id_rsa'); // path to our SSH private key
define ('CDR_ARCHIVE_DIR', 'archive'); // files are moved into this directory after successfull download
define ('CDR_MIN_DOWNLOAD_SPEED', 1048576); // minimum download speed is 1 mbps

// FTP destination
define ('CDR_DESTINATION_HOST', '162.222.64.20');
define ('CDR_DESTINATION_PORT', 21);
define ('CDR_DESTINATION_USERNAME', 'cdr');
define ('CDR_DESTINATION_PASSWORD', "c4$403*EPFceEJdR");
define ('CDR_DESTINATION_DIRECTORY', 'outbound/wholesale/gprs');

// FTP destination
define ('EXA_DESTINATION_HOST', '192.168.40.16');
define ('EXA_DESTINATION_PORT', 22);
define ('EXA_DESTINATION_USERNAME', 'ultra');
define ('EXA_DESTINATION_PASSWORD', "limecave51");

// map of directories on source to destination hosts, (relative to home directory): download -> upload
// each source directory also has 'archive' subdir where we move successfully download files
$cdr_directories = array(
  'outbound/wholesale/mms'         => 'outbound/wholesale/mms',
  'outbound/wholesale/voice_sms'   => 'outbound/wholesale/voice_sms',
  'outbound/wholesale/gprs'        => 'outbound/wholesale/gprs');

// others
define ('CDR_NAME', 'acc-cdr'); // process name as it appears in logs and PID
define ('CDR_VERSION', 2.4); // program version
define ('CDR_TIMEOUT', 4); // short network timeout to ensure script does not wait forever
define ('CDR_TEMP_DIR', '/cdr/outbound/wholesale'); // location of temporary local files, absolute
define ('CDR_TEMP_PREFIX', 'CDR-temp-'); // name prefix for temporary local files
define ('CDR_MAX_CPU_LOAD', 80); // CPU load beyond which we will quit, %
define ('CDR_MAX_RAM_LOAD', 90); // RAM usage beyond which we will quit, %
define ('CDR_MIN_FILE_SIZE', 32); // skip files smaller than this (possibly corrupt), bytes
define ('CDR_MAX_RUN_TIME', 11 * 60 * 60); // maximum run time after which we terminate, sec
define ('CDR_MAX_DOWNLOADS', 3); // maximum number of concurrent downloads (children processes)
define ('CDR_MAX_GZ_CHUNK', 1048576); // max amount of RAM to use for GZ compression and decompression
ini_set('memory_limit', '14336M'); // Allowed memory usage by this script


// logging
define ('CDR_SYSLOG_FACILITY', LOG_LOCAL5); // syslog facility, change as needed and update syslog config
define ('CDR_ESCALATION', 'ERROR ESCALATION ALERT DAILY'); // log escalation prefix for fatal errors

/*****************************************************************************/
/**************  NO  CONFIGURABLE  OPTIONS  BELOW  THIS  LINE  ***************/
/*****************************************************************************/

$recoverable = array(
  'ftp_fput(): Data connection already open; Transfer starting.',
  'ftp_delete(): The system cannot find the file specified.');
$recover = FALSE;

/**
 * initialization
 */

// open syslog
if (! openlog(CDR_NAME, LOG_CONS | LOG_ODELAY | LOG_PID, CDR_SYSLOG_FACILITY))
  die('FATAL: cannot connect to syslog');

// register custom error handler to catch all erors to prevent script death from
// non-exception errors returned by most network calls
set_error_handler('errorHandler', E_ALL);

// at this point we are ready to log to syslog via our custom error handler
trigger_error('starting ' . CDR_NAME . ' version ' . CDR_VERSION, E_USER_NOTICE);

// check server load to avoid overloading
if ($error = checkMachineLoad(CDR_MAX_CPU_LOAD, CDR_MAX_RAM_LOAD))
  trigger_error($error, E_USER_ERROR);

// register custom signal handlers so that we can clean up before getting killed
declare(ticks = 1);
if (! pcntl_signal(SIGTERM, 'signalHandler') || ! pcntl_signal(SIGHUP, 'signalHandler') || ! pcntl_signal(SIGINT, 'signalHandler'))
  trigger_error('failed to register signal handler', E_USER_ERROR);

// check if previous process is still running
if ($pid = findDuplicateProcess())
{
  trigger_error("process $pid is still running, exiting", E_USER_NOTICE);
  shutdown(0);
}

// cleanup temporary files from prerious run (not likely but possible)
cleanTempFiles(CDR_TEMP_DIR, CDR_TEMP_PREFIX);

// verify network connectivity to source and destinations
if (! checkTcpConnectivity(CDR_SOURCE_HOST, CDR_SOURCE_PORT))
  trigger_error('failed to verify network connectivity to host ' . CDR_SOURCE_HOST . ' on port ' . CDR_SOURCE_PORT, E_USER_ERROR);
if (! checkTcpConnectivity(CDR_DESTINATION_HOST, CDR_DESTINATION_PORT))
  trigger_error('failed to verify network connectivity to host ' . CDR_DESTINATION_HOST . ' on port ' . CDR_DESTINATION_PORT, E_USER_ERROR);
if (! checkTcpConnectivity(EXA_DESTINATION_HOST, EXA_DESTINATION_PORT))
  trigger_error('failed to verify network connectivity to host ' . EXA_DESTINATION_HOST . ' on port ' . EXA_DESTINATION_PORT, E_USER_ERROR);

// transfer all files in all directories
main($cdr_directories);

// normal end of execution
trigger_error(CDR_NAME . ' finished', E_USER_NOTICE);
shutdown(0);


/**
 * main
 * function that copies all source files to destination
 */
function main($directories)
{
  // init statistics
  $program_start = time();
  $file_count = 0; // how many files we transferred
  $skip_count = 0; // how many files we skipped
  $byte_count = 0; // how many bytes we transferred
  $download_speed; // current download speed, bits per second
  $download_stats = array(); // download speed of each file, bits per second
  $children = array(); // download children processe IDs

  // process each source -> destination dir
  foreach ($directories as $download => $upload)
  {
    trigger_error("processing directories $download -> $upload", E_USER_NOTICE);

    // connect to source host
    list($source, $sftp) = sftpConnect(CDR_SOURCE_HOST, CDR_SOURCE_PORT, CDR_SOURCE_USERNAME, CDR_PUBLIC_KEY, CDR_PRIVATE_KEY);

    // scan source directory
    if (($source_files = scanSftpDirectory($sftp, $download)) === NULL)
      trigger_error("failed to scan source directory $download", E_USER_ERROR);

    // disconnect from source
    sftpDisconnect($source, $sftp);

    // connect to destination host
    if ( ! $ftp = ftpConnect(CDR_DESTINATION_HOST, CDR_DESTINATION_PORT, CDR_DESTINATION_USERNAME, CDR_DESTINATION_PASSWORD))
      trigger_error("failed to connect to FTP server", E_USER_ERROR);

    // scan destination directory
    if (($destination_files = scanFtpDirectory($ftp, $upload)) === NULL)
      trigger_error("failed to scan destination directory $upload", E_USER_ERROR);

    // disconnect from destination host
    ftp_close($ftp);
    unset($ftp);

    // connect to destination host (exacaster)
    if ( ! $ftp_exa =  ftpConnect(EXA_DESTINATION_HOST, EXA_DESTINATION_PORT, EXA_DESTINATION_USERNAME, EXA_DESTINATION_PASSWORD))
     trigger_error("failed to connect to EXASTER FTP server", E_USER_ERROR);

    // scan EXACASTER destination directory
    if (($destination_files_exa = scanFtpDirectory($ftp_exa, $upload)) === NULL)
     trigger_error("failed to scan source directory $upload", E_USER_ERROR);

    // disconnect from source
    ftp_close($ftp_exa);
    unset($ftp_exa);

    // process each file: errors encountered are logged and execution continues with the next file
    foreach ($source_files as $file => $size)
    {
      trigger_error("processing source file $download/$file ($size bytes)", E_USER_NOTICE);
      // Exacaster will have the compressed version
      $file_exa="$file.gz";
      trigger_error('destination file '  . (empty($destination_files[$file]) ? 'does not exist' : $destination_files[$file] . ' bytes'), E_USER_NOTICE);
      trigger_error('destination file at EXA '  . (empty($destination_files_exa[$file_exa]) ? 'does not exist' : $destination_files_exa[$file_exa] . ' bytes'), E_USER_NOTICE);

      // skip (possibly) corrupt files
      if ($size < CDR_MIN_FILE_SIZE)
      {
        trigger_error("source file $download/$file ($size bytes) is too small, skipping", E_USER_WARNING);
        $skip_count++;
        continue;
      }

      // skip existing files of the same size
      if ( ! empty($destination_files[$file]) && $destination_files[$file] == $size && ! empty($destination_files_exa[$file_exa]) )
      {
        // archive existing file
        trigger_error("destination $upload/$file already exists, skipping", E_USER_NOTICE);
        list($source, $sftp) = sftpConnect(CDR_SOURCE_HOST, CDR_SOURCE_PORT, CDR_SOURCE_USERNAME, CDR_PUBLIC_KEY, CDR_PRIVATE_KEY);
        if ( ! ssh2_sftp_rename($sftp, "$download/$file", "$download/" . CDR_ARCHIVE_DIR . "/$file"))
          trigger_error("failed to archive source file $download/$file -> $download/" . CDR_ARCHIVE_DIR . "/$file", E_USER_WARNING);
        else
          trigger_error("archived source file $download/$file", E_USER_NOTICE);
        sftpDisconnect($source, $sftp);
        $skip_count++;
        continue;
      }

      // prevent forking more children than allowed
      while (count($children) >= CDR_MAX_DOWNLOADS)
      {
        sleep(1);
        $pid = pcntl_waitpid(-1, $status, WNOHANG);
        if ($pid == -1)
          trigger_error('pcntl_waitpid returned error', E_USER_ERROR);
        elseif ($pid)
        {
          trigger_error("child $pid has exited", E_USER_NOTICE);
          $status = pcntl_wexitstatus($status);
          foreach ($children as $index => $child)
            if ($child == $pid)
              unset($children[$index]);
        }
      }

      // fork download child
      $children[] = forkDownload($download, $upload, $file, $size, isset($destination_files[$file]), isset($destination_files_exa[$file]));
    }
    trigger_error("finished directory $download -> $upload", E_USER_NOTICE);

  }
}

/**
 * checkMachineLoad
 * check server CPU and RAM load
 * @param int max CPU load, % or NULL if no check wanted
 * @param int max memory usage, % or NULL of no check wanted
 * @return string NULL if within limits or error message if exceeded 
 */
function checkMachineLoad($cpu_max = NULL, $ram_max = NULL)
{
  // check CPU if requested
  if ($cpu_max !== NULL)
  {
    $cpu = sys_getloadavg();
    if ($cpu[0] > $cpu_max)
      return "CPU load {$cpu[0]}% over limit $cpu_max%";
  }

  // check RAM if requested
  if ($ram_max !== NULL)
  {
    list($ram) = getMemoryUsage();
    if ($ram > $ram_max)
      return "RAM usage $ram% over limit $ram_max";
  }

  trigger_error("CPU: {$cpu[0]}%, RAM: $ram%", E_USER_NOTICE);
  return NULL;
}


/**
 * findDuplicateProcess
 * locate identical process and return its pid
 * @author VYT, 14-10-06
 */
function findDuplicateProcess()
{
  global $argv;
  $proc = '/proc';
  $pid = getmypid();

  try
  {
    $command = file_get_contents("$proc/$pid/cmdline");
    if ($command === FALSE || empty($command))
      throw new Exception("failed to read $proc/$pid/cmdline");
    $command = substr($command, 0, strpos($command, $argv[0]) + strlen($argv[0]));

    $processes = scandir($proc);
    if (empty($processes) || ! count($processes))
      throw new Exception("failed to scan $proc directory");
    foreach ($processes as $spid)
      if (is_dir("$proc/$spid") && is_numeric($spid) && $pid != $spid)
      {
        $other = file_get_contents("$proc/$spid/cmdline");
        if ($other !== FALSE && ! empty($other) && substr($other, 0, strlen($command)) == $command)
          return $spid;
      }
  }
  catch(Exception $e)
  {
    trigger_error('EXCEPTION: ' . $e->getMessage(), E_USER_ERROR);
  }

  return NULL;
}


/**
 * getMemoryUsage
 * get server memory status by inspecting /proc/meminfo
 * @return array (float usage percentage, int bytes total, int bytes free)
 * @author VYT, 14-08-06
 */
function getMemoryUsage()
{
  $ram = 0;
  $free = 0;
  $usage = NULL;

  if ($info = fopen('/proc/meminfo', 'r'))
  {
    while ($line = fgets($info))
    {
      $match = NULL;
      if (strpos($line, 'MemTotal:') === 0)
      {
        preg_match('!\d+!', $line, $match);
        if (! empty($match[0]))
          $ram = $match[0];
      }
      elseif (strpos($line, 'MemFree:') === 0)
      {
        preg_match('!\d+!', $line, $match);
        if (! empty($match[0]))
          $free = $match[0];
      }
    }
    fclose($info);
  }
  if ($ram && $free)
    $usage = (float)sprintf('%.2f', $free / $ram * 100);
  return array($usage, $ram, $free);
}


/**
 * sftpConnect
 * create a connection to SFTP server
 * @param string host name
 * @param string username
 * @param string SSH public key file path
 * @param string SSH private key file path
 * @return array (object connection, object SFTP subsystem, string error message)
 */
function sftpConnect($host, $port, $username, $public, $private)
{
  $error = NULL;
  $connection = NULL;
  $subsystem = NULL;

  try
  {
    if (! $connection = ssh2_connect(CDR_SOURCE_HOST, $port))
      throw new Exception("failed to connect to SFTP host $host on port $port");

    if (! ssh2_auth_pubkey_file($connection, $username, CDR_PUBLIC_KEY, CDR_PRIVATE_KEY))
      throw new Exception('failed to authenticate to SFTP host');

    if (! $subsystem = ssh2_sftp($connection))
      throw new Exception ('failed to initialize SFTP subsystem');

    trigger_error("connected to SFTP host $host", E_USER_NOTICE);
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    trigger_error($error ? $error : 'failed to connect to SFTP host', E_USER_ERROR);
  }

  return array($connection, $subsystem);
}

/**
 * sftpDisconnect
 * close connection to SFTP host
 */
function sftpDisconnect(&$connection, &$subsystem)
{
  ssh2_exec($connection, 'exit;');
  unset($subsystem);
  unset($connection);
}


/**
 * ftpConnect
 * createa a connection to FTP server
 * @param string host name
 * @param string username
 * @param string password
 * @return object connection
 */
function ftpConnect($host, $port, $username, $password)
{
  $connection = NULL;

  try
  {
    if ( ! $connection = ftp_connect($host, $port, CDR_TIMEOUT))
      throw new Exception('failed to connect to FTP host');

    if (! ftp_login($connection, $username, $password))
      throw new Exception('failed to authenticate to FTP host');

    ftp_pasv($connection, TRUE);
 
    if ( ! ftp_set_option($connection, FTP_TIMEOUT_SEC, 600))
      throw new Exception("failed to set session timeout");

    trigger_error("connected to FTP host $host", E_USER_NOTICE);
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    trigger_error($error ? $error : 'failed to connect to FTP host', E_USER_NOTICE);
  }

  return $connection;
}


/**
 * checkTcpConnectivity
 * verify network connectivity to a host by attempting to make a TCP connection
 * @param string host
 * @param int port number
 * @param return boolean TRUE on success, FALSE on failure
 */
function checkTcpConnectivity($host, $port)
{
  $error = 0;
  $message = NULL;
  if (! $socket = fsockopen($host, $port, $error, $message, CDR_TIMEOUT))
    return FALSE;

  fclose($socket);
  return TRUE;
}


/**
 * cleanTempFiles
 * remove temprary files from previous run in case something got left behind due to errors
 * ignore any errors since OS will always create files with unique names
 * @param string directory
 * @param string name prefix
 */
function cleanTempFiles($dir, $prefix)
{
  foreach (scandir($dir) as $item)
    if ($item != '.' && $item != '..' && strpos($item, $prefix) === 0)
    {
      if (unlink("$dir/$item"))
        trigger_error("deleted old temporary file $dir/$item", E_USER_NOTICE);
      else
        trigger_error("failed to remove old temporary file $dir/$item", E_USER_WARNING);
    }
}


/**
 * errorHandler
 * custom error handler to trap errors generated by PHP native functions
 * @see http://us1.php.net/manual/en/function.set-error-handler.php
 */
function errorHandler($type, $message, $file, $line, $context)
{
  global $recoverable, $recover;
  // syslog(LOG_INFO, "type: $type, message: '$message', in_array: " . (in_array($message, $recoverable) ? 'T' : 'F'));

  switch ($type)
  {
    // info messages
    case E_NOTICE:
    case E_USER_NOTICE:
      syslog(LOG_INFO, $message);
      break;

    // recoverable errors
    case E_WARNING:
    case E_USER_WARNING:
      syslog(LOG_WARNING, "WARNING: $message (line $line)");
      $recover = TRUE;
      break;

    // unknown errors (including system) are fatal
    default:
      syslog(LOG_ERR, CDR_ESCALATION . ": $message (line $line), exiting");
      shutdown($type);
  }
}

/**
 * singnalHandler
 * catch termination signals and clean up before existing
 */
function signalHandler($signal)
{
  trigger_error("received signal $signal", E_USER_ERROR);
}


/**
 * isRecoverable
 * check if last error can be ingored
 */
function isRecoverable()
{
  global $recover;

  $result = $recover;
  $recover = FALSE;
  return $result;
}


/**
 * scanSftpDirectory
 * list files only and their sizes in a remote SFTP directory
 * @param resource host connection
 * @param string directory path
 * @result array(string file name => int file size)
 */
function scanSftpDirectory($connection, $path)
{
  $result = array();
  $work = array();
  $mtime = array();

  // check param
  if (! $connection || ! $path)
  {
    trigger_error('invalid parameter for ' . __FUNCTION__, E_USER_NOTICE);
    return NULL;
  }

  // open directory
  trigger_error("scanning SFTP directory $path", E_USER_NOTICE);
  $dir = opendir("ssh2.sftp://$connection/$path");
  if (! $dir)
  {
    trigger_error("failed to open directory $path", E_USER_NOTICE);
    return NULL;
  }

  // loop through all items in directory
  while ($item = readdir($dir))
  {
    if (! is_dir("ssh2.sftp://$connection/$path/$item"))
    {
      $info = stat("ssh2.sftp://$connection/$path/$item");
      if (empty($info))
        trigger_error("failed to get $item file info", E_USER_NOTICE);
      else
      {
        $work[$item] = $info['size'];
        $mtime[$item] = $info['mtime'];
      }
    }
  }
  closedir($dir);
  trigger_error('found ' . (count($work)) . ' files', E_USER_NOTICE);

  // MVNO-2736: download oldest files first: sort $mtime and copy $work into $result based on new $mtime order
  asort($mtime);
  foreach ($mtime as $file => $timestamp)
    $result[$file] = $work[$file];

  return $result;
}


/**
 * scanFtpDirectory
 * list fies and sizes in remote FTP directory (also returns subdirectories but we do not care)
 * unfortunately we cannot use PHP ftp/file functions to scan FTP directory because of 2GB bug
 * @see https://bugs.php.net/bug.php?id=41643
 * @param resource FTP connection
 * @return array(string 'name' => int size) or NULL on failure
 */
function scanFtpDirectory($ftp, $path)
{
  $result = array();
  trigger_error("scanning FTP directory $path", E_USER_NOTICE);

  // the list will be empty if 1) directory is empty or 2) directory does not exist
  $list = ftp_rawlist($ftp, $path);
  foreach ($list as $item)
  {
    $item = preg_replace('/\s+/', ' ', $item); // replace multiple white spaces with one
    //list($date, $time, $size, $name) = explode(' ', $item); // split values on space
    list($rights,$link, $owner, $group, $size, $date1, $date2, $time, $name)  = explode(' ', $item); // split values on space
    if (! empty($name) && ! empty($size)) // add valid values to result
      $result[$name] = $size;
  }

  trigger_error('found ' . (count($result)) . ' files', E_USER_NOTICE);
  return $result;
}


/**
 * shutdown
 * clean up and exit
 * @param int error type to return to shell
 */
function shutdown($error)
{
  // close syslog
  closelog();

  exit($error);
}

/**
 * printSpeed
 * format bits per second for printing
 */
function printSpeed($bps)
{
  if ($bps > (1024 * 1024))
    return round($bps / 1024 / 1024) . 'mbps';
  elseif ($bps > 1024)
    return round($bps / 1024) . 'kbps';
  else
    return $bps . 'bps';
}

/**
 * forkDownload
 * fork download process for a single file
 */
function forkDownload($source, $destination, $file, $size, $overwrite, $overwrite_exa)
{
  $error = NULL;
  $local = NULL;
  $temporary = NULL;

  // all exceptions are fatal and will terminate the process
  try
  {
    // fork child process and returns its PID
    $pid = pcntl_fork();
    if ($pid == -1)
      throw new Exception("failed to fork child process for $source/$file");
    elseif ($pid)
      return $pid;
    else
      $pid = getmypid();

    trigger_error("source: $source, destination: $destination, file: $file, size: $size, overwrite: " . ($overwrite ? 'Y' : 'N'), E_USER_NOTICE);
    trigger_error("source: $source, destination: $destination, file: $file, size: $size, overwrite_exa: " . ($overwrite_exa ? 'Y' : 'N'), E_USER_NOTICE);

    // check available local disk space
    $space = disk_free_space(CDR_TEMP_DIR);
    if ($space <= $size)
      throw new Exception('not enough disk space on ' . CDR_TEMP_DIR . ": $space available, $size needed");

    // create temporary file with our PID in its name and open it for writing
    $temporary = CDR_TEMP_DIR . '/' . CDR_TEMP_PREFIX . "$pid-$file";
    if ( ! $local = fopen($temporary, 'w'))
      throw new Exception("failed to create and open local file $temporary");

    // connect to source host
    list($connection, $sftp) = sftpConnect(CDR_SOURCE_HOST, CDR_SOURCE_PORT, CDR_SOURCE_USERNAME, CDR_PUBLIC_KEY, CDR_PRIVATE_KEY);

    // open source file for reading
    trigger_error("opening source file $source/$file ($size bytes)", E_USER_NOTICE);
    if (! $input = fopen("ssh2.sftp://$sftp/$source/$file", 'r'))
      throw new Exception("failed to open source file $source/$file");

    // download source file to local
    $start = time();
    $bytes = stream_copy_to_stream($input, $local);
    if ($bytes != $size)
      throw new Exception("transfer corrupt: received $bytes, expected $size");

    // finish download
    fclose($input);
    fclose($local);
    $local = NULL;
    trigger_error("finished downloading $bytes bytes in " . (time() - $start) . ' sec', E_USER_NOTICE);

    // Disconnect & reconnect before we archive
    sftpDisconnect($connection, $sftp);

    // verify local file size
    $actual = filesize($temporary);
    if ($actual != $size)
      throw new Exception("corrupt local file $temporary: size $actual bytes, expecting $size, skipping");

    // decompress the file if necessary and update final size and name
    $ext = '';
    if (substr($temporary, -3) == '.gz')
    {
      $temporary = decompressGzipFile($temporary);
      $size = filesize($temporary);
      $file = substr($file, 0, -3);
      $ext = '.gz';
    }

    // connect to destination host
    if ( ! $ftp = ftpConnect(CDR_DESTINATION_HOST, CDR_DESTINATION_PORT, CDR_DESTINATION_USERNAME, CDR_DESTINATION_PASSWORD))
      throw new Exception('no FTP server connection');

    // remove destination file if required
    if ($overwrite)
      if ( ! ftp_delete($ftp, "$destination/$file") && ! isRecoverable())
        throw new Exception("failed to deleted existing destination file $destination/$file");

    // re-open local source file for reading
    if ( ! $local = fopen($temporary, 'r'))
      throw new Exception("failed to open local file $temporary");

    //TECHOPS-374 uncommited transfer 
    $ext = '.uncommitted';
    $file_temp="$file$ext";
    // upload local file to destination host
    trigger_error("starting upload of local $temporary to destination $destination/$file_temp", E_USER_NOTICE);
    $start = time();
    // warning: using FTP_ASCII will append an extra char to end of each line, which results in file size discrepancy
    if ( ! ftp_fput($ftp, "$destination/$file_temp", $local, FTP_BINARY) && ! isRecoverable())
      throw new Exception("failed to upload local file $temporary to $destination/$file");
    trigger_error("finished upload in " . (time() - $start) . ' sec', E_USER_NOTICE);

    // verify uploaded file (unlike ftp_size() this method works with files > 2GB)
    for ($j = 0, $actual = NULL; $j < 8; $j++)
    {
      $response =  ftp_raw($ftp, "SIZE $destination/$file_temp");
      if (count($response) && ! empty($response[0]))
      {
        $values = explode(' ', $response[0]);
        if ($values[0] == '213')
        {
          $actual = $values[1];
          trigger_error("uploaded file size: $actual", E_USER_NOTICE);
          break;
        }
      }
      trigger_error("SIZE loop $j", E_USER_NOTICE);
    }
    if ($actual !== NULL && $actual != $size)
    {
      if ( ! ftp_delete($ftp, "$destination/$file_temp"))
        trigger_error("failed to delete corrupt uploaded file $destination/$file_temp", E_USER_NOTICE);
      else
        trigger_error("removed corrupt uploaded file $destination/$file_temp", E_USER_NOTICE);
      throw new Exception("uploaded file size $actual does not match expected $size");
    }
    // Finalize upload
      if ( ! ftp_rename($ftp, "$destination/$file_temp", "$destination/$file"))
       trigger_error("failed to rename temporary file", E_USER_NOTICE);
      else
       trigger_error("renamed file $destination/$file_temp to $destination/$file", E_USER_NOTICE);
    // Close the local file between transfers
	fclose($local);
    
    // compress the file for EXACASTER and update final size and name
    
    $temporary = compressGzipFile($temporary);
    $file_comp = "$file.gz";
    $size = filesize($temporary);
    $ext = '.uncommitted';
    $file_temp="$file_comp$ext";
  
      // connect to destination host
    if ( ! $ftp_exa = ftpConnect(EXA_DESTINATION_HOST, EXA_DESTINATION_PORT, EXA_DESTINATION_USERNAME, EXA_DESTINATION_PASSWORD))
      throw new Exception('no FTP server connection to EXACASTER');

     // remove destination file if required
    if ($overwrite_exa)
      if ( ! ftp_delete($ftp_exa, "$destination/$file_comp") && ! isRecoverable())
        throw new Exception("failed to deleted existing destination file $destination/$file_comp");

	// re-open local source file for reading
    if ( ! $local = fopen($temporary, 'r'))
      throw new Exception("failed to open local file $temporary");
	  
    // upload local file to destination host (EXA)
    trigger_error("starting upload of local $temporary to EXACASTER destination $destination/$file_comp", E_USER_NOTICE);
    $start = time();

    // warning: using FTP_ASCII will append an extra char to end of each line, which results in file size discrepancy
    if ( ! ftp_fput($ftp_exa, "$destination/$file_temp", $local, FTP_BINARY) && ! isRecoverable())
      throw new Exception("failed to upload local file $temporary to $destination/$file_temp");
    trigger_error("finished upload in " . (time() - $start) . ' sec', E_USER_NOTICE);

    // verify uploaded file (unlike ftp_size() this method works with files > 2GB)
    for ($j = 0, $actual = NULL; $j < 8; $j++)
    {
      $response =  ftp_raw($ftp_exa, "SIZE $destination/$file_temp");
      if (count($response) && ! empty($response[0]))
      {
        $values = explode(' ', $response[0]);
        if ($values[0] == '213')
        {
          $actual = $values[1];
          trigger_error("uploaded file size: $actual", E_USER_NOTICE);
          break;
        }
      }
      trigger_error("SIZE loop $j", E_USER_NOTICE);
    }
    if ($actual !== NULL && $actual != $size)
    {
      if ( ! ftp_delete($ftp_exa, "$destination/$file_temp"))
        trigger_error("failed to delete corrupt uploaded file $destination/$file_comp", E_USER_NOTICE);
      else
        trigger_error("removed corrupt uploaded file $destination/$file_comp", E_USER_NOTICE);
      throw new Exception("uploaded file size $actual does not match expected $size");
    }	

    // finish upload
    if ( ! ftp_rename($ftp_exa, "$destination/$file_temp", "$destination/$file_comp"))
     trigger_error("failed to rename temporary file", E_USER_NOTICE);
    else
     trigger_error("renamed file $destination/$file_temp to $destination/$file_comp", E_USER_NOTICE);
    fclose($local);
    $local = NULL;
    ftp_close($ftp_exa);
    
	//Reconnect to ACC to archive
    if ( ! list($connection, $sftp) = sftpConnect(CDR_SOURCE_HOST, CDR_SOURCE_PORT, CDR_SOURCE_USERNAME, CDR_PUBLIC_KEY, CDR_PRIVATE_KEY) ) 
     trigger_error("failed to connect to SFTP server for archiving", E_USER_ERROR);
    else
     // archive source file
     if ( ! ssh2_sftp_rename($sftp, "$source/$file", "$source/" . CDR_ARCHIVE_DIR . "/$file"))
      trigger_error("failed to archive source file $source/$file -> $source/" . CDR_ARCHIVE_DIR . "/$file", E_USER_WARNING);
     else
      trigger_error("archived source file $source/$file", E_USER_NOTICE);

  }

  catch(Exception $e)
  {
    $error = $e->getMessage();
  }

  // cleanup
  sftpDisconnect($connection, $sftp);
  if ($ftp)
    ftp_close($ftp);
  if ($local)
    fclose($local);
  if ($temporary)
    unlink($temporary);

  if ($error)
    trigger_error($error, E_USER_ERROR);
  else
  {
    trigger_error("finished processing $source/$file", E_USER_NOTICE);
    shutdown(0);
  }
}

/**
 * decompressGzipFile
 * uncompress GZ file into another file and return its path
 */
function decompressGzipFile($path)
{
  trigger_error("begin decompressing file $path", E_USER_NOTICE);

  if ( ! file_exists($path))
    trigger_error("cannot find GZ file $path", E_USER_ERROR);

  if ( ! $source = gzopen($path, 'r'))
    trigger_error("cannot open GZ file $path", E_USER_ERROR);

  $result = substr($path, 0, -3);
  if ( ! $dest = fopen($result, 'w'))
    trigger_error("cannot create destination file $result", E_USER_ERROR);

  while ( ! gzeof($source))
  {
    $chunk = gzread($source, CDR_MAX_GZ_CHUNK);
    $length = strlen($chunk);
    if (fwrite($dest, $chunk) != $length)
      trigger_error("failed to write $length bytes to uncompressed file $result", E_USER_ERROR);
  }

  gzclose($source);
  fclose($dest);

  // confirm that uncompressed file size if at last as large as original
  if (filesize($path) > filesize($result))
    trigger_error("decompressed file $result is smaller than original $path", E_USER_ERROR);

  // remove original
  if ( ! unlink($path))
    trigger_error("failed to remove compressed original $path", E_USER_NOTICE);

  trigger_error("finished decompressing $path -> $result", E_USER_NOTICE);
  return $result;
}

function compressGzipFile($path)
{

  if ( ! file_exists($path))
    trigger_error("cannot find file $path", E_USER_ERROR);

  $temp = "${path}.gz";
  $result = gzopen ($temp, 'w9');

  // Compress the file
  gzwrite ($result, file_get_contents($path));

  // Close the gz file and we're done
  gzclose($result);

  // remove original
  if ( ! unlink($path))
                trigger_error("failed to remove compressed original $path", E_USER_NOTICE);
   
  trigger_error("finished compressing $path -> $temp", E_USER_NOTICE);
  return $temp;
  
}

/*
      // print statistics
      $file_count++;
      $byte_count += $size;
      $download_speed = count($download_stats) ? array_sum($download_stats) / count($download_stats) : NULL;
      trigger_error("transferred $file_count files, $byte_count bytes, skipped $skip_count files" . ($download_speed ? ', download speed ' . printSpeed($download_speed) : NULL), E_USER_NOTICE);

      // check our runtime
      $runtime = time() - $program_start;
      if ($runtime > CDR_MAX_RUN_TIME)
      {
        if ($download_speed && $download_speed < CDR_MIN_DOWNLOAD_SPEED)
          trigger_error('download speed ' . printSpeed($download_speed) . ' is too low', E_USER_WARNING);
        trigger_error('exceeded maximum run time, terminating', E_USER_WARNING);
        shutdown(0);
      }
      else
        trigger_error('run time is ' . sprintf('%02d:%02d:%02d', $runtime / 3600, $runtime / 60 % 60, $runtime % 60));
    }
*/

?>
