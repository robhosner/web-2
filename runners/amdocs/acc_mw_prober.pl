use strict;
use warnings;


use Data::Dumper;
use FindBin;
use Getopt::Long;
use Ultra::Lib::Amdocs::Base;
use Ultra::Lib::Amdocs::Control::Prober;
use Ultra::Lib::Amdocs::Notification;
use Ultra::Lib::Amdocs::Notification::Prober;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::WSDL11;


my $params = setParameters();


if (
  $params->{'displayUsage'}
)
{
  displayUsage();
}
else
{
  main($params);
}


sub main
{
  my ($params) = @_;

  my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

  my $config = Ultra::Lib::Util::Config->new();

  my $MVNE_WSDL = $FindBin::Bin."/".$config->find_credential('amdocs/control/soap/wsdl');
  my $MVNO_WSDL = $FindBin::Bin."/".$config->find_credential('amdocs/notifications/soap/wsdl');

  if ( $params->{'production'} )
  {
    $MVNO_WSDL =~ s/_development.wsdl/_production.wsdl/;
  }

  print STDOUT "MVNE WSDL file = $MVNE_WSDL\n";
  print STDOUT "MVNO WSDL file = $MVNO_WSDL\n";

  if ( defined $params->{'configuration'} )
  {
    my $notificationObject = Ultra::Lib::Amdocs::Notification->new( JSON_CODER => $json_coder );
    my $controlObject      = Ultra::Lib::Amdocs::Base->new(      JSON_CODER => $json_coder );

    my $wsdlMVNE = XML::Compile::WSDL11->new( $MVNE_WSDL );
    $wsdlMVNE->compileCalls;

    my $wsdlMVNO = XML::Compile::WSDL11->new( $MVNO_WSDL );
    $wsdlMVNO->compileCalls;

    my $configuration =
    {
      % { $notificationObject->configuration( $wsdlMVNO , $params->{'configuration'} , 1 ) },
      % { $controlObject->configuration(      $wsdlMVNE , $params->{'configuration'} , 1 ) },
    };

    print STDOUT Dumper($configuration);
  }
  elsif ( defined $params->{'notification'} )
  {
    my $notificationProber = Ultra::Lib::Amdocs::Notification::Prober->new(
      JSON_CODER  => $json_coder,
      LOG_ENABLED => 1,
      WSDL_FILE   => $MVNO_WSDL
    );

    my $probeResult = $notificationProber->probeNotification( $params->{'notification'} , $params->{'data'} );

    print STDOUT Dumper($probeResult);
  }
  elsif ( defined $params->{'acc_mq'} )
  {
    my $controlProber = Ultra::Lib::Amdocs::Control::Prober->new(
      JSON_CODER  => $json_coder,
      LOG_ENABLED => 1,
      WSDL_FILE   => $MVNE_WSDL
    );

    my $probeResult = $controlProber->probeACCMWControlCommand( $params->{'acc_mq'} , $params->{'data'} );

    print STDOUT Dumper($probeResult);
  }
  elsif ( defined $params->{'control'} )
  {
    my $controlProber = Ultra::Lib::Amdocs::Control::Prober->new(
      JSON_CODER  => $json_coder,
      LOG_ENABLED => 1,
      WSDL_FILE   => $MVNE_WSDL
    );

    my $probeResult = $controlProber->probeControlCommand( $params->{'control'} , $params->{'data'} );

    print STDOUT Dumper($probeResult);
  }
  elsif ( defined $params->{'asp_throttling'} )
  {
    my $throttlingProber = Ultra::Lib::Aspider::Throttling::Prober->new(
      JSON_CODER  => $json_coder,
      LOG_ENABLED => 1,
      WSDL_FILE   => '/home/ht/www/rgalli3_dev/web/runners/soapy/MVNOWSAPICallback-20130913/ThrottlingListener.wsdl'
    );

    my $probeResult = $throttlingProber->probeThrottling();

    print STDOUT Dumper($probeResult);
  }
  else
  {
    print STDERR "Nothing to do.\n";
  }

  exit 0;
}


sub setParameters
{
  my $options = {};

  GetOptions(
    $options,
    'configuration:s',
    'notification:s',
    'acc_mq:s',
    'control:s',
    'data:s',
    'help',
    'production',
  );

  $options->{'displayUsage'} = $options->{'help'} if $options->{'help'};

  if ( $options->{'data'} )
  {
    $options->{'data'} = prepareInputData( $options->{'data'} );
  }

  print Dumper($options);

  return($options);
}


sub prepareInputData
{
  my $data = shift;

  my $dataHref = {};

  my $keyValuePairs = [ split ( /&/ , $data ) ];

  for my $keyValue( @$keyValuePairs )
  {
    if ( $keyValue =~ /^(.+)\=(.+)$/ )
    {
      my ($key,$value) = ($1,$2);

      if ( $key =~ /^(.+)__ARRAYOF(\d+)__(.+)__(.+)$/ )
      { # Example:  ResourceData__ARRAYOF0__Subscriber_ResourceData__productCategory=SIM
        $dataHref->{ $1 }->[ $2 ]->{ $3 }->{ $4 } = $value;
      }
      elsif ( $key =~ /^(.+)__(.+)__ARRAYOF(\d+)__(.+)$/ )
      { # Example:  ResourceData__Subscriber_ResourceData__ARRAYOF0__productCategory=SIM
        $dataHref->{ $1 }->{ $2 }->[ $3 ]->{ $4 } = $value;
      }
      elsif ( $key =~ /^(.+)__(.+)$/ )
      { # Example:  UserData__channelId=ULTRA
        $dataHref->{ $1 }->{ $2 } = $value;
      }
      else
      { # Example:  SuspendReason=test
        $dataHref->{ $key } = $value;
      }
    }
    else
    {
      die "Malformed parameters : $keyValue";
    }
  }

  return $dataHref;
}

sub displayUsage
{
  print qq|
Usage: $0 [options]

Options:
  --help            Displays this message.
  --notification    Triggers a mock inbound notification (SOAP call).
  --control         Triggers an outbound control command (SOAP call).
  --acc_mq          Enqueue an outbound messsage in the ACC MQ which will trigger an outbound control command (SOAP call).
  --configuration   Displays configuration for a command.
  --data            Data for the SOAP call. Nested data tags can be represented using '__' (see examples below).
  --production      If specified, hits production servers instead of development.

Examples:

  perl runners/amdocs/acc_mw_prober.pl --configuration 'ThrottlingAlert'
  perl runners/amdocs/acc_mw_prober.pl --notification 'ThrottlingAlert' --data 'Event=42&CounterName=42'
  perl runners/amdocs/acc_mw_prober.pl --notification 'PortOutDeactivation'
  perl runners/amdocs/acc_mw_prober.pl --notification 'NotificationReceived'
  perl runners/amdocs/acc_mw_prober.pl --notification 'PortOutRequest'
  perl runners/amdocs/acc_mw_prober.pl --notification 'ProvisioningStatus'
  perl runners/amdocs/acc_mw_prober.pl --acc_mq 'QuerySubscriber' --data 'MSISDN=6038920283&UserData__senderId=x&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA'
  perl runners/amdocs/acc_mw_prober.pl --control 'QuerySubscriber' --data 'key1=value1&key2=value2'
  perl runners/amdocs/acc_mw_prober.pl --control 'QuerySubscriber' --data 'MSISDN=6038920283&UserData__senderId=x&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA'
  perl runners/amdocs/acc_mw_prober.pl --control 'ResendOTA' --data 'MSISDN=6038920072&UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA'
  perl runners/amdocs/acc_mw_prober.pl --control SuspendSubscriber --data 'UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&MSISDN=6038920283&SuspendReason=LOST&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&ResourceData__Subscriber_ResourceData__ARRAYOF0__productCategory=SIM&ResourceData__Subscriber_ResourceData__ARRAYOF0__serialNumber=8901260541516428678&ResourceData__Subscriber_ResourceData__ARRAYOF1__productCategory=DEVICE&ResourceData__Subscriber_ResourceData__ARRAYOF1__serialNumber=996040891699759'
  perl runners/amdocs/acc_mw_prober.pl --control DeactivateSubscriber --data 'UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&MSISDN=3602241005&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&'
  perl runners/amdocs/acc_mw_prober.pl --control SuspendSubscriber --data 'UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&MSISDN=6038920283&SuspendReason=LOST&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&ResourceData__Subscriber_ResourceData__ARRAYOF0__productCategory=SIM&ResourceData__Subscriber_ResourceData__ARRAYOF0__serialNumber=8901260541516428678&ResourceData__Subscriber_ResourceData__ARRAYOF1__productCategory=DEVICE&ResourceData__Subscriber_ResourceData__ARRAYOF1__serialNumber=996040891699759'
  perl runners/amdocs/acc_mw_prober.pl --control ReactivateSubscriber --data 'UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&poolAccInd=1234&autoRenew=false&ContactData__preferredLanguage=en-US'
  perl runners/amdocs/acc_mw_prober.pl --control ActivateSubscriber --data 'ContactData__zipCode=10012&ContactData__AddressList__AddressInfo__ARRAYOF0__Address=123xyz&ContactData__AddressList__AddressInfo__ARRAYOF0__stateCode=NY&ContactData__AddressList__AddressInfo__ARRAYOF0__cityName=Newark&ContactData__AddressList__AddressInfo__ARRAYOF0__addressFormatType=S&PlanData__wholesalePlan=32&PlanData__retailPlanID=12001&poolAccInd=false&autoRenew=false&ContactData__preferredLanguage=en-US&UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&AmountData__specificationValue=45&AmountData__specificationValueName=INITIAL_BALANCE&autoRenew=false&PortInData__specificationValue=RE&PortInData__specificationValueName=PORTTYPE&ResourceData__Subscriber_ResourceData__ARRAYOF0__productCategory=DEVICE&ResourceData__Subscriber_ResourceData__ARRAYOF0__serialNumber=123451234512345&ResourceData__Subscriber_ResourceData__ARRAYOF1__productCategory=SIM&ResourceData__Subscriber_ResourceData__ARRAYOF1__serialNumber=8901260821100037212&'
  perl runners/amdocs/acc_mw_prober.pl --control SuspendSubscriber --data 'UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&MSISDN=2123650033&SuspendReason=LOST&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&ResourceData__Subscriber_ResourceData__ARRAYOF0__productCategory=SIM&ResourceData__Subscriber_ResourceData__ARRAYOF0__serialNumber=8901260821100037212&ResourceData__Subscriber_ResourceData__ARRAYOF1__productCategory=DEVICE&ResourceData__Subscriber_ResourceData__ARRAYOF1__serialNumber=310260820003721'
  perl runners/amdocs/acc_mw_prober.pl --control RestoreSubscriber --data 'UserData__senderId=MVNEACC&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA&OriginatorData__accountType=MVNE&OriginatorData__accountSubType=ULTRA&ResourceData__Subscriber_ResourceData__ARRAYOF0__productCategory=SIM&ResourceData__Subscriber_ResourceData__ARRAYOF0__serialNumber=8901260821100037212&ResourceData__Subscriber_ResourceData__ARRAYOF1__productCategory=DEVICE&ResourceData__Subscriber_ResourceData__ARRAYOF1__serialNumber=123451234512345&RestoreReason=LOST&MSISDN=2123650033'

|;
}


__END__


Goal:
  - Mock Inbound Notification requests from ACC.
  - Perform SOAP requests directly to ACC (Control items), passing from the ACC/MW Control Queue.

See AMDOCS-25.

Important: SIM is ICCID ; DEVICE is IMSI

      <tns:ResourceData>
        <tns:Subscriber_ResourceData>
          <tns:productCategory>
            SIM
          </tns:productCategory>
          <tns:serialNumber>
            8901260821100037212
          </tns:serialNumber>
        </tns:Subscriber_ResourceData>
        <tns:Subscriber_ResourceData>
          <tns:productCategory>
            DEVICE
          </tns:productCategory>
          <tns:serialNumber>
            123451234512345
          </tns:serialNumber>
        </tns:Subscriber_ResourceData>
      </tns:ResourceData>

