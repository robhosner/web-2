<?php

/**
 * the purpose of this runner is to populate notifications to DEV and QA
 * endpoints based on rules regarding their CorrelationIDs or lack thereof
 * See
 *  https://issues.hometowntelecom.com:8443/browse/API-302
 *  http://wiki.hometowntelecom.com:8090/display/SPEC/MW+redundancy+and+improvements
 *
 * Optional argument: a comma-delimited list of SOAP_LOG_ID
 */

require_once 'classes/NotificationPropagator.php';

$propagator = new \NotificationPropagator();
$propagator->run( $argv );

