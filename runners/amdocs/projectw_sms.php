<?php

/*

Example:

After Fer=bruary 1st, do
%> php runners/amdocs/projectw_sms.php day=1 max_count=2 dry_run=1

*/

require_once 'db.php';
require_once 'classes/ProjectW.php';
require_once 'lib/util-runners.php';

teldata_change_db();

$importObject = new \ProjectW();

$defaults = array(
  'day'       => array('type' => 'integer', 'default' => 0),
  'max_count' => array('type' => 'integer', 'default' => 20),
  'dry_run'   => array('type' => 'boolean', 'default' => FALSE)
);

$importObject->runSMSAfterMigration( \readNormalizedCommandLineArguments( $defaults , FALSE ) );

