<?php

/*

Project W Port-In Migration runner

See
https://issues.hometowntelecom.com:8443/browse/API-348
https://issues.hometowntelecom.com:8443/browse/API-355

http://wiki.hometowntelecom.com:8090/display/SPEC/Project+W+Migration
http://wiki.hometowntelecom.com:8090/display/ProjectW/Project+W+Plan+Configurations+-+New+Pricing
http://wiki.hometowntelecom.com:8090/pages/viewpage.action?title=Project+W+Balance+Reconciliation&spaceKey=ProjectW

 1 - Is runner enabled?

{ loop N iterations ( configurable )

 2 - Is runner blocked by resources?

 3 - Get next items to process

} end loop

*/

require_once 'db.php';
require_once 'classes/ProjectW.php';

teldata_change_db();

$importObject = new \ProjectW();

$importObject->run();

