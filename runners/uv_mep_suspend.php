<?php

/**
 * Univision Migration Extesnion Plan runner
 * suspend subs that overstayed in MEP
 * @see PJW-80, API-389
 */

require_once 'db.php';
require_once 'classes/MigrationExtensionPlan.php';


exit(main($argc, $argv));


/**
 * main
 */
function main($count, $arguments)
{
  logInfo('starting ' . __FILE__ . ' at ' . date('c'));

  teldata_change_db();
  $mep = new MigrationExtensionPlan;
  foreach ($mep->getExpiredMepCustomers() as $customer)
    if ( ! $mep->suspendRemoveCustomer($customer))
      logError("failed to suspend customer ID {$customer->CUSTOMER_ID} out of MEP");

  logInfo('finished ' . __FILE__ . ' at ' . date('c'));
  return 0;
}

