<?php

/* MVNO-700 */

require_once 'db.php';
include_once 'classes/Messenger.php';

// MVNO-2840
if (!\Ultra\UltraConfig\middleware_enabled_amdocs()) {
  dlog('', 'Middleware not enabled, exiting');
  exit;
}

date_default_timezone_set("America/Los_Angeles");

teldata_change_db();

if ( enabled_daily_init() )
{
  // provided if we want to run messenger on a single customer_id
  $single_customer_id = (isset($argv[2])) ? $argv[2] : null;

  $the_messenger = new Messenger([
    'initialize_day'     => 1,
    'single_customer_id' => $single_customer_id
  ]);
}
else
{
  $max_iterations     = max_iterations();

  if ( \Ultra\UltraConfig\messaging_runner_enabled() ) // EA-124
  {
    $the_messenger = new Messenger([
      'max_iterations' => $max_iterations
    ]);
  }
  else
  {
    dlog('', 'Messaging runner not enabled, exiting');
    exit;
  }
}

try
{
  $the_messenger->run();
}
catch(\Exception $e) { dlog('', 'ESCALATION ALERT IMMEDIATE - messaging runner fatal error - '.$e->getMessage()); }

function max_iterations()
{
  global $argv;

  $max_iterations = 55;

  if ( isset( $argv[1] ) && is_numeric($argv[1]) )
  {
    $max_iterations = $argv[1];
  }

  return $max_iterations;
}

