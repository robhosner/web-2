<?php

/*
Identical to internal__ResolvePendingTransitions

Purpose: Resolve all pending transitions
*/

require_once 'db.php';

$environments = ( isset($argv[1]) && $argv[1]             ) ? $argv[1] : NULL ;
$customer_id  = ( isset($argv[2]) && is_numeric($argv[2]) ) ? $argv[2] : NULL ;

if ( !is_null($environments) )
  $environments = explode(',',$environments);

$params = array(
  'environments' => $environments,
  'customer_id'  => $customer_id
);

teldata_change_db();

internal_func_resolve_pending_transitions($params);

?>
