#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;
use HTTP::Async;
use HTTP::Request;
use HTTP::Request::Common;
use JSON;


$|++;


# number of cycles to be processed
my $max_cycles = 0;

if ( scalar @ARGV )
{
  $max_cycles = shift @ARGV;
}

my $proto_url = shift @ARGV;

my @environments = @ARGV;

die "Syntax: MAX_CYCLES RESOLVER_URL MAIN_ENV [ENV2 ENV3 ...]"
 unless scalar @environments;

my $cycle = $max_cycles || 1;

my $async = HTTP::Async->new;

# main loop
while ( $cycle )
{
  process_next($proto_url, @environments);

  sleep 1;

  $cycle-- if $max_cycles;
}

while ( my $response = $async->wait_for_next_response )
{
  print $response->status_line . ' ' . $response->content . "\n";
  sleep 1;
}

sub debug
{
  print $_[0] if 0;
}

# process next request
sub process_next
{
 # TODO: respect HTT_ENV and look at the site config
  my $url = shift @_;
  my @env = @_;

  my $env = $env[0];

  my $site = 'rgalli2-dev.uvnv.com';
  my %sites = (
               rgalli2_dev             => 'rgalli2-dev.uvnv.com',
               rgalli3_dev             => 'rgalli3-dev.uvnv.com',
               seanapi_dev             => 'rgalli2-dev.uvnv.com',

               ultra_api_dev           => 'api-dev.ultra.me',

               tzz2_dev                => 'tzz2-dev.uvnv.com',

               ultra_api_prod_public   => 'live-api.ultra.me',
               ultra_api_prod_internal => 'live-api.ultra.me',
               ultra_api_prod_resolver => 'resolver-prod.ultra.me',
               epay_prod               => 'live-api.ultra.me',
               celluphone_prod         => 'live-api.ultra.me',
               rainmaker_prod          => 'live-api.ultra.me',
               rgalli4_dev             => 'live-api.ultra.me',
              );

  # only the 1st environment is validated - this could be improved
  if (exists $sites{$env})
  {
   $site = $sites{$env};
  }
  else
  {
   warn "Unknown target environment $env, using default $site";
  }

  $url =~ s,SITE,$site,g;

  my $data = 'request_epoch='.time;

  for my $e ( @env )
  {
    # environments is an array parameter
    $data .= '&environments[]='.$e;
  }

  user_agent_request($url,$data);
}

# perform request
sub user_agent_request
{
  my ($url,$data) = @_;

  my $headers = HTTP::Headers->new;

  $headers->authorization_basic('dougmeli','Flora');

  #print "$url\n".Dumper($data);

  my $request = new HTTP::Request 'POST', $url, $headers;
  $request->content_type('application/x-www-form-urlencoded');

  $request->content( $data );

  $async->add( $request );
}

__END__

The State Machine Runner: resolves transitions and actions


Usage:

  ./runners/state_machine/transition-runner.pl $MAX_CYCLES $PROTO_URL @ENVIRONMENTS


Example:

  perl runners/state_machine/transition-runner.pl 2 'https://dougmeli:Flora@SITE/pr/internal/1/ultra/api/internal__ResolvePendingTransitions' rgalli3_dev


internal__ResolvePendingTransitions responses:

# still open, will be done soon-ish ...
$VAR1 = {
          'warnings' => [],
          'success' => bless( do{\(my $o = 1)}, 'JSON::XS::Boolean' ),
          'errors' => [],
          'ultra_trans_id' => '',
          'ultra_trans_epoch' => 1349190102,
          'partner_tag' => 'partner_tag not given!',
          'open_transitions' => [
                                  '{1DA4FFBB-1E76-1E34-3CCE-540D1359E137}'
                                ],
          'transitions' => []
        };

# closed, yay!
$VAR1 = {
          'warnings' => [],
          'success' => bless( do{\(my $o = 1)}, 'JSON::XS::Boolean' ),
          'errors' => [],
          'ultra_trans_id' => '',
          'closed_transitions' => [
                                    '{1DA4FFBB-1E76-1E34-3CCE-540D1359E137}'
                                  ],
          'ultra_trans_epoch' => 1349190161,
          'partner_tag' => 'partner_tag not given!',
          'transitions' => []
        };

# ugh! aborted |[
$VAR1 = {
          'warnings' => [],
          'success' => bless( do{\(my $o = 1)}, 'JSON::XS::Boolean' ),
          'errors' => [
                        'ERR_API_INTERNAL: could not make external call'
                      ],
          'ultra_trans_id' => '',
          'ultra_trans_epoch' => 1349189113,
          'partner_tag' => 'partner_tag not given!',
          'aborted_transitions' => [
                                     '{3D03D189-E535-1B64-3A1B-46000FC33276}'
                                   ],
          'transitions' => []
        };

