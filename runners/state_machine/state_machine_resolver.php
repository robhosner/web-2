<?php

/*
 * Resolve pending transition created with Ultra/Lib/StateMachine
 */

require_once 'db.php';
require_once 'Ultra/Lib/StateMachine.php';
#require_once 'Primo/Lib/StateMachine/DataEngineMongoDB.php';
require_once 'Ultra/Lib/StateMachine/DataEngineMSSQL.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRAM.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRedis.php';

# Ultra specific actions and requirements
require_once 'Ultra/Lib/StateMachine/Action/functions.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';

//
class StateMachineRunner
{
  // TODO run on particular customer id
  private $max_iterations = 1;

  /**
   * main
   */
  public function main()
  {
    if ( ! $this->checkTransitionsEnabled())
    {
      \logit("execution halted since 'ultra/transitions/enabled' is set to FALSE");
      return;
    }

    teldata_change_db();

    // instantiate state machine
    $redis = new \Ultra\Lib\Util\Redis();
    $dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;
    
    $sm = new \Ultra\Lib\StateMachine( $dataEngineMSSQLObject , NULL , NULL , $redis );

    $transition = 'start';

    $i = 0;
    while ($i < $this->max_iterations && $transition)
    {
      // load next transition, wraps get_next_transition
      // get_next_transition reserves transition in redis
      $sm->loadTransitionData();
      if ($transition = $sm->getCurrentTransitionData())
      {
        // set customer data as array
        $customer = (array)get_customer_from_customer_id($transition['customer_id']);
        $sm->setCustomerData($customer);

        // set transition configuration
        $result = $sm->selectTransitionByLabel(
          $transition['transition_label'],
          $customer['plan_state'],
          get_plan_name_from_short_name(get_plan_from_cos_id($transition['from_cos_id']))
        );

        // continue here, couldn't find transition
        if ( ! $result)
        {
          \logError('ERROR could not find transition');
          continue;
        }

        // generation data for transition actions
        $sm->generateActionData();

        // loadTransitionData reserved customer
        if ( ! $sm->runTransition($reserve = FALSE) )
        {
          \logError('ERROR running transition');
          continue;
        }

        $removeAtKey = 'ultra/sortedset/transitions/' . $transition['htt_environment'] . '/' . $transition['priority'];
        $redis->zrem($removeAtKey, $transition['transition_uuid']);
      }

      $i++;
    }
  }

  /**
   * @return Bool
   */
  private function checkTransitionsEnabled()
  {
    $settings = new \Ultra\Lib\Util\Settings;
    return $settings->checkUltraSettings('ultra/transitions/enabled');
  }

  /**
   * @param  Integer
   * @return Null
   */
  public function setMaxIterations($max_iterations)
  {
    $this->max_iterations = $max_iterations;
  }
}

\logit('start execution');
$runner = new StateMachineRunner();
$runner->setMaxIterations(( isset($argv[1]) && is_numeric($argv[1]) ) ? $argv[1] : 1 );
$runner->main();
\logit('end execution');