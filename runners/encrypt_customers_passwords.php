<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Hash.php';
require_once('lib/util-common.php');

define('PASSWORD_UPDATE_LIMIT', 24);
define('REDIS_LAST_CUSTOMER', 'ultra/runners/encrypt_customers_passwords/last_customer_id');
define('REDIS_LAST_CUSTOMER_TTL', 60);

teldata_change_db();

dlog('', __FILE__ . ' START');

$redis = new \Ultra\Lib\Util\Redis;

$single_customer_id = ( isset($argv[1]) ) ? $argv[1] : NULL ;
$clause = NULL;

if ( ! $single_customer_id)
{
  $last_customer_id = $redis->get(REDIS_LAST_CUSTOMER);
  if ( ! $last_customer_id)
    $last_customer_id = 0;

  $next_customer_id = $last_customer_id + 1;

  dlog('', 'CUSTOMER_ID processed last : ' . $last_customer_id);
  dlog('', 'Begin processing customers at CUSTOMER_ID : ' . $next_customer_id);

  $customerId = $last_customer_id;
  $equals = 0;
}
else
{
  $customerId = $single_customer_id;
  $equals = 1;
}

$customer_select_query = sprintf(
  "EXEC [ULTRA].[GetCustomerLoginPasswords]
    @Top = %d,
    @Customer_ID = %d,
    @Equal = %d,
    @PasswordLen = %d",
  PASSWORD_UPDATE_LIMIT, $customerId, $equals, 65);

$customers = mssql_fetch_all_objects(logged_mssql_query($customer_select_query));

foreach ($customers as $customer)
{
  dlog('', 'Processing customer : ' . $customer->CUSTOMER_ID);

  $encrypted_password = \Ultra\Lib\Util\encryptPasswordHS($customer->LOGIN_PASSWORD);

  $customers_update_query = customers_update_query(array(
    'customer_id'    => $customer->CUSTOMER_ID,
    'login_password' => $encrypted_password
  ));

  if ( ! run_sql_and_check($customers_update_query))
  {
    dlog('', 'DB ERROR updating password for CUSTOMER_ID : ' . $customer->CUSTOMER_ID);
    exit;
  }

  dlog('', 'SUCCESSFUL processing CUSTOMER_ID : ' . $customer->CUSTOMER_ID);

  if ( ! $single_customer_id)
    $redis->set(REDIS_LAST_CUSTOMER, $customer->CUSTOMER_ID, REDIS_LAST_CUSTOMER_TTL);
}

dlog('', __FILE__ . ' STOP');
