<?php

/**
 * Dealer Activations Statistics runner
 * nightly job to compute dealer activations from the previous day
 * suggested execution time is 1AM to 2AM Pacific Time (no activaitions according to logs)
 * @see API-383
 * @author VYT 2016
 * logic:
 *   1. query all recent activations, the definition of 'recent' is dictated by ultra/locator/days/threshold and group by dealer, brand
 *   2. for each dealer check if the business is still active (to prevent saving activations for dealers that recently closed)
 *   3. insert a row into ULTRA.HISTORICAL_ACTIVATIONS_BY_BRAND for each dealer, brand
 */

require_once 'db.php';
require_once 'Ultra/Lib/DB/DealerPortalReporting.php';

exit(main($argc, $argv));


/**
 * main
 */
function main($argc, $argv)
{

  logInfo('starting ' . __FILE__ . ' at ' . date('c'));

  $settings = \Ultra\UltraConfig\getDealerLocatorThresholds();
  $cache = new \Ultra\Lib\DB\Cache(NULL, \Ultra\UltraConfig\find_config('ultra/celluphone/db/host'), \Ultra\UltraConfig\find_config('ultra/celluphone/db/name'));

  teldata_change_db();
  foreach (\Ultra\Lib\DB\DealerPortal\getNewActivationsStatistics($settings['LOCATOR_DAYS_THRESHOLD']) as $activations)
  {
    // validate that dealer is still open for business
    if ( ! $dealer = $cache->selectRow('TBLDEALERSITE', 'DEALERSITEID', $activations->DEALER_ID))
      logError("unable to find dealer ID {$activations->DEALER_ID}");

    // update statistics
    elseif ($dealer->ACTIVEFLAG && ! Ultra\Lib\DB\DealerPortal\saveActivationsStatistics($activations))
      logError("failed to update statistics for dealer {$activations->DEALER_ID}");
  }

  // remove statistics computed more than 12 hrs ago
  \Ultra\Lib\DB\DealerPortal\removeOldActivationsStatistics(time() - 12 * SECONDS_IN_HOUR);

  logInfo('finished ' . __FILE__ . ' at ' . date('c'));
  return 0;
}
