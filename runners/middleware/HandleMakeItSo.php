<?php

/*
Identical to middleware__HandleMakeItSo

Purpose: MakeItSo runner to perform MakeItSo tasks
*/

require_once 'db.php';
require_once 'Ultra/Lib/MVNE/MakeItSo.php';

$makeitso_queue_id = ( isset($argv[1]) ) ? $argv[1] : NULL ;

if ( \Ultra\UltraConfig\makeitso_enabled() )
{
  $makeItSo = new \Ultra\Lib\MVNE\MakeItSo();

  $result = $makeItSo->processNext($makeitso_queue_id);

  if ( $result->is_failure() )
    dlog('',"errors = %s",$result->get_errors());
}
else
  dlog('',"makeitso is disabled");

exit(0);

?>
