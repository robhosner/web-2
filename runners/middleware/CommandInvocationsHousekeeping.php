<?php

/*
Identical to middleware__CommandInvocationsHousekeeping

Purpose: invokes CommandInvocation::housekeeping for needed cleanups
*/

require_once 'db.php';
require_once 'classes/CommandInvocation.php';

// connect to ULTRA_ACC DB
\Ultra\Lib\DB\ultra_acc_connect();

$commandInvocation = new CommandInvocation();

$result = $commandInvocation->housekeeping();

if ( $result->is_failure() )
  dlog('',"errors = %s",$result->get_errors());

?>
