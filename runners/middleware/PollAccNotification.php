<?php

/*
Identical to middleware__PollAccNotification

Purpose: Extracts an Inbound Notification Message from the ACC MW Message queue (Perl notification listener) and process it.
*/

require_once 'db.php';
require_once 'Ultra/Lib/MQ/EndPoint.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

if ( ! \Ultra\UltraConfig\middleware_enabled_amdocs() )
{
  dlog('','middleware currently disabled');
}
else
{
  $endPoint = new \Ultra\Lib\MQ\EndPoint;

  // is there an inbound notification waiting in the ACC MW Notification Channel? (from the Perl notification listener)
  if ( $endPoint->peekNotificationChannelACCMW() )
  {
    // dequeue inbound ACC MW Notification Channel
    $message = $endPoint->dequeueNotificationChannelACCMW();

    if ( $message )
    {
      dlog('',"message = %s",$message);

      $data = $endPoint->extractFromMessage($message);

      if ( $data )
      {
        $data = (array) $data;

        dlog('',"data = %s",$data);

        $uuid = $data['_uuid'];

        $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Notification;

        $result = $accMiddleware->processNotification(
          array(
            'actionUUID' => $data['_actionUUID'],
            'uuid'       => $uuid,
            'command'    => $data['header'],
            'parameters' => $data['body']
          )
        );

        if ( $result->is_failure() )
          dlog('',"errors = %s",$result->get_errors());
      }
      else
        dlog('','ERR_API_INTERNAL: Cannot extract data from message');
    }
  }
}

?>
