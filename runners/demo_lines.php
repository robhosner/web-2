<?php

/**
 * Dealer Demonstration Line Runner
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Automation+of+Demo+Line+Program
 * @see DEMO-1
 * Logic:
 *    ensure that another runner process is not already running, hard kill it if found
 *    limit total run time to 10 min
 *
 *    execute daily eligibiligy check
 *      get eligible dealers who are EPP or have 10 or more activations this month as of yesterday
 *      loop for each dealer
 *        identify dealer type: EPP if dealer code starts with 'EPP', '1:1' for all others
 *        determine number of eligible lines
 *          EPP dealer: always 1 line, 2 lines for activations >= 20
 *          1:1 dealer: 1 line for activation >= 10, 2 lines for activations >= 20
 *        get all existing demo lines for this dealer
 *        loop for each eligible line
 *          if eligible line already exists and its status is 'eligible' and has expired then update status to 'unclaimed' unless dealer is EPP and required activations is 0
 *          if exiting line in in status 'warning' then update to 'active'
 *          if eligible line does not exist then add it with status 'eligible': if first half of the month, then claim by the end of the month, otherwise by the 10th of next month
 *
 *    execute monthly line status check on the first of the month
 *      get all dealers that have demo lines
 *      loop for each dealer
 *        identify dealer type: EPP if dealer code starts with 'EPP', '1:1' for all others
 *        determine number of eligible lines
 *          EPP dealer: always 1 line, 2 lines for activations >= 20
 *          1:1 dealer: 1 line for activation >= 10, 2 lines for activations >= 20
 *        get all existing lines
 *        loop for each existing line
 *          if line status is 'active' but it is not eligible then update status to 'warning' and send REMINDER SMS
 *          if line status is 'warning' then invalidate it and
 *            remove line from ULTRA.CUSTOMER_OPTIONS
 *            update line status to 'invalid'
 *            send CANCEL SMS
 *
 *    execute mid-month line status check on the 15th of each month
 *      get all dealers with lines in 'warning' status
 *      looop for each dealer
 *        identify line dealer type: EPP if dealer code starts with 'EPP', '1:1' for all others
 *        determine number of eligible lines
 *          EPP dealer: always 1 line, 2 lines for activations >= 20
 *          1:1 dealer: 1 line for activation >= 10, 2 lines for activations >= 20
 *        get all existing lines for this dealer
 *        loop for each existing line
 *          if line is not in status warning then queue up warning SMS
 */

require_once 'db.php';
require_once 'Ultra/Lib/DB/Schema.php';
require_once 'Ultra/Lib/DB/Celluphone/functions.php';
require_once 'Ultra/Lib/Util/ProcessControl.php';
require_once 'Ultra/Lib/DemoLine.php';

define('MAX_CPU_USAGE', 80); // quit if CPU usage > 80%
define('MAX_RUN_TIME',  600); // max process run time is 600 sec
define('MAX_RAM_REQUIRED', 10485760); // we require 10MB free RAM to execute

// main execution
$timezone = date_default_timezone_get();
dlog('', '%s: starting @ %s', __FILE__, date('c'));
if ( ! $result = initialize())
  exit($result);
if ( ! $result = main())
  exit($result);
date_default_timezone_set($timezone);
dlog('', '%s: finished @ %s', __FILE__, date('c'));
exit(0);


/**
 * initialize
 * runner process initialization
 * @return Boolean TRUE on success or FALSE on failure
 */
function initialize()
{
  $result = FALSE;

  try
  {
    // ensure that server load is within limits
    $control = new \Ultra\Lib\Util\ProcessControl(true);
    $cpu = $control->getCpuLoad();
    if ($cpu === NULL || $cpu > MAX_CPU_USAGE)
      throw new \Exception("CPU load $cpu is invalid or higher than " . MAX_CPU_USAGE);
    $free = $control->getFreeMemory();
    if ($free < MAX_RAM_REQUIRED)
      throw new \Exception("Free RAM $free is below required " . MAX_RAM_REQUIRED);

    // terminate any hung duplicate runners
    $hung = $control->getDuplicateProcesses();
    if ($hung === NULL)
      throw new \Exception('invalid duplicate process response');
    foreach ($hung as $pid)
    {
      $age = $control->getProcessAge($pid);
      if ($age === NULL)
        throw new \Exception("cannot get age of process $pid");
      if ($age > MAX_RUN_TIME)
      {
        dlog('', 'process %d exceeded maximum run time of %d, terminating', $pid, MAX_RUN_TIME);
        if ( ! $control->terminateProcess($pid))
          throw new \Exception("failed to terminate process $pid");
      }
    }

    dlog('', 'runner successfully initialized');
    $result = TRUE;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * main
 * runner main execution function
 * @return Boolean TRUE on success or FALSE on failure
 */
function main()
{
  $result = FALSE;

  try
  {
    $date = date('j');

    // daily check
    teldata_change_db();

    if ( ! $dealers = \Ultra\Lib\DemoLine\getEligibleDealers('daily'))
      throw new \Exception('failed to load dealers data');
    if ( ! \Ultra\Lib\DemoLine\dailyEligibilityCheck($dealers))
      throw new \Exception('failed daily eligibility check');

    // get todays' date and all active lines if executing at the start and midd of the month
    if ($date == 1 || $date == 15)
    {
      \Ultra\Lib\DemoLine\checkForUnclaimedLinesAndUpdate();

      if ( ! $lines = \Ultra\Lib\DemoLine\getActiveLines())
        throw new \Exception('no active lines found');

      if ($date == 1)
      {
        $dealers = \Ultra\Lib\DemoLine\getEligibleDealers('month');

        if ( ! \Ultra\Lib\DemoLine\startOfMonthCheck($dealers, $lines))
          throw new \Exception('failed start of month check');
      }

      if ($date == 15 && ! \Ultra\Lib\DemoLine\middleOfMonthCheck($dealers, $lines))
        throw new \Exception('failed start of month check');
    }

    $result = TRUE;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}



