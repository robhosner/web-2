<?php

/**
 * Command Invocation Automatic Recovery
 *
 * Loops through MISSING command invocations and attempts to resolve them
 * Usage: %> php runners/invocations_resync.php $NUM_ITERATIONS $COMMAND_INVOCATIONS_ID
 *
 * @author rgalli
 */


require_once 'db.php';
require_once 'classes/CommandInvocation.php';
require_once 'lib/util-runners.php';


$dir  = '/tmp';
$name = 'invocations_resync';

dlog('',"invocations_resync start ".time());

// create our PID file
if ( ! createPidFile($dir, $name) )
{
  dlog('',"failed to create PID file");
  exit;
}

$max_iterations = 5;
if ( isset( $argv[1] ) && is_numeric($argv[1]) )
  $max_iterations = $argv[1];

$command_invocations_id = NULL;
if ( isset( $argv[2] ) && is_numeric($argv[2]) )
  $command_invocations_id = $argv[2];

invocations_resync_main( $max_iterations , $command_invocations_id );

unlinkPidFile($dir, $name);

dlog('',"invocations_resync end ".time());


exit;


/**
 * invocations_resync_main
 *
 * @return NULL
 */
function invocations_resync_main( $max_iterations , $command_invocations_id=NULL )
{
  dlog('',"max_iterations = $max_iterations");

  // connect to ULTRA_ACC DB
  \Ultra\Lib\DB\ultra_acc_connect();

  $id = 1; # dummy

  while( $max_iterations-- && $id )
  {
    $id = invocation_resync( $command_invocations_id );

    if ( $id )
      dlog('',"processed command_invocations_id = $id");
  }

  return NULL;
}

/**
 * invocation_resync
 *
 * a) Selects a random 'MISSING' invocation, verifying that it's not locked yet
 * b) Does a QuerySubscriber on that customer.
 * c) If CurrentAsyncService is NOT EMPTY, move on.
 * d) If CurrentAsyncService is EMPTY then set STATUS='RESOLVED', TICKET_DETAILS='MISSING resolved by Invocations Resync' 
 *
 * @return integer
 */
function invocation_resync( $command_invocations_id=NULL )
{
  $commandInvocation = new CommandInvocation();

  if ( $command_invocations_id )
  {
    // the user required this specific command_invocations_id to be processed

    $result = $commandInvocation->loadPending( array( 'command_invocations_id' => $command_invocations_id ) );

    dlog('',"commandInvocation = %s",$commandInvocation);

    if ( $commandInvocation->status != 'MISSING' )
    {
      dlog('',"status is not MISSING");

      return 0;
    }

    perform_command_invocation_resync( $commandInvocation );

    return $command_invocations_id;
  }
  elseif ( $commandInvocation->loadRandomMissing() )
  {
    // process a random command_invocation

    perform_command_invocation_resync( $commandInvocation );

    return $commandInvocation->command_invocations_id;
  }
  else
    return 0;
}

/**
 * perform_command_invocation_resync
 *
 * @return NULL
 */
function perform_command_invocation_resync( $commandInvocation )
{
  if ( empty( $commandInvocation->command_invocations_id ) )
  {
    dlog('',"no command_invocations_id in object");

    return NULL;
  }

  $redis = new \Ultra\Lib\Util\Redis;

  $redis_key = 'ultra/invocation/resync/'.$commandInvocation->command_invocations_id;

  if ( ! $redis->get( $redis_key ) )
  {
    $redis->set( $redis_key , 1 , 60 * 60 * 2 ); // lock for 2 hours

    $result = $commandInvocation->resynchronize( 'invocations_resync.php' , 'MISSING resolved by invocations_resync.php' );

    if ( $result->is_failure() )
      dlog('',"errors = %s",$result->get_errors());
    else
      dlog('','resynchronize success!');
  }
}

?>
