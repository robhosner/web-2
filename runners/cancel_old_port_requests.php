<?php

/**
  * AMDOCS-231
  *
  * runner to cancel port-in requests older than X days
  *
  * @example php runners/cancel_old_port_requests.php [14]
  * @params int days, default is 14
  * @result int error code
  * @author VYT 2014-03-27
  */


require_once('db.php');
require_once('lib/util-common.php');

// defaults
define('CANCEL_PERIOD', 13); // cancel requests older than so many days if no parameters given

// init
$days = empty($argv[1]) ? CANCEL_PERIOD : $argv[1];
$count = 0; // how many customers processed
$trans = 0; // how many customers transitioned to a new state
$warn = 0; // warnings encountered
dlog('', "INFO: begin processing port-in customers $days days old");

// cancel old port requests
$acc = \Ultra\Lib\DB\ultra_acc_connect();
$result = cancel_old_port_requests($days);
if ($result->is_failure())
{
  dlog('', 'ERROR: Failed call to cancel_old_port_requests');
  exit(1);
}

// get all MVNE 2 port-in customers created $days or more ago
$dbc = teldata_change_db();
$customers = get_old_portin_customers($days, 2);
if (! count($customers))
{
  dlog('', "INFO: no port-in customers found");
  exit(0);
}

// process each customer
foreach($customers as $customer)
{
  dlog('', 'INFO: processing customer ' . $customer->CUSTOMER_ID);

  // get customer's recent transitions from 'Neutral' to 'Port-In Requested'
  $select = \Ultra\Lib\DB\makeSelectQuery(
    'HTT_TRANSITION_LOG', // table
    NULL, // limit
    NULL, // select
    array( // where
      'CUSTOMER_ID' => $customer->CUSTOMER_ID,
      'FROM_PLAN_STATE' => 'Neutral',
      'TO_PLAN_STATE' => 'Port-In Requested',
      'CREATED > DATEADD(DAY, -' . ($days - 1) . ', GETUTCDATE())'));
  if (empty($select))
    dlog('', 'ERROR: failed to create SQL query for customer ' . $customer->CUSTOMER_ID);
  else
  {
    // transition customer to a new state based on current state
    $transitions = mssql_fetch_all_objects(logged_mssql_query($select));

    if (! count($transitions))
    {
      $context = array('customer_id' => $customer->CUSTOMER_ID);
      $customer->cos_id = $customer->COS_ID; // correct schema inconsistencies
      $result = NULL;

      $resultCheck = mvneCustomerActiveCheck( $customer );

      $msisdn = ( $customer->current_mobile_number ) ? $customer->current_mobile_number : NULL ;
      $iccid  = ( $customer->CURRENT_ICCID_FULL    ) ? $customer->CURRENT_ICCID_FULL    : NULL ;

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $resultCheck = $mwControl->mwQuerySubscriber(
        array(
          'actionUUID' => getNewActionUUID('cancel_old_port_requests ' . time()),
          'msisdn'     => $msisdn,
          'iccid'      => $iccid
        )
      );

      if ( $resultCheck->is_success()
        && isset($resultCheck->data_array['body'])
        && ( property_exists( $resultCheck->data_array['body'] , 'SubscriberStatus' ) )
        && in_array( $resultCheck->data_array['body']->SubscriberStatus , array( 'Active' , 'Suspended' ) )
      )
        dlog('','The customer won\'t be cancelled since it is active on the network');
      elseif ($customer->plan_state == 'Port-In Requested')
        $result = change_customer_state($customer, $context, 'Port-In Denied');
      elseif ($customer->plan_state = 'Port-In Denied')
        $result = cancel_account($customer, $context, NULL, NULL, 'port-in expired', __FILE__);
      else
        dlog('','ERROR: unexpected PLAN_STATE ' . $customer->plan_state . ' for customer ' . $customer->CUSTOMER_ID);

      // check for errors
      if (! empty($result) && count($result['errors']))
      {
        foreach($result['errors'] as $error)
          dlog('', "ERROR: $error");
        $warn++;
      }
      else
        $trans++;
    }
    else
      dlog('', 'INFO: customer ' . $customer->CUSTOMER_ID . ' has ' . count($transitions) . ' recent transition(s)');
  }

  $count++;
}

dlog('', "INFO: finished processing $count customers, $trans were transitioned");
exit(0);

?>
