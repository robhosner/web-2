<?php

require_once 'db.php';
require_once 'classes/WebPosRunner.php';

usleep(((int)substr((string)crc32( gethostname() ), 0, 2))*10000);

// if a UUID is provided to the runner, process only one cycle for the given UUID
$uuid = NULL;
if ( isset( $argv[1] ) && $argv[1] )
  $uuid = $argv[1];

$webPosRunner = new WebPosRunner();
$webPosRunner->main($uuid);
