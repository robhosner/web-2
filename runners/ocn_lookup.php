<?php

// USAGE: ocn_lookup.php [$FROM_HRS_AGO] [$TO_HRS_AGO] [$MAX_RECORDS]
// e.g.default: ocn_lookup.php 24 0 100

include_once 'db.php';
include_once('lib/portal/functions.php');

// defaults
define('FROM_HRS_AGO', 24); // default if missing input
define('TO_HRS_AGO', 0); // default if missing input
define('MAX_RECORDS', 1000); // max number of records to fetch
define('QUERY_TTL', 72000); // do not query MSISDN more often than this (sec)
define('REDIS_PREFIX', 'ultra/ocn/msisdn/'); // redis key to keep track of queries

// process input
$from = empty($argv[1]) || ! is_numeric($argv[1]) ? FROM_HRS_AGO : $argv[1];
$to   = empty($argv[2]) || ! is_numeric($argv[2]) ? TO_HRS_AGO : $argv[2];
$max  = empty($argv[3]) || ! is_numeric($argv[3]) ? MAX_RECORDS : $argv[3];

// correct reversed times
if ($from < $to)
  list($from, $to) = array($to, $from);

// convert relative hours to datetime strings
$from = date('M d Y h:i:sA', time() - $from * SECONDS_IN_HOUR);
$to = date('M d Y h:i:sA', time() - $to * SECONDS_IN_HOUR);
dlog('', "Date range: $from - $to");

// prepare query on HTT_CANCELLATION_REASONS
$query = 'SELECT TOP ' . $max . ' r.CUSTOMER_ID, r.MSISDN, l.CLOSED '.
  'FROM HTT_CANCELLATION_REASONS r ' .
  'JOIN HTT_TRANSITION_LOG       l ON r.CUSTOMER_ID = l.CUSTOMER_ID ' .
  "WHERE l.CLOSED BETWEEN '$from' AND '$to' " .
  "AND   l.TO_PLAN_STATE = 'Cancelled'
   AND   LEN(r.MSISDN) in (10, 11) 
   AND   r.PORTOUT_OCN_REQUEST_DATE is NULL";

// init
$redis = new \Ultra\Lib\Util\Redis;
$count = 0; // how many MSISDN processed
$skip  = 0; // how many MSISDN skipped
$start = time();

// get records
teldata_change_db();
$records = mssql_fetch_all_rows(logged_mssql_query($query));
if (! $records || ! count($records))
  dlog('', 'ERROR: no activated subscribers found.');

// process each row
foreach($records as $row)
{
  // init values
  $customer = $row[0];
  $msisdn = normalize_msisdn($row[1], TRUE);

  $date = $row[2];
  $count++;

  // check if MSISDN was previously queried
  $key = REDIS_PREFIX . $msisdn;
  if ( ! $redis->get($key) )
  {
    dlog('',"Will lookup $msisdn (".$customer.")");

    // query comcetera.com
    $info = funcCarrierLookup(array('msisdn_list' => $msisdn));

    // check result (http://www.numberportabilitylookup.com/api)
    if ($info && ! count($info['errors']) && ! $info['timeout'] && isset($info['portability']) && is_array($info['portability']))
    {
      // ignore results with error codes returned as ERRnn in the first data field
      if (substr($info['portability'][0][$msisdn][0], 0, 3) != 'ERR')
      {
        // check received values (often empty) and prepare query parameters
        $values = array('PORTOUT_OCN_REQUEST_DATE' => $date);
        if (! empty($info['portability'][0][$msisdn][0]))
          $values['PORTOUT_OCN'] = $info['portability'][0][$msisdn][0];
        if (! empty($info['portability'][0][$msisdn][1]))
          $values['PORTOUT_CARRIER_NAME'] = $info['portability'][0][$msisdn][1];

        dlog('', "values = %s", $values); // {"PORTOUT_OCN_REQUEST_DATE":"Mar  3 2014 12:20:31:490AM","PORTOUT_OCN":"6006","PORTOUT_CARRIER_NAME":"VERIZON WIRELESS-CA"}

        // create and execute query and check result
        $query = \Ultra\Lib\DB\makeUpdateQuery('HTT_CANCELLATION_REASONS', $values, array('CUSTOMER_ID' => $customer));
        if ($query)
        {
          $result = run_sql_and_check_result($query);
          if ($result->is_failure())
          {
            $errors = $result->get_errors();
            dlog('', 'Failed to update HTT_CANCELLATION_REASONS: ' . (count($errors) ? $errors[0] : NULL));
          }
        }
      }
      else
        dlog('', "WARNING: received {$info['portability'][0][$msisdn][0]} for MSISDN $msisdn");
    }

    // set redis key
    $redis->set($key, TRUE, QUERY_TTL);
  }
  else
    $skip++;
}

// give some statistics
dlog('', 'DONE in ' . (time() - $start) . " sec: processed $count records, skipped $skip records, range $from - $to");

?>
