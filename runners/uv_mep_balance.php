<?php

/**
 * Univision Migration Extesnion Plan runner
 * adjust balances on newly migrated MEP subs
 * suggested execution time: after daily MEP migration
 * @see PJW-80, API-387
 */

require_once 'db.php';
require_once 'classes/MigrationExtensionPlan.php';


exit(main($argc, $argv));


/**
 * main
 */
function main($count, $arguments)
{
  logInfo('starting ' . __FILE__ . ' at ' . date('c'));

  teldata_change_db();
  $mep = new MigrationExtensionPlan;
  foreach ($mep->getNewMepCustomers() as $record)
    if ($customer = $mep->fetchCustomerRecord($record->CUSTOMER_ID))
    {
      $mep->adjustIntlBalance($customer);
      $mep->adjustDataBalance($customer);
    }

  logInfo('finished ' . __FILE__ . ' at ' . date('c'));
  return 0;
}
