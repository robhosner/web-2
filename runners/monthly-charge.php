<?php

/*
  MRC: Monthly Runner Charge
  @see http://wiki.hometowntelecom.com:8090/display/SPEC/Process+%3A+Monthly+Service+Charge
  @see MVNO-779
  @params list of value=name pairs:
    cancellation: boolean (accepted values are 'true', 'yes', '1')
      if set process Cancellation for customers which are 2 months in 'Suspended' state
      default is FALSE
    date: string
      today: process Active customers for which plan_expires < today
      dd-mm-yyyy: process Active customers for which plan_expires < today and date(plan_expires) is dd-mm-yyyy
      default is 'today'
    daily_init: boolean (accepted values are 'true', 'yes', '1')
      if set then execute group initialization only; otherwise process customers prepared in the initialization step
      default is FALSE
    customer_id_list: CSV
      a comma-separated list of customer ids (no spaces!)
      default is NULL (all customers)
    groups: CSV
      MRC groups to process
      default: -1 (old logic: no groups)
    max_customers: integer
      max number of customer to be processed on this run
      default is NULL (all customers)
    retry: boolean (accepted values are 'true', 'yes', '1')
      if set, process RETRY_CC instances in monthly service charge log
      default is FALSE
    skip_cc: boolean (accepted values are 'true', 'yes', '1')
      if set we skip all CC attempts (this is when we know that WEBCC is down)
      default is FALSE
  @usage
    initialize customers expiring tomorrow
    php runners/monthly-charge.php date=today daily_init=true
    php runners/monthly-charge.php date=25-04-2015 customer_id_list=12345,23456,98745 max_customers=3 skip_cc=true cancellation=false retry=true

*/

require_once 'lib/mrc/functions.php';

date_default_timezone_set("America/Los_Angeles");

$settings = NULL;

// delay execution to ensure multiple runners on different hosts do not start simultaneously
usleep( ( (int) substr( (string) crc32(gethostname() ), 0, 2) ) * rand(1000,30000) );

$start = time();
dlog('', "monthly-charge.php start %d", $start);
main( input_values() );
$end = time();
dlog('', "monthly-charge.php end   %d (%d sec)", $end, $end - $start);
