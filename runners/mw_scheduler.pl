#!/usr/bin/perl


use warnings;
use strict;


use Data::Dumper;
use HTTP::Request::Common;
use HTTP::Async;
use HTTP::Request;
use JSON;
use Ultra::Lib::Util::Config;


$|++;


my $maxExecutionSeconds = 60;


my $config = Ultra::Lib::Util::Config->new();


my ( $apiName , $delay , $repetitions ) = @ARGV;


die "Invalid parameters" unless $apiName;
$delay       ||= 20; # seconds
$repetitions ||=  1;


# which API to launch and how many seconds to sleep between calls
my $executionConfiguration = [ $apiName ];

while( --$repetitions )
{
  push( @$executionConfiguration, ( $delay , $apiName ) );
}


mwSchedulerMain( $executionConfiguration , $maxExecutionSeconds , $config );


# main middleware scheduler task
sub mwSchedulerMain
{
  my ( $executionConfiguration , $maxExecutionSeconds , $config ) = @_;

  my $startTime = time;

  my $async = HTTP::Async->new;

  for my $configEntry( @$executionConfiguration )
  {
    mwSchedule( $configEntry , $config , $async );

    #TODO: check $maxExecutionSeconds
  }

  my $endTime   = time;

  print "mwSchedulerMain ended after ".( $endTime - $startTime )." seconds\n";

  while ( my $response = $async->wait_for_next_response )
  {
    print Dumper( $response->status_line . ' ' . $response->content );
    sleep 1;
  }
}

# proceed with a scheduled task
sub mwSchedule
{
  my ( $configEntry , $config , $async ) = @_;

  if ( $configEntry =~ /^\d+$/ )
  {
    #print "sleep $configEntry\n";
    sleep $configEntry;
  }
  else
  {
    mwApiCall( $configEntry , $config , $async );
  }
}

sub getUrl
{
  my ( $config , $apiName , $username , $password ) = @_;

  #return 'https://dougmeli:Flora@'.$config->envConfigValue('www/sites').'/pr/middleware/1/ultra/api/middleware__'.$apiName;

  return 'https://'.$username.':'.$password.'@'.$config->envConfigValue('www/sites').'/pr/middleware/1/ultra/api/middleware__'.$apiName;
}

sub getCredentials
{
  my ( $config ) = @_;

  # TODO: implement this in ht.cf if possible
  my $urlCredentials =
  {
    'rgalli3-dev.uvnv.com'     => [ 'dougmeli'      , 'Flora'             ],
    'rgalli-acc-dev.ultra.me'  => [ 'rgalli-acc-dev', 'f879d47151f35dec!' ],
    'mw-aspider-prod.ultra.me' => [ 'ultra_mw_prod' , 'f879d47151f35dec!' ],
  };

  if ( ! exists $urlCredentials->{ $config->envConfigValue('www/sites') } )
  {
    die "Credentials not available for environment ".$config->envConfigValue('www/sites');
  }

  my ( $username , $password ) = @ { $urlCredentials->{ $config->envConfigValue('www/sites') } } ;

  return ( $username , $password );
}

# perform an API call
sub mwApiCall
{
  my ( $configEntry , $config , $async ) = @_;

  my $apiName = $configEntry;

  my $params = "";

  if ( $configEntry =~ /^(\S+)\|(.+)$/ )
  {
    $apiName = $1;
    $params  = $2;
  }

  $params .= "&partner_tag=".time;

  my ( $username , $password ) = getCredentials( $config );

  my $url = getUrl( $config , $apiName , $username , $password );

  print "$url\n";

  my $h = HTTP::Headers->new;

  $h->authorization_basic( $username , $password );

  my $r = new HTTP::Request 'POST', $url, $h;
  $r->content_type('application/x-www-form-urlencoded');
  $r->content( $params );

  $async->add( $r );
}

__END__

This runner schedules MW pollers:

 - MVNE2 :
   - ACC_MW_NOTF   ( Amdocs MW <- ACC MW MQ   ) middleware__PollAccNotification
   - ULTRA_MW_CTRL ( Amdocs MW -> ULTRA MW MQ ) middleware__PollUltraControl
   - ULTRA_MW_NOTF ( Ultra     <- Ultra MW MQ ) middleware__PollUltraNotification



Many instances of this runner can be executed in parallel.


Usage:

./runners/mw_scheduler.pl $API $SECONDS_DELAY $REPETITIONS


Examples:

perl runners/mw_scheduler.pl 'PollAspMWOutbound|communication=synchronous' 1 1

sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev runners/mw_scheduler.pl PollAspMWInbound 10 2'

