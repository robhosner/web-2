<?php

/**
 * Promo SMS Broadcast Runner
 *
 * a) loads an active campaign ( STATUS = 'ONGOING' ) - if there are more than one active campaigns, loads a random one
 * b) exit if not between 2:30 AM PST and 11:30 PM PST
 * c) run a thread
 *
 * ULTRA.PROMO_BROADCAST_CAMPAIGN.STATUS
 *  - NEW       Just added to DB. Not yet ready to run, it should be initialized first.
 *  - READY     Initialized successfully. Not yet enabled to run.
 *  - ONGOING   Initialized successfully. Enabled to run
 *  - PAUSED    Initialized successfully. Paused.
 *  - ERROR     Something went wrong. Not enabled to run.
 *  - ABORTED   Aborted by user. Not enabled to run.
 *  - COMPLETED Yay!
 *
 * for cleaning of old campain files pass parameter 'mode=clean'
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+Framework
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Promo SMS Broadcast
 */


require_once 'db.php';
require_once 'classes/PromoBroadcast.php';
require_once 'lib/util-common.php';
require_once 'lib/util-runners.php';

// MVNO-2840
if (!\Ultra\UltraConfig\middleware_enabled_amdocs())
{
  dlog('', 'Middleware not enabled, exiting');
  exit;
}

// MVNO-2662
if (!\Ultra\UltraConfig\promo_sms_campaign_enabled())
{
  dlog('', 'Promo SMS campaign runner not enabled, exiting');
  exit;
}

// exit if not between 2:30 AM PST and 11:30 PM PST
if ( timeAllowed() )
{
  promoBroadcast();
}
else
  dlog('',"Promo Broadcast not running between 5:30 AM EST and 11:30 PM PST");

exit;


function promoBroadcast()
{
  $params = readCommandLineArguments(array('mode' => 'run'));

  $promoBroadcastObject = new PromoBroadcast();

  if (in_array($params['mode'], array('clean', 'cleanse')))
  {
    \logInfo('entering cleaning mode');
    return $promoBroadcastObject->removeAgedFiles();
  }

  // connect to DB
  teldata_change_db();

  $result = $promoBroadcastObject->loadOngoingCampaign(
    array(
      'time_hours'   => sprintf("%02s",date("H")),
      'time_minutes' => sprintf("%02s",date("i"))
    )
  );

  if ( $result->is_success() )
  {
    $result = $promoBroadcastObject->runCampaign();

    dlog('',"result = %s",$result);

    if ( $result->is_failure() )
      dlog('',"%s",$result->get_errors());
  }
  else
    dlog('',"Nothing to do");
}

// exit if not between 2:30 AM PST and 11:30 PM PST
//                     5:30 AM EST and  2:30 AM EST (next day)
function timeAllowed()
{
  $minutes = ( ( date("H") * 60 ) + date("i") );

  dlog('',"H = %s , i = %s , minutes = %s",date("H"),date("i"),$minutes);

  return ! ! ( ( $minutes > 150 ) && ( $minutes < 1410 ) );
}


/*
to test:

sudo su service /bin/bash -c 'sh /scripts/promo_broadcast_web_dev_01.sh'

*/

?>
