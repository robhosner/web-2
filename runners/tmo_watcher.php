<?php

/*

https://issues.hometowntelecom.com:8443/browse/TT-3

Iterates over a list of customer ids, invoking mwGetMVNEDetails for each of them with action_uuid = "GetMVNEDetails-$date-$filename-$customer_id"
 - Files containing those lists should be placed under /var/tmp/Ultra/TMOWatcher/ in the same server where this script will run
 - Please keep files size small
 - Please keep file names small and consistent
 - File extension should be csv
File format example:
====================
123
234
[...]
987
====================
The directory should be readable and writeable for the caller

Usage ( $FILENAME not required , full path needed if provided ):
%> php runners/tmo_watcher.php $FILENAME

*/

require_once 'db.php';
require_once 'lib/util-common.php';
require_once 'classes/TMOWatcher.php';

if ( ! \Ultra\UltraConfig\middleware_enabled_amdocs() )
{
  dlog('', 'Middleware not enabled, exiting');
  exit;
}

tmo_watcher( $argv );

/**
 * tmo_watcher
 *
 * Get a csv file in PROMO_BROADCAST_WORK_DIRECTORY and for each customer id in the file, invokes mwGetMVNEDetails
 */
function tmo_watcher( $argv )
{
  // connect to DB
  teldata_change_db();

  $tmoWatcherObject = new TMOWatcher();

  $fileName = empty($argv[1]) ? NULL : $argv[1] ;

  $success = $tmoWatcherObject->processNextFile( $fileName );

  if ( $success )
    dlog('',"run ok");
  else
    dlog('',"run failed");
}



