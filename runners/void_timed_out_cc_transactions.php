<?php

/*
  void_time_out_cc_transactions
  see: MVNO-2969

  Voids instances in which the API timed-out but the MeS transaction went through
  resulting in the credit card being charged but funds not being added to the account.

  Candidates to be voided are transactions ( ULTRA.CC_TRANSACTIONS ) with TRANSACTION_ID set to NULL
 */

require_once 'db.php';
require_once 'Ultra/Lib/MeS.php';
require_once 'Ultra/Lib/Util/Redis/functions.php';

teldata_change_db();

// where TRANSACTION_ID IS NULL, STATUS = COMPLETE, RESULT != FAILED, older than 15 minutes
$transactions = get_ultra_cc_timed_out_transactions();

$redis = new \Ultra\Lib\Util\Redis();

if ( count($transactions) )
  dlog('',"%d transactions to be voided",count($transactions));

foreach ($transactions as $transaction)
{
  if (!isset($transaction->MERCHANT_TRANSACTION_ID) || empty($transaction->MERCHANT_TRANSACTION_ID)) 
  {
    dlog('', 'MERCHANT_TRANSACTION_ID is not defined.');
    continue;
  }

  // skip if VOID attempt was made recently
  if (\Ultra\Lib\Util\Redis\get_transaction_voided_attempt($transaction->MERCHANT_TRANSACTION_ID, $redis))
  {
    dlog('', 'Void already attempted with MERCHANT_TRANSACTION_ID : ' . $transaction->MERCHANT_TRANSACTION_ID);
    continue;
  }

  dlog('', "invoking void for MERCHANT_TRANSACTION_ID = %s",$transaction->MERCHANT_TRANSACTION_ID);

  $customer = get_ultra_customer_from_customer_id($transaction->CUSTOMER_ID, array('BRAND_ID'));

  if ( ! $customer || ! $customer->BRAND_ID)
  {
    dlog('', 'Could not find BRAND ID for transaction ' . $transaction->MERCHANT_TRANSACTION_ID);
    continue;
  }

  // VOID transaction on MeS
  $result = \Ultra\Lib\MeS\void(array(
    'transaction_id'          => $transaction->MERCHANT_TRANSACTION_ID,
    'client_reference_number' => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID)
  ));

  $errors = $result->get_errors();

  if (count($errors))
    dlog('', $errors[0]);

  if ($result->is_failure())
    dlog('', 'Failed voiding transaction with MERCHANT_TRANSACTION_ID : ' . $transaction->MERCHANT_TRANSACTION_ID);

  if ($result->is_success())
  {
    dlog('', "Successful void for MERCHANT_TRANSACTION_ID = %s",$transaction->MERCHANT_TRANSACTION_ID);

    // set VOID attempt TTL = 60 * 60 * 24 * 2
    \Ultra\Lib\Util\Redis\set_transaction_voided_attempt($transaction->MERCHANT_TRANSACTION_ID, $redis);

    // insert VOID transaction record
    $sql = ultra_cc_transactions_insert_query(array(
      'cc_holder_tokens_id'       => $transaction->CC_HOLDER_TOKENS_ID,
      'customer_id'               => $transaction->CUSTOMER_ID,
      'charge_amount'             => $transaction->CHARGE_AMOUNT,
      'type'                      => 'VOID',
      'description'               => $transaction->DESCRIPTION,
      'cc_transactions_id_parent' => $transaction->CC_TRANSACTIONS_ID,
      'completed_date_time'       => 'now'
    ));

    if (!run_sql_and_check($sql))
    {
      dlog('','ERROR: failed to insert ULTRA.CC_TRANSACTIONS');
      exit;
    }
  }
}

