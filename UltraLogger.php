<?php

// SRV-497: log4php can be installed in /opt/Logger (incorrect) or /usr/share/pear/log4php (correct)
require_once (file_exists('/opt/Logger') ? '' : 'log4php/') . 'Logger.php';
require_once (file_exists('/opt/Logger') ? '' : 'log4php/') . 'LoggerLevel.php';

/**
 * @author John Dohoney
 * @version 1.0
 * @since   August 20, 2013
 * @package UltraLogger
 *
 * @abstract This is a wrapper class on log4php, for the Ultra Environment, to properly use this class, the shell
 * environment variable, LOGGERCONFIG, must point to the config.xml, which is located in the web/config
 * directory.  This can be used as a stand-alone class, or as an API.
 */
class UltraLogger
{
    /**
     *
     * @var object $log -- Instance Variable to hold logger
     */
    private $log;
    /**
     *
     * @var string $context -- Instance variable to hold the name of the context
     */
    private $context;
    /**
     *
     * @var boolean $traceCaller -- Enables tracing of the caller
     */
    private $traceCaller;

    /**
     *
     * @var boolean $fromAPI -- used to determing tracing level for reporting a call using the API
     */
    private $fromAPI;
    /**
     * Brackets the use with "BEGIN...END" for debugging
     * @var boolean $logBeginEnd -- Brackets the scope with BEGIN...END
     */
    private $logBeginEnd;


    /**
     *
     * @param string $contextName -- A context name, identifier, anything you require to help search in logs
     * @param boolean $logBeginEnd -- Bracket a group of messages with Begin/End
     * @param boolean $traceCaller -- print out the name and line number of callers
     * @param boolean $fromAPI -- Used by API authors for calls from an API to get the proper trace level
     */
    function  __construct ($contextName = "", $logBeginEnd=FALSE, $traceCaller=FALSE, $fromAPI=FALSE)
    {
        Logger::configure('config.php');

        $this->logBeginEnd = $logBeginEnd;
        $this->context     = $contextName;
        $this->traceCaller = $traceCaller;
        $this->log = Logger::getLogger("UltraLogger");

        if ($this->logBeginEnd === TRUE)
        {
            if ($this->traceCaller === TRUE)
            {
               $beginBracket = sprintf("BEGIN(%s) ", $this->context);
               $this->log->fatal($beginBracket);
            }
            else {
                $beginBracket = sprintf("BEGIN(%s) ", $this->context);
                $this->log->fatal($beginBracket);
            }
        }
        else {
            if ($this->traceCaller === TRUE)
            {
                $beginBracket = sprintf("(%s) ", $this->context);
                $this->log->fatal($beginBracket);
            }
        }
    }

    /**
     *
     */
    function __destruct ()
    {
        if ($this->logBeginEnd === TRUE)
        {
            if ($this->traceCaller === TRUE)
            {
                $endBracket = sprintf("END(%s) ", $this->context);
                $this->log->fatal($endBracket);
            }
            else {
                $endBracket = sprintf("END(%s)", $this->context);
                $this->log->fatal($endBracket);
            }
        }
        else {
            if ($this->traceCaller === TRUE)
            {
                $endBracket = sprintf("(%s) ", $this->context);
                $this->log->fatal($endBracket);
            }
        }
    }

    /**
     * @abstract Changes the debugging level, the default is DEBUG.
     * Valid values are
     * LoggerLevel::OFF -- Off,
     * LoggerLevel::FATAL -- Fatal Errors only,
     * LoggerLevel::ERROR -- Error and Fatal only,
     * LoggerLevel::DEBUG -- Debug, Info, Warnings, Errors, and Fatal
     * @param const $loggingLevel
     */
    public function setLoggerLevel($loggingLevel = LoggerLevel::DEBUG)
    {
        $root = $this->log->getRootLogger();

        switch ($loggingLevel)
        {
            case LoggerLevel::OFF:
                $this->log->setLevel(LoggerLevel::getLevelAll());
                $root->setLevel(LoggerLevel::getLevelAll());
            break;

            case LoggerLevel::FATAL:
                $this->log->setLevel(LoggerLevel::getLevelFatal());
                $root->setLevel(LoggerLevel::getLevelFatal());
            break;

            case LoggerLevel::ERROR:
                $this->log->setLevel(LoggerLevel::getLevelError());
                $root->setLevel(LoggerLevel::getLevelError());
            break;

            case LoggerLevel::DEBUG:
                $this->log->setLevel(LoggerLevel::getLevelDebug());
                $root->setLevel(LoggerLevel::getLevelDebug());
            break;

            default:
                $this->log->setLevel(LoggerLevel::getLevelError());
                $root->setLevel(LoggerLevel::getLevelError());
        }
    }

    private function formatLogMessage($logMessage, $traceMessage)
    {
        $context = "";
        $trace = $this->getCallingFunctionName(TRUE);

        if ($this->context !== "") {
            $context = sprintf("(%s)", $this->context);
            if ($this->traceCaller == TRUE)
            {
                return ($context.':'. $trace . '-'.$logMessage);
            }
            else {
                return ($context.'-'.$logMessage);
            }
        }
        else {
            if ($this->traceCaller == TRUE)
            {
                return ($trace . '-'.$logMessage);
            }
            else {
                return ($context.'-'.$logMessage);
            }
        }
    }


    /**
     *
     * @param string $logMessage
     */
    public function logDebug($logMessage)
    {
        $loggerMessage = $this->formatLogMessage($logMessage, $this->getCallingFunctionName(TRUE));
        $this->log->debug($loggerMessage);
    }

    /**
     *
     * @param string $logMessage
     */
    public function logError($logMessage)
    {
        $loggerMessage = $this->formatLogMessage($logMessage, $this->getCallingFunctionName(TRUE));
        $this->log->error($loggerMessage);
    }

    /**
     *
     * @param string $logMessage
     */
    public function logFatal($logMessage)
    {
        $loggerMessage = $this->formatLogMessage($logMessage, $this->getCallingFunctionName(TRUE));
        $this->log->fatal($loggerMessage);
    }

    /**
     *
     * @param string $logMessage
     */
    public function logErrorAndNotify($logMessage)
    {
      $localLog = $this->log;

      $this->log = Logger::getLogger("LogAndNotify");
      $this->log->error($logMessage);

      $this->log = $localLog;
    }

    /**
     *
     * @param string $logMessage
     */
    public function logFatalAndNotify($logMessage)
    {
      $localLog = $this->log;

      $this->log = Logger::getLogger("LogAndNotify");
      $this->log->fatal($logMessage);

      $this->log = $localLog;
    }


    /**
     * Returns a string telling the calling Class or function,
     * this function is expensive, in terms of cycles as it climbs the call stack
     * and makes use of the Exception->getTrace() call
     *
     * @param boolean $completeTrace
     * @return string
     */
    private function getCallingFunctionName($completeTrace=false)
    {
        if ($completeTrace == TRUE)
        {
           $e = new Exception();
           $trace = $e->getTrace();

           if (isset($trace[2]) && !($this->fromAPI))
           {
             return $trace[2]['function'].':'.$trace[2]['line']; //[0]-getCallingFunctionName,[1]-formatLogMessage, [2] caller of log function--what we want to log
           }

           if (isset($trace[3]) && $this->fromAPI)
           {
               return $trace[3]['function'].':'.$trace[3]['line']; //[0]-getCallingFunctionName,[1]-formatLogMessage, [2] caller of log function--what we want to log
           }

          } else {
            return "";
        }
        return "";
    }
}

?>
