#!/bin/bash
if [ ! -f /etc/nginx/sites-enabled/api-prod.ultra.me.conf  ]; then
    cd /etc/nginx/sites-enabled && ln -s ../sites-available/api-prod.ultra.me.conf
fi

supervisord -n