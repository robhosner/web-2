FROM centos:latest

ENV HOME=/home/ht/www/ultra_api_dev
ENV HOME_PROD=/home/ht/www/ultra_api_prod

# Add the ngix and PHP dependent repository
ADD DockerDependencies/nginx.repo /etc/yum.repos.d/nginx.repo

# Add the php repo
ADD DockerDependencies/remi.repo /etc/yum.repos.d/remi.repo

# Add epel
ADD DockerDependencies/epel.repo /etc/yum.repos.d/epel.repo

# Adding the configuration file of the nginx
ADD DockerDependencies/nginx.conf /etc/nginx/nginx.conf

RUN mkdir /etc/nginx/sites-available && mkdir /etc/nginx/sites-enabled

# Add api configuration dependencies
ADD DockerDependencies/api.ultra.me.conf /etc/nginx/sites-available/api.ultra.me.conf
ADD DockerDependencies/api.ultra.me.conf /etc/nginx/sites-enabled/api.ultra.me.conf
ADD DockerDependencies/supervisord.conf /etc/supervisord.conf

WORKDIR $HOME/web

# Install PHP
RUN yum -y install git && yum -y install nginx --enablerepo=nginx && \
    yum -y install gcc openssl-devel php php-fpm php-common php-pecl-redis php-mssql php-opcache php-mcrypt php-mongo php-dom && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    mkdir -p /private/etc/ && cp /etc/php-fpm.conf /private/etc/php-fpm.conf  && \
    mkdir /var/log/htt && chown apache:apache /var/log/htt && \
    chown apache:apache /var/log/httpd && \
    ln -s $HOME $HOME_PROD && rm /etc/php-fpm.d/www.conf

# Installing supervisor
RUN yum install -y python-setuptools && easy_install pip && pip install supervisor

# Add php.ini
ADD DockerDependencies/php.ini /etc/php.ini
ADD DockerDependencies/php-fpm.conf /etc/php-fpm.d/www.conf

COPY . $HOME/web

RUN composer global require hirak/prestissimo && composer update && \
    cp -a -r vendor/apache/log4php/src/main/php/. $HOME/web/log4php && \
    cp -a -r vendor/phpseclib/phpseclib/phpseclib/Crypt $HOME/web/Crypt && \
    cp -a -r vendor/pear/validate/Validate.php $HOME/web/ && \
    cp -a -r vendor/pear/validate_finance_creditcard/Validate $HOME/web/Validate && \
    chmod +x dockerRunPhpunit.sh && chmod +x DockerRunApiDev.sh && chmod +x DockerRunApiProd.sh

RUN yum -y install php-xdebug

CMD ["supervisord", "-n"]