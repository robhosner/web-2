
This file describes the content of this repository
==================================================


cgi_util/
 Utility scripts/tools

classes/
 Small general purpose classes or stand alone classes

db/
 Code containing SQL queries, SQL queries MUST be placed in this directoryis used by PHP code

images/
 Various images, no code

js/
 Javascript code, no PHP code

lib/
 Various php files, not object oriented

partner-face/
 Ultra API v 1.0 PHP code

partner-meta/
 Ultra API v 1.0 JSON definitions

runners/
 Various runners/cron processes, this includes some Perl code for SOAP interaction with MVNE.

styles/
 CSS stuff

test/
 Various tests for not object oriented code

InComm
 Incomm inbound APIs (version 4.0)

TMo/
 Old migration code. Do not use. We are planning to remove this.

Ultra/Lib/Amdocs/
 Perl Object Oriented code for Amdocs interaction

Ultra/Lib/Api/
 Object Oriented code for Ultra API version 2.0 (and higher)

Ultra/Lib/Api/Meta/
 Ultra API v 2.0 JSON definitions

Ultra/Lib/Api/Partner/
 Object Oriented code for Ultra API v 2.0 - partner classes

Ultra/Lib/Aspider/
 Perl Object Oriented code for Aspider interaction. Obsolete. Do not use. We are planning to remove this.

Ultra/Lib/Billing/
 Functions for billing runner

Ultra/Lib/BoltOn/
 Functions for Bolt Ons

Ultra/Lib/CC/
 Object Oriented message queue code for Credit Card transactions

Ultra/Lib/DB/
 Object Oriented DB code - PHP and Perl - also stored procedures wrappers.

Ultra/Lib/inComm/
 Functions for inComm API calls

Ultra/Lib/MiddleWare/
 Object Oriented MiddleWare code - PHP and Perl

Ultra/Lib/MiddleWare/Adapter/
 Object Oriented PHP code interfacing with the ULTRA/MW

Ultra/Lib/MQ/
 Object Oriented Message Queues code - PHP and Perl

Ultra/Lib/PromoCampaign/
 Functions for Promo SMS Campaigns

Ultra/Lib/StateMachine
 New Object Oriented State Machine PHP code

Ultra/Lib/Util/
 Object Oriented code - PHP and Perl

Ultra/Lib/Util/Redis/
 Object Oriented Redis code

Ultra/BrandConfig.php
  functions for working with brands

Ultra/UltraConfig.php
 Configuration utility

Ultra/Makefile
 I have no idea

Ultra/tests/API/
 Object Oriented code for PHPUnit

Ultra/tests/API_SOAP/
 Code to test SOAP interaction with our APIs

Ultra/tests/
 Some tests

./core.php
 Core PHP functions - Not Object Oriented

./db.php
 Core PHP DB functions - Not Object Oriented

./dig.php
 Ultra Investigative Gadget - used to check customer status, history, DB values and network settings at MVNE level
 http://wiki.hometowntelecom.com:8090/pages/viewpage.action?pageId=6422761
 Redirects to actual version.

./fields.php
 Legacy code still in use - archaic PHP functions to modify customer's DB attributes - Not Object Oriented

./cosid_constants.php
 Legacy code still in use - constants and functions related with Ultra Plans - Not Object Oriented
 
./fraud.php
 Legacy code still in use - archaic PHP functions for auditing/fraud checking - Not Object Oriented

./partner.php
 Main script ( Not Object Oriented ) for Ultra API v 1.0 - Not Object Oriented

./session.php
 Legacy code still in use - archaic PHP core functions

./UltraLogger.php
 Logging Class





