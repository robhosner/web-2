<?php

// Set address fields and credit card fields
function portal__SetCustomerFields($partner_tag, $zsession, $first_name, $last_name, $cc_name, $cc_address1, $cc_address2, $cc_city, $cc_country, $cc_state_or_region, $cc_postal_code, $account_number_phone, $account_number_email, $account_cc_exp, $account_cc_cvv, $account_cc_number, $address1, $address2, $city, $country, $state_or_region, $postal_code, $preferred_language)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $customer = NULL;
  $warnings = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__SetCustomerFields",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__SetCustomerFields", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__SetCustomerFields",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
    $errors = $data_zsession['errors'];
  else
    $customer = $data_zsession['customer'];

  $validate_cc_fields_count = 0;

  if ( ! count($errors) )
  {
    // validate credit card info

    if ( isset($account_cc_number) && ( $account_cc_number != '' ) )
      $validate_cc_fields_count++;

    if ( isset($account_cc_cvv   ) && ( $account_cc_cvv    != '' ) )
    {
      $validate_cc_fields_count++;
      dlog('',"account_cc_cvv = $account_cc_cvv");
    }

    if ( isset($account_cc_exp   ) && ( $account_cc_exp    != '' ) )
    {
      $validate_cc_fields_count++;
      dlog('',"account_cc_exp = $account_cc_exp");
    }

    if ( isset($cc_name          ) && ( $cc_name           != '' ) )
    {
      $validate_cc_fields_count++;
      dlog('',"cc_name = $cc_name");
    }

    if ( ! ( ( $validate_cc_fields_count == 0 ) || ( $validate_cc_fields_count == 4 ) ) )
    {
      // 1,2,3
      if (
           // not enough credit card fields
           ( $validate_cc_fields_count <= 2 )
         ||
           // one credit card field field missing: we have only 3 fields including $account_cc_number
           ( isset($account_cc_number) && ( $account_cc_number != '' ) )
         ||
           // $account_cc_number was not provided and customer has no CC_NUMBER
           ( ! $customer->CC_NUMBER )
         )
      { $errors[] = "ERR_API_INVALID_ARGUMENTS: some credit card info are missing"; }
    }
  }

  if ( ! count($errors) )
  {
    if ( $cc_name || $cc_address1 || $cc_address2 || $cc_city || $cc_country || $country || $city || $address1 || $address2 || $cc_postal_code || $cc_state_or_region || $postal_code || $state_or_region || $account_number_phone || $account_number_email || $account_cc_number || $account_cc_cvv || $account_cc_exp )
    {
      // validate and store credit card info - this includes tokenization
      $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
        array(
          'customer_id'     => $customer->CUSTOMER_ID,
          'cc_name'         => $cc_name,
          'cc_address1'     => $cc_address1,
          'cc_address2'     => $cc_address2,
          'cc_city'         => $cc_city,
          'cc_country'      => $cc_country,
          'country'         => $country,
          'city'            => $city,
          'address1'        => $address1,
          'address2'        => $address2,
          'cc_postal_code'  => $cc_postal_code,
          'cc_state_region' => $cc_state_or_region,
          'postal_code'     => $postal_code,
          'state_region'    => $state_or_region,
          'local_phone'     => $account_number_phone,
          'e_mail'          => $account_number_email,
          'cc_number'       => $account_cc_number,
          'cvv'             => $account_cc_cvv,
          'expires_date'    => $account_cc_exp
        )
      );

      if ( $result->is_failure() )
      {
        dlog('',"cc_info result data = %s",$result->data_array);

        return flexi_encode(fill_return($p,
                                        "portal__SetCustomerFields",
                                        func_get_args(),
                                        array("success"  => FALSE,
                                              "errors"   => $result->get_errors(),
                                              'warnings' => $warnings)));
      }
    }

    // prepare fields which will be updated ( TODO: OBSOLETE - TO REMOVE - MVNO-1779 )

    $fields = array();

    if ( $first_name           ) { $fields['account_first_name']      = $first_name; }
    if ( $last_name            ) { $fields['account_last_name']       = $last_name; }

    // update statements wrapped in a SQL transaction

    if ( start_mssql_transaction() )
    {
      $fields_update_results = fields($fields, $customer, FALSE);

      $all_validated     = FALSE;
      $all_db_queries_ok = TRUE;

      foreach( $fields_update_results as $result_id => $result ) // loop through result array
      {
        if ( is_array( $result ) )
        {
          if ( isset($result['all_validated'] ) )
          {
            if ( $result['all_validated'] )
            {
              $all_validated = TRUE;
            }
          }
          else if ( isset($result['validated'] ) && is_array($result['validated']) )
          {
            if (
              ( ! $result['validated']['ok'] )
              &&
              ( $result['validated']['validation_msg'] )
            )
            {
              $errors[] = "ERR_API_INVALID_ARGUMENTS: ".$result['validated']['validation_msg'];
            }
          }
          else if ( isset($result['sql'] ) )
          {
            if ( $result['sql'][1] != 1 )
            {
              $errors[] = "ERR_API_INTERNAL: Database write error";

              $all_db_queries_ok = FALSE;
            }
          }
        }
      }

      if ( $preferred_language )
      {
        // we don't use fields.php
        $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
          array(
            'customer_id'        => $customer->CUSTOMER_ID,
            'preferred_language' => $preferred_language
          )
        );

        if ( ! run_sql_and_check($htt_customers_overlay_ultra_update_query) )
          $all_db_queries_ok = FALSE;
      }

      if ( $all_validated && $all_db_queries_ok )
      {
        if ( commit_mssql_transaction() )
        { $success = TRUE; }
        else
        { $errors[] = "ERR_API_INTERNAL: DB error"; }
      }
      else if ( ! rollback_mssql_transaction() )
      { $errors[] = "ERR_API_INTERNAL: DB error"; }
    }
    else
    { $errors[] = "ERR_API_INTERNAL: DB error"; }
  }

  // update voicemail language if requested
  if ( $success && $preferred_language )
  {
    if ( ! mvneSetVoicemailLanguage($customer, $preferred_language))
      $warnings[] = 'Failed to set voicemail language';

    teldata_change_db();
  }

  return flexi_encode(fill_return($p,
                                  "portal__SetCustomerFields",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors,
                                        'warnings' => $warnings)));
}

?>
