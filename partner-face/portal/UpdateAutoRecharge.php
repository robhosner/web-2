<?php

require_once 'Ultra/CreditCards/Repositories/Mssql/CreditCardRepository.php';
require_once 'Ultra/Messaging/Messenger.php';

// Update the monthly_cc_renewal column in htt_customers_overlay_ultra table.
use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Messaging\Messenger;

function portal__UpdateAutoRecharge($partner_tag, $zsession, $auto_recharge)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__UpdateAutoRecharge",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__UpdateAutoRecharge", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__UpdateAutoRecharge",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    // initial parameters for API
    $sql_params = array(
                    'customer_id'           => $data_zsession['customer']->CUSTOMER_ID,
                    'monthly_cc_renewal'    => $auto_recharge
                  );

    if ($auto_recharge)
    {
      $customerId = $data_zsession['customer']->CUSTOMER_ID;

      $creditCardInfo = (new CreditCardRepository())->getCcInfoFromCustomerId($customerId);
      $lastFour = empty($creditCardInfo->LAST_FOUR) ? '' : $creditCardInfo->LAST_FOUR;

      $result = (new Messenger())->enqueueImmediateSms($customerId, 'auto_recharge_active', ['last_four' => $lastFour]);

      if ($result->is_failure())
      {
        $errors[] = 'ERR_API_INTERNAL: SMS delivery error.';
      }
    }
    else
    {
      // handle when customer successfully turns off auto recharge
      $sql_params['easypay_activated'] = $auto_recharge;
    }
    
    $sql = htt_customers_overlay_ultra_update_query($sql_params);
    
    if (is_mssql_successful(logged_mssql_query($sql)))
    {
      $success = TRUE;
    }
    else
    { $errors[] = "ERR_API_INTERNAL: DB error."; }
  }

  return flexi_encode(fill_return($p,
                                  "portal__UpdateAutoRecharge",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
