<?php


include_once('db/htt_preferences_marketing.php');


// Retrieves Marketing Settings for a logged in customer.
function portal__GetMarketingSettings($partner_tag, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success            = FALSE;
  $marketing_settings = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__GetMarketingSettings",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__GetMarketingSettings", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__GetMarketingSettings",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) == 0 )
  {
    $customer = $data_zsession['customer'];

    $result = get_marketing_settings( array( 'customer_id' => $customer->CUSTOMER_ID ) );

    if ( count($result['errors']) == 0 )
    {
      $settings = array(
        'marketing_email_option',
        'marketing_sms_option'
      );

      foreach( $settings as $id => $field )
      {
        $marketing_settings[] = $field;
        $marketing_settings[] = $result['marketing_settings'][$field];
      }

      $voice_option = get_voice_preference($customer->CUSTOMER_ID);
      $marketing_settings[] = 'marketing_voice_option';
      $marketing_settings[] = ($voice_option) ? 0 : 1;

      $success = TRUE;
    }
    else
    { $errors = $result['errors']; }

  }
  else
  { $errors = $data_zsession['errors']; }

  return flexi_encode(fill_return($p,
                                  "portal__GetMarketingSettings",
                                  func_get_args(),
                                  array("success"            => $success,
                                        "marketing_settings" => $marketing_settings,
                                        "errors"             => $errors)));
}

?>
