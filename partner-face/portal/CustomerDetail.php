<?php

include_once 'db/htt_transition_log.php';

// Provides all the useful details on a customer based upon a customer zsession.
function portal__CustomerDetail($partner_tag, $zsession, $mode)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $customer = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__CustomerDetail",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__CustomerDetail", func_get_args(), $mock);

  if ( !($errors && count($errors) > 0) && ( strlen($zsession) < 36 ) )
    $errors[] = "zsession is invalid";

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__CustomerDetail",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));

  teldata_change_db(); // connect to the DB

  $return_array = array(
    "success" => FALSE,
    "errors"  => $errors
  );

  $data_zsession = get_customer_from_zsession($zsession);

  dlog('',"data_zsession = %s",$data_zsession);

  if ( count($data_zsession['errors']) )
    $errors = $data_zsession['errors'];
  else
  {
    $customer = $data_zsession['customer'];

    $return_array['success']             = TRUE;
    $return_array['current_msisdn']      = $customer->current_mobile_number;
    $return_array['first_name']          = $customer->FIRST_NAME;
    $return_array['last_name']           = $customer->LAST_NAME;
    $return_array['email']               = $customer->E_MAIL;
    $return_array['local_phone']         = $customer->LOCAL_PHONE;
    $return_array['cos']                 = cos_id_plan_description( $customer->cos_id );
    $return_array['customer_plan_start'] = $customer->plan_started_epoch;
    $return_array['plan_expires']        = $customer->plan_expires_epoch;
    $return_array['monthly_renewal_target'] = $customer->MONTHLY_RENEWAL_TARGET;
    $return_array['stored_value']        = $customer->stored_value;
    $return_array['balance']             = $customer->BALANCE;
    $return_array['packaged_balance1']   = $customer->PACKAGED_BALANCE1;
    $return_array['customer_status']     = $customer->plan_state;
    $return_array['customer_loginname']  = $customer->LOGIN_NAME;
    $return_array['customer_id']         = $customer->CUSTOMER_ID;
    $return_array['customer']            = $customer->CUSTOMER;
    $return_array['auto_recharge']       = ( $customer->monthly_cc_renewal == 1 );
    $return_array['address1']            = $customer->ADDRESS1;
    $return_array['address2']            = $customer->ADDRESS2;
    $return_array['city']                = $customer->CITY;
    $return_array['state_region']        = $customer->STATE_REGION;
    $return_array['postal_code']         = $customer->POSTAL_CODE;
    $return_array['country']             = $customer->COUNTRY;
    $return_array['cc_name']             = $customer->CC_NAME;
    $return_array['cc_address1']         = $customer->CC_ADDRESS1;
    $return_array['cc_address2']         = $customer->CC_ADDRESS2;
    $return_array['cc_city']             = $customer->CC_CITY;
    $return_array['cc_state_region']     = $customer->CC_STATE_REGION;
    $return_array['cc_postal_code']      = $customer->CC_POSTAL_CODE;
    $return_array['cc_country']          = $customer->CC_COUNTRY;
    $return_array['bolt_ons']            = get_bolt_ons_info_from_customer_options( $customer->CUSTOMER_ID );
    $return_array['4g_lte_remaining']    = NULL;
    $return_array['4g_lte_usage']        = NULL;
    $return_array['4g_lte_last_applied'] = 0;
    $return_array['4g_lte_bolton_percent_used'] = 0;
    $return_array['monthly_data_purchases'] = 0;

    // CCCP-15: get CC info
    $cc_info = get_cc_info_from_customer_id($customer->CUSTOMER_ID);
    $return_array['cc_exp_date']         = empty($cc_info->EXPIRES_DATE) ? NULL : $cc_info->EXPIRES_DATE;
    $return_array['cc_last_4']           = empty($cc_info->LAST_FOUR) ? NULL : $cc_info->LAST_FOUR;

    $return_array['iccid']               = $customer->CURRENT_ICCID_FULL;
    $return_array['brand']               = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
    $return_array['preferred_language']  = $customer->preferred_language;
    $return_array['activate_request_id'] = get_activation_request_id($customer->CUSTOMER_ID);
    #$return_array['porting_request_id']  = get_porting_request_id($customer->CUSTOMER_ID);
    $return_array['last_transition_name'] = get_last_transition_name_by_customer_id($customer->CUSTOMER_ID);
    $return_array['transition_in_progress'] = ( count_customer_open_transitions( $customer->CUSTOMER_ID ) > 0 ? '1' : '0' );
    $return_array['iccid']               = luhnenize($return_array['iccid']);
    $return_array['iccid']               = trim($return_array['iccid']);
    $return_array['originalsimproduct']  = 
      strlen($customer->CURRENT_ICCID_FULL) == 19 ? get_product_type_from_iccid($customer->CURRENT_ICCID_FULL) : '';

    { // get Port-in data for MVNE2
      $portInQueue = new \PortInQueue();

      $loadByCustomerIdResult = $portInQueue->loadByCustomerId( $customer->CUSTOMER_ID );

      if ( $loadByCustomerIdResult->is_success() && $portInQueue->portin_queue_id )
      {
        if ( property_exists( $portInQueue , 'msisdn') )
          $return_array['porting_msisdn']           = $portInQueue->msisdn;

        if ( property_exists( $portInQueue , 'account_number') )
          $return_array['porting_account_number']   = $portInQueue->account_number;

        if ( property_exists( $portInQueue , 'account_password') )
          $return_array['porting_account_password'] = $portInQueue->account_password;

        if ( property_exists( $portInQueue , 'account_zipcode') )
          $return_array['porting_account_zipcode']  = $portInQueue->account_zipcode;

        $redis_port = new \Ultra\Lib\Util\Redis\Port();

        $return_array['porting_request_id'] = $redis_port->getRequestId( $customer->CUSTOMER_ID , $customer->plan_state );
      }

      teldata_change_db();
    }

    $return_array['voice_minutes'] = 0;

    if ( ( $mode == 'OCS_BALANCE' ) && ( get_plan_from_cos_id( $customer->cos_id ) == 'L19' ) )
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $mwResult = $mwControl->mwCheckBalance([
        'actionUUID' => getNewActionUUID('dealerportal ' . time()),
        'msisdn'     => $customer->current_mobile_number
      ]);

      if ( $mwResult->is_success()
        && isset($mwResult->data_array['body'])
        && property_exists( $mwResult->data_array['body'] , 'BalanceValueList' )
        && property_exists( $mwResult->data_array['body']->BalanceValueList , 'BalanceValue' )
        && is_array( $mwResult->data_array['body']->BalanceValueList->BalanceValue ))
        foreach( $mwResult->data_array['body']->BalanceValueList->BalanceValue as $row )
          if ( $row->Type == 'DA1' )
            $return_array['voice_minutes'] = $row->Value;

      teldata_change_db();
    }

    if ( $mode == 'DATA_USAGE' )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      // get '4g_lte_last_applied' from HTT_DATA_EVENT_LOG

      $latest_data_event_log = get_latest_data_event_log( $customer->CUSTOMER_ID );

      if ( $latest_data_event_log )
        $return_array['4g_lte_last_applied'] = $latest_data_event_log->event_date_epoch;

      // invoke mwCheckBalance to check 4G LTE data usage for this customer
      if ( $customer->current_mobile_number )
      {
        list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown , $mvneError ) = mvneGet4gLTE($request_id, $customer->current_mobile_number, $customer->CUSTOMER_ID, $redis);

        $return_array['4g_lte_remaining'] = $remaining;
        $return_array['4g_lte_usage']     = $usage;

        if ( ! empty( $mintAddOnUsage )
          && ! empty( $mintAddOnUsage + $mintAddOnRemaining )
        )
        {
          $return_array['4g_lte_bolton_percent_used'] = ceil( ( $mintAddOnUsage * 100 ) / ( $mintAddOnUsage + $mintAddOnRemaining ) );
        }

        teldata_change_db();

        // MVNO-2612: add the count of data purchases within the current cycle
        $return_array['monthly_data_purchases'] = count(get_current_cycle_purchase_history($customer->CUSTOMER_ID, 'SPEND', 'DATA Purchase'));
      }
    }

    if ( ! isset($return_array['porting_request_id']) || ( ! $return_array['porting_request_id'] )  )
    {
      # select top 1 TRANSITION_UUID from [HTT_TRANSITION_LOG] where STATUS='OPEN' and customer_id='1292' order by created asc

      $htt_transition_log_select_query = htt_transition_log_select_query(
        array(
          'sql_limit'   => 1,
          'status'      => 'OPEN',
          'customer_id' => $customer->CUSTOMER_ID,
          'order_by'    => 'ORDER BY t.CREATED ASC'
        )
      );

      $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

      if ( ( is_array($query_result) ) && count($query_result) > 0 )
        $return_array['porting_request_id'] = $query_result[0]->TRANSITION_UUID;
    }

    // MVNO-2554: format data string regardless of value
    $return_array['4g_lte_remaining'] = beautify_4g_lte_string( $return_array['4g_lte_remaining'], FALSE );
    $return_array['4g_lte_usage']     = beautify_4g_lte_string( $return_array['4g_lte_usage'], TRUE );

  }

  return flexi_encode(fill_return($p,
                                  "portal__CustomerDetail",
                                  func_get_args(),
                                  $return_array));
}

