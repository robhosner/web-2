<?php

// Set billing address. (should match old API call)
function portal__SetBillingAddress($partner_tag, $cc_name, $cc_address1, $cc_address2, $cc_city, $cc_state_or_region, $cc_postal_code, $cc_country)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__SetBillingAddress",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__SetBillingAddress", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__SetBillingAddress",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "portal__SetBillingAddress",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));
}

?>
