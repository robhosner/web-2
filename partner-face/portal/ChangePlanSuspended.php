<?php

// Change the plan of a Suspended customer. This will allow them to re-activate under a different plan. Should allow for plan upgrades and downgrades.
function portal__ChangePlanSuspended($partner_tag, $zsession, $targetPlan)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $customer = NULL;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ChangePlanSuspended",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ChangePlanSuspended", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ChangePlanSuspended",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    $customer = $data_zsession['customer'];
  }

  if ( ! count($errors) )
  {
    try
    {
      if ( ! $customer->COS_ID )
      {
        throw new Exception("ERR_API_INTERNAL: customer plan could not be determined (1)");
      }

      // Get the customer plan
      $plan = get_plan_from_cos_id( $customer->COS_ID ); # L[12345]9

      if ( ! $plan )
      {
        throw new Exception("ERR_API_INTERNAL: customer plan could not be determined (2)");
      }

      if ( $targetPlan == $plan )
      {
        throw new Exception("ERR_API_INVALID_ARGUMENTS: customer already in plan $plan");
      }

      // Get the current state
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( ! $state )
      {
        throw new Exception("ERR_API_INTERNAL: customer state could not be determined");
      }

      if ( $state['state'] != 'Suspended' )
      {
        throw new Exception("ERR_API_INVALID_ARGUMENTS: customer is not Suspended");
      }

      $context = array(
        'customer_id' => $customer->CUSTOMER_ID
      );

      $result = change_suspended_plan($state,$customer,$context,$targetPlan);

      $errors = $result['errors'];
    }
    catch(Exception $e)
    {
      dlog('', $e->getMessage());
      $errors[] = $e->getMessage();
    }

    if ( ! count($errors) ) $success = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "portal__ChangePlanSuspended",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
