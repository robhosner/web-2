<?php

// Extend the lifespan of current zsession by 30 minutes.
function portal__ExtendSession($partner_tag, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ExtendSession",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ExtendSession", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ExtendSession",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  dlog('',"data_zsession = %s",$data_zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    session_save($data_zsession['customer'], $zsession);

    $verified = verify_session( $data_zsession['customer']->CUSTOMER );

    dlog('',"verified = %s",$verified);

    setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
    setcookielive('zsessionC', $data_zsession['customer']->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);

    $success = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "portal__ExtendSession",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
