<?php

// Check the status of a Shipwire order
function portal__ShipwireOrderDetails($partner_tag, $order_id, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ShipwireOrderDetails",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ShipwireOrderDetails", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ShipwireOrderDetails",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $output_params = array(
    'success' => FALSE,
    'errors'  => $errors
  );

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $output_params['errors'] = $data_zsession['errors'];
  }
  else
  {
    $customer = $data_zsession['customer'];

    if ( $customer )
    {
      $output = api_TrackingServices(
        array(
          'order_no' => $order_id
        )
      );

      dlog('',"api_TrackingServices output = %s",$output);

      if ( isset($output['error'] ) )
      {
        $errors[] = $output['error'];
      }
      else
      {
        if ( isset($output['data']) && is_array($output['data']) && isset($output['data']['orders']) && is_array( $output['data']['orders'] ) && count( $output['data']['orders'] ) )
        {
        $output_params = array(
          'shipped'                => $output['data']['orders'][0]['shipped'],
          'shipper_full_name'      => $output['data']['orders'][0]['shipperFullName'],
          'ship_date'              => $output['data']['orders'][0]['shipDate'],
          'expected_delivery_date' => $output['data']['orders'][0]['expectedDeliveryDate'],
          'returned'               => $output['data']['orders'][0]['returned'],
          'bookmark'               => $output['data']['bookmark']
        );
        }
        else
        {
          $output_params['success'] = FALSE;
          $output_params['errors']  = array("ERR_API_INTERNAL: server response error");
        }
      }

      $output_params['success'] = $output['success'];
      $output_params['errors']  = $errors;
    }
    else
    {
      $output_params['errors'][] = "ERR_API_INVALID_ARGUMENTS: invalid login";
    }

  }

  return flexi_encode(fill_return($p,
                                  "portal__ShipwireOrderDetails",
                                  func_get_args(),
                                  $output_params));
}

?>
