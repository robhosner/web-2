<?php

// Checks session status of currently logged in customer.
function portal__CheckSession($partner_tag)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $zsession             = '';
  $zexpire_timestamp    = '';
  $bypass_claim_account = '';
  $password_is_empty    = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__CheckSession",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__CheckSession", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__CheckSession",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  if (array_key_exists("zsession", $_COOKIE))
  {
    $_REQUEST['zsession'] = $_COOKIE['zsession'];
  }

  if (  array_key_exists("zsessionC", $_COOKIE ) &&
      ! array_key_exists("customer",  $_REQUEST) )
  {
    $_REQUEST['customer'] = $_COOKIE['zsessionC'];
  }

  $verified = array_key_exists("zsession", $_REQUEST) ? verify_session() : NULL;

  dlog('',"verified = %s",$verified);

  if ($verified)
  {
    $zsession = $verified[2];

    $data_zsession = get_customer_from_zsession($zsession);

    dlog('',"data_zsession = %s",$data_zsession);

    if ( count($data_zsession['errors']) )
    {
      $errors = $data_zsession['errors'];
    }
    else
    {
      $zexpire_timestamp    = $verified[1];
      $bypass_claim_account = $verified[3];

      $password_is_empty = !$data_zsession['customer']->LOGIN_PASSWORD;

      $success = TRUE;
    }
  }
  else
  {
    $errors = "ERR_API_INTERNAL: session not found";
  }

  return flexi_encode(fill_return($p,
                                  "portal__CheckSession",
                                  func_get_args(),
                                  array("success"              => $success,
                                        "zsession"             => $zsession,
                                        "expire_timestamp"     => $zexpire_timestamp,
                                        "bypass_claim_account" => $bypass_claim_account,
                                        "password_is_empty"    => $password_is_empty,
                                        "errors"               => $errors)));
}

?>
