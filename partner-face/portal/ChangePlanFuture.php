<?php


include_once('db/htt_customers_overlay_ultra.php');


// Sets the plan to be changed in the future.
function portal__ChangePlanFuture($partner_tag, $zsession, $targetPlan)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__ChangePlanFuture",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__ChangePlanFuture", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__ChangePlanFuture",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) == 0 )
  {
    $customer = $data_zsession['customer'];

    if ( $customer && is_object($customer) )
    {
      if ( ! isSameBrand(get_plan_from_cos_id($customer->COS_ID), $targetPlan))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: previous and future plan brands do not match';

      if ( ! is_eligible_for_plan_change($customer->CUSTOMER_ID, array('BILLING.MRC_PROMO_PLAN', 'BILLING.MRC_7_11', BILLING_OPTION_MULTI_MONTH)))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: The customer is not eligible to change their plan';

      if (count($errors))
      {
        return flexi_encode(
          fill_return(
            $p,
            "portal__ChangePlanFuture",
            func_get_args(),
            array(
              "success" => FALSE,
              "errors"  => $errors
            )
          )
        );
      }

      // MONTHLY_RENEWAL_TARGET is a string like L[12345]9
      $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
        array(
          'monthly_renewal_target' => $targetPlan,
          'customer_id'            => $customer->CUSTOMER_ID
        )
      );

      $success = ! ! is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query));

      if ( ! $success )
        $errors[] = "ERR_API_INTERNAL: Database error (2)";
    }
    else
    { $errors[] = "ERR_API_INTERNAL: Database error (1)"; }

  }
  else
  { $errors = $data_zsession['errors']; }

  return flexi_encode(fill_return($p,
                                  "portal__ChangePlanFuture",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}


?>
