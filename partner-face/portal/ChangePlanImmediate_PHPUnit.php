<?php

require_once 'classes/PHPUnitBase.php';

class ChangePlanImmediateTest extends PHPUnitBase
{
  public function test__portal__ChangePlanImmediate()
  {
    // API setup
    list($test, $partner, $api) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'version'   => 1,
      'partner'   => $partner));

    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    # print_r($result);
    $this->assertGreaterThan(0, count($result->errors));

    // API-395: test L39 -> D39 plan change
    $params = array(
      'zsession'    => $this->makeZsession(19449),
      'targetPlan'  => 'D39');
    $this->assertNotEmpty($params['zsession']);
    $result = $this->callApi($params);
    # print_r($result);
    $this->assertTrue($result->success);
  }
}