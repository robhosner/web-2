<?php


include_once('db/htt_billing_history.php');


// Retrieves transaction history for a customer.
function portal__GetTransactionHistory($partner_tag, $zsession, $start_epoch, $end_epoch)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $customer = FALSE;
  $transaction_history = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__GetTransactionHistory",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__GetTransactionHistory", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__GetTransactionHistory",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    $customer = $data_zsession['customer'];
  }

  if ( $customer )
  {
    $result = get_billing_transaction_history(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'start_epoch' => $start_epoch,
        'end_epoch'   => $end_epoch
      )
    );

    if ( count($result['errors']) == 0 )
    {
      foreach( $result['billing_transaction_history'] as $id => $data )
      {
        if ($data->result != 'COMPLETE')
          continue;

        #$l_nSeconds = $data->history_epoch;
        #$l_xDate = new DateTime();
        #$l_xDate->setTimestamp($l_nSeconds);
        #$l_xDate->setTimezone(new DateTimeZone('PDT'));
        #$transaction_history[] = $data->history_epoch + ( $l_xDate->getOffset() * 60 ); // PST
        $transaction_history[] = $data->history_epoch; // PST
        $transaction_history[] = $data->order_id;
        $transaction_history[] = $data->type;
        $transaction_history[] = $data->history_source;
        $transaction_history[] = $data->AMOUNT;
        $transaction_history[] = $data->description;
      }

      $success = TRUE;
    }
    else
    { $errors = $result['errors']; }

  }
  else
  { $errors[] = "ERR_API_INTERNAL: DB error (1)"; }

  return flexi_encode(fill_return($p,
                                  "portal__GetTransactionHistory",
                                  func_get_args(),
                                  array("success" => $success,
                                        "transaction_history" => $transaction_history,
                                        "errors"  => $errors)));
}

?>
