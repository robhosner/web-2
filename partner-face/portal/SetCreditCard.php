<?php

// Set credit card. TODO!!!
function portal__SetCreditCard($partner_tag, $account_cc_exp, $account_cc_cvv, $account_cc_number)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__SetCreditCard",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__SetCreditCard", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__SetCreditCard",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "portal__SetCreditCard",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
