<?php

// Resets the user's password to a temporary memcache password and sends it by email to the customer. Temp password should be valid for 72 hours.
function portal__ResetPasswordEmail($partner_tag, $login_string)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__ResetPasswordEmail",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__ResetPasswordEmail", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__ResetPasswordEmail",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = FALSE;
  $clauses  = FALSE;

  $attributes = array('CUSTOMER_ID','E_MAIL','FIRST_NAME','LAST_NAME');

  if ( is_numeric($login_string) )
  {
    $info = get_ultra_customers_from_msisdn($login_string, array('CUSTOMER_ID'));

    if ( $info && count($info) )
      $clauses = array('CUSTOMER_ID' => $info[0]->CUSTOMER_ID);
  }
  else
    $clauses = array('LOGIN_NAME' => $login_string);

  if ( $clauses )
    $customer = find_first(\Ultra\Lib\DB\makeSelectQuery('CUSTOMERS', 1, $attributes, $clauses, NULL, NULL, TRUE));

  if ( $customer )
  {
    dlog('',"customer = %s",$customer);

    // verify that the email is correct

    if ( empty( $customer->E_MAIL ) || ! $customer->E_MAIL )
      return flexi_encode(fill_return($p,
                                      "portal__ResetPasswordEmail",
                                      func_get_args(),
                                      array("success" => FALSE,
                                            "errors"  => 'ERR_API_INVALID_ARGUMENTS: The customer has no email address.')));

    if ( ! preg_match("/\w\w/",$customer->E_MAIL,$matches) || ! preg_match("/\@/",$customer->E_MAIL,$matches) )
      return flexi_encode(fill_return($p,
                                      "portal__ResetPasswordEmail",
                                      func_get_args(),
                                      array("success" => FALSE,
                                            "errors"  => 'ERR_API_INVALID_ARGUMENTS: The customer\'s email address is invalid.')));

    // generate a temporary password ( token = 'reset_password' )

    $password = func_create_temp_password( $customer->CUSTOMER_ID , 'reset_password', 60*60*72);
    $overlayCustomer = get_ultra_customer_from_customer_id($customer->CUSTOMER_ID);
    
    if ($overlayCustomer->BRAND_ID == 3)
    {
      $check = funcSendExemptCustomerEmail_mint_forgot_password([
        'customer'     => $customer,
        'new_password' => $password
      ]);
    }
    else
    {
      $check = funcSendExemptCustomerEmail_ultra_forgot_password([
        'customer'     => $customer,
        'new_password' => $password
      ]);
    }
    
    if ( $check )
    {
      // reset user password

      $check = reset_customer_password( $customer->CUSTOMER_ID );

      if ( $check )
        $success = TRUE;
      else
        $errors[] = "ERR_API_INTERNAL: DB error";
    }
    else
    { $errors[] = "ERR_API_INTERNAL: internal error"; }

  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "portal__ResetPasswordEmail",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
