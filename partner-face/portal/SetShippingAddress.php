<?php

// Set shipping address. TODO!!!
function portal__SetShippingAddress($partner_tag, $account_address1, $account_address2, $account_city, $account_state_or_region, $account_postal_code, $account_country)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__SetShippingAddress",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__SetShippingAddress", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__SetShippingAddress",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "portal__SetShippingAddress",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));
}

?>
