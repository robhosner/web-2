<?php

/*
Determine the Sales Tax and Regulatory fees that would be charged for a potential Credit Card purchase, provided a purchase amount.

 - verify that user is logged in
 - retrieve zip code associated with the customer
 - calculate taxes and fees if 'check_taxes' is TRUE
 - calculate fees if 'check_taxes' is FALSE
*/
function portal__CalculateTaxesFees($partner_tag, $zsession, $charge_amount, $zip, $check_taxes, $check_recovery_fee)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success      = FALSE;
  $sales_tax    = 0;
  $recovery_fee = 0;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__CalculateTaxesFees",
                                    func_get_args(),
                                    array("success"      => TRUE,
                                          "sales_tax"    => $sales_tax,
                                          "recovery_fee" => $recovery_fee,
                                          "errors"       => array(),
                                          "warnings"     => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__CalculateTaxesFees", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__CalculateTaxesFees",
                                    func_get_args(),
                                    array("success"      => $success,
                                          "sales_tax"    => $sales_tax,
                                          "recovery_fee" => $recovery_fee,
                                          "warnings"     => array(),
                                          "errors"       => $errors)));

  try
  {
    // connect to the DB
    teldata_change_db();

    // verify that user is logged in
    $data_zsession = get_customer_from_zsession($zsession);

    if ( count($data_zsession['errors']) )
      throw new \Exception( $data_zsession['errors'][0] );

    if ( !isset($data_zsession['customer']) || !is_object($data_zsession['customer']) )
      throw new \Exception( "ERR_API_INVALID_ARGUMENTS: customer not found");

    $customer = $data_zsession['customer'];

    if ( !$customer || !is_object($customer) )
      throw new \Exception( 'No customer found' );

    // get zip code
    if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
      $zip = $customer->CC_POSTAL_CODE;

    if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
      $zip = $customer->POSTAL_CODE;

    if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
      throw new \Exception( 'No zip code found for this customer' );

    if ( $check_taxes )
    {
      // calculate taxes and fees
      $result = calculate_taxes_fees( $charge_amount , $customer->CUSTOMER_ID , $zip , $customer->ACCOUNT );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        throw new \Exception( $errors[0] );
      }

      $sales_tax = $result->data_array['sales_tax'];

      if ( $check_recovery_fee )
        $recovery_fee = $result->data_array['recovery_fee'];
    }
    elseif( $check_recovery_fee )
      // calculate fees
      list( $recovery_fee , $recovery_fee_rule , $recovery_fee_basis ) = get_recovery_fee( $charge_amount );

    $success = TRUE;
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "portal__CalculateTaxesFees",
                                  func_get_args(),
                                  array("success"      => $success,
                                        "sales_tax"    => $sales_tax,
                                        "recovery_fee" => $recovery_fee,
                                        "errors"       => $errors)));
}

