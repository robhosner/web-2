<?php

// return the allowed Data Recharge items which the customer can buy given the plan.
function portal__GetAllowedDataRechargeByPlan($partner_tag, $plan)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__GetAllowedDataRechargeByPlan",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__GetAllowedDataRechargeByPlan", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__GetAllowedDataRechargeByPlan",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  $data_recharge = get_data_recharge_by_plan($plan);

  if ( $data_recharge )
  {
    $data_recharge = json_encode($data_recharge);
    $success       = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "portal__GetAllowedDataRechargeByPlan",
                                  func_get_args(),
                                  array("success"       => $success,
                                        "data_recharge" => $data_recharge,
                                        "errors"        => $errors)));
}

?>
