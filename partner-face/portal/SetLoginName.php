<?php

// Set Customer's Login.
function portal__SetLoginName($partner_tag, $account_login, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__SetLoginName",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__SetLoginName", func_get_args(), $mock);

  if ( is_numeric($account_login) )
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_login cannot be a number.";
  }

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__SetLoginName",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  dlog('',"data_zsession = %s",$data_zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    //
    // There is a Unique Constraint on login, so we will check to make sure there is no identical LOGIN_NAME first, before we attempt to Update it.
    
    if(check_unique_login_name($account_login) === true ) // Passed a database check on LOGIN_NAME in CUSTOMERS
    {   
      $query = customers_update_query(
        array(
          'login_name'  => $account_login,
          'customer_id' => $data_zsession['customer']->CUSTOMER_ID
        )
      );
    
      $success = ! ! is_mssql_successful(logged_mssql_query($query));
    }
    else 
    {
      $msg = sprintf("ERR_API_INTERNAL Duplicate Login Name exists, requested LOGIN_NAME = %s", $account_login);
      $errors[] = $msg;
    }
    
  }

  return flexi_encode(fill_return($p,
                                  "portal__SetLoginName",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
