<?php


include_once('cosid_constants.php');


// Change the plan immediately for a customer.
function portal__ChangePlanImmediate($partner_tag, $zsession, $targetPlan)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success         = FALSE;
  $account_balance = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__ChangePlanImmediate",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__ChangePlanImmediate", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__ChangePlanImmediate",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) == 0 )
  {
    $customer = $data_zsession['customer'];

    if ( $customer && is_object($customer) )
    {
      $results  = get_customer_state( $customer );
      if (count($results['errors']))
        $errors = $results['errors'];

      if ( ! isSameBrand(get_plan_from_cos_id($customer->COS_ID), $targetPlan))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: previous and future plan brands do not match';

      if ( count($errors) == 0 )
      {
        if ( $results['state']['state'] == 'Active' )
        {
          // verify billing_mrc_plan_info

          $billing_mrc_plan_info = \get_ultra_customer_options_by_customer_id( $customer->CUSTOMER_ID );

          if ( $billing_mrc_plan_info && is_array( $billing_mrc_plan_info ) )
          {
            if ($billing_mrc_plan_info[0] == 'BILLING.MRC_PROMO_PLAN' || $billing_mrc_plan_info[0] == BILLING_OPTION_MULTI_MONTH)
            {
              return flexi_encode(
                fill_return(
                  $p,
                  "portal__ChangePlanImmediate",
                  func_get_args(),
                  array(
                    "success" => $success,
                    "errors"  => array('ERR_API_INVALID_ARGUMENTS: the customer is not eligible to change his plan.')
                  )
                )
              );
            }
          }

          // Plan must be more expensive than the current plan or same value (upgrading from a grandfathered plan)

          $current_plan = get_plan_from_cos_id( $customer->COS_ID );

          if ( $current_plan != $targetPlan )
          {

            $dollar_targetPlan   = substr($targetPlan,-2);
            $dollar_current_plan = substr($current_plan,-2);

            dlog('',"current_plan = $current_plan ($dollar_current_plan)");
            dlog('',"targetPlan   = $targetPlan ($dollar_targetPlan)");

            if ( $dollar_current_plan <= $dollar_targetPlan )
            {
              // transition to new plan
              $result_status = transition_customer_plan($customer,$current_plan,$targetPlan,'Mid-Cycle');

              $errors  = $result_status['errors'];
              $success = $result_status['success'];

              if ( $success )
              {
                $customer = get_customer_from_customer_id( $customer->CUSTOMER_ID );

                if ( $customer && is_object($customer) ) { $account_balance = $customer->BALANCE; }
              }

            }
            else
            { $errors[] = "ERR_API_INVALID_ARGUMENTS: Cannot change plan to a lower one."; }

          }
          else
          { $errors[] = "ERR_API_INVALID_ARGUMENTS: Customer is already in plan $current_plan"; }

        }
        else
        { $errors[] = "ERR_API_INTERNAL: Customer is currently not active."; }

      }
    }
    else
    { $errors[] = "ERR_API_INTERNAL: Database error (1)"; }

  }
  else
  { $errors = $data_zsession['errors']; }

  return flexi_encode(fill_return($p,
                                  "portal__ChangePlanImmediate",
                                  func_get_args(),
                                  array("success"         => $success,
                                        "account_balance" => $account_balance,
                                        "errors"          => $errors)));
}

?>
