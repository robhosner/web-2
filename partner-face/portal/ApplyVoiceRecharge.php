<?php

// Debit the wallet and add Voice Balances.
function portal__ApplyVoiceRecharge($partner_tag, $msisdn, $voice_soc_id)
{
  # voice_soc_id example: 50_100

  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__ApplyVoiceRecharge",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__ApplyVoiceRecharge", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ApplyVoiceRecharge",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));

  teldata_change_db();

  // Get customer from msisdn

  $customer = get_customer_from_msisdn($msisdn);

  if ( $customer )
  {
    // Get the customer plan
    $plan = get_plan_from_cos_id( $customer->COS_ID ); # L[12345]9

    // Get the voice recharge data
    $voice_recharge = find_voice_recharge($plan,$voice_soc_id);

    dlog('',"plan : $plan ; voice_recharge : %s",$voice_recharge);

    try
    {
      $redis = new \Ultra\Lib\Util\Redis;

      if ( get_voice_recharge_semaphore( $redis , $customer->CUSTOMER_ID ) )
        throw new Exception("ERR_API_INTERNAL: a voice recharge has been processed less than 15 minutes ago.");

      if ( ! $voice_recharge )
        throw new Exception("ERR_API_INVALID_ARGUMENTS: voice_soc_id not valid");

      // Verify that the customer is Active

      $state = internal_func_get_state_from_customer_id( $customer->CUSTOMER_ID );

      if ( $state['state'] != STATE_ACTIVE )
        throw new Exception("ERR_API_INVALID_ARGUMENTS: customer state is not Active.");

      // Check for sufficient wallet balance for the given voice recharge

      $customer = get_customer_from_customer_id( $customer->CUSTOMER_ID );

      if ( ( $voice_recharge['cost'] / 100 ) > $customer->BALANCE )
        throw new Exception("ERR_API_INTERNAL: not enough money to perform this operation");

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwMakeitsoUpgradePlan(
        array(
          'actionUUID'         => getNewActionUUID('interactivecare ' . time()),
          'msisdn'             => $customer->current_mobile_number,
          'iccid'              => $customer->CURRENT_ICCID_FULL,
          'customer_id'        => $customer->CUSTOMER_ID,
          'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
          'ultra_plan'         => get_plan_from_cos_id( $customer->COS_ID ), # L[12345]9,
          'preferred_language' => $customer->preferred_language,
          'option'             => 'B-VOICE|' . $voice_recharge['voice_minutes']
        )
      );

      // connect back to the default DB
      teldata_change_db();

      if ( $result->is_failure() )
      {
        // Send failure SMS
        funcSendExemptCustomerSMSVoiceRechargeError(
          array(
            "customer" => $customer,
            "minutes"  => $voice_recharge['voice_minutes']
          )
        );

        dlog('',$result->get_errors());
        throw new Exception("ERR_API_INTERNAL: could not complete voice recharge process (2)");
      }

      // Debit the wallet accordingly

      $result = func_spend_from_balance(
        array(
         'customer_id' => $customer->CUSTOMER_ID,
         'amount'      => ( $voice_recharge['cost'] / 100 ), # in $
         'detail'      => __FUNCTION__,
         'reason'      => 'VOICE Purchase',
         'reference'   => 'VOICE_PAYMENT',
         'source'      => 'SPEND',
         'commissionable'   => 1,
         'reference_source' => create_guid('PHPAPI')
        )
      );

      if ( count($result['errors']) )
      {
        dlog('',$result['errors']);
        throw new Exception("ERR_API_INTERNAL: could not complete voice recharge process (3)");
      }

      // Record event
      $event_status = log_bucket_event(
        array(
          'action'      => 'Recharge ' . $voice_recharge['voice_minutes'] . ' minutes',
          'customer_id' => $customer->CUSTOMER_ID,
          'soc'         => $voice_soc_id
        )
      );

      // Set Redis semaphore to block another ApplyVoiceRecharge by the same customer for 15 minutes.

      set_voice_recharge_semaphore( $redis , $customer->CUSTOMER_ID );

      // Send confirmation via SMS

      funcSendExemptCustomerSMSVoiceRecharge(
        array(
          "customer"       => $customer,
          "cost_amount"    => ( $voice_recharge['cost'] / 100 ),
          "wallet_balance" => ( $customer->BALANCE - ( $voice_recharge['cost'] / 100 ) ),
          "minutes"        => $voice_recharge['voice_minutes']
        )
      );

    }
    catch(Exception $e)
    {
      dlog('', $e->getMessage());
      $errors[] = $e->getMessage();
    }
  }
  else
    $errors[] = array("ERR_API_INVALID_ARGUMENTS: customer not found");

  if ( count($errors) == 0 ) $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "portal__ApplyVoiceRecharge",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
