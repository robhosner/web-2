<?php

include_once 'db/htt_transition_log.php';

// Provides all the useful details on a customer based upon a valid zsession.
function portal__CustomerInfo($partner_tag, $zsession, $get_voice_minutes)
{
  global $p;
  global $mock;
  global $always_succeed;

  $customer    = FALSE;
  $customer_id = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__CustomerInfo",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__CustomerInfo", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__CustomerInfo",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));

  teldata_change_db(); // connect to the DB

  $return_array = array(
    "success" => FALSE,
    "errors"  => array()
  );

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
    $return_array['errors'] = $data_zsession['errors'];
  else
  {
    $results = get_customer_state( $data_zsession['customer'] );

    if ( count($results['errors']) == 0 )
    {
      $customer = $data_zsession['customer'];
      $plan = get_plan_from_cos_id($customer->cos_id);

      $return_array['zero_minutes']           = in_array( $plan, array('L19','L24','L34','L44') ) ? 0 : int_positive_or_zero(1000 - $customer->PERIOD_MINUTES_TO_DATE_BILLED);
      $return_array['recharge_status']        = $results['recharge_status'];
      $return_array['current_msisdn']         = $customer->current_mobile_number;
      $return_array['first_name']             = $customer->FIRST_NAME;
      $return_array['last_name']              = $customer->LAST_NAME;
      $return_array['email']                  = $customer->E_MAIL;
      $return_array['cos']                    = cos_id_plan_description( $customer->cos_id );
      $return_array['customer_plan_start']    = $customer->plan_started_epoch;
      $return_array['plan_expires']           = $customer->plan_expires_epoch;
      $return_array['monthly_renewal_target'] = $customer->MONTHLY_RENEWAL_TARGET;
      $return_array['stored_value']           = $customer->stored_value;
      $return_array['balance']                = $customer->BALANCE;
      $return_array['packaged_balance1']      = $customer->PACKAGED_BALANCE1;
      $return_array['customer_status']        = $customer->plan_state;
      $return_array['customer_loginname']     = $customer->LOGIN_NAME;
      $return_array['customer_id']            = $customer->CUSTOMER_ID;
      $return_array['customer']               = $customer->CUSTOMER;
      $return_array['tos_accepted']           = ! ! $customer->tos_accepted;
      $return_array['auto_recharge']          = ( $customer->monthly_cc_renewal == 1 );
      $return_array['originalsimproduct']     = ( strlen( $customer->CURRENT_ICCID_FULL ) == 19 ? get_product_type_from_iccid( $customer->CURRENT_ICCID_FULL ) : '');
      $return_array['success']                = TRUE;
      $return_array['transition_in_progress'] = ( count_customer_open_transitions( $customer->CUSTOMER_ID ) > 0 ? '1' : '0' );
      $return_array['bolt_ons']               = get_bolt_ons_info_from_customer_options( $customer->CUSTOMER_ID );

      // MVNO-2298 - If account is not Active, zero_minutes = 0
      if ($return_array['customer_status'] != STATE_ACTIVE)
        $return_array['zero_minutes'] = 0;

      // DATAQ-112
      if ( $get_voice_minutes && $plan == 'L34' )
        $return_array['voice_minutes'] = mvneGetVoiceMinutes($customer->customer_id);
      
      // MRP-103
      $multi_month_options = multi_month_info($customer->CUSTOMER_ID);
      $return_array['paid_through'] = $multi_month_options ? strtotime("+{$multi_month_options['months_left']} month", $customer->plan_expires_epoch) : NULL;
      $return_array['duration'] =  $multi_month_options ? $multi_month_options['months_total'] : NULL;

    }
    else
      $return_array['errors'] = $results['errors'];
  }

  $return_array['success'] = ! count($return_array['errors']);

  return flexi_encode(fill_return($p,
                                  "portal__CustomerInfo",
                                  func_get_args(),
                                  $return_array));
}

?>
