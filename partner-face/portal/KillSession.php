<?php

// Clear the current zsession.
function portal__KillSession($partner_tag, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__KillSession",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__KillSession", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__KillSession",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  dlog('',"data_zsession = %s",$data_zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    $session_id = ( isset($_COOKIE['zsession']) )
                   ?
                   $_COOKIE['zsession']
                   :
                   ''
                   ;

    if ( ! $session_id )
    {
      $session_id = substr(create_guid('PHP SESSION'), 1, -1);
    }

    $redis = new \Ultra\Lib\Util\Redis;
    $redis->del($session_id);
    $redis->del("$session_id expiration");

    setcookielive('zsession', '', 0, '/', $_SERVER["SERVER_NAME"]);
    setcookielive('zsessionC', '', 0, '/', $_SERVER["SERVER_NAME"]);

    $success = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "portal__KillSession",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
