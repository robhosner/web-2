<?php

// Set Customer's Password.
function portal__SetPassword($partner_tag, $account_password, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__SetPassword",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__SetPassword", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__SetPassword",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  dlog('',"data_zsession = %s",$data_zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    $query = customers_update_query(
      array(
        'login_password' => \Ultra\Lib\Util\encryptPasswordHS($account_password),
        'customer_id'    => $data_zsession['customer']->CUSTOMER_ID
      )
    );

    $success = ! ! is_mssql_successful(logged_mssql_query($query));
  }

  return flexi_encode(fill_return($p,
                                  "portal__SetPassword",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors"  => $errors)));
}

?>
