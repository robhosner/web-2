<?php

// Remove customer credit card info.
function portal__ClearCreditCard($partner_tag, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $customer = NULL;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ClearCreditCard",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ClearCreditCard", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ClearCreditCard",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $errors = $data_zsession['errors'];
  }
  else
  {
    $customer = $data_zsession['customer'];
  }

  if ( $customer && $customer->CUSTOMER_ID )
  {
    $customers_update_query = customers_update_query(
      array(
        'SET_NULL_CREDIT_CARD_DATA' => TRUE,
        'customer_id'               => $customer->CUSTOMER_ID
      )
    );

    if ( run_sql_and_check($customers_update_query) )
    {
      $success = TRUE;

      // CCCP-18: also update ULTRA.CC_HOLDERS
      $cc_result = \Ultra\Lib\DB\Setter\Customer\disable_cc_info(array('customer_id' => $customer->CUSTOMER_ID));
      if ($cc_result->is_failure())
      {
        $success = FALSE;
        $errors[] = "ERR_API_INTERNAL: failed to disable credit card";
      }
    }
    else
    {
      $errors[] = "ERR_API_INTERNAL: DB error (2)";
    }
  }
  else
  { $errors[] = "ERR_API_INTERNAL: DB error (1)"; }

  return flexi_encode(fill_return($p,
                                  "portal__ClearCreditCard",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
