<?php


include_once('fraud.php');
include_once('lib/inventory/functions.php');


date_default_timezone_set("America/Los_Angeles");


/**
 * Celluphone shipping acknowledges that the SIM kits have arrived at Celluphone.
 *
 * @param string $partner_tag
 * @param string $startICCID
 * @param string $endICCID
 * @param epoch $request_epoch
 * @param string $updated_by
 */
function inventory__StockSIMCelluphone($partner_tag, $startICCID, $endICCID, $request_epoch, $updated_by)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "inventory__StockSIMCelluphone",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "inventory__StockSIMCelluphone", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "inventory__StockSIMCelluphone",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  $fraud_data = array(
    'source' => '', 'dest' => descriptive_ICCID_range($startICCID,$endICCID), 'channel' => $updated_by, 'channel_type' => 'Celluphone User'
  );

  teldata_change_db(); // connect to the DB

  // make sure startICCID exist
  $query = htt_inventory_sim_select_query(array("iccid" => $startICCID));
  if ( ! find_first($query) )
    return flexi_encode(fill_return($p, "inventory__StockSIMCelluphone", func_get_args(), array("success" => FALSE, "errors" => array('ERR_API_INVALID_ARGUMENTS: startICCID does not exist'))));

  // make sure endICCID exist
  $query = htt_inventory_sim_select_query(array("iccid" => $endICCID  ));
  if ( ! find_first($query) )
    return flexi_encode(fill_return($p, "inventory__StockSIMCelluphone", func_get_args(), array("success" => FALSE, "errors" => array('ERR_API_INVALID_ARGUMENTS: endICCID does not exist'))));

  # Riz: I don't think there'll be a need to verify_luhn since we won't have the checksum.
  # $errors = verify_luhn_start_end( $startICCID , $endICCID );

  if ( ! ($errors && count($errors) > 0) )
  {
    list ($valid,$warning) = validate_start_end_ICCID($startICCID, $endICCID, 2500, 2100);

    if ( ! $valid )
    {
      $errors[] = "ERR_INV_TOO_MANY_AFFECTED: too many ICCID affected";
    }
    else
    {
      if ( $warning )
      {
        // audit: 2100 are a high number for many for inventory__StockSIMCelluphone

        fraud_event(NULL, 'inventory', 'StockSIMCelluphone', 'warning', $fraud_data);
      }

      # Validates that ALL have a status of AT_FOUNDRY. If there is at least 1 ICCIDs which does not validate, $success = FALSE
      # The 1st ($startICCID) and the last ($endICCID) should be in HTT_INVENTORY_SIM, but we allow gaps.

      $errors = batch_verify_htt_inventory_sim_status(
        array(
          'startICCID'           => $startICCID,
          'endICCID'             => $endICCID,
          'NOT#inventory_status' => 'AT_FOUNDRY'
        )
      );

      if ( count($errors) == 0 )
      {
        # update HTT_INVENTORY_SIM

        if ( start_mssql_transaction() )
        {

          $errors = batch_update_htt_inventory_sim(
            array(
              'startICCID'        => $startICCID,
              'endICCID'          => $endICCID,
              'inventory_status'  => 'AT_CELLUPHONE',
              'last_changed_date' => 'getutcdate()',
              'last_changed_by'   => $updated_by
            )
          );

          if ($errors && count($errors) > 0)
          {
            if ( ! rollback_mssql_transaction() )
            { $errors[] = "ERR_API_INTERNAL: DB error (3)"; }
          }
          else
          {
            if ( ! commit_mssql_transaction() )
            { $errors[] = "ERR_API_INTERNAL: DB error (2)"; }
          }

        }
        else { $errors[] = "ERR_API_INTERNAL: DB error (1)"; }
      }

      if ( ! ($errors && count($errors) > 0) )
      {
        $success = TRUE;
      }
    }
  }

  $fraud_status = ( $success ) ? 'success'  : 'error' ;

  fraud_event(NULL, 'inventory', 'StockSIMCelluphone', $fraud_status, $fraud_data);

  return flexi_encode(fill_return($p,
                                  "inventory__StockSIMCelluphone",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}


/**
 * Celluphone logs shipping a batch of SIM cards to a particular Master Agent. Use masteragent=(X) for shipments to Shipwire, i.e. the Direct Channel.
 *
 * @param string $partner_tag
 * @param string $startICCID
 * @param string $endICCID
 * @param epoch $request_epoch
 * @param string $updated_by
 * @param integer $masteragent
 */
function inventory__ShipSIMMaster($partner_tag, $startICCID, $endICCID, $request_epoch, $updated_by, $masteragent)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  teldata_change_db(); // connect to the DB

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "inventory__ShipSIMMaster",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "inventory__ShipSIMMaster", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "inventory__ShipSIMMaster",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));

  // make sure startICCID exist
  $query = htt_inventory_sim_select_query(array("iccid" => $startICCID));
  if ( ! find_first($query) )
    return flexi_encode(fill_return($p, 'inventory__ShipSIMMaster', func_get_args(), array("success" => FALSE, "errors" => array('ERR_API_INVALID_ARGUMENTS: startICCID does not exist'))));

  // make sure endICCID exist
  $query = htt_inventory_sim_select_query(array("iccid" => $endICCID  ));
  if ( ! find_first($query) )
    return flexi_encode(fill_return($p, 'inventory__ShipSIMMaster', func_get_args(), array("success" => FALSE, "errors" => array('ERR_API_INVALID_ARGUMENTS: endICCID does not exist'))));

  # Riz: I don't think there'll be a need to verify_luhn since we won't have the checksum.
  # $errors = verify_luhn_start_end( $startICCID , $endICCID );

  $fraud_data = array(
    'source' => '', 'dest' => descriptive_ICCID_range($startICCID,$endICCID), 'channel' => $updated_by, 'channel_type' => 'Celluphone User'
  );

  if ( ! ($errors && count($errors) > 0) )
  {
    list ($valid,$warning) = validate_start_end_ICCID($startICCID, $endICCID, 2500, 2100);

    if ( ! $valid )
    {
      $errors[] = "ERR_INV_TOO_MANY_AFFECTED: too many ICCID affected";
    }
    else
    {
      if ( $warning )
      {
        // audit: 2100 are a high number for many for inventory__ShipSIMMaster

        fraud_event(NULL, 'inventory', 'ShipSIMMaster', 'warning', $fraud_data);
      }

      # Validates that all the ICCIDs have a status of AT_CELLUPHONE,
      # or they have a status of SHIPPED_MASTER with a LAST_CHANGED_DATE within an hour of now.

      $errors = batch_verify_htt_inventory_sim_status(
        array(
          'startICCID'       => $startICCID,
          'endICCID'         => $endICCID,
          'ShipSIMMaster'    => 1,
        )
      );

      if ( count($errors) == 0 )
      {
        # update HTT_INVENTORY_SIM

        $errors = batch_update_htt_inventory_sim(
          array(
            'startICCID'            => $startICCID,
            'endICCID'              => $endICCID,
            'inventory_masteragent' => $masteragent,
            'inventory_status'      => 'SHIPPED_MASTER',
            'last_changed_by'       => $updated_by,
            'last_changed_date'     => 'getutcdate()'
          )
        );
      }

      if ( ! ($errors && count($errors) > 0) )
        $success = TRUE;
    }
  }

  $fraud_status = ( $success ) ? 'success'  : 'error' ;

  fraud_event(NULL, 'inventory', 'ShipSIMMaster', $fraud_status, $fraud_data);

  return flexi_encode(fill_return($p,
                                  "inventory__ShipSIMMaster",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

