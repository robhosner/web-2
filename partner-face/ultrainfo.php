<?php


include_once('session.php');
include_once('db/htt_country_sms_carriers.php');
include_once('db/htt_card_calling_rates.php');
include_once('db/htt_ultra_locations.php');
include_once('db/htt_coverage_info.php');


date_default_timezone_set("America/Los_Angeles");


// Provides the coverage levels of Ultra Service in the specified Zipcode
function ultrainfo__ZIPCoverageInfo($partner_tag, $zipcode)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "ultrainfo__ZIPCoverageInfo",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "ultrainfo__ZIPCoverageInfo", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                    "ultrainfo__ZIPCoverageInfo",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  $quality = 0;

  if ( ! verify_request_source() )
  {
    $errors[] = "ERR_API_ACCESS_DENIED: request source rejected";
  }

  if (! ($errors && count($errors) > 0) )
  {
    # check Redis for an already computed value within 3 days. Return that value if it exists.

    $redis = new \Ultra\Lib\Util\Redis;

    $quality_key = "coverage_info_quality/".$zipcode;

    $quality = $redis->get( $quality_key );

    if ( ! isset($quality) || $quality == '' )
    {
      # Redis does not contain a value for the given zip code, we must query the DB

      teldata_change_db(); // connect to the DB

      # query htt_coverage_info for the given zip code
      $query = htt_coverage_info_select_query( array('zip_code' => $zipcode) );

      $coverage_data = mssql_fetch_all_objects(logged_mssql_query($query));

      if ( $coverage_data && is_array($coverage_data) && count($coverage_data) )
      {
        $quality = $coverage_data[0]->quality;
      }
      else
      {
        $quality = 0;
      }

      # save the result in Redis.
      $redis->set( $quality_key ,  $quality , 3*60*60*24 );
    }

    $success = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "ultrainfo__ZIPCoverageInfo",
                                  func_get_args(),
                                  array("success" => $success,
                                        "quality" => $quality,
                                        "errors" => $errors)));
}

// Provides the calling card rates for the specified product
function ultrainfo__InternationalCallingRates($partner_tag, $product_name)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $rates_list = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "ultrainfo__InternationalCallingRates",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "rates_list" => $rates_list,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "ultrainfo__InternationalCallingRates", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "ultrainfo__InternationalCallingRates",
                                    func_get_args(),
                                    array("success" => $success,
                                          "rates_list" => $rates_list,
                                          "errors" => $errors)));

  teldata_change_db(); // connect to the DB

  # query table htt_card_calling_rates
  $query = htt_card_calling_rates_select_query(
    array(
      'card'    => $product_name,
      'display' => TRUE
    )
  );

  $rates_data = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $rates_data && is_array($rates_data) && count($rates_data) )
  {
    $success = TRUE;
    $rates_list = $rates_data;
  }
  else
    $errors[] = "ERR_API_INTERNAL: data not found";

  $success = ! (count($errors));

  $p['log_skip'] = $success && count($rates_list);

  return flexi_encode(fill_return($p,
                                  "ultrainfo__InternationalCallingRates",
                                  func_get_args(),
                                  array("success"    => $success,
                                        "rates_list" => $rates_list,
                                        "errors"     => $errors)));
}


// Provides a list of all the international locations an Ultra user can SMS.
function ultrainfo__SMSCountryList($partner_tag)
{
  global $p;
  global $mock;
  global $always_succeed;

  $sms_list = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "ultrainfo__SMSCountryList",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "sms_list" => $sms_list,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $success = FALSE;

  $errors = validate_params($p, "ultrainfo__SMSCountryList", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "ultrainfo__SMSCountryList",
                                    func_get_args(),
                                    array("success" => $success,
                                          "sms_list" => array(),
                                          "errors" => $errors)));
  }

  $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "ultrainfo__SMSCountryList",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}


// Provides a list of the topup-only, and dealer locations, registered with Ultra.
function ultrainfo__GetLocations($partner_tag, $latitude, $longitude, $radius, $max_entries, $location_type, $brand)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $warnings = array();
  $location_list = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "ultrainfo__GetLocations",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "location_list" => $location_list,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "ultrainfo__GetLocations", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "ultrainfo__GetLocations",
                                    func_get_args(),
                                    array("success" => $success,
                                          "location_list" => $location_list,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $thresholds = \Ultra\UltraConfig\getDealerLocatorThresholds();

  $thresholdActivations = $thresholds['LOCATOR_ACTIVATION_THRESHOLD'];
  $thresholdMile        = $thresholds['LOCATOR_MILE_THRESHOLD'];
  $thresholdDays        = $thresholds['LOCATOR_DAYS_THRESHOLD'];


  $brand    = \Ultra\BrandConfig\getBrandFromShortName($brand);
  $brand_id = $brand['id'];

  $query_params = array(
    "latitude"      => $latitude,
    "longitude"     => $longitude,
    "max_entries"   => $max_entries,
    "location_type" => $location_type,
    "radius"        => $radius,
    "brand_id"      => $brand_id
  );

  # query table htt_ultra_locations
  $query = htt_ultra_locations_select_query($query_params);

  $locations_data = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $locations_data && is_array($locations_data) && count($locations_data) )
  {
    // encode as UTF-8 to avoid json_encode errors
    foreach($locations_data as &$location)
    {
      $location->RETAILER_NAME = utf8_encode($location->RETAILER_NAME);
      $location->ADDRESS1 = utf8_encode($location->ADDRESS1);
      $location->ADDRESS2 = utf8_encode($location->ADDRESS2);
    }

    $location_list = $locations_data;

    if ($location_type == 'DEALER')
    {
      // 4 groups
      $groups = array(array(), array(), array(), array());

      foreach ($location_list as &$location)
      {
        $group = null;

        if ($location->location_distance_miles <= $thresholdMile && $location->ACTIVATIONS_COUNT >= $thresholdActivations)
          $group = 0;
        else if ($location->location_distance_miles <= $thresholdMile && $location->ACTIVATIONS_COUNT < $thresholdActivations)
          $group = 1;
        else if ($location->location_distance_miles > $thresholdMile && $location->ACTIVATIONS_COUNT >= $thresholdActivations)
          $group = 2;
        else if ($location->location_distance_miles > $thresholdMile && $location->ACTIVATIONS_COUNT < $thresholdActivations)
          $group = 3;

        if ($group)
          $groups[$group][] = $location;
      }

      $location_list = array_merge($groups[0], $groups[1], $groups[2], $groups[3]);
    }
    else
    {
      foreach ($location_list as &$location)
      {
        if ($location->ACTIVATIONS_COUNT >= $thresholdActivations)
          $location->LOCATION_TYPE = 'DEALER';
        else if ($location->ACTIVATIONS_COUNT < $thresholdActivations)
          $location->LOCATION_TYPE = 'TOPUP';
      }
    }

    $success = TRUE;
  }
  else
  {
    $warnings[] = "ERR_API_INTERNAL: data not found";
  }

  return flexi_encode(fill_return($p,
                                  "ultrainfo__GetLocations",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "location_list" => $location_list,
                                        "warnings" => $warnings,
                                        "errors" => $errors)));
}


?>
