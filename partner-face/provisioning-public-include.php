<?php


include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_coverage_info.php');
require_once 'db/ultra_session.php';
include_once('fraud.php');
include_once('lib/inventory/functions.php');
include_once('lib/payments/functions.php');
include_once('lib/provisioning/functions.php');
include_once('lib/state_machine/functions.php');
include_once('lib/transitions.php');
include_once('lib/util-common.php');
require_once 'Ultra/Lib/DB/Celluphone/functions.php';
include_once('partner-face/provisioning/CheckOrangeActCode.php');
include_once('partner-face/provisioning/checkSIMCard.php');
include_once('partner-face/provisioning/GetPortingState.php');
include_once('partner-face/provisioning/requestPortFundedCustomerAsync.php');
include_once('partner-face/provisioning/provision_check_transition.php');
include_once('partner-face/provisioning/requestProvisionOrangeCustomerAsync.php');
include_once('partner-face/provisioning/requestResubmitProvisionPortedCustomerAsync.php');
include_once('partner-face/provisioning/verifyActivateShippedNewCustomerAsync.php');
include_once('partner-face/provisioning/verifyProvisionPortedCustomerAsync.php');

date_default_timezone_set("America/Los_Angeles");


// Check to see if the ZIP Code is in the Activation Footprint.
function provisioning__checkZipCode($partner_tag, $zip_code)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "provisioning__checkZipCode",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "provisioning__checkZipCode", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "provisioning__checkZipCode",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $htt_coverage_info_select_query = htt_coverage_info_select_query( array('zip_code' => $zip_code) );

  $coverage_data = mssql_fetch_all_objects(logged_mssql_query($htt_coverage_info_select_query));

  $success = TRUE;
  $valid   = FALSE;

  if ( $coverage_data && is_array($coverage_data) && count($coverage_data) )
  {
    $valid = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "provisioning__checkZipCode",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors,
                                        "valid"   => $valid)));
}


// This API call is used by the website to load a 'Shipped' SIM customer 'activate' their SIM. This call uses an existing Ultra Customer.
function provisioning__requestActivateShippedNewCustomerAsync($partner_tag, $ICCID, $zsession)
{
  # $ICCID is 19 digits

  global $p;
  global $mock;
  global $always_succeed;

  # [ Pre-Funded ] ~~>> [ Active ]

  # This API call is used by the website to load a 'Shipped' SIM customer 'activate' their SIM. This call uses an existing Ultra Customer, This also assumes NO porting.
  # By the end of this process, if successful, the user is left with a SIM with a valid phone number that is in ACTIVE state.
  # Requires a logged in customer in Shipped State and a valid zsession.

  $success         = FALSE;
  $transition_uuid = '';
  $customer_id     = '';
  $fraud_data      = array(
    'amount'            => 0,
    'cos_id'            => '',
    'activation_userid' => '',
    'transition_uuid'   => create_guid('PROVISIONING_API')
  );

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__requestActivateShippedNewCustomerAsync",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__requestActivateShippedNewCustomerAsync", func_get_args(), $mock);

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $check_session = session_read($zsession);

  dlog("", "check session = ".json_encode($check_session));

  $customer_string = $check_session[0];

  $customer = get_customer_from_customer($customer_string);

  if ( $customer )
  {
    $fraud_data['cos_id'] = $customer->COS_ID;
    $customer_id          = $customer->CUSTOMER_ID;
    $customer             = get_customer_from_customer_id( $customer_id );
  }
  else
    $errors[] = "ERR_API_INTERNAL: user not logged in";

  if ( empty($errors) )
  {
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$ICCID,'VALID');

    if ( $error )
      $errors[] = $error;
  }

  /* *** validate ICCID *** */

  $valid = validate_ICCID($ICCID,1);

  if ( ! $valid ) { $errors[] = "ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used"; }

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** check if we can activate the ICCID *** */

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwCanActivate(
      array(
        'iccid'      => $ICCID,
        'actionUUID' => getNewActionUUID('provisioning ' . time())
      )
    );

    if ( $result->is_failure() )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the given ICCID cannot be activated";
  }

  if ($errors && count($errors) > 0)
  {
    fraud_event($customer, 'provisioning', 'requestActivateShippedNewCustomerAsync', 'error', $fraud_data);

    return flexi_encode(fill_return($p,
                                  "provisioning__requestActivateShippedNewCustomerAsync",
                                    func_get_args(),
                                    array("success"    => $success,
                                          "request_id" => $transition_uuid,
                                          "errors"     => $errors)));
  }

  if ( $customer )
  {

    /* *** get current state *** */
    $state = internal_func_get_state_from_customer_id($customer_id);

    if ( $state )
    {

      if ( $state['state'] == 'Pre-Funded' )
      {

        # update current_iccid

        $ICCID_18 = substr($ICCID,0,-1);

        $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
          array(
            'customer_id'        => $customer_id,
            # 19 digits
            'current_iccid_full' => $ICCID,   # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
            # 18 digits
            'current_iccid'      => $ICCID_18 # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
          )
        );

        if ( is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
        {

          # parameters for change_state #

          $context         = array('customer_id' => $customer->CUSTOMER_ID );
          $result_status   = array( 'success' => 1 , 'errors' => array() );
          $resolve_now     = FALSE;
          $max_path_depth  = 4;
          $dry_run         = TRUE;
          $state_name      = 'Active';
          $targetPlan      = get_plan_from_cos_id( $customer->COS_ID );

          /* *** Tests State Transition *** */

          $result_status = change_state($context, $resolve_now, $targetPlan, $state_name, $dry_run, $max_path_depth);

          if ( $result_status['success'] == 1 )
          {
            /* *** save activation attribution info in redis before attempting state transition *** */

            set_redis_provisioning_values($customer->CUSTOMER_ID, 54, 'WEB', 0, 34, 110); // web activations values

            /* *** If State Transition is allowed (pre-requisites are met), executes *** */

            $dry_run = FALSE;

            $result_status = change_state($context, $resolve_now, $targetPlan, $state_name, $dry_run, $max_path_depth);

            if ( $result_status['success'] == 1 )
            {
              $values = array_values($result_status['transitions']);
              $transition_uuid = end($values);

              $fraud_data['transition_uuid'] = $transition_uuid;

              $success = TRUE;
            }
            else # change_state failed
            {
              // clear activation attribution info from redis
              clear_redis_provisioning_values($customer->CUSTOMER_ID);

              $errors[] = "ERR_API_INTERNAL: state transition error (2)";
            }
          }
          else # change_state failed
          { $errors[] = "ERR_API_INTERNAL: state transition error (1)"; }
        }
        else # update htt_customers_overlay failed
        { $errors[] = "ERR_API_INTERNAL: Database write error"; }
      }
      else # customer state is not 'Pre-Funded'
      { $errors[] = "ERR_API_INTERNAL: customer state (".$state['state'].") is not valid for the given request"; }
    }
    else # cannot determine customer state
    { $errors[] = "ERR_API_INTERNAL: customer state could not be determined"; }
  }
  else
  { $errors[] = "ERR_API_INTERNAL: DB error"; }

  $outcome = ( $success ) ? 'success' : 'error' ;

  fraud_event($customer, 'provisioning', 'requestActivateShippedNewCustomerAsync', $outcome, $fraud_data);

  return flexi_encode(fill_return($p,
                                  "provisioning__requestActivateShippedNewCustomerAsync",
                                  func_get_args(),
                                  array("success"    => $success,
                                        "request_id" => $transition_uuid,
                                        "errors"     => $errors)));
}

