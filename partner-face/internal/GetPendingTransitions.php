<?php

// Get all pending (is_closed_or_aborted is false) transition IDs
function internal__GetPendingTransitions($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__GetPendingTransitions",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__GetPendingTransitions", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__GetPendingTransitions",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db();
  $transitions = internal_func_get_pending_transitions(array('customer_id' => $customer_id));
  $good = (NULL != $transitions);

  return flexi_encode(fill_return($p,
                                  "internal__GetPendingTransitions",
                                  func_get_args(),
                                  array("success" => $good,
                                        'transitions' => $transitions,
                                        "errors" => $errors)));
}

?>
