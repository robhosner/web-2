<?php

include_once('db/htt_action_log.php');

// Check an action
function internal__CheckAction($partner_tag, $action_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__CheckAction",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__CheckAction", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__CheckAction",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  $htt_action_log_select_query = htt_action_log_select_query(
    array(
      "action_uuid" => $action_id
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    $action_uuid       = $query_result[0]->ACTION_UUID;
    $transition_uuid   = $query_result[0]->TRANSITION_UUID;
    $status            = $query_result[0]->STATUS;
    $created           = $query_result[0]->CREATED;
    $pending_since     = $query_result[0]->PENDING_SINCE;
    $closed            = $query_result[0]->CLOSED;
    $action_seq        = $query_result[0]->ACTION_SEQ;
    $action_type       = $query_result[0]->ACTION_TYPE;
    $action_name       = $query_result[0]->ACTION_NAME;
    $action_sql        = $query_result[0]->ACTION_SQL;
    $action_result     = json_decode($query_result[0]->ACTION_RESULT);

    $success = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "internal__CheckAction",
                                  func_get_args(),
                                  array("success"         => $success,
                                        "action_uuid"     => $action_uuid,
                                        "transition_uuid" => $transition_uuid,
                                        "status"          => $status,
                                        "created"         => $created,
                                        "pending_since"   => $pending_since,
                                        "closed"          => $closed,
                                        "action_seq"      => $action_seq,
                                        "action_type"     => $action_type,
                                        "action_name"     => $action_name,
                                        "action_sql"      => $action_sql,
                                        "action_result"   => $action_result,
                                        "errors"          => $errors)));
}

?>
