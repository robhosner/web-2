<?php

// Attempts to recover a Failed Transition.
function internal__RecoverFailedTransition($partner_tag, $transition_uuid)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__RecoverFailedTransition",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__RecoverFailedTransition", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__RecoverFailedTransition",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  if ( strlen($transition_uuid) == 36 )
  {
    $transition_uuid = fix_transition_uuid_format( $transition_uuid );
  }

  list($success,$errors,$warnings,$notes) = recover_failed_transition($transition_uuid);

  return flexi_encode(fill_return($p,
                                  "internal__RecoverFailedTransition",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "notes"    => $notes,
                                        "errors"   => $errors)));
}

?>
