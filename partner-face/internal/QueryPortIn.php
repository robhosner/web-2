<?php

include_once('lib/state_machine/aspider.php');

// MVNE's QueryPortIn.
function internal__QueryPortIn($partner_tag, $customer_id, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $warnings = array();
  $status   = '';
  $message  = '';
  $r_code   = '';
  $carrier_name      = '';
  $port_query_status = '';
  $query_message     = '';
  $query_status      = '';
  $provision_status_date = '';
  $old_service_provider  = '';
  $port_due_time         = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__QueryPortIn",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__QueryPortIn", func_get_args(), $mock);

  if ( ! ( $errors && count($errors) > 0 ) )
    if ( ( ! $customer_id ) && ( ! $msisdn ) )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id or msisdn required.";

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__QueryPortIn",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db();

  $customer = FALSE;

  if ( isset($customer_id) && $customer_id )
  {
    $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
      array(
        'select_fields'    => array("current_mobile_number"),
        'customer_id'      => $customer_id
      )
    );

    $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

    if ( $ultra_customer_data && is_array($ultra_customer_data) && count($ultra_customer_data) )
    {
      $customer = $ultra_customer_data[0];

      if ( $customer->current_mobile_number )
        $msisdn = $customer->current_mobile_number;
      else
        $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id $customer_id has no phone number.";
    }
    else
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id $customer_id not found.";
  }

  if ( $msisdn )
  {
    $msisdn = normalize_msisdn_10($msisdn);

    $customers = get_ultra_customers_from_msisdn($msisdn, array(
      'plan_state', 'CURRENT_ICCID_FULL', 'BRAND_ID'
    ));

    $sim      = null;
    $customer = null;
    if ($customers && count($customers))
    {
      $customer = $customers[0];
      $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);
    }

    if ($customer && $sim && ($customer->BRAND_ID != $sim->BRAND_ID))
    {
      \logit("intra brand port");

      $query_status = ($customer->plan_state != STATE_ACTIVE) ? 'ERROR' : 'COMPLETE';
      $status       = $query_status;
      $provision_status_date = time();
      $carrier_name = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
      $old_service_provider = $carrier_name;
    }
    else
    {
      $portInQueue = new \PortInQueue();

      $result = $portInQueue->loadByMsisdn( $msisdn );

      if ( $result->is_failure() || !property_exists( $portInQueue , 'msisdn' ) || !$portInQueue->msisdn )
        $warnings[] = "No data found in PORTIN_QUEUE for $msisdn";
      else
      {
        if ( property_exists( $portInQueue , 'provstatus_error_msg' ) )
          $port_query_status = $portInQueue->provstatus_error_msg;

        if ( property_exists( $portInQueue , 'carrier_name' ) )
          $carrier_name      = $portInQueue->carrier_name;
      }

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwQueryStatus(
        array(
          'actionUUID'         => getNewActionUUID('internal ' . time()),
          'msisdn'             => $msisdn
        )
      );

      $warnings = array_merge( $warnings , $result->get_warnings() );

      if ( $result->is_success() )
      {
        if ( isset($result->data_array['errors']) && is_array($result->data_array['errors']) && count($result->data_array['errors']) )
          $errors = $result->data_array['errors'];
        elseif( isset( $result->data_array['body'] ) && is_object($result->data_array['body']) )
        {
          // check PORTIN_QUEUE
          $portInQueue = new \PortInQueue();

          $loadByMsisdnResult = $portInQueue->loadByMsisdn( $msisdn );

          if ( $loadByMsisdnResult->is_success() )
          {
            $error_code = ( property_exists( $portInQueue , 'provstatus_error_code' ) ) ? $portInQueue->provstatus_error_code : '';
            $error_msg  = ( property_exists( $portInQueue , 'provstatus_error_msg'  ) ) ? $portInQueue->provstatus_error_msg  : '';

            $r_code  = ( $error_code && $error_msg ) ? $error_code . '-' . $error_msg : '' ;

            $status  = $portInQueue->provstatus_description;
            $query_status      = ( property_exists( $portInQueue , 'querystatus_portstatus' ) ) ? $portInQueue->querystatus_portstatus : '';
            $query_message     = ( property_exists( $portInQueue , 'querystatus_errormsg'   ) ) ? $portInQueue->querystatus_errormsg   : '';

            $port_query_status = $r_code;
            $provision_status_date = $portInQueue->provstatus_epoch;
            $port_due_time = (isset($portInQueue->due_date_time)) ? $portInQueue->due_date_time : null;
            $old_service_provider = $portInQueue->old_service_provider;
          }
          else
          {
            $errors  = $loadByMsisdnResult->get_errors();
            $success = FALSE;
          }
        }
      }
      else
        $errors = $result->get_errors();
    }

    $success = !count($errors);
  }

  return flexi_encode(fill_return($p,
                                  "internal__QueryPortIn",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "status"   => $status,
                                        "r_code"   => $r_code,
                                        "provision_status_date" => $provision_status_date, // MVNE2 only
                                        "query_message"         => $query_message,         // MVNE2 only
                                        "query_status"          => $query_status,          // MVNE2 only
                                        "carrier_name"          => $carrier_name,
                                        "port_query_status"     => $port_query_status,
                                        "port_due_time"         => $port_due_time,
                                        "old_service_provider"  => $old_service_provider,
                                        "message"  => $message,  // MVNE1 only
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

?>
