<?php

include_once('db/htt_action_log.php');

// Gets all actions belonging to a transition
function internal__GetActionsByTransition($partner_tag, $transition_uuid)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $actions = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__GetActionsByTransition",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__GetActionsByTransition", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__GetActionsByTransition",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  // query HTT_ACTION_LOG first

  $htt_action_log_select_query = htt_action_log_select_query(
    array( 'transition_uuid' => $transition_uuid )
  );

  $htt_action_log_data = mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));

  if ( ! ( $htt_action_log_data && is_array($htt_action_log_data) && count($htt_action_log_data) ) )
  {
    // try to query HTT_ACTION_ARCHIVE instead

    $htt_action_log_select_query = htt_action_log_select_query(
      array( 'transition_uuid' => $transition_uuid , 'archive' => TRUE )
    );

    $htt_action_log_data = mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));
  }

  if ( $htt_action_log_data && is_array($htt_action_log_data) && count($htt_action_log_data) )
  {
    $success = TRUE;

    foreach( $htt_action_log_data as $id => $data )
    {
      $actions[] = $data->ACTION_UUID;
      $actions[] = $data->ACTION_SEQ;
      $actions[] = $data->STATUS;
      $actions[] = $data->ACTION_TYPE;
      $actions[] = $data->ACTION_NAME;
      $actions[] = $data->ACTION_RESULT;
    }
  }
  else
  {
    $errors[] = "ERR_API_INTERNAL: no data found";
  }

  return flexi_encode(fill_return($p,
                                  "internal__GetActionsByTransition",
                                  func_get_args(),
                                  array("success" => $success,
                                        "actions" => $actions,
                                        "errors"  => $errors)));
}

?>
