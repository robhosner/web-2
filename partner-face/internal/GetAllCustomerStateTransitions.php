<?php

include_once('db/account_aliases.php');
include_once('db/destination_origin_map.php');

// Get all state transitions for a given customer
function internal__GetAllCustomerStateTransitions($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $balance           = '';
  $packaged_balance1 = '';
  $packaged_balance2 = '';
  $customer_stored_balance = '';
  $account_alias     = '';
  $origin_map_account = '';
  $origin_map_origin  = '';
  $msisdn             = '';
  $iccid              = '';

  $state_transitions = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__GetAllCustomerStateTransitions",
                                    func_get_args(),
                                    array("success"                        => TRUE,
                                          "destination_origin_map_account" => $origin_map_account,
                                          "destination_origin_map_origin"  => $origin_map_origin,
                                          "account_alias"      => $account_alias,
                                          "balance"            => $balance,
                                          "packaged_balance1"  => $packaged_balance1,
                                          "packaged_balance2"  => $packaged_balance2,
                                          "customer_stored_balance"       => $customer_stored_balance,
                                          "state_transitions"  => $state_transitions,
                                          "warnings"           => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__GetAllCustomerStateTransitions", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__GetAllCustomerStateTransitions",
                                    func_get_args(),
                                    array("success"                        => $success,
                                          "destination_origin_map_account" => $origin_map_account,
                                          "destination_origin_map_origin"  => $origin_map_origin,
                                          "account_alias"      => $account_alias,
                                          "balance"            => $balance,
                                          "packaged_balance1"  => $packaged_balance1,
                                          "packaged_balance2"  => $packaged_balance2,
                                          "customer_stored_balance"       => $customer_stored_balance,
                                          "state_transitions"  => $state_transitions,
                                          "errors"             => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = get_ultra_customer_from_customer_id($customer_id,
    array('current_mobile_number', 'CURRENT_ICCID_FULL', 'stored_value'));

  $account = get_account_from_customer_id($customer_id,
    array('BALANCE', 'PACKAGED_BALANCE1', 'PACKAGED_BALANCE2'));

  if ( $customer && $account )
  {
    $result = get_all_customer_state_transitions($customer_id, TRUE);

    $state_transitions = internal_parse_state_transitions( $result['state_transitions'] );

    $warnings = $result['warnings'];
    $errors   = $result['errors'];

    if ( count($errors) == 0 )
    {
      $msisdn = $customer->current_mobile_number;
      $iccid  = $customer->CURRENT_ICCID_FULL;

      // get account alias

      $account_aliases_select_query = account_aliases_select_query( array( 'customer_id' => $customer_id ) ); 

      $account_aliases_data = mssql_fetch_all_objects(logged_mssql_query($account_aliases_select_query));

      if ( $account_aliases_data && is_array($account_aliases_data) && count($account_aliases_data) )
        $account_alias = $account_aliases_data[0]->ALIAS;

      // get DESTINATION_ORIGIN_MAP

      $destination_origin_map_select_query = destination_origin_map_select_query( array( 'destination' => '1' . $customer->current_mobile_number ) );

      $destination_origin_map_data = mssql_fetch_all_objects(logged_mssql_query($destination_origin_map_select_query));

      if ( $destination_origin_map_data && is_array($destination_origin_map_data) && count($destination_origin_map_data) )
      {
        $origin_map_account = $destination_origin_map_data[0]->ACCOUNT;
        $origin_map_origin  = $destination_origin_map_data[0]->ORIGIN;
      }

      // get some customer data

      $balance           = $account->BALANCE;
      $packaged_balance1 = $account->PACKAGED_BALANCE1;
      $packaged_balance2 = $account->PACKAGED_BALANCE2;
      $customer_stored_balance = $customer->stored_value;
      $iccid             = $customer->CURRENT_ICCID_FULL;

      $success = TRUE;
    }
  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "internal__GetAllCustomerStateTransitions",
                                  func_get_args(),
                                  array("success"            => $success,
                                        "warnings"           => $warnings,
                                        "msisdn"             => $msisdn,
                                        "iccid"              => $iccid,
                                        "destination_origin_map_account" => $origin_map_account,
                                        "destination_origin_map_origin"  => $origin_map_origin,
                                        "account_alias"      => $account_alias,
                                        "balance"            => $balance,
                                        "packaged_balance1"  => $packaged_balance1,
                                        "packaged_balance2"  => $packaged_balance2,
                                        "customer_stored_balance"       => $customer_stored_balance,
                                        "state_transitions"  => $state_transitions,
                                        "errors"             => $errors)));
}

/**
 * Parses out PDO Database result set of state transitions into a PHP array.
 * 
 * @param object $state_transitions_data
 * @return array $state_transitions
 */
function internal_parse_state_transitions($state_transitions_data)
{
  $state_transitions = array();

  if ( $state_transitions_data && is_array($state_transitions_data) && count($state_transitions_data) )
  {
    foreach( $state_transitions_data as $n => $data )
    {
      $state_transitions[] = $data->TRANSITION_UUID;
      $state_transitions[] = $data->STATUS;
      $state_transitions[] = $data->FROM_PLAN_STATE;
      $state_transitions[] = $data->TO_PLAN_STATE;
      $state_transitions[] = $data->TRANSITION_LABEL;
      // PLace the Created and Closed Dates in Local Time for ease by Customer Service Troubleshooter -- Requested by Kelvin
      $state_transitions[] = date('m/d/y h:i:s A', strtotime($data->CREATED_PST));
      $state_transitions[] = date('m/d/y h:i:s A', strtotime($data->CLOSED_PST));
      
      $state_transition_errors = '';

      if ( isset( $data->ACTION_RESULT ) && ( $data->ACTION_RESULT != '' ) )
      {
        $action_result = json_decode( $data->ACTION_RESULT );

        if ( $action_result && is_array($action_result) && count($action_result) && $action_result[0]->errors )
        {
          $state_transition_errors = implode( " ; " , $action_result[0]->errors );
        }
      }

      $state_transitions[] = $state_transition_errors;
    }
  }

  return $state_transitions;
}

