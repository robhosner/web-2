<?php

include_once('db/htt_bucket_event_log.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_data_event_log.php');
include_once('db/htt_inventory_pin.php');
include_once('lib/payments/functions.php');
require_once 'Ultra/Lib/DB/Customer.php';
include_once('partner-face/interactivecare/ApplyDataRecharge.php');
include_once('partner-face/interactivecare/ApplyInternationalFixedPromo.php');
include_once('partner-face/interactivecare/ApplyPIN.php');
include_once('partner-face/interactivecare/ApplyUpGlobe.php');
include_once('partner-face/interactivecare/ApplyVoiceRecharge.php');
include_once('partner-face/interactivecare/CalculateTaxesFees.php');
include_once('partner-face/interactivecare/CheckDataRechargeByMSISDN.php');
include_once('partner-face/interactivecare/CheckUpGlobe.php');
include_once('partner-face/interactivecare/CheckVoiceRechargeByMSISDN.php');
include_once('partner-face/interactivecare/GetCommandHelp.php');
include_once('partner-face/interactivecare/SetAutoRechargeByMSISDN.php');
include_once('partner-face/interactivecare/SetLanguagebyMSISDN.php');
include_once('partner-face/interactivecare/ShowCustomerInfo.php');
include_once('partner-face/interactivecare/ApplyRoamRecharge.php');
include_once('partner-face/interactivecare/RoamingBalance.php');
include_once('Ultra/Messaging/Templates.php');

?>
