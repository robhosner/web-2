<?php

// simply invokes CommandInvocation::housekeeping
function middleware__CommandInvocationsHousekeeping($partner_tag)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "middleware__CommandInvocationsHousekeeping",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "middleware__CommandInvocationsHousekeeping", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "middleware__CommandInvocationsHousekeeping",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));

  // connect to ULTRA_ACC DB
  \Ultra\Lib\DB\ultra_acc_connect();

  $commandInvocation = new CommandInvocation();

  $result = $commandInvocation->housekeeping();

  $errors = $result->get_errors();

  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "middleware__CommandInvocationsHousekeeping",
                                  func_get_args(),
                                  array("success"   => $success,
                                        "errors"    => $errors)));
}

?>
