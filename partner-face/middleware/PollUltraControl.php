<?php

// Extracts an Outbound Control Message from the Ultra MW Message queue and process it.
function middleware__PollUltraControl($partner_tag,$communication)
{
  global $p;
  global $mock;
  global $always_succeed;

  $controlUUID = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "middleware__PollUltraControl",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "middleware__PollUltraControl", func_get_args(), $mock);

  if ( !($errors && count($errors) > 0)
    && !\Ultra\UltraConfig\middleware_enabled_amdocs()
  )
    $errors[] = "ERR_API_INTERNAL: middleware currently disabled";

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "middleware__PollUltraControl",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));

  $controlChannel = new \Ultra\Lib\MQ\ControlChannel;

  // select an outbound ( Synch or Asynch ) channel + message
  $getNextOutboundMessageResult = $controlChannel->getNextOutboundMessage( ( ! ! ( $communication == 'asynchronous' ) ) );

  if ( count($getNextOutboundMessageResult) )
  {
    dlog('',"%s",$getNextOutboundMessageResult);

    list( $nextOutboundControlChannel , $message ) = $getNextOutboundMessageResult;

    dlog('',"outbound message = $message");

    $extractedData = $controlChannel->extractFromMessage( $message );

    dlog('',"extractedData = %s",$extractedData);

    if ( !property_exists( $extractedData , 'header' ) || !$extractedData->header )
      return flexi_encode(fill_return($p,
                                      "middleware__PollUltraControl",
                                      func_get_args(),
                                      array("success" => TRUE,
                                            "uuid"    => '',
                                            "errors"  => array('ERR_API_INTERNAL: No header in message'))));

    dlog('',$extractedData->_actionUUID." command = %s , parameters = %s",$extractedData->header,(array) $extractedData->body);

    // $message must be processed by the MW layer and should trigger one or more SOAP requests.

    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

    // execute the $extractedData->header command synchronously
    $controlCommandResult = $accMiddleware->processControlCommand(
      array(
        'command'    => $extractedData->header,
        'parameters' => (array) $extractedData->body,
        'actionUUID' => $extractedData->_actionUUID
      )
    );

    // asynchronous
    if ( $communication == 'asynchronous' )
    {
      dlog('',"asynchronous controlCommandResult = %s",$controlCommandResult);
      dlog('',"asynchronous controlCommandResult data = %s",$controlCommandResult->data_array);

      return flexi_encode(fill_return($p,
                                      "middleware__PollUltraControl",
                                      func_get_args(),
                                      array("success" => ( ! ! $controlCommandResult->is_success() ),
                                            "uuid"    => $extractedData->_actionUUID,
                                            "errors"  => $controlCommandResult->get_errors())));
    }

    if ( $controlCommandResult )
    {
      dlog('',"controlCommandResult = %s",$controlCommandResult);

      if ( $controlCommandResult->is_success() )
        $inboundReply = $controlCommandResult->get_data_key('message');
      else
      {
        $body            = $controlCommandResult->data_array;
        $body['errors']  = $controlCommandResult->get_errors();
        $body['success'] = FALSE;

        $inboundReply = $controlChannel->buildMessage(
          array(
            'header'     => $extractedData->header,
            'body'       => $body,
            'actionUUID' => $extractedData->_actionUUID
          )
        );
      }
    }
    else
      $inboundReply = $controlChannel->buildMessage(
        array( 
          'header'     => $extractedData->header,
          'body'       => array( 'errors' => array('processControlCommand did not return a Result') , 'success' => FALSE ),
          'actionUUID' => $extractedData->_actionUUID
        )
      );

    $nextInboundControlChannel = $controlChannel->toInbound( $nextOutboundControlChannel );

    dlog('',"inbound channel = $nextInboundControlChannel");
    dlog('',"inbound message = %s",$inboundReply);

    $controlUUID = $controlChannel->sendToControlChannel( $nextInboundControlChannel , $inboundReply );
  }
  else
  {
    #dlog('',"No outbound request channel found.");

    $p['log_skip'] = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "middleware__PollUltraControl",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "uuid"    => $controlUUID,
                                        "errors"  => $errors)));
}

/*

How to test:

1) Trigger an outbound control command
   %> php Ultra/Lib/MiddleWare/Adapter/Control_test.php

2) Process the outbound control command
   %> perl runners/amdocs/mw_scheduler.pl

*/

?>
