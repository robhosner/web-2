<?php

include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_bucket_event_log.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_data_event_log.php');
include_once('db/htt_portin.php');
include_once('db/htt_transition_log.php');
include_once('lib/inventory/functions.php');
include_once('lib/internal/functions.php');
include_once('lib/messaging/functions.php');
include_once('lib/payments/functions.php');
include_once('lib/provisioning/functions.php');
include_once('lib/state_machine/functions.php');
include_once('lib/transitions.php');
include_once('lib/util-common.php');
include_once('fraud.php');
include_once("session.php");
include_once('partner-face/portal-public-shipwire.php');
include_once('partner-face/portal/AcceptTermsOfService.php');
include_once('partner-face/portal/ActivateSuspendedCustomer.php');
include_once('partner-face/portal/ApplyDataRecharge.php');
include_once('partner-face/portal/ApplyVoiceRecharge.php');
include_once('partner-face/portal/CalculateTaxesFees.php');
include_once('partner-face/portal/ChangePlanImmediate.php');
include_once('partner-face/portal/ChangePlanFuture.php');
include_once('partner-face/portal/ChangePlanSuspended.php');
include_once('partner-face/portal/CheckDataRechargeByCustomerId.php');
include_once('partner-face/portal/CheckDataRechargeByMSISDN.php');
include_once('partner-face/portal/CheckVoiceRechargeByCustomerId.php');
include_once('partner-face/portal/CheckSession.php');
include_once('partner-face/portal/ClearCreditCard.php');
include_once('partner-face/portal/CustomerDetail.php');
include_once('partner-face/portal/CustomerInfo.php');
include_once('partner-face/portal/ExtendSession.php');
include_once('partner-face/portal/ForgotUsername.php');
include_once('partner-face/portal/GetAllowedDataRechargeByPlan.php');
include_once('partner-face/portal/GetCustomerShipwireOrders.php');
include_once('partner-face/portal/GetIDDHistory.php');
include_once('partner-face/portal/GetMarketingSettings.php');
include_once('partner-face/portal/GetTransactionHistory.php');
include_once('partner-face/portal/KillSession.php');
include_once('partner-face/portal/ReplaceSIMCard.php');
include_once('partner-face/portal/ResetPasswordEmail.php');
include_once('partner-face/portal/ResetPasswordSMS.php');
include_once('partner-face/portal/SetBillingAddress.php');
include_once('partner-face/portal/SetCreditCard.php');
include_once('partner-face/portal/SetCustomerFields.php');
include_once('partner-face/portal/SetLoginName.php');
include_once('partner-face/portal/SetMarketingSettings.php');
include_once('partner-face/portal/SetPassword.php');
include_once('partner-face/portal/SetShippingAddress.php');
include_once('partner-face/portal/UpdateAutoRecharge.php');
include_once('partner-face/portal/VerifyZSession.php');
include_once('classes/postageapp.inc.php');
require_once('Ultra/Lib/DB/Setter/Customer.php');


date_default_timezone_set("America/Los_Angeles");


// Uses existing throttling rules to prevent brute-force attacks.  Checks Memcached for the existance of an override password granted by (func name) for Dealers/CS agents.  Logs in, verifying CUSTOMER.LOGIN_NAME and CUSTOMER.PASSWORD, granting a zsession.
function portal__Login($partner_tag, $account_login, $account_password)
{
  global $p;
  global $mock;
  global $always_succeed;

  $customer  = NULL;
  $success   = FALSE;
  $verified  = FALSE;
  $zsessionC = '';
  $zsession  = '';

# for ultra only valid login usernames are current_msisdn or login_name
# login is not permitted if login_password isn't set

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__Login",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__Login", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__Login",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db();

  $redis = new \Ultra\Lib\Util\Redis;
  $maxAttempts = 5;
  $skipInDev = !\Ultra\UltraConfig\isDevelopmentDB();

  $fraud = checkIfUserIsBlockedFromApi(__FUNCTION__, $account_login, $maxAttempts, $redis);

  if ( $fraud )
  {
    // TODO: we should count *blocked* access attempts in Redis but not log them. (tzz)

    dlog('',"Login attempt blocked by fraud audit check");

    $errors[] = "ERR_API_ACCESS_DENIED";
  }
  else if ($account_password)
  {
    dlog('',"Verifying with given password");

    if (is_numeric($account_login))
    {
      $customer = find_customer(make_find_ultra_customer_query_anycosid($account_login));

      if ( ! $customer )
      {
        # try current_msisdn

        $customer = get_customer_from_msisdn($account_login);
      }
    }
    else
    {
      $customer = find_customer(make_find_ultra_customer_query_anycosid(-1, $account_login));
    }

    $temp_password = '';

    if ( $customer )
    {
      # the password used in ResetPasswordSMS
      $temp_password = func_read_temp_password($customer->CUSTOMER_ID, 'reset_password');

      /* *** get current state *** */
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( $state )
      {
        if ( $state['state'] == STATE_CANCELLED )
          $errors[] = "ERR_API_INVALID_ARGUMENTS: customer's account has been cancelled.";
        // PROD-566: reject Neutral logins
        elseif ($state['state'] == STATE_NEUTRAL)
          $errors[] = 'ERR_API_INVALID_ARGUMENTS: account is not yet activated.';
      }
      else
      { $errors[] = "ERR_API_INTERNAL: cannot determine customer status"; }
    }
    else
    {
      checkApiAbuseByIdentifier(__FUNCTION__, $account_login, $maxAttempts, $redis, true, false, $skipInDev);
      $errors[] = "ERR_API_INTERNAL: login failed";
    }

    if ( ! count($errors) )
    {
// TODO: $data
$data = array();

      if ($customer && ( 
           ( $account_password === $customer->LOGIN_PASSWORD || \Ultra\Lib\Util\authenticatePasswordHS($customer->LOGIN_PASSWORD, $account_password) ) 
        || ( ( $temp_password != '' ) && ( $account_password === $temp_password ) ) 
      ) )
      {
        dlog('',"Password is good, saving session");

        $_REQUEST['zsession'] = session_save($customer);

        $verified = verify_session( $customer->CUSTOMER );

        // audit successful login
        fraud_event($customer, 'customer_login', 'login', 'success', $data);
      }
      else if ($customer)
      {
        dlog('',"Password did not match, NOT saving session");

        checkApiAbuseByIdentifier(__FUNCTION__, $account_login, $maxAttempts, $redis, true, false, $skipInDev);
        // audit failed login
        fraud_event(null, 'customer_login', 'login', 'failed_wrongpassword', $data);

        $errors[] = 'ERR_API_INVALID_ARGUMENTS: Incorrect Password';
      }
      else
      {
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: Incorrect Login Information';

        checkApiAbuseByIdentifier(__FUNCTION__, $account_login, $maxAttempts, $redis, true, false, $skipInDev);
        // audit failed login
        fraud_event(null, 'customer_login', 'login', 'failed_usernotfound', $data);
      }
    }

  }

  if ($verified)
  {
    // value and expiration of the cookie come from the session
    setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
    setcookielive('zsessionC', $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);

    $zsessionC = $customer->CUSTOMER;
    $zsession  = $verified[2];

    $success = TRUE;
  }

  return flexi_encode(fill_return($p,
                                  "portal__Login",
                                  func_get_args(),
                                  array("success"   => $success,
                                        "zsession"  => $zsession,
                                        "zsessionC" => $zsessionC,
                                        "errors"    => $errors)));
}


// Creates a Customer, with a proper login name and password. Sets Neutral COS and customer info (should match old API call)
function portal__CreateWebUser($partner_tag, $account_login, $account_first_name, $account_last_name, $account_password, $account_number_email, $account_country, $preferred_language)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;
  $customer_id = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__CreateWebUser",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__CreateWebUser", func_get_args(), $mock);

  if ( is_numeric($account_login) )
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_login cannot be a number.";

  if ( ! validate_first_last_name($account_first_name) )
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_first_name contains invalid characters.";

  if ( ! validate_first_last_name($account_last_name) )
    $errors[] = "ERR_API_INVALID_ARGUMENTS: account_last_name contains invalid characters.";

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__CreateWebUser",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db(); // connect to the DB

  $redisKey = 'ultra/create/username' . $account_login;
  $redis = new \Ultra\Lib\Util\Redis(FALSE);

  if ($redis->get($redisKey))
    $errors[] = 'This username is already taken'; 
  else
  {
    $redis->setnx($redisKey, getmypid());

    if ($redis->get($redisKey) != getmypid())
      $errors[] = 'This username is already taken';
    else
    {
      # English is the default preferred language
      if ( is_null($preferred_language) || ! $preferred_language )
        $preferred_language = 'EN';

      $create_ultra_customer_params = array(
        'cos_id'         => get_cos_id_from_plan('STANDBY'),
        'first_name'     => $account_first_name,
        'last_name'      => $account_last_name,
        #'postal_code'    => $customerZip,
        'country'        => $account_country,
        'e_mail'         => $account_number_email,
        'login_name'     => $account_login,
        'login_password' => $account_password,
        "plan_state"            => 'Neutral',
        "plan_started"          => 'NULL',
        "plan_expires"          => 'NULL',
        "tos_accepted"          => 1,
        "customer_source"       => 'ULTRAME',
        "preferred_language"    => $preferred_language
      );

      # users created via web interface should have unique email address
      $errors1 = validate_unique_e_mail($account_number_email,$mock);

      $errors2 = validate_username_characters($account_login);

      $errors3 = validate_unique_username($account_login);

      $errors  = array_merge( $errors1, $errors2, $errors3 );
    }
  }

  if ( count($errors) == 0 )
  {
    $result = create_ultra_customer_db_transaction($create_ultra_customer_params);

    if ( isset($result['errors']) && count( $result['errors'] ) > 0 )
      $errors = $result['errors'];
    else if ( ! isset($result['customer']) )
      $errors[] = "ERR_API_INTERNAL: DB error"; 
    else
    {
      $customer_id = $result['customer']->CUSTOMER_ID;

      $success = TRUE;

      $email_sent = funcSendExemptCustomerEmail_ultra_account_created(
        array(
          'customer' => $result['customer']
        )
      );

      if ( ! $email_sent )
        dlog('',"Could not send email to customer ".$customer_id);
    }
  }

  $redis->del($redisKey);

  return flexi_encode(fill_return($p,
                                  "portal__CreateWebUser",
                                  func_get_args(),
                                  array("success"     => $success,
                                        "customer_id" => $customer_id,
                                        "errors"      => $errors)));
}


function validate_order_online_costs($chargeAmount, $targetPlan)
{
  # Total amount of SIM Cost + First Month Plan + Shipping.

  $errors = array();

  $sim_cost      = sim_cost();
  $shipping_cost = shipping_cost();
  $plan_cost     = ( substr($targetPlan,-2) * 100 ); # L[12345]9 => [12345]900

  dlog('',"validate_order_online_costs total cost   = ".( $sim_cost + $shipping_cost + $plan_cost ));
  dlog('',"validate_order_online_costs chargeAmount = ".( $chargeAmount * 100 ));

  if ( ( $sim_cost + $shipping_cost + $plan_cost ) > ( $chargeAmount * 100 ) )
  {
    $errors = array("ERR_API_INVALID_ARGUMENTS: insufficient amount");
  }

  return $errors;
}

