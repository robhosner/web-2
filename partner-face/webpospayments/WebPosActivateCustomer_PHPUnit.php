<?php

require_once 'classes/PHPUnitBase.php';

class WebPosActivateCustomerTest extends PHPUnitBase
{
  public function setUp()
  {
    parent::setUp();
    $this->testICCID  = '8901260842107734395';
    $this->testICCID2 = '8901260842107734718';
  }

  public function test__externalpayments__WebPosActivateCustomer()
  {
    // API setup
    list($test, $partner, $api) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'bath'      => 'soap',
      'version'   => 1,
      'partner'   => 'webpospayments',
      'wsdl'      => 'partner-meta/webpospayments.wsdl'));

    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    print_r($result);
    $this->assertGreaterThan(0, count($result->errors));

    // invalid dealer TMO ID
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '890126084210773967',
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'TMOMT',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: No celluphone dealer found for TMO code TMOMT', $result->errors);

    // inactive dealer
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '890126084210773967',
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'TMCABC0002',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: Dealer is not active', $result->errors);

    // used ICCID
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '101010101010101010',
      'zipcode'           => '11111',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '12345678901234',
      'load_amount'       => '1900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    // $this->assertContains('ERR_API_INVALID_SIM: The given ICCID cannot be activated', $result->errors);
    $this->assertFalse(! ! $result->success);

    // invalid zip code
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '99723', // Alaska, no coverage
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '12345678901234',
      'load_amount'       => '1900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ZIP: ZIP Code 99723 is not in coverage. Choose a valid Zip.', $result->errors);

    // invalid data bolt on
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_data'      => 'MONEY_SURFS',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '12345678901234',
      'load_amount'       => '1900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: invalid bolt on MONEY_SURFS', $result->errors);

    // invalid INTL bolt on
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'MONEY_TALKS',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '12345678901234',
      'load_amount'       => '1900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: invalid bolt on MONEY_TALKS', $result->errors);

    // invalid ePay SKU
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '12345678901234',
      'load_amount'       => '1900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: invalid SKU 12345678901234 for provider EPAY', $result->errors);

    // invalid SKu amount
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '2900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: invalid amount 2900 for EPAY SKU 00843788021269', $result->errors);

    // invalid plan name
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'SIXTY_NINE',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: invalid plan SIXTY_NINE', $result->errors);

    // invalid bolt on for given plan
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'TWENTY_FOUR',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: bolt on DATA_500_5 cannot be used with plan TWENTY_FOUR', $result->errors);

    // invalid subproduct
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PG13',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains('ERR_EPY_INVALID_SUBPRODUCT_ID: PG13 is not a valid subproduct_id', $result->errors);

    // invalid ICCID
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '890126084210773954',
      'zipcode'           => '11249',
      'language'          => 'ES',
      'provider_name'     => 'EPAY',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    // $this->assertContains('ERR_API_INVALID_SIM: The given ICCID cannot be activated', $result->errors);
    $this->assertFalse( ! ! $this->success);

    // duplicate API call
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '101010101010101042',
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    // $this->assertContains('ERR_API_INVALID_ARGUMENTS: This SIM has been already used', $result->errors);
    $this->assertFalse( ! ! $this->success);

    // success without bolt ons
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID,
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // success without bolt ons
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => $this->testICCID2,
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788021269', // $19.00
      'load_amount'       => '1900',
      'plan_name'         => 'NINETEEN',
      'bolt_on_intl'      => 'IDDCA_6.25_5',
      'bolt_on_data'      => 'DATA_500_5',
      'subproduct_id'     => 'PQ19',
      'tmo_id'            => 'aabbccdd',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // successfull API-267 test
    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => '101010120151120103',
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788035471', // UV20
      'load_amount'       => '2000',
      'plan_name'         => 'UVTWENTY',
      'subproduct_id'     => 'UV20',
      'tmo_id'            => 'TMoCodeXYZ',
      'brand'             => 'UNIVISION');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => 1010101312405131017,
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788026288', // S29
      'load_amount'       => '2900',
      'plan_name'         => 'STWENTY_NINE',
      'subproduct_id'     => 'S29',
      'tmo_id'            => 'TMoCodeXYZ',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    $params = array(
      'partner_tag'       => 'tag',
      'request_epoch'     => time(),
      'iccid'             => 1010101148205131025,
      'zipcode'           => '11249',
      'provider_name'     => 'EPAY',
      'provider_trans_id' => rand(111111111, 999999999),
      'sku'               => '00843788028831', // S44
      'load_amount'       => '4400',
      'plan_name'         => 'SFORTY_FOUR',
      'subproduct_id'     => 'S44',
      'tmo_id'            => 'TMoCodeXYZ',
      'brand'             => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertNotEmpty($result->ultra_payment_trans_id);
  }
}