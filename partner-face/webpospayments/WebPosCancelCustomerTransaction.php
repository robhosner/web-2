<?php

/*
 * Cancel WebPos / EPP new SIM Activation or Port-In attempt.
 * Any submitted activation is queued for 90 seconds in DB table ULTRA.EPP_ACTIONS, it can be voided using this API during this time frame.
 */
function externalpayments__WebPosCancelCustomerTransaction($partner_tag, $request_epoch, $iccid, $cancel_type, $provider_trans_id, $ultra_payment_trans_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "externalpayments__WebPosCancelCustomerTransaction",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  // phone number validation is included in this call
  $errors = validate_params($p, "externalpayments__WebPosCancelCustomerTransaction", func_get_args(), $mock);

  $ultra_trans_epoch      = NULL;
  $request                = NULL;

  if ( ! ($errors && count($errors) > 0) )
  {
    teldata_change_db(); // connect to the DB

    $transaction = ultra_webpos_actions_get_transaction($ultra_payment_trans_id);

    if ( ! $transaction )
      $errors = array("ERR_API_INVALID_ARGUMENTS: Could not find transaction for ID : $ultra_payment_trans_id");
    else
    {
      // invalid request if input ICCID does not match the ICCID in the ULTRA.WEBPOS_ACTIONS row
      if ($transaction->ICCID != luhnenize($iccid))
      {
        $errors = array('ERR_API_INVALID_ARGUMENTS: input ICCID does not match transaction data');
      }
      elseif ($transaction->TRANSACTION_ID != $provider_trans_id)
      {
        $errors = array('ERR_API_INVALID_ARGUMENTS: input PROVIDER_TRANS_ID does not match transaction data');
      }
      else
      {
        $ultra_trans_epoch = date_to_epoch($transaction->CREATED);

        // error if transaction is greater than 90 seconds or not open
        if (time() - $ultra_trans_epoch > \Ultra\UltraConfig\getPaymentProviderDelay('WEBPOS') || $transaction->STATUS != 'OPEN')
          $errors = array("ERR_API_INVALID_ARGUMENTS: Transaction in invalid status to cancel for ID : $ultra_payment_trans_id");
        else
        {
          if ( ! ultra_webpos_actions_update_transaction_status($ultra_payment_trans_id, $cancel_type))
            $errors = array("ERR_API_INVALID_ARGUMENTS: ERROR updating database for UUID : $ultra_payment_trans_id");
        }
      }
    }
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "externalpayments__WebPosCancelCustomerTransaction",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));

  $output_array = fill_return($p,
                              "externalpayments__WebPosCancelCustomerTransaction",
                              func_get_args(),
                              array('success'                 => TRUE,
                                    'errors'                  => $errors,
                                    'warnings'                => array(),
                                    'partner_tag'             => $partner_tag,
                                    'ultra_payment_trans_id'  => $ultra_payment_trans_id,
                                    'ultra_trans_epoch'       => $ultra_trans_epoch ));

  return flexi_encode($output_array);
}

