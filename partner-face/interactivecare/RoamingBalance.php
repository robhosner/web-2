<?php

require_once 'classes/CheckBalance.php';

function interactivecare__RoamingBalance($partner_tag, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $apiResult = new \VersionOneResult($p, 'interactivecare__RoamingBalance');
  $apiResult->setArgs(func_get_args(), $mock);

  if ($apiResult->hasErrors())
    return $apiResult->getJSON();

  try
  {
    teldata_change_db();

    $customer = get_customer_from_msisdn($msisdn,
      'current_mobile_number,
      preferred_language,
      u.BRAND_ID'
    );
    if ( ! $customer)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: invalid customer id');

    $checkBalance = new \Ultra\Lib\CheckBalance();
    $cbResult = $checkBalance->byMSISDN($customer->current_mobile_number);

    \logDebug(sprintf('CheckBalance result: %s', json_encode($cbResult)));

    if ( ! $cbResult)
      throw new Exception('ERR_API_INTERNAL: error receiving network information');

    $roaming_balance = isset($cbResult['roaming_balance']) 
      ? $cbResult['roaming_balance']
      : 0;

    $message = \Ultra\Messaging\Templates\SMS_by_language(
      [
        'message_type'     => 'customerinfo_roaming',
        'roaming_balance' => $roaming_balance / 100
      ],
      $customer->preferred_language,
      $customer->BRAND_ID
    );

    $apiResult->addData('message', $message);

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    $apiResult->addError($error);
  }

  return $apiResult->getJSON();
}