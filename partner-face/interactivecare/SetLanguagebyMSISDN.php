<?php

/**
  * Modifies customer's preferred language
  *
  * called by 3ci campaign 'lang', IVR
  *
  */
function interactivecare__SetLanguagebyMSISDN($partner_tag, $msisdn, $preferred_language, $notify)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "interactivecare__SetLanguagebyMSISDN",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "interactivecare__SetLanguagebyMSISDN", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "interactivecare__SetLanguagebyMSISDN",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));

  teldata_change_db();

  $msisdn = normalize_msisdn_10($msisdn);

  $customer = get_customer_from_msisdn($msisdn);

  if ( $customer && $customer->plan_state != 'Cancelled' )
  {
    if ( $customer->MVNE == '2' )
    {
      // change voicemail SOC
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $languageSOC = \Ultra\UltraConfig\mapLanguageToVoicemailSoc( $preferred_language );

      list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

      $result = $mwControl->mwMakeitsoUpgradePlan(
        array(
          'actionUUID'         => $request_id,
          'msisdn'             => $msisdn,
          'iccid'              => $customer->CURRENT_ICCID_FULL,
          'customer_id'        => $customer->CUSTOMER_ID,
          'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
          'ultra_plan'         => get_plan_from_cos_id( $customer->COS_ID ), # L[12345]9,
          'preferred_language' => $customer->preferred_language,
          'option'             => $accSocsDefinitionByUltraSoc[ $languageSOC ]['ultra_service_name']
        )
      );

      if ( $result->is_failure() )
      {
        dlog('',"errors = %s",$result->get_errors());

        $errors[] = "ERR_API_INTERNAL: Middleware error.";
      }

      teldata_change_db();
    }

    if ( !count($errors) )
    {
      $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
        array(
          'customer_id'        => $customer->CUSTOMER_ID,
          'preferred_language' => $preferred_language
        )
      );

      if ( ! run_sql_and_check($htt_customers_overlay_ultra_update_query) )
        $errors[] = "ERR_API_INTERNAL: DB error.";
    }

    // AMDOCS-415: note language change in HTT_BILLING_HISTORY
    $description = 'SMS' . ($customer->MVNE == '2' ? '+VM' : NULL) . " {$customer->preferred_language} -> {$preferred_language}";
    $params = array(
      'customer_id'            => $customer->CUSTOMER_ID,
      'date'                   => 'now',
      'cos_id'                 => $customer->COS_ID,
      'entry_type'             => 'LANGUAGE',
      'stored_value_change'    => 0,
      'balance_change'         => 0,
      'package_balance_change' => 0,
      'charge_amount'          => 0,
      'reference'              => 'SetLanguagebyMSISDN',
      'reference_source'       => 0,
      'detail'                 => 'change language',
      'description'            => $description,
      'result'                 => 'COMPLETE',
      'source'                 => 'API',
      'is_commissionable'      => NULL
    );
    $query = htt_billing_history_insert_query($params);
    if ( ! is_mssql_successful(logged_mssql_query($query)))
      $errors[] = 'ERR_API_INTERNAL: DB error';

    // MVNO-2865: also notify via SMS if requested
    if ($notify)
    {
      $result = funcSendCustomerSMS_internal(array(
        'customer'   => $customer,
        'message'    => \Ultra\Messaging\Templates\SMS_by_language(
          array('message_type' => 'language_notify'),
          $preferred_language,
          $customer->BRAND_ID
        ),
        'client_tag' => time()));
      if ( ! $result['sent'])
        dlog('', 'WARNING: failed to send notification SMS');
    }
  }
  else
    $errors[] = "ERR_API_INTERNAL: Ultra Mobile subscriber not found.";

  return flexi_encode(fill_return($p,
                                  "interactivecare__SetLanguagebyMSISDN",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors"  => $errors)));
}

?>
