<?php

function interactivecare__ApplyDataRecharge($partner_tag, $customer_id, $data_soc_id)
{
  $retval = json_decode(interactivecare__doApplyDataRecharge($partner_tag, $customer_id, $data_soc_id),TRUE);

  $clean_error=count($retval['errors']);

  if (count($retval['errors'])) {
    $clean_error = preg_replace ('/ERR_API_[^:]+: /', '', $retval['errors'][0]);
  }

  $retval['clean_error']=$clean_error;
  return json_encode($retval);
}

// Debit the wallet and add Data Balances.
function interactivecare__doApplyDataRecharge($partner_tag, $customer_id, $data_soc_id)
{
  # $data_soc_id example: PayGo_250_500

  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "interactivecare__ApplyDataRecharge",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "interactivecare__ApplyDataRecharge", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "interactivecare__ApplyDataRecharge",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db();

  // Get account from customer id

  $account = get_account_from_customer_id( $customer_id, array('CUSTOMER_ID','COS_ID','BALANCE') );

  if ( $account )
  {
    // Get the customer plan
    $plan = get_plan_from_cos_id( $account->COS_ID ); # L[12345]9

    // Get the data recharge data
    $data_recharge = find_data_recharge($plan,$data_soc_id);

    dlog('',"plan : $plan ; data_recharge : %s",$data_recharge);

    try
    {
      $redis = new \Ultra\Lib\Util\Redis;

      if ( get_data_recharge_semaphore( $redis , $account->CUSTOMER_ID ) )
        throw new Exception("ERR_API_INTERNAL: A data recharge has been processed less than 15 minutes ago.");

      if ( ! $data_recharge )
        throw new Exception("ERR_API_INVALID_ARGUMENTS: data_soc_id not valid");

      // Verify that the customer is Active

      $state = internal_func_get_state_from_customer_id( $account->CUSTOMER_ID );

      if ( $state['state'] != STATE_ACTIVE )
        throw new Exception("ERR_API_INVALID_ARGUMENTS: Customer state is not Active.");

      // Check for sufficient wallet balance for the given data SOC

      $customer = get_customer_from_customer_id( $account->CUSTOMER_ID );

      if ( $data_recharge['cost'] > $account->BALANCE )
        throw new Exception("ERR_API_INTERNAL: Not enough money to perform this operation");

      // initialize the context for the sequence of actions we will generate
      $context = array(
        'customer_id' => $account->CUSTOMER_ID
      );

      $action_seq         = 0;
      $action_transaction = NULL; # no TRANSITION_UUID yet

      // add action to validate balance
      $result_status = append_make_funcall_action(
        'assert_balance',
        $context,
        $action_transaction,
        $action_seq,
        ( $data_recharge['cost'] * 100 ),
        NULL,
        NULL,
        NULL,
        'interactivecare__ApplyDataRecharge'
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (1)");

      // enqueue to the TRANSITION_UUID created with the previous append_make_funcall_action
      $context['transition_id'] = $result_status['transitions'][0];

      $action_seq++;

/*
A-DATA-BLK         MB

L19 , L29       => 50,250,500
L39 , L49 , L59 => 500
*/
      $result_status = append_make_funcall_action(
        'mvneMakeitsoUpgradePlan',
        $context,
        $action_transaction,
        $action_seq,
        DEFAULT_ACC_DATA_ADD_ON . '|' . $data_recharge['MB'] // example: 'A-DATA-BLK|250'
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (2)");

      $action_seq++;

      // Debit the wallet accordingly, including the name of the data recharge in the HTT_BILLING_LOG entry.
      $result_status = append_make_funcall_action(
        'spend_from_balance',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge['cost'], # amount in dollars
        __FUNCTION__,           # detail
        'DATA Purchase',        # reason
        create_guid('PHPAPI')   # reference source
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (4)");

      $action_seq++;

      // send SMS to customer
      $result_status = append_make_funcall_action(
        'send_sms_data_recharge',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge["MB"]
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (5)");

      $action_seq++;

      // Track Data Socs to htt_customers_overlay_ultra
      $result_status = append_make_funcall_action(
        'record_customer_soc',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge['data_soc']
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (6)");

      unreserve_transition_uuid_by_pid($context['transition_id']);

      // Record event
      $event_status = log_data_event(
        array(
          'action'      => 'Recharge ' . $data_recharge['MB'] . 'MB',
          'customer_id' => $account->CUSTOMER_ID,
          'soc'         => $data_recharge['data_soc']
        )
      );

      // Set Redis semaphore to block another ApplyDataRecharge by the same customer for 15 minutes.
      set_data_recharge_semaphore($redis,$account->CUSTOMER_ID);

      // Set Redis semaphore to block SMS from notification__DataNotificationHandler for 5 minutes.
      set_data_recharge_notification_delay($redis,$account->CUSTOMER_ID);
    }
    catch(Exception $e)
    {
      dlog('', $e->getMessage());
      $errors[] = $e->getMessage();
    }
  }
  else
    $errors = array("ERR_API_INVALID_ARGUMENTS: Customer not found");

  if ( ! count($errors) ) $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "interactivecare__ApplyDataRecharge",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
