<?php

use function Ultra\UltraConfig\isBPlan;

require_once 'classes/CheckBalance.php';
require_once 'classes/Flex.php';
require_once 'classes/MessagingEvent/MessagingEvent.php';
require_once 'classes/Session.php';

function interactivecare__ShowCustomerInfoRefactor($partner_tag, $msisdn, $get_data_usage, $language, $customer)
{
  global $p;
  global $mock;
  global $always_succeed;

  $apiResult = new \VersionOneResult($p, 'interactivecare__ShowCustomerInfo');
  $apiResult->setArgs(func_get_args(), $mock);

  if ($apiResult->hasErrors())
    return $apiResult->getJSON();

  $message = '';

  $customer_id = $customer->CUSTOMER_ID;
  $plan_short_name = get_plan_from_cos_id($customer->COS_ID);
  $preferred_language = $customer->preferred_language;

  try
  {
    $flexInfo = \Ultra\Lib\Flex::getInfoByCustomerId($customer->CUSTOMER_ID);
    if ( ! $flexInfo)
      throw new \Exception('ERROR retrieving customer info');

    $event = null;

    if ($get_data_usage)
    {
      $event = ($flexInfo['memberCount'] > 1)
        ? 'flex_family_customerinfo_data'
        : 'flex_customerinfo_data';

    }
    else
      $event = 'flex_customerinfo_voice_no_zero';

    $msgEvent = new \Ultra\Lib\MessagingEvent(
      $customer->CUSTOMER_ID,
      $event,
      [],
      $customer->current_mobile_number,
      $customer->preferred_language,
      $customer->BRAND_ID
    );

    $messages = $msgEvent->getMessages();
    if (count($messages))
      $message  = $messages[0];

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    $apiResult->addError($error);
  }

  $apiResult->addData('customer_id', $customer_id);
  $apiResult->addData('message', $message);
  $apiResult->addData('short', $plan_short_name);
  $apiResult->addData('preferred_language', $preferred_language);

  return $apiResult->getJSON();
}

/**
 * Return cash balance and plan renewal date to customer
 *
 * Called by 3ci 'lang' campaing, IVR
 *
 */
function interactivecare__ShowCustomerInfo($partner_tag, $msisdn, $get_data_usage, $language)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $message = '';
  $success = FALSE;
  $preferred_language = 'EN';
  $plan_short_name = NULL;
  $remaining = NULL;
  $usage = NULL;
  $voice_minutes = NULL;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "interactivecare__ShowCustomerInfo",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "interactivecare__ShowCustomerInfo", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "interactivecare__ShowCustomerInfo",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  if ( (strlen($msisdn) == 11) )
    $msisdn = substr($msisdn, 1);

  $customer = get_customer_from_msisdn($msisdn);

  if ( $customer && is_object($customer) )
  {
    /*
     * branch into refactored messaging code
     */
    if (\Ultra\Lib\Flex::isFlexPlan($customer->COS_ID))
      return interactivecare__ShowCustomerInfoRefactor($partner_tag, $msisdn, $get_data_usage, $language, $customer);

    $plan_short_name = get_plan_from_cos_id($customer->COS_ID);

    /* *** get current state *** */
    $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

    if ( $state )
    {
      // parameters common for both types of requests
      $preferred_language = $customer->preferred_language;

      $plan_cost = get_plan_costs_by_customer_id($customer->CUSTOMER_ID, $customer->COS_ID);

      $renewal_date = get_date_from_full_date( $customer->latest_plan_date );
      $renewal_date = substr($renewal_date,0,strlen($renewal_date)-5);

      $checkBalance = new \Ultra\Lib\CheckBalance();
      $cbResult = $checkBalance->byMSISDN($customer->current_mobile_number);

      // data request parameters
      if ($customer->plan_state == STATE_ACTIVE)
      {
        if ($get_data_usage)
        {
          list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown , $mvneError, $threeGBreakDown) = mvneGet4gLTE($request_id, $msisdn, $customer->CUSTOMER_ID);

          $plan_limit = isset($cbResult['a_data_blk_plan_limit']) ? $cbResult['a_data_blk_plan_limit'] : 0;
          $plan_used  = isset($cbResult['a_data_blk_plan_used'])  ? $cbResult['a_data_blk_plan_used']  : 0;

          $block_limit = isset($cbResult['a_data_blk_limit']) ? $cbResult['a_data_blk_limit'] : 0;
          $block_used  = isset($cbResult['a_data_blk_used'])  ? $cbResult['a_data_blk_used']  : 0;

          $remaining = in_array($customer->COS_ID, ['100003', '100007']) ? 'unlimited' : beautify_4g_lte_string( $plan_limit + $block_limit, FALSE );
          $usage     = beautify_4g_lte_string( $plan_used + $block_used, TRUE );
        }
        else if ( get_plan_from_cos_id( $customer->COS_ID ) == 'L34' )
        {
          $voice_minutes = mvneGetVoiceMinutes($customer->CUSTOMER_ID);
        }
      }

      teldata_change_db(); // connect to the default DB

      if ( $state['state'] == "Suspended" )
      {
        $owe_us = $plan_cost['plan'] - $customer->BALANCE;
        $message = \Ultra\Messaging\Templates\SMS_by_language(
          array(
            'message_type' => 'account_suspended',
            'date'         => $renewal_date,
            'value'        => $owe_us,
            'plan_name'    => $plan_cost['name']),
          $language ? $language : $preferred_language,
          $customer->BRAND_ID
        );
        $success = TRUE;
      }
      elseif ( $state['state'] == "Active" )
      {
        if ($get_data_usage)
        {
          $token = Session::encryptToken($msisdn, Session::TOKEN_V3, [Session::SYSTEM_TOKEN]);
          dlog('', '%s', $breakDown);
          if (isset($breakDown['WPRBLK39S']))
          {
            $message = \Ultra\Messaging\Templates\SMS_by_language(
              array(
                'message_type'   => in_array($customer->COS_ID, ['100003', '100007', '100004']) ? 'customerinfo_unlimited_data_3g' : 'customerinfo_data_3g',
                'remaining'      => $remaining,
                'usage'          => $usage,
                '3g_remaining'   => $customer->COS_ID == '100004' ? 'unlimited' : beautify_4g_lte_string($threeGBreakDown['remaining3G'], false),
                '3g_used'        => beautify_4g_lte_string($threeGBreakDown['used3G'], true),
                'login_token'    => $token,
                ),
              $language ? $language : $preferred_language,
              $customer->BRAND_ID
            );
          }
          else
          {
            $message = \Ultra\Messaging\Templates\SMS_by_language(
              array(
                'message_type'   => 'customerinfo_data',
                'remaining'      => $remaining,
                'usage'          => $usage,
                'login_token'    => $token,
                ),
              $language ? $language : $preferred_language,
              $customer->BRAND_ID
            );
          }
        }
        else // voice usage
        {
          $date      = '';
          $plan_name = '';

          // $checkBalance = new \Ultra\Lib\CheckBalance();
          // $cbResult = $checkBalance->byMSISDN($customer->current_mobile_number);

          \logDebug(sprintf('CheckBalance result: %s', json_encode($cbResult)));

          if ( ! $cbResult)
            throw new Exception('ERR_API_INTERNAL: error receiving network information');

          $roaming_balance = isset($cbResult['roaming_balance'])
            ? $cbResult['roaming_balance']
            : 0;

          if (\Ultra\Lib\Util\validateMintBrandId($customer->BRAND_ID))
          {
            $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);
            $plan_name         = $plan_cost['name'];
            $date              = date('M d Y', strtotime($multiMonthOverlay->CYCLE_EXPIRES));
          }
          else
          {
            $multi_month = multi_month_info($customer->CUSTOMER_ID);
            $plan_name   = $multi_month ? 'Multi-Month' : $plan_cost['name'];
            $date        = $multi_month ? date('M d Y', strtotime("{$multi_month['months_left']} months", strtotime($renewal_date))) : $renewal_date;
          }

          $time_remaining = ( $customer->PACKAGED_BALANCE1 / 60000 );
          $time_remaining = round($time_remaining,2); # 12.345678 => 12.34

          $customer_voice_inputs = [
            'wallet_balance'  => sprintf("%.2f",$customer->BALANCE),
            'value'           => $time_remaining,
            'stored_value'    => sprintf("%.2f",$customer->stored_value),
            'plan_name'       => $plan_name,
            'date'            => $date,
            'roaming_balance' => $roaming_balance / 100
          ];

          if (in_array($plan_short_name, ['L19','S29','L34','L44','S44']))
          {
            $customer_voice_inputs['message_type'] = 'customerinfo_voice_no_zero'; //FLEX
          }
          else
          {
            $customer_voice_inputs['message_type'] = 'customerinfo_voice';
            $customer_voice_inputs['zero_minutes'] = ( in_array( $plan_short_name, array('L19','L24','L34','L44') ) ) ? 0 : int_positive_or_zero( 1000 - $customer->PERIOD_MINUTES_TO_DATE_BILLED );
          }

          $message = \Ultra\Messaging\Templates\SMS_by_language(
            $customer_voice_inputs,
            $language ? $language : $preferred_language,
            $customer->BRAND_ID
          );
        }

        $success = TRUE;
      }
      else
        $errors[] = "ERR_API_INTERNAL: customer is not Active nor Suspended";
    }
    else
      $errors[] = "ERR_API_INTERNAL: Could not load current state for customer";

  }
  else
  {
    $message = "Ultra Mobile subscriber not found.";

    $preferred_language = '';
  }

  return flexi_encode(fill_return($p,
                                  "interactivecare__ShowCustomerInfo",
                                  func_get_args(),
                                  array("success"            => $success,
                                        "message"            => $message,
                                        "4g_lte_remaining"   => $remaining,
                                        "4g_lte_usage"       => $usage,
                                        "voice_minutes"      => $voice_minutes,
                                        "preferred_language" => $preferred_language,
                                        "short"              => $plan_short_name,
                                        "customer_id"        => ( $customer ? $customer->CUSTOMER_ID : '' ),
                                        "errors"             => $errors)));
}

?>
