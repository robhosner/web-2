<?php

// Return to the caller info about customer and his/her Voice Recharges.
// TODO : ParnerTag?

function interactivecare__CheckVoiceRechargeByMSISDN($msisdn)
{
  $retval = json_decode(interactivecare__doCheckVoiceRechargeByMSISDN($msisdn),TRUE);

  $clean_error=count($retval['errors']);

  if (count($retval['errors'])) {
    $clean_error = preg_replace ('/ERR_API_[^:]+: /', '', $retval['errors'][0]);
  }

  $retval['clean_error']=$clean_error;
  return json_encode($retval);
}


// Return to the caller info about customer and his/her Voice Recharges.
function interactivecare__doCheckVoiceRechargeByMSISDN($msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $planCount = 0;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "interactivecare__CheckVoiceRechargeByMSISDN",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "interactivecare__CheckVoiceRechargeByMSISDN", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "interactivecare__CheckVoiceRechargeByMSISDN",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  $days_plan_expires = '';
  $voice_recharge    = '';

  teldata_change_db();

  $customer = get_customer_from_msisdn($msisdn);

  if ( $customer )
  {
    if ( ! $customer->COS_ID )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer is invalid";
    else
    {
      $success = TRUE;

      $days_plan_expires = $customer->days_before_expire;

      // Format things so the JSON can be parsed by 3ci - from Kate
      $voice_recharge     = array ();

/*
// ``buy voice minutes`` functionality is being retired

      $plans             = get_voice_recharge_by_plan( get_plan_from_cos_id($customer->COS_ID) );

      foreach ($plans as $plan) {
        $planCount++;
        $key = sprintf ('plan%d', $planCount);
        $voice_recharge[$key] = array (
          'voice_minutes'  => $plan['voice_minutes'],
          'voice_soc_id'   => $plan['voice_soc_id'],
          'cost'           => sprintf("%01.2f",$plan['cost']/100)
        );
      }
*/
    }
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";

  return flexi_encode(fill_return($p,
                                  "interactivecare__CheckVoiceRechargeByMSISDN",
                                  func_get_args(),
                                  array("success"           => $success,
                                        "number_of_plans"       => $planCount, 

                                        "days_plan_expires" => $days_plan_expires,
                                        "voice_recharge"    => $voice_recharge,
					"wallet_balance"    => sprintf("%01.2f",$customer->BALANCE),
                                        "customer_id"       => $customer->customer_id,
                                        "errors"            => $errors)));
}

?>
