<?php

function interactivecare__CheckUpGlobe($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $apiResult = new \VersionOneResult($p, 'interactivecare__CheckUpGlobe');
  $apiResult->setArgs(func_get_args(), $mock);

  if ($apiResult->hasErrors())
    return $apiResult->getJSON();
  
  try
  {
    teldata_change_db();

    $customer = get_ultra_customer_from_customer_id($customer_id, [ 'current_mobile_number' ]);

    if ( !$customer || empty($customer->current_mobile_number) || strlen($customer->current_mobile_number) != 10)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: invalid customer id');

    $aniFormats = \ani_format_select_upglobe($customer->current_mobile_number);
    if (!is_array($aniFormats))
      throw new Exception('ERR_API_INTERNAL: Database error');

    $apiResult->addData('has_globe', count($aniFormats) > 0);

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    $apiResult->addError($error);
  }

  return $apiResult->getJSON();
}
