<?php

// Gives a list of SMS commands to seek information.
function interactivecare__GetCommandHelp($partner_tag, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $message = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "interactivecare__GetCommandHelp",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "interactivecare__GetCommandHelp", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "interactivecare__GetCommandHelp",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $success = TRUE;

  $customers = get_ultra_customers_from_msisdn($msisdn, array('preferred_language', 'BRAND_ID'));
  if ($customers && count($customers))
  {
    $customer = $customers[0];
    $message =
      \Ultra\Messaging\Templates\SMS_by_language(
        array(
        'message_type' => 'commandhelp_balance'
        ),
        $customer->preferred_language,
        $customer->BRAND_ID
      ).
      \Ultra\Messaging\Templates\SMS_by_language(
        array(
          'message_type' => 'commandhelp_store'
        ),
        $customer->preferred_language,
        $customer->BRAND_ID
      ).
      \Ultra\Messaging\Templates\SMS_by_language(
        array(
          'message_type' => 'commandhelp_rechargepin'
        ),
        $customer->preferred_language,
        $customer->BRAND_ID
      ).
      \Ultra\Messaging\Templates\SMS_by_language(
        array(
          'message_type' => 'commandhelp_roam'
        ),
        $customer->preferred_language,
        $customer->BRAND_ID
      ).
      \Ultra\Messaging\Templates\SMS_by_language(
        array(
          'message_type' => 'commandhelp_menu'
        ),
        $customer->preferred_language,
        $customer->BRAND_ID
      ).
      \Ultra\Messaging\Templates\SMS_by_language(
        array(
          'message_type' => 'commandhelp_611'
        ),
        $customer->preferred_language,
        $customer->BRAND_ID
      );
  }
  else
  {
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: customer does not exist';
  }

  return flexi_encode(fill_return($p,
                                  "interactivecare__GetCommandHelp",
                                  func_get_args(),
                                  array("success" => $success,
                                        "message" => $message,
                                        "errors"  => $errors)));
}

?>
