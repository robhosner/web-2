<?php


// Apply a pincard to the customer's account.
function interactivecare__ApplyPIN($partner_tag, $msisdn, $pin_number, $language)
{
  global $p;
  global $mock;
  global $always_succeed;

  $message = '';
  $success = FALSE;
  $warnings = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "interactivecare__ApplyPIN",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "interactivecare__ApplyPIN", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "interactivecare__ApplyPIN",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  if ( (strlen($msisdn) == 11) )
  {
    $msisdn = substr($msisdn, 1);
  }

  $customer = get_customer_from_msisdn($msisdn);

  if ( ! $language)
    $language = ( $customer && $customer->preferred_language ) ? $customer->preferred_language : 'EN' ;

  if ( $pin_number )
  {
    if ( $customer )
    {
      /* *** get current state *** */
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( $state )
      {

        if ( $state['state'] == "Cancelled" )
        {
          $message = \Ultra\Messaging\Templates\SMS_by_language(
            array('message_type'=>'apply_pin_cancelled'),
            $language,
            $customer->BRAND_ID
          );
        }
        else // 'state' not "Cancelled"
        { 
          // apply PIN
          $return = func_apply_pin_cards(
            array(
              "pin_list"  => array( $pin_number ),
              "customer"  => $customer,
              "source"    => 'PHONEPIN',
              "reference" => create_guid('selfcare'),
              "reason"    => __FUNCTION__));

          // check for errors
          if ( count($return['errors']) == 0 )
          {
            $success = TRUE;

            $customer = get_customer_from_msisdn($msisdn);

            if ( $state['state'] == "Suspended" )
            {
              $whole_value = $customer->BALANCE + $customer->stored_value;

              $plan_amount = get_plan_from_cos_id($customer->COS_ID);

              $plan_amount = substr($plan_amount, -2);

              if ( $whole_value >= $plan_amount )
              {
                /* *** try to reactivate the account *** */

                $context = array(
                  'customer_id' => $customer->CUSTOMER_ID
                );

                $result_status = reactivate_suspended_account($state,$customer,$context);

                if ( $result_status['success'] )
                {
                  $message = \Ultra\Messaging\Templates\SMS_by_language(
                    array(
                      'message_type' => 'apply_pin_credited_activation',
                      'value'        => $return['total_amount']
                    ),
                    $language,
                    $customer->BRAND_ID
                  );
                }
                else
                {
                  $success = FALSE;
                  $errors  = $result_status['errors'];
                }
              }
              else
              {
                $message = \Ultra\Messaging\Templates\SMS_by_language(
                  array(
                    'message_type' => 'apply_pin_credited_no_activation',
                    'value'        => $return['total_amount'],
                    'amount'       => ( $plan_amount - $whole_value )
                  ),
                  $language,
                  $customer->BRAND_ID
                );
              }
            }
            else
            {
              $message = \Ultra\Messaging\Templates\SMS_by_language(
                array(
                  'message_type' => 'apply_pin_recharged',
                  'value'        => $return['total_amount'],
                  'balance'      => $customer->BALANCE
                ),
                $language,
                $customer->BRAND_ID
              );
            }
          }
          else // errors from func_apply_pin_cards
          {
            if ($return['at_least_one_customer_used'])
            {
              $message = \Ultra\Messaging\Templates\SMS_by_language(
                array('message_type'=>'apply_pin_number_already_used'),
                $language,
                $customer->BRAND_ID
              );
            }
            else
            {
              $message = \Ultra\Messaging\Templates\SMS_by_language(
                array('message_type'=>'apply_pin_number_not_found'),
                $language,
                $customer->BRAND_ID
              );
            }

            $errors = $return['errors'];
            $warnings = $return['warnings'];
          }
        } // end of state != Cancelled
      }
      else
        $errors[] = "ERR_API_INTERNAL: Could not load current state for customer";
    }
    else // no customer
      $message = "Ultra Mobile subscriber not found.";
  }
  else // no PIN
  {
    $message = \Ultra\Messaging\Templates\SMS_by_language(
      array('message_type'=>'apply_pin_help'),
      $language,
      $customer->BRAND_ID
    );
  }

  return flexi_encode(fill_return($p,
                                  "interactivecare__ApplyPIN",
                                  func_get_args(),
                                  array("success" => $success,
                                        "message" => $message,
                                        "errors"  => $errors,
                                        "warnings" => $warnings)));
}

?>
