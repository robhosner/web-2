<?php

require_once 'classes/Flex.php';

// Gives the customer increased $ that can be used for anything; but will not be commissioned.
function customercare__AddCourtesyCash($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $amount)
{
  # $amount in cents

  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__AddCourtesyCash",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__AddCourtesyCash", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__AddCourtesyCash",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {

    $return = func_courtesy_add_balance(
      array(
        'reason'      => $reason,
        'terminal_id' => $agent_name,
        'reference'   => $customercare_event_id,
        'amount'      => ( $amount / 100 ),
        'customer'    => $customer,
        'detail'      => __FUNCTION__
      )
    );

    if ( count($return['errors']) == 0 )
    {
      $success = TRUE;

      // attempt to reactivate
      if (!\Ultra\Lib\Flex::isFlexPlan($customer->COS_ID)) {
        /* *** get current state *** */
        $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

        if ( $state )
        {
           $context = array(
             'customer_id' => $customer->CUSTOMER_ID
           );

           /* *** transition to 'active' if possible *** */
           dlog('',"customercare__AddCourtesyCash invoking reactivate_suspended_account");

           $result_status = reactivate_suspended_account($state,$customer,$context);

           if ( count($result_status['errors']) > 0 )
           {
             # non-fatal errors
             foreach($result_status['errors'] as $e) { dlog('',$e); }
           }

           dlog('',"customercare__AddCourtesyCash invoking activate_provisioned_account");

           $result_status = activate_provisioned_account($state,$customer,$context);

           if ( count($result_status['errors']) > 0 )
           {
             # non-fatal errors
             foreach($result_status['errors'] as $e) { dlog('',$e); }
           }
        }
        else
        {
          # non-fatal error
          dlog('',"We could not determine customer state in customercare__AddCourtesyCash");
        }
      }
    }
    else
    {
      $errors = $return['errors'];
    }

  }
  else
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";
  }

  return flexi_encode(fill_return($p,
                                  "customercare__AddCourtesyCash",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

?>
