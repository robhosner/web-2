<?php

// validate whether it is a valid Ultra phone number.
function customercare__ValidateMSISDN($partner_tag, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__ValidateMSISDN",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__ValidateMSISDN", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__ValidateMSISDN",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  # force length to 10
  if ( (strlen($msisdn) == 11) ) { $msisdn = substr($msisdn, 1); }

  $sql = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields'         => array('customer_id'),
      'current_mobile_number' => $msisdn
    )
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  $valid = ! ! ( is_array($query_result) && count($query_result) );

  $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "customercare__ValidateMSISDN",
                                  func_get_args(),
                                  array("success" => $success,
                                        "valid"   => $valid,
                                        "errors"  => $errors)));
}

?>
