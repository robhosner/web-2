<?php

include_once('db/call_session_log.php');

// Returns debugging information on International Direct Dial attempts
function customercare__ShowInternationalDirectDialErrors($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $history = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__ShowInternationalDirectDialErrors",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "history"  => $history,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__ShowInternationalDirectDialErrors", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__ShowInternationalDirectDialErrors",
                                    func_get_args(),
                                    array("success" => $success,
                                          "history" => $history,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  $customer = get_account_from_customer_id( $customer_id , array('account') );

  if ( $customer )
  {
    $success = TRUE;

    $call_session_log_query = call_session_log_query(
      array(
        'select_fields' => array('DATE_TIME',
                                 'CALL_SESSION_CODE',
                                 'DESCRIPTION as ERROR',
                                 'dbo.UTC_TO_PT(DATE_TIME) as DATE_TIME_PST',
                                 'DETAIL as DIALED_NUMBER'),
        'account'       => $customer->account,
        'sql_limit'     => 50,
        'days_ago'      => 30
      )
    );

    $call_session_log_data = mssql_fetch_all_objects(logged_mssql_query($call_session_log_query));

    if ( $call_session_log_data && is_array($call_session_log_data) && count($call_session_log_data) )
    {
      foreach($call_session_log_data as $id => $data)
      {
        $history[] = implode(";", array( $data->DATE_TIME , $data->CALL_SESSION_CODE , $data->ERROR , $data->DATE_TIME_PST , $data->DIALED_NUMBER ) );
      }
    }
  }
  else
  { $errors[] = "ERR_API_INTERNAL: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__ShowInternationalDirectDialErrors",
                                  func_get_args(),
                                  array("success" => $success,
                                        "history" => $history,
                                        "errors"  => $errors)));
}

/*
traffic has a column on it called direction
direction = 0 is the inbound call attempt
direction = 1 is the outbound connection
one inbound attempt can have many records in traffic
because its running through carriers trying to find a connection
when it connects it will create one row with direction = 1 signaling it has connected the call
call_session_id will link an inbound and outbound attempt together
*/

?>
