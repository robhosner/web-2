<?php

// Send an SMS to the phone number of the customer.
function customercare__CustomerDirectSMS($partner_tag, $customer_id, $message, $agent)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__CustomerDirectSMS",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__CustomerDirectSMS", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__CustomerDirectSMS",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db();

  $customer = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID'));

  if ( $customer )
  {
    // add a SMS_INTERNAL to HTT_MESSAGING_QUEUE to be sent ASAP

    $messaging_queue_params = array();

    $message = 'From Ultra Customer Svc: '.$message;

    if ( strlen($message) > 100 )
    {
      $chunks = str_split($message, 100);

      $messaging_queue_params['message1'] = $chunks[0];
      $messaging_queue_params['message2'] = $chunks[1];
    }
    else
    {
      $messaging_queue_params['message'] = $message;
    }

    $params = array(
      'customer_id'                 => $customer_id,
      'reason'                      => 'customercare__CustomerDirectSMS',
      'expected_delivery_datetime'  => 'getutcdate()',
      'next_attempt_datetime'       => 'getutcdate()',
      'type'                        => 'SMS_INTERNAL',
      'messaging_queue_params'      => $messaging_queue_params
    );

    $result = htt_messaging_queue_add( $params );

    $errors  = $result->get_errors();
    $success = ! ! $result->is_success();
  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__CustomerDirectSMS",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

?>
