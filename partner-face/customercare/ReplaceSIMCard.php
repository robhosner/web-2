<?php

// Only available via CS - replaces SIM card for another.
function customercare__ReplaceSIMCard($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $old_ICCID, $new_ICCID)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $customer = NULL;
  $success  = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__ReplaceSIMCard",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  teldata_change_db(); // connect to the DB

  $errors = validate_params($p, "customercare__ReplaceSIMCard", func_get_args(), $mock);

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** verify that old_ICCID and new_ICCID are valid *** */

    if ( ! validate_ICCID($old_ICCID,0) )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: the given old_ICCID is invalid (1)";             }

    $newSim = get_htt_inventory_sim_from_iccid($new_ICCID);
    if ( ! $newSim || ! validate_ICCID($newSim,1) )
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given new_ICCID is invalid or already used'; }

    // block replacement ORANGE sim
    if (strtoupper($newSim->PRODUCT_TYPE) == 'ORANGE')
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot replace with Orange SIM'; }
  
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** do not allow SIM replacement during MISO tasks *** */

    if ( \Ultra\Lib\MVNE\exists_open_task( $customer_id ) )
      $errors[] = "ERR_API_INTERNAL: The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.";
  }

  $old_mvne = '2';
  $new_mvne = '2';

  // verify new ICCID
  if ( ! ($errors && count($errors) > 0) )
  {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $mwControl->mwCanActivate(
        array(
          'actionUUID' => getNewActionUUID('customercare ' . time()),
          'iccid'      => $new_ICCID
        )
      );

      if ($result->is_failure() || ! isset($result->data_array['available']) || $result->data_array['available'] != 'true')
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot activate this ICCID (2)';
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** verify that old_ICCID belongs to $customer_id in HTT_INVENTORY_SIM *** */

    $htt_inventory_sim_select_query = htt_inventory_sim_select_query(
      array(
        "iccid_number" => $old_ICCID
      )
    );

    $htt_inventory_sim_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

    if (
      ( ! $htt_inventory_sim_result )
      ||
      ( ! is_array($htt_inventory_sim_result) )
      ||
      ( count($htt_inventory_sim_result) < 1 )
      ||
      ( $htt_inventory_sim_result[0]->CUSTOMER_ID != $customer_id )
    )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: the given old_ICCID is invalid (2)"; }

  }

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** get customer data *** */

    $customer = get_customer_from_customer_id($customer_id);

    if ( ! $customer )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** check against HTT_CUSTOMERS_OVERLAY_ULTRA.CURRENT_ICCID_FULL *** */

    if ( $customer->CURRENT_ICCID_FULL != luhnenize($old_ICCID) )
    {
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the given old_ICCID does not belong to customer_id $customer_id (3)";
    }
    else
    {
      if ( ! isSameBrandByICCID($old_ICCID, $new_ICCID))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given new_ICCID is of an INVALID BRAND type';
    }
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    // PROD-511: prohibit modifications to in-Active subscribers
    if ($customer->plan_state !== 'Active')
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: Subscriber must be active to change SIM or phone number.';
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__ReplaceSIMCard",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  $warnings = array();
  $result   = NULL;

  // replace ICCID
    $result = $mwControl->mwChangeSIM(
      array(
        'actionUUID'         => getNewActionUUID('customercare ' . time()),
        'msisdn'             => normalize_msisdn($customer->current_mobile_number, TRUE),
        'old_iccid'          => $old_ICCID,
        'new_iccid'          => $new_ICCID,
        'customer_id'        => $customer->CUSTOMER_ID
      )
    );

    dlog('',"mwChangeSIM result data = %s",$result->data_array);

    // the MW call failed
    if ( ! $result->is_success() )
    {
      $errors = $result->get_errors();

      dlog('',"mwChangeSIM result errors = %s",$errors);
    }
    // the MW ChangeSIM command failed
    elseif ( ! $result->data_array['success'] )
    {
      $errors = $result->data_array['errors'];

      dlog('',"mwChangeSIM result data errors = %s",$errors);
    }

    teldata_change_db(); // done with MVNE2 calls

  if ( ! count($errors) )
    // side effects: HTT_CUSTOMERS_OVERLAY_ULTRA and HTT_INVENTORY_SIM
    $errors = callbackChangeSIM( $new_ICCID , $customer_id , $agent_name );

  if ( ! count($errors) )
    {
      /* *** store $reason, $agent_name, $customercare_event_id in HTT_BILLING_HISTORY *** */

      $description = 'Replace SIM';
      $entry_type  = 'SIM_REPLACEMENT';

      $history_params = array(
        "customer_id"            => $customer_id,
        "date"                   => 'now',
        "cos_id"                 => $customer->COS_ID,
        "entry_type"             => $entry_type,
        "stored_value_change"    => 0,
        "balance_change"         => 0,
        "package_balance_change" => 0,
        "charge_amount"          => 0,
        "reference"              => $customercare_event_id,
        "reference_source"       => 'CS',
        "detail"                 => 'customercare__ReplaceSIMCard',
        "description"            => $description,
        "result"                 => 'COMPLETE',
        "source"                 => $entry_type,
        "is_commissionable"      => 0,
        "terminal_id"            => $agent_name
      );

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      {
        $success = TRUE;

        // purge cache
        $cache = new \Ultra\Lib\DB\Cache();
        $cache->purge('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', $customer_id);
      }
      else # htt_billing_history insert error
        $errors[] = "ERR_API_INTERNAL: Database write error (2)";
    }

  return flexi_encode(fill_return($p,
                                  "customercare__ReplaceSIMCard",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

