<?php

include_once('lib/state_machine/functions.php');

// Cancels a Port Request. Then does an API QueryPortIn to send the customer to Port-In Denied.
function customercare__AspiderCancelPortIn($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $warnings = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__AspiderCancelPortIn",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__AspiderCancelPortIn", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__AspiderCancelPortIn",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db();

  $customer = get_customer_from_customer_id($customer_id);

  if ($customer)
  {
    // check if intra-brand port, cannot cancel
    $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);
    if ($sim && $customer->BRAND_ID != $sim->BRAND_ID)
      $errors[] = 'The porting attempt cannot be cancelled';
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id $customer_id not found.";

  if ( ! count($errors) )
  {
    $state = internal_func_get_state_from_customer_id($customer_id);

    if ( ! in_array( $state['state'] , array('Port-In Denied','Port-In Requested') ) )
      return flexi_encode(fill_return(
        $p,
        __FUNCTION__,
        func_get_args(),
        array(
          'success' => FALSE,
          'errors'  => array('ERR_API_INVALID_ARGUMENTS: customer must be in Port-In Denied state'))));

    $result = NULL;

    if ( ! $customer->current_mobile_number )
      return flexi_encode(fill_return($p,
                                  "customercare__AspiderCancelPortIn",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => array('The customer has no MSISDN'))));

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwCancelPortIn(
      array(
        'actionUUID'         => getNewActionUUID('customercare ' . time()),
        'msisdn'             => $customer->current_mobile_number
      )
    );

    dlog('',"mwCancelPortIn result = %s",$result);

    $warnings = $result->get_warnings();

    teldata_change_db();

    if ( isset($result->data_array['errors']) && is_array($result->data_array['errors']) && count($result->data_array['errors']) )
      $result->add_errors( $result->data_array['errors'] );

    if ( $result->is_success() )
    {
      // transition the customer to 'Cancelled'

      $context = array(
        'customer_id' => $customer->customer_id
      );

      $result_status = cancel_account($customer,$context,NULL,'customercare__AspiderCancelPortIn','AspiderCancelPortIn called','customercare API');

      $errors = $result_status['errors'];

      if ( ! count($errors) )
        $success = TRUE;
    }
    else
    {
      $errors = $result->get_errors();

      if ( isset($result->data_array['result']) &&
           ( $result->data_array['result'] == 'ERROR' ) &&
           isset($result->data_array['message']) &&
           $result->data_array['message']
      )
        $errors[] = $result->data_array['message'];
    }
  }

  return flexi_encode(fill_return($p,
                                  "customercare__AspiderCancelPortIn",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

?>
