<?php

function customercare__mvneQueryPortIn($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $warnings = array();
  $provision_status_date = '';
  $query_status  = '';
  $query_message = '';
  $status   = '';
  $r_code   = '';
  $carrier  = '';

  $errors[] = "ERR_API_INTERNAL: This API is deprecated";

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    __FUNCTION__,
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, __FUNCTION__, func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  __FUNCTION__,
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db();

  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields'    => array('plan_state', 'CURRENT_ICCID_FULL', 'BRAND_ID', 'current_mobile_number','MVNE'),
      'customer_id'      => $customer_id
    )
  );

  $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

  if ( $ultra_customer_data && is_array($ultra_customer_data) && count($ultra_customer_data) )
  {
    $customer = $ultra_customer_data[0];
    $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);
    if ($sim && $customer->BRAND_ID != $sim->BRAND_ID)
    {
      \logit("intra brand port");

      $query_status = ($customer->plan_state != STATE_ACTIVE) ? 'ERROR' : 'COMPLETE';
      $status       = $query_status;
      $provision_status_date = time();
      $carrier = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
    }
    else
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwQueryStatus(
        array(
          'actionUUID'         => getNewActionUUID('customercare ' . time()),
          'msisdn'             => $customer->current_mobile_number
        )
      );

      if ( $result->is_success() )
      {
        if ( isset($result->data_array['errors']) && is_array($result->data_array['errors']) && count($result->data_array['errors']) )
          $errors = $result->data_array['errors'];
        elseif( isset( $result->data_array['body'] ) && is_object($result->data_array['body']) )
        {
          $portInQueue = new \PortInQueue();

          $result = $portInQueue->loadByMsisdn( $customer->current_mobile_number );

          if ( $result->is_success() )
          {
            $error_code = ( property_exists( $portInQueue , 'provstatus_error_code' ) ) ? $portInQueue->provstatus_error_code : '';
            $error_msg  = ( property_exists( $portInQueue , 'provstatus_error_msg'  ) ) ? $portInQueue->provstatus_error_msg  : '';

            $r_code  = ( $error_code && $error_msg ) ? $error_code . '-' . $error_msg : '' ;

            $status  = $portInQueue->provstatus_description;

            $query_status      = ( property_exists( $portInQueue , 'querystatus_portstatus' ) ) ? $portInQueue->querystatus_portstatus : '';
            $query_message     = ( property_exists( $portInQueue , 'querystatus_errormsg'   ) ) ? $portInQueue->querystatus_errormsg   : '';

            $provision_status_date = $portInQueue->provstatus_epoch;

            $carrier = ( property_exists( $portInQueue , 'carrier_name' ) ) ? $portInQueue->carrier_name : '';
          }
          else
            $errors  = $result->get_errors();
        }
      }
      else
        $errors = $result->get_errors();
    }
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id $customer_id not found.";

  $success = !count($errors);

  return flexi_encode(fill_return($p,
                                  __FUNCTION__,
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "provision_status_date" => $provision_status_date,
                                        "query_status"          => $query_status,
                                        "query_message"         => $query_message,
                                        "status"   => $status,
                                        "r_code"   => $r_code,
                                        "carrier"  => $carrier,
                                        "errors"   => $errors)));
}

?>
