<?php

require_once 'classes/VersionOneResult.php';
require_once 'classes/VersionOneUtil.php';

class Util__customercare__ChangePhoneNumber extends VersionOneUtil
{
  public function __construct() {}
}

// Only avaialble via CS - replaces phone number for another.
function customercare__ChangePhoneNumber(
  $partner_tag,
  $reason,
  $agent_name,
  $customercare_event_id,
  $customer_id,
  $old_phone_number,
  $zipcode
) {
  global $p;
  global $mock;
  global $always_succeed;

  $apiResult = new \VersionOneResult($p, 'customercare__ChangePhoneNumber');
  $apiResult->setArgs(func_get_args(), $mock);

  if ($apiResult->hasErrors())
    return $apiResult->getJSON();

  $util = 'Util__' . __FUNCTION__;
  if ( ! class_exists($util))
    throw new Exception('ERR_API_INTERNAL: missing utility class');
  $util = new $util();

  try
  {
    teldata_change_db();

    // get and validate customer
    $customer = get_ultra_customer_from_customer_id($customer_id, [
      'current_mobile_number',
      'CURRENT_ICCID_FULL',
      'plan_state'
    ]);

    if ( ! $customer)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: customer not found');

    // validate old_phone_number
    if ( $customer->current_mobile_number != $old_phone_number )
      throw new Exception('ERR_API_INVALID_ARGUMENTS: data mismatch: wrong customer id or phone number.');
    
    // validate plan_state active
    if ( $customer->plan_state !== STATE_ACTIVE )
      throw new Exception('ERR_API_INVALID_ARGUMENTS: Subscriber must be active to change SIM or phone number.');

    // get and validate account
    $account = get_account_from_customer_id($customer_id, ['ACCOUNT_ID', 'COS_ID']);

    if ( ! $account)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: account not found');

    // update customer zip code
    $result = \Ultra\Lib\DB\Setter\Customer\cc_info([
      'customer_id'     => $customer_id,
      'postal_code'     => $zipcode
    ]);

    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();
      throw new Exception('ERR_API_INTERNAL: ' . $errors[0]);
    }

    // fail if customer has open transition(s)
    $actions_and_transitions = get_actions_and_transitions([
      'customer_id' => $customer_id,
      'status'      => 'OPEN'
    ]);

    if ( $actions_and_transitions
      && is_array($actions_and_transitions)
      && count($actions_and_transitions)
    ) throw new Exception('ERR_API_INVALID_ARGUMENTS: there is already an operation in progress for the given MSISDN.');

    // fail if customer has an open miso
    if ( \Ultra\Lib\MVNE\exists_open_task( $customer_id ) )
      throw new Exception('ERR_API_INTERNAL: The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.');

    // null previous result
    $result = NULL;

    // run ACC command ChangeMSISDN
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwChangeMSISDN([
      'actionUUID'  => getNewActionUUID('customercare ' . time()),
      'msisdn'      => $customer->current_mobile_number,
      'iccid'       => $customer->CURRENT_ICCID_FULL,
      'zipcode'     => $zipcode,
      'customer_id' => $customer_id
    ]);

    dlog('', "result = %s", $result);

    if ( isset($result->data_array['errors'])
      && $result->data_array['errors']
      && is_array($result->data_array['errors'])
      && count($result->data_array['errors'])
    ) {
      $errors = $result->data_array['errors'];
      throw new Exception( $errors[0] );
    }

    //
    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();

      if ( count($errors) )
        throw new Exception( $errors[0] );
      else
        throw new Exception('ERR_API_INTERNAL: middleware/MVNE call failed');
    }

    // check if ACC call return new MSISDN
    if ( ! isset($result->data_array['msisdn']) )
      throw new Exception('ERR_API_INTERNAL: middleware/MVNE call did not return a MSISDN');

    $new_msisdn = normalize_msisdn_10( $result->data_array['msisdn'] );

    dlog('', "mwChangeMSISDN success : new msisdn = %s", $new_msisdn);

    // update CURRENT_MOBILE_NUMBER in DB table HTT_CUSTOMERS_OVERLAY_ULTRA
    $check = run_sql_and_check(
      htt_customers_overlay_ultra_update_query([
        'customer_id'           => $customer_id,
        'current_mobile_number' => $new_msisdn
      ])
    );

    if ( ! $check )
      throw new Exception('ERR_API_INTERNAL: Customer DB write error');

    // add new msisdn to HTT_ULTRA_MSISDN
    $check = add_to_htt_ultra_msisdn( $customer_id , $new_msisdn , 1 , 'customercare__ChangePhoneNumber' , '2' );

    if ( ! $check )
      throw new Exception('ERR_API_INTERNAL: new MSISDN DB write error');

    // prepare query parameters
    $params = [
      'msisdn_activated'   => 0,
      'expires_date'       => 'GETUTCDATE()',
      'replaced_by_msisdn' => $new_msisdn,
      'customer_id'        => $customer_id,
      '! msisdn'           => $old_phone_number
    ];

    // create and execute query
    $check = run_sql_and_check(htt_ultra_msisdn_update_query_by_customer_id($params));
    if ( ! $check )
      throw new Exception('ERR_API_INTERNAL: old MSISDN DB write error');

    // update ACCOUNT_ALIASES
    $account_aliases_update_query = account_aliases_update_query([
      'msisdn'     => $new_msisdn,
      'account_id' => $account->ACCOUNT_ID
    ]);

    if ( ! run_sql_and_check($account_aliases_update_query) )
      throw new Exception('ERR_API_INTERNAL: account_aliases DB write error');

    // update DESTINATION_ORIGIN_MAP

    $destination_origin_map_update_query1 = destination_origin_map_update_query([
      'destination_from' => '1' . $old_phone_number, # old msisdn
      'destination_to'   => '1' . $new_msisdn        # new msisdn
    ]);

    $destination_origin_map_update_query2 = destination_origin_map_update_query([
      'destination_from' => $old_phone_number,             # old msisdn
      'destination_to'   => $new_msisdn                    # new msisdn
    ]);

    if ( ! run_sql_and_check($destination_origin_map_update_query1) )
      throw new Exception('ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP DB write error');

    if ( ! run_sql_and_check($destination_origin_map_update_query2) )
      throw new Exception('ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP DB write error');

    // check for ANI_FORMAT rows for old msisdn
    $ani_format_rows = ani_format_select_upglobe($old_phone_number);
    if ($ani_format_rows && count($ani_format_rows))
    {
      // remove ANI_FORMAT rows for old msisdn
      $check = ani_format_remove_upglobe($old_phone_number);
      if ( ! $check)
        throw new Exception('ERR_API_INTERNAL: error removing old msisdn from ANI_FORMAT');

      // add ANI_FORMAT rows for new msisdn
      $aniPrefixArr = [
               $new_msisdn,
        '1'  . $new_msisdn,
        '+1' . $new_msisdn
      ];
      foreach ($aniPrefixArr as $aniPrefix)
      {
        $result = ani_format_insert([
          'ani_prefix'  => $aniPrefix,
          'origin'      => '6309',
          'description' => 'UpGlobe'
        ]);

        if ( ! $result)
          throw new Exception('ERR_API_INTERNAL: error inserting row for ANI_FORMAT : ' . $aniPrefix);
      }
    }

    /* *** store $reason, $agent_name, $customercare_event_id in HTT_BILLING_HISTORY *** */
    $history_params = [
      'customer_id'             => $customer_id,
      'date'                    => 'now',
      'cos_id'                  => $account->COS_ID,
      'entry_type'              => 'MSISDN_REPLACEMENT',
      'stored_value_change'     => 0,
      'balance_change'          => 0,
      'package_balance_change'  => 0,
      'charge_amount'           => 0,
      'reference'               => $customercare_event_id,
      'reference_source'        => 'CS',
      'detail'                  => 'customercare__ChangePhoneNumber',
      'description'             => 'Replace MSISDN',
      'result'                  => 'COMPLETE',
      'source'                  => 'MSISDN_REPLACEMENT',
      'is_commissionable'       => 0,
      'terminal_id'             => $agent_name
    ];

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      throw new Exception('ERR_API_INTERNAL: Database write error');

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    $apiResult->addError($error);
  }

  return $apiResult->getJSON();
}