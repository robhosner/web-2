<?php


// Removes the Courtesy Cash given too eagerly by a rep.
function customercare__VoidCourtesyCash($partner_tag, $reason, $agent_name, $transaction_id, $customer_id, $amount)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__VoidCourtesyCash",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__VoidCourtesyCash", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__VoidCourtesyCash",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {

    // verify that the customer has sufficient balance to remove the amount
    if ( ( $customer->BALANCE * 100 ) >= $amount )
    {
      $result = func_courtesy_void_add_balance(
        array(
          'customer'       => $customer,
          'amount'         => ($amount/100),
          'reason'         => $reason,
          'terminal_id'    => $agent_name,
          'transaction_id' => $transaction_id
          #'reference'      => ''
        )
      );

      $success = $result['success'];
      $errors  = $result['errors'];
    }
    else
    {
      $errors[] = "ERR_API_INTERNAL: amount exceeds customer balance.";
    }
  }
  else
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found.";
  }

  return flexi_encode(fill_return($p,
                                  "customercare__VoidCourtesyCash",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}


?>
