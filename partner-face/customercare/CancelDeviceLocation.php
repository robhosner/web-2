<?php

include_once('lib/messaging/functions.php');
include_once('lib/state_machine/functions.php');
include_once('lib/provisioning/functions.php');
require_once 'lib/inventory/functions.php';

// Use msisdn and iccid_full and do a mwCancelDeviceLocation
function customercare__CancelDeviceLocation($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $warnings = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__CancelDeviceLocation",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "errors"   => array(),
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__CancelDeviceLocation", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__CancelDeviceLocation",
                                    func_get_args(),
                                    array("success"  => $success,
                                          "warnings" => $warnings,
                                          "errors"   => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = get_ultra_customer_from_customer_id( $customer_id , array("customer_id","current_mobile_number","mvne","current_iccid_full") );

  if ( $customer )
  {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwCancelDeviceLocation(
        array(
          'actionUUID'         => getNewActionUUID('internal ' . time()),
          'msisdn'             => $customer->current_mobile_number,
          'iccid'              => $customer->current_iccid_full
        )
      );

      dlog('',"mwCancelDeviceLocation result = %s",$result);

      if ( $result->has_errors() )
        $errors = $result->get_errors();
      else
      {
        if ( isset( $result->data_array['errors'] ) && is_array($result->data_array['errors']) && count( $result->data_array['errors'] ) )
          $errors = $result->data_array['errors'];
        elseif ( isset( $result->data_array['errors'] ) && !is_array($result->data_array['errors']) )
          $errors[] = $result->data_array['errors'];
      }

      $success = ! count($errors);
  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__CancelDeviceLocation",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

