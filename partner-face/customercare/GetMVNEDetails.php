<?php

/*
 * customercare__GetMVNEDetails
 *
 * capture all non-porting details associated with a particular network element and return them for debugging
 *
 * @param string MSISDN
 * @param string ICCID
 * @author: VYT 2014-03-05
 */
function customercare__GetMVNEDetails($partner_tag, $ICCID, $MSISDN, $action_uuid)
{
  global $p;
  global $mock;
  global $always_succeed;

  // init
  $success = FALSE;
  $warnings = array();
  $output = array();

  // always succeed
  if ($always_succeed)
    return flexi_encode(fill_return($p, __FUNCTION__, func_get_args(),
      array('success'  => TRUE, 'warnings' => array('ERR_API_INTERNAL: always_succeed'))));

  // validate parameters
  $errors = validate_params($p, __FUNCTION__, func_get_args(), $mock);
  if (! count($errors) && empty($ICCID) && empty($MSISDN))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: please provide ICCID or MSISDN';

  if (is_null($action_uuid))
  {
    global $request_id;
    $action_uuid = $request_id;
  }

  // call MW
  if (! count($errors))
  {
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
    $result = $mwControl->mwGetMVNEDetails(
      array(
        'actionUUID' => $action_uuid,
        'msisdn'     => $MSISDN,
        'iccid'      => $ICCID));
  }

  // did mwGetMVNEDetails fail?
  if (! count($errors) && $result->is_failure())
  {
    $err = $result->get_errors();
    $errors[] = 'ERR_API_INTERNAL: ' . $err[0];
  }

  // did GetMVNEDetails fail?
  if (! count($errors) && ! $result->data_array['success'])
    $errors[] = 'ERR_API_INTERNAL: ' . $result->data_array['errors'][0];

  // prepare output
  if (! count($errors))
  {
    $output['QuerySubscriber_wholesalePlan'] = empty($result->data_array['QuerySubscriber']->wholesalePlan) ? NULL : $result->data_array['QuerySubscriber']->wholesalePlan;
    $output['QuerySubscriber_IMSI'] = empty($result->data_array['QuerySubscriber']->IMSI) ? NULL : $result->data_array['QuerySubscriber']->IMSI;
    $output['QuerySubscriber_IMEI'] = empty($result->data_array['QuerySubscriber']->IMEI) ? NULL : $result->data_array['QuerySubscriber']->IMEI;
    $output['QuerySubscriber_AddOnFeatureInfoList'] = empty($result->data_array['QuerySubscriber']->AddOnFeatureInfoList) ? NULL : $result->data_array['QuerySubscriber']->AddOnFeatureInfoList;
    $output['QuerySubscriber_PlanEffectiveDate'] = empty($result->data_array['QuerySubscriber']->PlanEffectiveDate) ? NULL : $result->data_array['QuerySubscriber']->PlanEffectiveDate;
    $output['QuerySubscriber_MSISDN'] = empty($result->data_array['QuerySubscriber']->MSISDN) ? NULL : $result->data_array['QuerySubscriber']->MSISDN;
    $output['QuerySubscriber_AutoRenewalIndicator'] = empty($result->data_array['QuerySubscriber']->AutoRenewalIndicator) ? NULL : $result->data_array['QuerySubscriber']->AutoRenewalIndicator;
    $output['QuerySubscriber_SIM'] = empty($result->data_array['QuerySubscriber']->SIM) ? NULL : $result->data_array['QuerySubscriber']->SIM;
    $output['QuerySubscriber_PlanId'] = empty($result->data_array['QuerySubscriber']->PlanId) ? NULL : $result->data_array['QuerySubscriber']->PlanId;
    $output['QuerySubscriber_SubscriberStatus'] = empty($result->data_array['QuerySubscriber']->SubscriberStatus) ? NULL : $result->data_array['QuerySubscriber']->SubscriberStatus;
    $output['QuerySubscriber_PlanExpirationDate'] = empty($result->data_array['QuerySubscriber']->PlanExpirationDate) ? NULL : $result->data_array['QuerySubscriber']->PlanExpirationDate;

    $output['GetNetworkDetails_MSStatus'] = empty($result->data_array['GetNetworkDetails']->MSStatus) ? NULL : $result->data_array['GetNetworkDetails']->MSStatus;
    $output['GetNetworkDetails_NAPStatus'] = empty($result->data_array['GetNetworkDetails']->NAPStatus) ? NULL : $result->data_array['GetNetworkDetails']->NAPStatus;
    $output['GetNetworkDetails_SIMStatus'] = empty($result->data_array['GetNetworkDetails']->SIMStatus) ? NULL : $result->data_array['GetNetworkDetails']->SIMStatus;
    $output['GetNetworkDetails_INStatus'] = empty($result->data_array['GetNetworkDetails']->INStatus) ? NULL : $result->data_array['GetNetworkDetails']->INStatus;
    $output['GetNetworkDetails_SIM'] = empty($result->data_array['GetNetworkDetails']->SIM) ? NULL : $result->data_array['GetNetworkDetails']->SIM;
    $output['GetNetworkDetails_HLRFeatureList'] = empty($result->data_array['GetNetworkDetails']->HLRFeatureList) ? NULL : $result->data_array['GetNetworkDetails']->HLRFeatureList;
    $output['GetNetworkDetails_MSISDN'] = empty($result->data_array['GetNetworkDetails']->MSISDN) ? NULL : $result->data_array['GetNetworkDetails']->MSISDN;
    $output['GetNetworkDetails_NAPFeatureList'] = empty($result->data_array['GetNetworkDetails']->NAPFeatureList) ? NULL : $result->data_array['GetNetworkDetails']->NAPFeatureList;

    if ( ! empty($result->data_array['GetNetworkDetails']->APNList))
      $output['GetNetworkDetails_APNList'] = $result->data_array['GetNetworkDetails']->APNList;

    if ( ! empty($result->data_array['GetNetworkDetails']->IMEI_RAW))
      $output['GetNetworkDetails_IMEI_RAW'] = $result->data_array['GetNetworkDetails']->IMEI_RAW;

    if ( ! empty($result->data_array['GetNetworkDetails']->IMEI))
      $output['GetNetworkDetails_IMEI'] = $result->data_array['GetNetworkDetails']->IMEI;

    if ( ! empty($result->data_array['GetNetworkDetails']->IMSI))
      $output['GetNetworkDetails_IMSI'] = $result->data_array['GetNetworkDetails']->IMSI;

    $output['CheckBalance_MSISDN'] = empty($result->data_array['CheckBalance']->MSISDN) ? NULL : $result->data_array['CheckBalance']->MSISDN;
    $output['CheckBalance_BalanceValueList'] = empty($result->data_array['CheckBalance']->BalanceValueList) ? NULL : $result->data_array['CheckBalance']->BalanceValueList;

    // API-40
    // Check if plan effective date is after new Data SOCs release date
    if (strtotime($output['QuerySubscriber_PlanEffectiveDate']) > strtotime(\Ultra\UltraConfig\getEmuSwitchDate()))
    {
      if ( ! empty($output['QuerySubscriber_AddOnFeatureInfoList']))
        $output['QuerySubscriber_AddOnFeatureInfoList']->AddOnFeatureInfo = \Ultra\MvneConfig\parseEmuSwitchAddOnFeatureInfoList(
          $output['QuerySubscriber_AddOnFeatureInfoList']->AddOnFeatureInfo
        );

      if ( ! empty($output['CheckBalance_BalanceValueList']))
      {
        $output['CheckBalance_BalanceValueList']->BalanceValue = \Ultra\MvneConfig\parseEmuSwitchBalanceValueList(
          $output['CheckBalance_BalanceValueList']->BalanceValue,
          'customercare'
        );
      }
    }
  }

  // prepare SELECT query for possibly multiple customers
  if (! count($errors))
  {
    $params = array('select_fields' => array('customer_id'));
    if ($ICCID)
      $params['iccid'] = $ICCID;
    if ($MSISDN)
      $params['current_mobile_number'] = $MSISDN;

    // get customer data and check for possible inconsistency
    teldata_change_db();
    $customers = get_ultra_customers_from_multiple($params);
    if (count($customers) == 1)
      $output['customer_id'] = $customers[0]->customer_id;
    elseif (count($customers) > 1)
      dlog('', "ERROR ESCALATION ALERT DAILY $MSISDN $ICCID DB inconsistency - " .
        $customers[0]->customer_id . ' : ' . $customers[1]->customer_id);
    
    $success = TRUE;
  }

  // all done
  $result = array_merge(array('success' => $success, 'warnings' => $warnings, 'errors' => $errors), $output);
  return flexi_encode(fill_return($p, __FUNCTION__, func_get_args(), $result));
}

?>
