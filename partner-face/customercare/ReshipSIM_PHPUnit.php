<?php

require_once 'classes/PHPUnitBase.php';

class ReshipSIMTest extends PHPUnitBase
{
  public function test__customercare__ReshipSIM()
  {
    // API setup
    list($test, $partner, $api) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'version'   => 1,
      'partner'   => $partner));
/*
    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse($result->success);
    $this->assertNotEmpty($result->errors);

    // invalid customer_id
    $params = array(
      'customer_id'           => 99999,
      'partner_rag'           => 'VYT_TEST_16-01-15-01',
      'reason'                => 'replacement',
      'agent_name'            => 'vtarasov',
      'customercare_event_id' => rand(1000, 9999));
    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse($result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: customer not found', $result->errors);

    // shipping address is missing
    $params = array(
      'customer_id'           => 19852,
      'partner_rag'           => 'VYT_TEST_16-01-15-02',
      'reason'                => 'replacement',
      'agent_name'            => 'vtarasov',
      'customercare_event_id' => rand(1000, 9999));
    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse($result->success);
    $this->assertContains('ERR_API_INTERNAL: Shipping address is missing.', $result->errors);

    // present but invalid address: we allow the shipment regardless (customer is always right)
    $params = array(
      'customer_id'           => 3394,
      'partner_rag'           => 'VYT_TEST_16-01-15-03',
      'reason'                => 'replacement',
      'agent_name'            => 'vtarasov',
      'customercare_event_id' => rand(1000, 9999));
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
*/
    // valid address: Ultra
    $params = array(
      'customer_id'           => 2559,
      'partner_rag'           => 'VYT_TEST_16-01-15-04',
      'reason'                => 'replacement',
      'agent_name'            => 'vtarasov',
      'customercare_event_id' => rand(1000, 9999));
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
/*
    // partial address: Univision
    $params = array(
      'customer_id'           => 19493,
      'partner_rag'           => 'VYT_TEST_16-01-15-05',
      'reason'                => 'replacement',
      'agent_name'            => 'vtarasov',
      'customercare_event_id' => rand(1000, 9999));
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
*/
  }
}
