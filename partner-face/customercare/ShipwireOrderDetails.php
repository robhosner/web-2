<?php

// Check the status of a Shipwire order
function customercare__ShipwireOrderDetails($partner_tag, $order_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__ShipwireOrderDetails",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__ShipwireOrderDetails", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__ShipwireOrderDetails",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  $output_params = array();

  $output = api_TrackingServices(
    array(
      'order_no' => $order_id
    )
  );

  if ( isset($output['error'] ) )
  {
    $errors[] = $output['error'];
  }
  else
  {
    $output_params = array(
      'shipped'                => $output['data']['orders'][0]['shipped'],
      'shipper_full_name'      => $output['data']['orders'][0]['shipperFullName'],
      'ship_date'              => $output['data']['orders'][0]['shipDate'],
      'expected_delivery_date' => $output['data']['orders'][0]['expectedDeliveryDate'],
      'returned'               => $output['data']['orders'][0]['returned'],
      'bookmark'               => $output['data']['bookmark']
    );
  }

  $output_params['success'] = $output['success'];
  $output_params['errors']  = $errors;

  return flexi_encode(fill_return($p,
                                  "customercare__ShipwireOrderDetails",
                                  func_get_args(),
                                  $output_params));
}

?>
