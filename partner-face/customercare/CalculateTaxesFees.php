<?php

// Determine the Sales Tax and Regulatory fees that would be charged for a potential Credit Card purchase, provided a purchase amount.
function customercare__CalculateTaxesFees($partner_tag, $msisdn, $charge_amount, $check_taxes, $check_recovery_fee, $zip_code)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $message = '';
  $sales_tax    = 0;
  $recovery_fee = 0;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    'customercare__CalculateTaxesFees',
                                    func_get_args(),
                                    array('success'      => TRUE,
                                          'sales_tax'    => 0,
                                          'recovery_fee' => 0,
                                          'errors'       => array(),
                                          'warnings'     => array('ERR_API_INTERNAL: always_succeed'))));

  $errors = validate_params($p, 'customercare__CalculateTaxesFees', func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  'customercare__CalculateTaxesFees',
                                    func_get_args(),
                                    array('success' => $success,
                                          'errors'  => $errors)));

  if ( !$check_taxes )
  {
    $salesTaxCalculator = new \SalesTaxCalculator();
    $recovery_fee = 0;
    if ( $check_recovery_fee )
      $recovery_fee = $recovery_fee = $salesTaxCalculator->getRecoveryFee($charge_amount, $customer->COS_ID)['recovery_fee'];

    return flexi_encode(fill_return($p,
                                    'customercare__CalculateTaxesFees',
                                    func_get_args(),
                                    array('success'      => TRUE,
                                          'sales_tax'    => 0,
                                          'recovery_fee' => $recovery_fee,
                                          'errors'       => array(),
                                          'warnings'     => array())));
  }

  // $check_taxes is set to a TRUE value
  try
  {
    // connect to the DB
    teldata_change_db();

    // get customer data
    $customer = get_customer_from_msisdn( normalize_msisdn_10( $msisdn ) );

    if ( !$customer || !is_object($customer) )
      throw new \Exception( 'No customer found for MSISDN '.$msisdn );

    // get zip code
    if (empty($zip_code))
      $zip_code = $customer->CC_POSTAL_CODE;

    if ( !$zip_code || !is_numeric($zip_code) || ( strlen($zip_code) != 5 ) )
      $zip_code = $customer->POSTAL_CODE;

    if ( !$zip_code || !is_numeric($zip_code) || ( strlen($zip_code) != 5 ) )
      throw new \Exception( 'No zip code found for this customer' );

    // calculate
    // $result = calculate_taxes_fees( $charge_amount , $customer->CUSTOMER_ID , $zip_code , $customer->ACCOUNT );

    $salesTaxCalculator = new \SalesTaxCalculator();

    $salesTaxCalculator->setBillingInfoByCustomerId($customer->CUSTOMER_ID);
    $salesTaxCalculator->setAccount($customer->ACCOUNT);
    $salesTaxCalculator->setCosId($customer->COS_ID);
    $salesTaxCalculator->setBillingZipCode($zip_code);
    $salesTaxCalculator->setAmount($charge_amount);

    $result = $salesTaxCalculator->calculateTaxesFees();

    if ( $result->is_failure() )
    {
      $errors = $result->get_errors();

      throw new \Exception( $errors[0] );
    }

    // $sales_tax = $result->data_array['sales_tax'];
    $sales_tax = $result->data_array['sales_tax'] + $result->data_array['mts_tax'];

    if ( $check_recovery_fee )
      $recovery_fee = $recovery_fee = $salesTaxCalculator->getRecoveryFee($charge_amount, $customer->COS_ID)['recovery_fee'];

    $success = TRUE;
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());

    if ( !count($errors) )
      $errors[] = $e->getMessage();
  }

  return flexi_encode(fill_return($p,
                                  'customercare__CalculateTaxesFees',
                                  func_get_args(),
                                  array('success'      => $success,
                                        'warnings'     => array(),
                                        'sales_tax'    => $sales_tax,
                                        'recovery_fee' => $recovery_fee,
                                        'errors'       => $errors)));
}

?>
