<?php


include_once('db/htt_activation_log.php');
include_once('lib/messaging/functions.php');
include_once('classes/postageapp.inc.php');


// Customer Login for a dealer to Customer Care.

/*

If the provided agent_id matches up to the store that activated this account, no additional authorization is required. The API call will return with a url with a username and temporary one time password. The dealer should be directed to login at www.ultra.me. (This should behave exactly like LoginAsCustomer - including returning a URL) — Celluphone simply has to present this link on screen for the dealer to click and enter the site.

If the provided agent_id does not match, the customer will be SMSed with the password. The API call will return with a username and an flag noting that the password is being SMSed. Celluphone should present a Javascript dialog that would allow the dealer to type in the password and hit OK – once they type that in, Celluphone should append the password string to the URL and then present it on screen.

1 - Validate customer. 
2 - generate a 6 digit password (A-Z0-9), store in Redis with 15 minute TTL
3 - if agent_id = ACTIVATED_BY in activation log, then return to client.
4 - Otherwise, SMS to customer and then return empty password to client.

*/

function customercare__DealerLoginAsCustomer($partner_tag, $customer_id, $agent_id, $user_id)
{
  # celluphone $user_id is for audit log

  global $p;
  global $mock;
  global $always_succeed;

  $success   = FALSE;
  $login_url = '';
  $auth_required = FALSE;
  $warnings = array();
  $customer = NULL;
  $state    = NULL;

  dlog('',"customer_id = $customer_id");
  dlog('',"agent_id    = $agent_id");
  dlog('',"user_id     = $user_id");

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__DealerLoginAsCustomer",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__DealerLoginAsCustomer", func_get_args(), $mock);

  teldata_change_db(); // connect to the DB

  if ( ! ( $errors && count($errors) > 0 ) )
  {
    /* *** get customer data *** */

    $customer = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID'));

    if ( $customer )
    {
      /* *** get current state *** */
      $state = internal_func_get_state_from_customer_id($customer_id);

      if ( ! $state )
      { $errors[] = "ERR_API_INTERNAL: cannot determine customer status"; }
    }
    else
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__DealerLoginAsCustomer",
                                    func_get_args(),
                                    array("success"  => $success,
                                          "warnings" => $warnings,
                                          "errors"   => $errors)));

  if ( $state['state'] != 'Cancelled' )
  {

    /* *** get agent ID from activation log *** */

    $activation_agent_id = '';

    $htt_activation_log_select_query = htt_activation_log_select_query( array( 'customer_id'  => $customer->CUSTOMER_ID , 'nolock' => TRUE ) );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_activation_log_select_query));

    if ( $query_result && ( is_array($query_result) ) && count($query_result) > 0 )
    {
      $activation_agent_id = $query_result[0]->ACTIVATED_DEALER;

      dlog('',"ACTIVATED_BY = $activation_agent_id");
    }
    else
    {
      $warnings[] = "ERR_API_INTERNAL: cannot find activation data";

      $activation_agent_id = $agent_id; # MVNO-1131 : For now, if the activation data is not present, always succeed, with auth_required as false.
    }

    # set up a temporary password
    $password = func_create_temp_password($customer_id);

    dlog('',"temporary password = $password");

    #$login_url = 'https://services.ultra.me/login_api_restricted.php?customer_id='.urlencode($customer_id).'&password=';
    $login_url = sprintf('https://%s/login_api_restricted.php?customer_id=%s&password=',
                             find_credential("ultra/login/site"),
                             urlencode($customer_id));

    if ( $activation_agent_id == $agent_id ) #was === which wouldn't match 50 to '50'
    {
      # output in URL

      dlog('',"agent matches");

      $login_url .= urlencode($password);
    }
    else
    {
      dlog('',"agent does not match");

      # send SMS
      $auth_required=TRUE;
      $result = funcSendExemptCustomerSMSTempPasswordDealer(
        array(
          'customer_id'   => $customer->CUSTOMER_ID,
          'temp_password' => $password
        )
      );

      if ( ( ! $result['sent'] ) || ( count($result['errors']) ) )
      { $errors[] = "ERR_API_INTERNAL: SMS delivery error."; }

    }

    if ( ! count($errors) )
    {
      $success = TRUE;
    }

  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer's account has been cancelled."; }

  return flexi_encode(fill_return($p,
                                  "customercare__DealerLoginAsCustomer",
                                  func_get_args(),
                                  array("success"   => $success,
                                        "login_url" => $login_url,
                                        "auth_required" => $auth_required,
                                        "warnings"  => $warnings,
                                        "errors"    => $errors)));
}

?>
