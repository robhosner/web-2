<?php

// Applies the given Data Recharge to the customer account.
function customercare__ApplyDataRecharge($partner_tag, $msisdn, $data_soc_id)
{
  # $data_soc_id example: PayGo_250_500

  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__ApplyDataRecharge",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__ApplyDataRecharge", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__ApplyDataRecharge",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db();

  // Get customer from msisdn

  $customer = get_customer_from_msisdn($msisdn);

  if ( $customer )
  {
    // Get the customer plan
    $plan = get_plan_from_cos_id( $customer->COS_ID ); # L[12345]9

    // Get the data recharge data
    $data_recharge = find_data_recharge($plan,$data_soc_id);

    dlog('',"plan : $plan ; data_recharge : %s",$data_recharge);

    try
    {
      $redis = new \Ultra\Lib\Util\Redis;

      if ( ! $data_recharge )
        throw new Exception("ERR_API_INVALID_ARGUMENTS: data_soc_id not valid");

      if ( get_data_recharge_semaphore( $redis , $customer->CUSTOMER_ID ) )
        throw new Exception("ERR_API_INTERNAL: a data recharge has been processed less than 15 minutes ago.");

      // Verify that the customer is Active

      $state = internal_func_get_state_from_customer_id( $customer->CUSTOMER_ID );

      if ( $state['state'] != STATE_ACTIVE )
        throw new Exception("ERR_API_INVALID_ARGUMENTS: customer state is not Active.");

      // Check for sufficient wallet balance for the given data SOC

      $customer = get_customer_from_customer_id( $customer->CUSTOMER_ID );

      if ( $data_recharge['cost'] > $customer->BALANCE )
        throw new Exception("ERR_API_INTERNAL: not enough money to perform this operation");

      // Verify Mint threshold for Data Add Ons
      $data_add_on = \Ultra\MvneConfig\getDataAddOnForMakeitsoUpgradePlan( $customer , $data_recharge ); // example: 'A-DATA-BLK|250'
      if (\Ultra\MvneConfig\isMintDataSOC($data_add_on))
      {
        $info  = \Ultra\UltraConfig\getBoltOnInfoByUpgradePlanSoc($data_add_on);
        $error = \Ultra\Lib\BoltOn\verifyMintDataAddOnThreshold( $customer, $info['type'] );

        if ( $error )
          throw new Exception( $error );
      }

      // initialize the context for the sequence of actions we will generate
      $context = array(
        'customer_id' => $customer->CUSTOMER_ID
      );

      $action_seq         = 0;
      $action_transaction = NULL; # no TRANSITION_UUID yet

      // add action to validate balance
      $result_status = append_make_funcall_action(
        'assert_balance',
        $context,
        $action_transaction,
        $action_seq,
        ( $data_recharge['cost'] * 100 ),
        NULL,
        NULL,
        NULL,
        'customercare__ApplyDataRecharge'
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (1)");

      // enqueue to the TRANSITION_UUID created with the previous append_make_funcall_action
      $context['transition_id'] = $result_status['transitions'][0];

      $action_seq++;

      $result_status = append_make_funcall_action(
        'mvneMakeitsoUpgradePlan',
        $context,
        $action_transaction,
        $action_seq,
        $data_add_on // example: 'A-DATA-BLK|250'
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (2)");

      $action_seq++;

      // Debit the wallet accordingly, including the name of the data recharge in the HTT_BILLING_LOG entry.
      $result_status = append_make_funcall_action(
        'spend_from_balance',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge['cost'], # amount in dollars
        __FUNCTION__,           # detail
        'DATA Purchase',        # reason
        create_guid('PHPAPI')   # reference source
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (4)");

      $action_seq++;

      // send SMS to customer
      $result_status = append_make_funcall_action(
        'send_sms_data_recharge',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge["MB"]
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (5)");

      $action_seq++;

      // Track Data Socs to htt_customers_overlay_ultra
      $result_status = append_make_funcall_action(
        'record_customer_soc',
        $context,
        $action_transaction,
        $action_seq,
        $data_recharge['data_soc']
      );

      if ( ! $result_status['success'] )
        throw new Exception("ERR_API_INTERNAL: unexpected error (6)");

      unreserve_transition_uuid_by_pid($context['transition_id']);

      // Record event
      $event_status = log_data_event(
        array(
          'action'      => 'Recharge ' . $data_recharge['MB'] . 'MB',
          'customer_id' => $customer->CUSTOMER_ID,
          'soc'         => $data_recharge['data_soc']
        )
      );

      // Set Redis semaphore to block another ApplyDataRecharge by the same customer for 15 minutes.
      set_data_recharge_semaphore($redis,$customer->CUSTOMER_ID);

      // Set Redis semaphore to block SMS from notification__DataNotificationHandler for 5 minutes.
      set_data_recharge_notification_delay($redis,$customer->CUSTOMER_ID);
    }
    catch(Exception $e)
    {
      dlog('', $e->getMessage());
      $errors[] = $e->getMessage();
    }
  }
  else
    $errors = array("ERR_API_INVALID_ARGUMENTS: customer not found");

  if ( ! count($errors) ) $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "customercare__ApplyDataRecharge",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

