<?php

// Verify that the PIN card is in our system, it has not been used and not disabled.
// Wraps around func_validate_pin_cards .
function provisioning__checkPINCard($partner_tag, $pin)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;
  $pin_value   = '';
  $used_date   = '';
  $used_msisdn = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "provisioning__checkPINCard",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "provisioning__checkPINCard", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "provisioning__checkPINCard",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $pin_validation = func_validate_pin_cards(
    array(
      'pin_list' => array($pin)
    )
  );

  if ( $pin_validation['at_least_one_not_found'] )
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: the PIN is invalid";       }

  else if ( $pin_validation['at_least_one_at_foundry'] )
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: the PIN is not active";    }

  else if ( count($pin_validation['errors']) )
  { $errors = $pin_validation['errors'];                               }

  else if ( $pin_validation['at_least_one_customer_used'] )
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: the PIN is already used";

    // fill $used_date and $used_msisdn

    if ( $pin_validation['result'][ $pin ]['customer_used'] )
    {
      $customer = get_ultra_customer_from_customer_id( $pin_validation['result'][ $pin ]['customer_used'] , array('current_mobile_number') );

      if ( $customer )
      {
        $used_msisdn = $customer->current_mobile_number;
      }
    }

    $used_date = get_date_from_full_date( $pin_validation['result'][ $pin ]['last_changed_date'] );
  }

  else
  {
    $success = TRUE;

    $pin_value = $pin_validation['result'][$pin]['pin_value'] * 100;
  }

  return flexi_encode(fill_return($p,
                                  "provisioning__checkPINCard",
                                  func_get_args(),
                                  array("success"     => $success,
                                        "pin_value"   => $pin_value,
                                        "used_date"   => $used_date,
                                        "used_msisdn" => $used_msisdn,
                                        "errors"      => $errors)));
}

?>
