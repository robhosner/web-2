<?php

// Validates 1 or more PINS.
function provisioning__checkPINCards($partner_tag, $pin, $pin2, $pin3, $pin4, $pin5, $pin6)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "provisioning__checkPINCards",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "provisioning__checkPINCards", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "provisioning__checkPINCards",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $pin_list = array($pin);

  if ( $pin2 )
  { $pin_list[] = $pin2; }

  if ( $pin3 )
  { $pin_list[] = $pin3; }

  if ( $pin4 )
  { $pin_list[] = $pin4; }

  if ( $pin5 )
  { $pin_list[] = $pin5; }

  if ( $pin6 )
  { $pin_list[] = $pin6; }

  dlog('',"PIN list = %s",$pin_list);

  $validate_pin_result = array();

  try
  {
    $success = TRUE;

    $validate_pin_result = func_validate_pin_cards(
      array(
        'pin_list' => $pin_list
      )
    );

    dlog('',"validate_pin_result = %s",$validate_pin_result);

    if ( $validate_pin_result["at_least_one_customer_used"] )
    { throw new Exception("ERR_API_INTERNAL: One or more PINs are already used"); }

    if ( $validate_pin_result["at_least_one_not_found"] )
    { throw new Exception("ERR_API_INTERNAL: Could not find one or more PINs"); }

    // PIN status should be AT_MASTER
    if ( $validate_pin_result["at_least_one_at_foundry"] )
    { throw new Exception("ERR_API_INTERNAL: One or more PINs are cannot be used"); }
  }
  catch(Exception $e)
  {
    $success = FALSE;
    dlog('', $e->getMessage());

    // return an array containing each of the failed PIN cards along with a reason for the failure.

    foreach( $validate_pin_result["result"] as $pin_validated => $pin_result )
    {
      if ( $pin_result["not_found"] )
      {
        $errors[] = "ERR_API_INVALID_ARGUMENTS: PIN $pin_validated not found.";
      }
      elseif( ! empty($pin_result["customer_used"]))
      {
        $errors[] = "ERR_API_INVALID_ARGUMENTS: PIN $pin_validated is already used.";
      }
      elseif( $pin_result["status"] != "AT_MASTER" )
      {
        $errors[] = "ERR_API_INVALID_ARGUMENTS: PIN $pin_validated cannot be used.";
      }
    }
  }

  return flexi_encode(fill_return($p,
                                  "provisioning__checkPINCards",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
