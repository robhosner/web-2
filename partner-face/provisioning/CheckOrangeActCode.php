<?php

include_once 'classes/TransactionMonitor.php';
include_once 'session.php';

/**
 * Returns all the surrounding data around an OrangeActCode - is the act code valid, has it been used, what is the status, etc.
 * Used for both the web activation and IVR activation flow for Orange SIMs - the customer_id or mobile number is purposefully not included in this API call for security reasons.
 */
function provisioning__CheckOrangeActCode($partner_tag, $actcode, $mode, $brand)
{
  global $p;
  global $mock;
  global $always_succeed;

  $output_array = array(
    "success"  => TRUE,
    "errors"   => array(),
    "warnings" => array(),
    "request_id"         => '', // string  - The UUID of the transition initiated by requestProvisionOrangeCustomerAsync, if OPEN.
    "zsession"           => '', // string  - The zsession
    "customer_id"        => '', // int     - The Customer ID
    "sim_hot"            => '', // boolean - Is the SIM Hot? (ready to activate?)
    "sim_used"           => '', // boolean - Has the sim been activated/assigned a number?
    "user_state"         => '', // string  - The state of the user associated with the sim, if any.
    "mins_since_act"     => '', // int     - Minutes between user's associated creation date time (if any) and current time
    "sim_ready_activate" => '', // boolean - If in PREACTIVATION mode, confirms that sim is ready to be activated.
    "stored_value"       => '', // int     - The stored_value on the SIM, helps UI determine which monthly plan it is for
    "reservation_time"   => '', // string  - MSSQL datetime value of when the activation process began
    "msisdn"             => '0', // string  -- If the zSession is valid and the user_state = Active, return this value
    'duration'           => '',  // number of prepaid months
    'iccid'              => '',
    'master_agent'       => ''
  );

  if ($always_succeed)
  {
    $output_array['warnings'] = array("ERR_API_INTERNAL: always_succeed");

    return flexi_encode(fill_return($p,
      "provisioning__CheckOrangeActCode",
      func_get_args(),
      $output_array));
  }
  
  try
  {
    teldata_change_db(); // connect to the DB
      
    // Bypass the throttling check, if the Partner is Shore, env = rainmaker_dev or env = rainmaker_prod
    // all others, fall through
    dlog('',"Throttling Environment  = %s", trim( strtolower(get_htt_env() ) ) );
    
    if( ( trim( strtolower(get_htt_env())) != 'rainmaker_dev') && (trim( strtolower(get_htt_env())) != 'rainmaker_prod') )
    {
        // Check to see if the calling partner requires throttling
        $serverAddress = $_SERVER['REMOTE_ADDR'];
        dlog('',"Validating remote server = %s", $serverAddress);

        $redis = new \Ultra\Lib\Util\Redis;
    
        if( filter_var($serverAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ))
        {
            // First check our cache to see if the Transaction Manager Object is present
            $key      = "TransactionManager/" . $serverAddress;
            $mcObject = $redis->get($key);
            $server   = null;
    
            if( $mcObject !== false ) // We have a Transaction Manager object
            {
               $redis->del($key);
               $server = unserialize($mcObject);
               if ( is_object($server) )
               {
                 $server->setAccessList( time() ); // Set this access into our object

                 if( $server->needsThrottling($key) )
                 {
                   // Save off our incremented object, then inform the user they have been throttled.
                   $redis->set( $key , serialize( $server ) );
                   dlog('',"Rate Throttling remote server = %s, Key = %s", $serverAddress, $key);
                   throw new Exception("ERR_API_THROTTLE: You have made too many entries, please try again later.");
                 } else {
                   // Does not need throttling, just save off, and fall through to other logic.
                   $redis->set( $key , serialize( $server ) );
                 }
               } else {
                 $server = new TransactionMonitor($key);
                 dlog('', "creating new key=%s", $key);
                 $server->setAccessList( time() ); // Set this access into our object
                 $redis->set( $key , serialize( $server ) );
               }
             } else {
               // First request--at least on this reboot
               $server = new TransactionMonitor($key);
               dlog('', "creating new key=%s", $key);
               $server->setAccessList( time() ); // Set this access into our object
               $redis->set( $key , serialize( $server ) );
            }
    
        } else {
            // An invalid IP address
            dlog('',"POSSIBLE SECURITY VIOLATION--Invalid IP Address:remote server = %s", $serverAddress);
            throw new Exception("ERR_API_INTERNAL: Invalid IP/or Spoofed IP address detected.");
        }
    }


    $output_array['errors'] = validate_params($p, "provisioning__CheckOrangeActCode", func_get_args(), $mock);

    if ($output_array['errors'] && count($output_array['errors']) > 0)
    {
        $output_array['success'] = FALSE;

        // we use errors[] and warnings[] for more nuance instead of SoapFaults
        //throw new SoapFault("Server","Invalid parameters.");
        return flexi_encode(fill_return($p,
            "provisioning__CheckOrangeActCode",
            func_get_args(),
            $output_array));
    }


      // We got this far, they did not require rate throttling
    //
    $data = get_htt_inventory_sim_from_actcode( $actcode );

    if ( ( ! $data ) || ( ! is_array( $data ) ) )
      throw new Exception("ERR_API_INTERNAL: DB Error");

    if ( ! count( $data ) )
      throw new Exception("ERR_API_INTERNAL: actcode not found in DB");

    // check if brand is allowed
    if ( ! \Ultra\UltraConfig\isBrandIdAllowed($data[0]->BRAND_ID))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid');

    // check if input brand matches ICCID brand
    if (\Ultra\UltraConfig\getShortNameFromBrandId($data[0]->BRAND_ID) != $brand)
      throw new \Exception("ERR_API_INTERNAL: Invalid Activation Code");

    if ( $data[0]->PRODUCT_TYPE != 'ORANGE' )
      throw new Exception("ERR_API_INTERNAL: product type not valid");

    dlog('',"SIM_HOT = %s , SIM_ACTIVATED = %s",$data[0]->SIM_HOT,$data[0]->SIM_ACTIVATED);

    $output_array['sim_hot']    = ! ! $data[0]->SIM_HOT;
    $output_array['sim_used']   = ! ! $data[0]->SIM_ACTIVATED;
    $output_array['request_id'] = get_open_activation_transition_uuid_from_iccid( $data[0]->ICCID_FULL );
    $output_array['stored_value']   = $data[0]->STORED_VALUE;
    $output_array['reservation_time']   = $data[0]->RESERVATION_TIME;
    $output_array['duration'] = $data[0]->DURATION;
    $output_array['iccid']    = $data[0]->ICCID_FULL;
    $output_array['master_agent'] = $data[0]->INVENTORY_MASTERAGENT;

    if ( $data[0]->CUSTOMER_ID )
    {
      $customer = get_customer_from_actcode($actcode);

      if ( ! $customer )
        throw new Exception("ERR_API_INTERNAL: Customer not found");

      $output_array['user_state']     = $customer->plan_state;
      $output_array['mins_since_act'] = $customer->mins_since_act;

      $_REQUEST['zsession'] = session_save($customer);
      $verified = verify_session( $customer->CUSTOMER );
      $output_array['zsession'] = $verified[2];
      $output_array['zsessionC'] = $customer->CUSTOMER;
      setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
      setcookielive('zsessionC', $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);
      $output_array['customer_id'] = $customer->CUSTOMER_ID;
      
      if( $output_array['user_state'] == 'Active')
      {
          $output_array['msisdn'] = ( $customer->current_mobile_number > 0 ? $customer->current_mobile_number : 0 );
      }
      
      if ( $mode == 'PREACTIVATION' )
        $output_array['sim_ready_activate'] = FALSE;
    }
    else
    {
      if ( $mode == 'PREACTIVATION' )
      {
        $output_array['sim_ready_activate'] = ! ! ( ( $output_array['sim_hot'] ) && ! ( $output_array['sim_used'] ) );

        if ( $output_array['sim_ready_activate'] )
        {
          $result = NULL;

            $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
            $result = $mwControl->mwCanActivate(
              array(
                'actionUUID' => getNewActionUUID('provisioning ' . time()),
                'iccid' => $data[0]->ICCID_FULL));

            if ( ! isset( $result->data_array['available']))
            {
              $output_array['sim_ready_activate'] = FALSE;
              throw new Exception ('ERR_API_INTERNAL: MW did not return SIM availability');
            }

          dlog('',"mvne result data = %s",$result->data_array);

          if ( $result->is_timeout() )
          {
            $output_array['sim_ready_activate'] = FALSE;
            throw new Exception("ERR_API_INTERNAL: middleware timeout");
          }

          if ( $result->is_failure() )
          {
            $output_array['sim_ready_activate'] = FALSE;
            throw new Exception("ERR_API_INTERNAL: : the given ICCID cannot be activated");
          }

/*
** MVNE2 ONLY **

          if ( ! isset( $result->data_array['available']))
          {
            $output_array['sim_ready_activate'] = FALSE;
            throw new Exception ('ERR_API_INTERNAL: MW did not return SIM availability');
          }
*/

          // if the SIM comes back as ready to activate set a Redis flag with ttl = 5m noting that sim is 'good to activate'

          $redis = new \Ultra\Lib\Util\Redis;

          $redis->set( 'iccid/good_to_activate/' . $data[0]->ICCID_FULL , 1 , 60 * 20 );
        }
      }
    }

    if ( ( ! count($output_array['errors']) ) && ! $output_array['customer_id'] )
    {
      // check if there is a row in HTT_CUSTOMERS_OVERLAY_ULTRA with CURRENT_ICCID_FULL = $data[0]->ICCID_FULL

      $customer = get_customer_from_iccid( $data[0]->ICCID_FULL );

      if ( $customer && is_object($customer) && $customer->CUSTOMER_ID )
      {
        $output_array['customer_id'] = $customer->CUSTOMER_ID;
        $_REQUEST['zsession'] = session_save($customer);
        $verified = verify_session( $customer->CUSTOMER );
        $output_array['zsession'] = $verified[2];
        $output_array['zsessionC'] = $customer->CUSTOMER;
        setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
        setcookielive('zsessionC', $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);
        
        if( $output_array['user_state'] == 'Active')
        {
            $output_array['msisdn'] = ( $customer->current_mobile_number > 0 ? $customer->current_mobile_number : 0 );
        }
      }
    }
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $output_array['errors'][] = $e->getMessage();
  }
  
  $output_array['success'] = ! count($output_array['errors']);

  return flexi_encode(fill_return($p,
    "provisioning__CheckOrangeActCode",
    func_get_args(),
    $output_array));
}

?>
