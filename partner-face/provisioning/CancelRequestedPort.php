<?php

// Cancels a Port Request. (then does an API QueryPortIn to send the customer to Port-In Denied.)
function provisioning__CancelRequestedPort($partner_tag, $ICCID, $numberToPort)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $warnings = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__CancelRequestedPort",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__CancelRequestedPort", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "provisioning__CancelRequestedPort",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db();

  // Use ICCID and MSISDN to find the customer_id record.

  $customer = get_customer_from_msisdn($numberToPort);

  if ($customer)
  {
    // check if intra-brand port, cannot cancel
    $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);
    if ($sim && $customer->BRAND_ID != $sim->BRAND_ID)
      $errors[] = 'The porting attempt cannot be cancelled';
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: msisdn $numberToPort not found.";

  if ( ! count($errors) )
  {
    if ( $customer->CURRENT_ICCID_FULL == $ICCID )
    {
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
        $result = $mwControl->mwCancelPortIn(
          array(
            'actionUUID' => getNewActionUUID('provisioning ' . time()),
            'msisdn'     => $numberToPort));

        if ( $result->is_failure() )
        {
          if ( $result->is_timeout())
            $errors[] = 'ERR_API_INTERNAL: MW timeout';
          else
            $errors[] = 'ERR_API_INTERNAL: MW error - ' . implode(' ; ', $result->get_errors());
        }

        if ( ! $result->data_array['success'] )
          $errors[] = 'ERR_API_INTERNAL: MW error - ' . implode(' ; ', $result->data_array['errors']);

      if (! count($errors))
      {
        // transition the customer to 'Cancelled'

        $context = array(
          'customer_id' => $customer->customer_id
        );

        $result_status = cancel_account($customer,$context,NULL,'provisioning__CancelRequestedPort','CancelRequestedPort called','provisioning API');

        $errors = $result_status['errors'];

        if ( ! count($errors) )
        {
          $success = TRUE;
        }
      }
    }
    else
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: msisdn $numberToPort not associated with ICCID $ICCID."; }
  }

  return flexi_encode(fill_return($p,
                                  "provisioning__CancelRequestedPort",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

?>
