<?php

/**
 * provision_check_transition
 *
 * Returns an array of info about the given transition
 *
 * @return array
 */
function provision_check_transition($request_id)
{
  $transition_result = array(
    'pending'      => FALSE,
    'success'      => FALSE,
    'status'       => '',
    'phone_number' => '',
    'warnings'     => array(),
    'errors'       => array()
  );

  $transition_uuid = fix_transition_uuid_format($request_id);

  if ( $transition_uuid != $request_id ) { $transition_result['warnings'][] = "Queried transition_uuid: $transition_uuid instead of $request_id"; }

  // given a TRANSITION_UUID, provides the whole HTT_TRANSITION_LOG row, also the boolean flag is_closed
  $result = internal_func_check_transition(
    array(
      'transition_uuid' => $transition_uuid,
      'customer_id'     => ''
    )
  );

  dlog('',"internal_func_check_transition returns %s",$result);

  if ( $result && is_array($result) && ( count($result) > 0 ) )
  {
    $transition_result['port_attempt_customer_id'] = $result[0]->CUSTOMER_ID;
    $transition_result['transition_status'] = $result[0]->STATUS;
    $transition_result['transition_label'] = $result[0]->TRANSITION_LABEL;

    if ( $result[0]->STATUS == 'CLOSED' )
    {

      ##==-- retrieve $phone_number and $status --==##

      $customer = get_customer_from_customer_id( $result[0]->CUSTOMER_ID );

      if ( $customer )
      {
        $transition_result['success']      = TRUE;
        $transition_result['phone_number'] = $customer->current_mobile_number;
        $transition_result['status']       = $customer->plan_state;
        $transition_result['customer_id']  = $customer->CUSTOMER_ID;
      }
      else
      {
        $transition_result['errors'][] = "ERR_API_INTERNAL: customer not found.";
      }

    }
    else if ( $result[0]->STATUS == 'ABORTED' )
    {
      $transition_result['errors'][] = "ERR_API_INTERNAL: request aborted.";
    }
    else
    {
      # the transition is still not completed.

      $transition_result['success'] = TRUE;
      $transition_result['pending'] = TRUE;

      $customer = get_customer_from_customer_id( $result[0]->CUSTOMER_ID );

      $transition_result['customer_id']  = $customer->CUSTOMER_ID;
    }
  }
  else
  {
    $transition_result['errors'][] = "ERR_API_INVALID_ARGUMENTS: request not found.";
  }

  return $transition_result;
}

