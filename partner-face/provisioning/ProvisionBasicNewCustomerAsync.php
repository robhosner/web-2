<?php

/*

=======================================
APPARENTLY THIS API IS NOT USED ANYMORE
=======================================

*/

// Creates an Ultra customer, sets up their basic data, leaves it in a provisioned state.
function provisioning__ProvisionBasicNewCustomerAsync($partner_tag, $ICCID, $targetPlan, $customerZip, $preferred_language)
{
  global $p;
  global $mock;
  global $always_succeed;

  # This can be implemented as a Sync Wrapper around requestProvisionNewCustomerAsync,
  # with activation_masteragent=54, activation_agent=28, activation_userid=67, loadPayment=NONE

  $success      = FALSE;
  $phone_number = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__ProvisionBasicNewCustomerAsync",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors   = validate_params($p, "provisioning__ProvisionBasicNewCustomerAsync", func_get_args(), $mock);
  $warnings = array();

  if ( empty($errors) )
  {
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('customerZip',$customerZip,'in_coverage');

    if ( $error )
      $errors[] = $error;
  }

  if ( empty($errors) )
  {
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$ICCID,'VALID');

    if ( $error )
      $errors[] = $error;
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "provisioning__ProvisionBasicNewCustomerAsync",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  $asynch_partner_tag = '';

  if ( isset($partner_tag) )
    $asynch_partner_tag = $partner_tag;

  $apache_username = 'dougmeli';
  $apache_password = 'Flora';

  # TODO: those should go away sometime soon; what should we do in the dev env?
  $curl_options = array(
    CURLOPT_USERPWD  => "$apache_username:$apache_password",
    CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
  );

  $asynch_result = perform_asynch_command(
    array(
      'seconds_timeout' => 500,
      'url'             => find_site_url(),
      'partner'         => $_REQUEST['partner'],
      'namespace'       => 'provisioning',
      'command'         => 'ProvisionNewCustomerAsync',
      'curl_options'    => $curl_options,
      'curl_params'     => array(
        'partner_tag'            => $partner_tag,
        'ICCID'                  => $ICCID,
        'loadPayment'            => 'NONE',
        'targetPlan'             => $targetPlan,
        'customerFirstName'      => 'FirstName',
        'customerLastName'       => 'LastName',
        'customerZip'            => $customerZip,
        'customerEMail'          => 'ivr-activated@null',
        'mock'                   => 'allow_duplicate_email',
        'activation_masteragent' => '54',
        'activation_agent'       => '28',
        'activation_store'       => '28',
        'activation_userid'      => '67',
        'preferred_language'     => $preferred_language
      )
    )
  );

 #   [status] => Active

  $success = $asynch_result['success'];

  if ( isset($asynch_result['phone_number']) )
    $phone_number = $asynch_result['phone_number'];

  if ( isset($asynch_result['errors']) && count($asynch_result['errors']) )
    $errors = array_merge( $errors , $asynch_result['errors'] );

  if ( isset($asynch_result['warnings']) && count($asynch_result['warnings']) )
    $warnings = array_merge( $warnings , $asynch_result['warnings'] );

  return flexi_encode(fill_return($p,
                                  "provisioning__ProvisionBasicNewCustomerAsync",
                                  func_get_args(),
                                  array("success"      => $success,
                                        "warnings"     => $warnings,
                                        "phone_number" => $phone_number,
                                        "errors"       => $errors)));
}

