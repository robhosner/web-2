<?php

// Creates an Ultra customer using an Orange SIM, sets up their basic data, leaves it in a provisioned state.


// UNUSED


function provisioning__ProvisionBasicOrangeCustomerAsync($partner_tag, $actcode, $preferred_language, $customerZip)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__ProvisionBasicOrangeCustomerAsync",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__ProvisionBasicOrangeCustomerAsync", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "provisioning__ProvisionBasicOrangeCustomerAsync",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));

  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "provisioning__ProvisionBasicOrangeCustomerAsync",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

