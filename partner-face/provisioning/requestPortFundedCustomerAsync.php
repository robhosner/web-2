<?php

include_once 'lib/inventory/functions.php';
include_once 'lib/portal/functions.php';
include_once 'lib/portin/functions.php';
include_once 'lib/util-common.php';

require_once 'classes/VersionOneResult.php';
require_once 'classes/IntraBrand.php';

// This API call is used by the website to load a 'Pre-Funded' customer to request a port. This call uses an existing Ultra Customer.
function provisioning__requestPortFundedCustomerAsync($partner_tag, $ICCID, $zsession, $numberToPort, $portAccountNumber, $portAccountPassword, $portAccountZipcode)
{
  # $ICCID is 19 digits

  global $p;
  global $mock;
  global $always_succeed;

  // initiate version 1 result object
  $apiResult = new \VersionOneResult($p, 'provisioning__requestPortFundedCustomerAsync');
  $apiResult->setArgs(func_get_args(), $mock);

  # [ Pre-Funded ] ~~>> [ Port-In Requested ]

  $success                = FALSE;
  $transition_uuid        = '';
  $activation_masteragent = 54;
  $activation_distributor = NULL;
  $activation_agent       = 34;
  $activation_store       = NULL;
  $activation_userid      = 110;

  # trim spaces
  $portAccountPassword = trim($portAccountPassword);
  $portAccountNumber   = trim($portAccountNumber);

  $customer = null;
  $context = array(); // port transition context

  try
  {
    if ($always_succeed)
    {
      $apiResult->succeed();
      $apiResult->addWarning('ERR_API_INTERNAL: always_succeed');
      $apiResult->clearErrors();

      return $apiResult->getJSON();
    }

    // input validation errors
    if ($apiResult->hasErrors())
      return $apiResult->getJSON();

    teldata_change_db();

    # $customer = get_zsession_customer(); # BROKEN

    $check_session   = session_read($zsession);
    $customer_string = $check_session[0];

    $customer = get_customer_from_customer($customer_string);

    if ($customer)
    {
      $customer_id = $customer->CUSTOMER_ID;
      $customer    = get_customer_from_customer_id($customer->CUSTOMER_ID);

      if ( ! $customer)
        throw new Exception('ERR_API_INTERNAL: DB error loading customer from session');
    }
    else
      throw new Exception('ERR_API_INTERNAL: user not logged in');

    // validate ICCID
    if ( ! validate_ICCID($ICCID, 1))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used');

    save_port_account( $numberToPort , $portAccountNumber , $portAccountPassword );

    // transition name is different for intra and normal ports
    $transition_name = '';

    // check for intra-brand port
    if ($customer->BRAND_ID != get_brand_id_from_iccid($ICCID))
    {
      // is intra port
      $transition_name = 'Activate Intra Port ' . get_plan_name_from_short_name($target_plan);
      $context['old_iccid'] = luhnenize($customer->CURRENT_ICCID);
      $context['old_brand_id'] = $customer->BRAND_ID;
    }
    // normal port
    {
      // verify port eligibility over middleware
      $result = \Provisioning::verifyPortInEligibility($numberToPort);
      if ($error = $result->get_first_error())
        throw new Exception($error);

      // verifiy numberToPort not in system
      $result = \Provisioning::verifyNumberNotInSystem($numberToPort);
      if ($error = $result->get_first_error())
        throw new Exception($error);

      if ($customer->plan_state != STATE_PRE_FUNDED)
        throw new Exception("ERR_API_INTERNAL: customer state {$customer->plan_state} is not valid for the given request");

      // update customer with current_iccid
      $sql = htt_customers_overlay_ultra_update_query(
        array(
          'customer_id'           => $customer->CUSTOMER_ID,
          'current_mobile_number' => $numberToPort,
          'current_iccid_full'    => $ICCID,
          'current_iccid'         => substr($ICCID, 0, -1)
        )
      );

      if ( ! is_mssql_successful(logged_mssql_query($sql)) )
        throw new Exception('ERR_API_INTERNAL: Database write error');

      // change state to SIM Received Request Port - Transition [ Pre-Funded ] => [ Port-In Requested ]
      $transition_name = 'SIM Received Request Port';
    }
    // end normal port

    // zip code adjustments
    if ( ! $portAccountZipcode )
    {
      $portAccountZipcode = $customer->POSTAL_CODE;

      if ( ! $portAccountZipcode)
        $portAccountZipcode = $customer->CC_POSTAL_CODE;
    }

    // context to pass to transition
    $context['customer_id']              = $customer->CUSTOMER_ID;
    $context['port_in_msisdn']           = $numberToPort;
    $context['port_in_iccid']            = $ICCID;
    $context['port_in_account_number']   = $portAccountNumber;
    $context['port_in_account_password'] = $portAccountPassword;
    $context['port_in_zipcode']          = $portAccountZipcode;

    // perform dry run first
    $dry_run     = true;
    $resolve_now = false;

    $changeStateResult = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);

    if ( ! $changeStateResult['success'])
      throw new Exception('ERR_API_INTERNAL: state transition error (1)');

    // save activation attribution info in redis before attempting state transition
    set_redis_provisioning_values(
      $customer->CUSTOMER_ID,
      $activation_masteragent,
      $activation_agent,
      $activation_distributor,
      $activation_store,
      $activation_userid
    );

    // change state
    $dry_run     = false;
    $resolve_now = true;

    $changeStateResult = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);

    if ( ! $changeStateResult['success'])
    {
      clear_redis_provisioning_values($customer->CUSTOMER_ID);
      throw new Exception('ERR_API_INTERNAL: state transition error (2)');
    }

    $array_values = array_values($changeStateResult['transitions']);
    $request_id   = end($array_values);
    $apiResult->addData('request_id', $request_id);

    // memorize $request_id in Redis by customer_id for portal__SearchActivationHistoryDealer
    $redis_port = new \Ultra\Lib\Util\Redis\Port();
    $redis_port->setRequestId($customer->CUSTOMER_ID, $request_id);

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    if ( count($pipeError = explode('|', $error)) )
      $error = $pipeError[0];
    
    $apiResult->addError($error);
  }

  if ( ! $apiResult->isSuccess())
    $apiResult->addData('request_id', '');

  $fraud_data = array(
    'amount'            => 0,
    'activation_userid' => '',
    'cos_id'            => (isset($customer) && is_object($customer)) ? $customer->COS_ID : '',
    'transition_uuid'   => create_guid('PROVISIONING_API')
  );

  $outcome  = ($apiResult->isSuccess()) ? 'success' : 'error' ;
  $customer = (isset($customer) && is_object($customer)) ? $customer : null;
  fraud_event($customer, 'provisioning', 'requestPortFundedCustomerAsync', $outcome, $fraud_data);

  return $apiResult->getJSON();
}
