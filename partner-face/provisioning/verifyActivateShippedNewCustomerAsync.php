<?php

// This is the API to be invoked after requestActivateShippedNewCustomerAsync in order to get the complete response.
function provisioning__verifyActivateShippedNewCustomerAsync($partner_tag, $request_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success         = FALSE;
  $pending         = FALSE;
  $phone_number    = '';
  $customer_status = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "provisioning__verifyActivateShippedNewCustomerAsync",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "pending"  => FALSE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "provisioning__verifyActivateShippedNewCustomerAsync", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "provisioning__verifyActivateShippedNewCustomerAsync",
                                    func_get_args(),
                                    array("success" => $success,
                                          "pending" => $pending,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  ##==-- check transition --==##

  $transition_uuid = fix_transition_uuid_format($request_id);

  if ( $transition_uuid != $request_id ) { $transition_result['warnings'][] = "Queried transition_uuid: $transition_uuid instead of $request_id"; }

  $result = internal_func_check_transition(
    array(
      'transition_uuid' => $transition_uuid
    )
  );

  if ( $result && is_array($result) && ( count($result) > 0 ) )
  {

    if ( $result[0]->STATUS == 'CLOSED' )
    {

      ##==-- retrieve $phone_number and $status --==##

      $customer = get_ultra_customer_from_customer_id(
        $result[0]->CUSTOMER_ID,
        array('current_mobile_number','plan_state')
      );

      if ( $customer )
      {
        $success         = TRUE;
        $phone_number    = $customer->current_mobile_number;
        $customer_status = strtoupper($customer->plan_state);
      }
      else
      {
        $errors[] = "ERR_API_INTERNAL: customer not found.";
      }

    }
    else if ( $result[0]->STATUS == 'ABORTED' )
    {
      $errors[] = "ERR_API_INTERNAL: request aborted.";
    }
    else
    {
      # the transition is still not completed.

      $success = TRUE;
      $pending = TRUE;
    }

  }
  else
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: request not found.";
  }

  $errors = append_transition_failure_reason($errors,$request_id);

  return flexi_encode(fill_return($p,
                                  "provisioning__verifyActivateShippedNewCustomerAsync",
                                  func_get_args(),
                                  array("success"      => $success,
                                        "phone_number" => $phone_number,
                                        "status"       => $customer_status,
                                        "pending"      => $pending,
                                        "errors"       => $errors)));
}

?>
