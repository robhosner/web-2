<?php

// This API call is used to create a Orange SIM customer and 'activate' his SIM.
function provisioning__requestProvisionOrangeCustomerAsync($partner_tag, $actcode, $preferred_language, $customerZip, $activation_channel, $activation_info, $dealer_code)
{
  global $p;
  global $mock;
  global $always_succeed;

  $request_id = '';
  $zsession = '';
  $customer_id = '';
  
  if ($always_succeed)
    return flexi_encode(fill_return($p,
        "provisioning__requestProvisionOrangeCustomerAsync",
        func_get_args(),
        array("success"  => TRUE,
            "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__requestProvisionOrangeCustomerAsync", func_get_args(), $mock);

  teldata_change_db();

  if ( empty($errors) )
  {
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('customerZip',$customerZip,'in_coverage');

    if ( $error )
      $errors[] = $error;
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
        "provisioning__requestProvisionOrangeCustomerAsync",
        func_get_args(),
        array("success" => FALSE,
              "errors"  => $errors)));

  try
  {
    // PROD-868: use correct dealer and the corresponding master
    $dealer_code = empty($dealer_code) ? ($activation_info == 'IP' ? 'UMWeb' : 'UMIVR') : $dealer_code;
    if ( ! $dealer_info = \Ultra\Lib\DB\Celluphone\getDealerInfo($dealer_code))
      throw new Exception('ERR_API_INTERNAL: No celluphone dealer info found');
    $distributor = $dealer_info->distributorId;
    $masteragent = $dealer_info->masterId;
    $store       = $dealer_info->dealerId;
    $userid      = 67;

    // confirm activations
    if ( ! activations_enabled())
      throw new Exception('ERR_API_INTERNAL: account activations are temporarily disabled, please try later');

    // retrieve SIM data from $actcode
    $sim_data = get_htt_inventory_sim_from_actcode( $actcode );
    dlog('',"sim_data = %s",$sim_data);

    // validate SIM data
    if ( ( ! $sim_data ) || ( ! is_array( $sim_data ) ) )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: the given actcode is invalid.");
    
    if ( ! count( $sim_data ) )
      throw new Exception("ERR_API_INTERNAL: actcode not found in DB");

    if ( ! \Ultra\UltraConfig\isBrandIdAllowed($sim_data[0]->BRAND_ID))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid');
    
    if ( $sim_data[0]->PRODUCT_TYPE != 'ORANGE' )
      throw new Exception("ERR_API_INTERNAL: SIM product type not valid");
    
    if ( ! $sim_data[0]->SIM_HOT )
      throw new Exception("ERR_API_INTERNAL: the SIM cannot be activated.");

    if (! validate_ICCID($sim_data[0], TRUE))
      throw new Exception("ERR_API_INTERNAL: the SIM is already used");


    if ($sim_data[0]->BRAND_ID != 2 && !empty($sim_data[0]->OFFER_ID))
    {
      // check for stored value and that the plan price is the same as the stored value
      if (!$sim_data[0]->STORED_VALUE || $sim_data[0]->STORED_VALUE != getPlanCost($sim_data[0]))
      {
        throw new Exception('ERR_API_INVALID_ARGUMENTS: There is no stored value or the stored value is not the same as the plan cost for this SIM.');
      }
    }

    $redis = new \Ultra\Lib\Util\Redis;

    // Activate/Can
    if ( $redis->get( 'iccid/good_to_activate/' . $sim_data[0]->ICCID_FULL ) )
      $redis->del( 'iccid/good_to_activate/' . $sim_data[0]->ICCID_FULL );
    else
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwCanActivate(array(
        'iccid'      => $sim_data[0]->ICCID_FULL,
        'actionUUID' => getNewActionUUID('provisioning ' . time())));

      if ($result->is_failure() || ! isset($result->data_array['available']))
        throw new Exception("ERR_API_INTERNAL: the given ICCID cannot be activated (2)");
    }

    // English is the default preferred language
    if ( is_null($preferred_language) || ! $preferred_language )
      $preferred_language = 'EN';

    // create ULTRA user in DB
    
    $params = array(
      "dealer"                => $store,
      'masteragent'           => $sim_data[0]->INVENTORY_MASTERAGENT, // MVNO-2279: overwrite MASTER_AGENT, DISTRIBUTOR
      'distributor'           => $sim_data[0]->INVENTORY_DISTRIBUTOR,
      "userid"                => $userid,
      'preferred_language'    => $preferred_language,
      'cos_id'                => get_cos_id_from_plan('STANDBY'),
      'postal_code'           => $customerZip,
      'country'               => 'USA',
      "plan_state"            => 'Neutral',
      "plan_started"          => 'NULL',
      "plan_expires"          => 'NULL',
      "customer_source"       => 'ORANGE',
      "current_iccid"         => $sim_data[0]->ICCID_NUMBER,
      "current_iccid_full"    => $sim_data[0]->ICCID_FULL,
      "notes"                 => "CH: $activation_channel; INFO: $activation_info"
    );
    
    $result = create_ultra_customer_db_transaction($params);
    if (empty($result['customer']))
      throw new Exception("ERR_API_INTERNAL: cannot create customer.");
    $customer = $result['customer'];
 
    // log in customer

    $_REQUEST['zsession'] = session_save($customer);
    $verified = verify_session( $customer->CUSTOMER );
    $zsession = $verified[2];
    setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
    setcookielive('zsessionC', $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);
    $customer_id = $customer->CUSTOMER_ID;

    $session = new \Session();

    if (!$session->confirm($customer_id))
    {
      throw new Exception('ERR_API_INTERNAL: A problem occurred when trying to create a session.');
    }

    $funcResult = func_add_stored_value_from_orange_sim(
      array(
        'amount'   => $sim_data[0]->STORED_VALUE,
        'customer' => $customer,
        'session'  => $actcode,
        'cos_id'   => get_cos_id_from_plan( func_get_orange_plan_from_sim_data($sim_data[0]) ),
        'detail'   => __FUNCTION__
      )
    );

    if ( ! $funcResult->is_success() )
      throw new Exception("ERR_API_INTERNAL: DB error.");

    log_funding_in_activation_history( $customer->CUSTOMER_ID , 'PREPAID' , $sim_data[0]->STORED_VALUE );

    // save activation attribution info in redis before attempting state transition
    set_redis_provisioning_values($customer->CUSTOMER_ID, $masteragent, $store, $distributor, $store, $userid, $redis);

    // prepare state transition parameters
    
    $context          = array('customer_id' => $customer->CUSTOMER_ID );
    $resolve_now      = FALSE;
    $max_path_depth   = 1;
    $dry_run          = TRUE;
    $plan             = get_plan_name_from_short_name( func_get_orange_plan_from_sim_data($sim_data[0]) );
    $transition_name  = 'Orange Sim Activation '.$plan ;
    
    // Tests State Transition 'Neutral' => 'Active'
    
    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    dlog('',"%s",$result_status);
    
    if ( ! $result_status['success'] )
    {
      // clear activation attribution info from redis
      clear_redis_provisioning_values($customer->CUSTOMER_ID);

      throw new Exception("ERR_API_INTERNAL: state transition error (1)");
    }

    // State Transition is allowed (pre-requisites are met)
    
    $dry_run = FALSE;
    
    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    dlog('',"%s",$result_status);

    if ( ! $result_status['success'] )
    {
      // clear activation attribution info from redis
      clear_redis_provisioning_values($customer->CUSTOMER_ID);

      throw new Exception("ERR_API_INTERNAL: state transition error (2)");
    }

    reserve_iccid($sim_data[0]->ICCID_NUMBER);

    // transition uuid
    $values = array_values($result_status['transitions']);
    $request_id = end($values);
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }  
  
  $success = ! count($errors);

  $fraud_data = array(
    'amount'                 => 0,
    'activation_userid'      => '',
    'cos_id'                 => '',
    'transition_uuid'        => create_guid('PROVISIONING_API')
  );

  $fraud_status = count($errors) > 0 ? 'error' : 'success' ;
  $customer = (isset($customer) && is_object($customer)) ? $customer : null;
  fraud_event($customer, 'provisioning', 'requestProvisionOrangeCustomerAsync', $fraud_status, $fraud_data);

  return flexi_encode(fill_return($p,
      "provisioning__requestProvisionOrangeCustomerAsync",
      func_get_args(),
      array("success"  => $success,
          "request_id" => $request_id,
          "zsession"   => $zsession,
          "customer_id"=> $customer_id,
          "errors"     => $errors)));
}

function getPlanCost($sim)
{
  $map = get_orange_plan_map();
  return get_plan_cost_from_cos_id($map[$sim->OFFER_ID][0]) / 100;
}

?>
