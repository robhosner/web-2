<?php


include_once('cosid_constants.php');
include_once('db.php');
include_once('db/accounts.php');
include_once('db/htt_activation_log.php');
include_once('db/htt_bucket_event_log.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_data_event_log.php');
include_once('db/htt_inventory_sim.php');
include_once('db/htt_portin.php');
include_once('db/htt_ultra_msisdn.php');
include_once('lib/internal/functions.php');
include_once('lib/inventory/functions.php');
include_once('lib/payments/functions.php');
include_once('lib/state_machine/aspider.php');
include_once('lib/state_machine/functions.php');
include_once('lib/transitions.php');
include_once('lib/util-common.php');
include_once('fields.php');
include_once('session.php');
include_once('partner-face/provisioning-partners-include.php');
include_once('portal-public-shipwire.php');
include_once('partner-face/provisioning-public-include.php');
include_once('partner-face/customercare/AddCourtesyCash.php');
include_once('partner-face/customercare/AddCourtesyMinutes.php');
include_once('partner-face/customercare/AddCourtesyStoredValue.php');
include_once('partner-face/customercare/ApplyDataRecharge.php');
include_once('partner-face/customercare/ApplyVoiceRecharge.php');
include_once('partner-face/customercare/AspiderCancelPortIn.php');
include_once('partner-face/customercare/CalculateTaxesFees.php');
include_once('partner-face/customercare/CancelDeviceLocation.php');
include_once('partner-face/customercare/ChangePhoneNumber.php');
include_once('partner-face/customercare/ChargePinCard.php');
include_once('partner-face/customercare/CheckDataRechargeByCustomerId.php');
include_once('partner-face/customercare/CheckInternationalDirectDialSettings.php');
include_once('partner-face/customercare/CheckVoiceRechargeByCustomerId.php');
include_once('partner-face/customercare/CustomerDirectSMS.php');
include_once('partner-face/customercare/CustomerInfo.php');
include_once('partner-face/customercare/FlushStoredBalance.php');
include_once('partner-face/customercare/GetCustomerShipwireOrders.php');
include_once('partner-face/customercare/GetMVNEDetails.php');
include_once('partner-face/customercare/GetBillingHistory.php');
include_once('partner-face/customercare/GetRechargeHistory.php');
include_once('partner-face/customercare/JumpstartToActive.php');
include_once('partner-face/customercare/LoginAsCustomer.php');
include_once('partner-face/customercare/mvneQueryPortIn.php');
include_once('partner-face/customercare/RemoveCash.php');
include_once('partner-face/customercare/RemoveMinutes.php');
include_once('partner-face/customercare/ReplaceSIMCard.php');
include_once('partner-face/customercare/ResetPasswordSMS.php');
include_once('partner-face/customercare/ReshipSIM.php');
include_once('partner-face/customercare/SetCustomerCreditCard.php');
include_once('partner-face/customercare/ShipwireOrderDetails.php');
include_once('partner-face/customercare/ShowInternationalDirectDialErrors.php');
include_once('partner-face/customercare/TransitionProvisionedToActive.php');
include_once('partner-face/customercare/ValidateSIMOrigin.php');
include_once('partner-face/customercare/VoidCourtesyCash.php');
include_once('Ultra/Lib/BoltOn/functions.php');


date_default_timezone_set("America/Los_Angeles");


function customercare__mvneCancelPortIn($partner_tag, $customer_id)
{
  return customercare__AspiderCancelPortIn($partner_tag, $customer_id);
}


function customercare__mvneCancelLocation($partner_tag, $customer_id)
{
  return customercare__CancelDeviceLocation($partner_tag, $customer_id);
}


// Charges the stored CC and puts the balance into the account; then tries to reactivate a suspended account if necessary.
function customercare__ChargeCreditCard($partner_tag, $customer_id, $amount, $reason, $destination)
{
  # $amount in cents

  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;
  $reactivated = FALSE;
  $state       = '';
  $cc_errors   = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__ChargeCreditCard",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "errors"   => array(),
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__ChargeCreditCard", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__ChargeCreditCard",
                                    func_get_args(),
                                    array("success"  => $success,
                                          "warnings" => array(),
                                          "errors"   => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  $upgrade_seconds_gain = 0;
  $session              = create_guid('customercare');

  if ( $customer )
  {
    if ( ! $destination )
      $destination = 'WALLET';

    /* *** get current state *** */
    $state = internal_func_get_state_from_customer_id($customer_id);

    if ( $state )
    {
      $cc_params = array(
        'customer'           => $customer,
        'charge_amount'      => ( $amount / 100 ), // in $
        'detail'             => __FUNCTION__,
        'session'            => $session,
        'reason'             => $destination,
        'description'        => $reason,
        'include_taxes_fees' => TRUE
      );

      // charge customer's credit card and attempt to transition the customer to Active
      $result =
        ( $destination == 'WALLET' )
        ?
        func_add_balance_by_tokenized_cc( $cc_params )
        :
        func_add_stored_value_by_tokenized_cc( $cc_params );

      dlog('',"cc function returns %s",$result);

      if ( $result->is_failure() )
        $errors = $result->get_errors();

      if ( ! count($errors) )
        $reactivated = $result->data_array['activated'];
      else
      {
        #$cc_errors = $return['rejection_errors'];
        $cc_errors = $errors;
      }
    }
    else # cannot determine customer state
     $errors[] = "ERR_API_INTERNAL: customer state could not be determined";
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";

  return flexi_encode(fill_return($p,
                                  "customercare__ChargeCreditCard",
                                  func_get_args(),
                                  array("success"     => ( ! count($errors) ),
                                        "reactivated" => $reactivated,
                                        "warnings"    => array(),
                                        "cc_errors"   => $cc_errors,
                                        "errors"      => $errors)));
}


// Gives the customer increased ILD minutes for calling that will expire at the end of the service period; will not be commissioned.
function customercare__CourtesyMinutes($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $amount)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__CourtesyMinutes",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__CourtesyMinutes", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__CourtesyMinutes",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {

    $return = func_courtesy_add_ild_minutes(
      array(
        'reason'      => $reason,
        'terminal_id' => $agent_name,
        'reference'   => $customercare_event_id,
        'amount'      => $amount,
        'customer'    => $customer,
        'detail'      => __FUNCTION__
      )
    );

    if ( count($return['errors']) == 0 )
    {
      $success = TRUE;
    }
    else
    {
      $errors = $return['errors'];
    }

  }
  else
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";
  }

  return flexi_encode(fill_return($p,
                                  "customercare__CourtesyMinutes",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}


// Gives the customer increased $ that can be used for anything; but will not be commissioned.
function customercare__CourtesyCash($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $amount)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__CourtesyCash",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__CourtesyCash", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__CourtesyCash",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {

    $return = func_courtesy_add_balance(
      array(
        'reason'      => $reason,
        'terminal_id' => $agent_name,
        'reference'   => $customercare_event_id,
        'amount'      => ( $amount ),
        'customer'    => $customer,
        'detail'      => __FUNCTION__
      )
    );

    if ( count($return['errors']) == 0 )
    {
      $success = TRUE;

      /* *** get current state *** */
      $state = internal_func_get_state_from_customer_id($customer_id);

      if ( $state )
      {
         $context = array(
           'customer_id' => $customer->CUSTOMER_ID
         );

         /* *** transition to 'active' if possible *** */

         $result_status = reactivate_suspended_account($state,$customer,$context);
         if ( count($result_status['errors']) > 0 ) # non-fatal errors
         { foreach($result_status['errors'] as $e) { dlog('',$e); } }

         $result_status = activate_provisioned_account($state,$customer,$context);
         if ( count($result_status['errors']) > 0 ) # non-fatal errors
         { foreach($result_status['errors'] as $e) { dlog('',$e); } }

      }
      else # non-fatal error
        dlog('', "We could not determine customer state in customercare__CourtesyCash");

    }
    else
    { $errors = $return['errors']; }

  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__CourtesyCash",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}


?>
