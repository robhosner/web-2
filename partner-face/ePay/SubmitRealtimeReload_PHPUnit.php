<?php

require_once 'classes/PHPUnitBase.php';

class SubmitRealtimeReloadTest extends PHPUnitBase
{
  public function test__externalpayments__SubmitRealtimeReload()
  {

    // API setup
    list($test, $partner, $command) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$command}",
      'bath'      => 'soap',
      'version'   => 1,
      'partner'   => 'ePay',
      'wsdl'      => 'partner-meta/ePay.wsdl'));

    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    print_r($result);

    // test missing phone_number
    $params = array('request_epoch' => time());
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: missing required phone_number', $result->errors);

    // test missing request_epoch
    $params = array('phone_number' => '1001001111');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: missing required request_epoch', $result->errors);

    // test missing store_zipcode
    $params = array(
      'phone_number'  => '1001001111',
      'request_epoch' => time());
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: missing required store_zipcode', $result->errors);

    // test invalid subproduct_id
    $params = array(
      'store_zipcode'     => '12345',
      'terminal_id'       => '12345',
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'sku'               => '12345',
      'load_amount'       => 250,
      'provider_trans_id' => rand(111111111, 999999999),
      'phone_number'      => '1001001111',
      'request_epoch'     => time(),
      'provider_name'     => 'EPAY',
      'subproduct_id'     => 'CC11');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_EPY_INVALID_SUBPRODUCT_ID: {$params['subproduct_id']} is not a valid subproduct_id", $result->errors );

    // test invalid provider ePay SKU
    $params = array(
      'store_zipcode'     => '12345',
      'terminal_id'       => '12345',
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'sku'               => '12345',
      'load_amount'       => 250,
      'provider_trans_id' => rand(111111111, 999999999),
      'phone_number'      => '1001001111',
      'request_epoch'     => time(),
      'provider_name'     => 'EPAY',
      'subproduct_id'     => 'L34');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU {$params['sku']} for provider EPAY", $result->errors);

    // test ePay SKU not matching charge_amount
    $params = array(
      'store_zipcode'     => '12345',
      'terminal_id'       => '12345',
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'sku'               => '00843788028824',
      'load_amount'       => 250,
      'provider_trans_id' => rand(111111111, 999999999),
      'phone_number'      => '1001001111',
      'request_epoch'     => time(),
      'provider_name'     => 'EPAY',
      'subproduct_id'     => 'L34');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid amount {$params['load_amount']} for SKU {$params['sku']}", $result->errors);

    // test insufficinet recharge amount
    $params = array(
      'store_zipcode'     => '12345',
      'terminal_id'       => '12345',
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'sku'               => '00843788015459',
      'load_amount'       => 2900,
      'provider_trans_id' => rand(111111111, 999999999),
      'phone_number'      => '6466510916', // D39 sub
      'request_epoch'     => time(),
      'provider_name'     => 'EPAY',
      'subproduct_id'     => 'L29');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid amount", $result->errors);

    // test excess account balanace
    $params = array(
      'phone_number'      => '6095820782',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'L19',
      'sku'               => '00843788021269',
      'load_amount'       => 1900,
      'provider_trans_id' => '129912991299',
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet. Please use up some of your current balance before adding more.', $result->errors);

    // factored subproduct can fund provisioned and port-in-requested only
    $params = array(
      'phone_number'      => '7203053431', // Active
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'PQ29',
      'sku'               => '00843788026288',
      'load_amount'       => 2900,
      'provider_trans_id' => rand(11111111, 99999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: customer state is invalid for the given call', $result->errors);

    // successfull L39 SKU test
    $params = array(
      'phone_number'      => '9493969912',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'L39',
      'sku'               => '00843788015466',
      'load_amount'       => 3900,
      'provider_trans_id' => '129912991299',
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // successfull open demom test
    $params = array(
      'phone_number'      => '1008562640',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_ADD_BALANCE',
      'sku'               => '00843788026387',
      'load_amount'       => 2000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // RTRIN-44
    $params = array(
      'phone_number'      => '6094018424',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_ADD_BALANCE',
      'sku'               => '00843788026387',
      'load_amount'       => 2000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // API-267: brand now allowed (must fudge isBrandNameAllowed)
    $params = array(
      'phone_number'      => '5625690495',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'PQ24',
      'sku'               => '00843788035471',
      'load_amount'       => 2000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU {$params['sku']} for provider EPAY", $result->errors);

    // API-267: test UV payment to Ultra customer
    $params = array(
      'phone_number'      => '3128710940',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'UV50',
      'sku'               => '00843788035648',
      'load_amount'       => 5000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU {$params['sku']} for provider EPAY", $result->errors);

    // API-267: test Ultra payment to UV customer
    $params = array(
      'phone_number'      => '7202789809',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_ADD_BALANCE',
      'sku'               => '00843788026387',
      'load_amount'       => 2000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU {$params['sku']} for provider EPAY", $result->errors);

    // API-267: test UV subproduct ID
    $params = array(
      'phone_number'      => '7202789809',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'PQ35',
      'sku'               => '00843788035501',
      'load_amount'       => 3500,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    // API-385: test D39
    $params = array(
      'phone_number'      => '8188587803',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'L39',
      'sku'               => '00843788026295',
      'load_amount'       => 3900,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    /*
    // commented because of required 'fudging' comment below
    // API-346 test (must fudge isPaymentProviderBrandValidationEnabled)
    $params = array(
      'phone_number'      => '6467328530', // Active UV20 sub
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_ADD_BALANCE',
      'sku'               => '00843788026264', // Ultra open demon
      'load_amount'       => 2000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'EPAY');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->ultra_payment_trans_id);
    */

    // test IAS payment to UV customer
    $params = array(
      'phone_number'      => '7202789809',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'UV20',
      'sku'               => '200726',
      'load_amount'       => 2000,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'IAS');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( $result->success);

    // test IAS payment to ULTRA customer
    $params = array(
      'phone_number'      => '2027665636',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'L39',
      'sku'               => '200713',
      'load_amount'       => 3900,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'IAS');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( $result->success);


    // test IAS payment to ULTRA customer
    $params = array(
      'phone_number'      => '6573215227',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'S29',
      'sku'               => '200711',
      'load_amount'       => 2900,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'IAS');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( $result->success);

    // test IAS payment to ULTRA customer
    $params = array(
      'phone_number'      => '6573215225',
      'request_epoch'     => time(),
      'store_zipcode'     => 12345,
      'terminal_id'       => 32,
      'product_id'        => 'ULTRA_PLAN_RECHARGE',
      'subproduct_id'     => 'S44',
      'sku'               => '200714',
      'load_amount'       =>  4400,
      'provider_trans_id' => rand(111111111, 999999999),
      'provider_name'     => 'IAS');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( $result->success);
  }
}