<?php

// Test validator for ICCID
function test__Validator0($ICCID)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "test__Validator0",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "test__Validator0", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "test__Validator0",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "test__Validator0",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));

}

function test__Validator5($lcheck_whole, $lcheck19)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "test__Validator5",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "test__Validator5", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "test__Validator5",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "test__Validator5",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));
}


function test__Validator6($lcheck_whole, $lcheck19)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "test__Validator6",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "test__Validator6", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "test__Validator6",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "test__Validator6",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));
}

function test__Validator7($a,$b)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
        "test__Validator7",
        func_get_args(),
        array("success" => TRUE,
            "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "test__Validator7", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
        "test__Validator7",
        func_get_args(),
        array("success" => FALSE,
            "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
      "test__Validator7",
      func_get_args(),
      array("success" => TRUE,
          "errors" => $errors)));
}

// Test validators!
function test__Validators($mfunds_person, $mfunds_parent, $phone_number, $ultra_customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "test__Validators",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "test__Validators", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                    "test__Validators",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "test__Validators",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));

}

