<?php

/**
 * Prepares promotional SIMs.
 * 
 * @param integer $promo_id      -- ULTRA.PROMOTIONAL_PLANS.ULTRA_PROMOTIONAL_PLANS_ID
 * @param integer $prepare_count -- maximum number of promo customers to prepare. Default is 15, maximum is 100.
 */
function promotional__PreparePromoCustomers($partner_tag, $promo_id, $prepare_count)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ( ! $prepare_count ) $prepare_count = 15;

  $semaphore_key = 'promotional/PreparePromoCustomers/'.$promo_id;
  
  dlog('',"promo_id = $promo_id ; prepare_count = $prepare_count");

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "promotional__PreparePromoCustomers",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $redis = new \Ultra\Lib\Util\Redis;

  $errors = validate_params($p, "promotional__PreparePromoCustomers", func_get_args(), $mock);

  if ( $redis->get( $semaphore_key ) )
    $errors[] = "ERR_API_INTERNAL: Another process is running for promo_id $promo_id, please try later";
  
  if ($errors && count($errors) > 0)
  {
    $redis->del( $semaphore_key );
 
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "promotional__PreparePromoCustomers",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));
  }
  
  $redis->set( $semaphore_key , 1 , 15*60 );
  
  try
  {
    teldata_change_db();

    // system load sanity check: max 25 open transitions
    $htt_transition_log_open_count = htt_transition_log_open_count();

    if ( $htt_transition_log_open_count > 50 )
      throw new Exception("ERR_API_INTERNAL: too many OPEN transitions ($htt_transition_log_open_count), please retry later.");

    // load ULTRA.PROMOTIONAL_PLANS data
    $promo_data = get_ultra_promotional_plans_by_id( $promo_id );

    if ( ( ! $promo_data ) || ( ! is_array($promo_data)) || ( ! count($promo_data) ) )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: promotional plan not found");
    
    dlog('',"ULTRA.PROMOTIONAL_PLANS data = %s",$promo_data);

    $promo_data = $promo_data[0];
    
    // extract and validate Zipcode List
    list( $zipcodes , $error ) = extractZipcodesFromString( $promo_data->ZIPRANGE );
    
    if ( $error )
      throw new Exception( $error );

    dlog('',"zipcodes = %s",$zipcodes);

    if ( ! validateZipCodesCoverage( $zipcodes ) )
      throw new Exception("ERR_API_INTERNAL: one or more zip codes is not in coverage");

    // load HTT_INVENTORY_SIM data in range ( ICCID_START , ICCID_END )
    $sim_data = get_htt_inventory_sims_by_range( $promo_data->ICCID_START , $promo_data->ICCID_END );

    if ( ( ! $sim_data ) || ( ! is_array($sim_data) ) || ( ! count($sim_data) ) )
      throw new Exception("ERR_API_INTERNAL: No SIMs found in range ( ".$promo_data->ICCID_START." , ".$promo_data->ICCID_END." )");

    if ( $promo_data->PROMO_STATUS == 'VIRGIN' )
    {
      $used_count   = 0;
      $start_exists = FALSE;
      $end_exists   = FALSE;
      $error        = NULL;

      // loop though SIMS in the promo range
      foreach( $sim_data as $data )
      {
        if ( ( $data->SIM_ACTIVATED ) || ( $data->CUSTOMER_ID ) )
          $used_count++;

        if ( $promo_data->ICCID_START == $data->ICCID_FULL )
          $start_exists = TRUE;

        if ( $promo_data->ICCID_END   == $data->ICCID_FULL )
          $end_exists   = TRUE;

        // ensure that all SIMs are Hot
        if ( ! $data->SIM_HOT )
        {
          if ( $error ) $errors[] = $error;
          $error = "ERR_API_INTERNAL: SIM ".$data->ICCID_FULL." is not Hot";
          dlog('',$error);
        }

        // ensure that all SIMs are not expired
        if ( $data->is_expired )
        {
          if ( $error ) $errors[] = $error;
          $error = "ERR_API_INTERNAL: SIM ".$data->ICCID_FULL." is expired";
          dlog('',$error);
        }
      }

      if ( $error )
        throw new Exception( $error );

      // ensure that all SIMs are unused

      if ( $used_count )
        throw new Exception("ERR_API_INTERNAL: promo status is 'VIRGIN', but $used_count SIMS in range are already used.");

      // make sure that the SIM start and end exist

      if ( ! $start_exists )
        throw new Exception("ERR_API_INTERNAL: SIM range start does not exist.");

      if ( ! $end_exists )
        throw new Exception("ERR_API_INTERNAL: SIM range end does not exist.");

      // ensure sparse zip codes distribution
      $zipValidationResult = validate_sim_zip_code_distribution( $promo_data->ICCID_START , $promo_data->ICCID_END , $zipcodes );

      if ( $zipValidationResult->is_failure() )
      {
        $errors = $zipValidationResult->get_errors();

        if ( isset($errors[0]) && $errors[0] )
        {
          throw new Exception( $errors[0] );
        }
        else
        {
          throw new Exception( "ERR_API_INTERNAL: Zip Codes distribution is invalid" );
        }
      }

      // set ULTRA.PROMOTIONAL_PLANS.PROMO_STATUS to 'PREPARATION'
      if ( ! set_ultra_promotional_plan_status( $promo_id , 'PREPARATION' ) )
        throw new Exception("ERR_API_INTERNAL: DB error, could not update ULTRA.PROMOTIONAL_PLANS");
    }
    else // ( $promo_data->PROMO_STATUS == 'PREPARATION' )
    {
      // Promo SIMs with SIM_ACTIVATED = 0 and no CUSTOMER_ID
      $unused_sims = array();

      foreach( $sim_data as $data )
      {
        if ( ( ! $data->SIM_ACTIVATED ) && ( ! $data->CUSTOMER_ID ) )
          $unused_sims[] = $data;
      }

      if ( count($unused_sims) )
      {
        $sims_to_be_processed = array();

        // If the status is PREPARATION, find all ICCIDs still to be processed:
        //   - exclude if in htt_customers_overlay_ultra.current_iccid
        //   - exclude if in htt_customers_overlay_ultra.ACTIVATION_ICCID
        //   - exclude if htt_inventory_sim.SIM_ACTIVATED is 1
        // If this list is empty, throw an error that some sims are in an errored state or still processing.

        foreach( $unused_sims as $data )
        {
          $customer_match_by_sim = ultra_customer_match_by_sim( $data->ICCID_FULL );

          if ( $customer_match_by_sim )
          {
            dlog('',"ICCID %s already assigned to a customer",$data->ICCID_FULL);
          }
          else
          {
            $sims_to_be_processed[] = $data;
            if (count($sims_to_be_processed) > $prepare_count) // collect one more than needed.
            {
              break;
            }
          }
        }

        if ( count($sims_to_be_processed) )
        {
          // activate $prepare_count SIMS

          $count_processed = 0;

          foreach( $sims_to_be_processed as $data )
          {
            // we want to process maximum $prepare_count SIMs
            if ( $count_processed < $prepare_count )
            {
              $postal_code_id = array_rand($zipcodes, 1);

              // creates a new customer, associate it with the SIM, transition to 'Promo Unused'
              $result = internal_func_promotional_sim_init(
                array(
                  'ultra_promotional_data' => $promo_data,
                  'postal_code'            => $zipcodes[$postal_code_id], // a random zip out of ULTRA.PROMOTIONAL_PLANS.ZIPRANGE
                  'iccid'                  => $data->ICCID_FULL
                )
              );

              if ( $result->has_errors() )
                $errors = array_merge( $errors , $result->get_errors() );

              $count_processed++;
            }
          }

          dlog('',"count_processed = $count_processed");
        }
        else
        {
          throw new Exception("ERR_API_INTERNAL: some sims are in an errored state or still processing");
        }
      }
      else // all the SIMs are used
      {
        // If all the SIMs are used from htt_inventory_sim, set ULTRA.PROMOTIONAL_PLANS.PROMO_STATUS to 'READY'
        if ( ! set_ultra_promotional_plan_status( $promo_id , 'READY' ) )
          throw new Exception("ERR_API_INTERNAL: DB error, could not update ULTRA.PROMOTIONAL_PLANS");

        // If ULTRA.PROMOTIONAL_PLANS.ACTIVATION_MASTERAGENT = 54 or NULL
        // set  HTT_INVENTORY_SIM.INVENTORY_MASTERAGENT to ULTRA.PROMOTIONAL_PLANS.ACTIVATION_MASTERAGENT  
        // set  HTT_INVENTORY_SIM.INVENTORY_DISTRIBUTOR to $promo_data->ICCID_START
        // set  HTT_INVENTORY_SIM.INVENTORY_DEALER      to $promo_data->ICCID_END
        if ( ( ! $promo_data->ACTIVATION_MASTERAGENT ) || ( $promo_data->ACTIVATION_MASTERAGENT == 54 ) )
        {
          $status = htt_inventory_sim_update_range(
            array(
              'inventory_masteragent' => $promo_data->ACTIVATION_MASTERAGENT,
              'inventory_distributor' => $promo_data->ICCID_START,
              'inventory_dealer'      => $promo_data->ICCID_END,
              'iccid_full_from'       => $promo_data->ICCID_START,
              'iccid_full_to'         => $promo_data->ICCID_END
            )
          );

          if ( ! $status )
            throw new Exception("ERR_API_INTERNAL: DB error, could not update HTT_INVENTORY_SIM");
        }
      }
    }
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  $success = ! count($errors);

  $redis->del( $semaphore_key );

  return flexi_encode(fill_return($p,
                                  "promotional__PreparePromoCustomers",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
