<?php

// Kicksoff the state transition from 'Promo Used' to 'Active'.
function promotional__ActivatePromotionalCustomer($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "promotional__ActivatePromotionalCustomer",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "promotional__ActivatePromotionalCustomer", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "promotional__ActivatePromotionalCustomer",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));
  }

  try
  {
    teldata_change_db();

    $customer = get_customer_from_customer_id($customer_id);

    if ( ! $customer )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer not found.");

    // customer must be in 'Promo Unused' state
    $state = internal_func_get_state_from_customer_id( $customer->CUSTOMER_ID );

    if ( ! $state )
      throw new Exception("ERR_API_INTERNAL: customer state could not be determined.");

    if ( $state['state'] != 'Promo Unused' )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer is not in state 'Promo Unused'");

    $context = array( 'customer_id' => $customer->CUSTOMER_ID );

    $result_status = activate_promo_account($state,$customer,$context);

    $errors = $result_status['errors'];
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "promotional__ActivatePromotionalCustomer",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
